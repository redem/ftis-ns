/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_PutawayLocationException.js,v $
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Cart_PutawayLocationException.js,v $
 * Revision 1.2.2.9.4.19.2.55.2.3  2015/12/01 21:48:42  rrpulicherla
 * LP issue fixes
 *
 * Revision 1.2.2.9.4.19.2.55.2.2  2015/11/06 14:13:13  snimmakayala
 * 201415040
 *
 * Revision 1.2.2.9.4.19.2.55.2.1  2015/10/01 12:44:56  schepuri
 * case# 201412136
 *
 * Revision 1.2.2.9.4.19.2.55  2015/07/15 13:31:15  schepuri
 * case# 201413358
 *
 * Revision 1.2.2.9.4.19.2.54  2015/06/30 13:22:43  schepuri
 * case# 201413169
 *
 * Revision 1.2.2.9.4.19.2.53  2015/02/18 13:33:18  schepuri
 * issue# 201411170
 *
 * Revision 1.2.2.9.4.19.2.52  2014/11/05 10:41:25  sponnaganti
 * Case# 201410938
 * True Fab SB Issue fixed
 *
 * Revision 1.2.2.9.4.19.2.51  2014/11/03 16:33:37  gkalla
 * case # 201410852
 * CT item receipt issue fix
 *
 * Revision 1.2.2.9.4.19.2.50  2014/09/23 14:31:22  snimmakayala
 * Case: 20149505
 * Merge FIFO dates
 *
 * Revision 1.13.2.16.4.14.2.58.2.10  2014/09/23 14:20:11  snimmakayala
 * Case: 20149505
 * Merge FIFO dates
 *
 * Revision 1.13.2.16.4.14.2.58.2.9  2014/08/20 15:39:41  skavuri
 * Case# 201410051 Std Bundle issue fixed
 *
 * Revision 1.13.2.16.4.14.2.58.2.8  2014/08/14 10:17:51  sponnaganti
 * case# 20149953 20149954 20149998
 * stnd bundle issue fix
 *
 * Revision 1.13.2.16.4.14.2.58.2.7  2014/08/08 14:52:46  skreddy
 * case # 20149875
 * True Fabrications SB issue fix
 *
 * Revision 1.13.2.16.4.14.2.58.2.6  2014/08/07 05:53:42  skreddy
 * case # 20149850
 * True Fabrications SB issue fix
 *
 * Revision 1.13.2.16.4.14.2.58.2.5  2014/08/05 15:25:31  nneelam
 * case#  20149815
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.13.2.16.4.14.2.58.2.4  2014/07/25 14:04:55  grao
 * Case#: 20149579 Dealmed issue fixes
 *
 * Revision 1.13.2.16.4.14.2.58.2.3  2014/07/07 07:19:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149294
 *
 * Revision 1.13.2.16.4.14.2.58.2.2  2014/07/01 15:40:58  skavuri
 * Case# 20149154 Compatibility Issue fixed
 *
 * Revision 1.13.2.16.4.14.2.58.2.1  2014/07/01 15:33:57  skavuri
 * Case# 20149154 Compatibility Issue fixed
 *
 * Revision 1.13.2.16.4.14.2.58  2014/06/24 15:51:09  skavuri
 * Case# 20149026 New-UI Issue Fixed
 *
 * Revision 1.13.2.16.4.14.2.57  2014/06/19 15:21:41  skreddy
 * case # 20149008
 * nautilus SB issue fix
 *
 * Revision 1.13.2.16.4.14.2.56  2014/06/13 15:17:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Alphacommchanges
 *
 * Revision 1.13.2.16.4.14.2.55  2014/06/13 06:33:56  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.13.2.16.4.14.2.54  2014/06/06 12:18:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.13.2.16.4.14.2.53  2014/06/06 06:47:39  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.13.2.16.4.14.2.52  2014/06/03 13:57:11  skreddy
 * case # 20148611
 * Sonic SB issue fix
 *
 * Revision 1.13.2.16.4.14.2.51  2014/05/30 15:28:58  sponnaganti
 * case# 20148431
 * Stnd Bundle Issue Fix
 *
 * Revision 1.13.2.16.4.14.2.50  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.13.2.16.4.14.2.49  2014/05/16 15:10:59  skavuri
 * Case # 20148400 SB issue fixed
 *
 * Revision 1.13.2.16.4.14.2.48  2014/04/14 16:17:20  gkalla
 * case#20147944
 * Issue with both qty and location exceptions performed at  a time
 *
 * Revision 1.13.2.16.4.14.2.47  2014/03/25 15:34:04  skavuri
 * Case # 20127832 issue Fixed
 *
 * Revision 1.13.2.16.4.14.2.46  2014/02/13 15:10:27  rmukkera
 * Case # 20126900
 *
 * Revision 1.13.2.16.4.14.2.45  2014/01/31 13:32:06  schepuri
 * 20126968
 * standard bundle issue
 *
 * Revision 1.13.2.16.4.14.2.44  2014/01/09 12:35:17  schepuri
 * 20126720
 *
 * Revision 1.13.2.16.4.14.2.43  2014/01/08 13:52:59  schepuri
 * 20126691
 *
 * Revision 1.13.2.16.4.14.2.42  2014/01/06 13:12:59  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.13.2.16.4.14.2.41  2013/12/12 14:11:34  schepuri
 * 20125922
 *
 * Revision 1.13.2.16.4.14.2.40  2013/12/05 15:22:37  skreddy
 * Case# 20125918
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.13.2.16.4.14.2.39  2013/12/02 10:29:57  snimmakayala
 * CASE#:20125764
 * MHP: PO2TO Conversion.
 *
 * Revision 1.13.2.16.4.14.2.38  2013/12/02 10:09:00  snimmakayala
 * CASE#:20125764
 * MHP: PO2TO Conversion.
 *
 * Revision 1.13.2.16.4.14.2.37  2013/11/29 15:05:36  grao
 * Case# 20125980  related issue fixes in SB 2014.1
 *
 * Revision 1.13.2.16.4.14.2.36  2013/11/25 15:21:06  grao
 * Case# 20125919 related issue fixes in SB 2014.1
 *
 * Revision 1.13.2.16.4.14.2.35  2013/11/06 15:25:05  rmukkera
 * Case # 20125556
 *
 * Revision 1.13.2.16.4.14.2.34  2013/11/05 15:25:06  rmukkera
 * Case# 20125555
 *
 * Revision 1.13.2.16.4.14.2.33  2013/10/17 06:51:44  gkalla
 * Case# 20125023
 * Ryonet item receipt posting succesfully but failing at eBiz
 *
 * Revision 1.13.2.16.4.14.2.32  2013/10/09 15:40:39  rmukkera
 * Case# 20124670
 *
 * Revision 1.13.2.16.4.14.2.31  2013/09/30 15:58:09  rmukkera
 * Case#  20124588
 *
 * Revision 1.13.2.16.4.14.2.30  2013/09/17 06:09:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.13.2.16.4.14.2.29  2013/09/02 15:38:20  rmukkera
 * Case# 20124173
 *
 * Revision 1.13.2.16.4.14.2.28  2013/08/30 16:41:23  skreddy
 * Case# 20124154
 * standard bundle issue fix
 *
 * Revision 1.13.2.16.4.14.2.27  2013/08/29 15:55:19  nneelam
 * Case#.20124118
 * RF Location Exception, serial item item receipt failed
 *
 * Revision 1.13.2.16.4.14.2.26  2013/08/05 16:59:32  skreddy
 * Case# 20123720
 * issue rellated to location exception
 *
 * Revision 1.13.2.16.4.14.2.25  2013/08/05 13:44:08  rmukkera
 * Standard bundle Issue Fix for  case NO 20123733
 *
 * Revision 1.13.2.16.4.14.2.24  2013/07/26 21:14:35  grao
 * Case# 20123593
 * Inventory is not updated with new LP in Location exception related issues fixes
 *
 * Revision 1.13.2.16.4.14.2.23  2013/07/26 21:05:31  grao
 * no message
 *
 * Revision 1.13.2.16.4.14.2.22  2013/07/25 08:53:50  rmukkera
 * landed cost cr changes
 *
 * Revision 1.13.2.16.4.14.2.21  2013/07/11 14:43:55  gkalla
 * Case# 20123379
 * GFT Not updating lot no in create inventory after loc exception
 *
 * Revision 1.13.2.16.4.14.2.20  2013/07/04 18:49:50  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixed against the case for PMM invt sync.
 * We are not checking weather binloc is active or not.
 *
 * Revision 1.13.2.16.4.14.2.19  2013/06/28 15:42:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  Case# 20123242
 * Surftech ssue
 *
 * Revision 1.13.2.16.4.14.2.18  2013/06/27 16:33:31  gkalla
 * Case# 20123115, Case# 20123138
 * Standard bundle Issue Fix
 *
 * Revision 1.13.2.16.4.14.2.17  2013/06/14 13:16:38  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Issue Fixed for Standard Bundle not displaying the putaway Location exception screen
 *
 * Revision 1.13.2.16.4.14.2.16  2013/06/13 15:50:35  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.13.2.16.4.14.2.15  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.13.2.16.4.14.2.14  2013/06/05 22:11:22  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.13.2.16.4.14.2.13  2013/05/22 15:13:33  grao
 * While Doing Location Exception Inventory is Updated with Old Lp related issues fixed
 *
 * Revision 1.13.2.16.4.14.2.12  2013/05/21 07:27:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.13.2.16.4.14.2.11  2013/05/16 11:25:03  spendyala
 * CASE201112/CR201113/LOG201121
 * InvtRef# field of opentask record will be updated
 * to putaway task, once inbound process in completed
 *
 * Revision 1.13.2.16.4.14.2.10  2013/05/14 14:17:28  schepuri
 * Surftech issues of serial status update
 *
 * Revision 1.13.2.16.4.14.2.9  2013/05/03 15:37:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.13.2.16.4.14.2.8  2013/04/30 23:30:47  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.13.2.16.4.14.2.7  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.13.2.16.4.14.2.6  2013/04/03 01:37:59  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.13.2.16.4.14.2.5  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.13.2.16.4.14.2.4  2013/03/07 14:25:52  gkalla
 * CASE201112/CR201113/LOG201121
 * Chenaged the code by Lexjet Code review
 *
 * Revision 1.13.2.16.4.14.2.3  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.13.2.16.4.14.2.2  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.13.2.16.4.14.2.1  2013/02/26 12:52:10  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.13.2.16.4.14  2013/02/18 06:24:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Issue related to FIFO date
 *
 * Revision 1.13.2.16.4.13  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.13.2.16.4.12  2013/02/01 15:22:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.13.2.16.4.11  2013/01/08 15:41:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet issue for batch entry irrespective of location
 *
 * Revision 1.13.2.16.4.10  2012/12/03 15:40:36  rmukkera
 * CASE201112/CR201113/LOG2012392
 * UOM conversions code added
 *
 * Revision 1.13.2.16.4.9  2012/11/09 14:14:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.13.2.16.4.8  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.13.2.16.4.7  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.13.2.16.4.6  2012/10/11 14:50:02  grao
 * no message
 *
 * Revision 1.13.2.16.4.5  2012/10/01 05:18:14  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code from 2012.2.
 *
 * Revision 1.13.2.16.4.4  2012/09/27 11:00:10  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.13.2.16.4.3  2012/09/26 22:43:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.13.2.16.4.2  2012/09/24 22:45:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues related to Expiry date is resolved.
 *
 * Revision 1.13.2.16.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.13.2.16  2012/09/03 13:45:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.13.2.15  2012/08/30 01:59:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.13.2.14  2012/07/10 23:26:24  gkalla
 * CASE201112/CR201113/LOG201121
 * JAE invalid location issue
 *
 * Revision 1.13.2.13  2012/06/26 06:53:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Moving Putaway Task to closed task.
 *
 * Revision 1.13.2.12  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.13.2.11  2012/04/30 10:00:47  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal'
 *
 * Revision 1.13.2.10  2012/04/02 12:48:45  spendyala
 * CASE201112/CR201113/LOG201121
 * set true lot Value in our custom records depending upon Rule value.
 *
 * Revision 1.13.2.9  2012/03/27 00:16:32  gkalla
 * CASE201112/CR201113/LOG201121
 * solved the issue if we click F7 button in Location exc we are getting error
 *
 * Revision 1.13.2.8  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.13.2.7  2012/02/22 12:38:25  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.13.2.6  2012/02/16 14:09:05  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating  inventory records  order# field is updating with purchase order#.
 *
 * Revision 1.13.2.5  2012/02/14 14:09:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.13.2.4  2012/02/13 23:21:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.16  2012/02/13 23:11:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.15  2012/01/20 19:02:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Confirm Putaway Issue
 *
 * Revision 1.14  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.13  2011/12/28 13:21:33  spendyala
 * CASE201112/CR201113/LOG201121
 * resolved issues related to qty  exception while entering data into transaction order line table
 *
 * Revision 1.12  2011/12/28 06:57:49  spendyala
 * CASE201112/CR201113/LOG201121
 * made changes for location  exception
 *
 * Revision 1.11  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.10  2011/10/25 14:05:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO RF files
 *
 * Revision 1.9  2011/09/12 07:04:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/08/24 12:43:43  schepuri
 * CASE201112/CR201113/LOG201121
 * RF Putaway Location Exception based on putseq no
 *
 * Revision 1.7  2011/08/24 10:10:52  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/07/20 06:06:59  schepuri
 * CASE201112/CR201113/LOG201121
 * Loc Exception in RF
 *
 * Revision 1.5  2011/07/19 12:34:51  schepuri
 * CASE201112/CR201113/LOG201121
 * Loc Exception in RF
 *
 * Revision 1.4  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PutawayLocationException(request, response)
{

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') 
	{
		//	Get the LP#, Quantity, Location 
		//  from the previous screen, which is passed as a parameter	
		var getLPNo = request.getParameter('custparam_lpno');
		var getQuantity = request.getParameter('custparam_quantity');
		var getFetchedLocation = request.getParameter('custparam_location');
		var getFetchedItem = request.getParameter('custparam_item');
		var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		var exceptionqty = request.getParameter('custparam_exceptionquantity');
		var getRecordCount = request.getParameter('custparam_recordcount');
		var getFetchedLocationId = request.getParameter('custparam_location');
		var getPONo = request.getParameter('custparam_pono');
		var getLineNo = request.getParameter('custparam_lineno');
		var getRecordId = request.getParameter('custparam_recordid');
		var getCartLPNo = request.getParameter('custparam_cartno');
		if(getFetchedItem !=null && getFetchedItem !='')
			var itemName = nlapiLookupField('item', getFetchedItem, 'itemid');
		var getWHLocation = request.getParameter('custparam_whlocation'); 

		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		nlapiLogExecution('DEBUG', 'getRecordCount', getRecordCount);
		nlapiLogExecution('DEBUG', 'getWHLocation', getWHLocation);


		var getconfirmedLPCount = request.getParameter('custparam_confirmedLpCount');
		var getlpCount = request.getParameter('custparam_lpCount');
		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('DEBUG', 'confirmedLPCount ', getconfirmedLPCount);
		nlapiLogExecution('DEBUG', 'lpCount ', getlpCount);
		nlapiLogExecution('DEBUG', 'TempLPNoArray ', TempLPNoArray);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "";
			st1 = "INGRESAR / ESCANEAR DE LA UBICACI&#211;N REAL";
			st2 = "RAZ&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN ACT LOCATION";
			st2 = "REASON";
			st3 = "SEND";
			st4 = "PREV";

		}


		var getOptedField = request.getParameter('custparam_option');

		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterlocation').focus();";     

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "<body>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + "></td>";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocation' value=" + getFetchedLocation + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItem' value=" + getFetchedItem + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItemDescription' value=" + getFetchedItemDescription + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocationId' value=" + getFetchedLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordCount' value=" + getRecordCount + "></td>";
		html = html + "				<input type='hidden' name='hdnconfirmedLPCount' value=" + getconfirmedLPCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpCount' value=" + getlpCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + "></td>";
		html = html + "				<input type='hidden' name='hdnCartLPNo' value='" + getCartLPNo + "'>";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnLineNo' value=" + getLineNo + ">";
		html = html + "				<input type='hidden' name='hdnPONo' value=" + getPONo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnWHLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnexceptionqty' value=" + exceptionqty + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " : ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreason' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + "  <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st4 + "  <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		// This variable is to hold the LP entered.
		var getLocation = request.getParameter('enterlocation');
		nlapiLogExecution('DEBUG', 'getLocation', getLocation);

		var WHLocation = request.getParameter('hdnWHLocation');
		nlapiLogExecution('DEBUG', 'WHLocation', WHLocation);

		var getReason = request.getParameter('enterreason');
		nlapiLogExecution('DEBUG', 'getReason', getReason);

		var getLPNo = request.getParameter('hdnLPNo');	//request.getParameter('custparam_lpno');
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);		

		var getActQuantity = request.getParameter('custparam_quantity');

		var getRecordCount = request.getParameter('hdnRecordCount');
		nlapiLogExecution('DEBUG', 'getRecordCount', getRecordCount);

		var getRecordId = request.getParameter('hdnRecordId');
		nlapiLogExecution('DEBUG', 'getRecordId', getRecordId);


		//var exceptionqty=request.getParameter('custparam_exceptionquantity');
		var exceptionqty=request.getParameter('hdnexceptionqty');
		nlapiLogExecution('DEBUG', 'exceptionqty', exceptionqty);

		var exceptionqtyflag=request.getParameter('custparam_exceptionQuantityflag');
		nlapiLogExecution('DEBUG', 'exceptionqtyflag', exceptionqtyflag);

		var getCartLPNo = request.getParameter('custparam_cartno');

		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		//case 20126355 start : added lp parameter
		POarray["custparam_lpno"] = getLPNo;
		//case 20126355 end
		//added because of we are not getting values from error page 

		POarray["custparam_exceptionquantity"] = request.getParameter('hdnexceptionqty');
		POarray["custparam_item"] = request.getParameter('custparam_item');
		POarray["custparam_location"] = request.getParameter('custparam_location');
		POarray["custparam_itemDescription"] = request.getParameter('custparam_itemDescription');
		POarray["custparam_recordcount"] = request.getParameter('custparam_recordcount');
		POarray["custparam_pono"] = request.getParameter('custparam_pono');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_recordid"] = request.getParameter('custparam_recordid');


		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st5;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st5 = "UBICACI&#211;N NO V&#193;LIDA";

		}
		else
		{
			st5 = "INVALID LOCATION";

		}


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);
		filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');
		columns[1] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[3] = new nlobjSearchColumn('custrecord_sku');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[5] = new nlobjSearchColumn('custrecord_skudesc');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[7] = new nlobjSearchColumn('custrecord_line_no');
		columns[8] = new nlobjSearchColumn('custrecord_sku_status');
		columns[9] = new nlobjSearchColumn('custrecord_packcode');
		columns[10] = new nlobjSearchColumn('custrecord_batch_no');
		columns[11] = new nlobjSearchColumn('custrecord_wms_location');
		columns[12] = new nlobjSearchColumn('custrecord_ebizmethod_no');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		POarray["custparam_whlocation"] = WHLocation;
		POarray["custparam_error"] = st5;
		POarray["custparam_whlocation"] = WHLocation;
		POarray["custparam_screenno"] = 'CRT12';
		POarray["custparam_cartno"] = request.getParameter('custparam_cartno');
		POarray["custparam_confirmedLpCount"] = request.getParameter('hdnconfirmedLPCount');
		POarray["custparam_item"] = request.getParameter('custparam_item');
		nlapiLogExecution('DEBUG', 'Screen #', POarray["custparam_screenno"]);
		nlapiLogExecution('DEBUG', 'hdnconfirmedLPCount', request.getParameter('hdnconfirmedLPCount'));


		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				if (optedEvent == 'F7') 
				{
					POarray["custparam_confirmedLpCount"] = parseInt(request.getParameter('hdnconfirmedLPCount'))-1;
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
					nlapiLogExecution('DEBUG', 'F7 is selected', 'Success');
				}
				else 
				{
					nlapiLogExecution('DEBUG', 'getLocation', getLocation);
					if (getLocation != null && getLocation != '') 
					{
						nlapiLogExecution('DEBUG', 'inside getLocation', getLocation);
						nlapiLogExecution('DEBUG', 'searchresults', searchresults);
						if (searchresults != null && searchresults.length > 0) {
							nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

							var getLPId = searchresults[0].getId();
							nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);

							// Load a record into a variable from opentask table for the LP# entered
//							var PORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getLPId);

							POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
							POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
							POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
							POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
							POarray["custparam_itemid"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
							POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
							POarray["custparam_pointernalid"] = searchresults[0].getValue('custrecord_ebiz_cntrl_no');
							POarray["custparam_polinenumber"] = searchresults[0].getValue('custrecord_line_no');
							POarray["custparam_poitemstatus"] = searchresults[0].getValue('custrecord_sku_status');
							POarray["custparam_popackcode"] = searchresults[0].getValue('custrecord_packcode');
							POarray["custparam_pono"] = request.getParameter('hdnPONo');
							POarray["custparam_polotbatchno"] = searchresults[0].getValue('custrecord_batch_no');
							POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
							POarray["custparam_confirmedLpCount"] = request.getParameter('hdnconfirmedLPCount');
							POarray["custparam_lpCount"] = request.getParameter('hdnlpCount');
							POarray["custparam_recordid"] = getLPId;
							POarray["custparam_recordcount"] = getRecordCount;
							nlapiLogExecution('DEBUG', 'getLPId', getLPId);
							//POarray["custparam_optedEvent"] = optedEvent;
							nlapiLogExecution('DEBUG', 'getRecordCount', POarray["custparam_recordcount"]);

							var whloc=searchresults[0].getValue('custrecord_wms_location');
							var methodid=searchresults[0].getValue('custrecord_ebizmethod_no');


							nlapiLogExecution('DEBUG', 'custparam_confirmedLpCount', POarray["custparam_confirmedLpCount"]);
							nlapiLogExecution('DEBUG', 'custparam_lpCount', POarray["custparam_lpCount"]);
							nlapiLogExecution('DEBUG', 'custparam_optedEvent', POarray["custparam_optedEvent"]);

							nlapiLogExecution('DEBUG', 'geteBizControlNo', POarray["custparam_pointernalid"]);
							nlapiLogExecution('DEBUG', 'geteBizSKUNo', POarray["custparam_itemid"]);
							nlapiLogExecution('DEBUG', 'Line Number is', POarray["custparam_polinenumber"]);

//							if (getOptedField == 4) {
							//POarray["custparam_location"] = getLocation;
//							}
							//nlapiLogExecution('DEBUG', 'getLocation', getLocation);
							//var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation),new nlobjSearchColumn('name'));
							nlapiLogExecution('DEBUG', 'getLocation', getLocation);
							nlapiLogExecution('DEBUG', 'WHLocation', WHLocation);
							var binlocfilters = new Array();					
							binlocfilters[0] = new nlobjSearchFilter('name', null, 'is', getLocation);
							binlocfilters[1] = new nlobjSearchFilter('isinactive', null, 'is', "F");
							if(WHLocation!=null && WHLocation!='')
								binlocfilters[2] = new nlobjSearchFilter('custrecord_ebizsitelocf',null, 'anyof',[WHLocation]);
							binlocfilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));// case# 201411170
							var ItemColumns = new Array();
							ItemColumns.push(new nlobjSearchColumn('name'));
							var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,binlocfilters,ItemColumns);
							nlapiLogExecution('DEBUG', 'BinLocationSearch', BinLocationSearch);
							//	if the previous button 'F7' is clicked, it has to go to the previous screen 
							//  ie., it has to go to accept PO #.

							if (BinLocationSearch != null && BinLocationSearch != '')
							{
								var EndLocationId = '';
								POarray["custparam_beginlocation"] = BinLocationSearch[0].getValue('name');
								nlapiLogExecution('DEBUG', 'Location Name is', POarray["custparam_beginlocation"]);

								POarray["custparam_beginlocationinternalid"] = BinLocationSearch[0].getId();
								nlapiLogExecution('DEBUG', 'Begin Location Internal Id', POarray["custparam_beginlocationinternalid"]);

								var getBeginLocationInternalId = POarray["custparam_beginlocationinternalid"];
								//case# 20148431 starts
								itemDimensions = getSKUCubeAndWeight(POarray["custparam_itemid"],"",WHLocation);
								var itemCube = itemDimensions[0];

								var TotalItemCube = parseFloat(itemCube) * parseFloat(POarray["custparam_quantity"]);
								nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );						

								var binLocationRemainingCube = GeteLocCube(getBeginLocationInternalId);

								var remainingCube = parseFloat(binLocationRemainingCube) - parseFloat(TotalItemCube);
								if(remainingCube>0)
								{
									//case# 20148431 ends
									//var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation));
									var LocationSearch =BinLocationSearch;
									if (LocationSearch!=null && LocationSearch.length != 0) {
										nlapiLogExecution('DEBUG', 'Length of Location Search', LocationSearch.length);

										for (var s = 0; s < LocationSearch.length; s++) {
											EndLocationId = LocationSearch[s].getId();
											nlapiLogExecution('DEBUG', 'End Location Id', EndLocationId);
										}
									}

									var trantype = nlapiLookupField('transaction', POarray["custparam_pointernalid"], 'recordType');

									var qty;
									if(exceptionqtyflag=='true')
									{
										qty=exceptionqty;;
									}
									else
										qty=POarray["custparam_quantity"];

									var vId = -1;
									vId =	rf_confirmputaway(POarray["custparam_pointernalid"], POarray["custparam_polinenumber"],POarray["custparam_itemid"], 
											POarray["custparam_itemDescription"],POarray["custparam_poitemstatus"], POarray["custparam_popackcode"], 								
											qty, getLocation, getBeginLocationInternalId,POarray["custparam_lpno"], EndLocationId, exceptionqty, 
											getRecordId,trantype,POarray["custparam_polotbatchno"],whloc,methodid,response);


									nlapiLogExecution('DEBUG', 'vId2', vId);


									if(vId!=-1)
									{
										TrnLineUpdation(trantype, 'PUTW', POarray["custparam_pono"], POarray["custparam_pointernalid"], 
												POarray["custparam_polinenumber"], POarray["custparam_itemid"], null, 
												qty,"", POarray["custparam_poitemstatus"]);

										nlapiLogExecution('DEBUG', 'getRecordCount after confirming', getRecordCount);
										nlapiLogExecution('DEBUG', 'getCartLPNo after confirming', getCartLPNo);

										itemDimensions = getSKUCubeAndWeight(POarray["custparam_itemid"],"",WHLocation);
										var itemCube = itemDimensions[0];

										var TotalItemCube = parseFloat(itemCube) * parseFloat(POarray["custparam_quantity"]);
										nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );						

										var binLocationRemainingCube = GeteLocCube(getBeginLocationInternalId);

										var remainingCube = parseFloat(binLocationRemainingCube) - parseFloat(TotalItemCube);
										UpdateLocCube(getBeginLocationInternalId, parseFloat(remainingCube));

										nlapiLogExecution('DEBUG', 'remainingCube ', remainingCube );

										/*
										 * This block is to update the remaining cube for the bin location that was assigned
										 * by the system. 
										 */

										var oldBinLocationId = request.getParameter('hdnFetchedLocationId');
										var oldBinLocationRemainingCube =  GeteLocCube(oldBinLocationId);
										var actualRemainingCube = parseFloat(oldBinLocationRemainingCube) + parseFloat(TotalItemCube);

										nlapiLogExecution('DEBUG', 'oldBinLocationId ', oldBinLocationId );
										nlapiLogExecution('DEBUG', 'oldBinLocationRemainingCube ', oldBinLocationRemainingCube );
										nlapiLogExecution('DEBUG', 'actualRemainingCube ', actualRemainingCube );

										UpdateLocCube(oldBinLocationId, parseFloat(actualRemainingCube));

										if (getRecordCount > 1) {
											var filters = new Array();
											filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]));
											filters.push(new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

											var columns = new Array();
											columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
											columns[1] = new nlobjSearchColumn('custrecord_lpno');

											var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

											if (searchresults != null && searchresults.length > 0) {
												nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
												POarray["custparam_cartno"] = searchresults[0].getValue('custrecord_transport_lp');
												POarray["custparam_lpno"] =  getLPNo;
												POarray["custparam_recordcount"] = searchresults.length;

												if (POarray["custparam_beginlocation"] == null) {
													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaysku', 'customdeploy_ebiz_rf_cart_putawaysku_di', false, POarray);
													nlapiLogExecution('DEBUG', 'Back to Putaway SKU', 'Record count is not zero');
												}
												else {
													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);
													nlapiLogExecution('DEBUG', 'Back to Putaway confirmation', 'Record count is not zero');
												}
											}
											else {
												POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
												POarray["custparam_lpno"] = getLPNo;
												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												nlapiLogExecution('DEBUG', 'Here the Search Results ', 'Length is null1');
											}
										}
										else {


											var filtersmlp = new Array(); 
											filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('custparam_cartno'));

											var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

											if (SrchRecord != null && SrchRecord.length > 0) {
												var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
												rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'F');
												var recid = nlapiSubmitRecord(rec, false, true);
											}
											else {
												nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

											}


											POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
											POarray["custparam_lpno"] = getLPNo;
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaycmplete', 'customdeploy_ebiz_rf_cart_putawaycmplete', false, POarray);
										}
										/*
										 * 	This is to delete the check-in transactions from the inventory record while putaway confirmation. 
										 */						
										DeleteInvtRecCreatedforCHKNTask(POarray["custparam_pointernalid"], getLPNo);
									}
									else
									{
										POarray["custparam_error"] = 'Item  Receipt Failed';
										POarray["custparam_screenno"] = 'CRT12';
										POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
										POarray["custparam_lpno"] = getLPNo;
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										nlapiLogExecution('DEBUG', 'Item receipt failed', 'Item Receipt failed');
									}
								}//case# 20148431 starts
								else 
								{
									POarray["custparam_location"] = request.getParameter('hdnFetchedLocationId');
									nlapiLogExecution('DEBUG', 'location_exp_Location', POarray["custparam_location"]);
									POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
									POarray["custparam_lpno"] = getLPNo;
									POarray["custparam_error"] = 'INSUFFICIENT REMAINING CUBE';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									nlapiLogExecution('DEBUG', 'Ramining Cube is not sufficient', 'remaining cube');

								}//case# 20148431 ends
							}
							else
							{  

								POarray["custparam_location"] = request.getParameter('hdnFetchedLocationId');//Added by Ganapathi.
								nlapiLogExecution('DEBUG', 'location_exp_Location', POarray["custparam_location"]);
								POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
								POarray["custparam_lpno"] = getLPNo;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Entered Bin Location', 'Scanned bin location is wrong');
							}
						}
						else {
							POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
							POarray["custparam_lpno"] = getLPNo;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Here the Search Results ', 'Length is null2');
						}
					}
					else {
						POarray["custparam_cartno"]=request.getParameter('custparam_cartno');
						POarray["custparam_lpno"] = getLPNo;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Location is not entered / scanned', getLocation.toUpperCase());
					}
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');
				//case 20125952 start :
				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
				//case 20125952 end
			}
		}
		else
		{
			POarray["custparam_screenno"] = 'CRT9';
			POarray["custparam_error"] = 'LOCATION ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		}
	} 
}

function rf_confirmputaway(pointid, linenum, itemid, itemdesc, itemstatus, itempackcode, quantity, binlocationid, getBeginLocationInternalId, 
		invtlp, EndLocationId, ActQuantity, RecordId,trantype,batchno,whloc,methodid,response){
	nlapiLogExecution('DEBUG', 'inside rf_confirmputaway', 'Success');
	//Creating Item Receipt.
	var fromrecord;
	var fromid;
	var torecord;
	var qty;
	var vputwrecid=-1;
	var tempserialId = "";
	nlapiLogExecution('DEBUG', 'binlocationid is', binlocationid);
	nlapiLogExecution('DEBUG', 'Actual Quantity is', ActQuantity);
	nlapiLogExecution('DEBUG', 'binlocation internalid is', getBeginLocationInternalId);
	nlapiLogExecution('DEBUG', 'trantype', trantype);
	var veBizQty=quantity;
	//To get WH Location.
	var whLocation= '';
	if(trantype!='transferorder')
	{
		whLocation= nlapiLookupField('transaction',pointid,'location');
	}
	else
	{
		whLocation= nlapiLookupField('transferorder',pointid,'transferlocation');
	}
	nlapiLogExecution('DEBUG', 'whLocation internalid is', whLocation);
	var templocation=whLocation;
	fromrecord = trantype;
	fromid = pointid; // Transform PO with ID = 26 ;
	torecord = 'itemreceipt'; // Transform a record with given id to a different record type.
	var Itype = "";
	var serialInflg='F';
	// Get the object of the transformed record.       
	try {

		//case 20125737 start

		var systemRule= GetSystemRuleForPostItemReceiptby(templocation);

		if(systemRule=='LP')
		{
			idl=generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,RecordId,EndLocationId,batchno,null,templocation,null,quantity,response);
		}
		else
		{
			if(systemRule=='PO')
			{
				idl='-1';
			}
		}
		//case 20125737 end
		/*if(parseFloat(quantity)!=0){
			var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
			var polinelength = trecord.getLineItemCount('item');
			nlapiLogExecution('DEBUG', "polinelength", polinelength);
		 *//***Below code is merged from Lexjet production by Ganesh on 4th Mar 2013***//*
			var vAdvBinManagement=false;

			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);
		  *//***Upto here***//*
			var serialnumcsv = "";
			for (var j = 1; j <= polinelength; j++) {

				var item_id = trecord.getLineItemValue('item', 'item', j);
				var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
				var itemLineNo = trecord.getLineItemValue('item', 'line', j);
				//20123723 start
				var poItemUOM = trecord.getLineItemValue('item', 'units', j);//end
				var commitflag = 'N';

				var str = 'PO Item Line No. = ' + itemLineNo + '<br>';
				str = str + 'Task linenum. = ' + linenum + '<br>';	
				str = str + 'Transaction Type. = ' + trantype + '<br>';	
				str = str + 'poItemUOM. = ' + poItemUOM + '<br>';	

				nlapiLogExecution('DEBUG', 'Line Details', str);

				if(trantype!='transferorder')
				{
					if (itemLineNo == linenum) {
						whLocation=trecord.getLineItemValue('item', 'location', j);//value
						if(whLocation==null||whLocation=="")
							whLocation=templocation;

						if(poItemUOM!=null && poItemUOM!='')
						{
							var vbaseuomqty=0;
							var vuomqty=0;
							var eBizItemDims=geteBizItemDimensions(item_id);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('DEBUG', 'Dims Length', eBizItemDims.length);
								for(z=0; z < eBizItemDims.length; z++)
								{
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
									}
									nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
									if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
									{
										vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
									}
								}
								if(vuomqty==null || vuomqty=='')
								{
									vuomqty=vbaseuomqty;
								}
								nlapiLogExecution('DEBUG', 'vbaseuomqty', vbaseuomqty);
								nlapiLogExecution('DEBUG', 'vuomqty', vuomqty);

								if(quantity==null || quantity=='' || isNaN(quantity))
									quantity=0;
								else
									quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

							}
						}

						//Code Added by Ramana
						var varItemlocation;
						nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
						nlapiLogExecution('DEBUG', 'quantity', quantity);
						varItemlocation = getItemStatusMapLoc(itemstatus);
						//upto to here

						commitflag = 'Y';

					//	Itype = nlapiLookupField('item', itemid, 'recordType');
					var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;
						serialInflg = columns.custitem_ebizserialin;
						nlapiLogExecution('DEBUG', 'Itype', Itype);
						nlapiLogExecution('DEBUG', 'columns', columns);
					}


						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);

						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
						if (Itype == "serializedinventoryitem" || serialInflg=='T' ) {
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();
							var filters = new Array();
						var tempSerial = "";
						var filters = new Array();
						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
						filters[1] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1,17]);
						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);


							 if(trantype=='returnauthorization')
							 {
								  filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								 //Commented for RMA, For RMA no need to check  whether status is checkin/storage
								 //filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								 filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
								 filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								 //Commented for RMA, For RMA no need to check  whether status is checkin/storage
								 //filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								 filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
								 filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
								 filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','17']);
							 }
							 else
							 {
								 filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
								 filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','17']);
								 filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
								 filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							 }
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							//var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
								}

								serialnumcsv = tempSerial;
								tempSerial = "";
							}

							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
						}
						else 
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {

								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);

								var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								vatColumns[2] = new nlobjSearchColumn('custrecord_batch_no');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
											{	
												//tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												tempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//20123723 start
												tempBatch = tempbatchnew + '('+ quantity + ')';

												var str = 'tempbatchnew. = ' + tempbatchnew + '<br>';
												str = str + 'quantity = ' + quantity + '<br>';	
												str = str + 'tempBatch = ' + tempBatch + '<br>';	


												nlapiLogExecution('DEBUG', 'tempBatch_if', str);//end

											}
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();
												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												nlapiLogExecution('DEBUG','itemdetails',itemdetails);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												nlapiLogExecution('DEBUG', 'vAdvBinManagement:', vAdvBinManagement);
												nlapiLogExecution('DEBUG', 'vAdvBinManagement:', 'IfSucess');
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);

												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
												nlapiLogExecution('DEBUG', 'compSubRecord:', compSubRecord);
											}
										}
										else {
											if(confirmLotToNS == 'Y')
											{
												//tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												//20123723 start
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_batch_no')+ '('+ quantity + ')';
												nlapiLogExecution('DEBUG', 'tempBatch:_else', tempBatch);
												//end
											}
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempBatch);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
								if(!vAdvBinManagement)
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);

							}
						trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
						nlapiLogExecution('ERROR', 'itemstatus insert new', itemstatus);
						trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
						trecord.commitLineItem('item');
					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'itemid (Task)', itemid);	
					nlapiLogExecution('DEBUG', 'item_id (Line)', item_id);	

					itemLineNo=parseFloat(itemLineNo)-2;
					if (itemid == item_id && itemLineNo == linenum) {

						commitflag = 'Y';

						var varItemlocation;				
						varItemlocation = getItemStatusMapLoc(itemstatus);

						Itype = nlapiLookupField('item', itemid, 'recordType');


						var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
						if(columns != null && columns != '')
						{
							Itype = columns.recordType;
							serialInflg = columns.custitem_ebizserialin;
							nlapiLogExecution('DEBUG', 'Itype', Itype);
							nlapiLogExecution('DEBUG', 'columns', columns);
							nlapiLogExecution('DEBUG', 'serialInflg', columns);
						}

						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);
						if(trantype!='transferorder')
							trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();

							var tempSerial = "";
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);							 
							filters[1] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'equalto', linenum);
							filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1,17]);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

							var filters = new Array();

							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
							filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

							 nlapiLogExecution('DEBUG', 'xxx', 'xxx');
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
//							var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
							nlapiLogExecution('DEBUG', 'serchRec', serchRec);			
							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
									if(vAdvBinManagement)
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}
								}

								serialnumcsv = tempSerial;
								tempSerial = "";
							}

							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							if(!vAdvBinManagement)
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);


						}
						else 
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {

								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);

								var batfilter = new Array();
								batfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));
								batfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]));
								batfilter.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid));
								batfilter.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
								batfilter.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp));

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';

												}
											}
											if(vAdvBinManagement)											 
											{
												nlapiLogExecution('DEBUG', 'vAdvBinManagement:', vAdvBinManagement);
												nlapiLogExecution('DEBUG', 'vItQty:', vItQty);
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												if(vItQty !=quantity)
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);	

												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
												nlapiLogExecution('DEBUG', 'compSubRecord:', compSubRecord);
											}
										}
										else {
											if(confirmLotToNS == 'Y')
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempBatch);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
								if(!vAdvBinManagement)
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}

						trecord.commitLineItem('item');
					}
				}
				if (commitflag == 'N') {
					nlapiLogExecution('DEBUG', 'commitflag is N', commitflag);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
					trecord.commitLineItem('item');
				}

			}
			nlapiLogExecution('DEBUG', 'Before Item Receipt---', 'Before');
			if (parseFloat(quantity)>0)
				var idl = nlapiSubmitRecord(trecord);
			nlapiLogExecution('DEBUG', 'Item Receipt idl', idl);*/
		//
		if (idl != null||parseFloat(quantity)==0) {
			nlapiLogExecution('DEBUG', 'RecordId', RecordId);
			nlapiLogExecution('DEBUG', 'pointid', pointid);

			if(idl != null && idl != '' && idl != '-1')
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_ebiz_nsconfirm_ref_no', idl);

			var vbatchno='';
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid));
			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '2'));
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]));
			filters.push(new nlobjSearchFilter('internalid', null, 'is', RecordId));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_actendloc');
			columns[2] = new nlobjSearchColumn('custrecordact_begin_date');
			columns[3] = new nlobjSearchColumn('custrecord_act_end_date');
			columns[4] = new nlobjSearchColumn('custrecord_wms_status_flag');
			columns[5] = new nlobjSearchColumn('custrecord_actualbegintime');
			columns[6] = new nlobjSearchColumn('custrecord_actualendtime');
			columns[7] = new nlobjSearchColumn('custrecord_upd_date');
			columns[8] = new nlobjSearchColumn('custrecord_recordupdatetime');
			columns[9] = new nlobjSearchColumn('custrecord_batch_no');
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[11] = new nlobjSearchColumn('custrecord_sku_status');
			columns[12] = new nlobjSearchColumn('custrecord_packcode');
			columns[13] = new nlobjSearchColumn('custrecord_ebiz_lpno');
			columns[14] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[15] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
			columns[16] = new nlobjSearchColumn('custrecord_ebiz_ot_receipt_no');

			//a Date object to be used for a random value
			var now = new Date();
			//now= now.getHours();
			//Getting time in hh:mm tt format.
			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) {
				curr_min = "0" + curr_min;
			}
			nlapiLogExecution("DEBUG", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));
			// case# 201414340
			var mergeputmethods = getMergeLPMethods(whloc);		
			var varMergeLP = 'F';
			nlapiLogExecution('DEBUG', 'methodid', methodid);
			nlapiLogExecution('DEBUG', 'whloc', whloc);
			if(methodid!=null && methodid!='')
				varMergeLP = isMergeLPEnabled(whloc,methodid,mergeputmethods);
			//varMergeLP = isMergeLP(whloc,methodid);

			// execute the  search, passing null filter and return columns

			nlapiLogExecution('DEBUG', 'varMergeLP', varMergeLP);
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			var vEbizTrailerNo='';			
			var vEbizReceiptNo='';
			var opentaskintid='';
			var expdate='';
			var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
			var columns = nlapiLookupField('item', itemid, fields);
			/*	var ItemType = columns.recordType;					
				var batchflg = columns.custitem_ebizbatchlot;
				var itemfamId= columns.custitem_item_family;
				var itemgrpId= columns.custitem_item_group;
				var InvtRefNo="";

				var itemgrpId= columns.custitem_item_group;*/
			var ItemType='';					
			var batchflg ='F';
			var itemfamId='';
			var itemgrpId='';
			var serialInflg ='F';
			var InvtRefNo="";
			var fifovalue='';
			if(columns != null && columns != '')
			{
				ItemType = columns.recordType;					
				batchflg = columns.custitem_ebizbatchlot;
				itemfamId= columns.custitem_item_family;
				itemgrpId= columns.custitem_item_group;
				serialInflg = columns.custitem_ebizserialin;
			}
			nlapiLogExecution('DEBUG', 'veBizQty', veBizQty);
			if(veBizQty != null && veBizQty != '')
				quantity=veBizQty;
			nlapiLogExecution('DEBUG', 'quantity', quantity);
			if(varMergeLP=="T")
			{
				var putItemId,putItemStatus,putItemPC,putBinLoc,putLP,putQty,putoldBinLoc;
				for (var i = 0; i < searchresults.length; i++)
				{
					nlapiLogExecution('DEBUG', 'test1', 'test1');
					nlapiLogExecution('DEBUG', 'searchresults.length', searchresults.length);
					var searchresult = searchresults[i];
					opentaskintid=searchresult.getId();
					nlapiLogExecution('DEBUG', 'opentaskintid', opentaskintid);
					putItemId=searchresult.getValue('custrecord_ebiz_sku_no');
					putItemStatus=searchresult.getValue('custrecord_sku_status');
					putItemPC=searchresult.getValue('custrecord_packcode');
					putBinLoc=searchresult.getValue('custrecord_actbeginloc');
					putLP=searchresult.getValue('custrecord_ebiz_lpno');
//					putQty = searchresult.getValue('custrecord_expe_qty');
					putQty = quantity;
					putoldBinLoc= searchresult.getValue('custrecord_actbeginloc');
					if(searchresult.getValue('custrecord_ebiz_trailer_no') != null && searchresult.getValue('custrecord_ebiz_trailer_no') != '')
					{
						vEbizTrailerNo=searchresult.getValue('custrecord_ebiz_trailer_no');
					}
					if(searchresult.getValue('custrecord_ebiz_ot_receipt_no') != null && searchresult.getValue('custrecord_ebiz_ot_receipt_no') != '')
					{
						vEbizReceiptNo=searchresult.getValue('custrecord_ebiz_ot_receipt_no');
					}

					nlapiLogExecution('DEBUG', 'End', 'END');
				}

				nlapiLogExecution('DEBUG', 'putItemId', putItemId);
				nlapiLogExecution('DEBUG', 'putItemStatus', putItemStatus);
				nlapiLogExecution('DEBUG', 'putItemPC', putItemPC);
				nlapiLogExecution('DEBUG', 'putBinLoc', putBinLoc);
				nlapiLogExecution('DEBUG', 'putLP', putLP);
				nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
				nlapiLogExecution('DEBUG', 'EndLocationId', EndLocationId);
				//nlapiLogExecution('DEBUG', 'fifodate', fifodate);
				var varPaltQty = getMaxUOMQty(putItemId,putItemPC);

				nlapiLogExecution('DEBUG', 'varPaltQty', varPaltQty);

				var filtersinvt = new Array();
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', putItemId));
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', putItemStatus));
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', EndLocationId));
				filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
				if(putItemPC!=null && putItemPC!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', putItemPC));							

				if(getlotnoid!=null && getlotnoid!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', getlotnoid));							

				var columnsinvt = new Array();
				columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
				columnsinvt[0].setSort();

				var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
				if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
				{
					nlapiLogExecution('DEBUG', 'invtsearchresults.length', invtsearchresults.length);
					var newputqty=putQty;
					var BoolInvMerged=false;
					for (var i = 0; i < invtsearchresults.length; i++) 
					{
						var qoh=invtsearchresults[i].getValue('custrecord_ebiz_qoh');

						nlapiLogExecution('DEBUG', 'exiting qoh', qoh);
						nlapiLogExecution('DEBUG', 'putQty', putQty);
						nlapiLogExecution('DEBUG', 'varPaltQty', varPaltQty);

						if(newputqty>0 && (parseFloat(putQty)+parseFloat(qoh))<=parseFloat(varPaltQty))
						{
							nlapiLogExecution('DEBUG', 'Inventory Record ID', invtsearchresults[i].getId());
							BoolInvMerged=true;
							nlapiLogExecution('DEBUG', 'BoolInvMerged', BoolInvMerged);

							var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[i].getId());											

							var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
							var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
							var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

							nlapiLogExecution('DEBUG', 'varExistQOHQty', varExistQOHQty);
							nlapiLogExecution('DEBUG', 'varExistInvQty', varExistInvQty);

							var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(putQty);
							var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(putQty);

							invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

							var invtrecid = nlapiSubmitRecord(invttransaction, true);
							InvtRefNo=invtsearchresults[i].getId();
							nlapiLogExecution('DEBUG', 'nlapiSubmitRecord', 'Record Submitted');
							nlapiLogExecution('DEBUG', 'Inventory is merged to the LP '+varExistLP);
							newputqty=0;

							if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);
							if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T")
								//case 20125737 start 
								UpdateSerialNumLpForInvtMerge(trantype,pointid,linenum,invtlp,varExistLP);
							//case 20125737 end
						}
					}
					if(BoolInvMerged==false)
					{
						var accountNumber = "";

						//Creating Inventory Record.
						var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

						nlapiLogExecution('DEBUG', 'Creating INVT  Record', 'INVT');

						invtRec.setFieldValue('name', idl);
						invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
						invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
						invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
						invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
						if(itempackcode!=null&&itempackcode!="")
							invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
						invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
						invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
						invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
						invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
						invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
						invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
						if(accountNumber!=null && accountNumber!='')
						{
							invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
						}

						invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
						invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
						invtRec.setFieldValue('custrecord_invttasktype', '2');
						invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
						if(getuomlevel!=null && getuomlevel!='')
							invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);

						var getlotnoid="";
						var expdate='';
						var vfifodate;
						nlapiLogExecution('DEBUG', 'ItemType', ItemType);
						nlapiLogExecution('DEBUG', 'batchflg', batchflg);
						nlapiLogExecution('DEBUG', 'batchno', batchno);
						if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
						{
							if(searchresults != null && searchresults != '')
							{	
								batchno = searchresults[0].getValue('custrecord_batch_no');
								nlapiLogExecution('DEBUG', 'searchresults[0].getId()', searchresults[0].getId());
							}
							nlapiLogExecution('DEBUG', 'batchno', batchno);
							if(batchno!="" && batchno!=null)
							{

								var filterspor = new Array();
								filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
								if(whLocation!=null && whLocation!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));
								if(itemid!=null && itemid!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

								var column=new Array();
								column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
								column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');
								var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
								if(receiptsearchresults!=null)
								{
									getlotnoid= receiptsearchresults[0].getId();
									nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
									expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
									vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
								}
								nlapiLogExecution('DEBUG', 'expdate', expdate);
							}
						}
						/*if(getlotnoid!=null && getlotnoid!='')
						{
							var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
						}*/
						if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
						{ 										
							/*try
							{
								//Checking FIFO Policy.					
								if (getlotnoid != "") {
									fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
									nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
									invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
									invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
								}
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
							}*/

							try
							{
								//Checking FIFO Policy.					
								if (getlotnoid != "") {
									if(vfifodate == null || vfifodate =='')
									{fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
									nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
									}
									else
									{
										fifovalue=vfifodate;
									}
									invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
									//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
								}
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
							}
						}
						/*else
								invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());*/
						if(fifovalue == null || fifovalue == '')
						{
							fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
						}

						if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
						else
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());

						nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');

						var invtrecid = nlapiSubmitRecord(invtRec, false, true);
						InvtRefNo=invtrecid;
						nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
						if (invtrecid != null) {
							nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
						}
						else {
							nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
						}

						if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);


						//Added for updation of Cube information by ramana on 10th june 2011

						nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
						nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
						nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
						nlapiLogExecution('DEBUG', 'LP', putLP);
					}
					//Added for updation of Cube information by ramana on 10th june 2011

					nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
					nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
					nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
					nlapiLogExecution('DEBUG', 'LP', putLP);

					if(putBinLoc!=putoldBinLoc)
					{   
						/*
								//For the OLD Location Updation Purpose.    											 											
						 */

						var arrDims = getSKUCubeAndWeight(putItemId, 1,whLocation);
						var itemCube = 0;
						if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
						{
							var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
							nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
						} 
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var vOldRemainingCube = GeteLocCube(putoldBinLoc);
						}
						nlapiLogExecution('DEBUG', 'vOldRemainingCube', vOldRemainingCube);

						var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
						nlapiLogExecution('DEBUG', 'vOldLocationRemainingCube', vTotalCubeValue);
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
						}
						//upto to here old Location Updation purpose

						//For the New Location updation

						var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

						nlapiLogExecution('DEBUG', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

						//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
						var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

						nlapiLogExecution('DEBUG', 'vTotalNewLocation', vTotalNewLocationCube);
					}						 

					for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						opentaskintid = searchresult.getId();
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
						nlapiLogExecution("DEBUG","InvtRefNo",InvtRefNo);
						transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
						if(idl== '-1' || idl== null || idl==-1 )
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', '');
						}
						else
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						}

						if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
							try {
								var filters = new Array();
								if(trantype=='returnauthorization' || trantype=='transferorder' )
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
									//Commented for RMA, For RMA no need to check  whether status is checkin/storage
									//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

								}
								else
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
									filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
									filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
								}

								var columns = new Array();

								columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
								var serialnumcsv = "";
								var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
								var serialnumArray=new Array();
								if (serchRec !=null && serchRec !='' && serchRec.length>0) {
									for (var n1 = 0; n1 < serchRec.length; n1++) {
										//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
										if (tempserialId == "") {
											tempserialId = serchRec[n1].getId();
											tempserial=serchRec[n1].getValue('custrecord_serialnumber');
										}
										else {
											tempserialId = tempserialId + "," + serchRec[n1].getId();
											tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
										}
									}
									serialnumcsv=tempserial;
								}//end
							}
							catch (myexp) {
								nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
							}
						}

						transaction.setFieldValue('custrecord_serial_no', serialnumcsv);

						vputwrecid=nlapiSubmitRecord(transaction, false, true);
						//case no 20125922  
						/*if(systemRule=='LP')
						{
							MoveTaskRecord(vputwrecid);
						}*/

					} 

					//MoveTaskRecord(opentaskintid);
					if(vEbizTrailerNo != null && vEbizTrailerNo != '')
					{
						if(vEbizReceiptNo != null && vEbizReceiptNo != '')
						{
							nlapiLogExecution('DEBUG', 'vEbizReceiptNo', vEbizReceiptNo);
							nlapiLogExecution('DEBUG', 'vEbizTrailerNo', vEbizTrailerNo);
							var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

							if(poReceiptRec != null && poReceiptRec != '')
							{
								var previousrcvqty=0;
								if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
								{
									previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
								}	
								var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

								nlapiLogExecution('DEBUG', 'previousrcvqty', previousrcvqty);
								nlapiLogExecution('DEBUG', 'qty', qty);
								nlapiLogExecution('DEBUG', 'newrcvQty', newrcvQty);

								var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
								nlapiLogExecution('DEBUG','id',id);
							}	
						}	
					}
				}
				else
				{
					var accountNumber = "";
					var getlotnoid="";
					//Creating Inventory Record.
					var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

					nlapiLogExecution('DEBUG', 'Creating INVT  Record', 'INVT');

					invtRec.setFieldValue('name', idl);
					invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
					invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
					invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
					invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
					invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
					invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
					invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
					invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
					invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
					if(accountNumber!=null && accountNumber!=''){
						invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
					}
					invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
					invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
					invtRec.setFieldValue('custrecord_invttasktype', '2');
					invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
					if(getuomlevel!=null && getuomlevel!='')
						invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);
					if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")

					{ 										
						try
						{
							nlapiLogExecution('DEBUG', 'batchno', batchno);
							if(batchno!="" && batchno!=null)
							{

								var filterspor = new Array();
								filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
								if(whLocation!=null && whLocation!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));
								if(itemid!=null && itemid!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

								var column=new Array();
								column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
								column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');
								var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
								if(receiptsearchresults!=null)
								{
									getlotnoid= receiptsearchresults[0].getId();
									nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
									expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
									vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
									invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
								}
								nlapiLogExecution('DEBUG', 'expdate', expdate);
							}

							//Checking FIFO Policy.					
							/*if (getlotnoid != "") {
								fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
								nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
								invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
								invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
							}*/
							try
							{
								//Checking FIFO Policy.					
								if (getlotnoid != "") {
									if(vfifodate == null || vfifodate =='')
									{fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
									nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
									}
									else
									{
										fifovalue=vfifodate;
									}
									invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
									//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
								}
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
							}
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
						}
					}
					/*	else
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());*/


					if(fifovalue == null || fifovalue == '')
					{
						fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
					}

					if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
					else
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());


					nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');
					if(pointid!=null && pointid!="")
						invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

					var invtrecid = nlapiSubmitRecord(invtRec, false, true);
					InvtRefNo=invtrecid;
					nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
					if (invtrecid != null) {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
					}
					else {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
					}

					if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);

					//Added for updation of Cube information by ramana on 10th june 2011

					nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
					nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
					nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
					nlapiLogExecution('DEBUG', 'LP', putLP);

					if(putBinLoc!=putoldBinLoc)
					{   
						/*
								//For the OLD Location Updation Purpose.    											 											
						 */

						var arrDims = getSKUCubeAndWeight(putItemId, 1,whLocation);
						var itemCube = 0;
						if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
						{
							var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
							nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
						} 
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var vOldRemainingCube = GeteLocCube(putoldBinLoc);
						}
						nlapiLogExecution('DEBUG', 'vOldRemainingCube', vOldRemainingCube);

						var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
						nlapiLogExecution('DEBUG', 'vOldLocationRemainingCube', vTotalCubeValue);
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
						}
						//upto to here old Location Updation purpose

						//For the New Location updation

						var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

						nlapiLogExecution('DEBUG', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

						//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
						var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

						nlapiLogExecution('DEBUG', 'vTotalNewLocation', vTotalNewLocationCube);
						//upto to here new location

					}                     
					//upto to here on 10th june 2011

					for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_status_flag', 4);
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(quantity).toFixed(4));
						nlapiLogExecution("DEBUG","InvtRefNo",InvtRefNo);
						transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
						if(idl== '-1' || idl== null || idl==-1 )
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', '');
						}
						else
						{
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						}

						if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
							try {
								var filters = new Array();
								if(trantype=='returnauthorization' || trantype=='transferorder' )
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
									//Commented for RMA, For RMA no need to check  whether status is checkin/storage
									//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

								}
								else
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
									filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
									filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
									filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
								}

								var columns = new Array();

								columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
								var serialnumcsv = "";
								var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
								var serialnumArray=new Array();
								if (serchRec !=null && serchRec !='' && serchRec.length>0) {
									for (var n1 = 0; n1 < serchRec.length; n1++) {
										//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
										if (tempserialId == "") {
											tempserialId = serchRec[n1].getId();
											tempserial=serchRec[n1].getValue('custrecord_serialnumber');
										}
										else {
											tempserialId = tempserialId + "," + serchRec[n1].getId();
											tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
										}
									}
									serialnumcsv=tempserial;
								}//end
							}
							catch (myexp) {
								nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
							}
						}


						transaction.setFieldValue('custrecord_serial_no', serialnumcsv);

						vputwrecid=nlapiSubmitRecord(transaction, false, true);
						/*if(systemRule=='LP')
						{
							MoveTaskRecord(vputwrecid);
						}*/

					} 

					//MoveTaskRecord(opentaskintid);
					if(vEbizTrailerNo != null && vEbizTrailerNo != '')
					{
						if(vEbizReceiptNo != null && vEbizReceiptNo != '')
						{
							nlapiLogExecution('DEBUG', 'vEbizReceiptNo', vEbizReceiptNo);
							nlapiLogExecution('DEBUG', 'vEbizTrailerNo', vEbizTrailerNo);
							var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

							if(poReceiptRec != null && poReceiptRec != '')
							{
								var previousrcvqty=0;
								if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
								{
									previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
								}	
								var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

								nlapiLogExecution('DEBUG', 'previousrcvqty', previousrcvqty);
								nlapiLogExecution('DEBUG', 'qty', qty);
								nlapiLogExecution('DEBUG', 'newrcvQty', newrcvQty);

								var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
								nlapiLogExecution('DEBUG','id',id);
							}	
						}	
					}
				}
				if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
					try {

						var filters = new Array();
						if(trantype=='returnauthorization' || trantype=='transferorder' )
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							//Commented for RMA, For RMA no need to check  whether status is checkin/storage
							//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}

						var columns = new Array();

						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						if (serchRec !=null && serchRec !='' && serchRec.length>0) {
							for (var n1 = 0; n1 < serchRec.length; n1++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n1].getId();
									tempserial=serchRec[n1].getValue('custrecord_serialnumber');
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n1].getId();
									tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
								}
							}
							serialnumcsv=tempserial;
						}//end


						if (tempserialId != null && tempserialId !='')  {
							var temSeriIdArr = new Array();
							temSeriIdArr = tempserialId.split(',');
							nlapiLogExecution('DEBUG', 'updating Serial num Records to', 'Storage Status');
							for (var p = 0; p < temSeriIdArr.length; p++) {
								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';
								fields[1] = 'custrecord_serial_location';
								fields[2] = 'custrecord_serialbinlocation';
								values[0] = '3';
								values[1] = whLocation;
								values[2] = EndLocationId;
								var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
								nlapiLogExecution('DEBUG', 'Inside loop serialId ', temSeriIdArr[p]);

								nlapiLogExecution('DEBUG', 'Serial num Records to', 'Storage Status Success');
							}
						}
					} 
					catch (myexp) {
						nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
					}
				}

			}
			else
			{

				/*for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						opentaskintid=searchresult.getId();
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						nlapiLogExecution('DEBUG', 'EndDate', DateStamp());
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
						transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						vputwrecid=nlapiSubmitRecord(transaction, false, true);

						MoveTaskRecord(vputwrecid);

					} //for loop closing.
				 */

				/*var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
				var columns = nlapiLookupField('item', itemid, fields);
				var ItemType = columns.recordType;					
				var batchflg = columns.custitem_ebizbatchlot;
				var itemfamId= columns.custitem_item_family;
				var itemgrpId= columns.custitem_item_group;*/
				var getlotnoid="";
				var expdate='';
				var vfifodate='';
				//case 20123924 :start
				var vbatchno = batchno;
				//end
				nlapiLogExecution('DEBUG', 'ItemType', ItemType);
				if(ItemType == "lotnumberedinventoryitem" || batchflg == "T" || ItemType == "lotnumberedassemblyitem")
				{
					if(searchresults != null && searchresults!='')
						vbatchno=searchresults[0].getValue('custrecord_batch_no');
					nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);
					if(vbatchno!=""&&vbatchno!=null)
					{
						var filterspor = new Array();
						filterspor.push(new nlobjSearchFilter('name', null, 'is', vbatchno));
						if(itemid!=null && itemid!="")
							filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

						var column=new Array();
						column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
						column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

						var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
						if(receiptsearchresults!=null)
						{
							getlotnoid= receiptsearchresults[0].getId();
							nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
							expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
							vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
						}
					}
				}

				var getBaseUOM='';
				var getuomlevel='';

				var eBizItemDims=geteBizItemDimensions(itemid);
				if(eBizItemDims!=null&&eBizItemDims.length>0)
				{
					for(z=0; z < eBizItemDims.length; z++)
					{
						if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
						{
							getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
							getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
						}
					}
				}

				nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
				nlapiLogExecution('DEBUG', 'getuomlevel', getuomlevel);
				nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
				nlapiLogExecution('DEBUG', 'expdate', expdate);

				///Getting Records to insert in to inventory table.
				nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation ', 'Record Creation');
				var accountNumber = "";

				//Creating Inventory Record.
				var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

				nlapiLogExecution('DEBUG', 'Creating INVT  Record', 'INVT');
				var accountNumber;
				if(whLocation !=null && whLocation!='')
				{ accountNumber= getAccountNumber(whLocation);}
				else
				{accountNumber= '';}

				if(trantype!='transferorder')
				{
					invtRec.setFieldValue('name', idl);
				}			
				invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
				invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
				invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
				invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
				invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
				invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
				invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
				invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
				invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
				if(accountNumber!=null && accountNumber!=''){
					invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
				}
				invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
				invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
				invtRec.setFieldValue('custrecord_invttasktype', '2');
				invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
				if(getuomlevel!=null && getuomlevel!='')
					invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);
				/*if(getlotnoid!=null && getlotnoid!='')
				{
					var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
				}*/
				if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//					if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
				{ 										
					try
					{
						//Checking FIFO Policy.					
						if (getlotnoid != null && getlotnoid != '') {
							if(vfifodate == null || vfifodate ==''){
								fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
								nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
							}
							else
							{
								fifovalue=vfifodate;
							}
							nlapiLogExecution('DEBUG','fifovalue for lotitem',fifovalue);
							invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
							//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
							invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
						}
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
					}
				}
				/*else
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());*/


				if(fifovalue == null || fifovalue == '')
				{
					fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
				}

				if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
				else
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());

				nlapiLogExecution('DEBUG','accountNumber',accountNumber);
				if(accountNumber != null && accountNumber != '')
				{
					invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
				}
				invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
				invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
				nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');

				var invtrecid = nlapiSubmitRecord(invtRec, false, true);
				InvtRefNo=invtrecid;

				nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
				if (invtrecid != null) {
					nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
				}
				else {
					nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
				}

				if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);

				for (var i = 0; i < searchresults.length; i++) {
					vbatchno=searchresults[i].getValue('custrecord_batch_no');
					var searchresult = searchresults[i];
					opentaskintid=searchresult.getId();
					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

					nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

					if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
						transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
						transaction.setFieldValue('custrecordact_begin_date', DateStamp());
						//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					}

					transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					transaction.setFieldValue('custrecord_upd_date', DateStamp());
					transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
					transaction.setFieldValue('custrecord_act_end_date', DateStamp());
					nlapiLogExecution('DEBUG', 'EndDate', DateStamp());
					transaction.setFieldValue('custrecord_wms_status_flag', 3);
					transaction.setFieldValue('custrecord_act_qty', parseFloat(quantity).toFixed(4));
					nlapiLogExecution("DEBUG","InvtRefNo",InvtRefNo);
					transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
					if(idl != null && idl != '' && idl != '-1')
						transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);


					// case#20127407 starts (to show the serialNo# in opentask after doing cart putaway )
					if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
						try {
							var filters = new Array();
							if(trantype=='returnauthorization' || trantype=='transferorder' )
							{
								filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

							}
							else
							{
								filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
								filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
								filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							}

							var columns = new Array();

							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
							var serialnumArray=new Array();
							if (serchRec !=null && serchRec !='' && serchRec.length>0) {
								for (var n1 = 0; n1 < serchRec.length; n1++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n1].getId();
										tempserial=serchRec[n1].getValue('custrecord_serialnumber');
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n1].getId();
										tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
									}
								}
								serialnumcsv=tempserial;
							}//end
						}
						catch (myexp) {
							nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
						}
					}


					nlapiLogExecution("ERROR","serialnumcsv",serialnumcsv);
					transaction.setFieldValue('custrecord_serial_no', serialnumcsv);
					vputwrecid=nlapiSubmitRecord(transaction, false, true);

					//MoveTaskRecord(vputwrecid);

				} //for loop closing.

				//serial Entry status updation to S
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
					try {

						/*	
						var filters = new Array();
						if(trantype=='returnauthorization' || trantype=='transferorder' )
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							//Commented for RMA, For RMA no need to check  whether status is checkin/storage
							//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}

						var columns = new Array();

						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						if (serchRec !=null && serchRec !='' && serchRec.length>0) {
							for (var n1 = 0; n1 < serchRec.length; n1++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n1].getId();
									tempserial=serchRec[n1].getValue('custrecord_serialnumber');
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n1].getId();
									tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
								}
							}

							serialnumcsv=tempserial;
						}*///end



						nlapiLogExecution('DEBUG', 'Inside loop tempserialId ', tempserialId);

						if (tempserialId != null && tempserialId != '') {
							var temSeriIdArr = new Array();
							temSeriIdArr = tempserialId.split(',');
							nlapiLogExecution('DEBUG', 'updating Serial num Records to', 'Storage Status');
							for (var p = 0; p < temSeriIdArr.length; p++) {
								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';
								fields[1] = 'custrecord_serial_location';
								fields[2] = 'custrecord_serialbinlocation';
								values[0] = '3';
								values[1] = whLocation;
								values[2] = EndLocationId;
								var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
								nlapiLogExecution('DEBUG', 'Inside loop serialId ', temSeriIdArr[p]);

								nlapiLogExecution('DEBUG', 'Serial num Records to', 'Storage Status Success');
							}
						}
					} 
					catch (myexp) {
						nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
					}
				}
			}

		} //if Item receipt is not null.
		else {
			nlapiLogExecution('DEBUG', 'If Item Receipt Failed', 'Error in Item Receipt');
			nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
			transaction.setFieldValue('custrecord_actendloc', EndLocationId);
			nlapiSubmitRecord(transaction, false, true);
			nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);
			return idl;
		}
		//}
	} 
	catch (e) {
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
		nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('DEBUG', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('DEBUG', 'Exception in TransformRec (PurchaseOrder) : ', e);

		if (e instanceof nlobjError) 
			nlapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());

		nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		transaction.setFieldValue('custrecord_ebiz_error_log', e);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);
	}

	return vputwrecid;
}

function isMergeLP(poloc,methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('name', null, 'is', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}

function getMergeLPMethods(poloc)
{
	nlapiLogExecution('ERROR','Into  getMergeLPMethods');

	var putwmethodsearchresults = new Array();
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filtersputmethod.push(new nlobjSearchFilter('custrecord_mergelp', null, 'is', 'T'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_methodid');  
	colsputmethod[1] = new nlobjSearchColumn('name');  

	putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	nlapiLogExecution('ERROR','Out of  getMergeLPMethods');

	return putwmethodsearchresults;
}
function isMergeLPEnabled(poloc,methodid,mergeputmethods)
{
	nlapiLogExecution('ERROR','Into  isMergeLPEnabled');

	var varMergeLP = 'F';

	if(mergeputmethods!=null && mergeputmethods!='' && mergeputmethods.length>0)
	{
		nlapiLogExecution('ERROR','Merge Methods Length',mergeputmethods.length);

		for (var s = 0; s < mergeputmethods.length; s++) {

			var mergemethodid = mergeputmethods[s].getValue('custrecord_methodid');
			var mergemethodname = mergeputmethods[s].getValue('name');
			nlapiLogExecution('ERROR','mergemethodid',mergemethodid);
			nlapiLogExecution('ERROR','mergemethodname',mergemethodname);
			nlapiLogExecution('ERROR','task methodid',methodid);
			if(mergemethodid==methodid || mergemethodname==methodid)
			{
				varMergeLP='T';
			}
		}
	}
	else
	{
		varMergeLP='F';
	}

	nlapiLogExecution('ERROR','Out of  isMergeLPEnabled');
	return varMergeLP;
}

function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}

	return varPaltQty;
}

function DeleteInvtRecCreatedforCHKNTask(pointid, lp)
{
	nlapiLogExecution('DEBUG','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');
	var Ifilters = new Array();
	Ifilters.push(new nlobjSearchFilter('name', null, 'is', pointid));
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [17]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1]));
	Ifilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));

	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('DEBUG','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('DEBUG','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('DEBUG','CHKN INVT Id',invtId);
			nlapiLogExecution('DEBUG','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('DEBUG','Invt Deleted record Id',invtId);

		}
	}
}

function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}

function getAccountNumber(whLocation){
	var filtersAccount = new Array();
	filtersAccount[0] = new nlobjSearchFilter('custrecord_location', null, 'is', whLocation);

	var columnsAccount = new Array();
	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);              

	if (accountSearchResults != null){
		var accountNumber = accountSearchResults[0].getValue('custrecord_accountno');                                
		//nlapiLogExecution('DEBUG', 'Account #',accountNumber);
	}

	return accountNumber;
}
function generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,RecordId,
		EndLocationId,batchno,qtyexceptionFlag,templocation,getScannedSerialNoinQtyexception,quantityineach,response)
{
	try{
		var TranferInvFlag = 'F';
		var tempserialId = "";
		nlapiLogExecution('DEBUG', 'ActQuantity', ActQuantity);
		var whLocation=templocation;
		if(parseFloat(ActQuantity)!=0){
			var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
			var polinelength = trecord.getLineItemCount('item');
			nlapiLogExecution('DEBUG', "polinelength", polinelength);
			/***Below code is merged from Lexjet production by Ganesh on 4th Mar 2013***/
			var vAdvBinManagement=false;

			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);
			/***Upto here***/
			var serialnumcsv = "";
			for (var j = 1; j <= polinelength; j++) {

				var item_id = trecord.getLineItemValue('item', 'item', j);
				var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
				var itemLineNo = trecord.getLineItemValue('item', 'line', j);
				//20123723 start
				var poItemUOM = trecord.getLineItemValue('item', 'units', j);//end
				var commitflag = 'N';
				// case no 20125922
				var quantity=0;
				quantity = ActQuantity;

				nlapiLogExecution('ERROR', 'quantity', quantity);
				var str = 'PO Item Line No. = ' + itemLineNo + '<br>';
				str = str + 'Task linenum. = ' + linenum + '<br>';	
				str = str + 'Transaction Type. = ' + trantype + '<br>';	
				str = str + 'poItemUOM. = ' + poItemUOM + '<br>';	

				nlapiLogExecution('DEBUG', 'Line Details', str);

				if(trantype!='transferorder')
				{
					if (itemLineNo == linenum) {
						whLocation=trecord.getLineItemValue('item', 'location', j);//value
						if(whLocation==null||whLocation=="")
							whLocation=templocation;

						if(poItemUOM!=null && poItemUOM!='')
						{
							var vbaseuomqty=0;
							var vuomqty=0;
							var eBizItemDims=geteBizItemDimensions(item_id);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('DEBUG', 'Dims Length', eBizItemDims.length);
								for(z=0; z < eBizItemDims.length; z++)
								{
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
									}
									//nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
									//nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
									//nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
									if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
									{
										vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
									}
								}
								if(vuomqty==null || vuomqty=='')
								{
									vuomqty=vbaseuomqty;
								}
								nlapiLogExecution('DEBUG', 'vbaseuomqty', vbaseuomqty);
								nlapiLogExecution('DEBUG', 'vuomqty', vuomqty);

								if(quantity==null || quantity=='' || isNaN(quantity))
									quantity=0;
								else
									quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

							}
						}

						//Code Added by Ramana
						var varItemlocation;
						nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
						nlapiLogExecution('DEBUG', 'quantity', quantity);
						varItemlocation = getItemStatusMapLoc(itemstatus);
						//upto to here

						commitflag = 'Y';

						//	Itype = nlapiLookupField('item', itemid, 'recordType');
						var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
						if(columns != null && columns != '')
						{
							Itype = columns.recordType;
							serialInflg = columns.custitem_ebizserialin;
							nlapiLogExecution('DEBUG', 'Itype', Itype);
							nlapiLogExecution('DEBUG', 'columns', columns);
						}


						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);

						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
						if (Itype == "serializedinventoryitem" || serialInflg=='T' ) {
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();
							var filters = new Array();
							var tempSerial = "";
							/*	var filters = new Array();
						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
						filters[1] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1,17]);
						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							 */

							if(trantype=='returnauthorization')
							{
								/* filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								 //Commented for RMA, For RMA no need to check  whether status is checkin/storage
								 //filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								 filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);*/
								filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								//Commented for RMA, For RMA no need to check  whether status is checkin/storage
								//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
								filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
								filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
								filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','17']);
							}
							else
							{
								filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
								filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','17']);
								filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
								filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							}
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
							//var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
									// case no 20126676
									if(vAdvBinManagement)
									{
										nlapiLogExecution('ERROR', 'custrecord_serialnumber', serchRec[n].getValue('custrecord_serialnumber'));
										//case 20125918 start : serial numbers posting
										if(n == 0)
											var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										//case 20125918 end
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										//	compSubRecord.commit();
									}

									//if(parseInt(n)== parseInt(quantity-1))
									//	break;




								}
								//case 20125918 start : serial numbers posting
								if(vAdvBinManagement)
									compSubRecord.commit();
							}

							serialnumcsv = tempSerial;
							tempSerial = "";


							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							//trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
							if(!vAdvBinManagement)
							{
								if ((Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem")  && (trecord!=null && trecord!='')) {
									//trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
									trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
								}
							}
						}
						else 
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem" ) {
								//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {

								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);

								var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								vatColumns[2] = new nlobjSearchColumn('custrecord_batch_no');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
											{	
												//tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												tempbatchnew = batchsearchresults[n].getValue('custrecord_batch_no');
												//20123723 start
												tempBatch = tempbatchnew + '('+ quantity + ')';

												var str = 'tempbatchnew. = ' + tempbatchnew + '<br>';
												str = str + 'quantity = ' + quantity + '<br>';	
												str = str + 'tempBatch = ' + tempBatch + '<br>';	


												nlapiLogExecution('DEBUG', 'tempBatch_if', str);//end

											}
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();
												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												nlapiLogExecution('DEBUG','itemdetails',itemdetails);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												nlapiLogExecution('DEBUG', 'vAdvBinManagement:', vAdvBinManagement);
												nlapiLogExecution('DEBUG', 'vAdvBinManagement:', 'IfSucess');
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);

												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
												nlapiLogExecution('DEBUG', 'compSubRecord:', compSubRecord);
											}
										}
										else {
											if(confirmLotToNS == 'Y')
											{
												//tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
												//20123723 start
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_batch_no')+ '('+ quantity + ')';
												nlapiLogExecution('DEBUG', 'tempBatch:_else', tempBatch);
												//end
											}
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempBatch);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
								if(!vAdvBinManagement)
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);

							}
						trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
						nlapiLogExecution('ERROR', 'itemstatus insert new', itemstatus);
						trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
						trecord.commitLineItem('item');
					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'itemid (Task)', itemid);	
					nlapiLogExecution('DEBUG', 'item_id (Line)', item_id);	

					itemLineNo=parseFloat(itemLineNo)-2;
					if (itemid == item_id && itemLineNo == linenum) {

						commitflag = 'Y';

						var varItemlocation;				
						varItemlocation = getItemStatusMapLoc(itemstatus);

						Itype = nlapiLookupField('item', itemid, 'recordType');


						var columns = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
						if(columns != null && columns != '')
						{
							Itype = columns.recordType;
							serialInflg = columns.custitem_ebizserialin;
							nlapiLogExecution('DEBUG', 'Itype', Itype);
							nlapiLogExecution('DEBUG', 'columns', columns);
							nlapiLogExecution('DEBUG', 'serialInflg', columns);
						}

						nlapiLogExecution('DEBUG', 'Value of J', j);
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', quantity);
						if(trantype!='transferorder')
							trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

						if(whLocation == varItemlocation)
						{
							trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
						}
						else
						{
							TranferInvFlag = 'T';
							trecord.setCurrentLineItemValue('item', 'location', whLocation);
						}

						nlapiLogExecution('DEBUG', 'Serialquantity', quantity);
						nlapiLogExecution('DEBUG', 'Into SerialNos');
						if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
							nlapiLogExecution('DEBUG', 'Into SerialNos');
							nlapiLogExecution('DEBUG', 'invtlp', invtlp);					
							var serialline = new Array();
							var serialId = new Array();

							var tempSerial = "";
							/*var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);							 
							filters[1] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'equalto', linenum);
							filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1,17]);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							 */
							var filters = new Array();

							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
							filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

							nlapiLogExecution('DEBUG', 'xxx', 'xxx');
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
//							var serialnumcsv = "";
							var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
							nlapiLogExecution('DEBUG', 'serchRec', serchRec);			
							if (serchRec) {
								for (var n = 0; n < serchRec.length; n++) {
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');
									}
									else {
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');

									}
									//Case # 20126900�Start 

									/*if(vAdvBinManagement)
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}*/
									//Case # 20126900�End
								}

								serialnumcsv = tempSerial;
								tempSerial = "";
							}

							nlapiLogExecution('DEBUG', 'Serial nums assigned', serialnumcsv);
							nlapiLogExecution('DEBUG', 'Serial nums tempserialId', tempserialId);
							if(!vAdvBinManagement)
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);


						}
						else 
							if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem" ) {
								//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {

								nlapiLogExecution('DEBUG', 'Into LOT/Batch');
								nlapiLogExecution('DEBUG', 'LP:', invtlp);
								var tempBatch = "";
								var batchcsv = "";

								var confirmLotToNS='Y';

								confirmLotToNS=GetConfirmLotToNS(varItemlocation);

								nlapiLogExecution('DEBUG', 'confirmLotToNS:', confirmLotToNS);

								var batfilter = new Array();
								batfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));
								batfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]));
								batfilter.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid));
								batfilter.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
								batfilter.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp));

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';

												}
											}
											//Case # 20126900�Start 
											/*if(vAdvBinManagement)											 
											{
												nlapiLogExecution('DEBUG', 'vAdvBinManagement:', vAdvBinManagement);
												nlapiLogExecution('DEBUG', 'vItQty:', vItQty);
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												if(vItQty !=quantity)
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);	

												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempbatchnew);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
												nlapiLogExecution('DEBUG', 'compSubRecord:', compSubRecord);
											}*/
											//Case # 20126900�End
										}
										else {
											if(confirmLotToNS == 'Y')
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('DEBUG','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';
												}
											}
											//Case # 20126900�Start 
											/*if(vAdvBinManagement)											 
											{
												var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vItQty);
												if(confirmLotToNS == 'Y')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempBatch);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
												compSubRecord.commitLineItem('inventoryassignment');
												compSubRecord.commit();
											}*/
											//Case # 20126900�End
										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}
								nlapiLogExecution('DEBUG', 'LOT/Batch nums assigned', batchcsv);
								if(!vAdvBinManagement)
									trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
						// case no 20126691
						trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
						trecord.commitLineItem('item');
					}
				}
				if (commitflag == 'N') {
					nlapiLogExecution('DEBUG', 'commitflag is N', commitflag);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
					trecord.commitLineItem('item');
				}

			}
			nlapiLogExecution('DEBUG', 'Before Item Receipt---', 'Before');


			if (parseFloat(quantity)>0)
				var idl = nlapiSubmitRecord(trecord);
			nlapiLogExecution('DEBUG', 'Item Receipt idl', idl);


			if(TranferInvFlag == 'T')
				var inventorytransfer = InvokeNSInventoryTransfer(itemid,itemstatus,whLocation,varItemlocation,ActQuantity,batchno,null,null);
			return idl;
		}	
	}
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
		nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('DEBUG', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('DEBUG', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('DEBUG', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', exp.toString());

		nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		transaction.setFieldValue('custrecord_ebiz_error_log', exp);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;

	}
}

function GetSystemRuleForPostItemReceiptby(whloc) //Case# 20149154
{
	try
	{
		var rulevalue='';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
		//case# 20149824
		if(whloc!=null && whloc!='')
		{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
		}
		else if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// Case# 20149154 starts
		/*else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
			}*/
		// Case# 20149154 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		var siteval = '';
		if(searchresult!=null && searchresult!="")
		{
			for(var k = 0;k<searchresult.length;k++)
			{
				siteval = searchresult[k].getValue('custrecord_ebizsite');
				nlapiLogExecution('ERROR','whloc',whloc);
				nlapiLogExecution('ERROR','siteval',siteval);

				if(whloc!=null && whloc!='')
				{
					if(whloc == siteval)
					{
						rulevalue=searchresult[k].getValue('custrecord_ebizrulevalue');
						break;
					}
				}
			}

			if(rulevalue==null || rulevalue=='')
			{
				for(var z = 0;z<searchresult.length;z++)
				{
					siteval = searchresult[z].getValue('custrecord_ebizsite');

					if(siteval==null || siteval=='')
					{
						rulevalue=searchresult[z].getValue('custrecord_ebizrulevalue');
						break;
					}
				}

			}
		}

		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}

//Update SerialNumbers LP value when Invt is Merge
//case 20125737 start 
function UpdateSerialNumLpForInvtMerge(trantype,pointid,linenum,actlp,InvtLP)
{
	nlapiLogExecution('ERROR', 'into UpdateSerialNumLpForInvtMerge', 'done');
	nlapiLogExecution('ERROR', 'trantype', trantype);
	nlapiLogExecution('ERROR', 'pointid', pointid);
	nlapiLogExecution('ERROR', 'linenum', linenum);
	nlapiLogExecution('ERROR', 'actlp', actlp);
	nlapiLogExecution('ERROR', 'InvtLP', InvtLP);

	try {						

		var tempserialId='';
		var filters = new Array();
		if(trantype=='returnauthorization' || trantype=='transferorder' )
		{
			filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
			//Commented for RMA, For RMA no need to check  whether status is checkin/storage
			//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
			filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

		}
		else
		{
			filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
			filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
			filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
			filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', actlp);
		}

		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var serialnumcsv = "";
		var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
		var tempserialId=new Array();
		if (serchRec !=null && serchRec !='' && serchRec.length>0) {
			for (var n1 = 0; n1 < serchRec.length; n1++) {
				//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
				if (tempserialId == "") {
					tempserialId = serchRec[n1].getId();
					tempserial=serchRec[n1].getValue('custrecord_serialnumber');
				}
				else {
					tempserialId = tempserialId + "," + serchRec[n1].getId();
					tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
				}
			}
		}//end
		nlapiLogExecution('ERROR', 'tempserialId', tempserialId);
		if (tempserialId != null && tempserialId !='') {
			var temSeriIdArr = new Array();
			temSeriIdArr = tempserialId.split(',');
			nlapiLogExecution('ERROR', 'updating Serial num Records to', 'Storage Status');
			for (var p = 0; p < temSeriIdArr.length; p++) {
				nlapiLogExecution('ERROR', 'temSeriIdArr.length ', temSeriIdArr.length);
				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialparentid';
				fields[1] = 'custrecord_serialwmsstatus';
				//fields[2] = 'custrecord_serial_lpno';
				values[0] = InvtLP;
				values[1] = '3';
				//values[2] = actlp;

				nlapiLogExecution('ERROR', 'Inside loop serialId ', temSeriIdArr[p]);
				var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
				nlapiLogExecution('ERROR', 'Inside loop serialId ', temSeriIdArr[p]);

				nlapiLogExecution('ERROR', 'UpdateSerialNumLpForInvtMerge', 'LP Merge Status Success');
			}
		}
	} 
	catch (exp) {
		nlapiLogExecution('ERROR', 'UpdateSerialNumLpForInvtMerge exeption', exp);
	}

}
//case 20125737 end
