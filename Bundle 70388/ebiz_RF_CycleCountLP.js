/***************************************************************************
������������������������������� � ����������������������������� ��eBizNET
�����������������������                           eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountLP.js,v $
*� $Revision: 1.9.2.5.4.6.2.8 $
*� $Date: 2014/08/28 11:25:00 $
*� $Author: sponnaganti $
*� $Name: b_WMS_2015_2_StdBundle_Issues $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_CycleCountLP.js,v $
*� Revision 1.9.2.5.4.6.2.8  2014/08/28 11:25:00  sponnaganti
*� Case# 201410024
*� Stnd Bundle Issue fix
*�
*� Revision 1.9.2.5.4.6.2.7  2014/06/13 10:09:34  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.9.2.5.4.6.2.6  2014/05/30 00:34:20  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.9.2.5.4.6.2.5  2013/12/30 15:34:21  rmukkera
*� Case # 20126584
*�
*� Revision 1.9.2.5.4.6.2.4  2013/09/23 07:12:55  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue related case#201216858 is resolved.
*�
*� Revision 1.9.2.5.4.6.2.3  2013/06/10 14:56:06  rmukkera
*� Fix for
*� 	Not able to navigate or perform operations using F7 and F8 keys in Cycle count screen attached
*�
*� Revision 1.9.2.5.4.6.2.2  2013/04/17 16:02:37  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.9.2.5.4.6.2.1  2013/03/05 13:35:38  rmukkera
*� Merging of lexjet Bundle files to Standard bundle
*�
*� Revision 1.9.2.5.4.6  2013/02/14 01:35:04  kavitha
*� CASE201112/CR201113/LOG201121
*� Cycle count process - Issue fixes
*�
*� Revision 1.9.2.5.4.5  2013/02/06 03:32:57  kavitha
*� CASE201112/CR201113/LOG201121
*� Serial # functionality - Inventory process
*�
*� Revision 1.9.2.5.4.4  2013/02/01 07:58:15  rrpulicherla
*� CASE201112/CR201113/LOG201121
*� Cyclecountlatestfixed
*�
*� Revision 1.9.2.5.4.3  2012/10/05 06:34:12  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting Multiple language into given suggestions
*�
*� Revision 1.9.2.5.4.2  2012/09/24 10:08:56  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi Lnaguage
*�
*� Revision 1.9.2.5.4.1  2012/09/21 15:01:30  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.9.2.5  2012/09/03 13:09:02  schepuri
*� CASE201112/CR201113/LOG201121
*� Previous button is driven according to LP Required Rule Value.
*�
*� Revision 1.9.2.4  2012/03/22 14:38:52  spendyala
*� CASE201112/CR201113/LOG201121
*� Check for Lp validation.
*�
*� Revision 1.9.2.3  2012/03/16 14:08:54  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable-button functionality is been added.
*�
*� Revision 1.9.2.2  2012/02/23 00:27:34  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� supress lp in Invtmove and lp merge based on fifodate
*�
*� Revision 1.9.2.1  2012/02/21 13:23:07  schepuri
*� CASE201112/CR201113/LOG201121
*� function Key Script code merged
*�
*� Revision 1.9  2011/12/08 07:24:20  spendyala
*� CASE201112/CR201113/LOG201121
*� solved issues related to expected item, displaying on the screen.
*�
*
****************************************************************************/

/**
 * @author LN
 *@version
 *@date
 */
function CycleCountLP(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
    
		var st0,st1,st2,st3,st4,st5;


		if( getLanguage == 'es_ES')
		{
			st0 = "CICLO DE CUENTA";
			st1 = "N&#218;MERO DE MATR&#205;CULA ESPERADO:";
			st2 = "INGRESAR / ESCANEO DEL N&#218;MERO DE LA MATR&#205;CULA REAL";
			st3 = "";
			st4 = "CONT";
			st5 = "ANTERIOR";
			
		}
		else
		{
			st0 = "CYCLE COUNT LP";
			st1 = "EXP LP NO:";
			st2 = "ENTER/SCAN";
			st3 = "ACTUAL LP NO";
			st4 = "CONT";
			st5 = "PREV";
			st6 = "INVALID LICENSE PLATE #";
		}

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getExpLPNo = request.getParameter('custparam_lpno');
		nlapiLogExecution('ERROR', 'getExpLPNo', getExpLPNo);

		var getRecordId = request.getParameter('custparam_recordid');
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		var rulevalue =  request.getParameter('custparam_ruleValue');
		nlapiLogExecution('ERROR', 'Expected Quantity', getExpectedQuantity);

		var getExpItem = request.getParameter('custparam_expitem');
		//var getExpItemId = request.getParameter('custparam_expitemId');
		var getExpItemId = request.getParameter('custparam_expiteminternalid');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var SiteId=request.getParameter('custparam_siteid');
		var iteminternalId = "";
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'RecordCount', RecordCount);
//		nlapiLogExecution('ERROR', 'request.getParameter custparam_expiteminternalid', request.getParameter('custparam_expiteminternalid'));
//		if(request.getParameter('custparam_expiteminternalid') == null || request.getParameter('custparam_expiteminternalid') == "")
//		{
//		nlapiLogExecution('ERROR', 'Expected Quantity', 'if');

//		iteminternalId = request.getParameter('custparam_expitemno');
//		}
//		else
//		{
//		nlapiLogExecution('ERROR', 'Expected Quantity', 'else');
//		iteminternalId = request.getParameter('custparam_expiteminternalid');
//		}

		nlapiLogExecution('ERROR', 'getExpItemId', getExpItemId);
		if(getExpItemId == null || getExpItemId == "")
		{
			nlapiLogExecution('ERROR', 'Expected Quantity', 'if');

			iteminternalId = request.getParameter('custparam_expitemno');
		}
		else
		{
			nlapiLogExecution('ERROR', 'Expected Quantity', 'else');
			iteminternalId = getExpItemId;
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountlp'); 
		var html = "<html><head><title>" + st0 + " </title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlP').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountlp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getExpLPNo+ "</label>";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnExpectedItem' value=" + getExpItem + ">";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + iteminternalId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + getExpLPNo + ">";
		html = html + "				<input type='hidden' name='hdnrulevalue' value=" + rulevalue + ">";
		html = html + "			    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "			    <input type='hidden' name='hdnSiteId' value=" + SiteId + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2 +  " </td></tr>";
		html = html + " 			<td align = 'left'> " + st3; 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlP' id='enterlP' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlP').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);   
	}
	else 
	{
		try {

			var CCarray = new Array();

			var getActualLP = request.getParameter('enterlP');
			nlapiLogExecution('ERROR', 'Actual LP #', getActualLP);
			
			var getLanguage = request.getParameter('hdngetLanguage');
			CCarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);
	    	
			
			var st6;
			if( getLanguage == 'es_ES')
			{
				
				st6 = "NO V&#193;LIDO PLACA # ";
				
			}
			else
			{
				
				st6 = "INVALID LICENSE PLATE #";
				
			}

			var getActualLP = request.getParameter('enterlP');
			nlapiLogExecution('ERROR', 'Actual LP #', getActualLP);
			var getExistLp = request.getParameter('hdnExpLPNo');
             //Case # 20126584 Start
			/*if(getActualLP=='' || getActualLP==null)
			{
				getActualLP=getExistLp;
			}*/
              //Case # 20126584 End
			var optedEvent = request.getParameter('cmdPrevious');
			var optedEvent1 = request.getParameter('cmdSkip');
			CCarray["custparam_error"] = st6;
			CCarray["custparam_screenno"] = '30';
			CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
			CCarray["custparam_planno"] = request.getParameter('custparam_planno');
			CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
			CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
			CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
			CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
			CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
			CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
			CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
			CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');	
			CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
			CCarray["custparam_ruleValue"] = request.getParameter('hdnrulevalue');
			CCarray["custparam_skustatus"] = '';
			CCarray["custparam_siteid"] = request.getParameter('hdnSiteId');


			nlapiLogExecution('ERROR', 'getExistLp',getExistLp);

			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getExistLp));
			filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
			if (searchresults !=null) 
			{
				CCarray["custparam_packcode"] = searchresults[0].getValue('custrecord_ebiz_inv_packcode');    			
				CCarray["custparam_skustatus"] = searchresults[0].getValue('custrecord_ebiz_inv_sku_status'); 
			}	


			nlapiLogExecution('ERROR', 'custparam_expitem',CCarray["custparam_expitem"]);
			nlapiLogExecution('ERROR', 'custparam_expiteminternalid',CCarray["0"]);

			if (optedEvent == 'F7') 
			{
				nlapiLogExecution('ERROR', 'Cycle Count LP F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
			}
			else if(optedEvent1=='F8')
			{
				nlapiLogExecution('ERROR','SKIP','SKIPthetask');
				nlapiLogExecution('ERROR', 'getRecordId', CCarray["custparam_recordid"]);
				var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
				var skipcount=CCRec.getFieldValue('custrecord_cyc_skip_task');
				nlapiLogExecution('ERROR','skipcount',skipcount);
				
					if(skipcount=='' || skipcount==null)
					{
						skipcount=0;
					}
					skipcount=parseInt(skipcount)+1;
					CCRec.setFieldValue('custrecord_cyc_skip_task',skipcount);
				var id=	nlapiSubmitRecord(CCRec,true);
				nlapiLogExecution('ERROR','skipid',id);
				

				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
			}
			else 
			{
				if (getActualLP != '') 
				{
					if(getExistLp==getActualLP)
					{
						CCarray["custparam_actlp"] = getActualLP;
						nlapiLogExecution('ERROR', 'Actual Quantity');
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
					}
					else
					{
						nlapiLogExecution('ERROR', 'getsysItemLP', 'userdefined');
						//case# 201410024 starts(passing loc and lptype to avoid unnecessary looping)
						//var LPReturnValue = ebiznet_LPRange_CL(getActualLP, '2','');
						var LPReturnValue = ebiznet_LPRange_CL(getActualLP, '2',CCarray["custparam_siteid"],'1');
						//case# 201410024 ends
						if (LPReturnValue == true)
						{
							var LpStatus=CheckLpMaster(getActualLP);
							if(LpStatus=='N')
							{
								CCarray["custparam_actlp"] = getActualLP;
								nlapiLogExecution('ERROR', 'LpStatus is N');
								response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
							}
							else
							{
								CCarray["custparam_error"] = "LP already exists" ;
								nlapiLogExecution('ERROR', 'LpStatus is Y');
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
								nlapiLogExecution('ERROR', 'Cycle Count LP not found1');
							}

						}
						else 
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
							nlapiLogExecution('ERROR', 'Cycle Count LP not found2');
						}
					}
				}
				else 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					nlapiLogExecution('ERROR', 'Cycle Count LP not found3');
				}
			}
		}
		catch(e)
		{
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			nlapiLogExecution('ERROR', 'Cycle Count LP not found4');
		}
	}
}


function CheckLpMaster(vLp)
{
	try {
		var vLpExists;
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', vLp);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('ERROR', 'LP FOUND');

			vLpExists = 'Y';
		}
		else {
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', vLp);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', vLp);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			vLpExists = 'N';
		}
		return vLpExists;
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}
}
