

/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_OrdSummaryDetailReport_SL.js,v $
 *     	   $Revision: 1.3.10.2 $
 *     	   $Date: 2012/10/02 22:45:04 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_NSWMS_2013_1_2_4 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_OrdSummaryDetailReport_SL.js,v $
 * Revision 1.3.10.2  2012/10/02 22:45:04  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.2  2011/11/02 14:17:55  rmukkera
 * CASE201112/CR201113/LOG201121
 * replaced the location field in the report with printed field
 *
 * Revision 1.1  2011/11/02 12:33:33  rmukkera
 * CASE201112/CR201113/LOG201121
 *
 * new file
 * v */






function OrderSummaryDetailsPDFReport(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Order Summary Details Report');	

		var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

		var filters = new Array();	

		if (( request.getParameter('custpage_fromdate') != "" &&  request.getParameter('custpage_fromdate') != null) && ( request.getParameter('custpage_todate') != "" &&  request.getParameter('custpage_todate') != null) ) {

			filters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within',  request.getParameter('custpage_fromdate'),  request.getParameter('custpage_todate')));			 


		} 
		if (( request.getParameter('custpage_fulfillmentorder') != "" &&  request.getParameter('custpage_fulfillmentorder') != null )){

			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'is',  request.getParameter('custpage_fulfillmentorder')));

		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['1','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','24','25','26','27','28','29','30']));


		}
		if ( request.getParameter('custpage_order') !=null &&  request.getParameter('custpage_order') !=""){


			filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is',  request.getParameter('custpage_order')));
		}
		if ( request.getParameter('custpage_wave') !=null &&  request.getParameter('custpage_wave') !=""){


			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto',  request.getParameter('custpage_wave')));
		}

		if ( request.getParameter('custpage_customer') !=null &&  request.getParameter('custpage_customer') !=""){


			filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'anyof', [ request.getParameter('custpage_customer')]));
		}
		if ( request.getParameter('custpage_location') !=null &&  request.getParameter('custpage_location') !=""){

			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',  request.getParameter('custpage_location')));
		}

		if ( request.getParameter('custpage_salesorder') !=null &&  request.getParameter('custpage_salesorder') !=""){

			filters.push(new nlobjSearchFilter('name', null, 'is', request.getParameter('custpage_salesorder')));
		}

		nlapiLogExecution('ERROR', 'filters.length', filters.length);
		var salesColumns = new Array();
		salesColumns[0] = new nlobjSearchColumn('custrecord_ns_ord');
		salesColumns[1] = new nlobjSearchColumn('custrecord_do_customer');
		salesColumns[2] = new nlobjSearchColumn('custrecord_record_linedate');
		//salesColumns[3] = new nlobjSearchColumn('status', 'custrecord_ns_ord');
		salesColumns[3] = new nlobjSearchColumn('custrecord_lineord');
		salesColumns[4] = new nlobjSearchColumn('custrecord_linestatus_flag');
		salesColumns[5] = new nlobjSearchColumn('custrecord_ebiz_wave');
		salesColumns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');

		salesColumns[7] = new nlobjSearchColumn('custrecord_ord_qty');
		salesColumns[8] = new nlobjSearchColumn('custrecord_pickqty');		
		salesColumns[9] = new nlobjSearchColumn('custrecord_linestatus_flag');
		//salesColumns[11] = new nlobjSearchColumn('trackingnumbers', 'custrecord_ns_ord');
		//salesColumns[10] = new nlobjSearchColumn('buildentireassembly', 'custrecord_ebiz_linesku');

		salesColumns[10] = new nlobjSearchColumn('custrecord_do_order_priority');
		salesColumns[11] = new nlobjSearchColumn('custrecord_ordline_wms_location');
		salesColumns[12] = new nlobjSearchColumn('custrecord_lineord');
		salesColumns[13] = new nlobjSearchColumn('custrecord_ship_qty');
		salesColumns[14] = new nlobjSearchColumn('custrecord_printflag');
		var salessearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, salesColumns);
		//nlapiLogExecution('ERROR','searchresults',searchresults.length);
		var vline, vitem, vqty, vTaskType,vlotbatch, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono,vcontlp,vcontsize,vclusno;
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"7\">\n";
		//nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");
		var strxml ="<table width='100%'> ";
//		strxml += "<tr align='center'>";
//		strxml += "<td>";
		strxml += "<tr ><td valign='top' align='left' width='30%'></td><td valign='middle' align='left'  style='font-weight:bold;font-size:14;text-decoration:underline;'>";
		strxml += "Order Summary Details Report";		
		//strxml += "PICK LIST FOR WAVE # " + vQbWave ;
		strxml += "</td></tr></table><table border='1' width='100%'>";
		strxml =strxml+  "<tr style=\"font-weight:bold;background-color:gray;color:white;font-size:7\"><td width='5%' style='border-bottom: 1px; border-right: 1px;border-color: #000000;white-space: nowrap;'>";
		strxml += "Ord #";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;' >";
		strxml += "Customer #";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='20%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Order Date";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='20%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;' >";
		strxml += "Order Priority";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='10%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Wave #";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";		
		strxml += "<td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";			
		strxml += "Item";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Order Qty";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Picked Qty";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='5%' style='border-bottom: 1px; border-right: 1px;border-color: #000000;white-space: nowrap;'>";
		strxml += "Ship Qty";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='7%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
		strxml += "Printed?";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='6%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Fullfiment Order #";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='10%' style='border-bottom: 1px; border-color: #000000'>";
		strxml += "Fullfiment Order Status#";
		strxml =strxml+  "</td></tr>";
		//nlapiLogExecution('ERROR','searchresults',searchresults.length);
		if (salessearchresults != null) {
			for (var i = 0; i < salessearchresults.length; i++) {


				var dono=salessearchresults[i].getValue('custrecord_lineord');
				var temparray=dono.split('.');

				var ordno = temparray[0];
				var customer= salessearchresults[i].getText('custrecord_do_customer');
				var orddate = salessearchresults[i].getValue('custrecord_record_linedate');
				//var nsstatus = salessearchresults[i].getValue('status', 'custrecord_ns_ord');
				//var trackingno = salessearchresults[i].getValue('trackingnumbers', 'custrecord_ns_ord');

				var fulfillordno = salessearchresults[i].getValue('custrecord_lineord');
				var fulfillordstatus= salessearchresults[i].getText('custrecord_linestatus_flag');
				var fulfillordstatusvalue= salessearchresults[i].getValue('custrecord_linestatus_flag');
				var waveno = salessearchresults[i].getValue('custrecord_ebiz_wave');
				var item = salessearchresults[i].getText('custrecord_ebiz_linesku');
				var assembly = '';//salessearchresults[i].getValue('buildentireassembly', 'custrecord_ebiz_linesku');
				var Priority = salessearchresults[i].getValue('custrecord_do_order_priority');
				var location = salessearchresults[i].getText('custrecord_ordline_wms_location');
				var shipqty = salessearchresults[i].getValue('custrecord_ship_qty');
				var print= salessearchresults[i].getValue('custrecord_printflag');
				var printflag="";
				if(print=='T')
				{
					printflag="Yes";
				}
				else
				{
					printflag="No";
				}

				if(assembly == 'T')
					assembly='YES';
				else
					assembly='NO';

				var fulfillflag = salessearchresults[i].getValue('custrecord_linestatus_flag');
				//nlapiLogExecution('ERROR', 'fulfillflag', fulfillflag);
				if(fulfillflag == '25')
					fulfillflag='YES';
				else
					fulfillflag='NO';
				var ordqty = salessearchresults[i].getValue('custrecord_ord_qty');
				var pickedqty = salessearchresults[i].getValue('custrecord_pickqty');
				nlapiLogExecution('ERROR', 'ordqty', ordqty);
				strxml =strxml+  "<tr><td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += ordno;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += customer.replace(replaceChar,'');
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='10%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += orddate;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='10%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += Priority;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += waveno;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";				
				strxml += "<td width='5%'  style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";			
				strxml += item;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += ordqty;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += pickedqty;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='5%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += shipqty;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='10%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += printflag;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='10%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += fulfillordno;
				strxml +="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='10%' style='border-bottom: 1px; border-color: #000000'>";
				strxml += fulfillordstatus;
				strxml +=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
			}
		}
		strxml =strxml+"</table>";		
		strxml =strxml+ "\n</body>\n</pdf>";		
		nlapiLogExecution('ERROR', 'strxml', strxml);
		xml=xml +strxml;
		nlapiLogExecution('ERROR','XML',xml);

		var file = nlapiXMLToPDF(xml);	
		response.setContentType('PDF','OrderSummaryDetailsReport.pdf');
		response.write( file.getValue() );
	}
	else
	{

	}
}