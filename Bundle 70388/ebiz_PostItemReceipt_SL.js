/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_PostItemReceipt_SL.js,v $
<<<<<<< ebiz_PostItemReceipt_SL.js
 *     	   $Revision: 1.1.2.38.2.3 $
 *     	   $Date: 2015/11/20 10:53:26 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.1.2.38.2.3 $
 *     	   $Date: 2015/11/20 10:53:26 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.1.2.24
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PostItemReceipt_SL.js,v $
 * Revision 1.1.2.38.2.3  2015/11/20 10:53:26  grao
 * 2015.2 Issue Fixes 201415783
 *
 * Revision 1.1.2.38.2.2  2015/11/18 11:52:00  gkalla
 * case# 201415748
 * Added wms status flag filter with putaway completed, check in.
 *
 * Revision 1.1.2.38.2.1  2015/10/07 13:16:32  schepuri
 * case# 201414894
 *
 * Revision 1.1.2.38  2015/08/17 10:56:28  skreddy
 * Case# 201414022
 * Bedgear SB issue fix
 *
 * Revision 1.1.2.37  2015/08/13 15:35:45  skreddy
 * Case# 201413992,201413993
 * Bedgear SB issue fix
 *
 * Revision 1.1.2.36  2015/08/05 04:33:38  schepuri
 * case# 201413736
 *
 * Revision 1.1.2.35  2015/07/28 15:29:19  skreddy
 * Case# 201413628
 * Sighwarehouse SB issue fix
 *
 * Revision 1.1.2.34  2015/07/28 13:36:07  schepuri
 * case# 201413606
 *
 * Revision 1.1.2.33  2015/07/23 15:46:01  nneelam
 * case# 201413493
 *
 * Revision 1.1.2.32  2015/06/18 15:59:01  nneelam
 * case# 201413007
 *
 * Revision 1.1.2.31  2015/06/12 15:49:44  grao
 * SB issue fixes 201412880
 *
 * Revision 1.1.2.30  2015/06/01 15:42:12  grao
 * SB  2015.2 issue fixes  201412877
 *
 * Revision 1.1.2.29  2015/05/14 12:40:26  skreddy
 * Case# 201412777
 * signwarehouse SB issue fix
 *
 * Revision 1.1.2.28  2015/02/23 17:51:34  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.27  2015/02/18 14:26:02  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.26  2015/01/27 13:49:07  schepuri
 * issue # 201411360
 *
 * Revision 1.1.2.25  2014/09/19 19:07:32  grao
 * Case#: 201410450 CT issue Sb fixes
 *
 * Revision 1.1.2.24  2014/09/19 11:40:00  skavuri
 * case # 201410246
 *
 * Revision 1.1.2.23  2014/09/01 14:08:53  skreddy
 * case # 20148852
 * post item receipt by scheduler
 *
 * Revision 1.1.2.22  2014/08/25 06:38:10  skreddy
 * case # 20148852
 * post item receipt by scheduler
 *
 * Revision 1.1.2.21  2014/07/23 15:29:01  sponnaganti
 * Case# 20149602
 * Compatibility Issue fix
 *
 * Revision 1.1.2.20  2014/07/22 16:03:14  sponnaganti
 * Case# 20149601
 * Compatibility Issue fix
 *
 * Revision 1.1.2.19  2014/07/04 14:22:20  skavuri
 * Case # 20149236 Compatibility Issue Fixed
 *
 * Revision 1.1.2.18  2014/06/12 13:43:18  skreddy
 * case # 20148847
 * Sonic SB issue fix
 *
 * Revision 1.1.2.17  2014/04/29 15:17:09  rmukkera
 * Case # 20141363
 *
 * Revision 1.1.2.16  2014/04/17 14:23:02  rmukkera
 * Case # 20141363
 *
 * Revision 1.1.2.15  2014/04/15 15:50:29  nneelam
 * case#  20148010
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.14  2014/04/14 15:52:23  nneelam
 * case#  20141363
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.13  2014/04/09 15:45:48  nneelam
 * case#  20141363�
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.12  2014/03/28 15:26:38  skavuri
 * Case # 20127871 issue fixed
 *
 * Revision 1.1.2.11  2014/03/03 07:19:03  snimmakayala
 * 20127286
 *
 * Revision 1.1.2.10  2014/02/25 23:56:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127387
 *
 * Revision 1.1.2.9  2014/02/13 15:13:56  rmukkera
 * Case # 20127042,20127117
 *
 * Revision 1.1.2.8  2014/02/12 15:16:17  grao
 * Case# 20127117 related issue fixes in MHP SB issue fixes
 *
 * Revision 1.1.2.7  2014/02/07 15:48:24  nneelam
 * case#  20127042
 * std bundle issue fix
 *
 * Revision 1.1.2.6  2014/02/06 14:53:43  rmukkera
 * Case # 20127077
 *
 * Revision 1.1.2.5  2014/02/04 14:25:37  rmukkera
 * Case # 20126968
 *
 * Revision 1.1.2.4  2013/12/02 08:54:37  schepuri
 * 20125992,20125937
 *
 * Revision 1.1.2.3  2013/11/22 16:29:28  gkalla
 * case#20125888
 * Post Item receipt error
 *
 * Revision 1.1.2.2  2013/10/10 19:10:17  grao
 * Issue fixes reelated to the 20124983
 *
 * Revision 1.1.2.1  2013/07/25 08:59:08  rmukkera
 * landed cost cr new file
 *
 * Revision 1.23.2.41.4.12.2.25  2013/06/04 11:54:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.24  2013/06/04 11:05:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.23  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.22  2013/05/31 15:17:36  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.23.2.41.4.12.2.21  2013/05/16 11:25:03  spendyala
 * CASE201112/CR201113/LOG201121
 * InvtRef# field of opentask record will be updated
 * to putaway task, once inbound process in completed
 *
 * Revision 1.23.2.41.4.12.2.20  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.19  2013/05/14 14:17:28  schepuri
 * Surftech issues of serial status update
 *
 * Revision 1.23.2.41.4.12.2.18  2013/05/10 11:02:30  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.17  2013/05/09 12:47:51  spendyala
 * CASE201112/CR201113/LOG2012392
 * Line level location is considered first if it is empty then header level location is taken.
 *
 * Revision 1.23.2.41.4.12.2.16  2013/05/03 15:37:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.23.2.41.4.12.2.15  2013/05/01 12:45:02  rmukkera
 * Changes done in the code to submit the more than 3800 chars to the net suite serial numbers.
 *
 * Revision 1.23.2.41.4.12.2.14  2013/04/29 14:41:15  schepuri
 * uom conversion issues
 *
 * Revision 1.23.2.41.4.12.2.13  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.23.2.41.4.12.2.12  2013/04/16 15:05:06  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.11  2013/04/16 12:48:56  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.10  2013/04/10 15:48:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.9  2013/04/04 16:00:13  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related  to Putgenqty in PO status Report
 *
 * Revision 1.23.2.41.4.12.2.8  2013/04/04 15:08:27  rmukkera
 * Issue Fix related to Tsg while TO receiving for Serial item, system allows to scan the new serial#
 *
 * Revision 1.23.2.41.4.12.2.7  2013/04/03 06:45:58  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of standard bundle
 *
 * Revision 1.23.2.41.4.12.2.6  2013/04/02 14:38:18  schepuri
 * tsg sb putaway qty exception
 *
 * Revision 1.23.2.41.4.12.2.5  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.4  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.3  2013/03/13 13:57:16  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.23.2.41.4.12.2.2  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.23.2.41.4.12.2.1  2013/02/26 12:52:10  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.23.2.41.4.12  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.23.2.41.4.11  2013/02/07 08:40:30  skreddy
 * CASE201112/CR201113/LOG201121
 * RF Lot auto generating FIFO enhancement
 *
 * Revision 1.23.2.41.4.10  2013/01/08 15:41:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet issue for batch entry irrespective of location
 *
 * Revision 1.23.2.41.4.9  2013/01/08 14:17:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.23.2.41.4.8  2013/01/04 14:53:24  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.41.4.7  2012/12/17 15:14:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Reapting same lp after confirm putaway
 *
 * Revision 1.23.2.41.4.6  2012/12/12 16:02:32  gkalla
 * CASE201112/CR201113/LOG201121
 * Actual Bin management enhancement
 *
 * Revision 1.23.2.41.4.5  2012/12/11 16:24:45  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.41.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.23.2.41.4.3  2012/10/03 11:33:31  schepuri
 * CASE201112/CR201113/LOG201121
 * NSUOM Convertion
 *
 * Revision 1.23.2.41.4.2  2012/09/27 10:57:49  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.23.2.41.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.23.2.41  2012/09/03 13:45:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.23.2.40  2012/08/27 15:24:28  schepuri
 * CASE201112/CR201113/LOG201121
 * fetched location is considered with uppercase
 *
 * Revision 1.23.2.39  2012/08/24 18:13:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changes
 *
 * Revision 1.23.2.38  2012/08/10 14:46:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To create inventory if merged qty greater than pallet qty
 *
 * Revision 1.23.2.37  2012/08/10 14:45:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation issue when F7 is pressed resolved.
 *
 * Revision 1.23.2.36  2012/08/06 06:49:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Quantity exception issues is resolved.
 *
 * Revision 1.23.2.35  2012/07/31 06:54:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Creating Inventory was resolved.
 *
 * Revision 1.23.2.34  2012/07/30 23:17:32  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.23.2.33  2012/07/10 23:15:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert Mails
 *
 * Revision 1.23.2.32  2012/06/15 07:13:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Merge LP is fixed.
 *
 * Revision 1.23.2.31  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.30  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.23.2.29  2012/05/23 14:16:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.28  2012/05/18 17:58:28  gkalla
 * CASE201112/CR201113/LOG201121
 * TO Item receipt issue fixed. Commented partial TO receipt check condition
 *
 * Revision 1.23.2.27  2012/05/16 13:22:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM changes
 *
 * Revision 1.23.2.26  2012/05/11 14:50:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to item receipt is resolved.
 *
 * Revision 1.23.2.25  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.23.2.24  2012/05/09 14:47:33  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Fifo value for BatchItem while creating inv rec is fixed.
 *
 * Revision 1.23.2.23  2012/05/09 14:37:27  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating Inv Record ExpireDate is not getting updated for a batch item,issue is resolved.
 *
 * Revision 1.23.2.22  2012/05/07 09:36:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * confirm putaway
 *
 * Revision 1.23.2.21  2012/05/04 13:54:05  gkalla
 * CASE201112/CR201113/LOG201121
 * Putaway qty exception for Batch item
 *
 * Revision 1.23.2.20  2012/04/30 15:09:07  gkalla
 * CASE201112/CR201113/LOG201121
 * To check lineno while doing Itemreceipt in transferorder
 *
 * Revision 1.23.2.19  2012/04/30 10:00:47  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal'
 *
 * Revision 1.23.2.18  2012/04/13 23:12:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF CART PUTAWAY issues.
 *
 * Revision 1.23.2.17  2012/04/03 11:05:06  gkalla
 * CASE201112/CR201113/LOG201121
 * Added WMS status filter for Merge LP
 *
 * Revision 1.23.2.16  2012/04/02 06:16:19  spendyala
 * CASE201112/CR201113/LOG201121
 * set true lot in our custom records.
 *
 * Revision 1.23.2.15  2012/03/28 12:48:03  schepuri
 * CASE201112/CR201113/LOG201121
 * validating from entering another loc other than fetched location
 *
 * Revision 1.23.2.14  2012/03/21 11:09:44  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.23.2.13  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.23.2.12  2012/03/12 13:51:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing  Record id to the query string.
 *
 * Revision 1.23.2.11  2012/03/09 07:45:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunk.
 *
 * Revision 1.23.2.10  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.23.2.9  2012/02/24 00:01:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * merge lp in cart putaway
 *
 * Revision 1.23.2.8  2012/02/23 23:09:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * merge lp
 *
 * Revision 1.23.2.7  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.23.2.6  2012/02/22 12:37:46  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.23.2.5  2012/02/16 14:08:11  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating  inventory records, order# field is updating with purchase order#
 *
 * Revision 1.23.2.4  2012/02/13 23:21:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.28  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.27  2012/02/10 08:58:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.26  2012/01/20 19:02:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Confirm Putaway Issue
 *
 * Revision 1.25  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.24  2012/01/09 13:16:10  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.23  2011/12/28 23:14:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Putaway changes
 *
 * Revision 1.22  2011/12/28 13:20:51  spendyala
 * CASE201112/CR201113/LOG201121
 * resolved issues related to qty exception while entering data into transaction order line table
 *
 * Revision 1.21  2011/12/28 06:57:22  spendyala
 * CASE201112/CR201113/LOG201121
 * made changes for qty exception
 *
 * Revision 1.20  2011/12/24 15:52:57  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.19  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.18  2011/12/23 13:32:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.17  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.16  2011/11/17 08:55:20  spendyala
 * CASE201112/CR201113/LOG201121
 * changed the wms flag for DeleteInvtRecCreatedforCHKNTask rec in crete inventory
 *
 * Revision 1.15  2011/10/24 21:36:20  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wrong Status updated in Inventory.
 * It should be 19.
 *
 * Revision 1.14  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/28 16:45:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/28 11:37:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.11  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/08/24 12:43:03  schepuri
 * CASE201112/CR201113/LOG201121
 * RF Putaway Location based on putseq no
 *
 * Revision 1.8  2011/08/23 09:42:59  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/07/12 06:39:08  pattili
 * CASE201112/CR201113/LOG201121
 * 1. Changes to RF Putaway SKU and Putaway Location.
 *
 * Revision 1.6  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.5  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.4  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PostItemReceipt(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Post Item Receipt');

		var soField = form.addField('custpage_po', 'text', 'Order#').setMandatory(true);
		form.setScript('customscript_ebiz_postitemreceipt_cl');
		var tempvar = form.addField('custpage_temp', 'text', 'tempvar').setDisplayType('hidden');
		var tempvarnew = form.addField('custpage_tempnew', 'text', 'tempvar').setDisplayType('hidden');//Case# 20149236
		var OrderType = form.addField('custpage_ordertype', 'select', 'Order Type').setMandatory(true);
		OrderType.addSelectOption("","");
		OrderType.addSelectOption("purchaseorder","PO");
		OrderType.addSelectOption("transferorder","TO");
		OrderType.addSelectOption("returnauthorization","RMA");
		if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
		{
			OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
		}
		//var soField = form.addField('custpage_po', 'text', 'Enter PO/TO').setMandatory(true);
		form.setScript('customscript_ebiz_postitemreceipt_cl');
		//var tempvar = form.addField('custpage_temp', 'text', 'tempvar').setDisplayType('hidden');// Case #20127871
		//tempvar.setDefaultValue(soField);
		form.addSubmitButton('Post Item Receipt');
		response.writePage(form);
	}
	else {
		nlapiLogExecution('ERROR', 'into', 'response');
		var form = nlapiCreateForm('Post Item Receipt');
		var tempvar = form.addField('custpage_temp', 'text', 'tempvar').setDisplayType('hidden');
		if(request.getParameter('custpage_po')!=null && request.getParameter('custpage_po')!='')
		{
			tempvar.setDefaultValue(request.getParameter('custpage_po'));
		}

		var tempvarnew = form.addField('custpage_tempnew', 'text', 'tempvar').setDisplayType('hidden');
		var soField = form.addField('custpage_po', 'text', 'Order#').setMandatory(true);// case# 201411360
		var OrderType = form.addField('custpage_ordertype', 'select', 'Order Type').setMandatory(true);
		OrderType.addSelectOption("","");
		OrderType.addSelectOption("purchaseorder","PO");
		OrderType.addSelectOption("transferorder","TO");
		OrderType.addSelectOption("returnauthorization","RMA");

		var ordertype = request.getParameter('custpage_ordertype');

		if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
		{
			OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
		}

		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null).setLayoutType('outside','startcol');		  

		var POarray = new Array();
		//var soField = form.addField('custpage_po', 'text', 'Order#').setMandatory(true);
		POarray["custparam_screenno"] = 'PR01';
		var getPOid=request.getParameter('custpage_po');
		nlapiLogExecution('ERROR', 'into ordertype', ordertype);
		//var trantype = nlapiLookupField('transaction', getPOid, 'recordType');

		if(getPOid !=null && getPOid!='')
		{
			form.setScript('customscript_ebiz_postitemreceipt_cl');
			var POtrantypefilters=new Array();
			POtrantypefilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid.toString()));
			POtrantypefilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

			var POtrantypecols=new Array();
			POtrantypecols[0]=new nlobjSearchColumn('internalid');

			if(ordertype == 'purchaseorder')
				var PORecinternalids=nlapiSearchRecord('purchaseorder',null,POtrantypefilters,POtrantypecols);
			else if(ordertype == 'returnauthorization')
				var PORecinternalids=nlapiSearchRecord('returnauthorization',null,POtrantypefilters,POtrantypecols);
			else
				var PORecinternalids=nlapiSearchRecord('transferorder',null,POtrantypefilters,POtrantypecols);
			nlapiLogExecution('ERROR','PORecinternalids',PORecinternalids);

			var poid='';
			if(PORecinternalids!=null && PORecinternalids!='')
			{
				poid=PORecinternalids[0].getValue('internalid');
				//}
				nlapiLogExecution('ERROR','poid',poid);
				if(poid!=null&&poid!="")
					trantype = nlapiLookupField('transaction', poid, 'recordType');
				nlapiLogExecution('ERROR','trantype',trantype);

				tempvar.setDefaultValue(poid);
				nlapiLogExecution('ERROR','trantype',trantype);
				POarray["custparam_trantype"] = trantype;
				var POfilters=new Array();
				POfilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
				POfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
				POfilters.push(new nlobjSearchFilter('recordtype',null,'is',trantype));
				var vRoleLocation=getRoledBasedLocation();
				if(trantype == 'transferorder'){
					if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
					{
						POfilters.push(new nlobjSearchFilter('transferlocation', null, 'anyof', vRoleLocation));
					}
				}
				else{
					if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
					{
						POfilters.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
					}
				}

				var POcols=new Array();
				POcols[0]=new nlobjSearchColumn('status');
				POcols[1]=new nlobjSearchColumn('location');
//				POcols[2]= new nlobjSearchColumn('custbody_nswms_company');

				var PORec=nlapiSearchRecord('transaction',null,POfilters,POcols);
				if(PORec!=null&&PORec!='')
				{
					var poStatus=PORec[0].getValue('status');
					nlapiLogExecution('ERROR','poStatus',poStatus);
					var poToLocationID=PORec[0].getValue('location');
					nlapiLogExecution('ERROR','poToLocation',poToLocationID);

					var fields = ['custrecord_ebizwhsite'];
					var locationcolumns = nlapiLookupField('Location', poToLocationID, fields);
					var Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
					nlapiLogExecution('ERROR','Tomwhsiteflag',Tomwhsiteflag);
					// Call method to retrieve the PO based on the PO ID

					//Case# 201410246
					//if(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' ||poStatus=='pendingBillPartReceived'|| poStatus=='pendingRefundPartReceived')
					if(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' ||poStatus=='pendingBillPartReceived' ||poStatus=='pendingReceiptPartFulfilled' ||poStatus=='pendingRefundPartReceived')
					{
						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2,6]));
						filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
						filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isempty'));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

						var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
						nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
						if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
						{
							//Case # 20126968 Start

							var filters = new Array();
							filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
							filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
							filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
							filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
							filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

							var columns=new Array();
							columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
							columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
							columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
							columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
							columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');

							var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
							nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);

							if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
							{
								//Case # 20126968 End
								nlapiLogExecution('ERROR','opentaskSearchResults.length111',opentaskSearchResults.length);
								POarray["custparam_error"] = 'There are open putaway records for this PO/TO,<br>Do u want to Continue';
								POarray["custparam_poid"] = poid;

								//tempvar.setDefaultValue('open putaways');
								var tempvarnew=request.getParameter('custpage_tempnew');

								nlapiLogExecution('ERROR','tempvarnew',tempvarnew);
								//partial itemreceipt generation
								if(tempvarnew!=null && tempvarnew!='' && tempvarnew=='yes')
								{

									var filters = new Array();
									filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
									filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
									filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
									filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
									filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

									var columns=new Array();
									columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
									columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
									columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
									columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
									//columns[2]=new nlobjSearchColumn('custrecord_act_qty',null,'sum');
									//columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
									columns[3]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
									columns[4]=new nlobjSearchColumn('custrecord_item_status_location_map','custrecord_sku_status','group');
									columns[5]=new nlobjSearchColumn('custrecord_sku_status',null,'group');

									var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
									nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);



									if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
									{
										nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
										if(parseInt(opentaskSearchResults.length)>=50)
										{
											nlapiLogExecution('ERROR','invoke Scheduler','done in if');
											var param = new Array();
											param['custscript_poid'] = poid;
											param['custscript_trantype'] = trantype;
											param['custscript_locationid']=poToLocationID
											nlapiScheduleScript('customscript_ebiz_postitem_receipt_sch', null,param);
											msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Item Receipt posting has been initiated successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
										}
										else
										{

											var ItemStatusArray =new Array();
											var ItemStatusMapLocArray =new Array();
											var itemReceiptArray =new Array();
											for(var k=0;k<opentaskSearchResults.length;k++)
											{
												var	itemstatus1=opentaskSearchResults[k].getValue('custrecord_sku_status',null,'group');
												nlapiLogExecution('ERROR','itemstatus1',itemstatus1);
												var	MapLoc=opentaskSearchResults[k].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');

												if(ItemStatusArray.indexOf(itemstatus1) == -1)
												{
													ItemStatusArray.push(itemstatus1);
												}
												if(ItemStatusMapLocArray.indexOf(MapLoc) == -1)
												{
													ItemStatusMapLocArray.push(MapLoc);
											}
											}
											//for(j=0;j<ItemStatusArray.length;j++)
											for(j=0;j<ItemStatusMapLocArray.length;j++)
											{
												//var vItemStatus = ItemStatusArray[j];
												var WHLocation = ItemStatusMapLocArray[j];
												var trecord = nlapiTransformRecord(trantype, poid, 'itemreceipt');
												for(var i=0;i<opentaskSearchResults.length;i++)
												{
													var ActQuantity=opentaskSearchResults[i].getValue('formulanumeric',null,'SUM');
													var linenum=opentaskSearchResults[i].getValue('custrecord_line_no',null,'group');
													var	itemid=opentaskSearchResults[i].getValue('custrecord_sku',null,'group');
													var	itemstatus=opentaskSearchResults[i].getValue('custrecord_sku_status',null,'group');
													var	itemstatus;
													//case # 20141363�
													var	batchno=opentaskSearchResults[i].getValue('custrecord_batch_no',null,'group');
													var	MapLoc=opentaskSearchResults[i].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');
													nlapiLogExecution('ERROR','MapLoc',MapLoc);
													nlapiLogExecution('ERROR','WHLocation',WHLocation);
													//var invtlp = opentaskSearchResults[i].getValue('custrecord_ebiz_lpno',null,'group');
													if(WHLocation == MapLoc)
													{
													//var invtlp = opentaskSearchResults[i].getValue('custrecord_ebiz_lpno',null,'group');
													nlapiLogExecution('ERROR', 'invtlp here 2', invtlp);	
													//generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,poToLocationID,invtlp);
													//generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,'',poToLocationID,invtlp);
														generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,batchno,poToLocationID,itemstatus,MapLoc);
													}

										}
										if(trecord != null && trecord != '')
										{
											idl = nlapiSubmitRecord(trecord);
											nlapiLogExecution('ERROR','idl',idl);
											var currentrow=[ItemStatusArray[j],idl];
											itemReceiptArray.push(currentrow);
													if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
													{
														UpdateSerialNumbersStatus(SerialArray);
														SerialArray.length=0; //clear the Serial# Array
													}
										}
											}
										nlapiLogExecution('ERROR','idl',idl);
										if(idl!=null && idl!='')
										{	
											msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Item Receipt Posted Sucessfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");

											var arrayLP = new Array();
											var SerialLP = new Array();
											var filtersLP = new Array();
											if(poid != null && poid != "")
												filtersLP.push(new nlobjSearchFilter('custrecord_serialebizpono', null, 'is',poid));
											filtersLP.push(new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'S'));
											var lpcolumns = new Array();
											lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
											lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
											var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersLP, lpcolumns);
											for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {

												arrayLP.push(lpsearchresults[i].getId());
											}
											nlapiLogExecution('ERROR','arrayLP in PUTW creation',arrayLP);
											for(var n=0;arrayLP != null && n < arrayLP.length;n++)
											{
												var fields = new Array();
												var values = new Array();

												fields[0] = 'custrecord_serialstatus';

												values[0] = 'I';
												var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', arrayLP[n], fields, values);
												nlapiLogExecution('ERROR','updateStatus in PUTW creation',updateStatus);
											}
												if(trantype=='transferorder')
													InvokeInventoryTransfer(poid);
											if(itemReceiptArray !=null && itemReceiptArray !='' && itemReceiptArray.length>0)
											{
												nlapiLogExecution('ERROR','itemReceiptArray',itemReceiptArray);
												for (var cnt=0;cnt<itemReceiptArray.length ;cnt++)
												{
													var vItemstatus=itemReceiptArray[cnt][0];
													var IRid=itemReceiptArray[cnt][1];
													//nlapiLogExecution('ERROR','IRid',IRid);
													var SeachResults=OpentaskrecordsforItemStatus(poid,vItemstatus);

													for(var M=0;M<SeachResults.length;M++)
													{
														nlapiLogExecution('ERROR','SeachResults[M]',SeachResults[M]);
														var updateStatus = nlapiSubmitField('customrecord_ebiznet_trn_opentask', SeachResults[M].getId(), 'custrecord_ebiz_nsconfirm_ref_no', IRid);
													}

												}

											}
											var filters2 = new Array();
											filters2.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
											filters2.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
											//filters2.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
											filters2.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
											filters2.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
											
										
											
										//	var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
											var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters2,null);
											
											if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
											{
												nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
												for(var j=0;j<opentaskSearchResults.length;j++)
												{
													nlapiLogExecution('ERROR','opentaskSearchResultsID',opentaskSearchResults[j].getId());
													//case# 20141363?
													MoveTaskRecord(opentaskSearchResults[j].getId(),idl);
												}
											}
										}





										}

									}

									soField.setDefaultValue(getPOid);
								}
								//Case # 20126968 Start
							}
							else
							{
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'There are no putaway records for this PO/TO,', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							}
							//Case # 20126968 End


						}
						else
						{
							tempvar.setDefaultValue('no open putaways');
							var filters = new Array();
							filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
							filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
							filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
							filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
							filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

							var columns=new Array();
							columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
							columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
							columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
							columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
							//columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
							columns[3]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
							columns[4]=new nlobjSearchColumn('custrecord_item_status_location_map','custrecord_sku_status','group');
							columns[5]=new nlobjSearchColumn('custrecord_sku_status',null,'group');

							var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
							nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);

							if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
							{
								nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
								if(parseInt(opentaskSearchResults.length)>=50)//case# 201413606
								{
									nlapiLogExecution('ERROR','invoke Scheduler','done in else');
									var param = new Array();
									param['custscript_poid'] = poid;
									param['custscript_trantype'] = trantype;
									param['custscript_locationid']=poToLocationID
									nlapiScheduleScript('customscript_ebiz_postitem_receipt_sch', null,param);
									msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Item Receipt posting has been initiated successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
								}
								else
								{
									var ItemStatusArray =new Array();
									var ItemStatusMapLocArray =new Array();
									var itemReceiptArray =new Array();
									for(var k=0;k<opentaskSearchResults.length;k++)
									{
										var	itemstatus1=opentaskSearchResults[k].getValue('custrecord_sku_status',null,'group');
										nlapiLogExecution('ERROR','itemstatus1',itemstatus1);
										var	MapLoc=opentaskSearchResults[k].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');

										if(ItemStatusArray.indexOf(itemstatus1) == -1)
										{
											ItemStatusArray.push(itemstatus1);
										}
										if(ItemStatusMapLocArray.indexOf(MapLoc) == -1)
										{
											ItemStatusMapLocArray.push(MapLoc);
										}
									}

									//for(j=0;j<ItemStatusArray.length;j++)
									for(j=0;j<ItemStatusMapLocArray.length;j++)
									{
										//var vItemStatus = ItemStatusArray[j];
										var WHLocation = ItemStatusMapLocArray[j];
										var trecord = nlapiTransformRecord(trantype, poid, 'itemreceipt');
										for(var i=0;i<opentaskSearchResults.length;i++)
										{
											var ActQuantity=opentaskSearchResults[i].getValue('formulanumeric',null,'SUM');
											var linenum=opentaskSearchResults[i].getValue('custrecord_line_no',null,'group');
											var	itemid=opentaskSearchResults[i].getValue('custrecord_sku',null,'group');
											var	itemstatus=opentaskSearchResults[i].getValue('custrecord_sku_status',null,'group');
											var invtlp = opentaskSearchResults[i].getValue('custrecord_ebiz_lpno',null,'group');
											var	itemstatus;
											//case # 20141363�
											var batchno = opentaskSearchResults[i].getValue('custrecord_batch_no',null,'group');
											var	MapLoc=opentaskSearchResults[i].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');
											if(WHLocation == MapLoc)
											{
											nlapiLogExecution('ERROR', 'invtlp here', invtlp);	
											//generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,poToLocationID,invtlp);
											//generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,'',poToLocationID,invtlp);
												generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,batchno,poToLocationID,itemstatus,MapLoc);
											}

								}
								if(trecord != null && trecord != '')
								{
									idl = nlapiSubmitRecord(trecord);
									nlapiLogExecution('ERROR','idl',idl);
									var currentrow=[ItemStatusArray[j],idl];
									itemReceiptArray.push(currentrow);
											if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
											{
												UpdateSerialNumbersStatus(SerialArray);
												SerialArray.length=0; //clear the Serial# Array
											}
										}
									}
								nlapiLogExecution('ERROR','idl',idl);
								if(idl!=null && idl!='')
								{
									if(trantype=='transferorder')
										InvokeInventoryTransfer(poid);
									
									if(itemReceiptArray !=null && itemReceiptArray !='' && itemReceiptArray.length>0)
									{
										nlapiLogExecution('ERROR','itemReceiptArray',itemReceiptArray);
										for (var cnt=0;cnt<itemReceiptArray.length ;cnt++)
										{
											var vItemstatus=itemReceiptArray[cnt][0];
											var IRid=itemReceiptArray[cnt][1];
											//nlapiLogExecution('ERROR','IRid',IRid);
											var SeachResults=OpentaskrecordsforItemStatus(poid,vItemstatus);

											for(var M=0;M<SeachResults.length;M++)
											{
												nlapiLogExecution('ERROR','SeachResults[M]',SeachResults[M]);
												var updateStatus = nlapiSubmitField('customrecord_ebiznet_trn_opentask', SeachResults[M].getId(), 'custrecord_ebiz_nsconfirm_ref_no', IRid);
											}

										}

									}
									var filters2 = new Array();
									filters2.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
									filters2.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
									//filters2.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
									filters2.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
									filters2.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
									
									
									

									msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Item Receipt Posted Sucessfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
								//var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
									var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters2,null);
									if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
									{
										nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
										for(var j=0;j<opentaskSearchResults.length;j++)
										{
											nlapiLogExecution('ERROR','opentaskSearchResultsID',opentaskSearchResults[j].getId());
											//case# 20141363?
											MoveTaskRecord(opentaskSearchResults[j].getId(),idl);
										}
									}
									}





								}

							}

						}



					}
					else
					{
						//case# 201414894
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please Enter Valid PO/TO/RMA', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						/*POarray["custparam_error"] = 'Please Enter Valid PO/TO';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;	*/
					}

				}
				else
				{
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please Enter Valid PO/TO/RMA', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
					/*POarray["custparam_error"] = 'Please Enter Valid PO/TO';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;	*/
				}
			}
			else
			{
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please Enter Valid PO/TO/RMA', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
				/*POarray["custparam_error"] = 'Please Enter Valid PO/TO';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				return;	*/
			}

		}
		else
		{
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please Enter PO/TO/RMA', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			/*
			POarray["custparam_error"] = 'Please Enter PO/TO';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			return;*/
		}





		form.addSubmitButton('Post Item	Receipt');
		response.writePage(form);

	}	
}

function InvokeInventoryTransfer(poid)
{
	nlapiLogExecution('ERROR', 'Into InvokeInventoryTransfer',poid);		

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
	columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
	columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
	columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
	columns[4]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[5]=new nlobjSearchColumn('custrecord_batch_no',null,'group');

	var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
	if(opentaskSearchResults!=null && opentaskSearchResults!='')
	{
		for(var k=0;k<opentaskSearchResults.length;k++)
		{
			var poLine = opentaskSearchResults[k].getValue('custrecord_line_no',null,'group');
			var poItem = opentaskSearchResults[k].getValue('custrecord_sku',null,'group');
			var poQty = opentaskSearchResults[k].getValue('formulanumeric',null,'SUM');
			var poItemStatus = opentaskSearchResults[k].getValue('custrecord_sku_status',null,'group');
			var poLocation = opentaskSearchResults[k].getValue('custrecord_wms_location',null,'group');
			var poBatch = opentaskSearchResults[k].getValue('custrecord_batch_no',null,'group');
			var vItemStatuslocation='';
			var Invtrymveloc='';

			if(poItemStatus!=null && poItemStatus!='')
				vItemStatuslocation = getItemStatusMapLoc(poItemStatus);

			if(poLocation!=vItemStatuslocation)
			{

				Invtrymveloc =  InvokeNSInventoryTransfer(poItem,poItemStatus,poLocation,
						vItemStatuslocation,poQty,poBatch,null);
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of InvokeInventoryTransfer',poid);		
}

function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot,serialno)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);
		nlapiLogExecution('ERROR', 'test1', 'test1');

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='N';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		//Changed on 30/4/12 by suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of changes as on 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
		invttransfer.setCurrentLineItemValue('inventory', 'unitconversionrate', '1');


		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('ERROR', 'subs', subs);
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);			
		}


		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;	

			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
				{
					var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
					nlapiLogExecution('ERROR', 'test1', 'test1');
					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');
					compSubRecord.commit();
				}
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('ERROR', 'serialnoNowithQty', serialno);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  serialno);
				}	
				else
				{
					nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
				}
			}
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);

		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}

	nlapiLogExecution('ERROR', 'Out of InvokeNSInventoryTransfer');	
}
var SerialArray=new Array();
function generateItemReceipt(trecord,fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,batchno,templocation,vItemStatus,MapLoc)
{
	try {



		var polinelength = trecord.getLineItemCount('item');
		nlapiLogExecution('ERROR', "polinelength", polinelength);
		var idl;
		var vAdvBinManagement=true;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		//}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);
		for (var j = 1; j <= polinelength; j++) {
			nlapiLogExecution('ERROR', "Get Item Id", trecord.getLineItemValue('item', 'item', j));
			nlapiLogExecution('ERROR', "Get Item Text", trecord.getLineItemText('item', 'item', j));

			var item_id = trecord.getLineItemValue('item', 'item', j);
			var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = trecord.getLineItemValue('item', 'line', j);
			//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);//text
			var poItemUOM = trecord.getLineItemValue('item', 'units', j);//value

			var commitflag = 'N';
			var quantity=0;
			quantity = ActQuantity;

			nlapiLogExecution('ERROR', 'quantity', quantity);

			var inventorytransfer="";
			nlapiLogExecution('ERROR', "itemLineNo(PO/TO)", itemLineNo);
			nlapiLogExecution('ERROR', "linenum(Task)", linenum);
			nlapiLogExecution('ERROR', "poItemUOM", poItemUOM);

			if(trantype!='transferorder')
			{
				//if (itemLineNo == linenum && vItemStatus == itemstatus) {
				if (itemLineNo == linenum){

					//This part of the code will fetch the line level location.
					//If line level is empty then header level location is considered.
					//code added by suman on 080513.
					whLocation=trecord.getLineItemValue('item', 'location', j);//value
					if(whLocation==null||whLocation=="")
						whLocation=templocation;
					//End of code as of 080513.

					//var vrcvqty=arrQty[i];
					var vrcvqty=quantity;
					var vbaseuomqty=0;
					var vuomqty=0;
					var eBizItemDims=geteBizItemDimensions(item_id);
					if(poItemUOM!=null && poItemUOM!='')
					{
						/*var vbaseuomqty=0;
						var vuomqty=0;
						var eBizItemDims=geteBizItemDimensions(item_id);*/
						if(eBizItemDims!=null&&eBizItemDims.length>0)
						{
							nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
							for(var z=0; z < eBizItemDims.length; z++)
							{
								if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
								{
									vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
								}
								nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
								if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
								{
									vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
								}
							}
							if(vuomqty==null || vuomqty=='')
							{
								vuomqty=vbaseuomqty;
							}
							nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
							nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

							if(quantity==null || quantity=='' || isNaN(quantity))
								quantity=0;
							else
								quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

						}
					}
					//Code Added by Ramana
					var varItemlocation;
					nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
					nlapiLogExecution('ERROR', 'MapLoc', MapLoc);
					nlapiLogExecution('ERROR', 'quantity', quantity);
					varItemlocation=MapLoc;
					//varItemlocation = getItemStatusMapLoc(itemstatus);
					//upto to here

					commitflag = 'Y';

					var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
					//var columns = nlapiLookupField('item', itemid, fields);
					var columns = nlapiLookupField('item', item_id, fields);

					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	


						serialInflg = columns.custitem_ebizserialin;
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					nlapiLogExecution('ERROR', 'Itype', Itype);
					if (Itype != "serializedinventoryitem" || serialInflg != "T")
					{
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', parseFloat(quantity).toFixed(5));

						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana
					}

					nlapiLogExecution('ERROR', 'Serialquantity', parseFloat(quantity).toFixed(5));
					nlapiLogExecution('ERROR', 'Into SerialNos');
					var totalconfirmedQty=0;

					var tempserialId = "";
					var tempSerial = "";
					var serialnumcsv = "";

					//	if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") 
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" )
					{


						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
						filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
						filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
						//case# 20149601  starts(added filter for itemid)
						filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
						filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
						//case# 20149601 ends
						//case 20125736 start :
						var columns=new Array();
						/*columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
						columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
						columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
						columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
						columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
						columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpno',null,'group');*/
						columns[0]=new nlobjSearchColumn('custrecord_invref_no');
						columns[1]=new nlobjSearchColumn('custrecord_line_no');
						columns[2]=new nlobjSearchColumn('custrecord_sku_status');
						columns[3]=new nlobjSearchColumn('custrecord_ebiz_lpno');
						columns[4]=new nlobjSearchColumn('custrecord_sku');
						columns[5]=new nlobjSearchColumn('custrecord_act_qty');

						var opentaskSearchResultsforlp=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);

						if(opentaskSearchResultsforlp != null && opentaskSearchResultsforlp != '')
						{
							for(var k=0;k<opentaskSearchResultsforlp.length;k++)
							{

								var invtlp = opentaskSearchResultsforlp[k].getValue('custrecord_ebiz_lpno');
								nlapiLogExecution('ERROR', 'Into SerialNos');
								nlapiLogExecution('ERROR', 'invtlp new', invtlp);	
								nlapiLogExecution('ERROR', 'trantype', trantype);		
								var serialline = new Array();
								var serialId = new Array();
								//case# 20149597 starts(linenumber is checking with itemLineNo)
								var linenumber  = opentaskSearchResultsforlp[k].getValue('custrecord_line_no');
								
								nlapiLogExecution('ERROR', 'itemLineNo', itemLineNo);
								nlapiLogExecution('ERROR', 'linenumber', linenumber);
								
								if(itemLineNo==linenumber)
								{	
									var filters = new Array();


//									filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//									filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
//									filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
//									filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

									if(trantype=='returnauthorization')
									{
										filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
										filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
										filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

									}
									else
									{
										filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
										filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
										filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
										//filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
									}
									//filters.push(new nlobjSearchFilter('custrecord_serialstatus', null, 'doesnotstartwith', 'I'));
									filters.push(new nlobjSearchFilter('custrecord_serialstatus', null, 'doesnotstartwith', 'P'));

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_serialnumber');

									var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
									var serialnumArray=new Array();
									if (serchRec) {
										//case # 20141363
										
										nlapiLogExecution('ERROR', 'serchRec', serchRec.length);
										
										
										
										for (var n = 0; n < serchRec.length; n++) {
											//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
											if (tempserialId == "") {
												tempserialId = serchRec[n].getId();
												SerialArray.push(serchRec[n].getId()); //capturing serialnumbers internal id's
											}
											else {
												tempserialId = tempserialId + "," + serchRec[n].getId();
												SerialArray.push(serchRec[n].getId()); //capturing serialnumbers internal id's
											}

											//This is for Serial num loopin with Space separated.
											if (tempSerial == "") {
												tempSerial = serchRec[n].getValue('custrecord_serialnumber');

												serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

											}
											else {

												tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
												
												nlapiLogExecution('ERROR', 'tempSerial', tempSerial);
												
												if(tempSerial.length>3500)
												{
													tempSerial='';
													trecord.selectLineItem('item', j);
													trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
													var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
													trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

													trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
													trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
													nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

													trecord.commitLineItem('item');
													idl = nlapiSubmitRecord(trecord);
													serialnumArray=new Array();
													totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
													if(n<serchRec.length)
													{
														var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
														nlapiLogExecution('ERROR', 'tQty1', tQty);
														var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
														var polinelength = trecord.getLineItemCount('item');
														/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
														var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
														var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
														var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
														var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

														/*if (itemLineNo == linenum)
											{*/
														if(poItemUOM!=null && poItemUOM!='')
														{
															var vbaseuomqty=0;
															var vuomqty=0;
															var eBizItemDims=geteBizItemDimensions(item_id);
															if(eBizItemDims!=null&&eBizItemDims.length>0)
															{
																nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
																for(var z=0; z < eBizItemDims.length; z++)
																{
																	if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
																	{
																		vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
																	}
																	nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
																	nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
																	nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
																	if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
																	{
																		vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
																	}
																}
																if(vuomqty==null || vuomqty=='')
																{
																	vuomqty=vbaseuomqty;
																}
																nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
																nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

																if(tQty==null || tQty=='' || isNaN(tQty))
																	tQty=0;
																else
																	tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

															}
														}
														nlapiLogExecution('ERROR', 'tQtylast', tQty);
														trecord.selectLineItem('item', itemLineNo);
														trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
														var tempqty=parseInt(n)+parseInt(1);
														trecord.setCurrentLineItemValue('item', 'quantity', tQty);

														trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
														//}

														//}
													}






												}
												serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

											}
											
											

											//if(parseInt(n)== parseInt(quantity-1))
											//	break;




										}
										
										
										
										
										if(vAdvBinManagement)
										{
											nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray.length);
											//var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
											
											
											var compSubRecord = trecord.editCurrentLineItemSubrecord('item','inventorydetail');
											nlapiLogExecution('ERROR', 'compSubRecord',compSubRecord);
											if(compSubRecord !=null && compSubRecord!='')
											{
												compSubRecord.selectLineItem('inventoryassignment', 1);
											}
											else
											{
												nlapiLogExecution('ERROR', 'Create',compSubRecord);
												compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
												
											}
											
											if(serialnumArray!=null && serialnumArray!='')
											{
												for(var k1 = 0; k1<serialnumArray.length;k1++)
												{
													nlapiLogExecution('ERROR', 'Serial@No', serialnumArray[k1]);
													compSubRecord.selectNewLineItem('inventoryassignment');
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialnumArray[k1]);

													compSubRecord.commitLineItem('inventoryassignment');
													
												}
												compSubRecord.commit();	
												
											}
											
										}
										
										/*if(vAdvBinManagement)
										{
											
										}*/
										//case # 20148010
										//	}
										/*nlapiLogExecution('ERROR', 'qtyexceptionFlag', qtyexceptionFlag);
									if(qtyexceptionFlag == "true")
									{
										serialnumcsv = getScannedSerialNoinQtyexception;
										nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is true', serialnumcsv);
									}
									else
									{
										serialnumcsv = tempSerial;
										nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is false', serialnumcsv);
									}*/


									}
									tempSerial = "";
									nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
									nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
									if(!vAdvBinManagement)
									{
										if ((Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem")  && (trecord!=null && trecord!='')) {
											trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
											//case 201412777
											/*	if(serialnumcsv!=null && serialnumcsv!='')
											{
												trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
											}
											else	
											{
												trecord.setCurrentLineItemValue('item', 'serialnumbers', tempserialId);
											}*/
										}
									}

								}//case# 20149597
							}
						}
					}
					else if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem")
					{
						//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
						nlapiLogExecution('ERROR', 'Into LOT/Batch');
						//nlapiLogExecution('ERROR', 'LP:', invtlp);
						var tempBatch = "";
						var batchcsv = "";
						var confirmLotToNS='Y';

						confirmLotToNS=GetConfirmLotToNS(varItemlocation);
						nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);

						if(!vAdvBinManagement)
						{
							var vItemname;

							if(confirmLotToNS == 'Y')
							{
								var filters = new Array();
								filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
								filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
								filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
								filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
								filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

								var columns=new Array();
								columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  
								columns[1]=new nlobjSearchColumn('custrecord_batch_no');

								var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
								nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
								var tempBatch='';
								if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
								{
									nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
									for(var k1=0;k1<opentaskSearchResults.length;k1++)
									{
										if(tempBatch!='')
											tempBatch = tempBatch +','+opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');
										else
											tempBatch = opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');  
									}
								}
								nlapiLogExecution('ERROR','tempbatch',tempBatch);


							}
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}
							trecord.setCurrentLineItemValue('item', 'serialnumbers', tempBatch);
						}
						else
						{
							var vItemname;
							nlapiLogExecution('ERROR', 'item_id:', itemid);
							var filters1 = new Array();          
							filters1[0] = new nlobjSearchFilter('internalid', null, 'is',itemid);
							var columns = new Array();

							columns[0] = new nlobjSearchColumn('itemid');

							var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
							if (itemdetails !=null) 
							{
								vItemname=itemdetails[0].getValue('itemid');
								nlapiLogExecution('ERROR', 'vItemname:', vItemname);
								//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
								vItemname=vItemname.replace(/ /g,"-");
								tempBatch=vItemname + '('+ quantity + ')';


							}



							var filters = new Array();
							filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
							filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
							filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
							filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
							if(itemstatus!=null && itemstatus!='')
								filters.push(new nlobjSearchFilter('custrecord_sku_status', null, 'anyof',itemstatus));
							filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
							filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));

							var columns=new Array();
							columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  
							columns[1]=new nlobjSearchColumn('custrecord_batch_no');
							columns[2]=new nlobjSearchColumn('custrecord_act_qty');
							var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
							nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
							var tempBatch='';

							if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
							{
								//var var compSubRecord ='';
								compSubRecord = trecord.editCurrentLineItemSubrecord('item','inventorydetail');
								editSubrecord='T';
								nlapiLogExecution('ERROR','compSubRecord',compSubRecord);
								var subrecordcount=0;
								if(compSubRecord !=null && compSubRecord !='')
								{
									subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
									nlapiLogExecution('ERROR','subrecordcount ',subrecordcount);
								}

								if(compSubRecord ==null || compSubRecord =='')
								{
									nlapiLogExecution('ERROR','compSubRecord create',compSubRecord);
									editSubrecord='F';
									compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								}
								nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
								for(var k1=0;k1<opentaskSearchResults.length;k1++)
								{
									//case # 20141363ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½start
									var batch = opentaskSearchResults[k1].getValue('custrecord_batch_no');
									//var batch = opentaskSearchResults[0].getValue('custrecord_batch_no');

									var quantity =opentaskSearchResults[k1].getValue('custrecord_act_qty');
									nlapiLogExecution('ERROR','opentask batch',batch);
									nlapiLogExecution('ERROR','opentask quantity',quantity);
									nlapiLogExecution('ERROR', 'editSubrecord', editSubrecord);
									if(poItemUOM!=null && poItemUOM!='')
									{
										/*var vbaseuomqty=0;
										var vuomqty=0;
										var eBizItemDims=geteBizItemDimensions(item_id);*/
										if(eBizItemDims!=null&&eBizItemDims.length>0)
										{
											nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
											for(var z=0; z < eBizItemDims.length; z++)
											{
												if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
												{
													vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
												}
												nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
												nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
												nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
												if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
												{
													vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
												}
											}
											if(vuomqty==null || vuomqty=='')
											{
												vuomqty=vbaseuomqty;
											}
											nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
											nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

											if(quantity==null || quantity=='' || isNaN(quantity))
												quantity=0;
											else
												quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

										}
									}

									nlapiLogExecution('ERROR', 'after conversion', quantity);	
									if(editSubrecord=='F')
									{
										nlapiLogExecution('ERROR', 'editSubrecord', editSubrecord);
										compSubRecord.selectNewLineItem('inventoryassignment');
									}
									else
									{
										try
										{

											if(subrecordcount<=opentaskSearchResults.length)
											{

												compSubRecord.selectLineItem('inventoryassignment',(parseInt(k1)+1));
											}
											else
											{
												nlapiLogExecution('ERROR','Newline','Newline');
												compSubRecord.selectNewLineItem('inventoryassignment');
											}

										}
										catch(e)
										{	
											nlapiLogExecution('ERROR', 'k2');
											compSubRecord.selectNewLineItem('inventoryassignment');
										}

									}
									nlapiLogExecution('ERROR', 'inventoryassignment quantity', quantity);

									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(quantity).toFixed(5));
									if(confirmLotToNS == 'Y')
									{	
										nlapiLogExecution('ERROR', 'inventoryassignment batch', batch);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batch);
									}
									else
									{
										nlapiLogExecution('ERROR', 'vItemname', vItemname);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
									}
									compSubRecord.commitLineItem('inventoryassignment');



									nlapiLogExecution('ERROR', 'commitLineItem');

									//case # 20141363ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½end


								}
								nlapiLogExecution('ERROR', 'commitLineItem1234');                                                                    
								compSubRecord.commit();
								nlapiLogExecution('ERROR', 'commit');
							}


							//nlapiLogExecution('ERROR','tempbatch',tempBatch);

						}


					}
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');
				}
			}
			else
			{ 

				nlapiLogExecution('ERROR', 'itemid (Task)', itemid);	
				nlapiLogExecution('ERROR', 'item_id (Line)', item_id);	
				//if(vTORestrictFlag =='F')
				//{	
				itemLineNo=parseFloat(itemLineNo)-2;
				if (itemid == item_id && itemLineNo == linenum) {

					commitflag = 'Y';

					var varItemlocation;				
					varItemlocation = getItemStatusMapLoc(itemstatus);

					var fields = ['recordType', 'custitem_ebizserialin'];
					var columns = nlapiLookupField('item', item_id, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	

						//	var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;
						nlapiLogExecution('ERROR', 'Itype', Itype);
						nlapiLogExecution('ERROR', 'columns', columns);
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', parseFloat(quantity).toFixed(5));
					if(trantype!='transferorder')
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					//if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") 
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" )
					{
						nlapiLogExecution('ERROR', 'Into SerialNos');
						//nlapiLogExecution('ERROR', 'invtlp', invtlp);					
						var serialline = new Array();
						var serialId = new Array();



						var tempSerial = "";
						var filters = new Array();
						/*if(trantype=='returnauthorization' || trantype=='transferorder')
						{*/
						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
						}
						else if(trantype=='transferorder')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19','8']);
							filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						}
//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						//var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();var totalconfirmedQty=0;
						if (serchRec) {
							nlapiLogExecution('ERROR', 'Into SerialNos11',serchRec.length);
							for (var n = 0; n < serchRec.length; n++) {
								var tempserialcount=parseInt(n)+1;
								if(parseInt(tempserialcount)<=parseInt(quantity)){
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');

										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									else {

										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
										if(tempSerial.length>3500)
										{
											tempSerial='';
											trecord.selectLineItem('item', j);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
											nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

											trecord.commitLineItem('item');
											idl = nlapiSubmitRecord(trecord);
											serialnumArray=new Array();
											totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
											if(n<serchRec.length)
											{
												var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
												nlapiLogExecution('ERROR', 'tQty1', tQty);
												var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
												var polinelength = trecord.getLineItemCount('item');
												/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
												var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
												var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
												//var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
												var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

												/*if (itemLineNo == linenum)
											{*/
												if(poItemUOM!=null && poItemUOM!='')
												{
													var vbaseuomqty=0;
													var vuomqty=0;
													var eBizItemDims=geteBizItemDimensions(item_id);
													if(eBizItemDims!=null&&eBizItemDims.length>0)
													{
														nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
														for(var z=0; z < eBizItemDims.length; z++)
														{
															if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
															{
																vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
															}
															nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
															if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
															{
																vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
															}
														}
														if(vuomqty==null || vuomqty=='')
														{
															vuomqty=vbaseuomqty;
														}
														nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
														nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

														if(tQty==null || tQty=='' || isNaN(tQty))
															tQty=0;
														else
															tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

													}
												}
												nlapiLogExecution('ERROR', 'tQtylast', tQty);
												trecord.selectLineItem('item', itemLineNo);
												trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
												var tempqty=parseInt(n)+parseInt(1);
												trecord.setCurrentLineItemValue('item', 'quantity', tQty);

												trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
												//}

												//}
											}






										}
										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									//Case # 20127042 Start
									/*if(vAdvBinManagement)
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}	*/
									//Case # 20127042 End
								}
							}

							serialnumcsv = tempSerial;
							tempSerial = "";
						}

						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
							}
						}
					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							//nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";

							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);

							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);



							var vItemname;

							var batchnonew='';
							if(confirmLotToNS == 'Y')
							{
								var filters = new Array();
								filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
								filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
								filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
								filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
								filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

								var columns=new Array();
								columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  
								columns[1]=new nlobjSearchColumn('custrecord_batch_no');	
								var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
								nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
								var tempBatch='';
								if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
								{
									nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
									for(var k1=0;k1<opentaskSearchResults.length;k1++)
									{
										if(tempBatch!='')
											tempBatch = tempBatch +','+opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');
										else
										{
											batchnonew=opentaskSearchResults[k1].getValue('custrecord_batch_no');
											tempBatch = opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');
										}
									}
								}
								nlapiLogExecution('ERROR','tempbatch',tempBatch);


							}
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}
							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
							else
							{
								//Case # 20127042 Start
								/*nlapiLogExecution('ERROR', 'quantity', quantity);
								nlapiLogExecution('ERROR', 'batchnonew', batchnonew);
								var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
								if(confirmLotToNS == 'Y')
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
								else
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
								compSubRecord.commitLineItem('inventoryassignment');
								compSubRecord.commit();*/
								//Case # 20127042 End
							}	
						}
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');

				}
				//Code Added by Ramana
				/*}
				else
				{
					trecord=null;
					idl=pointid;
				}*/	
			}
			if (commitflag == 'N' && trecord != null && trecord != '') {
				nlapiLogExecution('ERROR', 'commitflag is N', commitflag);
				trecord.selectLineItem('item', j);
				trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
				trecord.commitLineItem('item');
			}
		}
		nlapiLogExecution('ERROR', 'Before Item Receipt---', 'Before');

		/*if(trecord != null && trecord != '')
			idl = nlapiSubmitRecord(trecord);*/


		//nlapiLogExecution('ERROR', 'Item Receipt idl', idl);

		/*return idl;*/
	}
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('ERROR', 'DetailsError', functionality);	
		nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('ERROR', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('ERROR', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', exp.toString());

		/*nlapiLogExecution('ERROR', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('ERROR', 'PUTW task updated with act end location', EndLocationId);*/

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;
	}	
}
function OpentaskrecordsforItemStatus(poid,vItemStatus)
{
	nlapiLogExecution('ERROR','in to OpentaskrecordsforItemStatus',poid);
	nlapiLogExecution('ERROR','vItemStatus',vItemStatus);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
	filters.push(new nlobjSearchFilter('custrecord_sku_status', 'null', 'anyof', vItemStatus));
	var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
	nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
	return opentaskSearchResults;
}

