/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_cyclecountrecord_SL.js,v $
 *     	   $Revision: 1.12.2.12.4.8.2.52.2.9 $
 *     	   $Date: 2015/12/03 15:52:56 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_cyclecountrecord_SL.js,v $
 * Revision 1.12.2.12.4.8.2.52.2.9  2015/12/03 15:52:56  deepshikha
 * 2015.2 issues
 * 201415689
 *
 * Revision 1.12.2.12.4.8.2.52.2.8  2015/11/17 15:14:28  grao
 * 2015.2 Issue Fixes 201415453
 *
 * Revision 1.12.2.12.4.8.2.52.2.7  2015/11/17 10:47:35  grao
 * 2015.2 Issue Fixes 201415453
 *
 * Revision 1.12.2.12.4.8.2.52.2.6  2015/11/14 15:19:21  rrpulicherla
 * 2015.2 issues
 *
 * Revision 1.12.2.12.4.8.2.52.2.5  2015/11/09 16:07:09  deepshikha
 * 2015.2 issues
 * 201415124
 *
 * Revision 1.12.2.12.4.8.2.52.2.4  2015/11/06 16:37:25  deepshikha
 * 2015.2 issue fixes
 * 201415124
 *
 * Revision 1.12.2.12.4.8.2.52.2.3  2015/11/05 16:04:39  deepshikha
 * 2015.2 issue fixes
 * 201415120
 *
 * Revision 1.12.2.12.4.8.2.52.2.2  2015/10/30 13:45:51  schepuri
 * case# 201415293
 *
 * Revision 1.12.2.12.4.8.2.52.2.1  2015/10/29 06:42:34  aanchal
 * 2015.2 Issue fix
 * 201415124,201415120
 *
 * Revision 1.12.2.12.4.8.2.52  2015/07/23 15:35:08  skreddy
 * Case# 201413373,201413059
 * SW  SB  issue fix
 *
 * Revision 1.12.2.12.4.8.2.51  2015/07/13 15:37:01  grao
 * 2015.2 ssue fixes  201413326:
 *
 * Revision 1.12.2.12.4.8.2.50  2015/06/01 15:42:55  grao
 * SB  2015.2 issue fixes  201412929
 *
 * Revision 1.12.2.12.4.8.2.49  2015/02/19 14:10:56  nneelam
 * case# 201411397
 *
 * Revision 1.12.2.12.4.8.2.48  2015/01/22 13:35:06  schepuri
 * issue # 201411402
 *
 * Revision 1.12.2.12.4.8.2.47  2015/01/21 13:56:40  schepuri
 * issue # 201411389
 *
 * Revision 1.12.2.12.4.8.2.46  2014/09/02 10:59:27  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.12.2.12.4.8.2.45  2014/08/19 12:52:48  skavuri
 * Case# 20149982 Std Bundle issue fixed
 *
 * Revision 1.12.2.12.4.8.2.44  2014/07/10 07:59:23  skavuri
 * Case# 20149271, 20126510  and 20148167 Compatibility Issue Fixed
 *
 * Revision 1.12.2.12.4.8.2.43  2014/06/20 14:45:36  rmukkera
 * Case # 20127185�,20148167
 *
 * Revision 1.12.2.12.4.8.2.42  2014/06/06 12:18:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.12.2.12.4.8.2.41  2014/05/13 15:31:14  skavuri
 * Case# 20148351 SB Issue Fixed
 *
 * Revision 1.12.2.12.4.8.2.40  2014/04/17 15:49:36  skavuri
 * Case # 20148035 SB issue fixed
 *
 * Revision 1.12.2.12.4.8.2.39  2014/04/08 15:51:47  nneelam
 * case#  20141261
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.12.2.12.4.8.2.38  2014/02/06 15:34:41  nneelam
 * case#  20127074
 * std bundle issue fix
 *
 * Revision 1.12.2.12.4.8.2.37  2014/02/04 06:55:43  nneelam
 * case#  20127032
 * std bundle issue fix
 *
 * Revision 1.12.2.12.4.8.2.36  2014/01/30 15:24:37  rmukkera
 * Case # 20126952
 *
 * Revision 1.12.2.12.4.8.2.35  2014/01/06 08:00:26  schepuri
 * 20126236
 *
 * Revision 1.12.2.12.4.8.2.34  2013/12/30 13:51:25  schepuri
 * 20126483
 *
 * Revision 1.12.2.12.4.8.2.33  2013/12/27 15:39:10  rmukkera
 * Case # 20126109
 *
 * Revision 1.12.2.12.4.8.2.32  2013/12/26 15:38:57  gkalla
 * case#20126567
 * Standard bundle issue
 *
 * Revision 1.12.2.12.4.8.2.31  2013/12/24 15:23:49  skreddy
 * case#20126544 & 20126545
 * TSG prod issue fix
 *
 * Revision 1.12.2.12.4.8.2.30  2013/12/18 15:10:38  rmukkera
 * Case # 20126109
 *
 * Revision 1.12.2.12.4.8.2.29  2013/12/16 15:11:40  rmukkera
 * no message
 *
 * Revision 1.12.2.12.4.8.2.28  2013/12/16 11:16:58  rmukkera
 * Case# 20126108�,20126280�
 *
 * Revision 1.12.2.12.4.8.2.27  2013/12/13 14:01:36  schepuri
 * 20126280
 *
 * Revision 1.12.2.12.4.8.2.24.2.2  2013/12/11 13:55:23  schepuri
 * 20126280
 *
 * Revision 1.12.2.12.4.8.2.25  2013/12/10 13:07:11  schepuri
 * 20126280
 *
 * Revision 1.12.2.12.4.8.2.24  2013/12/02 15:15:07  skreddy
 * Case# 20126108
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.12.2.12.4.8.2.23  2013/10/24 13:01:02  rmukkera
 * Case# 20125285
 *
 * Revision 1.12.2.12.4.8.2.22  2013/10/21 15:59:19  rmukkera
 * Case # 20124764�
 *
 * Revision 1.12.2.12.4.8.2.21  2013/10/19 13:19:56  rmukkera
 * Case # 20124813,20125159
 *
 * Revision 1.12.2.12.4.8.2.20  2013/10/08 15:47:21  rmukkera
 * Case# 20124768  ,20124770  ,20124772
 *
 * Revision 1.12.2.12.4.8.2.19  2013/09/18 14:33:24  rmukkera
 * Case# 20124315�
 *
 * Revision 1.12.2.12.4.8.2.18  2013/08/27 15:24:04  skreddy
 * Case#: 20124079,20124081,20124082
 * inventory updated for blind sku and qty
 *
 * Revision 1.12.2.12.4.8.2.17  2013/08/09 15:22:15  rmukkera
 * standard bundle issue fix
 *
 * Revision 1.12.2.12.4.8.2.16  2013/08/03 21:16:08  snimmakayala
 * Case# 201214994
 * Cycle Count Issue Fixes
 *
 * Revision 1.12.2.12.4.8.2.15  2013/08/02 15:34:11  rmukkera
 * add new item related changes
 *
 * Revision 1.12.2.12.4.8.2.14  2013/07/15 07:54:39  spendyala
 * case# 201216057
 * fixed issue related to blank screen
 *
 * Revision 1.12.2.12.4.8.2.13  2013/07/11 15:32:53  grao
 * GSUSA Changes and issues fixes(No Case Numbers)
 *
 * Revision 1.12.2.12.4.8.2.12  2013/06/03 06:54:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inventory Changes
 *
 * Revision 1.12.2.12.4.8.2.11  2013/05/09 15:33:25  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.12.2.12.4.8.2.10  2013/05/02 15:37:15  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.12.2.12.4.8.2.9  2013/04/30 15:57:08  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.12.2.12.4.8.2.8  2013/04/23 15:25:46  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.12.2.12.4.8.2.7  2013/04/19 15:41:30  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes
 *
 * Revision 1.12.2.12.4.8.2.6  2013/04/17 15:55:08  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes in cycle count process
 *
 * Revision 1.12.2.12.4.8.2.5  2013/04/16 05:37:05  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to generate CYCC process
 *
 * Revision 1.12.2.12.4.8.2.4  2013/04/03 20:40:48  kavitha
 * CASE201112/CR201113/LOG2012392
 * TSG Issue fixes
 *
 * Revision 1.12.2.12.4.8.2.3  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.12.2.12.4.8.2.2  2013/03/05 13:35:46  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.12.2.12.4.8.2.1  2013/02/26 12:52:09  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.12.2.12.4.8  2013/02/06 03:18:57  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process
 *
 * Revision 1.12.2.12.4.7  2013/02/01 07:57:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Cyclecountlatestfixed
 *
 * Revision 1.12.2.12  2012/09/04 20:46:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Incorporate Allocated Qty in the list.
 *
 * Revision 1.12.2.11  2012/07/19 12:55:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Record Actual LP issue fixed
 *
 * Revision 1.12.2.10  2012/07/04 14:08:05  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issues
 *
 * Revision 1.12.2.9  2012/06/29 13:57:55  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.12.2.8  2012/06/28 08:39:22  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.12.2.7  2012/06/13 22:23:28  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.12.2.6  2012/04/25 06:54:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blind Cycle Count
 *
 * Revision 1.12.2.5  2012/04/20 13:26:24  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.12.2.4  2012/04/19 15:07:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blind Cycle Count
 *
 * Revision 1.12.2.3  2012/04/06 14:07:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.12.2.2  2012/03/22 14:35:45  spendyala
 * CASE201112/CR201113/LOG201121
 * Check for Lp validation.
 *
 * Revision 1.12.2.1  2012/03/20 15:17:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Onchange function which opens a new window when ever user enter the new batch#.
 *
 * Revision 1.12  2011/11/29 07:00:10  spendyala
 * CASE201112/CR201113/LOG201121
 * deleted unwanted alerts statements
 *
 * Revision 1.11  2011/11/28 15:18:59  spendyala
 * CASE201112/CR201113/LOG201121
 * added recorded date,time and user
 *
 * Revision 1.10  2011/11/28 08:51:44  spendyala
 * CASE201112/CR201113/LOG201121
 * created the filter criteria for cycle count dropdown
 *
 * Revision 1.9  2011/11/08 14:37:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count - Lot/Batch Issue Fixes
 *
 * Revision 1.8  2011/11/04 14:28:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count Issue Fixes
 *
 * Revision 1.7  2011/11/02 12:05:44  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cycle Count file
 *
 * Revision 1.5  2011/07/21 06:31:39  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function PlanNoField(form, varCycleCountPlanNo,maxno){
	var filtersso = new Array();		

	filtersso.push(new nlobjSearchFilter('custrecord_cyclerec_date',null, 'isnotempty', null));
	filtersso.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]));
	//custrecord_cyclesite_id
	var vRoleLocation=getRoledBasedLocation();
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
	}
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('id');	 
	columnsinvt[1] = new nlobjSearchColumn('custrecord_cycle_count_plan');
//	columnsinvt[0].setSort(true);
	//columnsinvt[2] = new nlobjSearchColumn('custrecord_shipment_no');
	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');

	varCycleCountPlanNo.addSelectOption("", "");


	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filtersso,columnsinvt);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('custrecord_cycle_count_plan') != null && customerSearchResults[i].getValue('custrecord_cycle_count_plan') != "" && customerSearchResults[i].getValue('custrecord_cycle_count_plan') != " ")
		{
			var resdo = form.getField('custpage_cyclecountplan').getSelectOptions(customerSearchResults[i].getValue('custrecord_cycle_count_plan'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		varCycleCountPlanNo.addSelectOption(customerSearchResults[i].getValue('custrecord_cycle_count_plan'), customerSearchResults[i].getValue('custrecord_cycle_count_plan'));
	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		//fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno);	
		PlanNoField(form, varCycleCountPlanNo,maxno);
	}
}


function ebiznet_CycleCountRecord_SL(request, response){

	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Record');
		var newrow=form.addField('custpage_newrow', 'text', 'newrow').setDisplayType('hidden');
		newrow.setDefaultValue('F');
//		var selcyclecount = form.addField('custpage_cyclecountplan', 'select', 'Cycle Count Plan#', 'customrecord_ebiznet_cylc_createplan');
		var varCycleCountPlanNo =  form.addField('custpage_cyclecountplan','select', 'Cycle Count Plan#');
		varCycleCountPlanNo.addSelectOption("", "");

		//PlanNoField(form, varCycleCountPlanNo,-1);
		varCycleCountPlanNo.addSelectOption("","");
		var filtersitem = new Array();
		filtersitem.push(new nlobjSearchFilter('custrecord_cyclerec_date',null, 'isnotempty', null));
		filtersitem.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]));
		//custrecord_cyclesite_id
		var vRoleLocation=getRoledBasedLocation();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			filtersitem.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_cycle_count_plan',null,'group');
		columns[0].setSort(true);

		var result = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filtersitem,columns);

		for (var i = 0; result != null && i < result.length; i++) {

			var soresult=result[i];	
			var vplan=soresult.getValue('custrecord_cycle_count_plan',null,'group');	

			var res=  form.getField('custpage_cyclecountplan').getSelectOptions(vplan, 'is');
			if (res != null) {				
				if (res.length > 0) {
					continue;
				}
			}		
			varCycleCountPlanNo.addSelectOption(vplan, vplan);
		}

		var buttondisplay = form.addSubmitButton('Display');
		response.writePage(form);
	}
	else {

		var form = nlapiCreateForm('Record');
		nlapiLogExecution('ERROR', 'cyclecountplan', request.getParameter('custpage_cyclecountplan'));
		//Case# 20124247�Start
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null).setLayoutType('outside','startrow');
		var msgFld=request.getParameter('custpage_msgfield');
		form.addField('custpage_msgfield','text','').setDisplayType('hidden');
		//Case# 20124247�end
		nlapiLogExecution('ERROR', 'msgFld', msgFld);
		form.addPageLink('crosslink', 'Go Back', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=102&deploy=1');
		//Case# 20124247�Start
		if(msgFld!=null && msgFld!='' && msgFld=='cycRecordedSucessfully')
		{
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Recorded sucessfully', NLAlertDialog.TYPE_LOWSET_PRIORITY,  '100%', null, null, null);</script></div>");	
		}
		//Case# 20124247�end
		if (request.getParameter('custpage_cyclecountplan') != null ) {

			var newrow=form.addField('custpage_newrow', 'text', 'newrow').setDisplayType('hidden');
			newrow.setDefaultValue('F');
			nlapiLogExecution('ERROR', 'DS', request.getParameter('custpage_cyclecountplan'));
			//case # 20141261�
			/*var varCCplanNo = request.getParameter("custpage_cyclecountplan");
			var planno = form.addField('custpage_cyclecountplan', 'text', 'Cycle Count Plan No:').setDisplayType('disabled');
			planno.setDefaultValue(varCCplanNo);*/
			//Check for the condition weather the cycle count is closed or not.
			//Coded added by suman on 18/01/13
			var cyclecountstatus=CheckForcycleCountStatus(request.getParameter('custpage_cyclecountplan'));
			//end of code as of 18/01/13.

			if(cyclecountstatus=='F')
			{
				if(request.getParameter('custpage_hiddenfieldselectpage') != 'F'){
					//form.setScript('customscript_ebiz_cyccountrec_cl');
					form.setScript('customscript_cyclecountrecordsubmit');
					//        var selcyclecount = form.addField('custpage_cyclecountplan', 'select', 'Cycle Count Plan#');
					//        var buttondisplay = form.addSubmitButton('Display');

//					var buttonsave = form.addSubmitButton('Save');
					/*
             LN,Defining SubList lines
					 */
					/*var sublist = form.addSubList("custpage_items", "list", "Record");
				sublist.addMarkAllButtons();
				sublist.addField("custpage_rec", "checkbox", "Record").setDefaultValue('F');


				var location=	sublist.addField("custpage_location", "text", "Location");
				if(request.getParameter('custpage_newrow')=='T')
				{
					location.setDisplayType('entry');
				}
				else
				{
					location.setDisplayType('inline');
				}
				var expecteditem=	sublist.addField("custpage_expsku", "select", "Expected Item", "item").setDisplayType('inline');
				var actsku = sublist.addField("custpage_actsku", "select", "Actual Item", "item").setDisplayType('entry');
				actsku.setMandatory(true);
				var expectedqty = sublist.addField("custpage_expqty", "text", "Expected Qty");
				var allocqty = sublist.addField("custpage_allocqty", "text", "Allocated Qty");
				var actqty = sublist.addField("custpage_actqty", "text", "Actual Qty").setDisplayType('entry');
				actqty.setMandatory(true);
				sublist.addField("custpage_explp", "text", "Expected LP");
				var actlp = sublist.addField("custpage_actlp", "text", "Actual LP").setDisplayType('entry');
				actlp.setMandatory(true);
				nlapiLogExecution('ERROR', 'after', 'sublistitemsdeclaration');
				sublist.addButton("custpage_addnewitem", "Add New Item", "addNewRow();");

				sublist.addField("custpage_expbatch", "text", "Expected LOT#");
				sublist.addField("custpage_actbatch", "text", "Actual LOT#").setDisplayType('entry');
				sublist.addField("custpage_exppc", "select", "Expected Pack Code", "customlist_ebiznet_packcode").setDisplayType('inline');
				sublist.addField("custpage_actpc", "select", "Actual Pack Code", "customlist_ebiznet_packcode").setDisplayType('entry');
				sublist.addField("custpage_expitemstatus", "select", "Expected Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
				sublist.addField("custpage_actitemstatus", "select", "Actual Item Status", "customrecord_ebiznet_sku_status").setDisplayType('entry');
				sublist.addField("custpage_id", "text", "ID").setDisplayType('hidden');
				sublist.addField("custpage_date", "text", "date").setDisplayType('hidden').setDefaultValue(DateStamp());
				sublist.addField("custpage_time", "text", "time").setDisplayType('hidden').setDefaultValue(TimeStamp());
				sublist.addField("custpage_siteid", "text", "Site Internal ID").setDisplayType('hidden');
				sublist.addField("custpage_binlocationid", "text", "Bin Location Internal ID").setDisplayType('hidden');
				//nlapiLogExecution('DEBUG', 'id', request.getParameter('custpage_cyclecountplan'))

				//sublist.addField("custpage_planno", "text", "plan no").setDisplayType('hidden').setDefaultValue(request.getParameter('custpage_cyclecountplan'));
				sublist.addField("custpage_invtid", "text", "invt id").setDisplayType('hidden');//.setDefaultValue(request.getParameter('custpage_cyclecountplan'));
				sublist.addField("custpage_actualqty", "text", "Actual Qty").setDisplayType('hidden');
					 */
					//LN, define search filters
					var Qty='0';
					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', request.getParameter('custpage_cyclecountplan')));
					filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', ['20']));
					//filters[2] = new nlobjSearchFilter('custrecord_cycleexp_qty', null, 'greaterthan', Qty);
					//custrecord_cyclesite_id
					var vRoleLocation=getRoledBasedLocation();
					if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
					{
						filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
					}

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
					columns[1] = new nlobjSearchColumn('custrecord_cyclecount_exp_ebiz_sku_no');
					columns[2] = new nlobjSearchColumn('custrecord_cycleact_sku');
					columns[3] = new nlobjSearchColumn('custrecord_cycleexp_qty');
					columns[4] = new nlobjSearchColumn('custrecord_cycle_act_qty');
					columns[5] = new nlobjSearchColumn('custrecord_cyclelpno');
					columns[6] = new nlobjSearchColumn('custrecord_cycact_lpno');
					columns[7] = new nlobjSearchColumn('custrecord_cycleabatch_no');
					columns[8] = new nlobjSearchColumn('custrecord_expcycleabatch_no');
					columns[9] = new nlobjSearchColumn('custrecord_expcyclepc');
					columns[10] = new nlobjSearchColumn('custrecord_cyclepc');
					columns[11] = new nlobjSearchColumn('custrecord_expcyclesku_status');
					columns[12] = new nlobjSearchColumn('custrecord_cyclesku_status');
					columns[13] = new nlobjSearchColumn('custrecord_cyclesite_id');
					columns[14] = new nlobjSearchColumn('custrecord_invtid');
					columns[15] = new nlobjSearchColumn('custrecord_startingputseqno','custrecord_cycact_beg_loc');
					columns[15].setSort(false);

					//LN, execute the  search, passing null filter and return columns
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
					/*var loc='', expsku='', actsku='',actebizskuno='', expqty='', actqty='', explp='', actlp='', actbatch='', expbatch='', actpc='', exppc='', actitemstatus='', expitemstatus='';
				var site;
				var invtid='';*/
					var locvalue='';
					var rcptqty = new Array();
					var blindItemRule=false;
					var blindQtyRule=false;
					if(searchresults!=null && searchresults.length>0)
					{
						var buttonsave = form.addSubmitButton('Save');
						var varCCplanNo = request.getParameter("custpage_cyclecountplan");
						var planno = form.addField('custpage_cyclecountplan', 'text', 'Cycle Count Plan No:').setDisplayType('disabled');
						planno.setDefaultValue(varCCplanNo);

						var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
						hiddenfieldselectpage.setDefaultValue('F');

						var sublist = form.addSubList("custpage_items", "list", "Record");
						sublist.addMarkAllButtons();
						sublist.addField("custpage_rec", "checkbox", "Record").setDefaultValue('F');


						var location=	sublist.addField("custpage_location", "text", "Location");
						if(request.getParameter('custpage_newrow')=='T')
						{
							location.setDisplayType('entry');
						}
						else
						{
							location.setDisplayType('inline');
						}
						var expecteditem=	sublist.addField("custpage_expsku", "select", "Expected Item", "item").setDisplayType('inline');
						var actsku = sublist.addField("custpage_actsku", "select", "Actual Item", "item").setDisplayType('entry');
						//actsku.setMandatory(true);
						var expectedqty = sublist.addField("custpage_expqty", "text", "Expected Qty");
						var allocqty = sublist.addField("custpage_allocqty", "text", "Allocated Qty");
						var actqty = sublist.addField("custpage_actqty", "text", "Actual Qty").setDisplayType('entry');
						//actqty.setMandatory(true);
						sublist.addField("custpage_explp", "text", "Expected LP");
						var actlp = sublist.addField("custpage_actlp", "text", "Actual LP").setDisplayType('entry');
						//actlp.setMandatory(true);
						nlapiLogExecution('ERROR', 'after', 'sublistitemsdeclaration');
						sublist.addButton("custpage_addnewitem", "Add New Item", "addNewRow();");

						sublist.addField("custpage_expbatch", "text", "Expected LOT#");
						sublist.addField("custpage_actbatch", "text", "Actual LOT#").setDisplayType('entry');
						sublist.addField("custpage_exppc", "select", "Expected Pack Code", "customlist_ebiznet_packcode").setDisplayType('inline');
						//sublist.addField("custpage_actpc", "select", "Actual Pack Code", "customlist_ebiznet_packcode").setDisplayType('entry');
						sublist.addField("custpage_actpc", "select", "Actual Pack Code", "customlist_ebiznet_packcode").setDisplayType('hidden');//.setDisplayType('entry');// case# 201411402
						sublist.addField("custpage_expitemstatus", "select", "Expected Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
						sublist.addField("custpage_actitemstatus", "select", "Actual Item Status", "customrecord_ebiznet_sku_status").setDisplayType('entry');
						sublist.addField("custpage_id", "text", "ID").setDisplayType('hidden');
						sublist.addField("custpage_date", "text", "date").setDisplayType('hidden').setDefaultValue(DateStamp());
						sublist.addField("custpage_time", "text", "time").setDisplayType('hidden').setDefaultValue(TimeStamp());
						sublist.addField("custpage_siteid", "text", "Site Internal ID").setDisplayType('hidden');
						// case no 20126236
						sublist.addField("custpage_expsku1", "select", "Expected Item1", "item").setDisplayType('hidden');
						sublist.addField("custpage_binlocationid", "text", "Bin Location Internal ID").setDisplayType('hidden');
						//nlapiLogExecution('DEBUG', 'id', request.getParameter('custpage_cyclecountplan'))

						//sublist.addField("custpage_planno", "text", "plan no").setDisplayType('hidden').setDefaultValue(request.getParameter('custpage_cyclecountplan'));
						sublist.addField("custpage_invtid", "text", "invt id").setDisplayType('hidden').setDefaultValue(request.getParameter('custpage_cyclecountplan'));
						sublist.addField("custpage_actualqty", "text", "Actual Qty").setDisplayType('hidden');



						var blindItemSysRuleFilters = new Array();//
						blindItemSysRuleFilters.push(new nlobjSearchFilter('name', null, 'is', 'CYCN - Blind Item'));
						// Case# 20148351
						var locsite=searchresults[0].getValue('custrecord_cyclesite_id');
						nlapiLogExecution('ERROR', 'locsite is ', locsite);
						var arraynew=new Array();
						if(locsite!=null && locsite!='' && locsite!='null')
						{
							arraynew.push(locsite);
							arraynew.push('@NONE@');
							blindItemSysRuleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', arraynew));
						}
						// Case# 20148351 end
						//case # 20127074,added location filter.
						else
						{
							if(vRoleLocation!=null && vRoleLocation!='')
								blindItemSysRuleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'is', vRoleLocation));
						}
						var blindItemSysRuleColumns = new Array();
						blindItemSysRuleColumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
						var blindItemSystemRulesSearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, blindItemSysRuleFilters, blindItemSysRuleColumns);
						if(blindItemSystemRulesSearchresults!=null && blindItemSystemRulesSearchresults.length>0)
						{
							var ruleValue=blindItemSystemRulesSearchresults[0].getValue('custrecord_ebizrulevalue');
							nlapiLogExecution('ERROR', 'blindItem-ruleValue', ruleValue);
							if(ruleValue=='N')
							{
								blindItemRule=false;
							}
							else
							{
								blindItemRule=true;
							}
						}
						else
						{
							blindItemRule=false;
						}
						var blindQtySysRuleFilters = new Array();//
						blindQtySysRuleFilters.push(new nlobjSearchFilter('name', null, 'is', 'CYCN - Blind Qty'));
						// Case# 20148351
						//var locsite=searchresult[0].getValue('custrecord_cyclesite_id');
						var arraynew1=new Array();
						if(locsite!=null && locsite!='' && locsite!='null')
						{
							arraynew1.push(locsite);
							arraynew1.push('@NONE@');
							blindQtySysRuleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', arraynew1));
						}

						// Case# 20148351
						//case # 20127074,added location filter.
						else
						{
							if(vRoleLocation!=null && vRoleLocation!='')
								blindQtySysRuleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'is', vRoleLocation));
						} //end

						var blindQtySysRuleColumns = new Array();
						blindQtySysRuleColumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
						var blindQtySystemRulesSearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, blindQtySysRuleFilters, blindQtySysRuleColumns);
						if(blindQtySystemRulesSearchresults!=null && blindQtySystemRulesSearchresults.length>0)
						{
							var ruleValue=blindQtySystemRulesSearchresults[0].getValue('custrecord_ebizrulevalue');
							nlapiLogExecution('ERROR', 'blindQty-ruleValue', ruleValue);
							if(ruleValue=='N')
							{
								blindQtyRule=false;
							}
							else
							{
								blindQtyRule=true;
							}
						}
						else
						{
							blindQtyRule=false;
						}
					}
					else
					{
//						var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);

					}
					/*
             LN, Searching records from custom record and dynamically adding to the sublist lines
					 */
					for (var i = 0; searchresults != null && i < searchresults.length; i++) {
						var loc='', expsku='', actsku='',actebizskuno='', expqty='', actqty='', explp='', actlp='', actbatch='', expbatch='', actpc='', exppc='', actitemstatus='', expitemstatus='';
						var site;
						var invtid='';
						var locvalue='';
						var searchresult = searchresults[i];
						if((searchresult.getValue('custrecord_cycleexp_qty')>0) || ((searchresult.getValue('custrecord_invtid') == null) || (searchresult.getValue('custrecord_invtid') == "")))
						{
							invtid=searchresult.getValue('custrecord_invtid');
							site=searchresult.getValue('custrecord_cyclesite_id');
							loc = searchresult.getText('custrecord_cycact_beg_loc');
							//locvalue
							locvalue = searchresult.getValue('custrecord_cycact_beg_loc');
							expsku = searchresult.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
							actebizskuno = searchresult.getValue('custrecord_cycleact_sku');
							if (actebizskuno == 0) 
								actebizskuno = searchresult.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
							actsku=searchresult.getText('custrecord_cycleact_sku');
							if (actebizskuno == 0) 
								actsku = searchresult.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
							expqty = searchresult.getValue('custrecord_cycleexp_qty');
							actqty = searchresult.getValue('custrecord_cycle_act_qty');
							if (actqty == null || actqty == '') 
								actqty = searchresult.getValue('custrecord_cycleexp_qty');
							explp = searchresult.getValue('custrecord_cyclelpno');
							actlp = searchresult.getValue('custrecord_cycact_lpno');
							if (actlp == null || actlp == '') 
								actlp = searchresult.getValue('custrecord_cyclelpno');
							expbatch = searchresult.getValue('custrecord_expcycleabatch_no');
							actbatch = searchresult.getValue('custrecord_cycleabatch_no');
							if (actbatch == null || actbatch == '') 
								actbatch = searchresult.getValue('custrecord_expcycleabatch_no');
							exppc = searchresult.getValue('custrecord_expcyclepc');
							actpc = searchresult.getValue('custrecord_cyclepc');
							if (actpc == null || actpc == '') 
								actpc = searchresult.getValue('custrecord_expcyclepc');
							expitemstatus = searchresult.getValue('custrecord_expcyclesku_status');
							actitemstatus = searchresult.getValue('custrecord_cyclesku_status');
							if (actitemstatus == null || actitemstatus == '') 
								actitemstatus = searchresult.getValue('custrecord_expcyclesku_status');
						}
						form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
						nlapiLogExecution('ERROR', 'blindRule', blindItemRule);
						if(blindItemRule==false)
						{
							expecteditem.setDisplayType('inline');
							form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i + 1, expsku);	
							form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i + 1, actebizskuno);
							form.getSubList('custpage_items').setLineItemValue('custpage_expsku1', i + 1, expsku);
						}
						else
						{
							expecteditem.setDisplayType('hidden');
							form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i + 1, '');	
							form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i + 1, '');
						}
						if(blindQtyRule==false)
						{	
							expectedqty.setDisplayType('inline');
							form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i + 1, expqty);
							var allocdqty = 0;
							try
							{
								if(invtid!=null && invtid!='')
								{
									//allocdqty = nlapiLookupField('customrecord_ebiznet_createinv',invtid,'custrecord_ebiz_alloc_qty');
									var creareinvrec = nlapiLookupField('customrecord_ebiznet_createinv',invtid,['custrecord_ebiz_alloc_qty','custrecord_ebiz_qoh']);
									allocdqty = creareinvrec.custrecord_ebiz_alloc_qty;
									expqty = creareinvrec.custrecord_ebiz_qoh;
								}
							}
							catch(exp)
							{
								nlapiLogExecution('ERROR', 'Exception', exp);
								allocdqty=0;
							}
							form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i + 1, expqty);
							form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', i + 1, allocdqty);
							form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, actqty);
							form.getSubList('custpage_items').setLineItemValue('custpage_actualqty', i + 1, actqty);
						}
						else
						{	
							expectedqty.setDisplayType('hidden');
							form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i + 1, '');
							form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', i + 1, '');
							form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, '');
						}

						form.getSubList('custpage_items').setLineItemValue('custpage_explp', i + 1, explp);
						form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i + 1, actlp);
						form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i + 1, expbatch);
						form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i + 1, actbatch);
						form.getSubList('custpage_items').setLineItemValue('custpage_exppc', i + 1, exppc);
						form.getSubList('custpage_items').setLineItemValue('custpage_actpc', i + 1, actpc);
						form.getSubList('custpage_items').setLineItemValue('custpage_expitemstatus', i + 1, expitemstatus);
						form.getSubList('custpage_items').setLineItemValue('custpage_actitemstatus', i + 1, actitemstatus);
						//case 20126545 start
						if(searchresult !=null && searchresult !='') //case 20126545 end
							form.getSubList('custpage_items').setLineItemValue('custpage_id', i + 1, searchresult.getId());
						form.getSubList('custpage_items').setLineItemValue('custpage_siteid', i + 1, site);
						form.getSubList('custpage_items').setLineItemValue('custpage_binlocationid', i + 1, locvalue);
						form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i + 1, invtid);

					}
					if(request.getParameter('custpage_newrow')!=null && request.getParameter('custpage_newrow')!=''&& request.getParameter('custpage_newrow')=='T' && searchresults !=null && searchresults!=''&& searchresults.length>0)
					{
						var count=parseInt(searchresults.length)+1;
						form.getSubList('custpage_items').setLineItemValue('custpage_expqty', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_actqty', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_expsku', count, '');	
						form.getSubList('custpage_items').setLineItemValue('custpage_actsku', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_explp', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_actlp', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_exppc', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_actpc', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_expitemstatus', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_actitemstatus', count, '');
						form.getSubList('custpage_items').setLineItemValue('custpage_id', count, '');
						//case # 20125160� Start 
						if(site!=null && site!='' && site!='null')
						{
							form.getSubList('custpage_items').setLineItemValue('custpage_siteid', count, site);
						}

						// Case # 20125160� End


						form.getSubList('custpage_items').setLineItemValue('custpage_invtid', count, '');
					}
				}
				else{
					nlapiLogExecution('ERROR','elsehdn2',request.getParameter('custpage_hiddenfieldselectpage'));
					var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
					hiddenfieldselectpage.setDefaultValue('F');

					var lineCnt = request.getLineItemCount('custpage_items');


					for(var linecount = 1; linecount<=lineCnt; linecount++){
						var lineselected=request.getLineItemValue('custpage_items', 'custpage_rec', linecount);
						nlapiLogExecution('ERROR','lineselected',lineselected);
						if(lineselected=="T")
						{
							var vactsku = request.getLineItemValue('custpage_items', 'custpage_actsku', linecount);
							var vactqty = request.getLineItemValue('custpage_items', 'custpage_actqty', linecount);
							var vactlp = request.getLineItemValue('custpage_items', 'custpage_actlp', linecount);
							var vactbatch = request.getLineItemValue('custpage_items', 'custpage_actbatch', linecount);
							var vactpc = request.getLineItemValue('custpage_items', 'custpage_actpc', linecount);
							var vactitemstatus = request.getLineItemValue('custpage_items', 'custpage_actitemstatus', linecount);
							var vactsku = request.getLineItemValue('custpage_items', 'custpage_actsku', linecount);

							//else values
							var vlocation = request.getLineItemValue('custpage_items', 'custpage_location', linecount);
							var cyclecountplan = request.getParameter('custpage_cyclecountplan');
							var site = request.getLineItemValue('custpage_items', 'custpage_siteid', linecount);

							//	var vcycrecid=nlapiGetLineItemValue('custpage_items', 'custpage_id', linecount);
							var vcycrecid=request.getLineItemValue('custpage_items', 'custpage_id', linecount);
							var ActualQty1 = nlapiGetLineItemValue('custpage_items','custpage_actualqty',linecount);
							var EnteredQty = nlapiGetLineItemValue('custpage_items','custpage_actqty',linecount);
							nlapiLogExecution('ERROR', 'vactsku', vactsku);
							nlapiLogExecution('ERROR', 'vactqty', vactqty);
							nlapiLogExecution('ERROR', 'vactlp', vactlp);
							nlapiLogExecution('ERROR', 'vactbatch', vactbatch);
							nlapiLogExecution('ERROR', 'vactpc', vactpc);
							nlapiLogExecution('ERROR', 'actitemstatus', actitemstatus);
							nlapiLogExecution('ERROR', 'vactsku', vactsku);
							nlapiLogExecution('ERROR', 'vcycrecid', vcycrecid);

							nlapiLogExecution('ERROR', 'vlocation', vlocation);
							nlapiLogExecution('ERROR', 'cyclecountplan', cyclecountplan);
							nlapiLogExecution('ERROR', 'site', site);
							nlapiLogExecution('ERROR', 'ActualQty1', ActualQty1);
							nlapiLogExecution('ERROR', 'EnteredQty', EnteredQty);
							var filtersser = new Array();
							if(vactsku!=null&&vactsku!="")
							{
								filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', vactsku);
								if(vactlp!=null && vactlp!='')
								filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vactlp);
								//	filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'noneof', '32');

								if((parseFloat(EnteredQty)>parseFloat(ActualQty1)))
								{
									filtersser[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
								}
								else if((parseFloat(EnteredQty)<parseFloat(ActualQty1)))
								{
									filtersser[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');	
								}
								else
								{

								}
								var columnser = new Array();
								columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

								var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
								if(SrchRecord != null && SrchRecord != '' && SrchRecord.length > 0)
								{
									var serialno='';
									nlapiLogExecution('ERROR', 'SrchRecord.length', SrchRecord.length);
									for (var a = 0; a < SrchRecord.length; a++) {

										nlapiLogExecution('ERROR', 'SrchRecord[a].getId()', SrchRecord[a].getId());
										nlapiLogExecution('ERROR', 'custrecord_serialnumber', SrchRecord[a].getValue('custrecord_serialnumber'));

										var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', SrchRecord[a].getId());
										var fields = new Array();
										var values = new Array();
										fields[0] = 'custrecord_serialstatus';
										values[0] = '';
										var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecord[a].getId(), fields, values);

										if(serialno == "" || serialno == null)
										{
											serialno = SrchRecord[a].getValue('custrecord_serialnumber');
										}
										else
										{
											serialno = serialno +","+ SrchRecord[a].getValue('custrecord_serialnumber');
										}
									}

								}
								//Case # 20126109 End

							}
							//	alert('vcycrecid:' + vcycrecid);
							if(vactsku != null && vactsku !=''){
								if(vcycrecid!=null && vcycrecid!='')
								{
									//alert('vcycrecid'+vcycrecid);
									var fieldNames = new Array(); 
									fieldNames.push('custrecord_act_beg_date');  
									fieldNames.push('custrecord_cycact_beg_time');
									fieldNames.push('custrecord_cycleact_end_date');
									fieldNames.push('custrecord_cycact_end_time');
									fieldNames.push('custrecord_cyclerec_upd_date');
									fieldNames.push('custrecord_cyclerec_upd_time');
									fieldNames.push('custrecord_cycleact_sku');
									fieldNames.push('custrecord_cyclecount_act_ebiz_sku_no');
									fieldNames.push('custrecord_cycle_act_qty');
									fieldNames.push('custrecord_cycact_lpno');
									fieldNames.push('custrecord_cycleabatch_no');
									fieldNames.push('custrecord_cyclepc');
									fieldNames.push('custrecord_cyclesku_status');
									fieldNames.push('custrecord_cyclestatus_flag');
									fieldNames.push('custrecord_cycleupd_user_no');
									fieldNames.push('custrecord_cyccplan_recorddate');
									fieldNames.push('custrecord_cyccplan_recordtime');
									fieldNames.push('custrecord_cyccplan_recordedby');
									fieldNames.push('custrecord_cycle_serialno');

									var newValues = new Array(); 
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									newValues.push(vactsku);
									newValues.push(vactsku);
									newValues.push(vactqty);
									newValues.push(vactlp);
									newValues.push(vactbatch);
									newValues.push(vactpc);
									newValues.push(vactitemstatus);
									/*	newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actpc', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));*/
									newValues.push('31');
									newValues.push(nlapiGetContext().getUser());
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									newValues.push(nlapiGetContext().getUser());

									newValues.push(serialno);
									nlapiSubmitField('customrecord_ebiznet_cyclecountexe', vcycrecid, fieldNames, newValues);
									//Case# 20124247�Start
									//	nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
									//Case# 20124247?End
								}
								else
								{
									//alert('here3');
									var fields=new Array();

									fields[0]=new nlobjSearchFilter('name', null, 'is', vlocation);
									var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);

									var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
									customrecord.setFieldValue('custrecord_tasktype', 7);
									customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
									customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
									customrecord.setFieldValue('custrecord_expe_qty', '0');
									customrecord.setFieldValue('custrecord_lpno', vactlp);
									customrecord.setFieldValue('custrecord_sku', vactsku);
									customrecord.setFieldValue('custrecord_ebiz_sku_no', vactsku);
									if(sresult!=null && sresult.length>0)
										customrecord.setFieldValue('custrecord_actbeginloc', sresult[0].getId());
									customrecord.setFieldValue('custrecord_ebiz_cntrl_no', cyclecountplan);					
									customrecord.setFieldValue('name', cyclecountplan);
									if(site!=null && site!='')
										customrecord.setFieldValue('custrecord_site_id', site);	
									var opentaskRecId = nlapiSubmitRecord(customrecord);

									var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
									cyclecntrec.setFieldValue('custrecord_cycle_count_plan', cyclecountplan);
									cyclecntrec.setFieldValue('name', cyclecountplan);
									cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
									cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cyclesku',vactsku);
									cyclecntrec.setFieldValue('custrecord_cycleact_sku',vactsku);
									cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', vactsku);
									cyclecntrec.setFieldValue('custrecord_cyclecount_act_ebiz_sku_no', vactsku);
									cyclecntrec.setFieldValue('custrecord_cycleexp_qty', '0');
									cyclecntrec.setFieldValue('custrecord_cyclelpno', vactlp);
									cyclecntrec.setFieldValue('custrecord_cycact_lpno', vactlp);
									cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 31);//status flag is 'R'
									if(sresult!=null && sresult.length>0)								
										cyclecntrec.setFieldValue('custrecord_cycact_beg_loc',sresult[0].getId());
									cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskRecId);
									cyclecntrec.setFieldValue('custrecord_invtid', null);
									cyclecntrec.setFieldValue('custrecord_expcycleabatch_no',vactbatch);
									//Case # 20126952 Start
									cyclecntrec.setFieldValue('custrecord_cycleabatch_no',vactbatch);
									//Case # 20126952 End
									cyclecntrec.setFieldValue('custrecord_expcyclesku_status', vactitemstatus);
									cyclecntrec.setFieldValue('custrecord_cyclesku_status', vactitemstatus);
									cyclecntrec.setFieldValue('custrecord_expcyclepc', vactpc);
									cyclecntrec.setFieldValue('custrecord_cycle_act_qty', vactqty);
									cyclecntrec.setFieldValue('custrecord_cycleupd_user_no', nlapiGetContext().getUser());
									cyclecntrec.setFieldValue('custrecord_cyccplan_recorddate', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cyccplan_recordtime', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cyccplan_recordedby', nlapiGetContext().getUser());
									if(site!=null && site!='')
										cyclecntrec.setFieldValue('custrecord_cyclesite_id', site);
									cyclecntrec.setFieldValue('custrecord_act_beg_date', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cycact_beg_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cycleact_end_date', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cycact_end_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cyclerec_upd_date', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cyclerec_upd_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cycle_notes', 'fromaddnewitem');
									cyclecntrec.setFieldValue('custrecord_cycle_serialno', serialno);
									//	alert('custrecord_serialitem :' + nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
									//	alert('custrecord_serialparentid :' + nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
									//nlapiLogExecution('ERROR', 'custrecord_serialparentid', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
									//Case # 20126109 Start
									/*var filtersser = new Array();
									if(vactsku!=null&&vactsku!="")
									{
										filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', vactsku);
										filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vactlp);
										//	filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'noneof', '32');
										var columnser = new Array();
										columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

										var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
										if(SrchRecord != null && SrchRecord != '' && SrchRecord.length > 0)
										{
											var serialno='';
											nlapiLogExecution('ERROR', 'SrchRecord.length', SrchRecord.length);
											for (var a = 0; a < SrchRecord.length; a++) {

												nlapiLogExecution('ERROR', 'SrchRecord[a].getId()', SrchRecord[a].getId());
												nlapiLogExecution('ERROR', 'custrecord_serialnumber', SrchRecord[a].getValue('custrecord_serialnumber'));

												var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', SrchRecord[a].getId());
												var fields = new Array();
												var values = new Array();
												fields[0] = 'custrecord_serialstatus';
												values[0] = '';
												var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecord[a].getId(), fields, values);

												if(serialno == "" || serialno == null)
												{
													serialno = SrchRecord[a].getValue('custrecord_serialnumber');
												}
												else
												{
													serialno = serialno +","+ SrchRecord[a].getValue('custrecord_serialnumber');
												}
											}
											cyclecntrec.setFieldValue('custrecord_cycle_serialno', serialno);
										}
										//Case # 20126109 End

									}*/




									var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
									//Case# 20124247?Start
									//	nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
									//Case# 20124247?End
								}
							}
							else{
								if(vcycrecid!=null && vcycrecid!='')
								{
									//alert('vcycrecid'+vcycrecid);
									var fieldNames = new Array(); 
									fieldNames.push('custrecord_act_beg_date');  
									fieldNames.push('custrecord_cycact_beg_time');
									fieldNames.push('custrecord_cycleact_end_date');
									fieldNames.push('custrecord_cycact_end_time');
									fieldNames.push('custrecord_cyclerec_upd_date');
									fieldNames.push('custrecord_cyclerec_upd_time');
									fieldNames.push('custrecord_cycleact_sku');
									fieldNames.push('custrecord_cyclecount_act_ebiz_sku_no');
									fieldNames.push('custrecord_cycle_act_qty');
									fieldNames.push('custrecord_cycact_lpno');
									fieldNames.push('custrecord_cycleabatch_no');
									fieldNames.push('custrecord_cyclepc');
									fieldNames.push('custrecord_cyclesku_status');
									fieldNames.push('custrecord_cyclestatus_flag');
									fieldNames.push('custrecord_cycleupd_user_no');
									fieldNames.push('custrecord_cyccplan_recorddate');
									fieldNames.push('custrecord_cyccplan_recordtime');
									fieldNames.push('custrecord_cyccplan_recordedby');
									fieldNames.push('custrecord_cycle_serialno');

									var newValues = new Array(); 
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									if(vactsku != null && vactsku != '')
									{
										newValues.push(vactsku);
										newValues.push(vactsku);
									}
									if(vactqty != null && vactqty != '')
										newValues.push(vactqty);
									if(vactlp != null && vactlp != '')
										newValues.push(vactlp);
									if(vactbatch != null && vactbatch != '')
										newValues.push(vactbatch);
									if(vactpc != null && vactpc != '')
										newValues.push(vactpc);
									if(vactitemstatus != null && vactitemstatus != '')
										newValues.push(vactitemstatus);
									/*	newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actpc', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));*/
									newValues.push('31');
									newValues.push(nlapiGetContext().getUser());
									newValues.push(DateStamp());
									newValues.push(TimeStamp());
									newValues.push(nlapiGetContext().getUser());

									newValues.push(serialno);
									nlapiSubmitField('customrecord_ebiznet_cyclecountexe', vcycrecid, fieldNames, newValues);
									//Case# 20124247?Start
									var fields=new Array();

									fields[0]=new nlobjSearchFilter('name', null, 'is', vlocation);
									var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);

									var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
									customrecord.setFieldValue('custrecord_tasktype', 7);
									customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
									customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
									customrecord.setFieldValue('custrecord_expe_qty', '0');
									customrecord.setFieldValue('custrecord_lpno', vactlp);
									customrecord.setFieldValue('custrecord_sku', vactsku);
									customrecord.setFieldValue('custrecord_ebiz_sku_no', vactsku);
									if(sresult!=null && sresult.length>0)
										customrecord.setFieldValue('custrecord_actbeginloc', sresult[0].getId());
									customrecord.setFieldValue('custrecord_ebiz_cntrl_no', nlapiGetFieldValue('custpage_cyclecountplan'));					
									customrecord.setFieldValue('name', nlapiGetFieldValue('custpage_cyclecountplan'));
									if(site!=null && site!='')
										customrecord.setFieldValue('custrecord_site_id', site);	
									var opentaskRecId = nlapiSubmitRecord(customrecord);
									//	nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
									//Case# 20124247?End
								}
								else
								{
									//alert('here3');
									var fields=new Array();

									fields[0]=new nlobjSearchFilter('name', null, 'is', vlocation);
									var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);

									var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
									customrecord.setFieldValue('custrecord_tasktype', 7);
									customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
									customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
									customrecord.setFieldValue('custrecord_expe_qty', '0');
									customrecord.setFieldValue('custrecord_lpno', vactlp);
									customrecord.setFieldValue('custrecord_sku', vactsku);
									customrecord.setFieldValue('custrecord_ebiz_sku_no', vactsku);
									if(sresult!=null && sresult.length>0)
										customrecord.setFieldValue('custrecord_actbeginloc', sresult[0].getId());
									customrecord.setFieldValue('custrecord_ebiz_cntrl_no', cyclecountplan);					
									customrecord.setFieldValue('name', cyclecountplan);
									if(site!=null && site!='')
										customrecord.setFieldValue('custrecord_site_id', site);	
									var opentaskRecId = nlapiSubmitRecord(customrecord);

									var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
									cyclecntrec.setFieldValue('custrecord_cycle_count_plan', cyclecountplan);
									cyclecntrec.setFieldValue('name', cyclecountplan);
									cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
									cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cyclesku',vactsku);
									cyclecntrec.setFieldValue('custrecord_cycleact_sku',vactsku);
									cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', vactsku);
									cyclecntrec.setFieldValue('custrecord_cyclecount_act_ebiz_sku_no', vactsku);
									cyclecntrec.setFieldValue('custrecord_cycleexp_qty', '0');
									cyclecntrec.setFieldValue('custrecord_cyclelpno', vactlp);
									cyclecntrec.setFieldValue('custrecord_cycact_lpno', vactlp);
									cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 31);//status flag is 'R'
									if(sresult!=null && sresult.length>0)								
										cyclecntrec.setFieldValue('custrecord_cycact_beg_loc',sresult[0].getId());
									cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskRecId);
									cyclecntrec.setFieldValue('custrecord_invtid', null);
									cyclecntrec.setFieldValue('custrecord_expcycleabatch_no',vactbatch);
									//Case # 20126952 Start
									cyclecntrec.setFieldValue('custrecord_cycleabatch_no',vactbatch);
									//Case # 20126952 End
									cyclecntrec.setFieldValue('custrecord_expcyclesku_status', vactitemstatus);
									cyclecntrec.setFieldValue('custrecord_cyclesku_status', vactitemstatus);
									cyclecntrec.setFieldValue('custrecord_expcyclepc', vactpc);
									cyclecntrec.setFieldValue('custrecord_cycle_act_qty', vactqty);
									cyclecntrec.setFieldValue('custrecord_cycleupd_user_no', nlapiGetContext().getUser());
									cyclecntrec.setFieldValue('custrecord_cyccplan_recorddate', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cyccplan_recordtime', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cyccplan_recordedby', nlapiGetContext().getUser());
									if(site!=null && site!='')
										cyclecntrec.setFieldValue('custrecord_cyclesite_id', site);
									cyclecntrec.setFieldValue('custrecord_act_beg_date', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cycact_beg_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cycleact_end_date', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cycact_end_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cyclerec_upd_date', DateStamp());
									cyclecntrec.setFieldValue('custrecord_cyclerec_upd_time', TimeStamp());
									cyclecntrec.setFieldValue('custrecord_cycle_notes', 'fromaddnewitem');

									//	alert('custrecord_serialitem :' + nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
									//	alert('custrecord_serialparentid :' + nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
									//nlapiLogExecution('ERROR', 'custrecord_serialparentid', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
									//Case # 20126109 Start
									var filtersser = new Array();
									if(vactsku!=null&&vactsku!="")
									{
										filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', vactsku);
									if(vactlp!=null && vactlp!='')
										filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vactlp);
										//	filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'noneof', '32');
										var columnser = new Array();
										columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

										var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
										if(SrchRecord != null && SrchRecord != '' && SrchRecord.length > 0)
										{
											var serialno='';
											nlapiLogExecution('ERROR', 'SrchRecord.length', SrchRecord.length);
											for (var a = 0; a < SrchRecord.length; a++) {

												nlapiLogExecution('ERROR', 'SrchRecord[a].getId()', SrchRecord[a].getId());
												nlapiLogExecution('ERROR', 'custrecord_serialnumber', SrchRecord[a].getValue('custrecord_serialnumber'));

												var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', SrchRecord[a].getId());
												var fields = new Array();
												var values = new Array();
												fields[0] = 'custrecord_serialstatus';
												values[0] = '';
												var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecord[a].getId(), fields, values);

												if(serialno == "" || serialno == null)
												{
													serialno = SrchRecord[a].getValue('custrecord_serialnumber');
												}
												else
												{
													serialno = serialno +","+ SrchRecord[a].getValue('custrecord_serialnumber');
												}
											}
											cyclecntrec.setFieldValue('custrecord_cycle_serialno', serialno);
										}
										//Case # 20126109 End

									}




									var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
									//Case# 20124247�Start
									//	nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
									//Case# 20124247�End
								}
							}
						}
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Recorded Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
				}
			}
			else {
				nlapiLogExecution('ERROR', 'ELSE1', 'ELSEXE--->');
				//var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Cycle Count Plan is Closed', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			}
		}

		else {
			nlapiLogExecution('ERROR', 'ELSE', 'ELSEXE--->');
			//var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Recorded Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}

		response.writePage(form);


	}
}

function ebiznet_CycleCoundRecordSave_CS(request)
{

	if(nlapiGetFieldValue('custpage_newrow')=='F')
	{
		try 
		{
			var chkValue='';
			chkValue=checkAtleast('custpage_items', 'custpage_rec');
			//alert(chkValue);
			if(chkValue == false) 
				return false;

		}
		catch(exp)
		{
			alert('Expception'+exp);
			return false;
		}
	}


	/*var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actsku');
	var fields = ['recordType','custitem_ebizbatchlot'];
	var columns = nlapiLookupField('item', itemId, fields);
	var ItemType = columns.recordType;	
	var batchflag="F";
	batchflag= columns.custitem_ebizbatchlot;
	var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');

	if (ItemType != "lotnumberedinventoryitem" || ItemType!="lotnumberedassemblyitem" || batchflag!="T" )
	{
		//alert(LotBatchText);
		if(LotBatchText!='' && LotBatchText!=null)
		{
			alert("For the ItemType: "+ItemType+" ,Actual LOT# need to be empty");
			return false;
		}
	}*/



	var qtyerrorflag = "N";
	var vactQty =  nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
	var site='';
	for (var i = 0; i < nlapiGetLineItemCount('custpage_items'); i++) 
	{
		if (nlapiGetLineItemValue('custpage_items', 'custpage_rec', i + 1) == 'T' && nlapiGetFieldValue('custpage_newrow')=='F') 
		{
			//Case# 20149982 starts
			var Binlocation = nlapiGetLineItemValue('custpage_items', 'custpage_location',i+1);
			//alert("Binlocation:" + Binlocation);
			var filter=new Array();
			filter.push(new nlobjSearchFilter('name',null,'is',Binlocation));
			filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var Searchrecord=nlapiSearchRecord('customrecord_ebiznet_location',null,filter,null);
			if(Searchrecord == null || Searchrecord == '')
			{
				alert("Enter Valid Binlocation");
				return false;
			}
			//Case# 20149982 ends
			site=nlapiGetLineItemValue('custpage_items','custpage_siteid',1);

			var ActualQty = nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1);
			var ExpQty = nlapiGetLineItemValue('custpage_items', 'custpage_expqty', i + 1);
			var invtid =  nlapiGetLineItemValue('custpage_items','custpage_invtid', i + 1);	
			var siteId = nlapiGetLineItemValue('custpage_items', 'custpage_siteid',i+1);
			var ExpItemid = nlapiGetLineItemValue('custpage_items', 'custpage_expsku',i+1);

			var transaction=null;
			var allocQty='';
			/*if(invtid!=null && invtid!='')
			{

				transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',invtid);

				allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
			}*/

			var allocQty = nlapiGetLineItemValue('custpage_items','custpage_allocqty', i + 1);
			if(allocQty == null || allocQty== "")
				allocQty=0;

			var itemId = nlapiGetLineItemValue('custpage_items','custpage_actsku', i + 1); 
			var expitmid = nlapiGetLineItemValue('custpage_items', 'custpage_expsku1',i + 1);	

			var fields = ['recordType','custitem_ebizbatchlot','custitem_ebizserialin'];
			//case 20124079,20124081,20124082 start
			if(itemId != null && itemId !='')
			{



				var serialInflg="F";		

				//Case # 20126109 Start

				var columns = nlapiLookupField('item', itemId, fields);
				var ItemType = columns.recordType;	
				var batchflag="F";
				batchflag= columns.custitem_ebizbatchlot;
				serialInflg = columns.custitem_ebizserialin;
				var LotBatchText=nlapiGetLineItemValue('custpage_items','custpage_actbatch', i + 1);
				var Location = nlapiGetLineItemValue('custpage_items','custpage_siteid',i+1);
				var BinLocation = nlapiGetLineItemValue('custpage_items','custpage_binlocationid',i+1);
				var item = nlapiGetLineItemValue('custpage_items', 'custpage_actsku',i+1);
				var ActualQty1 = nlapiGetLineItemValue('custpage_items','custpage_actualqty',i+1);
				var EnteredQty = nlapiGetLineItemValue('custpage_items','custpage_actqty',i+1);
				var expectedQty=nlapiGetLineItemValue('custpage_items','custpage_expqty',i+1);
				var LP = nlapiGetLineItemValue('custpage_items','custpage_actlp',i+1);
				//case no 20126108
				//Case # 20126109 End
				var TransactionNo = "";
				var getActualItem=nlapiGetLineItemValue('custpage_items', 'custpage_actsku',i+1);
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
				{
					if(invtid != null && invtid != "")
					{
						/*var fieldsPO = ['custrecord_ebiz_transaction_no'];
						var columnsPO = nlapiLookupField('customrecord_ebiznet_createinv', invtid, fieldsPO);*/
						//case 20126108 : added condition to check transno is null or empty
						/*if(columnsPO != null && columnsPO != '')
						{
							if(columnsPO.custrecord_ebiz_transaction_no != null && columnsPO.custrecord_ebiz_transaction_no !='' && columnsPO.custrecord_ebiz_transaction_no !='null')
							{
								TransactionNo = columnsPO.custrecord_ebiz_transaction_no;
							}
						}*/
						var fields=new Array();
						fields[0]=new nlobjSearchFilter('id', null, 'is', invtid);

						var columnser = new Array();
						columnser[0] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');
						var sresult=nlapiSearchRecord('customrecord_ebiznet_createinv', null, fields, columnser);


						//case 20126108 : added condition to check transno is null or empty
						if(sresult != null && sresult != '')
						{
							if(sresult[0].getValue('custrecord_ebiz_transaction_no') !=null && sresult[0].getValue('custrecord_ebiz_transaction_no') !='' && sresult[0].getValue('custrecord_ebiz_transaction_no') !='null')
							{
								/*if(columnsPO.custrecord_ebiz_transaction_no != null && columnsPO.custrecord_ebiz_transaction_no !='' && columnsPO.custrecord_ebiz_transaction_no !='null')
							{*/
								TransactionNo = sresult[0].getValue('custrecord_ebiz_transaction_no');
								//}
							}
						}
					}
					//Case # 20126109 Start
					if(ActualQty1==null || ActualQty1=='')
					{
						ActualQty1=0;
					}

					var getActLP = nlapiGetLineItemValue('custpage_items','custpage_actlp',i+1);

					if(getActLP == null || getActLP == '')
					{
						alert("Please enter Actual LP");
						qtyerrorflag = "Y";
						return false;
					}
					if(ActualQty == null || ActualQty == '' || isNaN(ActualQty) || ActualQty<0)
					{
						alert("Please enter a valid Actual Qty");
						qtyerrorflag = "Y";
						return false;
					}

					if(expitmid != item)
					{
						var filtersser1 = new Array();
						filtersser1[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getActualItem);
						filtersser1[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getActLP);
						// case no 20126280

						filtersser1[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
						//filtersser1[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'noneof', '32');
						var columnser1 = new Array();
						columnser1[0] = new nlobjSearchColumn('custrecord_serialnumber');

						var SrchRecordnew = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser1, columnser1);


						//if(SrchRecordnew!=null && SrchRecordnew!='' && parseFloat(SrchRecordnew.length)!=parseFloat(EnteredQty))
						if(SrchRecordnew==null || SrchRecordnew=='')
						{
							//alert(parseFloat(SrchRecordnew.length));
							//alert(parseFloat(EnteredQty));
							var vfrmName = 'CyclecountRecord';
							var NewQty = parseFloat(EnteredQty) - parseFloat(ActualQty1);
							//alert("Please enter value(s)for: Serial Items in Serial Entry for line No"+nlapiGetCurrentLineItemIndex('custpage_items'));
							var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
							/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
							//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
							//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
							linkURL = linkURL + '&custparam_serialskuid=' + item;
							linkURL = linkURL + '&custparam_serialskulp=' + LP;
							linkURL = linkURL + '&custparam_serialpoid=' + TransactionNo;
							// case no 20126483
							//linkURL = linkURL + '&custparam_serialskuchknqty=' + NewQty;
							linkURL = linkURL + '&custparam_serialskuchknqty=' + EnteredQty;
							linkURL = linkURL + '&custparam_serialformname=' + vfrmName;
							linkURL = linkURL + '&custparam_seriallocation=' + Location;
							linkURL = linkURL + '&custparam_serialbinlocation=' + BinLocation;
							//linkURL = linkURL + '&custparam_lot=' + BatchNo;
							//linkURL = linkURL + '&custparam_name=' + BatchNo;
							//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
							//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
							window.open(linkURL);	

							alert("Please enter value(s)for: Serial Items in Serial Entry for line No"+nlapiGetCurrentLineItemIndex('custpage_items'));
							nlapiSetLineItemValue('custpage_items','custpage_rec',i+1,'F');
							return false;
						}
					}
					//Case # 20126109 End
					if(parseFloat(EnteredQty) != 0)
					{
						var getActualItem=nlapiGetLineItemValue('custpage_items', 'custpage_actsku',i+1);
						var getActLP = nlapiGetLineItemValue('custpage_items','custpage_actlp',i+1);
						var filtersser = new Array();
						filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getActualItem);
						filtersser[1] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');
						if(getActLP!=null && getActLP!='')
						filtersser[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getActLP);
						// case no 20126280

						
						//	filtersser[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'noneof', '32');
						var columnser = new Array();
						columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
						// case no 20126280
						//if((parseFloat(EnteredQty)>parseFloat(ActualQty1)) && (SrchRecord!=null && SrchRecord!='' && parseFloat(SrchRecord.length)!=parseFloat(EnteredQty)))
						if((parseFloat(EnteredQty)>parseFloat(ActualQty1)) && (SrchRecord==null || SrchRecord==''))
						{


							var filtersser1 = new Array();
							filtersser1[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getActualItem);
							filtersser1[1] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
							if(getActLP!=null && getActLP!='')
							filtersser1[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getActLP);
							// case no 20126280

						
							//	filtersser1[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'noneof', '32');
							var columnser1 = new Array();
							columnser1[0] = new nlobjSearchColumn('custrecord_serialnumber');

							var SrchRecordnew = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser1, columnser1);


							//if(SrchRecordnew!=null && SrchRecordnew!='' && parseFloat(SrchRecordnew.length)!=parseFloat(EnteredQty))
							if(SrchRecordnew==null || SrchRecordnew=='')
							{
								//alert(parseFloat(SrchRecordnew.length));
								//alert(parseFloat(EnteredQty));
								var vfrmName = 'CyclecountRecord';
								var NewQty = parseFloat(EnteredQty) - parseFloat(ActualQty1);
								//alert("Please enter value(s)for: Serial Items in Serial Entry for line No"+nlapiGetCurrentLineItemIndex('custpage_items'));
								var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
								/*var ctx = nlapiGetContext();
								if (ctx.getEnvironment() == 'PRODUCTION') {
									linkURL = 'https://system.netsuite.com' + linkURL;			
								}
								else 
									if (ctx.getEnvironment() == 'SANDBOX') {
										linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
									}*/
								//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
								//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
								linkURL = linkURL + '&custparam_serialskuid=' + itemId;
								linkURL = linkURL + '&custparam_serialskulp=' + LP;
								linkURL = linkURL + '&custparam_serialpoid=' + TransactionNo;
								// case no 20126483
								//linkURL = linkURL + '&custparam_serialskuchknqty=' + NewQty;
								linkURL = linkURL + '&custparam_serialskuchknqty=' + EnteredQty;
								linkURL = linkURL + '&custparam_serialformname=' + vfrmName;
								linkURL = linkURL + '&custparam_seriallocation=' + Location;
								linkURL = linkURL + '&custparam_serialbinlocation=' + BinLocation;
								//linkURL = linkURL + '&custparam_lot=' + BatchNo;
								//linkURL = linkURL + '&custparam_name=' + BatchNo;
								//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
								//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
								window.open(linkURL);	

								alert("Please enter value(s)for: Serial Items in Serial Entry for line No"+nlapiGetCurrentLineItemIndex('custpage_items'));
								nlapiSetLineItemValue('custpage_items','custpage_rec',i+1,'F');
								return false;
							}


						}		
						//else if((parseFloat(EnteredQty)<parseFloat(ActualQty1)) && (SrchRecord!=null && SrchRecord!='' && parseFloat(SrchRecord.length)!=parseFloat(EnteredQty)))
						else if((parseFloat(EnteredQty)<parseFloat(ActualQty1)) && (SrchRecord==null || SrchRecord==''))
						{

							//var ReqQty = parseFloat(EnteredQty);
							var ReqQty =EnteredQty; //parseFloat(ActualQty1) - parseFloat(EnteredQty);
							var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');
							/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
							//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
							//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
							linkURL = linkURL + '&custparam_serialskuid=' + itemId;
							linkURL = linkURL + '&custparam_serialsku=' + item;
							linkURL = linkURL + '&custparam_serialskulp=' + LP;
							linkURL = linkURL + '&custparam_serialskuqty=' + ReqQty;	
							//linkURL = linkURL + '&custparam_lot=' + BatchNo;
							//linkURL = linkURL + '&custparam_name=' + BatchNo;
							//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
							//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
							window.open(linkURL);

							alert("Please select required Serial numbers to adjust for line No " +nlapiGetCurrentLineItemIndex('custpage_items'));
							nlapiSetLineItemValue('custpage_items','custpage_rec',i+1,'F');
							return false;

						}
					}	
				}
				if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" || batchflag =="T" )
				{
					//alert(LotBatchText);
					//return true;

					if((expitmid != null && expitmid != '' ) && (item != null && item != '' ) && (expitmid != item))
					{
						var LotBatchText=nlapiGetLineItemValue('custpage_items', 'custpage_actbatch',i+1);
						var siteId=nlapiGetLineItemValue('custpage_items', 'custpage_siteid',i+1);
						if(LotBatchText)
						{				//alert(LotBatchText);						


							var batchCustRecChk = BatchCustRecordChk(LotBatchText,item);
							
							if(batchCustRecChk == false)
							{

								alert("Enter the details in Batch Entry");
								//nlapiGetLineItemValue('custpage_items', 'custpage_rec', i + 1)
								
								nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
								return false;
							}
							
							var RecID=getLotBatchId(LotBatchText);//
							//alert(RecID);
							if(RecID==null||RecID=="")
							{
								var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
								var ctx = nlapiGetContext();
								if (ctx.getEnvironment() == 'PRODUCTION') {
									linkURL = 'https://system.netsuite.com' + linkURL;			
								}
								else 
									if (ctx.getEnvironment() == 'SANDBOX') {
										linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
									}
								linkURL = linkURL + '&custparam_lot=' + LotBatchText;
								if(siteId!=null&&siteId!="")
									linkURL = linkURL + '&custparam_locationid='+siteId;
								linkURL = linkURL + '&custparam_serialskuid=' + itmid;
								linkURL = linkURL + '&custparam_name=' + LotBatchText;
								window.open(linkURL);
							}
						}
						else
						{
							var LotBatchText=nlapiGetLineItemValue('custpage_items', 'custpage_actbatch',i+1);
							if(LotBatchText ==null || LotBatchText =='')
							{
								alert("Please enter Actual LOT#");
								//nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
								return false;
							}
						}


					}
					
					
					
					if(item != null && item != '')
					{
						var LotBatchText=nlapiGetLineItemValue('custpage_items', 'custpage_actbatch',i+1);
						var siteId=nlapiGetLineItemValue('custpage_items', 'custpage_siteid',i+1);



						if(LotBatchText)
						{				//alert(LotBatchText);						


							var batchCustRecChk = BatchCustRecordChk(LotBatchText,item);

							if(batchCustRecChk == false)
							{

								alert("Enter the details in Batch Entry");
								//nlapiGetLineItemValue('custpage_items', 'custpage_rec', i + 1)

								nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
								return false;
							}

							var RecID=getLotBatchId(LotBatchText);//
							//alert(RecID);
							if(RecID==null||RecID=="")
							{
								var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
								var ctx = nlapiGetContext();
								if (ctx.getEnvironment() == 'PRODUCTION') {
									linkURL = 'https://system.netsuite.com' + linkURL;			
								}
								else 
									if (ctx.getEnvironment() == 'SANDBOX') {
										linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
									}
								linkURL = linkURL + '&custparam_lot=' + LotBatchText;
								if(siteId!=null&&siteId!="")
									linkURL = linkURL + '&custparam_locationid='+siteId;
								linkURL = linkURL + '&custparam_serialskuid=' + itmid;
								linkURL = linkURL + '&custparam_name=' + LotBatchText;
								window.open(linkURL);
							}
						}
						else
						{
							var LotBatchText=nlapiGetLineItemValue('custpage_items', 'custpage_actbatch',i+1);
							if(LotBatchText ==null || LotBatchText =='')
							{
								alert("Please enter Actual LOT#");
								//nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
								return false;
							}
						}


					}
					
					if((expitmid != null && expitmid != '' ) && (item != null && item != '' ) && (expitmid == item))
					{
						
						var LotBatchText=nlapiGetLineItemValue('custpage_items', 'custpage_actbatch',i+1);
						if(LotBatchText ==null || LotBatchText =='')
						{
							alert("Please enter Actual LOT#");
							//nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
							return false;
						}
						else
						{

							var batchCustRecChk = BatchCustRecordChk(LotBatchText,item);

							if(batchCustRecChk == false)
							{

								alert("Enter the details in Batch Entry");							
								nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
								return false;
							}
						}
						
						
					}
					
					if(ActualQty == null || ActualQty == '' || isNaN(ActualQty) || ActualQty<0)
					{
						alert("Please enter a valid Actual Qty");
						qtyerrorflag = "Y";
						return false;
					}
					
					
					
					

				}
				else
				{
					if(ActualQty == null || ActualQty == '' || isNaN(ActualQty) || ActualQty<0)
					{
						alert("Please enter a valid Actual Qty");
						qtyerrorflag = "Y";
						return false;
					}
					
					if(LotBatchText!='' && LotBatchText!=null)
					{
						alert("For the ItemType: "+ItemType+" ,Actual LOT# need to be empty");
						return false;
					}
				}

				//end
				//alert('vactQty'+vactQty);
				//alert('ActualQty'+ActualQty);
				//alert('allocQty'+allocQty);

				if(parseInt(allocQty)>parseInt(ActualQty))
				{				
					alert("Actual Qty ("+ActualQty+") should not be less than Allocated Qty ("+allocQty+")");
					qtyerrorflag = "Y";
					return false;
				}
				//case# 20124768�Start
				var palletQuantity = 0;
				if(itemId  !=null && itemId!='')
				{
					palletQuantity = fetchPalletQuantity(itemId,siteId,null);
				}
				//alert(palletQuantity);
				if((parseFloat(palletQuantity) != 0) && (parseFloat(ActualQty) > parseFloat(palletQuantity)))
				{
					alert('Actual Qty Should not be Greater than Pallet Qty');
					return false;
				}
				//Case# 20124768� End
//				else if(parseInt(ActualQty)<0)
//				{//alert('hi1');
//				alert("Actual Qty Should Be Greater Than 0");
//				qtyerrorflag = "Y";
//				return false;
//				}
//				else if(parseInt(vactQty)<=0)
//				{//alert('hi2');
//				alert("Actual Qty Should Be Greater Than 0");
//				qtyerrorflag = "Y";
//				return false;
//				}

				else
				{
					//alert('getActualItem'+getActualItem);
					if(getActualItem != null && getActualItem != '')
					{	
						/*var transaction = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', nlapiGetLineItemValue('custpage_items', 'custpage_id', i + 1));            
			transaction.setFieldValue('custrecord_act_beg_date', DateStamp());
			transaction.setFieldValue('custrecord_cycact_beg_time', TimeStamp());
			transaction.setFieldValue('custrecord_cycleact_end_date', DateStamp());
			transaction.setFieldValue('custrecord_cycact_end_time', TimeStamp());
			transaction.setFieldValue('custrecord_cyclerec_upd_date', DateStamp());
			transaction.setFieldValue('custrecord_cyclerec_upd_time', TimeStamp());       
			transaction.setFieldValue('custrecord_cycleact_sku', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
			transaction.setFieldValue('custrecord_cyclecount_act_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
			transaction.setFieldValue('custrecord_cycle_act_qty', nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));            
			transaction.setFieldValue('custrecord_cycact_lpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
			transaction.setFieldValue('custrecord_cycleabatch_no', nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1));
			transaction.setFieldValue('custrecord_cyclepc', nlapiGetLineItemValue('custpage_items', 'custpage_actpc', i + 1));
			transaction.setFieldValue('custrecord_cyclesku_status', nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));
			transaction.setFieldValue('custrecord_cyclestatus_flag', 31);
			transaction.setFieldValue('custrecord_cycleupd_user_no', nlapiGetContext().getUser());
			transaction.setFieldValue('custrecord_cyccplan_recorddate', DateStamp());
			transaction.setFieldValue('custrecord_cyccplan_recordtime', TimeStamp());
			transaction.setFieldValue('custrecord_cyccplan_recordedby', nlapiGetContext().getUser());
			var id=nlapiSubmitRecord(transaction, true);*/
						var getActualItem=nlapiGetLineItemValue('custpage_items', 'custpage_actsku',i+1);
						var getActLP = nlapiGetLineItemValue('custpage_items','custpage_actlp',i+1);
						var serialno = '';
						var filtersser = new Array();
						filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getActualItem);
						if(getActLP!=null && getActLP!='')
						filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getActLP);
						//Case # 20127185�,20148167� Start
						/*if(itemId != expitmid)
				filtersser[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
				else
					filtersser[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');*/

						if((parseFloat(EnteredQty)>parseFloat(ActualQty1)))
						{
							filtersser[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
						}
						else if((parseFloat(EnteredQty)<parseFloat(ActualQty1)))
						{
							filtersser[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');	
						}
						else
						{

						}
						//Case # 20127185,20148167�� end

						var columnser = new Array();
						columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
						if(SrchRecord != null && SrchRecord != '' && SrchRecord.length > 0)
						{
							nlapiLogExecution('ERROR', 'SrchRecord.length', SrchRecord.length);
							for (var a = 0; a < SrchRecord.length; a++) {

								nlapiLogExecution('ERROR', 'SrchRecord[a].getId()', SrchRecord[a].getId());
								nlapiLogExecution('ERROR', 'custrecord_serialnumber', SrchRecord[a].getValue('custrecord_serialnumber'));

								var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', SrchRecord[a].getId());
								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialstatus';
								values[0] = '';
								var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecord[a].getId(), fields, values);

								if(serialno == "" || serialno == null)
								{
									serialno = SrchRecord[a].getValue('custrecord_serialnumber');
								}
								else
								{
									serialno = serialno +","+ SrchRecord[a].getValue('custrecord_serialnumber');
								}
							}
						}
						/*	var vcycrecid=nlapiGetLineItemValue('custpage_items', 'custpage_id', i + 1);
						if(vcycrecid!=null && vcycrecid!='')
						{
							//alert('vcycrecid'+vcycrecid);
							var fieldNames = new Array(); 
							fieldNames.push('custrecord_act_beg_date');  
							fieldNames.push('custrecord_cycact_beg_time');
							fieldNames.push('custrecord_cycleact_end_date');
							fieldNames.push('custrecord_cycact_end_time');
							fieldNames.push('custrecord_cyclerec_upd_date');
							fieldNames.push('custrecord_cyclerec_upd_time');
							fieldNames.push('custrecord_cycleact_sku');
							fieldNames.push('custrecord_cyclecount_act_ebiz_sku_no');
							fieldNames.push('custrecord_cycle_act_qty');
							fieldNames.push('custrecord_cycact_lpno');
							fieldNames.push('custrecord_cycleabatch_no');
							fieldNames.push('custrecord_cyclepc');
							fieldNames.push('custrecord_cyclesku_status');
							fieldNames.push('custrecord_cyclestatus_flag');
							fieldNames.push('custrecord_cycleupd_user_no');
							fieldNames.push('custrecord_cyccplan_recorddate');
							fieldNames.push('custrecord_cyccplan_recordtime');
							fieldNames.push('custrecord_cyccplan_recordedby');
							fieldNames.push('custrecord_cycle_serialno');
							fieldNames.push('custrecord_cycleexp_qty');



							var newValues = new Array(); 
							newValues.push(DateStamp());
							newValues.push(TimeStamp());
							newValues.push(DateStamp());
							newValues.push(TimeStamp());
							newValues.push(DateStamp());
							newValues.push(TimeStamp());
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actpc', i + 1));
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));
							newValues.push('31');
							newValues.push(nlapiGetContext().getUser());
							newValues.push(DateStamp());
							newValues.push(TimeStamp());
							newValues.push(nlapiGetContext().getUser());
							newValues.push(serialno);
							newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_expqty', i + 1));
							nlapiSubmitField('customrecord_ebiznet_cyclecountexe', vcycrecid, fieldNames, newValues);
							//Case# 20124247�Start
							nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
							//Case# 20124247�End
						}
						else
						{
							var fields=new Array();

							fields[0]=new nlobjSearchFilter('name', null, 'is', nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
							var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);

							var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
							customrecord.setFieldValue('custrecord_tasktype', 7);
							customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
							customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
							customrecord.setFieldValue('custrecord_expe_qty', '0');
							customrecord.setFieldValue('custrecord_lpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
							customrecord.setFieldValue('custrecord_sku', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							customrecord.setFieldValue('custrecord_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							if(sresult!=null && sresult.length>0)
								customrecord.setFieldValue('custrecord_actbeginloc', sresult[0].getId());
							customrecord.setFieldValue('custrecord_ebiz_cntrl_no', nlapiGetFieldValue('custpage_cyclecountplan'));					
							customrecord.setFieldValue('name', nlapiGetFieldValue('custpage_cyclecountplan'));
							if(site!=null && site!='')
								customrecord.setFieldValue('custrecord_site_id', site);	
							var opentaskRecId = nlapiSubmitRecord(customrecord);

							var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
							cyclecntrec.setFieldValue('custrecord_cycle_count_plan', nlapiGetFieldValue('custpage_cyclecountplan'));
							cyclecntrec.setFieldValue('name', nlapiGetFieldValue('custpage_cyclecountplan'));
							cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
							cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
							cyclecntrec.setFieldValue('custrecord_cyclesku',nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							cyclecntrec.setFieldValue('custrecord_cycleact_sku',nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							cyclecntrec.setFieldValue('custrecord_cyclecount_act_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							cyclecntrec.setFieldValue('custrecord_cycleexp_qty', '0');
							cyclecntrec.setFieldValue('custrecord_cyclelpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
							cyclecntrec.setFieldValue('custrecord_cycact_lpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
							cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 31);//status flag is 'R'
							if(sresult!=null && sresult.length>0)								
								cyclecntrec.setFieldValue('custrecord_cycact_beg_loc',sresult[0].getId());
							cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskRecId);
							cyclecntrec.setFieldValue('custrecord_invtid', null);
							cyclecntrec.setFieldValue('custrecord_expcycleabatch_no',nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1));
							//Case # 20126952 Start
							cyclecntrec.setFieldValue('custrecord_cycleabatch_no',nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1));
							//Case # 20126952 End
							cyclecntrec.setFieldValue('custrecord_expcyclesku_status', nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));
							cyclecntrec.setFieldValue('custrecord_cyclesku_status', nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));
							cyclecntrec.setFieldValue('custrecord_expcyclepc', nlapiGetLineItemValue('custpage_items', 'custpage_actpc', i + 1));
							cyclecntrec.setFieldValue('custrecord_cycle_act_qty', nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
							cyclecntrec.setFieldValue('custrecord_cycleupd_user_no', nlapiGetContext().getUser());
							cyclecntrec.setFieldValue('custrecord_cyccplan_recorddate', DateStamp());
							cyclecntrec.setFieldValue('custrecord_cyccplan_recordtime', TimeStamp());
							cyclecntrec.setFieldValue('custrecord_cyccplan_recordedby', nlapiGetContext().getUser());
							if(site!=null && site!='')
								cyclecntrec.setFieldValue('custrecord_cyclesite_id', site);
							cyclecntrec.setFieldValue('custrecord_act_beg_date', DateStamp());
							cyclecntrec.setFieldValue('custrecord_cycact_beg_time', TimeStamp());
							cyclecntrec.setFieldValue('custrecord_cycleact_end_date', DateStamp());
							cyclecntrec.setFieldValue('custrecord_cycact_end_time', TimeStamp());
							cyclecntrec.setFieldValue('custrecord_cyclerec_upd_date', DateStamp());
							cyclecntrec.setFieldValue('custrecord_cyclerec_upd_time', TimeStamp());
							cyclecntrec.setFieldValue('custrecord_cycle_notes', 'fromaddnewitem');


							//Case # 20126109 Start
							var filtersser = new Array();
							filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
							filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));

							var columnser = new Array();
							columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

							var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
							if(SrchRecord != null && SrchRecord != '' && SrchRecord.length > 0)
							{
								var serialno='';
								nlapiLogExecution('ERROR', 'SrchRecord.length', SrchRecord.length);
								for (var a = 0; a < SrchRecord.length; a++) {

									nlapiLogExecution('ERROR', 'SrchRecord[a].getId()', SrchRecord[a].getId());
									nlapiLogExecution('ERROR', 'custrecord_serialnumber', SrchRecord[a].getValue('custrecord_serialnumber'));

									var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', SrchRecord[a].getId());
									var fields = new Array();
									var values = new Array();
									fields[0] = 'custrecord_serialstatus';
									values[0] = '';
									var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecord[a].getId(), fields, values);

									if(serialno == "" || serialno == null)
									{
										serialno = SrchRecord[a].getValue('custrecord_serialnumber');
									}
									else
									{
										serialno = serialno +","+ SrchRecord[a].getValue('custrecord_serialnumber');
									}
								}
								cyclecntrec.setFieldValue('custrecord_cycle_serialno', serialno);
							}
							//Case # 20126109 End






							var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
							//Case# 20124247�Start
							nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
							//Case# 20124247�End
						}*/
					}
				}//Here
			}
			else
			{
				alert("Please enter Actual Item");
				nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');

				return false;

				if(ExpItemid != null && ExpItemid != '')
				{	
					alert("Please enter Actual Item");
					qtyerrorflag = "Y";
					return false;
				}
				else
				{
					/*var vcycrecid=nlapiGetLineItemValue('custpage_items', 'custpage_id', i + 1);
					if(vcycrecid!=null && vcycrecid!='')
					{
						//alert('vcycrecid'+vcycrecid);
						var fieldNames = new Array(); 
						fieldNames.push('custrecord_act_beg_date');  
						fieldNames.push('custrecord_cycact_beg_time');
						fieldNames.push('custrecord_cycleact_end_date');
						fieldNames.push('custrecord_cycact_end_time');
						fieldNames.push('custrecord_cyclerec_upd_date');
						fieldNames.push('custrecord_cyclerec_upd_time');
						if(vActItem1 != null && vActItem1 != '')
						{	
							fieldNames.push('custrecord_cycleact_sku');
							fieldNames.push('custrecord_cyclecount_act_ebiz_sku_no');
						}
						if(vActQty1 != null && vActQty1 != '')
							fieldNames.push('custrecord_cycle_act_qty');
						if(vActLP1 != null && vActLP1 != '')
							fieldNames.push('custrecord_cycact_lpno');
						if(vActBatch1 != null && vActBatch1 != '')
							fieldNames.push('custrecord_cycleabatch_no');
						if(vActPC1 != null && vActPC1 != '')
							fieldNames.push('custrecord_cyclepc');
						if(vActItemStatus1 != null && vActItemStatus1 != '')
							fieldNames.push('custrecord_cyclesku_status');
						fieldNames.push('custrecord_cyclestatus_flag');
						fieldNames.push('custrecord_cycleupd_user_no');
						fieldNames.push('custrecord_cyccplan_recorddate');
						fieldNames.push('custrecord_cyccplan_recordtime');
						fieldNames.push('custrecord_cyccplan_recordedby');
						fieldNames.push('custrecord_cycle_serialno');
						fieldNames.push('custrecord_cycleexp_qty');

						var newValues = new Array(); 
						newValues.push(DateStamp());
						newValues.push(TimeStamp());
						newValues.push(DateStamp());
						newValues.push(TimeStamp());
						newValues.push(DateStamp());
						newValues.push(TimeStamp());
						var vActItem1=nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1);
						var vActQty1=nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1);
						var vActLP1=nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1);
						var vActBatch1=nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1);
						var vActPC1=nlapiGetLineItemValue('custpage_items', 'custpage_actpc', i + 1);
						var vActItemStatus1=nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1);

						if(vActItem1 != null && vActItem1 != '')
						{
							newValues.push(vActItem1);
							newValues.push(vActItem1);
						}
						if(vActQty1 != null && vActQty1 != '')
							newValues.push(vActQty1);
						if(vActLP1 != null && vActLP1 != '')
							newValues.push(vActLP1);
						if(vActBatch1 != null && vActBatch1 != '')
							newValues.push(vActBatch1);
						if(vActPC1 != null && vActPC1 != '')
							newValues.push(vActPC1);
						if(vActItemStatus1 != null && vActItemStatus1 != '')
							newValues.push(vActItemStatus1);
						newValues.push('31');
						newValues.push(nlapiGetContext().getUser());
						newValues.push(DateStamp());
						newValues.push(TimeStamp());
						newValues.push(nlapiGetContext().getUser());
						newValues.push(serialno);
						newValues.push(nlapiGetLineItemValue('custpage_items', 'custpage_expqty', i + 1));
						nlapiSubmitField('customrecord_ebiznet_cyclecountexe', vcycrecid, fieldNames, newValues);
						//Case# 20124247�Start

						var fields=new Array();

						fields[0]=new nlobjSearchFilter('name', null, 'is', nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
						var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);

						var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
						customrecord.setFieldValue('custrecord_tasktype', 7);
						customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
						customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
						customrecord.setFieldValue('custrecord_expe_qty', '0');
						customrecord.setFieldValue('custrecord_lpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
						customrecord.setFieldValue('custrecord_sku', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						customrecord.setFieldValue('custrecord_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						if(sresult!=null && sresult.length>0)
							customrecord.setFieldValue('custrecord_actbeginloc', sresult[0].getId());
						customrecord.setFieldValue('custrecord_ebiz_cntrl_no', nlapiGetFieldValue('custpage_cyclecountplan'));					
						customrecord.setFieldValue('name', nlapiGetFieldValue('custpage_cyclecountplan'));
						if(site!=null && site!='')
							customrecord.setFieldValue('custrecord_site_id', site);	
						var opentaskRecId = nlapiSubmitRecord(customrecord);

						nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
						//Case# 20124247�End
					}
					else
					{
						var fields=new Array();

						fields[0]=new nlobjSearchFilter('name', null, 'is', nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
						var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);

						var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
						customrecord.setFieldValue('custrecord_tasktype', 7);
						customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
						customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
						customrecord.setFieldValue('custrecord_expe_qty', '0');
						customrecord.setFieldValue('custrecord_lpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
						customrecord.setFieldValue('custrecord_sku', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						customrecord.setFieldValue('custrecord_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						if(sresult!=null && sresult.length>0)
							customrecord.setFieldValue('custrecord_actbeginloc', sresult[0].getId());
						customrecord.setFieldValue('custrecord_ebiz_cntrl_no', nlapiGetFieldValue('custpage_cyclecountplan'));					
						customrecord.setFieldValue('name', nlapiGetFieldValue('custpage_cyclecountplan'));
						if(site!=null && site!='')
							customrecord.setFieldValue('custrecord_site_id', site);	
						var opentaskRecId = nlapiSubmitRecord(customrecord);

						var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
						cyclecntrec.setFieldValue('custrecord_cycle_count_plan', nlapiGetFieldValue('custpage_cyclecountplan'));
						cyclecntrec.setFieldValue('name', nlapiGetFieldValue('custpage_cyclecountplan'));
						cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
						cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
						cyclecntrec.setFieldValue('custrecord_cyclesku',nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						cyclecntrec.setFieldValue('custrecord_cycleact_sku',nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						cyclecntrec.setFieldValue('custrecord_cyclecount_act_ebiz_sku_no', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						cyclecntrec.setFieldValue('custrecord_cycleexp_qty', '0');
						cyclecntrec.setFieldValue('custrecord_cyclelpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
						cyclecntrec.setFieldValue('custrecord_cycact_lpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
						cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 31);//status flag is 'R'
						if(sresult!=null && sresult.length>0)								
							cyclecntrec.setFieldValue('custrecord_cycact_beg_loc',sresult[0].getId());
						cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskRecId);
						cyclecntrec.setFieldValue('custrecord_invtid', null);
						cyclecntrec.setFieldValue('custrecord_expcycleabatch_no',nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1));
						cyclecntrec.setFieldValue('custrecord_expcyclesku_status', nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));
						cyclecntrec.setFieldValue('custrecord_cyclesku_status', nlapiGetLineItemValue('custpage_items', 'custpage_actitemstatus', i + 1));
						cyclecntrec.setFieldValue('custrecord_expcyclepc', nlapiGetLineItemValue('custpage_items', 'custpage_actpc', i + 1));
						cyclecntrec.setFieldValue('custrecord_cycle_act_qty', nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
						cyclecntrec.setFieldValue('custrecord_cycleupd_user_no', nlapiGetContext().getUser());
						cyclecntrec.setFieldValue('custrecord_cyccplan_recorddate', DateStamp());
						cyclecntrec.setFieldValue('custrecord_cyccplan_recordtime', TimeStamp());
						cyclecntrec.setFieldValue('custrecord_cyccplan_recordedby', nlapiGetContext().getUser());
						if(site!=null && site!='')
							cyclecntrec.setFieldValue('custrecord_cyclesite_id', site);
						cyclecntrec.setFieldValue('custrecord_act_beg_date', DateStamp());
						cyclecntrec.setFieldValue('custrecord_cycact_beg_time', TimeStamp());
						cyclecntrec.setFieldValue('custrecord_cycleact_end_date', DateStamp());
						cyclecntrec.setFieldValue('custrecord_cycact_end_time', TimeStamp());
						cyclecntrec.setFieldValue('custrecord_cyclerec_upd_date', DateStamp());
						cyclecntrec.setFieldValue('custrecord_cyclerec_upd_time', TimeStamp());
						cyclecntrec.setFieldValue('custrecord_cycle_notes', 'fromaddnewitem');


						//Case # 20126109 Start
						var filtersser = new Array();
						filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
						filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));

						var columnser = new Array();
						columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
						if(SrchRecord != null && SrchRecord != '' && SrchRecord.length > 0)
						{
							var serialno='';
							nlapiLogExecution('ERROR', 'SrchRecord.length', SrchRecord.length);
							for (var a = 0; a < SrchRecord.length; a++) {

								nlapiLogExecution('ERROR', 'SrchRecord[a].getId()', SrchRecord[a].getId());
								nlapiLogExecution('ERROR', 'custrecord_serialnumber', SrchRecord[a].getValue('custrecord_serialnumber'));

								var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', SrchRecord[a].getId());
								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialstatus';
								values[0] = '';
								var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecord[a].getId(), fields, values);

								if(serialno == "" || serialno == null)
								{
									serialno = SrchRecord[a].getValue('custrecord_serialnumber');
								}
								else
								{
									serialno = serialno +","+ SrchRecord[a].getValue('custrecord_serialnumber');
								}
							}
							cyclecntrec.setFieldValue('custrecord_cycle_serialno', serialno);
						}
						//Case # 20126109 End






						var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
						//Case# 20124247�Start
						nlapiSetFieldValue('custpage_msgfield','cycRecordedSucessfully');
						//Case# 20124247�End
					}*/
				}
			}
		}
	}    

	if(qtyerrorflag == "N")
	{
		return true;
	}
}


//Code added on 20Th Mar by suman.
//This will trigger a new window if the batch no entered is new.
/**
 * @param name
 */
function Onchange(type,name)
{
	try{
		//case  20126544 start
		if(name=='custpage_location')
		{	

			var Binlocation = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_location');
			//alert(Binlocation);

			var filter=new Array();
			filter.push(new nlobjSearchFilter('name',null,'is',Binlocation));
			filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

			var Searchrecord=nlapiSearchRecord('customrecord_ebiznet_location',null,filter,null);
			if(Searchrecord == null || Searchrecord == '')
			{
				alert("Enter Valid Binlocation");
				return false;
			}

		}
		//case  20126544 end
		if(name=='custpage_actbatch')
		{
			var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actsku');
			var fields = ['recordType','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', itemId, fields);
			var ItemType = columns.recordType;	
			var batchflag="F";
			batchflag= columns.custitem_ebizbatchlot;

			if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
			{
				var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');
				var siteId=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_siteid');
				if(LotBatchText)
				{
					var RecID=getLotBatchId(LotBatchText);
					if(RecID==null||RecID=="")
					{
						var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
						/*var ctx = nlapiGetContext();
						if (ctx.getEnvironment() == 'PRODUCTION') {
							linkURL = 'https://system.netsuite.com' + linkURL;			
						}
						else 
							if (ctx.getEnvironment() == 'SANDBOX') {
								linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
							}*/
						linkURL = linkURL + '&custparam_lot=' + LotBatchText;
						if(siteId!=null&&siteId!="")
							linkURL = linkURL + '&custparam_locationid='+siteId;
						linkURL = linkURL + '&custparam_serialskuid=' + itemId;
						linkURL = linkURL + '&custparam_name=' + LotBatchText;
						window.open(linkURL);
					}
				}
				else
				{
					alert("For the ItemType: "+ItemType+" ,Actual LOT# need not to be empty");
					nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
				}
			}
			else
			{
				var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');
				if(LotBatchText !=null && LotBatchText !=''){
					var value='';
					alert("For the ItemType: "+ItemType+" ,Actual LOT# need to be empty");
					nlapiSetCurrentLineItemValue('custpage_items','custpage_actbatch',value);
					return false;
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ONChange client script for Batch Entry',exp);
	}

	try
	{
		if(name=='custpage_actsku')
		{

			var ActualQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_actualqty');
			var EnteredQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
			var LP = nlapiGetCurrentLineItemValue('custpage_items','custpage_actlp');
			var InvtID = nlapiGetCurrentLineItemValue('custpage_items','custpage_invtid');
			var TransactionNo = "";
			var Location = nlapiGetCurrentLineItemValue('custpage_items','custpage_siteid');
			var BinLocation = nlapiGetCurrentLineItemValue('custpage_items','custpage_binlocationid');

			var itmid = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actsku');
			var fields = ['recordType','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', itmid, fields);
			var ItemType = columns.recordType;	
			var batchflag="F";
			batchflag= columns.custitem_ebizbatchlot;

			if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
			{
				var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');
				if(LotBatchText ==null || LotBatchText =='')
				{
					alert("Please enter Actual LOT#");
					return false;
				}
			}
			else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
			{


				if(parseFloat(EnteredQty) != 0)
				{
					var vfrmName = 'CyclecountRecord';
					var NewQty = parseFloat(EnteredQty) - parseFloat(ActualQty);

					if(InvtID != null && InvtID != "")
					{
						var fieldsPO = ['custrecord_ebiz_transaction_no'];
						var columnsPO = nlapiLookupField('customrecord_ebiznet_createinv', InvtID, fieldsPO);
						if(columnsPO != null && columnsPO != '')
						{
							if(columnsPO.custrecord_ebiz_transaction_no != null)
							{
								TransactionNo = columnsPO.custrecord_ebiz_transaction_no;
							}
						}
					}

					//Case # 20126109 Start
					var filtersser1 = new Array();
					filtersser1[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itmid);
					filtersser1[1] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
					if(LP!=null && LP!='')
					filtersser1[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
					
					// case no 20126280

					
					var columnser1 = new Array();
					columnser1[0] = new nlobjSearchColumn('custrecord_serialnumber');

					var SrchRecordnew = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser1, columnser1);


					//if(SrchRecordnew!=null && SrchRecordnew!='' && parseFloat(SrchRecordnew.length)!=parseFloat(EnteredQty))
					if(SrchRecordnew==null || SrchRecordnew=='')
					{

						//Case # 20126109 End


						alert("Please enter value(s)for: Serial Items in Serial Entry");
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
						/*var ctx = nlapiGetContext();
						if (ctx.getEnvironment() == 'PRODUCTION') {
							linkURL = 'https://system.netsuite.com' + linkURL;			
						}
						else 
							if (ctx.getEnvironment() == 'SANDBOX') {
								linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
							}*/
						//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
						//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
						linkURL = linkURL + '&custparam_serialskuid=' + itmid;
						linkURL = linkURL + '&custparam_serialskulp=' + LP;
						linkURL = linkURL + '&custparam_serialpoid=' + TransactionNo;
						// case no 20126483
						linkURL = linkURL + '&custparam_serialskuchknqty=' + NewQty;
						//linkURL = linkURL + '&custparam_serialskuchknqty=' + EnteredQty;
						linkURL = linkURL + '&custparam_serialformname=' + vfrmName;
						linkURL = linkURL + '&custparam_seriallocation=' + Location;
						linkURL = linkURL + '&custparam_serialbinlocation=' + BinLocation;
						//linkURL = linkURL + '&custparam_lot=' + BatchNo;
						//linkURL = linkURL + '&custparam_name=' + BatchNo;
						//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
						//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
						window.open(linkURL);

					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ONChange client script for Batch Entry',exp);
	}	
	//case# 201411389
	try
	{
		if(name=='custpage_actitemstatus')
		{
			var actitemstatus = nlapiGetCurrentLineItemValue('custpage_items','custpage_actitemstatus');
			var siteid1 = nlapiGetCurrentLineItemValue('custpage_items','custpage_siteid');

			var itemStatusFilters = new Array();
			itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
			itemStatusFilters[1] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',siteid1);
			itemStatusFilters[2] = new nlobjSearchFilter('internalid',null, 'is',actitemstatus);

			var itemStatusColumns = new Array();			
			itemStatusColumns[0] = new nlobjSearchColumn('name');

			var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);			
			if(itemStatusSearchResult == null || itemStatusSearchResult == '')
			{
				//ItemStatus = itemStatusSearchResult[0].getId();
				alert("Please Enter Valid ItemStatus");
				//nlapiSetCurrentLineItemValue('custpage_items','custpage_actitemstatus','');
				return false;
			}

		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ONChange client script for itemstatus',exp);
	}
	try
	{
		if(name=='custpage_rec')
		{
			var flagstatus=nlapiGetCurrentLineItemValue('custpage_items','custpage_rec');
			if(flagstatus=='T')
			{

				var itmid = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actsku');	
				var expitmid = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_expsku1');	
				var item = nlapiGetCurrentLineItemText('custpage_items', 'custpage_actsku');
				var ActualQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_actualqty');
				var EnteredQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
				var ExpectedQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_expqty');
				//var QOH = nlapiGetCurrentLineItemValue('custpage_items','custpage_invadj_itemquantity');
				var LP = nlapiGetCurrentLineItemValue('custpage_items','custpage_actlp');
				var InvtID = nlapiGetCurrentLineItemValue('custpage_items','custpage_invtid');
				var TransactionNo = "";
				var Location = nlapiGetCurrentLineItemValue('custpage_items','custpage_siteid');
				var BinLocation = nlapiGetCurrentLineItemValue('custpage_items','custpage_binlocationid');
				var batchflag="F";
				var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', itmid, fields);
				var ItemType = columns.recordType;
				batchflag= columns.custitem_ebizbatchlot;
				var serialInflg="F";		
				serialInflg = columns.custitem_ebizserialin;

				if(ActualQty == null || ActualQty == "")
				{
					ActualQty = 0;
				}


				if((expitmid != null && expitmid != '' ) && (itmid != null && itmid != '' ) && (expitmid != itmid))
				{
					if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
					{

						var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');
						var siteId=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_siteid');
						if(LotBatchText)
						{/*
							//alert(LotBatchText);
							var RecID=getLotBatchId(LotBatchText);
							//alert(RecID);
							if(RecID==null||RecID=="")
							{
								var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
								var ctx = nlapiGetContext();
								if (ctx.getEnvironment() == 'PRODUCTION') {
									linkURL = 'https://system.netsuite.com' + linkURL;			
								}
								else 
									if (ctx.getEnvironment() == 'SANDBOX') {
										linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
									}
								linkURL = linkURL + '&custparam_lot=' + LotBatchText;
								if(siteId!=null&&siteId!="")
									linkURL = linkURL + '&custparam_locationid='+siteId;
								linkURL = linkURL + '&custparam_serialskuid=' + itmid;
								linkURL = linkURL + '&custparam_name=' + LotBatchText;
								window.open(linkURL);
							}
						 */}
						else
						{
							var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');
							if(LotBatchText ==null || LotBatchText =='')
							{
								alert("Please enter Actual LOT#");
								nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
								return false;
							}
						}

					}
				}

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
				{
					if(InvtID != null && InvtID != "")
					{
						var fieldsPO = ['custrecord_ebiz_transaction_no'];
						var columnsPO = nlapiLookupField('customrecord_ebiznet_createinv', InvtID, fieldsPO);
						if(columnsPO != null && columnsPO != '')
						{
							if(columnsPO.custrecord_ebiz_transaction_no != null)
							{
								TransactionNo = columnsPO.custrecord_ebiz_transaction_no;
							}
						}
					}

					if(parseFloat(EnteredQty) != 0)
					{
						if(parseFloat(EnteredQty)>parseFloat(ActualQty))
						{
							var vfrmName = 'CyclecountRecord';
							var NewQty = parseFloat(EnteredQty) - parseFloat(ActualQty);



							//Case # 20126109 Start
							var filtersser1 = new Array();
							filtersser1[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itmid);
							filtersser1[1] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
							if(LP!=null && LP!='')
							filtersser1[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
							// case no 20126280

							
							var columnser1 = new Array();
							columnser1[0] = new nlobjSearchColumn('custrecord_serialnumber');

							var SrchRecordnew = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser1, columnser1);


							//if(SrchRecordnew!=null && SrchRecordnew!='' && parseFloat(SrchRecordnew.length)!=parseFloat(EnteredQty))
							if(SrchRecordnew==null || SrchRecordnew=='')
							{

								//Case # 20126109 End


								alert("Please enter value(s)for: Serial Items in Serial Entry");
								var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
								/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
								//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
								//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
								linkURL = linkURL + '&custparam_serialskuid=' + itmid;
								linkURL = linkURL + '&custparam_serialskulp=' + LP;
								linkURL = linkURL + '&custparam_serialpoid=' + TransactionNo;
								// case no 20126483
								if(EnteredQty!=null && EnteredQty!=''){
									linkURL = linkURL + '&custparam_serialskuchknqty=' + EnteredQty;
									}
									else{
									linkURL = linkURL + '&custparam_serialskuchknqty=' + ActualQty;
									}
								//linkURL = linkURL + '&custparam_serialskuchknqty=' + NewQty;
								//linkURL = linkURL + '&custparam_serialskuchknqty=' + EnteredQty;
								linkURL = linkURL + '&custparam_serialformname=' + vfrmName;
								linkURL = linkURL + '&custparam_seriallocation=' + Location;
								linkURL = linkURL + '&custparam_serialbinlocation=' + BinLocation;
								//linkURL = linkURL + '&custparam_lot=' + BatchNo;
								//linkURL = linkURL + '&custparam_name=' + BatchNo;
								//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
								//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
								window.open(linkURL);

							}
						}		
						else if(parseFloat(EnteredQty)<parseFloat(ActualQty))
						{
							alert("Please select required Serial numbers to adjust");
							//var ReqQty = parseFloat(EnteredQty);
							var ReqQty = EnteredQty;//parseFloat(ActualQty) - parseFloat(EnteredQty);
							var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');
							/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
							//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
							//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
							linkURL = linkURL + '&custparam_serialskuid=' + itmid;
							linkURL = linkURL + '&custparam_serialsku=' + item;
							linkURL = linkURL + '&custparam_serialskulp=' + LP;
							linkURL = linkURL + '&custparam_serialskuqty=' + ReqQty;	
							//linkURL = linkURL + '&custparam_lot=' + BatchNo;
							//linkURL = linkURL + '&custparam_name=' + BatchNo;
							//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
							//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
							window.open(linkURL);	
						}
					}	
				}		
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in ONChange client script for Serial Entry',exp);
	}

//	Check for LpRange and LPType
	try
	{
		if(name=='custpage_actlp')
		{
			var getActualLP = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actlp');
			//alert("getActualLP " + getActualLP);
			//case # 20127032 start, added if condition.
			if(getActualLP!=''){
				var siteId=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_siteid');
				nlapiLogExecution('ERROR', 'getsysItemLP userdefined',getActualLP);
				var LPReturnValue = ebiznet_LPRange_CL(getActualLP, '2',siteId,'1');//santosh 


				//	alert("LPReturnValue " + LPReturnValue);

				if (LPReturnValue == true)
				{
					var LpStatus=CheckLpMaster(getActualLP);

					if(LpStatus=='N')
					{
						return true;
					}
					else
					{
						alert("LP Already Exists");
						nlapiSetCurrentLineItemValue('custpage_items', 'custpage_actlp','',true,true);
						return false;
					}
				}
				else 
				{
					alert('LP# Out of Range');
					return false;
				}
			}
			return true;
		}
	}
	catch(exps)
	{
		nlapiLogExecution('ERROR','Exception in ONChange client script for LpRange',exps);
	}


//	actual Qty should not be greater than pallet Qty

	try
	{
		if(name=='custpage_actqty')
		{
			//alert(name);
			var getItemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actsku');
			var siteId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_siteid');
			var vactQty =  nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');

			// Case# 20148035 starts
			//var itmid = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actsku');	
			var expitmid = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_expsku1');	
			var item = nlapiGetCurrentLineItemText('custpage_items', 'custpage_actsku');
			var ActualQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_actualqty');
			//var EnteredQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
			//var QOH = nlapiGetCurrentLineItemValue('custpage_items','custpage_invadj_itemquantity');
			var LP = nlapiGetCurrentLineItemValue('custpage_items','custpage_actlp');
			var InvtID = nlapiGetCurrentLineItemValue('custpage_items','custpage_invtid');
			var TransactionNo = "";
			//var Location = nlapiGetCurrentLineItemValue('custpage_items','custpage_siteid');
			var BinLocation = nlapiGetCurrentLineItemValue('custpage_items','custpage_binlocationid');
			var batchflag="F";
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', getItemId, fields);
			var ItemType = columns.recordType;
			batchflag= columns.custitem_ebizbatchlot;
			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;
			if(vactQty ==null ||vactQty=='' || isNaN(vactQty) || vactQty<0)
			{
				alert('Please Enter Valid Actual Qty');
				nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
				return false;
			}

			var palletQuantity = 0;
			palletQuantity = fetchPalletQuantity(getItemId,siteId,null);
			//alert(palletQuantity);
			if((parseFloat(palletQuantity) != 0) && (parseFloat(vactQty) > parseFloat(palletQuantity)))
			{
				alert('Actual Qty Should not be Greater than Pallet Qty');
				return false;
			}
			else
			{
				return true;
			}
			if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
			{
				var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');
				var siteId=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_siteid');
				if(LotBatchText)
				{/*
					//alert(LotBatchText);
					var RecID=getLotBatchId(LotBatchText);
					//alert(RecID);
					if(RecID==null||RecID=="")
					{
						var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
						var ctx = nlapiGetContext();
						if (ctx.getEnvironment() == 'PRODUCTION') {
							linkURL = 'https://system.netsuite.com' + linkURL;			
						}
						else 
							if (ctx.getEnvironment() == 'SANDBOX') {
								linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
							}
						linkURL = linkURL + '&custparam_lot=' + LotBatchText;
						if(siteId!=null&&siteId!="")
							linkURL = linkURL + '&custparam_locationid='+siteId;
						linkURL = linkURL + '&custparam_serialskuid=' + itmid;
						linkURL = linkURL + '&custparam_name=' + LotBatchText;
						window.open(linkURL);
					}
				 */}
				else
				{
					var LotBatchText=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_actbatch');
					if(LotBatchText ==null || LotBatchText =='')
					{
						alert("Please enter Actual LOT#");
						nlapiSetCurrentLineItemValue('custpage_items','custpage_rec','F');
						return false;
					}
				}
			}
			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
			{
				if(InvtID != null && InvtID != "")
				{
					var fieldsPO = ['custrecord_ebiz_transaction_no'];
					var columnsPO = nlapiLookupField('customrecord_ebiznet_createinv', InvtID, fieldsPO);
					if(columnsPO != null && columnsPO != '')
					{
						if(columnsPO.custrecord_ebiz_transaction_no != null)
						{
							TransactionNo = columnsPO.custrecord_ebiz_transaction_no;
						}
					}
				}
				if(parseFloat(vactQty) != 0)
				{
					if(parseFloat(vactQty)>parseFloat(ActualQty))
					{
						var vfrmName = 'CyclecountRecord';
						var NewQty = parseFloat(vactQty) - parseFloat(ActualQty);
						//Case # 20126109 Start
						var filtersser1 = new Array();
						filtersser1[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getItemId);
						filtersser1[1] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
						if(LP!=null && LP!='')
						filtersser1[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
						// case no 20126280
						
						var columnser1 = new Array();
						columnser1[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var SrchRecordnew = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser1, columnser1);
						//if(SrchRecordnew!=null && SrchRecordnew!='' && parseFloat(SrchRecordnew.length)!=parseFloat(EnteredQty))
						if(SrchRecordnew==null || SrchRecordnew=='')
						{
							//Case # 20126109 End
							alert("Please enter value(s)for: Serial Items in Serial Entry");
							var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
							/*var ctx = nlapiGetContext();
						if (ctx.getEnvironment() == 'PRODUCTION') {
							linkURL = 'https://system.netsuite.com' + linkURL;			
						}
						else 
							if (ctx.getEnvironment() == 'SANDBOX') {
								linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
							}*/
							//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
							//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
							linkURL = linkURL + '&custparam_serialskuid=' + getItemId;
							linkURL = linkURL + '&custparam_serialskulp=' + LP;
							linkURL = linkURL + '&custparam_serialpoid=' + TransactionNo;
							// case no 20126483
							linkURL = linkURL + '&custparam_serialskuchknqty=' + NewQty;
							//linkURL = linkURL + '&custparam_serialskuchknqty=' + vactQty;
							linkURL = linkURL + '&custparam_serialformname=' + vfrmName;
							linkURL = linkURL + '&custparam_seriallocation=' + siteId;
							linkURL = linkURL + '&custparam_serialbinlocation=' + BinLocation;
							//linkURL = linkURL + '&custparam_lot=' + BatchNo;
							//linkURL = linkURL + '&custparam_name=' + BatchNo;
							//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
							//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
							window.open(linkURL);
						}
					}		
					else if(parseFloat(vactQty)<parseFloat(ActualQty))
					{
						alert("Please select required Serial numbers to adjust");
						//var ReqQty = parseFloat(EnteredQty);
						var ReqQty = vactQty;//parseFloat(ActualQty) - parseFloat(EnteredQty);
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');
						/*var ctx = nlapiGetContext();
						if (ctx.getEnvironment() == 'PRODUCTION') {
							linkURL = 'https://system.netsuite.com' + linkURL;			
						}
						else 
							if (ctx.getEnvironment() == 'SANDBOX') {
								linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
							}*/
						//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_polocationid',p);
						//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_pocompanyid',p);
						linkURL = linkURL + '&custparam_serialskuid=' + getItemId;
						linkURL = linkURL + '&custparam_serialsku=' + item;
						linkURL = linkURL + '&custparam_serialskulp=' + LP;
						linkURL = linkURL + '&custparam_serialskuqty=' + ReqQty;	
						//linkURL = linkURL + '&custparam_lot=' + BatchNo;
						//linkURL = linkURL + '&custparam_name=' + BatchNo;
						//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
						//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
						window.open(linkURL);	
					}
				}	
			}
			//  Case# 20148035 ends
		}
	}
	catch(exps)
	{
		nlapiLogExecution('ERROR','Exception in ONChange client script for LpRange',exps);
	}




	/*//	Check for act qty should not be less than allocatred qty
	try
	{
		if(name=='custpage_actqty')
		{//alert('HI');
			var vactQty =  nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
			var PlanNo =  nlapiGetCurrentLineItemValue('custpage_items','custpage_planno');
			var cycexeid =  nlapiGetCurrentLineItemValue('custpage_items','custpage_id');

			var invrecidArray=new Array;
			var cycleexerecidArray=new Array;
			var cycleexerecid = '';
			var invrecid='';
			var filters = new Array();

			filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',PlanNo));
			filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_invtid');     
			var List = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);

			if( List!=null && List!='')
			{ //alert(List.length);
				for (var k = 1; k <= List.length; k++) {
					//invrecid.push(List[k].getValue('custrecord_invtid'));
					//cycleexerecidArray.push(List[k].getId());

					cycleexerecid =  List[k].getId();
					if(cycexeid == cycleexerecid)
					{
						invrecid = List[k].getValue('custrecord_invtid')
						//alert('invrecid'+invrecid);	
						var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',invrecid);

						var allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
						//alert(allocQty);
						//alert(vactQty);
						if(parseInt(allocQty)>parseInt(vactQty))
						{
							alert("AllocQty is greater than ActualQty");
							return false;    		
						}  
					}
				}

			}


		}
	}
	catch(exps)
	{
		nlapiLogExecution('ERROR','Exception in ONChange client script for QTy',exps);
	}*/
}

/**
 * @param lotbatchtext
 * @returns {String}
 */
function getLotBatchId(lotbatchtext)
{
	var batchid='';
	var filters = new Array();          
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',lotbatchtext));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	var batchdetails = new nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
	{
		batchid=batchdetails[0].getValue('internalid');
	}
	return batchid;
}

/**
 * @param vLp
 * @returns
 */
function CheckLpMaster(vLp)
{
	try {
		//alert('HELLO');
		var vLpExists;
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', vLp);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('ERROR', 'LP FOUND');
			//alert('IF');
			vLpExists = 'Y';
		}
		else
		{

			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', vLp);//santosh
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', vLp);//santosh 
			var rec = nlapiSubmitRecord(customrecord, false, true);
			//alert('ELSE');
			vLpExists = 'N';
		}

		return vLpExists;
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}
}

function CheckForcycleCountStatus(cyclecountplan)
{
	try
	{
		var IsClosed='F';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('internalid',null,'is',cyclecountplan));
		filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_cyccplan_close');

		var search=nlapiSearchRecord('customrecord_ebiznet_cylc_createplan',null,filter,column);

		if(search!=null&&search!="")
		{
			IsClosed=search[0].getValue('custrecord_cyccplan_close');
		}
		nlapiLogExecution('ERROR','IsClosed',IsClosed);
		return IsClosed;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in check for cyclecount status',exp);
	}
}
function addNewRow()
{

	var totalLineCount=	nlapiGetLineItemCount('custpage_items');

	/*  var newline=parseInt(totalLineCount)+parseInt(1);
	alert(newline);
	nlapiInsertLineItem('custpage_items', newline);
	nlapiCommitLineItem('custpage_items');*/
	nlapiSetFieldValue('custpage_newrow','T');
	nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
	for(var k=1;k<=totalLineCount;k++)
	{
		nlapiSetLineItemValue('custpage_items', 'custpage_rec', k,'F');	
	}
	NLDoMainFormButtonAction("submitter",true);	

}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function BatchCustRecordChk(ChknBatchNo,item)
{
//	alert(ChknBatchNo);
	var batch=ChknBatchNo.split("(")[0];
//	alert(batch);
	var filtersbat = new Array();
	filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', batch);
	if(item !='' && item!='null' && item!=null) // Case# 20148594
	filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', item);

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
//	alert(SrchRecord);
	if (SrchRecord) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}
