/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Pack_ScanComplete.js,v $
 *     	   $Revision: 1.1.2.1.4.2.4.5 $
 *     	   $Date: 2014/06/13 12:59:20 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Pack_ScanComplete.js,v $
 * Revision 1.1.2.1.4.2.4.5  2014/06/13 12:59:20  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.4.2.4.4  2014/06/06 07:03:44  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.1.4.2.4.3  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.2.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.1.4.2.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.1.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.1  2012/06/15 10:08:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.5  2012/02/16 10:42:38  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.4  2011/08/24 12:41:49  schepuri
 * CASE201112/CR201113/LOG201121
 * RF PutAway complete based on putseq no
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PackComplete(request, response)
{
    if (request.getMethod() == 'GET') 
	{
		var orderno = request.getParameter('custparam_orderno');
		var getLanguage = request.getParameter('custparam_language');	
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "LISTA DE EMPAQUE IMPRESO CON &#201;XITO DE ESTA ORDEN #:";
			st2 = "PRESIONE F8 PARA NEW ORDER";
			st3 = "PR&#211;XIMO";
		}
		else
		{
			st0 = "";
			st1 = "PACKING LIST PRINTED SUCESSFULLY FOR THIS ORDER #:";
			st2 = "PRESS F8 FOR NEW ORDER";
			st3 = "NEXT";
			

		}
		
		 var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
       	var html = "<html><head><title>" + st0 + "</title>";
       	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
        html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
      //Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
        //html = html + " document.getElementById('cmdSend').focus();";        
        html = html + "</script>";
        html = html +functionkeyHtml;
		 html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
        html = html + "	<form name='_rf_putaway_lp' method='POST'>";
        html = html + "		<table>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st1 +" "+orderno+"  </td></tr>";
        html = html + "				<td align = 'left'>" + st2;			
        html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' id='cmdSend' type='submit' value='F8'/>";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "		 </table>";
        html = html + "	</form>";
      //Case# 20148882 (added Focus Functionality for Textbox)
        html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
        html = html + "</body>";
        html = html + "</html>";
        
        response.write(html);
    }
    else 
	{
        nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
        
		
        var optedEvent = request.getParameter('cmdSend');
        
        var getLanguage = request.getParameter('hdngetLanguage');
		//loadtrlrarray["custparam_language"] = getLanguage;
        if (optedEvent == 'F8') 
		{
        	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanorder', 'customdeploy_ebiz_rf_pack_scanorder_di', false, optedEvent);
        	
            
			
        }
        nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}