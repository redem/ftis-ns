/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Schedulers/Attic/ebiz_transactionmetrics.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2014/02/05 13:36:24 $
 *     	   $Author: grao $
 *     	   $Name: t_NSWMS_2014_1_2_0 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_transactionmetrics.js,v $
 * Revision 1.1.2.1  2014/02/05 13:36:24  grao
 * Case# 20126629
 * Scheduler to capture outbound and inventory statistics.
 *
 *
 **************************************************************************************************/

function OutboundMetrics(type)
{
	nlapiLogExecution('DEBUG', 'Into OutboundMetrics', type);

	var vToDay = DateStamp();
	var vLocArr = new Array();

	//vToDay = '1/21/2014';

	nlapiLogExecution('DEBUG', 'vToDay', vToDay);

	var filters = new Array();

	filters.push(new nlobjSearchFilter('trandate', null, 'on', vToDay));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid',null,'group');	
	columns[1] = new nlobjSearchColumn('line',null,'count');
	columns[2] = new nlobjSearchColumn('location',null,'group').setSort();

	var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);

	if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
	{
		for (var i = 0; i < salesOrderLineDetails.length; i++)
		{
			var vLocation = salesOrderLineDetails[i].getValue('location',null,'group');
			vLocArr.push(vLocation);
		}

		if(vLocArr!=null && vLocArr!='')
		{
			vLocArr = removeDuplicateElement(vLocArr);
		}

		for (var j = 0; j < vLocArr.length; j++)
		{
			var vLoc = vLocArr[j];

			nlapiLogExecution('DEBUG', 'vLoc', vLoc);

			if(vLoc!=null && vLoc!='')
			{
				var vNoOfSOs = 0;
				var vNoOfLines = 0;
				var vNoOfWHOrders = 0;
				var vNoOfOrdsForWave = 0;
				var vNoOfLinesForWave = 0;
				var vNoOfWavesBuilt = 0;
				var vNoOfOrdsFailedPicks = 0;
				var vNoOfLinesFailedPicks = 0;
				var vNoOfPicksProcessed = 0;
				var vNoOfOrdsPicking = 0;
				var vNoOfLinesPicking = 0;
				var vNoOfOrdsPacking = 0;
				var vNoOfLinesPacking = 0;
				var vNoOfOrdsShipped = 0;
				var vNoOfLinesShipped = 0;
				var vNoOfShipUnits = 0;
				var vNoOfCartons = 0;
				var vNoOfTrailers = 0;
				var vNoOfTrackingNos = 0;


				for (var k = 0; k < salesOrderLineDetails.length; k++)
				{
					var vSOLoc = salesOrderLineDetails[k].getValue('location',null,'group');

					if(vLoc==vSOLoc)
					{
						vNoOfSOs = parseInt(vNoOfSOs)+1;
						vNoOfLines = parseInt(vNoOfLines)+parseInt(salesOrderLineDetails[k].getValue('line',null,'count'));
					}
				}

				nlapiLogExecution('DEBUG', 'vNoOfSOs', vNoOfSOs);
				nlapiLogExecution('DEBUG', 'vNoOfLines', vNoOfLines);

				//**************************** # OF WAREHOUSE ORDERS START **********************************************//

				var filters = new Array();

				filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', vToDay));
				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vLoc));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('internalid','custrecord_ns_ord','count');
				columns[1] = new nlobjSearchColumn('custrecord_ordline_wms_location',null,'group').setSort();

				var FOLineDetails = new nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

				if(FOLineDetails!=null && FOLineDetails!='' && FOLineDetails.length>0)
				{
					vNoOfWHOrders=parseInt(FOLineDetails[0].getValue('internalid','custrecord_ns_ord','count'));
				}

				nlapiLogExecution('DEBUG', 'vNoOfWHOrders', vNoOfWHOrders);


				//*********************************** # OF WAREHOUSE ORDERS END ********************************************//

				//**************************** # OF ORDERS READY TO WAVE START *******************************************//

				var filters = new Array();

				filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', vToDay));
				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vLoc));
				filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25]));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ordline_wms_location',null,'group').setSort();
				columns[1] = new nlobjSearchColumn('internalid','custrecord_ns_ord','group');
				columns[2] = new nlobjSearchColumn('custrecord_ordline',null,'count');

				var openFOLineDetails = new nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

				if(openFOLineDetails!=null && openFOLineDetails!='' && openFOLineDetails.length>0)
				{
					for (var l = 0; l < openFOLineDetails.length; l++)
					{
						vNoOfOrdsForWave = parseInt(vNoOfOrdsForWave)+1;
						vNoOfLinesForWave = parseInt(vNoOfLinesForWave)+parseInt(openFOLineDetails[l].getValue('custrecord_ordline',null,'count'));
					}				
				}

				nlapiLogExecution('DEBUG', 'vNoOfOrdsForWave', vNoOfOrdsForWave);
				nlapiLogExecution('DEBUG', 'vNoOfLinesForWave', vNoOfLinesForWave);

				//************************************# OF ORDERS READY TO WAVE END *****************************************//

				//************************************ # OF WAVES BUILT START *******************************************//

				vNoOfWavesBuilt = getNoOfWavesBuilt(vLoc,vToDay);

				nlapiLogExecution('DEBUG', 'vNoOfWavesBuilt', vNoOfWavesBuilt);

				//***************************************** # OF WAVES BUILT END **************************************//

				//************************************ # OF ORDERS FAILED PICKS START *******************************************//

				var failedpicksDet = getNoOfFailedPicks(vLoc,vToDay);

				if(failedpicksDet!=null && failedpicksDet!='' && failedpicksDet.length>0)
				{
					for (var n = 0; n < failedpicksDet.length; n++)
					{
						vNoOfOrdsFailedPicks = parseInt(vNoOfOrdsFailedPicks)+1;
						vNoOfLinesFailedPicks = parseInt(vNoOfOrdsFailedPicks)+parseInt(failedpicksDet[n].getValue('custrecord_line_no',null,'count'));
					}				
				}

				nlapiLogExecution('DEBUG', 'vNoOfOrdsFailedPicks', vNoOfOrdsFailedPicks);
				nlapiLogExecution('DEBUG', 'vNoOfLinesFailedPicks', vNoOfLinesFailedPicks);

				//************************************ # OF ORDERS FAILED PICKS END *******************************************//

				vNoOfPicksProcessed = getNoOfPicksProcessed(vLoc,vToDay);

				nlapiLogExecution('DEBUG', 'vNoOfPicksProcessed', vNoOfPicksProcessed);

				//************************************ # OF ORDERS IN PICKING START *******************************************//

				var picktaskDet = getNoOfOrdersInPicking(vLoc,vToDay);

				if(picktaskDet!=null && picktaskDet!='' && picktaskDet.length>0)
				{
					for (var p = 0; p < picktaskDet.length; p++)
					{
						vNoOfOrdsPicking = parseInt(vNoOfOrdsPicking)+1;
						vNoOfLinesPicking = parseInt(vNoOfLinesPicking)+parseInt(picktaskDet[p].getValue('custrecord_line_no',null,'count'));
					}				
				}

				nlapiLogExecution('DEBUG', 'vNoOfOrdsPicking', vNoOfOrdsPicking);
				nlapiLogExecution('DEBUG', 'vNoOfLinesPicking', vNoOfLinesPicking);

				//************************************ # OF ORDERS IN PICKING END *******************************************//

				//************************************ # OF ORDERS IN PACKING START *******************************************//

				var packtaskDet = getNoOfOrdersInPacking(vLoc,vToDay);

				if(packtaskDet!=null && packtaskDet!='' && packtaskDet.length>0)
				{
					for (var q = 0; q < packtaskDet.length; q++)
					{
						vNoOfOrdsPacking = parseInt(vNoOfOrdsPacking)+1;
						vNoOfLinesPacking = parseInt(vNoOfLinesPacking)+parseInt(packtaskDet[q].getValue('custrecord_line_no',null,'count'));
					}				
				}

				nlapiLogExecution('DEBUG', 'vNoOfOrdsPacking', vNoOfOrdsPacking);
				nlapiLogExecution('DEBUG', 'vNoOfLinesPacking', vNoOfLinesPacking);

				//************************************ # OF ORDERS IN PACKING END *******************************************//

				//************************************ # OF ORDERS SHIPPED START *******************************************//

				var shiptaskDet = getNoOfOrdersShipped(vLoc,vToDay);

				if(shiptaskDet!=null && shiptaskDet!='' && shiptaskDet.length>0)
				{
					for (var r = 0; r < shiptaskDet.length; r++)
					{
						vNoOfOrdsShipped = parseInt(vNoOfOrdsShipped)+1;
						vNoOfLinesShipped = parseInt(vNoOfLinesShipped)+parseInt(shiptaskDet[r].getValue('custrecord_ordline',null,'count'));
					}				
				}

				nlapiLogExecution('DEBUG', 'vNoOfOrdsShipped', vNoOfOrdsShipped);
				nlapiLogExecution('DEBUG', 'vNoOfLinesShipped', vNoOfLinesShipped);

				//************************************ # OF ORDERS SHIPPED END *******************************************//

				vNoOfShipUnits = getNoOfShipUnits(vLoc,vToDay);
				nlapiLogExecution('DEBUG', 'vNoOfShipUnits', vNoOfShipUnits);

				vNoOfCartons = getNoOfCartons(vLoc,vToDay);
				nlapiLogExecution('DEBUG', 'vNoOfCartons', vNoOfCartons);

				vNoOfTrailers = getNoOfTrailers(vLoc,vToDay);
				nlapiLogExecution('DEBUG', 'vNoOfTrailers', vNoOfTrailers);

				vNoOfTrackingNos = getNoOfTrackingNumbers(vLoc,vToDay);
				nlapiLogExecution('DEBUG', 'vNoOfTrackingNos', vNoOfTrackingNos);


				//******************************************* Insert Metrics ********************************************//

				var ebizstatistics = nlapiCreateRecord('customrecord_ebiz_transaction_stats');	

				ebizstatistics.setFieldValue('name', vToDay); 
				ebizstatistics.setFieldValue('custrecord_ebiz_tranlocation', vLoc); 
				ebizstatistics.setFieldValue('custrecord_ebiz_processdate', vToDay); 
				ebizstatistics.setFieldValue('custrecord_ebiz_transactiontype', 'SO');			
				ebizstatistics.setFieldValue('custrecord_ebiz_noofsos', vNoOfSOs);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofsolines', vNoOfLines);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofwarehouseorders', vNoOfWHOrders);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofordersshipped', vNoOfOrdsShipped);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofordlineshipped', vNoOfLinesShipped);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofordersatpackstn', vNoOfOrdsPacking);
				ebizstatistics.setFieldValue('custrecord_ebiz_nooforderlinesatpackstn', vNoOfLinesPacking);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofordersinpicking', vNoOfOrdsPicking);
				ebizstatistics.setFieldValue('custrecord_ebiz_orderlinesinpicking', vNoOfLinesPicking);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofordersreadytowave', vNoOfOrdsForWave);
				ebizstatistics.setFieldValue('custrecord_ebiz_nooflinesreadytowave', vNoOfLinesForWave);
				ebizstatistics.setFieldValue('custrecord_ebiz_nooforderspicksfailed', vNoOfOrdsFailedPicks);
				ebizstatistics.setFieldValue('custrecord_ebiz_nooflinespicksfailed', vNoOfLinesFailedPicks);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofwavesbuilt', vNoOfWavesBuilt);
				ebizstatistics.setFieldValue('custrecord_ebiz_noofpicks', vNoOfPicksProcessed);			
				ebizstatistics.setFieldValue('custrecord_ebiz_noofcartonsbuilt', vNoOfCartons);			
				ebizstatistics.setFieldValue('custrecord_ebiz_noofshipunits', vNoOfShipUnits);
				ebizstatistics.setFieldValue('custrecord_ebiz_nooftrailers', vNoOfTrailers);
				ebizstatistics.setFieldValue('custrecord_ebiz_nooftrackingnumbers', vNoOfTrackingNos);

				var recid = nlapiSubmitRecord(ebizstatistics);

				//******************************************* Insert Metrics *********************************************//

				//******************************************* Inventory Adjustments *************************************//
			}
		}

	}

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_recorddate', null, 'on', vToDay));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_siteid',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_tasktype',null,'group');

	var invadjDetails = new nlapiSearchRecord('customrecord_ebiznet_invadj', null, filters, columns);
	if(invadjDetails!=null && invadjDetails!='')
	{
		for (var s = 0; s < invadjDetails.length; s++)
		{
			var vAdjLoc = invadjDetails[s].getValue('custrecord_ebiz_siteid',null,'group');
			var vAdjType = invadjDetails[s].getValue('custrecord_ebiz_tasktype',null,'group');
			var vAdjTypeName = invadjDetails[s].getText('custrecord_ebiz_tasktype',null,'group');

			var adjItemarr = new Array();
			var adjBinLocarr = new Array();
			var vQtyAdjusted = 0;
			var vNoOfAdustments = 0;
			var vNoOfItemsAdjusted = 0;
			var vNoOfBinLocsAdjusted = 0;

			var adjfilters = new Array();

			adjfilters.push(new nlobjSearchFilter('custrecord_ebiz_recorddate', null, 'on', vToDay));
			adjfilters.push(new nlobjSearchFilter('custrecord_ebiz_siteid', null, 'anyof', vAdjLoc));
			adjfilters.push(new nlobjSearchFilter('custrecord_ebiz_tasktype', null, 'anyof', vAdjType));

			var adjcolumns = new Array();
			adjcolumns[0] = new nlobjSearchColumn('custrecord_ebizskuno').setSort();
			adjcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_binloc');
			adjcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_adjustqty');

			var adjDetails = new nlapiSearchRecord('customrecord_ebiznet_invadj', null, adjfilters, adjcolumns);
			if(adjDetails!=null && adjDetails!='')
			{
				for (var t = 0; t < adjDetails.length; t++)
				{
					adjItemarr.push(adjDetails[t].getValue('custrecord_ebizskuno'));
					adjBinLocarr.push(adjDetails[t].getValue('custrecord_ebiz_binloc'));

					vQtyAdjusted = parseFloat(vQtyAdjusted)+parseFloat(adjDetails[t].getValue('custrecord_ebiz_adjustqty'));
				}

				adjItemarr = removeDuplicateElement(adjItemarr);
				adjBinLocarr = removeDuplicateElement(adjBinLocarr);

				vNoOfAdustments = adjDetails.length;
				vNoOfItemsAdjusted = adjItemarr.length;
				vNoOfBinLocsAdjusted = adjBinLocarr.length;
			}

			var invstatistics = nlapiCreateRecord('customrecord_ebiz_transaction_stats');	

			invstatistics.setFieldValue('name', vToDay); 
			invstatistics.setFieldValue('custrecord_ebiz_tranlocation', vAdjLoc); 
			invstatistics.setFieldValue('custrecord_ebiz_processdate', vToDay); 
			invstatistics.setFieldValue('custrecord_ebiz_transactiontype', 'INV');						
			invstatistics.setFieldValue('custrecord_ebiz_adustmenttype', vAdjTypeName);
			invstatistics.setFieldValue('custrecord_ebiz_noofadjustments', vNoOfAdustments);
			invstatistics.setFieldValue('custrecord_ebiz_noofitemsadjusted', vNoOfItemsAdjusted);
			invstatistics.setFieldValue('custrecord_ebiz_noofbinlocsadjusted', vNoOfBinLocsAdjusted);
			invstatistics.setFieldValue('custrecord_ebiz_totalqtyadjusted', vQtyAdjusted);

			var invrecid = nlapiSubmitRecord(invstatistics);						
		}
	}

	//******************************************* Inventory Adjustments *************************************//



	nlapiLogExecution('DEBUG', 'Out of OutboundMetrics');
}

function getNoOfWavesBuilt(vWhloc,vDate)
{

	var vNoOfWavesBuilt = 0;
	var vWavesArr = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');

	var waveDetails1 = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(waveDetails1!=null && waveDetails1!='' && waveDetails1.length>0)
	{
		for (var m = 0; m < waveDetails1.length; m++)
		{
			vWavesArr.push(waveDetails1[m].getValue('custrecord_ebiz_wave_no',null,'group'));
		}
	}

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_wave_no', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',[3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiztask_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_wave_no',null,'group');

	var waveDetails2 = new nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);
	if(waveDetails2!=null && waveDetails2!='' && waveDetails2.length>0)
	{
		for (var n = 0; n < waveDetails2.length; n++)
		{
			vWavesArr.push(waveDetails2[n].getValue('custrecord_ebiztask_ebiz_wave_no',null,'group'));
		}
	}

	if(vWavesArr!=null && vWavesArr!='')
	{
		vWavesArr = removeDuplicateElement(vWavesArr);

		vNoOfWavesBuilt = vWavesArr.length;
	}

	return vNoOfWavesBuilt;
}

function getNoOfFailedPicks(vWhloc,vDate)
{		
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[26]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group');
	columns[2] = new nlobjSearchColumn('custrecord_line_no',null,'count');

	var openPickDet = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return openPickDet;
}

function getNoOfPicksProcessed(vWhloc,vDate)
{
	var vNoOfPicksProcessed = 0;

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[8,14,28]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_wave_no');

	var openTaskDet1 = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openTaskDet1!=null && openTaskDet1!='' && openTaskDet1.length>0)
	{
		vNoOfPicksProcessed = openTaskDet1.length;
	}

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof',[8,14,28]));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',[3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiztask_wms_location').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_wave_no');

	var openTaskDet2 = new nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);
	if(openTaskDet2!=null && openTaskDet2!='' && openTaskDet2.length>0)
	{
		vNoOfPicksProcessed = parseInt(vNoOfPicksProcessed) + openTaskDet2.length;
	}

	return vNoOfPicksProcessed;
}

function getNoOfOrdersInPicking(vWhloc,vDate)
{
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[8]));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group');
	columns[2] = new nlobjSearchColumn('custrecord_line_no',null,'count');

	var openPickDet = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return openPickDet;
}

function getNoOfOrdersInPacking(vWhloc,vDate)
{
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[28]));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group');
	columns[2] = new nlobjSearchColumn('custrecord_line_no',null,'count');

	var openPickDet = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return openPickDet;
}

function getNoOfOrdersShipped(vWhloc,vDate)
{
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [13,14]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ordline_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('internalid','custrecord_ns_ord','group');
	columns[2] = new nlobjSearchColumn('custrecord_ordline',null,'count');

	var FOLineDetails = new nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	return FOLineDetails;
}

function getNoOfShipUnits(vWhloc,vDate)
{
	var vNoOfShipUnits = 0;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[4]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_line_no');

	var openPickDet = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openPickDet!=null && openPickDet!='' && openPickDet.length>0 )
	{
		vNoOfShipUnits = openPickDet.length;
	}

	return vNoOfShipUnits;
}

function getNoOfCartons(vWhloc,vDate)
{
	var vCartonsArr = new Array();
	var vNoOfCartonsBuilt = 0;

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3]));
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isnotempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');

	var waveDetails1 = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(waveDetails1!=null && waveDetails1!='' && waveDetails1.length>0)
	{
		for (var m = 0; m < waveDetails1.length; m++)
		{
			vCartonsArr.push(waveDetails1[m].getValue('custrecord_container_lp_no',null,'group'));
		}
	}

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', vWhloc));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',[3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiztask_wms_location',null,'group').setSort();
	columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no',null,'group');

	var waveDetails2 = new nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);
	if(waveDetails2!=null && waveDetails2!='' && waveDetails2.length>0)
	{
		for (var n = 0; n < waveDetails2.length; n++)
		{
			vCartonsArr.push(waveDetails2[n].getValue('custrecord_ebiztask_ebiz_contlp_no',null,'group'));
		}
	}

	if(vCartonsArr!=null && vCartonsArr!='')
	{
		vCartonsArr = removeDuplicateElement(vCartonsArr);

		vNoOfCartonsBuilt = vCartonsArr.length;
	}

	return vNoOfCartonsBuilt;
}

function getNoOfTrailers(vWhloc,vDate)
{
	var vNoOfTrailers = 0;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_createdate', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitetrailer', null, 'anyof', vWhloc));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name').setSort();

	var trailerDet = new nlapiSearchRecord('customrecord_ebiznet_trailer', null, filters, columns);
	if(trailerDet!=null && trailerDet!='' && trailerDet.length>0 )
	{
		vNoOfTrailers = trailerDet.length;
	}

	return vNoOfTrailers;
}

function getNoOfTrackingNumbers(vWhloc,vDate)
{
	var vNoOfTrackingNumbers = 0;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_shippeddate', null, 'on', vDate));
	filters.push(new nlobjSearchFilter('custrecord_ship_location', null, 'is', vWhloc));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name').setSort();

	var shipmanifestDet = new nlapiSearchRecord('customrecord_ship_manifest', null, filters, columns);
	if(shipmanifestDet!=null && shipmanifestDet!='' && shipmanifestDet.length>0 )
	{
		vNoOfTrackingNumbers = shipmanifestDet.length;
	}

	return vNoOfTrackingNumbers;
}