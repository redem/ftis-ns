/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInCriteriaMenu.js,v $
 *     	   $Revision: 1.8.2.6.4.7.4.6 $
 *     	   $Date: 2014/06/13 07:25:15 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInCriteriaMenu.js,v $
 * Revision 1.8.2.6.4.7.4.6  2014/06/13 07:25:15  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.8.2.6.4.7.4.5  2014/06/04 14:46:46  rmukkera
 * Case # 20148705
 *
 * Revision 1.8.2.6.4.7.4.4  2014/05/30 00:26:48  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.8.2.6.4.7.4.3  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.8.2.6.4.7.4.2  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.8.2.6.4.7.4.1  2013/04/10 15:48:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.8.2.6.4.7  2012/12/24 15:19:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.8.2.6.4.6  2012/09/27 10:56:55  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.8.2.6.4.5  2012/09/26 12:47:07  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.8.2.6.4.4  2012/09/25 12:22:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.8.2.6.4.3  2012/09/25 11:57:32  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.8.2.6.4.2  2012/09/25 11:52:00  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.8.2.6.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.8.2.6  2012/05/29 23:41:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * trantype issue
 *
 * Revision 1.8.2.5  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.8.2.4  2012/05/09 15:56:42  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing Trantype in query string was missing
 *
 * Revision 1.8.2.3  2012/04/20 10:52:55  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.8.2.2  2012/02/22 12:19:09  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.8.2.1  2012/02/13 13:50:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Added BaseUomQty parameter to the query string.
 *
 * Revision 1.8  2011/12/08 16:34:52  gkalla
 * CASE201112/CR201113/LOG201121
 * not to transfer by default  to Transfer order
 *
 * Revision 1.7  2011/12/08 15:51:38  gkalla
 * CASE201112/CR201113/LOG201121
 * not to transfer by default  to Transfer order
 *
 * Revision 1.5  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.4  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function CheckinCriteriaMenu(request, response){
    if (request.getMethod() == 'GET') 
	{		
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		
		var st0,st1,st2,st3,st4,st5,st6,st7;
		
		if( getLanguage == 'es_ES')
		{
			st0 = "CHECK-IN CRITERIOS";
			st1 = "NUEVA ORDEN DE COMPRA / RECIBO"; 
			st2 = "NUEVO ART&#205;CULO";
			st3 = "NUEVO NO. DE LOTE";
			st4 = "NUEVA CANTIDAD";
			st5 = "REGISTRO DE SALIDA";
			st6 = "INGRESAR SELECCI&#211;N";
			st7 = "CONTINUAR";			
			
		}
		else
		{
			st0 = "CHECK-IN CRITERIA";
			st1 = "NEW PO/RECEIPT"; 
			st2 = "NEW ITEM";
			st3 = "NEW LOT#";
			st4 = "NEW QTY";
			st5 = "EXIT CHECK-IN";
			st6 = "ENTER SELECTION";
			st7 = "CONTINUE";
		
		}
		
		 var functionkeyHtml=getFunctionkeyScript('_rfcheckincriteriamenu'); 
        var html = "<html><head><title>" + st0 + "</title>";
        html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		 html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
        html = html + "	<form name='_rfcheckincriteriamenu' method='POST'>";
        html = html + "		<table>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'> 1. " + st1;
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'> 2. " + st2;
        html = html + "				</td>";
        html = html + "			</tr> ";
	/*	html = html + "			<tr>";
        html = html + "				<td align = 'left'> 3. NEW LOT#";
        html = html + "				</td>";
        html = html + "			</tr> ";*/
		html = html + "			<tr>";
        html = html + "				<td align = 'left'> 3. " + st4;
        html = html + "				</td>";
        html = html + "			</tr> ";
        html = html + "				<td align = 'left'> 4. " + st5;
        html = html + "				</td>";
        html = html + "			</tr> ";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st6;
        html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">"; 
        html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" +  st7 + " <input name='cmdSend' type='submit' value='ENT'/>";
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "		 </table>";
        html = html + "	</form>";
      //Case# 20148882 (added Focus Functionality for Textbox)
        html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
        html = html + "</body>";
        html = html + "</html>";
        
        response.write(html);
    }
    else {
        nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
        
		var optedField = request.getParameter('selectoption');
		
		// This variable is to hold the Quantity entered.
        var POarray = new Array();
        
        
        var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
		
		
		var st8,st9;
		if( getLanguage == 'es_ES')
		{
			st8 = "OPCI&#211;N V&#193;LIDA";
			st9 = "CheckinMenu";
			
		}
		else
		{
			st8 = "INVALID OPTION";
			st9 = "CheckinMenu";
		}
		
        
        
        
        
        
		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_error"] = st8;
		POarray["custparam_screenno"] = st9;
		//	Get the PO# from the previous screen, which is passed as a parameter		
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
        POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
        POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
        POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
        POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
        POarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
        POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		
		nlapiLogExecution('DEBUG', 'request.getParameter(custparam_poid)', request.getParameter('custparam_poid'));
		nlapiLogExecution('DEBUG', 'request.getParameter(custparam_pointernalid)', request.getParameter('custparam_pointernalid'));
		nlapiLogExecution('DEBUG', 'trantype', request.getParameter('custparam_trantype'));
		var trantype = nlapiLookupField('transaction', request.getParameter('custparam_pointernalid'), 'recordType');

        var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();
		
		nlapiLogExecution('DEBUG', 'ActualBeginDate', ActualBeginDate);
		nlapiLogExecution('DEBUG', 'ActualBeginTime', ActualBeginTime);
		
		POarray["custparam_actualbegindate"] = ActualBeginDate;
		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');
				
		POarray["custparam_actualbegintime"] = TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = TimeArray[1];
		
		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);
		
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
        POarray["custparam_itemquantity"] = request.getParameter('hdnQuantity');
        POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
        POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
        POarray["custparam_trantype"] = trantype;
        
        //code added on 13 feb 2012 by suman
		//To Pass the baseuom qty to query string.
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		//end of code as of 13 feb 2012.
				
		nlapiLogExecution('DEBUG', 'POarray["custparam_itemcube"]', POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_baseuomqty"]', POarray["custparam_baseuomqty"]);
		
        if(trantype=='purchaseorder')
        {

        	if (optedField == '1')
        	{
        		response.sendRedirect('SUITELET', 'customscript_rf_checkin_po', 'customdeploy_rf_checkin_po_di', false,POarray);
        	}
        	else if (optedField == '2') 
        	{
        		response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false,POarray);
        	}
        	/*else if (optedField == '3') 
        	{
        		//response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false,optedField);
        	}*/
        	else if (optedField == '3') 
        	{
        		 POarray["custparam_poqtyentered"] ="";
        		response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false,POarray);
        	}
        	else if (optedField == '4') 
        	{
        		response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
        	}
        	else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}
        }
        else if(trantype=='returnauthorization')
        {
        	if (optedField == '1')
    		{
    			response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin', 'customdeploy_rf_rma_checkin_di', false,POarray);
    		}
    		else if (optedField == '2') 
    		{
    			response.sendRedirect('SUITELET', 'customscript_rf_rmacheckin_sku', 'customdeploy_rf_rmacheckin_sku_di', false,POarray);
    		}
    		/*else if (optedField == '3') 
    		{
    			//response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false,optedField);
    		}*/
    		else if (optedField == '3') 
    		{
    			POarray["custparam_poqtyentered"] ="";
    			response.sendRedirect('SUITELET', 'customscript_rf_rma_checkinqty', 'customdeploy_rf_rmacheckin_qty_di', false,POarray);
    		}
    		else if (optedField == '4') 
    		{
    			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
    		}
    		else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}
        		
       	}
        else
        {
        	if (optedField == '1')
    		{
    			response.sendRedirect('SUITELET', 'customscript_rf_to_checkin', 'customdeploy_rf_to_checkin_di', false,POarray);
    		}
    		else if (optedField == '2') 
    		{
    			response.sendRedirect('SUITELET', 'customscript_rf_tocheckin_sku', 'customdeploy_rf_tocheckin_sku_di', false,POarray);
    		}
    		/*else if (optedField == '3') 
    		{
    			//response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false,optedField);
    		}*/
    		else if (optedField == '3') 
    		{
    			POarray["custparam_poqtyentered"] ="";
    			response.sendRedirect('SUITELET', 'customscript_rf_to_checkinqty', 'customdeploy_rf_tocheckin_qty_di', false,POarray);
    		}
    		else if (optedField == '4') 
    		{
    			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false,POarray);
    		}
    		else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}
        }
		
        nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
    }
}
