


/***************************************************************************
                                                                  eBizNET
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_CycleCountGenerateRelease_SL.js,v $
*  $Revision: 1.5.2.2.4.1.2.2.4.1 $
*  $Date: 2015/11/03 12:49:44 $
*  $Author: schepuri $
*  $Name: t_WMS_2015_2_StdBundle_1_66 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_CycleCountGenerateRelease_SL.js,v $
*  Revision 1.5.2.2.4.1.2.2.4.1  2015/11/03 12:49:44  schepuri
*  case# 201415339
*
*  Revision 1.5.2.2.4.1.2.2  2014/06/06 15:02:12  skavuri
*  Case# 20148794 SB Issue Fixed
*
*  Revision 1.5.2.2.4.1.2.1  2013/03/05 13:35:46  rmukkera
*  Merging of lexjet Bundle files to Standard bundle
*
*
****************************************************************************/






function CycleCountQBGenerate(request, response)
{
	 if (request.getMethod() == 'GET') 
	 {		
			var form = nlapiCreateForm('Generate & Release');		
//			var varCycleCountPlanNo =  form.addField('custpage_cyclecountplanno','select', 'Cycle Count Plan No','customrecord_ebiznet_cylc_createplan');
			//varCycleCountPlanNo.setSort(true);


			var varCycleCountPlanNo =  form.addField('custpage_cyclecountplanno','select', 'Cycle Count Plan No');
			/*var filtersitem = new Array();
			filtersitem.push(new nlobjSearchFilter('custrecord_cyccplan_close',null,'is','F'));
			filtersitem.push(new nlobjSearchFilter('isinactive',null,'is','F'));// Case# 20148794
			//custrecord_ebiz_cyclecntplan_location
			var vRoleLocation=getRoledBasedLocation();
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				filtersitem.push(new nlobjSearchFilter('custrecord_ebiz_cyclecntplan_location', null, 'is', vRoleLocation));
			}
			
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_cylc_createplan', null, filtersitem,null);*/
			//varCycleCountPlanNo.addSelectOption(null,'Select');
			
			PlanNoField(form,varCycleCountPlanNo, -1);
			
		
			
			/*if(searchresults!=null&&searchresults!="")
			{
				nlapiLogExecution('DEBUG', 'SO count', searchresults.length);
				//for (var i = searchresults.length; i > Math.min(500,0); i--)
				for (var i = searchresults.length; i > 0; i--)
				{	
					varCycleCountPlanNo.addSelectOption(searchresults[i-1].getId(),searchresults[i-1].getId());				
				}
			}
*/
			form.addSubmitButton('Display');
			response.writePage(form);

	 }
	 else
	 {	

		var vargetplan =  request.getParameter('custpage_cyclecountplanno');
		var CycleCountarray = new Array();
        CycleCountarray ["custpage_ccount"] = vargetplan;
		response.sendRedirect('SUITELET', 'customscript_cycc_generate_main', 'customdeploy_cycc_generate_main_di', false, CycleCountarray);

			
	 }
}

// case# 201415339
function PlanNoField(form,varCycleCountPlanNo, maxno){
	
	
	//var varCycleCountPlanNo =  form.addField('custpage_cyclecountplanno','select', 'Cycle Count Plan No');
	
	var filtersitem = new Array();
	filtersitem.push(new nlobjSearchFilter('custrecord_cyccplan_close',null,'is','F'));
	filtersitem.push(new nlobjSearchFilter('isinactive',null,'is','F'));// Case# 20148794
	//custrecord_ebiz_cyclecntplan_location
	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation); 
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersitem.push(new nlobjSearchFilter('custrecord_ebiz_cyclecntplan_location', null, 'is', vRoleLocation));
	}
	nlapiLogExecution('ERROR', 'maxno', maxno); 
	if(maxno!=-1)
	{
		//filtersitem.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
		filtersitem.push(new nlobjSearchFilter('id', null, 'greaterthan', parseInt(maxno)));
	}
	
	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_cylc_createplan', null, filtersitem,null);
	
	varCycleCountPlanNo.addSelectOption(null,'Select');
	//for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {
	for (var i = customerSearchResults.length;customerSearchResults != null && i > 0; i--){

		
		if(customerSearchResults[i-1].getId() != null && customerSearchResults[i-1].getId() != "" && customerSearchResults[i-1].getId() != " ")
		{
			var resdo = form.getField('custpage_cyclecountplanno').getSelectOptions(customerSearchResults[i-1].getId(), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		//varCycleCountPlanNo.addSelectOption(customerSearchResults[i].getId(), customerSearchResults[i].getId());
		varCycleCountPlanNo.addSelectOption(customerSearchResults[i-1].getId(),customerSearchResults[i-1].getId());	
	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		nlapiLogExecution('DEBUG', 'SO count', customerSearchResults.length);
		//var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		//fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno);
		var maxno=customerSearchResults[customerSearchResults.length-1].getId();	
		PlanNoField(form,varCycleCountPlanNo, maxno);
	}
	
		
	
}
