/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_InventorySearch.js,v $
 *     	   $Revision: 1.1.2.1.2.5.4.1 $
 *     	   $Date: 2015/11/09 23:27:48 $
 *     	   $Author: nneelam $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_103 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 */



function InventorySearch(request, response){
	if (request.getMethod() == 'GET') {
		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_main');
		var html = "<html><head><title>INVENTORY SEARCH</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_main' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER"; //ANY/ALL";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOCATION";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LP";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ITEM";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			</tr>";
		html = html + "				<tr><td align = 'left'>SEARCH <input name='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		//html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getLP = request.getParameter('enterlp');
		var getBinLocation = request.getParameter('enterlocation');
		var getItem = request.getParameter('enteritem');
		//var getItemDesc = request.getParameter('enterdescription');
		var getLPId = ''; 
		var getBinLocationId =''; 
		var getItemId='';
		var getItemDescId='';
		var Searchtype='';

		nlapiLogExecution('ERROR', 'enterlocation', request.getParameter('enterlocation'));       

		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();

		var IMarray = new Array();

		IMarray["custparam_actualbegindate"] = ActualBeginDate;

		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		IMarray["custparam_actualbegintime"] = TimeArray[0];
		IMarray["custparam_actualbegintimeampm"] = TimeArray[1];

		IMarray["custparam_screenno"] = 'INVTSEAR';

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, IMarray);
		}
		else 
		{
			try 
			{
				var LPFilters = new Array();
				LPFilters[0] = new nlobjSearchFilter('name', null, 'is', getLP);		         

				if (getLP != null && getLP != '') 
				{
					var LPSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, LPFilters, null);  				

					if (LPSearchResults != null && LPSearchResults.length > 0) 
					{						
						nlapiLogExecution('ERROR', 'LPSearchResults', LPSearchResults.length);
						var getLPId = LPSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'LP Id', getLPId );
						IMarray["custparam_lpid"] = getLPId;
						IMarray["custparam_lp"] = getLP;
						Searchtype="L";
					}
					else 
					{
						IMarray["custparam_error"] = 'INVALID LP';
						nlapiLogExecution('ERROR', 'SearchResults of given LP', 'LP is not found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'LP is not entered', getLP);
						return;
					}
				}



				if (getBinLocation != null && getBinLocation != '') 
				{
					var BinLocationFilters = new Array();
					BinLocationFilters[0] = new nlobjSearchFilter('name', null, 'is', getBinLocation);

					var LocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, BinLocationFilters, null);    				

					if (LocationSearchResults != null && LocationSearchResults.length > 0) 
					{
						nlapiLogExecution('ERROR', 'LocationSearchResults', LocationSearchResults.length);
						var getBinLocationId = LocationSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'Location Id', getBinLocationId );
						IMarray["custparam_imbinlocationid"] = getBinLocationId;
						IMarray["custparam_imbinlocationname"] = getBinLocation;
						Searchtype=Searchtype+"B";
					}
					else 
					{
						IMarray["custparam_error"] = 'INVALID LOCATION';
						nlapiLogExecution('ERROR', 'SearchResults of given Location', 'Location is not found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Bin Location is not entered', getBinLocation);
						return;
					}	
				}


				if (getItem != null && getItem != '') 
				{
					nlapiLogExecution('ERROR', 'getItem', getItem);
					/*var ItemFilters=new Array();
					ItemFilters.push(new nlobjSearchFilter('nameinternal', null, 'is', getItem));
					ItemFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
					var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilters, null);   	
				*/
				
					var currItem = validateSKU(getItem,'','','');
					if(currItem!= null && currItem!='')
					{
						var itemRecord = eBiz_RF_GetItemForItemId(currItem, '');
						nlapiLogExecution('ERROR', 'currItem', itemRecord.getId());
						//var getItemId = ItemSearchResults[0].getId();
						var getItemId = itemRecord.getId();
						nlapiLogExecution('ERROR', 'Item Id', getItemId);
						IMarray["custparam_imitemid"] = getItemId;
						IMarray["custparam_imitem"] = getItem;
						Searchtype=Searchtype+"I";
					}
					else 
					{
						IMarray["custparam_error"] = 'INVALID ITEM';
						nlapiLogExecution('ERROR', 'SearchResults of given Item', 'Item is found');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Item is not entered', getItem);
						return;
					}
				}


				/*if (getItemDesc != null && getItemDesc != '') 
				{
					var ItemDescFilters = new Array();
					ItemDescFilters[0] = new nlobjSearchFilter('custitem_ebizdescriptionitems', null, 'is', getItemDesc);

					var ItemDescSearchResults = nlapiSearchRecord('item', null, ItemDescFilters, null);   		        

					if (ItemDescSearchResults != null && ItemDescSearchResults.length > 0) 
					{
						nlapiLogExecution('ERROR', 'ItemDescSearchResults', ItemDescSearchResults.length);
						var getItemDescId = ItemDescSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'Item Id', getItemDescId);
						IMarray["custparam_itemDescid"] = getItemDescId;
						IMarray["custparam_itemDesc"] = getItemDesc;
					}
					else 
					{
						IMarray["custparam_error"] = 'INVALID ITEM';
						nlapiLogExecution('ERROR', 'SearchResults of given Item', 'Item is found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Item is not entered', getItem);
						//return;
					}
				}*/


				if((getLP!=null & getLP!='')||  (getBinLocation!=null && getBinLocation!='')||(getItem!=null && getItem!=''))
				{
					IMarray["custparam_searchtype"] = Searchtype;
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_inventory_details', 'customdeploy_ebiz_rf_inventory_details_d', false, IMarray);
					nlapiLogExecution('ERROR', 'Navigating to SKU/LOCATION Selected', 'Success');
				}
				else
				{
					IMarray["custparam_error"] = 'ENTER LOCATION/LP/ITEM';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
				}
			}
			catch (e) 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}        
	}
}

function IMSearch(LP, BinLocation, Item, ItemDescription)
{
	var IMFilter = new Array();
	IMFilter[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);
	IMFilter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var i = 3;


	if (LP != '') {
		IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LP);
		i++;
	}
	else 
		if (BinLocation != '') {
			IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'equalto', BinLocation);
			i++;
		}
		else 
			if (Item != '') {
				IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'equalto', Item);
				i++;
			}
			else 
				if (ItemDescription != '') {
					IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_itemdesc', null, 'is', ItemDescription);
				}

	nlapiLogExecution('DEBUG', 'IMFilter', IMFilter);

	var IMSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, IMFilter, null);

	if (IMSearchResults != null && IMSearchResults.length != 0) 
	{   
		var IMId = IMSearchResults[0].getId();
		var IMRecord = nlapiLoadRecord('customrecord_ebiznet_createinv', IMId);
	}

	return IMFilter;
}

function SearchLP(LP){
	var LPId;
	var LPFilter = new Array();
	LPFilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LP));
	LPFilter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]));
	LPFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var LPSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, LPFilter, null);

	if (LPSearchResults != null && LPSearchResults.length != 0) {
		LPId = LPSearchResults[0].getId();
	}

	return LPId;
}

function SearchBinLocation(BinLocation){
	var BinLocationId;

	var BinLocationFilter = new Array();
	BinLocationFilter[0] = new nlobjSearchFilter('name', null, 'is', BinLocation);
	BinLocationFilter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var BinLocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, BinLocationFilter, null);

	if (BinLocationSearchResults != null && BinLocationSearchResults.length != 0) {
		BinLocationId = BinLocationSearchResults[0].getId();
	}

	return BinLocationId;
}

function SearchItem(Item, ItemDescription){
	var ItemId;

	var ItemFilter = new Array();
	ItemFilter[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	if (Item != null) {
		//Changed as on 30/4/12 by suman
		ItemFilter[1] = new nlobjSearchFilter('nameinternal', null, 'is', Item);
		if (ItemDescription != null) {
			ItemFilter[2] = new nlobjSearchFilter('custitem_ebizdescriptionitems', null, 'is', ItemDescription);
		}
	}
	else {
		if (ItemDescription != null) {
			ItemFilter[1] = new nlobjSearchFilter('custitem_ebizdescriptionitems', null, 'is', ItemDescription);
		}

	}
	var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilter, null);

	if (ItemSearchResults != null && ItemSearchResults.length != 0) {
		ItemId = ItemSearchResults[0].getId();
	}

	return ItemId;
}




function validateSKU(itemNo, location, company,poid){
	nlapiLogExecution('ERROR', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company + '<br>';;
	inputParams = inputParams + 'PO Id = ' + poid;
	nlapiLogExecution('ERROR', 'Input Parameters', inputParams);

	var currItem = eBiz_RF_GetItemForItemNo(itemNo, location);
	
	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo, location);
	}
	
	if(currItem == ""){
		var vendor='';
		if(poid!=null && poid!='')
		{
			var po  = nlapiLoadRecord('purchaseorder',poid);
			vendor=po.getFieldValue('entity');
		}
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location,vendor);
	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {
		logMsg = 'Item = ' + currItem;
	}
	
	nlapiLogExecution('ERROR', 'Item Retrieved', logMsg);
	nlapiLogExecution('ERROR', 'validateSKU', 'End');
	return currItem;
}