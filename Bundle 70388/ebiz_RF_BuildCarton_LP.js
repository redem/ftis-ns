/***************************************************************************
�������������������������eBizNET
�������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_BuildCarton_LP.js,v $
*� $Revision: 1.1.2.1.4.3.4.5 $
*� $Date: 2014/06/13 08:30:22 $
*� $Author: skavuri $
*� $Name: t_NSWMS_2014_1_3_125 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_BuildCarton_LP.js,v $
*� Revision 1.1.2.1.4.3.4.5  2014/06/13 08:30:22  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.1.2.1.4.3.4.4  2014/06/06 06:41:23  skavuri
*� Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
*�
*� Revision 1.1.2.1.4.3.4.3  2014/05/30 00:26:46  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.1.4.3.4.2  2013/06/11 14:30:40  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.1.2.1.4.3.4.1  2013/04/17 16:04:01  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.1.4.3  2012/09/27 10:53:53  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting multiple language with given Spanish terms
*�
*� Revision 1.1.2.1.4.2  2012/09/26 12:42:29  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.1.2.1.4.1  2012/09/24 10:07:04  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi Lnaguage
*�
*� Revision 1.1.2.1  2012/06/26 06:25:48  spendyala
*� CASE201112/CR201113/LOG201121
*� New Script for Build Carton.
*�
*
****************************************************************************/

function BuildCarton_LP(request,response)
{
	if (request.getMethod() == 'GET') 
	{
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR / ESCANEAR  TOTE N&#218;MERO DE PLACA";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";
				
		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN TOTE LP# ";
			st2 = "SEND";
			st3 = "PREV";

		}
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_buildcarton_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_buildcarton_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"</td></tr>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{

		var POarray=new Array();
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
    	
		
		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4= "LP NO V&#193;LIDO";
			st5 = "INGRESE LP #";
			
		}
		else
		{

			st4= "INVALID LP";
			st5 = "ENTER LP#";
			
		}
		
		var vLp=request.getParameter('enterlp');
		nlapiLogExecution('DEBUG','vLp',vLp);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else 
		{
			try
			{
				if (vLp != '' && vLp != null) 
				{
					var res=ValidateLp(vLp);
					var vflag=res[0];
					var searchrec=res[1];
					if(vflag==true)
					{
						POarray["custparam_poid"]=nlapiLookupField('purchaseorder',searchrec[0].getValue('custrecord_ebiz_order_no'),'tranid');
						POarray["custparam_item"]=searchrec[0].getText('custrecord_sku');
						POarray["custparam_quantity"]=searchrec[0].getValue('custrecord_expe_qty');
						POarray["custparam_recordid"]=searchrec[0].getId();
						response.sendRedirect('SUITELET', 'customscript_rf_buildcarton_cart', 'customdeploy_rf_buildcarton_cart_di', false, POarray);
					}
					else
					{
						POarray["custparam_screenno"]='buildcartonLp';
						POarray["custparam_error"] = st4;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
				}
				else
				{
					POarray["custparam_screenno"]='buildcartonLp';
					POarray["custparam_error"] = st5;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}
			}
			catch(exp)
			{
				nlapiLogExecution('DEBUG','Exception in main Else',exp);
			}
		}
	}
}


function ValidateLp(vLp)
{
	try
	{
		nlapiLogExecution('DEBUG','vLp',vLp);
		var recordArray=new Array();
		var filterOpentask=new Array();
		filterOpentask.push(new nlobjSearchFilter('custrecord_lpno',null,'is',vLp));
		filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',2));//2=STATUS.INBOUND.LOCATIONS_ASSIGNED
		filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',2));//PUTW
		filterOpentask.push(new nlobjSearchFilter('custrecord_transport_lp',null,'isempty'));
		
		var columnOpentask=new Array();
		columnOpentask[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');
		columnOpentask[1]=new nlobjSearchColumn('custrecord_sku');
		columnOpentask[2]=new nlobjSearchColumn('custrecord_expe_qty');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filterOpentask,columnOpentask);

		if(searchresult!=null&&searchresult!="")
		{
			recordArray[0]=true;
			recordArray[1]=searchresult;
		}
		else
		{
			recordArray[0]=false;
			recordArray[1]='';
		}
		return recordArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in ValidateLp',exp);
	}
}