/***************************************************************************
������������������������������� � ����������������������������� ��eBizNET
�����������������������                           eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CartScan.js,v $
 *� $Revision: 1.2.2.10.4.6.2.17.2.3 $
 *� $Date: 2015/07/22 15:43:21 $
 *� $Author: grao $
 *� $Name: t_eBN_2015_1_StdBundle_1_213 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_CartScan.js,v $
 *� Revision 1.2.2.10.4.6.2.17.2.3  2015/07/22 15:43:21  grao
 *� 2015.2   issue fixes  201413127
 *�
 *� Revision 1.2.2.10.4.6.2.17.2.2  2014/10/16 14:02:01  skavuri
 *� Case# 201410724 Std Bundle issue fixed
 *�
 *� Revision 1.2.2.10.4.6.2.17.2.1  2014/09/16 14:41:57  skreddy
 *� case # 201410256
 *�  TPP SB issue fix
 *�
 *� Revision 1.2.2.10.4.6.2.17  2014/06/13 07:28:32  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.2.2.10.4.6.2.16  2014/05/30 00:26:48  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.2.10.4.6.2.15  2014/05/14 14:06:51  gkalla
 *� case#20148382
 *� RF RMA Cart checkin issue
 *�
 *� Revision 1.2.2.10.4.6.2.14  2014/04/22 14:54:01  grao
 *�  LL Sb RMA CARTChekin enhancement case#:20148087
 *�
 *� Revision 1.2.2.10.4.6.2.13  2014/03/21 15:59:31  skavuri
 *� Case # 20127806 issue fixed
 *�
 *� Revision 1.2.2.10.4.6.2.12  2014/02/21 15:14:23  rmukkera
 *� no message
 *�
 *� Revision 1.2.2.10.4.6.2.11  2014/02/14 06:28:19  grao
 *� Case# 20127161 related issue fixes in Dealmed  issue fixes
 *�
 *� Revision 1.2.2.10.4.6.2.10  2013/12/12 07:29:01  nneelam
 *� Case# 20125983
 *� std bundle issue fix..
 *�
 *� Revision 1.2.2.10.4.6.2.9  2013/12/02 08:59:59  schepuri
 *� 20126048
 *�
 *� Revision 1.2.2.10.4.6.2.8  2013/11/29 15:03:44  grao
 *� Case# 20125983  related issue fixes in SB 2014.1
 *�
 *� Revision 1.2.2.10.4.6.2.7  2013/11/01 12:54:15  rmukkera
 *� Case# 20125455
 *�
 *� Revision 1.2.2.10.4.6.2.6  2013/10/24 13:00:45  rmukkera
 *� Case# 20125285
 *�
 *� Revision 1.2.2.10.4.6.2.5  2013/10/22 15:06:08  skreddy
 *� Case# 20124932
 *� standard bundle issue fix
 *�
 *� Revision 1.2.2.10.4.6.2.4  2013/09/06 15:05:33  skreddy
 *� Case# 20124293
 *� standard bundle issue fix
 *�
 *� Revision 1.2.2.10.4.6.2.3  2013/09/05 16:54:02  gkalla
 *� Case# 20124304
 *� after click on auto cart system displaying order as TO even if processing PO for Afosa
 *�
 *� Revision 1.2.2.10.4.6.2.2  2013/06/11 14:30:41  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.2.2.10.4.6.2.1  2013/04/17 16:04:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.2.2.10.4.6  2013/02/07 15:10:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� disabling ENTER Button func added
 *�
 *� Revision 1.2.2.10.4.5  2012/11/08 14:01:01  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Dafiti Cart Putaway invalid serial,cart,item no issue fix
 *�
 *� Revision 1.2.2.10.4.4  2012/09/28 10:08:22  grao
 *� no message
 *�
 *� Revision 1.2.2.10.4.3  2012/09/27 10:56:55  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.2.2.10.4.2  2012/09/26 12:47:07  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.2.2.10.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.2.2.10  2012/09/06 06:33:08  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Missing parameter Actual begin time while passing to query string.
 *�
 *� Revision 1.2.2.9  2012/07/31 08:31:12  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Change 'vendor' and 'po line' Heading
 *�
 *� Revision 1.2.2.8  2012/07/23 22:45:12  spendyala
 *� CASE201112/CR201113/LOG201121
 *�  searching LP Range w.r.t site specific.
 *�
 *� Revision 1.2.2.7  2012/04/17 10:38:39  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF Cart To Checkin
 *�
 *� Revision 1.2.2.6  2012/03/16 13:56:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Disable-button functionality is been added.
 *�
 *� Revision 1.2.2.5  2012/03/13 23:26:00  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� cart checkin
 *�
 *� Revision 1.2.2.4  2012/03/13 15:03:48  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Showing vendorname and No. of lines in Po.
 *�
 *� Revision 1.2.2.3  2012/02/23 13:48:08  schepuri
 *� CASE201112/CR201113/LOG201121
 *� fixed TPP issues
 *�
 *� Revision 1.3  2012/02/23 11:46:12  rmukkera
 *� CASE201112/CR201113/LOG201121
 *� focus given to the input box and actual begindate parameters added to poarray
 *�
 *� Revision 1.2  2012/02/16 10:37:44  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 *� Revision 1.1  2012/02/02 09:08:56  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/

function EnterCartLP(request, response)
{
	var matchFound = true;
	if (request.getMethod() == 'GET') 
	{   

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var trantype = request.getParameter('custparam_trantype');
		nlapiLogExecution('DEBUG', 'Into Request', getPONo);

		var vendorNo,LinesInPO="";
		var POId='';
		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('tranid', null, 'is', getPONo));

		var poLineSearchResults = nlapiSearchRecord(trantype, null, filters, null);

		if(poLineSearchResults != null && poLineSearchResults.length > 0)
		{
			POId=poLineSearchResults[0].getId();
		}

		if(POId!=null&&POId!="")
		{
			var searchresults = nlapiLoadRecord(trantype,POId);
			vendorNo=searchresults.getFieldText('entity');
			LinesInPO=searchresults.getLineItemCount('item');
		}


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		var packcode= request.getParameter('custparam_packcode');//Case# 201410724
		var st0,st1,st2,st3,st4,st5,st6;
		//20126048
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "N&#218;MERO DE PROVEEDOR";
			st2 = "N&#218;MERO DE L&#205;NEAS EN LA ORDEN DE COMPRA";
			st3 = "N&#218;MERO DE L&#205;NEAS EN LA ORDEN DE TRANSFERENCIA";
			st4 = "ENTRAR / ESCANEAR N&#218;MERO DE CARRO";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st7 = "N&#218;MERO DE L&#205;NEAS EN LA ORDEN DE RMA";

		}
		else
		{
			st0 = "";
			if(trantype=='returnauthorization')
			{
				st1 = "CUSTOMER# ";

			}			
			else
			{
				st1 = "VENDOR# ";
			}
			st2 = "NO.OF LINES IN PO#";
			st3 = "NO.OF LINES IN TO#";
			st4 = "ENTER/SCAN CART#";
			st5 = "SEND";
			st6 = "PREV";
			st7 = "NO.OF LINES IN RMA#";
		}




		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";       
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + vendorNo + "</label></td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		if(trantype=='purchaseorder')
		{
			html = html + "				<td align = 'left'>" + st2 +" : <label>" + LinesInPO + "</label></td>";
		}
		else if(trantype=='transferorder')
		{
			html = html + "				<td align = 'left'>" + st3 + " : <label>" + LinesInPO + "</label></td>";
		}
		else
		{
			html = html + "				<td align = 'left'>" + st7 + " : <label>" + LinesInPO + "</label></td>";
		}
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+  st4 + ":";	

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+  st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+  st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";   
		html = html + "				<input type='hidden' name='hdnPoID' value=" + getPONo + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdnpackcode' value=" + packcode + ">";//Case# 201410724
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var vgetItemLP = request.getParameter('enterlp').toUpperCase();
		var varEnteredLP = request.getParameter('enterlp');


		nlapiLogExecution('DEBUG', 'varEnteredLP', varEnteredLP);
		nlapiLogExecution('DEBUG', 'vgetItemLP', vgetItemLP);

		if(varEnteredLP == vgetItemLP)
		{
			varEnteredLP = request.getParameter('enterlp').toUpperCase();
		}
		else
		{
			varEnteredLP = request.getParameter('enterlp');
		}

		//Case # 20125983?start
		//var varEnteredLP = request.getParameter('enterlp').toUpperCase();
		//Case # 20125983?End
		nlapiLogExecution('DEBUG', 'Entered Cart#', varEnteredLP);
		if (request.getParameter('enterlp') != null) {
			varEnteredLP = request.getParameter('enterlp').toUpperCase();
		}
		nlapiLogExecution('DEBUG', 'Entered Cart#', varEnteredLP);
		var trantype=request.getParameter('hdntrantype');
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		POarray["custparam_cartlpno"] = varEnteredLP;
		POarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
		POarray["custparam_company"] = request.getParameter('custparam_company');
		POarray["custparam_actualbegintime"]=request.getParameter('custparam_actualbegintime');
		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_trantype"] = trantype;
		POarray["custparam_packcode"] = request.getParameter('hdnpackcode'); //Case# 201410724

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');       

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 

		if (optedEvent == 'F7') {	
			if(trantype=='purchaseorder')
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_po', 'customdeploy_ebiz_rf_cartcheckin_po_di', false, POarray);
			else if(trantype=='transferorder')					
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_to', 'customdeploy_ebiz_rf_cartcheckin_to_di', false, POarray);
			else
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_rma', 'customdeploy_ebiz_rf_cartcheckin_rma_di', false, POarray);
		}
		else 
		{   
			POarray["custparam_trantype"] = trantype;
			if (varEnteredLP != '' && varEnteredLP != null) 
			{
				POarray["custparam_cartlpno"] = varEnteredLP;
				POarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
				POarray["custparam_company"] = request.getParameter('custparam_company');
				POarray["custparam_actualbegintime"]=request.getParameter('custparam_actualbegintime');
				POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
				POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
				POarray["custparam_poid"] = request.getParameter('custparam_poid');
				POarray["custparam_trantype"] = trantype;
				nlapiLogExecution('DEBUG', 'Enterd Cart LP', varEnteredLP);	 
				lpErrorMessage= MastLPExists(varEnteredLP,POarray["custparam_whlocation"],vgetItemLP);//check for manual cart lp exists
				if(lpErrorMessage=="")
				{
					NewCartLPNo=varEnteredLP;
					POarray["custparam_cartlpno"] = NewCartLPNo;
					nlapiLogExecution('DEBUG', 'POarray["custparam_cartlpno"]', POarray["custparam_cartlpno"]);	 
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false, POarray);
				}
				else
				{
					POarray["custparam_error"] = lpErrorMessage;
					POarray["custparam_screenno"] = 'CRT2';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}
			}
			else //To go to Cart LP Confirmation
			{
				POarray["custparam_cartlpno"] = '';
				POarray["custparam_poid"] =  request.getParameter('hdnPoID');
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartlpconfirm', 'customdeploy_ebiz_rf_cartlpconfirm_di', false, POarray);
				nlapiLogExecution('DEBUG', 'CART LP Not Entered', varEnteredLP);
				nlapiLogExecution('DEBUG', 'Redirecting to confirmation for auto generated', varEnteredLP);
				return;
			}	 
		}
	}
}
/**
 * To check manually generated Cart LP# exists in master LP or not
 * 
 * @param manualShipLP   
 * @returns message with proper error message
 */
function MastLPExists(manualCartLP,whloc,vgetItemLP)
{
	var message="";
	var filters =new Array();
	var vResult=new Array();
	vResult=ebiznet_LPRange_CL_withLPType1(manualCartLP,'2','4',whloc);
	if (vResult[0] == 'Y')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', manualCartLP));
//		filters.push(new nlobjSearchFilter('custrecord_ebiz_cart_closeflag', null, 'is', 'T'));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag'));
		// To get the data from Master LP based on selection criteria
		searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
		if(searchresults!= null && searchresults.length>0)
		{
			if(searchresults[0].getValue('custrecord_ebiz_cart_closeflag')=='T')
			{
				message="Cart LP# already closed, Please enter another Cart LP#";
				nlapiLogExecution('DEBUG',"message ",message );
				return message;//Case #20127806
			}
			else
			{
				return message="";
			}
			//
//			else
//			{
//			message="Cart LP# already exists, Please enter another Cart LP#";
//			nlapiLogExecution('DEBUG',"message ",message );
//			return message;
//			}
		}
		else
		{
			//Case 20124932 added for validating invalid cart

			vResult=ebiznet_LPRange_CL_withLPType1(manualCartLP,'2','4',whloc,vgetItemLP)// for User Defined  with Cart LPType
			if (vResult[0] == 'Y')

			{
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				customrecord.setFieldValue('name', manualCartLP);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', manualCartLP);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 4);//LP type=CART
				var rec = nlapiSubmitRecord(customrecord, false, true);
				//case 20124932 start
				return message;
				//case end
			}
			else if (vResult[1] == 'O')
			{
				message="CART LP# IS OUT OF RANGE";
				//case 20124932 start
				return message;
				//case end
			}
			else if(vResult[1] == 'I')
			{
				message="Please enter valid cart no";
				//case 20124932 start
				return message;
				//case end

			}
			else if(vResult[1] == 'R')
			{

				message="PLs give Range along with LpPrefix";
				//case 20124932 start
				return message;
				//case end

			}
			//Case 20124932 end
			/*if(ebiznet_LPRange_CL_withLPType(manualCartLP,'2','4',whloc))// for User Defined  with Cart LPType
		{
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', manualCartLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', manualCartLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			return message;
		}
		else
		{
			message="CART LP# IS OUT OF RANGE";
			return message;
		}*/
		}
	}
	else
	{
		return message="Invalid CartLP Range";
	}
}
