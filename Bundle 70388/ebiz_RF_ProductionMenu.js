

function ProductionMenu(request,response)
{
	if(request.getMethod()=='GET')
	{
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6,st7;

		if( getLanguage == 'es')
		{
			st0 = "PRODUCCI&#211;N DE MEN&#218;";
			st1 = "APROBACI&#211;N INICIAL QC";
			st2 = "APROBACION FINAL QC";
			st3 = "AJUSTES DE CHATARRA";
			st4 = "ENTRAR SELECCI&#211;N";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st7 = "CONSTRUIR LAS �RDENES DE TRABAJO";

		}
		else
		{
			st0 = "PRODUCTION MENU";
			st1 = "INITIAL QC APPROVAL";
			st2 = "FINAL QC APPROVAL";
			st3 = "SCRAP ADJUSTMENTS";
			st4 = "ENTER SELECTION";
			st5 = "SEND";
			st6 = "PREV";
			st7 = "BUILD WORK ORDERS";
		}

		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di');
		var linkURL_1 = checkInURL_1; 
		var functionkeyHtml=getFunctionkeyScript('_rfproductionmenu'); 
		var html = "<html><head><title>" + st0 +  "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('selectoption').focus();"; 
		
		html = html + "</script>";		
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfproductionmenu' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		/*		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 1. WORK ORDER PICKING";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. " + st7;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st7 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		/*	html = html + "			<tr>";
		html = html + "				<td align = 'left'> 2. " + st1;
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 3. " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 4. " + st3;
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";
		response.write(html);
	}
	else
	{
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdPrevious');
		var optedField = request.getParameter('selectoption');
		var PRODarray=new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		PRODarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', PRODarray["custparam_language"]);


		var st7,st8;
		if( getLanguage == 'es')
		{
			st7 = "Opci&#243;n no v&#225;lida";
			st8 = "Producci&#243;n Men&#250;";
		}
		else
		{
			st7 = "Invalid Option";
			st8 = "ProductionMenu";
		}


		PRODarray["custparam_error"] = st7;
		PRODarray["custparam_screenno"] = st8;





		if (optedEvent == 'F7') {
			nlapiLogExecution('ERROR', 'optedEvent if', optedEvent);
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, null);
		}
		else 
		{
			if (optedField == '1') {
				PRODarray["custparam_option"] = 'BuildWO';
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, PRODarray);
			}
			else
				/*if (optedField == '2') {
					PRODarray["custparam_option"] = 'InitialQC';
					response.sendRedirect('SUITELET', 'customscript_rf_production_qc_approval', 'customdeploy_rf_production_qc_approv_di', false, PRODarray);
				}
			else 
				if (optedField == '3') {
					PRODarray["custparam_option"] = 'finalQC';
					response.sendRedirect('SUITELET', 'customscript_rf_production_qc_approval', 'customdeploy_rf_production_qc_approv_di', false, PRODarray);
				}
				else 
					if (optedField == '4') {
						response.sendRedirect('SUITELET', 'customscript_rf_scrap_adjustment', 'customdeploy_rf_scrap_adjustment_di', false, optedField);
					}
					else */
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, PRODarray);
			}
		}
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
	}
}