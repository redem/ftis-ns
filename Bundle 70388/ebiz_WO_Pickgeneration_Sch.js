/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Oct 2014     GA300732
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WO_Pickgeneration_Sch.js,v $
 *     	   $Revision: 1.1.2.2.2.1 $
 *     	   $Date: 2015/09/21 14:02:28 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WO_Pickgeneration_Sch.js,v $
 * Revision 1.1.2.2.2.1  2015/09/21 14:02:28  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.2  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.1  2015/02/13 08:00:36  skreddy
 * Case# 201410541
 * Work order location generation sch & wave generation for work order
 *
 * Revision 1.1.4.4.2.39  2014/09/19 12:59:59  snimmakayala
 * Case: 201410288
 * Pick Strategies by Order Priority
 *
 * Revision 1.1.4.4.2.38  2014/09/19 07:41:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Changes
 *
 * Revision 1.1.4.4.2.37  2014/09/16 14:55:35  skreddy
 * case # 201410299
 * TPP SB issue fix
 *
 *****************************************************************************/
 

function setClusterNo(eBizWaveNo)
{
	nlapiLogExecution('Debug','ClusterWaveNo',eBizWaveNo);
	var pickMethodIds=new Array();
	pickMethodIds=getpickmethod();
	if(pickMethodIds!=null && pickMethodIds.length>0)
	{
		var ebizMethodNo=new Array();
		var ebizInternalRecordId=new Array();
		var ebizOrderId=new Array();

		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',parseFloat(eBizWaveNo)));
		filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',[3]));
		filter.push(new nlobjSearchFilter('custrecord_actualendtime',null,'isempty'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizmethod_no').setSort();
		column[1]=new nlobjSearchColumn('internalid');
		column[2]=new nlobjSearchColumn('name').setSort();

		var searchRecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, column);
		//nlapiLogExecution('Debug','ClusterSearchRecord',searchRecord.length);

		if(searchRecord!=null && searchRecord!='')
		{
			for(var count=0;count < searchRecord.length; count++)
			{
				ebizMethodNo[count]=searchRecord[count].getValue('custrecord_ebizmethod_no');
				ebizInternalRecordId[count]=searchRecord[count].getValue('internalid');
				ebizOrderId[count]=searchRecord[count].getValue('name');
			}
		}

		nlapiLogExecution('Debug','ClusterpickMethodIds',pickMethodIds);
		nlapiLogExecution('Debug','ebizMethodNo',ebizMethodNo);
		nlapiLogExecution('Debug','A_pickMethodIds[itemcount][1]',pickMethodIds[0][1]);
		nlapiLogExecution('Debug','A_pickMethodIds[itemcount][2]',pickMethodIds[0][2]);
		nlapiLogExecution('Debug','ebizInternalRecordId',ebizInternalRecordId);
		var clusterno = GetMaxTransactionNo('WAVECLUSTER');
		nlapiLogExecution('Debug','clusternobeforefor',clusterno);
		
		for(var itemcount=0;itemcount<pickMethodIds.length;itemcount++)
		{
			var ordno="";
			var maxPickFlagCount=0;
			var tempcount=0;
			nlapiLogExecution('Debug','searchRecord',searchRecord);
			//nlapiLogExecution('Debug','searchRecord.length',searchRecord.length);
			
			for(var loopcount=0;(searchRecord!=null&&loopcount<searchRecord.length);loopcount++)
			{  
				nlapiLogExecution('Debug','searchRecord.length',searchRecord.length);
				nlapiLogExecution('Debug','pickMethodIds[itemcount][0]',pickMethodIds[itemcount][0]);
				nlapiLogExecution('Debug','ebizMethodNo[loopcount]',ebizMethodNo[loopcount]);
				//nlapiLogExecution('Debug','clustervalues',pickMethodIds[itemcount][0]+','+ebizMethodNo[loopcount]);
				if(pickMethodIds[itemcount][0]==ebizMethodNo[loopcount])
				{


					//for a particular pickmethod ,if max order no. is not null and max pickno. is null
					//then the below code execute.
					if(pickMethodIds[itemcount][1]!="" && pickMethodIds[itemcount][2]=="")
					{
						nlapiLogExecution('Debug','ordno',ordno);
						nlapiLogExecution('Debug','ebizOrderId[loopcount]',ebizOrderId[loopcount]);
						//nlapiLogExecution('Debug','BlockOfCodeForMaxPickIsNULL','chkpt');
						if(ordno!=ebizOrderId[loopcount])
						{
							nlapiLogExecution('Debug','ebizInternalRecordId[loopcount]',ebizInternalRecordId[loopcount]);

							if(parseInt(tempcount)<pickMethodIds[itemcount][1] && tempcount !=0)
							{
								tempcount=parseInt(tempcount)+1;
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								ordno = ebizOrderId[loopcount];
							}
							else
							{
								clusterno = GetMaxTransactionNo('WAVECLUSTER');
								//nlapiLogExecution('Debug','clusternoafterelse',clusterno);
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								ordno = ebizOrderId[loopcount];
								tempcount=1;
							}
						}
						else
						{
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
						}
					}



					//for a particular pickmethod ,if max orderno. is null and max pickno.is not null 
					//then the below code execute.
					if(pickMethodIds[itemcount][1] == "" && pickMethodIds[itemcount][2] != "")
					{
						//nlapiLogExecution('Debug','BlockOfCodeForMaxorderIsNULL','chkpt');
						tempcount=parseInt(tempcount)+1;
						if(parseInt(tempcount)<=pickMethodIds[itemcount][2])
						{
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
						}
						else
						{
							clusterno = GetMaxTransactionNo('WAVECLUSTER');
							//nlapiLogExecution('Debug','clusternoafterelse',clusterno);
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							tempcount=1;
						}
					}



					//for a pickmethod, if max orderno. and max pickno is not null 
					//then the below code execute.
					if(pickMethodIds[itemcount][1]!= "" && pickMethodIds[itemcount][2] != "")
					{
						//nlapiLogExecution('Debug','BlockOfCodeForNONEIsNULL','chkpt');
						maxPickFlagCount= parseInt(maxPickFlagCount)+1;
						if(parseInt(maxPickFlagCount)<=pickMethodIds[itemcount][2])
						{
							if(ordno!=ebizOrderId[loopcount])
							{
								tempcount=parseInt(tempcount)+1;

								if(parseInt(tempcount)<=pickMethodIds[itemcount][1])
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
									//nlapiLogExecution('Debug','clusternoafterelse',clusterno);
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
									tempcount=1;
								}
							}
							else
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							}
						}
						else
						{
							clusterno = GetMaxTransactionNo('WAVECLUSTER');
//							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							maxPickFlagCount= 1;

							if(ordno!=ebizOrderId[loopcount])
							{
								tempcount=parseInt(tempcount)+1;

								if(parseInt(tempcount)<=pickMethodIds[itemcount][1])
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
									//nlapiLogExecution('Debug','clusternoafterelse',clusterno);
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
									tempcount=1;
								}
							}
							else
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							}
						}
					}
				}
			}
		}
	}	
} 

function PickGenerationScheduler(type)
{
	var context = nlapiGetContext(); 
	var vordno="";
	var vordlineno="";
	var vwaveno="";
	var vlastlineinorder='F';
	var vlocation="";
	var vcompany="";
	var userId="";
	var allocstrategy='';
	var vlastlineinwave='F';
	var vwaveordrecid=-1;
	var vusedflag='';
	var voldordno='';
	var lefoflag='';

	try{
		var vcuruserId = context.getUser();
		vwaveno = context.getSetting('SCRIPT', 'custscript_ebizwowaveno');
		var vOrdIdList = context.getSetting('SCRIPT', 'custscript_ebizwoid_ordlist');
		var vOrdNameList = context.getSetting('SCRIPT', 'custscript_ebizwo_ordlist');
		updateScheduleScriptStatus('WAVE RELEASE',vcuruserId,'In Progress',null,null);

		
		nlapiLogExecution('Debug', 'Time Stamp at the start of scheduler',TimeStampinSec());


		var str = 'vwaveno. = ' + vwaveno + '<br>';
		str = 'vOrdIdList. = ' + vOrdIdList + '<br>';
		nlapiLogExecution('Debug', 'Parameter Details', str);

		nlapiLogExecution('Debug','Remaining usage at the start',context.getRemainingUsage());


		try{

			if(type != 'aborted' &&
					vwaveno != null)
			{
				if(vOrdIdList != null && vOrdIdList != '')
				{
					var vOrdList = vOrdIdList.split(',');
					var vOrdNameList = vOrdNameList.split(',');
					var vStatus='F';
					for(var k=0;k<vOrdList.length;k++)
					{	
						var woidname=vOrdNameList[k];
						var woidno=vOrdList[k];
						var vResultVal=GenerateLocationsSch(woidname,woidno,vwaveno);
						if(vResultVal ==true || vResultVal =='true')
							vStatus='T';
						nlapiLogExecution('DEBUG', 'vResultVal', vResultVal);
					}
					if(vStatus == 'T')
						setClusterNo(vwaveno);
					nlapiLogExecution('ERROR', 'Time Stamp before pickreportscheduler',TimeStampinSec());
					pickreportscheduler(vwaveno);
					nlapiLogExecution('ERROR', 'Time Stamp after pickreportscheduler',TimeStampinSec());
				}

			}

		}
		catch(exp) 
		{
			nlapiLogExecution('Debug', 'Exception in PickGenerationSchScript : ', exp);		
		}
	}
	catch(exp1) 
	{
		nlapiLogExecution('Debug', 'Exception in PickGenerationSchScript1 : ', exp1);	
	}

	nlapiLogExecution('Debug','Remaining usage at the end',context.getRemainingUsage());
}

function GenerateLocationsSch(woidname,woidno,vwaveno)
{
	try
	{
		 
		var context = nlapiGetContext(); 
		nlapiLogExecution('DEBUG', 'type', type);
		nlapiLogExecution('DEBUG', 'Into GenerateLocationsPOSTRequest', TimeStampinSec());



		nlapiLogExecution('Debug', 'Time Stamp at the start of scheduler',TimeStampinSec());

		nlapiLogExecution('Debug','Remaining usage at the start',context.getRemainingUsage());


		var str = 'woidno.' + woidno + '<br>';
		str = str + 'woidname.' + woidname + '<br>';

		nlapiLogExecution('DEBUG', 'WO Values', str);


		nlapiLogExecution('ERROR', 'Time Stamp before CheckAvailability',TimeStampinSec());
		var vResult= CheckAvailability(woidno,vwaveno);
		nlapiLogExecution('DEBUG', 'vResult', vResult);
		nlapiLogExecution('ERROR', 'Time Stamp after CheckAvailability',TimeStampinSec());
		
		//AllocateInv(woidno,request);
		//response.writePage(form);

		nlapiLogExecution('ERROR', 'Out of GenerateLocationsPOSTRequest',TimeStampinSec());
		return vResult;
	}
	catch(exp1) 
	{
		nlapiLogExecution('Debug', 'Exception in WorkGenerationSchScript1 : ', exp1);	
	}
} 

function CheckAvailability(woidno,vwaveno)
{
	nlapiLogExecution('ERROR', 'Into Check Availablity '); 
	var vMessage= fnGetDetails(woidno,vwaveno);

	if(vMessage== 'SUCCESS') 
	{ 
		return true;
	} 
	else
	{
		if(vMessage!= null && vMessage != "")
		{
			var vArrMsg=vMessage.split('~');
			if(vArrMsg.length>1)
			{
				nlapiLogExecution('ERROR','Allocation Failed ',vArrMsg[1]);
				 
				//var ErrorMsg = nlapiCreateError('CannotAllocate',vArrMsg[1], true);
				//throw ErrorMsg; //throw this error object, do not catch it
			}
			else
			{
				nlapiLogExecution('ERROR','eBiz Allocation Failed ');
				 
				//var ErrorMsg = nlapiCreateError('CannotAllocate','Allocation Failed', true);
				//throw ErrorMsg; //throw this error object, do not catch it
			}	
		}
		return false;
	}

}


function fnGetDetails(vId,vwaveno)
{

	var context = nlapiGetContext(); 
	nlapiLogExecution('ERROR','Into Get Details');
	//var vId = request.getParameter('custpage_workorder');
	var SearchresultsArray = new Array();//added by santosh
	nlapiLogExecution('Debug','Time stamp 3',TimeStampinSec());
	var searchresults = nlapiLoadRecord('workorder', vId);
	nlapiLogExecution('Debug','Time stamp 4',TimeStampinSec());
	var itemcount= searchresults.getLineItemCount('item');
	var vWOId=searchresults.getFieldValue('tranid');
	var SkuNo=searchresults.getFieldValue('assemblyitem');
	var vordertype=searchresults.getFieldValue('custbody_nswmssoordertype');
	nlapiLogExecution('Debug','vordertype',vordertype);
	var varAssemblyQty=searchresults.getFieldValue('quantity');
	var vLocation= searchresults.getFieldValue('location');
	var vWMSlocation=searchresults.getFieldValue('location');
	var qty=0;
	if(qty1!= null && qty1!="" && parseFloat(qty1)>0)
		qty=qty1;

	//nlapiLogExecution('ERROR', 'vId', vId);
	//nlapiLogExecution('ERROR', 'vWOId', vWOId);
	/*var searchresults = nlapiLoadRecord('workorder', vId);
	if(searchresults !=null && searchresults != "" && searchresults.length>0)
		vWOId=searchresults.getFieldValue('tranid');
	nlapiLogExecution('ERROR', 'vWOId', vWOId);*/

	nlapiLogExecution('ERROR', 'ItemCount', itemcount);
	//nlapiLogExecution('ERROR', 'assemItem', assemItem);

	var veBizContLP = GetMaxLPNo('1', '2',vWMSlocation);
	CreateMasterLPRecord(veBizContLP,null,null,null,null,vWMSlocation)
	var vInvReturn=new Array();
	//alert("3");
	var strItem="",strQty=0,vcomponent,vlineno=0;

	/*	var ItemType = nlapiLookupField('item', vitem, 'recordType');=0
	nlapiLogExecution('ERROR','ItemType',ItemType);
	var searchresultsitem;
	if(ItemType!=null && ItemType !='')

	{
		nlapiLogExecution('ERROR','Item Type',ItemType);
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

		searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
	}

	else
	{
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
	}
	var SkuNo=searchresultsitem.getFieldValue('itemid');*/
	var recCount=0; 
	//Written by Ganesh
	//alert("ItemCount : "+ itemcount);
	var vAlert='';
	//nlapiLogExecution('ERROR','itemcount ',itemcount);
	var vItemArr=new Array();

	for(var s=1; s<=itemcount;s++)
	{
		var vCompItem = searchresults.getLineItemValue('item', 'item', s);
		var CompItemType = searchresults.getLineItemValue('item', 'itemtype', s);
		//nlapiLogExecution('ERROR','CompItemType',CompItemType);
		var vCommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', s);

		if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && vItemArr.indexOf(vCompItem) == -1 && (vCommittedQty !=null && vCommittedQty !='' && vCommittedQty !=0))
			vItemArr.push(vCompItem);
	}
	nlapiLogExecution('Debug','Time stamp 5',TimeStampinSec());
	if(vItemArr.length>0)
	{
		var filters2 = new Array();

		var columns2 = new Array();
		columns2[0] = new nlobjSearchColumn('custitem_item_family');
		columns2[1] = new nlobjSearchColumn('custitem_item_group');
		columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
		columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
		columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
		columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
		columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

		filters2.push(new nlobjSearchFilter('internalid', null, 'is', vItemArr));

		var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

		var filters3=new Array();
		filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		if(vordertype!=null&&vordertype!="")
			filters3.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@',vordertype]));

		var columns3=new Array();

		columns3[0] = new nlobjSearchColumn('name');
		columns3[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
		columns3[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
		//columns3[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
		columns3[3]=new nlobjSearchColumn('formulanumeric');
		columns3[3].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
		columns3[4] = new nlobjSearchColumn('custrecord_ebizruleidpick');
		columns3[5] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
		columns3[6] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
		columns3[7] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');

		//columns3[3].setSort();
		var pickrulesarray = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters3, columns3);
	}

	var pfLocationResults = getPFLocationsForOrder(vWMSlocation,vItemArr);

	nlapiLogExecution('Debug','Time stamp 6',TimeStampinSec());
	
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no','null','is',vId));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag','null','anyof',['9','8']));
	//filter.push(new nlobjSearchFilter('custrecord_line_no','null','is',LineNo));
	//filter.push(new nlobjSearchFilter('custrecord_sku','null','anyof',lineItemvalue));
	filter.push(new nlobjSearchFilter('custrecord_tasktype','null','anyof','3'));
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_line_no',null,'group');

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);	
	
	var display = "false";
	
	for(var m=1; m<=itemcount;m++) 
	{
		var serialNo="";
		//var vBatch="";
		//alert("SkuNo" + SkuNo);
		strItem = searchresults.getLineItemValue('item', 'item', m);
		//alert("strItem" + strItem);
		var lineItem = searchresults.getLineItemText('item', 'item', m);
		strQty = searchresults.getLineItemValue('item', 'quantity', m);
		vlineno = searchresults.getLineItemValue('item','line',m);
		nlapiLogExecution('ERROR', 'vlineno', vlineno);
		var CompItemType = searchresults.getLineItemValue('item', 'itemtype', m);
		nlapiLogExecution('ERROR','CompItemType',CompItemType);
		var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', m);
		
var expqty = 0;
		
		if(searchrecord!=null && searchrecord!=''){
			nlapiLogExecution('ERROR','searchrecord.length',searchrecord.length);
			for(var i =0; i<searchrecord.length; i++){
				var otlineno =searchrecord[i].getValue('custrecord_line_no',null,'group'); 
				var otitemname = searchrecord[i].getText('custrecord_sku',null,'group');
				nlapiLogExecution('ERROR','otitemname',otitemname);
				nlapiLogExecution('ERROR','lineItem',lineItem);
				nlapiLogExecution('ERROR','otlineno',otlineno);
				nlapiLogExecution('ERROR','vlineno',vlineno);
				if((otitemname == lineItem) && (parseInt(otlineno) == parseInt(vlineno))){
					expqty = searchrecord[i].getValue('custrecord_expe_qty',null,'sum');
					break;
				}
			}
		}
		
		nlapiLogExecution('ERROR','expqty',expqty);
		
		if(parseFloat(expqty)<parseFloat(CommittedQty))
			display = "true";
		
		nlapiLogExecution('ERROR','CommittedQty1',CommittedQty);
		CommittedQty = (CommittedQty) - (expqty);
nlapiLogExecution('ERROR','CommittedQty2',CommittedQty);
		
		
		
		
		if(CommittedQty !=null && CommittedQty !='' && CommittedQty!=0)
			strQty=CommittedQty;
		if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && (CommittedQty !=null && CommittedQty !='' && CommittedQty !=0))
		{
			var Lotno = searchresults.getLineItemValue('item', 'serialnumbers', m);
			nlapiLogExecution('ERROR','Lotno',Lotno);
			if(Lotno== null || Lotno =="")
				Lotno="";

			var LotnoArray = Lotno.split('');
			if(LotnoArray.length>0)
			{
				nlapiLogExecution('ERROR', 'LotnoArray.length',LotnoArray.length);
				var vitemqty=0;
				var vrecid="" ;
				var vlp="",vlocation="",vqty="",vlotnoWithQty="",vremainqty="",location,vlocationText="";
				var vMultipleLp="",vMultipleQty="",vMultipleTempLot="",vMultipleLocation="",vMultipleRecid="";
				var vTempLot="";
				for(var z=0; z<LotnoArray.length;z++)
				{				
					nlapiLogExecution('ERROR', 'Z',z);
					var vBatch="";
					var vQty="";
					var vLotname="";
					var vLotno="";
					if(LotnoArray.length>1)// for multiple Lotno s
					{
						var vLotNumber=LotnoArray[z];      // ex:ebiz(1),batch(1)
						nlapiLogExecution('ERROR', 'LotNumber',vLotNumber);

						var pos = vLotNumber.indexOf("(")+1; 
						nlapiLogExecution('ERROR', 'pos ', pos);

						vQty=vLotNumber.slice(pos, -1);
						nlapiLogExecution('ERROR', 'vQty', vQty); //ex:1

						vLotname=vLotNumber.slice(0, pos-1);
						nlapiLogExecution('ERROR', 'vLotname', vLotname); //Ex:ebiz

					}
					else
					{
						vLotname=Lotno;
						vQty=strQty;
					}

					//Get Lotno Id;
					if(vLotname!=null && vLotname!='')
					{
						nlapiLogExecution('Debug','Time stamp 7',TimeStampinSec());
						var filterbatch = new Array();
						filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vLotname);
						filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', strItem);
						var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
						nlapiLogExecution('Debug','Time stamp 8',TimeStampinSec());
						if(Lotnosearchresults!=null)
						{
							vLotno= Lotnosearchresults[0].getId();
						}
					}


					//var lineCount = request.getLineItemCount('custpage_deliveryord_items');
					var lineCount= context.getSetting('SCRIPT', 'custscript_linecount');
					nlapiLogExecution('ERROR','lineCount',lineCount);

					vBatch=vLotno;

					var vSkuArray = new Array();
					vSkuArray = context.getSetting('SCRIPT', 'custscript_vskuarray');
					if(vSkuArray !=null && vSkuArray !='' && vSkuArray.length>0)
						vSkuArray =vSkuArray.Split(',');

					var vOldLOTnoArray = new Array();
					vOldLOTnoArray = context.getSetting('SCRIPT', 'custscript_voldlotnoarray');
					if(vOldLOTnoArray !=null && vOldLOTnoArray !='' && vOldLOTnoArray.length>0)
						vOldLOTnoArray =vOldLOTnoArray.Split(',');

					var vNewLOTnoArray = new Array();
					vNewLOTnoArray = context.getSetting('SCRIPT', 'custscript_vnewlotnoarray');
					if(vNewLOTnoArray !=null && vNewLOTnoArray !='' && vNewLOTnoArray.length>0)
						vNewLOTnoArray =vNewLOTnoArray.Split(',');

					var gridLinenoArray= new Array();
					gridLinenoArray = context.getSetting('SCRIPT', 'custscript_gridlinenoarray');
					if(gridLinenoArray !=null && gridLinenoArray !='' && gridLinenoArray.length>0)
						gridLinenoArray =gridLinenoArray.Split(',');

					var vCommentsArray = new Array();
					vCommentsArray = context.getSetting('SCRIPT', 'custscript_vcommentsarray');
					if(vCommentsArray !=null && vCommentsArray !='' && vCommentsArray.length>0)
						vCommentsArray =vCommentsArray.Split(',');

					var str1 = 'vSkuArray.' + vSkuArray + '<br>';
					str1 = str1 + 'vOldLOTnoArray. ' + vOldLOTnoArray + '<br>';	
					str1 = str1 + 'vNewLOTnoArray. ' + vNewLOTnoArray + '<br>';	
					str1 = str1 + 'gridLinenoArray. ' + gridLinenoArray + '<br>';	
					str1 = str1 + 'vCommentsArray. ' + vCommentsArray + '<br>';	
					nlapiLogExecution('DEBUG', 'Lot Values', str1);
					//for(var k=1;k<=lineCount;k++){
					for(var k=0;k<lineCount;k++){
						nlapiLogExecution('ERROR','into for loop',k);	
						var vOldLOTno='';
						var vNewLOTno='';
						var vComments='';

						var vSku = vSkuArray[k];
						if(vOldLOTnoArray !=null && vOldLOTnoArray !='' && vOldLOTnoArray.length>0)
							vOldLOTno =vOldLOTnoArray[k];
						if(vNewLOTnoArray !=null && vNewLOTnoArray !='' && vNewLOTnoArray.length>0)
							vNewLOTno =vNewLOTnoArray[k];
						var gridLineno=gridLinenoArray[k];
						if(vCommentsArray !=null && vCommentsArray !='' && vCommentsArray.length>0)
							vComments = vCommentsArray[k];
						if(vOldLOTno=="" ||vOldLOTno==null)
						{
							vOldLOTno="";
						}
						if(vLotno=="" ||vLotno==null)
						{
							vLotno="";
						}
						if(vNewLOTno=="" ||vNewLOTno==null)
						{
							vNewLOTno="";
						}

						var str = 'vSku.' + vSku + '<br>';
						str = str + 'vNewLOTno. ' + vNewLOTno + '<br>';	
						str = str + 'vOldLOTno. ' + vOldLOTno + '<br>';	
						str = str + 'gridLineno. ' + gridLineno + '<br>';	
						str = str + 'vComments. ' + vComments + '<br>';	

						nlapiLogExecution('DEBUG', 'Lot Values', str);

						if((vNewLOTno!=vLotno) && (gridLineno==vlineno) && (strItem==vSku) && (vOldLOTno==vLotno))
						{
							vBatch=vNewLOTno;
							nlapiLogExecution('ERROR','Lot# s are not same',vBatch);
							break;
						}
//						else
//						{
//						vBatch=vLotno;
//						}

					}
					nlapiLogExecution('Debug','Time stamp 9',TimeStampinSec());
					var fields = ['custitem_item_family', 'custitem_item_group','recordType','custitem_ebizbatchlot'];


					var columns = nlapiLookupField('item', strItem, fields);
					var vItemType="";
					var batchflag="F";
					nlapiLogExecution('Debug','Time stamp 10',TimeStampinSec());
					if (columns != null && columns !='')
					{
						//nlapiLogExecution('ERROR','Hi2 ');
						//var columns = nlapiLookupField(ComponentItemType, strItem, fields);

						nlapiLogExecution('Debug','Time stamp 11',TimeStampinSec());
						var itemfamily = columns.custitem_item_family;
						var itemgroup = columns.custitem_item_group;
						vItemType = columns.recordType;
						batchflag= columns.custitem_ebizbatchlot;
					}
					//avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
					//alert("avlqty " + avlqty);
					//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, avlqty);
					//nlapiLogExecution('ERROR','Hi3 ');

					//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, strQty,vLocation,vBatch);

					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/	
					// to get PickfaceLocaion details for the Item
					nlapiLogExecution('Debug','Time stamp before getPFLocationsForOrder',TimeStampinSec());
					//var pfLocationResults = getPFLocationsForOrder(vWMSlocation,strItem);
					nlapiLogExecution('Debug','Time stamp after getPFLocationsForOrder',TimeStampinSec());
					// PickfaceLocationResults added as a parameter to PickStrategieKittoStockNew function
					var arryinvt=PickStrategieKittoStockNew(strItem,vQty,vLocation,ItemInfoResults,pickrulesarray,vBatch,
							pfLocationResults,vItemType,batchflag);
					nlapiLogExecution('Debug','Time stamp after PickStrategieKittoStockNew',TimeStampinSec());
					/*** up to here ***/
					vitemText=searchresults.getLineItemText('item', 'item', m);
					if(arryinvt.length>0)
					{
						nlapiLogExecution('ERROR','arryinvt.length',arryinvt.length);
						var vLot="";
						var vPrevLot="";
						var vLotQty=0;
						var vPickzone=''
							var vPickStrategy='';
						var vPickMethod='';
						//var vTempLot="";
						var vBoolFound=false;
						for (j=0; j < arryinvt.length; j++) 
						{			
							var invtarray= arryinvt[j];  	

							//vitem =searchresults[i].getValue('memberitem');		
							//vitemText =searchresults[i].getText('memberitem');


							if(vlp == "" || vlp == null)
								vlp = invtarray[2];
							else
							{
								vlp = vlp+","+ invtarray[2];
								vMultipleLp=invtarray[2];
							}
							if(vTempLot == "" || vTempLot == null)
								vTempLot = invtarray[4];
							else
							{
								vTempLot = vTempLot+","+ invtarray[4];
								vMultipleTempLot=invtarray[4];
								nlapiLogExecution('ERROR','vMultipleTempLot',vMultipleTempLot);
							}
							if(vPrevLot==null || vPrevLot== "")
							{
								vPrevLot=invtarray[4];
								vLotQty=parseFloat(invtarray[0]);
								vLot=invtarray[4];
								vBoolFound=true;
							}
							else if(vPrevLot==invtarray[4])
							{
								vLotQty=vLotQty+ parseFloat(invtarray[0]);						
							}
							else if(vPrevLot!=invtarray[4])
							{
								if(vlotnoWithQty == "" || vlotnoWithQty == null)
									vlotnoWithQty = vPrevLot+"(" + vLotQty +")";//invtarray[7] is lotno id
								else
									vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + vPrevLot +"(" + vLotQty +")";

								if(vLot == "" || vLot == null)
									vLot = invtarray[4];
								else
									vLot = vLot+","+ invtarray[4];

								vPrevLot=invtarray[4];
								vLotQty=parseFloat(invtarray[0]);

							}
							if(vqty == "" || vqty == null)
								vqty = invtarray[0];
							else
							{
								vqty = vqty+","+ invtarray[0];
								vMultipleQty=invtarray[0];
							}

							var qty1 = invtarray[0];

							if(vlocationText == "" || vlocationText == null)
								vlocationText = invtarray[6];
							else
								vlocationText = vlocationText+","+ invtarray[6];

							if(vlocation == "" || vlocation == null)
								vlocation = invtarray[1];
							else
							{
								vlocation = vlocation+","+ invtarray[1];
								vMultipleLocation=invtarray[1];
							}

							nlapiLogExecution('ERROR','invtarray[3] ',invtarray[3]);

							if(vrecid == "" || vrecid == null)
								vrecid = invtarray[3];
							else
							{
								vrecid = vrecid+","+ invtarray[3];
								vMultipleRecid=invtarray[3];
							}



							if(vremainqty == "" || vremainqty == null)
								vremainqty = invtarray[5];
							else
								vremainqty = vremainqty+","+ invtarray[5];
							location = invtarray[1];
							vitemqty=parseFloat(vitemqty)+parseFloat(qty1);

							vPickzone=invtarray[8];
							vPickStrategy=invtarray[9];
							vPickMethod=invtarray[10]

							if(vMultipleLp!=""||vMultipleQty!=""||vMultipleTempLot!=""||vMultipleLocation!=""||vMultipleRecid!="")
							{
								nlapiLogExecution('ERROR', 'If_muliple ','sucess');
								var CurrentRow=[vId,vMultipleRecid,vWOId,strItem,vMultipleLp,vMultipleQty,vMultipleTempLot,vMultipleLocation,vWMSlocation,vComments,vlineno,vPickzone,vPickStrategy,vPickMethod,'',display];
								SearchresultsArray.push(CurrentRow);
								nlapiLogExecution('ERROR', 'CurrentRow for multiple ',CurrentRow);

							}
							else
							{
								nlapiLogExecution('ERROR', 'else_muliple','sucess');
								var CurrentRow=[vId,vrecid,vWOId,strItem,vlp,vqty,vTempLot,vlocation,vWMSlocation,vComments,vlineno,vPickzone,vPickStrategy,vPickMethod,'',display];
								SearchresultsArray.push(CurrentRow);
								nlapiLogExecution('ERROR', 'CurrentRow ',CurrentRow);

							}				
						}
						nlapiLogExecution('Debug','Time stamp 15',TimeStampinSec());
						if(vBoolFound==true)
						{
							/*if(vlotnoWithQty == "" || vlotnoWithQty == null)
							vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";//invtarray[7] is lotno id
						else
							vlotnoWithQty = vlotnoWithQty+","+ invtarray[4]+"(" + vLotQty +")";*/

							if(vlotnoWithQty == "" || vlotnoWithQty == null)
								vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";
							else
								vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + invtarray[4]+"(" + vLotQty +")";


							//alert("vTempLot" + vTempLot);
							//alert("vLot.split(',').length" + vLot.split(',').length);
							// alert("vLotQty " + vlotnoWithQty);				
							nlapiLogExecution('ERROR', 'vLot ', vLot);
							nlapiLogExecution('ERROR', 'vlotnoWithQty ', vlotnoWithQty);
							if(vLot.split(',').length>1)
								vLot=vlotnoWithQty;
						}
						nlapiLogExecution('ERROR', 'EndOf for Loop ', 'FORLoopEND');
					}
					else
					{
						// case # 20123356 start
						if(LotnoArray.length>1)
						{
							vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";
							nlapiLogExecution('ERROR', 'if muliple lot#s for Item,no Inventory for ',vitemText);
						}
						// case # 20123356 end				
					}
					nlapiLogExecution('Debug','Time stamp 16',TimeStampinSec());

				}//end of lotnoarray forloop

				if(strQty > vitemqty )			
				{ 
					vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";

				}
				else 
				{

					try { 
						searchresults.setLineItemValue('item','custcol_ebiz_lp',m,vlp);
						searchresults.setLineItemValue('item','custcol_locationsitemreceipt',m, location);//previously it is vlocation
						searchresults.setLineItemValue('item','custcol_ebizwoinventoryref', m,vrecid);
						nlapiLogExecution('ERROR', 'After ', 'After');

						var confirmLotToNS='Y';
						nlapiLogExecution('Debug','Time stamp 17',TimeStampinSec());
						confirmLotToNS=GetConfirmLotToNS(location);
						nlapiLogExecution('Debug','Time stamp 18',TimeStampinSec());
						if(confirmLotToNS=='N')
							vLot=vitemText.replace(/ /g,"-");

						nlapiLogExecution('ERROR', 'vItemType',vItemType);

						nlapiLogExecution('ERROR', 'confirmLotToNS',confirmLotToNS);
						nlapiLogExecution('ERROR', 'vLot',vLot);				 
						//if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T" )
						if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
							searchresults.setLineItemValue('item','serialnumbers',m, vLot);


						/*var newLot = vLot.split(','); 
							newLot = newLot.join(String.fromCharCode(5)); 
							nlapiLogExecution('ERROR', 'Before Set Lot no',m, newLot);
							searchresults.setLineItemValue('item','serialnumbers',m, newLot);*/
						//nlapiLogExecution('ERROR', 'After Set Lot no', newLot);

						//nlapiLogExecution('ERROR', 'invref ', searchresults.getLineItemValue('item', 'custcol_ebizwoinventoryref', m));

						searchresults.setFieldValue('custbody_ebiz_update_confirm','T');
						//if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T") && vTempLot != null && vTempLot != "")
						if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem") && vTempLot != null && vTempLot != "")
							searchresults.setLineItemValue('item','custcol_ebizwolotinfo',m, vTempLot);

						searchresults.setLineItemValue('item','custcol_ebizwoavalqty',m, vqty);
						searchresults.setLineItemValue('item','custcol_ebizwobinloc',m, vlocation);
						searchresults.setLineItemValue('item','custcol_ebizwobinlocvalue',m, vlocationText);						
						searchresults.setLineItemValue('item','custcol_ebizwolotlineno', m,m);

						//nlapiCommitLineItem('item');
						nlapiLogExecution('ERROR', 'linelot ', searchresults.getLineItemValue('item','custcol_ebizwolotlineno',m));


					}
					catch (e) {

						if (e instanceof nlobjError) 
						{
							nlapiLogExecution('ERROR', 'system error', e.getCode()+ '\n' + e.getDetails());

						}

						else 
						{ 
							nlapiLogExecution('ERROR', 'unexpected error', e.toString());
							//alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
//						nlapiCancelLineItem('item');

						nlapiLogExecution('ERROR','Error ', e.toString());
						var ErrorMsg = nlapiCreateError('CannotAllocate',e.toString(), true);
						throw ErrorMsg;

						break;

					}

				}


			}

		}
	}
	if(vAlert!=null && vAlert!='')
	{ 
		/*var vErrorRet=new Array();
		vErrorRet.push('FAIL');
		vErrorRet.push(vAlert);
		return vErrorRet;*/
		return 'FAIL~' + vAlert;
	}
	else
	{
		nlapiLogExecution('ERROR', 'SearchresultsArray',SearchresultsArray.length);
		//nlapiLogExecution('ERROR', 'Before commit ');
		//nlapiLogExecution('Debug','Time stamp 19',TimeStampinSec());
		var CommittedId = nlapiSubmitRecord(searchresults, true);
		nlapiLogExecution('Debug','Time stamp before AllocateInv',TimeStampinSec());
		AllocateInv(vId,SearchresultsArray,vWOId,vwaveno,veBizContLP,ItemInfoResults,pickrulesarray,pfLocationResults);
		nlapiLogExecution('Debug','Time stamp after AllocateInv',TimeStampinSec());
		//nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);
		//nlapiLogExecution('ERROR', 'Committed ', 'Success');
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at End',context.getRemainingUsage());
		return 'SUCCESS';
	}
}
/**To get confirm LOT toNS based on system rule
 * 
 * @param Site
 * @returns Y or N
 */
function GetConfirmLotToNS(Site)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function AllocateInv(vId,SearchresultsArray,vWOName,vwaveno,veBizContLP,ItemInfoResults,pickrulesarray,pfLocationResults)
{
	// Create a Record in Opentask
	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	nlapiLogExecution('Debug','Time stamp 21',TimeStampinSec());
	nlapiLogExecution('Debug','ItemInfoResults',ItemInfoResults);
	nlapiLogExecution('Debug','pickrulesarray',pickrulesarray);
	nlapiLogExecution('Debug','pfLocationResults',pfLocationResults);
	var tempFlag = "F";
	if(SearchresultsArray!=null && SearchresultsArray!='' && SearchresultsArray.length>0)
	{
		nlapiLogExecution('ERROR', 'SearchresultsArray',SearchresultsArray.length);
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		var InvtRecordArray = new Array();
		var OpentaskInternalId = new Array();

		for (var i=0; i<SearchresultsArray.length; i++){
			var vId=SearchresultsArray[i][0];
			var vLineRec=SearchresultsArray[i][1];
			var vWOId=SearchresultsArray[i][2];
			var varsku=SearchresultsArray[i][3];
			var vLP=SearchresultsArray[i][4];
			var vLineQty=SearchresultsArray[i][5];
			var vBatch=SearchresultsArray[i][6];
			var vactLocationtext=SearchresultsArray[i][7];
			var vLocation=SearchresultsArray[i][8];
			var vComments=SearchresultsArray[i][9];
			var vlineno=SearchresultsArray[i][10];
			var vpickzone=SearchresultsArray[i][11];
			var vpickruleId=SearchresultsArray[i][12];
			var vpickmethod=SearchresultsArray[i][13];
			var vInvtResFlag = SearchresultsArray[i][14];
			var display = SearchresultsArray[i][15];

			var str = 'vId.' + vId + '<br>';
			str = str + 'vLineRec.' + vLineRec + '<br>';	
			str = str + 'varsku. ' + varsku + '<br>';	
			str = str + 'vBatch. ' + vBatch + '<br>';	
			str = str + 'vactLocationtext. ' + vactLocationtext + '<br>';	
			str = str + 'vComments. ' + vComments + '<br>';	
			str = str + 'vLineQty. ' + vLineQty + '<br>';	
			str = str + 'vlineno. ' + vlineno + '<br>';	
			str = str + 'vpickzone. ' + vpickzone + '<br>';	
			str = str + 'vpickruleId. ' + vpickruleId + '<br>';	
			str = str + 'vpickmethod. ' + vpickmethod + '<br>';	
			str = str + 'vInvtResFlag. ' + vInvtResFlag + '<br>';
			str = str + 'display. ' + display + '<br>';

			nlapiLogExecution('DEBUG', 'SearchresultsArray values', str);
			if(display == "true"){
				var SkipFlag ="F";
				nlapiLogExecution('Debug','Time stamp 22',TimeStampinSec());
				var scount=1;
				LABL1: for(var k=0;k<scount;k++)
				{
					try
					{
						var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vLineRec);
						nlapiLogExecution('Debug','Time stamp 23',TimeStampinSec());
						var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
						var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
						var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
						var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
						var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');
						var vAvailableQty=transaction.getFieldValue('custrecord_ebiz_avl_qty');


						nlapiLogExecution('ERROR', 'qty', qty);
						nlapiLogExecution('ERROR', 'allocqty', allocqty);
						nlapiLogExecution('ERROR', 'vAvailableQty', vAvailableQty);
						if(allocqty == null || allocqty == '')
							allocqty=0;
						nlapiLogExecution('ERROR', 'varqty', vLineQty);
						if(parseFloat(vAvailableQty)>=parseFloat(vLineQty))
						{
							transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vLineQty)).toFixed(5));
							transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');			 
							transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
							nlapiSubmitRecord(transaction, false, true);

							//capture createinvt internal ids
							var currentArray = new Array(); 
							currentArray = [vLineRec,vLineQty];
							InvtRecordArray.push(currentArray);
						}
						else
						{
							nlapiLogExecution('Debug','Time stamp 1011',TimeStampinSec());
							// if inventory is available in another binlocation / handling  inventory concurrency 
							if(vInvtResFlag == 'T')
							{

								tempFlag =ConcurrencyHandlingForInvt(varsku,vLineQty,vLocation,vlineno,vComments,ItemInfoResults,pickrulesarray,vBatch,pfLocationResults,tempFlag,InvtRecordArray,OpentaskInternalId,vId,vWOName,vwaveno,veBizContLP);
								nlapiLogExecution('Debug','tempFlag',tempFlag);
								if(tempFlag == "F")
									SkipFlag = "T";
							}
							else
							{
								tempFlag ="T";
							}
						}
						nlapiLogExecution('Debug','Time stamp 24',TimeStampinSec());
					}
					catch(ex)
					{
						nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

						var exCode='CUSTOM_RECORD_COLLISION'; 
						var wmsE='Inventory record being updated by another user. Please try again...';
						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}  

						nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}

				if(tempFlag == "T")
				{
					//break the main Forloop, if any of the component item is failed for location generation. 
					break;
				}

				if(vWOName == null || vWOName == '')
				{
					var rcptrecordid3 = nlapiLoadRecord('workorder', vId);

					var woidWO= rcptrecordid3.getFieldValue('tranid');
					nlapiLogExecution('ERROR','woidWO  ',woidWO);
					vWOId=woidWO;
				}
				else
					vWOId=vWOName;
				nlapiLogExecution('Debug','Time stamp 25',TimeStampinSec());
				nlapiLogExecution('Debug','SkipFlag',SkipFlag);

				//if SkipFlag ="T" means open task record is created in concurrency function no need to create again.

				if(tempFlag == "F" && SkipFlag != "T")
				{

					var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
					customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
					//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
					customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

					//customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					//customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					//customrecord.setFieldValue('custrecord_expe_qty', varqty); 
					//nlapiLogExecution('ERROR','expe qty ',vLineQty);
					nlapiLogExecution('ERROR','binlocation',vactLocationtext);

					customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vLineQty).toFixed(5));  //Commented and added to resovle decimal issue
					customrecord.setFieldValue('custrecord_act_qty', parseFloat(vLineQty).toFixed(5));
					customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process


					customrecord.setFieldValue('custrecord_actbeginloc',vactLocationtext);	
					//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
					//customrecord.setFieldValue('custrecord_lpno', vlp); 
					customrecord.setFieldValue('custrecord_upd_date', DateStamp());
					//customrecord.setFieldValue('custrecord_sku', vskuText);
					customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
					customrecord.setFieldValue('name', vWOId);		

					customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
					customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
					//uncommented on 22/01/15
					//customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
					customrecord.setFieldValue('custrecord_ebiz_wave_no', vwaveno);
					customrecord.setFieldValue('custrecord_ebiz_order_no', vId);
					customrecord.setFieldValue('custrecord_batch_no', vBatch);
					customrecord.setFieldValue('custrecord_ebiz_cntrl_no', vId);
					//customrecord.setFieldValue('custrecord_ebiz_cntrl_no', rcptrecordid);

					customrecord.setFieldValue('custrecord_sku', varsku);
					customrecord.setFieldValue('custrecord_line_no', parseFloat(vlineno));
					customrecord.setFieldValue('custrecord_lpno', vLP);
					customrecord.setFieldValue('custrecord_packcode', vPackcode);					
					customrecord.setFieldValue('custrecord_sku_status', vSKUStatus);
					customrecord.setFieldValue('custrecord_wms_location', vLocation);
					customrecord.setFieldValue('custrecord_invref_no', vLineRec);
					customrecord.setFieldValue('custrecord_notes', vComments);	
					customrecord.setFieldValue('custrecord_ebizrule_no', vpickruleId);
					customrecord.setFieldValue('custrecord_ebizmethod_no', vpickmethod);
					customrecord.setFieldValue('custrecord_ebizzone_no', vpickzone);

					if(vpickzone !=null && vpickzone !='')
					{
						customrecord.setFieldValue('custrecord_ebiz_zoneid', vpickzone);
					}
					nlapiLogExecution('DEBUG', 'vwaveno',vwaveno);
					if(vwaveno != null && vwaveno != '')
					{
						customrecord.setFieldValue('custrecord_ebiz_wave_no', parseFloat(vwaveno));
					}
					nlapiLogExecution('DEBUG', 'veBizContLP',veBizContLP);
					if(veBizContLP != null && veBizContLP != '')
					{
						customrecord.setFieldValue('custrecord_container_lp_no', veBizContLP);
					}

					nlapiLogExecution('DEBUG', 'beginLocationId',vactLocationtext);
					if(vactLocationtext!=null && vactLocationtext!='')
					{
						nlapiLogExecution('Debug','Time stamp 26',TimeStampinSec());
						var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
						var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', vactLocationtext, locgroupfields);
						nlapiLogExecution('Debug','Time stamp 27',TimeStampinSec());
						var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
						var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;
						nlapiLogExecution('DEBUG', 'inblocgroupid',inblocgroupid);
						var locgroupseqfields = ['custrecord_sequenceno'];
						//code added on 011012 by suman
						//Allow to insert sequence no only if we get some value form inbonlog group Id.
						//if(inblocgroupid!=null&&inblocgroupid!="")
						if(oublocgroupid!=null && oublocgroupid!="")
						{
							nlapiLogExecution('Debug','Time stamp 28',TimeStampinSec());
							//var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', inblocgroupid, locgroupseqfields);
							var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
							var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;
							nlapiLogExecution('DEBUG', 'locgroupidseq',locgroupidseq);

							customrecord.setFieldValue('custrecord_bin_locgroup_seq', locgroupidseq);
							nlapiLogExecution('Debug','Time stamp 29',TimeStampinSec());
						}
					}

					var expiryDateInLot='';
					if(vBatch!=null && vBatch !=''){
						var filterbatch = new Array();
						filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vBatch);
						filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varsku);
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebizexpirydate'); 
						var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
						if(Lotnosearchresults!=null && Lotnosearchresults!='')
						{
							nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
							vLotno= Lotnosearchresults[0].getId();
							expiryDateInLot= Lotnosearchresults[0].getValue('custrecord_ebizexpirydate');
						}
						nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
						if(expiryDateInLot !=null && expiryDateInLot!='')
							customrecord.setFieldValue('custrecord_expirydate', expiryDateInLot);

					}

					nlapiLogExecution('Debug','Time stamp 30',TimeStampinSec());
					//nlapiLogExecution('ERROR','eBizWaveNo ',eBizWaveNo);


					var OpentaskId= nlapiSubmitRecord(customrecord);			
					nlapiLogExecution('Debug','Time stamp 31',TimeStampinSec());
					nlapiLogExecution('ERROR', 'Success');

					if(OpentaskId !=null && OpentaskId !='')
					{
						nlapiLogExecution('ERROR', 'OpentaskId',OpentaskId);
						OpentaskInternalId.push(OpentaskId);
					}
				}
			}

		}
		
		
		nlapiLogExecution('ERROR', 'tempFlag',tempFlag);


		if(tempFlag == "T")
			ComponentItemsReversal(InvtRecordArray,OpentaskInternalId);


	}
	
	nlapiLogExecution('ERROR', 'tempFlag',tempFlag);
	if(tempFlag == "F")
	{
		nlapiLogExecution('ERROR','Bin location generation was sucessfully',vWOId);
		/*var WOarray = new Array();
		nlapiLogExecution('ERROR', 'vId',vId);
		WOarray["custparam_wo"] = vId;
		response.sendRedirect('SUITELET', 'customscript_ebiz_bom_report_sl', 'customdeploy_ebiz_bom_report_di', false,WOarray);
		return false;*/
	}
	else
	{

		nlapiLogExecution('ERROR','Allocation Failed ');
		nlapiLogExecution('ERROR','Bin location generation is failed for this Work Order ',vWOId);
		/*var ErrorMsg = nlapiCreateError('CannotAllocate','Bin location generation is failed for this Work Order', true);
		throw ErrorMsg;*/
	}
	/*var WOarray = new Array();
	nlapiLogExecution('ERROR', 'vId',vId);
	WOarray["custparam_wo"] = vId;
	response.sendRedirect('SUITELET', 'customscript_ebiz_bom_report_sl', 'customdeploy_ebiz_bom_report_di', false,WOarray);
	return false;*/


}


function PickStrategieKittoStock(item,itemfamily,itemgroup,avlqty,location,serialNo){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);
	nlapiLogExecution('ERROR','location',location);
	nlapiLogExecution('ERROR','serialNo',serialNo);

	var filters = new Array();	

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]));

	if (itemgroup != "" && itemgroup != null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul',null,  'anyof', ['@NONE@',itemgroup]));
	}
	if (itemfamily!= "" && itemfamily!= null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@',itemfamily]));
	}

	if (location!= "" && location!= null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@',location]));
	}


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		var searchresultpick = searchresults[i];				
		var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

		nlapiLogExecution('ERROR', "LP: " + LP);
		nlapiLogExecution('ERROR','PickZone',vpickzone);			
		var filterszone = new Array();	
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', ['@NONE@',vpickzone]));
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone,null));
		filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',location]));

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR','Loc Group Fetching');

		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup'+ searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));

			var filtersinvt = new Array();

			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
			filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));

			if (location!= "" && location!= null) {
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));
			}
			//code added by santosh on 13aug12
			if (serialNo!= "" && serialNo!= null) {
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', serialNo));
			}
			//end of the code on 13Aug12
			nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno'); 
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_qoh');			
			columnsinvt[7] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[4].setSort();
			columnsinvt[5].setSort();
			columnsinvt[6].setSort(false);

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
				var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
				var Recid = searchresult.getId(); 


				//alert(" 664 actqty:" +actqty);
				//alert(" 664 allocqty:" +allocqty);
				//alert(" 664 LP:" +LP);
				//alert(" 664 vactLocation:" +vactLocation);
				//alert(" 664 vactLocationtext:" +vactLocationtext);
				//alert(" 664 vlotno:" +vlotno);
				//alert(" 664 vlotText:" +vlotText);
				//alert(" 664 Recid:" +Recid);


				nlapiLogExecution('ERROR', 'Recid1', Recid);
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if( parseFloat(allocqty)<0)
				{
					allocqty=0;
				}
				if( parseFloat(actqty)<0)
				{
					actqty=0;
				}
				nlapiLogExecution('ERROR', "vactLocation: " + vactLocation);
				nlapiLogExecution('ERROR', "vactLocationtext: " + vactLocationtext);
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);
				var remainqty=0;

				//Added by Ganesh not to allow negative or zero Qtys 
				if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
					remainqty = parseFloat(actqty) - parseFloat(allocqty);


				var cnfmqty;

////				alert("remainqty"+remainqty );
////				alert("avlqty"+avlqty );
				if (parseFloat(remainqty) > 0) {

					////alert("parseFloat(avlqty) "+parseFloat(avlqty));

					if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
						cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
						actallocqty = avlqty;
						////alert('cnfmqty in if '+cnfmqty);
					}
					else {
						cnfmqty = remainqty;
						////alert('cnfmqty2 ' +cnfmqty);
						actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
					}

					//alert(" 708 cnfmqty:" +cnfmqty);
					//alert(" 709 actallocqty:" +actallocqty);

					////alert('BeforeIf ' +cnfmqty);
					if (parseFloat(cnfmqty) > 0) {
						////alert('AfterIf ' +cnfmqty);
						//Resultarray
						var invtarray = new Array();
						////alert('cnfmqty3 ' +cnfmqty);
						invtarray[0]= cnfmqty;
						////alert('cnfmqty4 ' +cnfmqty);
						invtarray[1]= vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = vlotText;
						invtarray[5] =remainqty;
						invtarray[6] =vactLocationtext;
						invtarray[7] =vlotno;
						//alert("cnfmqty "+cnfmqty);
						//alert("cnfmqty " + cnfmqty);
						//alert(vlotno);
						//alert(vlotText);
						nlapiLogExecution('ERROR', 'RecId:',Recid);
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);								
					}


				}

				if ((avlqty - actallocqty) == 0) {

					return Resultarray;

				}

			} 
		}
	}

	return Resultarray;  
}

/*code merged from monobind on feb 27th 2013 by Radhika added kititemstatus filter condition */

function GetLotname(id)
{
	var batchname="";
	var filterbatch = new Array();
	filterbatch[0] = new nlobjSearchFilter('InternalId', null, 'is', id);

	var columns=new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('InternalId');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
	if(searchresults!=null)
	{
		batchname= searchresults[0].getValue('name');
		nlapiLogExecution('ERROR', 'NewBatchno', batchname);
		return batchname;
	}

}

var allocatedInv=new Array();
//added pickfacelocationresults as a parameter
function PickStrategieKittoStockNew(item,avlqty,location,ItemInfoArr,pickrulesarray,serialNo,pfLocationResults,vItemType,
		batchflag){
	nlapiLogExecution('ERROR','Into PickStrategieKittoStockNew');
	var actallocqty = 0;
	var Resultarray = new Array();
	/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/	
	var PfLocArray =new Array();
	/*** up to here ***/

	var ItemFamily;
	var ItemGroup;

	if(ItemInfoArr!= null && ItemInfoArr != '' )
	{
		nlapiLogExecution('ERROR', 'ItemInfoArr.length', ItemInfoArr.length);
		for(var p=0;p<ItemInfoArr.length;p++)
		{
			if(ItemInfoArr[p].getId()==item)
			{
				ItemFamily = ItemInfoArr[p].getValue('custitem_item_family');
				ItemGroup = ItemInfoArr[p].getValue('custitem_item_group');

			}	
		}	
	}

	var str = 'item.' + item + '<br>';
	str = str + 'ItemInfoArr.' + ItemInfoArr + '<br>';	
	str = str + 'ItemFamily.' + ItemFamily + '<br>';	
	str = str + 'ItemGroup. ' + ItemGroup + '<br>';	
	str = str + 'avlqty. ' + avlqty + '<br>';	
	str = str + 'location. ' + location + '<br>';
	str = str + 'pickrulesarray. ' + pickrulesarray + '<br>';
	str = str + 'serialNo. ' + serialNo + '<br>';
	str = str + 'pfLocationResults. ' + pfLocationResults + '<br>';
	str = str + 'vItemType. ' + vItemType + '<br>';
	str = str + 'batchflag. ' + batchflag + '<br>';

	nlapiLogExecution('DEBUG', 'Function parameters', str);

	nlapiLogExecution('Debug','Time stamp 101',TimeStampinSec());

	var filterStatus = new Array();

	filterStatus.push(new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T'));
	filterStatus.push(new nlobjSearchFilter('custrecord_activeskustatus', null, 'is','T'));
	if(location != null && location != '')
		filterStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',location));
	var searchresultsStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filterStatus, null);
	nlapiLogExecution('Debug','Time stamp 102',TimeStampinSec());
	var vStatusArr=new Array();
	if(searchresultsStatus != null && searchresultsStatus != '')
	{
		for(var k=0;k<searchresultsStatus.length;k++)
		{
			vStatusArr.push(searchresultsStatus[k].getId());
		}

		nlapiLogExecution('ERROR', 'vStatusArr',vStatusArr);
	}	

	nlapiLogExecution('Debug','Time stamp 103',TimeStampinSec());


	var vPickRules=new Array();
	if(pickrulesarray != null && pickrulesarray != '' && pickrulesarray.length>0)
	{
		nlapiLogExecution('ERROR', 'pickrulesarray.length', pickrulesarray.length);
		for(var j = 0; j < pickrulesarray.length; j++)
		{
			if((pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') ==ItemFamily))	
			{
				if((pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') ==ItemGroup))	
				{
					if((pickrulesarray[j].getValue('custrecord_ebizskupickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskupickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskupickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskupickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskupickrul') ==item))	
					{
						vPickRules.push(j);
					}	
				}	
			}	
		}	
	}

	nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());



	if (vPickRules != null) {
		nlapiLogExecution('ERROR', 'rules count', vPickRules.length);

		var vRemainingQty=avlqty;
		nlapiLogExecution('ERROR', 'vRemainingQty', vRemainingQty);
		for (var s = 0; s < parseFloat(vPickRules.length) && parseFloat(vRemainingQty) > 0; s++) {
			var vPickZoneArr=new Array();

			var vpickzone=pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickzonerul');
			var pickRuleId = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickmethod');
			var pickBinlocGroup = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizlocationgrouppickrul');
			var vBinloction = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizlocationpickrul');
			var pickmethodpickface = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');

			var pfBinLocationgroupId=new Array();
			if ((pickmethodpickface == 'T') && (pfLocationResults != null && pfLocationResults.length > 0))
			{
				for(var p = 0; p < pfLocationResults.length; p++)
				{
					var vPFSKU=pfLocationResults[p].getValue('custrecord_pickfacesku');
					if(item == vPFSKU)
						pfBinLocationgroupId.push(pfLocationResults[p].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc'));
				}
			}

			var str = 'vPickRules[s].' + vPickRules[s] + '<br>';
			str = str + 'vRemainingQty.' + vRemainingQty + '<br>';	
			str = str + 'Id.' + pickrulesarray[vPickRules[s]].getId() + '<br>';	
			str = str + 'vpickzone. ' + vpickzone + '<br>';	
			str = str + 'pickRuleId. ' + pickRuleId + '<br>';	
			str = str + 'pickMethodId. ' + pickMethodId + '<br>';
			str = str + 'pickBinlocGroup. ' + pickBinlocGroup + '<br>';
			str = str + 'vBinloction. ' + vBinloction + '<br>';
			str = str + 'pickmethodpickface. ' + pickmethodpickface + '<br>';
			str = str + 'pfBinLocationgroupId. ' + pfBinLocationgroupId + '<br>';

			nlapiLogExecution('DEBUG', 'Pick strategy parameters', str);

			vPickZoneArr.push(vpickzone);
			//}	
			//}

			var vLocGrpArr=new Array();
			if(vPickZoneArr != null && vPickZoneArr != '')
			{
				nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());
				if(pickBinlocGroup ==null || pickBinlocGroup =='')
					vLocGrpArr=GetAllLocGroups(vPickZoneArr,location,-1);
				else 
					vLocGrpArr.push(pickBinlocGroup);
				nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());

			}
			else if(pickBinlocGroup !=null && pickBinlocGroup !='')
				vLocGrpArr.push(pickBinlocGroup);	


			var searchresultsinvt;
			var InvtresultsFlag ='F';
			if((vLocGrpArr != null && vLocGrpArr != '' && vLocGrpArr.length>0) ||(vBinloction != null && vBinloction != ''))
			{
				searchresultsinvt=GetAllInventory(item,vLocGrpArr,location,serialNo,vStatusArr,vItemType,batchflag,vBinloction,pfBinLocationgroupId);
				nlapiLogExecution('Debug','Time stamp 105',TimeStampinSec());

			}
			if(searchresultsinvt != null && searchresultsinvt != '')
			{
				//Updating invt flag to'T' ,if invt available in multiple bin location
				if(searchresultsinvt != null && searchresultsinvt != '' && searchresultsinvt.length>1)
					InvtresultsFlag ='T';			
				
				
				/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
				//consider first Pickface location to fetch the Inventory for the Item
				if(pickmethodpickface == 'T')
				{
					if (pfLocationResults != null && pfLocationResults.length > 0)
					{
						var remainingQuantity=avlqty;
						//

						nlapiLogExecution('ERROR', 'Within PF Location Result search');
						nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
						nlapiLogExecution('ERROR', 'pfLocationResults.length', pfLocationResults.length);

						for(var z = 0; z < pfLocationResults.length; z++)
						{
							if(parseFloat(remainingQuantity) > 0)
							{
								var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
								var Autoreplenflag=pfLocationResults[z].getValue('custrecord_autoreplen');

								if (pfItem == item)
								{
									nlapiLogExecution('ERROR', 'item Match',pfItem);
									pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
									var pfBinLocationText = pfLocationResults[z].getText('custrecord_pickbinloc');
									pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
									pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
									pffixedlp = pfLocationResults[z].getValue('custrecord_pickface_ebiz_lpno');
									pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
									replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
									var pfBinLocationGroup = pfLocationResults[z].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
									var pfzone=pfLocationResults[z].getValue('custrecord_pickzone');

									var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
									str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';	
									str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
									str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
									str = str + 'pfstatus. = ' + pfstatus + '<br>';
									str = str + 'replenrule. = ' + replenrule + '<br>';
									str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
									str = str + 'pfBinLocationGroup. = ' + pfBinLocationGroup + '<br>';
									str = str + 'pfzone. = ' + pfzone + '<br>';

									nlapiLogExecution('ERROR', 'Pickface Details', str);

									PfLocArray.push(pfBinLocationId);

									if(parseFloat(pfMaximumPickQuantity) >= parseFloat(remainingQuantity))
									{

										var availableQty ="";
										var allocQty = "";
										var recordId = "";

										var itemStatus = "";
										var fromLP = "";
										var inventoryItem="";
										var fromLot="";
										// Get inventory details for this pickface location
										nlapiLogExecution('Debug','Time stamp 106',TimeStampinSec());
										var binLocnInvDtls = getAvailQtyFromInventory(searchresultsinvt, pfBinLocationId, allocatedInv,location,pfItem);
										nlapiLogExecution('Debug','Time stamp 107',TimeStampinSec());
										var fromLotId;
										if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
										{
											nlapiLogExecution('Debug','binLocnInvDtls',binLocnInvDtls.length);

											for(var bincount=0;bincount<binLocnInvDtls.length;bincount++)
											{
												if(parseFloat(remainingQuantity) > 0)
												{
													availableQty = binLocnInvDtls[bincount][2];
													allocQty = binLocnInvDtls[bincount][3];
													recordId = binLocnInvDtls[bincount][8];
													if(binLocnInvDtls[bincount][4]!=null && binLocnInvDtls[bincount][4]!='')
														packCode = binLocnInvDtls[bincount][4];
													itemStatus = binLocnInvDtls[bincount][5];										
													whLocation = binLocnInvDtls[bincount][6];
													fromLP = binLocnInvDtls[bincount][7];											
													inventoryItem = binLocnInvDtls[bincount][9];
													fromLot = binLocnInvDtls[bincount][10];
													expiryDate = binLocnInvDtls[bincount][11];
													fromLotId = binLocnInvDtls[bincount][12];
													nlapiLogExecution('ERROR', 'binLocnInvDtls[0][10] For Batch',binLocnInvDtls[bincount][10]);
//													}
													/*
													 * If the allocation strategy is not null check in inventory for that item.						 
													 * 		if the allocation strategy = min quantity on the pallet
													 * 			search for the inventory where the inventory has the least quantity
													 * 		else
													 * 			search for the inventory where the inventory has the maximum quantity 
													 * 			on the pallet 
													 */

													var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
													str = str + 'availableQty. = ' + availableQty + '<br>';	

													nlapiLogExecution('ERROR', 'Item Status', str);

													//if(parseFloat(availableQty) > 0 && itemStatus== pfstatus)
													if(parseFloat(availableQty) > 0)
													{
														nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
														// allocate to this bin location
														var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
														var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
														remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);
														vRemainingQty=remainingQuantity;
														//nlapiLogExecution('ERROR', 'actualAllocQty', actualAllocQty);
														//nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
														var expectedQuantity = parseFloat(actualAllocQty);

														var contsize="";								
														//var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
														//var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

														// Allocate the quantity to the pickface location if the allocation quantity is 
														// 	less than or equal to the pick face maximum quantity
														//nlapiLogExecution('ERROR', 'actualAllocQty', actualAllocQty);
														//nlapiLogExecution('ERROR', 'pfMaximumPickQuantity', pfMaximumPickQuantity);

														var str = 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
														str = str + 'actualAllocQty. = ' + actualAllocQty + '<br>';	
														str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	

														nlapiLogExecution('ERROR', 'Qty Details Status', str);

														if(actualAllocQty <= pfMaximumPickQuantity)
														{
															//1-Ship Cartons	2-Build Cartons		3-Ship Pallets

															var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
															allocatedInv.push(allocatedInvRow);
															//PfLocArray.push(pfBinLocationId);
														}
														if (parseFloat(actualAllocQty) > 0) {
															////alert('AfterIf ' +cnfmqty);
															//Resultarray

															nlapiLogExecution('Debug','Time stamp 108',TimeStampinSec());

															if(pfzone==null || pfzone=='')
															{

																var filtersLocGrp = new Array();
																filtersLocGrp.push(new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof', pfBinLocationGroup));
																var columnsLocGrp = new Array();
																columnsLocGrp[0] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
																var SearchLocGrp = nlapiSearchRecord('customrecord_zone_locgroup', null, filtersLocGrp, columnsLocGrp);
																nlapiLogExecution('ERROR', 'vpickzone b', vpickzone);
																if(SearchLocGrp != null && SearchLocGrp != '')
																{
																	pfzone = SearchLocGrp[0].getValue('custrecordcustrecord_putzoneid');
																}
															}

															nlapiLogExecution('ERROR', 'pfzone a', pfzone);


															var invtarray = new Array();
															////alert('cnfmqty3 ' +cnfmqty);
															invtarray[0]= actualAllocQty;
															////alert('cnfmqty4 ' +cnfmqty);
															invtarray[1]= pfBinLocationId;
															invtarray[2] = fromLP;
															invtarray[3] = recordId;
															invtarray[4] = fromLot;
															invtarray[5] =availableQty;
															invtarray[6] =pfBinLocationText;
															invtarray[7] =fromLotId;
															invtarray[8] =pfzone;
															invtarray[9] =pickRuleId;
															invtarray[10] =pickMethodId;
															invtarray[11] =InvtresultsFlag;
															//alert("cnfmqty "+cnfmqty);
															//alert("cnfmqty " + cnfmqty);
															//alert(vlotno);
															//alert(vlotText);
															nlapiLogExecution('ERROR', 'RecId:',Recid);
															nlapiLogExecution('Debug','Time stamp 109',TimeStampinSec());
															//invtarray[3] = vpackcode;
															//invtarray[4] = vskustatus;
															//invtarray[5] = vuomid;
															Resultarray.push(invtarray);
															if (parseFloat(remainingQuantity) == 0) {
																nlapiLogExecution('ERROR', 'remainingQuantity in jump statement:',remainingQuantity);
															}
														}
													}
													nlapiLogExecution('ERROR','remainingQuantity',remainingQuantity);
												}
											}
											if (parseFloat(remainingQuantity) <= 0)
												return Resultarray;
										}
									}
								}
							}
						}
					}
				}
				/*** Up to here ***/
				for (var l = 0; l < searchresultsinvt.length; l++) {

					var searchresult = searchresultsinvt[l];
					var actqty = searchresult.getValue('custrecord_ebiz_qoh');
					var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
					var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
					var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
					var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
					var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
					var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
					var Recid = searchresult.getId(); 

					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, vactLocation,LP,item,vlotText);

					nlapiLogExecution('ERROR', 'alreadyAllocQty', alreadyAllocQty);
					nlapiLogExecution('ERROR', 'actqty', actqty);

					actqty = parseInt(actqty) - parseInt(alreadyAllocQty);

					nlapiLogExecution('ERROR', 'actqty', actqty);

					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
					//to fetch remaining Invt from bulk Location
					if(remainingQuantity != null && remainingQuantity != '' && parseFloat(remainingQuantity)>0)
					{
						avlqty=remainingQuantity;
						vRemainingQty=remainingQuantity;
					}
					/*** up to here ***/
					//alert(" 664 actqty:" +actqty);
					//alert(" 664 allocqty:" +allocqty);
					//alert(" 664 LP:" +LP);
					//alert(" 664 vactLocation:" +vactLocation);
					//alert(" 664 vactLocationtext:" +vactLocationtext);
					//alert(" 664 vlotno:" +vlotno);
					//alert(" 664 vlotText:" +vlotText);
					//alert(" 664 Recid:" +Recid);
					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
					//to restrict pickface Location for the Item
					var vBulkLocProceed='T';
					if(PfLocArray.length>0)
					{
						if(PfLocArray.indexOf(vactLocation)!=-1)
							vBulkLocProceed='F';
					}
					/*** up to here ***/

					nlapiLogExecution('ERROR', 'Recid1', Recid);
					if(vBulkLocProceed=='T')
					{
						if (isNaN(allocqty)) {
							allocqty = 0;
						}
						if (allocqty == "") {
							allocqty = 0;
						}
						if( parseFloat(allocqty)<0)
						{
							allocqty=0;
						}
						if( parseFloat(actqty)<0)
						{
							actqty=0;
						}

						var str = 'vactLocation. = ' + vactLocation + '<br>';
						str = str + 'vactLocationtext. = ' + vactLocationtext + '<br>';	
						str = str + 'allocqty. = ' + allocqty + '<br>';
						str = str + 'LP. = ' + LP + '<br>';
						str = str + 'vlotText. = ' + vlotText + '<br>';
						str = str + 'allocqty. = ' + allocqty + '<br>';
						str = str + 'actqty. = ' + actqty + '<br>';
						str = str + 'vpickzone. = ' + vpickzone + '<br>';

						nlapiLogExecution('ERROR', 'Bulk Loc Details', str);


						var remainqty=0;
						//Added by Ganesh not to allow negative or zero Qtys 
						if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0 && (parseFloat(actqty)-parseFloat(allocqty))>0)
						{
							remainqty = parseFloat(actqty) - parseFloat(allocqty);
							vRemainingQty=remainqty;
						}
						var cnfmqty;
						nlapiLogExecution('ERROR', "vRemainingQty: ",vRemainingQty );
						//nlapiLogExecution('ERROR', "remainqty: ",remainqty );
////						alert("remainqty"+remainqty );
////						alert("avlqty"+avlqty );
						if (parseFloat(remainqty) > 0) {
							////alert("parseFloat(avlqty) "+parseFloat(avlqty));
							if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
								cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
								actallocqty = avlqty;
								////alert('cnfmqty in if '+cnfmqty);
							}
							else {
								cnfmqty = remainqty;
								////alert('cnfmqty2 ' +cnfmqty);
								actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
							}
							//alert(" 708 cnfmqty:" +cnfmqty);
							//alert(" 709 actallocqty:" +actallocqty);
							////alert('BeforeIf ' +cnfmqty);
							if (parseFloat(cnfmqty) > 0) {
								////alert('AfterIf ' +cnfmqty);
								//Resultarray
								var invtarray = new Array();
								////alert('cnfmqty3 ' +cnfmqty);
								invtarray[0]= cnfmqty;
								////alert('cnfmqty4 ' +cnfmqty);
								invtarray[1]= vactLocation;
								invtarray[2] = LP;
								invtarray[3] = Recid;
								invtarray[4] = vlotText;
								invtarray[5] =remainqty;
								invtarray[6] =vactLocationtext;
								invtarray[7] =vlotno;
								invtarray[8] =vpickzone;
								invtarray[9] =pickRuleId;
								invtarray[10] =pickMethodId;
								invtarray[11] =InvtresultsFlag;
								//alert("cnfmqty "+cnfmqty);
								//alert("cnfmqty " + cnfmqty);
								//alert(vlotno);
								//alert(vlotText);
								nlapiLogExecution('ERROR', 'RecId:',Recid);
								//invtarray[3] = vpackcode;
								//invtarray[4] = vskustatus;
								//invtarray[5] = vuomid;
								Resultarray.push(invtarray);								
							}
						}
						if ((avlqty - actallocqty) == 0) {
							return Resultarray;
						}
					} 
				}
			}
		}
	}
	nlapiLogExecution('Debug','Time stamp 110',TimeStampinSec());
	return Resultarray;  
}

var vAllGrps=new Array();
function GetAllLocGroups(vpickzone,location,maxno){

	nlapiLogExecution('Debug','Into GetAllLocGroups',TimeStampinSec());

	var str = 'vpickzone.' + vpickzone + '<br>';
	str = str + 'location.' + location + '<br>';	
	str = str + 'maxno. ' + maxno + '<br>';	

	nlapiLogExecution('DEBUG', 'GetAllLocGroups Parameters', str);

	if(vpickzone!= null && vpickzone !='')
		vpickzone.push('@NONE@');

	var filterszone = new Array();
	filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', vpickzone));

	filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',location]));
	if(maxno!=-1)
	{

		filterszone.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
	}
	else
		vAllGrps=new Array();

	var columnzone = new Array();
	columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columnzone[1]=new nlobjSearchColumn('internalid');
	columnzone[1].setSort(true);

	var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
	nlapiLogExecution('ERROR','Loc Group Fetching');
	if(searchresultszone != null && searchresultszone != '')
	{	
		for(var k=0;k<searchresultszone.length;k++)
		{
			vAllGrps.push(searchresultszone[k].getValue('custrecord_locgroup_no'));
		}
		if(searchresultszone!=null && searchresultszone.length>=1000)
		{ 
			var maxno=searchresultszone[searchresultszone.length-1].getValue('internalid');		
			GetAllLocGroups(vpickzone,location,maxno); 
		}
	}

	nlapiLogExecution('Debug','Out of GetAllLocGroups',TimeStampinSec());
	return vAllGrps;
}


var vAllInv=new Array();
function GetAllInventory(item,vlocgroupno,location,serialNo,vStatusArr,vItemType,batchflag,binlocation,
		pfBinLocationgroupId){

	nlapiLogExecution('Debug','Into GetAllInventory',TimeStampinSec());

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));

	if((vlocgroupno != null && vlocgroupno != '') && (pfBinLocationgroupId!=null && pfBinLocationgroupId !=''))
	{

		for(var j=0;j<pfBinLocationgroupId.length;j++)
			vlocgroupno.push(pfBinLocationgroupId[j]);

		filtersinvt.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', vlocgroupno));
	}

	if((vlocgroupno != null && vlocgroupno != '') && (pfBinLocationgroupId ==null || pfBinLocationgroupId ==''))
	{

		filtersinvt.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', vlocgroupno));
	}

	if(binlocation != null && binlocation != '')
	{

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof', binlocation));
	}
	if (location!= "" && location!= null) {

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));
	}

	//code added by santosh on 13aug12
	if (serialNo!= "" && serialNo!= null) {

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', serialNo));
	}

	if (vStatusArr!= "" && vStatusArr!= null) {

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', vStatusArr));
	}

	//end of the code on 13Aug12
	if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{

		var expdate=DateStamp();
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'after', expdate));
		//end of the code on 13Aug12

	}

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));

	nlapiLogExecution('ERROR', "vStatusArr:  :  vItemType  : batchflag :pfBinLocationgroupId " , vStatusArr + ' : ' + vItemType + ' : ' + batchflag + ' : ' +pfBinLocationgroupId );
	nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku:  :  vlocgroupno  : binlocation  :   serialNo " , item + ' : ' + vlocgroupno + ' : ' + binlocation + ' : ' + serialNo );
	nlapiLogExecution('ERROR', "vlocgroupno.length:  :  vStatusArr.length " , vlocgroupno.length + ' : ' + vStatusArr.length );


	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columnsinvt[1]=new nlobjSearchColumn('internalid');
	columnsinvt[0].setSort();
	columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columnsinvt[5] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columnsinvt[7] = new nlobjSearchColumn('custrecord_pickseqno'); 
	columnsinvt[8] = new nlobjSearchColumn('custrecord_ebiz_qoh');			
	columnsinvt[9] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
	columnsinvt[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columnsinvt[11] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columnsinvt[12] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columnsinvt[13] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	/*** up to here ***/	
	columnsinvt[6].setSort();
	columnsinvt[7].setSort();
	columnsinvt[8].setSort(false);

	var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

	nlapiLogExecution('Debug','Out of GetAllInventory',TimeStampinSec() + " : " + searchresultsinvt);

	return searchresultsinvt;
}



/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/

function getAvailQtyFromInventory(inventorySearchResults, binLocation, allocatedInv,dowhlocation,fulfilmentItem){

	nlapiLogExecution('ERROR','Into getAvailQtyFromInventory',TimeStampinSec());

	var str = 'inventorySearchResults.' + inventorySearchResults + '<br>';
	str = str + 'binLocation.' + binLocation + '<br>';	
	str = str + 'allocatedInv. ' + allocatedInv + '<br>';	
	str = str + 'dowhlocation. ' + dowhlocation + '<br>';	
	str = str + 'fulfilmentItem. ' + fulfilmentItem + '<br>';	

	nlapiLogExecution('DEBUG', 'getAvailQtyFromInventory Parameters', str);

	var binLocnInvDtls = new Array();
	var matchFound = false;

	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		nlapiLogExecution('ERROR','inventorySearchResults length',inventorySearchResults.length);
		for(var i = 0; i < inventorySearchResults.length; i++){
			if(!matchFound){
				invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
				var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
				var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');

				var str = 'dowhlocation. = ' + dowhlocation + '<br>';
				str = str + 'InvWHLocation. = ' + InvWHLocation + '<br>';	
				str = str + 'invBinLocnId. = ' + invBinLocnId + '<br>';
				str = str + 'binLocation. = ' + binLocation + '<br>';
				str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
				str = str + 'invItem. = ' + invItem + '<br>';

				nlapiLogExecution('ERROR', 'Inventory & FO Details', str);

				if((dowhlocation==InvWHLocation)&&(parseFloat(invBinLocnId) == parseFloat(binLocation)) && fulfilmentItem==invItem){

					var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
					var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
					var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
					var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');
					var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
					var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
					var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
					var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');
					var fromLotId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lot');
					var expiryDate = inventorySearchResults[i].getValue('custrecord_ebiz_expdate');
					var str = 'Invt QOH. = ' + actualQty + '<br>';
					str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
					str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
					str = str + 'packCode. = ' + packCode + '<br>';
					str = str + 'itemStatus. = ' + itemStatus + '<br>';
					str = str + 'whLocation. = ' + whLocation + '<br>';
					str = str + 'fromLP. = ' + fromLP + '<br>';
					str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
					str = str + 'fromLot. = ' + fromLot + '<br>';

					nlapiLogExecution('ERROR', 'Inventory Details', str);

					var recordId = inventorySearchResults[i].getId();

					// Get allocation already done
					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

					nlapiLogExecution('ERROR', 'alreadyAllocQty', alreadyAllocQty);

					// Adjusting for allocations already done
					inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

					nlapiLogExecution('ERROR', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

					var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
					                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot,expiryDate,fromLotId];

					binLocnInvDtls.push(currentRow);

					nlapiLogExecution('ERROR', 'inventoryAvailable_currentRow', currentRow);
					//matchFound = true;
					matchFound = false;
				}
			}
		}
	}

	nlapiLogExecution('ERROR','Out of getAvailQtyFromInventory',TimeStampinSec());
	return binLocnInvDtls;
}

function getPFLocationsForOrder(whlocation,skuList){
	var pfLocationResults = new Array();
	pfLocationResults = getSKUQtyInPickfaceLocns(whlocation,skuList);
	return pfLocationResults;
}

/**
 * Function to return the list of SKU quantity for all SKUs in all pickface locations
 * NOTE: WILL NEVER RETURN NULL; WILL ATLEAST RETURN AN EMPTY ARRAY
 * 
 * @returns {Array}
 */
function getSKUQtyInPickfaceLocns(whlocation,skulist){
	/*
	 * This function should return the pickface locn, min qty, max qty, qoh for all
	 * SKUs in all pickface locations
	 */

	nlapiLogExecution('ERROR', 'Into  getSKUQtyInPickfaceLocns',TimeStampinSec());
	nlapiLogExecution('ERROR', 'whlocation',whlocation);
	nlapiLogExecution('ERROR', 'skulist',skulist);
	var pfLocnResults = new Array();

	// Search for all active records with a valid replen rule id
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custrecord_pickruleid', null, 'noneof', ['@NONE@']));

	if(whlocation!=null && whlocation!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', ['@NONE@',whlocation]));

	if(skulist!=null && skulist!='')
		filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skulist));

	var columns = new Array();    
	columns[0] = new nlobjSearchColumn('custrecord_pickfacesku');
	columns[1] = new nlobjSearchColumn('custrecord_replenqty');
	columns[2] = new nlobjSearchColumn('custrecord_maxqty');
	columns[3] = new nlobjSearchColumn('custrecord_pickbinloc');
	columns[4] = new nlobjSearchColumn('custrecord_pickruleid');
	columns[5] = new nlobjSearchColumn('custrecord_minqty');    
	columns[6] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[7] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_maxpickqty');
	columns[9] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
	columns[10] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[11] = new nlobjSearchColumn('custrecord_roundqty');
	columns[12] = new nlobjSearchColumn('custrecord_pickface_location');	
	columns[13] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
	columns[14] = new nlobjSearchColumn('custrecord_pickzone');
	columns[15] = new nlobjSearchColumn('custrecord_autoreplen');


	// sort by pick face SKU
	columns[0].setSort();

	// Search for all active
	pfLocnResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);    

	nlapiLogExecution('ERROR', 'Out of  getSKUQtyInPickfaceLocns',TimeStampinSec());

	return pfLocnResults;
}
function getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,item,lot){
	nlapiLogExecution('ERROR', 'Into getAlreadyAllocatedInv',TimeStampinSec());
	var alreadyAllocQty = 0;
	if(allocatedInv != null && allocatedInv.length > 0){
		for(var i = 0; i < allocatedInv.length; i++){
//			nlapiLogExecution('ERROR', 'fromLP',fromLP);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][2]',allocatedInv[i][2]);
//			nlapiLogExecution('ERROR', 'binLocation',binLocation);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][0]',allocatedInv[i][0]);
//			nlapiLogExecution('ERROR', 'item',item);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][3]',allocatedInv[i][3]);
//			nlapiLogExecution('ERROR', 'lot',lot);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][4]',allocatedInv[i][4]);

			if(fromLP == allocatedInv[i][2] && binLocation==allocatedInv[i][0] && item==allocatedInv[i][3] && lot==allocatedInv[i][4])
				alreadyAllocQty = parseFloat(alreadyAllocQty) + parseFloat(allocatedInv[i][1]);
		}
	}
	nlapiLogExecution('ERROR', 'Out of getAlreadyAllocatedInv',TimeStampinSec());
	return alreadyAllocQty;
}
/*** up to here ***/

function getpickmethod()
{
	nlapiLogExecution('Debug','ClustergetPickMethod','chkptTrue');
	var pickInternalId=new Array();
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizcreatecluster',null,'is','T'));
	var column=new Array();
	column[0]=new nlobjSearchColumn('internalid').setSort();
	column[1]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders');
	column[2]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks');
	var searchresult=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filter, column);

	if(searchresult!=null)
	{
		for ( var i = 0; i < searchresult.length; i++)
		{
			pickInternalId[i]=new Array();
			pickInternalId[i][0]=searchresult[i].getValue('internalid');
			pickInternalId[i][1]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxorders');
			pickInternalId[i][2]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxpicks');
		}
	}

	return pickInternalId;
}


function CreateMasterLPRecord(vContainerLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag,location)
{
	var ResultText=GenerateLable(uompackflag,location);
	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
	MastLP.setFieldValue('name', vContainerLpNo);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContainerLpNo);
	if(vContainerSize!=null && vContainerSize!='')
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
	if(TotalWeight!=null && TotalWeight!='')
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight));
	if(ContainerCube!=null && ContainerCube!='')
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube));
	
	//MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);	 
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_site', location);	 
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);	

	var currentContext = nlapiGetContext();
	var retktoval = nlapiSubmitRecord(MastLP);
	nlapiLogExecution('Debug', 'Return Value from CreateMasterLPRecord',retktoval);
}



function pickreportscheduler(getwaveNo)
{

	var context = nlapiGetContext(); 
	//var form = nlapiCreateForm('WO Pick Report');
	//var vQbWave,vQbCluster,vQbfullfillmentNo ="";
	//var getwaveNo=request.getParameter('custparam_ebiz_wave_no');
	//var getFullfillmentNo=request.getParameter('custparam_ebiz_fullfilmentno');

	/*var ruleDet = IsCustomScreenRequired('PICKREPORTPDF');
	if(ruleDet!=null && ruleDet!='')
	{
		var vScreenType = ruleDet[0].getValue('custrecord_ebizrulevalue');
		var vScriptId = ruleDet[0].getValue('custrecord_ebizrule_scriptid');
		var vDeployId = ruleDet[0].getValue('custrecord_ebizrule_deployid');

		nlapiLogExecution('DEBUG', 'vScreenType', vScreenType);
		nlapiLogExecution('DEBUG', 'vScriptId', vScriptId);
		nlapiLogExecution('DEBUG', 'vDeployId', vDeployId);		

		var pickrptparams = new Array();

		if (getwaveNo!=null &&  getwaveNo != "") {
			pickrptparams["custparam_ebiz_wave_no"] = getwaveNo;
		}

		if (getFullfillmentNo!=null &&  getFullfillmentNo != "") {
			pickrptparams["custparam_ebiz_fullfilmentno"] = getFullfillmentNo;
		}

		if(vScreenType=='CUSTOM')
		{
			response.sendRedirect('SUITELET', vScriptId, vDeployId, false, pickrptparams );
			return;
		}
	}*/



	//var getButtonId=request.getParameter('id');
	SetPrintFlag(getwaveNo,null);
	var filters = new Array();

//	9-STATUS.OUTBOUND.PICK_GENERATED ,26-STATUS.OUTBOUND.FAILED
	//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
	filters.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	
	//if(request.getParameter('custparam_ebiz_wave_no')!=null && request.getParameter('custparam_ebiz_wave_no')!="")
	if(getwaveNo!=null && getwaveNo!="")
	{
		
		nlapiLogExecution('DEBUG', 'getwaveNo inside', getwaveNo);		
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getwaveNo)));
	} 
	/*if(request.getParameter('custparam_ebiz_clus_no')!=null && request.getParameter('custparam_ebiz_clus_no')!="")
	{
		vQbCluster = request.getParameter('custparam_ebiz_clus_no');
		nlapiLogExecution('DEBUG', 'vQbCluster inside', vQbCluster);	
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vQbCluster));
	} 
	*/

	/*if(request.getParameter('custparam_ebiz_fullfilmentno')!= null && request.getParameter('custparam_ebiz_fullfilmentno')!= "")
	{
		vQbfullfillmentNo = request.getParameter('custparam_ebiz_fullfilmentno');
		filters.push(new nlobjSearchFilter('name', null, 'is', vQbfullfillmentNo));
	}*/

	vQbWave = getwaveNo;

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[1] = new nlobjSearchColumn('custrecord_skiptask');
	columns[2] = new nlobjSearchColumn('custrecord_line_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_bin_locgroup_seq');
	columns[5] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[8] = new nlobjSearchColumn('custrecord_tasktype');
	columns[9] = new nlobjSearchColumn('custrecord_lpno');
	columns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[11] = new nlobjSearchColumn('custrecord_sku_status');
	columns[12] = new nlobjSearchColumn('custrecord_packcode');
	columns[13] = new nlobjSearchColumn('name');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
	columns[15] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[16] = new nlobjSearchColumn('custrecord_container');
	columns[17] = new nlobjSearchColumn('description','custrecord_sku');
	columns[18] = new nlobjSearchColumn('upccode','custrecord_sku');
	columns[19] = new nlobjSearchColumn('custrecord_batch_no');
	columns[20] = new nlobjSearchColumn('custrecord_total_weight');
	columns[21] = new nlobjSearchColumn('custrecord_ebizwmscarrier');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
	columns[23] = new nlobjSearchColumn('custrecord_expirydate');

	columns[0].setSort();
	columns[1].setSort();
	columns[4].setSort();
	columns[5].setSort();
	columns[6].setSort();
	columns[7].setSort(true);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	var itemsarr = new Array();
	var itemdimsarr = new Array();
	nlapiLogExecution('ERROR', 'searchresults',searchresults);
	if(searchresults!=null && searchresults.length > 0)
	{
		for (var i1 = 0; i1 < searchresults.length; i1++){
			var searchresult = searchresults[i1];
			itemsarr.push(searchresult.getValue('custrecord_ebiz_sku_no'));
		}
	

	itemsarr = removeDuplicateElement(itemsarr);
	nlapiLogExecution('ERROR', 'itemsarr',itemsarr);

	nlapiLogExecution('ERROR', 'Time Stamp Before calling  for all items',TimeStampinSec());
	itemdimsarr = getItemDimensions(itemsarr,-1);
	nlapiLogExecution('ERROR', 'Time Stamp After calling  for all items',TimeStampinSec());
	}

	if(searchresults!=null&&searchresults!=''&&searchresults.length>0)
	{
		var totalwt=0;

		var SoIds=new Array();
		for ( var intg = 0; intg < searchresults.length; intg++)
		{
			SoIds[intg]=searchresults[intg].getValue('custrecord_ebiz_order_no');
		}


		var distinctSoIds = removeDuplicateElement(SoIds);

		var vline, vitem, vqty, vTaskType,vlotbatch, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono,vcontlp,vcontsize,vclusno,vweight,vcaseqty,vnoofcases;
		var upccode="";
		var itemdesc="";
		var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

		//date
		var sysdate=DateStamp();
		var systime=TimeStamp();
		var Timez=calcTime('-5.00');
		var datetime= new Date();
		datetime=datetime.toLocaleTimeString() ;
		var finalimageurl = '';

		var url;
		/*var ctx = nlapiGetContext();
		if (ctx.getEnvironment() == 'PRODUCTION') 
		{
			url = 'https://system.netsuite.com';			
		}
		else if (ctx.getEnvironment() == 'SANDBOX') 
		{
			url = 'https://system.sandbox.netsuite.com';				
		}*/

		/*var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			var imageurl = filefound.getURL();
			//finalimageurl = url + imageurl;//+';';
			finalimageurl = imageurl;//+';';
			finalimageurl=finalimageurl.replace(/&/g,"&amp;");

		} 
		else 
		{
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}*/

		for ( var count = 0; count < distinctSoIds.length; count++)
		{
			var CartonArray=new Array();
			var totalwt=0;var pageno=0;
			var vtotalcube=0;
			var trantype = nlapiLookupField('transaction', SoIds, 'recordType');
			var salesorder = nlapiLoadRecord(trantype, distinctSoIds[count]);
			var	address = salesorder.getFieldValue('shipaddressee');
			nlapiLogExecution('ERROR', 'salesorder',salesorder);
			var ismultilineship=salesorder.getFieldValue('ismultishipto');
			if(searchresults!=null && searchresults!='')
			{
				var vlineno=searchresults[0].getValue('custrecord_line_no');

			}
			var shiptovalue=salesorder.getLineItemValue('item','shipaddress',vlineno);
			var shiptotext=salesorder.getLineItemText('item','shipaddress',vlineno);
			nlapiLogExecution('ERROR', 'ismultilineship',ismultilineship);
			nlapiLogExecution('ERROR', 'shiptovalue',shiptovalue);
			nlapiLogExecution('ERROR', 'shiptotext',shiptotext);
			var customerrecord=salesorder.getFieldValue('entity');



			var locationinternalid=salesorder.getFieldValue('location');

			var LogoValue;
			var LogoText;
			if(locationinternalid != null && locationinternalid != ""){
				var companylist = nlapiLoadRecord('location', locationinternalid);
				LogoValue=companylist.getFieldValue('logo');
				LogoText=companylist.getFieldText('logo');
			}
			nlapiLogExecution('ERROR','logo value',LogoValue);
			nlapiLogExecution('ERROR','logo text ',LogoText);
			var filefound='';
			//var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
			if(LogoText !=null && LogoText !='')
				filefound = nlapiLoadFile('Images/'+LogoText+''); 
			else
				filefound = nlapiLoadFile('Images/LOGOCOMP.jpg');

			if (filefound) 
			{ 
				nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
				var imageurl = filefound.getURL();
				nlapiLogExecution('ERROR','imageurl',imageurl);
				//var finalimageurl = url + imageurl;//+';';
				var finalimageurl = imageurl;//+';';
				//finalimageurl=finalimageurl+ '&expurl=T;';
				nlapiLogExecution('ERROR','imageurl',finalimageurl);
				finalimageurl=finalimageurl.replace(/&/g,"&amp;");

			} 
			else 
			{
				nlapiLogExecution('ERROR', 'Event', 'No file;');
			}

			nlapiLogExecution('ERROR', 'customerrecord',customerrecord);
			var entityrecord ;
			if(customerrecord != "" && customerrecord != null)
			{

				entityrecord = nlapiLoadRecord('customer', customerrecord);
				nlapiLogExecution('ERROR', 'entityrecord',entityrecord);
			}


			if(address != null && address !="")
				address=address.replace(replaceChar,'');
			else
				address="";

			var	HNo = salesorder.getFieldValue('shipaddr1');
			if(HNo != null && HNo !="")
				HNo=HNo.replace(replaceChar,'');
			else
				HNo="";
			var	addr2 = salesorder.getFieldValue('shipaddr2');
			if(addr2 != null && addr2 !="")
				addr2=addr2.replace(replaceChar,'');
			else
				addr2="";
			nlapiLogExecution('ERROR','addr2',addr2);
			var	city = salesorder.getFieldValue('shipcity');
			if(city != null && city !="")
				city=city.replace(replaceChar,'');
			else
				city="";
			var	state = salesorder.getFieldValue('shipstate');
			if(state != null && state !="")
				state=state.replace(replaceChar,'');
			else
				state="";
			var	country = salesorder.getFieldValue('shipcountry');
			if(country != null && country !="")
				country=country.replace(replaceChar,'');
			else
				country="";
			var	zipcode = salesorder.getFieldValue('shipzip');
			var	carrier = salesorder.getFieldText('shipmethod');
			if(carrier != null && carrier !="")
				carrier=carrier.replace(replaceChar,'');
			var SalesorderNo= salesorder.getFieldValue('tranid');

			var tempInstructions=salesorder.getFieldValue('custbody_nswmspoinstructions');
			var Instructions="";
			if(tempInstructions!=null)
			{
				Instructions=tempInstructions;
			}
			if(Instructions != null && Instructions !="")
				Instructions=Instructions.replace(replaceChar,'');
			else
				Instructions="";

			//calculate total wt for particular so#
			/*for (var x = 0; x < searchresults.length; x++)
			{
				var searchresult = searchresults[x];
				var ContainerLP=searchresult.getValue('custrecord_container_lp_no');
				vdono = searchresult.getValue('name');
				if(vdono.split('.')[0] == SalesorderNo)
				{

					if( searchresults[x].getValue('custrecord_total_weight')!=null && searchresults[x].getValue('custrecord_total_weight')!="")
						totalwt=parseFloat(totalwt)+parseFloat(searchresults[x].getValue('custrecord_total_weight'));
				}
				var result=GetCubeWt_LpMaster(ContainerLP,salesorder.getFieldValue('location'));
				totalwt=parseFloat(totalwt)+parseFloat(result[0]);
			}*/

			if(ismultilineship=='T')
			{
				if(entityrecord!=null && entityrecord!='')
				{
					var custlineitemcount=entityrecord.getLineItemCount('addressbook');
					for(var customerline=1;customerline<=custlineitemcount;customerline++)
					{	var custline=parseInt(customerline).toString();
//					var customerlabel = entityrecord.getLineItemValue('addressbook','label',custline);
					//phonenumber=entityrecord.getLineItemValue('addressbook','phone',custline);
//					if(customerlabel==shiptotext)
					var customerlabelid = entityrecord.getLineItemValue('addressbook','internalid',custline);
					if(customerlabelid==shiptovalue)
					{
						nlapiLogExecution('ERROR', 'test','test1');
						address = entityrecord.getLineItemValue('addressbook','addressee',custline);
						if(address != null && address !="")
							address=address.replace(replaceChar,'');
						else
							address="";
						HNo= entityrecord.getLineItemValue('addressbook','addr1',custline);
						if(HNo != null && HNo !="")
							HNo=HNo.replace(replaceChar,'');
						else
							HNo="";
						addr2 = entityrecord.getLineItemValue('addressbook','addr2',custline);
						if(addr2 != null && addr2 !="")
							addr2=addr2.replace(replaceChar,'');
						else
							addr2="";

						city = entityrecord.getLineItemValue('addressbook','city',custline);
						if(city != null && city !="")
							city=city.replace(replaceChar,'');
						else
							city="";
						state = entityrecord.getLineItemValue('addressbook','dropdownstate',custline);
						if(state != null && state !="")
							state=state.replace(replaceChar,'');
						else
							state="";
						zipcode = entityrecord.getLineItemValue('addressbook','zip',custline);
						if(zipcode != null && zipcode !="")
							zipcode=zipcode.replace(replaceChar,'');
						country=entityrecord.getLineItemValue('addressbook','country',custline);
						if(country != null && country !="")
							country=country.replace(replaceChar,'');
						else
							country="";
					}
					}
				}
			}

			for (var x = 0; x < searchresults.length; x++)
			{
				var searchresult = searchresults[x];
				var ContainerLP=searchresult.getValue('custrecord_container_lp_no');
				CartonArray[x]=ContainerLP;
				//nlapiLogExecution('ERROR','ContainerLP',ContainerLP);
			}

			var duplicateContainerLp= vremoveDuplicateElement(CartonArray);
			nlapiLogExecution('ERROR','duplicateContainerLp',duplicateContainerLp);

			/*for ( var count = 0; count < duplicateContainerLp.length; count++) 
			{
				nlapiLogExecution('ERROR','duplicateContainerLp',duplicateContainerLp.length);
				var result=GetCubeWt_LpMaster(duplicateContainerLp[count],whLocation);
				nlapiLogExecution('ERROR','result',result[0]);

				totalwt=totalwt+(parseFloat(result[0])*parseFloat(1));
				vtotalcube=vtotalcube+(parseFloat(result[1])*parseFloat(1));
			}
			 */
			for (var x = 0; x < searchresults.length; x++)
			{
				var searchresult = searchresults[x];
				vdono = searchresult.getValue('name');
				if(vdono.split('.')[0] == SalesorderNo)
				{

					if( searchresults[x].getValue('custrecord_total_weight')!=null && searchresults[x].getValue('custrecord_total_weight')!="")
						totalwt=parseFloat(totalwt)+parseFloat(searchresults[x].getValue('custrecord_total_weight'));
				}
			}

			nlapiLogExecution('ERROR','totalwt',totalwt);
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

			if(count==0)
				var strxml = "<table width='100%' >";
			else
			{
				var strxml=strxml+"";
				strxml += "<table width='100%' >";
			}
			if(pageno==0)
			{
				pageno=parseFloat(pageno+1);
			}


			strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
			strxml += "WO Pick Report ";
			strxml += "</td><td align='right'>&nbsp;</td></tr></table>";
			strxml += "<p align='right'>Date/Time:"+Timez+"</p>";
			strxml +="<table style='width:100%;'>";
			strxml +="<tr><td valign='top'>";
			strxml +="<table align='left' style='width:70%;' border='1'>";
			strxml +="<tr><td align='left' style='width:51px'>Wave# :</td>";

			strxml +="<td>";
			if(vQbWave != null && vQbWave!= "")
			{
				strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
				strxml += parseInt(vQbWave);
				strxml += "\"/>";
			}
			strxml += "</td></tr>";

			strxml +="<tr><td align='left' style='width:51px'>Work Order# :</td>";

			strxml +="<td>";
			if(SalesorderNo != null && SalesorderNo != "")
			{
				strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
				strxml += SalesorderNo;
				strxml += "\"/>";
			}
			strxml += "</td></tr>";

			strxml +="<tr><td align='left' style='width:51px'>Total Weight(lbs):</td>";
			strxml +="<td>"+totalwt+"</td></tr>";
			strxml +="</table><table><tr><td>&nbsp;</td></tr></table>	<table align='left' style='width:70%;' border='1'>";
			strxml +="<tr><td align='left' style='width:51px'>Carrier:</td>";
			strxml +="<td align='left'>"+carrier+"</td></tr>"; 

			/*//splinstructions = splinstructions.replace(replaceChar,'');
			strxml +="<tr><td align='left' style='width:51px'>Special Instructions:</td>";
			//strxml +="<td align='left'>"+splinstructions+"</td></tr>"; 
			strxml +="<td align='left'>"+Instructions+"</td></tr>"; */

			strxml +="</table></td>";
			strxml +="<td>";
			strxml +="<table align='right' style='width:60%;' border='1'>";
			strxml +="<tr><td align='center' colspan='2'><b>Ship To</b></td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>Address:</td>";
			strxml +="<td>"+address+"</td></tr>";
			strxml +="<tr><td style='width:51px'>&nbsp;</td>";
			strxml +="<td>"+HNo+"</td></tr>";
			if(addr2 != null && addr2 != '')
			{	
				strxml +="<tr><td style='width:51px'>&nbsp;</td>";
				strxml +="<td>"+addr2.replace(replaceChar,'')+"</td></tr>";
			}
			strxml +="<tr><td align='right' style='width:51px'>City:</td>";
			strxml +="<td>"+city+"</td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>State:</td>";
			strxml +="<td>"+state+"</td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>Zip:</td>";
			strxml +="<td>"+zipcode+"</td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>Country:</td>";
			strxml +="<td>"+country+"</td></tr>";			
			strxml +="</table>";
			strxml +=" <p>&nbsp;</p>";
			strxml +="</td></tr></table>";
			if(Instructions!="")
			{
				strxml +=" <p>&nbsp;</p>";
				strxml +="<table  width='100%' style='width:100%;' >";
				strxml +="<tr style=\"font-weight:bold\"><td width='100%' style='border-width: 0.5px; border-color: #000000'>";
				strxml +="<span style='font-size:9'>Order Instructions: <p> "+Instructions.replace(replaceChar,'')+"</p></span>";
				strxml +="</td></tr></table>";
				strxml +=" <p>&nbsp;</p>";
			}
			strxml +="<table  width='100%'>";
			strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

			/*strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
			strxml += " Fulfillment#";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";*/

			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Line #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
			strxml += " Part#/Item";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
			strxml += "UPC Code";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='15%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Item Description";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Status";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Pack Code";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";	

			strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
			strxml += "Qty";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
			strxml += "No.Of Cases";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Bin Location";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "LP #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Lot #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Expiry Date";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Container LP";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Container Size";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Cluster #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Weight(lbs)";
			strxml += "</td>";


			strxml += "<td style='border-width: 1px; border-color: #000000'>";
			strxml += "Zone";
			strxml += "</td>";

			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
			strxml += "WMS Carrier";
			strxml =strxml+  "</td></tr>";
			var tempLineNo;var pagetotalno=0;
			if (searchresults != null) {
				for (var i = 0; i < searchresults.length; i++) {
					var searchresult = searchresults[i];

					vline = searchresult.getValue('custrecord_line_no');
					vlotbatch = searchresult.getValue('custrecord_batch_no');
					vitem = searchresult.getValue('custrecord_ebiz_sku_no');
					vqty = searchresult.getValue('custrecord_expe_qty');
					vTaskType = searchresult.getText('custrecord_tasktype');
					vLpno = searchresult.getValue('custrecord_lpno');
					vlocationid = searchresult.getValue('custrecord_actbeginloc');
					vlocation = searchresult.getText('custrecord_actbeginloc');
					vSKUID=searchresult.getValue('custrecord_sku');

					//The below code is commented by Satish.N on 12/05/2012 as we can deirectly get Item name from open task.
					//var Itype = nlapiLookupField('item', vSKUID, 'recordType');
					//var ItemRec = nlapiLoadRecord(Itype, vSKUID);
					//nlapiLogExecution('ERROR', 'Time Stamp after loading Item record',TimeStampinSec());
					//vSKU = ItemRec.getFieldValue('itemid');
					nlapiLogExecution('ERROR', 'searchresult Id ', searchresult.getId());
					vSKU = searchresult.getText('custrecord_sku');
					vskustatus = searchresult.getText('custrecord_sku_status');
					vpackcode = searchresult.getValue('custrecord_packcode');
					vdono = searchresult.getValue('name');
					vcontlp=searchresult.getValue('custrecord_container_lp_no');
					vcontsize=searchresult.getText('custrecord_container');
					//vclusno=searchresult.getValue('custrecord_ebiz_clus_no');
					vclusno=searchresult.getValue('custrecord_ebiz_clus_no');
					upccode=searchresult.getValue('upccode','custrecord_sku');
					itemdesc=searchresult.getValue('description','custrecord_sku');
					vweight=searchresult.getValue('custrecord_total_weight');
					var vWMSCarrier=searchresult.getText('custrecord_ebizwmscarrier');
					var vzone=searchresult.getText('custrecord_ebiz_zoneid'); 
					var vExpiryDate=searchresult.getValue('custrecord_expirydate');

					if(upccode != null && upccode !="")
						upccode=upccode.replace(replaceChar,'');

					if(vskustatus != null && vskustatus !="")
						vskustatus=vskustatus.replace(replaceChar,'');

					if(vlotbatch != null && vlotbatch !="")
						vlotbatch=vlotbatch.replace(replaceChar,'');

					if(vcontsize != null && vcontsize !="")
						vcontsize=vcontsize.replace(replaceChar,'');

					if(vzone != null && vzone !="")
						vzone=vzone.replace(replaceChar,'');

					if(vWMSCarrier != null && vWMSCarrier !="")
						vWMSCarrier=vWMSCarrier.replace(replaceChar,'');

					if (itemdimsarr != null && itemdimsarr.length > 0) 
					{	
						//nlapiLogExecution('ERROR', 'AllItemDims.length', itemdimsarr.length);
						for(var p = 0; p < itemdimsarr.length; p++)
						{
							var itemval = itemdimsarr[p][0];

							if(itemdimsarr[p][0] == vitem)
							{
								//nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', vitem);

								var skuDim = itemdimsarr[p][2];
								var skuQty = itemdimsarr[p][3];	
								var packflag = itemdimsarr[p][5];	
								var weight = itemdimsarr[p][6];
								var cube = itemdimsarr[p][7];
								var uom = itemdimsarr[p][1];

								if(skuDim == '2'){
									vcaseqty = skuQty;
									break;
								}								
							}	
						}
					}
					//nlapiLogExecution('ERROR', 'Case Qty: ', vcaseqty);
//					case# 20124184ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½ Start
					if(vcaseqty != 0 && vcaseqty!='' && vcaseqty!=null && !isNaN(vcaseqty)){
						vnoofcases = parseFloat(vqty / vcaseqty);
					}
					else{
						//case# 20124229 Start
						if(vqty==null || vqty=='')
							vqty=0;
						//case# 20124229 End
						vnoofcases = parseFloat(vqty);
					}
					//nlapiLogExecution('ERROR', '# of Cases: ', vnoofcases);
					nlapiLogExecution('ERROR', 'tempLineNo ', tempLineNo);
					nlapiLogExecution('ERROR', 'vline ', vline);
					if(vdono.split('.')[0] == SalesorderNo)// case# 201416962
					{
						if(tempLineNo==null)
						{
							tempLineNo=vline;
						}

						if(vline!=tempLineNo)
						{
							var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);
							if(lineInstructions!=null && lineInstructions!="")
							{
								nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
								nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
								strxml +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
								strxml +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions.replace(replaceChar,'')+"</p>";
								strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
							}
						}

						strxml =strxml+  "<tr>";

						/*strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						if(vdono != null && vdono != "")
						{
							//strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
							strxml += vdono.replace(replaceChar,'');
							//strxml += "\"/>";
						}
						strxml += "</td>";*/

						strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
						strxml += vline;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						if(vSKU != null && vSKU != "")
						{
							strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
							strxml += vSKU.replace(/"/g,"&#34;");
							strxml += "\"/>";
						}
						strxml += "</td>";

						strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";
						strxml += upccode;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						if(itemdesc != null && itemdesc != "")
							strxml += itemdesc.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						strxml += vskustatus;
						strxml += "</td>";

						strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vpackcode;
						strxml += "</td>";	

						strxml += "<td width='3%' align='right' style='border-width: 1px; border-color: #000000'>";			
						strxml += vqty;
						strxml += "</td>";

						strxml += "<td width='3%' align='right' style='border-width: 1px; border-color: #000000'>";			
						strxml += vnoofcases.toFixed(2);
						strxml += "</td>";

						strxml += "<td width='5%' style='border-width: 1px; border-color: #000000'>";
						strxml += vlocation.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vLpno.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
						strxml += vlotbatch;
						strxml += "</td>";

						strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
						strxml += vExpiryDate;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vcontlp.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vcontsize;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						nlapiLogExecution('ERROR', 'vclusno ', vclusno);
						if(vclusno != null && vclusno != "")
						{
							//strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
							strxml += vclusno;
							//strxml += "\"/>";
						}
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						strxml += vweight;
						strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";


						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vzone;
						strxml +="</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						strxml += vWMSCarrier;
						strxml =strxml+  "</td></tr>";	
						nlapiLogExecution('ERROR', 'tempLineNo ', tempLineNo);
						nlapiLogExecution('ERROR', 'vline ', vline);
						tempLineNo=vline;
						pagetotalno=parseFloat(pagetotalno)+1;
					}
				}
			}
			nlapiLogExecution('ERROR', 'tempLineNo ', tempLineNo);
			nlapiLogExecution('ERROR', 'vline ', vline);
			var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);

			if(lineInstructions!=null && lineInstructions!="")
			{
				lineInstructions=lineInstructions.replace(replaceChar,'');

				strxml +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
				strxml +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions.replace(replaceChar,'')+"</p>";
				strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
				tempLineNo=null;
			}
			strxml =strxml+"</table>";
			if((distinctSoIds.length-count)>1)
			{
				pageno=parseFloat(pageno)+1;
				strxml=strxml+ "<p style='page-break-after:always'></p>";
			}
			else
			{
				//pageno=parseFloat(pageno)+1;
				//strxml=strxml+ "<p style='vertical-align:bottom;align:right;font-size:9'>Page No: "+pageno+" </p>";
			}
		}
		strxml =strxml+ "\n</body>\n</pdf>";
		xml=xml +strxml;

		var file = nlapiXMLToPDF(xml);	
		nlapiLogExecution('ERROR','file',file);
		//response.setContentType('PDF','PickReport.pdf');
		//response.write( file.getValue() );
		var pdffilename = getwaveNo+'_PickReport.pdf';

		nlapiLogExecution('ERROR','pdffilename',pdffilename);

		var filevalue=file.getValue();
		var newAttachment = nlapiCreateFile(pdffilename,file.type,filevalue);

		var userId = context.getUser();
		nlapiLogExecution('ERROR','userId',userId);
		var userAccountId = context.getCompany();
		var username='';
		try
		{
			var transaction = nlapiLoadRecord('Employee', userId);
			var variable=transaction.getFieldValue('email');
			username=variable.split('@')[0];
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','exp',exp);
		}

		var strContent="";

		var strSubject="Pick Report printed for Wave# "+vQbWave;//+" and Fulfillment Order# "+vQbfullfillmentNo;// +"  by user "+username+" at  "+Timez; 
		strContent +="Wave#:"+vQbWave;
		strContent +="<br/>";
		strContent +="AccountId#:"+userAccountId;
		strContent +="<br/>";
		strContent +=strSubject;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_email_alert_type', null, 'anyof', ['2']));
		filters.push(new nlobjSearchFilter('custrecord_email_trantype', null, 'anyof', ['2']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_user_email');
		columns[1] = new nlobjSearchColumn('custrecord_email_option');
		var searchresults = nlapiSearchRecord('customrecord_email_config', null, filters, columns);		 
		var email= "";
		var emailbcc="";
		var emailcc="";
		var emailappend="";
		var emailbccappend= new Array();
		var emailccappend=new Array();
		var count=0;
		var bcccount=0;
		for(var g=0;searchresults!=null && g<searchresults.length;g++)
		{
			var emailtext=searchresults[g].getText('custrecord_email_option');
			nlapiLogExecution('ERROR','emailtext',emailtext);
			if(emailtext=="BCC")
			{
				emailbccappend[bcccount]=searchresults[g].getValue('custrecord_user_email');
				bcccount++;		 
			}
			else if(emailtext=="CC")
			{
				emailccappend[count]=searchresults[g].getValue('custrecord_user_email');
				count++;		 
			}
			else
			{
				email =searchresults[g].getValue('custrecord_user_email');
				emailappend +=email+";";
			}
		} 
		var substirngemail= emailappend .substring(0, emailappend .length-1);
		nlapiLogExecution('ERROR','variable ',variable );
		if(variable != null && variable != '')
		{
			substirngemail = substirngemail + ';' + variable;
		}
		nlapiLogExecution('ERROR','substirngemail',substirngemail);

		nlapiSendEmail(userId,substirngemail,strSubject,strContent,emailccappend,emailbccappend,null,newAttachment);

	}

}


function SetPrintFlag(waveno,fullfillment)
{
	nlapiLogExecution('ERROR','Into SetPrintFlag',waveno);
	var filter=new Array();

	if(waveno!=null && waveno!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave','null','equalto',parseFloat(waveno)));

	if(fullfillment!=null&&fullfillment!="")
		filter.push(new nlobjSearchFilter('custrecord_lineord','null','is',fullfillment.toString()));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_printflag');
	column[1]=new nlobjSearchColumn('custrecord_print_count');

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);
	if(searchrecord!=null)
	{
		for ( var count = 0; count < searchrecord.length; count++) {
			var intId=searchrecord[count].getId();
			var flagcount=searchrecord[count].getValue('custrecord_print_count');
			var Newflagcount;
			if(flagcount==null || flagcount=="")
				Newflagcount=1;
			else
				Newflagcount=parseFloat(flagcount)+1;

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_printflag');  
			fieldNames.push('custrecord_print_count');
			fieldNames.push('custrecord_ebiz_pr_dateprinted');

			var newValues = new Array(); 
			newValues.push('T');
			newValues.push(Newflagcount);
			newValues.push(DateStamp());			

			nlapiSubmitField('customrecord_ebiznet_ordline', intId, fieldNames, newValues);
		}
	}
	nlapiLogExecution('ERROR','Out of SetPrintFlag',waveno);
}

function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}

var itemDimensionList = new Array();
function getItemDimensions(skuList,maxno){
	nlapiLogExecution('Debug','into  getItemDimensions - # of items', skuList.length);
	nlapiLogExecution('Debug','into  getItemDimensions - maxno', maxno);
	var itemDimCount;
	if(maxno==-1)
		itemDimCount = 0;
	else
		itemDimCount=itemDimensionList.length;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuList));		// ALL SKUs IN SKU LIST
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));							// ACTIVE RECORDS ONLY
	filters.push(new nlobjSearchFilter('custrecord_ebiz_notallowedforpicking', null, 'is', 'F'));
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM LEVEL
	columns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
	columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE UOM FLAG
	columns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK FLAG	
	columns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT	
	columns[7] = new nlobjSearchColumn('custrecord_ebizcube');									// CUBE	
	columns[8] = new nlobjSearchColumn('internalid');
	//columns[9] = new nlobjSearchColumn('custrecord_ebiz_dims_bulkpick');
	columns[8].setSort(true);

	// SEARCH FROM ITEM DIMENSIONS
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(searchResults != null && searchResults != '' && searchResults.length >= 1000){
		var maxno1=searchResults[searchResults.length-1].getValue(columns[8]);
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');
			//var vbulkpick = searchResults[i].getValue('custrecord_ebiz_dims_bulkpick');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];
			itemDimensionList.push(skuDimRecord);
		}

		getItemDimensions(skuList,maxno1);
	}
	else if(searchResults != null && searchResults != '')
	{
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');
			//var vbulkpick = searchResults[i].getValue('custrecord_ebiz_dims_bulkpick');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];
			itemDimensionList.push(skuDimRecord);
		}

	}
	nlapiLogExecution('Debug','out of  getItemDimensions', skuList.length);
	return itemDimensionList;
}
function vremoveDuplicateElement(arr)
{
	var dups = {}; 
	return arr.filter(
			function(el) { 
				var hash = el.valueOf(); 
				var isDup = dups[hash]; 
				dups[hash] = true; 
				return !isDup; 
			}
	); 
}



function ConcurrencyHandlingForInvt(varsku,vLineQty,vWMSLocation,vlineno,vComments,ItemInfoResults,pickrulesarray,vBatch,pfLocationResults,tempFlag,InvtRecordArray,OpentaskInternalId,vId,vWOName,vwaveno,veBizContLP) {

	try
	{
		nlapiLogExecution('DEBUG', 'In to ConcurrencyHandlingForInvt', 'sucess');
		
		
		var now = new Date();
		//a Date object to be used for a random value
		var now = new Date();
		//now= now.getHours();
		//Getting time in hh:mm tt format.
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}
		
		
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		var str = 'varsku.' + varsku + '<br>';
		str = str + 'vLineQty.' + vLineQty + '<br>';	
		str = str + 'vWMSLocation. ' + vWMSLocation + '<br>';	
		str = str + 'ItemInfoResults. ' + ItemInfoResults + '<br>';	
		str = str + 'pickrulesarray. ' + pickrulesarray + '<br>';	
		str = str + 'pfLocationResults. ' + pfLocationResults + '<br>';
		str = str + 'vBatch. ' + vBatch + '<br>';
		str = str + 'tempFlag. ' + tempFlag + '<br>';
		str = str + 'InvtRecordArray. ' + InvtRecordArray + '<br>';
		str = str + 'OpentaskInternalId. ' + OpentaskInternalId + '<br>';
		str = str + 'vId. ' + vId + '<br>';
		str = str + 'vWOName. ' + vWOName + '<br>';

		nlapiLogExecution('DEBUG', 'SearchresultsArrar values1', str);

		var Feilds= ['custitem_item_family', 'custitem_item_group','recordType','custitem_ebizbatchlot'];
		var columns = nlapiLookupField('item', varsku, Feilds);
		var vItemType="";
		var batchflag="F";
		nlapiLogExecution('Debug','Time stamp 10',TimeStampinSec());
		if (columns != null && columns !='')
		{
			nlapiLogExecution('Debug','Time stamp 11',TimeStampinSec());
			var itemfamily = columns.custitem_item_family;
			var itemgroup = columns.custitem_item_group;
			vItemType = columns.recordType;
			batchflag= columns.custitem_ebizbatchlot;
		}

		// LABL3 is used for, if secound time also failed due to concurrency
		var vCount=1;
		LABL3: for(var m=0; m<vCount; m++)
		{
			var arryinvt=PickStrategieKittoStockNew(varsku,vLineQty,vWMSLocation,ItemInfoResults,pickrulesarray,vBatch,
					pfLocationResults,vItemType,batchflag);
			

			if(arryinvt !=null && arryinvt !='' && arryinvt.length>0)
			{
				for (var z=0; z<arryinvt.length; z++)
				{
					var invtarray= arryinvt[z]; 
					var vTempId = '';
					var vTempLineRec = invtarray[3];
					var vTempWOId = invtarray[2];
					var varTempsku = varsku;
					var vTempLP = invtarray[4];
					var vTempLineQty = invtarray[0];
					var vTempBatch = invtarray[4];
					var vTempactLocationtext = invtarray[6];
					var vTempLocation = invtarray[1];
					var vTempComments = vComments;
					var vTempLineno = vlineno;
					var vTempPickzone = invtarray[8];
					var vTempPickruleId = invtarray[9];
					var vTempPickmethod = invtarray[10];
					var vTempInvtResFlag = invtarray[11];


					var str = 'vTempId.' + vTempId + '<br>';
					str = str + 'vLineRec.' + vTempLineRec + '<br>';	
					str = str + 'varsku. ' + varTempsku + '<br>';	
					str = str + 'vTempBatch. ' + vTempBatch + '<br>';	
					str = str + 'vTempactLocationtext. ' + vTempactLocationtext + '<br>';	
					str = str + 'vTempComments. ' + vTempComments + '<br>';	
					str = str + 'vLineQty. ' + vTempLineQty + '<br>';	
					str = str + 'vTempLineno. ' + vTempLineno + '<br>';	
					str = str + 'vTempPickzone. ' + vTempPickzone + '<br>';	
					str = str + 'vTempPickruleId. ' + vTempPickruleId + '<br>';	
					str = str + 'vTempPickmethod. ' + vTempPickmethod + '<br>';	
					str = str + 'vTempInvtResFlag. ' + vTempInvtResFlag + '<br>';	


					nlapiLogExecution('DEBUG', 'SearchresultsArray values2', str);


					var vscount=1;
					LABL2: for(var t=0;t<vscount;t++)
					{

						try
						{
							var transaction1 = nlapiLoadRecord('customrecord_ebiznet_createinv', vTempLineRec);
							nlapiLogExecution('Debug','Time stamp 23',TimeStampinSec());
							var vTempQty = transaction1.getFieldValue('custrecord_ebiz_qoh');
							var vTempallocqty = transaction1.getFieldValue('custrecord_ebiz_alloc_qty');
							var vTempLP = transaction1.getFieldValue('custrecord_ebiz_inv_lp');
							var vTempPackcode = transaction1.getFieldValue('custrecord_ebiz_inv_packcode');
							var vTempSKUStatus = transaction1.getFieldValue('custrecord_ebiz_inv_sku_status');
							var vTempAvailableQty = transaction1.getFieldValue('custrecord_ebiz_avl_qty');

							nlapiLogExecution('ERROR', 'vTempQty', vTempQty);
							nlapiLogExecution('ERROR', 'vTempallocqty', vTempallocqty);
							nlapiLogExecution('ERROR', 'vTempAvailableQty', vTempAvailableQty);
							if(vTempallocqty == null || vTempallocqty == '')
								vTempallocqty=0;
							nlapiLogExecution('ERROR', 'vTempLineQty', vTempLineQty);
							if(parseFloat(vTempAvailableQty)>=parseFloat(vTempLineQty))
							{
								transaction1.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(vTempallocqty)+ parseFloat(vTempLineQty)).toFixed(5));
								transaction1.setFieldValue('custrecord_ebiz_displayfield', 'N');			 
								transaction1.setFieldValue('custrecord_ebiz_callinv', 'N');
								nlapiSubmitRecord(transaction1, false, true);

								//capture createinvt internal ids
								var currentArray = new Array(); 
								currentArray = [vTempLineRec,vTempLineQty];
								InvtRecordArray.push(currentArray);

								//create Open task records

								var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
								customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
								//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
								customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
								customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));


								nlapiLogExecution('ERROR','vTempactLocationtext',vTempactLocationtext);

								customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vTempLineQty).toFixed(5));  //Commented and added to resovle decimal issue
								customrecord.setFieldValue('custrecord_act_qty', parseFloat(vTempLineQty).toFixed(5));
								customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process


								customrecord.setFieldValue('custrecord_actbeginloc',vTempLocation);	
								//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
								//customrecord.setFieldValue('custrecord_lpno', vlp); 
								customrecord.setFieldValue('custrecord_upd_date', DateStamp());// case# 201417112
								//customrecord.setFieldValue('custrecord_sku', vskuText);
								customrecord.setFieldValue('custrecord_ebiz_sku_no', varTempsku);
								customrecord.setFieldValue('name', vWOName);		

								customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
								customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
								//customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
								customrecord.setFieldValue('custrecord_ebiz_order_no', vId);
								customrecord.setFieldValue('custrecord_batch_no', vTempBatch);
								customrecord.setFieldValue('custrecord_ebiz_cntrl_no', vId);
								//customrecord.setFieldValue('custrecord_ebiz_cntrl_no', rcptrecordid);

								customrecord.setFieldValue('custrecord_sku', varTempsku);
								customrecord.setFieldValue('custrecord_line_no', parseFloat(vTempLineno));
								customrecord.setFieldValue('custrecord_lpno', vTempLP);
								customrecord.setFieldValue('custrecord_packcode', vTempPackcode);					
								customrecord.setFieldValue('custrecord_sku_status', vTempSKUStatus);
								customrecord.setFieldValue('custrecord_wms_location', vWMSLocation);
								customrecord.setFieldValue('custrecord_invref_no', vTempLineRec);
								customrecord.setFieldValue('custrecord_notes', vTempComments);	
								customrecord.setFieldValue('custrecord_ebizrule_no', vTempPickruleId);
								customrecord.setFieldValue('custrecord_ebizmethod_no', vTempPickmethod);
								customrecord.setFieldValue('custrecord_ebizzone_no', vTempPickzone);

								if(vTempPickzone !=null && vTempPickzone !='')
								{
									customrecord.setFieldValue('custrecord_ebiz_zoneid', vTempPickzone);
								}

								
								nlapiLogExecution('DEBUG', 'vwaveno',vwaveno);
								if(vwaveno != null && vwaveno != '')
								{
									customrecord.setFieldValue('custrecord_ebiz_wave_no', parseFloat(vwaveno));
								}
								nlapiLogExecution('DEBUG', 'veBizContLP',veBizContLP);
								if(veBizContLP != null && veBizContLP != '')
								{
									customrecord.setFieldValue('custrecord_container_lp_no', veBizContLP);
								}


								nlapiLogExecution('DEBUG', 'beginLocationId',vTempLocation);
								if(vTempLocation!=null && vTempLocation!='')
								{
									nlapiLogExecution('Debug','Time stamp 26',TimeStampinSec());
									var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
									var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', vTempLocation, locgroupfields);
									nlapiLogExecution('Debug','Time stamp 27',TimeStampinSec());
									var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
									var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;
									nlapiLogExecution('DEBUG', 'inblocgroupid',inblocgroupid);
									var locgroupseqfields = ['custrecord_sequenceno'];
									//code added on 011012 by suman
									//Allow to insert sequence no only if we get some value form inbonlog group Id.
									//if(inblocgroupid!=null&&inblocgroupid!="")
									if(oublocgroupid!=null && oublocgroupid!="")
									{
										nlapiLogExecution('Debug','Time stamp 28',TimeStampinSec());
										//var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', inblocgroupid, locgroupseqfields);
										var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
										var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;
										nlapiLogExecution('DEBUG', 'locgroupidseq',locgroupidseq);

										customrecord.setFieldValue('custrecord_bin_locgroup_seq', locgroupidseq);
										nlapiLogExecution('Debug','Time stamp 29',TimeStampinSec());
									}
								}

								var expiryDateInLot='';
								if(vTempBatch!=null && vTempBatch !=''){
									var filterbatch = new Array();
									filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vTempBatch);
									filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varTempsku);
									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_ebizexpirydate'); 
									var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
									if(Lotnosearchresults!=null && Lotnosearchresults!='')
									{
										nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
										vLotno= Lotnosearchresults[0].getId();
										expiryDateInLot= Lotnosearchresults[0].getValue('custrecord_ebizexpirydate');
									}
									nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
									if(expiryDateInLot !=null && expiryDateInLot!='')
										customrecord.setFieldValue('custrecord_expirydate', expiryDateInLot);

								}

								nlapiLogExecution('Debug','Time stamp 30',TimeStampinSec());
								//nlapiLogExecution('ERROR','eBizWaveNo ',eBizWaveNo);


								var OpentaskId= nlapiSubmitRecord(customrecord);			
								nlapiLogExecution('Debug','Time stamp 31',TimeStampinSec());
								nlapiLogExecution('ERROR', 'Success');

								if(OpentaskId !=null && OpentaskId !='')
								{
									nlapiLogExecution('ERROR', 'OpentaskId',OpentaskId);
									OpentaskInternalId.push(OpentaskId);
								}



							}
							else
							{

								if(vTempAvailableQty == 'T')
								{						
									nlapiLogExecution('ERROR', '2nd time Concurrency occured',vTempAvailableQty);
									vCount=vCount+1;
									continue LABL3;
								}
								else
								{
									tempFlag ="T";
									nlapiLogExecution('ERROR', 'no inventory available',tempFlag);
									nlapiLogExecution('ERROR', 'tempFlag',tempFlag);
								}

							}
						}

						catch(ex1)
						{
							nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex1);

							var exCode='CUSTOM_RECORD_COLLISION'; 
							var wmsE='Inventory record being updated by another user. Please try again...';
							if (ex1 instanceof nlobjError) 
							{	
								wmsE=ex1.getCode() + '\n' + ex1.getDetails();
								exCode=ex1.getCode();
							}
							else
							{
								wmsE=ex1.toString();
								exCode=ex1.toString();
							}  

							nlapiLogExecution('ERROR', 'Exception in Invt Updation : ', ex1);

							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{ 
								scount=scount+1;
								continue LABL2;
							}
							else break LABL2;
						}


					}// end of for loop of LABL2

				}  //end of for loop of arryinvt

			} //end of if condition of arryinvt

		} // end of for loop of LABL3
		nlapiLogExecution('ERROR', 'End of loop : ', tempFlag);
		return tempFlag;
	}
	catch(exp1)
	{
		nlapiLogExecution('ERROR', 'exception in ConcurrencyHandlingForInvt',exp1);
		tempFlag ="T";
		return tempFlag;
	}


}
