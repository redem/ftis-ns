/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInExpDate.js,v $
 *     	   $Revision: 1.5.2.7.4.7.2.10 $
 *     	   $Date: 2014/10/17 12:56:27 $
 *     	   $Author: skavuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInExpDate.js,v $
 * Revision 1.5.2.7.4.7.2.10  2014/10/17 12:56:27  skavuri
 * Case# 201410580 Std bundle Issue fixed
 *
 * Revision 1.5.2.7.4.7.2.9  2014/06/13 07:24:05  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.5.2.7.4.7.2.8  2014/05/30 00:26:48  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.5.2.7.4.7.2.7  2013/10/24 14:32:31  schepuri
 * 20125193
 *
 * Revision 1.5.2.7.4.7.2.6  2013/08/05 16:58:37  skreddy
 * Case# 20123721
 * issue rellated to expiry date
 *
 * Revision 1.5.2.7.4.7.2.5  2013/07/12 16:00:39  skreddy
 * Case# 20123368
 * issue rellated to expiry date for Lot#
 *
 * Revision 1.5.2.7.4.7.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.5.2.7.4.7.2.3  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.5.2.7.4.7.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.5.2.7.4.7.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.5.2.7.4.7  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.5.2.7.4.6  2013/02/07 08:39:36  skreddy
 * CASE201112/CR201113/LOG201121
 * RF Lot auto generating FIFO enhancement
 *
 * Revision 1.5.2.7.4.5  2012/12/03 16:38:48  schepuri
 * CASE201112/CR201113/LOG201121
 * Partial put away generated PO is not displayed in the put away report list
 *
 * Revision 1.5.2.7.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.5.2.7.4.3  2012/09/27 10:56:55  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.5.2.7.4.2  2012/09/26 12:47:07  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.5.2.7.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.5.2.7  2012/07/04 07:08:13  spendyala
 * CASE201112/CR201113/LOG201121
 * Invalid parameter type while passing to query string.
 *
 * Revision 1.5.2.6  2012/07/04 07:03:06  spendyala
 * Invalid parameter type while passing to query string.
 *
 * Revision 1.5.2.5  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.5.2.4  2012/03/02 13:57:14  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue Related to validating Mfg date and ExpDate .
 *
 * Revision 1.5.2.3  2012/02/24 12:28:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing BaseUomQty To Query String.
 *
 * Revision 1.5.2.2  2012/02/22 12:19:27  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.5.2.1  2012/02/10 07:31:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.5  2011/12/30 14:30:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Added code to validated Dates that are entered.
 *
 * Revision 1.4  2011/12/22 22:42:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.3  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

/**
 * @param request
 * @param response
 */
function CheckInExpDate(request, response){
	if (request.getMethod() == 'GET') {

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemBaseUomQty=request.getParameter('custparam_baseuomqty');
/*		var getItemQuantity = request.getParameter('hdnQuantity');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');*/
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);
		nlapiLogExecution('DEBUG','getBatchNo',getBatchNo);
		var getPOLineItemStatusText = request.getParameter('custparam_polineitemstatustext');//Case# 201410580
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		var actScannedBatchno = request.getParameter('custparam_actscanbatchno');
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "CHECKIN FECHA DE CADUCIDAD";
			st1 = "FECHA DE MANUFACTURA";	
			st2 = "FECHA DE EXPIRACI&#211;N";
			st3 = "FECHA DE CADUCIDAD";
			st4 = "FORMATO: MMDDAA";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st7 = "FORMATO: DDMMAA";

		}
		else
		{
			st0 = "CHECKIN EXPIRY DATE";
			st1 = "MFG DATE";	
			st2 = "EXP DATE";
			st3 = "BEST BEFORE DATE";
			st4 = "FORMAT : MMDDYY";
			st5 = "SEND";
			st6 = "PREV";
			st7 = "FORMAT : DDMMYY";


		}		
		var dtsettingFlag = DateSetting();
		nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
	//	html = html + " document.getElementById('entermfgdate').focus();";   

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";

		
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":</td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value='" + getPOLineItemStatus + "'></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getItemBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnscanbatchno' value=" + actScannedBatchno + ">";
		html = html + "				<input type='hidden' name='hdnitemstatustext' value='" + getPOLineItemStatusText + "'>";//Case# 201410580
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entermfgdate' id='entermfgdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ":</td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterexpdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + ": </td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbbdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(dtsettingFlag == 'DD/MM/YYYY')
		{

			html = html + "			<tr>";
			html = html + "				<td align = 'left'><label>" + st7;
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		else
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><label>" + st4;
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entermfgdate').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {

			var optedEvent = request.getParameter('cmdPrevious');

			var getMfgDate = request.getParameter('entermfgdate');
			var getExpDate = request.getParameter('enterexpdate');
			var getBestBeforeDate = request.getParameter('enterbbdate');

			nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

			var POarray = new Array();
			var getLanguage = request.getParameter('hdngetLanguage');
			POarray["custparam_language"] = getLanguage;
			nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
	    	
			
			var st8,st9;
			if( getLanguage == 'es_ES')
			{
				st8 = "ERROR EN LA PANTALLA DE FECHA EXP";
				st9 = "MFGDATE NO DEBE SER MAYOR O IGUAL A EXPDATE";
				st10 = "FECHAS NO V&#193;LIDAS";
				st11 = "MFG formato de la fecha debe ser MMDDAA";
				st12 = "EXP formato de fecha debe ser MMDDAA";
				st13 = "MEJOR ANTES formato de la fecha debe ser MMDDAA";
				
			}
			else
			{
				
				st8 = "ERROR IN EXP DATE SCREEN";
				st9 = "MFGDATE SHOULD NOT BE GREATER THAN OR EQUAL TO EXPDATE";
				st10 = "INVALID DATES";
				st11 = "MFG DATE format should be MMDDYY";
				st12 = "EXP DATE format should be MMDDYY";
				st13 = "BEST BEFORE DATE format should be MMDDYY";
				
			}
			
			POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
			POarray["custparam_poid"] = request.getParameter('custparam_poid');
			POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
			POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
			POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
			POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
			POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
			POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
			POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); //request.getParameter('custparam_polinepackcode');
			POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
			POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
			POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');

			//code added on 24 feb 2012 by suman
			//To get the baseuom qty .
			POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');
			nlapiLogExecution('DEBUG','ITEMINFO[0]',POarray["custparam_itemcube"]);
			nlapiLogExecution('DEBUG','ITEMINFO[1]',POarray["custparam_baseuomqty"]);
			//end of code as of 24 feb 2012.

			POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_error"] = st8;
			POarray["custparam_screenno"] = '3B';
			POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
			nlapiLogExecution('DEBUG','hdnBatchNo',request.getParameter('hdnBatchNo'));
			POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);
			POarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
			POarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
			POarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
			POarray["custparam_actscanbatchno"] = request.getParameter('hdnscanbatchno');
			var itemShelflife = request.getParameter('hdnItemShelfLife');
			var CaptureExpirydate = request.getParameter('hdnCaptureExpiryDate');
			var CaptureFifodate = request.getParameter('hdnCaptureFifoDate');
			nlapiLogExecution('DEBUG', 'itemShelflife', itemShelflife);
			nlapiLogExecution('DEBUG', 'CaptureExpirydate', CaptureExpirydate);
			nlapiLogExecution('DEBUG', 'CaptureFifodate', CaptureFifodate);
			POarray["custparam_polineitemstatustext"] = request.getParameter('hdnitemstatustext');//Case# 201410580
			var error='';
			var errorflag='F';
			var dtsettingFlag = DateSetting();
			nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);
			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'CHECK IN EXP DATE F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
			}
			else {
				var ValueMfgDate="";
				var ValueExpDate="";
				
				var getMfgDateresult=ValidateNumeric(getMfgDate);
				var getExpDateresult=ValidateNumeric(getExpDate);
				var getBestBeforeDateresult=ValidateNumeric(getBestBeforeDate);
				
				nlapiLogExecution('Debug', 'getMfgDateresult', getMfgDateresult);
				nlapiLogExecution('Debug', 'getExpDateresult', getExpDateresult);
				nlapiLogExecution('Debug', 'getBestBeforeDateresult', getBestBeforeDateresult);
// case no 20125193�

				if((getMfgDate != '' && getMfgDate.length > 6) || (getMfgDateresult == false))
				{
					POarray["custparam_error"] = st11;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else if((getExpDate != '' && getExpDate.length > 6) || ( getExpDateresult == false))
				{
					POarray["custparam_error"] = st12;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else if((getBestBeforeDate != '' && getBestBeforeDate.length > 6) || ( getBestBeforeDateresult == false))
				{
					POarray["custparam_error"] = st13;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				//Case# 20123368 start . added one more condition in if statement
				if ((getMfgDate != '' && getExpDate == '') || (getMfgDate == '' && getExpDate != '') || (getMfgDate != '' && getExpDate != '' && getBestBeforeDate != '')||(getMfgDate != '' && getExpDate != '' && getBestBeforeDate == '')) {
					if (getMfgDate != '' && getExpDate == '')
					{
						var Mfgdate= RFDateFormat(getMfgDate,dtsettingFlag);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('DEBUG','Mfgdate',Mfgdate[1]);
							POarray["custparam_mfgdate"]=Mfgdate[1];
							ValueMfgDate=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}
					}
					if (getMfgDate == '' && getExpDate != '')
					{
						var Expdate= RFDateFormat(getExpDate,dtsettingFlag);
						if(Expdate[0]=='true')
						{
							nlapiLogExecution('DEBUG','Expdate',Expdate[1]);
							POarray["custparam_expdate"]=Expdate[1];
							ValueExpDate=Expdate[1];
						}
						else {
							errorflag='T';
							error=error+'Expdate:'+Expdate[1];
						}

					}
					//Case# 20123368 start . added one more condition in if statement
					if ((getMfgDate != '' && getExpDate != '' && getBestBeforeDate != '') || (getMfgDate != '' && getExpDate != '' && getBestBeforeDate == '')) {

						var Mfgdate= RFDateFormat(getMfgDate,dtsettingFlag);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('DEBUG','Mfgdate',Mfgdate[1]);
							POarray["custparam_mfgdate"]=Mfgdate[1];
							ValueMfgDate=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}

						var Expdate= RFDateFormat(getExpDate,dtsettingFlag);
						if(Expdate[0]=='true')
						{
							nlapiLogExecution('DEBUG','Expdate',Expdate[1]);
							POarray["custparam_expdate"]=Expdate[1];
							ValueExpDate=Expdate[1];
						}
						else {
							errorflag='T';
							error=error+'Expdate:'+Expdate[1];
						}
						//Case# 20123368  start: added if condition
						if(getBestBeforeDate !=null && getBestBeforeDate !='')
						{
							var BestBeforedate= RFDateFormat(getBestBeforeDate,dtsettingFlag);
							if(BestBeforedate[0]=='true')
							{
								nlapiLogExecution('DEBUG','BestBeforedate',BestBeforedate[1]);
								POarray["custparam_bestbeforedate"]=BestBeforedate[1];
							}
							else {
								errorflag='T';
								error=error+'BestBeforeDate:'+BestBeforedate[1];
							}
						}
						//Case# 20123368  start end 
						//POarray["custparam_mfgdate"] = RFDateFormat(getMfgDate);
						//POarray["custparam_expdate"] = RFDateFormat(getExpDate);
						//POarray["custparam_bestbeforedate"] = RFDateFormat(getBestBeforeDate);
					}
					nlapiLogExecution('DEBUG','DEBUG',error);
					if(errorflag=='F')
					{
						//Added on 2Mar 2012 by Suman
						//Checking Condn that MfgDate shld not be greater than or equal to ExpDate. 
						nlapiLogExecution('DEBUG','ValueMfgDate',ValueMfgDate);
						nlapiLogExecution('DEBUG','ValueExpDate',ValueExpDate);
						var currdate = DateStamp();
						nlapiLogExecution('DEBUG','currdate',currdate);
						if (Date.parse(ValueExpDate) <= Date.parse(currdate)) 
								{
								nlapiLogExecution('DEBUG','currdate new',currdate);
								POarray["custparam_error"] = "The Expiry Date should not be less than or equal to the current date";
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;
								}
						var d = new Date();
						var vExpiryDate='';
						if(ValueExpDate ==null || ValueExpDate =='') //if expiryDate is null 
						{
							var vExpiryDate='';
							if(itemShelflife !=null && itemShelflife!='')
							{
								d.setDate(d.getDate()+parseInt(itemShelflife));
								//vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));

								if(dtsettingFlag == 'DD/MM/YYYY')
								{
									vExpiryDate=((d.getDate())+"/"+(parseFloat(now.getMonth()) + 1)+"/"+(d.getFullYear()));
								}
								else
								{
									vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
								}

							}
							else
							{
								vExpiryDate='01/01/2099';										     
							}
							POarray["custparam_expdate"]=vExpiryDate;
							nlapiLogExecution('DEBUG','vExpiryDate',vExpiryDate);
							ValueExpDate=vExpiryDate;
						}
						nlapiLogExecution('DEBUG','ValueExpDate',ValueExpDate);
						if (Date.parse(ValueMfgDate) >= Date.parse(ValueExpDate)) 
						{
							POarray["custparam_error"] = st9;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						}
						else
						{
							if(CaptureFifodate =='F')
							{
								//If ItemCapatureFifoDate Unchecked
								POarray["custparam_fifodate"]=DateStamp();
								POarray["custparam_lastdate"]='';
								POarray["custparam_fifocode"]='';
							   response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
							}
							else
							{
							response.sendRedirect('SUITELET', 'customscript_rf_checkin_fifo_date', 'customdeploy_rf_checkin_fifo_date_di', false, POarray);
							}
						}
						//end of code as of 2Mar  
					}
					else
					{
						POarray["custparam_error"] = error;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
				}
				else {
//case # 20123721 start
					if(errorflag=='F')
					{
						//Added on 2Mar 2012 by Suman
						//Checking Condn that MfgDate shld not be greater than or equal to ExpDate. 

						var d = new Date();
						var vExpiryDate='';
						var vExpiryDate='';
						if(itemShelflife !=null && itemShelflife!='')
						{
							d.setDate(d.getDate()+parseInt(itemShelflife));
							vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
						}
						else
						{
							vExpiryDate='01/01/2099';										     
						}
						POarray["custparam_expdate"]=vExpiryDate;
						nlapiLogExecution('DEBUG','vExpiryDate',vExpiryDate);
						//ValueExpDate=vExpiryDate;

						if(POarray["custparam_mfgdate"]==null || POarray["custparam_mfgdate"]=='')
							POarray["custparam_mfgdate"]='';
						if(POarray["custparam_bestbeforedate"]==null || POarray["custparam_bestbeforedate"]=='')
							POarray["custparam_bestbeforedate"]='';

						if(CaptureFifodate =='F')
						{
							//If ItemCapatureFifoDate Unchecked
							POarray["custparam_fifodate"]=DateStamp();
							POarray["custparam_lastdate"]='';
							POarray["custparam_fifocode"]='';
							response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_rf_checkin_fifo_date', 'customdeploy_rf_checkin_fifo_date_di', false, POarray);
						}

						//end of code as of 2Mar  
					}
//end
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Catch: Location not found',e);

		}
	}
}



function ValidateNumeric(string)
{
	nlapiLogExecution('DEBUG','string',string);
	
	var iChars = "0123456789";
	if(string==null || string=='')
		return true;
	var length=string.length;
	for(var i=0;i<=length;i++)
	{
		if(iChars.indexOf(string.charAt(i))==-1)
		{
			nlapiLogExecution('DEBUG','string new',string);
			//alert("you may only enter number into this field\n");
			//break;
			return false;
		}
	}
	return true;
}
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}