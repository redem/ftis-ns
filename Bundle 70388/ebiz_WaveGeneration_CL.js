/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_WaveGeneration_CL.js,v $
 *     	   $Revision: 1.3.4.1.4.3.4.10.2.4 $
 *     	   $Date: 2015/12/01 08:30:27 $
 *     	   $Author: sponnaganti $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_204 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveGeneration_CL.js,v $
 * Revision 1.3.4.1.4.3.4.10.2.4  2015/12/01 08:30:27  sponnaganti
 * Case# 201415900
 * LP SB Issue fix
 *
 * Revision 1.3.4.1.4.3.4.10.2.3  2015/11/13 15:54:59  rmukkera
 * case # 201414431
 *
 * Revision 1.3.4.1.4.3.4.10.2.2  2015/10/21 09:17:55  schepuri
 * case# 201415140
 *
 * Revision 1.3.4.1.4.3.4.10.2.1  2015/10/15 10:20:25  skreddy
 * case:201414992
 * KRM shipcomplete issue fix
 *
 * Revision 1.3.4.1.4.3.4.10  2015/08/25 10:17:34  schepuri
 * case# 201414020
 *
 * Revision 1.3.4.1.4.3.4.9  2014/09/17 16:01:15  sponnaganti
 * Case# 201410403
 * Stnd Bundle Issue fix
 *
 * Revision 1.3.4.1.4.3.4.8  2014/09/05 09:04:11  sponnaganti
 * case# 201410245
 * Stnd Bundle issue fix
 *
 * Revision 1.3.4.1.4.3.4.7  2014/05/22 15:38:22  nneelam
 * case#  20148452
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.1.4.3.4.6  2014/04/22 09:16:52  nneelam
 * case#  20127499
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.1.4.3.4.5  2014/01/17 22:41:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20126821
 *
 * Revision 1.3.4.1.4.3.4.4  2013/06/21 23:33:44  skreddy
 * CASE201112/CR201113/LOG201121
 * to restrict when payment credit limit exceed
 *
 * Revision 1.3.4.1.4.3.4.3  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.3.4.1.4.3.4.2  2013/03/15 09:28:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.3.4.1.4.3.4.1  2013/02/26 12:56:03  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 * Revision 1.3.4.1.4.3  2012/12/20 23:52:29  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3.4.1.4.2  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.1.4.1  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.3.4.1  2012/04/17 08:40:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.3  2011/09/23 15:45:51  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/07/21 07:08:50  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function WaveGeneration_CL()
{
	var flag = 'T';
	var lineSelectedFlag = 'F';
	var unCheckArray = new Array();
	if(nlapiGetFieldValue('custpage_hiddenfieldselectpage')!='T')
	{
		try 
		{
			var lincount = nlapiGetLineItemCount('custpage_items');
			var totnooflines=0;
			var totnooforderlines=0;
			for (var p = 1; p <= lincount; p++) 
			{
				var lineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_so', p);
				var orderlineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_totallines', p);
				
				var shipcompleteflag = nlapiGetLineItemValue('custpage_items', 'custpage_shipcomplete',p);
				var FOnumber = nlapiGetLineItemValue('custpage_items', 'custpage_sono',p);
				var shipcompleteflag = nlapiGetLineItemValue('custpage_items', 'custpage_shipcomplete',p);
				var nooflinesselected = nlapiGetLineItemValue('custpage_items', 'custpage_lineno',p);
				
				
				if(lineSelected=='T')
				{
					if(shipcompleteflag == 'T')
					{
						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecord_lineord',null, 'contains', FOnumber));

						//var columns = new Array();
						//columns.push(new nlobjSearchColumn('internalid','custrecord_ns_ord','group').setSort());

						//var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
						var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, null);

						if(orderList != null && orderList != '')
						{
							var FOlength = orderList.length;

						}
						if(parseInt(FOlength) > parseInt(nooflinesselected))
						{
							//alert(FOlength);
							//alert(orderlineSelected);
							//alert('ShipComplete Flag is checked for the FO# '+FOnumber);
							alert('Ship Complete is enabled for this FO '+FOnumber+ '. Please include all the lines in a single Wave.');
							//nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F');
							return false;
						}
					}
					
					// code added from Boombah aacount on 26Feb13 by santosh
					//var nooflines=nlapiGetLineItemValue('custpage_items', 'custpage_lineno', p);
					//totnooflines=totnooflines+parseInt(nooflines);
					totnooflines=parseInt(totnooflines)+parseInt(1);
					//alert('totnooflines selected' +totnooflines);
					totnooforderlines=parseInt(totnooforderlines)+parseInt(orderlineSelected);
					//alert('totnooforderlines selected' +totnooforderlines);
					/*nlapiSetFieldValue('custpage_orderscount',totnooflines,false);
					nlapiSetFieldValue('custpage_orderslinecount',totnooforderlines,false);*/

				}	

				if(lineSelected=='T')
				{
					lineSelectedFlag = 'T';
					var FO=nlapiGetLineItemValue('custpage_items', 'custpage_sono', p);
					var HoldStatus=nlapiGetLineItemValue('custpage_items', 'custpage_paymenthold', p); 
					var AcctCreditPrefer=nlapiGetLineItemValue('custpage_items', 'custpage_acctcreditpreference', p);
					//alert(HoldStatus)
					if(HoldStatus == '<font color=#ff0000><b>CLOSED</b></font>')
						HoldStatus = "CLOSED";
					if(HoldStatus == '<font color=#ff0000><b>CANCELLED</b></font>')
						HoldStatus = "CANCELLED";
					if(HoldStatus == '<font color=#ff0000><b>HOLD</b></font>')
						HoldStatus = "HOLD";
					if(HoldStatus == '<font color=#ff0000><b>Days Over Due</b></font>')
						HoldStatus = "Days Over Due";
					if(HoldStatus == '<font color=#ff0000><b>Credit Limit Exceed</b></font>')
						HoldStatus = "Credit Limit Exceed";
					/*if(HoldStatus == "CLOSED" || HoldStatus == "CANCELLED" || HoldStatus == "HOLD" || HoldStatus == "Days Over Due")
					{
						flag = 'F';
						unCheckArray.push(p);
					}*/
					
					
					if(HoldStatus == "CLOSED")
					{
						alert('Either Sales Order is Closed');
						return false;
					}
					else if(HoldStatus == "CANCELLED")
					{
						alert('Either Sales Order is Cancelled');
						return false;
					}
					else if(HoldStatus == "HOLD")
					{
						alert('Either Sales Order Payment is HOLD');
						return false;
					}
					else if(HoldStatus == "Days Over Due")
					{
						alert('Either Sales Order Payment is Days Over Due');
						return false;
					}
															
					if(AcctCreditPrefer == 'WARNING' && flag != 'F')
					{

						if(HoldStatus == "Credit Limit Exceed")
						{
							alert('Customer Exceeded the Credit Limit ');
							return true;
						}

					}
					if(AcctCreditPrefer == 'HOLD' && flag != 'F')
					{
						if(HoldStatus == "Credit Limit Exceed")
						{
							alert('Customer Exceeded the Credit Limit');
							return false;
						}
					}
				}
			}
			nlapiSetFieldValue('custpage_orderscount',totnooflines,false);
			nlapiSetFieldValue('custpage_orderslinecount',totnooforderlines,false);
			if(parseFloat(totnooforderlines)>500)// case#201414020
			{
				alert('Total no. of lines selected cannot be greater than 500.');
				//nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F',false);
				return false;
			}

			/*if(flag == 'F')
			{
				alert('Either Sales Order is Closed/Cancelled or Payment is HOLD/Days Over Due/Credit Limit Exceed for the Orders in RED color,Please Uncheck');
				return false;
			}*/
			return checkAtleast('custpage_items', 'custpage_so');
		}
		catch(exp)
		{
			alert('Exception '+exp);
		}
	}
	return true;
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function ValidatePickLine(type, name)
{	

	if(trim(name)!=trim('custpage_selectpage'))
	{
		var lincount = nlapiGetLineItemCount('custpage_items');
		var totnooflines=0;
		var vLineSelected='F';
		var vCurrentItemIndex=0;
		var totnooforderlines=0; // case# 201415140
		if(trim(name) == trim('custpage_so'))
		{
			
			var lineSelecte = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_so');

			if(lineSelecte=='T')
			{
				vCurrentItemIndex=nlapiGetCurrentLineItemIndex('custpage_items');
			}
			
		}

		if(vCurrentItemIndex != null && vCurrentItemIndex != '')
		{
			var shipcompleteflag = nlapiGetLineItemValue('custpage_items', 'custpage_shipcomplete',vCurrentItemIndex);
			//alert('shipcompleteflag  ' +shipcompleteflag);


			var FOnumber = nlapiGetLineItemValue('custpage_items', 'custpage_sono',vCurrentItemIndex);
			var shipcompleteflag = nlapiGetLineItemValue('custpage_items', 'custpage_shipcomplete',vCurrentItemIndex);
			var nooflinesselected = nlapiGetLineItemValue('custpage_items', 'custpage_lineno',vCurrentItemIndex);
			//alert('lineSelecte  ' +lineSelecte);
			//alert(shipcompleteflag);
			if(lineSelecte=='T')
			{
				if(shipcompleteflag == 'T')
				{ 
					var FOlinelength=0;
					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_lineord',null, 'contains', FOnumber));
					filters.push(new nlobjSearchFilter('custrecord_linestatus_flag',null, 'anyof', [25]));

					var columns = new Array();
					columns.push(new nlobjSearchColumn('custrecord_lineord',null,'group').setSort());
					columns.push(new nlobjSearchColumn('custrecord_ordline',null,'count').setSort());

					var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

					if(orderList != null && orderList != '')
					{
						FOlinelength = orderList[0].getValue('custrecord_ordline',null,'count');

					}
					//alert(FOlinelength);
					//alert(nooflinesselected);
					if(parseInt(FOlinelength) > parseInt(nooflinesselected))
					{						
						//alert(orderlineSelected);
						alert('Ship Complete is enabled for this FO '+FOnumber+ '. Please include all the lines in a single Wave.');
						nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F');
						return false;
					}
				}
			}
		}
		//for (var p = 0; p < lincount; p++) 
		for (var p = 1; p <= lincount; p++)
		{
			var lineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_so', p);
			var orderlineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_totallines', p);
			if(lineSelected=='T')
			{
				totnooflines=parseInt(totnooflines)+parseInt(1);
				//alert('totnooflines selected' +totnooflines);
				totnooforderlines=parseInt(totnooforderlines)+parseInt(orderlineSelected);
				//alert('totnooforderlines selected' +totnooforderlines);
				/*nlapiSetFieldValue('custpage_orderscount',totnooflines,false);
				nlapiSetFieldValue('custpage_orderslinecount',totnooforderlines,false);

				if(parseFloat(totnooforderlines)>500)
				{
					alert('Total # of Order Lines should not be greater than 500');
					nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F',false);
					return false;
				}*/
			}
			/*else
			{
				nlapiSetFieldValue('custpage_orderscount',totnooflines,false);
				nlapiSetFieldValue('custpage_orderslinecount',totnooforderlines,false);
			}*/
		}
		nlapiSetFieldValue('custpage_orderscount',totnooflines,false);
		nlapiSetFieldValue('custpage_orderslinecount',totnooforderlines,false);
		if(lineSelecte=='T')
		{
			if(parseFloat(totnooforderlines)>500)
			{
				alert('Total # of Lines should not be greater than 500');
				nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F',false);
				return false;
			}
		}
	}
	else
	{
		if(trim(name)==trim('custpage_selectpage'))
		{
			nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
			NLDoMainFormButtonAction("submitter",true);	
		}
		else
		{
			return true;
		}
	}

}

// code added from Boombah aacount on 26Feb13 by santosh
function ValidatePickLine_wavebyline(type, name)
{	

	if(trim(name)!=trim('custpage_selectpage'))
	{
		var lincount = nlapiGetLineItemCount('custpage_items');
		var totnooflines=0;
		for (var p = 0; p < lincount; p++) 
		{
			var lineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_so', p);

			if(lineSelected=='T')
			{
				//var nooflines=nlapiGetLineItemValue('custpage_items', 'custpage_lineno', p);
				totnooflines=parseInt(totnooflines)+1;

				if(parseInt(totnooflines)>100)
				{
					alert('Total no. of lines selected cannot be greater than 100.');
					nlapiSetCurrentLineItemValue('custpage_items','custpage_so','F');
					return false;
				}
			}
		}
	}
	else
	{
		if(trim(name)==trim('custpage_selectpage'))
		{
			nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
			NLDoMainFormButtonAction("submitter",true);	
		}
		else
		{
			return true;
		}
	}

}
//upto here

function ValidatePickLine2()
{	
	var lincount = nlapiGetLineItemCount('custpage_items');
	var totnooflines=0;
	for (var p = 0; p < lincount; p++) 
	{
		var lineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_ordselect', p);

		if(lineSelected=='T')
		{
			/*var nooflines=nlapiGetLineItemValue('custpage_items', 'custpage_lineno', p);
			totnooflines=totnooflines+parseFloat(nooflines);*/
			totnooflines=parseInt(totnooflines)+1;
			//alert(totnooflines);
			if(parseFloat(totnooflines)>100)
			{
				alert('Total # of lines should not be greater than 100');
				nlapiSetCurrentLineItemValue('custpage_items','custpage_ordselect','F');
				return false;
			}
		}
	}
}


function backtosearch()
{
	//alert('hi');
	//var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavebyheaderqb', 'customdeploy_wavebyheaderqb');
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavebyorderqb', 'customdeploy_wavebyorderqb');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WaveQBURL = 'https://system.netsuite.com' + WaveQBURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WaveQBURL = 'https://system.sandbox.netsuite.com' + WaveQBURL;				
		}*/
	//window.open(WavePDFURL);
	window.location.href = WaveQBURL;
}


function backtogeneratesearch()
{
	//alert('hi');
	var queryparams=nlapiGetFieldValue('custpage_qeryparams');

	var querystrings='';

	if(queryparams!=null && queryparams!='')
	{
		var temparray=queryparams.split(',');

		if(temparray[0]!='')
		{
			var param=temparray[0];
			querystrings=querystrings+"&custpage_qbso="+param+"";
		}
		if(temparray[1]!='')
		{
			querystrings=querystrings+"&custpage_company="+temparray[1]+"";
		}
		if(temparray[2]!='')
		{
			querystrings=querystrings+"&custpage_item="+temparray[2]+"";
		}
		if(temparray[3]!='')
		{
			querystrings=querystrings+"&custpage_consignee="+temparray[3]+"";
		}
		if(temparray[4]!='')
		{
			querystrings=querystrings+"&custpage_ordpriority="+temparray[4]+"";
		}
		if(temparray[5]!='')
		{
			querystrings=querystrings+"&custpage_ordtype="+temparray[5]+"";
		}
		if(temparray[6]!='')
		{
			querystrings=querystrings+"&custpage_itemgroup="+temparray[6]+"";
		}
		if(temparray[7]!='')
		{
			querystrings=querystrings+"&custpage_itemfamily="+temparray[7]+"";
		}
		if(temparray[8]!='')
		{
			querystrings=querystrings+"&custpage_packcode="+temparray[8]+"";
		}
		if(temparray[9]!='')
		{
			querystrings=querystrings+"&custpage_uom="+temparray[9]+"";
		}
		if(temparray[10]!='')
		{
			querystrings=querystrings+"&custpage_itemstatus="+temparray[10]+"";
		}
		if(temparray[11]!='')
		{
			querystrings=querystrings+"&custpage_siteminfo1="+temparray[11]+"";
		}
		if(temparray[12]!='')
		{
			querystrings=querystrings+"&custpage_siteminfo2="+temparray[12]+"";
		}
		if(temparray[13]!='')
		{
			querystrings=querystrings+"&custpage_siteminfo3="+temparray[13]+"";
		}
		if(temparray[14]!='')
		{
			querystrings=querystrings+"&custpage_shippingcarrier="+temparray[14]+"";
		}
		if(temparray[15]!='')
		{
			querystrings=querystrings+"&custpage_country="+temparray[15]+"";
		}
		if(temparray[16]!='')
		{
			querystrings=querystrings+"&custpage_state="+temparray[16]+"";
		}
		if(temparray[17]!='')
		{
			querystrings=querystrings+"&custpage_city="+temparray[17]+"";
		}
		if(temparray[18]!='')
		{
			querystrings=querystrings+"&custpage_addr1="+temparray[18]+"";
		}
		if(temparray[19]!='')
		{
			querystrings=querystrings+"&custpage_shipmentno="+temparray[19]+"";
		}
		if(temparray[20]!='')
		{
			querystrings=querystrings+"&custpage_routeno="+temparray[20]+"";
		}
		if(temparray[21]!='')
		{
			querystrings=querystrings+"&custpage_carrier="+temparray[21]+"";
		}
		if(temparray[22]!='')
		{
			querystrings=querystrings+"&custpage_soshipdate="+temparray[22]+"";
		}
		if(temparray[23]!='')
		{
			var param=temparray[23];
			querystrings=querystrings+"&custpage_wmsstatusflag="+param+"";
		}
	}
	//var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_hook_sl_1', 'customdeploy_ebiz_hook_sl_1');
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavebyorder', 'customdeploy_wavebyorder');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WaveQBURL = 'https://system.netsuite.com' + WaveQBURL+querystrings;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WaveQBURL = 'https://system.sandbox.netsuite.com' + WaveQBURL+querystrings;				
		}*/
	//window.open(WavePDFURL);

	window.location.href = WaveQBURL;
}


function Printreport(ebizwaveno){
	//alert('Print');
	var fullfillment=null;
	
	var vCustomReportRule = getSystemRuleCurstomReport('PICKREPORTPRINT');
	
	var WavePDFURL;
	if(vCustomReportRule !=null && vCustomReportRule != '' && vCustomReportRule != 'Y')
	{
		var vScriptId=vCustomReportRule.getValue('custrecord_ebizrule_scriptid');
		var vDeploymentId=vCustomReportRule.getValue('custrecord_ebizrule_deployid');
		var vRuleValue=vCustomReportRule.getValue('custrecord_ebizrulevalue');
		if((vRuleValue == 'CUSTOM' || vRuleValue == 'STANDARD') && vScriptId != null && vScriptId != '' && vDeploymentId != null && vDeploymentId != '')
			WavePDFURL = nlapiResolveURL('SUITELET', vScriptId, vDeploymentId);
		else
			WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
	}	
	else
		WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
	
	
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	//var linkURL = "https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=141&deploy=1";	

	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ ebizwaveno+'&custparam_ebiz_fullfilmentno=';
	//alert(WavePDFURL);
	window.open(WavePDFURL);
}

function getSystemRuleCurstomReport(RuleValue)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', RuleValue);
		//filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');
		columns[2] = new nlobjSearchColumn('custrecord_ebizrule_scriptid');
		columns[3] = new nlobjSearchColumn('custrecord_ebizrule_deployid');
		
		

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0];
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetSystemRules: ', exp);
		return 'Y';
	}	
}

function backtoreleaseorders()
{
	//var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavebyheaderqb', 'customdeploy_wavebyheaderqb');
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_wavereleaseqb', 'customdeploy_ebiz_wavereleaseqb');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WaveQBURL = 'https://system.netsuite.com' + WaveQBURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WaveQBURL = 'https://system.sandbox.netsuite.com' + WaveQBURL;				
		}*/
	//window.open(WavePDFURL);
	window.location.href = WaveQBURL;
}

function gotowavestatus()
{
	//var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavebyheaderqb', 'customdeploy_wavebyheaderqb');
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavestatusreport', 'customdeploy_wavestatusreport');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WaveQBURL = 'https://system.netsuite.com' + WaveQBURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WaveQBURL = 'https://system.sandbox.netsuite.com' + WaveQBURL;				
		}*/
	//window.open(WavePDFURL);
	window.location.href = WaveQBURL;
}
function backtoreleaseWOorders()
{
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_wo_wave_release_qb', 'customdeploy_ebiz_wo_wave_release_qb');
	
	window.location.href = WaveQBURL;
}
function WOgotowavestatus(waveno)
{
	
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wopickreport', 'customdeploy_wopickreport');
	WaveQBURL = WaveQBURL + '&custpage_waveno='+ waveno;
	window.location.href = WaveQBURL;
}
function gotoclusterstatus()
{
	//var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavebyheaderqb', 'customdeploy_wavebyheaderqb');
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_clusterpickreport', 'customdeploy_clusterpickreport');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WaveQBURL = 'https://system.netsuite.com' + WaveQBURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WaveQBURL = 'https://system.sandbox.netsuite.com' + WaveQBURL;				
		}*/
	//window.open(WavePDFURL);
	window.location.href = WaveQBURL;
}
function fieldChanged(type, name)
{
	var lincount = nlapiGetLineItemCount('custpage_items');
	var totnooflines=0;
	for (var p = 0; p < lincount; p++) 
	{
		var lineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_ordselect', p);

		if(lineSelected=='T')
		{
			/*var nooflines=nlapiGetLineItemValue('custpage_items', 'custpage_lineno', p);
			totnooflines=totnooflines+parseFloat(nooflines);*/
			totnooflines=parseInt(totnooflines)+1;

			if(parseFloat(totnooflines)>100)
			{
				alert('Total # of lines should not be greater than 100');
				nlapiSetCurrentLineItemValue('custpage_items','custpage_ordselect','F');
				return false;
			}
		}
	}
}
function validatenooflines()
{
	var lincount = nlapiGetLineItemCount('custpage_items');
	var totnooflines=0;
	for (var p = 0; p < lincount; p++) 
	{
		var lineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_ordselect', p);

		if(lineSelected=='T')
		{
			/*var nooflines=nlapiGetLineItemValue('custpage_items', 'custpage_lineno', p);
			totnooflines=totnooflines+parseFloat(nooflines);*/
			totnooflines=parseInt(totnooflines)+1;

			if(parseFloat(totnooflines)>100)
			{
				alert('Total no. of lines selected cannot be greater than 100.');
				nlapiSetCurrentLineItemValue('custpage_items','custpage_ordselect','F');
				return false;
			}
		}
	}
}
