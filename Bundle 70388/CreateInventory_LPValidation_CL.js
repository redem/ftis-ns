/***************************************************************************
�eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/CreateInventory_LPValidation_CL.js,v $
 *� $Revision: 1.1.2.10.4.2.2.55.2.3 $
 *� $Date: 2015/11/16 07:22:22 $
 *� $Author: grao $
 *� $Name: t_WMS_2015_2_StdBundle_1_147 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: CreateInventory_LPValidation_CL.js,v $
 *� Revision 1.1.2.10.4.2.2.55.2.3  2015/11/16 07:22:22  grao
 *� 2015.2 Issue Fixes 201415615
 *�
 *� Revision 1.1.2.10.4.2.2.55.2.2  2015/11/09 15:19:00  grao
 *� 2015.2 Issue Fixes 201415368
 *�
 *� Revision 1.1.2.10.4.2.2.55.2.1  2015/10/27 14:28:04  aanchal
 *� 2015.2 issue fixes
 *� 201415008
 *�
 *� Revision 1.1.2.10.4.2.2.55  2015/09/01 15:07:03  aanchal
 *� 2015.2 issue fixes
 *� 201414069
 *�
 *� Revision 1.1.2.10.4.2.2.54  2015/08/13 15:36:02  skreddy
 *� Case# 201413829
 *� Bedgear SB issue fix
 *�
 *� Revision 1.1.2.10.4.2.2.53  2015/08/11 15:40:59  grao
 *� 2015.2   issue fixes  201413925
 *�
 *� Revision 1.1.2.10.4.2.2.52  2015/07/21 15:10:14  grao
 *� 2015.2   issue fixes  201412928
 *�
 *� Revision 1.1.2.10.4.2.2.51  2015/05/05 15:21:43  grao
 *� SB issue fixes  201412639
 *�
 *� Revision 1.1.2.10.4.2.2.50  2015/02/18 13:37:26  schepuri
 *� issue# 201410840
 *�
 *� Revision 1.1.2.10.4.2.2.49  2014/10/17 13:34:54  skavuri
 *� Case# 201410605 Std bundle Issue fixed
 *�
 *� Revision 1.1.2.10.4.2.2.48  2014/08/08 06:58:01  nneelam
 *� case# 20149821
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.10.4.2.2.47  2014/08/07 15:28:46  nneelam
 *� case#  20149821�
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.10.4.2.2.46  2014/06/19 15:43:04  sponnaganti
 *� case# 20148981
 *� Compatability test acc issue fixes
 *�
 *� Revision 1.1.2.10.4.2.2.45  2014/06/19 15:31:50  grao
 *� Case#: 20148979   New GUI account issue fixes
 *�
 *� Revision 1.1.2.10.4.2.2.44  2014/06/18 13:56:34  grao
 *� Case#: 20148921 New GUI account issue fixes
 *�
 *� Revision 1.1.2.10.4.2.2.43  2014/06/03 08:51:03  skavuri
 *� Case # 20148699 SB Issue Fixed
 *�
 *� Revision 1.1.2.10.4.2.2.42  2014/06/03 07:15:40  grao
 *� Case#: 20148615 Standard  issue fixes
 *�
 *� Revision 1.1.2.10.4.2.2.41  2014/05/30 14:42:33  grao
 *� Case#: 20148615�,20148616,20148617,20148618 Standard  issue fixes
 *�
 *� Revision 1.1.2.10.4.2.2.40  2014/05/23 15:24:49  sponnaganti
 *� case# 20148195
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.1.2.10.4.2.2.39  2014/05/22 15:32:33  sponnaganti
 *� case# 20148195
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.1.2.10.4.2.2.38  2014/05/22 06:52:13  spendyala
 *� Issue fixed related to case#20148312
 *�
 *� Revision 1.1.2.10.4.2.2.37  2014/05/01 14:09:39  sponnaganti
 *� case# 20148216,20148221
 *� Standard bundle Issue fix
 *�
 *� Revision 1.1.2.10.4.2.2.36  2014/04/29 15:52:33  skavuri
 *� Case# 20148217 SB Issue Fixed
 *�
 *� Revision 1.1.2.10.4.2.2.35  2014/04/24 16:06:32  skavuri
 *� Case# 20148037 issue fixed
 *�
 *� Revision 1.1.2.10.4.2.2.34  2014/03/20 15:51:07  skavuri
 *� Case # 20127766 issue fixed
 *�
 *� Revision 1.1.2.10.4.2.2.33  2014/03/18 15:22:44  skavuri
 *� Case# 20127729 issue fixed
 *�
 *� Revision 1.1.2.10.4.2.2.32  2014/03/14 15:54:17  skreddy
 *� case 20127474
 *� Standard Bundle issue fix
 *�
 *� Revision 1.1.2.10.4.2.2.31  2014/03/12 06:36:32  skreddy
 *� case 20127214
 *� Deal med SB issue fixs
 *�
 *� Revision 1.1.2.10.4.2.2.30  2014/03/05 15:34:24  skavuri
 *� Case # 20127475 issue fixed
 *�
 *� Revision 1.1.2.10.4.2.2.29  2014/03/04 15:35:33  skavuri
 *� Case # 20127475 issue fixed
 *�
 *� Revision 1.1.2.10.4.2.2.28  2014/02/25 23:42:04  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue fixed related to case#20127382
 *�
 *� Revision 1.1.2.10.4.2.2.27  2014/02/17 15:36:08  nneelam
 *� case#  20127186
 *� Standard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.10.4.2.2.26  2014/02/13 14:49:50  sponnaganti
 *� case# 20127148
 *� (filter added for checking Location type is stage)
 *�
 *� Revision 1.1.2.10.4.2.2.25  2014/02/07 13:23:58  sponnaganti
 *� case# 20127085
 *� if condition is added for checking remaining cube null or empty
 *�
 *� Revision 1.1.2.10.4.2.2.24  2014/01/06 15:43:12  nneelam
 *� case# 20126635
 *� Std Bundle Issue Fix.
 *�
 *� Revision 1.1.2.10.4.2.2.23  2013/12/10 13:07:28  schepuri
 *� 20126279
 *�
 *� Revision 1.1.2.10.4.2.2.22  2013/12/02 13:40:39  gkalla
 *� case#20125996
 *� Lot number validation in GUI create inventory
 *�
 *� Revision 1.1.2.10.4.2.2.21  2013/11/28 15:20:32  grao
 *� Case# #: 20126023 , 20126024, 20126025, 20126026, 20126027, 20126028, 20126029, 20126030, 20126031, 20126032  related issue fixes in SB 2014.1
 *�
 *� Revision 1.1.2.10.4.2.2.20  2013/10/19 13:11:20  grao
 *� Issue fixes 20125108
 *�
 *� Revision 1.1.2.10.4.2.2.19  2013/10/03 09:07:26  schepuri
 *� system is saying Lot# is not required for normal item for Lot item  Item #
 *�
 *� Revision 1.1.2.10.4.2.2.18  2013/09/25 06:52:24  schepuri
 *� FIFO Date and Expiry Dates are not getting updated in inventory report
 *�
 *� Revision 1.1.2.10.4.2.2.17  2013/09/19 15:34:46  nneelam
 *� Case#. 20124490
 *� Invaid LP alert even if given correct LP Issue  Fixed.
 *�
 *� Revision 1.1.2.10.4.2.2.16  2013/09/04 15:37:21  skreddy
 *� Case# 20124230
 *� standard bundle issue fix
 *�
 *� Revision 1.1.2.10.4.2.2.15  2013/08/28 15:58:10  nneelam
 *� Case#.20124080
 *� Creating Inventory Not updating through UI
 *�
 *� Revision 1.1.2.10.4.2.2.14  2013/08/27 16:04:16  nneelam
 *� Case#.  20123793
 *� Create Inventory Validation
 *�
 *� Revision 1.1.2.10.4.2.2.13  2013/08/19 16:45:51  grao
 *� Standard bundle issues fixes 20123864�
 *�
 *� System should  not allow to create the Inventory when the bin location is not related to the selected site. And display the valid message
 *�
 *� Revision 1.1.2.10.4.2.2.12  2013/08/13 15:59:52  nneelam
 *� Case#.  20123887
 *� Standard Bundle Issue Fixed..
 *�
 *� Revision 1.1.2.10.4.2.2.11  2013/08/13 15:10:39  rmukkera
 *� Issue Fix related to 20123791�,20123790�,20123789�
 *�
 *� Revision 1.1.2.10.4.2.2.10  2013/08/09 15:21:42  rmukkera
 *� standard bundle issue fix
 *�
 *� Revision 1.1.2.10.4.2.2.9  2013/05/31 15:04:41  grao
 *� CASE201112/CR201113/LOG201121
 *� PMM Issues fixes
 *�
 *� Revision 1.1.2.10.4.2.2.8  2013/05/21 07:30:30  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.1.2.10.4.2.2.7  2013/05/20 15:28:48  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.1.2.10.4.2.2.6  2013/05/01 15:22:27  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issue fixes
 *�
 *� Revision 1.1.2.10.4.2.2.5  2013/04/15 15:02:14  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.1.2.10.4.2.2.4  2013/04/03 20:40:48  kavitha
 *� CASE201112/CR201113/LOG2012392
 *� TSG Issue fixes
 *�
 *� Revision 1.1.2.10.4.2.2.3  2013/04/03 17:11:38  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� Standed bundle
 *�
 *� Revision 1.1.2.10.4.2.2.2  2013/04/03 03:14:29  kavitha
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.1.2.10.4.2.2.1  2013/03/22 11:44:30  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.1.2.10.4.2  2013/02/06 00:36:21  kavitha
 *� CASE201112/CR201113/LOG201121
 *� Serial # functionality - Inventory process
 *�
 *� Revision 1.1.2.10.4.1  2012/11/01 14:54:56  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.1.2.10  2012/07/19 14:14:31  schepuri
 *� CASE201112/CR201113/LOG201121
 *� validation on NS confirm
 *�
 *� Revision 1.1.2.9  2012/04/20 13:20:29  schepuri
 *� CASE201112/CR201113/LOG201121
 *� changing the Label of Batch #  field to Lot#
 *�
 *� Revision 1.1.2.8  2012/03/13 12:47:24  schepuri
 *� CASE201112/CR201113/LOG201121
 *� removing unwanted alerts
 *�
 *� Revision 1.1.2.7  2012/03/02 13:05:15  schepuri
 *� CASE201112/CR201113/LOG201121
 *� stable bundle issue fixing
 *�
 *� Revision 1.1.2.6  2012/02/27 13:44:46  schepuri
 *� CASE201112/CR201113/LOG201121
 *� issue fixing
 *�
 *� Revision 1.1.2.5  2012/02/27 13:41:10  schepuri
 *� CASE201112/CR201113/LOG201121
 *� issue fixing
 *�
 *� Revision 1.1.2.4  2012/02/24 13:07:26  schepuri
 *� CASE201112/CR201113/LOG201121
 *� issue fixing for batch item validation
 *�
 *� Revision 1.1.2.3  2012/02/02 13:01:00  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Added columnlp variable for search inv record.
 *�
 *� Revision 1.1.2.2  2012/01/20 13:20:38  spendyala
 *� CASE201112/CR201113/LOG201121
 *� issue fixed.
 *�
 *� Revision 1.1.2.1  2012/01/20 13:17:24  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Fixed search criteria  issue.
 *�
 *
 ****************************************************************************/

var eventtype;
var QtyChangeCheck=false;
var QtyOldValue="";
var enteredQuantity;
function  myPageInit(type){
	QtyOldValue = nlapiGetFieldValue('custrecord_ebiz_inv_qty');
	eventtype=type;
}
function  CheckQtyChange(type,name,linenum){
	if (name=='custrecord_ebiz_inv_qty'){	
		QtyChangeCheck=true;
	}
}

function ebiznet_CreateInventory_LPRange_CL(fld){

	nlapiLogExecution('ERROR', 'type', eventtype);
	//var inventoryQuantity = nlapiGetFieldValue('custrecord_ebiz_inv_qty');
	var allocationQuantity = nlapiGetFieldValue('custrecord_ebiz_alloc_qty');
	var quantityOnHand = nlapiGetFieldValue('custrecord_ebiz_qoh');
	var availableQuantity = nlapiGetFieldValue('custrecord_ebiz_avl_qty');		
	enteredQuantity = nlapiGetFieldValue('custrecord_ebiz_inv_qty');
	var packCode = nlapiGetFieldValue('custrecord_ebiz_inv_packcode'); 
	var lpNo = nlapiGetFieldValue('custrecord_ebiz_inv_lp');
	var itemId = nlapiGetFieldValue('custrecord_ebiz_inv_sku');
	var binLocationId = nlapiGetFieldValue('custrecord_ebiz_inv_binloc');
	var vId = nlapiGetFieldValue('id');
	var company=nlapiGetFieldValue('custrecord_ebiz_inv_company');
	var site=nlapiGetFieldValue('custrecord_ebiz_inv_loc');
	var locationtext=nlapiGetFieldText('custrecord_ebiz_inv_loc');
	var locationId=nlapiGetFieldValue('custrecord_ebiz_inv_loc');
	//case # 20126635 start
	var itemStatus=nlapiGetFieldText('custrecord_ebiz_inv_sku_status');
	//End

	nlapiLogExecution('ERROR', 'quantityOnHand', quantityOnHand);
	nlapiLogExecution('ERROR', 'enteredQuantity', enteredQuantity);
	nlapiSetFieldValue('custrecord_ebiz_qoh',enteredQuantity); // case# 201410840
	//case # 20124490� Start
	nlapiLogExecution('ERROR', 'locationId', locationId);
	//case # 20124490� Start
	//case# 20148195 starts
	//alert('company ' + company);
	//case # 20149821
	
	if(company!=null && company!=''){
		
		var filters = new Array();
		if(site!=null && site!='')
			filters.push(new nlobjSearchFilter('internalid',null,'is',site));
		filters.push(new nlobjSearchFilter('custrecordcompany',null,'anyof',['@NONE@',company]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		
		var binlocresult = nlapiSearchRecord('location', null, filters, null);

		//alert('binlocresult ' + binlocresult);
		if(binlocresult == null || binlocresult == '')
		{
			alert('Entered Company is not related to the: ' + " ' " +  locationtext  + " '" + ' Site');
			return false;
		}
		
	}
	var rolelocation=getRoledBasedLocation();
	if(locationId!=null && locationId !=''&& rolelocation!='null' && rolelocation!=null && rolelocation!='' && rolelocation!=0)
	{
		if(rolelocation.indexOf(parseInt(locationId))!= -1)
		{
		//	return true;
		}
		else
		{
			alert("INVALID LOCATION");
			return false;
			//return true;
		}
		
	}
	
	
	var ConfirmtoNS=nlapiGetFieldValue('custrecord_ebiz_callinv');
	var adjusttype=nlapiGetFieldValue('custrecord_ebiz_inv_adjusttype');
	var tasktype=nlapiGetFieldValue('custrecord_invttasktype');
	var ItemType='';
		var batchflag='F';
	//case# 20123793 start
	if(itemId!=null && itemId!=''){
		// Case# 20148217 starts
		//var columns = nlapiLookupField('item', itemId, 'recordType','custitem_ebizbatchlot');
		var columns = nlapiLookupField('item', itemId, ['recordType','custitem_ebizbatchlot']);
		// Case# 20148217 ends
		if(columns != null && columns != '')
		{
			ItemType=columns.recordType;
			batchflag=columns.custitem_ebizbatchlot;
		}

		if(ItemType!= 'lotnumberedinventoryitem' && ItemType!= 'lotnumberedassemblyitem' && batchflag=="F")
		
		{
			var LOT=nlapiGetFieldValue('custrecord_ebiz_inv_lot');

			if(LOT!=null && LOT!="")
			{
				alert('No LOT# required for Normal Item');
				return false;
			}

		}
	}
	else
		{
		alert('Please enter value for Item');
		return false;
		}
	
	
	/*if(binLocationId !=null && binLocationId !='')
	{
	var filters = new Array();
		
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binLocationId));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'noneof', itemId));
	

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	

	if(invtsearchresults != null && invtsearchresults !='')
	{
		
		var res = confirm("There exists another item with same binlocation do you want to merge?");
		if(res!=true)
		{
			nlapiSetFieldValue('custrecord_ebiz_inv_binloc','');
			return false;
		}
		}
	}
	else
	{
		alert("Please enter value for: Binlocation");
		return false;
		
	}*/
	
	//case# 20123793 end


	/*if(ConfirmtoNS == 'y' || ConfirmtoNS == 'n' || ConfirmtoNS == 'Y' || ConfirmtoNS == 'N')
	{
		ConfirmtoNS = 'Y';
		nlapiSetCurrentLineItemValue('customrecord_ebiznet_createinv','custrecord_ebiz_callinv',ConfirmtoNS);
	}
	else
	{
		alert("Please enter value in Confirm to NS (Y/N)");
		return false;
	}*/

	//case # 20126635 start
	
	if(itemStatus!=null && itemStatus!=''){
	
		
		var itemStatusfilters = new Array();
	
			itemStatusfilters.push(new nlobjSearchFilter('name',null,'is',itemStatus));
			//case # 20127475 starts
			if(site!=''&& site!='null'&& site!=null)					
		itemStatusfilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus',null,'anyof',site));
		itemStatusfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));		 
		var itemStatuslocresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusfilters, null);

	
		if(itemStatuslocresults == null || itemStatuslocresults == '')
		{
			alert('Entered Item Status is not related to the: ' + " ' " +  locationtext  + " '" + ' Site');
			return false;
		}
		
	}
	
	//End
	
	
	if(binLocationId!=null && binLocationId!='')
	{
		//alert("site:" + site);
		//  alert("binLocationId:" + binLocationId);
		//alert("site:" + site);

		var binfilters = new Array();
		if(site!=null && site!='')
			binfilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',site));
		binfilters.push(new nlobjSearchFilter('internalid',null,'is',binLocationId));
		binfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		//case# 20127148 starts (filter added for checking Location type is stage)
		//binfilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'noneof', ['8']));//8 stage //Case# 201410605
		binfilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'noneof', ['8','3']));//8 stage  3Dock
		//case# 20127148 end
		var binlocresults = nlapiSearchRecord('customrecord_ebiznet_location', null, binfilters, null);

		//Case Start 20126023�
		if(binlocresults == null || binlocresults == '')
		{
			//alert('Entered bin location is not related to the: ' + " ' " +  locationtext  + " '" + ' Site');
			//case # 20127186...........alert msg changed.
			alert('Invalid bin location');
			return false;
		}
		//end
	}


	if(ConfirmtoNS == 'y' || ConfirmtoNS == 'Y')
	{

		ConfirmtoNS = 'Y';
		nlapiSetFieldValue('custrecord_ebiz_callinv',ConfirmtoNS);
	}
	else if(ConfirmtoNS == 'n' || ConfirmtoNS == 'N')
	{
		ConfirmtoNS = 'N';
		nlapiSetFieldValue('custrecord_ebiz_callinv',ConfirmtoNS);
	}
	else
	{
		alert("Invalid value in Confirm to NS ,please enter (Y/N)");
		return false;
	}

	var locationCubeValidate;

	var type = "PALT";
	var getLPNo = lpNo;
	var result = "";
	var uomLevel = 3;
	var UOM = "PALLET";

	if(parseFloat(enteredQuantity) <= 0){
		alert('Qty should be greater than 0');
		return false;
	}
	else if(parseFloat(allocationQuantity)<0){
		alert('Allocated Qty should be greater than 0');
		return false;
	}
	else if(parseFloat(quantityOnHand)<=0){
		alert('Qty On Hand should be greater than 0');
		return false;
	}
	else if(parseFloat(availableQuantity)<=0){
		alert('Qty Available should be greater than 0');
		return false;
	}
	if(ConfirmtoNS == 'Y'){

		//alert('ConfirmtoNS2'+ConfirmtoNS);

		var filterStAccNo = new Array();
		var colsStAccNo = new Array();

		if(adjusttype != '' && adjusttype != null){
			filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));
		}
		
		if(tasktype != "" && tasktype != null && tasktype != " "){
			//alert('tasktype',+tasktype);
			filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));
		}
		else
		{
			//alert('tasktype1',+tasktype);
			filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'is',['10']));
		}
		if(site != '' && site != null){
			filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[site]));
		}
		filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));


		colsStAccNo[0] = new nlobjSearchColumn('name');    

		var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);

		if(StockAdjustAccResults == null || StockAdjustAccResults == '')
		{
			alert("Please enter valid Adjust Type or Task Type");
			return false;
		}

		}
	//}

	//case# 20148216 starts(if condition added)
	if((itemId!=null &&itemId!='')&&(packCode!=null && packCode!=''))
	var itemQuantity = getPalletQuantityFromItemDims(itemId, packCode);
	//case# 20148216 end
	if(parseFloat(itemQuantity)==0)
	{
		alert("Please Select valid packcode");
		return false; 
	}

	if(parseFloat(enteredQuantity) > parseFloat(itemQuantity)){
		alert("Qty is greater than Pallet Qty");
		return false;    		
	}
	if(eventtype=='edit' && QtyChangeCheck==true)
	{
		if(parseFloat(QtyOldValue)!=parseFloat(enteredQuantity)){

			if(parseFloat(QtyOldValue)>parseFloat(enteredQuantity)){
				var val=parseFloat(QtyOldValue)-parseFloat(enteredQuantity);
				locationCubeValidate = validateLocationCube(val, itemId, binLocationId);
			}
			else if(parseFloat(QtyOldValue)<parseFloat(enteredQuantity))
			{
				var val=parseFloat(enteredQuantity)-parseFloat(QtyOldValue);
				locationCubeValidate = validateLocationCube(val, itemId, binLocationId);
			}
		}
	}
	else if(eventtype=='edit' && QtyChangeCheck==false)
	{    	
		var str=ValidateLot(itemId);
		return str;
	}
	else if(eventtype=='create' || eventtype=='copy')
	{
		//case# 20148221 starts (if conditions added)
		if(binLocationId !=null && binLocationId!='')
			{
			if(itemId !=null && itemId!='')
				{				
					if(enteredQuantity !=null && enteredQuantity!='' && enteredQuantity>0)
					{
					locationCubeValidate = validateLocationCube(enteredQuantity, itemId, binLocationId);
					}
					else{
						alert('Plesae Enter Quantity');
						return false;
					}
				}
			else{
				alert('Please Enter Item');
				return false;
			}
			}
		else{
			alert('Please Enter BinLocation');
			return false;
		}
		//case# 20148221 end
	}

	//nlapiLogExecution('ERROR','locationCubeValidate', locationCubeValidate.toString());// Case# 20148699
	//alert('locationCubeValidate' +locationCubeValidate);


	if(binLocationId !=null && binLocationId != "")
	{
		//Case#:20125108�Start  -->Not allow the create inventory for the bin location which is not related to the given site.
		//alert("site:" + site);		
		//alert("binLocationId:" + binLocationId);
		var binfilters = new Array();
		if(site!=null && site!='')
			binfilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',site));
		binfilters.push(new nlobjSearchFilter('internalid',null,'is',binLocationId));
		binfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		//case# 20127148 starts (filter added for checking Location type is stage)
		//binfilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'noneof', ['8']));//8 stage //Case# 201410605
		binfilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'noneof', ['8','3']));//8 stage  3Dock
		//case# 20127148 end
		var binlocresults = nlapiSearchRecord('customrecord_ebiznet_location', null, binfilters, null);

		if(binlocresults == null || binlocresults == '')
		{
		//	alert('Entered bin location is not related to the: ' + " ' " +  locationtext  + " '" + ' Site');
			alert('Invalid bin location');
			return false;
		}
		
		//end
		
		if(company != null && company != "")
		{
			if (locationCubeValidate != false){
				if (getLPNo != "" && getLPNo != null){
					if(eventtype!='edit')
					{
						//case # 20124490� Start
						result = validateLPRange(getLPNo,locationId);
						//case # 20124490� End
						if (result == "Y") {
							var validateLPNo = validateLPMaster(getLPNo,company,site);
							if(validateLPNo != null){
								alert('LP Already Exists');
								return false;
							}
							else{
								var str=ValidateLot(itemId);
								return str;
							}
						}

						else{
							alert('Invalid LP Range!');
							return false;
						}	
					}
					else
					{
						var str=ValidateLot(itemId);
						return str;
					}
				}
				else{
					alert('Please Enter LP No');
					return false;
				}
			}
			else{

				alert('Quantity exceeds location cube');		
				return false;
			}
		}
		else
		{
			alert("Please enter value for: Company");
			return false;
		}
	}
	else
	{
		alert("Please enter value for: Binlocation");
		return false;

	}


	/* var lpno = nlapiGetFieldValue('custrecord_ebiz_inv_lp');
    var type = "PALT";
    var vargetlpno = lpno;
    var vResult = "";

    if (vargetlpno.length > 0) {

    	var column=new Array();
    	column[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, null, column);

        if (searchresults) {
            for (var i = 0; i < Math.min(50, searchresults.length); i++) {
                try {
                    var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');

                    var recid = searchresults[i].getId();

                    var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);

                    var varBeginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');

                    var varEndRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');

                    var getLPGenerationTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');

                    var getLPTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');

                    if (getLPTypeValue == "1") {
                        var getLPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');

                        var LPprefixlen = getLPrefix.length;
                        var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();


                        if (vLPLen == getLPrefix) {

                            var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);

                            if (varnum.length > varEndRange.length) {

                                vResult = "N";
                                break;
                            }


                            if ((parseFloat(varnum) < parseFloat(varBeginLPRange)) || (parseFloat(varnum) > parseFloat(varEndRange))) {

                                vResult = "N";
                                break;
                            }
                            else {
                                vResult = "Y";
                                break;
                            }
                        }
                        else {
                            vResult = "N";
                        }

                    } //end of if statement
                    else {
                        //alert("in else");
                    }

                } 
                catch (err) {
                    // alert("exception" + err + "value is ");

                }
            } //end of for loop
        }

        if (vResult == "Y") {
            //alert('LP is with in the range of numbers');
            return true;

        }
        else {
            alert('Invalid LP Range!');
            return false;
        }
    }
    else {
        alert('Invalid LP No');
        return false;
    }*/

}

/**
 * This function is to filter and get the pallet quantity from the item dimensions for
 * 	the item, uom and pack code
 * 	This returns the itemQuantity for the specific item
 * @param itemId
 * @param uomLevel
 * @param packCode
 * @returns {Number}
 */
function getPalletQuantityFromItemDims(itemId, packCode){
	var itemQuantity = 0;
	var filters = new Array(); 
	//case #20127475 starts
	if(itemId!=''&&itemId!='null'&&itemId!=null)
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is',itemId);
	//	filters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', [3]);
	//Case# 20127729	if(packCode!=''&&packCode!='null'&&packCode!=null)
		// case #20127475 end
	filters[1] = new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',packCode);
	//case# 20123887, removed one extra closed bracket.
	filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true);//it fetches the records with highest uomlevel first.

	var searchItemResults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);
	if(searchItemResults!=null){    		
		itemQuantity = searchItemResults[0].getValue('custrecord_ebizqty');  
	}    	

	return itemQuantity;	
}

/**
 * This function is to validate the LP number if it exists within the LP Range
 * 	The parameters that this function checks are:
 * 		LP Prefix, LP Begin number and LP End number
 */
function validateLPRange(getLPNo,loc){
	var result="";
   
	//case # 20124490� Start
	
	var filtersLpRange = new Array();

	if(loc!=null && loc!="")
	filtersLpRange.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', loc));
	// Case # 20127766 starts
	filtersLpRange.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', '2'));
	filtersLpRange.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	// Case # 20127766 ends
	//case # 20124490? End
	
	var columnLPRange=new Array();
	columnLPRange[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	var lpRangeSearchResults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filtersLpRange, columnLPRange);

	if (lpRangeSearchResults) {
		for (var i = 0; i < lpRangeSearchResults.length; i++) {
			try {
				var getLPPrefix = (lpRangeSearchResults[i].getValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();
				var recordId = lpRangeSearchResults[i].getId();

				var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recordId);
				var beginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');
				var endRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');
				var lpGenerationType = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');
				var lpType = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');

				if (lpType == '1') {
					var lpPrefix = (transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();
					var lpPrefixLen = getLPPrefix.length;
					var vLPLen = getLPNo.substring(0, lpPrefixLen).toUpperCase();
					if (vLPLen == getLPPrefix) {
						var varnum = getLPNo.substring(lpPrefixLen, getLPNo.length);
						
						
						if(isNaN(varnum)){
							result="N";
							break;

						}
												
						if (varnum.length > endRange.length) {
							result = "N";
							break;
						}
						if ((parseFloat(varnum,10) < parseFloat(beginLPRange)) || (parseFloat(varnum,10) > parseFloat(endRange))) {
							result = "N";
							break;
						}
						else{
							result = "Y";
							break;
						}

					}
					else {
						result = "N";
					}
				}
			} 
			catch (err) {
			}
		}
	}
	return result;
}

/**
 * This function is to validate the LP # if it exists im the LP master
 */

function validateLPMaster(lpNo,company,site){
	var lpMasterFilters = new Array();				
	lpMasterFilters[0] = new nlobjSearchFilter('name', null, 'is', lpNo);
	lpMasterFilters[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof',['@NONE@',site]);
	lpMasterFilters[2] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_company', null, 'anyof', ['@NONE@',company]);
	var lpMasterColumns = new Array();
	lpMasterColumns[0] = new nlobjSearchColumn('name');
	var lpMasterSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpMasterFilters,lpMasterColumns);		
	return lpMasterSearchResults;
}


/**
 * This function is to validate the location cube for the item, bin location and the quantity passed
 */
function validateLocationCube(quantity, itemValue, binLocation){

	var cube = 0.0;
	var baseUOMQty = 0.0;
	var remCube = 0.0;
	var itemUomCube = 0.0;
	var anotherItemCube = 0.0;

	var resultQty = quantity;

//	This filters are to fetch the item dimensions for the item and the bin location	
	var filters = new Array();
	//case #20127475 starts
	//case #20148216 starts(filter is changed to type)
	if(itemValue!=''&&itemValue!='null'&&itemValue!=null)//case #20127475 ends
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemValue));		    
	filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	//case #20148216 end
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(skuDimsSearchResults != null && skuDimsSearchResults.length > 0){
		for (var i = 0; i < skuDimsSearchResults.length; i++){
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			baseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	if ((!isNaN(cube))){
		var uomqty = parseFloat(quantity)/parseFloat(baseUOMQty);
		itemUomCube = parseFloat(uomqty) * parseFloat(cube);
		var itemCube = itemUomCube.toString();

	} else {
		itemUomCube = 0;
		itemCube=0;
	}

	if(isNaN(itemCube)|| itemCube==null || itemCube=='')
		itemCube = 0;
	var LocRemCube=0;
	if(binLocation!=null && binLocation!='')
	 LocRemCube = GetLocCube(binLocation);
	//alert('LocRemCube '+LocRemCube);
	var locRem = LocRemCube.toString();

	//alert('Loc remaincube '+locRem);

	if(eventtype=='edit')
	{
		if(parseFloat(QtyOldValue)>parseFloat(enteredQuantity)){
			remCube = parseFloat(locRem) + parseFloat(itemCube);
			//alert('remCube1 '+remCube);

		}
		else if(parseFloat(QtyOldValue)< parseFloat(enteredQuantity))
		{

			remCube = parseFloat(locRem) - parseFloat(itemCube);
			//alert('remCube2 '+remCube);
		}
	}
	else
	{
		//alert('parseFloat(locRem) '+parseFloat(locRem));
		//alert('parseFloat(itemCube) '+parseFloat(itemCube));
		remCube = parseFloat(locRem) - parseFloat(itemCube);
		//alert('remCube3 '+remCube);
	}


	var remainingCube = remCube.toString();
	//alert('remainingCube '+remainingCube);

	if (binLocation != null && binLocation != "" && remainingCube >= 0){
		//return true;
	}
	else{
		return false;
	}
}

/**
 * @param LocID
 * @returns {Number}
 */
function GetLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GetLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GetLocCube:Remaining Cube for Location', remainingCube);
	//case# 20127085 starts (if condition added for checking remainingCube is empty or not)
	if(remainingCube==null&&remainingCube=='')
		remainingCube=0;
	else
		return remainingCube;
	//case# 20127085 end
}


/**
 * this function is to validate the batch field if the item is of batch type
 * and check whether the batch# is belongs to same batch item or not.
 * created by suman.
 * @param itemid
 * @returns {Boolean}
 */
function ValidateLot(itemid)
{
// case 20126279

	var poItemFields = ['custitem_ebiz_item_shelf_life','custitem_ebiz_item_cap_expiry_date','recordType','custitem_ebizbatchlot','custitem_ebizserialin'];
	var ItemRec = nlapiLookupField('item', itemid, poItemFields);
	var ItemType = '';
	var batchFlag = 'F';
	var serIn = 'F';
	var Quantity =nlapiGetFieldValue('custrecord_ebiz_inv_qty');	
	if(ItemRec != null && ItemRec != '')
	{
		ItemType = ItemRec.recordType;
		batchFlag = ItemRec.custitem_ebizbatchlot;
		serIn = ItemRec.custitem_ebizserialin;
	}
	try{
		if(ItemType=='lotnumberedinventoryitem'||ItemType=='lotnumberedassemblyitem' ||  batchFlag == 'T')
		{
			var itemShelflife=ItemRec.custitem_ebiz_item_shelf_life;
			var CaptureExpiryDate=ItemRec.custitem_ebiz_item_cap_expiry_date;
			var LOT=nlapiGetFieldValue('custrecord_ebiz_inv_lot');
			if(LOT==null||LOT=="")
			{
				alert('LOT# cant be left empty for LOT Item');
				return false;
			}
			else
			{
				var filterBatchEntry=new Array();
				filterBatchEntry.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid));

				var columnBatchEntry=new Array();
				columnBatchEntry[0]=new nlobjSearchColumn('custrecord_ebizlotbatch');
				columnBatchEntry[1]=new nlobjSearchColumn('custrecord_ebizfifodate');
				columnBatchEntry[2]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				columnBatchEntry[3]=new nlobjSearchColumn('internalid');

				var searchrecordBatchEntry=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatchEntry,columnBatchEntry);

				if(searchrecordBatchEntry!=null&&searchrecordBatchEntry!="")
				{
					var checkflag=0;
					var BatchTxt=nlapiGetFieldText('custrecord_ebiz_inv_lot');
					var BatchId=nlapiGetFieldValue('custrecord_ebiz_inv_lot');
					var BatchEntryExpDate='';
					var BatchEntryFifoDate='';
					for ( var count = 0; count < searchrecordBatchEntry.length; count++) {
						nlapiLogExecution('ERROR','ONcomparing',BatchTxt+'='+searchrecordBatchEntry[count].getValue('custrecord_ebizlotbatch'));
						if((BatchTxt==searchrecordBatchEntry[count].getValue('custrecord_ebizlotbatch'))&&(BatchId==searchrecordBatchEntry[count].getValue('internalid')))
						{
							checkflag=1;
							BatchEntryExpDate=searchrecordBatchEntry[count].getValue('custrecord_ebizexpirydate');
							BatchEntryFifoDate=searchrecordBatchEntry[count].getValue('custrecord_ebizfifodate');
						}
					}
					if(checkflag!=1)
					{
						alert('LOT# doesnt matches with the Item Specified');
						return false;
					}
					else
					{
						//alert(BatchEntryExpDate);
						//alert(BatchEntryFifoDate);
						if(BatchEntryExpDate != null && BatchEntryExpDate != '' )
							nlapiSetFieldValue('custrecord_ebiz_expdate',BatchEntryExpDate);
						else
						{
							var d = new Date();
							var vExpiryDate='';

							if(itemShelflife !=null && itemShelflife!='')
							{
								d.setDate(d.getDate()+parseInt(itemShelflife));
								vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
							}
							else
							{
								vExpiryDate='01/01/2099';										     
							}
							nlapiSetFieldValue('custrecord_ebiz_expdate',vExpiryDate);

						}

						nlapiSetFieldValue('custrecord_ebiz_inv_fifo',BatchEntryFifoDate);
						return true;
					}
				}
				else
				{
					alert('LOT# doesnt matches with the Item Specified');
					return false;
				}
			}
		}
		else
			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem"  ||  serIn == 'T') {
				
				var qtynew =  nlapiGetFieldValue('custrecord_ebiz_inv_qty');	
				
				if(parseInt(qtynew) != qtynew)
				{
					alert("For Serial item ,Decimal quantities not allowed");

					return false;

				}
				

				var itemvalue = nlapiGetFieldValue('custrecord_ebiz_inv_sku');
				var newlp = nlapiGetFieldValue('custrecord_ebiz_inv_lp');
				var filters= new Array();			
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', itemvalue);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', newlp);
				var column =new Array(); 
				column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

				var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
				if(searchresultser == null || searchresultser =='')
				{
					var frmname="CreateInvt";
					alert("Please enter value(s)for: Serial Items in Serial Entry");
					//var linkURL = "https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=159&deploy=1";
					var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
//					var ctx = nlapiGetContext();
//					if (ctx.getEnvironment() == 'PRODUCTION') {
//					linkURL = 'https://system.netsuite.com' + linkURL;			
//					}
//					else 
//					if (ctx.getEnvironment() == 'SANDBOX') {
//					linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
//					}
					//linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetFieldValue('custrecord_ebiz_inv_loc');
					linkURL = linkURL + '&custparam_serialskuid=' + nlapiGetFieldValue('custrecord_ebiz_inv_sku');
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetFieldValue('custrecord_ebiz_inv_lp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetFieldValue('custrecord_ebiz_inv_qty');					
					linkURL = linkURL + '&custparam_serialformname=' + frmname;
					linkURL = linkURL + '&custparam_seriallocation=' + nlapiGetFieldValue('custrecord_ebiz_inv_loc');
					linkURL = linkURL + '&custparam_serialbinlocation=' + nlapiGetFieldValue('custrecord_ebiz_inv_binloc');

					window.open(linkURL);
				} //case 20127474  start :checking serial numbers with quantity
				else if(searchresultser != null && searchresultser !='' && searchresultser.length>0)
				{
					var ActualQty=searchresultser.length;
	if(parseInt(Quantity) != Quantity)
					{
						alert("For Serial item ,Decimal quantities not allowed");

						return false;

					}
					if(parseFloat(Quantity)<parseFloat(ActualQty))
					{
						alert("Please select required Serial numbers to adjust");
						var ReqQty = Quantity;
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');

						linkURL = linkURL + '&custparam_serialskuid=' + nlapiGetFieldValue('custrecord_ebiz_inv_sku');
						linkURL = linkURL + '&custparam_serialsku=' + nlapiGetFieldText('custrecord_ebiz_inv_sku');
						linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetFieldValue('custrecord_ebiz_inv_lp');
						linkURL = linkURL + '&custparam_serialskuqty=' + ReqQty;	
						//linkURL = linkURL + '&custparam_lot=' + BatchNo;
						//linkURL = linkURL + '&custparam_name=' + BatchNo;
						//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_poitemstatus',p);
						//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invadj_items','custpage_popackcode',p);
						window.open(linkURL);
					}
					else if(parseFloat(Quantity)>parseFloat(ActualQty))
					{
						
						var ReqQty =parseFloat(Quantity)-parseFloat(ActualQty);
						
						if(parseInt(ReqQty) != ReqQty)
						{
							alert("For Serial item ,Decimal quantities not allowed");

							return false;

						}
						var frmname="CreateInvt";
						alert("Please enter value(s)for: Serial Items in Serial Entry");
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
						
						linkURL = linkURL + '&custparam_serialskuid=' + nlapiGetFieldValue('custrecord_ebiz_inv_sku');
						linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetFieldValue('custrecord_ebiz_inv_lp');
						linkURL = linkURL + '&custparam_serialskuchknqty=' + ReqQty;					
						linkURL = linkURL + '&custparam_serialformname=' + frmname;
						linkURL = linkURL + '&custparam_seriallocation=' + nlapiGetFieldValue('custrecord_ebiz_inv_loc');
						linkURL = linkURL + '&custparam_serialbinlocation=' + nlapiGetFieldValue('custrecord_ebiz_inv_binloc');

						window.open(linkURL);

					}
				else
					return true;	
				}

			}
			else 
				return true;
	}
	catch(exe)
	{
		nlapiLogExecution('ERROR','error in the ValidateLot fn',exe); 
	}
}



function IsLPExists(type, name){
	//if lp exists in open task,then dont allow	
	if (name == 'custrecord_ebiz_inv_lp') {
		var lpNo = nlapiGetFieldValue('custrecord_ebiz_inv_lp');
		var company=nlapiGetFieldValue('custrecord_ebiz_inv_company');
		var site=nlapiGetFieldValue('custrecord_ebiz_inv_loc');

		var lpMasterFilters = new Array();				
		lpMasterFilters[0] = new nlobjSearchFilter('name', null, 'is', lpNo);
		lpMasterFilters[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof',['@NONE@',site]);
		lpMasterFilters[2] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_company', null, 'anyof', ['@NONE@',company]);
		var lpMasterColumns = new Array();
		lpMasterColumns[0] = new nlobjSearchColumn('name');
		var lpMasterSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpMasterFilters,lpMasterColumns);



		/* var getLP = nlapiGetFieldValue('custrecord_ebiz_inv_lp');
        var filterslp = new Array();
        filterslp[0] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [2, 10]);
        filterslp[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [2, 6, 19]);
        filterslp[2] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', getLP);
        var columnlp=new Array();
        columnlp[0]= new nlobjSearchColumn('custrecord_ebiz_inv_lp');
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filterslp,columnlp);*/
		//alert('Length: ' + searchresults.length)
		if (lpMasterSearchResults != null && lpMasterSearchResults.length > 0) {
			alert("LP already in use!");
			return false;
		}
	}
	//  Always return true at this level, to continue validating other fields	
	return true;
}

function SetPackcodeforItem(type, name)
{
	if (name == 'custrecord_ebiz_inv_sku') {
		var item = nlapiGetFieldValue('custrecord_ebiz_inv_sku');
		//alert(item);
		var SkudimensionsFilters = new Array();		
		if(item!=''&&item!='null'&&item!=null)// Case#20127729
		SkudimensionsFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', item));
		SkudimensionsFilters.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', '1'));

		var SkudimensionsColumns = new Array();
		SkudimensionsColumns[0] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim');

		var SkuSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, SkudimensionsFilters,SkudimensionsColumns);

		if(SkuSearchResults != null && SkuSearchResults != '')
		{
			//alert(SkuSearchResults[0].getValue('custrecord_ebizpackcodeskudim'));
			nlapiSetFieldValue('custrecord_ebiz_inv_packcode',SkuSearchResults[0].getValue('custrecord_ebizpackcodeskudim'));
		}
	}
}
