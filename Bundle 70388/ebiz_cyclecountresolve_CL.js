/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/ebiz_cyclecountresolve_CL.js,v $
 *     	   $Revision: 1.14.2.4.4.1.4.6.2.1 $
 *     	   $Date: 2015/09/21 14:02:30 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_8 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_cyclecountresolve_CL.js,v $
 * Revision 1.14.2.4.4.1.4.6.2.1  2015/09/21 14:02:30  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.14.2.4.4.1.4.6  2014/08/25 07:45:18  sponnaganti
 * Case# 201410132
 * Stnd Bundle Issue fixed
 *
 * Revision 1.14.2.4.4.1.4.5  2014/08/19 12:53:52  skavuri
 * Case# 20149982 Std Bundle issue fixed
 *
 * Revision 1.14.2.4.4.1.4.4  2014/03/18 15:21:00  skavuri
 * Case# 20127752 issue fixed
 *
 * Revision 1.14.2.4.4.1.4.3  2013/06/04 07:47:07  rmukkera
 * Issue Fix For
 * Cycle Count Resolve Screen does not show records when Show All is selected.
 *
 * Revision 1.14.2.4.4.1.4.2  2013/05/01 01:30:16  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.14.2.4.4.1.4.1  2013/04/19 15:40:56  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes
 *
 * Revision 1.14.2.4.4.1  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.14.2.4  2012/06/29 14:00:22  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.14.2.3  2012/04/30 11:33:37  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.14.2.2  2012/04/27 15:11:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Added onChange function.
 *
 * Revision 1.14.2.1  2012/02/07 12:34:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cyclecounting issue fixes
 *
 * Revision 1.15  2012/02/06 22:22:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * cycle count issue fixes
 *
 * Revision 1.14  2011/11/23 14:48:42  spendyala
 * CASE201112/CR201113/LOG201121
 * changed the wms status flag
 *
 * Revision 1.13  2011/11/15 20:00:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * include inactive condition in stock adjustment filters.
 *
 * Revision 1.12  2011/11/11 17:21:25  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle count for Batch SKUs
 *
 * Revision 1.11  2011/11/08 14:37:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count - Lot/Batch Issue Fixes
 *
 * Revision 1.10  2011/11/05 10:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count Issue Fixes
 *
 * Revision 1.9  2011/11/04 14:28:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cycle Count Issue Fixes
 *
 * Revision 1.8  2011/11/03 17:03:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Inventory status should be Storage once the CC plan is resolved or ignored
 *
 * Revision 1.7  2011/11/02 12:05:44  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cycle Count file
 *
 * Revision 1.5  2011/10/25 13:12:27  gkalla
 * CASE201112/CR201113/LOG201121
 * To update NS while doing Cycle count Resolve
 *
 * Revision 1.14  2011/09/30 11:22:53  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Lot# has been added as extra parameter for inventory posting function.
 * this is enhanced for TNT
 *
 * Revision 1.13  2011/09/21 18:58:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Fixes
 *
 * Revision 1.12  2011/09/16 15:08:04  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/08/20 07:22:41  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/07/29 11:13:39  rmukkera
 * CASE201112/CR201113/LOG201121
 * Modified the update loccube functionality
 *
 * Revision 1.9  2011/07/21 07:04:32  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function ebiznet_CycleCoundResolveSave_CS(){/*
	
//	alert(nlapiGetLineItemValue('custpage_items', 'custpage_id', 1));
	nlapiLogExecution('ERROR', 'entered1', 'entered1');

	for (var i = 0; i < nlapiGetLineItemCount('custpage_items'); i++) {
		//alert(nlapiGetLineItemCount('custpage_items'));
		if (nlapiGetLineItemValue('custpage_items', 'custpage_rec', i + 1) == 'T') {

			var transaction = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', nlapiGetLineItemValue('custpage_items', 'custpage_id', i + 1));

			//transaction.setFieldValue('custrecord_act_beg_date', nlapiGetLineItemValue('custpage_items', 'custpage_date', i + 1));
			//transaction.setFieldValue('custrecord_cycact_beg_time', nlapiGetLineItemValue('custpage_items', 'custpage_time', i + 1));
			//transaction.setFieldValue('custrecord_cycleact_end_date', DateStamp());
			//transaction.setFieldValue('custrecord_cycact_end_time', TimeStamp());
			transaction.setFieldValue('custrecord_cyclerec_upd_date', DateStamp());
			transaction.setFieldValue('custrecord_cyclerec_upd_time', TimeStamp());
			transaction.setFieldValue('custrecord_cycleupd_user_no', nlapiGetContext().getUser());

			if (nlapiGetFieldValue('restype') == 'recount') {

				transaction.setFieldValue('custrecord_cycleact_sku', '');
				transaction.setFieldValue('custrecord_cyclecount_act_ebiz_sku_no', '');
				transaction.setFieldValue('custrecord_cycle_act_qty', '');
				transaction.setFieldValue('custrecord_cycact_lpno', '');
			}
			if (nlapiGetFieldValue('restype') == 'recount') {
				transaction.setFieldValue('custrecord_cyclestatus_flag', 20);
			}
			else 
				if (nlapiGetFieldValue('restype') == 'ignore') {
					transaction.setFieldValue('custrecord_cyclestatus_flag', 21);
				}
				else 
					if (nlapiGetFieldValue('restype') == 'resolve') {
						transaction.setFieldValue('custrecord_cyclestatus_flag', 22);
					}
					else {
						transaction.setFieldValue('custrecord_cyclestatus_flag', 22);
					}

			// nlapiLogExecution('DEBUG', 'STATUS after',
			// searchresult.getValue('custrecord_status_flag'));

			nlapiSubmitRecord(transaction, true);

		}
	}
//	alert('value:'+nlapiGetFieldValue('restype'));
	if (nlapiGetFieldValue('restype') == 'ignore' || nlapiGetFieldValue('restype') == 'resolve'|| nlapiGetFieldValue('restype') == 'close') {
		for (var i = 0; i < nlapiGetLineItemCount('custpage_items'); i++) {
			if (nlapiGetLineItemValue('custpage_items', 'custpage_rec', i + 1) == 'T') {
				var accountno="";
				var sitelocn="";
				var resultQty=0;
				var memo="";
				if (nlapiGetFieldValue('restype') == 'resolve'|| nlapiGetFieldValue('restype') == 'close') {
					try {
						var vWHLoc;
						var vTaskType='7';
						var vAdjusttype;
						vWHLoc=nlapiGetLineItemValue('custpage_items', 'custpage_site', i + 1);
//						alert("vWHLoc "+ vWHLoc);
						//alert("vWHLoc " + vWHLoc);
						var AccountNo = getStockAdjustmentAccountNoNew(vWHLoc,vTaskType,vAdjusttype);
//						alert("AccountNo "+ AccountNo.length);
						if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
						{
							var LotBatchId='';
							var LotBatchText=nlapiGetLineItemValue('custpage_items', 'custpage_actbatch', i + 1);

							if(LotBatchText!=null && LotBatchText!='')
								LotBatchId=getLotBatchId(LotBatchText);
							//alert(LotBatchText);
							//alert("Into Create Inv");	
							//alert('Location:' + nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1) + '\n' + 'Qty:' + nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1) + '\n' + 'LP:' + nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1) + '\n' + 'Item:' + nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1))
//							alert(nlapiGetLineItemValue('custpage_items', 'custpage_invtid', i + 1));
							var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', nlapiGetLineItemValue('custpage_items', 'custpage_invtid', i + 1));
							transaction.setFieldValue('custrecord_ebiz_inv_qty', nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
							transaction.setFieldValue('custrecord_ebiz_inv_packcode', '1');
							var totalQty = nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1);

							accountno = transaction.getFieldValue('custrecord_ebiz_inv_account_no');
							sitelocn = transaction.getFieldValue('custrecord_ebiz_inv_loc');
							memo = transaction.getFieldValue('custrecord_ebiz_inv_note1');
							var allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
							var qohqty = parseFloat(totalQty) - parseFloat(allocQty);

							//alert("allocQty :" + allocQty);
							//alert("qohqty :" + qohqty);
							transaction.setFieldValue('custrecord_ebiz_qoh', qohqty);
							transaction.setFieldValue('custrecord_ebiz_avl_qty', qohqty);
							if(nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1) != null && nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1) != "")

								transaction.setFieldValue('custrecord_ebiz_inv_lp', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
							if(nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1) != null && nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1) != "")
								transaction.setFieldValue('custrecord_ebiz_inv_sku', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
//							if(nlapiGetLineItemValue('custpage_items', 'custpage_actskustatusvalue', i + 1) != null && nlapiGetLineItemValue('custpage_items', 'custpage_actskustatusvalue', i + 1) != "")
//							{
//							transaction.setFieldValue('custrecord_ebiz_inv_sku_status', nlapiGetLineItemValue('custpage_items', 'custpage_actskustatusvalue', i + 1));
//							}
//							else
//							{
//							alert("Please Configure Item Status");
//							return false;
//							}


							// alert("itemstatus :" +  nlapiGetLineItemValue('custpage_items', 'custpage_actskustatusvalue', i + 1));
							transaction.setFieldValue('custrecord_ebiz_invholdflg', 'F');
							transaction.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);
							transaction.setFieldValue('custrecord_ebiz_inv_updateddate', DateStamp());
							transaction.setFieldValue('custrecord_ebiz_inv_updatedtime', TimeStamp());
							transaction.setFieldValue('custrecord_wms_inv_status_flag', '19');//Storage

							//shylaja
							//transaction.setFieldValue('custrecord_ebiz_callinv', 'Y');
							transaction.setFieldValue('custrecord_ebiz_callinv', 'N');// To call NS without User event
							//alert("ns flag :"+transaction.getFieldValue('custrecord_ebiz_callinv'));

							var id = nlapiSubmitRecord(transaction, true);

							try
							{
								if(qohqty == 0)
								{					
									var delid = nlapiDeleteRecord('customrecord_ebiznet_createinv', nlapiGetLineItemValue('custpage_items', 'custpage_invtid', i + 1));				
								}
							}
							catch (exp) 
							{
								nlapiLogExecution('ERROR', 'Exception in Delete Inventory Record ', exp);	
							}

							//To pass the values to NS


							var expqty1 = nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1);
							var actqty1 = nlapiGetLineItemValue('custpage_items', 'custpage_expqty', i + 1);
							var resultQty1 = parseFloat(expqty1) - parseFloat(actqty1);

							var ItemId=nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1);
							var itmstatus=null;
							var ItemNewStatus=null;


							var FromAccNo=AccountNo[0];
							var ToAccNo=null;

							var notes="";
							//alert("resultQty1 "+ resultQty1);
							//alert("ItemId "+ ItemId);
							//alert("FromAccNo "+ FromAccNo);

							//alert(nlapiGetFieldValue('custpage_confirmtohost'));

							if(nlapiGetFieldValue('custpage_confirmtohost')=='T')
							{
								//InvokeNSInventoryAdjustment(ItemId,itmstatus,vWHLoc,-(parseFloat(resultQty1)),notes,vTaskType,vAdjusttype,LotBatchText);
								InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(resultQty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);

								//InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vOldWHlocatin,-(parseFloat(ItemNewQty)),"",vTaskType,InvAdjType,LotBatchText,FromAccNo);
								//alert("AccountNo.length "+ AccountNo.length);
								if(AccountNo.length>2)
								{
									ToAccNo=AccountNo[2];
									InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,resultQty1,notes,vTaskType,vAdjusttype,LotBatchText,ToAccNo);
								}
							}

						}
						else
						{
							//To throw error message to configure stock adjustment type
							alert("Stock adjustment type is not configured for Task type Cycle Count");
							return false;
						}




						//alert("In Invt Adjust start");
						var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');                   
						//nlapiLogExecution('ERROR', 'New Item Status', ItemStatusNewText);
						//invAdjustCustRecord.setFieldValue('name', sku + iter);
						//alert(nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
						//	alert(nlapiGetLineItemValue('custpage_items', 'custpage_expsku', i + 1));
						//alert("invAdjustCustRecord " + invAdjustCustRecord);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_binloc', nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
						invAdjustCustRecord.setFieldValue('custrecord_ebizskuno', nlapiGetLineItemValue('custpage_items', 'custpage_expsku', i + 1));
						//invAdjustCustRecord.setFieldValue('custrecord_skustatus', skuStatus);
						//if(skuStatus == ItemStatusOldVal)
						//	invAdjustCustRecord.setFieldValue('custrecord_ebiz_oldval', ItemStatusNewText);
						//else
						//	invAdjustCustRecord.setFieldValue('custrecord_ebiz_oldval', ItemStatusOldText);
						//invAdjustCustRecord.setFieldValue('custrecord_ebiz_newval', ItemStatusNewText);
						//invAdjustCustRecord.setFieldValue('custrecord_ebiz_batchno', lot);

						//	alert("Cycle LP" + nlapiGetLineItemValue('custpage_items', 'custpage_explp', i + 1));
						//	alert("Expected Qty" + nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
						//alert("Actual Qty" + nlapiGetLineItemValue('custpage_items', 'custpage_expqty', i + 1));
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_expeqty', nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_invjlpno', nlapiGetLineItemValue('custpage_items', 'custpage_explp', i + 1));

						if(AccountNo!=null && AccountNo!='')
						{

							invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjtype', AccountNo[1]);
						}
						//invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1', notes);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_siteid', sitelocn);
						var expqty = nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1);
						var actqty = nlapiGetLineItemValue('custpage_items', 'custpage_expqty', i + 1);
						resultQty = parseFloat(expqty) - parseFloat(actqty);


						//alert("retValue :" +retValue);
						//alert("Adjust Qty" + resultQty);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustqty', resultQty);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_tasktype', 7);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_recorddate', DateStamp());
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_rectime', TimeStamp());
						//invAdjustCustRecord.setFieldValue('custrecord_ebiz_packcode', packCode);
						//alert("Adjust Qty" + nlapiGetContext().getUser());
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_updateduserno', nlapiGetContext().getUser());

						//	alert("nlapiGetContext().getUser() " + nlapiGetContext().getUser());

						var id = nlapiSubmitRecord(invAdjustCustRecord, false, true);
						//alert(id);
						var arrdims = new Array();

						arrdims = getSKUCubeAndWeight(nlapiGetLineItemValue('custpage_items', 'custpage_expsku', i + 1), 1);
						//alert("SKUDims ");
						var itemCube = 0;
						if (arrdims["BaseUOMItemCube"] != "" && (!isNaN(arrdims["BaseUOMItemCube"]))) 
						{
							var uomqty = ((parseFloat(resultQty))/(parseFloat(arrdims["BaseUOMQty"])));			
							itemCube = (parseFloat(uomqty) * parseFloat(arrdims["BaseUOMItemCube"]));						

						} else 
						{							
							itemCube = 0;
						}
						//alert("itemCube :" +itemCube);
						//alert("Loc ID : " + nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));	 
						//alert("LOCCube ");
						LocRemCube = GeteLocCube(nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
						//alert("LocRemCube :" + LocRemCube);
						var oldLocRemCube =0;	                   
						var oldremcube = (parseFloat(LocRemCube) + parseFloat(itemCube));			
						oldLocRemCube = parseFloat(oldremcube);		           			 
						//alert("UpdateLocCube ");
						//alert("oldLocRemCube :" + oldLocRemCube);
						//alert("Id " + nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
						//alert("LocRemCube " + oldLocRemCube);
						var retValue = UpdateLocCubeNew(nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1), oldLocRemCube);
						//alert("updated ");

						if (nlapiGetFieldValue('restype') == 'close') {

							var cyccplanrecord = nlapiLoadRecord('customrecord_ebiznet_cylc_createplan', nlapiGetLineItemValue('custpage_items', 'custpage_planno', i + 1));
							cyccplanrecord.setFieldValue('custrecord_cyccplan_close', 'T');							
							var id = nlapiSubmitRecord(cyccplanrecord, false, true);							
						}

					} 
					catch (e) {

						if (e instanceof nlobjError) 
						{
							// nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
							alert('system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
						}

						else 
						{
							//nlapiLogExecution('ERROR', 'unexpected error', e.toString());
							alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
						alert('Save unsuccessful into Create Inventory' + '\n' + 'Error: ' + e.toString());
						return false;
					}
				}
				
				

				try {
					//alert(nlapiGetLineItemValue('custpage_items', 'custpage_name', i + 1) + '\n' + nlapiGetLineItemValue('custpage_items', 'custpage_planno', i + 1) + '\n' + nlapiGetLineItemValue('custpage_items', 'custpage_date', i + 1) + '\n' + nlapiGetLineItemValue('custpage_items', 'custpage_time', i + 1) + '\n' + nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1) + '\n' + nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1) + '\n' + nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1))
					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', nlapiGetLineItemValue('custpage_items', 'custpage_opentaskid', i + 1));
					// transaction.setFieldValue('name', nlapiGetLineItemValue('custpage_items', 'custpage_name', i + 1));
					// transaction.setFieldValue('custtransaction_ebiz_cntrl_no', nlapiGetLineItemValue('custpage_items', 'custpage_planno', i + 1));
					// transaction.setFieldValue('custtransactionact_begin_date', nlapiGetLineItemValue('custpage_items', 'custpage_date', i + 1));
					transaction.setFieldValue('custtransaction_act_end_date', DateStamp());
					//  transaction.setFieldValue('custtransaction_actualbegintime', nlapiGetLineItemValue('custpage_items', 'custpage_time', i + 1));
					transaction.setFieldValue('custtransaction_actualendtime', TimeStamp());
					transaction.setFieldValue('custtransaction_sku', nlapiGetLineItemValue('custpage_items', 'custpage_actsku', i + 1));
					transaction.setFieldValue('custtransaction_act_qty', nlapiGetLineItemValue('custpage_items', 'custpage_actqty', i + 1));
					transaction.setFieldValue('custtransaction_lpno', nlapiGetLineItemValue('custpage_items', 'custpage_actlp', i + 1));
					if (nlapiGetFieldValue('restype') == 'resolve') {
						transaction.setFieldValue('custrecord_wms_status_flag', 22);
					}
					if (nlapiGetFieldValue('restype') == 'ignore') {
						transaction.setFieldValue('custrecord_wms_status_flag', 21);
					}
					transaction.setFieldValue('custtransaction_upd_date', DateStamp());
					//transaction.setFieldValue('custtransaction_actbeginloc', nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
					//transaction.setFieldValue('custtransaction_actendloc', nlapiGetLineItemValue('custpage_items', 'custpage_location', i + 1));
					transaction.setFieldValue('custtransaction_transactionupdatetime', TimeStamp());

					var id = nlapiSubmitRecord(transaction, true);
				} 
				catch (e) {
					alert('Save unsuccessful into OPEN TASK' + '\n' + 'Error: ' + e.toString());
				}

				
				 * Creating a record in Inventory Adjustment
				 


				
				 * Inventory updation to NetSuite
				 
				 commented by shylaja it is already there in UE
	                try
	                {
	                	//alert(accountno);
	                	//alert(sitelocn);
	                	var outAdj = nlapiCreateRecord('inventoryadjustment');
	        			outAdj.setFieldValue('account', accountno);
	        			outAdj.setFieldValue('memo', memo);

	        			outAdj.insertLineItem('inventory', 1);
	        			outAdj.setLineItemValue('inventory', 'item', 1, nlapiGetLineItemValue('custpage_items', 'custpage_expsku', i + 1));
	        			outAdj.setLineItemValue('inventory', 'location', 1, sitelocn);
	        			outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(resultQty));
	        			nlapiSubmitRecord(outAdj, false, true);
	        			//alert("NS Updation End");
	        			nlapiLogExecution('ERROR', 'type argument', 'type is create');
	                } 
	                catch (e) {
	                	//alert('Create unsuccessful into Inventory updation To NS' + '\n' + 'Error: ' + e.toString());
	                }
				 
			}
		}
	}

	return true;
	//alert('Saved Successfully...!');
*/}

function getSKUCubeAndWeight(skuNo, uom){
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight', 'Start');
	var timestamp1 = new Date();
	var cube = 0;
	var BaseUOMQty = 0;    
	var dimArray = new Array();
	if (uom == "")
		uom = 1;

	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:SKU info', skuNo);
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:UOM', uom);

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', skuNo);
	//filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', uom);
	filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var t1 = new Date();
	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:getSKUCubeAndWeight:skudims',
			getElapsedTimeDuration(t1, t2));

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	//dimArray[0] = cube;
	//dimArray[1] = BaseUOMQty;

	dimArray["BaseUOMItemCube"] = cube;
	dimArray["BaseUOMQty"] = BaseUOMQty;

	var timestamp2 = new Date();
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight Duration', getElapsedTimeDuration(timestamp1, timestamp2));

	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:Cube', cube);
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:Weight', BaseUOMQty);

	return dimArray;
}

function GeteLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}

function getLotBatchId(lotbatchtext)
{
	var batchid='';
	var filters = new Array();          
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',lotbatchtext));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	var batchdetails = new nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
	{
		batchid=batchdetails[0].getValue('internalid');
	}
	return batchid;
}


/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);
		//alert("AccNo :: "+ AccNo);

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));

		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		//alert("itemdetails " + itemdetails);
		if (itemdetails !=null) 
		{
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		
		//alert("NS Qty "+ parseFloat(qty));
		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', loc);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, loc);	
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		//alert('Hi1');
		//alert("vAvgCost "+ vAvgCost);
		if(vAvgCost != null &&  vAvgCost != "")	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
		}
		//alert('Hi2');
		//outAdj.setFieldValue('subsidiary', 4);

		//Added by Sudheer on 093011 to send lot# for inventory posting

		if(lot!=null && lot!='')
		{
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);

			nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
		}
		//alert('Hi3');
		nlapiSubmitRecord(outAdj, true, true);
		//alert("Success NS");
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(e) {
		if (e instanceof nlobjError) 
		{
			// nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
			alert('system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
		}

		else 
		{
			//nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
		}
		
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', e);
		//alert("Error" + exp.toString());

	}
}

function getStockAdjustmentAccountNoNew(loc,tasktype,adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();
	//alert("loc : "+loc);
	if(loc!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[loc]));	

	if(tasktype!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));

//	if(tasktype==11) //11 is the internalid value of tasktype "ADJT";
//	{	
//	if(adjusttype!=null || adjusttype!="")
//	filterStAccNo.push(new nlobjSearchFilter('custrecord_adjustment_type',null,'anyof',adjusttype));
//	filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));
//	}	

	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));
	
	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');
	colsStAccNo[2] = new nlobjSearchColumn('internalid');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	//alert("StockAdjustAccResults " + StockAdjustAccResults);
	//alert("StockAdjustAccResults " + StockAdjustAccResults.length);
	if(StockAdjustAccResults !=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		//alert("1 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('internalid'));

		//alert("2 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
		if(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != null && StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != "")
			StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
		
		
	}	

	return StAdjustmentAccountNo;	
}

/**
 * Updates the bin location volume for the specified location
 * @param locnId
 * @param remainingCube
 */
function UpdateLocCubeNew(locnId, remainingCube){
	//alert("Val1 "+ locnId);
	//alert("Val2 "+ remainingCube);
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube).toFixed(5);
	//alert("parse "+ parseFloat(remainingCube));
	//nlapiLogExecution('ERROR','Location Internal Id', locnId);

	//nlapiLogExecution('ERROR','Location Internal IdremainingCube', remainingCube);

	//alert("into");
	//var t1 = new Date();
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	//alert("Success");
	//var t2 = new Date();
	//nlapiLogExecution('ERROR', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}

function Onchange(type,name ,linenum)
{
	if(name=='custpage_actqty')
	{
		var vactQty =  nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
		var recid = nlapiGetCurrentLineItemValue('custpage_items','custpage_invtid');
		
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',recid);
		
		var allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		
		if(parseFloat(allocQty)>parseFloat(vactQty))
		{
			alert("AllocQty is greater than ActualQty");
			return false;    		
		}  
	}
	if(trim(name)==trim('custpage_selectpage'))
	{
		nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}	

}


function OnSave()
{
	//alert('HI');

	try 
	{
		var lineCount=nlapiGetLineItemCount('custpage_items');
			var vPlanNo = nlapiGetFieldValue('custpage_cyclecountplan');
			var vopt =  nlapiGetFieldValue('restype');

			if(vopt == 'close'){

				var totalLineCount = 0;

				var vSearchRes = 0;

				var filters = new Array();

				filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',vPlanNo));
				filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_invtid');     
				var List = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
				if( List!=null && List!='')
				{ 
					vSearchRes = List.length;
				}
				
				if(lineCount!=null && lineCount!='' && lineCount>0)
				{

					for (var i = 0; i < lineCount; i++) 
					{

						if(nlapiGetLineItemValue('custpage_items', 'custpage_rec', i+1) == 'T' )
						{
							totalLineCount++;
						}
					}
				}
				
				if(parseInt(totalLineCount)<parseInt(vSearchRes)){
					alert("Please Resolve all the Records to Close the Plan");
					return false;
				}

			}
		if(lineCount!=null && lineCount!='' && lineCount>0)
		{
		//Case# 20149982 starts	
			for (var i = 0; i < lineCount; i++) 
			{
				var chkValue=checkAtleast('custpage_items', 'custpage_rec', i+1);
				//alert("chkValue" + chkValue);
				if(chkValue == true )
				{
					var Binlocation = nlapiGetLineItemValue('custpage_items', 'custpage_locationtext',i+1);
					//alert("Binlocation:" + Binlocation);
					var filter=new Array();
					filter.push(new nlobjSearchFilter('name',null,'is',Binlocation));
					filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					var Searchrecord=nlapiSearchRecord('customrecord_ebiznet_location',null,filter,null);
					if(Searchrecord == null || Searchrecord == '')
					{
						alert("Enter Valid Binlocation");
						return false;
					}
				}
			if(chkValue == false) 
				return false;
			else
				return true;
			}
		}
		//case# 201410132//else block uncommented
		else
		{
			return true;
		}
//Case# 20149982 ends
	}
	catch(exp)
	{
		alert('Expception'+exp);
		return false;
	}


}


function showall()
{
	
	nlapiSetFieldValue('custpage_hiddenfieldshow','ALL');
	nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
	NLDoMainFormButtonAction("submitter",true);	
	
}
function showdiscrepancy()
{
	nlapiSetFieldValue('custpage_hiddenfieldshow','D');
	nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
	NLDoMainFormButtonAction("submitter",true);	
	
}
function shownondiscrepancy()
{
	nlapiSetFieldValue('custpage_hiddenfieldshow','ND');
	nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
	NLDoMainFormButtonAction("submitter",true);	
	
}

//Case# 20127752 starts
function Printreportpdf(){
	
	var varCycleCountPlanNo = nlapiGetFieldValue('custpage_cyclecountplan');
	nlapiLogExecution('ERROR', 'varCycleCountPlanNo',varCycleCountPlanNo);
	var CycWhLocation = nlapiGetFieldValue('custpage_cyclecountwhlocation');
	var Cycitem=nlapiGetFieldValue('custpage_cyclecountitem');
	
	var CycbinLoc=nlapiGetFieldValue('custpage_cyclecountbinloc');
	
	var queryparams=nlapiGetFieldValue('custpage_qeryparams');
	
	var hiddenfieldselectpage=nlapiGetFieldValue('custpage_hiddenfieldselectpage');
	
	var hiddenfieldshow=nlapiGetFieldValue('custpage_hiddenfieldshow');
	var radiobtnflag=nlapiGetFieldValue('restypeflag');
	
	
	

	var CycPDFURL = nlapiResolveURL('SUITELET','customscript_ebiz_cyclecountreportpdf','customdeploy_ebiz_cyclecountreportpdf_di');
	//alert(CycPDFURL);
	nlapiLogExecution('ERROR', 'CycPDFURL',CycPDFURL);					
	CycPDFURL = CycPDFURL + '&custparam_cyclecountplan='+ varCycleCountPlanNo+'&custparam_cyclecountwhlocation='+CycWhLocation+'&custparam_cyclecountitem='+Cycitem+'&custparam_cyclecountbinloc='+CycbinLoc+'&custparam_qeryparams='+queryparams+'&custparam_hiddenfieldselectpage='+hiddenfieldselectpage+'&custparam_hiddenfieldshow='+hiddenfieldshow+'&custparam_restypeflag='+radiobtnflag;
	nlapiLogExecution('ERROR', 'CycPDFURL',CycPDFURL);
	//alert(CycPDFURL);
	window.open(CycPDFURL);

}

//Case# 20127752 ends
