function CreatePackApacklisthtml(vebizOrdNo,fulfillmentnumber,printername,salesorderdetails,trantype,wavenumber)
{
	nlapiLogExecution('ERROR', 'into CreatePackApacklisthtml ', 'into CreatePackApacklisthtml');
	nlapiLogExecution('ERROR', 'createpackcodehtmlvebizOrdNo ', vebizOrdNo);
	nlapiLogExecution('ERROR', 'fulfillmentnumber ', fulfillmentnumber);

	var billtoaddress=salesorderdetails.getFieldValue('billaddress'); 
	var shipaddress=salesorderdetails.getFieldValue('shipaddress');	
	var orderdate=salesorderdetails.getFieldValue('trandate');
	var ordernumber=salesorderdetails.getFieldValue('tranid');	
	var customerpo=salesorderdetails.getFieldValue('otherrefnum');
	var entity=salesorderdetails.getFieldText('entity');
	var locationId =salesorderdetails.getFieldValue('location');
	var shipmethod=salesorderdetails.getFieldText('shipmethod');
	var shipDate=salesorderdetails.getFieldValue('shipdate');
	if((customerpo==null)||(customerpo==''))
	{
		customerpo="";
	}
	if((shipDate==null)||(shipDate==''))
	{
		shipDate="";
	}
	if((orderdate==null)||(orderdate==''))
	{
		orderdate="";
	}

	shipmethod=shipmethod.replace(/\s/g, "");
	if((shipmethod==null)||(shipmethod==''))
	{
		shipmethod="";
	}
	var FOB='';

	nlapiLogExecution('ERROR', 'location ', locationId);
	var shipaddressee="";var shipaddr1="";var shipaddr2="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
	shipaddressee=salesorderdetails.getFieldValue('shipaddressee'); 
	shipaddr1=salesorderdetails.getFieldValue('shipaddr1'); 
	shipaddr2=salesorderdetails.getFieldValue('shipaddr2');
	shipcity=salesorderdetails.getFieldValue('shipcity'); 
	shipcountry=salesorderdetails.getFieldValue('shipcountry'); 
	shipstate=salesorderdetails.getFieldValue('shipstate'); 
	shipzip=salesorderdetails.getFieldValue('shipzip'); 
	if((shipaddressee==null)||(shipaddressee==''))
	{
		shipaddressee="";
	}
	if((shipaddr1==null)||(shipaddr1==''))
	{
		shipaddr1="";
	}
	if((shipaddr2==null)||(shipaddr2==''))
	{
		shipaddr2="";
	}
	if((shipcity==null)||(shipcity==''))
	{
		shipcity="";
	}
	if((shipcountry==null)||(shipcountry==''))
	{
		shipcountry="";
	}
	if((shipstate==null)||(shipstate==''))
	{
		shipstate="";
	}
	if((shipzip==null)||(shipzip==''))
	{
		shipzip="";
	}
	shipaddr1=shipaddr1+", "+shipaddr2;
	shipstateandcountry=shipcity+" "+shipstate+" "+shipzip;
	var locationadress =nlapiLoadRecord('Location',locationId);

	var addr1="";var city="";var state="";var zip=""; var stateandzip=""; var returnadresse="";
	addr1=locationadress.getFieldValue('addr1');
	city=locationadress.getFieldValue('city');
	state=locationadress.getFieldValue('state');
	zip=locationadress.getFieldValue('zip');
	returnadresse=locationadress.getFieldValue('addressee');
	if((addr1==null)||(addr1==''))
	{
		addr1="";
	}
	if((city==null)||(city==''))
	{
		city="";
	}
	if((state==null)||(state==''))
	{
		state="";
	}
	if((zip==null)||(zip==''))
	{
		zip="";
	}
	if((returnadresse==null)||(returnadresse==''))
	{
		returnadresse="";
	}
	stateandzip=city+" "+state+" "+zip;



	var groupopentaskfilterarray = new Array();
	//Please uncheck  the fillementnumber code 
	groupopentaskfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
	groupopentaskfilterarray.push(new nlobjSearchFilter('name', null, 'is',fulfillmentnumber.toString()));
	groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3])); 
	groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[28]));
	if((wavenumber!=null)&&(wavenumber!=''))
	{
		nlapiLogExecution('ERROR', 'openwavenumber ', wavenumber);
		groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',wavenumber)); 
	}
	var groupopentaskcolumnarray = new Array();
	//groupopentaskcolumnarray[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
	groupopentaskcolumnarray[0] = new nlobjSearchColumn('custrecord_sku', null, 'group');
	groupopentaskcolumnarray[1] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
	groupopentaskcolumnarray[2] = new nlobjSearchColumn('custrecord_act_end_date', null, 'group');


	groupopentaskcolumnarray[3] = new nlobjSearchColumn('formulanumeric', null, 'sum');	
	groupopentaskcolumnarray[3].setFormula("TO_NUMBER({custrecord_act_qty})");
	groupopentaskcolumnarray[4] = new nlobjSearchColumn('custrecord_parent_sku_no', null, 'group').setSort(true);	
	var groupopentasksearchresults  = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, groupopentaskfilterarray, groupopentaskcolumnarray);
	var appendcontlp="";
	var actualenddate="";
	var strVar="";
	var noofCartons=0;

	if(groupopentasksearchresults!=null)
	{
		nlapiLogExecution('ERROR', 'groupopentasksearchresults.length ', groupopentasksearchresults.length);
		var totalamount='';
		var groupcount=groupopentasksearchresults.length;
		var grouplength=0;
		var invoicetasklength=groupopentasksearchresults.length;
		var linenumber=1;
		var pagecount=1;

		var totalinvoice=0;

		var totalcount=groupopentasksearchresults.length;
		var totalshipqty=0;
		var totalcube=0;
		var totalweight=0;
		var strVar="";
		while(0<totalcount)
		{


			//for(var h=0; h<totalcount;h++)
			//{
			var count=0;
			var kititemcount=0;
			strVar +="<html>";

			strVar += " <body>";
			strVar += "    <table style=\"width: 100%;\">";
			strVar += "    <tr>";
			strVar += "    <td >";
			strVar += "    <table>";
			strVar += " <td align=\"left\" style=\"width: 65%;\">";
			strVar += "        <table style=\"width: 25%;\" align=\"left\">";
			strVar += "            <tr>";
			strVar += "                <td>";
			strVar += "                    <img src=\"headerimage\" width=\"320\" height=\"65\" />";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 12px; font-family:Arial;\">";
			strVar += "                    <b>AccuQuilt </b>";
			strVar += "<br \/>" +returnadresse+" <br \/>" +addr1+" <br \/>" +stateandzip+"" ;			
			strVar += "                </td>";
			strVar += "            </tr>";			
			strVar += "        </table>";
			strVar += "        </td>";
			strVar += " <td></td>";
			strVar += "    <td></td>";
			strVar += "<td style=\"width: 35%; font-family:Arial;\" valign=\"top\">";
			strVar += "        <b>";
			strVar += "            <h2 align=\"right\">";
			strVar += "                Packing Slip</h2>";
			strVar += "        </b>";
			strVar += "        <table style=\"width: 150px;\" frame=\"box\" rules=\"all\" align=\"right\" border=\"0\" cellpadding=\"0.5\"";
			strVar += "            cellspacing=\"0\">";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 14px; text-align: center; border-top: 1px solid black; border-right: 1px solid black;";
			strVar += "                    border-left: 1px solid black; border-bottom: 1px solid black;\">";
			strVar += "                    Order Date";			
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                    border-bottom: 1px solid black; height: 18px\">";
			strVar += "					"+orderdate+"";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "        </table>";
			strVar += "        </td>";


			strVar += "</table>";
			strVar += "    </td>";
			strVar += "   </tr>";
			strVar += "   <tr>";
			strVar += "<td align=\"left\" style=\"width: 100%;\">";			
			strVar += "        <table style=\"width: 100%\">";
			strVar += "            <tr>";
			strVar += "                <td>";
			strVar += "                    <table style=\"width: 55%;\" rules=\"all\" align=\"left\" border=\"0\" frame=\"box\">";
			strVar += "                        <tr>";
			strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
			strVar += "                                border-left: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                               &nbsp Ship To";
			strVar += "                            </td>";
			strVar += "                        </tr>";
			strVar += "                        <tr>";
			strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height: 80px;\" valign=\"top\">";
			strVar += "                                <table>";
			strVar += "                                    <tr>";
			strVar += "                                        <td style=\"font-size: 12px;\">";
			strVar += "											"+shipaddressee+"";
			strVar += "                                        </td>";
			strVar += "                                    </tr>";
			strVar += "                                    <tr>";
			strVar += "                                        <td style=\"font-size: 12px;\">";
			strVar += "											"+shipaddr1+"";
			strVar += "                                        </td>";
			strVar += "                                    </tr>";
			strVar += "                                    <tr>";
			strVar += "                                        <td style=\"font-size: 12px;\">";
			strVar += "											"+shipstateandcountry+"";
			strVar += "                                        </td>";
			strVar += "                                    </tr>";
			strVar += "                                </table>";
			strVar += "                            </td>";
			strVar += "                        </tr>";
			strVar += "                    </table>";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <br />";
			strVar += "            <tr>";
			strVar += "                <td>";
			strVar += "                    <table style=\"width: 100%;\" rules=\"all\" border=\"0\" frame=\"box\" cellpadding=\"0.5\"";
			strVar += "                        cellspacing=\"0\">";
			strVar += "                        <tr>";
			strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
			strVar += "                                border-left: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                                &nbsp Ship Date";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                                 &nbsp Ship Via";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                               &nbsp PO #";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                               &nbsp FOB";
			strVar += "                            </td>";
			strVar += "                        </tr>";
			strVar += "                        <tr>";
			strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height: 22px;\">";
			strVar += "								 &nbsp"+shipDate+"";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                height: 22px;\">";
			strVar += "								&nbsp"+shipmethod+"";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                height: 22px;\">";
			strVar += "								&nbsp"+customerpo+"";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                height: 22px;\">";
			strVar += "								&nbsp"+FOB+"";
			strVar += "                            </td>";
			strVar += "                        </tr>";
			strVar += "                    </table>";


			strVar += "                    <table style=\"width: 100%;\" rules=\"all\" border=\"0\" frame=\"box\" cellpadding=\"0.5\"";
			strVar += "                        cellspacing=\"0\">";
			strVar += "                        <tr>";
			strVar += "                            <td style=\"font-size: 15px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                               &nbsp Shipping Notes";
			strVar += "                            </td>";
			strVar += "                        </tr>";
			strVar += "                        <tr>";
			strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height: 22px;\">";

			strVar += "                            </td>";
			strVar += "                        </tr>";
			strVar += "                    </table>";


			strVar += "                    <table style=\"width: 100%;\" rules=\"all\" border=\"0\" frame=\"box\" cellpadding=\"0.5\"";
			strVar += "                        cellspacing=\"0\">";
			strVar += "                        <tr>";
			strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                                Item #";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                               &nbsp Description";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                                Ordered";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                                Units";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                                Back Order";
			strVar += "                            </td>";
			strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
			strVar += "                                Shipped";
			strVar += "                            </td>";
			strVar += "                        </tr>";
			//loop starts
			var repeatpartentsku;
			for(var g=grouplength; g<groupopentasksearchresults.length;g++)
			{
				count++;
				grouplength++;
				var itemText = groupopentasksearchresults [g].getText('custrecord_sku', null, 'group');
				var ItemId= groupopentasksearchresults [g].getValue('custrecord_sku', null, 'group');
				var lineno = groupopentasksearchresults [g].getValue('custrecord_line_no', null, 'group');
				var totalqty = groupopentasksearchresults [g].getValue('formulanumeric', null, 'sum');
				nlapiLogExecution('ERROR', 'totalqty ', totalqty);
				var unitvalue,backordervalue,decscription,suggestedprice;
				var parentskuitemid = groupopentasksearchresults [g].getValue('custrecord_parent_sku_no', null, 'group');
				var parentsku = groupopentasksearchresults [g].getText('custrecord_parent_sku_no', null, 'group');
				nlapiLogExecution('ERROR', 'groupopentasklineno ', lineno);
				nlapiLogExecution('ERROR', 'parentskuitemid ', parentskuitemid);
				var parentitemtype='';
				var parentitemSubtype='';
				if(parentskuitemid!=null && parentskuitemid!='')
				{
					parentitemSubtype = nlapiLookupField('item', parentskuitemid, ['recordType']);
					parentitemtype=parentitemSubtype.recordType;
					if(parentitemtype!="kititem")
					{
						if(trantype!='transferorder')
						{
							/* Code commented on OCT23 by Siva Krishna.Vanama
					//unitvalue=salesorderdetails.getLineItemValue('item','rate',lineno);
					//suggestedprice=salesorderdetails.getLineItemValue('item','price_display',lineno);
					backordervalue=salesorderdetails.getLineItemValue('item','quantitybackordered',lineno);
					decscription=salesorderdetails.getLineItemValue('item','description',lineno); 
							 */
							var lineitemcount=salesorderdetails.getLineItemCount('item');
							for(var p=1;p<=lineitemcount;p++)
							{
								var iteminternalid=salesorderdetails.getLineItemValue('item','item',p);
								if(iteminternalid==ItemId)
								{
									//unitvalue=salesorderdetails.getLineItemValue('item','rate',p);
									//suggestedprice=salesorderdetails.getLineItemValue('item','price_display',p);
									backordervalue=salesorderdetails.getLineItemValue('item','quantitybackordered',p);
									decscription=salesorderdetails.getLineItemValue('item','description',p);

									break;


								}


							}
						}
						else
						{
							var lineitemcount=salesorderdetails.getLineItemCount('item');
							for(var p=1;p<=lineitemcount;p++)
							{
								var iteminternalid=salesorderdetails.getLineItemValue('item','item',p);
								if(iteminternalid==ItemId)
								{
									//unitvalue=salesorderdetails.getLineItemValue('item','rate',p);
									//suggestedprice=salesorderdetails.getLineItemValue('item','price_display',p);
									backordervalue=salesorderdetails.getLineItemValue('item','quantitybackordered',p);
									decscription=salesorderdetails.getLineItemValue('item','description',p);

									break;


								}


							}
						}
					}
				}
				else
				{
					backordervalue="0";
					var itemdescription = nlapiLookupField('item', ItemId, ['displayname']);
					decscription=itemdescription.displayname;
				}
				if(parentitemtype=="kititem")
				{
					if(parentsku!=repeatpartentsku)
					{
						var parentskudesc =nlapiLookupField('item', parentskuitemid, ['displayname']);
						var parentdescription=parentskudesc.displayname;
						kititemcount++;
						strVar += "<tr>";
						strVar += "<td style=\"font-size: 14px;font-family:Times New Roman; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
						strVar += " border-bottom: none; height:22px;\">";
						strVar += "								"+parentsku+"";
						strVar += "</td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar +=""+parentdescription+"";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar += "                                           &nbsp";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar += "                                           &nbsp ";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar +=" &nbsp";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar += "   &nbsp";
						strVar += "                            </td>";
						strVar += "                        </tr>";
					}
				}
				strVar += "                        <tr>";
				if(parentskuitemid==ItemId)
				{
					strVar += "                            <td style=\"font-size: 14px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
					strVar += "                                border-bottom: none; height:22px;\">";
					strVar += "								"+itemText+"";
					strVar += "                            </td>";
				}
				else
				{
					strVar += "                            <td style=\"font-size: 14px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
					strVar += "                                border-bottom: none; height:22px;\">";
					strVar += "							&nbsp&nbsp&nbsp"+itemText+"";
					strVar += "                            </td>";
				}
				if(parentskuitemid==ItemId)
				{
					strVar += "                            <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: none; height:22px;\">";
					strVar +=""+decscription+"";
					strVar += "                            </td>";
				}
				else
				{
					strVar += "                            <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: none; height:22px;\">";
					strVar +="&nbsp&nbsp&nbsp"+decscription+"";
					strVar += "                            </td>";
				}
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar += "                                            "+totalqty+" &nbsp";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar += "                                           &nbsp EA";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar +=" &nbsp"+backordervalue+"";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar += "                                            "+totalqty+" &nbsp";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				var pagebreakcount=parseInt(count)+parseInt(kititemcount);
				if(pagebreakcount==10)
				{
					break;
				}
				//Loop Ends
				repeatpartentsku=parentsku;
			}

			// start of for Not Having lines

			var Height='';
			if(pagecount==1)
			{
				Height='230px';
			}
			if(pagecount>1)
			{
				Height='420px';
			}
			var recordCount=pagebreakcount;
			Height=parseInt(Height)-parseInt((recordCount*20));
			nlapiLogExecution('ERROR', 'height ', Height);

			strVar += "                                    <tr>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                                            border-bottom: 1px solid black; height: "+Height+";\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";		
			strVar += "                                    </tr>";

			// End of for Not Having lines

			strVar += "  </table>";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "        </table>";
			strVar += " </td>";
			strVar += "    </tr>";			

			strVar += " <tr>";
			strVar += "    <td>";
			strVar += "    <br \/>";
			strVar += "    ----------------------------------------------------------------------------------------------------------------------------";
			strVar += "    <br \/>";
			strVar += "    <br \/>    ";
			strVar += "    </td>";
			strVar += "    </tr>";

//			strVar += "  <tr>";
//			strVar += "    <td  style=\"width: 55%;\"align=\"right\">";
//			strVar += "    <b>Customer Return From <\/b>";
//			strVar += "    </td>";
//			strVar += "    </tr>";


			strVar += "  <tr>";
			strVar += "   <td>";


			strVar += "        <table style=\"width: 45%;\" align=\"left\">";
			strVar += "<tr>";
			strVar += "            <td style=\"font-size: 15px;\">";
			strVar += "               <br \/>";
			strVar += "            </td>";
			strVar += "        </tr>";
			strVar += " <tr>";
			strVar += "            <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                <b>Ship Returns To<\/b>";
			strVar += "            </td>";
			strVar += "        </tr>";

			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 12px; \">";
			strVar +=""+returnadresse+" <br /> "+addr1+" <br /> "+stateandzip+" ";
			strVar += "                </td>";
			strVar += "            </tr>";

			strVar += "        </table>";


			strVar += "        <table style=\"width: 55%;\" align=\"right\">";			
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\"><b>Customer Return From </b>";
			strVar += "                </td>";
			strVar += "                <td >";			
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                    <b>Customer </b>";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 12px;\">";
			strVar += "											"+shipaddressee+"";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                    <b>Order  </b>";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 12px;\">";
			strVar +=""+ordernumber+"";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                    <b>R.A. # </b>";
			strVar += "                </td>";
			strVar += "                <td>";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "        </table>";
			strVar += "        <br />";
			strVar += "        <br />";
			strVar += "        <br />";
			strVar += "        <br />";			
			strVar += " </td>";
			strVar += "     </tr>";

			strVar += " <tr>";
			strVar += "     <td>";
			strVar += "        <table style=\"width: 100%;\" frame=\"box\" rules=\"all\" align=\"right\" border=\"0\" cellpadding=\"0.5\"";
			strVar += "            cellspacing=\"0\">";
			strVar += "            <tr style=\"background-color: Gray;\">";
			strVar += "                <td style=\"font-size: 15px; text-align: left; color: white; border-top: 1px solid black;";
			strVar += "                    border-right: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black; font-family:Arial;\">";
			strVar += "                    &nbsp Item";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 15px; text-align: left; color: white; border-top: 1px solid black;";
			strVar += "                    border-right: 1px solid black; border-bottom: 1px solid black; font-family:Arial;\">";
			strVar += "                    &nbsp Quantity";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 15px; text-align: left; color: white; border-top: 1px solid black;";
			strVar += "                    border-right: 1px solid black; border-bottom: 1px solid black; font-family:Arial;\">";
			strVar += "                   &nbsp Reason For Returning";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 16px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                    border-bottom: 1px solid black;\" height=\"55px\">";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 16px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;\"";
			strVar += "                    height=\"55px\">";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 16px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;\"";
			strVar += "                    valign=\"Top\">";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "        </table>";
			strVar += "</td>";
			strVar += "    </tr>";

			if(grouplength==groupopentasksearchresults.length)
			{
				strVar +="<p style=\" page-break-after:avoid\"></p>";
			}
			else
			{
				strVar +="<p style=\" page-break-after:always\"></p>";
			}
			strVar += "        </table>";
			strVar += " </body>";
			strVar += "        </html>";
			nlapiLogExecution('ERROR', 'totalcount', totalcount);
			totalcount=parseInt(totalcount)-parseInt(count);
			nlapiLogExecution('ERROR', 'totalcountafter', totalcount);

		}
		var tasktype='14';
		var labeltype='PackList';
		var print='F';
		var reprint='F';
		var company='';
		var location='';
		var formattype='html';
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name', fulfillmentnumber); 
		labelrecord.setFieldValue('custrecord_labeldata',strVar);  
		labelrecord.setFieldValue('custrecord_label_refno',fulfillmentnumber);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);//chkn task   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);    
		labelrecord.setFieldValue('custrecord_label_lp',fulfillmentnumber);

		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_company', company);
		labelrecord.setFieldValue('custrecord_label_location', location);
		labelrecord.setFieldValue('custrecord_label_printername',printername);

		//nlapiLogExecution('ERROR', 'htmlstring ', strVar);

		var tranid = nlapiSubmitRecord(labelrecord);
		nlapiLogExecution('ERROR', 'tranid ', tranid);
		//PackListMessage(tranid,printername,request);
	}

}



function getcustomerdetails(customerid,storeid)
{
	// start of getting Customer details
	var templete;
	var templateinternalid;
	var templatearray=new Array();

	var labelPrintingcustomerfilterarray= new Array();
	nlapiLogExecution('ERROR', 'In to Function templetType','In to Function templetType');
	nlapiLogExecution('ERROR', 'customerid', customerid);
	//nlapiLogExecution('ERROR', 'subsidiary', subsidiary);
	nlapiLogExecution('ERROR', 'storeid', storeid);
	//nlapiLogExecution('ERROR', 'templetType', templetType);
	//Please uncheck  the fillementnumber code 
	// labelPrintingcustomerfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_packlist_partnername', null, 'is',customerAddressee));
	//var templateresults;
	if(customerid!=null && customerid!='')
	{
		labelPrintingcustomerfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_packlist_customer', null, 'anyof',customerid));
	}
	if(storeid!=null && storeid!='')
	{
		labelPrintingcustomerfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_packlist_storenumber', null, 'is',storeid));
	}

	var labelPrintingcustomercolumnarray= new Array();
	labelPrintingcustomercolumnarray[0] = new nlobjSearchColumn('custrecord_ebiz_packlist_template');
	labelPrintingcustomercolumnarray[1] = new nlobjSearchColumn('InternalId');
	labelPrintingcustomercolumnarray[1].setSort();
	var labelPrintingcustomersearchresults= nlapiSearchRecord('customrecord_ebiz_packlistreference', null,labelPrintingcustomerfilterarray, labelPrintingcustomercolumnarray);		

	var websiteAddress='';var servicePhoneNo='';var returninformation='';
	if(labelPrintingcustomersearchresults!='' && labelPrintingcustomersearchresults!=null)
	{	
		templete=labelPrintingcustomersearchresults[0].getText('custrecord_ebiz_packlist_template');
		templateinternalid=labelPrintingcustomersearchresults[0].getValue('InternalId');
	}
	nlapiLogExecution('ERROR', 'Template Result', templete);
	var row=[templete,templateinternalid];
	templatearray.push(row);

	return templatearray;
}


function createDynamicPacklistHtml(vebizOrdNo,fulfillmentnumber,printername,trantype,salesorderdetails,locationRecord)
{
	nlapiLogExecution('ERROR', 'In to createDynamicPacklistHtml', 'In to createDynamicPacklistHtml');
	var SODetails=SalesOrderpackList(vebizOrdNo);



	var customerid=SODetails[0].getValue('entity');
	nlapiLogExecution('ERROR', 'customerid is:', customerid);
	var customerdetails =nlapiLoadRecord('customer',customerid);
	nlapiLogExecution('ERROR', 'customerdetails', customerdetails);
	var templetType=customerdetails.getFieldValue('custentity_cust_template');
	nlapiLogExecution('ERROR', 'templetType is:', templetType);

	var storeid=SODetails[0].getValue('otherrefnum');
	nlapiLogExecution('ERROR', 'storeid is:', storeid);

	templateresults=getcustomerdetails(customerid,storeid)
	if(templateresults !=null && templateresults !='')
	{
		for(m=0; m < templateresults.length; m++)
		{

			var templete=  templateresults[m][0] ;
			var templeteinternalid= templateresults[m][1] ;


		}
	}
	var wavenumber='';
	nlapiLogExecution('ERROR', 'templete', templete);
	nlapiLogExecution('ERROR', 'OutOf get templete Function', templeteinternalid);	

	if(templete=='PackA')
	{
		nlapiLogExecution('ERROR', 'IntoPackA', 'IntoPackA');
		CreatePackApacklisthtml(vebizOrdNo,fulfillmentnumber,printername,salesorderdetails,trantype,wavenumber,templete);
	}
	else if(templete=='PackB')
	{
		nlapiLogExecution('ERROR', 'IntoPackB', 'IntoPackB');
		CreatePackBpacklisthtml(vebizOrdNo,fulfillmentnumber,printername,salesorderdetails,trantype,wavenumber,templete);
	}

	else if(templete==''||templete==null)
	{
		nlapiLogExecution('ERROR', 'IntoNoTemplate', 'IntoNoTemplate');
		createStandardPacklistHtml(vebizOrdNo,fulfillmentnumber,printername,templetType,locationid,shipmethod,templeteinternalid,masterlp)
	}
}

function SalesOrderpackList(vebizOrdNo) {
	nlapiLogExecution('Debug', 'General Functions', 'In to SalesOrderList');
	var searchresults = new Array();
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('shipcarrier');
	columns[2] = new nlobjSearchColumn('custbody_nswms_company');
	columns[3] = new nlobjSearchColumn('shipaddress1');
	columns[4] = new nlobjSearchColumn('shipaddress2');
	columns[5] = new nlobjSearchColumn('shipcity');
	columns[6] = new nlobjSearchColumn('shipstate');
	columns[7] = new nlobjSearchColumn('shipcountry');
	columns[8] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[9] = new nlobjSearchColumn('entity');
	columns[10] = new nlobjSearchColumn('custbody_nswmsfreightterms');
	columns[11] = new nlobjSearchColumn('custbody_customer_phone');
	columns[12] = new nlobjSearchColumn('shipzip');
	columns[13] = new nlobjSearchColumn('email');
	columns[14] = new nlobjSearchColumn('custbody_nswmssosaturdaydelivery');
	columns[15] = new nlobjSearchColumn('custbody_nswmscodflag');
	columns[16] = new nlobjSearchColumn('shipmethod');
	columns[17] = new nlobjSearchColumn('otherrefnum');
	columns[18] = new nlobjSearchColumn('shipattention');
	columns[19] = new nlobjSearchColumn('location');
	columns[20] = new nlobjSearchColumn('shipaddressee');
	columns[21] = new nlobjSearchColumn('custbody_ebiz_thirdpartyacc');
	columns[22] = new nlobjSearchColumn('custbody_nswms_company');
	columns[23] = new nlobjSearchColumn('custbody_nswms_company');	
	columns[24] = new nlobjSearchColumn('entity');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	return searchresults;
}

