/***************************************************************************
 eBizNET Solutions LTD               
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_CYCC_ResolveScheduler.js,v $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_CYCC_ResolveScheduler.js,v $
 * Revision 1.1.4.3.2.1  2015/09/21 14:02:10  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.4.3  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.4.2  2015/02/04 05:44:08  snimmakayala
 * Case#: 201411397
 * Cycle Count Resolve Scheduler
 * 
 *****************************************************************************/

function ResolveScheduler(type) {

	nlapiLogExecution('ERROR','Into  Resolve Schduler','');
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
	var restype = context.getSetting('SCRIPT', 'custscript_nsrestype');
	var venteredfieldvalues = context.getSetting('SCRIPT', 'custscript_ns_params');
	nlapiLogExecution('ERROR','restype',restype);
	nlapiLogExecution('ERROR','venteredfieldvalues',venteredfieldvalues);
	var curuserId = context.getUser();


	var LineLevelEnteredValueSet=venteredfieldvalues.split("$");
	nlapiLogExecution('ERROR', 'Strart LineLevelEnteredValueSet', LineLevelEnteredValueSet.length);

	//

	var vcyccplanno = 0;

	if(LineLevelEnteredValueSet!=null && LineLevelEnteredValueSet!='')
	{

		var vLineLevelValues = LineLevelEnteredValueSet[0].split("&");

		nlapiLogExecution('ERROR','vLineLevelValues',vLineLevelValues);

		var vcyccRecID=vLineLevelValues[0];	
		nlapiLogExecution('ERROR','vcyccRecID',vcyccRecID);

		var CyccExeRecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',vcyccRecID);

		vcyccplanno = CyccExeRecord.getFieldValue('custrecord_cycle_count_plan');
	}

	updateScheduleScriptStatus('CYC Resolve',curuserId,'In Progress',vcyccplanno,null);

	if (restype == 'ignore' || restype == 'resolve'|| restype == 'close')
	{
		var TempArrVal = new Array(); 
		nlapiLogExecution('ERROR', 'into checking', 'done');	
		var tempmsg='';
		for (var venteredlinecount=0;venteredlinecount<LineLevelEnteredValueSet.length;venteredlinecount++) {


			nlapiLogExecution('ERROR', 'Inventory Update', LineLevelEnteredValueSet[venteredlinecount]);
			var LineLevelEnteredValue=LineLevelEnteredValueSet[venteredlinecount].split("&");
			nlapiLogExecution('ERROR', 'Inventory lebght', LineLevelEnteredValue.length);
			nlapiLogExecution('ERROR', 'Values', LineLevelEnteredValue);
			var CyccRecID=LineLevelEnteredValue[0];	
			nlapiLogExecution('ERROR', 'Execution CyccRecID', CyccRecID);
			if(CyccRecord!=null && CyccRecord!='')
			{
				var CyccRecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',CyccRecID); // 2 UNITS
				var invtrecid = CyccRecord.getFieldValue('custrecord_invtid');
				var actqty = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
				var lp = CyccRecord.getFieldValue('custrecord_cycact_lpno');

				nlapiLogExecution('ERROR','invtrecid',invtrecid);
				nlapiLogExecution('ERROR','actqty',actqty);
				nlapiLogExecution('ERROR','lp',lp);

				if (restype == 'resolve'|| restype == 'close') {					

					try
					{
						var transaction1 = nlapiLoadRecord('customrecord_ebiznet_createinv', invtrecid);
						// Case#20127831 starts
						transaction1.setFieldValue('custrecord_ebiz_invholdflg', 'F');
						transaction1.setFieldValue('custrecord_ebiz_cycl_count_hldflag', 'F');
						nlapiSubmitRecord(transaction1, false, true);
						// Case#20127831 ends
						var allocQty1 = transaction1.getFieldValue('custrecord_ebiz_alloc_qty');
						var quanty1=actqty;
						nlapiLogExecution('ERROR','allocQty1',allocQty1);
						if(parseFloat(quanty1) < parseFloat(allocQty1))
						{
							nlapiLogExecution('ERROR', 'into tempmsg', tempmsg);	
							tempmsg= lp + ","+ tempmsg;						
						}
					}
					catch (e) {
						nlapiLogExecution('ERROR', 'This record doesnt exists so cannot load the record', e);

					}
				}

			}
		}
		if( tempmsg !=null && tempmsg !='')
		{
			var flag=false;
			TempArrVal.push(flag);
			TempArrVal.push(tempmsg);
			return TempArrVal;
		}
	}	

	for (var venteredlinecount1=0;venteredlinecount1<LineLevelEnteredValueSet.length;venteredlinecount1++) {

		var fieldNames = new Array(); 
		var newValues = new Array(); 		

		nlapiLogExecution('ERROR', 'Update Cycle Execution', LineLevelEnteredValueSet[venteredlinecount1]);
		var LineLevelEnteredValue1=LineLevelEnteredValueSet[venteredlinecount1].split("&");
		nlapiLogExecution('ERROR', 'LineLevelEnteredValue111', LineLevelEnteredValue1.length);
		nlapiLogExecution('ERROR', 'LineLevelEnteredValue112', LineLevelEnteredValue1);

		var CyccRecID=LineLevelEnteredValue1[0];
		if(CyccRecID!=null && CyccRecID!='')
		{
			var CyccRecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',CyccRecID); // 2 UNITS
			var invtrecid = CyccRecord.getFieldValue('custrecord_invtid');


			nlapiLogExecution('ERROR', 'CyccRecID', CyccRecID);	

			fieldNames.push('custrecord_cyclerec_upd_date'); 
			newValues.push(DateStamp());
			fieldNames.push('custrecord_cyclerec_upd_time'); 
			newValues.push(TimeStamp());
			fieldNames.push('custrecord_cycleupd_user_no'); 
			newValues.push(nlapiGetContext().getUser());

			if (restype == 'recount') {


				fieldNames.push('custrecord_cycleact_sku'); 
				newValues.push('');
				fieldNames.push('custrecord_cyclecount_act_ebiz_sku_no'); 
				newValues.push('');
				fieldNames.push('custrecord_cycle_act_qty'); 
				newValues.push('');
				fieldNames.push('custrecord_cycact_lpno'); 
				newValues.push('');
				fieldNames.push('custrecord_cycleabatch_no'); 
				newValues.push('');
				fieldNames.push('custrecord_cyclesku_status'); 
				newValues.push('');
			}
			if (restype == 'recount') {

				fieldNames.push('custrecord_cyclestatus_flag'); 
				newValues.push('20');
			}
			else 
				if (restype == 'ignore') {
					fieldNames.push('custrecord_cyclestatus_flag'); 
					newValues.push('21');
				}
				else 
					if (restype == 'resolve') {
						fieldNames.push('custrecord_cyclestatus_flag'); 
						newValues.push('22');
					}
					else {
						fieldNames.push('custrecord_cyclestatus_flag'); 
						newValues.push('22');
					}

			fieldNames.push('custrecord_cyccplan_resolvedate'); 
			newValues.push(DateStamp());
			fieldNames.push('custrecord_cyccplan_resolvetime'); 
			newValues.push(TimeStamp());
			fieldNames.push('custrecord_cyccplan_resolvedby'); 
			newValues.push(nlapiGetContext().getUser());

			nlapiSubmitField('customrecord_ebiznet_cyclecountexe', CyccRecID, fieldNames, newValues);	
			var cycctransaction;
			if(CyccRecID!=null && CyccRecID!='')
			{
				cycctransaction = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', CyccRecID);
				vcycActSKU = cycctransaction.getFieldValue('custrecord_cycleact_sku');
			}


			nlapiLogExecution("ERROR","invtrecid",invtrecid);
			if(invtrecid!=null&&invtrecid!=""&&invtrecid!="null")
			{
				var scount=1;
				LABL1: for(var j=0;j<scount;j++)
				{	
					nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', j);
					try
					{
						var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtrecid);
						transaction.setFieldValue('custrecord_ebiz_invholdflg', "F");
						transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', "F");						

						nlapiSubmitRecord(transaction, false, true);
						nlapiLogExecution('ERROR', 'Cyclecount generated for inv ref ',invtrecid);

					}
					catch(ex)
					{
						var exCode='CUSTOM_RECORD_COLLISION'; 

						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}				 
						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{ 
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}
			}
		}

	}
	nlapiLogExecution('ERROR', 'before loop', 'done');
	if (restype == 'ignore' || restype == 'resolve'|| restype == 'close' || restype == 'recount') {
		nlapiLogExecution('ERROR', 'entered into process', 'done');

		for (var venteredlinecount2=0;venteredlinecount2<LineLevelEnteredValueSet.length;venteredlinecount2++) {

			nlapiLogExecution('ERROR', 'SetLineLevelEnteredValue[venteredlinecount]', LineLevelEnteredValueSet[venteredlinecount2]);
			var LineLevelEnteredValue2=LineLevelEnteredValueSet[venteredlinecount2].split("&");
			nlapiLogExecution('ERROR', 'LineLevelEnteredValue1', LineLevelEnteredValue2.length);
			nlapiLogExecution('ERROR', 'LineLevelEnteredValue', LineLevelEnteredValue2);

			var CyccRecID=LineLevelEnteredValue2[0];
			if(CyccRecID!=null && CyccRecID!='')
			{
				var CyccRecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',CyccRecID); // 2 UNITS
				var invtrecid = CyccRecord.getFieldValue('custrecord_invtid');

				var accountno="";
				var sitelocn="";
				var resultQty=0;
				var resultQty1 = 0;
				var memo="";
				var NSAdjustid='';
				var NSAdjustid1='';				
				var CyccRecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',CyccRecID); // 2 UNITS
				var expserialno = CyccRecord.getFieldValue('custrecord_cycle_expserialno');
				var actserialno = CyccRecord.getFieldValue('custrecord_cycle_serialno');				
				var vItemId = CyccRecord.getFieldValue('custrecord_cycleact_sku');
				var vExpItemId=CyccRecord.getFieldValue('custrecord_cyclesku');
				var isfromaddNewItem=CyccRecord.getFieldValue('custrecord_cycle_notes');
				nlapiLogExecution('ERROR', 'isfromaddNewItem', isfromaddNewItem);

				if (restype == 'resolve'|| restype == 'close') {
					try {
						var vWHLoc;
						var vTaskType='7';
						var vAdjusttype;
						vWHLoc=CyccRecord.getFieldValue('custrecord_cyclesite_id');
						var qtyonhand;
						var opentaskid = CyccRecord.getFieldValue('custrecord_opentaskid');
						var planno = CyccRecord.getFieldValue('custrecord_cycle_count_plan');


						var AccountNo = getStockAdjustmentAccountNoNew(vWHLoc,vTaskType,vAdjusttype);

						if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
						{
							var CreatePlanRecord = nlapiLoadRecord('customrecord_ebiznet_cylc_createplan',planno); // 2 UNITS
							var Cycexeplanid= planno;
							var vEmptyBinLoc = CreatePlanRecord.getFieldValue('custrecord_ebiz_include_emploc');
							nlapiLogExecution('ERROR', 'vEmptyBinLoc', vEmptyBinLoc);
							nlapiLogExecution('ERROR', 'Cycexeplanid', Cycexeplanid);
							if(isfromaddNewItem!=null && isfromaddNewItem=='fromaddnewitem')
							{
								vEmptyBinLoc='T';
							}
							var filterscycexec = new Array();
							filterscycexec.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [22]));
							filterscycexec.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',Cycexeplanid));
							filterscycexec.push(new nlobjSearchFilter('custrecord_opentaskid', null, 'equalto',opentaskid));
							var vRoleLocation=getRoledBasedLocation();
							if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
							{
								filterscycexec.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
							}

							var columns = new Array();
							columns.push(new nlobjSearchColumn('custrecord_cycle_count_plan'));
							columns.push(new nlobjSearchColumn('custrecord_cycleact_sku'));
							columns.push(new nlobjSearchColumn('custrecord_cycle_act_qty'));
							columns.push(new nlobjSearchColumn('custrecord_cycact_lpno'));
							columns.push(new nlobjSearchColumn('custrecord_cyclecount_act_ebiz_sku_no'));
							columns.push(new nlobjSearchColumn('custrecord_cyclestatus_flag'));
							columns.push(new nlobjSearchColumn('custrecord_cycact_beg_loc'));
							columns.push(new nlobjSearchColumn('custrecord_expcyclesku_status'));
							columns.push(new nlobjSearchColumn('custrecord_cyclepc'));
							columns.push(new nlobjSearchColumn('custrecord_cyclesite_id'));
							columns.push(new nlobjSearchColumn('custrecord_cyclesku_status'));
							columns.push(new nlobjSearchColumn('custrecord_invtid'));
							columns.push(new nlobjSearchColumn('custrecord_cycle_skudesc'));
							var CycleCountSearchResult = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filterscycexec,columns);
							nlapiLogExecution('ERROR', 'cyclecountexecutionresult', CycleCountSearchResult);

							if(vEmptyBinLoc=='T' && CycleCountSearchResult !=null && CycleCountSearchResult.length>0 && (CycleCountSearchResult[0].getValue('custrecord_invtid') == null || CycleCountSearchResult[0].getValue('custrecord_invtid') == ""))
							{
								var expvarianceqty='';
								var actualvarianceqty='';
								var varianceinActserialNoNew='';

								var varianceinExpserialNo = '';
								var varianceinActserialNo = '';


								var actualvarianceqtyNew=0;
								var expvarianceqtynotforNS=0;
								var expserialnoToNS ='';
								var expserialnoNotforNS ='';
								var actnewserialnoToNS ='';

								nlapiLogExecution('ERROR', 'cyclecountexecutionresult.length', CycleCountSearchResult.length);
								nlapiLogExecution('ERROR', 'actserialno in empty bin loc', actserialno);
								if(actserialno != null && actserialno != '')
								{
									actserialno = actserialno.trim();
									var getactserialArr = actserialno.split(',');
									if(expserialno != null && expserialno != '')
									{
										expserialno = expserialno.trim();
										var getexpserialArr = expserialno.split(',');
									}

									for (var q = 0; q < getactserialArr.length; q++) 
									{
										if(getexpserialArr != null && getexpserialArr != '')
										{
											if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
											{
												if (varianceinActserialNo == null || varianceinActserialNo == '') {
													varianceinActserialNo= getactserialArr[q];

												}
												else {
													varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

												}
											}
										}
										else
										{
											if (varianceinActserialNo == null || varianceinActserialNo == '') {
												varianceinActserialNo= getactserialArr[q];

											}
											else {
												varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

											}
										}
										varianceinActserialNo = getactserialArr[q];
										nlapiLogExecution('ERROR', 'getactserialArr[q]', getactserialArr[q]);
										var filters = new Array();
										filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getactserialArr[q]);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_serialparentid');
										var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

										if(searchResults != null && searchResults != "" && searchResults.length == 1)
										{

											if (varianceinActserialNoNew == "") {
												varianceinActserialNoNew= getactserialArr[q];

											}
											else {
												varianceinActserialNoNew = varianceinActserialNoNew + "," + getactserialArr[q];

											}
										}
									}

									nlapiLogExecution('ERROR', 'vItemId ', vItemId);
									nlapiLogExecution('ERROR', 'vWHLoc ', vWHLoc);

									var totalNSserialno = '';

									var filters = new Array();
									filters.push(new nlobjSearchFilter('item',null,'anyof', vItemId));
									filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('inventorynumber');
									columns[1] = new nlobjSearchColumn('internalid').setSort();

									var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

									for (var i2 = 0; searchresultsitem != null && i2 < searchresultsitem.length; i2++) 
									{
										if(totalNSserialno == null || totalNSserialno == '')
											totalNSserialno = searchresultsitem[i2].getValue('inventorynumber');
										else
											totalNSserialno = totalNSserialno + ',' + searchresultsitem[i2].getValue('inventorynumber');

									}

									var vtotNSSerialARR = new Array();
									if(totalNSserialno != null && totalNSserialno  != '')
										vtotNSSerialARR = totalNSserialno.split(',');

									nlapiLogExecution('ERROR', 'vtotNSSerialARR ', vtotNSSerialARR);


									if(varianceinActserialNoNew != null && varianceinActserialNoNew != '')
									{
										nlapiLogExecution('ERROR', 'varianceinActserialNoNew inside if', varianceinActserialNoNew);	
										var vMissedActSerialARR = varianceinActserialNoNew.split(',');
										nlapiLogExecution('ERROR', 'vMissedActSerialARR ', vMissedActSerialARR);

										for(m=0;m<vMissedActSerialARR.length;m++)
										{
											if(vtotNSSerialARR.indexOf(vMissedActSerialARR[m])==-1)// found temSeriIdArr

											{
												if(actnewserialnoToNS ==null || actnewserialnoToNS =='')
													actnewserialnoToNS = vMissedActSerialARR[m];
												else
													actnewserialnoToNS = actnewserialnoToNS + ',' +vMissedActSerialARR[m];
											}
										}
									}

									if(varianceinActserialNo != null && varianceinActserialNo != '')
										actualvarianceqty =varianceinActserialNo.split(',').length;

									if(actnewserialnoToNS != null && actnewserialnoToNS != '')
										actualvarianceqtyNew =actnewserialnoToNS.split(',').length;

									nlapiLogExecution('ERROR', 'actualvarianceqty ', actualvarianceqty);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);										
									nlapiLogExecution('ERROR', 'varianceinActserialNo ', varianceinActserialNo);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);									
									nlapiLogExecution('ERROR', 'actnewserialnoToNS ', actnewserialnoToNS);	

								}

								nlapiLogExecution('ERROR', 'cyclecountexecutionresultlength', CycleCountSearchResult.length);
								var vStatusFlag=CycleCountSearchResult[0].getValue('custrecord_cyclestatus_flag');
								nlapiLogExecution('ERROR', 'vStatusFlag',vStatusFlag );
								if(vStatusFlag!=null && vStatusFlag==22)
								{
									var LotBatchId='';

									var LotBatchText = CyccRecord.getFieldValue('custrecord_cycleabatch_no');
									var vtempactualItem = CyccRecord.getFieldValue('custrecord_cycleact_sku');


									if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText,vtempactualItem);

									/*if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText);*/
									var ItemStatus = "";

									if(CycleCountSearchResult[0].getValue('custrecord_cyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_cyclesku_status').toString();
									}
									else if(CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status').toString();
									}
									else
									{
										var vRoleLocation=getRoledBasedLocation();
										nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
										var itemStatusFilters = new Array();
										itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
										itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
										itemStatusFilters[2] = new nlobjSearchFilter('custrecord_defaultskustaus',null, 'is','T');
										if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
										{
											//Case # 20126438 Start
											itemStatusFilters[3] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',vRoleLocation);
											//Case # 20126438 End
										}

										var itemStatusColumns = new Array();			
										itemStatusColumns[0] = new nlobjSearchColumn('name');

										var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);			
										if(itemStatusSearchResult != null && itemStatusSearchResult != '')
										{
											ItemStatus = itemStatusSearchResult[0].getId();
										}
										nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
									}

									var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
									var columns;
									var ItemType;
									var serialInflg="F";
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!=null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!='')
									{
										columns = nlapiLookupField('item', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'), fields);
										ItemType = columns.recordType;
										serialInflg = columns.custitem_ebizserialin;
									}
									//var columns = nlapiLookupField('item', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'), fields);
									//nlapiLogExecution('ERROR', 'Time Stamp at the end of Item Lookup',TimeStampinSec());
									//var ItemType = columns.recordType;	


									var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
									nlapiLogExecution('ERROR', 'Creating INVT Record ', 'INVT');
									var loc=CycleCountSearchResult[0].getValue('custrecord_cyclesite_id');
									var getsysItemLP = GetMaxLPNo(1, 1,loc);
									if(Cycexeplanid!= null && Cycexeplanid != "")
										invtRec.setFieldValue('name', Cycexeplanid);
									nlapiLogExecution('ERROR', 'Cycexeplanid',Cycexeplanid );
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_binloc', CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc'));
									nlapiLogExecution('ERROR', 'actbeginloc',CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') );
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_lpno')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_lpno') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_lp', CycleCountSearchResult[0].getValue('custrecord_cycact_lpno'));
									else
										invtRec.setFieldValue('custrecord_ebiz_inv_lp', getsysItemLP);
									nlapiLogExecution('ERROR', 'invlp', CycleCountSearchResult[0].getValue('custrecord_cycact_lpno'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									nlapiLogExecution('ERROR', 'sku_no',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatus);
									nlapiLogExecution('ERROR', 'sku_status',CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclepc')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclepc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_packcode', CycleCountSearchResult[0].getValue('custrecord_cyclepc'));
									nlapiLogExecution('ERROR', 'packcode',CycleCountSearchResult[0].getValue('custrecord_cyclepc') );
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty') != "")
									{
										var qty=parseFloat(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty'));
										invtRec.setFieldValue('custrecord_ebiz_qoh', qty);
										invtRec.setFieldValue('custrecord_ebiz_inv_qty', qty.toString());
										nlapiLogExecution('ERROR', 'actqty',qty);
									}
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_inv_ebizsku_no', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'));
									//
									nlapiLogExecution('ERROR', 'sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									// Case# 20127753 starts
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') != "")
										//Case# 20127753 ends
										invtRec.setFieldValue('custrecord_ebiz_itemdesc', CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc'));
									nlapiLogExecution('ERROR', 'itemdesc',CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') );
									invtRec.setFieldValue('custrecord_invttasktype', 2);
									invtRec.setFieldValue('custrecord_wms_inv_status_flag', ['19']); //Inventory Storage
									if(CycleCountSearchResult[0].getValue('custrecord_cyclesite_id')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesite_id') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_loc',CycleCountSearchResult[0].getValue('custrecord_cyclesite_id').toString());
									invtRec.setFieldValue('custrecord_ebiz_callinv','N');
									invtRec.setFieldValue('custrecord_ebiz_displayfield','N');
									if(LotBatchId!=null&&LotBatchId!="")
									{
										invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);	
										var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatchId,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
										var fifodate=rec.custrecord_ebizfifodate;
										var expdate=rec.custrecord_ebizexpirydate;
										if(fifodate!=null && fifodate!='')
											invtRec.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);

										invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
									}
									else
									{
										if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
										{ 										
											var itemid=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');
											nlapiLogExecution('ERROR', 'itemid', itemid);
											var Batchfilters = new Array();
											Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',itemid ));
											var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, Batchfilters);
											if(Batchsearchresults!=null)
											{
												var	getlotnoid= Batchsearchresults[0].getId();
												nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
												invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
											}	
										}
									}

									var LP = "";
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_lpno')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_lpno') != "")
									{
										LP = CycleCountSearchResult[0].getValue('custrecord_cycact_lpno');
									}									

									var serialnumbers = "";
									var localSerialNoArray = new Array();
									if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
									{
										var sku = CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');
										var qty=parseFloat(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty'));
										//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(qty),sku,LP,restype,CyccRecID);
										AdjustSerialNumbers(localSerialNoArray,parseFloat(qty),sku,LP,restype,CyccRecID,'');
									}
									if(AccountNo!= null && AccountNo != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_account_no', AccountNo[0]);	

									try{
										nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORD');
										var invtrecid = nlapiSubmitRecord(invtRec, false, true);
										nlapiLogExecution('ERROR', 'After Submitting invtrecid',invtrecid );
									}
									catch (e)
									{
										if ( e instanceof nlobjError )
											nlapiLogExecution( 'ERROR', 'system error', e.getCode() + '\n' + e.getDetails() );
										else
											nlapiLogExecution( 'ERROR', 'unexpected error', e.getCode() + '\n' + e.getDetails() );
									}

									var confirmhost=LineLevelEnteredValue2[2];	
									nlapiLogExecution('ERROR', 'confirmhost',confirmhost );
									if(confirmhost=='T')//
									{
										var LotBatchText='';

										LotBatchText = CyccRecord.getFieldValue('custrecord_cycleabatch_no');
										if(LotBatchText == "" || LotBatchText == null)
										{
											nlapiLogExecution( 'ERROR', 'skuno', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
											var Batchfilters = new Array();
											Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') ));
											var Batchcolumns=new Array();
											Batchcolumns[0]=new nlobjSearchColumn('name');
											var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry',null, Batchfilters, Batchcolumns);

											nlapiLogExecution( 'ERROR', 'Batchsearchresults', Batchsearchresults );
											if(Batchsearchresults!=null)
											{
												LotBatchText= Batchsearchresults[0].getValue('name');
												nlapiLogExecution( 'ERROR', 'LotBatchText', LotBatchText );
											}
										}
										var itmstatus=null;

										var actqty1 = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
										var FromAccNo=AccountNo[0];
										var ItemId=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');										
										var notes="This is from Cycle count";

										nlapiLogExecution('ERROR','ItemStatus',ItemStatus);
										try
										{
											NSAdjustid =InvokeNSInventoryAdjustmentNew(ItemId,ItemStatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,varianceinActserialNoNew,FromAccNo);
										}
										catch(exp)
										{
											nlapiLogExecution('ERROR','Exception in InvokeNSInventoryAdjustmentNew',exp);
										}
									}
								}

							}
							else
							{	
								var LP = "";
								var TotQty = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
								var palletQuantity = 0;

								var expvarianceqty='';
								var actualvarianceqty='';
								var varianceinActserialNoNew='';

								var varianceinExpserialNo = '';
								var varianceinActserialNo = '';

								var varianceinActserialNoNew=0;
								var varianceinActserialNoNew=0;
								var actualvarianceqtyNew=0;
								var expvarianceqtytoNS=0;
								var expvarianceqtynotforNS=0;
								var expserialnoToNS ='';
								var expserialnoNotforNS ='';
								var actnewserialnoToNS ='';

								if((actserialno != null && actserialno != '') || (expserialno != null && expserialno != ''))
								{
									var getexpserialArr =  new Array();
									var getactserialArr =  new Array();

									if(expserialno != null && expserialno != '')
									{
										expserialno = expserialno.trim();
										getexpserialArr = expserialno.split(',');
									}

									if(actserialno != null && actserialno != '')
									{
										actserialno = actserialno.trim();
										getactserialArr = actserialno.split(',');
									}

									if(getexpserialArr != null && getexpserialArr != '')
									{
										for (var p = 0; p < getexpserialArr.length; p++) 
										{

											if(actserialno != null && actserialno != '')
											{
												if(actserialno.indexOf(getexpserialArr[p])==-1)//not found temSeriIdArr
												{
													if (varianceinExpserialNo == "") {
														varianceinExpserialNo= getexpserialArr[p];

													}
													else {
														varianceinExpserialNo = varianceinExpserialNo + "," + getexpserialArr[p];

													}
												}
											}
										}
									}

									if(getactserialArr != null && getactserialArr != '')
									{
										for (var q = 0; q < getactserialArr.length; q++) 
										{

											if(getexpserialArr != null && getexpserialArr != '')
											{
												if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
												{
													nlapiLogExecution('ERROR', 'getactserialArr[q] in side', getactserialArr[q]);
													if (varianceinActserialNo == "") {
														varianceinActserialNo= getactserialArr[q];

													}
													else {
														varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

													}

													var filters = new Array();
													filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getactserialArr[q]);

													var columns = new Array();
													columns[0] = new nlobjSearchColumn('custrecord_serialparentid');
													var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

													if(searchResults != null && searchResults != "" && searchResults.length == 1)
													{

														if (varianceinActserialNoNew == "") {
															varianceinActserialNoNew= getactserialArr[q];

														}
														else {
															varianceinActserialNoNew = varianceinActserialNoNew + "," + getactserialArr[q];

														}
													}
												}
											}
											else
											{
												// case no 20126236

												nlapiLogExecution('ERROR', 'getactserialArr[q] in side', getactserialArr[q]);
												if (varianceinActserialNo == "") {
													varianceinActserialNo= getactserialArr[q];

												}
												else {
													varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

												}

												var filters = new Array();
												filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getactserialArr[q]);

												var columns = new Array();
												columns[0] = new nlobjSearchColumn('custrecord_serialparentid');
												var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

												if(searchResults != null && searchResults != "" && searchResults.length == 1)
												{

													if (varianceinActserialNoNew == "") {
														varianceinActserialNoNew= getactserialArr[q];

													}
													else {
														varianceinActserialNoNew = varianceinActserialNoNew + "," + getactserialArr[q];

													}
												}

											}
										}
									}
									nlapiLogExecution('ERROR', 'varianceinExpserialNo ', varianceinExpserialNo);
									nlapiLogExecution('ERROR', 'varianceinActserialNoNew ', varianceinActserialNoNew);	

									nlapiLogExecution('ERROR', 'vItemId ', vItemId);
									nlapiLogExecution('ERROR', 'vWHLoc ', vWHLoc);

									var totalNSserialno = '';
									var totalNSserialnoforexpitem = '';
									var vtotNSSerialARR = new Array();
									var vtotNSSerialARRforexpitem = new Array();

									if(vExpItemId == vItemId)
									{
										var filters = new Array();
										filters.push(new nlobjSearchFilter('item',null,'anyof', vItemId));
										filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('inventorynumber');
										columns[1] = new nlobjSearchColumn('internalid').setSort();

										var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

										for (var i1 = 0; searchresultsitem != null && i1 < searchresultsitem.length; i1++) 
										{
											if(totalNSserialno == null || totalNSserialno == '')
												totalNSserialno = searchresultsitem[i1].getValue('inventorynumber');
											else
												totalNSserialno = totalNSserialno + ',' + searchresultsitem[i1].getValue('inventorynumber');

										}

										if(totalNSserialno != null && totalNSserialno  != '')
											vtotNSSerialARR = totalNSserialno.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARR if same item', vtotNSSerialARR);


										if(totalNSserialnoforexpitem == null || totalNSserialnoforexpitem  == '')
											vtotNSSerialARRforexpitem = totalNSserialno.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARRforexpitem  if same item', vtotNSSerialARRforexpitem);
									}
									else
									{
										//Search for expitem total serial numbers from NS
										var filters = new Array();
										filters.push(new nlobjSearchFilter('item',null,'anyof', vExpItemId));
										filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('inventorynumber');
										columns[1] = new nlobjSearchColumn('internalid').setSort();

										var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

										for (var i1 = 0; searchresultsitem != null && i1 < searchresultsitem.length; i1++) 
										{
											if(totalNSserialnoforexpitem == null || totalNSserialnoforexpitem == '')
												totalNSserialnoforexpitem = searchresultsitem[i1].getValue('inventorynumber');
											else
												totalNSserialnoforexpitem = totalNSserialnoforexpitem + ',' + searchresultsitem[i1].getValue('inventorynumber');

										}

										//search for actitem total serial numbers from NS			


										var filters = new Array();
										filters.push(new nlobjSearchFilter('item',null,'anyof', vItemId));
										filters[1] = new nlobjSearchFilter('serialnumberlocation','item','anyof', vWHLoc);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('inventorynumber');
										columns[1] = new nlobjSearchColumn('internalid').setSort();

										var searchresultsitem = nlapiSearchRecord('inventorynumber', null, filters, columns);

										for (var i1 = 0; searchresultsitem != null && i1 < searchresultsitem.length; i1++) 
										{
											if(totalNSserialno == null || totalNSserialno == '')
												totalNSserialno = searchresultsitem[i1].getValue('inventorynumber');
											else
												totalNSserialno = totalNSserialno + ',' + searchresultsitem[i1].getValue('inventorynumber');

										}


										if(totalNSserialnoforexpitem != null && totalNSserialnoforexpitem  != '')
											vtotNSSerialARRforexpitem = totalNSserialnoforexpitem.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARRforexpitem ', vtotNSSerialARRforexpitem);



										if(totalNSserialno != null && totalNSserialno  != '')
											vtotNSSerialARR = totalNSserialno.split(',');

										nlapiLogExecution('ERROR', 'vtotNSSerialARR ', vtotNSSerialARR);

									}



									//	var vMissedSerial = 'S42000000000899,S42000000000900,XTATM265-A06-1211460243';
//									the below is to find whether varianceexpected serails number are there in NS or not if it there then it goes to if cond if not it goes to else
									if(varianceinExpserialNo != null && varianceinExpserialNo != '')
									{
										nlapiLogExecution('ERROR', 'varianceinExpserialNo in side if', varianceinExpserialNo);
										var vMissedExpSerialARR = varianceinExpserialNo.split(',');

										for(k=0;k<vMissedExpSerialARR.length;k++)
										{
											//nlapiLogExecution('ERROR', 'vMissedExpSerialARR ', vMissedExpSerialARR);
											if(vtotNSSerialARRforexpitem.indexOf(vMissedExpSerialARR[k])!=-1)// found temSeriIdArr
											{
												if(expserialnoToNS ==null || expserialnoToNS =='')
													expserialnoToNS = vMissedExpSerialARR[k];
												else
												{
													if(expserialnoToNS.length>3500)
													{
														expserialnoToNS = expserialnoToNS + ',' +vMissedExpSerialARR[k]+"@";
														expserialnoToNS='';
													}
													else
													{
														expserialnoToNS = expserialnoToNS + ',' +vMissedExpSerialARR[k];
													}
												}

											}
											else
											{
												if(expserialnoNotforNS ==null || expserialnoNotforNS =='')
													expserialnoNotforNS = vMissedExpSerialARR[k];
												else
													expserialnoNotforNS = expserialnoNotforNS + ',' +vMissedExpSerialARR[k];
											}
										}
									}
									if(varianceinActserialNoNew != null && varianceinActserialNoNew != '')
									{
										nlapiLogExecution('ERROR', 'varianceinActserialNoNew inside if', varianceinActserialNoNew);	
										var vMissedActSerialARR = varianceinActserialNoNew.split(',');
										nlapiLogExecution('ERROR', 'vMissedActSerialARR ', vMissedActSerialARR);

										for(m=0;m<vMissedActSerialARR.length;m++)
										{
											//if(varianceinActserialNoNew.indexOf(vtotNSSerialARR[k])==-1)// found temSeriIdArr
											if(vtotNSSerialARR.indexOf(vMissedActSerialARR[m])==-1)// found temSeriIdArr
											{
												if(actnewserialnoToNS ==null || actnewserialnoToNS =='')
													actnewserialnoToNS = vMissedActSerialARR[m];
												else
												{
													if(actnewserialnoToNS.length>3500)
													{
														actnewserialnoToNS = actnewserialnoToNS + ',' +vMissedActSerialARR[m]+"@";
														actnewserialnoToNS='';
													}
													else
													{
														actnewserialnoToNS = actnewserialnoToNS + ',' +vMissedActSerialARR[m];
													}
												}
											}
										}
									}
									if(varianceinExpserialNo != null && varianceinExpserialNo != '')
										expvarianceqty =varianceinExpserialNo.split(',').length;

									if(expserialnoToNS != null && expserialnoToNS != '')
										expvarianceqtytoNS =expserialnoToNS.split(',').length;

									if(expserialnoNotforNS != null && expserialnoNotforNS != '')
										expvarianceqtynotforNS =expserialnoNotforNS.split(',').length;


									if(varianceinActserialNo != null && varianceinActserialNo != '')
										actualvarianceqty =varianceinActserialNo.split(',').length;

									if(actnewserialnoToNS != null && actnewserialnoToNS != '')
										actualvarianceqtyNew =actnewserialnoToNS.split(',').length;

									nlapiLogExecution('ERROR', 'expvarianceqty ', expvarianceqty);		
									nlapiLogExecution('ERROR', 'actualvarianceqty ', actualvarianceqty);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);	
									nlapiLogExecution('ERROR', 'varianceinExpserialNo ', varianceinExpserialNo);	
									nlapiLogExecution('ERROR', 'expvarianceqtynotforNS ', expvarianceqtynotforNS);	
									nlapiLogExecution('ERROR', 'varianceinActserialNo ', varianceinActserialNo);	
									nlapiLogExecution('ERROR', 'actualvarianceqtyNew ', actualvarianceqtyNew);	
									nlapiLogExecution('ERROR', 'expserialnoToNS ', expserialnoToNS);	
									nlapiLogExecution('ERROR', 'expserialnoNotforNS ', expserialnoNotforNS);	
									nlapiLogExecution('ERROR', 'actnewserialnoToNS ', actnewserialnoToNS);	


								}
								nlapiLogExecution('ERROR', 'vcycActSKU ', vcycActSKU);
								palletQuantity = fetchPalletQuantity(vcycActSKU,sitelocn,null);
								nlapiLogExecution('ERROR','palletQuantity',palletQuantity);
								nlapiLogExecution('ERROR','TotQty',TotQty);

								var LotBatchId='';
								var oldLotBatchId='';

								var LotBatchText = CyccRecord.getFieldValue('custrecord_cycleabatch_no');								
								var oldLotBatchText = CyccRecord.getFieldValue('custrecord_expcycleabatch_no');
								var vtempactualItem = CyccRecord.getFieldValue('custrecord_cycleact_sku');
								var vtempexpectedItem = CyccRecord.getFieldValue('custrecord_cyclesku');



								if(LotBatchText!=null && LotBatchText!='')
									LotBatchId=getLotBatchId(LotBatchText,vtempactualItem);

								if(oldLotBatchText!=null && oldLotBatchText!='')
									oldLotBatchId=getLotBatchId(oldLotBatchText,vtempexpectedItem);

								nlapiLogExecution('ERROR', 'LotBatchText tst', LotBatchText);	
								nlapiLogExecution('ERROR', 'LotBatchId new tst', LotBatchId);


								nlapiLogExecution('ERROR', 'oldLotBatchText tst', oldLotBatchText);	
								nlapiLogExecution('ERROR', 'oldLotBatchId new tst', oldLotBatchId);

								var itmstatus = CyccRecord.getFieldValue('custrecord_cyclesku_status');
								var ItemNewStatus = CyccRecord.getFieldValue('custrecord_expcyclesku_status');						



								nlapiLogExecution('ERROR','itmstatus',itmstatus);
								nlapiLogExecution('ERROR','ItemNewStatus',ItemNewStatus);

								var CallFlag='N';
								var OldMakeWHFlag='Y';
								var NewMakeWHFlag='Y';
								var vNewWHlocatin = "";
								var vOldWHlocatin = "";
								var NewWhLocation = "";
								var OldWhLocation = "";
								var vNSFlag;
								var UpdateFlag;
								var vNSCallFlag='N';
								var qtyonhand;
								var accountno;
								var sitelocn;
								var memo;
								if(itmstatus==ItemNewStatus)
									CallFlag='N';
								else
								{

									vNSFlag=GetNSCallFlag(itmstatus,ItemNewStatus);
									nlapiLogExecution('ERROR', 'vNSFlag',vNSFlag);

									if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)
									{
										CallFlag=vNSFlag[0];
										nlapiLogExecution('ERROR', 'vNSFlag[0]',vNSFlag[0]);
									}
									if(vNSFlag[0]=='N')
										CallFlag='N';
									else if(vNSFlag[0]=='Y') 
									{ 
										OldMakeWHFlag= GetMWFlag(vNSFlag[1]);
										NewMakeWHFlag= GetMWFlag(vNSFlag[2]);
										vNewWHlocatin=vNSFlag[2]; 
										vOldWHlocatin=vNSFlag[1]; 
										OldWhLocation=vNSFlag[3]; 
										NewWhLocation=vNSFlag[4]; 
									}
								}

								nlapiLogExecution('ERROR','CallFlag',CallFlag);
								nlapiLogExecution('ERROR','OldMakeWHFlag',OldMakeWHFlag);
								nlapiLogExecution('ERROR','NewMakeWHFlag',NewMakeWHFlag);
								nlapiLogExecution('ERROR','vNewWHlocatin',vNewWHlocatin);
								nlapiLogExecution('ERROR','vOldWHlocatin',vOldWHlocatin);

								var scount=1;

								LABL1: for(var p=0;p<scount;p++)
								{	

									nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', p);
									try
									{
										var invtid = CyccRecord.getFieldValue('custrecord_invtid');
										var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtid);

										transaction.setFieldValue('custrecord_ebiz_inv_packcode', '1');
										var totalQty = CyccRecord.getFieldValue('custrecord_cycle_act_qty');


										//var qtyonhand = transaction.getFieldValue('custrecord_ebiz_qoh');
										/***Below code is merged from Lexjet Production on 4th March 2013 by Ganesh K***/
										if((parseFloat(palletQuantity) != 0) && (parseFloat(TotQty) > parseFloat(palletQuantity)))
										{
											nlapiLogExecution('ERROR', 'TotQty old',TotQty);
											nlapiLogExecution('ERROR', 'palletQuantity old',palletQuantity);
											totalQty = palletQuantity;
										}
										transaction.setFieldValue('custrecord_ebiz_inv_qty',parseFloat(totalQty).toFixed(5));
										nlapiLogExecution('ERROR', 'totalQty ', totalQty);
										/***Upto here***/
										qtyonhand = transaction.getFieldValue('custrecord_ebiz_qoh');
										accountno = transaction.getFieldValue('custrecord_ebiz_inv_account_no');
										sitelocn = transaction.getFieldValue('custrecord_ebiz_inv_loc');
										memo = transaction.getFieldValue('custrecord_ebiz_inv_note1');
										var allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
										nlapiLogExecution('ERROR', 'totalQty', totalQty);	
										nlapiLogExecution('ERROR', 'allocQty', allocQty);
										// Case # 20124813 Start
										if(allocQty==null || allocQty=='' || allocQty=='null')
										{ 
											allocQty=0;
										}
										// Case # 20124813 END
										/* code added from FactoryMation on 28Feb13 by santosh, DecimalConversion */
										var availqty = parseFloat(totalQty) - parseFloat(allocQty);

										nlapiLogExecution('ERROR', 'availqty', availqty);
										nlapiLogExecution('ERROR', 'qtyonhand', qtyonhand);
										nlapiLogExecution('ERROR', 'qtytobe adjust', LineLevelEnteredValue2[1]);
										// case # 20126953 starts
										//var totalonhandQty = parseFloat(qtyonhand) + parseFloat(request.getLineItemValue('custpage_items', 'custpage_qtytobeadjust', i + 1));
										var totalonhandQty =  parseFloat(LineLevelEnteredValue2[1]);

										var actlp = CyccRecord.getFieldValue('custrecord_cycact_lpno');
										var actsku = CyccRecord.getFieldValue('custrecord_cycleact_sku');
										var actskustatus = CyccRecord.getFieldValue('custrecord_cyclesku_status');

										// case 20126953 ends
										nlapiLogExecution('ERROR', 'totalonhandQty', totalonhandQty);

										if(parseFloat(totalonhandQty) <= 0)
											totalonhandQty=0;
										//else
										//	totalonhandQty=totalQty;
										nlapiLogExecution('ERROR', 'Math.round(totalonhandQty)', Math.round(totalonhandQty));
										//transaction.setFieldValue('custrecord_ebiz_qoh', Math.round(totalQty));
										//transaction.setFieldValue('custrecord_ebiz_qoh', Math.round(totalonhandQty));

										//case # 20127037 (,resolve qty(record actual qty) is taken and set into allocated qty of create inventory table)
										//transaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totalonhandQty).toFixed(5));
										transaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totalQty).toFixed(5));
										transaction.setFieldValue('custrecord_ebiz_avl_qty', parseFloat(availqty).toFixed(5));
										//case # 20127037 ends
										if((actlp != null && actlp != ""))
										{
											LP = actlp;
											transaction.setFieldValue('custrecord_ebiz_inv_lp', LP);
										}									
										if(actsku != null && actsku != "")
											transaction.setFieldValue('custrecord_ebiz_inv_sku', actsku);
										//custpage_actskustatus
										if(actskustatus != null && actskustatus != "")
											transaction.setFieldValue('custrecord_ebiz_inv_sku_status', actskustatus);
										transaction.setFieldValue('custrecord_ebiz_invholdflg', 'F');
										transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', 'F');
										transaction.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);
										transaction.setFieldValue('custrecord_ebiz_inv_updateddate', DateStamp());
										transaction.setFieldValue('custrecord_ebiz_inv_updatedtime', TimeStamp());
										transaction.setFieldValue('custrecord_wms_inv_status_flag', '19');//Storage
										transaction.setFieldValue('custrecord_invttasktype', '7');//Storage
										transaction.setFieldValue('custrecord_ebiz_callinv', 'N');

										// Case# 20127753 starts
										transaction.setFieldValue('custrecord_ebiz_itemdesc', CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc'));
										// Case# 20127753 end

										if(LotBatchId!=null&&LotBatchId!="")
										{
											transaction.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);	
											var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatchId,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
											var fifodate=rec.custrecord_ebizfifodate;
											var expdate=rec.custrecord_ebizexpirydate;
											if(fifodate!=null && fifodate!='')
												transaction.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
											/*else
												transaction.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
											transaction.setFieldValue('custrecord_ebiz_expdate', expdate);
										}
										//vNewWHlocatin
										if(NewWhLocation != null && NewWhLocation != "")
										{
											if(NewWhLocation != sitelocn)
											{
												transaction.setFieldValue('custrecord_ebiz_inv_loc', NewWhLocation);
											}
										}
										var id = nlapiSubmitRecord(transaction, true);

										try
										{
											//case# 20127131 starts (totalonhandQty is replaced with totalQty)
											nlapiLogExecution('ERROR', "allocQty/totalQty",allocQty+'/'+totalQty);
											if(parseInt(allocQty)== 0&&parseInt(totalQty)==0&&id!=null&&id!="")
											{
												//case# 20127131 end
												nlapiLogExecution('ERROR', ' in Delete Inventory Record ', id);	
												var delid = nlapiDeleteRecord('customrecord_ebiznet_createinv',id);				
											}
										}
										catch (exp) 
										{
											nlapiLogExecution('ERROR', 'Exception in Delete Inventory Record ', exp);	
										}


									}
									catch(ex)
									{
										var exCode='CUSTOM_RECORD_COLLISION'; 
										var wmsE='Inventory record being updated by another user. Please try again...';
										if (ex instanceof nlobjError) 
										{	
											wmsE=ex.getCode() + '\n' + ex.getDetails();
											exCode=ex.getCode();
										}
										else
										{
											wmsE=ex.toString();
											exCode=ex.toString();
										} 

										nlapiLogExecution('ERROR', 'Exception in Cycle count Generate release : ', wmsE); 
										if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
										{ 
											scount=scount+1;
											continue LABL1;
										}
										else break LABL1;
									}
								}
								var actqty1 = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
								var expqty1 = CyccRecord.getFieldValue('custrecord_cycleexp_qty');


								if(parseFloat(palletQuantity) != 0 && (parseFloat(TotQty) > parseFloat(palletQuantity)))
								{
									actqty1 = palletQuantity;
								}

								nlapiLogExecution('ERROR', 'expqty1', expqty1);	
								nlapiLogExecution('ERROR', 'actqty1', actqty1);	
								resultQty1 = parseFloat(actqty1) - parseFloat(expqty1);
								nlapiLogExecution('ERROR', 'resultQty1', resultQty1);	

								var ItemId = CyccRecord.getFieldValue('custrecord_cycleact_sku');
								var expItemId = CyccRecord.getFieldValue('custrecord_cyclesku');

								//var itmstatus=null;
								var ItemNewStatus=null;

								var itmstatus = CyccRecord.getFieldValue('custrecord_expcyclesku_status');

								var FromAccNo=AccountNo[0];
								var ToAccNo=null;

								nlapiLogExecution('ERROR', 'Exp. Item ', expItemId);	
								nlapiLogExecution('ERROR', 'Act. Item ', ItemId);	

								var notes="This is from Cycle count";

								var fields = ['recordType', 'custitem_ebizserialin'];
								var columns = nlapiLookupField('item', ItemId, fields);
								var ItemType = columns.recordType;
								nlapiLogExecution('ERROR','ItemType',ItemType);

								var serialInflg="F";		
								serialInflg = columns.custitem_ebizserialin;

								var serialnumbers = "";
								var localSerialNoArray = new Array();
								if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg =="T") 
								{
									var adjqty = 0;
									adjqty = parseFloat(resultQty1);
									if(parseFloat(actqty1) == 0)
									{
										adjqty = 0;
									}
//									here
									AdjustSerialNumbers(localSerialNoArray,parseFloat(adjqty),ItemId,LP,restype,CyccRecID,varianceinExpserialNo);

								}							
								var expfields = ['recordType', 'custitem_ebizserialin'];
								var expcolumns = nlapiLookupField('item', expItemId, expfields);
								var expItemType = expcolumns.recordType;
								nlapiLogExecution('ERROR','expItemType',expItemType);

								var expserialInflg="F";		
								expserialInflg = expcolumns.custitem_ebizserialin;



								if (expItemType == "serializedinventoryitem" || expItemType == "serializedassemblyitem" || expserialInflg == "T") 
								{
									//if(localSerialNoArray != null && localSerialNoArray != "")
									//{
									//nlapiLogExecution('ERROR','localSerialNoArray',localSerialNoArray.ToString());
									//if(localSerialNoArray.length != 0)
									//{
									//nlapiLogExecution('ERROR','localSerialNoArray.length',localSerialNoArray.length);
									//serialnumbers = getSerialNoCSV(localSerialNoArray,LP);
									serialnumbers = getSerialNoCSVvalues(ItemId,LP);
									nlapiLogExecution('ERROR','serialnumbers',serialnumbers);
									LotBatchText = serialnumbers;
									//}
									//}
								}

								var confirmhost=LineLevelEnteredValue2[2];
								nlapiLogExecution('ERROR','host',confirmhost);
								if(confirmhost=='T'  )
								{
									try
									{
										if(expItemId==ItemId && parseFloat(resultQty1)!=0 && oldLotBatchId==LotBatchId && ((varianceinExpserialNo == null || varianceinExpserialNo == "" || varianceinExpserialNo == 0) && (varianceinActserialNoNew == null || varianceinActserialNoNew == '' || varianceinActserialNoNew ==0)) && ((expserialnoToNS == null || expserialnoToNS == "") && (actnewserialnoToNS == null || actnewserialnoToNS == '')))
										{
											nlapiLogExecution('ERROR','test1','test1');
											NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(resultQty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
										}
										else if(expItemId==ItemId && ((expserialnoToNS != null && expserialnoToNS != "") || (actnewserialnoToNS != null && actnewserialnoToNS != '')))
										{
											nlapiLogExecution('ERROR','expserialnoToNS before NS inv',expserialnoToNS);
											//commented the below line becuase of doubt related to qty sent and no.of serialno sent
											//NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expvarianceqty),notes,vTaskType,vAdjusttype,expserialnoToNS,FromAccNo);
											NSAdjustid1 = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expvarianceqtytoNS),notes,vTaskType,vAdjusttype,expserialnoToNS,FromAccNo);
											if(parseFloat(actualvarianceqtyNew)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actualvarianceqtyNew),notes,vTaskType,vAdjusttype,actnewserialnoToNS,FromAccNo);
											}
										}
										else if(expItemId!=ItemId && ((expserialnoToNS != null && expserialnoToNS != "") || (actnewserialnoToNS != null && actnewserialnoToNS != '')))
										{
											nlapiLogExecution('ERROR','expserialnoToNS before NS inv if itemid is not equal',expserialnoToNS);

											if(actnewserialnoToNS != null && actnewserialnoToNS != '')
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actualvarianceqtyNew),notes,vTaskType,vAdjusttype,actnewserialnoToNS,FromAccNo);//positive
											if ((expItemType == "serializedinventoryitem" || expItemType == "serializedassemblyitem" || expserialInflg =="T")  && expserialnoToNS != null && expserialnoToNS != "") 
												NSAdjustid1 = InvokeNSInventoryAdjustmentNew(expItemId,itmstatus,vWHLoc,- parseFloat(expvarianceqtytoNS),notes,vTaskType,vAdjusttype,expserialnoToNS,FromAccNo);//negative
											else
												NSAdjustid1 = InvokeNSInventoryAdjustmentNew(expItemId,itmstatus,vWHLoc,- parseFloat(expqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);//negative
										}
										else if(expItemId!=ItemId && ((varianceinExpserialNo == null || varianceinExpserialNo == "") && (varianceinActserialNoNew == null || varianceinActserialNoNew == '')))
										{
											if(parseFloat(actqty1)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);//positive
											}
											NSAdjustid1 = InvokeNSInventoryAdjustmentNew(expItemId,itmstatus,vWHLoc,- parseFloat(expqty1),notes,vTaskType,vAdjusttype,oldLotBatchText,FromAccNo);//negative
										}
										else if(oldLotBatchId!=LotBatchId && parseFloat(resultQty1)==0)
										{
											nlapiLogExecution('ERROR', 'parseFloat(resultQty1) 2 ', parseFloat(resultQty1));
											if(parseFloat(actqty1)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
											}
											NSAdjustid1 = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expqty1),notes,vTaskType,vAdjusttype,oldLotBatchText,FromAccNo);
										}
										else if(oldLotBatchId!=LotBatchId && parseFloat(resultQty1)!=0)
										{
											nlapiLogExecution('ERROR', 'parseFloat(actqty1) 1 ', parseFloat(actqty1));
											if(parseFloat(actqty1)>0)
											{
												NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
											}
											NSAdjustid1 = InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,-parseFloat(expqty1),notes,vTaskType,vAdjusttype,oldLotBatchText,FromAccNo);
										}
									}
									catch(exp)
									{
										nlapiLogExecution('ERROR','Exception in InvokeNSInventoryAdjustmentNew',exp);
									}
								}
								if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
								{
									var tempserial = actserialno.split(',');
									nlapiLogExecution('ERROR', 'tempserial 1 ', tempserial);
									nlapiLogExecution('ERROR', 'tempserial.length 1 ', tempserial.length);
									var serial='';
									for(var i=0;i<tempserial.length;i++)
									{
										if(serial == null || serial =='' )
											serial = tempserial[i] + "(" + actqty1 + ")";
										else
											serial = serial + " " + tempserial[i] + "(" + actqty1 + ")";

										nlapiLogExecution('ERROR', 'serial 1 ', serial);
									}

									LotBatchText = serial;
								}
								nlapiLogExecution('ERROR', 'LotBatchText 1 ', LotBatchText);
								if(CallFlag=='Y')//To trigger NS automatically
								{
									try
									{
										InvokeNSInventoryTransfer(ItemId,ItemNewStatus,vOldWHlocatin,vNewWHlocatin,actqty1,LotBatchText);
										var actqty = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
										var expqty = CyccRecord.getFieldValue('custrecord_cycleexp_qty');

										var QtyVariance = (parseFloat(actqty) - parseFloat(expqty)).toFixed(2)
										//case 20126530 start

										nlapiLogExecution('ERROR', 'QtyVariance',QtyVariance);
										if(expItemId==ItemId && parseInt(QtyVariance)!=0)
										{
											nlapiLogExecution('ERROR', 'into QtyVariance',QtyVariance);
											InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vWHLoc,parseInt(QtyVariance),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
										}
										//case 20126530 end

									}
									catch(exp)
									{
										nlapiLogExecution('ERROR','Exception in InvokeNSInventoryTransfer',exp);
									}


									/* This part of code is commented as per inputs from NS team
									 * For all vitual location inventory move NS team suggested us to us Inventory Transfer object instead of inventory adjustment object
									 * Averate cost/price of the item will not change if we use Inventory Transfer object

									//Code for fetching account no from stockadjustment custom record	
									var AccountNo = getStockAdjustmentAccountNoNew(InvAdjType);

									if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
									{
										var FromAccNo=AccountNo[0];
										var ToAccNo=AccountNo[1];
										nlapiLogExecution('ERROR', 'vOldWHlocatin',vOldWHlocatin);
										nlapiLogExecution('ERROR', 'vNewWHlocatin',vNewWHlocatin);

										nlapiLogExecution('ERROR', 'FromAccNo',FromAccNo);
										nlapiLogExecution('ERROR', 'ToAccNo',ToAccNo);
										nlapiLogExecution('ERROR', 'LotBatchText',LotBatchText);
										InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vOldWHlocatin,-(parseFloat(ItemNewQty)),"",vTaskType,InvAdjType,LotBatchText,FromAccNo);
										InvokeNSInventoryAdjustmentNew(ItemId,ItemNewStatus,vNewWHlocatin,ItemNewQty,"",vTaskType,InvAdjType,LotBatchText,ToAccNo);

									}
									 */ 
								}


								if((parseFloat(palletQuantity) != 0) && (parseFloat(TotQty) > parseFloat(palletQuantity)))
								{
									nlapiLogExecution('ERROR','Creating new record as qty is more than pallet qty',TotQty);
									var LotBatchId='';

									var LotBatchText = CyccRecord.getFieldValue('custrecord_cycleabatch_no');

									var vtempactualItem = CyccRecord.getFieldValue('custrecord_cycleact_sku');
									var vtempexpectedItem = CyccRecord.getFieldValue('custrecord_cyclesku');

									if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText,vtempactualItem);

									/*if(LotBatchText!=null && LotBatchText!='')
										LotBatchId=getLotBatchId(LotBatchText);*/

									var ItemStatus = "";
									if(CycleCountSearchResult[0].getValue('custrecord_cyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_cyclesku_status').toString();
									}
									else if(CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status')!= null && CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status') != "")
									{
										ItemStatus = CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status').toString();
									}
									else
									{
										var vRoleLocation=getRoledBasedLocation();
										nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
										var itemStatusFilters = new Array();
										itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
										itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');										
										itemStatusFilters[2] = new nlobjSearchFilter('custrecord_defaultskustaus',null, 'is','T');
										if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
										{
											//Case # 20126438 Start
											itemStatusFilters[3] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',vRoleLocation);
											//Case # 20126438 End
										}

										var itemStatusColumns = new Array();			
										itemStatusColumns[0] = new nlobjSearchColumn('name');

										var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);			
										if(itemStatusSearchResult != null && itemStatusSearchResult != '')
										{
											ItemStatus = itemStatusSearchResult[0].getId();
										}
										nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
									}

									var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
									var columns = nlapiLookupField('item', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'), fields);
									//nlapiLogExecution('ERROR', 'Time Stamp at the end of Item Lookup',TimeStampinSec());
									var ItemType = columns.recordType;	
									var serialInflg="F";		
									serialInflg = columns.custitem_ebizserialin;
									var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
									nlapiLogExecution('ERROR', 'Creating INVT Record ', 'INVT');
									if(Cycexeplanid!= null && Cycexeplanid != "")
										invtRec.setFieldValue('name', Cycexeplanid);
									nlapiLogExecution('ERROR', 'Cycexeplanid',Cycexeplanid );
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_binloc', CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc'));
									nlapiLogExecution('ERROR', 'actbeginloc',CycleCountSearchResult[0].getValue('custrecord_cycact_beg_loc') );
									var getItemLP = GetMaxLPNo(1, 1,CycleCountSearchResult[0].getValue('custrecord_cyclesite_id').toString());
									invtRec.setFieldValue('custrecord_ebiz_inv_lp', getItemLP);
									nlapiLogExecution('ERROR', 'invlp', CycleCountSearchResult[0].getValue('custrecord_cycact_lpno'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									nlapiLogExecution('ERROR', 'sku_no',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatus);
									nlapiLogExecution('ERROR', 'sku_status',CycleCountSearchResult[0].getValue('custrecord_expcyclesku_status'));
									if(CycleCountSearchResult[0].getValue('custrecord_cyclepc')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclepc') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_packcode', CycleCountSearchResult[0].getValue('custrecord_cyclepc'));
									nlapiLogExecution('ERROR', 'packcode',CycleCountSearchResult[0].getValue('custrecord_cyclepc') );
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty') != "")
									{
										nlapiLogExecution('ERROR', 'TotQty new',TotQty);
										nlapiLogExecution('ERROR', 'palletQuantity new',palletQuantity);
										//var qty=parseFloat(CycleCountSearchResult[0].getValue('custrecord_cycle_act_qty'));
										var qty = parseFloat(TotQty) - parseFloat(palletQuantity);
										invtRec.setFieldValue('custrecord_ebiz_qoh', qty);
										invtRec.setFieldValue('custrecord_ebiz_inv_qty', qty.toString());
										nlapiLogExecution('ERROR', 'actqty',qty);
									}
									if(CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') != "")
										invtRec.setFieldValue('custrecord_inv_ebizsku_no', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no'));
									//
									nlapiLogExecution('ERROR', 'sku',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
									// Case# 20127753 starts
									if(CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc')!= null && CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') != "")
										// Case# 20127753 ends	
										invtRec.setFieldValue('custrecord_ebiz_itemdesc', CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc'));
									nlapiLogExecution('ERROR', 'itemdesc',CycleCountSearchResult[0].getValue('custrecord_cycle_skudesc') );
									invtRec.setFieldValue('custrecord_invttasktype', 2);
									invtRec.setFieldValue('custrecord_wms_inv_status_flag', ['19']); //Inventory Storage
									if(CycleCountSearchResult[0].getValue('custrecord_cyclesite_id')!= null && CycleCountSearchResult[0].getValue('custrecord_cyclesite_id') != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_loc',CycleCountSearchResult[0].getValue('custrecord_cyclesite_id').toString());
									invtRec.setFieldValue('custrecord_ebiz_callinv','N');
									invtRec.setFieldValue('custrecord_ebiz_displayfield','N');
									if(LotBatchId!=null&&LotBatchId!="")
									{
										invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatchId);	
										var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatchId,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
										var fifodate=rec.custrecord_ebizfifodate;
										var expdate=rec.custrecord_ebizexpirydate;
										if(fifodate!=null && fifodate!='')
											invtRec.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
										/*else
											invtRec.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
										invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
									}
									else
									{
										if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
										{ 										
											var itemid=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');
											nlapiLogExecution('ERROR', 'itemid', itemid);
											var Batchfilters = new Array();
											Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',itemid ));
											var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, Batchfilters);
											if(Batchsearchresults!=null)
											{
												var	getlotnoid= Batchsearchresults[0].getId();
												nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
												invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
											}	
										}
									}
									//}
									var LP = "";
									if(CycleCountSearchResult[0].getValue('custrecord_cycact_lpno')!= null && CycleCountSearchResult[0].getValue('custrecord_cycact_lpno') != "")
									{
										LP = CycleCountSearchResult[0].getValue('custrecord_cycact_lpno');
									}									

									var serialnumbers = "";
									var localSerialNoArray = new Array();
									if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
									{
										//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(actqty1),sku,LP,restype,CyccRecID,'');
										AdjustSerialNumbers(localSerialNoArray,parseFloat(actqty1),sku,LP,restype,CyccRecID,'');
									}

									if(AccountNo!= null && AccountNo != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_account_no', AccountNo[0]);	

									try{
										nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORD');
										var invtrecid = nlapiSubmitRecord(invtRec, false, true);
										nlapiLogExecution('ERROR', 'After Submitting invtrecid',invtrecid );
									}
									catch (e)
									{
										if ( e instanceof nlobjError )
											nlapiLogExecution( 'ERROR', 'system error', e.getCode() + '\n' + e.getDetails() );
										else
											nlapiLogExecution( 'ERROR', 'unexpected error', e.getCode() + '\n' + e.getDetails() );
									}
									var confirmhost=LineLevelEnteredValue2[2];
									nlapiLogExecution('ERROR','host',confirmhost);
									if(confirmhost =='T')
									{
										var LotBatchText='';
										LotBatchText = CyccRecord.getFieldValue('custrecord_cycleabatch_no');
										if(LotBatchText == "" || LotBatchText == null)
										{
											nlapiLogExecution( 'ERROR', 'skuno', CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') );
											var Batchfilters = new Array();
											Batchfilters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no') ));
											var Batchcolumns=new Array();
											Batchcolumns[0]=new nlobjSearchColumn('name');
											var Batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry',null, Batchfilters, Batchcolumns);

											nlapiLogExecution( 'ERROR', 'Batchsearchresults', Batchsearchresults );
											if(Batchsearchresults!=null)
											{
												LotBatchText= Batchsearchresults[0].getValue('name');
												nlapiLogExecution( 'ERROR', 'LotBatchText', LotBatchText );
											}
										}
										var itmstatus=null;
										var actqty1 = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
										var FromAccNo=AccountNo[0];
										var ItemId=CycleCountSearchResult[0].getValue('custrecord_cyclecount_act_ebiz_sku_no');										

										if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
										{
											if(localSerialNoArray != null && localSerialNoArray != "")
											{
												//nlapiLogExecution('ERROR','localSerialNoArray',localSerialNoArray.ToString());
												if(localSerialNoArray.length != 0)
												{
													nlapiLogExecution('ERROR','localSerialNoArray.length',localSerialNoArray.length);
													serialnumbers = getSerialNoCSV(localSerialNoArray);
													nlapiLogExecution('ERROR','serialnumbers',serialnumbers);
													LotBatchText = serialnumbers;
												}
											}
										}
										//InvokeNSInventoryAdjustment(ItemId,itmstatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText);
										try
										{
											NSAdjustid = InvokeNSInventoryAdjustmentNew(ItemId,ItemStatus,vWHLoc,parseFloat(actqty1),notes,vTaskType,vAdjusttype,LotBatchText,FromAccNo);
										}
										catch(exp)
										{
											nlapiLogExecution('ERROR','Exception in InvokeNSInventoryAdjustmentNew',exp);
										}
									}
								}
							}
						}
						else
						{
							//To throw error message to configure stock adjustment type
							nlapiLogExecution('ERROR',"Stock adjustment type is not configured for Task type Cycle Count");
						}


						var binloc = CyccRecord.getFieldValue('custrecord_cycact_beg_loc');
						nlapiLogExecution('ERROR',"binloc",binloc);
						var skuno = CyccRecord.getFieldValue('custrecord_cyclesku');
						var expqty1 = CyccRecord.getFieldValue('custrecord_cycle_act_qty');						
						var explp = CyccRecord.getFieldValue('custrecord_cyclelpno');					


						var accoutno='';
						if(AccountNo!=null && AccountNo!='')
						{
							accoutno = AccountNo[1];
						}



						var actqty = CyccRecord.getFieldValue('custrecord_cycle_act_qty');						
						var expqty = CyccRecord.getFieldValue('custrecord_cycleexp_qty');		

						if(actqty=='' || actqty==null || isNaN(actqty))
							actqty=0;

						if(expqty=='' || expqty==null || isNaN(expqty))
							expqty=0;



						var actItem = CyccRecord.getFieldValue('custrecord_cycleact_sku');
						var expItem = CyccRecord.getFieldValue('custrecord_cyclesku');

						var adjustqty = parseFloat(actqty) - parseFloat(expqty);

						if(resultQty1=='' || resultQty1==null || isNaN(resultQty1))
							resultQty1=0;

						var arrdims = new Array();
						if(vEmptyBinLoc=='T')
						{
							//createInvtAdjtRecord(binloc,actItem,0,explp,accoutno,sitelocn,actqty,actnewserialnoToNS,NSAdjustid);
							arrdims = getSKUCubeAndWeight(actItem, 1);
						}
						else
						{/*
							if(expItemId==ItemId && ((varianceinExpserialNo == null || varianceinExpserialNo == "") && (varianceinActserialNoNew == null || varianceinActserialNoNew == '')))
							{
								createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,adjustqty,serialnumbers,NSAdjustid);
							}
							else if(expItemId==ItemId && ((varianceinExpserialNo != null && varianceinExpserialNo != "") || (actnewserialnoToNS != null && actnewserialnoToNS != '')))
							{
								if(expserialnoToNS != null && expserialnoToNS != '')
									createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,-expvarianceqtytoNS,expserialnoToNS,NSAdjustid1);
								//createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,-expvarianceqty,expserialnoToNS);
								if(expserialnoNotforNS != null && expserialnoNotforNS != '')
									createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,-expvarianceqtynotforNS,expserialnoNotforNS,'');
								if(actnewserialnoToNS != null && actnewserialnoToNS != '')
									createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,sitelocn,actualvarianceqtyNew,actnewserialnoToNS,NSAdjustid);
							}
							else//have to check this cond when items are diff for a serial item						
							{
								createInvtAdjtRecord(binloc,expItem,expqty,explp,accoutno,sitelocn,-expqty,serialnumbers,NSAdjustid1);
								createInvtAdjtRecord(binloc,actItem,0,explp,accoutno,sitelocn,actqty,serialnumbers,NSAdjustid);
							}
							arrdims = getSKUCubeAndWeight(request.getLineItemValue('custpage_items', 'custpage_expsku', i + 1), 1);
						 */
							//  Case 20125356ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½Start Loc Remaining cube update
							arrdims = getSKUCubeAndWeight(expItemId, 1);
							//end	
						}

						if(resultQty1=='' || resultQty1==null || isNaN(resultQty1))
							resultQty1=0;

						nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:length11', arrdims.length);

						var itemCube = 0;
						var uomqty = 0;
						//if (arrdims["BaseUOMItemCube"] != "" && (!isNaN(arrdims["BaseUOMItemCube"]))) 
						/*	if (arrdims[0] != "" && (!isNaN(arrdims[0])))
						{
							nlapiLogExecution('ERROR','resultQty1', parseFloat(resultQty1));
							nlapiLogExecution('ERROR','arrdims[1]', parseFloat(arrdims[1]));
							if(parseFloat(resultQty1)>0)
							{
								uomqty = ((parseFloat(resultQty1))/(parseFloat(arrdims[1])));
							}
							else
							{
								uomqty = ((parseFloat(actqty1))/(parseFloat(arrdims[1])));
							}
							nlapiLogExecution('ERROR','uomqty',uomqty);
							itemCube = (parseFloat(uomqty) * parseFloat(arrdims[0]));
							nlapiLogExecution('ERROR','itemCubeinif', itemCube);

						} 
						else 
						{	
							nlapiLogExecution('ERROR','test1else', 'test1else');						
							itemCube = 0;
						}*/

						/*LocRemCube = GeteLocCube(binloc);
						var oldLocRemCube =0;	
						//  Case 20125356ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½Start Loc Remaining cube update 
						if(parseInt(resultQty1)>0)
						{
							oldremcube = (parseFloat(LocRemCube) - parseFloat(itemCube));
						}
						else
						{
							oldremcube = (parseFloat(LocRemCube) + parseFloat(itemCube));
						}*/
						//end
						//var oldremcube = (parseFloat(LocRemCube) + parseFloat(itemCube));			
						//	oldLocRemCube = parseFloat(oldremcube);
						//nlapiLogExecution('ERROR','itemCube', itemCube);
						//	nlapiLogExecution('ERROR','LocRemCube', LocRemCube);
						//	nlapiLogExecution('ERROR','LocRemCubeafteradding', oldLocRemCube);
						//var retValue = UpdateLocCubeNew(request.getLineItemValue('custpage_items', 'custpage_location', i + 1), oldLocRemCube);

						if (restype == 'close') {
							var planno = CyccRecord.getFieldValue('custrecord_cycle_count_plan');
							nlapiSubmitField('customrecord_ebiznet_cylc_createplan', planno,'custrecord_cyccplan_close', 'T');							
						}
					}
					catch (e) {

						if (e instanceof nlobjError) 
						{
							nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
						}

						else 
						{
							nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
						nlapiLogExecution('ERROR','Save unsuccessful into Create Inventory' + '\n' + 'Error: ' + e.toString());
						nlapiLogExecution('ERROR','unexpected error',  e);
					}

				} 
				else if(restype == 'ignore')
				{
					try
					{						
						var ItemId = CyccRecord.getFieldValue('custrecord_cycleact_sku');

						var fields = ['recordType', 'custitem_ebizserialin'];
						var columns = nlapiLookupField('item', ItemId, fields);
						var ItemType = columns.recordType;
						nlapiLogExecution('ERROR','ItemType',ItemType);

						var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;	

						var serialnumbers = "";
						var localSerialNoArray = new Array();
						if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
						{

							var LP = CyccRecord.getFieldValue('custrecord_cycact_lpno');
							var actqty1 = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
							var expqty1  = CyccRecord.getFieldValue('custrecord_cycleexp_qty');

							nlapiLogExecution('ERROR', 'expqty1', expqty1);	
							nlapiLogExecution('ERROR', 'actqty1', actqty1);	
							var resultQty1 = parseFloat(actqty1) - parseFloat(expqty1);
							nlapiLogExecution('ERROR', 'resultQty1', resultQty1);	

							//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,restype,CyccRecID,'');
							AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,restype,CyccRecID,'');
						}

						var scount=1;
						LABL1: for(var j=0;j<scount;j++)
						{	
							nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', j);
							try
							{
								var createinvid = CyccRecord.getFieldValue('custrecord_invtid');
								var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', createinvid);
								transaction.setFieldValue('custrecord_ebiz_invholdflg', 'F');
								transaction.setFieldValue('custrecord_ebiz_cycl_count_hldflag', 'F');
								transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
								var id = nlapiSubmitRecord(transaction, true);	

							}
							catch(ex)
							{
								var exCode='CUSTOM_RECORD_COLLISION'; 

								if (ex instanceof nlobjError) 
								{	
									wmsE=ex.getCode() + '\n' + ex.getDetails();
									exCode=ex.getCode();
								}
								else
								{
									wmsE=ex.toString();
									exCode=ex.toString();
								}				 
								if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
								{ 
									scount=scount+1;
									continue LABL1;
								}
								else break LABL1;
							}
						}

					}
					catch (e) {

						if (e instanceof nlobjError) 
						{
							nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
						}

						else 
						{
							nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
						nlapiLogExecution('ERROR','Save unsuccessful into Create Inventory when ignore status is choosen' + '\n' + 'Error: ' + e.toString());
						nlapiLogExecution('ERROR','Save unsuccessful into Create Inventory when ignore status is choosen' ,  e);
					}
				}
				else if(restype == 'recount')
				{
					nlapiLogExecution('ERROR', '*************** Into recount', restype);
					try
					{						


						var ItemId = CyccRecord.getFieldValue('custrecord_cycleact_sku');

						var fields = ['recordType','custitem_ebizserialin'];
						var columns = nlapiLookupField('item', ItemId, fields);
						var ItemType = columns.recordType;
						nlapiLogExecution('ERROR','ItemType',ItemType);

						var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;	

						var serialnumbers = "";
						var localSerialNoArray = new Array();
						if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
						{
							var LP = CyccRecord.getFieldValue('custrecord_cycact_lpno');
							var actqty1 = CyccRecord.getFieldValue('custrecord_cycle_act_qty');
							var expqty1  = CyccRecord.getFieldValue('custrecord_cycleexp_qty');
							nlapiLogExecution('ERROR', 'expqty1', expqty1);	
							nlapiLogExecution('ERROR', 'actqty1', actqty1);	
							//var resultQty1 = parseFloat(expqty1) - parseFloat(actqty1);
							var resultQty1 = parseFloat(actqty1) - parseFloat(expqty1);
							nlapiLogExecution('ERROR', 'resultQty1', resultQty1);	

							//localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,restype,CyccRecID,'');
							AdjustSerialNumbers(localSerialNoArray,parseFloat(resultQty1),ItemId,LP,restype,CyccRecID,'');
						}

					}
					catch (e) {

						if (e instanceof nlobjError) 
						{
							nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
						}

						else 
						{
							nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
						}
					}
				}

				try {
					nlapiLogExecution('ERROR','test1', 'test1');
					var vopenrecid=CyccRecord.getFieldValue('custrecord_opentaskid');
					nlapiLogExecution('ERROR','id', vopenrecid);
					nlapiLogExecution('ERROR','sku', CyccRecord.getFieldValue('custrecord_cycleact_sku'));
					nlapiLogExecution('ERROR','DateStamp', DateStamp());
					nlapiLogExecution('ERROR','TimeStamp', TimeStamp());
					nlapiLogExecution('ERROR','LPno', CyccRecord.getFieldValue('custrecord_cycact_lpno'));

					var fieldNames = new Array(); 
					fieldNames.push('custrecord_act_end_date');  
					fieldNames.push('custrecord_actualendtime');
					fieldNames.push('custrecord_sku');
					fieldNames.push('custrecord_act_qty');
					fieldNames.push('custrecord_lpno');

					var newValues = new Array(); 
					newValues.push(DateStamp());
					newValues.push(TimeStamp());
					newValues.push(CyccRecord.getFieldValue('custrecord_cycleact_sku'));
					newValues.push(parseFloat(actqty).toFixed(5));
					newValues.push(CyccRecord.getFieldValue('custrecord_cycact_lpno'));
					if (restype == 'resolve') {
						fieldNames.push('custrecord_wms_status_flag'); 
						newValues.push('22');
					}
					if (restype == 'ignore') {
						fieldNames.push('custrecord_wms_status_flag'); 
						newValues.push('21');
					}
					fieldNames.push('custrecord_upd_date');
					newValues.push(DateStamp());
					fieldNames.push('custrecord_sku_status');


					var OldItemstatus = CyccRecord.getFieldValue('custrecord_expcyclesku_status');
					var NewItemStatus = CyccRecord.getFieldValue('custrecord_cyclesku_status');

					var vitemstatus = OldItemstatus;
					nlapiLogExecution('ERROR','vitemstatus before if', vitemstatus);
					if(NewItemStatus !=null && NewItemStatus !='')
					{
						if(parseInt(NewItemStatus)!= parseInt(OldItemstatus))
						{
							vitemstatus = NewItemStatus;
						}

					}
					nlapiLogExecution('ERROR','vitemstatus after if', vitemstatus);
					newValues.push(vitemstatus);

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', vopenrecid, fieldNames, newValues);

				} 
				catch (e) {
					nlapiLogExecution('ERROR','Save unsuccessful into OPEN TASK' + '\n' + 'Error: ' + e.toString());
				}

			}
		}

	}

	updateScheduleScriptStatus('CYC Resolve',curuserId,'Completed',vcyccplanno,null);

} 
function updateScheduleScriptStatus(processname,curuserId,status,transactionrefno,trantype,notes)
{
	nlapiLogExecution('DEBUG','Into updateScheduleScriptStatus',status);

	var str = 'processname. = ' + processname + '<br>';
	str = str + 'curuserId. = ' + curuserId + '<br>';	
	str = str + 'transactionrefno. = ' + transactionrefno + '<br>';	
	str = str + 'trantype. = ' + trantype + '<br>';	
	str = str + 'notes. = ' + notes + '<br>';	
	str = str + 'status.=' + status + '<br>';	

	nlapiLogExecution('Debug', 'Function Parameters', str);

	if(status=='Submitted')
	{
		var datetime = DateStamp() +" "+ getClockTime();   
		var schedulestatus = nlapiCreateRecord('customrecord_ebiz_schedulescripts_status');
		schedulestatus.setFieldValue('name',processname);
		schedulestatus.setFieldValue('custrecord_ebiz_processname',processname);
		schedulestatus.setFieldValue('custrecord_ebiz_processstatus',status);
		schedulestatus.setFieldValue('custrecord_ebiz_initiateddatetime',datetime);
		schedulestatus.setFieldValue('custrecord_ebiz_processtranrefno',transactionrefno);
		if(curuserId!=null && curuserId!='')
			schedulestatus.setFieldValue('custrecord_ebiz_processinitiatedby',curuserId);
		if(trantype!=null && trantype!='')
			schedulestatus.setFieldValue('custrecord_ebiz_process_trantype',trantype);
		var tranid = nlapiSubmitRecord(schedulestatus);
	}
	else if(status=='In Progress') 
	{
		var filter=new Array();
		if(curuserId!=null && curuserId!='')
			filter.push(new nlobjSearchFilter('custrecord_ebiz_processinitiatedby',null,'anyof',curuserId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processstatus',null,'is','Submitted'));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processname',null,'is',processname));
		if(transactionrefno!=null && transactionrefno!=''&& transactionrefno!='null')
			filter.push(new nlobjSearchFilter('custrecord_ebiz_processtranrefno',null,'is',transactionrefno.toString()));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_processname');
		column[1]=new nlobjSearchColumn('id').setSort();

		var searchresult=nlapiSearchRecord('customrecord_ebiz_schedulescripts_status',null,filter,column);
		if(searchresult!=null && searchresult!='')
		{
			var vid=searchresult[0].getId();
			nlapiLogExecution('DEBUG','vid in In Progress',vid);
			var fields = new Array();
			var values = new Array();

			fields[0] = 'custrecord_ebiz_processstatus';
			fields[1] = 'custrecord_ebiz_processbegindate';
			fields[2] = 'custrecord_ebiz_processbegintime';

			values[0] = status;
			values[1] = DateStamp();
			values[2] = TimeStamp();

			nlapiSubmitField('customrecord_ebiz_schedulescripts_status', vid, fields, values);
		}

	}
	else if(status=='Completed') 
	{
		var filter=new Array();
		if(curuserId!=null && curuserId!='')
			filter.push(new nlobjSearchFilter('custrecord_ebiz_processinitiatedby',null,'anyof',curuserId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processstatus',null,'is','In Progress'));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processname',null,'is',processname));
		if(transactionrefno!=null && transactionrefno!=''&& transactionrefno!='null')
			filter.push(new nlobjSearchFilter('custrecord_ebiz_processtranrefno',null,'is',transactionrefno.toString()));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_processname');
		column[1]=new nlobjSearchColumn('id').setSort();

		var searchresult=nlapiSearchRecord('customrecord_ebiz_schedulescripts_status',null,filter,column);
		if(searchresult!=null && searchresult!='')
		{
			var vid=searchresult[0].getId();
			nlapiLogExecution('DEBUG','vid in In Completed',vid);
			var fields = new Array();
			var values = new Array();

			fields[0] = 'custrecord_ebiz_processstatus';
			fields[1] = 'custrecord_ebiz_processenddate';
			fields[2] = 'custrecord_ebiz_processendtime';
			fields[3] = 'custrecord_ebiz_processnotes';

			values[0] = status;
			values[1] = DateStamp();
			values[2] = TimeStamp();
			values[3] = notes;

			nlapiSubmitField('customrecord_ebiz_schedulescripts_status', vid, fields, values);
		}
	}
}
function getStockAdjustmentAccountNoNew(loc,tasktype,adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();
	//alert("loc : "+loc);
	if(loc!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[loc]));	

	if(tasktype!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));


	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');
	colsStAccNo[2] = new nlobjSearchColumn('internalid');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	//alert("StockAdjustAccResults " + StockAdjustAccResults);
	//alert("StockAdjustAccResults " + StockAdjustAccResults.length);
	if(StockAdjustAccResults !=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		//alert("1 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('internalid'));

		//alert("2 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
		if(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != null && StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != "")
			StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));


	}	

	return StAdjustmentAccountNo;	
}

function AdjustSerialNumbers(localSerialNoArray,qty,item,lp,restype,CyccRecID,varianceinExpserialNo)
{	
	try
	{
		nlapiLogExecution('ERROR','AdjustSerialNumbers qty',parseFloat(qty));
		nlapiLogExecution('ERROR','item',item);
		nlapiLogExecution('ERROR','lp',lp);
		nlapiLogExecution('ERROR','restype',restype);
		nlapiLogExecution('ERROR','CyccRecID',CyccRecID);
		if(restype == 'resolve'|| restype == 'close')
		{
			var fields = ['custrecord_cycle_serialno','custrecord_cycle_expserialno'];
			var columns = nlapiLookupField('customrecord_ebiznet_cyclecountexe', CyccRecID, fields);
			var ActSerialNos = columns.custrecord_cycle_serialno;
			var ActSerialNosArrayTemp=new Array();

			if(ActSerialNos!=null && ActSerialNos!='')
			{
				ActSerialNosArrayTemp=ActSerialNos.split(',');
			}

			nlapiLogExecution('ERROR', 'ActSerialNos in AdjustSerialNumbers: ', ActSerialNos);
			nlapiLogExecution('ERROR', 'ActSerialNosArrayTemp in AdjustSerialNumbers: ', ActSerialNosArrayTemp);
			var ExpSerialNos = columns.custrecord_cycle_expserialno;
			nlapiLogExecution('ERROR', 'ExpSerialNos in AdjustSerialNumbers: ', ExpSerialNos);


			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'D');
				filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',[3,19]);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "" && ActSerialNos == "")
				{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in if: ', searchResults.length);
				for (var i = 0; i < searchResults.length; i++) 
				{   
					if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
					{
						var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
						var InternalID = searchResults[i].getId();

						//var currentRow = [SerialNo];					
						//localSerialNoArray.push(currentRow);
						//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

						//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
						//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
					}
				}
				}
				else
				{


					var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
					filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
					var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

					if(searchResults != null && searchResults != "")
					{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in else: ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');

							nlapiLogExecution('ERROR', 'searchResults.length : SerialNo ', SerialNo);
							nlapiLogExecution('ERROR', 'searchResults.length : ActSerialNos', ActSerialNos);


							//case # 20126283
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								//if(varianceinExpserialNo.indexOf(SerialNo) != -1)
								//{
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos : ', SerialNo);
								var InternalID = searchResults[i].getId();

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/									

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}
						}
					}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				//Case #  20126109  Start,
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
				//Case # 20126109 End.

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				columns[1] = new nlobjSearchColumn('custrecord_serialstatus');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var SerialStatus=searchResults[i].getValue('custrecord_serialstatus');
							/*var InternalID = searchResults[i].getId();

							var currentRow = [SerialNo];					
							localSerialNoArray.push(currentRow);
							nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								/*nlapiLogExecution('ERROR', 'Index of SerialNo in ExpSerialNosArrayTemp  ', ExpSerialNosArrayTemp.indexOf(SerialNo));
								if(ExpSerialNosArrayTemp.indexOf(SerialNo) != -1)
								{*/
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos if qty>0: ', SerialNo);
								var InternalID = searchResults[i].getId();

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/
								//case #  20126283 Added If condition.
								/*if(SerialStatus!='I'){*/

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}
						}
					}
				}
			}
			else if(parseFloat(qty) == 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');							
							//var InternalID = searchResults[i].getId();



							//added now
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								var InternalID = searchResults[i].getId();
								//nlapiLogExecution('ERROR', 'Serial Entry rec id to tst',InternalID);

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray in resolve and qty ==0: ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
								LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

								var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);*/
								//nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);

							}
							//added now


							//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);	


						}
					}
				}
			}
		}
		if(restype == 'ignore' || restype == 'recount')
		{
			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);
						}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());							

							var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
						}
					}
				}
			}		
		}		

	}
	catch (e) {
		nlapiLogExecution('ERROR', 'Catch', e);
	}
	return localSerialNoArray;
}

function getSerialNoCSVvalues(ItemId,LP)
{
	var filters = new Array();

	filters[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemId);	
	filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
	filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', '32');
	var tempSerial = "";

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
	var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if(serchRec!=null && serchRec!='')
	{
		for (var z = 0; z < serchRec.length; z++) {

			if (tempSerial == "") {
				tempSerial = serchRec[z].getValue('custrecord_serialnumber');

			}
			else {

				tempSerial = tempSerial + "," + serchRec[z].getValue('custrecord_serialnumber');
			}

		}


	}
	return tempSerial;
}

function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('ERROR', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0];
			}
			else
			{
				csv += arrayLP[i][0] + ",";
			}
		}
	}
	return csv;
}

function AdjustSerialNumbers(localSerialNoArray,qty,item,lp,restype,CyccRecID,varianceinExpserialNo)
{	
	try
	{
		nlapiLogExecution('ERROR','AdjustSerialNumbers qty',parseFloat(qty));
		nlapiLogExecution('ERROR','item',item);
		nlapiLogExecution('ERROR','lp',lp);
		nlapiLogExecution('ERROR','restype',restype);
		nlapiLogExecution('ERROR','CyccRecID',CyccRecID);
		if(restype == 'resolve'|| restype == 'close')
		{
			var fields = ['custrecord_cycle_serialno','custrecord_cycle_expserialno'];
			var columns = nlapiLookupField('customrecord_ebiznet_cyclecountexe', CyccRecID, fields);
			var ActSerialNos = columns.custrecord_cycle_serialno;
			var ActSerialNosArrayTemp=new Array();

			if(ActSerialNos!=null && ActSerialNos!='')
			{
				ActSerialNosArrayTemp=ActSerialNos.split(',');
			}

			nlapiLogExecution('ERROR', 'ActSerialNos in AdjustSerialNumbers: ', ActSerialNos);
			nlapiLogExecution('ERROR', 'ActSerialNosArrayTemp in AdjustSerialNumbers: ', ActSerialNosArrayTemp);
			var ExpSerialNos = columns.custrecord_cycle_expserialno;
			nlapiLogExecution('ERROR', 'ExpSerialNos in AdjustSerialNumbers: ', ExpSerialNos);


			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'D');
				filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',[3,19]);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "" && ActSerialNos == "")
				{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in if: ', searchResults.length);
				for (var i = 0; i < searchResults.length; i++) 
				{   
					if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
					{
						var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
						var InternalID = searchResults[i].getId();

						//var currentRow = [SerialNo];					
						//localSerialNoArray.push(currentRow);
						//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

						//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
						//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
					}
				}
				}
				else
				{


					var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
					filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
					var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

					if(searchResults != null && searchResults != "")
					{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in else: ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');

							nlapiLogExecution('ERROR', 'searchResults.length : SerialNo ', SerialNo);
							nlapiLogExecution('ERROR', 'searchResults.length : ActSerialNos', ActSerialNos);


							//case # 20126283
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								//if(varianceinExpserialNo.indexOf(SerialNo) != -1)
								//{
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos : ', SerialNo);
								var InternalID = searchResults[i].getId();

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/									

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}
						}
					}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				//Case #  20126109  Start,
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
				//Case # 20126109 End.

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				columns[1] = new nlobjSearchColumn('custrecord_serialstatus');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var SerialStatus=searchResults[i].getValue('custrecord_serialstatus');
							/*var InternalID = searchResults[i].getId();

							var currentRow = [SerialNo];					
							localSerialNoArray.push(currentRow);
							nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								/*nlapiLogExecution('ERROR', 'Index of SerialNo in ExpSerialNosArrayTemp  ', ExpSerialNosArrayTemp.indexOf(SerialNo));
								if(ExpSerialNosArrayTemp.indexOf(SerialNo) != -1)
								{*/
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos if qty>0: ', SerialNo);
								var InternalID = searchResults[i].getId();

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/
								//case #  20126283 Added If condition.
								/*if(SerialStatus!='I'){*/

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}
						}
					}
				}
			}
			else if(parseFloat(qty) == 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');							
							//var InternalID = searchResults[i].getId();



							//added now
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								var InternalID = searchResults[i].getId();
								//nlapiLogExecution('ERROR', 'Serial Entry rec id to tst',InternalID);

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray in resolve and qty ==0: ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
								LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

								var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);*/
								//nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);

							}
							//added now


							//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);	


						}
					}
				}
			}
		}
		if(restype == 'ignore' || restype == 'recount')
		{
			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);
						}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());							

							var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
						}
					}
				}
			}		
		}		

	}
	catch (e) {
		nlapiLogExecution('ERROR', 'Catch', e);
	}
	return localSerialNoArray;
}
function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}


function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		var vCost;
		var vAvgCost;
		var vItemname;
		var NSid='';
		var confirmLotToNS='N';
		nlapiLogExecution('ERROR', 'item::', item);
		nlapiLogExecution('ERROR', 'itemstatus::', itemstatus);
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);
		nlapiLogExecution('ERROR', 'lot::', lot);
		//alert("AccNo :: "+ AccNo);
		confirmLotToNS=GetConfirmLotToNS(loc);

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));

		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		//alert("itemdetails " + itemdetails);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		

		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
		nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
		if(AccNo==null || AccNo=="")
		{		
			AccNo = getAccountNo(loc,vItemMapLocation);
			nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
		}
		//alert("NS Qty "+ parseFloat(qty));
		//	if(lot==null || lot=='' || lot=='null')
		//	{
		var outAdj = nlapiCreateRecord('inventoryadjustment');			

		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', vItemMapLocation);
		outAdj.selectNewLineItem('inventory');
		outAdj.setCurrentLineItemValue('inventory', 'item', item);
		outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
		outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('Debug', 'subs', subs);
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
		}
		//	}


		//alert('Hi1');
		//alert("vAvgCost "+ vAvgCost);
		if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			if(outAdj!=null && outAdj!='' && outAdj!='null')	
				outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
		}

		var vAdvBinManagement=false;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

		var vItemType="";


		if(lot!=null && lot!='')
		{
			var fields = ['recordType','custitem_ebizserialin'];
			var columns = nlapiLookupField('item', item, fields);
			vItemType = columns.recordType;
			nlapiLogExecution('ERROR','vItemType',vItemType);

			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;	

			if(vAdvBinManagement)
			{
				if (vItemType == "lotnumberedinventoryitem" || vItemType == "lotnumberedassemblyitem")
				{
					var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');

					if(confirmLotToNS=='N')
						lot=vItemname;			

					nlapiLogExecution('ERROR', 'lot', lot);

					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');

					compSubRecord.commit();

				}
			}
			if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
			{
				vItemname=vItemname.replace(/ /g,"-");

				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if(confirmLotToNS=='N')
					lot=vItemname;			

				nlapiLogExecution('ERROR', 'lot', lot);

				nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
			}
			else if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T")
			{
				//case # 20127185ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½,split based on ,.
				var cnt=lot.split(',');
				nlapiLogExecution('ERROR', 'cnt', cnt);

				/*	for(var n=0;n<cnt.length;n++)
				{*/

				//var lotnumbers=cnt[n];
				var lotnumbers = lot.split(',');
				nlapiLogExecution('ERROR', 'lotnumbers', lotnumbers);
				if(lotnumbers!=null && lotnumbers!='' && lotnumbers!='null')
				{

					var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
					nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
					nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
					if(AccNo==null || AccNo=="")
					{		
						AccNo = getAccountNo(loc,vItemMapLocation);
						nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
					}
					var outAdj = nlapiCreateRecord('inventoryadjustment');	
					outAdj.setFieldValue('account', AccNo);
					outAdj.setFieldValue('memo', notes);
					outAdj.setFieldValue('adjlocation', vItemMapLocation);
					outAdj.selectNewLineItem('inventory');
					outAdj.setCurrentLineItemValue('inventory', 'item', item);
					outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
					outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
					//Case # 20126291,20126107,20126190  start
					if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
					{

						var	compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');

						//var tempQtyArray=lotnumbers.split(',');
						var tempQtyArray=lotnumbers;

						for (var x2 = 0; x2 < tempQtyArray.length; x2++)
						{
							var tempQty;

							if(parseFloat(qty)<0)
							{
								tempQty=-1;
							}
							else
							{
								tempQty=1;
							}

							/*if(x==0)
								var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');*/
							compSubRecord.selectNewLineItem('inventoryassignment');
							//case # 20126232ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½ start
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQty);
							//case # 20126232ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â½ end
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempQtyArray[x2]);

							compSubRecord.commitLineItem('inventoryassignment');

						}
						compSubRecord.commit();
					}
					//Case # 20126291,20126107,20126190  End

					var subs = nlapiGetContext().getFeature('subsidiaries');
					nlapiLogExecution('Debug', 'subs', subs);
					if(subs==true)
					{
						var vSubsidiaryVal=getSubsidiaryNew(loc);
						nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
						if(vSubsidiaryVal != null && vSubsidiaryVal != '')
							outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
					}


					if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
					{
						nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
						outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
					}
					else
					{
						nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
						if(outAdj!=null && outAdj!='' && outAdj!='null')	
							outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
					}

					nlapiLogExecution('ERROR', 'lot with serial no', lotnumbers);

					nlapiLogExecution('ERROR', 'LotNowithQty', lotnumbers);
					outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lotnumbers);


					var subs = nlapiGetContext().getFeature('subsidiaries');
					nlapiLogExecution('ERROR', 'subs', subs);
					if(subs==true)
					{
						var vSubsidiaryVal=getSubsidiaryNew(loc);
						if(vSubsidiaryVal != null && vSubsidiaryVal != '')
							outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
						nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
					}
					outAdj.commitLineItem('inventory');
					NSid=nlapiSubmitRecord(outAdj, true, true);
					//alert("Success NS");
					nlapiLogExecution('ERROR', 'NSid', NSid);
				}
				//}	
			}
			//}
		}
		//alert('Hi3');
		if (vItemType != "serializedinventoryitem" && vItemType != "serializedassemblyitem" && serialInflg != "T")
		{
			var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
			nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
			if(AccNo==null || AccNo=="")
			{		
				AccNo = getAccountNo(loc,vItemMapLocation);
				nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
			}
			var subs = nlapiGetContext().getFeature('subsidiaries');
			nlapiLogExecution('ERROR', 'subs', subs);
			if(subs==true)
			{
				var vSubsidiaryVal=getSubsidiaryNew(loc);
				if(vSubsidiaryVal != null && vSubsidiaryVal != '')
					outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
				nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			}
			outAdj.commitLineItem('inventory');
			NSid=nlapiSubmitRecord(outAdj, true, true);
			//alert("Success NS");
			nlapiLogExecution('ERROR', 'NSid', NSid);

			nlapiLogExecution('ERROR', 'type argument', 'type is create');
		}
	}


	catch(e) {
		if (e instanceof nlobjError) 
		{
			// nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
			nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
		}

		else 
		{
			//nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
		}

		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', e);
		//alert("Error" + exp.toString());

	}
	return NSid;
}