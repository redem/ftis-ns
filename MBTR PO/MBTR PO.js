function massUpdate(recType, recId) {

 var loadPO = nlapiLoadRecord(recType, recId);
 var itemCount = loadPO.getLineItemCount('item');
 
 for(x = 1; x <= itemCount; x++){
 
  var itemRecord = loadPO.getLineItemValue('item','item', x);
  var itemMBTR = nlapiLookupField('inventoryitem', itemRecord, 'matchbilltoreceipt');
  
  loadPO.setLineItemValue('item', 'matchbilltoreceipt', x, itemMBTR);
 }
 
 var submitRec = nlapiSubmitRecord(loadPO);
}