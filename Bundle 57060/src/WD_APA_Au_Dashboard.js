/**
 * (c) 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 *
*/

var APA;
if (!APA) { APA = {}; };
if (!APA.Dashboard) { APA.Dashboard = {}; };

APA.Dashboard.Reports = {

   URL : '/app/common/search/searchresults.nl?searchid=',

   getReportsList : function _getReportsList() {

       var reports = [];

       reports.push({reportname : 'Spend by Department', reportid : 'customsearch_ap_rpt_spend_dept'});
       reports.push({reportname : 'Spend by Class', reportid : 'customsearch_ap_rpt_spend_class'});
       reports.push({reportname : 'Spend by Location', reportid : 'customsearch_ap_rpt_spend_loc'});
       reports.push({reportname : 'Spend by Vendor', reportid : 'customsearch_ap_rpt_spend_vendor'});

       return reports;

   }

};

APA.Dashboard.Shortcuts = {

   URL : '/app/accounting/transactions/',

   getShortcutsList : function _getShortcutsList() {

       var shortcuts = [];

       shortcuts.push({shortcutname : 'Order Requisitions', shortcutid : 'orderpurchreqs.nl'});
       shortcuts.push({shortcutname : 'Order Items', shortcutid : 'orderitems.nl'});

       return shortcuts;

    }

};