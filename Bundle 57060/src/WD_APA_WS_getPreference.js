/**
 * (c) 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Jul 2014     dcalibuyo
 *
 */

var APA;
if (!APA) { APA = {}; }
/*
 * 
 */
APA.getUserPreference = function _getUserPreference() {
	var type = nlapiGetContext().getSetting('SCRIPT', 'custscript_apa_pref_type');
	var prefId = nlapiGetContext().getSetting('SCRIPT', 'custscript_apa_pref_id');
	var status = 0;
    nlapiLogExecution('DEBUG','APA.getUserPreference', 'Type: ' + type + ' Prefid: ' + prefId);
    //NOTE: valid for checkbox only. modify accordingly when checking for dropdown fields.
	if (nlapiLoadConfiguration(type).getFieldValue(prefId) == 'T') {
		status = 1;
	}
     nlapiLogExecution('DEBUG','APA.getUserPreference', 'status: ' + status);
	return status;
};

