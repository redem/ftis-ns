/**
 * (c) 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Apr 2014     drodriguez
 *
 */

var APA_RAW;
if (!APA_RAW) { APA_RAW = {}; }
if (!APA_RAW.Employee) { APA_RAW.Employee = {}; }
/*
 * 
 */
APA_RAW.Employee.getEntityApprovalDetails = function _getEntityApprovalDetails(internalid)
{
	
	nlapiLogExecution('DEBUG', 'getEntityApprovalDetails: start', 'USAGE: ' + APA.Context.getRemainingUsage() + '\n'
			 			 + '<br/>internalid:  ' + internalid + '\n'
						);
			
	var entityObj={};
	
	try {
		var filters = [];
		var columns = [];
		var searchresults = null;
		var searchresult;	

		filters.push(new nlobjSearchFilter('internalid', null , 'is', internalid));		
		columns.push(new nlobjSearchColumn('supervisor', null, null));
		columns.push(new nlobjSearchColumn('purchaseorderapprover', null, null));		
		columns.push(new nlobjSearchColumn('purchaseorderapprovallimit', null, null));
		searchresults = nlapiSearchRecord('employee', null, filters, columns);
		
		if (searchresults) {
			searchresult = searchresults[0];			
			entityObj.supervisor = searchresult.getValue('supervisor', null, null);
			entityObj.purchaseorderapprover = searchresult.getValue('purchaseorderapprover', null, null);			
			entityObj.purchaseorderapprovallimit = searchresult.getValue('purchaseorderapprovallimit', null, null);
		}
	
	}
	catch (ex) {
		var subject_message = 'getEntityApprovalDetails';
		var body_message = '';
			
		if (ex instanceof nlobjError) {
			body_message = 'system error: <p>' + ex.getCode() + ': <p>' + ex.getDetails();
			nlapiLogExecution('ERROR', subject_message, body_message); 		
		} else {
			body_message = 'unexpected error: <p>' + ex.toString();
			nlapiLogExecution('ERROR', subject_message, body_message); 
		}
	}   
		
	nlapiLogExecution('DEBUG', 'getEntityApprovalDetails: end', 'USAGE: '+ APA.Context.getRemainingUsage() + '\n'
			 			+ '<br/>internalid:  ' + internalid + '\n'
			 			+ '<br/>entityObj:  '+ entityObj + '\n'
						);

	return entityObj;
};//APA_RAW.Employee.getEntityApprovalDetails

//Gets the Highest Approver with Purchase Approval Limit Value
APA_RAW.Employee.getLastApproverDetails = function _getLastApproverDetails()
{	
	nlapiLogExecution('DEBUG', 'getLastApproverDetails: start', 'USAGE: ' + APA.Context.getRemainingUsage() + '\n'
			 			 + '<br/>entityinternalid:  ' + entityinternalid + '\n'
						);
			
	var entityinternalid = nlapiGetNewRecord().getFieldValue('entity');	
	
	var entityObj = {};	
	var supervisor;
	var purchaseorderapprover;	
	var purchaseorderapprovallimit = 0;
	
	try {
		entityObj = APA_RAW.Employee.getEntityApprovalDetails(entityinternalid);		
		supervisor = entityObj.supervisor;
		purchaseorderapprover = entityObj.purchaseorderapprover;		
		
		var approver;

		while (APA.Library.isValidObject(purchaseorderapprover) || APA.Library.isValidObject(supervisor)) {

			if (APA.Library.isValidObject(purchaseorderapprover)) {
				approver = purchaseorderapprover;
			} else {
				approver = supervisor;
			}

			entityObj=APA_RAW.Employee.getEntityApprovalDetails(approver);		

			//if the Approver has no Purchase Approval Limit Value, exit the loop
			if (!APA.Library.isValidObject(entityObj.purchaseorderapprovallimit)) {
				break;
			}

			supervisor = entityObj.supervisor;
			purchaseorderapprover = entityObj.purchaseorderapprover;		
			purchaseorderapprovallimit = entityObj.purchaseorderapprovallimit;

			nlapiLogExecution('DEBUG', 'getLastApproverDetails: ', '\n'
					+ '<br/>approver:  ' + approver + '\n'				
					+ '<br/>supervisor:  ' + supervisor + '\n'
					+ '<br/>purchaseorderapprover:  ' + purchaseorderapprover + '\n'			 	
					+ '<br/>purchaseorderapprovallimit:  ' + purchaseorderapprovallimit + '\n'
			);								
		}
		
		
	} catch (ex) {
		var subject_message = 'getLastApproverDetails';
		var body_message = '';
			
		if (ex instanceof nlobjError) {
			body_message = 'system error: <p>' + ex.getCode() + ': <p>' + ex.getDetails();
			nlapiLogExecution('ERROR', subject_message, body_message); 
				
		} else {
			body_message = 'unexpected error: <p>' + ex.toString();
			nlapiLogExecution('ERROR', subject_message, body_message); 
			
		}
	} 
	
	nlapiLogExecution('DEBUG', 'getLastApproverDetails: ', 'purchaseorderapprovallimit: '+ purchaseorderapprovallimit);
	
	nlapiLogExecution('DEBUG', 'getLastApproverDetails: end', 'USAGE: '+ APA.Context.getRemainingUsage() + '\n'
				 			+ '<br/>entityObj:  ' + entityObj + '\n'
							);
	
	return purchaseorderapprovallimit;
};//APA_RAW.Employee.getEntityApprovalDetails


