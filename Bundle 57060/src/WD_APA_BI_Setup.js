/**
 * (c) 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 	
*/

var APA;
if (!APA) { APA = {}; }
if (!APA.Setup_BI) { APA.Setup_BI = {}; }


APA.Setup_BI.beforeUpdate = function _beforeUpdate(fromversion, toversion)
{ 
	
	//Always check if Approval Routing is enabled	
	var approvalroutingOn = APA.Library.isAcctFeatureOn('APPROVALROUTING');
	if (!approvalroutingOn) { 		
		throw nlapiCreateError('INSTALLATION_ERROR',
				'Feature Approval Routing must be enabled. Please enable the feature and re-try.', false);
	}
		
	//Always check if workflow is enabled	
	var suiteflowOn = APA.Library.isAcctFeatureOn('WORKFLOW');
	if (!suiteflowOn) { 					
		throw nlapiCreateError('INSTALLATION_ERROR',
				'Feature SuiteFlow must be enabled. Please enable the feature and re-try.', false);
	}
	
	nlapiLogExecution('DEBUG', 'beforeUpdate ', 'approvalroutingOn=' + approvalroutingOn +'  suiteflowOn=' + suiteflowOn); 
	
};
