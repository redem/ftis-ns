/**
 * (c) 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 *
 */

var APA;
if (!APA) { APA = {}; }

APA.DashboardReports_Po = {};

APA.DashboardReports_Po.showReportsList = function _showReportsList(portlet, column) {

    var reports = APA.Dashboard.Reports.getReportsList();
    var reportsURL = APA.Dashboard.Reports.URL;

    portlet.setTitle('Reports');
    var html = [];

    for(var i = 0; i < reports.length; i++) {

        html.push('<ul class="ns-portlet-column" style="line-height:21px; display: block;">'
                    + '<li>'
                    + '<span>'
                    + '<a href="' + reportsURL + reports[i]['reportid'] + '">' + reports[i]['reportname'] + '</a>'
                    + '</span>'
                    + '</li>'
                    + '</ul>');

    }

    var content = '';
    if(html.length > 0) {

        content = html.join('\n');

    } else {

        content = 'No reports found.';

    }

    portlet.setHtml( content );

};