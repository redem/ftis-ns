/**
 * (c) 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
*/

var APA;
if (!APA) { APA = {}; };
if (!APA.Context) { APA.Context = {}; };
if (!APA.Library) { APA.Library = {}; };

/*
 * test valid input
 */

APA.Library.isValidObject = function _isValidObject(objectToTest)
{
	var isValidObject = false;
	isValidObject = (objectToTest!=null && objectToTest!='' && objectToTest!=undefined) ? true : false;
	return isValidObject;
	
};//end APA.Library.isValidObject
/*
 * check if feature is enabled
 */
APA.Library.isAcctFeatureOn = function _isAcctFeatureOn(featureName)
{   
    var featureStatus = APA.Context.getFeature(featureName);   
    
    return featureStatus;	
};//end APA.Library.trim
/*
 * check if preference is on
 */
APA.Library.isAcctPreferenceOn = function _isAcctPreferenceOn(preferenceid)
{
    var isPreferenceOn = false;
    var preferenceStatus = APA.Context.getPreference(preferenceid);
    if (preferenceStatus == 'T' || preferenceStatus == 't') { 
    	isPreferenceOn = true; 
    }
    
    return isPreferenceOn;	
};//end APA.Library.isAcctPreferenceOn


APA.Library.setPreference = function _setPreference(preftype, preferenceid, value)
{
	var acctprefobj = nlapiLoadConfiguration(preftype);
	acctprefobj.setFieldValue(preferenceid, value);
	nlapiSubmitConfiguration(acctprefobj);
		
};


APA.Context = nlapiGetContext();