/**
 * (c) 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 *
 */

var APA;
if (!APA) { APA = {}; }

APA.DashboardShortcuts_Po = {};

APA.DashboardShortcuts_Po.showScList = function _showScList(portlet, column) {

    var shortcuts = APA.Dashboard.Shortcuts.getShortcutsList();
    var shortcutsURL = APA.Dashboard.Shortcuts.URL;

    portlet.setTitle('Shortcuts');
    var html = [];

    for (var i = 0; i < shortcuts.length; i++) {

        html.push('<ul class="ns-portlet-column" style="line-height:21px; display: block;">'
                    + '<li>'
                    + '<span>'
                    + '<a href="' + shortcutsURL + shortcuts[i]['shortcutid'] + '">' + shortcuts[i]['shortcutname'] + '</a>'
                    + '</span>'
                    + '</li>'
                    + '</ul>');

    }

    var content = '';

    if (html.length > 0) {

        content = html.join('\n');

    } else {

        content = 'No shortcuts found.';

    }

    portlet.setHtml( content );

};