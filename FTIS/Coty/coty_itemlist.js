/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Apr 2018     noeh.canizares
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	 
	this.mctx = nlapiGetContext();
	this.request = request;
	this.response = response;
	this.runFunction = function() {
		if (this.request.getMethod() == 'GET') {
			this.GET();
		} else {
			this.POST();
		}

	};
	this.GET = function() {
		var house = this.request.getParameter('house');
		
		var prod = this.request.getParameter('prod');
		
		
			var form = nlapiCreateForm('Simple Form',true);
		     
		 
			var sublist = form.addSubList('custpage_sublist','inlineeditor','Inline Editor Sublist', 'tab1');
			 
			// add fields to the sublist
			sublist.addField('sublist1','date', 'Date');
			sublist.addField('sublist2','text', 'Name');
			sublist.addField('sublist3','currency', 'Currency');
			sublist.addField('sublist4','textarea', 'Large Text');
			sublist.addField('sublist5','float', 'Float');
			
			// make the Name field unique. Users cannot provide the same value for the Name field.
			sublist.setUniqueField('sublist2');
		
			
			response.writePage( form );
		
	};
	
	
	
	


}


function getItem(house,prodline){
	var results = nlapiSearchRecord("item",null,
	[
	   ["subsidiary","anyof","7"], 
	   "AND", 
	   ["isinactive","is","F"], 
	   "AND", 
	   ["type","anyof","Assembly","InvtPart"], 
	   "AND", 
	   ["custitem_ftss_house","anyof",house], 
	   "AND", 
	   ["custitem_coty_prodline_cr","anyof",prodline]
	], 
	[
	   new nlobjSearchColumn("internalid"),
	   new nlobjSearchColumn("custitem_ftss_house"), 
	   new nlobjSearchColumn("custitem_coty_prodline_cr"), 
	   new nlobjSearchColumn("displayname"), 
	   new nlobjSearchColumn("quantityonhand"), 
	   new nlobjSearchColumn("quantityonorder"), 
	   new nlobjSearchColumn("quantityavailable"), 
	   new nlobjSearchColumn("quantitybackordered"), 
	   new nlobjSearchColumn("baseprice"),
	   new nlobjSearchColumn("quantitycommitted")
	]
	);
	var cont =[];
	for(var x = 0; x< results.length;x++){
		var data = {};
		data.internalid = results[x].getValue('internalid') ;
		data.displayname = results[x].getValue('displayname') ;
		data.custitem_ftss_house = results[x].getValue('custitem_ftss_house') ;
		data.custitem_coty_prodline_cr = results[x].getValue('custitem_coty_prodline_cr') ;
		data.quantityonhand = results[x].getValue('quantityonhand') ;
		data.quantityonorder = results[x].getValue('quantityonorder') ;
		data.quantityavailable = results[x].getValue('quantityavailable') ;
		data.quantitybackordered = results[x].getValue('quantitybackordered') ;
		data.quantitycommitted = results[x].getValue('quantitycommitted');
		data.baseprice = 100 ;
		
		cont.push(data);
	}
	return cont;
}

function runForm(request, response) {
	new suitelet(request, response).runFunction();
}

