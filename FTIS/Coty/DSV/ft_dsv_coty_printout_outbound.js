/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Apr 2018     noeh.canizares
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	 
	this.mctx = nlapiGetContext();
	this.request = request;
	this.response = response;
	this.runFunction = function() {
		if (this.request.getMethod() == 'GET') {
			this.GET();
		} else {
			this.POST();
		}

	};
	this.GET = function() {
		var rectype = this.request.getParameter('rectype');
		var recid = this.request.getParameter('recid');
		var ptype = this.request.getParameter('ptype');
		try{
			var record = nlapiLoadRecord(rectype, recid);
			
			switch(rectype){
				case 'purchaseorder': //tally
					// Read the template
					var template = nlapiLoadFile(47934).getValue(); //prod 47934 //sand 44850
					
					// Replace tags on the template
					var renderer = nlapiCreateTemplateRenderer();
					renderer.setTemplate(template);
					renderer.addRecord('record', record);
					
					
					var xml = renderer.renderToString();
					
					// Convert the template to PDF and send as response
					var file = nlapiXMLToPDF(xml);
					this.response.setContentType('PDF', rectype+'_'+recid+'.pdf', 'inline');
					this.response.write(file.getValue());
					break;
				case 'itemreceipt': //receive conf
					
					if(ptype == '1'){//rc
						var template = nlapiLoadFile(47936).getValue(); //prod 47936 //sandbox 45050
						// Replace tags on the template
						var renderer = nlapiCreateTemplateRenderer();
						renderer.setTemplate(template);
						renderer.addRecord('record', record);
						
						
						var xml = renderer.renderToString();
						
						// Convert the template to PDF and send as response
						var file = nlapiXMLToPDF(xml);
						this.response.setContentType('PDF', 'receiveconfirmation'+rectype+'_'+recid+'.pdf', 'inline');
						this.response.write(file.getValue());
						
					}
					
					break;
				case 'itemfulfillment':
					var itemcount = record.getLineItemCount('item');
					for(var x = 1; x<= itemcount;x++){
						var litem = record.getLineItemValue('item','item',x);
						var sunit = nlapiLookupField('item', litem, 'unitstype',true);
						(sunit)?record.setLineItemValue('item','unitsdisplay',x,sunit): '';
					}
					
					var template = '';
					switch(ptype){
						case '1':
							template = nlapiLoadFile(47935).getValue(); //do //production 47935 //sandbox 45150
							break;
						case '2':
							template = nlapiLoadFile(47933).getValue();//picklist //production 47933 //sandbox 45250
							break;
						default:
							this.response.write('Invalid Request');		
					}	
					
					// Replace tags on the template
					var renderer = nlapiCreateTemplateRenderer();
					renderer.setTemplate(template);
					renderer.addRecord('record', record);
					var xml = renderer.renderToString();
					// Convert the template to PDF and send as response
					var file = nlapiXMLToPDF(xml);
					this.response.setContentType('PDF', rectype+'_'+recid+'.pdf', 'inline');
					this.response.write(file.getValue());
					
					
					break;
				default:
					this.response.write('invalid request');
				
			}
			
		
			
			
			
		}catch(ex){
			this.response.write(ex);
		}
	};
}

function runForm(request, response) {
	new suitelet(request, response).runFunction();
}



/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function pobfl(type, form, request){
	var rectype = nlapiGetRecordType();
	var sub = nlapiGetFieldValue('subsidiary');
	if(sub == '7'){
	form.setScript('customscript_ft_dsv_coty_cs1');
		if(type == 'view'){		
			if(rectype == 'purchaseorder'){
				form.addButton('custpage_talyshet', 'Tally Sheet', 'printTally');
			}
			if(rectype == 'itemreceipt'){
				form.addButton('custpage_talyshet', 'Receive Confirmation Sheet', 'rconfirm');
				form.addButton('custpage_talyshet', 'Print Labels', 'plabel');
			}
			if(rectype == 'itemfulfillment'){
				form.addButton('custpage_printdo', 'Delivery Order', 'printdodsv');
				form.addButton('custpage_printpicklist', 'Pick List', 'printpldsv');
			}
			
		}
	}
	
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	var sub = nlapiGetFieldValue('subsidiary');
	if(sub == '7'){
		nlapiSetFieldDisabled('location',true);
	}
	
	
   
}
//production URL /app/site/hosting/scriptlet.nl?script=1690&deploy=1
//sandbox URL /app/site/hosting/scriptlet.nl?script=1715&deploy=1

var produrl = '/app/site/hosting/scriptlet.nl?script=1690&deploy=1';


function printdodsv(){
	var id = nlapiGetRecordId();
	var rectype = nlapiGetRecordType();
	var url = produrl+'&recid='+id+'&rectype='+rectype+'&ptype=1';
	window.open(url,'Delivery Order DSV');
}
function printpldsv(){
	var id = nlapiGetRecordId();
	var rectype = nlapiGetRecordType();
	var url = produrl+'&recid='+id+'&rectype='+rectype+'&ptype=2';
	window.open(url, 'Pick List DSV ');
}

function plabel(){
	var id = nlapiGetRecordId();
	var rectype = nlapiGetRecordType();
	var url = produrl+'&recid='+id+'&rectype='+rectype+'&ptype=2';
	window.open(url, 'Print Label ');
}

function rconfirm(){
	var id = nlapiGetRecordId();
	var rectype = nlapiGetRecordType();
	var url = produrl+'&recid='+id+'&rectype='+rectype+'&ptype=1';
	window.open(url, 'Receive Confirmation ');
}

function printTally(){
	var id = nlapiGetRecordId();
	var rectype = nlapiGetRecordType();
	var url = produrl+'&recid='+id+'&rectype='+rectype;
	window.open(url, 'Purchase Order Print out ');
}
