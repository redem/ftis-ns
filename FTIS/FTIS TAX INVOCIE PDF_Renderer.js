/**
 * 
 */

function generate(request, response)
{
	var invoiceId 			= request.getParameter('invoiceId');
	var itemFulfillmentId 	= request.getParameter('itemfulfillmentId');
	var record = nlapiLoadRecord('invoice', invoiceId); //'1845'); // Load employee record by ID 
	var fulfillment = nlapiLoadRecord('itemfulfillment', itemFulfillmentId); //'1843'); // Load employee record by ID 
	var renderer = nlapiCreateTemplateRenderer();  
   /*
   var template = '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">'+
   '<pdf><head></head>'+
      '<body width="2.35in" height="3.75in">'+
         //'<img align="center" width="100px" height="125px" src="${employee.image?html}"></img>'+
         '<p align="center" style="font-size:24px;font-weight:bold;">${employee.entityId}</p>'+
         '<p align="center">${employee.hiredate@label}<br />${employee.hiredate}</p>'+
      '</body></pdf>';
       
   */
   var template = nlapiLoadFile('2510').getValue(); //DOCUMENTS > TEMPLATES > PDF TEMPLATES
   
   renderer.setTemplate(template); // Passes in raw string of template to be transformed by FreeMarker
   renderer.addRecord('record', record); // Binds the invoice record object to the variable used in the template
   renderer.addRecord('fulfillment', fulfillment); //Binds the fulfillment record object to the variable used in the template
   var xml = renderer.renderToString(); // Returns template content interpreted by FreeMarker as XML string that can be passed to the nlapiXMLToPDF function.
   nlapiLogExecution('Debug', 'PDFXML', xml);
   var file = nlapiXMLToPDF(xml); // Produces PDF output.

   response.setContentType('PDF', 'taxinvoice.pdf', 'inline');
   response.write(file.getValue());
}