/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Jul 2016     amu
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	
	var record  = nlapiGetNewRecord();
	
	generateT1Number(record);
  
}



function generateT1Number(record){
	
	var t1Number 	= record.getFieldValue('custbody_t1_number');
	var itemFFNumber= record.getFieldValue('custbody_fulfillment_number');
	var invoiceId	= record.getId();
	var subsidiary	= record.getFieldValue('subsidiary');
	
	if(t1Number > 0 ){ return true;} // dont generate t1 again
	if(subsidiary != '3') {return true;} // run only for FTT subsidairy.
	
	if(itemFFNumber > 0 ){
		
			var t1Rec = nlapiCreateRecord('customrecord_t1_number');
			t1Rec.setFieldValue('custrecord_t1_if_number', itemFFNumber);
			t1Rec.setFieldValue('custrecord_t1_invoice_number', invoiceId);
			
			var t1RecId 	= nlapiSubmitRecord(t1Rec);
			nlapiSubmitField('invoice', invoiceId, 'custbody_t1_number', t1RecId);
			
		

	}
		
}
