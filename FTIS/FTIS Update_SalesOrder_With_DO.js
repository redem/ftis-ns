/**
 * 
 */

var environment = nlapiGetContext().getEnvironment();
var context		= nlapiGetContext().getExecutionContext();

function afterSubmit_updateSO_with_DO(type){
	
	nlapiLogExecution('Debug', 'Execution Context ', context);
	var details 	= '';
	
	if(type == 'delete') return true;	
	
	var record					= nlapiGetNewRecord();
	var recordId				= record.getId();
	
	var createdFrom 			= record.getFieldValue('createdfrom');
	var createdFromRecordType	= nlapiLookupField('transaction', createdFrom, 'recordtype'); 	
	
	details += '\n createdFrom ' + createdFrom;
	details += '\t createdFromRecordType ' + createdFromRecordType;
	
	
	if(createdFromRecordType != 'salesorder' && createdFromRecordType != 'transferorder' ) return true;
	
	if(createdFromRecordType == 'transferorder' ) return true; // temporarily excluded...due to error
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', null, 'anyof', createdFrom);
	filters[filters.length] = new nlobjSearchFilter('mainline', null, 'is', 'F');
	filters[filters.length] = new nlobjSearchFilter('taxline', null, 'is', 'F');
	filters[filters.length] = new nlobjSearchFilter('shipping', null, 'is', 'F');
	filters[filters.length] = new nlobjSearchFilter('fulfillingtransaction', null, 'is', recordId);
	
	
	
	var columns = new Array();
	
	columns[columns.length] = new nlobjSearchColumn('fulfillingtransaction');
	columns[columns.length] = new nlobjSearchColumn('line');
	columns[columns.length] = new nlobjSearchColumn('custcol_country_of_origin');
	columns[columns.length] = new nlobjSearchColumn('tranid');
	columns[columns.length] = new nlobjSearchColumn('item');
	
	var results = nlapiSearchRecord(createdFromRecordType, null, filters, columns);
	
	if(results){
		var salesOrder	= nlapiLoadRecord(createdFromRecordType, createdFrom);
		var lineCount	= salesOrder.getLineItemCount('item');
		
		for (var i=0; i< results.length; i++){
			var fulfillNumber = results[i].getValue('fulfillingtransaction');		
			var lineId	= results[i].getValue('line');
			var coo		= results[i].getValue('custcol_country_of_origin');
			var tranId	= results[i].getValue('tranid');
			var item	= results[i].getValue('item');
			
			details += '\n Order#'+ tranId ;
			details += '\t Order Line Id'+ lineId ;
			details += '\t Fulfilling Transaction ' + fulfillNumber;
			details += '\t COO ' + coo;
			details += '\t ITEM ' + item;
			//nlapiLogExecution('Debug', 'Order Details', details);
			
			if(fulfillNumber>0){
				
				for(var j=1; j<= lineCount; j++ ){
					var line = salesOrder.getLineItemValue('item','line', j);
					details += '\n line ' + line;
					details += '\t Order Line Id'+ lineId ;
					if(line==lineId){
						
						salesOrder.setLineItemValue('item','custcol_fulfillment_number', j, fulfillNumber);
						salesOrder.setLineItemValue('item','custcol_country_of_origin', j, coo);
						salesOrder.setLineItemValue('item','minqty', j, 1); // when updating SO, ignore the minqty rule.
						
					}
					
				}
				
			}
		}
		nlapiLogExecution('Debug', 'Order Details', details);
		nlapiSubmitRecord(salesOrder, false);
	}
	
	
	
	
}


