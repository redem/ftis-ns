/**
 * 
 */

// User Interface for generating the packingslip and pallet reports

var searchId	= 'customsearch423';

function ftis_wms_pdf_Report(request, reponse) {
	
	var customer 	= null;
	var orderNumbers = new Array();
	 var subsidiary = null;
	
	if ( request.getMethod() == 'GET' )
	   {
			var form = nlapiCreateForm('FTIS WMS PDF Report Generator');
			
			var subsidiaryFld = form.addField('custpage_subsidiary','select', 'Subsidiary', 'subsidiary').setMandatory(true);
			subsidiaryFld.setDefaultValue('4'); // FTIS
		    
			var customerFld = form.addField('custpage_customer','select', 'Customer', 'customer').setMandatory(true);
		        
		    var salesorderFld = form.addField('custpage_salesorder','select', 'Order #', 'transaction' ).setMandatory(true);
		        
		    var pdfoutputFld = form.addField('custpage_pdfoutput','select', 'PDF Output' ).setMandatory(true);
		    		    
		    pdfoutputFld.addSelectOption('','');
		    pdfoutputFld.addSelectOption('a', 'Packing Slip');
		    pdfoutputFld.addSelectOption('b', 'Pallet Report');		    
		    pdfoutputFld.setDefaultValue('a');
		        
		    var sublist = form.addSubList('sublist','list', 'Open Tasks List');
		    //sublist.addMarkAllButtons();
	        sublist.addField('sublist_chkbx', 'checkbox','Choose');
	        sublist.addField('sublist_internalid', 'integer', 'OpenTask ID').setDisplayType('inline');
	        sublist.addField('sublist_subsidiary', 'text', 'Subsidiary');
	        sublist.addField('sublist_customer', 'text', 'Customer');
	        sublist.addField('sublist_ordernumber', 'text', 'OrderNumber');
	        sublist.addField('sublist_item', 'text', 'Item');	        
	       
	        sublist.addField('sublist_wavenumber', 'textarea', 'Wave Numbers');
	        sublist.addField('sublist_containerlp', 'text', 'Container LP');
	        sublist.addField('sublist_lpno', 'text', 'LP #');
	       // sublist.addField('sublist_tasktype_id', 'text', 'Task Type ID');
	       // sublist.addField('sublist_tasktype', 'text', 'Task Type');
	       // sublist.addField('sublist_wmsstatusflag_id', 'text', 'WMS Status Flag ID');
	       // sublist.addField('sublist_wmsstatusflag', 'text', 'WMS Status Flag');
	        sublist.addField('sublist_actualquantity', 'float', 'Actual Quantity');
	        
	        
	        subsidiary	= nlapiGetFieldValue('custpage_subsidiary');
	        orderNumbers[orderNumbers.length]= nlapiGetFieldValue('sublist_ordernumber');
	        var openTasks 	= searchOpenTasksList(subsidiary, customer, orderNumbers);
	        
	        if(openTasks){
	        	for(var i=0; i<openTasks.length; i++){
	        		
	        		sublist.setLineItemValue('sublist_customer',i+1, openTasks[i].getText('mainname','custrecord_ebiz_order_no'));
	        		sublist.setLineItemValue('sublist_internalid',i+1, openTasks[i].getValue('internalid'));
	        		sublist.setLineItemValue('sublist_ordernumber',i+1, openTasks[i].getText('custrecord_ebiz_order_no'));
	        		sublist.setLineItemValue('sublist_item',i+1, openTasks[i].getText('custrecord_sku'));
	        		sublist.setLineItemValue('sublist_actualquantity',i+1, openTasks[i].getValue('custrecord_act_qty'));
	        		sublist.setLineItemValue('sublist_wavenumber',i+1, openTasks[i].getValue('custrecord_ebiz_wave_no'));
	        		sublist.setLineItemValue('sublist_containerlp',i+1, openTasks[i].getValue('custrecord_container_lp_no'));
	        		sublist.setLineItemValue('sublist_lpno',i+1, openTasks[i].getValue('custrecord_lpno'));
	        		//sublist.setLineItemValue('sublist_tasktype_id',i+1, openTasks[i].getValue('custrecord_tasktype'));
	        		//sublist.setLineItemValue('sublist_tasktype',i+1, openTasks[i].getText('custrecord_tasktype'));
	        		//sublist.setLineItemValue('sublist_wmsstatusflag_id',i+1, openTasks[i].getValue('custrecord_wms_status_flag'));
	        		//sublist.setLineItemValue('sublist_wmsstatusflag',i+1, openTasks[i].getText('custrecord_wms_status_flag'));
	        		sublist.setLineItemValue('sublist_subsidiary',i+1, openTasks[i].getText('subsidiary','custrecord_ebiz_order_no'));
	        		
	        		
	        	}
	        	
	        }
	        
	        
	        form.addSubmitButton('Submit');
	 
	        
	        response.writePage( form );
	   }
	   else
	       { 
		   	//TO DO
	       }
}

function searchOpenTasksList(subsidiary, customer, orderNumbers){
	//if(customer != null && customer !=''){
		
	var filters = new Array();
	
	//filters[filters.length] = new nlobjSearchFilter('custrecord_ebiz_order_no', 'is', orderNumbers);
		
		var openTasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',searchId, filters,null);
		
		return openTasks;
	//}
	
	
}

function Printreport(ebizwaveno){
	nlapiLogExecution('ERROR', 'into Printreport ebizwaveno',ebizwaveno);
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pakinglistreportpdf', 'customdeploy_ebizpackinglistpdf_di');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	WavePDFURL = WavePDFURL + '&custparam_ebiz_fullfill_no='+ ebizwaveno;	
	window.open(WavePDFURL);

}
