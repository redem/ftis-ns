/**
 * 
 */

var environment = nlapiGetContext().getEnvironment();

function onBeforeLoad(type, form)
{
	try{
		
		if(type !='view') return true;
		var subs = nlapiGetFieldValue('subsidiary');
		if(subs != '7'){
			//if(subsidiary != '3') return true;
			
			form.setScript("1102");
			//	if(environment == 'SANDBOX') {
			//		form.setScript("1086");
			//	}
			form.addButton("custpage_printtaxinvoice","Print Tax Invoice & T1", "printTaxInvoiceAndT1()");
		}
		
		
	}catch(ex){
		nlapiLogExecution('ERROR','BF ex' ,ex);
	}
}


//Before Submit Script in Invoice Record

function beforeSubmit_updateInvoice_withDO(type){
	try{
		var subs = nlapiGetFieldValue('subsidiary');
		if(subs != '7'){
			if(type=='delete') return true;
			
			var record = nlapiGetNewRecord();
			nlapiLogExecution('Debug', 'Line Item Count', record.getLineItemValue('item','custcol_fulfillment_number',1));
			var createdFrom = record.getFieldValue("createdfrom");
			
			var fulfillmentNumber = record.getLineItemValue('item','custcol_fulfillment_number',1);
			if(fulfillmentNumber){
				record.setFieldValue('custbody_fulfillment_number', fulfillmentNumber);
			}
			else{
				if(createdFrom){
					//throw nlapiCreateError('INCOMPLETE_ITEMFULFILLMENT', "COMPLETE THE WEIGHT AND CARTON NUMBER IN ITEM FULLFILLMENT BEFORE CREATING INVOICE", true);
				}
			}	
		}	
	}catch(ex){
		nlapiLogExecution('ERROR','BF ex' ,ex);
	}
}

function printTaxInvoiceAndT1()
{
	var invoiceId = nlapiGetRecordId();	
	var itemFulfillmentId = nlapiLookupField('invoice', invoiceId, 'custbody_fulfillment_number');	
	
	
	var url = nlapiResolveURL('SUITELET', 'customscript_taxinvoice_pdf_sl', "customdeploy_taxinvoice_pdf_sl", false);
	
	var params = '&invoiceId='+invoiceId+'&itemfulfillmentId='+itemFulfillmentId;
	//alert(url+params);
	
	window.open(url+params, '_blank');
	
	//NLInvokeButton(document.getElementById('print'));	
	//nlapiRequestURL( url, null, params, null);

	
	
}



function getFulfillmentNumber(){
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', null, 'anyof', createdFrom);
	filters[filters.length] = new nlobjSearchFilter('mainline', null, 'is', 'F');
	filters[filters.length] = new nlobjSearchFilter('taxline', null, 'is', 'F');
	filters[filters.length] = new nlobjSearchFilter('shipping', null, 'is', 'F');
	
	
	var columns = new Array();
	
	columns[columns.length] = new nlobjSearchColumn('fulfillingtransaction');
	columns[columns.length] = new nlobjSearchColumn('line');
	columns[columns.length] = new nlobjSearchColumn('custcol_country_of_origin');
	
	var results = nlapiSearchRecord('salesorder', null, filters, columns);
	
	if(results){
		var salesOrder	= nlapiLoadRecord(createdFromRecordType, createdFrom);
		
		for (var i=0; i< results.length; i++){
			var fulfillNumber = results[i].getValue('fulfillingtransaction');		
			var lineId	= results[i].getValue('line');
			var coo		= results[i].getValue('custcol_country_of_origin');
			
			details += '\n Line #'+ lineId ;
			details += '\n Fulfilling Transaction ' + fulfillNumber;
			details += '\n COO ' + coo;
			
			if(fulfillNumber>0){
				
				salesOrder.setLineItemValue('item','custcol_fulfillment_number', lineId, fulfillNumber);
				salesOrder.setLineItemValue('item','custcol_country_of_origin', lineId, coo);
				
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
}