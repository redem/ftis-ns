/**
 * 
 */

var environment = nlapiGetContext().getEnvironment();
function generatePackingList(request, response)
{
	var environment = nlapiGetContext().getEnvironment();
	
	var createdFromType		= request.getParameter('recordtype');
	var salesordInternalId 	= request.getParameter('orderId');
	var itemFulfillmentId 	= request.getParameter('itemfulfillmentId');
	//var vtrantype 			= nlapiLookupField('transaction', salesordInternalId, 'recordtype');
	//var record 				= nlapiLoadRecord(vtrantype, salesordInternalId); //'1845'); // Load employee record by ID 
	var fulfillment 		= nlapiLoadRecord('itemfulfillment', itemFulfillmentId); //'1843'); // Load employee record by ID 
	
	var vQbFullfillmentName = "";
	/*
	var linesearchfilters = new Array();
	if (vQbFullfillmentName != "" && vQbFullfillmentName != null) {
		nlapiLogExecution('ERROR', 'vQbFullfillmentName', vQbFullfillmentName);
		//linesearchfilters.push(new nlobjSearchFilter('name', null, 'is', vQbFullfillmentName));			 
	}
	
	linesearchfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', salesordInternalId));			 
	linesearchfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', '28'));
	linesearchfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
	
	var linecolumns = new Array();
	linecolumns[0] = new nlobjSearchColumn('custrecord_line_no');
	linecolumns[1] = new nlobjSearchColumn('custrecord_sku');
	linecolumns[2] = new nlobjSearchColumn('custrecord_skudesc');	
	linecolumns[3] = new nlobjSearchColumn('custrecord_act_qty');
	linecolumns[4] = new nlobjSearchColumn('name');	
	linecolumns[5] = new nlobjSearchColumn('name');	
	linecolumns[6] = new nlobjSearchColumn('name');	
	linecolumns[7] = new nlobjSearchColumn('name');	
	
	
	
	// new search column merged from monobid bundle on feb 27th 2013 by radhika /		
	linecolumns[5] = new nlobjSearchColumn('custrecord_batch_no');
	var linesearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, linesearchfilters, linecolumns);

	//for getting header details
	
	var salesorderheadresearch = nlapiLoadRecord(vtrantype, salesordInternalId); 
	var soname = salesorderheadresearch.getFieldValue('tranid');
	var	shippingadd=salesorderheadresearch.getFieldValue('shipaddress'); 
	var customerpono=salesorderheadresearch.getFieldValue('otherrefnum');	
	var customerpo=salesorderheadresearch.getFieldValue('entity');	
	var companyid=salesorderheadresearch.getFieldValue('custbody_nswms_company');
	var shippingCondition=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');
	var locationinternalid=salesorderheadresearch.getFieldValue('location');
	var incoterms=salesorderheadresearch.getFieldText('custbody_nswmsfreightterms');
	
	
	
	var opentasks	= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', 'customsearch_packinglist_opentasks',null, null);
	
	nlapiLogExecution('ERROR','locationinternalid ',locationinternalid);
	nlapiLogExecution('ERROR','lsresults length ',linesearchresults==null? 0 : linesearchresults.length);
	nlapiLogExecution('ERROR','opentasks length ',opentasks == null? 0: opentasks.length);
	
	*/
	
	var renderer = nlapiCreateTemplateRenderer();  
	 nlapiLogExecution('Debug', 'Renderer created', "");
  
	 var packingslipTemplateId = '2816'; // Singapore Template XML file
	 if(environment =='SANDBOX') {packingslipTemplateId = '2401';} 
   var template = nlapiLoadFile(packingslipTemplateId).getValue(); //DOCUMENTS >FILES > FILE CABINET > TEMPLATES >

   
   
   
   renderer.setTemplate(template); // Passes in raw string of template to be transformed by FreeMarker
   renderer.addRecord('record', fulfillment); // Binds the invoice record object to the variable used in the template
   //renderer.addRecord('fulfillment', fulfillment); //Binds the fulfillment record object to the variable used in the template
   //renderer.addSearchResults('lsresults', linesearchresults);
   //renderer.addSearchResults('opentasks', opentasks);
   
   var xml = renderer.renderToString(); // Returns template content interpreted by FreeMarker as XML string that can be passed to the nlapiXMLToPDF function.
   
   // Build the XML
   
     
	   
   nlapiLogExecution('Debug', 'PDFXML', xml);
   var file = nlapiXMLToPDF(xml); // Produces PDF output.

   var d = new Date();
   var sequence = d.yyyymmdd();
	
   var ext = '.pdf';
   var filename = 'PackingList' + sequence + ext ;
   
   response.setContentType('PDF', filename, 'inline');
      
   response.write(file.getValue());
}

Date.prototype.yyyymmdd = function() {
	   var yyyy = this.getFullYear().toString();
	   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
	   var dd  = this.getDate().toString();
	   var hh  = this.getHours().toString();
	   var mm  = this.getMinutes().toString();
	   var ss  = this.getSeconds().toString();
	   
	   
	   return yyyy + (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0])+ hh+mm+ss; // padding
	  };

	