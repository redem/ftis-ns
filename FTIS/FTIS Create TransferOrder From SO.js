/**
 * 
 */

function onAfterSubmit(type) {
	
	var so 			= nlapiGetNewRecord();
	
	var createTO 	= so.getFieldValue('custbody_create_to');
	var sotoId		= so.getFieldValue('custbody_so_to_number');
	var orderId		= so.getId();
	
	if(isNull(sotoNumber) && createTO == 'T'){
		createTransferOrder(so);
	}
	
	if(!isNull(sotoNumber) && createTO == 'T'){
		updateTransferOrder(so, sotoId);
	}
	
	return true;
	
}




function createTransferOrder(so){	
	
	var trecord = nlapiCreateRecord('transferorder');
	
	saveTransferOrder(so, trecord);
	
}


function updateTranserOrder(so, sotoId){
	
	var trecord = nlapiLoadRecord('transferorder', sotoId);
	
	for(var i=trecord.getLineItemCount('item'); i>=1; i--){
		
		trecord.removeLineItem('item', i, true);
	}
	
	
	
}



function saveTransferOrder(so, trecord){
	
	var transferredDate = so.getFieldValue('trandate');
	var fromLocation	= so.getFieldValue('location');
	var toLocation		= so.getFieldValue('custbody_to_location');
	
	trecord.setFieldValue('trandate',transferredDate);
	trecord.setFieldValue('location',fromLocation);
	trecord.setFieldValue('transferlocation',toLocation);
	trecord.setFieldValue('custbody_so_to', so.getId());
	
	for(var i=1; i<=so.getLineItemCount('item'); i++){
		
		var itemTransferred = so.getLineItemValue('item','item', i);
		var qtyTransferred 	= so.getLineItemValue('item','quantity', i);
		
		trecord.selectNewLineItem('item');	
		trecord.setCurrentLineItemValue('item', 'item', itemTransferred); 	
		trecord.setCurrentLineItemValue('item', 'quantity', qtyTransferred); 	
		trecord.commitLineItem('item');
		
	}
	
	var toId = nlapiSubmitRecord(trecord, true);
	nlapiSubmitField('salesorder', so.getId(), ['custbody_so_to','custbody_create_to'], [toId,'F']);
}



function isNull(string){
	
	if(string==''|| string==null) return true;
	if(string==undefined ) return true;
	
	return false;
	
}