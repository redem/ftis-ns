/**
 * 
 */

var environment = nlapiGetContext().getEnvironment();
function generate(request, response)
{
	var environment = nlapiGetContext().getEnvironment();
	
	var invoiceId 			= request.getParameter('invoiceId');
	var itemFulfillmentId 	= request.getParameter('itemfulfillmentId');
	var record 				= nlapiLoadRecord('invoice', invoiceId); //'1845'); // Load employee record by ID 
	var fulfillment 		= nlapiLoadRecord('itemfulfillment', itemFulfillmentId); //'1843'); // Load employee record by ID 
	var renderer 			= nlapiCreateTemplateRenderer();  
  

	var invoiceTemplateId = '2510';
	if(environment =='SANDBOX') { invoiceTemplateId = '2395';}
	
   var template = nlapiLoadFile(invoiceTemplateId).getValue(); //DOCUMENTS >FILES > FILE CABINET > TEMPLATES >

   
   
   
   renderer.setTemplate(template); // Passes in raw string of template to be transformed by FreeMarker
   renderer.addRecord('record', record); // Binds the invoice record object to the variable used in the template
   renderer.addRecord('fulfillment', fulfillment); //Binds the fulfillment record object to the variable used in the template
   var xml = renderer.renderToString(); // Returns template content interpreted by FreeMarker as XML string that can be passed to the nlapiXMLToPDF function.
   nlapiLogExecution('Debug', 'PDFXML', xml);
   var file = nlapiXMLToPDF(xml); // Produces PDF output.

   var d = new Date();
   var sequence = d.yyyymmdd();
   
   var ext = '.pdf';
   var filename = 'TaxInvoiceAndT1' + sequence + ext ;
   response.setContentType('PDF', filename, 'inline');
   response.write(file.getValue());
}

Date.prototype.yyyymmdd = function() {
	   var yyyy = this.getFullYear().toString();
	   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
	   var dd  = this.getDate().toString();
	   var hh  = this.getHours().toString();
	   var mm  = this.getMinutes().toString();
	   var ss  = this.getSeconds().toString();
	   
	   
	   return yyyy + (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0])+ hh+mm+ss; // padding
	  };
