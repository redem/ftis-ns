/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Jun 2016     amu
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	
	
	

}



function afterSubmit_updateTransaction(type){
	
	 var context = nlapiGetContext();
	 
	//var cRec = nlapiGetNewRecord();
	//var tranType		= cRec.getFieldText("custrecord130");
	//var tranInternalId	= cRec.getFieldValue("custrecord131");
	var maxRecords = 1000;
	
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('custrecord132', null, 'is', 'F');// processed
	filters[filters.length] = new nlobjSearchFilter('custrecord134', null, 'isnot', 'transferorder');
	filters[filters.length] = new nlobjSearchFilter('custrecord137', null, 'is', 'F'); // do not process
	
	var columns = new Array();
	
	columns[columns.length] = new nlobjSearchColumn("custrecord131");
	
	
	var cRecs = nlapiSearchRecord('customrecord411', null, filters, columns);
	if(cRecs){
		nlapiLogExecution('Debug', 'Cust REC Results', " Count "+ cRecs.length);
		
		for(var i=0; i< cRecs.length && i< maxRecords; i++)
			{
				
				var cRec = cRecs[i];
				var cRecId= cRec.getId();
				
				
				var tranInternalId	= cRec.getValue("custrecord131");
				var recordType		= nlapiLookupField('transaction',tranInternalId, 'recordtype');
				
				if(recordType == "transferorder") {
					
					nlapiSubmitField('customrecord411', cRecId, 'custrecord134',  recordType);
					continue;
				}
				
				nlapiLogExecution('Debug', 'Cust REC DETAILS', 
						"index i :"+ i+ 
						" record type :" + recordType +
						" record id :" + tranInternalId		
				);
			
				var record = nlapiLoadRecord(recordType, tranInternalId);
				//record.setFieldValue('customform',180);
				
				try{
					nlapiSubmitRecord(record, true);
					nlapiSubmitField('customrecord411', cRecId, 'custrecord132',  'T');
				}
				catch(error){
					nlapiLogExecution('Debug','Save Record Errors', error.message);
					nlapiSubmitField('customrecord411', cRecId, 'custrecord133',  error.message);
				}
				//nlapiDeleteRecord('customrecord411', cRecId);
				
				
				if ( context.getRemainingUsage() <= 100 && (i+1) < cRecs.length )
			      {
			         var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId())
			         if ( status == 'QUEUED' )
			            break;     
			      }
				
			}
		
	}
	
}
