
/** User event script to show the Print button */
function beforeLoad(type, form, request) 
{
	var ctx = nlapiGetContext().getExecutionContext();
	if(type=='view' && ctx=='userinterface') 
	{
		var buttonName = nlapiGetContext().getSetting('script', 'custscript_slpb_button_name');
		var templateFileId = nlapiGetContext().getSetting('script', 'custscript_slpb_template_file');
		var savedSearch1 = nlapiGetContext().getSetting('script', 'custscript_slpb_savedsearch_1') || '';
		var savedSearch2 = nlapiGetContext().getSetting('script', 'custscript_slpb_savedsearch_2') || '';
		
		var printURL = nlapiResolveURL('suitelet', 'customscript_sl_customprint', 'customdeploy_sl_customprint');
		printURL+= '&tpl='+templateFileId + '&rectype=' + nlapiGetRecordType() + '&recid=' + nlapiGetRecordId() +
					'&search1='+savedSearch1+'&search2='+savedSearch2;
		
		buttonName = buttonName || 'Custom Print';
		
		form.addButton('custpage_printbtn', buttonName, "window.open('"+printURL+"')");
	}
	
}


/** Given record info and a PDF template file, generate a PDF */
function main(request, response) 
{
	// Get some parameters needed to generate PDF
	var recordType = request.getParameter('rectype');
	var recordId = request.getParameter('recid');
	var pdfTemplateId = request.getParameter('tpl');
	var savedSearch1 = request.getParameter('search1');
	var savedSearch2 = request.getParameter('search2');
	
	// Check if there is a template id given
	if(isEmpty(pdfTemplateId)) {
		response.write('PDF Template is not specified');
		return;
	}
	
	// Load NetSuite data
	var record = nlapiLoadRecord(recordType, recordId);
	
	// Load some data from saved search results
	var results_1 = executeSavedSearch(savedSearch1);
	var results_2 = executeSavedSearch(savedSearch2);
	
	// Read the template
	var template = nlapiLoadFile(pdfTemplateId).getValue();
	
	// Replace tags on the template
	var renderer = nlapiCreateTemplateRenderer();
	renderer.setTemplate(template);
	renderer.addRecord('record', record);
	if(results_1) {
		renderer.addSearchResults('results', results_1);
	}
	if(results_2) {
		renderer.addSearchResults('results_2', results_2);
	}
	var xml = renderer.renderToString();
	
	// Convert the template to PDF and send as response
	var file = nlapiXMLToPDF(xml);
	response.setContentType('PDF', recordType+'_'+recordId+'.pdf', 'inline');
	response.write(file.getValue());
}


function executeSavedSearch(savedSearchId) {
	if(isEmpty(savedSearchId)) {
		return null;
	}
	
	nlapiLogExecution('debug', 'executeSavedSearch', 'savedSearchId('+savedSearchId+')');
	var search = nlapiLoadSearch(null, savedSearchId);
	var searchType = search.getSearchType();
	var results = nlapiSearchRecord(searchType, savedSearchId);
	nlapiLogExecution('debug', 'executeSavedSearch', 'Found '+(results?results.length:0)+' results');
	
	return results;
}

