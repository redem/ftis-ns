/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Jul 2017     NEOH
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	var ctx = nlapiGetContext();
	 this.request = request;
	 this.response = response;
	 this.runFunction = function() {
         if (this.request.getMethod() == 'GET') {
             this.GET();
         } else {
             this.POST();
         }
     };
     this.GET = function() {  
    	 var form = nlapiCreateForm("QR CODE");
 
    	 form.addField('custpage_sales_order', 'text', 'Name ', null); 

    	 var so = form.addField('custpage_sample', 'select', 'Sales order ', 'transaction'); 
    	 so.setDisplayType('hidden');
    	 form.setScript('customscript1228');
    	 this.response.writePage(form);
     }
}
function runForm(request,response){
	   new suitelet(request,response).runFunction();
	 }