/**
 * 
 */


function updateSalesOrder_afterSubmit(type){
	
	var deposit = nlapiGetNewRecord();
	var soId	= deposit.getFieldValue('salesorder');
	var so		= nlapiLoadRecord('salesorder', soId);
	var requireCustomerPayment = so.getFieldValue('custbody_requireadvpayment');
	var orderstatus	= so.getFieldValue('orderstatus');
	
	var debug 	='soId : '+ soId + '\n' +
				'requireCustomerPayment : ' + requireCustomerPayment ;
	
	if(type !='delete'){
		
			
		if(requireCustomerPayment == 'T' && orderstatus == 'A'){
			so.setFieldValue('orderstatus', 'B');
			so.setFieldValue('custbody_depositstatus', 'T');
			
			var id = nlapiSubmitRecord(so, true);
			
			debug +='updated sales order id: ' + id;
		}		
	}
	
	if(type =='delete'){
		
		if(requireCustomerPayment == 'T' && orderstatus == 'B'){
			so.setFieldValue('orderstatus', 'A');
			so.setFieldValue('custbody_depositstatus', 'F');
			
			var id = nlapiSubmitRecord(so, true);
			
			debug +='updated sales order id: ' + id;
		}
	}
	
	nlapiLogExecution('Debug', 'Deposit Details ', debug );
	
	return true;
}