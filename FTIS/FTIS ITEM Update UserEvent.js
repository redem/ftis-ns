/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 May 2016     amu
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	
	nlapiLogExecution('Debug', 'BEFORELOAD', type);
	
	if(type != 'create') return true;
	var record = nlapiGetNewRecord();
	
	try{
		record.setFieldValue('supplyreplenishmentmethod','TIME_PHASED');// default the supply replenishment method field
		//record.setFieldValue('leadtime', 'F');
		//var replenishmentmethod =
	
	}
	catch(error){
		var body = "Error Details " + error.message;
		body += " item id " + record.getId();
		
		//nlapiSendEmail(-5, 'amu_ftis@hotmail.com', 'Error in Item Update', body , null, null, null, null, null, null, null);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	try{
		var details ='';
		var record = nlapiGetNewRecord();
		
		
		var distributioncategory= record.getFieldValue('custitem_ftis_distribution_category');
		var distributionnetwork	= record.getFieldValue('custitem_ftis_distribution_network');
		
		details += '\ndistributioncategory ' + distributioncategory;
		details += '\distributionnetwork ' + distributionnetwork;
		
		nlapiLogExecution('Debug', 'Distributionn Fields', details);
		
		//record.setFieldValue('distributioncategory',distributioncategory);
		//record.setFieldValue('distributionnetwork',distributionnetwork);
		
		updateCustomFields(record);
	}catch(error){
		nlapiLogExecution('ERROR', 'ERROR ITEM UEBEFORESUB', JSON.stringify(error));
	}
	
}


function userEventAfterSubmit(type){
	
	
	
	  
}


function onPageInit(){
	
	
	//alert("PageInit");
	//nlapiSetFieldValue("supplyreplenishmentmethod","TIME_PHASED");
	//nlapiSetFieldValue('leadtime', 'F');
	return true;
}
function onFieldChanged(type, name){
	
	//if(name =='supplyreplenishmentmethod') alert(name);
	
	return true;
}


function onSaveRecord(){
	
	//alert("onSave");
	
	return true;
}



function updateCustomFields(record){
	
	var usdbaseprice = getUSDBasePrice(record);
	if(usdbaseprice ==null || usdbaseprice ==undefined || usdbaseprice ==''){usdbaseprice =0;};
	record.setFieldValue('custitem_to_sell_price_usd_baseprice', usdbaseprice);
	
	
}

function getUSDBasePrice(record){
	var priceID	= 'price1'; // Internal ID of USD is 1
	
	record.selectLineItem(priceID, 1); 
	var basePrice = record.getCurrentLineItemMatrixValue(priceID, 'price', 1);
	nlapiLogExecution('Debug', 'Item Pricing ', basePrice);
	
	return basePrice;
}
