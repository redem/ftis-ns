/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Mar 2018     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	try{
		var tdate = nlapiGetFieldValue('trandate');
		var currency = nlapiGetFieldValue('currency');
		var rest = getRate1(tdate,currency);
		var exrates = rest[0].custrecord_cur_sell;
		var exrateb = rest[0].custrecord_cur_buy;
		nlapiSetFieldValue('exchagerate', exrates);
		nlapiLogExecution('ERROR', exrates, esrateb);
		
	}catch(ex){
		nlapiLogExecution('ERROR', 'error bforsubmit', ex);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	
	
	
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	try{
		var tdate = nlapiGetFieldValue('trandate');
		var currency = nlapiGetFieldValue('currency');
		var rest = getRate1(tdate,currency);
		var exrates = rest[0].custrecord_cur_sell;
		var exrateb = rest[0].custrecord_cur_buy;
		var idd = nlapiGetRecordId();
		nlapiLogExecution('ERROR','aft',idd);
		nlapiSubmitField('vendorbill',idd,'exchangerate', exrates, false);
		
	}catch(ex){
		nlapiLogExecution('ERROR', 'error bforsubmit', ex);
	}
	
}


function getRate1(trandate,currency){
	var customrecord_ft_curSearch = nlapiSearchRecord("customrecord_ft_cur",null,
	[
	   ["custrecord_cur_cur","anyof",currency], 
	   "AND", 
	   ["custrecord_cur_efdate","on",trandate]
	], 
	[
	   new nlobjSearchColumn("custrecord_cur_cur",null,null), 
	   new nlobjSearchColumn("custrecord_cur_exdate",null,null).setSort(false), 
	   new nlobjSearchColumn("custrecord_cur_efdate",null,null), 
	   new nlobjSearchColumn("custrecord_cur_sell",null,null), 
	   new nlobjSearchColumn("custrecord_cur_buy",null,null)
	]
	);
	var cont = [];
	for(var x=0; x<customrecord_ft_curSearch.length; x++){
		var data = {};
		data.custrecord_cur_cur = customrecord_ft_curSearch[x].getValue('custrecord_cur_cur');
		data.custrecord_cur_exdate = customrecord_ft_curSearch[x].getValue('custrecord_cur_exdate');
		data.custrecord_cur_efdate = customrecord_ft_curSearch[x].getValue('custrecord_cur_efdate');
		data.custrecord_cur_sell = customrecord_ft_curSearch[x].getValue('custrecord_cur_sell');
		data.custrecord_cur_buy = customrecord_ft_curSearch[x].getValue('custrecord_cur_buy');
		cont.push(data);
	}
	
	return cont;
}