/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Mar 2018     noeh.canizares
 *
 */



/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
		var subs = nlapiGetFieldValue('subsidiary');
		if(subs == '3'){
			
				var tdate = nlapiGetFieldValue('trandate');
				var currency = nlapiGetFieldValue('currency');
				var rest = getRate1(tdate,currency);
				var exrates = rest[0].custrecord_cur_sell;
				var exrateb = rest[0].custrecord_cur_buy;
				nlapiSetFieldValue('exchangerate',exrates);
				nlapiLogExecution('ERROR', 'xx',exrates);
		}
		return true;
	
}



function getRate1(trandate,currency){
	var customrecord_ft_curSearch = nlapiSearchRecord("customrecord_ft_cur",null,
	[
	   ["custrecord_cur_cur","anyof",currency], 
	   "AND", 
	   ["custrecord_cur_efdate","on",trandate]
	], 
	[
	   new nlobjSearchColumn("custrecord_cur_cur",null,null), 
	   new nlobjSearchColumn("custrecord_cur_exdate",null,null).setSort(false), 
	   new nlobjSearchColumn("custrecord_cur_efdate",null,null), 
	   new nlobjSearchColumn("custrecord_cur_sell",null,null), 
	   new nlobjSearchColumn("custrecord_cur_buy",null,null)
	]
	);
	var cont = [];
	for(var x=0; x<customrecord_ft_curSearch.length; x++){
		var data = {};
		data.custrecord_cur_cur = customrecord_ft_curSearch[x].getValue('custrecord_cur_cur');
		data.custrecord_cur_exdate = customrecord_ft_curSearch[x].getValue('custrecord_cur_exdate');
		data.custrecord_cur_efdate = customrecord_ft_curSearch[x].getValue('custrecord_cur_efdate');
		data.custrecord_cur_sell = customrecord_ft_curSearch[x].getValue('custrecord_cur_sell');
		data.custrecord_cur_buy = customrecord_ft_curSearch[x].getValue('custrecord_cur_buy');
		cont.push(data);
	}
	
	return cont;
}
