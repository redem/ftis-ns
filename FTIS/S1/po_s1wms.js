/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Sep 2017     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	if(type != 'delete'){
		var count = nlapiGetLineItemCount('item');
		for(var selecteditem=1; selecteditem<=count; selecteditem++){
			var fromLocation = nlapiGetFieldValue('location');
			nlapiSelectLineItem('item', selecteditem);
			var linelocation = nlapiGetCurrentLineItemValue('item','location');
			fromLocation =(fromLocation)?fromLocation:linelocation;
			
			nlapiLogExecution('DEBUG', 'fromLocation', fromLocation);
			nlapiSetFieldValue('location',fromLocation);
			if(fromLocation!=null&&fromLocation!="")
			{
				var itemId = nlapiGetCurrentLineItemValue('item','item');
				var searchresult = SetItemStatus("PurchaseOrder",itemId,fromLocation,null);
				if(searchresult != null && searchresult != '')
				{	
					nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',searchresult[0],false);
					nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',searchresult[1],false);
					nlapiCommitLineItem('item');
				}	
			}	
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	if(type != 'delete'){
		var id = nlapiGetRecordId();
		var rec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());		
	}
  
}



