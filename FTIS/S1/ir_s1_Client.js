/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Dec 2017     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
   nlapiLogExecution('Debug',type,type);
}

function bobso(){
	//var headers = {"User-Agent-x": "SuiteScript-Call",
    //        "Content-Type": "application/jsonp"};
	var url = nlapiResolveURL('SUITELET','customscript_bob_ss','customdeploy_bob_ss');
	var cfm = nlapiGetFieldValue('createdfrom');
    window.open(url +'&method=irtoso'+'&cfm='+cfm+'&type='+nlapiGetRecordType() + '&id='+nlapiGetRecordId(),'_blank');
    window.focus();
	//var data = nlapiRequestURL(purl,null ,headers, null, 'GET');
	//var data = nlapiRequestURL(purl,{'type':nlapiGetRecordType(),'id':nlapiGetRecordId()} ,headers, null, 'GET');
	// POST var data = nlapiRequestURL(purl,JSON.stringify(pstdata));
}

