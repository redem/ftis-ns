/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Jan 2018     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @returns {Void}
 */
function clientPostSourcing(type, name) {
   if(type == 'item'){
	 
	  
		   if(name == 'item'){
			   var itemid = nlapiGetCurrentLineItemValue(type, name);
			   if(itemid){
				   var itemdesc = nlapiLookupField('item', itemid, 'custitem_ftis_item_description');
				   nlapiSetCurrentLineItemValue(type, 'custcol_ftis_item_description', itemdesc);
			   }
		   }
	   
		 
	  
   }
}

