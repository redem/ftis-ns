/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Dec 2017     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function poS1BF(type, form, request){
	try{
		nlapiLogExecution('ERROR', 'wew', type);
		
		var rectype = nlapiGetRecordType();
		nlapiLogExecution('ERROR', 'rectype', rectype);
		if(rectype === 'purchaseorder'){
				
			var rec = nlapiGetNewRecord();
			var subs = rec.getFieldValue('subsidiary');
			if(subs != '7'){
				form.setScript('customscript_ft_bob_po_cs1');
				nlapiLogExecution('ERROR', 'rec', rec);
				if(type == 'view'){
					var ent = rec.getFieldValue('entity');
					nlapiLogExecution('ERROR', 'rectype', ent);
					var locs = rec.getFieldValue('location');
					nlapiLogExecution('ERROR', 'locs', locs);
									
					if(ent != '161'){
						var id = nlapiGetRecordId();
						var defstat = ["Fully Billed","Pending Bill","Closed"];
						
						nlapiLogExecution('ERROR', subs, subs);
						
						var stats = rec.getFieldValue('status');
						
						if(locs == '33' || locs == '16'){
							//if(!_.contains(defstat,stats)){
								
									form.addButton('custpage_bopo','BOB PO','bobpo');
								
							//}
							
							
							form.addTab('custpage_tab_bob', 'BOB Related Record');
							
							var bolist =  form.addSubList('custpage_bob_list','list', 'BOB FT Related Transaction','custpage_tab_bob');
							bolist.addField('custpage_bob_relid', 'text', 'Purchase Order').setDisplayType('normal');
							bolist.addField('custpage_bob_pdate', 'date', 'Purcahse Order Date');
							bolist.addField('custpage_bob_pstatus', 'text', 'Purcahse Order Status');
							
							bolist.addField('custpage_bob_so', 'text', 'Sales Order').setDisplayType('normal');
							bolist.addField('custpage_bob_sdate', 'date', 'Sales Order Date');
							bolist.addField('custpage_bob_sstatus', 'text', 'Sales Order Status');
							
							var result = [];
							result = getRelatedRec(id);
							
							nlapiLogExecution('Error','result',JSON.stringify(result));
							
							if(result.length){
								var y = 1;
								for(var x=0;x<result.length;x++){
									var res = result[x];
									var viewUrl = nlapiResolveURL('RECORD', 'purchaseorder', res.custpage_bob_relid).toString();
								   
									bolist.setLineItemValue('custpage_bob_relid',y, '<a href='+viewUrl+' target="_blank">'+res.tranid+'</a>');
									bolist.setLineItemValue('custpage_bob_pdate', y, res.custpage_bob_pdate);
									bolist.setLineItemValue('custpage_bob_pstatus', y, res.custpage_bob_pstatus);
									var soref = '/app/accounting/transactions/salesord.nl?id='+res.custpage_bob_so;
									var slink = (soref != 'undifined')?'<a href='+soref+' target="_blank">'+res.custpage_sotranid+'</a>':' ';
									
									bolist.setLineItemValue('custpage_bob_so', y,slink);
									bolist.setLineItemValue('custpage_bob_sdate', y, res.custpage_bob_sdate||'');
									bolist.setLineItemValue('custpage_bob_sstatus', y, res.custpage_bob_sstatus||'');
									y++;
									
								}
							}
						}
								
					}
				}
			
				if(type == 'create'){
					var loc = request.getParameter('loc');
					var cfm = request.getParameter('cfm');
					
					if(cfm && loc){
						rec.setFieldValue('location',loc);
						rec.setFieldValue('custbody_ftis_interco_tran',cfm);
						var thaddress = nlapiLookupField('customer','1565','shipaddress');
						rec.setFieldValue('shipaddress', thaddress);
						
						var crec = nlapiLoadRecord('purchaseorder', cfm);
						var citem = [];
						var crecount = crec.getLineItemCount('item');
						
						for(var w = 1; w<=crecount;w++){
							var data ={};
							data.line =w;
							data.item = crec.getLineItemValue('item', 'item', w);
							data.quantity = crec.getLineItemValue('item', 'quantity', w) - crec.getLineItemValue('item', 'quantityreceived', w);
							var bprice = nlapiLookupField('item',data.item,'baseprice');
							nlapiLogExecution('ERROR','bprice',bprice);
							var fprice = bprice * .955;
							nlapiLogExecution('ERROR','fprice',fprice);
							data.rate = fprice;
							data.units = crec.getLineItemValue('item', 'units', w);
							data.custcol_brand = crec.getLineItemValue('item', 'custcol_brand', w);
							data.custcol_ftis_item_description = crec.getLineItemValue('item', 'custcol_ftis_item_description', w);
							data.custcol_ftis_etawarehouse = crec.getLineItemValue('item', 'custcol_ftis_etawarehouse', w);
							data.custcol_country_of_origin = crec.getLineItemValue('item', 'custcol_country_of_origin', w);
							data.class = crec.getLineItemValue('item', 'class', w);
							data.custcol_request_date = crec.getLineItemValue('item', 'custcol_request_date', w);
							data.custcol_ftis_vesseletd = crec.getLineItemValue('item', 'custcol_ftis_vesseletd', w);
							data.custcol_ftis_exfactory = crec.getLineItemValue('item', 'custcol_ftis_exfactory', w);
							data.custcol_ftis_etawarehouse = crec.getLineItemValue('item', 'custcol_ftis_etawarehouse', w);
							data.custcol_ft_req_date_wh_arr = crec.getLineItemValue('item', 'custcol_ft_req_date_wh_arr', w);
							nlapiLogExecution('ERROR','data',data);
							if(data.quantity > 0){
								citem.push(data);
							}
							
						}
						var z = 1;
						for(var t = 0; t < citem.length;t++){
							var datas = citem[t];
							rec.setLineItemValue('item','item',z, datas.item);
							var itemdesc = nlapiLookupField('item', datas.item, 'custitem_ftis_item_description');
							rec.setLineItemValue('item', 'custcol_ftis_item_description', z,itemdesc);
							rec.setLineItemValue('item', 'quantity',z, datas.quantity);
							rec.setLineItemValue('item', 'rate',z, datas.rate);
							rec.setLineItemValue('item', 'taxcode',z,53);
							rec.setLineItemValue('item', 'units',z, datas.units);
							rec.setLineItemValue('item', 'custcol_brand',z, datas.custcol_brand);
							rec.setLineItemValue('item', 'custcol_ftis_item_description',z, datas.custcol_ftis_item_description);
							rec.setLineItemValue('item', 'custcol_ftis_etawarehouse',z, datas.custcol_ftis_etawarehouse);
							rec.setLineItemValue('item', 'custcol_country_of_origin',z, datas.custcol_country_of_origin);
							rec.setLineItemValue('item', 'class',z, datas.class);
							rec.setLineItemValue('item', 'custcol_request_date',z, datas.custcol_request_date);
							rec.setLineItemValue('item', 'custcol_ftis_vesseletd',z, datas.custcol_ftis_vesseletd);
							rec.setLineItemValue('item', 'custcol_ftis_exfactory',z, datas.custcol_ftis_exfactory);
							rec.setLineItemValue('item', 'custcol_ftis_etawarehouse',z, datas.custcol_ftis_etawarehouse);
							rec.setLineItemValue('item', 'custcol_ft_req_date_wh_arr',z, datas.custcol_ft_req_date_wh_arr);
							z++;
						}	
					}
				}
			}
		}
	}catch(exx){
		nlapiLogExecution('ERROR','error before load bob', exx);
	}
}


function getRelatedRec(id){
	var result = nlapiSearchRecord("purchaseorder",null,
	[
	   ["type","anyof","PurchOrd"], 
	   "AND", 
	   ["custbody_ftis_interco_tran.internalidnumber","equalto",id], 
	   "AND", 
	   ["mainline","is","T"], 
	   "AND", 
	   ["mainname","anyof","161"], 
	   "AND", 
	   ["custbody_ftis_interco_tran.mainline","is","T"], 
	   "AND", 
	   ["taxline","is","F"]
	], 
	[
	   new nlobjSearchColumn("internalid",null,null), 
	   new nlobjSearchColumn("tranid",null,null), 
	   new nlobjSearchColumn("trandate",null,null).setSort(false), 
	   new nlobjSearchColumn("statusref",null,null), 
	   new nlobjSearchColumn("intercotransaction",null,null)
	]
	);
	var cont = [];
	if(result){
		for(var x=0; x< result.length; x++){
			var data = {};
			var res = result[x];
			data.tranid = res.getValue('tranid');
			data.custpage_bob_relid = res.getValue('internalid');
			data.custpage_bob_pdate = res.getValue('trandate');
			data.custpage_bob_pstatus = res.getValue('statusref');
			var iso = res.getValue('intercotransaction');
			if(iso){
				var dso = nlapiLookupField('salesorder', iso, ['tranid','statusref','trandate']);
				
				data.custpage_bob_so =iso;
				data.custpage_sotranid = dso.tranid;
				data.custpage_bob_sdate = dso.trandate;
				data.custpage_bob_sstatus = dso.statusref;
			}
			cont.push(data);
		}
	}

	
	
	return cont;
	
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	try{
		var rec = nlapiGetNewRecord();
		nlapiLogExecution('ERROR', 'SO AFT', rec);
		var rectype = nlapiGetRecordType();
		if(type == 'create'){
			if(rectype === 'salesorder'){
				var intercopo = rec.getFieldValue('intercotransaction');
				if(intercopo){
					nlapiSubmitField(nlapiGetRecordType(),nlapiGetRecordId(),['custbody_ftis_interco_tran','custbody_ftis_incoterm_tran'],[intercopo,2]);	
				}
			}
			
		}
	}catch(ex){
		nlapiLogExecution('ERROR', 'ERROR SO AFT', ex);
	}
	
}