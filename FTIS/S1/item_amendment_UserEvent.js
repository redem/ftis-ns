/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Nov 2017     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	if(type=='create' || type=='edit') 
	{
		var record = nlapiGetNewRecord();
		var oldRecord = nlapiGetOldRecord();
				
		var nchanges = _getChanges(record);
		var ochanges = _getChanges(oldRecord);
		
		var addhc = JSON.stringify(nchanges) != JSON.stringify(ochanges);
		if(addhc)
		{
			var changesNew = difference(nchanges, ochanges);
			var changesOld = difference(ochanges, nchanges);
			
			if(changesNew.length >= changesOld.length) {
				for ( var i = 0; i < changesNew.length; i++) 
				{
					var change = changesNew[i];
					var oldChange = _.where(ochanges, { v: change.v, s: change.s }); // makes vendor, subsidiary, currency unique        
					var isAdded = !oldChange || oldChange.length==0;
					if(isAdded) {
						acreateLog(change, null, LOG_ACTION_ADD);
					} else {
						acreateLog(change, oldChange[0], LOG_ACTION_UPDATE);
					}
					nlapiLogExecution('debug', 'afterSubmit', 'isAdded('+isAdded+') ' + JSON.stringify(change));
				}
			}
			
			// Something was deleted
			if(changesNew.length < changesOld.length) {
				for ( var i = 0; i < changesOld.length; i++) 
				{
					var change = changesOld[i];
					var oldChange = _.where(nchanges, { v: change.v, s: change.s }); // makes vendor, subsidiary, currency unique
					var isDeleted = !oldChange || oldChange.length==0;
					if(isDeleted) {
						acreateLog(change, null, LOG_ACTION_REMOVED);
					}
					nlapiLogExecution('debug', 'afterSubmit', 'isDeleted('+isDeleted+') ' + JSON.stringify(change));
				}
			}
		}
		
		var rnchanges = _getChangesftrem(record);
		var rochanges = _getChangesftrem(oldRecord);
		
		var addhcrm = JSON.stringify(rnchanges) != JSON.stringify(rochanges);
		if(addhcrm)
		{
			var changesNew = difference(rnchanges, rochanges);
			var changesOld = difference(rochanges, rnchanges);
			
			if(changesNew.length >= changesOld.length) {
				for ( var i = 0; i < changesNew.length; i++) 
				{
					var change = changesNew[i];
					var oldChange = _.where(rochanges, { v: change.v, s: change.s }); // makes vendor, subsidiary, currency unique        
					var isAdded = !oldChange || oldChange.length==0;
					if(isAdded) {
						acreateLog(change, null, LOG_ACTION_ADD);
					} else {
						acreateLog(change, oldChange[0], LOG_ACTION_UPDATE);
					}
					nlapiLogExecution('debug', 'afterSubmit', 'isAdded('+isAdded+') ' + JSON.stringify(change));
				}
			}
			
			// Something was deleted
			if(changesNew.length < changesOld.length) {
				for ( var i = 0; i < changesOld.length; i++) 
				{
					var change = changesOld[i];
					var oldChange = _.where(rnchanges, { v: change.v, s: change.s }); // makes vendor, subsidiary, currency unique
					var isDeleted = !oldChange || oldChange.length==0;
					if(isDeleted) {
						acreateLog(change, null, LOG_ACTION_REMOVED);
					}
					nlapiLogExecution('debug', 'afterSubmit', 'isDeleted('+isDeleted+') ' + JSON.stringify(change));
				}
			}
		}
		
	
	}
}

function _getChanges(record) 
{	
	var cfields = ['custitem_ft_price_amendment_remark','vendorcode'];
	var ch = [];
	
	if(record==null) {
		return changes;
	}
	
	var tab = 'itemvendor';
	var lines = record.getLineItemCount(tab);
	for ( var i = 1; i <= lines; i++) 
	{
		//var priceText = record.getLineItemValue(tab, 'vendorprices', i); // example data => THB: 1000.00|USD: 300.00
		var vendorId = record.getLineItemValue(tab, 'vendor', i);
		var subsidiaryId = record.getLineItemValue(tab, 'subsidiary', i);
		var vendorName = record.getLineItemText(tab, 'vendor', i);
		var vendorCode = record.getLineItemValue(tab,'vendorcode',i);
	
		if(!isEmpty(vendorCode)) 
		{
			ch.push({ 
				i: record.getId(), 
				v: vendorId, 
				vn: vendorName,
				f:'Vendor Code',
				p: vendorCode, 
				s: subsidiaryId
			});
			ch = _.sortBy(ch, function(obj){
				return obj.v + obj.s;
			});
		}
			
	}
		
	return ch;
}

function _getChangesftrem(record) 
{	
	var cfields = ['custitem_ft_price_amendment_remark','vendorcode'];
	var ch = [];
	
	if(record==null) {
		return changes;
	}
	
	
	var premarks = record.getFieldValue('custitem_ft_price_amendment_remark');
	var subs = record.getFieldValue('subsidiary');
	if(!isEmpty(premarks)) {
		ch.push({
			i: record.getId(),
			i: record.getId(), 
			v: '', 
			vn: '',
			f:'FT PRICE AMENDMENT REMARK',
			p: premarks,
			s: subs
		});
	}
	
	return ch;
}






function acreateLog (newChange, oldChange, actionType,field) 
{	
	nlapiLogExecution('debug', 'createLog', 'newChange => ' + JSON.stringify(newChange));
	nlapiLogExecution('debug', 'createLog', 'oldChange => ' + JSON.stringify(oldChange));
	
	var record = nlapiCreateRecord('customrecord_f_item_vendor_price_log');
	record.setFieldValue('custrecord_ivp_item', newChange.i);
	record.setFieldValue('custrecord_ivp_vendor', newChange.v);
	record.setFieldValue('custrecord_ivp_subsidiary', newChange.s);
	if(actionType==LOG_ACTION_UPDATE) {
		record.setFieldValue('custrecord_ivp_old_val', oldChange.p || '');
	}
	record.setFieldValue('custrecord_ivp_field', newChange.f);
	record.setFieldValue('custrecord_ivp_new_val', newChange.p);
	record.setFieldValue('custrecord_ivp_action_type', actionType);
	
	var logId = nlapiSubmitRecord(record, false, true);
	
	var actionLabel = {
		LOG_ACTION_ADD : 'Add',
		LOG_ACTION_UPDATE : 'Update',
		LOG_ACTION_REMOVED : 'Remove',
	};
	nlapiLogExecution('audit', 'createLog', 'Created log('+logId+') Action: '+ (actionLabel[actionType] || 'None'));
	
	
}



/** Performs a deep comparison of JSON objects */
var difference = function(array) {
	var rest = Array.prototype.concat.apply(Array.prototype, Array.prototype.slice.call(arguments, 1));
	var containsEquals = function(obj, target) {
		if (obj == null) {
			return false;
		}
		return _.any(obj, function(value) {
			return _.isEqual(value, target);
		});
	};
	return _.filter(array, function(value) {
		return !containsEquals(rest, value);
	});
};