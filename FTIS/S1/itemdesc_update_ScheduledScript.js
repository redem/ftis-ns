/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Jan 2018     noeh.canizares
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	
		//var id =['629972','630308','630331','630361','630384','630389','630393','630397','630405','630425','630426','630427','630527','630812','630819','630822','630842','630846','630848','630850'];
		var id = ['629971','630096','630127','630129','630130','630131','630171','630299','630315','630319','630326','630329','630337','630338','630345','630346','630357','630379','630391','630399','630419','630421','630544','630569','630570','630671','630681','630815','630816','630837','630847','630954'];
		for(var x =0; x < id.length; x++){
				try{
					var rec = nlapiLoadRecord('salesorder', id[x]);
					var itemcount = rec.getLineItemCount('item');
					if(itemcount){
						for(var y=1; y <=itemcount; y++){
							rec.selectLineItem('item',y);
							var item = rec.getCurrentLineItemValue('item','item');
							if(item){
								var itemdesc = nlapiLookupField('item',item,'custitem_ftis_item_description');
								if(itemdesc){
									rec.setCurrentLineItemValue('item', 'custcol_ftis_item_description',itemdesc);
									rec.commitLineItem('item',true);
								}
							}
						}
						var recs = nlapiSubmitRecord(rec,false,false);
						nlapiLogExecution('ERROR', 'recs', recs);
					}
				}catch(eex){
					nlapiLogExecution('ERROR', 'eex ' +id[x], eex);
				}
			}
			
		
	
	
}
