/**
 * 
 */

function onBeforeSubmit(type){
	var ctx = nlapiGetContext().getExecutionContext();
	
	//if(ctx != 'csvimport') return true;
	
	var record 		= nlapiGetNewRecord();
	var recordType 	= record.getRecordType();
	var recordid 	= record.getId();
	var subsidiary 	= record.getFieldValue('subsidiary');
	var isDelete	= record.getFieldValue('custbody_delete');
	var location	= record.getFieldValue('location');
	if(isDelete !='T') return true;
	
	nlapiLogExecution("Debug", "Details", 
			" subsidiary "	+ subsidiary + 
			" recordtype "	+ recordType + 
			" location " 	+ location);
	
	if( subsidiary == 3 || subsidiary == 4){
		var itemCount = record.getLineItemCount('item');
		
		//if(location =='' || location == null) 
		{ 
			if(subsidiary == '4') {
				record.setFieldValue('location', '14');	// OR 5 FT-TH
				for(var i=1; i<=itemCount; i++){
					record.setLineItemValue('item','item', i, '4297'); // TEST AMU ITEM
					record.setLineItemValue('item','amount', i, '0'); 
					record.setLineItemValue('item','location', i, '14'); 
					
				}
			}
			
			if(subsidiary == '3') {
				record.setFieldValue('location', '5');	// OR 5 FT-TH
				for(var i=1; i<=itemCount; i++){
					record.setLineItemValue('item','item', i, '4297'); // TEST AMU ITEM
					record.setLineItemValue('item','amount', i, '0'); 
					record.setLineItemValue('item','location', i, '5'); 
					
				}
			}
			
		}
		
		record.setFieldValue('intercotransaction', '');
	
	
	}
	
	
}


function onAfterSubmit(type){
	
	var record 		= nlapiGetNewRecord();
	var recordType 	= record.getRecordType();
	var recordid 	= record.getId();
	var subsidiary 	= record.getFieldValue('subsidiary');
	var isDelete	= record.getFieldValue('custbody_delete');
	if(isDelete !='T') return true;
	
	nlapiDeleteRecord(recordType, recordid);
	
}