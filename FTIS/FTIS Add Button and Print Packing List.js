/**
 * 
 */

var environment = nlapiGetContext().getEnvironment();

function onBeforeLoad(type, form)
{
	if(type!='view') return true;
	var sub = nlapiGetFieldValue('subsidiary');
	if(sub != '7'){
		form.setScript("1104");	
		form.addButton("custpage_printpackinglist","Print Packing List", "printPackingList()");
	}

}




function printPackingList()
{
	
	var itemFulfillmentId = nlapiGetRecordId();	
	var orderId	= nlapiGetFieldValue('createdfrom');
	
	var url = nlapiResolveURL('SUITELET', 'customscript_packingslist_pdf_sl', "customdeploy_packingslist_pdf_sl", false);
	
	var params = '&orderId='+orderId+ '&itemfulfillmentId='+itemFulfillmentId;
	//alert(url+params);
	
	window.open( url+params,'_blank');
	
	//NLInvokeButton(document.getElementById('print'));	
	nlapiRequestURL( url, null, params, null);

	
	
}