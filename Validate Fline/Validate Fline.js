function validateFLineBeforeSubmit(type){
	try{
		var subs = nlapiGetFieldValue('subsidiary');
		if(subs != '7'){
			if (type=='create'){
				//add validation only for customer
				var cust = nlapiGetFieldValue('entity');
				if(cust != '1565'){
					var lineItemCount = nlapiGetLineItemCount('item');
					for (var i = 1; i <= lineItemCount; i++){
					var fline = nlapiGetLineItemValue('item','custcol_fulfillment_number',i);
					var itemtype = nlapiGetLineItemValue('item','itemtype',i);
						if ((itemtype == 'InvtPart' || itemtype == 'Assembly') && fline == null){
						var error = nlapiCreateError('Error','Please enter missing Fulfillment # (Line).');	
						throw error;
						}
					}	
				}
			}
		}
	}catch(ex){
		nlapiLogExecution('ERROR', 'ex invoice', ex);
	}
	
}