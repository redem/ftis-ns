/**
 * Copyright NetSuite, Inc. 2013 All rights reserved. 
 * The following code is a demo prototype. Due to time constraints of a demo,
 * the code may contain bugs, may not accurately reflect user requirements 
 * and may not be the best approach. Actual implementation should not reuse 
 * this code without due verification.
 * 
 * (Module description here. Whole header length should not exceed 
 * 100 characters in width. Use another line if needed.)
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Feb 2017     gbautista
 * 
 */


    var SUBLIST = 'custpage_list';
	var SS_ITEM_BALANCE = 'customsearch_ft_th_item_bal_2';
	var FG_SEARCH = 'custpage_search_group';
	
	var FLD_START_DATE = 'custpage_date_from';
	var FLD_END_DATE = 'custpage_date_to';
	var FLD_LOCATION = 'custpage_location';
	var FLD_ITEM = 'custpage_item';
	
	var NUM_MAX_ROWS_PER_PAGE = 50;

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @return {void} Any output is written via response object
 */
function itemBalance(request, response) {
	//create form
    var form = nlapiCreateForm('Item Balance');
	
	var aFields = new Array();
	var aFilters = new Array();
	
	//create buttons
    var btnSubmit = form.addSubmitButton('Submit');
	
	//create fieldgroups
	var searchGroup = form.addFieldGroup(FG_SEARCH, 'Search');
	
	var fldStart = form.addField(FLD_START_DATE,'date','Start Date',null,FG_SEARCH);
	var fldtem = form.addField(FLD_ITEM,'select','Item','item',FG_SEARCH);
	var fldEnd = form.addField(FLD_END_DATE,'date','End Date',null,FG_SEARCH);
	var fldLoc = form.addField(FLD_LOCATION,'select','Location','location',FG_SEARCH);
	
	fldtem.setMandatory(true);
	fldLoc.setMandatory(true);
	
	var sublist = form.addSubList(SUBLIST, 'staticlist', 'Item Balance');
	
	//add fields to the sublist
    sublist.addField('custcol_line_number','text','Line Number');
    var colItem = sublist.addField('custcol_item', 'select', 'Item', 'item');
	colItem.setDisplayType('inline');
	//sublist.addField('custcol_onhand', 'text', 'On Hand');
    sublist.addField('custcol_transit', 'text', 'In Transit');
    sublist.addField('custcol_trandate', 'date', 'Transaction Date');
	sublist.addField('custcol_trantype', 'text', 'Transaction Type');
	//var colLoc = sublist.addField('custcol_location', 'select', 'Location', 'location');
	//colLoc.setDisplayType('inline');
	sublist.addField('custcol_docnum', 'text', 'Document Number');
    sublist.addField('custcol_reqdate', 'date', 'Request Date');
	sublist.addField('custcol_condate', 'date', 'Confirmed Date');
	sublist.addField('custcol_quantity', 'text', 'Quantity');
	sublist.addField('custcol_runbalance', 'text', 'Running Balance');
	
	if(request.getMethod() == 'POST') {
		
		var nRunning = 0;
		if(request.getParameter(FLD_ITEM) != null) {
			fldtem.setDefaultValue(request.getParameter(FLD_ITEM));
			aFilters.push(new nlobjSearchFilter('item',null,'is',request.getParameter(FLD_ITEM)));
		}
		
		if(request.getParameter(FLD_LOCATION) != null) {
			fldLoc.setDefaultValue(request.getParameter(FLD_LOCATION));
			aFilters.push(new nlobjSearchFilter('inventorylocation','item','is',request.getParameter(FLD_LOCATION)));
			aFilters.push(new nlobjSearchFilter('location',null,'is',request.getParameter(FLD_LOCATION)));
		}
		
		if(request.getParameter(FLD_START_DATE) != null && request.getParameter(FLD_START_DATE) != '') {
			fldStart.setDefaultValue(request.getParameter(FLD_START_DATE));
			aFilters.push(new nlobjSearchFilter('trandate',null,'onorafter',request.getParameter(FLD_START_DATE)));
		}
		
		if(request.getParameter(FLD_END_DATE) != null && request.getParameter(FLD_END_DATE) != '') {
			fldEnd.setDefaultValue(request.getParameter(FLD_END_DATE));
			aFilters.push(new nlobjSearchFilter('trandate',null,'onorbefore',request.getParameter(FLD_END_DATE)));
		}
		
		var aResult = nlapiSearchRecord('transaction',SS_ITEM_BALANCE, aFilters);
	
		if(aResult != null) {
			var aColumns = aResult[0].getAllColumns();
			nlapiLogExecution('ERROR','result',aResult.length);
			nRunning = Number(aResult[0].getValue(aColumns[1]));
			sublist.setLabel(aResult[0].getText(aColumns[0]) + " - On Hand: " + nRunning);
			for(var i = 0; i < aResult.length; i++) {
				sublist.setLineItemValue('custcol_item', i+1, aResult[i].getValue(aColumns[0]));
				//sublist.setLineItemValue('custcol_onhand', i+1, aResult[i].getValue(aColumns[1]));
				sublist.setLineItemValue('custcol_transit', i+1, aResult[i].getValue(aColumns[2]));
				sublist.setLineItemValue('custcol_trandate', i+1, aResult[i].getValue(aColumns[3]));
				sublist.setLineItemValue('custcol_trantype', i+1, aResult[i].getText(aColumns[4]));
				//sublist.setLineItemValue('custcol_location', i+1, aResult[i].getValue(aColumns[5]));
				sublist.setLineItemValue('custcol_docnum', i+1, aResult[i].getValue(aColumns[5]));
				sublist.setLineItemValue('custcol_condate', i+1, aResult[i].getValue(aColumns[6]));
				sublist.setLineItemValue('custcol_reqdate', i+1, aResult[i].getValue(aColumns[7]));
				sublist.setLineItemValue('custcol_quantity', i+1, aResult[i].getValue(aColumns[8]));
                sublist.setLineItemValue('custcol_line_number', i+1, aResult[i].getValue(aColumns[9]));
				nRunning += Number(aResult[i].getValue(aColumns[8]));

				sublist.setLineItemValue('custcol_runbalance', i+1, nRunning.toFixed(0));
			}
		}
	}
	
	response.writePage(form);
}

/*
//sample client side script to null action and perform another load (e.g. to update form values and not perform submit action)
function fieldChanged(type, name) {
    if (name=='custpage_fld1' || name=='custpage_fld2') {

        nlapiSetFieldValue('custpage_action','');
        //need to use form submit to avoid the form modified warning message
        var button = document.forms['main_form'].elements['submitter'];
        if (button) {
            button.click();
        } else {
            document.forms[0].submit();
        }
    }
}*/

