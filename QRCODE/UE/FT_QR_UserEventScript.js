/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/search'],
/**
 * @param {runtime} runtime
 * @param {search} search
 */
function(runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	var form = scriptContext.form;
    	var newr = scriptContext.newRecord;
    	
       	if(scriptContext.type == 'view'){
    		nlog('ctx type');   
    		var rec = nyapiIn(newr);
    		var recFields = rec.fields;
    		var sublist = rec.sublists;
    		var items = sublist.item;
    		var recItem = getItem(items);
    		var validate  = false;
    		nlog('recFields',recFields);
    		if(recFields.entity == '1556'){
    			
    			//var givenItems = ["4244","4250","4243"];
    			var givenItems = getHaier();
    			nlog('debug',givenItems);
        		for(var x = 0; x < recItem.length; x++){
        			if( givenItems.inArray(recItem[x]) ){
        				validate = true;
        			}
        		}
    		}

    		if(validate){
				f = "24545";
				form.clientScriptFileId = f;
            	form.addButton({
            	    id : 'custpage_qr',
            	    label : 'Print QR',
            	    functionName : 'doSomething'
            	});
            	form.addButton({
            	    id : 'custpage_pkh',
            	    label : 'Packing List Haier',
            	    functionName : 'fpkh'
            	});
            	
            	var spk = form.getButton({
            	    id : 'custpage_printpackinglist'
            	});
            	spk.isHidden = true;
    		}
        	
       	}
    	
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {

    }

    function getHaier(){
    	var customrecord_haier_pnSearchObj = search.create({
    		   type: "customrecord_haier_pn",
    		   filters:[],
    		   columns: [
    		      search.createColumn({
    		         name: "custrecord_item",
    		         sort: search.Sort.ASC
    		      })
    		   ]
    		});
    		var container = [];
    		customrecord_haier_pnSearchObj.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			container.push(result.getValue('custrecord_item'));
    		   return true;
    		});
    		
    		return container;
    	
    }
     
    
    function nlog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    
    function getItem(obj){
    	
    	var item = [];
    	for(line in obj){
    		item.push(obj[line].item);
    	}
    	return item;
    }
    	
    function nyapiIn(nsObj){
    	var tos = JSON.stringify(nsObj);
    	return JSON.parse(tos);
    }
    
    Array.prototype.inArray = function (value)
    {
     // Returns true if the passed value is found in the
     // array. Returns false if it is not.
     var i;
     for (i=0; i < this.length; i++)
     {
       if (this[i] == value)
       {
         return true;
       }
     }
     return false;
    };
    
    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});