/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/runtime','N/url'],
/**
 * @param {runtime} runtime
 * @param {url} url
 */
function(runtime, url) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {

    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    
    }
    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function doSomething(scriptContext){
    	var itfid = window.nlapiGetRecordId();
    	var param = {
    			ifid: itfid,
    			p:true
    	};
    	var output = url.resolveScript({
    	    scriptId: 'customscript_sl_qrcode',
    	    deploymentId: 'customdeploy_sl_qrcode',
    	    params: param
    	});
    	window.open(output, '_self');
 	    window.focus();
    }
    
    function fpkh(scriptContext){
    	var itfid = window.nlapiGetRecordId();
    	var param = {
    			ifid: itfid
    	};
    	var output = url.resolveScript({
    	    scriptId: 'customscript_ft_pl_haeir_ss',
    	    deploymentId: 'customdeploy_ft_pl_haeir_ss',
    	    params: param
    	});
    	window.open(output);
 	    window.focus();
    }
    
    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        doSomething: doSomething,
        fpkh:fpkh
    };
    
});