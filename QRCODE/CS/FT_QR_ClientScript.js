/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */

define(['N/url','N/http','N/currentRecord','N/runtime','N/search'],
/**
 * @param {url} url
 * @param {http} http
 * @param {search} search
 */
function(url,http,currentRecord,runtime,search) {
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
	
    function pageInit(scriptContext) {
    	NS.jQuery('head').append('<link rel="stylesheet" href="https://system.sandbox.netsuite.com/core/media/media.nl?id=23293&c=240300&h=4932d5003c9410b69f8f&_xt=.css"/>');
    	NS.jQuery('head').append('<script src="https://system.sandbox.netsuite.com/core/media/media.nl?id=22393&c=240300&h=364f407621f472e08807&_xt=.js"></script>');
    	NS.jQuery('head').append('<script src="https://system.sandbox.netsuite.com/core/media/media.nl?id=22493&c=240300&h=270c758813909da9e3e2&_xt=.js"></script>');
    	NS.jQuery('head').append('<script src="https://system.sandbox.netsuite.com/core/media/media.nl?id=21993&c=240300&h=9bd051fa46bbb001d9f8&_xt=.js"></script>');  

      	var crec = scriptContext.currentRecord;
     	if (window.onbeforeunload){
     	   window.onbeforeunload=function() { null;};
     	};
     	var data = nyapiS();
    	var option = {
    			data: data,
    		    getValue: "name",
    		    requestDelay: 300,
    		    placeholder: "Enter and Select Sales Order Document Number",
    		    list: {
    	 	    	match: {
    	 				enabled: true
    	 			},
    	 			sort: {
    	 				enabled: true
    	 			},
    	 			onClickEvent: function() {
    	 				var sotext =  NS.jQuery("#custpage_sales_order").getSelectedItemData().name;
    					var value = NS.jQuery("#custpage_sales_order").getSelectedItemData().internalid;
    				    nyapisfv(crec,'custpage_sample',value);
    		        	if(value && sotext){
    		        		var param = {
    		            			soid: value,
    		            			record:sotext
    		            	};
    		            	var output = url.resolveScript({
    		            	    scriptId: 'customscript_sl_qrcode',
    		            	    deploymentId: 'customdeploy_sl_qrcode',
    		            	    params: param
    		            	});
    		            	console.log(output);
    		            	location = output;
    		        	}
    				},
    				onLoadEvent : function() {
    					nyapisfv(crec,'custpage_sample','');
    				}
    			},
    			theme: "square"
    		};
    	NS.jQuery("#custpage_sales_order").easyAutocomplete(option);

    
    
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	var crec = scriptContext.currentRecord;
    	var custf = scriptContext.fieldId;
    }
    
    function nyapigfv(crec,fld){
    	var res = crec.getValue({
            fieldId: fld
        });
    	return res;
    }
    
    function nyapisfv(crec,fld,val){
    	crec.setValue({
            fieldId: fld,
            value: val
        });
    	return;
    }
    function nyapiS(){
    	var wew = [];
    	var salesorderSearchObj = search.create({
    		   type: "salesorder",
    		   filters: [
    		      ["type","anyof","SalesOrd"], 
    		      "AND", 
    		      ["item","anyof","4244","4250","4243"], 
    		      "AND", 
    		      ["mainline","is","F"]
    		   ],
    		   columns: [
				 search.createColumn({
				     name: "internalid",
				     summary: "GROUP"
				 }),
    		      search.createColumn({
    		         name: "tranid",
    		         summary: "GROUP"
    		      })
    		   ]
    		});
    		salesorderSearchObj.run().each(function(result){
    		   var res = {internalid:result.getValue({name:'internalid',summary:search.Summary.GROUP}) , name:result.getValue({name:'tranid',summary:search.Summary.GROUP})};
    		   wew.push(res);
    		  return true;
    		});
    		return wew;
    }
    
    
    function bclick(){
        var record = currentRecord.get();
    	var so = nyapigfv(record,'custpage_sample');
    	var sotext = nyapigfv(record,'custpage_sales_order');
    	if(so && sotext){
    		var param = {
        			soid: so,
        			record:sotext
        	};
        	var output = url.resolveScript({
        	    scriptId: 'customscript_sl_qrcode',
        	    deploymentId: 'customdeploy_sl_qrcode',
        	    params: param
        	});
        	location = output;
        	
    	}else{
    		alert("please in enter a valid so");
    	}
    }
    function nyapiS(){
    	var wew = [];
    	var salesorderSearchObj = search.create({
    		   type: "salesorder",
    		   filters: [
    		      ["type","anyof","SalesOrd"], 
    		      "AND", 
    		      ["item","anyof","4244","4250","4243"], 
    		      "AND", 
    		      ["mainline","is","F"]
    		   ],
    		   columns: [
				 search.createColumn({
				     name: "internalid",
				     summary: "GROUP"
				 }),
    		      search.createColumn({
    		         name: "tranid",
    		         summary: "GROUP"
    		      })
    		   ]
    		});
    		salesorderSearchObj.run().each(function(result){
    		   var res = {internalid:result.getValue({name:'internalid',summary:search.Summary.GROUP}) , name:result.getValue({name:'tranid',summary:search.Summary.GROUP})};
    		   wew.push(res);
    		  return true;
    		});
    		return wew;
    }
    
    
    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        bclick:bclick
       };
    
});
