/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define([ 'N/ui/serverWidget', 'N/runtime', 'N/url', 'N/file',
		'SuiteScripts/QRCODE/lib/trimpath.js' ],
/**
 * @param {serverWidget} serverWidget
 * @param {runtime} runtime
 * @param {url} url
 * @param {file} file
 */
function(serverWidget, runtime, url, file) {

	/**
	 * Definition of the Suitelet script trigger point.
	 *
	 * @param {Object} context
	 * @param {ServerRequest} context.request - Encapsulation of the incoming request
	 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
	 * @Since 2015.2
	 */
	function onRequest(context) {

		this.request = context.request;
		this.response = context.response;

		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};

		this.GET = function() {
			var param = this.request.parameters;
			var ses = runtime.getCurrentSession();

			var nszebcon = file.load('26096');
			var nszcontent = nszebcon.getContents();

			var form = serverWidget.createForm({
				title : 'FTIS QR CODE GENERATOR'
			});
			var tso = form.addField({
				id : 'custpage_ext_page',
				label : 'qz',
				type : serverWidget.FieldType.INLINEHTML
			});
			tso.defaultValue = nszcontent;

			this.response.writePage(form, true);
		};

		this.POST = function() {

		};
	}
	function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
	function setGo(context) {
		new onRequest(context).runFunction();
	}
	return {
		onRequest : setGo
	};

});
