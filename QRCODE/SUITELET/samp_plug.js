/**
 * @NApiVersion 2.0
 * @NScriptType plugintypeimpl
 */
define(['N/file', 'N/record', 'N/runtime', 'N/search'],
/**
 * @param {file} file
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(file, http, record, runtime, search) {
	
    return {
    	first: function(){
    		return "sample first";
    	},
    	second: function(){
    		return "sample second";
    	}    
        
    };
    
});
