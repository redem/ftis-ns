/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define(
		[ 'N/ui/serverWidget', 'N/search', 'N/runtime', 'N/record', 'N/log',
				'N/file', 'N/render', 'N/record',
				'SuiteScripts/QRCODE/lib/trimpath.js',
				'SuiteScripts/QRCODE/lib/qrenc3.js' ],

		/**
		 * @param {runtime}
		 *            runtime
		 * @param {search}
		 *            search
		 * @param {record}
		 *            record
		 * @param {log}
		 *            log
		 * @param {file}
		 *            file
		 * @param {render}
		 *            render
		 * @param {record}
		 *            record
		 */
		function(serverWidget, search, runtime, record, log, file, render,
				record) {

			/**
			 * Definition of the Suitelet script trigger point.
			 * 
			 * @param {Object}
			 *            context
			 * @param {ServerRequest}
			 *            context.request - Encapsulation of the incoming
			 *            request
			 * @param {ServerResponse}
			 *            context.response - Encapsulation of the Suitelet
			 *            response
			 * @Since 2015.2
			 */
			function onRequest(context) {
				this.request = context.request;
				this.response = context.response;

				this.runFunction = function() {
					if (this.request.method == 'GET') {
						this.GET();
					} else {
						this.POST();
					}
				};
				this.GET = function() {
					var param = this.request.parameters;
				
					if (param.ifid && param.p == 'true') {
					
						var f = "24447";
						
						var temp = file.load(f);
						var content = temp.getContents();

						
						var xmlStr = file.load('27837');
			            var renderer = render.create();
			            renderer.templateContent = xmlStr.getContents();
			            
			            renderer.addCustomDataSource({
			                format: render.DataSource.OBJECT,
			                alias: "JS",
			                data: outtemp
			                });
			            
			            
			            var newfile = renderer.renderAsPdf();
			            this.response.writeFile(newfile,true); 		



						this.response.writeLine(content);
					} else {
						var form = serverWidget.createForm({
							title : 'FTIS QR CODE GENERATOR'
						});
						//form.clientScriptFileId = 21893;
						var tso = form.addField({
							id : 'custpage_sales_order',
							label : 'Sales Order it',
							type : serverWidget.FieldType.TEXT
						});
						tso.isMandatory = true;
						if (param.record) {
							tso.defaultValue = param.record;
						}

						var cust_so = form.addField({
							id : 'custpage_sample',
							label : 'Sales Order',
							type : serverWidget.FieldType.SELECT,
							source : 'salesorder'
						});
						if (param.soid && param.record) {
							cust_so.defaultValue = param.soid;
						}
						cust_so.updateDisplayType({
							displayType : serverWidget.FieldDisplayType.HIDDEN
						});
						var list = form.addSublist({
							id : 'custpage_sublist',
							type : serverWidget.SublistType.LIST,
							label : 'Item Fulfillment'
						});
						list.addField({
							id : 'custpage_tranid',
							type : serverWidget.FieldType.TEXT,
							label : 'Item Fulfillment'
						});
						list.addField({
							id : 'custpage_status',
							type : serverWidget.FieldType.TEXT,
							label : 'Status'
						});

						list.addField({
							id : 'custpage_subsidiary',
							type : serverWidget.FieldType.TEXT,
							label : 'Subsidiary'
						});
						list.addField({
							id : 'custpage_trandate',
							type : serverWidget.FieldType.DATE,
							label : 'Date'
						});
						// if (param.soid) {
						form.addButton({
							id : 'custpage_print',
							label : 'Preview',
							functionName : 'bclick'
						});
						// }

						this.response.writePage(form);
					}
				};
				this.POST = function() {
					var ss = this.request.parameters;
					this.response.write(ss);
				};
			}

			function nyapiGetIf(id) {
				var wew = [];
				var itemfulfillmentSearchObj = search.create({
					type : "itemfulfillment",
					filters : [ [ "type", "anyof", "ItemShip" ], "AND",
							[ "internalid", "anyof", id ], "AND",
							[ "cogs", "is", "F" ] ],
					columns : [ search.createColumn({
						name : "item"
					}), search.createColumn({
						name : "tranid"
					}), search.createColumn({
						name : "quantity"
					}), search.createColumn({
						name : "quantity",
						join : "inventoryDetail"
					}), search.createColumn({
						name : "inventorynumber",
						join : "inventoryDetail"
					}) ]
				});
				itemfulfillmentSearchObj.run().each(function(result) {

					var items = result.getValue({
						name : 'item'
					});
					var internalid = result.getValue({
						name : 'tranid'
					});
					var haier = "";
					
					var customrecord_haier_pnSearchObj = search.create({
						   type: "customrecord_haier_pn",
						   filters: [
						      ["custrecord_item","anyof",items]
						   ],
						   columns: [
						      search.createColumn({
						         name: "name",
						         sort: search.Sort.ASC
						      })
						   ]
						});
						customrecord_haier_pnSearchObj.run().each(function(res){
						   // .run().each has a limit of 4,000 results
							haier = res.getValue('name');
						   return true;
						});
						
					var intmask = Math.floor(internalid);
					var res = {
						
						internalid : pad(intmask,6) ,
						item : items,
						haier : haier,
						quantity : result.getValue({
							name : 'quantity'
						}),
						dquanity : result.getValue({
							name : 'quantity',
							join : 'inventoryDetail'
						}),
						inventorynumber : result.getText({
							name : 'inventorynumber',
							join : 'inventoryDetail'
						}),

					};
					wew.push(res);
					return true;
				});
				return wew;
			}

			function nyapiLoad(type, id) {
				var rec = record.load({
					type : type,
					id : id,
					isDynamic : true
				});

				return rec;
			}

			function nyapiS() {
				var wew = [];
				var salesorderSearchObj = search.create({
					type : "salesorder",
					filters : [ [ "type", "anyof", "SalesOrd" ], "AND",
							[ "item", "anyof", "4244", "4250", "4243" ], "AND",
							[ "mainline", "is", "F" ] ],
					columns : [ search.createColumn({
						name : "internalid",
						summary : "GROUP"
					}), search.createColumn({
						name : "tranid",
						summary : "GROUP"
					}) ]
				});
				salesorderSearchObj.run().each(function(result) {
					var res = {
						internalid : result.getValue({
							name : 'internalid',
							summary : search.Summary.GROUP
						}),
						name : result.getValue({
							name : 'tranid',
							summary : search.Summary.GROUP
						})
					};
					wew.push(res);
					return true;
				});
				return wew;
			}
			function setGo(context) {
				new onRequest(context).runFunction();
			}
			function nlog(title_log, details_log) {
				log.debug({
					title : title_log,
					details : details_log
				});
			}

			/** start * */
			function getcart(items) {

				var sval = ("undefined" === typeof items) ? false : true;
				if (sval) {
					var finalc = {};
					var s = calculatecartoons(items);

					var temps = s.upstep;

					var temps2 = s.downstep;
					
					nlog('temps',JSON.stringify(temps));
					nlog('temps2',JSON.stringify(temps2));
					
					finalc.upstep = temps;
					finalc.downstep = temps2;
					
					
					var me = getInternalCart(finalc);
					
					return me;
				}

				return "invalid request";

			}

	
		function newggdcart(zz,counter) {
			
				var downstep2 = zz;
				downstep2 = downstep2.sort(function(a, b) { // sort high to low
					return parseFloat(b.dquanity) - parseFloat(a.dquanity);
				});
				nlog('thiszz',JSON.stringify(downstep2));
				counter = (counter) ? counter : 1;
				var dcont = [];
				for (var y = 0; y < downstep2.length; y++) {
					if (dcont.length) {
						var cnt = true;
						for (var x = 0; x < dcont.length; x++) {
							if (dcont[x].item == downstep2[y].item) {
								if ("undefined" != typeof dcont[x].internal) {
									var qty = parseInt(downstep2[y].dquanity);
									var qtycount = qty / 1600;
									var qtymod = (qty % 1600) ? 1 : 0;
									var innerboxes = Math.floor(qtycount)
											+ qtymod;
									var toroll = qty;
									var intbox = dcont[x].internal.length
											+ innerboxes;

									if (qty < 6400 && intbox <= 4) {
										for (var w = 0; w < innerboxes; w++) {
											var inqty = 1600;
											if (toroll >= 1600) {
												toroll = toroll - 1600;
											} else {
												inqty = toroll;
											}
											var internal = {};
											var tocount = pad(counter,2);
											internal.internalid = dcont[x].internalid
													+ "" + tocount;
											internal.item = dcont[x].item;
											internal.haier = dcont[x].haier;
											internal.quantity = dcont[x].quantity;
											internal.origdquanity = qty;
											internal.dquanity = inqty;
											/**internal.inventorynumber = dcont[x].inventorynumber
													+ ", "
													+ downstep2[y].inventorynumber;**/
											internal.inventorynumber =  downstep2[y].inventorynumber;
											internal.qr = "dintqr" + counter;
											dcont[x].internal.push(internal);
											counter++;
										}
										dcont[x].dquanity = parseInt(dcont[x].dquanity)
												+ parseInt(downstep2[y].dquanity);
										dcont[x].origdquanity = dcont[x].origdquanity
												+ " + "
												+ downstep2[y].origdquanity;
										dcont[x].inventorynumber = dcont[x].inventorynumber
										+ ", "
										+ downstep2[y].inventorynumber;
										cnt = false;
									}
								}
							}
						}

						if (cnt) {

							var container = {};
							container.internalid = downstep2[y].internalid;
							container.item = downstep2[y].item;
							container.haier = downstep2[y].haier;
							container.quantity = downstep2[y].quantity;
							container.origdquanity = downstep2[y].origdquanity;
							container.dquanity = parseInt(downstep2[y].dquanity);
							container.inventorynumber = downstep2[y].inventorynumber;
							container.qr = "dqr" + y;
							container.internal = [];

							var qty = parseInt(downstep2[y].dquanity);
							var qtycount = qty / 1600;
							var qtymod = (qty % 1600) ? 1 : 0;
							var innerboxes = Math.floor(qtycount) + qtymod;
							var toroll = qty;
							if (qty < 6400 && innerboxes <= 4) {
								for (var w = 0; w < innerboxes; w++) {
									var inqty = 1600;
									if (toroll >= 1600) {
										toroll = toroll - 1600;
									} else {
										inqty = toroll;
									}
									var internal = {};
									var tocount = pad(counter,2);
									internal.internalid = downstep2[y].internalid
											+ "" + tocount;
									internal.item = downstep2[y].item;
									internal.haier = downstep2[y].haier;
									internal.quantity = downstep2[y].quantity;
									internal.origdquanity = qty;
									internal.dquanity = inqty;
									internal.inventorynumber = downstep2[y].inventorynumber;
									internal.qr = "dintqr" + counter;
									container.internal.push(internal);
									counter++;
								}
							}
							dcont.push(container);
						}
					} else {
						var container = {};
						container.internalid = downstep2[y].internalid;
						container.item = downstep2[y].item;
						container.haier = downstep2[y].haier;
						container.quantity = downstep2[y].quantity;
						container.origdquanity = downstep2[y].origdquanity;
						container.dquanity = parseInt(downstep2[y].dquanity);
						container.inventorynumber = downstep2[y].inventorynumber;
						container.qr = "dqr" + y;
						container.internal = [];

						var qty = parseInt(downstep2[y].dquanity);
						var qtycount = qty / 1600;
						var qtymod = (qty % 1600) ? 1 : 0;
						var innerboxes = Math.floor(qtycount) + qtymod;
						var toroll = qty;
						if (qty < 6400) {
							for (var w = 0; w < innerboxes; w++) {
								var inqty = 1600;
								if (toroll >= 1600) {
									toroll = toroll - 1600;
								} else {
									inqty = toroll;
								}
								var internal = {};
								var tocount = pad(counter,2);
								internal.internalid = downstep2[y].internalid
										+ "" + tocount;
								internal.item = downstep2[y].item;
								internal.haier = downstep2[y].haier;
								internal.quantity = downstep2[y].quantity;
								internal.origdquanity = qty;
								internal.dquanity = inqty;
								internal.inventorynumber = downstep2[y].inventorynumber;
								internal.qr = "dintqr" + counter;
								container.internal.push(internal);
								counter++;
							}
						}
						dcont.push(container);
					}

				}
				return dcont;
			}

	
			function downcart(zz, counter) {
			
				var downstep2 = zz;
				downstep2 = downstep2.sort(function(a, b) { // sort high to low
					return parseFloat(b.dquanity) - parseFloat(a.dquanity);
				});

				var dcont = [];
				for (var y = 0; y < downstep2.length; y++) {
					counter = (counter) ? counter : 1;
					if (dcont.length) {
						var cnt = true;
						for (var x = 0; x < dcont.length; x++) {
							if (dcont[x].item == downstep2[y].item) {
								if ("undefined" != typeof dcont[x].internal) {
									var qty = parseInt(downstep2[y].dquanity);
									var qtycount = qty / 1600;
									var qtymod = (qty % 1600) ? 1 : 0;
									var innerboxes = Math.floor(qtycount)
											+ qtymod;
									var toroll = qty;
									var intbox = dcont[x].internal.length
											+ innerboxes;

									if (qty < 6400 && intbox <= 4) {
										if (dcont[x].inventorynumber == downstep2[y].inventorynumber) {
											for (var w = 0; w < innerboxes; w++) {
												var inqty = 1600;
												if (toroll >= 1600) {
													toroll = toroll - 1600;
												} else {
													inqty = toroll;
												}
												var internal = {};
												var tocount = pad(counter,2);
												internal.internalid = dcont[x].internalid
														+ "" + tocount;
												internal.item = dcont[x].item;
												internal.haier = dcont[x].haier;												
												internal.quantity = dcont[x].quantity;
												internal.origdquanity = qty;
												internal.dquanity = inqty;
												internal.inventorynumber = dcont[x].inventorynumber;
												internal.qr = "dintqr"
														+ counter;

												dcont[x].internal
														.push(internal);
												counter++;
											}

										} else {
											for (var w = 0; w < innerboxes; w++) {
												var inqty = 1600;
												if (toroll >= 1600) {
													toroll = toroll - 1600;
												} else {
													inqty = toroll;
												}
												var internal = {};
												var tocount = pad(counter,2);
												internal.internalid = downstep2[y].internalid
														+ "" + tocount;
												internal.item = downstep2[y].item;
												internal.haier = downstep2[y].haier;												
												internal.quantity = downstep2[y].origdquanity;
												internal.origdquanity = qty;
												internal.dquanity = inqty;
												internal.inventorynumber = downstep2[y].inventorynumber;
												internal.qr = "dintqr"
														+ counter;

												dcont[x].internal
														.push(internal);
												counter++;
											}
										}
										dcont[x].dquanity = parseInt(dcont[x].dquanity)
												+ parseInt(downstep2[y].dquanity);
										dcont[x].origdquanity = dcont[x].origdquanity
												+ " + "
												+ downstep2[y].origdquanity;
										dcont[x].inventorynumber = dcont[x].inventorynumber
												+ ", "
												+ downstep2[y].inventorynumber;

										cnt = false;

									}
								}
							}
						}

						if (cnt) {

							var container = {};
							container.internalid = downstep2[y].internalid;
							container.item = downstep2[y].item;
							container.haier = downstep2[y].haier;
							container.quantity = downstep2[y].quantity;
							container.origdquanity = downstep2[y].origdquanity;
							container.dquanity = parseInt(downstep2[y].dquanity);
							container.inventorynumber = downstep2[y].inventorynumber;
							container.qr = "dqr" + y;
							container.internal = [];

							var qty = parseInt(downstep2[y].dquanity);
							var qtycount = qty / 1600;
							var qtymod = (qty % 1600) ? 1 : 0;
							var innerboxes = Math.floor(qtycount) + qtymod;
							var toroll = qty;
							if (qty < 6400 && innerboxes <= 4) {
								for (var w = 0; w < innerboxes; w++) {
									var inqty = 1600;
									if (toroll >= 1600) {
										toroll = toroll - 1600;
									} else {
										inqty = toroll;
									}
									var internal = {};
									var tocount = pad(counter,2);
									internal.internalid = downstep2[y].internalid
											+ "" + tocount;
									internal.item = downstep2[y].item;
									internal.haier = downstep2[y].haier;									
									internal.quantity = downstep2[y].quantity;
									internal.origdquanity = qty;
									internal.dquanity = inqty;
									internal.inventorynumber = downstep2[y].inventorynumber;
									internal.qr = "dintqr" + counter;
									container.internal.push(internal);
									counter++;
								}
							}
							dcont.push(container);
						}
					} else {
						var container = {};
						container.internalid = downstep2[y].internalid;
						container.item = downstep2[y].item;
						container.haier = downstep2[y].haier;						
						container.quantity = downstep2[y].quantity;
						container.origdquanity = downstep2[y].origdquanity;
						container.dquanity = parseInt(downstep2[y].dquanity);
						container.inventorynumber = downstep2[y].inventorynumber;
						container.qr = "dqr" + y;
						container.internal = [];

						var qty = parseInt(downstep2[y].dquanity);
						var qtycount = qty / 1600;
						var qtymod = (qty % 1600) ? 1 : 0;
						var innerboxes = Math.floor(qtycount) + qtymod;
						var toroll = qty;
						if (qty < 6400) {
							for (var w = 0; w < innerboxes; w++) {
								var inqty = 1600;
								if (toroll >= 1600) {
									toroll = toroll - 1600;
								} else {
									inqty = toroll;
								}
								var internal = {};
								var tocount = pad(counter,2);
								internal.internalid = downstep2[y].internalid
										+ "" + tocount;
								internal.item = downstep2[y].item;
								internal.haier = downstep2[y].haier;
								internal.quantity = downstep2[y].quantity;
								internal.origdquanity = qty;
								internal.dquanity = inqty;
								internal.inventorynumber = downstep2[y].inventorynumber;
								internal.qr = "dintqr" + counter;
								container.internal.push(internal);

								counter++;
							}
						}
						dcont.push(container);
					}

				}
				return dcont;
			}

			function getInternalCart(obj) {
				var layout = [];
				var cart = ("undefined" === typeof obj.upstep) ? []
						: obj.upstep;
				var dcart = ("undefined" === typeof obj.downstep) ? []
						: obj.downstep;
				nlog('mao ni', JSON.stringify(obj));
				nlog('dcart', JSON.stringify(dcart));
				
				if (cart.length) {
					var counter = 1;
					for (var x = 0; x < cart.length; x++) {
						var qty = cart[x].dquanity;
						var qtycount = qty / 1600;
						var qtymod = (qty % 1600) ? 1 : 0;
						var innerboxes = Math.floor(qtycount) + qtymod;
						var toroll = qty;
						var intbox = [];

						for (var z = 0; z < innerboxes; z++) {
							var inqty = 1600;
							if (toroll >= 1600) {
								toroll = toroll - 1600;
							} else {
								inqty = toroll;
							}
							var internal = {};
							var tocount = pad(counter,2);
							internal.internalid = cart[x].internalid + ""
									+ tocount;
							internal.item = cart[x].item;
							internal.haier = cart[x].haier;
							internal.quantity = cart[x].quantity;
							internal.origdquanity = qty;
							internal.dquanity = inqty;
							internal.inventorynumber = cart[x].inventorynumber;
							internal.qr = "innerqrcode" + counter;
							intbox.push(internal);
							counter++;
						}
						cart[x].qr = "outerqrcode" + x;
						cart[x].internal = intbox;
						layout.push(cart[x]);

					}

					/**if (dcart.length) {
						var valdcart = downcart(dcart,counter);
						for (lines in valdcart) {
							layout.push(valdcart[lines]);
						}
					}**/
					

					if (dcart.length) {
						var z = newggdcart(dcart,counter);
						for (lines in z) {
							layout.push(z[lines]);
						}
					}
					
					
					nlog('layout', JSON.stringify(layout));
					return layout;
				} else {
					return "invalid request";
				}

			}

			function calculatecartoons(items) {
				var downstep2 = ("undefined" === typeof items.downstep) ? []
						: items.downstep;
				var bots = items;
				/**bots = bots.sort(function(a, b) { // sort high to low
					return parseFloat(a.inventorynumber) - parseFloat(b.inventorynumber);
				});**/

				
				if (downstep2.length) {
					dstep = ggdcart(downstep2);
					bots.downstep = dstep;
				} else {
					bots = gcart(items);
				}

				return bots;

			}

			function ggdcart(zz) {
			
				var downstep2 = zz;
				downstep2 = downstep2.sort(function(a, b) { // sort high to low
					return parseFloat(b.dquanity) - parseFloat(a.dquanity);
				});
				nlog('thiszz',JSON.stringify(downstep2));
				var dcont = [];
				for (var y = 0; y < downstep2.length; y++) {
					var counter = 1;
					if (dcont.length) {
						var cnt = true;
						for (var x = 0; x < dcont.length; x++) {
							if (dcont[x].item == downstep2[y].item) {
								if ("undefined" != typeof dcont[x].internal) {
									var qty = parseInt(downstep2[y].dquanity);
									var qtycount = qty / 1600;
									var qtymod = (qty % 1600) ? 1 : 0;
									var innerboxes = Math.floor(qtycount)
											+ qtymod;
									var toroll = qty;
									var intbox = dcont[x].internal.length
											+ innerboxes;

									if (qty < 6400 && intbox <= 4) {
										for (var w = 0; w < innerboxes; w++) {
											var inqty = 1600;
											if (toroll >= 1600) {
												toroll = toroll - 1600;
											} else {
												inqty = toroll;
											}
											var internal = {};
											var tocount = pad(counter,2);
											internal.internalid = dcont[x].internalid
													+ "" + tocount;
											internal.item = dcont[x].item;
											internal.haier = dcont[x].haier;
											internal.quantity = dcont[x].quantity;
											internal.origdquanity = qty;
											internal.dquanity = inqty;
											/**internal.inventorynumber = dcont[x].inventorynumber
													+ ", "
													+ downstep2[y].inventorynumber;**/
											internal.inventorynumber =  downstep2[y].inventorynumber;
											internal.qr = "dintqr" + counter;
											dcont[x].internal.push(internal);
											counter++;
										}
										dcont[x].dquanity = parseInt(dcont[x].dquanity)
												+ parseInt(downstep2[y].dquanity);
										dcont[x].origdquanity = dcont[x].origdquanity
												+ " + "
												+ downstep2[y].origdquanity;
										dcont[x].inventorynumber = dcont[x].inventorynumber
										+ ", "
										+ downstep2[y].inventorynumber;
										cnt = false;
									}
								}
							}
						}

						if (cnt) {

							var container = {};
							container.internalid = downstep2[y].internalid;
							container.item = downstep2[y].item;
							container.haier = downstep2[y].haier;
							container.quantity = downstep2[y].quantity;
							container.origdquanity = downstep2[y].origdquanity;
							container.dquanity = parseInt(downstep2[y].dquanity);
							container.inventorynumber = downstep2[y].inventorynumber;
							container.qr = "dqr" + y;
							container.internal = [];

							var qty = parseInt(downstep2[y].dquanity);
							var qtycount = qty / 1600;
							var qtymod = (qty % 1600) ? 1 : 0;
							var innerboxes = Math.floor(qtycount) + qtymod;
							var toroll = qty;
							if (qty < 6400 && innerboxes <= 4) {
								for (var w = 0; w < innerboxes; w++) {
									var inqty = 1600;
									if (toroll >= 1600) {
										toroll = toroll - 1600;
									} else {
										inqty = toroll;
									}
									var internal = {};
									var tocount = pad(counter,2);
									internal.internalid = downstep2[y].internalid
											+ "" + tocount;
									internal.item = downstep2[y].item;
									internal.haier = downstep2[y].haier;
									internal.quantity = downstep2[y].quantity;
									internal.origdquanity = qty;
									internal.dquanity = inqty;
									internal.inventorynumber = downstep2[y].inventorynumber;
									internal.qr = "dintqr" + counter;
									container.internal.push(internal);
									counter++;
								}
							}
							dcont.push(container);
						}
					} else {
						var container = {};
						container.internalid = downstep2[y].internalid;
						container.item = downstep2[y].item;
						container.haier = downstep2[y].haier;
						container.quantity = downstep2[y].quantity;
						container.origdquanity = downstep2[y].origdquanity;
						container.dquanity = parseInt(downstep2[y].dquanity);
						container.inventorynumber = downstep2[y].inventorynumber;
						container.qr = "dqr" + y;
						container.internal = [];

						var qty = parseInt(downstep2[y].dquanity);
						var qtycount = qty / 1600;
						var qtymod = (qty % 1600) ? 1 : 0;
						var innerboxes = Math.floor(qtycount) + qtymod;
						var toroll = qty;
						if (qty < 6400) {
							for (var w = 0; w < innerboxes; w++) {
								var inqty = 1600;
								if (toroll >= 1600) {
									toroll = toroll - 1600;
								} else {
									inqty = toroll;
								}
								var internal = {};
								var tocount = pad(counter,2);
								internal.internalid = downstep2[y].internalid
										+ "" + tocount;
								internal.item = downstep2[y].item;
								internal.haier = downstep2[y].haier;
								internal.quantity = downstep2[y].quantity;
								internal.origdquanity = qty;
								internal.dquanity = inqty;
								internal.inventorynumber = downstep2[y].inventorynumber;
								internal.qr = "dintqr" + counter;
								container.internal.push(internal);
								counter++;
							}
						}
						dcont.push(container);
					}

				}
				return dcont;
			}

			function gcart(items) {
				var upstep2 = [];
				var bots = [];
				var downstep2 = [];

				for (var z = 0; z < items.length; z++) {
					if (items[z].dquanity >= 6400) {
						var calcout = items[z].dquanity / 6400;// default
						var calmod = (calcout > 1) ? items[z].dquanity % 6400
								: 0;
						var calmodbox = (calmod > 0) ? 1 : 0;
						var outerboxes = Math.floor(calcout) + calmodbox;

						var remqty = items[z].dquanity;
						for (var outc = 0; outc < outerboxes; outc++) {
							var ins2 = {};

							if (remqty >= 6400) {
								ins2.internalid = items[z].internalid;
								ins2.item = items[z].item;
								ins2.haier = items[z].haier;
								ins2.quantity = items[z].quantity;
								ins2.origdquanity = items[z].dquanity;
								ins2.dquanity = 6400;
								ins2.inventorynumber = items[z].inventorynumber;
								upstep2.push(ins2);
							} else {
								var tbot = false;
								/*if (bots.length) {
									for (var p = 0; p < bots.length; p++) {
										if (bots[p].item == items[z].item) {
											bots[p].inventorynumber = bots[p].inventorynumber
													+ ","
													+ items[z].inventorynumber;
											bots[p].dquanity = parseInt(bots[p].dquanity)
													+ parseInt(remqty);
											tbot = true;
										}
									}
								}*/
								if (!tbot) {
									ins2.internalid = items[z].internalid;
									ins2.item = items[z].item;
									ins2.haier = items[z].haier;
									ins2.quantity = items[z].quantity;
									ins2.origdquanity = items[z].dquanity;
									ins2.dquanity = remqty;
									ins2.inventorynumber = items[z].inventorynumber;
									bots.push(ins2);
								}
							}
							remqty = remqty - 6400;
						}

					} else {
						var tbot = false;
						if (!tbot) {
							var ins2 = {};
							ins2.internalid = items[z].internalid;
							ins2.item = items[z].item;
							ins2.haier = items[z].haier;
							ins2.quantity = items[z].quantity;
							ins2.origdquanity = items[z].dquanity;
							ins2.dquanity = items[z].dquanity;
							ins2.inventorynumber = items[z].inventorynumber;
							downstep2.push(ins2);

						}
					}
				}

				for (line in bots) {
					downstep2.push(bots[line]);
				}

				var step2 = {};
				step2.upstep = upstep2;
				step2.downstep = downstep2;

				return step2;
			}
			/** end * */
			function nyapiIn(nsObj) {
				var tos = JSON.stringify(nsObj);
				return JSON.parse(tos);
			}
			function getItem(obj) {

				var item = [];
				for (line in obj) {
					item.push(obj[line].item);
				}
				return item;
			}
			function pad(d,n) {
				if(n == 2){
					return (d < 10) ? '0' + d.toString() : d.toString();
				}
				if(n == 6){
					return (d < 10) ? '00000' + d.toString() : (d < 100)? '0000' + d.toString() : (d < 1000) ? '000' + d.toString() : (d < 10000) ? '00' + d.toString() : (d < 100000) ? '0' + d.toString() : d.toString();
				}
			}

			return {
				onRequest : setGo
			};

		});
