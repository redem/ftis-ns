/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperConfig', '../lib/wrapper/9997_NsWrapperRecord'],

function(config, record) {
    var COUNTRY = "country";
    var FILE_FORMAT_REC = "customrecord_2663_payment_file_format";
    var FORMAT_COUNTRY = "custrecord_2663_format_country";
    var DROPDOWN_STATE = "dropdownstate";
    
    // Retrieve company LOVs
    // H5 hack. This lists the countries or states that could be selected in the account
    function retrieve() {
        var lovObj = {};
        
        lovObj.countries = getCountries();
        lovObj.states = getStates();
        
        return lovObj;
    }
    
    var companyInfo;
    function getCompanyInfo() {
        if (!companyInfo) {
            companyInfo = config.load({
                type: config.Type.COMPANY_INFORMATION,
                isDynamic: true
            });
        }
        return companyInfo;
    }
    
    function getCountries() {
        var companyInfo = getCompanyInfo();
        var countryField = companyInfo.getField({
            fieldId: COUNTRY
        });
        
        var companyInfoCountries = countryField.getSelectOptions() || [];
        var dummyFormat = record.create({
            type: FILE_FORMAT_REC,
            isDynamic: true
        });
        countryField = dummyFormat.getField({
            fieldId: FORMAT_COUNTRY
        });
        var formatCountries = countryField.getSelectOptions() || [];
        var countryCodes = {};
        var countries = {};
        
        companyInfoCountries.forEach(function(country) {
            countryCodes[country.text] = country.value;
        });
        
        formatCountries.forEach(function(country) {
            countries[country.value] = {};
            countries[country.value].text = country.text;
            countries[country.value].code = countryCodes[country.text];
        });
        
        return countries;
    }
    
    function getStates() {
        var companyInfo = getCompanyInfo();
        var stateField = companyInfo.getField({
            fieldId: DROPDOWN_STATE
        });
        var companyInfoStates = stateField.getSelectOptions() || [];
        var states = [];
        
        companyInfoStates.forEach(function(state) {
            var tempState = {
                id: state.value,
                text: state.text
            };
            states.push(tempState);
        });
        
        return states;
    }
    
    return {
        retrieve: retrieve,
    };
});
