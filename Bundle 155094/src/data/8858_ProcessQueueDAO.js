/**
 * Copyright 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope Public 
 */

define(['../lib/wrapper/9997_NsWrapperSearch', 
        '../lib/wrapper/9997_NsWrapperRecord', 
        '../lib/9997_DAOFactory',
        '../lib/wrapper/9997_NsWrapperError',
        '../lib/wrapper/9997_NsWrapperRuntime',
        '../data/8858_QueueSettingDAO'],
function(search, record, daoFactory, error, runtime, queueSettingDao) {
    var ERROR_MSG_LOAD_SEARCH = 'Error encountered in loading the saved search PFA Queue.';
    
    var SINGLE_THREAD_PFA_QUEUE = 'customsearch_ep_single_thread_pfa_queue'; 
    
    var RECORD_TYPE = 'customrecord_2663_file_admin';
    var FIELD_MAP = {
        'name': 'name',
        'paymentType': 'custrecord_2663_payment_type',
        'lastProcess': 'custrecord_2663_last_process',
        'fileProcessed': 'custrecord_2663_file_processed',
        'refNote': 'custrecord_2663_ref_note',
        'bankAccount': 'custrecord_2663_bank_account',
        'paymentsForProcessId': 'custrecord_2663_payments_for_process_id',
        'paymentsForProcessAmt': 'custrecord_2663_payments_for_process_amt',
        'processDate': 'custrecord_2663_process_date',
        'postingPeriod': 'custrecord_2663_posting_period',
        'aggMethod': 'custrecord_2663_agg_method',
        'fileRef': 'custrecord_2663_file_ref',
        'account': 'custrecord_2663_account',
        'paymentsForNotify': 'custrecord_2663_payments_for_notify',
        'paymentsForReversal': 'custrecord_2663_payments_for_reversal',
        'department': 'custrecord_2663_department',
        'classification': 'custrecord_2663_class',
        'location': 'custrecord_2663_location',
        'fileCreationDate': 'custrecord_2663_file_creation_date',
        'reprocessFlag': 'custrecord_2663_reprocess_flag',
        'processingErrors': 'custrecord_2663_processing_errors',
        'paymentSubsidiary': 'custrecord_2663_payment_subsidiary',
        'fileCreationTimestamp': 'custrecord_2663_file_creation_timestamp',
        'sequenceId': 'custrecord_2663_sequence_id',
        'lastCheckNo': 'custrecord_2663_last_check_no',
        'priority': 'custrecord_ep_priority',
        'paymentCreationQueue': 'custrecord_ep_payment_creation_queue',
        'batchid': 'custrecord_2663_batchid',
        'exchangeRates': 'custrecord_2663_exchange_rates',
        'reversalDate': 'custrecord_2663_reversal_date',
        'reversalPeriod': 'custrecord_2663_reversal_period',
        'reversalReasons': 'custrecord_2663_reversal_reasons',
        'notificationMails': 'custrecord_2663_notification_mails',
        'notificationSender': 'custrecord_2663_notification_sender',
        'parentDeployment': 'custrecord_2663_parent_deployment',
        'directDebitType': 'custrecord_2663_direct_debit_type',
        'updatedEntities': 'custrecord_2663_updated_entities',
        'batchDetail': 'custrecord_2663_batch_detail',
        'journalKeys': 'custrecord_2663_journal_keys',
        'totalAmount': 'custrecord_2663_total_amount',
        'totalTransactions': 'custrecord_2663_total_transactions',
        'status': 'custrecord_2663_status',
        'number': 'custrecord_2663_number',
        'timeStamp': 'custrecord_2663_time_stamp',
        'removedKeys': 'custrecord_2663_removed_keys',
        'approvalLevel': 'custrecord_2663_approval_level',
        'prevApprovalLevel': 'custrecord_2663_prev_approval_level',
        'aggregate': 'custrecord_2663_aggregate',
        'fromBatch': 'custrecord_2663_from_batch',
        'notificationSubject': 'custrecord_2663_notification_subject',
        'emailReceivers': 'custrecord_2663_email_receivers',
        'ccReceivers': 'custrecord_2663_cc_receivers',
        'bccReceivers': 'custrecord_2663_bcc_receivers',
        'partiallyPaidKeys': 'custrecord_2663_partially_paid_keys',
        'transactionEntities': 'custrecord_2663_transaction_entities',
        'dueBatchDetails': 'custrecord_2663_due_batch_details',
        'updatedCount': 'custrecord_2663_updated_count',
        'paymentsForProcessDis': 'custrecord_2663_payments_for_process_dis',
        'appliedCredits': 'custrecord_2663_applied_credits',
        'appliedCreditsAmt': 'custrecord_2663_applied_credits_amt',
        'batchWithCredit': 'custrecord_2663_batch_with_credit',
        'batchMapEntityAmt': 'custrecord_2663_batch_map_entity_amt',
        'amountMap': 'custrecord_8858_tranline_amt_map',
        'discountMap': 'custrecord_8858_tranline_disc_map',
        'aggregationList': 'custrecord_8858_aggregation_list',
        'usingSStwo' : 'custrecord_8858_is_using_ss_2',
        'isLicensed': 'custrecord_8858_is_licensed',
        'processUser': 'custrecord_8858_process_user'
    };
   
   var daoParams = {
       recordType: RECORD_TYPE,
       fieldMap: FIELD_MAP,
   };
   
   var dao = daoFactory.getDAO(daoParams);
   
   function retrieve(id) {
       return dao.retrieve(id);
   }
   
   function retrieveNsRecord(id) {
       return dao.retrieveNsRecord(id);
   }
    
   function runQueue(subsidiaryFilter) {
       var tranSearch;
       var searchResult = [];
       var loadSearchErrorParams = {
           name: 'EP_PROCESS_QUEUE_SEARCH_ERROR',
           message: ERROR_MSG_LOAD_SEARCH
       };
       
       try{
           tranSearch = search.load({
               id: 'customsearch_8859_process_queue'
           });
           
           if(!tranSearch){
               throw error.create(loadSearchErrorParams);
           }
           
           if (subsidiaryFilter) {
               tranSearch.filters.push(subsidiaryFilter);
           }
           
           searchResult = tranSearch.run().getRange({start: 0, end: 1});
       }
       catch(e){
           log.error(e.name, e.message);
           throw error.logAndCreateError(loadSearchErrorParams.name, loadSearchErrorParams.message);
       }
       
       return searchResult;
   }
   
   function front() {
       var qFront;
       
       /* Retrieval of QueueSetting instance associated with the current deployment*/
       var currentDeployment =  runtime.getCurrentScript().deploymentId;
       log.debug("Current Deployment", runtime.getCurrentScript().deploymentId);
       var qs = queueSettingDao.getByDeployment(currentDeployment);
       
       /* Retrieval of subsidiaries from the PFA instance to be processed */
       var subsidiaryFilter;

       if (!hasQueuedSingleThreadPFA()){
           subsidiaryFilter = createSubsidiaryFilter(qs);
           log.debug("Process Queue Subsidiary Filter", JSON.stringify(subsidiaryFilter))
       }
       
       var searchResult = runQueue(subsidiaryFilter);
       
       if (searchResult.length > 0) {
           var result = searchResult[0];
           
           var qFront = {};
           qFront.id = result.id;
           qFront.name = result.getValue({name: FIELD_MAP.name});
           qFront.paymentType = result.getValue({name: FIELD_MAP.paymentType});
           qFront.paymentSubsidiary = result.getValue({name: FIELD_MAP.paymentSubsidiary});
           qFront.priority = result.getValue({name: FIELD_MAP.priority});
           qFront.lastProcess = result.getValue({name: FIELD_MAP.lastProcess});
           qFront.usingSStwo = result.getValue({name: FIELD_MAP.usingSStwo});
           qFront.isLicensed = result.getValue({name: FIELD_MAP.isLicensed});
           qFront.processUser = result.getValue({name: FIELD_MAP.processUser});
           qFront.lastmodified = result.getValue({name: 'lastmodified'});
               qFront.parentDeployment =  result.getValue({name: FIELD_MAP.parentDeployment});
           }
       
       return qFront;
   }
   
   function createSubsidiaryFilter(qs){
       /* If QueueSetting is Default, PFA to be processed must not be of any subsidiary defined in all QueueSettings
        * Else, PFA must be of any subsidiary defined in the QueueSetting
        * */
       var params;
       var qsSubsidiaries;
       var isDefaultQueueSetting = qs.isDefault;
       var subsidiaryFilter;
       
       if (isDefaultQueueSetting) {
           qsSubsidiaries = getAllQUeueSettingSubsidaries();
           
           params = {name: "custrecord_2663_payment_subsidiary",
                     operator: search.Operator.NONEOF,
                     values: qsSubsidiaries};
       } else {
           qsSubsidiaries = qs.subsidiary.split(",");
           
           params = {name: "custrecord_2663_payment_subsidiary",
                     operator: search.Operator.ANYOF,
                     values: qsSubsidiaries};
       }
       
       if (qsSubsidiaries.length > 0) {
           subsidiaryFilter = search.createFilter(params);
       }
       
       return subsidiaryFilter;
   }
   
   function getAllQUeueSettingSubsidaries(){
       var subsidiaries = []; 
       var qSettings = queueSettingDao.getNonDefaultQueueSettings();
       var subs;
       
       for (var i = 0; i < qSettings.length; i++) {
           subs = qSettings[i].subsidiary;
           
           if (subs) {
               subsidiaries = subsidiaries.concat(subs.split(","));
           }
       }
       
       return subsidiaries;
   }
    
   function hasQueuedSingleThreadPFA(){
       var pfaQueue = search.load({
           id: SINGLE_THREAD_PFA_QUEUE
       });
       
       var searchResult = pfaQueue.run().getRange({start: 0, end: 1}) || [];
       
       return searchResult.length > 0;
   }
   
    return {
        retrieve: retrieve,
        retrieveNsRecord: retrieveNsRecord,
        getFront : front
    };
});
