/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperSearch',
        '../lib/wrapper/9997_NsWrapperError',
        '../app/9997_SearchListIterator'],

function (search, error, searchListIterator) {
    var EMPLOYEE = search.Type.EMPLOYEE;
    var VENDOR = search.Type.VENDOR;
    var CUSTOMER = search.Type.CUSTOMER;

    function retrieveEFTEntities(entityIds, searchColumns) {
        var entityTypes = [EMPLOYEE, VENDOR];

        var entityData = [];
        var listIterator = searchListIterator.create(entityIds);
        while (listIterator.hasNext()) {
            var entityIdsSubset = listIterator.next();

            var entitySubset = retrieveEntities(entityTypes, entityIdsSubset, searchColumns);
            entityData = entityData.concat(entitySubset);
        }

        return orderByList(entityData, entityIds);
    }

    function retrieveDDEntities(entityIds, searchColumns) {
        var entityTypes = [CUSTOMER];

        var entityData = retrieveEntities(entityTypes, entityIds, searchColumns);
        return orderByList(entityData, entityIds);
    }

    function retrieveEntities(entityTypes, entityIds, searchColumns) {
        var filters = [
            ['isinactive', search.Operator.IS, 'F']
        ];
        var columns = [];
        var entityData = [];

        if (entityIds && entityIds.length > 0) {
            log.debug('EntityDAO.retrieveEntities', 'Entity Ids: ' + entityIds.length);
            filters.push('and');
            filters.push(['internalid', search.Operator.ANYOF, entityIds]);
        }

        if (searchColumns) {
            columns = columns.concat(searchColumns);
        }

        if (entityTypes.indexOf(EMPLOYEE) != -1) {
            var employeeColumns = columns;
            employeeColumns = employeeColumns.concat(['socialsecuritynumber']);

            var employeeData = getAllSearchResults(EMPLOYEE, filters, employeeColumns);
            entityData = entityData.concat(employeeData);
        }

        if (entityTypes.indexOf(VENDOR) != -1) {
            var vendorColumns = columns;
            vendorColumns = vendorColumns.concat(['isperson', 'companyname', 'printoncheckas', 'vatregnumber']);

            var vendorData = getAllSearchResults(VENDOR, filters, vendorColumns);
            entityData = entityData.concat(vendorData);
        }

        if (entityTypes.indexOf(CUSTOMER) != -1) {
            var customerColumns = columns;
            customerColumns = customerColumns.concat(['isperson', 'companyname', 'vatregnumber']);

            var customerData = getAllSearchResults(CUSTOMER, filters, customerColumns);
            entityData = entityData.concat(customerData);
        }

        return entityData;
    }

    function orderByList(entityData, entityIds) {
        var entityDataMap = {};
        var orderedEntityData = [];
        var missingEntityIds = [];

        for (var i = 0; entityData && i < entityData.length; i++) {
            var entity = entityData[i];
            entityDataMap[entity.id] = entity;
        }

        for (var i = 0; entityIds && i < entityIds.length; i++) {
            var entityId = entityIds[i];
            
            var entity = entityDataMap[entityId];

            if (entity) {
                orderedEntityData.push(entity);
            } else {
                missingEntityIds.push(entityId);
            }
        }
        
        if (missingEntityIds.length > 0) {
            var code = 'EP_ENTITY_NOT_FOUND';
            var message = 'Error in retrieving the entities ' + missingEntityIds.join(',') + '. Verify that the following entity records are active and set up for electronic payment.';
            throw error.logAndCreateError(code, message);
        }
        
        return orderedEntityData;
    }

    function getAllSearchResults(type, filters, columns) {
        var genericSearch = search.create({
            type: type,
            columns: columns,
            filters: filters
        });

        var data = [];
        var iterator = genericSearch.getIterator();
        while (iterator.hasNext()) {
            var result = iterator.next();
            data.push(result);
        }

        return data;
    }

    function createColumn(columnName) {
        return search.createColumn({
            name: columnName
        });
    }

    return {
        retrieveEFTEntities: retrieveEFTEntities,
        retrieveDDEntities: retrieveDDEntities
    };
});
