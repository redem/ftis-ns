/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperSearch',
        '../lib/9997_DAOFactory'],

function (search, daoFactory) {
    var RECORD_TYPE = 'customrecord_2663_queue_settings';
    
    var QS_EP_REFERENCE_FIELD = 'custrecord_2663_qs_ep_preference';
    var QS_SUBSIDIARY_FIELD = 'custrecord_2663_qs_subsidiary';
    
    var QS_TRANSACTION_MARKER_DEPLOYMENT_FIELD = 'custrecord_2663_qs_tranmarker_deployment';
    var QS_PAYMENT_PROCESS_DEPLOYMENT_FIELD = 'custrecord_2663_qs_payme_proc_deployment';
    var QS_FILE_CREATOR_DEPLOYMENT_FIELD = 'custrecord_2663_qs_file_creat_deployment';
    var QS_ROLLBACK_DEPLOYMENT_FIELD = 'custrecord_2663_qs_rback_mr_deployment';
    var QS_REVERSE_DEPLOYMENT_FIELD = 'custrecord_2663_qs_reverse_mr_deployment';
    var QS_NOTIFY_DEPLOYMENT_FIELD = 'custrecord_2663_qs_notify_mr_deployment';
    var QS_REMOVE_UNPROC_DEPLOYMENT_FIELD = 'custrecord_2663_qs_remove_mr_deployment';
    var QS_BANKFILE_DEPLOYMENT_FIELD = 'custrecord_2663_qs_bnkfile_mr_deployment';
    var QS_SS1_PARENT_DEPLOYMENT_FIELD = 'custrecord_2663_qs_parent_deployment';
    var QS_SS2_SCHEDULER_FIELD = 'custrecord_2663_qs_ss2_sched_deployment';
    var QS_IS_DEFAULT_FIELD = 'custrecord_2663_qs_default';
    
    var FIELD_MAP = {
        epPreference : QS_EP_REFERENCE_FIELD,
        subsidiary: QS_SUBSIDIARY_FIELD,
        transactionMarkerDeployment: QS_TRANSACTION_MARKER_DEPLOYMENT_FIELD,
        paymentProcessorDeployment: QS_PAYMENT_PROCESS_DEPLOYMENT_FIELD,
        fileCreatorDeployment: QS_FILE_CREATOR_DEPLOYMENT_FIELD,
        rollbackDeployment: QS_ROLLBACK_DEPLOYMENT_FIELD,
        reverseDeployment: QS_REVERSE_DEPLOYMENT_FIELD,
        notifyDeployment: QS_NOTIFY_DEPLOYMENT_FIELD,
        removeUnprocessedDeployment: QS_REMOVE_UNPROC_DEPLOYMENT_FIELD,
        instantDeployment: QS_BANKFILE_DEPLOYMENT_FIELD,
        parentDeployment: QS_SS1_PARENT_DEPLOYMENT_FIELD,
        schedulerDeployment: QS_SS2_SCHEDULER_FIELD,
        isDefault: QS_IS_DEFAULT_FIELD
    }; 

    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };

    var dao = daoFactory.getDAO(daoParams);

    function retrieve(id){
        return dao.retrieve(id);
    }
    
    function getQueueSettings(subsidiary, schedulerDeployment){
        var queueSettings = [];
        var columns = getAllColumns();
        var filters = [];
        
        /* Filters */
        var filterDefs = [];
        
        filterDefs.push({name: QS_EP_REFERENCE_FIELD,
                         operator: search.Operator.NONEOF,
                         values: ['@NONE@']});
        
        if (subsidiary) {
            filterDefs.push({name: QS_SUBSIDIARY_FIELD,
                 operator: search.Operator.ANYOF,
                 values: [subsidiary]});
        }
        
        if (schedulerDeployment){
            filterDefs.push({name: QS_SS2_SCHEDULER_FIELD,
                operator: search.Operator.IS,
                values: schedulerDeployment});
        }
        
        for(var i = 0 ; i < filterDefs.length; i++){
            filters.push(search.createFilter(filterDefs[i]));
        }
        
        /* Search */
        var pfaSearch = search.create({
            type: RECORD_TYPE,
            filters: filters,
            columns: columns
        });
                       
        var iterator = pfaSearch.getIterator();
        while (iterator.hasNext()) {
            var result = iterator.next();
            queueSettings.push(result);
        }
        
        return queueSettings;
    }
    function getAllColumns(){
        return Object.keys(FIELD_MAP).map(
            function(key){
                return FIELD_MAP[key];
            }
        );
        
    }
    
    function getByParentDeployment(deploymentId){
        var filterDefs = [
            {
                name: QS_EP_REFERENCE_FIELD,
                operator: search.Operator.NONEOF,
                values: ['@NONE@']
            },
            {
                name: QS_SS1_PARENT_DEPLOYMENT_FIELD,
                operator: search.Operator.IS,
                values: deploymentId
            },
        ];
        
        var queueSetting = searchQueueSetting(filterDefs);
        
        return queueSetting;
    }
    
    function searchQueueSetting(filterDefs){
        var columns = getAllColumns();
        var filters = [];
        
        for(var i = 0 ; i < filterDefs.length; i++){
            filters.push(search.createFilter(filterDefs[i]));
        }
        var queueSetting;
        var qsSearch = search.create({
            type: RECORD_TYPE,
            filters: filters,
            columns: columns
        });
        
        var iterator = qsSearch.getIterator();
        if (iterator.hasNext()) {
            var result = iterator.next();
            
            queueSetting = {};
            for ( var i in FIELD_MAP) {
                var fieldName = FIELD_MAP[i];
                queueSetting[i] = result.getValue(fieldName);
            }
        }
        log.debug('queueSetting', queueSetting);
        return queueSetting;
    }
    
    function searchQueueSettings(filterDefs){
        var columns = getAllColumns();
        var filters = [];
        
        for(var i = 0 ; i < filterDefs.length; i++){
            filters.push(search.createFilter(filterDefs[i]));
        }
        var queueSettings = [];
        var queueSetting;
        var qsSearch = search.create({
            type: RECORD_TYPE,
            filters: filters,
            columns: columns
        });
        
        var iterator = qsSearch.getIterator();
        while (iterator.hasNext()) {
            var result = iterator.next();
            
            queueSetting = {};
            for ( var i in FIELD_MAP) {
                var fieldName = FIELD_MAP[i];
                queueSetting[i] = result.getValue(fieldName);
            }
            queueSettings.push(queueSetting);
        }
        
        return queueSettings;
    }
    
    function getByDeployment(deploymentId){
    	log.debug('getByDeployment:deploymentId', deploymentId);
        var filters = [
            [QS_EP_REFERENCE_FIELD, search.Operator.NONEOF, ['@NONE@']],
            'and',
            [ // search among ss2 deployments only
                [QS_SS2_SCHEDULER_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_BANKFILE_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_REMOVE_UNPROC_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_NOTIFY_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_REVERSE_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_ROLLBACK_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_FILE_CREATOR_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_PAYMENT_PROCESS_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
                'or',
                [QS_TRANSACTION_MARKER_DEPLOYMENT_FIELD, search.Operator.IS, deploymentId],
            ]
        ];
        
        var queueSetting;
        var qsSearch = search.create({
            type: RECORD_TYPE,
            filters: filters,
            columns: getAllColumns()
        });
        
        var iterator = qsSearch.getIterator();
        if (iterator.hasNext()) { // expecting only 1 result
        	var result = iterator.next();
        	 queueSetting = {};
             for ( var i in FIELD_MAP) {
                 var fieldName = FIELD_MAP[i];
                 queueSetting[i] = result.getValue(fieldName);
             }
        }
        log.debug('getByDeployment queueSetting', queueSetting);
        
        return queueSetting;
    }
    
    function getBySchedulerDeployment(deploymentId){
        var filterDefs = [
            {
                name: QS_EP_REFERENCE_FIELD,
                operator: search.Operator.NONEOF,
                values: ['@NONE@']
            },
            {
                name: QS_SS2_SCHEDULER_FIELD,
                operator: search.Operator.IS,
                values: deploymentId
            },
        ];
        
        var queueSetting = searchQueueSetting(filterDefs);
        
        return queueSetting;
    }
    
    function getByInstantBankFileDeployment(deploymentId){
        var filterDefs = [
            {
                name: QS_EP_REFERENCE_FIELD,
                operator: search.Operator.NONEOF,
                values: ['@NONE@']
            },
            {
                name: QS_BANKFILE_DEPLOYMENT_FIELD,
                operator: search.Operator.IS,
                values: deploymentId
            },
        ];
        
        var queueSetting = searchQueueSetting(filterDefs);
        
        return queueSetting;
    }
    
    function getNonDefaultQueueSettings(){
    	var filterDefs = [
    	                  {
    	                      name: QS_IS_DEFAULT_FIELD,
    	                      operator: search.Operator.IS,
    	                      values: "F"
    	                  }
    	              ];
    	              
    	var queueSettings = searchQueueSettings(filterDefs);
    	
    	return queueSettings;
    }

    return {
        retrieve: retrieve,
        getQueueSettings: getQueueSettings,
        getByParentDeployment: getByParentDeployment,
        getBySchedulerDeployment: getBySchedulerDeployment,
        getByInstantBankFileDeployment: getByInstantBankFileDeployment,
        getByDeployment : getByDeployment,
        getNonDefaultQueueSettings: getNonDefaultQueueSettings
    };
});


