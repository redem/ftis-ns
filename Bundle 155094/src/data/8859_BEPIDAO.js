/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/9997_DAOFactory', '../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperRecord'],

function(daoFactory, search, record) {
    var RECORD_TYPE = 'customrecord_2663_bill_eft_payment';
    
    var FIELD_MAP = {
        'eftPaymentProcessed': 'custrecord_2663_eft_payment_processed',
        'eftFileId': 'custrecord_2663_eft_file_id',
        'eftPaymentAmount': 'custrecord_2663_eft_payment_amount',
        'parentEntity': 'custrecord_2663_parent_entity',
        'parentBill': 'custrecord_2663_parent_bill',
        'referenceLine': 'custrecord_2663_reference_line',
        'parentPayment': 'custrecord_2663_parent_payment',
        'referenceId': 'custrecord_2663_reference_id',
        'parentBatch': 'custrecord_2663_parent_batch',
        'eftDiscountAmount': 'custrecord_2663_eft_discount_amount'
    };
    
    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };
    
    var dao = daoFactory.getDAO(daoParams);
    
    function create(params) {
        return dao.create(params);
    }
    
    function updateFields(bepiId, keyValueMapping) {
        record.submitFields({
            type: RECORD_TYPE,
            id: bepiId,
            values: keyValueMapping,
        });
    }
    
    function getBEPIsByPFA(pfaId) {
        var bepiSearch = search.create({
            type: RECORD_TYPE,
            filters: ['custrecord_2663_eft_file_id', 'is', pfaId]
        });
        
        var rs = bepiSearch.getIterator();
        
        var bepiList = [];
        while (rs.hasNext()) {
            var curr = rs.next();
            bepiList.push({
                type: RECORD_TYPE,
                id: curr.id,
                pfaId: pfaId
            });
        }
        
        return bepiList;
    }
    
    function getBEPIIDsByAggregation(params) {
        var filters = buildTranIdFilters(params);
        var lineFilterObj = buildLineFilterObject(params.paymentInfoList);
        
        // instead of filtering by reference line + bill, return reference line instead
        var tranSearch = search.create({
            type: RECORD_TYPE,
            filters: filters,
            columns: ['custrecord_2663_parent_bill', 'custrecord_2663_reference_line']
        });
        
        var rs = tranSearch.getIterator();
        var bepiList = [];
        
        
        while (rs.hasNext()) {
            var curr = rs.next();
            var currLine = curr.getValue('custrecord_2663_reference_line');
            var currId = curr.getValue('custrecord_2663_parent_bill');
            
            if (lineFilterObj[currId].indexOf(currLine) > -1) {
                bepiList.push(curr.id);
            }
        }
        
        return bepiList;
    }
    
    function getBEPIIDsByPayment(params) {
        var filters = [['custrecord_2663_eft_file_id', search.Operator.IS, params.pfaId],
            'and',
            ['custrecord_2663_parent_bill.applyingtransaction',  search.Operator.IS , params.paymentId]];
        
        var bepiSearch = search.create({
            type: RECORD_TYPE,
            filters: filters,
            columns: []
        });
        
        var rs = bepiSearch.getIterator();
        var bepiList = [];
        
        while (rs.hasNext()) {
            var curr = rs.next();
            bepiList.push(curr.id);
        }
        
        return bepiList;
    }
    
    function buildLineFilterObject(paymentInfoList) {
        paymentInfoList = paymentInfoList || [];
        var lineFilterObj = {};
        
        for (var i = 0; i < paymentInfoList.length; i++) {
            var paymentInfo = paymentInfoList[i];
            var id = paymentInfo.id;
            var line = paymentInfo.line;
            lineFilterObj[id] = lineFilterObj[id] || [];
            lineFilterObj[id].push(line);
        }
        
        return lineFilterObj;
    }
    
    function buildTranIdFilters(params) {
        
        var tranFilters = [];
        var tranIds = extractTranIds(params.paymentInfoList);
        while (tranIds.length > 0) {
            var segment = tranIds.splice(0, 500);
            if (tranFilters.length > 0) {
                tranFilters.push('OR');
            }
            tranFilters.push(['custrecord_2663_parent_bill', 'anyof', segment]);
        }
        var filters = [['custrecord_2663_eft_file_id', 'is', params.pfaId]];
        if (tranFilters.length > 0){
            filters.push('AND');
            filters.push(tranFilters);
        }
        
        return filters;
    }
    
    function extractTranIds(paymentInfoList) {
        paymentInfoList = paymentInfoList || [];
        
        var tranIds = [];
        for (var i = 0; i < paymentInfoList.length; i++) {
            var paymentInfo = paymentInfoList[i];
            tranIds.push(paymentInfo.id);
        }
        
        return tranIds;
    }
    
    function deleteBEPI(id){
        return dao['delete'](id);
    }
    
    return {
        create: create,
        updateFields: updateFields,
        getBEPIIDsByAggregation: getBEPIIDsByAggregation,
        'delete': deleteBEPI,
        getBEPIsByPFA: getBEPIsByPFA,
        getBEPIIDsByPayment : getBEPIIDsByPayment 
    };
});
