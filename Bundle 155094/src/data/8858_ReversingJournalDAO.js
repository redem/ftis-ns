/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperRecord', '../lib/9997_DAOFactory',
        '../lib/wrapper/9997_NsWrapperRuntime'],

function(record, daoFactory, runtime) {
    var RECORD_TYPE = record.Type.JOURNAL_ENTRY;
    
    var FIELD_MAP = {
            'createdFrom': 'createdfrom',
            'void': 'void',
            'tranDate': 'trandate',
            'class': 'class',
            'department': 'department',
            'location': 'location',
            'postingPeriod': 'postingperiod'
    };
    
    var SUBLIST_MAPS = {
        'line': {
            'account': 'account',
            'debit': 'debit',
            'credit': 'credit',
            'memo': 'memo',
            'entity': 'entity',
            'class': 'class',
            'department': 'department',
            'location': 'location',
        }
    };
    

    
    var dao = null;
    
    function getDAO(){
        if (dao){return dao;}
        
        if (runtime.isOW()){
            FIELD_MAP.subsidiary = 'subsidiary';
        }
        
        if (runtime.isMultiCurrency()){
            FIELD_MAP.currency = 'currency';
        }
        
        var daoParams = {
            recordType: RECORD_TYPE,
            fieldMap: FIELD_MAP,
            subListMaps: SUBLIST_MAPS,
            };
        
        dao = daoFactory.getDAO(daoParams);
        return dao;
    }
    
    function create(params) {
        return getDAO().create(params);
    }
    
    return {
        create: create
    };
});
