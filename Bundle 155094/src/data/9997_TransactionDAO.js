/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperSearch',
        '../lib/wrapper/9997_NsWrapperRuntime',
        '../lib/wrapper/9997_NsWrapperRecord',
        '../app/9997_SearchListIterator',
        '../lib/wrapper/9997_NsWrapperFormat',],

function (search, runtime, record, searchListIterator, formatter) {
    var PAYMENT_PARENT_FIELD = 'custbody_9997_pfa_record';
    var TRANSACTION_APPLYING_TO_FIELD = 'applyingtransaction.internalid';

    function retrieveVendorPayments(pfaId, searchColumns){
        return searchPaymentsByPFAId(search.Type.VENDOR_PAYMENT, pfaId, searchColumns);
    }

    function retrieveCustomerPayments(pfaId, searchColumns){
        return searchPaymentsByPFAId(search.Type.CUSTOMER_PAYMENT, pfaId, searchColumns);
    }

    function retrieveEFTTransactions(paymentIds, searchColumns) {
        var tranTypes = ['VendBill', 'ExpRept', 'Journal'];

        if (runtime.isPartnerCommissionEnabled() || runtime.isEmployeeCommissionEnabled()) {
            tranTypes.push('Commissn');
        }

        return searchApplyingTransactionsByBatch(tranTypes, paymentIds, searchColumns);
    }

    function retrieveDDTransactions(paymentIds, searchColumns) {
        var tranTypes = ['CustInvc', 'Journal'];

        return searchApplyingTransactionsByBatch(tranTypes, paymentIds, searchColumns);
    }

    function searchPaymentsByPFAId(paymentType, pfaId, searchColumns) {
        var searchFilters = [
            ['mainline', search.Operator.IS, 'T'],
            'and',
            [PAYMENT_PARENT_FIELD, search.Operator.IS, pfaId]
        ];

        var customerPayments = searchTransactionByType(paymentType, searchFilters, searchColumns);
        return sortByMemo(customerPayments);
    }
    
    function searchApplyingTransactionsByBatch(tranTypes, paymentIds, searchColumns) {
        var applyingTransactions = [];
        var listIterator = searchListIterator.create(paymentIds);

        while (listIterator.hasNext()) {
            var paymentIdsSubset = listIterator.next();

            var applyingTransactionSubset = searchApplyingTransactions(tranTypes, paymentIdsSubset, searchColumns);
            applyingTransactions = applyingTransactions.concat(applyingTransactionSubset);
        }

        return applyingTransactions;
    }

    function searchTransactionsByIdList(paymentIds, searchColumns) {
        var applyingTransactions = [];
        var listIterator = searchListIterator.create(paymentIds);

        while (listIterator.hasNext()) {
            var paymentIdsSubset = listIterator.next();

            var searchFilters = [
                                 ['mainline', search.Operator.IS, 'T'],
                                 'and',
                                 ['internalid', search.Operator.ANYOF, paymentIdsSubset],
                                 'and',
                                 ['applyingtransaction.voided', 'is', 'F']
                             ];
            
            var applyingTransactionSubset = searchTransactionByType('transaction', searchFilters, searchColumns);
            applyingTransactions = applyingTransactions.concat(applyingTransactionSubset);
        }

        return applyingTransactions;
    }
    
    function searchApplyingTransactions(tranTypes, paymentIds, searchColumns) {
        var searchFilters = [
            ['mainline', search.Operator.IS, 'T'],
            'and',
            ['type', 'anyof', tranTypes],
            'and',
            [TRANSACTION_APPLYING_TO_FIELD, search.Operator.ANYOF, paymentIds],
            'and',
            ['applyingtransaction.voided', 'is', 'F']
        ];

        if (runtime.isMultiCurrency()) {
            searchFilters.push('and');
            searchFilters.push(['applyingforeignamount', 'greaterthan', 0]);            
        }

        return searchTransactionByType('transaction', searchFilters, searchColumns);
    }

    function retrieveVendorCreditsByIds(transIds){
    	var searchFilters = [['internalid', 'anyof', transIds], 
    	                      'AND', 
    	                      ['mainline', 'is', 'T']]
    	
    	return searchTransactionByType("vendorcredit", searchFilters, []).concat(
    	    searchTransactionByType("vendorpayment", searchFilters, []));
    }
    
    function searchTransactionByType(type, searchFilters, searchColumns) {
        var vendorPaymentSearch = search.create({
            type: type,
            columns: searchColumns,
            filters: searchFilters
        });

        var data = [];
        var iterator = vendorPaymentSearch.getIterator();
        while (iterator.hasNext()) {
            var result = iterator.next();
            data.push(result);
        }

        return data;
    }

    function sortByMemo(searchResults) {
        searchResults.sort(function(a, b){
            var memoA = a.getValue('memo') || '';
            var memoB = b.getValue('memo') || '';
            var aId = 0;
            var bId = 0;
            if (memoA.indexOf('/') != -1) {
                aId = parseInt(memoA.substring(memoA.indexOf('/') + 1, memoA.length), 10);
            }
            if (memoB.indexOf('/') != -1) {
                bId = parseInt(memoB.substring(memoB.indexOf('/') + 1, memoB.length), 10);
            }
            return aId - bId;
        });
        
        return searchResults;
    }

    function retrievePaymentsForNotification(ids, isFileGen){
        var columns = ['internalid', 'tranid', 'entity', 'memo',
                       search.createColumn({
                           name: 'internalid', 
                           join: 'vendor'
                       }),
                       search.createColumn({
                           name: 'email', 
                           join: 'vendor'
                       }),
                       search.createColumn({
                           name: 'custentity_2663_email_address_notif', 
                           join: 'vendor'
                       }),
                       search.createColumn({
                           name: 'internalid', 
                           join: 'employee'
                       }),
                       search.createColumn({
                           name: 'email', 
                           join: 'employee'
                       }),
                       search.createColumn({
                           name: 'custentity_2663_email_address_notif', 
                           join: 'employee'
                       }),
                       search.createColumn({
                           name: 'internalid', 
                           join: 'customer'
                       }),
                       search.createColumn({
                           name: 'email', 
                           join: 'customer'
                       }),
                       search.createColumn({
                           name: 'custentity_2663_email_address_notif', 
                           join: 'customer'
                       }),
                       'custbody_9997_pfa_record'
                       ];
        return searchTransactionsByIdList(ids, columns);
    }
    
    return {
        retrieveCustomerPayments: retrieveCustomerPayments,
        retrieveVendorPayments: retrieveVendorPayments,
        retrieveEFTTransactions: retrieveEFTTransactions,
        retrieveDDTransactions: retrieveDDTransactions,
        retrieveVendorCreditsByIds: retrieveVendorCreditsByIds,
        retrievePaymentsForNotification: retrievePaymentsForNotification
    };
});
