/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Aug 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */

define([
    '../lib/wrapper/9997_NsWrapperRecord',
    '../lib/wrapper/9997_NsWrapperRuntime',
    '../lib/wrapper/9997_NsWrapperError',
    '../lib/8858_Translator',
    '../lib/8858_StringFormatter',
    '../app/8858_PaymentInfoListAdapter'],

function(record, runtime, error, translator, stringFormatter, paymentInfoListAdapter) {
    var RECORD_TYPES = record.Type;
    var RECORD_TYPE = record.Type.VENDOR_PAYMENT;
    var DEFAULT_TRANLIST_THRESHOLD = 1000;
    var EP_COULD_NOT_PAY_ENTITY = 'EP_COULD_NOT_PAY_ENTITY';
    var EP_COULD_NOT_FIND_TRANS = 'EP_COULD_NOT_FIND_TRANS';
    var EP_COULD_NOT_PAY_ENTITY_BASE = 'EP_COULD_NOT_PAY_ENTITY_BASE';
    var EP_COULD_NOT_PAY_ENTITY_LIST = 'EP_COULD_NOT_PAY_ENTITY_LIST';
    
    var APPLY = 'apply';
    
    function createEFTPayment(params) {
        var paymentInfoList = params.trans.slice(0, params.trans.length); // clone the list of payment objects
        var pfa = params.pfa;
        var pfaName = pfa.name;
        var entity = params.entity;
        var currency = params.currency;
        var bankDetails = params.bankDetails;
        var paymentCount = params.paymentCount;
        
        var paymentRecord;
        /**
         * TODO
         * Support the following case:
         *  - Number of transactions > DEFAULT_TRANLIST_THRESHOLD (currently 1K)
         *  - Number of open bills for the entity > 10K
         *  - one of the selected transactions is the 10001st or higher
         * */
        if (paymentInfoList.length > DEFAULT_TRANLIST_THRESHOLD) {
            // Remove this option once enhancement issue 399732 is resolved
            paymentRecord = createRecordFromVendor(entity);
        } else {
            paymentRecord = createRecord(params);
        }
        
        paymentRecord.setValue('apacct', pfa.account);
        paymentRecord.setValue('tobeprinted', false);
        paymentRecord.setValue('toach', false);
        
        if (runtime.isOW()) {
            paymentRecord.setValue('subsidiary', pfa.paymentSubsidiary);
        }
        
        if (currency) {
            paymentRecord.setValue('currency', currency);
        }
        paymentRecord.setValue('postingperiod', Number(pfa.postingPeriod));
        
        paymentRecord.setValue('trandate', pfa.processDate);
        
        paymentRecord.setValue('custbody_9997_pfa_record', pfa.id);
        

        var results = selectApplyLines(paymentRecord, paymentInfoList);
        var transInApply = results.transInApply;
        var missingTranCount = results.missingTranCount;
        var entityTransactions = results.entityTransactions;
        
        // There are remaining transactions not found in the paymentRecord's apply list
        if (paymentInfoList.length > 0 || missingTranCount > 0) {
            var errorMessage = translator.getString(EP_COULD_NOT_PAY_ENTITY);
            
            stringFormatter.setString(errorMessage);
            stringFormatter.replaceParameters({
                ENTITY: entity,
            });
            errorMessage = stringFormatter.toString();
            
            stringFormatter.setString(translator.getString(EP_COULD_NOT_PAY_ENTITY_BASE));
            stringFormatter.replaceParameters({
                ENTITY: entity,
            });
            var logSubject = stringFormatter.toString();
            
            stringFormatter.setString(translator.getString(EP_COULD_NOT_PAY_ENTITY_LIST));
            stringFormatter.replaceParameters({
                TRANLIST: entityTransactions.join(', '),
                
            });
            var tranList = stringFormatter.toString();
            log.error(logSubject, tranList);
            
            throw error.create({
                name: EP_COULD_NOT_PAY_ENTITY,
                message: errorMessage
            });
            
        } else if (transInApply.length > 0) {
            paymentRecord.setValue('account', bankDetails.glBankAccount);
            paymentRecord.setValue('tranid', pfaName + '/' + paymentCount);
            paymentRecord.setValue('memo', pfaName + '/' + paymentCount);
            
            if (params.department) {
                paymentRecord.setValue('department', params.department);
            }
            
            if (params.classification) {
                paymentRecord.setValue('class', params.classification);
            }
            
            if (params.location) {
                paymentRecord.setValue('location', params.location);
            }
            
            if (runtime.isMultiCurrency() && currency) {
                var fxRate;
                try {
                    var fxRates = JSON.parse(pfa.exchangeRates);
                    fxRate = fxRates[currency];
                    log.debug('fxRate', fxRate)
                } catch (e) {
                    log.audit('Error in parsing exchange rate string: ' + pfa.exchangeRates);
                }
                if (fxRate) {
                    paymentRecord.setValue('exchangerate', fxRate);
                }
                
            }
            return paymentRecord.save();
        }
        
    }
    
    // Loop among all apply lines and if set the apply and amount field if applicable
    function selectApplyLines(paymentRecord, paymentInfoList) {
        var transInApply = [];
        var entityTransactions = [];
        
        var applyCount = paymentRecord.getLineCount({
            sublistId: APPLY
        });
        

        for (var n = 0; n < applyCount; n++) {
            paymentRecord.selectLine({
                sublistId: APPLY,
                line: n
            });
            
            var transactionId = paymentRecord.getCurrentSublistValue({
                sublistId: APPLY,
                fieldId: 'doc'
            });
            
            var transactionLine = paymentRecord.getCurrentSublistValue({
                sublistId: APPLY,
                fieldId: 'line'
            });
            var paymentInfo = null;
            
            if (paymentInfoList.length == 0) {
                break;
            }
            
            // loop through the list of paymentInfo items, use loop instead of filter to simplify reduction of the
            // paymentInfoList
            for (var paymentInfoIndex = 0; paymentInfoIndex < paymentInfoList.length; paymentInfoIndex++) {
                var pInfo = paymentInfoList[paymentInfoIndex];
                if (pInfo.id == transactionId && pInfo.line == transactionLine) {
                    paymentInfo = pInfo;
                    paymentInfoList.splice(paymentInfoIndex, 1);
                    break;
                }
            }
            
            // if the current sublist line is not to be applied, continue to the next line item
            if (paymentInfo == null) {
                continue;
            }
            
            var id = paymentInfo.id;
            var line = paymentInfo.line;
            var amount = Number(paymentInfo.amount);
            var discount = Number(paymentInfo.discount);
            entityTransactions.push(id);
            
            var isPayable = amount > 0;
            
            paymentRecord.setCurrentSublistValue({
                sublistId: APPLY,
                fieldId: APPLY,
                value: true
            });
            
            paymentRecord.setCurrentSublistValue({
                sublistId: APPLY,
                fieldId: 'amount',
                value: amount
            });
            
            if (isPayable) {
                paymentRecord.setCurrentSublistValue({
                    sublistId: APPLY,
                    fieldId: 'disc',
                    value: discount
                });
            }
            
            paymentRecord.commitLine({
                sublistId: APPLY
            });
            
            transInApply.push(id + '-' + line);
        }
        
        if (paymentInfoList.length > 0){
            var messageParams = {ENTITY: paymentRecord.getValue('entity')};
            var logMessage = translator.getString(EP_COULD_NOT_FIND_TRANS);
            
            
            paymentInfoList.map(function(paymentInfo){
                var id = paymentInfo.id;
                
                messageParams.TRANSACTION = id;
                stringFormatter.setString(logMessage);
                stringFormatter.replaceParameters(messageParams);
                
                log.error('Error encountered in transaction: ' + id, stringFormatter.toString());
                entityTransactions.push(id);
            });
        }
        
        return {
            transInApply: transInApply,
            missingTranCount: paymentInfoList.length,
            entityTransactions: entityTransactions
        };
    }
    
    function createRecordFromVendor(entityId) {
        var paymentRecord = record.transform({
            fromType: RECORD_TYPES.VENDOR,
            fromId: entityId,
            toType: RECORD_TYPE,
            isDynamic: true,
        });
        
        return paymentRecord;
    }
    
    function createRecord(params) {
        var transactionList = paymentInfoListAdapter.toIDList(params.trans);
        
        var paymentRecord = record.create({
            type: RECORD_TYPE,
            isDynamic: true,
            defaultValues: {
                entity: params.entity,
                vendorbills: transactionList.join(',')
            }
        });
        
        return paymentRecord;
    }
    
    function deletePayment(id){
        return record.deleteRecord({
            type: RECORD_TYPE,
            id: id
        });
    }

    return {
        'delete': deletePayment,
        createEFTPayment: createEFTPayment,
    };
    
});
