/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperSearch',
        '../lib/9997_DAOFactory', 'N/task'],

function (search, daoFactory, task) {
    var RECORD_TYPE = 'scheduledscriptinstance';

    var FIELD_MAP = {
    		
    }; 

    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };

    var dao = daoFactory.getDAO(daoParams);

    function retrieve(id){
        return dao.retrieve(id);
    }
    
    function getUnavailableDeployments(deploymentIDs){
		var unavailableDeployments = [];
		var columns = [];
		
		var deploymentScriptID = search.createColumn({
			name: "scriptid",
			join: "scriptdeployment"
		});
		columns.push(deploymentScriptID);	
		
		var deploymentFilter = [];
    	for(var i = 0; i < deploymentIDs.length; i++){
    		if(deploymentFilter.length > 0) deploymentFilter.push("OR");
    		
    		var dfilter = ["scriptdeployment.scriptid", search.Operator.IS, deploymentIDs[i]];
    		deploymentFilter.push(dfilter);
    	}
    	
    	var statusFilter = ["status", search.Operator.NONEOF, ["COMPLETE", "FAILED"]];
    	
		var pfaSearch = search.create({
			type: RECORD_TYPE,
			filters: [statusFilter, "AND", deploymentFilter],
			columns: columns
		});
			           
		var iterator = pfaSearch.getIterator();
		while (iterator.hasNext()) {
			var result = iterator.next();
			unavailableDeployments.push(result.getValue({
				name: "scriptid",
				join: "scriptdeployment"
			}).toLowerCase());
		}
		
		return unavailableDeployments;
	}

    return {
        retrieve: retrieve,
        getUnavailableDeployments: getUnavailableDeployments
    };
});

