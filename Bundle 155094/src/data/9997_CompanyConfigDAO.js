/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['N/config'],

function (config) {
    // Retrieve company prefs
    function retrieve(){
        var configObj = {};

        var features = config.load({
            type: config.Type.FEATURES
        });

        configObj.isMulticurrency = features.getValue({
            fieldId: 'multicurrency'
        });

        var companyInfo = config.load({
            type: config.Type.COMPANY_INFORMATION
        });
        
        configObj.baseCurrency = companyInfo.getValue({
            fieldId: 'basecurrency'
        });
        
        return configObj;
    }

    return {
        retrieve: retrieve,
    };
});
