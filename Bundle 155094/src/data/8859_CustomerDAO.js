/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope Public 
 */

define(['../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperRecord', '../lib/9997_DAOFactory'],

function(search, record, daoFactory) {
    var RECORD_TYPE = record.Type.CUSTOMER;
    
    var FIELD_MAP = {
        usesElectronicPayments: 'custentity_2663_direct_debit',
        usesEFTRefunds: 'custentity_2663_customer_refund',
        usesDD: 'custbody_9997_is_for_ep_dd'
    };
    
    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };
    
    var dao = daoFactory.getDAO(daoParams);
    
    function retrieve(id) {
        return dao.retrieve(id);
    }
    
    return {
        retrieve: retrieve
    };
});
