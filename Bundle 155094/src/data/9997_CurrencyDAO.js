/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/9997_DAOFactory', '../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperRecord'],

function(daoFactory, search, record) {
    var RECORD_TYPE = 'currency';
    var NAME = 'name';
    var SYMBOL = 'symbol'
    var FIELD_MAP = {
        'name': NAME,
        'symbol': SYMBOL
    };
    
    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };
    
    var dao = daoFactory.getDAO(daoParams);
    
    function retrieve(id) {
        return dao.retrieve(id);
    }
    
    function retrieveNsRecord(id) {
        return dao.retrieveNsRecord(id);
    }
    
    // Only added for currency for now
    function getAllCurrencies() {
        var list = [];
        
        var currencySearch = search.create({
            type: record.Type.CURRENCY,
            columns: [NAME, SYMBOL]
        })

        var rs = currencySearch.getIterator();
        while (rs.hasNext()) {
            list.push(rs.next())
        }
        return list;
    }
    
    return {
        retrieve: retrieve,
        retrieveNsRecord: retrieveNsRecord,
        getAllCurrencies: getAllCurrencies
    };
});
