/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([
    '../lib/wrapper/9997_NsWrapperFile',
    '../lib/wrapper/9997_NsWrapperSearch',
    '../lib/wrapper/9997_NsWrapperError'],

function(file, search, error) {
    

    function retrieveContents() {
        // retrieve EP FreeMarker Library
        var fSearch = search.create({
            type: 'file',
            filters: ['name', 'is', '2663_free_marker_lib.txt']
        });
        var rs = fSearch.getIterator();
        

        if (!rs.hasNext()) {
            throw error.create({
                name: 'EP_CANNOT_FIND_FREE_MARKER_LIB',
                message: 'Cannot find free marker library file',
                notifyOff: true
            });
        }
        var fileTemplateLib = file.load({
            id: rs.next().id
        });
        
        if (rs.hasNext()) {
            throw error.create({
                name: 'EP_DUPLICATE_FREE_MARKER_LIB',
                message: 'Free marker library file is not unique',
                notifyOff: true
            });
        }
        
        return removeComments(fileTemplateLib.getContents());
    }
    
    function removeComments(data) {
        // remove FreeMarker comments <#-- -->
        // current limitation is that strings with <#-- --> are ommitted
        var result = '';
        if (data.indexOf('<#--') == -1) {
            result = data;
        } else {
            var subData = data;
            while (subData.indexOf('<#--') > -1) {
                var start = subData.indexOf('<#--');
                result += subData.substring(0, start);
                var tmp = subData.substring(start);
                var end = tmp.indexOf('-->') + '-->'.length;
                subData = tmp.substring(end);
            }
            result += subData;
        }
        return result;
    }
    

    return {
        retrieveContents: retrieveContents
    }

})
