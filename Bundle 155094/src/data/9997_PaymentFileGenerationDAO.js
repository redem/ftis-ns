/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author aaclan
 * @NApiVersion 2.0
 * @NModuleScope Public
 */

define(['../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperError', 'N/format','N/url', 'N/record'],

function(search, error, format, url, record, searchValidator) {
    var ERROR_MSG_LOAD_SEARCH = 'Click the Edit Search link if you want to review the filtering criteria or you may select another saved search.';
    var ERROR_MSG_GET_SUMMARY = 'There was an error in getting the summary of the selected saved search.';

    var FILE_RECORD_TYPE = 'file';
    
    var CREATE_SEARCH_TASK_ID = 'EDIT_SAVEDSEARCH'; // See Help Doc: Task Ids
    var SEARCH_RESULT_TASK_ID = 'LIST_SEARCHRESULTS';
    
    var BANK_ACCOUNT_RECORD_TYPE = 'customrecord_2663_bank_details';
    var FILE_FORMAT_RECORD_TYPE = 'customrecord_2663_payment_file_format';
    var PFA_RECORD_TYPE = 'customrecord_2663_file_admin';
    
    var BANK_ACCOUNT_FIELD_MAP = {
        eftTemplate: 'custrecord_2663_eft_template',
        ddTemplate: 'custrecord_2663_dd_template',
        ppTemplate: 'custrecord_2663_pp_template',
        department: 'custrecord_2663_bank_department',
        bnkClass: 'custrecord_2663_bank_class',
        location: 'custrecord_2663_bank_location',
        subsidiary: 'custrecord_2663_subsidiary',
        currency: 'custrecord_2663_currency',
        glBankAccount: 'custrecord_2663_gl_bank_account',
        isInactive: 'isinactive',
        name: 'name',
        id: 'internalid',
    };
    
    var FILE_FORMAT_FIELD_MAP = {
        formatCurrency: 'custrecord_2663_format_currency',
        includeAllCurrencies: 'custrecord_2663_include_all_currencies',
        maxLine: 'custrecord_2663_max_lines',
        name: 'name',
        isInactive: 'isinactive'
    };

    var PFA_FIELD_MAP = {
        fileProcessed: 'custrecord_2663_file_processed',
        lastProcess: 'custrecord_2663_last_process',
        bankAccount: 'custrecord_2663_bank_account',
        paymentType: 'custrecord_2663_payment_type'
    };
    
    // TODO add to constants
    // Processing Status
    var PAYQUEUED = 1;
    var PAYMARKPAYMENTS = 5;
    var PAYPROCESSING = 2;
    var PAYCREATINGFILE = 3;
    var PAYPROCESSED = 4;
    var PAYREVERSAL = 6;
    var PAYNOTIFICATION = 7;
    var PAYERROR = 8;
    var PAYFAILED = 9;
    var PAYCANCELLED = 10;
    var PAYDELETINGFILE = 11;
    var REQUEUED = 12;
    
    // EP Processes
    var EP_PROCESSPAYMENTS = 1;
    var EP_REPROCESS = 2;
    var EP_ROLLBACK = 3;
    var EP_REVERSEPAYMENTS = 4;
    var EP_EMAILNOTIFICATION = 5;
    var EP_CREATEFILE = 6;
    
    
    // TODO: limit to allowed formats (i.e. check country and if licensed)
    function getEPBankAccounts(templateField){
        
        var bankAccountSearch = search.create({
            type: BANK_ACCOUNT_RECORD_TYPE,
            columns: getBankAccountColumns(templateField)
        });

        var filterDefs = [
            {
            name: templateField,
            operator: search.Operator.NONEOF,
            values: ['@NONE@']
            },
            {
                name: BANK_ACCOUNT_FIELD_MAP.isInactive,
                operator: search.Operator.IS,
                values: ['F']
            },
            {
                name: FILE_FORMAT_FIELD_MAP.isInactive,
                join: templateField,
                operator: search.Operator.IS,
                values: ['F']
            },
        ];
        
        var filters = [];
        
        for(var i = 0 ; i < filterDefs.length; i++){
            filters.push(search.createFilter(filterDefs[i]));
        }

        bankAccountSearch.filters = filters;
        
        var resultSet = [];
        
        var iterator = bankAccountSearch.getIterator();
        
        while (iterator.hasNext()) {
            var result = iterator.next();
            var obj = convertBankAccountResultToObj(result, templateField);
            resultSet.push(obj);
        }
        
        return resultSet;
    }
    
    function getBankAccountColumns(templateField){
        var columnDefs = [
            //bank account
            {
                name: BANK_ACCOUNT_FIELD_MAP.id,
            },
            {
                name: BANK_ACCOUNT_FIELD_MAP.name,
                sort: search.Sort.ASC,
            },
            {
                name: BANK_ACCOUNT_FIELD_MAP.currency,
            },
            {
                name: BANK_ACCOUNT_FIELD_MAP.glBankAccount,
            },
            {
                name: templateField,
            },
            //file format
            {
                name: FILE_FORMAT_FIELD_MAP.name,
                join: templateField,
            },
            {
                name: FILE_FORMAT_FIELD_MAP.maxLine,
                join: templateField,
            },
            {
                name: FILE_FORMAT_FIELD_MAP.formatCurrency,
                join: templateField,
            },
            //subsidiary
            {
                name: BANK_ACCOUNT_FIELD_MAP.subsidiary,
            },
            {
                name: 'name',
                join: BANK_ACCOUNT_FIELD_MAP.subsidiary,
            },
            {
                name: 'currency',
                join: BANK_ACCOUNT_FIELD_MAP.subsidiary,
            }
            
        ];
        var columns = [];
        for(var i=0; i< columnDefs.length; i++){
            columns.push(search.createColumn(columnDefs[i]));
        }
        return columns;
    }
    
    function getBankAccountsWithEFTFormats(){
        return getEPBankAccounts(BANK_ACCOUNT_FIELD_MAP.eftTemplate);
    }
    
    function getBankAccountsWithDDFormats(){
        return getEPBankAccounts(BANK_ACCOUNT_FIELD_MAP.ddTemplate);
    }
    
    function convertBankAccountResultToObj(result, templateField){
        var obj = {
            name: result.getValue('name'),
            id: result.getValue('internalid'),
            currency: result.getText(BANK_ACCOUNT_FIELD_MAP.currency),
            currencyId: result.getValue(BANK_ACCOUNT_FIELD_MAP.currency),
            isInactive: result.getValue(BANK_ACCOUNT_FIELD_MAP.isInactive),
            glBankAccount: result.getValue(BANK_ACCOUNT_FIELD_MAP.glBankAccount),
            glBankAccountTxt: result.getText(BANK_ACCOUNT_FIELD_MAP.glBankAccount),
            subsidiary: {
                id: result.getValue({
                    name: BANK_ACCOUNT_FIELD_MAP.subsidiary,
                }),
                name: result.getValue({
                    name: 'name',
                    join: BANK_ACCOUNT_FIELD_MAP.subsidiary,
                }),
                baseCurrency: result.getText({
                    name: 'currency',
                    join: BANK_ACCOUNT_FIELD_MAP.subsidiary,
                }),
                baseCurrencyId: result.getValue({
                    name: 'currency',
                    join: BANK_ACCOUNT_FIELD_MAP.subsidiary,
                }),
                
            },
            template:{
                id: result.getValue(templateField),
                maxLine: result.getValue({
                    name: FILE_FORMAT_FIELD_MAP.maxLine,
                    join: templateField,
                }),
                currency: result.getValue({
                    name: FILE_FORMAT_FIELD_MAP.formatCurrency,
                    join: templateField,
                }),
                name: result.getText(templateField),
            }
        };
        return obj;
    }
    
    function getSearch(searchId){
        var tranSearch;
        var loadSearchErrorParams = {
            name: 'EP_SEARCH_ERROR',
            message: ERROR_MSG_LOAD_SEARCH
        };

        try{
            tranSearch = search.load({
                id: searchId
            });
            
            if(!tranSearch){
                throw error.create(loadSearchErrorParams);
                
            }
        }
        catch(e){
            log.error(e.name, e.message);
            throw error.logAndCreateError(loadSearchErrorParams.name, loadSearchErrorParams.message);
        }
        
        return tranSearch;
        
    }
    
    function getSearchSummary(tranSearch){
        var summary = {};
        
        var totalAmountColDef = {
            name: 'amount',
            summary: search.Summary.SUM,
            'function': 'absoluteValue',
        };
        
        var countColDef = {
            name: 'internalid',
            summary: search.Summary.COUNT,
        };
        
        tranSearch.columns = [
            search.createColumn(totalAmountColDef),
            search.createColumn(countColDef),
        ];
        
        var getSummaryError = {
            name: 'EP_GET_SUMMARY_ERROR',
            message: ERROR_MSG_GET_SUMMARY
        };
        
        try{
            var tranSearchResultSet = tranSearch.run();
            
            tranSearchResultSet.each(function(result) {
                summary.count = result.getValue(countColDef);
                var totalAmount = result.getValue(totalAmountColDef) || 0;
                summary.totalAmount = format.format({
                    value: totalAmount,
                    type: format.Type.CURRENCY
                });
            });
        } catch(e) {
            log.error(e.name, e.message);
            throw error.logAndCreateError(getSummaryError.name, getSummaryError.message);
        }
        


        return summary;
    }

    function getProcessingPFACount(parameters){
        var glBankAccount = parameters.glBankAccount;
        var fileFormat = parameters.fileFormat;
        
        
        var processingStatusList = [PAYPROCESSED, PAYFAILED, PAYERROR, PAYCANCELLED];
        var processList = [EP_PROCESSPAYMENTS, EP_CREATEFILE];
        
        var filters = [
            [PFA_FIELD_MAP.fileProcessed, search.Operator.NONEOF, processingStatusList],
            'and',
            [PFA_FIELD_MAP.lastProcess, search.Operator.ANYOF, processList],
            'and',
            [PFA_FIELD_MAP.bankAccount + '.' + BANK_ACCOUNT_FIELD_MAP.glBankAccount, search.Operator.IS, glBankAccount],
            'and',
            [ 
                  [PFA_FIELD_MAP.bankAccount +'.'+ BANK_ACCOUNT_FIELD_MAP.eftTemplate, search.Operator.IS, fileFormat],
                  'or',
                  [PFA_FIELD_MAP.bankAccount +'.'+ BANK_ACCOUNT_FIELD_MAP.ddTemplate, search.Operator.IS, fileFormat],
                  'or',
                  [PFA_FIELD_MAP.bankAccount +'.'+ BANK_ACCOUNT_FIELD_MAP.ppTemplate, search.Operator.IS, fileFormat]
            ]
        ];
        
        var countColumnDef = {
            name: 'internalid',
            summary: search.Summary.COUNT,
        };
        
        var pfaSearch = search.create({
            type: PFA_RECORD_TYPE,
            columns: [
                search.createColumn(countColumnDef),
            ],
            filters: filters
        });

        
        var iterator = pfaSearch.getIterator();
        var count = 0;
        
        if (iterator.hasNext()) {
            var result = iterator.next();
            count = result.getValue(countColumnDef);
        }

        return count;
    }
    
    function getClientScriptFileId(fileName) {
        var fileObj = {id: null};
        
        var fileSearch = search.create({
            type: FILE_RECORD_TYPE,
            columns: [],
            filters: ['name', 'is', fileName]
        });
        
        var iterator = fileSearch.getIterator();
        
        if (iterator.hasNext()) {
            var result = iterator.next();
            
            fileObj.id = result.id;
        }
        
        return fileObj.id;
    }
    
    function getSearchLink(searchId, isEdit){
        var label;
        var url;
        
        if(isEdit){
            label = 'Edit Search';
            url = getEditSearchUrl({id:searchId, e: 'T'});
        }else{
            label = 'Preview';
            url = getSearchResultUrl({searchid:searchId});
        }
        
        return createLink(label, url);
    }
    
    function createLink(label, url){
        return '<a target="_blank" href="'+ url +'"> ' + label + '</a>';
    }
    
    function getEditSearchUrl(param){
        var creatSearchUrl = url.resolveTaskLink({
            id: CREATE_SEARCH_TASK_ID, // Since there is no direct edit saved search task id, use link for create saved search instead.
            params: param
        });
        
        var searchUrl = creatSearchUrl.replace(/e=F/,'');
        return searchUrl; 
    }
    
    function getSearchResultUrl(param){
        var searchUrl = url.resolveTaskLink({
            id: SEARCH_RESULT_TASK_ID,
            params: param
        });
        
        return searchUrl; 
    }
    
    function getDefaultSavedSearchId(savedSearch){
        var searchId;
        
        try {
            var savedSearchSearch = search.load({
                id: savedSearch
            });
            searchId = savedSearchSearch.searchId;
        }catch(e){
            log.debug('getDefaultSavedSearchId', savedSearch + ' is not found.');
        }
        
        return searchId;
    }
    
    
    return {
        getBankAccountsWithEFTFormats : getBankAccountsWithEFTFormats,
        getBankAccountsWithDDFormats : getBankAccountsWithDDFormats,
        getSearch: getSearch,
        getSearchSummary : getSearchSummary,
        getClientScriptFileId : getClientScriptFileId,
        getSearchLink  : getSearchLink,
        getDefaultSavedSearchId: getDefaultSavedSearchId,
        getProcessingPFACount: getProcessingPFACount
    };
});