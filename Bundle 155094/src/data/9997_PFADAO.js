/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperRecord', '../lib/9997_DAOFactory', '../data/8858_QueueSettingDAO'],

function(search, record, daoFactory, queueSettingDAO) {
    var RECORD_TYPE = 'customrecord_2663_file_admin';
    
    // TODO move to constants and in an enum
    // EP Status
    var PAYPROCESSED = 4;
    var PROCESSED_PAYMENTS_WITH_ERRORS = 17;
    var PROCESSED_PAYMENTS = 16;
    var FOR_CANCELLATION = 18;
    var MARKED = 15;
    var QUEUED = 1;
    var PAYFAILED = 9;
    var PAYERROR = 8;
    var PAYCANCELLED = 10;
    
    // EP Process
    var JOB_PROCESS = '1';
    var ROLLBACK = '3';
    var REVERSE = '4';
    var REPROCESS = '2';
    
    var FIELD_MAP = {
        'name': 'name',
        'paymentType': 'custrecord_2663_payment_type',
        'lastProcess': 'custrecord_2663_last_process',
        'fileProcessed': 'custrecord_2663_file_processed',
        'refNote': 'custrecord_2663_ref_note',
        'bankAccount': 'custrecord_2663_bank_account',
        'paymentsForProcessId': 'custrecord_2663_payments_for_process_id',
        'paymentsForProcessAmt': 'custrecord_2663_payments_for_process_amt',
        'processDate': 'custrecord_2663_process_date',
        'postingPeriod': 'custrecord_2663_posting_period',
        'aggMethod': 'custrecord_2663_agg_method',
        'fileRef': 'custrecord_2663_file_ref',
        'account': 'custrecord_2663_account',
        'paymentsForNotify': 'custrecord_2663_payments_for_notify',
        'paymentsForReversal': 'custrecord_2663_payments_for_reversal',
        'department': 'custrecord_2663_department',
        'classification': 'custrecord_2663_class',
        'location': 'custrecord_2663_location',
        'fileCreationDate': 'custrecord_2663_file_creation_date',
        'reprocessFlag': 'custrecord_2663_reprocess_flag',
        'processingErrors': 'custrecord_2663_processing_errors',
        'paymentSubsidiary': 'custrecord_2663_payment_subsidiary',
        'fileCreationTimestamp': 'custrecord_2663_file_creation_timestamp',
        'sequenceId': 'custrecord_2663_sequence_id',
        'lastCheckNo': 'custrecord_2663_last_check_no',
        'priority': 'custrecord_ep_priority',
        'paymentCreationQueue': 'custrecord_ep_payment_creation_queue',
        'batchid': 'custrecord_2663_batchid',
        'exchangeRates': 'custrecord_2663_exchange_rates',
        'reversalDate': 'custrecord_2663_reversal_date',
        'reversalPeriod': 'custrecord_2663_reversal_period',
        'reversalReasons': 'custrecord_2663_reversal_reasons',
        'notificationMails': 'custrecord_2663_notification_mails',
        'notificationSender': 'custrecord_2663_notification_sender',
        'parentDeployment': 'custrecord_2663_parent_deployment',
        'directDebitType': 'custrecord_2663_direct_debit_type',
        'updatedEntities': 'custrecord_2663_updated_entities',
        'batchDetail': 'custrecord_2663_batch_detail',
        'journalKeys': 'custrecord_2663_journal_keys',
        'totalAmount': 'custrecord_2663_total_amount',
        'totalTransactions': 'custrecord_2663_total_transactions',
        'status': 'custrecord_2663_status',
        'number': 'custrecord_2663_number',
        'timeStamp': 'custrecord_2663_time_stamp',
        'removedKeys': 'custrecord_2663_removed_keys',
        'approvalLevel': 'custrecord_2663_approval_level',
        'prevApprovalLevel': 'custrecord_2663_prev_approval_level',
        'aggregate': 'custrecord_2663_aggregate',
        'fromBatch': 'custrecord_2663_from_batch',
        'notificationSubject': 'custrecord_2663_notification_subject',
        'emailReceivers': 'custrecord_2663_email_receivers',
        'ccReceivers': 'custrecord_2663_cc_receivers',
        'bccReceivers': 'custrecord_2663_bcc_receivers',
        'partiallyPaidKeys': 'custrecord_2663_partially_paid_keys',
        'transactionEntities': 'custrecord_2663_transaction_entities',
        'dueBatchDetails': 'custrecord_2663_due_batch_details',
        'updatedCount': 'custrecord_2663_updated_count',
        'paymentsForProcessDis': 'custrecord_2663_payments_for_process_dis',
        'appliedCredits': 'custrecord_2663_applied_credits',
        'appliedCreditsAmt': 'custrecord_2663_applied_credits_amt',
        'batchWithCredit': 'custrecord_2663_batch_with_credit',
        'batchMapEntityAmt': 'custrecord_2663_batch_map_entity_amt',
        'amountMap': 'custrecord_8858_tranline_amt_map',
        'discountMap': 'custrecord_8858_tranline_disc_map',
        'aggregationList': 'custrecord_8858_aggregation_list',
        'fileGenData': 'custrecord_9997_file_gen_suitelet_data',
        'isLicensed': 'custrecord_8858_is_licensed'
    };
    
    var TEMPLATE_TYPE = {'eft': 1, 'dd': 2};
    
    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };
    
    var dao = daoFactory.getDAO(daoParams);
    
    function retrieve(id) {
        return dao.retrieve(id);
    }
    
    function retrieveNsRecord(id) {
        return dao.retrieveNsRecord(id);
    }
    
    function getFields(pfaId, pfaFieldList) {
        return search.lookupFields({
            type: RECORD_TYPE,
            id: pfaId,
            columns: pfaFieldList
        });
    }
    
    function updateFields(pfaId, keyValueMapping) {
        record.submitFields({
            type: RECORD_TYPE,
            id: pfaId,
            values: keyValueMapping,
        });
    }
    
    function getSequences(options) {
        var companyBankId = options.companyBank.id;
        var templateId = options.paymentFileDetails.templateId;
        var paymentType = options.paymentFileDetails.type;
        
        var filters = [
            ['custrecord_2663_file_processed', 'is', PAYPROCESSED],
            'AND',
            ['custrecord_2663_bank_account', 'is', companyBankId],
            'AND',
            ['custrecord_2663_payment_type', 'is', TEMPLATE_TYPE[paymentType]],
             'AND',
            ['custrecord_2663_sequence_id', search.Operator.ISNOTEMPTY, '']];
        
        if (options.sameDaySequenceOnly) {
            filters.push('AND');
            filters.push(['custrecord_2663_file_creation_timestamp', 'on', options.pfa.createDate]);
        }
        
        var columns = ['custrecord_2663_sequence_id', search.createColumn({
            name: 'custrecord_2663_file_creation_timestamp',
            sort: search.Sort.DESC
        })];
        
        var sequenceSearch = search.create({
            type: 'customrecord_2663_file_admin',
            columns: columns,
            filters: filters
        });
        
        var sequences = [];
        var rs = sequenceSearch.getIterator();
        while (rs.hasNext()) {
            sequences.push(rs.next());
        }
        
        return sequences;
        
    }
    
    function getPFAsForPaymentProcessing(){
        var pfaSearch = search.create({
            type: RECORD_TYPE,
            filters: ['custrecord_2663_file_processed', 'is', MARKED]
        });
        
        var rs = pfaSearch.getIterator();
        var pfaIds = [];
        
        while(rs.hasNext()){
            var result = rs.next();
            pfaIds.push(result.id);
        }
        
        return pfaIds;
    }
    
    function getPFAForPrinting(){
        var pfaSearch = search.create({
            type: RECORD_TYPE,
            filters: ['custrecord_2663_file_processed', 'anyof', [PROCESSED_PAYMENTS, PROCESSED_PAYMENTS_WITH_ERRORS]],
            columns: ['owner', 'custrecord_2663_file_processed']
        });
        
        var rs = pfaSearch.getIterator();
        var pfaForPrinting = {};
        
        if (rs.hasNext()) {
            var result = rs.next();
            pfaForPrinting.id = result.id;
            pfaForPrinting.owner = result.getValue('owner');
            pfaForPrinting.fileProcessed = result.getValue('custrecord_2663_file_processed');
        }
        
        return pfaForPrinting;
    }
    
    function getPFAsForMarking(columns){
        var pfaColumns = columns || [];
        var priorityColumn = search.createColumn({
            name: 'custrecord_ep_priority',
            sort: search.Sort.ASC
        });
        
        pfaColumns.push(priorityColumn);

        var filters = [
           ['custrecord_2663_last_process', 'anyof', [JOB_PROCESS, REVERSE, ROLLBACK, REPROCESS]],
           'and',
           ['custrecord_2663_file_processed', 'is', QUEUED]
        ];
        
        var pfaSearch = search.create({
            type: RECORD_TYPE,
            columns: pfaColumns,
            filters: filters,
        });
        
        var rs = pfaSearch.getIterator();
        var pfaRecs = [];
        
        while(rs.hasNext()){
            var result = rs.next();
            pfaRecs.push(result);
        }
        
        return pfaRecs;
    }
    
    function getPFAForCancellation(){
        var pfaColumns = [];
        var priorityColumn = search.createColumn({
            name: 'custrecord_ep_priority',
            sort: search.Sort.ASC
        });
        
        var lastModifiedColumn = search.createColumn({
            name: 'lastmodified',
            sort: search.Sort.ASC
        })
        
        pfaColumns.push(priorityColumn);
        pfaColumns.push(lastModifiedColumn);

        var filters = [
           'custrecord_2663_file_processed', 'is', FOR_CANCELLATION
        ];
        
        var pfaSearch = search.create({
            type: RECORD_TYPE,
            columns: pfaColumns,
            filters: filters,
        });
        
        var rs = pfaSearch.getIterator();
        
        
        if (rs.hasNext()){
            var curr = rs.next();
            return retrieve(curr.id);
        }
        
        return null;
    }
    
    function getProcessingPFACount(pfaRecord, currentQueueSetting){
        var NOT_PROCESSING_LIST = [PAYPROCESSED, PAYFAILED, PAYERROR, PAYCANCELLED, QUEUED];
        
        var columns = [];
        
        var filters = [
            ['custrecord_2663_file_processed', search.Operator.NONEOF, NOT_PROCESSING_LIST],
            'and',
            ['custrecord_2663_file_processed', search.Operator.NONEOF, '@NONE@'], // pfa record which is for bill payment batch has no processing status
            'and',
            ['internalid', search.Operator.NONEOF, pfaRecord.id]
        ];
        
        if(currentQueueSetting){
            var deployments = [ currentQueueSetting.schedulerDeployment, currentQueueSetting.parentDeployment];
            filters.push('and');
            filters.push(
                [  // search among pfa's using ss1 & ss2 deployments
                    ['custrecord_2663_parent_deployment', search.Operator.IS, deployments[0]],
                    'or',
                    ['custrecord_2663_parent_deployment', search.Operator.IS, deployments[1]]
                ]
            );
        }

        var pfaSearch = search.create({
            type: RECORD_TYPE,
            filters: filters,
            columns: columns
        });
        
        var pfaList = [];
        var iterator = pfaSearch.getIterator();
        while (iterator.hasNext()) {
            var result = iterator.next();
            pfaList.push(result.id);
        }
        
        log.debug('getProcessingPFACount result', pfaList);
        return pfaList.length;
     }
    
    
    function retrieveViaLookup(id){
        return dao.retrieveViaLookup(id);
    }
    
    return {
        retrieve: retrieve,
        retrieveNsRecord: retrieveNsRecord,
        retrieveViaLookup: retrieveViaLookup,
        getFields: getFields,
        updateFields: updateFields,
        getSequences: getSequences,
        getPFAsForMarking: getPFAsForMarking,
        getPFAsForPaymentProcessing: getPFAsForPaymentProcessing,
        getPFAForPrinting: getPFAForPrinting,
        getPFAForCancellation: getPFAForCancellation,
        getProcessingPFACount: getProcessingPFACount
    };
});
