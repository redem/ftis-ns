/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Jun 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([
        '../data/9997_PaymentFileGenerationDAO',
        'N/config',
        '../lib/wrapper/9997_NsWrapperRuntime',
        '../lib/8858_Translator',
        '../lib/8858_TranslationServiceCaller'
        ],

function(paymentFileGenerationDAO, config, runtime, translator, translationService) {
    var CLIENT_SCRIPT_NAME = '9997_payment_file_generation_cs.js';

    var TITLE = 'eft.form.title';
    var PAYMENT_FIELD_GROUP_LABEL = 'eft.form.fieldGroupLabel';
    var REFERENCE_NOTE_LABEL = 'eft.form.referenceNoteLabel';

    var MESSAGEMAP;
    function initializeMessageTranslationMap(){
        var messageCodes = [TITLE, 
                            PAYMENT_FIELD_GROUP_LABEL, 
                            REFERENCE_NOTE_LABEL];
        var map;
        var execContext = runtime.executionContext;
        
        if (execContext == runtime.ContextType.USER_INTERFACE) {
            map = translationService.translate(messageCodes);
        } else {
            map = translator.getStringMap(messageCodes);
        }
        MESSAGEMAP = {};
        MESSAGEMAP[TITLE] = map[TITLE];
        MESSAGEMAP[PAYMENT_FIELD_GROUP_LABEL] = map[PAYMENT_FIELD_GROUP_LABEL];
        MESSAGEMAP[REFERENCE_NOTE_LABEL] = map[REFERENCE_NOTE_LABEL];
    }

    function getBankAccounts() {
        return paymentFileGenerationDAO.getBankAccountsWithEFTFormats();
    }

    function create() {
        if (!MESSAGEMAP) {
            initializeMessageTranslationMap();
        }

        return {
            title: MESSAGEMAP[TITLE],
            clientScriptFile: CLIENT_SCRIPT_NAME,
            paymentFieldGroupLabel: MESSAGEMAP[PAYMENT_FIELD_GROUP_LABEL],
            referenceNoteLabel: MESSAGEMAP[REFERENCE_NOTE_LABEL],
            savedSearchFieldId: 'custscript_9997_file_gen_saved_search',
            getBankAccounts: getBankAccounts,
            defaultSavedSearch: 'customsearch_9997_payments_for_ep'
        };
    }
    
    return {
        create: create
    };
})
