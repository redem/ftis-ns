/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Dec 2016     mjaurigue
 *
 * @NModuleScope Public
 */

define(["../lib/wrapper/9997_NsWrapperRecord",
        "../lib/wrapper/9997_NsWrapperRuntime",
        "../lib/wrapper/9997_NsWrapperSearch",
        "../lib/wrapper/9997_NsWrapperFormat"], 
        function(record, runtime, search, format) {
	var THREAD_PROCESSING_RESULT_RECORD = "customrecord_ep_proc_results";
	var PFA_ID_FIELD = "custrecord_ep_pfa_id";
	var DEPLOYMENT_ID_FIELD = "custrecord_ep_deployment_id";
	var STATE_FIELD = "custrecord_ep_state";
	var NUMBER_OF_TRANSACTIONS_FIELD = "custrecord_ep_num_trans";
	var PAID_TRANSACTIONS_FIELD = "custrecord_ep_paid_trans";
	var THREAD_TRANSACTIONS_FIELD = "custrecord_ep_thread_transactions";
	var THREAD_LAST_UPDATE_FIELD = "custrecord_ep_thread_last_updated";
	
	function createMonitorThread(pfa) {
		var threadTransactions = [];
		var recId;
		
		try {
			var amounts = JSON.parse(pfa.amountMap);
	        var discounts = JSON.parse(pfa.discountMap);
			var paymentObjects = JSON.parse(pfa.aggregationList);
			
			for (var i = 0; i < paymentObjects.length; i++) {
				threadTransactions.push(createThreadDetail(paymentObjects[i], amounts, discounts));
			}
			
			var processResultRec = record.create({
				type: THREAD_PROCESSING_RESULT_RECORD,
				isDynamic: true
			});
			
			processResultRec.setValue(PFA_ID_FIELD, pfa.id);
			processResultRec.setValue(DEPLOYMENT_ID_FIELD, runtime.getCurrentScript().deploymentId);
			processResultRec.setValue(STATE_FIELD, pfa.fileProcessed);
			processResultRec.setValue(NUMBER_OF_TRANSACTIONS_FIELD, paymentObjects.length);
			processResultRec.setValue(THREAD_TRANSACTIONS_FIELD, JSON.stringify(threadTransactions));
			processResultRec.setValue(PAID_TRANSACTIONS_FIELD, 0);
			recId = processResultRec.save();
		} catch(ex) {
			log.debug("Error while creating thread monitor records", ex);
		}
		
		return recId;
	}
	
	function createThreadDetail(paymentObject, amounts, discounts){
		var transactions = [];
		
		var entity = paymentObject.entity;
		var currency = paymentObject.curr;
	    var transactions = [];
		var tranDetail;
		var key;
		var amount;
		var discount;
		var id;
		var line;
		var transId;
		var transLookupValues;
		
		for (var k = 0; k < paymentObject.trans.length; k++) {
			id = paymentObject.trans[k].id;
			line = paymentObject.trans[k].line;
			key = [id, line].join('-');
		    amount = Number(amounts[key]);
		    discount = Number(discounts[key]);
		    transLookupValues = search.lookupFields({type: "transaction", id: id, columns: ["tranid"]});
		    transId = transLookupValues.tranid;
		    
			tranDetail = {
					id: id,
					transId: transId,
					transLine: line,
					transPayAmt: amount,
					transDiscAmt: discount
				};
			
			transactions.push(tranDetail);
		}
		
		var threadDetail = {"entity": entity, "transactions": transactions, "currency": currency};
		
		return threadDetail;
	}
	
	function updateState(params){
		var id = params.processResultRecId;
		var state = params.state;
		
		var submitfieldValues = {};
		submitfieldValues[STATE_FIELD] = state;
		submitfieldValues[THREAD_LAST_UPDATE_FIELD] = format.format({value: new Date(), type: format.Type.DATETIMETZ});
		
		if (!isNaN(params.transactionCount)){
		    submitfieldValues[PAID_TRANSACTIONS_FIELD] = params.transactionCount; 
		}
		
		record.submitFields({type: THREAD_PROCESSING_RESULT_RECORD, id: id, values: submitfieldValues});
	}
	
    return {
        createMonitorThread: createMonitorThread,
        updateState: updateState
    };
    
});

