/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author aaclan
 * @NScriptName 9997_PaymentFileGenerationPageBuilder
 * @NApiVersion 2.0
 * @NModuleScope Public
 */

define(['require',
        '../lib/wrapper/9997_NsWrapperServerWidget',
        '../lib/wrapper/9997_NsWrapperFormat',
        '../lib/9997_FormBuilder',
        '../data/9997_PaymentFileGenerationDAO',
        '../lib/wrapper/9997_NsWrapperConfig',
        '../lib/wrapper/9997_NsWrapperRuntime'
       ],

function(require, serverWidget, format, formBuilder, paymentFileGenerationDAO, config, runtime) {

	var FLH_COMP_BANK_DETAILS = 'Select the name of the Company Bank Details record associated with the company bank account used for electronic payments.';
	var FLH_SAVED_SEARCH = 'Select the transaction saved search to be used for retrieving the payment transactions.\n\nFor information about creating transaction saved searches, see the Defining Transaction Saved Searches for Electronic Payment topic in the Help Center.';
	var FLH_REFERENCE_NOTE = 'Enter a note to reference this payment batch.';
	var FLH_FILE_FORMAT = 'This field displays the payment file format that has been set on the Company Bank Details record.';
	var FLH_DATE = 'This field displays the current date as the date of processing by the bank. You can specify the date when you want the bank to process the payment.';
	var FLH_BANK_CURRENCY = 'This field displays the currency associated with the GL bank account.';
	var FLH_GL_BANK = 'This field displays the GL bank account where the transactions processed for electronic payment will be posted.';
	var FLH_SUBSIDIARY = 'This field displays the subsidiary associated with the GL bank account.';
	var FLH_SUBSIDIARY_CURRENCY = 'This field displays the base currency of the subsidiary associated with the GL bank account.';
	var FLH_COUNT = 'This field displays the total number of valid transactions for electronic payment, based on the results of your selected saved search.';
	var FLH_TOTAL = 'This field displays the computed total amount of all transactions for electronic payment, based on the results of your selected saved search.';
	
	var SAVED_SEARCHES_INTERNAL_ID = '-119'; // See Help Doc: Using Internal IDs, External IDs, and References
    var PAYMENTS_SEARCH_FLD_GRP = 'paymenttransearchid';
    var PAYMENT_INFO_FLD_GRP = 'paymentinfo';
    var CLIENT_SCRIPT_NAME = '9997_payment_file_generation_cs.js';

    var EP_PAYMENT_FORM_DEFINITION = {
        'eft' : './9997_EFTFormDefinition.js',
        'dd' : './9997_DDFormDefinition.js'
    };
    
    var templateType = runtime.getCurrentScript().getParameter('custscript_9997_file_gen_type') || 'eft';
    var formDefinitionClass = EP_PAYMENT_FORM_DEFINITION[templateType.toLowerCase()];
    var isOW = runtime.isOW();
    
    var formDefinitionDS;
        require([formDefinitionClass], function(module) {
            formDefinitionDS = module.create();
        });
    
    var formDefinition = {
        title: formDefinitionDS.title,
        
        clientScriptId:  paymentFileGenerationDAO.getClientScriptFileId(formDefinitionDS.clientScriptFile),
        
        fieldgroups: [
            {
                id: PAYMENTS_SEARCH_FLD_GRP,
                label: 'Payment Transaction Search',
            },
            {
                id: PAYMENT_INFO_FLD_GRP,
                label: formDefinitionDS.paymentFieldGroupLabel,
            },
        ],
        
        fields: [
            {
                id: 'bankaccount',
                type: serverWidget.FieldType.SELECT,
                label: 'Company Bank Account',
                container: PAYMENTS_SEARCH_FLD_GRP,
                isMandatory: true,
                help: FLH_COMP_BANK_DETAILS
            },
            {
                 id: 'bankaccountinfo',
                 type: serverWidget.FieldType.LONGTEXT,
                 label: 'Company Bank Account Info',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.HIDDEN,
             },
             {
                 id: 'bankcurrency',
                 type: serverWidget.FieldType.TEXT,
                 label: 'Bank Account Currency',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.INLINE,
                 help: FLH_BANK_CURRENCY
             },
             {
                 id: 'glbankaccount',
                 type: serverWidget.FieldType.TEXT,
                 label: 'GL Bank Account',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.INLINE,
                 help: FLH_GL_BANK
             },
             {
                 id: 'subsidiary',
                 type: serverWidget.FieldType.TEXT,
                 label: 'Subsidiary',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: isOW ? serverWidget.FieldDisplayType.INLINE : serverWidget.FieldDisplayType.HIDDEN,
             },
             {
                 id: 'subsidiaryid',
                 type: serverWidget.FieldType.TEXT,
                 label: 'Subsidiary ID',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.HIDDEN,
                 help: FLH_SUBSIDIARY
             },
             {
                 id: 'basecurrency',
                 type: serverWidget.FieldType.TEXT,
                 label: 'Subsidiary Base Currency',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: isOW ? serverWidget.FieldDisplayType.INLINE : serverWidget.FieldDisplayType.HIDDEN,
                 help: FLH_SUBSIDIARY_CURRENCY
             },
             {
                 id: 'fileformat',
                 type: serverWidget.FieldType.TEXT,
                 label: 'Payment File Format',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.INLINE,
                 help: FLH_FILE_FORMAT
             },
             {
                 id: 'fileformatid',
                 type: serverWidget.FieldType.TEXT,
                 label: 'Payment File Format ID',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.HIDDEN,
             },
             {
                 id: 'templatetype',
                 type: serverWidget.FieldType.TEXT,
                 label: 'Payment File TemplateType',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.HIDDEN,
                 defaultValue: templateType
             },
             {
                 id: 'transavedsearch',
                 type: serverWidget.FieldType.SELECT,
                 label: 'Select Transaction Saved Search',
                 source: SAVED_SEARCHES_INTERNAL_ID,
                 breakType : serverWidget.FieldBreakType.STARTCOL,
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 isMandatory: true,
                 help: FLH_SAVED_SEARCH
             },
             {
                 id: 'editsearch',
                 type: serverWidget.FieldType.TEXT,
                 label: ' ',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.INLINE,
                 layoutType: serverWidget.FieldLayoutType.STARTROW
             },
             {
                 id: 'previewsearch',
                 type: serverWidget.FieldType.TEXT,
                 label: ' ',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.INLINE,
                 layoutType: serverWidget.FieldLayoutType.ENDROW
             },
             {
                 id: 'summary',
                 type: serverWidget.FieldType.INLINEHTML,
                 label: 'Summary',
                 defaultValue: '<br/><h1>Summary</h1>',
                 container: PAYMENTS_SEARCH_FLD_GRP,
             },
             {
                 id: 'trancount',
                 type: serverWidget.FieldType.TEXT,
                 label: 'No. of Transactions',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.INLINE,
                 help: FLH_COUNT
             },
             {
                 id: 'totalamount',
                 type: serverWidget.FieldType.TEXT, // currency field type is not used since it does weird formatting, value being set here should be formatted first
                 label: 'Total Amount for Payment',
                 container: PAYMENTS_SEARCH_FLD_GRP,
                 displayType: serverWidget.FieldDisplayType.INLINE,
                 help: FLH_TOTAL
             },
             {
                 id: 'datetobeprocessed',
                 type: serverWidget.FieldType.DATE,
                 label: 'Date To Be Processed',
                 container: PAYMENT_INFO_FLD_GRP,
                 isMandatory: true,
                 layoutType: serverWidget.FieldLayoutType.ENDROW,
                 help: FLH_DATE
             },
             {
                 id: 'referencenote',
                 type: serverWidget.FieldType.TEXT,
                 label: formDefinitionDS.referenceNoteLabel,
                 container: PAYMENT_INFO_FLD_GRP,
                 isMandatory: true,
                 help: FLH_REFERENCE_NOTE
             },
         ],
        
        submitButton: {
            id: 'generatefile',
            label: 'Generate File',
        }
    };
   
    function buildPage(params){
        setProperties(params);
        var form = formBuilder.buildForm(formDefinition);
        return form;
    }
    
    function setProperties(params){
        var searchId;
        
        searchId = getCachedSavedSearch(formDefinitionDS.savedSearchFieldId) || paymentFileGenerationDAO.getDefaultSavedSearchId(formDefinitionDS.defaultSavedSearch);
        
        var bankAccounts = formDefinitionDS.getBankAccounts();
        
        var propertiesByField = {
            'bankaccount': {
                selectOptions: getBankAccountOptions(bankAccounts),
            },
            'transavedsearch': {
                defaultValue: searchId,
            },
            'editsearch': {
                defaultValue: paymentFileGenerationDAO.getSearchLink(searchId, true),
            },
            'previewsearch': {
                defaultValue: paymentFileGenerationDAO.getSearchLink(searchId),
            },
            
        };
        
        for (var i = 0; i < formDefinition.fields.length ; i++){
            var field = formDefinition.fields[i];
            var additionalProperties = propertiesByField[field.id];
            if(additionalProperties){
                for (var property in additionalProperties ){
                    field[property] = additionalProperties[property];
                }
                
                formDefinition.fields[i] = field;
            }
        }
    }
    
    function getBankAccountOptions(bankAccounts){
        var options = [];
        
        options.push({ value: '{}', text: '&nbsp;' }); // blank option
        for (var i = 0; i < bankAccounts.length; i++){
            var bankAccount = bankAccounts[i];
            
            var text = bankAccount.name;
            var value = JSON.stringify(bankAccount);
            
            options.push({ value: value, text: text });
        }
        
        return options;
    }
    
    function getCachedSavedSearch(scriptField){
        var cachedSavedSearch = config.load({ type: config.Type.USER_PREFERENCES }).getValue({ fieldId: scriptField });
        try {
            // check if saved search is active by trying to save it
            var objConfig = config.load({ type: config.Type.USER_PREFERENCES });
            objConfig.setValue({ fieldId: scriptField, value: cachedSavedSearch });
            objConfig.save();
        }
        catch(ex){
            // failure to save means saved search is inactive, set to null instead
            cachedSavedSearch = null;
        }
        return cachedSavedSearch;
    }
    
    var PageBuilder = {
        buildPage : buildPage
    };
    
    return PageBuilder;
});