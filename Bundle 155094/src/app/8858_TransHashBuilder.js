/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * @NModuleScope Public
 * @NApiVersion 2.0
 */
define([],

function() {
    var TRANSACTION_APPLYING_TO_FIELD = 'applyingtransaction';
    var FM_TRANSACTION_HASHNAME  = 'tx';
    
    function getTransHashString(dataSource){
        var transHash = {};
        var transactions = dataSource.getTransactions() || [];
        
        for (var transactionIdx = 0; transactionIdx < transactions.length; transactionIdx++){
            var transaction = transactions[transactionIdx];
            var paymentId = transaction.getValue(TRANSACTION_APPLYING_TO_FIELD);
            var transactionInFMList = transHash[paymentId] || [];
            var transactionInFM = FM_TRANSACTION_HASHNAME + '[' + transactionIdx + ']';
            transactionInFMList.push(transactionInFM);
            transHash[paymentId] = transactionInFMList;
        }
        
        return stringifyHashOfStringList(transHash);
    }
    
    function stringifyHashOfStringList(hash){
        var keyValuePairs = [];
        for (var key in hash){
            var list = hash[key];
            var keyStr = '"'+ key +'"';
            var valStr ='[' + list.join(',') + ']';
            keyValuePairs.push( keyStr + ':' + valStr );
        }
        return '{' + keyValuePairs.join(',') + '}';
    }
    
    return {
        getTransHashString : getTransHashString,
    };
});
