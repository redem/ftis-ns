/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount
 */

define(['../lib/wrapper/9997_NsWrapperError'],

function (error) {
    function ListIterator(list, maxLines) {
        var MAX_LINES = maxLines || 1000;
        
        if (list === null || list === undefined) {
            list = [];
        }

        var startIndex = 0;
        var endIndex = MAX_LINES;

        if (list instanceof Array === false) {
            throw error.logAndCreateError('INVALID_LIST_TYPE', 'The list entered should be an Array');
        }

        this.hasNext = function hasNext() {
            return startIndex < list.length;
        };

        this.next = function next() {
            var sublist = list.slice(startIndex, endIndex);
            startIndex += MAX_LINES;
            endIndex += MAX_LINES;

            return sublist;
        };
    }

    function create(list, maxLines) {
        return new ListIterator(list, maxLines);
    }

    return {
        create: create,
    };
});
