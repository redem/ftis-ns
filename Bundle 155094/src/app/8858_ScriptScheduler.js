/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2016     aalcabasa
 *
 * @NModuleScope Public
 */
define([
    '../lib/wrapper/9997_NsWrapperSearch',
    '../lib/wrapper/9997_NsWrapperTask',
    '../data/9997_PFADAO',
    '../lib/wrapper/9997_NsWrapperError',
    '../lib/8858_Translator',
    '../lib/wrapper/9997_NsWrapperRecord',
    '../data/8858_ProcessQueueDAO',
    '../app/8858_DeploymentManager',
    '../lib/wrapper/9997_NsWrapperRuntime',
    '../data/8858_QueueSettingDAO'
    ],

function(search, task, pfaDAO, error, translator, record, processQueueDAO, deploymentManager, runtime, queueSettingDAO) {
    var EP_INVALID_PROCESS_ID = 'EP_INVALID_PROCESS_ID';
    var NO_AVAILABLE_DEPLOYMENT = 'NO_AVAILABLE_DEPLOYMENT';
    var PFA_RECORD = 'customrecord_2663_file_admin';
    
    // EP Processes
    var EP_PROCESSPAYMENTS = '1';
    var EP_REPROCESS = '2';
    var EP_ROLLBACK = '3';
    var EP_REVERSEPAYMENTS = '4';
    var EP_EMAILNOTIFICATION = '5';
    var EP_CREATEFILE = '6';
    var EP_CREATEBATCH = '7';
    var EP_CLOSEBATCH = '8';
    var EP_REMOVEUNPROCESSTRANS = '9';
    var EP_MARKTRANSACTIONS = '10';
    var EP_GENERATEBANKFILE = '11';
    
    var EP_SCHEDULER = 'SCHEDULER';//special case for rescheduling this SS
    
    // Payment Status's
    var PAYQUEUED = 1;
    var PAYMARKEDTRANS = 15;
    var PAYPROCESSEDPAYMENTS = 16;
    var PAYPROCESSEDPAYMENTS_WITH_ERRORS = 17;
    var FOR_CANCELLATION = 18;
    var PAYPROCESSED = 4;
    
    // Processing Status
    var PROCESSING_PAYMENTS = 2;
    var MARKING = 5;
    var CREATING_FILE = 3;
    var PROCESSING_REVERSAL = 6;
    var PROCESSING_NOTIF = 7;
    var UNLINKING = 14;
    
    var SS1_SCRIPT_SCHEDULER_SCRIPTID = 'customscript_2663_payment_processing_ss';

    var PROCESS_SETTINGS = {};
    
    PROCESS_SETTINGS[EP_MARKTRANSACTIONS] = {
        pendingStatusList: [PAYQUEUED],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_8859_transaction_marker',
        pfa_id_param: 'custscript_8858_trans_marker_pfa_id',
        processingStatus: MARKING
    };

    PROCESS_SETTINGS[EP_PROCESSPAYMENTS] = {
        pendingStatusList: [PAYMARKEDTRANS],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_8858_payment_processor_mr', 
        pfa_id_param: 'custscript_8858_payment_proc_pfa_id'
    };

    PROCESS_SETTINGS[EP_CREATEFILE] = {
        pendingStatusList: [PAYPROCESSEDPAYMENTS, PAYPROCESSEDPAYMENTS_WITH_ERRORS],
        taskType: task.TaskType.SCHEDULED_SCRIPT,
        scriptId: 'customscript_9997_file_generation_ss',
        pfa_id_param: 'custscript_9997_file_generation_pfa',
        processingStatus: CREATING_FILE
    };

    PROCESS_SETTINGS[EP_ROLLBACK] = {
        pendingStatusList: [FOR_CANCELLATION],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_8859_ep_rollback_mr',
        pfa_id_param: 'custscript_8859_ep_rollback_mr_pfa',
        processingStatus: PROCESSING_REVERSAL
    };

    PROCESS_SETTINGS[EP_SCHEDULER] = {
        pendingStatusList: [PAYPROCESSEDPAYMENTS, PAYPROCESSEDPAYMENTS_WITH_ERRORS],
        taskType: task.TaskType.SCHEDULED_SCRIPT,
        scriptId: 'customscript_8858_scheduler_ss',
    };

    PROCESS_SETTINGS[EP_REPROCESS] = {
        pendingStatusList: [],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_8859_ep_rollback_mr',
        pfa_id_param: 'custscript_8859_ep_rollback_mr_pfa',
        processingStatus: PROCESSING_REVERSAL
    };

    PROCESS_SETTINGS[EP_REVERSEPAYMENTS] = {
        pendingStatusList: [],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_8858_reverse_payments_mr',
        pfa_id_param: 'custscript_8858_reverse_payments_pfa_id',
        processingStatus: PROCESSING_REVERSAL
    };

    PROCESS_SETTINGS[EP_REMOVEUNPROCESSTRANS] = {
        pendingStatusList: [],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_9997_trans_unlinker_mr',
        pfa_id_param: 'custscript_9997_pfa_id',
        processingStatus: UNLINKING
    }; 
    
    PROCESS_SETTINGS[EP_GENERATEBANKFILE] = {
        pendingStatusList: [],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_9997_payment_to_admin_mr',
        pfa_id_param: 'custscript_9997_file_gen_pfa_id',
        processingStatus: MARKING
    };
    
    PROCESS_SETTINGS[EP_EMAILNOTIFICATION] = {
        pendingStatusList: [],
        taskType: task.TaskType.MAP_REDUCE,
        scriptId: 'customscript_8859_email_notification_mr',
        pfa_id_param: 'custscript_8859_email_notification_pfa',
        processingStatus: PROCESSING_NOTIF
    };

    function getProcessSettings(processId, params){
        var processDeployment;
    
        if(params && params.isLicensed){
            processDeployment = deploymentManager.getProcessDeployment(processId, params);
        } else {
            processDeployment = deploymentManager.getDefaultDeployment(processId);
        }
        var processSetting = PROCESS_SETTINGS[processId];
        processSetting.deploymentId = processDeployment;    
        
        if (params.currentDeployment === processDeployment && processSetting.taskType === task.TaskType.MAP_REDUCE){
            processSetting = PROCESS_SETTINGS[EP_SCHEDULER];
            processSetting.deploymentId = params.schedulerDeployment;
            log.debug('getProcessSettings', 'Current Deployment is same with next deployment, triggering scheduler');
        }
        
        return processSetting;
    }
    
    function startNewProcess(){
        var runId;
        
        var pfaRecord = getPFAToBeProcessed();
        log.debug("PFA Record for Processing", JSON.stringify(pfaRecord));
        
        if (pfaRecord) {
            runId = initiateProcessing(pfaRecord);
        } else {
            log.debug('startNewProcess', 'No PFAs to process.');
        }
        
        return runId;
    }
    
    function initiateProcessing (pfaRecord) {
        var runId;
        var pfaId = pfaRecord.id;
        var processUser = pfaRecord.processUser;
        var isUsingSS2 = pfaRecord.usingSStwo;
        var subsidiary = pfaRecord.paymentSubsidiary;
        
        var currentDeployment = runtime.getCurrentScript().deploymentId;
        var currentQueueSetting = queueSettingDAO.getByDeployment(currentDeployment);
        var schedulerDeployment = currentQueueSetting ? currentQueueSetting.schedulerDeployment : null;
        var currentQSParentDeployment = currentQueueSetting.parentDeployment;
        
        var processingPFACount = pfaDAO.getProcessingPFACount(pfaRecord, currentQueueSetting);
        
        if (processingPFACount != 0) {
            log.debug('initiateProcessing', 'There are other processing PFAs, count:'+ processingPFACount);
            return;
        }
        
        if (isUsingSS2 || (pfaRecord.lastProcess == EP_REMOVEUNPROCESSTRANS)) { // removal of unprocessed is always via ss2
            /* Proceed with SS 2.0 process */
            var processId = getProcessId(pfaRecord);
            
            var isLicensed = pfaRecord.isLicensed;
            log.debug('processId: ' + processId);
            
            
            var params = {
                pfaId: pfaId,
                subsidiary: subsidiary,
                isLicensed: isLicensed,
                schedulerDeployment: schedulerDeployment,
                currentDeployment: currentDeployment,
                isEntryPoint: true // should only update status of PFA if this is the process entry point
            };
            
            runId = triggerProcess(processId, params);
            
        } else {
            /* Divert to SS 1.0 Scheduler */
            var ssTask = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
            ssTask.scriptId = SS1_SCRIPT_SCHEDULER_SCRIPTID;
            ssTask.deploymentId = (pfaRecord.parentDeployment) ? pfaRecord.parentDeployment : currentQSParentDeployment;

            ssTask.params = {
                'custscript_2663_file_id': pfaId,
                'custscript_8858_payment_processing_user': processUser
            };
            
            if(isLookedUpPFAQueued(pfaId)){
                try {
                    runId = ssTask.submit();
                    log.debug('SS 1.0  Scheduler was triggered.');
                } catch (e){
                    log.error(e.name, e.message);
                }
            }
        }
        
        log.debug('Run ID', runId );
        
        return runId;
    }
    
    function triggerProcess(processId, params){
        var settings = getProcessSettings(processId, params);
        
        log.debug('triggerProcess', 'settings: '+JSON.stringify(settings));
        
        if(!settings){
            log.debug('Process ID', processId);
            var errorMessage = translator.getString(EP_INVALID_PROCESS_ID);
            throw error.logAndCreateError(EP_INVALID_PROCESS_ID, errorMessage);
        }
        
        if(!settings.deploymentId){
            log.debug('Script ID', settings.scriptId);
            var errorMessage = translator.getString(NO_AVAILABLE_DEPLOYMENT);
            throw error.logAndCreateError(NO_AVAILABLE_DEPLOYMENT, errorMessage);
        }
        
        var mrTask = task.create({ taskType: settings.taskType });
        mrTask.scriptId = settings.scriptId;
        mrTask.deploymentId = settings.deploymentId;
        
        if (params){
            params[settings.pfa_id_param] = params.pfaId;
            mrTask.params = params;
        }
        
        var pfaId = params.pfaId;
        var runId;
        var pfaNSRecord = pfaDAO.retrieveNsRecord(pfaId); // use load and save for NS optimistic locking
        var errorCode;
        if(params.isEntryPoint){
            
            if(isPFANSRecordQueued(pfaNSRecord)){ // make sure that this pfa is not being processed before triggering the process
                if(settings.processingStatus){
                    pfaNSRecord.setValue({ fieldId: 'custrecord_2663_file_processed', value: settings.processingStatus});
                } 
                
                if(params.schedulerDeployment){
                    log.debug('triggerProcess','Assigning new parentDeployment:' + params.schedulerDeployment);
                    pfaNSRecord.setValue({ fieldId: 'custrecord_2663_parent_deployment', value: params.schedulerDeployment});
                }
                
                // since altname is a required field, set its value same as pfa name
                var pfaName = pfaNSRecord.getValue('name');
                pfaNSRecord.setValue({ fieldId: 'altname', value: pfaName});
                
                try {
                    pfaNSRecord.save();
                    runId = mrTask.submit();
                    log.debug('triggerProcess PFA id:'+ params.pfaId, 'Triggered process');
                } catch (e){
                    if(e.name != 'CUSTOM_RECORD_COLLISION'){ // error didn't occure in PFA update, set pfa back to queued
                        pfaDAO.updateFields(params.pfaId, {'custrecord_2663_file_processed': PAYQUEUED});
                    }
                    log.error(e.name, e.message);
                    startNewProcess();
                }

            } else {
                log.debug('triggerProcess PFA id:'+ params.pfaId, 'Current front PFA is already processing. Starting another process.');
                startNewProcess();
            }
            
        } else {
            runId = mrTask.submit();
        }
        
        log.debug('Script Triggered', JSON.stringify(mrTask));
        
        return runId;
        
    }
    
    function isLookedUpPFAQueued(pfaId){
        var pfaRecord = pfaDAO.retrieve(pfaId);
        var currentState = Number(pfaRecord.fileProcessed);
        log.debug('isLookedUpPFAQueued', JSON.stringify({pfaId: pfaId, currentState: currentState}));
        return (currentState == PAYQUEUED);
    }
    
    function isPFANSRecordQueued(pfaNSRecord){
        var currentState = Number(pfaNSRecord.getValue('custrecord_2663_file_processed'));
        log.debug('isPFANSRecordQueued', JSON.stringify({pfaId: pfaNSRecord.id, currentState: currentState}));
        return (currentState == PAYQUEUED);
    }
    
    function getPFAToBeProcessed(){
        return processQueueDAO.getFront();
    }
    
    function getProcessId(pfaRecord){
        var processId;
        var initiatedProcess = pfaRecord.lastProcess;
        
        if (initiatedProcess == EP_PROCESSPAYMENTS) {
            /* Payment Processing */
            processId = EP_MARKTRANSACTIONS;
        } else {
            processId = initiatedProcess;
        }
        
        return processId;
    }
    
    // TODO move to PFA DAO
    function getPFACountForProcessing(statusList){
       var filters = [
           ['custrecord_2663_file_processed', search.Operator.ANYOF, statusList]
       ];

       var columns = [];
       var countCol = search.createColumn({
           name: 'internalid',
           summary: 'count'
       });
       columns.push(countCol);
       
       var pfaSearch = search.create({
           type: PFA_RECORD,
           filters: filters,
           columns: columns
       });
       
       var count = 0;
       
       var iterator = pfaSearch.getIterator();
       if (iterator.hasNext()) {
           var result = iterator.next();
           count = result.getValue(countCol);
       }
       
       log.debug('PFA Count', count);
       
       return Number(count);
    }
    
    return {
        triggerProcess: triggerProcess,
        startNewProcess: startNewProcess
    };
});
