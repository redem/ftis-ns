/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([
    '../lib/wrapper/9997_NsWrapperError',
    './9997_EPDataSourceProcessor',
    '../lib/wrapper/9997_NsWrapperFile',
    './9997_PaymentFileContentPostProcessor',
    '../data/9997_PFADAO',
    '../lib/8858_Translator'],

function(error, epDataSourceProcessor, file, postProcessor,pdaDAO, translator) {
    var EP_TEMPLATE_RENDERER_ERROR = 'EP_TEMPLATE_RENDERER_ERROR';

    function generateFile(parameters) {
        var templateRenderer = parameters.templateRenderer;
        var dataSource = parameters.dataSource;
        
        epDataSourceProcessor.processDataSource({
            dataSource: dataSource,
            templateRenderer: templateRenderer
        });
        
        var renderedContent = '';
        try {
            renderedContent = templateRenderer.renderAsString();
        } catch (e) {
            log.error('generateFile', e);
            throw error.create({
                name: EP_TEMPLATE_RENDERER_ERROR,
                message: translator.getString(EP_TEMPLATE_RENDERER_ERROR)
            });
        }
        
        var fileData = dataSource.getFileDetails();
        
        var processedResults = postProcessor.processRenderedContent(renderedContent, fileData.isXML);
        
        fileData.contents = processedResults.fileContent;
        
        if (processedResults.sequenceId){
            pdaDAO.updateFields(dataSource.pfaId, {
                'custrecord_2663_sequence_id':processedResults.sequenceId
            });
        }
        
        
        var paymentFile = file.create(fileData);
        var fileId = paymentFile.save();
        
        log.debug('Generated File ID', fileId);
        
        return fileId;
    }
    
    return {
        generateFile: generateFile
    };
});
