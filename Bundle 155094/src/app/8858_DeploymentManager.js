/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Oct 2016     mjaurigue
 *
 * @NModuleScope Public
 */
define(['../lib/wrapper/9997_NsWrapperTask',
        '../data/8858_ScheduledScriptInstanceDAO',
        '../data/8858_QueueSettingDAO'],

function(task, ssInstanceDao, queueSettingDao) {
    var QS_TRANSACTION_MARKER_DEPLOYMENT_FIELD = "custrecord_2663_qs_tranmarker_deployment";
    var QS_PAYMENT_PROCESS_DEPLOYMENT_FIELD = "custrecord_2663_qs_payme_proc_deployment";
    var QS_FILE_CREATOR_DEPLOYMENT_FIELD = "custrecord_2663_qs_file_creat_deployment";
    var QS_ROLLBACK_DEPLOYMENT_FIELD = "custrecord_2663_qs_rback_mr_deployment";
    var QS_REVERSAL_DEPLOYMENT_FIELD = "custrecord_2663_qs_reverse_mr_deployment";
    var QS_REMOVED_UNPROCESSED_DEPLOYMENT_FIELD = "custrecord_2663_qs_remove_mr_deployment";
    var QS_BANK_FILE_GENERATION_DEPLOYMENT_FIELD = "custrecord_2663_qs_bnkfile_mr_deployment";
    var QS_EMAIL_NOTIF_DEPLOYMENT_FIELD = "custrecord_2663_qs_notify_mr_deployment";
    var QS_SS1_PARENT_DEPLOYMENT_FIELD = "custrecord_2663_qs_parent_deployment";
    
    var DEFAULT_TRANSACTION_MARKER_DEPLOYMENT = "customdeploy_8859_transaction_marker";
    var DEFAULT_PAYMENT_PROCESSOR_DEPLOYMENT = "customdeploy_8858_payment_processor_mr";
    var DEFAULT_FILE_CREATION_DEPLOYMENT = "customdeploy_9997_file_generation_ss";
    var DEFAULT_ROLLBACK_DEPLOYMENT = "customdeploy_8859_ep_rollback_mr";
    var DEFAULT_SCHEDULER_DEPLOYMENT = "customdeploy_8858_scheduler_ss";
    var DEFAULT_REPROCESS_DEPLOYMENT = "customdeploy_8859_ep_rollback_mr";
    var DEFAULT_REVERSAL_DEPLOYMENT = "customdeploy_8858_reverse_payments_mr";
    var DEFAULT_REMOVE_UNPROCESSED_DEPLOYMENT = "customdeploy_9997_trans_unlinker_mr";
    var DEFAULT_BANK_FILE_GENERATION_DEPLOYMENT = "customdeploy_9997_payment_to_admin_mr";
    var DEFAULT_NOTIFICATION_DEPLOYMENT = "customdeploy_8859_email_notification_mr";
    
    var DEFAULT_SS1_PARENT_DEPLOYMENT = "customdeploy_2663_payment_processing_ss";
    
    // EP Processes
    var EP_PROCESSPAYMENTS = '1';
    var EP_REPROCESS = '2';
    var EP_ROLLBACK = '3';
    var EP_REVERSEPAYMENTS = '4';
    var EP_EMAILNOTIFICATION = '5';
    var EP_CREATEFILE = '6';
    var EP_CREATEBATCH = '7';
    var EP_CLOSEBATCH = '8';
    var EP_REMOVEUNPROCESSTRANS = '9';
    var EP_MARKTRANSACTIONS = '10';
    var EP_GENERATEBANKFILE = '11';
    
    var DEPLOYMENT_RETRIEVAL_INPUT = {};
    
    DEPLOYMENT_RETRIEVAL_INPUT[EP_MARKTRANSACTIONS] = {
            deploymentField: QS_TRANSACTION_MARKER_DEPLOYMENT_FIELD,
            defaultDeployment: DEFAULT_TRANSACTION_MARKER_DEPLOYMENT
    };
    
    DEPLOYMENT_RETRIEVAL_INPUT[EP_PROCESSPAYMENTS] = {
             deploymentField: QS_PAYMENT_PROCESS_DEPLOYMENT_FIELD,
             defaultDeployment: DEFAULT_PAYMENT_PROCESSOR_DEPLOYMENT
    };

    DEPLOYMENT_RETRIEVAL_INPUT[EP_CREATEFILE] = {
             deploymentField: QS_FILE_CREATOR_DEPLOYMENT_FIELD,
             defaultDeployment: DEFAULT_FILE_CREATION_DEPLOYMENT
    };

    DEPLOYMENT_RETRIEVAL_INPUT[EP_ROLLBACK] = {
            deploymentField: QS_ROLLBACK_DEPLOYMENT_FIELD,
            defaultDeployment: DEFAULT_ROLLBACK_DEPLOYMENT
    };

    DEPLOYMENT_RETRIEVAL_INPUT[EP_REPROCESS] = {
            deploymentField: QS_ROLLBACK_DEPLOYMENT_FIELD,
            defaultDeployment: DEFAULT_ROLLBACK_DEPLOYMENT
    };
    
    DEPLOYMENT_RETRIEVAL_INPUT[EP_REVERSEPAYMENTS] = {
            deploymentField: QS_REVERSAL_DEPLOYMENT_FIELD,
            defaultDeployment: DEFAULT_REVERSAL_DEPLOYMENT
    };

    DEPLOYMENT_RETRIEVAL_INPUT[EP_REMOVEUNPROCESSTRANS] = {
            deploymentField: QS_REMOVED_UNPROCESSED_DEPLOYMENT_FIELD,
            defaultDeployment: DEFAULT_REMOVE_UNPROCESSED_DEPLOYMENT
    };
    
    DEPLOYMENT_RETRIEVAL_INPUT[EP_GENERATEBANKFILE] = {
            deploymentField: QS_BANK_FILE_GENERATION_DEPLOYMENT_FIELD,
            defaultDeployment: DEFAULT_BANK_FILE_GENERATION_DEPLOYMENT
    };
    
    DEPLOYMENT_RETRIEVAL_INPUT[EP_EMAILNOTIFICATION] = {
            deploymentField: QS_EMAIL_NOTIF_DEPLOYMENT_FIELD,
            defaultDeployment: DEFAULT_NOTIFICATION_DEPLOYMENT
    };
    
    function getSS1ParentDeployment(subsidiary){
        var processDeployment;
        
        var input = {};
        input.subsidiary = subsidiary;
        input.deploymentField = QS_SS1_PARENT_DEPLOYMENT_FIELD;
        input.defaultDeployment = DEFAULT_SS1_PARENT_DEPLOYMENT;

        processDeployment = getDeployment(input);
        
        return processDeployment;
    }
    
    function getProcessDeployment(processId, params){
        log.debug('getProcessDeployment:params',JSON.stringify(params));
        var input = DEPLOYMENT_RETRIEVAL_INPUT[processId];
        input.subsidiary = params.subsidiary;
        input.schedulerDeployment = params.schedulerDeployment;
        input.currentDeployment = params.currentDeployment;
        
        var processDeployment = getDeployment(input);
        
        return processDeployment;
    }
    
    function getDefaultDeployment(processId){
        var input = DEPLOYMENT_RETRIEVAL_INPUT[processId];
        var defaultDeployment = input.defaultDeployment;
        
        return defaultDeployment;
    }

    function getDeployment(input){
        /* 
         * If there is no subsidiary (SI), the default deployment shall be returned.
         * If there is no deployment assigned to the subsidiary, the default deployment shall be returned.
         * If there are at least one deployment assigned to the subsidiary, the available deployment shall be returned; 
         * else, no deployment shall be. 
         * 
         * On bundle update, the deployment fields will be blank. In this case, default deployment shall be used.
         */
        
        var selectedDeployment;
        var subsidiary = input.subsidiary;
        var deploymentField = input.deploymentField;
        var defaultDeployment = input.defaultDeployment;
        log.debug('input', JSON.stringify(input));
        var queueSettings = getQueueSettings(subsidiary, input.schedulerDeployment);
        
        
        if (queueSettings.length > 0) {
            var deployments = [];
            
            for(var i = 0; i < queueSettings.length; i++){
                log.debug("queueSetting", "internal id: "+queueSettings[i].id+" | deployment: "+queueSettings[i].getValue(deploymentField));
                deployments.push(queueSettings[i].getValue(deploymentField));
            }
            
            if (deployments.length > 0) {
                log.debug("Potential Deployments", deployments);
                selectedDeployment = selectAvailableDeployment(deployments, input);
            }
        } else {
            selectedDeployment = defaultDeployment;
        }
            
        log.debug("selectedDeployment", selectedDeployment);
        
        return selectedDeployment;
    }
    
    function selectAvailableDeployment(deployments, input){
        /*
         * Select the first deployment to be found available. 
         */
        var status;
        var selectedDeployment;
        
    	if (!deployments[0]) {
    		/* 
        	 * If deployment is blank, set value with the default deployment
        	 * This is scenario is possible after initial bundle update with additional deployment fields
        	 */
    		log.debug("Deployment is empty");
    		selectedDeployment = input.defaultDeployment;
    		
    	} else {
    		var unvailableDeployments = getUnavailableDeployments(deployments);
            log.debug("Unavailable Deployments", unvailableDeployments);
            
            for(var i = 0; i < deployments.length; i++){
                /* check if found in array unavailable deployments */
                var foundAvailable = unvailableDeployments.indexOf(deployments[i].toLowerCase()) == -1;
                
                if (foundAvailable) {
                    selectedDeployment = deployments[i];
                    break;
                } else if (input.currentDeployment === deployments[i]){
                    selectedDeployment = deployments[i];
                    break;
                }
            }
    	}
        
        return selectedDeployment;
    }
    
    function getQueueSettings(subsidiary, schedulerDeployment){
        return queueSettingDao.getQueueSettings(subsidiary, schedulerDeployment);
    }
    
    function getUnavailableDeployments(deploymentIDs){
        return ssInstanceDao.getUnavailableDeployments(deploymentIDs);
    }
    
    return {
        getDefaultDeployment: getDefaultDeployment,
        getProcessDeployment: getProcessDeployment,
        getSS1ParentDeployment: getSS1ParentDeployment
    };
});