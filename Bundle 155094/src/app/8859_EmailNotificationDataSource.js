/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Dec 2016     aalcabasa
 *
 * @NModuleScope1 TargetAccount
 * @NModuleScope Public
 */
define(['../data/9997_TransactionDAO', '../lib/wrapper/9997_NsWrapperRuntime'],

function(transactionDAO, runtime) {
    

    function getNotifications(pfa) {
        var author = pfa.notificationSender;
        var notificationMails = pfa.notificationMails ? JSON.parse(pfa.notificationMails) : {};
        var receivers = pfa.emailReceivers ? JSON.parse(pfa.emailReceivers) : {};
        var ccReceivers = pfa.ccReceivers ? JSON.parse(pfa.ccReceivers) : {};
        var bccReceivers = pfa.bccReceivers ? JSON.parse(pfa.bccReceivers) : {};
        var emailSubject = pfa.notificationSubject;
        var isFromFileGen = ((pfa.fileGenData || '') > 0);
        
        var paymentsForNotification = pfa.paymentsForNotify ? pfa.paymentsForNotify.split('|') : [];
        
        log.debug('pfa', JSON.stringify(pfa));
        var transactionDetails = transactionDAO.retrievePaymentsForNotification(paymentsForNotification, isFromFileGen);
        
        log.debug('paymentsForNotification', JSON.stringify(paymentsForNotification) + transactionDetails.length);
        
        var processingObjs = [];
        if (transactionDetails.length === 0) {
            return processingObjs;
        }
        
        for (var i = 0; i < transactionDetails.length; i++) {
            
            var payment = transactionDetails[i];
            var paymentId = payment.id;
            var tranId = payment.getValue('tranid');
            var entityId = payment.getValue('entity');
            
            var processingObj = {
                id: paymentId,
                recordType: payment.recordType,
                entity: tranId,
                pfaId: pfa.id,
                subsidiaryId: runtime.isOW() ? pfa.paymentSubsidiary : null,
                entityId: entityId
            };
            
            var curEmailSubject = emailSubject + " " + tranId;
            
            var emailObj = {
                author: author,
                subject: curEmailSubject,
                body: ''
            };
            if (notificationMails[paymentId]) {
                emailObj.body = notificationMails[paymentId];
            } else if (notificationMails['template']) {
                processingObj.template = notificationMails['template']
                processingObj.forRendering = true;
            } else {
                //SS2 no longer allows an empty body parameter
                emailObj.body = (notificationMails['all'] || '').trim() || ' ';
            }
            
            emailObj.recipients = receivers[paymentId] ? receivers[paymentId].split(';') : [];
            emailObj.cc = ccReceivers[paymentId] ? ccReceivers[paymentId].split(';') : [];
            emailObj.bcc = bccReceivers[paymentId] ? bccReceivers[paymentId].split(';') : [];
            emailObj.relatedRecords = {
                entityId: entityId,
                transactionId: paymentId
            };
            
            processingObj.email = emailObj;
            
            processingObjs.push(processingObj);
        }
        
        return processingObjs;
    }
    
    return {
        getNotifications: getNotifications
    };
    
});
