/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount
 */

define(['../data/9997_TransactionDAO',
        '../data/9997_EntityDAO',
        '../data/9997_EntityBankDAO',
        './9997_EPDataSource'],

function (transactionDAO, entityDAO, entityBankDAO, epDataSource) {
    function create(settings) {
        var baseDataSource = epDataSource.create(settings);

        baseDataSource.retrievePayments = function (parameters) {
            return transactionDAO.retrieveCustomerPayments(parameters.pfaId, parameters.searchColumns);
        };

        baseDataSource.retrieveEntities = function (parameters) {
            return entityDAO.retrieveDDEntities(parameters.entityIds, parameters.searchColumns);
        };

        baseDataSource.retrieveEntityBankDetails = function (parameters) {
            return entityBankDAO.retrieveCustomerBanks(parameters.templateId, parameters.entityIds, parameters.searchColumns);
        };

        baseDataSource.retrieveTransactions = function (parameters) {
            return transactionDAO.retrieveDDTransactions(parameters.paymentIds, parameters.searchColumns);
        };

        return baseDataSource;
    }

    return {
        create: create,
    };
});
