/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

define(['../lib/wrapper/9997_NsWrapperRecord',
        '../lib/wrapper/9997_NsWrapperFormat',
        '../lib/wrapper/9997_NsWrapperTask',
        '../lib/wrapper/9997_NsWrapperRuntime',
        '../lib/wrapper/9997_NsWrapperRedirect',
        '../lib/wrapper/9997_NsWrapperConfig',
        '../app/8858_DeploymentManager',
        '../data/8858_QueueSettingDAO'],

function (record, format, task, runtime, redirect, config, deploymentManager, queueSettingDao) {
    
    // TODO: link to constants.js
    var PAYQUEUED = 1;
    var PAYMARKPAYMENTS = 5;
    var EP_GENERATE_BANK_FILE = '11';
    var EP_EFT = '1';
    var DEFAULT_SCHEDULER_DEPLOYMENT = "customdeploy_8858_scheduler_ss";
    
    var TEMPLATE_TYPE_MAP = {
        'eft': 1,
        'dd': 2
    };

    function storeSavedSearch(params){
        var objConfig = config.load({ type: config.Type.USER_PREFERENCES });
        var savedSearchField = params.templatetype === 'eft' ? 'custscript_9997_file_gen_saved_search' : 'custscript_9997_dd_file_gen_saved_search';

        objConfig.setValue({
            fieldId: savedSearchField,
            value: params.transavedsearch
        });

        objConfig.save();
    };
    
    function createAdminRecord(params){
        log.debug('request params', JSON.stringify(params));
        
        var bankAccount = JSON.parse(params.bankaccount);
        
        var formData = {
            transavedsearch: params.transavedsearch,
            bankaccount: bankAccount.id,
            bankAccountInfo: bankAccount,
            datetobeprocessed: params.datetobeprocessed,
            referencenote: params.referencenote,
            totalamount: format.parse({
                value: params.totalamount,
                type: format.Type.CURRENCY
            }),
            trancount: params.trancount,
            templatetype: TEMPLATE_TYPE_MAP[params.templatetype],
            templatetypetext: params.templatetype
        };

        var rec = record.create({ type: 'customrecord_2663_file_admin', isDynamic: true });
        rec.setValue({ fieldId: 'custrecord_2663_file_processed', value: PAYQUEUED });
        rec.setValue({ fieldId: 'custrecord_2663_last_process', value: EP_GENERATE_BANK_FILE });
        rec.setValue({ fieldId: 'custrecord_2663_bank_account', value: formData.bankaccount });
        rec.setValue({ fieldId: 'custrecord_ep_priority', value: 3 });
        rec.setValue({ fieldId: 'custrecord_2663_process_date', value: format.parse({ value: formData.datetobeprocessed, type: format.Type.DATE }) });
        rec.setValue({ fieldId: 'custrecord_2663_file_creation_timestamp', value: new Date() });
        rec.setValue({ fieldId: 'custrecord_9997_file_gen_suitelet_data', value: JSON.stringify(formData) });
        rec.setValue({ fieldId: 'custrecord_2663_payment_type', value: formData.templatetype });
        rec.setValue({ fieldId: 'custrecord_2663_ref_note', value: formData.referencenote });
        rec.setValue({ fieldId: 'custrecord_8858_is_using_ss_2', value: true });
        rec.setValue({ fieldId: 'custrecord_8858_process_user', value: runtime.getCurrentUser().id });
        
        //save the ss2 scheduler deployment the queue setting with the available instant bank file generation deployment 
        rec.setValue({ fieldId: 'custrecord_2663_parent_deployment', value: getSS2SchedulerDeployment(bankAccount.subsidiary.id)}); 
        
        var recId = rec.save({ ignoreMandatoryFields: true });

        return recId;
    }
    
    function getSS2SchedulerDeployment(subsidiaryId){
    	log.debug('subsidiaryId', subsidiaryId);
        //get available instant bank file deployment
    	var params = {subsidiary: subsidiaryId};
    	var instantBankFileDeployment = deploymentManager.getProcessDeployment(EP_GENERATE_BANK_FILE, params);
        var qs = queueSettingDao.getByInstantBankFileDeployment(instantBankFileDeployment);
        
        var ss2Scheduler;
        if(qs){
            ss2Scheduler = qs.schedulerDeployment || DEFAULT_SCHEDULER_DEPLOYMENT; // default would have been used if there is no deployment value in queue setting
        }
        log.debug('pfa parent deployment', ss2Scheduler);
        
        return ss2Scheduler;
    }
        
    function redirectToAdminRecord(recId){
        redirect.toRecord({ id: recId, type: 'customrecord_2663_file_admin' });    
    }

    return {
        storeSavedSearch: storeSavedSearch,
        createAdminRecord: createAdminRecord,
        redirectToAdminRecord: redirectToAdminRecord
    };
});
