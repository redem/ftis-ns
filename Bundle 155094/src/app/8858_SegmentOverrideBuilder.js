/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Sep 2016     aalcabasa
 *
 * @NModuleScope Public
 */
define(['../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperRuntime'],

function(search, runtime) {
    var ID_FILTER_THRESHOLD = 500; // based on old computation, TODO verify if this is still the optimal threshold
    
    var NO_AGG = '@NONE@';
    var LOCATION = 'location';
    var DEPARTMENT = 'department';
    var CLASSIFICATION = 'class';
    
    var FIELD_MAP = {
        'department': 'department',
        'class': 'classification',
        'location': 'location',
    };
    
    function getSegmentOverrides(params) {
        var idList = params.tranIdList;
        var aggMethod = params.aggMethod;
        var segmentMap = {};
        
        log.debug('aggMethod', aggMethod);
        log.debug('idList', idList);
        

        var columns = buildColumns(aggMethod);
        
        if (columns.length === 0 || idList.length === 0) {
            return segmentMap;
        }
        
        var filters = buildFilters(idList);
        var segmentMap = getTransactionGroupSummary({
            columns: columns,
            filters: filters
        });
        
        for ( var name in segmentMap) {
            if (segmentMap[name].length === 1) {
                segmentMap[name] = segmentMap[name][0];
            } else {
                segmentMap[name] = '';
            }
        }
        return segmentMap;
    }
    
    // create a list of summary columns for each applicable segment
    // Custom Segments are still unsupported
    function buildColumns(aggMethod) {
        var columns = [];
        
        var summarizeDepts = (aggMethod === NO_AGG || aggMethod === DEPARTMENT) && runtime.isDepartmentEnabled()
                             && !runtime.isDeptPerLine();
        var summarizeClasses = (aggMethod === NO_AGG || aggMethod === CLASSIFICATION) && runtime.isClassEnabled()
                               && !runtime.isClassPerLine();
        var summarizeLocs = (aggMethod === NO_AGG || aggMethod === LOCATION) && runtime.isLocationEnabled()
                            && !runtime.isLocPerLine();
        
        // Return map as soon as we find not segments can be summarized
        if (!(summarizeDepts || summarizeClasses || summarizeLocs)) {
            return columns;
        }
        
        if (summarizeDepts) {
            columns.push(search.createColumn({
                name: 'department',
                summary: 'group',
                sort: search.Sort.ASC
            }));
        }
        
        if (summarizeClasses) {
            columns.push(search.createColumn({
                name: 'class',
                summary: 'group',
                sort: search.Sort.ASC
            }));
        }
        
        if (summarizeLocs) {
            columns.push(search.createColumn({
                name: 'location',
                summary: 'group',
                sort: search.Sort.ASC
            }));
        }
        
        return columns;
    }
    
    // create a list of filters
    // uses current solution of splitting the list in groups of 500
    function buildFilters(idList) {
        var filters = [
            ['mainline', search.Operator.IS, 'T'],
            'AND',
            ['amountremainingisabovezero', search.Operator.IS, 'T']
            ];
        
        var idFilters = createIdFilters(idList);
        filters.push('AND');
        filters.push(idFilters);
        
        return filters;
    }
    
    // create a list of filters given a list of transactions IDs
    // Since a search filter can only support a limited number of values,
    // we have to split the the idList into groups of 500
    function createIdFilters(idList) {
        var idFilters = [];
        while (idList.length > 0) {
            var list = idList.splice(0, ID_FILTER_THRESHOLD);
            
            if (idFilters.length > 0) {
                idFilters.push('OR');
            }
            
            idFilters.push(['internalid', search.Operator.ANYOF, list]);
        }
        return idFilters;
    }
    
    // retrieve an object that contains segmentValues for each field
    // this function would be moved to TransactionDAO if to be used for other classes
    function getTransactionGroupSummary(params) {
        var segmentMap = {};
        var columns = params.columns;
        
        var paymentSearch = search.create({
            type: search.Type.TRANSACTION,
            columns: columns,
            filters: params.filters
        });
        var rs = paymentSearch.getIterator();
        
        while (rs.hasNext()) {
            var curr = rs.next();
            
            for (var i = 0; i < columns.length; i++) {
                var column = columns[i];
                var propertyName = FIELD_MAP[column.name]
                var summaryValues = segmentMap[propertyName] || [];
                var value = curr.getValue(column);
                if (summaryValues.indexOf(value) === -1) {
                    summaryValues.push(value);
                }
                segmentMap[propertyName] = summaryValues;
            }
        }
        
        return segmentMap;
    }
    
    function allowSegmentOverrides(aggMethod) {
        return [NO_AGG, LOCATION, DEPARTMENT, CLASSIFICATION].indexOf(aggMethod) > -1;
    }
    
    return {
        getSegmentOverrides: getSegmentOverrides,
        allowSegmentOverrides: allowSegmentOverrides
    };
});
