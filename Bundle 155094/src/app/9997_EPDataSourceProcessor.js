/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define(['../lib/wrapper/9997_NsWrapperRuntime', '../lib/8858_Translator', '../lib/wrapper/9997_NsWrapperError'],

function(runtime, translator, error) {
    var PFA = 'pfa';
    var CBANK = 'cbank';
    var SEQUENCES = 'sequences';
    var CURRENCIES = 'currencies';
    var PAYMENTS = 'payments';
    var ENTITIES = 'entities';
    var ENTITY_BANK_DETAILS = 'ebanks';
    var TRANSACTIONS = 'transactions';
    
    var EP_FILE_TEMPLATE_INACTIVE = 'EP_FILE_TEMPLATE_INACTIVE';
    
    function processDataSource(parameters) {
        var dataSource = parameters.dataSource;
        var templateRenderer = parameters.templateRenderer;
        log.debug('dataSource.isTemplateInactive()', dataSource.isTemplateInactive());
        if (dataSource.isTemplateInactive()){
            throw error.create({
                name: EP_FILE_TEMPLATE_INACTIVE, 
                message: translator.getString(EP_FILE_TEMPLATE_INACTIVE)
            });
        }
        
        templateRenderer.setTemplateContents(dataSource.getTemplateContent());
        templateRenderer.addRecord(PFA, dataSource.getPfa());
        templateRenderer.addRecord(CBANK, dataSource.getBankDetails());
        
        if (dataSource.requiresSequences()) {
            templateRenderer.addSearchResults(SEQUENCES, dataSource.getSequences());
        }
        
        var currencies = dataSource.getCurrencies();
        if (runtime.isMultiCurrency()) {
            templateRenderer.addSearchResults(CURRENCIES, currencies);
        } else {
            templateRenderer.addRecord(CURRENCIES, currencies);
        }
        
        templateRenderer.addSearchResults(PAYMENTS, dataSource.getPayments());
        templateRenderer.addSearchResults(ENTITIES, dataSource.getEntities());
        templateRenderer.addSearchResults(ENTITY_BANK_DETAILS, dataSource.getEntityBankDetails());
        templateRenderer.addSearchResults(TRANSACTIONS, dataSource.getTransactions());
        
        return templateRenderer;
    }
    
    return {
        processDataSource: processDataSource
    };
});
