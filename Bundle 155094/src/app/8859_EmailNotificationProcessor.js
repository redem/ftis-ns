/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Dec 2016     aalcabasa
 *
 * @NModuleScope Public
 */
define(['./8859_EmailNotificationRenderer', '../lib/wrapper/9997_NsWrapperEmail', '../lib/wrapper/9997_NsWrapperRender'],

function(notificationRenderer, email, render) {
    
    function process(notification) {
        var emailParams = notification.email;
        
        if (notification.forRendering) {
            emailParams = notificationRenderer.render(notification);
        } else {
            var file = render.transaction({
                entityId: Number(notification.id), // Yes, this is correct based on help
                printMode: render.PrintMode.PDF
            });
            
            if (file) {
                emailParams.attachments = [file];
            }
            log.debug('file email',file);
        }
        log.debug('Sending email', JSON.stringify(notification));
        email.send(emailParams);
    }
    
    return {
        process: process
    };
    
});
