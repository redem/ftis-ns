/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Dec 2016     aalcabasa
 *
 * @NModuleScope1 TargetAccount
 * @NModuleScope Public
 */
define(['../lib/wrapper/9997_NsWrapperTemplateRenderer', '../lib/wrapper/9997_NsWrapperRecord', '../lib/wrapper/9997_NsWrapperFile'],

function(renderer, record, file) {
    var subsidiaryRecord;
    var templateContent;
    
    function render(notification) {
        
        if(!templateContent){
            var templateId = notification.template;
            templateContent = file.load(templateId).getContents();
        }
        renderer.setTemplateContents(templateContent);
        
        var paymentRecord = record.load({
            id: notification.id,
            type: notification.recordType
        });
        
        renderer.addRecord('record', paymentRecord.getRecord());
        
        if (notification.recordType === record.Type.CUSTOMER_PAYMENT) {
            var entityRecord = record.load({
                type: record.Type.CUSTOMER,
                id: notification.entityId
            });
            renderer.addRecord('entity', entityRecord.getRecord());
        }
        
        if (notification.subsidiaryId) {
             if(!subsidiaryRecord){
                 subsidiaryRecord = record.load({
                     type: record.Type.SUBSIDIARY,
                     id: notification.subsidiaryId
                 });                 
             }
             renderer.addRecord('subsidiary', subsidiaryRecord.getRecord());
        }
        
        notification.email.body = renderer.renderAsString();
        return notification.email;
    }
    
    return {
        render: render
    };
    
});
