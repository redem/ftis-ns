/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author aaclan
 * @NApiVersion 2.0
 * @NModuleScope Public
 */

define(['../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperRuntime', 'N/error'],

function(search, runtime, error) {
    var ERROR_MSG_INVALID_SEARCH_TYPE_EFT = 'Verify that the saved search Transaction filter has been set to Bill Payment only. Click the Edit Search link if you want to review the filtering criteria.';
    var ERROR_MSG_INVALID_SEARCH_TYPE_DD = 'Verify that the saved search Transaction filter has been set to Customer Payment only. Click the Edit Search link if you want to review the filtering criteria.';    
    var ERROR_MSG_INVALID_SEARCH_FILTERS = 'There are missing or incorrect filters in this saved search. Click the Edit Search link if you want to review the filtering criteria. For more information, see the Defining Transaction Saved Searches for Electronic Payment topic in the Help Center.';
    var ERROR_MSG_NO_BANK_DETAILS = 'You must select the company bank account first before selecting the transaction saved search.';
    
    var EFT_TEMPLATE_TYPE = 'eft';
    
    var VALIDATOR_SETTINGS = {
        'eft': {
            searchType: search.Type.VENDOR_PAYMENT,
            searchFilterTypeValues: ['VendPymt'],
            markerColumn: 'custbody_9997_is_for_ep_eft',
            fileFormatColumn: 'custcol_2663_eft_file_format',
            invalidTypeMsg: ERROR_MSG_INVALID_SEARCH_TYPE_EFT
        },
        'dd': {
            searchType: search.Type.CUSTOMER_PAYMENT,
            searchFilterTypeValues: ['CustPymt'],
            markerColumn: 'custbody_9997_is_for_ep_dd',
            fileFormatColumn: 'custcol_9997_dd_file_format',
            invalidTypeMsg: ERROR_MSG_INVALID_SEARCH_TYPE_DD
        }
    };    
    
    var PAYMENT_SEARCH_DEFAULT_FILTERS = [{
        'name': 'mainline',
        'operator': 'is',
        'values': ['T'],
        'isor': false,
        'isnot': false,
        'leftparens': 0,
        'rightparens': 0
    }, {
        'name': 'custbody_9997_pfa_record',
        'operator': 'anyof',
        'values': ['@NONE@'],
        'isor': false,
        'isnot': false,
        'leftparens': 0,
        'rightparens': 0
    }, {
    	'name': 'internalid',
        'join': 'custrecord_2663_parent_payment',
        'operator': 'anyof',
        'values': ['@NONE@'],
        'isor': false,
        'isnot': false,
        'leftparens': 0,
        'rightparens': 0
    } ];
    
    var PAYMENT_SEARCH_DYNAMIC_FILTERS = [{
        'name': 'account',
        'operator': 'anyof',
        'values' : null,
        'isor': false,
        'isnot': false,
        'leftparens': 0,
        'rightparens': 0
    }];
    
    var FILTER_ATTR_TO_CHECK =  ['name', 'values', 'operator', 'isor', 'isnot', 'leftparens', 'rightparens'];
    
    var FILE_FORMAT_FILTER = {
        'name': '',
        'operator': 'anyof',
        'values' : null,
        'isor': false,
        'isnot': false,
        'leftparens': 0,
        'rightparens': 0,
    };
    
    function validatePaymentSearch(tranSearch, parameters){
        var validatorSettings = getValidatorSettings(parameters);
        
        if(tranSearch.searchType != validatorSettings.searchType){
            var errorParams = {
                name: 'EP_INVALID_SEARCH_TYPE',
                message: validatorSettings.invalidTypeMsg,
            };
            log.error(errorParams.name, errorParams.message );
            throw error.create(errorParams);
        }
        
        var filterValidation = validateFilters(tranSearch.filters, parameters);
        if((filterValidation.missingFilters && filterValidation.missingFilters.length > 0) || 
            (filterValidation.incorrectFilters && filterValidation.incorrectFilters.length > 0 )) {
            var errorParams = {
                name: 'EP_INVALID_SEARCH_FILTERS',
                message: ERROR_MSG_INVALID_SEARCH_FILTERS
            };
            log.debug('Payments Search Filter Validation', JSON.stringify(filterValidation));
            log.error(errorParams.name, errorParams.message);
            throw error.create(errorParams);
        }
        
    }

    function validateFilters(actualFilters, parameters){
        var defaultFilters = buildDefaultFilters(parameters);
        var dynamicFilters = buildDynamicFilters(parameters);
        
        var requiredFilters = defaultFilters.concat(dynamicFilters);
        
        var actualFiltersByName = {};
        
        for (var i = 0 ; i < actualFilters.length ; i++){
            var actualFilter = actualFilters[i];
            // TODO: call toJSON to access the values (This is not documented. See Enhancement Issue: 395183 to have ability to access filter values)
            actualFilter = actualFilter.toJSON(); 
            var key = getFilterKey(actualFilter);
            actualFiltersByName[key] = actualFilter;
        }
        
        var missing = [];
        var incorrect = [];
        
        for (var i = 0 ; i < requiredFilters.length ; i++){
            var requiredFilter = requiredFilters[i];
            var key = getFilterKey(requiredFilter);
            
            if(!actualFiltersByName[key]){
                missing.push(key);
            } else if (!isFilterValid(requiredFilter, actualFiltersByName[key])){
                incorrect.push(key);
            }
        }
        return { missingFilters: missing, incorrectFilters: incorrect };
    }
    
    function getFilterKey(filterJSON){
    	var name = filterJSON.name;
        var join = filterJSON.join ? filterJSON.join + '.' : '';
        return join + name;
    }
    
    function buildDefaultFilters(parameters){
        var validatorSettings = getValidatorSettings(parameters);
        return [
            {
                'name': 'type',
                'operator': 'anyof',
                'values': validatorSettings.searchFilterTypeValues,
                'isor': false,
                'isnot': false,
                'leftparens': 0,
                'rightparens': 0
            }, {
                'name': validatorSettings.markerColumn,
                'operator': 'is',
                'values': ['T'],
                'isor': false,
                'isnot': false,
                'leftparens': 0,
                'rightparens': 0
            }, 
                ].concat(PAYMENT_SEARCH_DEFAULT_FILTERS);
    }
    
    function buildDynamicFilters(parameters){
        var bankAccountInfo;
        
        if (!(parameters && parameters.bankAccountInfo && parameters.bankAccountInfo.id)){
            var errorParams = {
                name: 'EP_NO_BANK_DETAILS',
                message: ERROR_MSG_NO_BANK_DETAILS,
            };
            log.error(errorParams.name, errorParams.message);
            throw error.create(errorParams);
        } else {
            bankAccountInfo = parameters.bankAccountInfo;
        }
        
        var dynamicFilters = PAYMENT_SEARCH_DYNAMIC_FILTERS;
        
        var requiredFilters = [];
        for(var i = 0 ; i < dynamicFilters.length ; i ++ ){
            var filter = dynamicFilters[i] ;
            
            switch(filter.name) {
                case 'account':
                    filter.values = [bankAccountInfo.glBankAccount];
                    requiredFilters.push(filter);
                    break;
                default:
            }
        }
        
        var validatorSettings = getValidatorSettings(parameters);
        var formatFilter = FILE_FORMAT_FILTER;
        formatFilter.name = validatorSettings.fileFormatColumn;
        formatFilter.values = [bankAccountInfo.template.id];
        requiredFilters.push(formatFilter);
        
        return requiredFilters;
    }
    
    function isFilterValid(filterA, filterB){
        var attrToCheck = FILTER_ATTR_TO_CHECK;
        var isEqual = true;
        
        for (var i = 0; i < attrToCheck.length ; i++){
            var attr = attrToCheck[i];
            var filterValueA = filterA[attr];
            var filterValueB = filterB[attr];
            
            if((filterValueA instanceof Array) && (filterValueB instanceof Array)){
                isEqual = isSubSet(filterValueA,filterValueB);
            }
            else if(filterValueA != filterValueB){
                isEqual = false;
            }
            if(!isEqual){
                break;
            }
        }
        return isEqual;
    }
    
    function isSubSet(superSet, subSet){
        if(superSet.length < subSet.length){
            return false;
        }
        
        for(var i=0; i<subSet.length; i++){
            if(superSet.indexOf(subSet[i]) < 0){
                return false;
            }
        }
        return true;
    }
    
    // TODO Add validation for DD and remove this wrapper function
    function validateEFTSearch(tranSearch, parameters) {
        if (parameters.templateType === EFT_TEMPLATE_TYPE) {
            validatePaymentSearch(tranSearch, parameters);
        }
    }
    
    function getValidatorSettings(parameters){
        return VALIDATOR_SETTINGS[parameters.templateType];
    }
    
    return {
        validatePaymentSearch : validatePaymentSearch,
    };
});