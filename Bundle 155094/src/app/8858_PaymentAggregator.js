/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * @NModuleScope TargetAccount
 * @NApiVersion 2.0
 */
define([
    '../lib/wrapper/9997_NsWrapperError',
    ],

function(error) {
    var NONE = '@NONE@';
    
    function getAggregationColumns(aggregationMethod){
        var aggregateColumns = ['entity', 'currency'];
        
        if(aggregationMethod == NONE){
            aggregateColumns.push('bepi'); // use bepi which is a unique key if no aggregation
        } else if(aggregationMethod){
            aggregateColumns.push(aggregationMethod);
        } 
        
        return aggregateColumns;
    }
    
    function aggregateTransactions(aggregationMethod, transactions){
        var aggregateColumns = getAggregationColumns(aggregationMethod);
        
        var aggregationMap = {};
        var discountMap = {};
        var amountMap = {};
        
        for(var i=0; i<transactions.length; i++){
            var transaction = transactions[i];
            
            var groupingKey = getAggregationKey(aggregateColumns, transaction);
            var tranObj = {id: transaction.id, line: transaction.line};
            
            if(!aggregationMap[groupingKey]){
                aggregationMap[groupingKey] = {};
                aggregationMap[groupingKey]['trans'] = [];
                aggregationMap[groupingKey]['trans'].push(tranObj);
                aggregationMap[groupingKey]['entity'] = transaction.entity;
                aggregationMap[groupingKey]['curr'] = transaction.currency;
            }else{
                aggregationMap[groupingKey]['trans'].push(tranObj);
            }
            
            var mapId = [transaction.id, transaction.line].join('-');
            discountMap[mapId] = transaction.discount;
            amountMap[mapId] = transaction.amount;
        }
        log.debug('aggregationMap',JSON.stringify(aggregationMap));
        
        var aggregationList = [];
        
        for (var groupingKey in aggregationMap){
            var aggregation = aggregationMap[groupingKey];
            aggregationList.push(aggregation);
        }
        
        var obj = {
            discountMap : discountMap,
            amountMap: amountMap,
            aggregationList : aggregationList
        };
        log.debug('aggregation', JSON.stringify(obj));
        return obj;
    }
    
    // stringified column values list
    function getAggregationKey(aggregationColumns, transaction){
        var colValues=[];
        
        for (var i=0; i<aggregationColumns.length; i++){
            var col = aggregationColumns[i];
            colValues.push(transaction[col]);
        }
        
        return JSON.stringify(colValues);
    }
    
     
    return {
        aggregateTransactions : aggregateTransactions
    };
});
