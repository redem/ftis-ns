/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jun 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([],

function() {
    
    // TODO this function should also retrieve formatting settings
    function getTemplateSettings(templateContent) {
        
        var templateExpressions = extractExpressions(templateContent);
        var sequenceSettings = getSequenceSettings(templateExpressions);
        
        return {
            sequenceSettings: sequenceSettings
        };
    }
    
    // TODO: Understand the logic and think of how to improve how this is done
    var templateExpressions;
    function extractExpressions(templateContent) {
        
        // those inside ${ }
        templateExpressions = [];
        var regEx = /\${([^}]+)}/g;
        var currentMatch;
        while (currentMatch = regEx.exec(templateContent)) {
            templateExpressions.push(currentMatch[1]);
        }
        
        // those inside <# >
        regEx = /<#([^}]+)>/g;
        while (currentMatch = regEx.exec(templateContent)) {
            templateExpressions.push(currentMatch[1]);
        }
        
        return templateExpressions;
    }
    

    // TODO Ask PM if there were cases where getSequenceId(true) and getSequenceId(false) were in the same template
    function getSequenceSettings(templateExpressions) {
        var sequenceSettings = {
            requiresSequences: false,
            sameDaySequenceOnly: false
        };
        
        for (var i = 0; i < templateExpressions.length; i++) {
            var expression = templateExpressions[i];
            if (expression.indexOf("getSequenceId(true)") > -1) {
                sequenceSettings.requiresSequences = true;
                sequenceSettings.sameDaySequenceOnly = true;
                break;
            } else if (expression.indexOf("getSequenceId(false)") > -1) {
                sequenceSettings.requiresSequences = true;
                break;
            }
        }
        return sequenceSettings;
    }
    
    return {
        getTemplateSettings: getTemplateSettings
    }
});
