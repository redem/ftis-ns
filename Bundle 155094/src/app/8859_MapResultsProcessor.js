/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Nov 2016     aalcabasa
 *
 * @NModuleScope Public
 */
define([],

function() {
    
    function compileResults(processingResults) {
        
        var results = {
            successCount: 0,
            failureCount: 0,
            totalCount: 0,
            errors: []
        };
        
        processingResults.map(function(res) {
            var result = JSON.parse(res);
            if (result.isSuccess) {
                results.successCount++;
            } else {
                results.failureCount++;
                results.errors.push(result.message);
            }
        });
        
        results.totalCount = results.successCount + results.failureCount;
        
        return results;
    }
    
    return {
        compileResults: compileResults
    };
});
