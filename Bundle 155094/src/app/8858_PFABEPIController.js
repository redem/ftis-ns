/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Oct 2016     aalcabasa
 *
 * @NModuleScope Public
 */

define(['../lib/wrapper/9997_NsWrapperRuntime', '../data/8859_BEPIDAO'],

function(runtime, bepiDAO) {
    var USAGE_THRESHOLD = 500;
    
    var FAILED = 9;
    var PROCESSED = 4;
    
    function updateBEPIListStatus(bepiIds, status) {
        bepiIds = bepiIds || [];
        while (runtime.getCurrentScript().getRemainingUsage() > USAGE_THRESHOLD && bepiIds.length > 0) {
            var currId = bepiIds.splice(0, 1)[0];
            bepiDAO.updateFields(currId, {
                'custrecord_2663_eft_payment_processed': status
            });
        }
        
        return bepiIds;
    }
    
    function setToFailed(bepiIds) {
        return updateBEPIListStatus(bepiIds, FAILED);
    }
    
    function setToProcessed(bepiIds) {
        return updateBEPIListStatus(bepiIds, PROCESSED);
    }
    
    return {
        setToFailed: setToFailed,
        setToProcessed: setToProcessed
    };
    
});
