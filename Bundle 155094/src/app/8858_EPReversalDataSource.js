/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount
 */

define(['../lib/wrapper/9997_NsWrapperSearch',
        '../lib/wrapper/9997_NsWrapperRuntime',
        '../lib/wrapper/9997_NsWrapperFormat',
        '../lib/wrapper/9997_NsWrapperRecord',
        '../lib/wrapper/9997_NsWrapperError',
        '../data/9997_PFADAO',
        '../lib/8858_Translator',
        '../lib/8858_TranslationServiceCaller',
        '../lib/8858_StringFormatter',
        '../app/9997_SearchListIterator'
        ],

function (search, runtime, formatter, record, error, pfaDAO, translator, translationService, stringFormatter, searchListIterator) {

    var CUSTPAYMENTS_DISC_UNSUPPORTED = 'CUSTPAYMENTS_DISC_UNSUPPORTED';
    var MSG_VOID_VENDOR_PAYMENT = 'reversal.voidVendorPayment';
    var MSG_VOID_CUST_PAYMENT = 'reversal.voidCustomerPayment';
    
    var MESSAGEMAP;
    initializeMessageTranslationMap();

    var TRANTYPE = {
        '1': 'vendorpayment',
        '2': 'customerpayment'
    };
    
    var REFERENCE_LINE_TYPE = {
        'vendorpayment': 'debit',
        'customerpayment': 'credit'
    };
    
    var REVERSING_LINE_TYPE = {
        'vendorpayment': 'credit',
        'customerpayment': 'debit'
    };
    
    var MEMO_PREFIX = {
        'vendorpayment': MESSAGEMAP[MSG_VOID_VENDOR_PAYMENT],
        'customerpayment': MESSAGEMAP[MSG_VOID_CUST_PAYMENT]
    };
    
    var DISCOUNT_ACCOUNT = {
        'vendorpayment' : 'Purchase Discounts',
        'customerpayment': 'Sales Discounts'
    };
    
    var REVERSAL_ACCOUNT_FIELD = {
        'vendorpayment': 'apacct',
        'customerpayment': 'aracct'
    };
    
    var ACCOUNT_TYPE = {
        'vendorpayment': 'AcctPay',
        'customerpayment': 'AcctRec'
    };
    
    var BANK_ACCOUNT_RECORD_TYPE = 'customrecord_2663_bank_details';
    
    var pfaRecord;


    function getPaymentsForReversal(pfaId) {
        var dataSource = {};
        var pfa = getPFA(pfaId);
        var paymentsForReversal = getPayments(pfa);
        log.debug('paymentsForReversal', JSON.stringify(paymentsForReversal));
        
        return paymentsForReversal;
    }
    
    function getPayments(pfa){
        var payments = [];
        
        var paymentRecords = searchPaymentRecordsForReversal(pfa);
        
        var reversalInfo = getReversalInfo(pfa); 
        var reversalReason = reversalInfo.reason;
        
        var paymentType = getPaymentType(pfa);
        var tranType = TRANTYPE[paymentType];
        
        var discountAccountId = getDiscountAccountId(tranType);
        var discountMap = getDiscountMap(tranType, discountAccountId, getPaymentIDsForReversal(pfa));
        
        var bankAccountId = pfa.getValue('custrecord_2663_bank_account');
        var bankAccountRecord = getBankAccountRecord(bankAccountId);
        var glBankAccount = bankAccountRecord.getValue('custrecord_2663_gl_bank_account');
        
        for (var i=0; i < paymentRecords.length ; i++){
            var payment = paymentRecords[i];
            
            var paymentObj = {};
            paymentObj.id = payment.id;
            paymentObj.tranId = payment.getValue('tranid');
            paymentObj.type = tranType;
            paymentObj.entity = payment.getValue('entity');
            paymentObj.reversalNumber = payment.getValue('reversalnumber');

            if(runtime.isOW()){
                paymentObj.subsidiary = payment.getValue('subsidiary');
            }
            
            var amountColumn = 'amount';
            
            if(runtime.isMultiCurrency()){
                paymentObj.currency = payment.getValue('currency');
                amountColumn = 'fxamount';
            }
            
            paymentObj.amount = payment.getValue(amountColumn);
            
            if(runtime.isClassEnabled()){
                paymentObj['class'] = payment.getValue('class');
            }

            if(runtime.isDepartmentEnabled()){
                paymentObj.department = payment.getValue('department');
            }

            if(runtime.isLocationEnabled()){
                paymentObj.location = payment.getValue('location');
            }
            
            paymentObj.discountAmount = discountMap[paymentObj.id];
            paymentObj.discountAccountId = discountAccountId;
            paymentObj.reversalDate = reversalInfo.date;
            paymentObj.reversalReason = reversalReason[paymentObj.id] || reversalReason['all'] || '';
            paymentObj.reversalPeriod = reversalInfo.reversalPeriod;
            
            var newTranId = createNewTranId(paymentObj.tranId);
            var memo = [ MEMO_PREFIX[tranType], newTranId].join('');

            paymentObj.newTranId = newTranId;
            paymentObj.memo = memo;
            paymentObj.glBankAccount = glBankAccount;
            paymentObj.pfaRecord = pfa.id;
            
            payments.push(paymentObj);
        }
        
        return payments;
    }
    
    function padLeft(minLength, character, str) {
        stringFormatter.setString(String(str));
        return stringFormatter.padLeft(minLength, character);
    }
    
    function createNewTranId(oldTranId){
        var now = new Date(); // server time is in PST
        log.debug('datetimestamp', String(now)); // log date and timezone
        
        var month = now.getMonth() + 1;
        var date = now.getDate();
        var year = now.getFullYear();
        var hour = now.getHours();
        var min = now.getMinutes();
        var sec = now.getSeconds();
        
        var dateMMDDYYYY = padLeft(2, '0', month) + padLeft(2, '0', date) + padLeft(4, '0', year);
        var dateHHMMSS = padLeft(2, '0', hour) + padLeft(2, '0', min) + padLeft(2, '0',  sec);
        var newTranId = [ oldTranId, dateMMDDYYYY, dateHHMMSS ].join('-');
        
        return newTranId;
    }
    
    function getPFA(pfaId){
        if (!pfaRecord) {
            pfaRecord = pfaDAO.retrieveNsRecord(pfaId);
        }
        return pfaRecord;
        
    }
    
    function getBankAccountRecord(bankAccountId){
        var wrapperRecord = record.load({
            type: BANK_ACCOUNT_RECORD_TYPE,
            id: bankAccountId
        });

        return wrapperRecord.getRecord();
    }
    
    function searchPaymentRecords(paymentType, paymentIds){
        var filters = [
            ['internalid', search.Operator.ANYOF, paymentIds],
            'and',
            ['mainline', search.Operator.IS, 'T'],
        ];
                 
        var columns = [];
        columns.push(createColumn('memo'));
        columns.push(createColumn('tranid'));
        columns.push(createColumn('amount'));
        columns.push(createColumn('entity'));
        columns.push(createColumn('reversalnumber'));
        
        if(runtime.isOW()){
            columns.push(createColumn('subsidiary'));
        }
        
        if(runtime.isMultiCurrency()){
            columns.push(createColumn('fxamount'));
        columns.push(createColumn('currency'));
        }
        
        if(runtime.isClassEnabled()){
            columns.push(createColumn('class'));
        }
        
        if(runtime.isDepartmentEnabled()){
            columns.push(createColumn('department'));
        }
        
        if(runtime.isLocationEnabled()){
            columns.push(createColumn('location'));
        }
        
        return searchRecord(TRANTYPE[paymentType], filters, columns);
    }
    
    function searchPaymentRecordsForReversal(pfa){
        var paymentIds = getPaymentIDsForReversal(pfa);
        var listIterator = searchListIterator.create(paymentIds);
        
        var paymentType = getPaymentType(pfa);
        
        var paymentRecords = [];
        
        while (listIterator.hasNext()) {
            var paymentIdsSubset = listIterator.next();
            var paymentRecordsSubset = searchPaymentRecords(paymentType, paymentIdsSubset);
            paymentRecords = paymentRecords.concat(paymentRecordsSubset);
        }
        
        log.debug('searchPaymentRecordsForReversal result', paymentRecords);
        return paymentRecords;
        
    }
    
    function getPaymentIDsForReversal(pfa){
        var paymentIdsStr = pfa.getValue('custrecord_2663_payments_for_reversal') || '';
        var paymentIds = paymentIdsStr.split('|');
        return paymentIds;
    }
    
    function getPaymentType(pfa){
        return pfa.getValue('custrecord_2663_payment_type');
    }
    
    function getReversalInfo(pfa){
       var reversalInfo = {};
       
       reversalInfo.reason = JSON.parse(pfa.getValue('custrecord_2663_reversal_reasons')) || {};
       
       var reversalDate = pfa.getValue('custrecord_2663_reversal_date');
       reversalInfo.date = formatter.format({value: reversalDate, type: formatter.Type.DATETIME});

       reversalInfo.reversalPeriod = pfa.getValue('custrecord_2663_reversal_period');
       
       return reversalInfo;
    }

    function getDiscountMap(tranType, discountAccountId, transactionIds){
        var discountMap = {};
        
        if(!discountAccountId){
            return discountMap;
        }
        
        var filters = [
           ['internalid', search.Operator.ANYOF, transactionIds],
           'and',
           ['account', search.Operator.IS, discountAccountId],
        ];
        
        var columns = [];
        var amountColumn = runtime.isMultiCurrency() ? 'fxamount' : 'amount';
        columns.push(createColumn(amountColumn));
        
        var discounts = searchRecord(tranType, filters, columns);

        for(var i=0; i<discounts.length; i++){
            var discount = discounts[i];
            var amount = discount.getValue(amountColumn); 
            discountMap[discount.id] = amount;
        }
        
        return discountMap;
    }
    
    function getDiscountAccountId(tranType){
        var filters = [
            ['name', search.Operator.IS, DISCOUNT_ACCOUNT[tranType]],
        ];
        var result = searchRecord('account', filters, []);
        var discountAccountId = result.length > 0 ? result[0].id : null;

        return discountAccountId;
    }
    
    
    function createColumn(columnName) {
        return search.createColumn({
            name: columnName
        });
    }
    
    function searchRecord(type, searchFilters, searchColumns) {
        var recordSearch = search.create({
            type: type,
            columns: searchColumns,
            filters: searchFilters
        });

        var data = [];
        var iterator = recordSearch.getIterator();
        while (iterator.hasNext()) {
            var result = iterator.next();
            data.push(result);
        }
        return data;
    }
    
    function getReversalDateFromPFA(pfaId){
    	var pfa = getPFA(pfaId);
        var reversalDate = pfa.getValue('custrecord_2663_reversal_date');
        return reversalDate;
    }
    
    function createVoidingJournalObj(data){
        var journalEntry = {};

        journalEntry.createdFrom = data.id;
        
        // retrieve reversal date from pfa to avoid formatting error when date format preference is changed during processing
        journalEntry.tranDate = getReversalDateFromPFA(data.pfaRecord); 
        
        journalEntry.postingperiod = data.reversalPeriod;
        
        if(runtime.isOW()){
            journalEntry.subsidiary = data.subsidiary;
        }
        if(runtime.isMultiCurrency()){
            journalEntry.currency = data.currency;
        }
        if(runtime.isClassMandatory()){
            journalEntry['class'] = data['class'];
        }
        if(runtime.isDeptMandatory()){
            journalEntry.department = data.department;
        }
        if(runtime.isLocMandatory()){
            journalEntry.location = data.location;
        }
        
        journalEntry['void'] = 'T';
        journalEntry.sublists = {};
        journalEntry.sublists.line = createLines(data);
        
        log.debug('JE params', JSON.stringify(journalEntry) );
        
        return journalEntry;
        
    }
    
    function createLines(data){
        var lines = [];
        var tranType = data.type;
        var amount = formatAmount(data.amount);
        
        var line = {};
        line[REFERENCE_LINE_TYPE[tranType]] = amount;
        line.memo = data.memo;
        line.account = data.glBankAccount;
        setLineCommonValues(line, data);
        lines.push(line);
        
        var reversalLine = {};
        reversalLine[REVERSING_LINE_TYPE[tranType]] = amount;
        reversalLine.account = data.account;
        setLineCommonValues(reversalLine, data);
        lines.push(reversalLine);
        
        if(data.discountAmount && data.discountAccountId){
            
            if(tranType == 'customerpayment'){
                var errorMessage = MESSAGEMAP[CUSTPAYMENTS_DISC_UNSUPPORTED];
                throw error.logAndCreateError(CUSTPAYMENTS_DISC_UNSUPPORTED, errorMessage);
            }
            
            var discountAmount = formatAmount(data.discountAmount);
            
            var discountLine = {};
            discountLine['credit'] = discountAmount;
            discountLine.account = data.account;
            setLineCommonValues(discountLine, data);
            lines.push(discountLine);
            
            var discountRevLine = {};
            discountRevLine['debit'] = discountAmount;
            discountRevLine.account = data.discountAccountId;
            setLineCommonValues(discountRevLine, data);
            lines.push(discountRevLine);
        }
        
        return lines;
    }
    
    function createDiscountLines(parameters){
        var amount = parameters.amount;
        var lines = [];

        var line = {};
        line['credit'] = amount;

        var reversalLine = {};
        reversalLine['debit'] = amount;
        
        lines.push(line);
        lines.push(reversalLine);
        
        return {line: line, reversalLine: reversalLine};
    }
    
    function setLineCommonValues(line, data){
        line.entity = data.entity;
        line['class'] = data['class'];
        line.department = data.department;
        line.location = data.location;
        return line;
    }
    
    
    function formatAmount(amount){
        return formatter.parse({
            value: Math.abs(amount),
            type: formatter.Type.CURRENCY
        });
    }
    function getReversalAccount(tranType, paymentId){
        
        var wrapperRecord = record.load({
            type: tranType,
            id: paymentId
        });
        var rec = wrapperRecord.getRecord();

        var reversalAccount = rec.getValue(REVERSAL_ACCOUNT_FIELD[tranType]);
        
        if(!reversalAccount){
            reversalAccount = searchAccountIdByType(ACCOUNT_TYPE[tranType]);
        }
        
        return reversalAccount;
    }
    
    function searchAccountIdByType(accountType){
        var filters = [
            ['type', search.Operator.IS, accountType ],
        ];
       var result = searchRecord(search.Type.ACCOUNT, filters, []);
       var account = result.length > 0 ? result[0].id : null;
       return account;
    }

    function initializeMessageTranslationMap(){
        var messageCodes = [CUSTPAYMENTS_DISC_UNSUPPORTED, 
                            MSG_VOID_VENDOR_PAYMENT, 
                            MSG_VOID_CUST_PAYMENT];
        var map;
        var execContext = runtime.executionContext;
        
        if (execContext == runtime.ContextType.USER_INTERFACE) {
            map = translationService.translate(messageCodes);
        } else {
            map = translator.getStringMap(messageCodes);
        }
        MESSAGEMAP = {};
        MESSAGEMAP[CUSTPAYMENTS_DISC_UNSUPPORTED] = map[CUSTPAYMENTS_DISC_UNSUPPORTED];
        MESSAGEMAP[MSG_VOID_VENDOR_PAYMENT] = map[MSG_VOID_VENDOR_PAYMENT];
        MESSAGEMAP[MSG_VOID_CUST_PAYMENT] = map[MSG_VOID_CUST_PAYMENT];
    }

    return {
        getPaymentsForReversal: getPaymentsForReversal,
        getReversalAccount: getReversalAccount,
        createVoidingJournalObj: createVoidingJournalObj,
    };
});
