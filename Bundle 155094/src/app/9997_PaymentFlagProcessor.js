/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount
 */

define([],

function () {
    var CONFIG = {
        vendorpayment: {
            epFlag: "custbody_9997_is_for_ep_eft",
            paymentOptions: {
                toach: "checkbox",
                billpay: "checkbox",
                tobeprinted: "checkbox",
                tranid: "text"
            },
            savePreviousState: true
        },
        customerpayment: {
            epFlag: "custbody_9997_is_for_ep_dd",
            paymentOptions: {
                paymentmethod: "list",
                creditcard: "sourcelist",
                checknum: "text",
                chargeit: "checkbox",
                ccapproved: "checkbox",
            },
            savePreviousState: false
        }
    };

    var FALSE_VALUE = {
        list: "",
        sourcelist: "",
        text: "",
        checkbox: false
    };
    
    var TYPE_SOURCELIST = 'sourcelist';

    function create(paymentType, record) {
        var payment = CONFIG[paymentType];
        var fieldTypeMap = payment ? payment.paymentOptions : {};

        return {

            isValidPayment: function isValidPayment() {
                return payment != undefined;
            },

            isForEP: function isForEP() {
                return Boolean( payment && record.getValue(payment.epFlag) );
            },

            clearOtherPaymentMethods: function clearOtherPaymentMethods() {
                var fields = Object.keys(fieldTypeMap);
                for (var i = 0; i < fields.length; i++) {

                    var fieldName = fields[i];
                    var fieldType = fieldTypeMap[fieldName];

                    var falseValue = FALSE_VALUE[fieldType];
                    var ignoreFieldChange = (fieldType != TYPE_SOURCELIST);
                    record.setValue({ fieldId: fieldName, value: falseValue, ignoreFieldChange: ignoreFieldChange });
                }
            },

            isFieldForEP: function isFieldForEP(fieldName) {
                return Boolean( payment && payment.epFlag == fieldName );
            },

            isFieldForOtherPaymentMethod: function isFieldForOtherPaymentMethod(fieldName) {
                return fieldTypeMap[fieldName] != undefined;
            },

            isValueForTypeTrue: function isValueForTypeTrue(fieldName, fieldValue) {
                var fieldType = fieldTypeMap[fieldName];

                return (FALSE_VALUE[fieldType] != fieldValue);
            },

            inactivateEPFlag: function inactivateEPFlag() {
                if (payment != undefined) {
                    record.setValue({ fieldId: payment.epFlag, value: false, ignoreFieldChange: true });
                }
            },
            
            activateEPFlag: function activateEPFlag() {
                if (payment != undefined) {
                    record.setValue({ fieldId: payment.epFlag, value: true, ignoreFieldChange: true });
                }
            },
        };
    }

    return {
        create: create
    };
});
