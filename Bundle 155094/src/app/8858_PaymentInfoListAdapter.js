/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Sep 2016     aalcabasa
 *
 * @NModuleScope Public
 */
define([], function() {
    
    function toIDList(paymentInfoList) {
        var transactionList = [];
        paymentInfoList.map(function(tranInfo) {
            transactionList.push(tranInfo.id);
        });
        
        return transactionList;
    }
    
    return {
        toIDList: toIDList
    };
});
