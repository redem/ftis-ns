/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Aug 2016     aalcabasa
 *
 * @NModuleScope Public
 */
define([
    '../data/9997_TransactionDAO',
    '../data/8859_CustomerDAO',
    '../data/8859_VendorDAO',
    './9997_PaymentFlagProcessor',
    '../lib/wrapper/9997_NsWrapperRecord',
    '../lib/wrapper/9997_NsWrapperSearch'],

function(transactionDAO, customerDAO, vendorDAO, paymentFlagProcessor, record, search) {
    
    var PAYMENT_SETTINGS = {
        'customerpayment': {
            'epFlag': 'custbody_9997_is_for_ep_dd',
            'entityField': 'customer',
            'dao': customerDAO,
            'entityEPFlag': 'usesDD'
        },
        'vendorpayment': {
            'epFlag': 'custbody_9997_is_for_ep_eft',
            'entityField': 'entity',
            'dao': vendorDAO,
            'entityEPFlag': 'usesEFT'
        }
    }


    function create(params) {
        var paymentRec = params.record;
        var flagProcessor = paymentFlagProcessor.create(paymentRec.type, paymentRec);
        var settings = PAYMENT_SETTINGS[paymentRec.type];
        
        var obj = {
            setupNewRecord: function setupNewRecord() {
                var entityId = paymentRec.getValue(settings.entityField);
                var entity = settings.dao.retrieve(entityId);
                if (entity[settings.entityEPFlag]) {
                    flagProcessor.activateEPFlag();
                    flagProcessor.clearOtherPaymentMethods();
                }
            },
            cleanUpEditedPayment: function cleanUpEditedPayment() {
                var forEP = this.isForEP();
                if (forEP) {
                	flagProcessor.inactivateEPFlag();
                }
            },
            clearOtherPaymentMethods: function clearOtherPaymentMethods() {
                var forEP = this.isForEP();
                if (forEP) {
                	flagProcessor.clearOtherPaymentMethods();
                }
            },
            cleanUpNewPayment: function() {
                var forEP = flagProcessor.isForEP();
                var isVendorPayment = paymentRec.type === record.Type.VENDOR_PAYMENT;
                var isOverridden = false;
                var vendPaymentByEP = false;

                if (forEP && isVendorPayment) {
                    
                    var pfaRec = paymentRec.getValue({
                        fieldId: 'custbody_9997_pfa_record'
                    });
                    // In this case, we're sure that the vendor payment is clean.
                    // This is currently not applicable to Customer Payments
                    vendPaymentByEP = pfaRec ? true : false;

                    // For bulk vendor payments, if tobeprinted or billpay is enabled in the UI
                    // then we have to inactivate EP Flag
                    var toBePrinted = paymentRec.getValue({
                        fieldId: 'tobeprinted'
                    });

                    var isBillPay = paymentRec.getValue({
                        fieldId: 'billpay'
                    });
                    log.debug('isBillPay pre-processing', isBillPay);
                    isBillPay = isBillPay == 'F' ? false : isBillPay;
                    
                    
                    log.debug('toBePrinted ', toBePrinted);
                    isOverridden = toBePrinted || isBillPay;
                }

                log.debug('forEP ', forEP);
                log.debug('isVendorPayment ', isVendorPayment);
                log.debug('vendPaymentByEP ', vendPaymentByEP);
                log.debug('toBePrinted ', toBePrinted);
                log.debug('isBillPay ', isBillPay);
                log.debug('isOverridden ', isOverridden);

                if (isOverridden) {
                    flagProcessor.inactivateEPFlag();
                } else if (!vendPaymentByEP && forEP) {
                    flagProcessor.activateEPFlag();
                    flagProcessor.clearOtherPaymentMethods();
                }
            },
            isForEP: function isForEP() {
                var forEP = flagProcessor.isForEP();
                forEPIsUnknown = (forEP === null || forEP === undefined || forEP === '');
                log.debug('forEP ', forEP);
                log.debug('forEPIsUnknown ', forEPIsUnknown);
                log.debug('isXEditOrCSV ', params.isXEditOrCSV);
                if (params.isXEditOrCSV && forEPIsUnknown) {
                    var flagField = settings.epFlag;
                    var dbRecord = search.lookupFields({
                        type: paymentRec.type,
                        id: paymentRec.id,
                        columns: [flagField]
                    });
                    forEP = dbRecord[flagField];
                }
                
                return forEP;
            }
        };
        
        return obj;
    }
    

    return {
        create: create
    };
    
});
