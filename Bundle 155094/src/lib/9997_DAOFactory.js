/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Sep 2015     aalcabasa
 * @NModuleScope TargetAccount
 */
define(["./wrapper/9997_NsWrapperRecord", "./wrapper/9997_NsWrapperSearch"], function(record, search) {
    
    /**
     * Returns a basic DAO with fields and the record type setup
     * 
     * @param {Object} params
     * @param {String} params.recordType
     * @param {Object} params.fieldMap - a mapping of object attribute names and their corresponding NS Record field names
     * 
     * */
    function getDAO(params) {
        return new DAO(params);
    }
    

    function DAO(params) {
        var recordType = params.recordType;
        var fieldMap = params.fieldMap;
        var subListMaps = params.subListMaps;
        
        this.create = function create(object) {
            var rec = record.create({
                type: recordType,
                isDynamic: true,
            });
            for ( var objFieldId in fieldMap) {
                var nsFieldId = fieldMap[objFieldId];
                rec.setValue(nsFieldId, object[objFieldId]);
            }
            
            var sublists = object.sublists || {};
            var sublistParams = {
                record: rec
            };
            for ( var sublistId in sublists) {
                sublistParams.sublist = sublists[sublistId];
                sublistParams.sublistId = sublistId;
                setSublistValues(sublistParams);
            }
            
            return rec.save();
        };
        
        function setSublistValues(params){
            var rec = params.record;
            var sublist = params.sublist;
            var sublistId = params.sublistId;
            var sublistReference = {
                sublistId: sublistId
            }
            var sublistMap = subListMaps[sublistId];
            var currentLine; 
            for (var index = 0; index < sublist.length; index++){
                currentLine = sublist[index];
                rec.selectNewLine(sublistReference);
                for (var field in sublistMap){
                    rec.setCurrentSublistValue({
                        sublistId: sublistId,
                        fieldId: sublistMap[field],
                        value:  currentLine[field]
                    });
                }
                rec.commitLine(sublistReference);
            }
        }
        
        this.retrieve = function retrieve(id) {
            var object = {
                id: id,
                sublists: {}
            };
            var rec = record.load({
                type: recordType,
                id: id
            });
            for ( var i in fieldMap) {
            	var fieldName = fieldMap[i];
                object[i] = rec.getValue(fieldName);
                var field = rec.getField(fieldName); 
                if (field && (field.type === 'select')){
                	object[i + 'Text'] = rec.getText(fieldName); 
                }
            }
            
            var subListMap;
            var params;
            var sublists = object.sublists;
            for ( var j in subListMaps) {
                subListMap = subListMaps[j];
                params = {
                    record: rec,
                    subList: j,
                    subListMap: subListMap
                };
                sublists[j] = extractSubListValues(params);
            }
            
            return object;
        };

        this.retrieveNsRecord = function retrieveNsRecord(id) {
            var wrapperRecord = record.load({
                type: recordType,
                id: id
            });

            return wrapperRecord.getRecord();
        };

        this.retrieveViaLookup = function retrieveViaLookup(id){
            var fields = [];
            
            
            for ( var key in fieldMap){
                fields.push(fieldMap[key]);
            }
            
            var lookupObj = search.lookupFields({
                type: recordType,
                id: id,
                columns: fields
            });
            log.debug('lookupObj', JSON.stringify(lookupObj))
            var obj = {
                id: id
            };
            
            for (var key in fieldMap){
                var value = lookupObj[fieldMap[key]];
                if (value instanceof Array){
                    obj[key] = value[0] ? value[0].value : '';
                    obj[key + 'Text'] = value[0] ? value[0].text: '';
                } else {
                    obj[key] = value;
                }
            }
            
            return obj;
        }
        
        this['delete'] = function deleteRecord(id){
            return record.deleteRecord({
                type: recordType,
                id: id
            });
        }
        
        function extractSubListValues(params) {
            var rec = params.record;
            var subListName = params.subList;
            var subListMap = params.subListMap;
            
            var count = rec.getLineCount({
                sublistId: subListName
            });
            var lines = [];
            for ( var i = 0; i < count; i++) {
                var line = {};
                for ( var j in subListMap) {
                    line[j] = rec.getSublistValue({
                        sublistId: subListName,
                        fieldId: subListMap[j],
                        line: i
                    });
                }
                lines.push(line);
            }
            return lines;
        }
        
    }
    
    return {
        getDAO: getDAO
    };
});
