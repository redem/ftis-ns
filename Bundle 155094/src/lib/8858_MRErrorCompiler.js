/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Aug 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([],

function() {
    
    function compile(summary) {
        var errors = [];
        
        summary.errors.iterator().each(function(key, value) {
            errors.push(value);
        });
        
        return errors;
    }
    
    return {
        compile: compile
    };
});
