/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */

define([], function() {

    var translation = {};

    // External errors
    translation['EP_COULD_NOT_PAY_ENTITY'] = 'Could not pay entity {ENTITY}. View the list of unprocessed transactions. See logs for more details.';
    translation['EP_COULD_NOT_FIND_TRANS'] = 'Could not find transaction {TRANSACTION} for entity: {ENTITY}.';
    translation['EP_COULD_NOT_PAY_ENTITY_BASE'] = 'Could not pay entity {ENTITY}.';
    translation['EP_COULD_NOT_PAY_ENTITY_LIST'] = 'List of transactions:\n{TRANLIST}.';
    translation['EP_INCOMPLETE_PAYMENTS'] = 'EP_INCOMPLETE_PAYMENTS\nView the list of unprocessed transactions. See logs for more details.';
    translation['EP_NO_PAYMENTS_CREATED'] = 'EP_NO_PAYMENTS_CREATED\nFailed to create payment transactions for all records in the Unprocessed Transactions list. See logs for more details.';
    translation['CUSTPAYMENTS_DISC_UNSUPPORTED'] = 'Reversal of Customer Payment with Discounts is not supported.';
    translation['EP_FILE_TEMPLATE_INACTIVE'] = 'The payment file template must be active throughout EP processing.';
    translation['EP_INVALID_PROCESS_ID'] = 'Invalid Process ID';
    translation['EP_UNSUPPORTED_PAYMENT_FILE_TYPE'] = 'File type {TYPE} is currently not supported by Electronic Payments.';
    translation['EP_CREATE_FILE_ERROR'] = 'An error has occurred during payment file creation. See logs for more details.';
    translation['EP_TEMPLATE_RENDERER_ERROR'] = 'FreeMarker errors were found in the custom payment file template. See logs for more details. For information, see the Working with Advanced Templates topic in the Help Center.';
    translation['IPM_INVALID_AGGREGATION'] = 'Aggregation method not found on the payment transactions: Internal ID ';
    translation['EP_NO_TRANSACTIONS_FOUND'] = 'No transactions were found for marking. Verify that all transactions are still existing and available for EP processing. See logs for more details.';
    translation['EP_ENTITY_BANK_NOT_FOUND'] =  'Error in retrieving bank details. Verify that bank accounts have been assigned to the following entities : {MISSING_ENTITIES}.';
    translation['NO_AVAILABLE_DEPLOYMENT'] = 'No deployment is available';
    
    // Reversal
    translation['reversal.voidVendorPayment'] = 'Void Of Bill Payment #';
    translation['reversal.voidCustomerPayment'] = 'Void Of Customer Payment #';

    // Notification
    translation['notif.subject.success'] = 'PFA {PFA_NUMBER} Completed';
    translation['notif.subject.error'] = 'PFA {PFA_NUMBER} Error Notification';
    translation['notif.body.success'] = '<p>The payment file  {PFA_NUMBER} has been successfully created.<br/><br/>You can view the Payment File Administration record <a href="{FILE_LINK}">here</a></p>';
    translation['notif.body.error'] = 'While creating PFA {PFA_NUMBER}, the following errors were encountered during processing:<br/>{ERROR_MESSAGE}<br/><br/><p>For more information, please access the record <a href="{PFA_LINK}">here</a>';
    translation['notif.message.error'] = '<p>An error was encountered during processing</p>';
    
    // Forms
    translation['dd.form.title'] = 'EP DD - Invoice Payment File Generation';
    translation['dd.form.fieldGroupLabel'] = 'DD Payment Information';
    translation['dd.form.referenceNoteLabel'] = 'DD File Reference Note';

    translation['eft.form.title'] = 'EP EFT - Bill Payment File Generation';
    translation['eft.form.fieldGroupLabel'] = 'EFT Payment Information';
    translation['eft.form.referenceNoteLabel'] = 'EFT File Reference Note';

    return translation;
});
