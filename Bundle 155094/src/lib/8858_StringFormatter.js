/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * This module assembles string with parameter replacements
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Oct 2015     mjaurigue
 * 
 * @NModuleScope TargetAccount
 */


define(["N/error"], function (error) {
    
    var string;
    
    function setString(s){
        if(!s){
            throw error.create({
                name: "REQUIRED_PARAM_MISSING",
                message: "Method setString of String Formatter must not be null or undefined."
            });
        }
        string = s;
    }
    
    function replaceParameters(parameters){
        for ( var i in parameters) {
            var re = new RegExp("\\{" + i + "\\}", "g");
            string = string.replace(re, parameters[i]);
        }
    }
    
    function toString() {
        return string.toString();
    }
    
    
    function getPadString(minLength, character) {
        var remainingLength = parseInt(minLength, 10) - string.length;
        return remainingLength > 0 ? new Array(remainingLength + 1).join(character || " ") : "";
    }

    function padLeft(minLength, character) {
        string = getPadString(minLength, character) + string;
        return string;
    }
    
    return {
        setString: setString,
        replaceParameters: replaceParameters,
        toString: toString,
        padLeft: padLeft
    };
});