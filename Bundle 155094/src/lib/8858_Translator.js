/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code. 
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Dec 2015         ldimayuga
 * 
 * @NModuleScope TargetAccount
 */
define(["N/file", "N/runtime", "require"], function (file, runtime, require) {
	var locale;
	var LANG = "LANGUAGE";
	var translation;
	
	
	
	function getTranslation() {
		var deltaTranslation;
		
		if(!locale){
			locale = runtime.getCurrentUser().getPreference(LANG);
		}
		
		
		require(["./translation/en_US"], function (translationObj){
			translation = translationObj;
        });

		if (locale.substring(0,2) !== "en") {
			try {
				require(["./translation/"+locale], function (translationObj){
					deltaTranslation = translationObj;
		        });	
			}catch (e) {
				log.error("EI_TRANSLATOR_ERROR", e);
			}
		}
	
		translation = updateTranslation(translation, deltaTranslation);
		
		return translation;
		
	}
	
	function updateTranslation(t, d){
		for(var key in d) {
		    var value = d[key];
		    t[key] = value;
		}
		
		return t;
	}
	
	function getString(code){
		if(!translation){
			translation = getTranslation();
		}
		
		return translation[code];
	}
	
	function getStringMap(stringArray) {
		var stringMap = {};

		for (var i = 0; i < stringArray.length; i++) {
			var key = stringArray[i];
			stringMap[key] = getString(key);
		}
		
		return stringMap;
	}
	
	function setLocale(l){
		locale = l;
	}
	
	return {
		getString : getString,
		getStringMap : getStringMap,
		setLocale : setLocale,
		getTranslation: getTranslation
	};
});

