/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NModuleScope TargetAccount
 */

define(['N/format'], function(formatter) {

    function format(options) {
        return formatter.format(options);
    }

    function parse(options){
        return formatter.parse(options);
    }

    return {
        Type: formatter.Type,
        format: format,
        parse: parse,
    };
});