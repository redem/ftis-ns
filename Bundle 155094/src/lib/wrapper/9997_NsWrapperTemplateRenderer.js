/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code. 
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Sep 2015         ldimayuga
 *
 */
define(['N/render', 'N/file'],

function(render, file) {
    var templateRenderer;
    
    function getRenderer() {
        if (!templateRenderer) {
            templateRenderer = render.create();
        }
        return templateRenderer;
    }
    // uses a file for loading
    function setTemplate(template) {
        var xmlTemplateFile = file.load(template);
        getRenderer().templateContent = xmlTemplateFile.getContents();
    }
    // uses the string for setting the content
    function setTemplateContents(contents) {
        getRenderer().templateContent = contents;
    }
    function addRecord(label, record) {
        try {
            getRenderer().addRecord(label, record);
        } catch (e) {
            log.error('error', JSON.stringify(e));
        }
        
    }
    
    function addSearchResults(label, search) {
        getRenderer().addSearchResults(label, search);
    }
    
    function renderAsString() {
        return getRenderer().renderAsString();
    }
    
    function renderToResponse(response) {
        return getRenderer().renderToResponse(response);
    }
    
    function addDataSource(label, object) {
        var renderer = getRenderer();
        
        switch (typeof object) {
            case 'nlobjRecord':
                renderer.addRecord(label, object);
                break;
            case 'nlobjSearchResult':
                renderer.addSearchResults(label, object);
                break;
            default:
                break;
        }
        
    }
    
    function addCustomDataSource(params) {
        getRenderer().addCustomDataSource(params);
    }
    
    return {
        setTemplate: setTemplate,
        setTemplateContents: setTemplateContents,
        addRecord: addRecord,
        addSearchResults: addSearchResults,
        addCustomDataSource: addCustomDataSource,
        addDataSource: addDataSource,
        renderAsString: renderAsString,
        renderToResponse: renderToResponse,
        DataSource: render.DataSource
    };
});
