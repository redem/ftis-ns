/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * @NModuleScope TargetAccount
 */
define(["N/redirect"], function(redirect) {

    function toRecord(params) {
        redirect.toRecord(params);
    }
    
    return {
        toRecord: toRecord
    };
});
