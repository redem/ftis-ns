/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author aaclan
 * @NScriptName 9997_NsWrapperMessage.js
 * @NApiVersion 2.0
 * @NModuleScope Public
 */

define(['N/ui/message', 
        ],

function(nsMessage) {
     
     var banner;
     
     function hidePreviousBanner(){
         if(banner){
             banner.hide();
         }
     }
     
     function showError(title, message){
         showMessage({
             title: title, 
             message: message, 
             type: nsMessage.Type.ERROR
         });
     }
     
     function showWarning(title, message){
         showMessage({
             title: title, 
             message: message, 
             type: nsMessage.Type.WARNING
         });
     }
     
     function showInformation(title, message){
         showMessage({
             title: title, 
             message: message, 
             type: nsMessage.Type.INFORMATION
         });
     }

     function showMessage(params){
         hidePreviousBanner();
         banner = nsMessage.create(params);
         banner.show();
     }
     
     
     return {
         hide : hidePreviousBanner,
         error : showError,
         warn : showWarning,
         info : showInformation
     };
});