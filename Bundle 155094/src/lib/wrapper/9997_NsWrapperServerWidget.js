/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Jan 2016     aalcabasa
 *
 * @NModuleScope public
 */
define(["N/ui/serverWidget"], function(serverWidget) {
    return {
        FieldType: serverWidget.FieldType,
        FieldDisplayType: serverWidget.FieldDisplayType,
        FieldBreakType: serverWidget.FieldBreakType,
        FieldLayoutType: serverWidget.FieldLayoutType,
        SublistType: serverWidget.SublistType,
        createForm: serverWidget.createForm
    };
});
