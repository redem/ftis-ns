/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Oct 2015     mjaurigue
 *
 */

define(["./9997_NsWrapperServerWidget"], function (serverWidget) {
    var nsForm;
    
    function createForm(obj){
        nsForm = serverWidget.createForm(obj);
    }
    
    function addSubmitButton(obj, disabled) {
        var button = nsForm.addSubmitButton(obj);
        button.isDisabled = disabled;
    }
    
    function getFormObject() {
        return nsForm;
    }
    
    function addClientScriptFileId(id) {
        nsForm.clientScriptFileId = id;
    }
    
    function addField(obj) {
        var field = nsForm.addField(obj);
        if (obj.help) {
            field.setHelpText(obj.help);
        }

        if (obj.defaultValue) {
            field.defaultValue = obj.defaultValue;
        }
        
        if (obj.linkText) {
            field.linkText = obj.linkText;
        }
        
        if (obj.breakType) {
            field.updateBreakType({"breakType": obj.breakType});
        }
        
        if (obj.layoutType) {
            field.updateLayoutType({"layoutType": obj.layoutType});
        }
        
        if (obj.isMandatory) {
            field.isMandatory = obj.isMandatory;
        }
        
        if (obj.displayType) {
            field.updateDisplayType({"displayType": obj.displayType});
        }
        
        if (obj.displaySize) {
            field.updateDisplaySize(obj.displaySize);
        }
        
        if (obj.selectOptions){
            addSelectOptions(field, obj.selectOptions);
        }
        
    }
    
    function addSelectOptions(field, options){
    	for (var i = 0; i < options.length ; i++){
    		field.addSelectOption(options[i]);
    	}
        
    }
    
    function addButton(obj) {
        nsForm.addButton(obj);
    }
    
    function addFieldGroup(obj) {
        nsForm.addFieldGroup(obj);
    }

    return {
        FieldType: serverWidget.FieldType,
        FieldDisplayType: serverWidget.FieldDisplayType,
        FieldBreakType: serverWidget.FieldBreakType,
        FieldLayoutType: serverWidget.FieldLayoutType,
        createForm: createForm,
        addSubmitButton: addSubmitButton,
        getFormObject: getFormObject,
        addField: addField,
        addFieldGroup: addFieldGroup,
        addButton: addButton,
        addClientScriptFileId: addClientScriptFileId
    };
});