/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code. 
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Sep 2015         ldimayuga
 *
 * @NModuleScope1 TargetAccount
 * @NModuleScope Public
 *
 */
define(['N/render'],

function(render) {
    
    function transaction(options) {
        return render.transaction(options);
    }
    
    return {
        PrintMode: render.PrintMode,
        transaction: transaction
    };
    
});
