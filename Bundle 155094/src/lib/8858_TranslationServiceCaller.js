/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * This module serves as client scripts' entry point for connecting with the translation service.
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Feb 2016     mjaurigue
 *
 * @NModuleScope TargetAccount
 */


define(["N/url", "N/https"], function(url, https) {

    function translate(stringArray){
        var SERVICE_SU_SCRIPT = "customscript_8858_translation_service_su";
        var SERVICE_SU_DEPLOY = "customdeploy_8858_translation_service_su";

        var parameters = {stringArray: stringArray.join(",")};

        var suiteletURL = url.resolveScript({
            scriptId: SERVICE_SU_SCRIPT, 
            deploymentId: SERVICE_SU_DEPLOY,
        });

        var response = https.post({
            url: suiteletURL,
            body: parameters 
        });

        var stringMap = JSON.parse(response.body);
        return stringMap;
    }

    return {
        translate: translate
    };
});
