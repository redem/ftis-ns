/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author aaclan
 * @NScriptName 9997_FormBuilder.js
 * @NApiVersion 2.0
 * @NModuleScope Public
 */

define(['./wrapper/9997_NsWrapperForm'
        ],

function(nsForm) {
     
     function buildForm(formDefinition){
         nsForm.createForm({
             title: formDefinition.title,
         });
         
         if(formDefinition.clientScriptId){
             nsForm.addClientScriptFileId(formDefinition.clientScriptId);
         }
         
         var submitButton = formDefinition.submitButton;

         if(submitButton){
             nsForm.addSubmitButton({ id: submitButton.id, label: submitButton.label,}, false);
         }
         
         var fieldGroups = formDefinition.fieldgroups;
         
         for (var i = 0; i < fieldGroups.length; i++ ){
             nsForm.addFieldGroup(fieldGroups[i]);
         }
         
         var fields = formDefinition.fields;
         
         for (var i = 0; i < fields.length ; i++){
             nsForm.addField(fields[i]);
         }
         
         return nsForm.getFormObject();
     }
     
     return {
        buildForm : buildForm,
     };
});