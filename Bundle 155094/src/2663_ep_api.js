/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */


EPPaymentSelectionForm = function() {
	
	//start classes
		/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2013/08/29  263190 		  3.00.00				  Initial version
 * 2013/09/25  263190 		  3.00.00				  Add governance and time limit handling on getAllResults method
 * 2013/09/26  263190 		  3.00.00				  Add getCurrencyPrecision && getConfigurationObject functions
 * 2013/11/20  270099 		  3.00.10				  Add getMap functions
 * 2013/11/20  269865 		  3.00.10				  Support workaround using nlapiSearchRecord
 * 2013/11/21  269865 		  3.00.10				  Replace while loop with do while loop
 * 2014/01/07  274281  		  3.00.00				  Add error logs on method getAllResults
 * 2013/11/26                 3.00.10				  Add method to determine domain url
 * 2014/01/07  274281  		  3.00.00				  Add error logs on method getAllResults
 * 2014/03/14  236313 		       			          Add string utility object
 * 2014/04/25  287659		  3.02.4				  Add function to get EP specific preferences
 */

var _2663 = _2663 || {};

_2663.Logger = function Logger(mainTitle) {
	mainTitle = mainTitle || 'Electronic Payments';
	var debugDisabled = false;
	var errorDisabled = false;

	function setTitle(title) {
		mainTitle = title;
	}
	
	function debug(details, enabled) {
		if (!debugDisabled || enabled) {
			nlapiLogExecution('DEBUG', mainTitle, details);	
		}
	}

	function error(details, ex, enabled) {
		if (!errorDisabled || enabled) {
			var errorMessage = details || '';
			if (ex) {
				errorMessage += errorMessage ? ': ' : '';
				errorMessage += stringifyException(ex);
			}
			nlapiLogExecution('ERROR', mainTitle, errorMessage);	
		}
	}
	
	function disable(type) {
		if (type == 'debug') {
			debugDisabled = true;
		} else if (type == 'error') {
			errorDisabled = true;
		}
	}

	function stringifyException(ex) {
		var exStr = '';
		if (ex) {
			if (ex instanceof nlobjError) {
				exStr += ex.getCode() + '\n' + ex.getDetails() + '\n' + ex.getStackTrace();
			} else {
				exStr += ex.toString() + '\n' + ex.stack;
			}	
		}
		return exStr;
	}
	
	this.debug = debug;
	this.error = error;
	this.disable = disable;
	this.setTitle = setTitle;
	this.stringifyException = stringifyException;
};

_2663.Search = function Search(type) {
	var logTitle = 'Search';
	var logger = new _2663.Logger(logTitle);
	var filters = [];
	var columns = [];
	var savedSearch = '';
	
	function setSavedSearch(savedSearchId) {
		savedSearch = savedSearchId;
	}

	function addFilter(name, join, operator, value1, value2, summary, formula) {
		var filter = new nlobjSearchFilter(name, join, operator, value1, value2);
		filters.push(filter);
		if (summary) {
			filter.setSummaryType(summary);
		}
		if (formula) {
			filter.setFormula(formula);
		} 
	}
	
	function getFilters() {
		return filters;
	}
	
	function addFilters(arrFilters) {
		if (arrFilters && arrFilters.length > 0) {
			filters = filters.concat(arrFilters);
		}
	}

	function addColumn(name, join, summary) {
		columns.push(new nlobjSearchColumn(name, join, summary));
	}

	function getColumns() {
		return columns;
	}
	
	function addColumns(arrColumns) {
		if (arrColumns && arrColumns.length > 0) {
			columns = columns.concat(arrColumns);
		}
	}

	function setSort(index, order) {
		columns[index].setSort(order);
	}

	function removeLastFilter() {
		filters.pop();
	}

	function setOr(filterIndex, setOr) {
		filters[filterIndex].setOr(setOr);
	}

	function setLeftParens(filterIndex, numParens) {
		filters[filterIndex].setLeftParens(numParens);
	}

	function setRightParens(filterIndex, numParens) {
		filters[filterIndex].setRightParens(numParens);
	}
	
	function clearFilters() {
		filters = [];
	}
	
	function clearColumns() {
		columns = [];
	}

	function getResults() {
		if(type == ''){
			logger.error('Please set record type.');
			return null;
		}

		return nlapiSearchRecord(type, savedSearch, filters, columns) || [];
	}

	function getAllResults(restartTrigger, startTime, maxTime, useOldSearch, line, maxResultLength) {
		var allSearchResults = [];
		allSearchResults.errorFlag = false;
		if (useOldSearch) {
			var markerObj = {};
			markerObj.lastIndex = 1;
			do {
				try {
					// search with marker
					var searchResults = _2663.searchRecordWithMarker(type, savedSearch, filters, columns, line, markerObj, maxResultLength, restartTrigger, startTime, maxTime);
					// concatenate to results to return
					allSearchResults = allSearchResults.concat(searchResults || []);
	        	} catch (ex) {
	        		allSearchResults.errorFlag = true;
	        		logger.error('_2663.searchRecordWithMarker', ex);
	        		logger.error('total search results: ' + allSearchResults.length + '<br>' + 'elapsed time: ' + (new Date() - startTime));
	        		break;
	        	}
			} while (searchResults && searchResults.length > 0)
		} else {
			var search = savedSearch ? nlapiLoadSearch(type, savedSearch) : nlapiCreateSearch(type, filters, columns);
		    if (savedSearch) {
		    	if (filters) {
		    		search.addFilters(filters);
		    	}
		    	if (columns) {
		    		logger.debug('columns: ' + columns.length);
		    		search.addColumns(columns);
		    	}
		    }
		    var searchResultSet = search.runSearch();
		    var startIdx = 0;
		    var endIdx = 1000;
		    do {
	        	try {
	        		if (_2663.governanceReached(restartTrigger)) {
	                    throw nlapiCreateError('EP_GOVERNANCE_REACHED', 'Governance reached.', true);
	                }

	                if (_2663.hasTimedOut(startTime, maxTime)) {
	                    throw nlapiCreateError('EP_SEARCH_TIME_EXCEEDED', 'Search time exceeded.', true);
	                }
	                var searchResults = searchResultSet.getResults(startIdx, endIdx);
	    	        if (searchResults) {	        		
		        		allSearchResults = allSearchResults.concat(searchResults);
			            startIdx += 1000;
			            endIdx += 1000;	
	    	        }
	        	} catch (ex) {
	        		allSearchResults.errorFlag = true;
	        		logger.error('getAllResults', ex);
	        		logger.error('total search results: ' + allSearchResults.length + '<br>' + 'elapsed time: ' + (new Date() - startTime));
	        		break;
	        	}
		    }
		    while(searchResults && searchResults.length > 0);
		}
		return allSearchResults;
	}
	
	this.setSavedSearch = setSavedSearch;
	this.addFilter = addFilter;
	this.addFilters = addFilters;
	this.addColumn = addColumn;
	this.addColumns = addColumns;
	this.setSort = setSort;
	this.setOr = setOr;
	this.setLeftParens = setLeftParens;
	this.setRightParens = setRightParens;
	this.clearFilters = clearFilters;
	this.clearColumns = clearColumns;
	this.getResults = getResults;
	this.getAllResults = getAllResults;
	this.getFilters = getFilters;
	this.getColumns = getColumns;
};

_2663.StringUtil = function StringUtil(){
    /**
     * Applies padding to a string 
     *
     * @param   {String}    dir         - direction of padding ("left" or "right")
     * @param   {String}    pad         - padding to be inserted
     * @param   {String}    str         - original string value
     * @param   {Integer}   length      - length of the output string
     *
     * @return  {String}    -   resulting string (empty if checks fail)
     */
    function applyPadding(dir, pad, str, length) {        

        var res = '';

        // check parameters
        try {
            if (!dir){
                var error = 'applyPadding: no padding direction';
                nlapiCreateError('MISSING_PARAMETER', error, false);
                throw error;
            }
            if (dir && dir != 'left' && dir != 'right'){
                var error = 'applyPadding: invalid padding direction';
                nlapiCreateError('INVALID_PARAMETER', error, false);
                throw error;            
            }
            if (!pad){
                var error = 'applyPadding: no padding character';
                nlapiCreateError('MISSING_PARAMETER', error, false);
                throw error;
            }
            if (pad && pad.length > 1){
                var error = 'applyPadding: invalid pad character';
                nlapiCreateError('MISSING_PARAMETER', error, false);
                throw error;
            }            
            if (!length){
                var error = 'applyPadding: no length';
                nlapiCreateError('MISSING_PARAMETER', error, false);
                throw error;
            }
        }
        catch (ex){
            alert('ERROR: ' + ex + '. Returning blank value.');
            return '';
        }
        
        // proceed with apply padding
        for (var i = 1, ii = length - str.length; i <= ii; i++){
            res += pad;
        }        
        if (dir == 'left'){
            res = res + str;
        }
        else if (dir == 'right'){
            res = str + res;        
        }
        return res;
    } 
    
    this.applyPadding = applyPadding;
}

/**
 * Establish whether governance has been reached.
 *
 * @param {Object} strRestartTrigger (in units)
 */
_2663.governanceReached = function governanceReached(strRestartTrigger){
    var lReschedule = false;
    var context = nlapiGetContext();
    var usageRemaining = context.getRemainingUsage();
    
    // Default the restart trigger to 100 units.
    strRestartTrigger = strRestartTrigger || 100;
    if (usageRemaining < strRestartTrigger) {
        lReschedule = true;
        nlapiLogExecution('debug', 'Schedule Governance', 'Governance Reached. Units remaining  : ' + usageRemaining);
    }
    return lReschedule;
};

_2663.hasTimedOut = function hasTimedOut(startTime, maxTime) {
    var timeOutFlag = false;
    if (startTime && maxTime) {
        var currentTime = (new Date()).getTime();
        var timeElapsed = currentTime - startTime;
        if (timeElapsed >= maxTime) {
            timeOutFlag = true;
        }
    }
    return timeOutFlag;
};

_2663.isApprovalRoutingEnabled = function isApprovalRoutingEnabled() {
	var objPreference = _2663.getEPPreferences();
	if (objPreference && objPreference.eft_approval_routing) {
		return objPreference.eft_approval_routing;
	}
	return false;
};

_2663.getEPPreferences = function getEPPreferences() {
	var objPreference = {
		eft_approval_routing: false,
		include_name: false
	};
	
	var prefSearch = new _2663.Search('customrecord_ep_preference');
	prefSearch.addColumn('custrecord_ep_eft_approval_routing');
	prefSearch.addColumn('custrecord_ep_include_name');
	
	var preference = (prefSearch.getResults())[0];
	if (preference) {
		objPreference.eft_approval_routing =  preference.getValue('custrecord_ep_eft_approval_routing') == 'T'; 
		objPreference.include_name =  preference.getValue('custrecord_ep_include_name') == 'T';
	}
	return objPreference;
};

_2663.getCurrencyPrecision = function getCurrencyPrecision(currencyId) {
    var precision = 2;
    if (currencyId) {
        var currency = nlapiLoadRecord('currency', currencyId);
        if (currency) {
            precision = currency.getFieldValue('currencyprecision') || 2;
        }
    }
    return precision;
};

_2663.getConfigurationObject = function getConfigurationObject() {
	var logger = new _2663.Logger('[ep] Common: getConfigurationObject');
    var companyInfoConfigurationObject = {};
    try {
        var adminFieldsUrl = nlapiResolveURL('SUITELET', 'customscript_ep_admin_data_loader_s', 'customdeploy_ep_admin_data_loader_s', true);
        var params = {};
        params['custparam_ep_language'] = nlapiGetContext().getPreference('LANGUAGE');
        var adminFields = nlapiRequestURL(adminFieldsUrl, params);
        var adminFieldsResult = adminFields.getBody();
        companyInfoConfigurationObject = JSON.parse(adminFieldsResult);
    } catch(ex) {
    	logger.error('Error occurred while loading the configuration', ex);
    }
    return companyInfoConfigurationObject;
};

_2663.getMap = function getMap(type, name) {
	var map = {};
	if (type) {
		name = name || 'name';
		var search = new _2663.Search(type);
		search.addColumn(name);
		var searchResults = search.getResults();
		for (var i = 0, ii = searchResults.length; i < ii; i++) {
			var searchResult = searchResults[i];
			map[searchResult.getId()] = searchResult.getValue(name);
		}
	}
	return map;
};

/**
 * Get the internet domain used
 *
 * @returns {String}
 */
_2663.getDomainUrl = function getDomainUrl(){
    //determine domain url by resolving an existing suitelet
    var domainUrl = nlapiResolveURL('SUITELET','customscript_2663_payment_selection_s','customdeploy_2663_payment_selection_s',true);
    domainUrl = domainUrl.split('/app')[0];
    domainUrl = domainUrl.replace('forms','system');
    return domainUrl;
};

/**
 * Gets succeeding search results from the given internal id and line id (if line == true)
 * - Use the markerObj to store the state. For the initial search, set markerObj.lastIndex = 1.
 * - Calls to this function must handle the Script Execution Limit and Script API 
 *   Governance exceptions
 * 
 * @param {String} recordType
 * @param {String} savedSearch
 * @param {Object} filters
 * @param {Object} columns
 * @param {Integer} line
 * @param {Object} markerObj
 * @param {Integer} maxResultLength
 * @param {Integer} restartTrigger
 * @param {Date} startTime
 * @param {Integer} maxTime
 */
_2663.searchRecordWithMarker = function searchRecordWithMarker(recordType, savedSearch, filters, columns, line, markerObj, maxResultLength, restartTrigger, startTime, maxTime) {
    var searchResults = [];

	if (_2663.governanceReached(restartTrigger)) {
        throw nlapiCreateError('EP_GOVERNANCE_REACHED', 'Governance reached.', true);
    }

    if (_2663.hasTimedOut(startTime, maxTime)) {
        throw nlapiCreateError('EP_SEARCH_TIME_EXCEEDED', 'Search time exceeded.', true);
    }
	
    if (recordType && markerObj) {
    	columns = columns || [];
        columns.push(new nlobjSearchColumn('internalid').setSort());
        if (line) {
            columns.push(new nlobjSearchColumn('line').setSort());
        }
        var limitFilters = [];
        limitFilters = limitFilters.concat(filters);
        if (markerObj.lastInternalId) {
            limitFilters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthanorequalto', markerObj.lastInternalId));
            var formula = 'CASE WHEN {internalid} = ' + markerObj.lastInternalId;
            if (line) {
                formula += ' AND {line} <= ' + markerObj.lastLineNum;
            }
            formula += ' THEN 0 ELSE 1 END';
            limitFilters.push(new nlobjSearchFilter('formulanumeric', null, 'equalto', 1).setFormula(formula));
        }
        var tmpSearchResults = nlapiSearchRecord(recordType, savedSearch, limitFilters, columns);
        if (tmpSearchResults && maxResultLength && maxResultLength < tmpSearchResults.length) {
            for (var i = 0; i < maxResultLength; i++) {
                searchResults.push(tmpSearchResults[i]);
            }
        } else {
            searchResults = tmpSearchResults;
        }

        if (searchResults) {
            nlapiLogExecution('debug', 'Payment Module Processing', 'searchWithMarker results: ' + searchResults.length);
            markerObj.lastIndex = searchResults.length - 1;
            markerObj.lastInternalId = searchResults[markerObj.lastIndex].getId();
            if (line) {
                markerObj.lastLineNum = searchResults[markerObj.lastIndex].getValue('line');
            }
        } else {
            markerObj.lastIndex = 0;
        }
    }
    return searchResults;
};
/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2013/09/19  263190 		  3.00.00				  Initial version
 * 2013/09/26  263190 		  3.00.00				  Fixed errors on _2663.Batch
 * 2013/10/05  265406 		  3.00.00				  Refactor code to use Payment File Administration record instead of Payment Batch
 * 2013/10/07  265406 		  3.00.00				  Add methods for Priority and File Processed fields
 * 2013/11/07  267722 		  3.00.00				  Add methods for Process Date and Posting Period fields
 * 2013/11/20  269865 		  3.00.10				  Replace getResults with getAllResults when removing transactions
 * 													  using old search workaround
 * 2014/01/24  273463 		  3.00.10				  Added methods for Partially Payed Keys and Transaction Entities fields
 * 													  Update methods for adding and removing transactions in a batch
 * 2014/05/19  273487         3.00.3       			  Add setters and getters for fields Parent Deployment, Last Process and Due Batch Details     
 */

var _2663 = _2663 || {};

_2663.PaymentBatch = function PaymentBatch() {
	var logTitle = '_2663.PaymentBatch'; 
	var logger = new _2663.Logger(logTitle);
	var recordType = 'customrecord_2663_file_admin';
	var record = null;
	var batchTransaction = new _2663.PaymentBatchTransaction();
	var batchTranRecType = batchTransaction.getRecordType();
	var thisObj = this;
	
	function setName(value) {
		record.setFieldValue('altname', value);
	}
	
	function setSubsidiary(value) {
		record.setFieldValue('custrecord_2663_payment_subsidiary', value);
	}
	
	function setBankAccount(value) {
		record.setFieldValue('custrecord_2663_bank_account', value);
	}
	
	function setAccount(value) {
		record.setFieldValue('custrecord_2663_account', value);
	}
	
	function setPaymentType(value) {
		record.setFieldValue('custrecord_2663_payment_type', value);	
	}
	
	function setDetail(value) {
		record.setFieldValue('custrecord_2663_batch_detail', value);	
	}
	
	function setTransactionKeys(value) {
		record.setFieldValue('custrecord_2663_payments_for_process_id', JSON.stringify(value || []));	
	}
	
	function setTransactionAmounts(value) {
		record.setFieldValue('custrecord_2663_payments_for_process_amt', JSON.stringify(value || []));
	}
	
	function setTransactionEntities(value) {
		record.setFieldValue('custrecord_2663_transaction_entities', JSON.stringify(value || []));
	}
	
	function setJournalKeys(value) {
		record.setFieldValue('custrecord_2663_journal_keys', JSON.stringify(value || []));	
	}
	
	function setPartiallyPaidKeys(value) {
		record.setFieldValue('custrecord_2663_partially_paid_keys', JSON.stringify(value || []));	
	}
	
	function setTotalAmount(value) {
		record.setFieldValue('custrecord_2663_total_amount', value);
	}
	
	function setTotalTransactions(value) {
		record.setFieldValue('custrecord_2663_total_transactions', value);
	}
	
	function setStatus(value) {
		record.setFieldValue('custrecord_2663_status', value);	
	}
	
	function setNumber(value) {
		record.setFieldValue('custrecord_2663_number', value);	
	}
	
	function setTimeStamp(value) {
		record.setFieldValue('custrecord_2663_time_stamp', value);	
	}
	
	function setApprovalLevel(value) {
		record.setFieldValue('custrecord_2663_approval_level', value);
	}
	
	function setPreviousApprovalLevel(value) {
		record.setFieldValue('custrecord_2663_prev_approval_level', value);
	}
	
	function setLastProcess(value) {
		record.setFieldValue('custrecord_2663_last_process', value);	
	}
	
	function setParentDeployment(value) {
		record.setFieldValue('custrecord_2663_parent_deployment', value);	
	}
	
	function setFileProcessed(value) {
		record.setFieldValue('custrecord_2663_file_processed', value);	
	}
	
	function setPriority(value) {
		record.setFieldValue('custrecord_ep_priority', value);	
	}
	
	function setProcessDate(value) {
		record.setFieldValue('custrecord_2663_process_date', value);	
	}
	
	function setPostingPeriod(value) {
		record.setFieldValue('custrecord_2663_posting_period', value);	
	}
	
	function setDueBatchDetails(arrValue) {
		record.setFieldValues('custrecord_2663_due_batch_details', arrValue);	
	}
	
	function getRecordType() {
		return recordType;
	}
	
	function getId() {
		return record.getId();
	}
	
	function getName() {
		return record.getFieldValue('altname');
	}
	
	function getSubsidiary(){
		return record.getFieldValue('custrecord_2663_payment_subsidiary');
	}
	
	function getBankAccount() {
		return record.getFieldValue('custrecord_2663_bank_account');
	}
	
	function getAccount() {
		return record.getFieldValue('custrecord_2663_account');
	}
	
	function getPaymentType() {
		return record.getFieldValue('custrecord_2663_payment_type');	
	}
	
	function getDetail() {
		return record.getFieldValue('custrecord_2663_batch_detail');	
	}
	
	function getTransactionKeys() {
		return JSON.parse(record.getFieldValue('custrecord_2663_payments_for_process_id') || '[]');
	}
	
	function getTransactionAmounts() {
		return JSON.parse(record.getFieldValue('custrecord_2663_payments_for_process_amt') || '[]');
	}
	
	function getTransactionEntities() {
		return JSON.parse(record.getFieldValue('custrecord_2663_transaction_entities') || '[]');
	}
	
	function getJournalKeys() {
		return JSON.parse(record.getFieldValue('custrecord_2663_journal_keys') || '[]');
	}
	
	function getPartiallyPaidKeys() {
		return JSON.parse(record.getFieldValue('custrecord_2663_partially_paid_keys') || '[]');
	}
	
	function getTotalAmount() {
		return (record.getFieldValue('custrecord_2663_total_amount') || 0) * 1;
	}
	
	function getTotalTransactions() {
		return record.getFieldValue('custrecord_2663_total_transactions');
	}
	
	function getStatus() {
		return record.getFieldValue('custrecord_2663_status');	
	}
	
	function getNumber() {
		return record.getFieldValue('custrecord_2663_number');	
	}
	
	function getTimeStamp() {
		return record.getFieldValue('custrecord_2663_time_stamp');	
	}
	
	function getRemovedKeys() {
		return JSON.parse(record.getFieldValue('custrecord_2663_removed_keys') || '[]');
	}
	
	function getApprovalLevel() {
		return record.getFieldValue('custrecord_2663_approval_level');
	}
	
	function getPreviousApprovalLevel() {
		return record.getFieldValue('custrecord_2663_prev_approval_level');
	}
	
	function getOwner() {
		return record.getFieldValue('ownerid');
	}
	
	function getFileProcessed() {
		return record.getFieldValue('custrecord_2663_file_processed');	
	}
	
	function getLastProcess() {
		return record.getFieldValue('custrecord_2663_last_process');	
	}
	
	function getParentDeployment() {
		return record.getFieldValue('custrecord_2663_parent_deployment');	
	}
	
	function getPriority() {
		return record.getFieldValue('custrecord_ep_priority');	
	}
	
	function getProcessDate() {
		return record.getFieldValue('custrecord_2663_process_date');	
	}
	
	function getPostingPeriod() {
		return record.getFieldValue('custrecord_2663_posting_period');	
	}
	
	function getDueBatchDetails() {
		var value = record.getFieldValues('custrecord_2663_due_batch_details');
		if (value && (typeof value == 'string')) {
			value = [value];
		}
		return value;	
	}
	
	function addTransactions(tranKeys, tranAmts, tranEntities) {
		var added = 0;
		if (tranKeys && tranKeys.length > 0 && tranAmts && tranAmts.length > 0 && tranEntities && tranEntities.length > 0) {
			var addedAmount = 0;
			for (var i = 0, ii = tranKeys.length; i < ii; i++) {
				thisObj.addTransaction(tranKeys[i], tranAmts[i], tranEntities[i]);
				addedAmount += tranAmts[i] * 1;
				added++;
			}
			
			thisObj.setTotalTransactions(thisObj.getTotalTransactions() + tranKeys.length);
			thisObj.setTotalAmount(nlapiFormatCurrency(thisObj.getTotalAmount() + addedAmount));	
		}
		
		return added;
	}
	
	function addTransaction(tranKey, tranAmt, tranEntity, autoBatch) {
		var arrTranKey = tranKey.split('-');
		var tranId = arrTranKey[0];
		var tranLine = arrTranKey[1] * 1;
		
		batchTransaction.create();
		batchTransaction.setBatch(record.getId());
		if (autoBatch) {
			batchTransaction.setAutoBatch(record.getId());
		}
		batchTransaction.setReferenceBill(tranId);
		batchTransaction.setReferenceLine(tranLine);
		batchTransaction.setAmount(tranAmt);
		batchTransaction.setEntity(tranEntity);
		batchTransaction.submit();
	}

	function removeTransaction(id) {
		nlapiDeleteRecord(batchTranRecType, id);
	}
	
	function removeTransactions(tranKeys) {
		var removed = 0;
		
		if (tranKeys && tranKeys.length > 0) {
			var tranIds = thisObj.getTranIds(tranKeys);
			var batchTranKeys = thisObj.getTransactionKeys();
			var batchTranAmts = thisObj.getTransactionAmounts();
			var batchTranEntities = thisObj.getTransactionEntities();
			var batchJournalKeys = thisObj.getJournalKeys();
			var batchPartiallyPaidKeys = thisObj.getPartiallyPaidKeys();
			var batchTransactionSearch = new _2663.Search(batchTranRecType);
			batchTransactionSearch.addFilter('custrecord_2663_eft_file_id', null, 'is', record.getId());
			batchTransactionSearch.addFilter('custrecord_2663_parent_bill', null, 'anyof', tranIds);
			batchTransactionSearch.addColumn('custrecord_2663_parent_bill');
			batchTransactionSearch.addColumn('custrecord_2663_reference_line');
			var results = batchTransactionSearch.getAllResults(null, null, null, true);
			for (var i = 0, ii = results.length; i < ii; i++) {
				var res = results[i];
				var tranKey = [res.getValue('custrecord_2663_parent_bill'), res.getValue('custrecord_2663_reference_line')].join('-');
				if (tranKeys.indexOf(tranKey) > -1) {
					thisObj.removeTransaction(res.getId());
					var tranKeyIndex = batchTranKeys.indexOf(tranKey);
					if (tranKeyIndex > -1) {
						batchTranKeys.splice(tranKeyIndex, 1);
						batchTranAmts.splice(tranKeyIndex, 1);
						batchTranEntities.splice(tranKeyIndex, 1);
					}
					
					var journalKeyIndex = batchJournalKeys.indexOf(tranKey);
					if (journalKeyIndex > -1) {
						batchJournalKeys.splice(journalKeyIndex, 1);
					}
					
					var partialKeyIndex = batchPartiallyPaidKeys.indexOf(tranKey);
					if (partialKeyIndex > -1) {
						batchPartiallyPaidKeys.splice(partialKeyIndex, 1);
					}
					
					removed++;	
				}
			}
			
			if (removed > 0) {
				var totalAmt = 0;
				for (var i = 0, ii = batchTranAmts.length; i < ii; i++) {
					totalAmt += batchTranAmts[i] * 1;
				}
				
				thisObj.setTotalAmount(nlapiFormatCurrency(totalAmt));
				thisObj.setTotalTransactions(batchTranKeys.length);
				thisObj.setTransactionKeys(batchTranKeys);
				thisObj.setTransactionAmounts(batchTranAmts);
				thisObj.setTransactionEntities(batchTranEntities);
				thisObj.setJournalKeys(batchJournalKeys);
				thisObj.setPartiallyPaidKeys(batchPartiallyPaidKeys);
				thisObj.submit();
			}
		}
		
		return removed;
	}
	
	function getTranIds(tranKeys) {
		var tranIds = [];
		if (tranKeys && tranKeys.length > 0) {
			for (var i = 0, ii = tranKeys.length; i < ii; i++) {
				var arrTranKey = tranKeys[i].split('-');
				var tranId = arrTranKey[0];
				if (tranIds.indexOf(tranId) < 0) {
					tranIds.push(tranId);
				}
			}	
		}
		return tranIds;
	}
	
	function getApprovers() {
		var bankAccount = getBankAccount();
		var approvalLevel = getApprovalLevel();
		var approvers = [];
		if (bankAccount && approvalLevel) {
			var approvalRoutingSearch = new _2663.Search('customrecord_2663_approval_routing');
	    	approvalRoutingSearch.addFilter('custrecord_2663_ar_bank_acct', null, 'is', bankAccount);
	    	approvalRoutingSearch.addFilter('custrecord_2663_ar_level', null, 'is', approvalLevel);
	    	approvalRoutingSearch.addColumn('custrecord_2663_ar_approver');
	    	
	    	var res = approvalRoutingSearch.getResults();
	    	if (res && res.length > 0) {
	    		var strApprovers = res[0].getValue('custrecord_2663_ar_approver');
	    		if (strApprovers) {
	    			approvers = strApprovers.split(',');	
	    		}
	    	}	
		}
		
		return approvers; 
	}
	
	function create() {
		record = nlapiCreateRecord(recordType);
	}
	
	function load(id) {
		record = nlapiLoadRecord(recordType, id);
	}
	
	function submit(doSourcing, ignoreMandatoryFields) {
		return nlapiSubmitRecord(record, doSourcing, ignoreMandatoryFields);
	}
	
	function deleteRecord(id) {
		nlapiDeleteRecord(recordType, id);
	}
	
	function submitField(id, fldName, val) {
		nlapiSubmitField(recordType, id, fldName, val);
	}
	
	// set public methods
	this.create = create;
	this.load = load;
	this.submit = submit;
	this.deleteRecord = deleteRecord;
	this.submitField = submitField;
	this.setName = setName;
	this.setSubsidiary = setSubsidiary;
	this.setBankAccount = setBankAccount;
	this.setAccount = setAccount;
	this.setDetail = setDetail;
	this.setPaymentType = setPaymentType;
	this.setTransactionKeys = setTransactionKeys;
	this.setTransactionAmounts = setTransactionAmounts;
	this.setTransactionEntities = setTransactionEntities;
	this.setJournalKeys = setJournalKeys;
	this.setPartiallyPaidKeys = setPartiallyPaidKeys;
	this.setTotalAmount = setTotalAmount;
	this.setTotalTransactions = setTotalTransactions;
	this.setStatus = setStatus;
	this.setNumber = setNumber;
	this.setTimeStamp = setTimeStamp;
	this.setApprovalLevel = setApprovalLevel;
	this.setPreviousApprovalLevel = setPreviousApprovalLevel;
	this.setFileProcessed = setFileProcessed;
	this.setLastProcess = setLastProcess;
	this.setParentDeployment = setParentDeployment;
	this.setPriority = setPriority;
	this.setProcessDate = setProcessDate;
	this.setPostingPeriod = setPostingPeriod;
	this.setDueBatchDetails = setDueBatchDetails;
	this.getRecordType = getRecordType;
	this.getId = getId;
	this.getName = getName;
	this.getSubsidiary = getSubsidiary;
	this.getBankAccount = getBankAccount;
	this.getAccount = getAccount;
	this.getDetail = getDetail;
	this.getPaymentType = getPaymentType;
	this.getTransactionKeys = getTransactionKeys;
	this.getTransactionAmounts = getTransactionAmounts;
	this.getTransactionEntities = getTransactionEntities;
	this.getJournalKeys = getJournalKeys;
	this.getPartiallyPaidKeys = getPartiallyPaidKeys;
	this.getTotalAmount = getTotalAmount;
	this.getTotalTransactions = getTotalTransactions;
	this.getStatus = getStatus;
	this.getNumber = getNumber;
	this.getTimeStamp = getTimeStamp;
	this.getRemovedKeys = getRemovedKeys;
	this.getApprovalLevel = getApprovalLevel;
	this.getPreviousApprovalLevel = getPreviousApprovalLevel;
	this.getTranIds = getTranIds;
	this.getApprovers = getApprovers;
	this.getOwner = getOwner;
	this.getFileProcessed = getFileProcessed;
	this.getLastProcess = getLastProcess;
	this.getParentDeployment = getParentDeployment;
	this.getPriority = getPriority;
	this.getProcessDate = getProcessDate;
	this.getPostingPeriod = getPostingPeriod;
	this.getDueBatchDetails = getDueBatchDetails;
	this.addTransaction = addTransaction;
	this.addTransactions = addTransactions;
	this.removeTransaction = removeTransaction;
	this.removeTransactions = removeTransactions;
};

_2663.PaymentBatchTransaction = function PaymentBatchTransaction() {
	var recordType = 'customrecord_2663_bill_eft_payment';
	var record = null;
	
	function setBatch(value) {
		record.setFieldValue('custrecord_2663_eft_file_id', value);
	}
	
	function setAutoBatch(value) {
		record.setFieldValue('custrecord_2663_parent_batch', value);
	}
	
	function setReferenceBill(value) {
		record.setFieldValue('custrecord_2663_parent_bill', value);
	}
	
	function setReferenceLine(value) {
		record.setFieldValue('custrecord_2663_reference_line', value);
	}
	
	function setPayment(value) {
		record.setFieldValue('custrecord_2663_parent_payment', value);
	}
	
	function setEntity(value) {
		record.setFieldValue('custrecord_2663_parent_entity', value);
	}
	
	function setAmount(value) {
		record.setFieldValue('custrecord_2663_eft_payment_amount', nlapiFormatCurrency(value));
	}
	
	function getRecordType() {
		return recordType;
	}
	
	function getId() {
		return record.getId();
	}
	
	function getBatch() {
		return record.getFieldValue('custrecord_2663_eft_file_id');
	}
	
	function getAutoBatch() {
		return record.getFieldValue('custrecord_2663_parent_batch');
	}
	
	function getReferenceBill() {
		return record.getFieldValue('custrecord_2663_parent_bill');
	}
	
	function getReferenceLine() {
		return record.getFieldValue('custrecord_2663_reference_line');
	}
	
	function getPayment() {
		return record.getFieldValue('custrecord_2663_parent_payment');
	}
	
	function getEntity() {
		return record.getFieldValue('custrecord_2663_parent_entity');
	}
	
	function getAmount() {
		return record.getFieldValue('custrecord_2663_eft_payment_amount');
	}
	
	function create() {
		record = nlapiCreateRecord(recordType);
	}
	
	function load() {
		record = nlapiLoadRecord(recordType);
	}
	
	function submit() {
		return nlapiSubmitRecord(record);
	}
	
	function deleteRecord(id) {
		nlapiDeleteRecord(recordType, id);
	}
	
	function submitField(id, fldName, val) {
		nlapiSubmitField(recordType, id, fldName, val);
	}
	
	this.create = create;
	this.load = load;
	this.submit = submit;
	this.deleteRecord = deleteRecord;
	this.submitField = submitField;
	this.setBatch = setBatch;
	this.setAutoBatch = setAutoBatch;
	this.setReferenceBill = setReferenceBill;
	this.setReferenceLine = setReferenceLine;
	this.setPayment = setPayment;
	this.setEntity = setEntity;
	this.setAmount = setAmount;
	this.getRecordType = getRecordType;
	this.getId = getId;
	this.getBatch = getBatch;
	this.getAutoBatch = getAutoBatch;
	this.getReferenceBill = getReferenceBill;
	this.getReferenceLine = getReferenceLine;
	this.getPayment = getPayment;
	this.getEntity = getEntity;
	this.getAmount = getAmount;
};

/**
 * @author alaurito
 */

/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2012/03/08  217041         GPM Beta - 1.19.2       Support edition control
 * 2012/04/18  218410         1.21                    Use list of native formats in source code
 *                                                    to get the allowed formats for an account
 *             219964         1.21                    Calls the Generic Payments Configuration
 *                                                    Loader suitelet to get the account country
 *                                                    and country code-country name map
 * 2012/04/23  220254         1.21.1                  Include custom formats in allowed format
 *                                                    list when license is valid (injection from
 *                                                    previous fix)
 * 2012/04/23  220350         1.21.1                  Pass language parameter to avoid error
 *                                                    in nlobjSearchFilter when user preferred
 *                                                    language is not English
 * 2012/06/25  224734         1.22.2                  Set license expiry up to 42 days
 * 2012/07/03  217328         1.20.1       			  Add CA CPA-005 format
 * 2012/07/05  223301  		  1.20.1       			  Add Equens - Clieop (ING Bank) format
 * 			   225752
 * 2012/07/17  225904         1.22.3       			  Add Raiffeisen Domestic Transfer format
 * 2012/07/24  225904         1.22.3       			  Add Hungary in _2663.AllowedFormatsByEdition
 * 2012/08/01  225006         1.22.3       			  Add BACS - Bank of Scotland format
 * 2012/08/01  225007         1.22.3       			  Add BACS - Albany ALBACS-IP format
 * 2012/09/07  230558		  1.22.3       			  Add SEPA Credit Transfer (Austria) format
 * 2012/11/26  237873         2.00.3                  Filter for multi-currency formats based on license 
 *                                                    and multi-currency settings.
 * 2013/01/04  238105		  2.00.5				  Add ABBL VIR 2000 format                                                   
 * 2013/01/04  239423  		  2.00.6				  Allow multicurrency formats if there's no license
 * 													  and add LU in _2663.AllowedFormatsByEdition
 * 2013/02/13  240170 		  2.00.7				  Add support for SEPA Credit Transfer (Netherlands)	
 * 2012/02/15  243362 		  2.00.8				  Add support for SEPA version pain 001.001.002		
 * 2013/03/01  244881 		  2.00.10	  			  Add support for ANZ EFT	
 * 2013/03/11  235816		  2.00.3				  Add support for ASB EFT 				
 * 2013/03/27  242897		  2.00.3				  Add support for BACS - Bank of Ireland
 * 2013/04/24  235777		  2.00.2				  Add support for ACH-PPD
 * 2013/05/03  249771		  1.01.2012.11.29.1		  Enable TSTDRV accounts to use OW features even without a license
 * 2013/05/03  248118 		  2.00.13				  Allow selection of Positive Pay formats even without a license
 * 2013/05/12  235362		  2.00.15				  Add support for BACSTEL-IP (EFT)
 * 2013/05/14  239906 		  2.00.12	 			  Added support for SVB - CDA (Positive Pay)
 * 2013/05/20  244071		  2.00.10				  Add support for CNAB 240	
 * 2013/07/23  254155         2.00.17				  Added support for SEPA Credit Transfer (ABN AMRO) 
 * 2013/07/23  235778 		  2.00.2			      Add support for BACS DD
 * 2013/08/01  242348 	      2.00.8				  Add support for Westpac-Deskbank EFT format
 * 2013/08/05  240583 	      1.01.2012.11.29.1		  Add support for SEPA Credit Transfer (Germany) format
 * 2013/08/22  219495 		  1.22.1.2012.05.10.1     Add support for J.P. Morgan Freeform GMT
 * 2013/08/26  235194		  2.00.3				  Add support for PNC ActivePay	
 * 2013/09/23  255091		  2.00.18				  Add support for SEPA Direct Debit (Germany)
 * 2013/10/17  266518		  3.00	  				  Add support for ABA (DD)
 * 2013/12/12  256853 		  3.00.00     			  Add support for SEPA Credit Transfer (CBI)
 * 2013/12/12  256855 		  3.00.00     			  Add support for SEPA Direct Debit (CBI)
 * 2013/12/17  263344	      3.01.1.2013.12.24.1     Add support for SEPA Credit Transfer (Luxembourg)
 * 2013/12/18  261618 		  3.01.1     			  Add support for SEPA Credit Transfer (Bank of Ireland)
 * 2013/12/23  240676 		  3.01.1     			  Add support for SEPA Credit Transfer (Belgium)
 * 2013/12/26  240671 		  3.01.5     			  Add support for SEPA Credit Transfer (France)
 * 2014/03/14  236313 		       			          Add support for ABO format
 * 2014/03/18  244069 		  3.01.1.2014.03.18.3     Add support for DTAZV format
 * 2014/03/20  254510 		  3.01.1.2014.03.25.3     Add support for JP Morgan GIRO format
 * 2014/03/21  229156 		       			          Add support for HSBC ISO 20022 (Singapore) format
 */

var _2663;

if (!_2663) 
    _2663 = {};

_2663.AllowedFormatsByEdition = {
    'US' : ['US', 'CA'],
    'AT' : ['AT'],
    'AU' : ['AU', 'NZ'],
    'CA' : ['CA', 'US'],
    'JP' : ['JP'],
    'GB' : ['GB'],
    'BE' : ['BE'],
    'FR' : ['FR'],
    'DE' : ['DE'],
    'ES' : ['ES'],
    'IT' : ['IT'],
    'NL' : ['NL'],
    'ZA' : ['ZA'],
    'SG' : ['SG'],
    'NZ' : ['NZ', 'AU'],
    'HU' : ['HU'],
    'LU' : ['LU'],
    'IE' : ['IE'],
    'BR' : ['BR'],
    'BE' : ['BE']
};

_2663.PaymentFileTypes = {
    'EFT' : '1',
    'DD' : '2',
    'Positive Pay' : '3'
};

_2663.NativeFormats = [
    { 
        name: 'ABA', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'ABA', 
        type: _2663.PaymentFileTypes['DD'] 
    },
    { 
        name: 'ABBL VIR 2000', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'ABO', 
        type: _2663.PaymentFileTypes['EFT'] 
    },        
    { 
        name: 'ACH - CCD/PPD', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'ACH - CTX (Free Text)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'ACH - PPD', 
        type: _2663.PaymentFileTypes['DD'] 
    },
    { 
        name: 'AEB - Norma 34', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'ANZ', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'ASB', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'BACS', 
        type: [_2663.PaymentFileTypes['EFT'], , _2663.PaymentFileTypes['DD']] 
    },
    { 
        name: 'BACS - Bank of Ireland', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'BACS - Bank of Scotland', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'BACS - Albany ALBACS-IP', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'BACSTEL-IP', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'BNZ', 
        type: _2663.PaymentFileTypes['EFT']
    },
    { 
        name: 'BoA/ML', 
        type: _2663.PaymentFileTypes['Positive Pay'] 
    },
    { 
        name: 'CPA-005', 
        type: _2663.PaymentFileTypes['EFT']
    },
    { 
        name: 'CBI Collections', 
        type: _2663.PaymentFileTypes['DD'] 
    },
    { 
        name: 'CBI Payments', 
        type: _2663.PaymentFileTypes['EFT']
    },
    { 
        name: 'CFONB', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'CIRI-FBF', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'CNAB 240', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'DBS - IDEAL', 
        type: [_2663.PaymentFileTypes['EFT'], _2663.PaymentFileTypes['DD']] 
    },
    { 
        name: 'DTAUS', 
        type: [_2663.PaymentFileTypes['EFT'], _2663.PaymentFileTypes['DD']] 
    },
    { 
        name: 'DTAZV', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'Equens - Clieop', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'Equens - Clieop (ING Bank)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'HSBC ISO 20022 (Singapore)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },    
    {
    	name: 'Interbank GIRO (JP Morgan)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    {
    	name: 'J.P. Morgan Freeform GMT', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    {
    	name: 'PNC ActivePay', 
    	type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'Raiffeisen Domestic Transfer', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'RBC', 
        type: ['Positive Pay'] 
    },
    { 
        name: 'SEPA Credit Transfer (Austria)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA Credit Transfer (Belgium)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA Credit Transfer (France)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA (Austria) Pain.001.001.02', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA Credit Transfer (ABN AMRO)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA Credit Transfer (Germany)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    {
        name: 'SEPA Credit Transfer (Luxembourg)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA Credit Transfer (Netherlands)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA Direct Debit (Germany)', 
        type: _2663.PaymentFileTypes['DD'] 
    },
    { 
        name: 'SEPA Credit Transfer (CBI)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SEPA Direct Debit (CBI)', 
        type: _2663.PaymentFileTypes['DD'] 
    },
    { 
        name: 'SEPA Credit Transfer (Bank of Ireland)', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'Standard Bank', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'SVB - CDA', 
        type: _2663.PaymentFileTypes['Positive Pay'] 
    },
    { 
        name: 'UoB - BIB-IBG', 
        type: [_2663.PaymentFileTypes['EFT'], _2663.PaymentFileTypes['DD']] 
    },
    { 
        name: 'Westpac - Deskbank', 
        type: _2663.PaymentFileTypes['EFT'] 
    },
    { 
        name: 'Zengin', 
        type: _2663.PaymentFileTypes['EFT'] 
    }
];


_2663.EditionControl = function() {
    function isLicensed() {
        var result = true;
        
        //special case for TSTDRV accounts
        if (/^TSTDRV/.test(nlapiGetContext().getCompany())) {
        	return result;
        }
        
        // get license parameters
        var licenseParams = (new _2663.LicenseFields()).GetLatestLicenseFieldsParameters();
        nlapiLogExecution('debug', '[ep] EditionControl:isLicensed', 'last updated: ' + licenseParams.timestamp);
        
        var licenseValidator = new _2663.LicenseValidator(); 
        var isValidLicense = licenseValidator.CheckValidLicense(licenseParams);
        var isWithinPeriod = licenseValidator.CheckLicenseWithinPeriod(licenseParams);
        // if license is valid
        if (isValidLicense == false) {
            nlapiLogExecution('error', '[ep] EditionControl:isLicensed', 'The license key that you have provided is not valid. Please re-install the Electronic Payments License Update to update your license.');
            result = false;
        }
        else if (isWithinPeriod == false) {
            nlapiLogExecution('error', '[ep] EditionControl:isLicensed', 'Your license has expired. Please re-install the Electronic Payments License Update to update your license.');
            result = false;
        }

        return result;
    }
    
    this.IsLicensed = isLicensed;
};

_2663.LicenseFields = function() {
    function update(licenseParams) {
        var id;
        if (licenseParams && licenseParams.timestamp && licenseParams.licensekey) {
            try {
                var existingLicenseParams = getLatestLicenseFieldsParameters();
                id = existingLicenseParams.id;
                if (id) {
                    nlapiLogExecution('debug', '[ep] LicenseFields:update', 'existing preference internal id: ' + id);
                    // update fields
                    nlapiSubmitField('customrecord_ep_preference', id, ['custrecord_ep_last_updated', 'custrecord_ep_license_key'], [licenseParams.timestamp, licenseParams.licensekey], false);
                } 
                else {
                    // create if does not exist
                    var licenseRecord = nlapiCreateRecord('customrecord_ep_preference', {recordmode: 'dynamic'});
                    licenseRecord.setFieldValue('custrecord_ep_last_updated', licenseParams.timestamp);
                    licenseRecord.setFieldValue('custrecord_ep_license_key', licenseParams.licensekey);
                    id = nlapiSubmitRecord(licenseRecord);
                }
                nlapiLogExecution('debug', '[ep] LicenseFields:update', 'new preference internal id: ' + id);
            }
            catch(ex) {
                nlapiLogExecution('error', '[ep] LicenseFields:update', 'An error occurred while updating the license fields: ' + ex.getCode() + '\n' + ex.getDetails());
                throw nlapiCreateError('IPM_LICENSE_UPDATE_ERROR', 'An error occurred while updating the license fields: ' + ex.getCode() + '\n' + ex.getDetails(), true);
            }
        }
        return id;
    }
    
    function getLatestLicenseFieldsParameters() {
        var licenseParams = {};
        var columns = [];
        columns.push(new nlobjSearchColumn('custrecord_ep_last_updated').setSort(true));
        columns.push(new nlobjSearchColumn('custrecord_ep_license_key'));
        var searchRes = nlapiSearchRecord('customrecord_ep_preference', null, null, columns);
        if (searchRes) {
            licenseParams.id = searchRes[0].getId();
            licenseParams.timestamp = searchRes[0].getValue('custrecord_ep_last_updated');
            licenseParams.licensekey = searchRes[0].getValue('custrecord_ep_license_key');
        }
        return licenseParams;
    }
    
    this.Update = update;
    this.GetLatestLicenseFieldsParameters = getLatestLicenseFieldsParameters;
};

_2663.LicenseValidator = function() {
    function checkValidLicense(licenseParams) {
        var result = true;
        if (licenseParams && licenseParams.timestamp && licenseParams.licensekey) {
            // check if valid license
            nlapiLogExecution('debug', '[ep] LicenseValidator:checkValidLicense', 'timestamp: ' + licenseParams.timestamp + ', licensekey: ' + licenseParams.licensekey);
            var expectedLicenseKey = (new _2663.LicenseKeyCreatorEP()).BuildLicenseKey(licenseParams.timestamp);
//            nlapiLogExecution('debug', '[ep] LicenseValidator:checkValidLicense', 'expected licensekey: ' + expectedLicenseKey);
            if (expectedLicenseKey != licenseParams.licensekey) {
                result = false;
            }
        }
        else {
            result = false;
        }
        return result;
    }
    
    function checkLicenseWithinPeriod(licenseParams) {
        var result = true;
        if (licenseParams && licenseParams.timestamp && licenseParams.licensekey) {
            nlapiLogExecution('debug', '[ep] LicenseValidator:checkLicenseWithinPeriod', 'timestamp: ' + licenseParams.timestamp + ', licensekey: ' + licenseParams.licensekey);
            // check if date is within 42 days
            var currDateMs = (new Date()).getTime();
            var timestampDate = parseDateFromPlaintext(licenseParams.timestamp);
            if (timestampDate) {
                nlapiLogExecution('debug', '[ep] LicenseValidator:checkLicenseWithinPeriod', 'parsed date: ' + timestampDate);
                var timestampMs = timestampDate.getTime();
                var maxAllowedDaysMs = 42 * 24 * 60 * 60 * 1000; // 42 days * 24 hours + 60 mins + 60 secs + 1000 ms
                var dayDiff = (currDateMs - timestampMs);
                nlapiLogExecution('debug', '[ep] LicenseValidator:checkLicenseWithinPeriod', 'currDateMs: ' + currDateMs + '<br>' + 'timestampMs: ' + timestampMs);
                nlapiLogExecution('debug', '[ep] LicenseValidator:checkLicenseWithinPeriod', 'day diff: ' + dayDiff + ', max allowed: ' + maxAllowedDaysMs);
                if (dayDiff > maxAllowedDaysMs) {
                    result = false;
                }
            }
            else {
                result = false;
            }
        }
        else {
            result = false;
        }
        return result;
    }
    
    function parseDateFromPlaintext(timestamp) {
        var date;
        if (timestamp && timestamp.length == 12) {
            timestamp = new String(timestamp);
            var year = timestamp.substring(0, 4);
            var month = parseInt(timestamp.substring(4, 6), 10) - 1;
            var date = timestamp.substring(6, 8);
            date = new Date(year, month, date, 23, 59); // consider midnight as end of day
        }
        return date;
    }
    
    this.CheckValidLicense = checkValidLicense;
    this.CheckLicenseWithinPeriod = checkLicenseWithinPeriod;
};

_2663.LicenseKeyCreatorEP = function() {
    function buildPlainTextDate(date) {
        var plainTextDate;
        if (date) {
            var yearStr = new String(date.getFullYear());
            var monthStr = new String(date.getMonth() + 1);
            var dateStr = new String(date.getDate());
            var hourStr = new String(date.getHours());
            var minuteStr = new String(date.getMinutes());
            monthStr = monthStr.length == 2 ? monthStr : '0' + monthStr;
            dateStr = dateStr.length == 2 ? dateStr : '0' + dateStr;
            hourStr = hourStr.length == 2 ? hourStr : '0' + hourStr ;
            minuteStr = minuteStr.length == 2 ? minuteStr : '0' + minuteStr ;
            plainTextDate = yearStr + monthStr + dateStr + hourStr + minuteStr;
        }
        nlapiLogExecution('debug', '[ep] LicenseKeyCreatorEP:buildTimestampInPlainText', plainTextDate);
        return plainTextDate;
    }
    
    function buildLicenseKey(timestamp) {
        var licenseKey;
        if (timestamp) {
            var cipherText = timestamp + nlapiGetContext().getCompany();
            licenseKey = nlapiEncrypt(cipherText, 'aes', getSharedKey());
        }
//        nlapiLogExecution('debug', '[ep] LicenseKeyCreatorEP:buildLicenseKey', licenseKey);
        return licenseKey;
    }

    function getSharedKey() {
        var key = nlapiEncrypt('e' + nlapiGetContext().getCompany() + 'p', 'aes');
        return key;
    }

    this.BuildPlainTextDate = buildPlainTextDate;
    this.BuildLicenseKey = buildLicenseKey;
};

_2663.FormatRestrictor = function() {
    /**
     * Returns whether a given format record is editable based on the license availability.
     * Format type:
     * - Positive Pay - return all regardless of license
     * - EFT and DD - return only those allowed for edition
     * 
     * @param formatRecord
     * @returns {Boolean}
     */
    function isEditableFormat(formatRecord) {
        var result = false;
        var isLicensed = (new _2663.EditionControl()).IsLicensed();
        nlapiLogExecution('debug', '[ep] FormatRestrictor:isEditableFormat', 'licensed: ' + isLicensed);
        if (formatRecord) {
            var type = formatRecord.getFieldText('custrecord_2663_payment_file_type');
            if (type == 'Positive Pay' || isLicensed == true) {
                result = true;
            }
        }
        return result;
    }
    
    /**
     * Returns the list of allowed formats based on format type and license availability.
     * Format type:
     * - Positive Pay - return all regardless of license
     * - EFT and DD - return only those allowed for edition if license is not available
     * 
     * When license is available
     * 
     * @param formatType
     * @returns {Array}
     */
    function getAllowedFormats(formatType) {
        var isLicensed = (new _2663.EditionControl()).IsLicensed();
        var allowedFormats = {}; // to contain list of file format id-name pairs
        
        if (formatType) {
            var formatTypes = {
                'EFT' : '1',
                'DD' : '2',
                'Positive Pay' : '3',
            };
            
            var filters = [];
            
            // set the flag to search to true
            // this will be set to false if there's no license/not positive pay and no formats are assigned to the account's edition
            var searchFlag = true;
            
            filters.push(new nlobjSearchFilter('custrecord_2663_payment_file_type', null, 'anyof', formatTypes[formatType]));
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            nlapiLogExecution('debug', '[ep] FormatRestrictor:getAllowedFormats', 'isLicensed: ' + isLicensed);
            if (formatType != 'Positive Pay' && isLicensed == false) {
                // allow only native formats
                filters.push(new nlobjSearchFilter('custrecord_2663_native_format', null, 'is', 'T'));
                
                // filter by edition
                var configObject = getConfigurationObject();
                var editionValue = configObject.companyinformation.country;
                var allowedCountryFormats = _2663.AllowedFormatsByEdition[editionValue];
                
                nlapiLogExecution('debug', '[ep] FormatRestrictor:getAllowedFormats', 'edition: value-' + editionValue);

                if (allowedCountryFormats) {
                    // --- workaround for country field values ---
                    // get countryIdMap from company info and custom field
                    // match based on the country text in order to get the country id's to used in filter
                    var companyInfoCountryIdMap = configObject.countryIdMap;
                    var customFieldCountryIdMap = createCountryIdMapFromCustomField();
                    var countryIds = [];
                    for (var i = 0; i < allowedCountryFormats.length; i++) {
                        var countryName = companyInfoCountryIdMap[allowedCountryFormats[i]];
                        nlapiLogExecution('debug', '[ep] FormatRestrictor:getAllowedFormats', 'allowed country: value-' + allowedCountryFormats[i] + ',text-' + countryName);
                        var countryId = customFieldCountryIdMap[countryName];
                        nlapiLogExecution('debug', '[ep] FormatRestrictor:getAllowedFormats', 'allowed country: id-' + countryId);
                        countryIds.push(countryId);
                    }
                    filters.push(new nlobjSearchFilter('custrecord_2663_format_country', null, 'anyof', countryIds));
                }
                else {
                    nlapiLogExecution('debug', '[ep] FormatRestrictor:getAllowedFormats', 'No formats for edition: ' + editionValue);
                    searchFlag = false;
                }
            }
            
            var columns = [];
            columns.push(new nlobjSearchColumn('name').setSort());
            
            if (searchFlag == true) {
                var searchResults = nlapiSearchRecord('customrecord_2663_payment_file_format', null, filters, columns);  // 20 points
                
                if (searchResults) {
                    // get all the format names to only include those formats in the source code
                    var nativeFormatNameList = getNativeFormatNames();
                    
                    for (var i = 0; i < searchResults.length; i++) {
                    	if (formatType == 'Positive Pay' || isLicensed == true || nativeFormatNameList.indexOf(searchResults[i].getValue('name')) != -1) {
                            allowedFormats[searchResults[i].getId()] = searchResults[i].getValue('name');
                        }
                        else {
                            nlapiLogExecution('debug', '[ep] FormatRestrictor:getAllowedFormats', 'Invalid native format: ' + searchResults[i].getValue('name'));
                        }
                    }
                }
            }
        }
        
        return allowedFormats;
    }
    
    /**
     * Returns map of country id's with their text values based on the custom country field.
     * Country Id is in numeric format.
     * Map: key = country name, value = country id
     * 
     * @returns {___anonymous5238_5239}
     */
    function createCountryIdMapFromCustomField() {
        var countryMap = {};
        
        var dummyFormat = nlapiCreateRecord('customrecord_2663_payment_file_format', {recordmode: 'dynamic'});  // 2 points
        var fld = dummyFormat.getField('custrecord_2663_format_country');
        var countries = fld.getSelectOptions();

        if (countries) {
            for (var i = 0; i < countries.length; i++) {
                countryMap[countries[i].text] = countries[i].id;
            }
        }

        return countryMap;
    }

    /**
     * Returns a list of the native formats as listed in the source code (prevents usage of imported formats) 
     * @returns {Array}
     */
    function getNativeFormatNames() {
        var nativeFormatNameList = []; 
        for (var i = 0; i < _2663.NativeFormats.length; i++) {
            nativeFormatNameList.push(_2663.NativeFormats[i].name);
        }
        return nativeFormatNameList;
    }
    
    /**
     * Wraps the call to the nlapiLoadConfiguration object and returns the result account country and country map
     * @returns {___anonymous17253_17254}
     */
    function getConfigurationObject() {
        var companyInfoConfigurationObject = {};
        try {
            nlapiLogExecution('debug', '[ep] FormatRestrictor:getConfigurationObject', 'Retrieving configuration object');
            var adminFieldsUrl = nlapiResolveURL('SUITELET', 'customscript_ep_admin_data_loader_s', 'customdeploy_ep_admin_data_loader_s', true);
            var params = {};
            nlapiLogExecution('debug', '[ep] FormatRestrictor:getConfigurationObject', 'User language: ' + nlapiGetContext().getPreference('LANGUAGE'));
            params['custparam_ep_language'] = nlapiGetContext().getPreference('LANGUAGE');
            var adminFields = nlapiRequestURL(adminFieldsUrl, params);
            var adminFieldsResult = adminFields.getBody();
            companyInfoConfigurationObject = JSON.parse(adminFieldsResult);
        }
        catch(ex) {
            nlapiLogExecution('error', '[ep] FormatRestrictor:getConfigurationObject', 'Error occurred while loading the configuration: ' + ex.getCode() + '\n' + ex.getDetails());
        }
        return companyInfoConfigurationObject;
    }
    

    this.IsEditableFormat = isEditableFormat;
    this.GetAllowedFormats = getAllowedFormats;
};
/**
 * renamed from eft_lib.js
 */

/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2011/05/20  198389         1.00.2011.05.20.04      Add JSON processing
 * 2011/07/26  202307         1.05                    Remove replace of < and > symbols when
 *                                                    reading XML file
 * 2011/08/12  203530         1.07.2011.08.11.01      Fix for 1000 record limit
 * 2011/08/17  203853         1.07.2011.08.18.02      Parallel runs for payment scheduled script
 * 2011/08/25  204379         1.08.2011.08.25.02      Support for large volume processing
 * 2011/09/15  203845         1.07.2011.08.18.02      Search with markers to allow filter to reach
 *                                                    entities after the 1000 record index
 *             205014         1.10                    Set flags for making DCL fields mandatory
 * 2011/10/27  208503         1.15.2011.10.27.4       Add US Market Requirements - AP (ACH - CCD/PPD)
 * 2011/12/15  210788         1.17.2011.12.08.3       Modify searchRecordWithMarker to check for
 *                                                    API governance limits
 * 2012/02/20  215424         1.19.1                  Workaround for large data performance issue of
 *                                                    nlapiSearchRecord
 * 2012/03/30  218911         1.20                    Add Currency class
 * 2012/04/23  220361         1.21                    Add xml utility functions for XML Output support
 * 2012/07/05  225630         1.22                    Remove setting of location as mandatory when
 *                                                    Multi-location inventory is enabled
 * 2012/08/23  229386         1.22.3       			  Check for 'null' string values for XML attributes 
 * 													  encoding and standalone (optional attributes)		                                                    
 * 2012/08/10  227867         2.01.1                  Add support for DCL according to aggregation
 * 2012/09/12  230808		  1.22.3       			  Add VALID_COUNTRY_CODES
 * 2012/12/13  237873         2.00.3                  Use nlapiLoadRecord to get root currency
 * 2013/02/28  243925         2.00                    Update DCL utility functions to check if DCL is activated                         
 * 2013/03/04  244623         2.00                    Update DCL utility functions to check if DCL is activated
 * 2013/03/08  235816   	  2.00.3				  Add support for ASB EFT
 * 2013/03/12  239628 		  2.00.6				  Add bank account types for ACH file format
 * 2013/05/17  251637 		  2.00.15				  Add constant TERMINAL_DEPLOYMENT_STATUSES
 * 2013/05/30  252743 		  2.00.16				  Add constant REQUEUED	
 * 			   247323  
 * 2013/06/17  254204 		  2.00.18				  Add EP Process constants
 * 2013/07/18  257505 		  2.00.18				  Add error logging when governance or time limit is reached
 * 2013/07/15  245723		  2.00.10				  Add method for checking if commission is enabled
 * 2013/08/01  242348 		  2.00.8				  Changed ASB utility class to NZAccountNumber
 * 2013/08/29  250004   	  2.00.24				  Removed unused constants
 * 2013/08/29  261772 		  2.00.25.2013.08.29.3    Add method for checking for employee commission feature
 * 2013/09/23  255091		  2.00.18				  Add method to get an xml child node using an attribute
 * 2013/10/04  265150		  3.00.0.2013.10.03.6	  Add support for hiding fields which contain empty values 
 * 2013/10/11  265898		  3.00.0.2013.10.10.6	  Remove extra space and lines from removeTag and getChildNodeByAttribute
 */


// Payment Status's
var PAYQUEUED = 1;
var PAYMARKPAYMENTS = 5;
var PAYPROCESSING = 2;
var PAYCREATINGFILE = 3;
var PAYPROCESSED = 4;
var PAYREVERSAL = 6;
var PAYNOTIFICATION = 7;
var PAYERROR = 8;
var PAYFAILED = 9;
var PAYCANCELLED = 10;
var PAYDELETINGFILE = 11;
var REQUEUED = 12;

var VALID_COUNTRY_CODES = [
	"AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE",
	"BZ","BJ","BM","BT","BO","BQ","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","IC","CV","KY","CF",
	"EA","TD","CL","CN","CX","CC","CO","KM","CD","CG","CK","CR","CI","HR","CU","CW","CY","CZ","DK","DJ","DM","DO",
	"TP","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI",
	"GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE",
	"IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT",
	"LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA",
	"MZ","MM","NA","NR","NP","NL","AN","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG",
	"PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","KN","LC","MF","VC","WS","SM","ST","SA",
	"SN","RS","CS","SC","SL","SG","SX","SK","SI","SB","SO","ZA","GS","SS","ES","LK","PM","SD","SR","SJ","SZ","SE",
	"CH","SY","TW","TJ","TZ","TH","TG","TK","TO","TT","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UY","UM",
	"UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW"
];

var VALID_SCHEDULE_STATUSES = ['QUEUED', 'INQUEUE', 'INPROGRESS'];
var TERMINAL_DEPLOYMENT_STATUSES = ['COMPLETED', 'NOTSCHEDULED'];

// EP Processes
var EP_PROCESSPAYMENTS = '1';
var EP_REPROCESS = '2';
var EP_ROLLBACK = '3';
var EP_REVERSEPAYMENTS = '4';
var EP_EMAILNOTIFICATION = '5';
var EP_CREATEFILE = '6';

//bank account types
var BANK_ACCT_TYPE_CHECKING = 1;
var BANK_ACCT_TYPE_SAVINGS = 2;

/**
 * Is Null or Empty.
 *
 * @param {Object} strVal
 */
function isNullorEmpty(strVal){
    return (strVal == null || strVal == '');
}


/**
 * Establish whether Class is activated.
 */
function isClass(){
    return (nlapiGetContext().getSetting('FEATURE', 'CLASSES') == 'T');
}


/**
 * Establish whether Department is activated.
 */
function isDepartment(){
    return (nlapiGetContext().getSetting('FEATURE', 'DEPARTMENTS') == 'T');
}


/**
 * Establish whether Location is activated.
 */
function isLocation(){
    return (nlapiGetContext().getSetting('FEATURE', 'LOCATIONS') == 'T');
}

/**
 * Establish whether Class field is mandatory. 
 */
function isClassMandatory() {
	var context = nlapiGetContext();
    return (context.getSetting('FEATURE', 'CLASSES') == 'T' && context.getPreference('CLASSMANDATORY') == 'T');
}

/**
 * Establish whether Department field is mandatory. 
 */
function isDeptMandatory() {
	var context = nlapiGetContext();
    return (context.getSetting('FEATURE', 'DEPARTMENTS') == 'T' && context.getPreference('DEPTMANDATORY') == 'T');	
}

/**
 * Establish whether Location field is mandatory. 
 */
function isLocMandatory() {
	var context = nlapiGetContext();
    return (context.getSetting('FEATURE', 'LOCATIONS') == 'T' && context.getPreference('LOCMANDATORY') == 'T');	
}

/**
 * Establish whether Class field is per transaction line
 * @returns {Boolean}
 */
function isClassPerLine() {
	var context = nlapiGetContext();
    return (context.getSetting('FEATURE', 'CLASSES') == 'T' && context.getPreference('CLASSESPERLINE') == 'T');
}

/**
 * Establish whether Department field is per transaction line
 * @returns {Boolean}
 */
function isDeptPerLine() {
	var context = nlapiGetContext();
    return (context.getSetting('FEATURE', 'DEPARTMENTS') == 'T' && context.getPreference('DEPTSPERLINE') == 'T');
}

/**
 * Establish whether Location field is per transaction line
 * @returns {Boolean}
 */
function isLocPerLine() {
	var context = nlapiGetContext();
    return (context.getSetting('FEATURE', 'LOCATIONS') == 'T' && context.getPreference('LOCSPERLINE') == 'T');
}

/**
 * Establish whether Journal CDL fields are per transaction line
 * @returns {Boolean}
 */
function isJournalCDLPerLine() {
    return (nlapiGetContext().getPreference('CDLPERLINEONJE') == 'T');
}

/**
 * Returns all DCL options
 */
function getDCLSettings() {
    var dclSettings = {};
    
    dclSettings.deptField = {
		isDisplayed: isDepartment(),
		isMandatory: isDeptMandatory()
	};
    dclSettings.classField = {
		isDisplayed: isClass(),
		isMandatory: isClassMandatory()
	};
    dclSettings.locField = {
		isDisplayed: isLocation(),
		isMandatory: isLocMandatory()
	};
    
    return dclSettings;
}

/**
 * Establish whether the account is One World.
 */
function isOneWorld(){
    return (nlapiGetContext().getSetting('FEATURE', 'SUBSIDIARIES') == 'T');
}

/**
 * Establish whether the account is using multicurrency.
 */
function isMultiCurrency(){
    return (nlapiGetContext().getSetting('FEATURE', 'MULTICURRENCY') == 'T');
}

/**
 * Establish whether the account is using multiple scheduled script queues.
 */
function isMultipleQueue() {
	return (nlapiGetContext().getSetting('FEATURE', 'serversidemultiq') == 'T');
}

/**
 * Checks if val is member of a given array
 * 
 * @param {String} val
 * @param {Array} arr
 */
function isInArray(val, arr) {
	var inArray = false;
	if (val && arr) {
		for (var i =0, ii = arr.length; i < ii; i++) {
			if (arr[i] == val) {
				inArray = true;
				break;
			}
		}
	}
	return inArray;
}

/**
 * Establish whether governance has been reached.
 *
 * @param {Object} strRestartTrigger (in units)
 */
function governanceReached(strRestartTrigger){
    var lReschedule = false;
    var context = nlapiGetContext();
    var usageRemaining = context.getRemainingUsage();
    
    // Default the restart trigger to 100 units.
    strRestartTrigger = isNullorEmpty(strRestartTrigger) ? 100 : strRestartTrigger;
    if (usageRemaining < strRestartTrigger) {
        lReschedule = true;
        nlapiLogExecution('debug', 'Schedule Governance', 'Governance Reached. Units remaining  : ' + usageRemaining);
    }
    return lReschedule;
}

/**
 * Establish whether the account has enabled Partner Commissions.
 */
function isCommissionEnabled(){
    return (nlapiGetContext().getFeature('PARTNERCOMMISSIONS'));
}

/**
 * Establish whether the account has enabled Commissions for Employees.
 */
function isEmployeeCommissionEnabled(){
    return (nlapiGetContext().getFeature('COMMISSIONS'));
}

/**
 * Workaround for search record limit
 * 
 * @param {Object} recordType
 * @param {Object} savedSearch
 * @param {Object} filters
 * @param {Object} columns
 * @param {Object} line
 */
function searchRecord(recordType, savedSearch, filters, columns, line, maxResultLength, startTime, maxTime) {
    var limitSearchResults = [];
	if (recordType) {
		var markerObj = {};
		markerObj.lastIndex = 1;
        var searchResults = searchRecordWithMarker(recordType, savedSearch, filters, columns, line, markerObj, maxResultLength, startTime, maxTime);
		while (searchResults) {
			// concatenate to results to return
            limitSearchResults = limitSearchResults.concat(searchResults);
			
			// search with marker
            searchResults = searchRecordWithMarker(recordType, savedSearch, filters, columns, line, markerObj, maxResultLength, startTime, maxTime);
		}
	}
    return limitSearchResults;
}

/**
 * Gets succeeding search results from the given internal id and line id (if line == true)
 * - Use the markerObj to store the state. For the initial search, set markerObj.lastIndex = 1.
 * - Calls to this function must handle the Script Execution Limit and Script API 
 *   Governance exceptions
 * 
 * @param {Object} recordType
 * @param {Object} savedSearch
 * @param {Object} filters
 * @param {Object} columns
 * @param {Object} line
 * @param {Object} markerObj
 */
function searchRecordWithMarker(recordType, savedSearch, filters, columns, line, markerObj, maxResultLength, startTime, maxTime) {
    var searchResults = [];

	if (governanceReached()) {
		nlapiLogExecution('error', 'searchRecordWithMarker', 'Governance reached.');
		return null;
	}

    if (startTime && maxTime) {
        var currentTime = (new Date()).getTime();
        var timeElapsed = currentTime - startTime;
        nlapiLogExecution('debug', 'Payment Module Processing', 'Time elapsed: ' + timeElapsed);
        if (timeElapsed >= maxTime) {
        	nlapiLogExecution('error', 'searchRecordWithMarker', 'Allowed search time exceeded.');
            return null;
        }
    }
	
    if (recordType && markerObj) {
        var lastIndex = 1;
        if (!columns) {
            columns = [];
        }
        columns.push(new nlobjSearchColumn('internalid').setSort());
        if (line) {
            columns.push(new nlobjSearchColumn('line').setSort());
        }
        var limitFilters = [];
        limitFilters = limitFilters.concat(filters);
        if (markerObj.lastInternalId) {
            limitFilters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthanorequalto', markerObj.lastInternalId));
            var formula = 'CASE WHEN {internalid} = ' + markerObj.lastInternalId;
            if (line) {
                formula += ' AND {line} <= ' + markerObj.lastLineNum;
            }
            formula += ' THEN 0 ELSE 1 END';
            limitFilters.push(new nlobjSearchFilter('formulanumeric', null, 'equalto', 1).setFormula(formula));
        }
        var tmpSearchResults = nlapiSearchRecord(recordType, savedSearch, limitFilters, columns);
        if (tmpSearchResults) {
            var maxLength = (maxResultLength != null && maxResultLength < tmpSearchResults.length) ? maxResultLength : tmpSearchResults.length;
            for (var i = 0; i < maxLength; i++) {
                searchResults.push(tmpSearchResults[i]);
            }
        }
        else {
            searchResults = tmpSearchResults;
        }

        if (searchResults) {
            nlapiLogExecution('debug', 'Payment Module Processing', 'searchWithMarker results: ' + searchResults.length);
            markerObj.lastIndex = searchResults.length - 1;
            markerObj.lastInternalId = searchResults[markerObj.lastIndex].getId();
            if (line) {
                markerObj.lastLineNum = searchResults[markerObj.lastIndex].getValue('line');
            }
        }
        else {
            markerObj.lastIndex = 0;
        }
    }
    return searchResults;
}

/**
 * Schedules the script on the available deployments
 * 
 * @param {Object} scriptId
 * @param {Object} deployments
 * @param {Object} params
 */
function scheduleScript(scriptId, deployments, params) {
    var rtnSchedule;
	
    if (scriptId && deployments && deployments.length > 0) {
        // schedule the processing
        for (var i = 0; i < deployments.length; i++) {
            rtnSchedule = nlapiScheduleScript('customscript_2663_payment_processing_ss', deployments[i], params);
            if (rtnSchedule == 'QUEUED') {
                nlapiLogExecution('debug', 'Payment Module Processing', 'Deployed on : ' + deployments[i]);
                break;
            }
        }
    }
    
    return rtnSchedule;
}

// -----------------------
// XML Util

var _2663;

if (!_2663) 
    _2663 = {};

_2663.XmlUtil = function(){
    function createTag(name, value){
        return '&lt;' + name + '&gt;' + value + '&lt;/' + name + '&gt;';
    }

    function removeTag(name, source){
        var preName = source.substring(0,source.indexOf('{' + name + '}'));
        var tag = source.substring(preName.lastIndexOf('<') + 1,preName.lastIndexOf('>'));
        var startIndex = source.indexOf(tag) - 1;
        var endIndex = source.lastIndexOf(tag) + tag.length;
        var part1 = source.substring(0, startIndex);
        var part2 = source.substring(endIndex + 1);
        return (part1.trim() + part2);        
    }
  
    function loadXmlObject(xmlStr){
        var xmlObj;
        //xmlStr = xmlStr.replace(/\&lt;/g, '<');
        //xmlStr = xmlStr.replace(/\&gt;/g, '>');
        if (xmlStr) {
            xmlObj = nlapiStringToXML(xmlStr);
        }
        return xmlObj;
    }
    
    function getAttributeValue(node, attributeName){
        var attrName = '@' + attributeName;
        var attr = new String(nlapiSelectNode(node, attrName));
        if (attr.indexOf('=') != -1) {
            attr = attr.substring(attr.indexOf('=') + 2, attr.length - 1);
        }
        return attr;
    }
    
    function getNodeValue(parentNode, nodeName){
        var nodeText = '';
        if (nodeName) {
            nodeText = nodeName + '/';
        }
        nodeText += 'text()';
        var nodeVal = new String(nlapiSelectNode(parentNode, nodeText));
        if (nodeVal.indexOf(':') != -1) {
            nodeVal = nodeVal.substring(nodeVal.indexOf(':') + 2, nodeVal.length - 1);
        }
        return nodeVal;
    }
    
    function removeComments(xmlStr){
    	var re = /<!--[\s\S]*?-->/g;
    	if (xmlStr) {
    		return xmlStr.replace(re, '');
    	}
    	return xmlStr;
    }
    
    function getChildNode(xmlStr, parentNodeName){
    	var re = new RegExp('<' + parentNodeName +'[\\s\\S]*?>');
    	var matchStr = (xmlStr.match(re) || [])[0]; 
    	var startIndex = xmlStr.indexOf(matchStr) + matchStr.length;
    	var endIndex = xmlStr.indexOf('</' + parentNodeName + '>');
    	return xmlStr.slice(startIndex, endIndex);
    }
    
    function getChildNodeByAttribute(xmlStr, parentNodeName, attributeName, attributeValue){
    	var re = new RegExp('<' + parentNodeName + '[\\s\\S]*?' + attributeName + '[\\s\\S]*?' + '=' + '[\\\'|\\\"]' + attributeValue + '[\\\'|\\\"]' + '[\\s\\S]*?' + '>');
    	
    	var matchStr = (xmlStr.match(re) || [])[0]; 
    	
    	var startIndex = xmlStr.indexOf(matchStr) + matchStr.length;
    	var endIndex = xmlStr.indexOf('</' + parentNodeName + '>', startIndex);
    	var res = xmlStr.slice(startIndex, endIndex);
    	return res.trim();
    }
    
    function getStringNode(xmlStr, elementName) {
    	var startIndex = xmlStr.indexOf('<' + elementName);
    	var endIndex = xmlStr.indexOf('</' + elementName + '>') + elementName.length + 3;
    	return startIndex > -1 ? xmlStr.slice(startIndex, endIndex) : '';
    }
    
    function createDeclaration(version, encoding, standalone) {
    	//sample declaration:	
    	//<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    	var declaration = '';
    	
    	if (version && version != 'null') {
    		declaration = declaration.concat('<?xml version="', version, '"',
				(encoding && encoding != 'null' ? ' encoding="' + encoding + '"' : ''),
				(standalone && (standalone == 'yes' || standalone == 'no') ? ' standalone="' + standalone + '"' : ''),
				'?>'
			);
    	} else {
    		throw nlapiCreateError('IPM_XML_VERSION_MANDATORY', 'The XML attribute version is mandatory.', true);
    	}
    	return declaration;
    }
	
	this.CreateTag = createTag;
	this.RemoveTag = removeTag;
	this.LoadXmlObject = loadXmlObject;
	this.GetAttributeValue = getAttributeValue;
	this.GetNodeValue = getNodeValue;
	this.RemoveComments = removeComments;
	this.GetChildNode = getChildNode; 
	this.GetStringNode = getStringNode;
	this.CreateDeclaration = createDeclaration;
	this.GetChildNodeByAttribute = getChildNodeByAttribute;
};

//-----------------------
//ACH Util
//-- used by ACH - CCD/PPD (EFT)
_2663.ACH = function(routingNumber){
	routingNumber = new String(routingNumber);
	
	this.isValidRoutingNumber = function() {
		var isValid = false;
		if (routingNumber && routingNumber.length == 9) {
			if (this.getCheckDigit() == routingNumber.charAt(8)) {
				isValid = true;
			}
		}
		return isValid;
	};
	
	this.getFederalReserveRoutingSymbol = function() {
		var federalReserveRoutingSymbol = '';
		if (routingNumber && routingNumber.length == 9 && !isNaN(routingNumber)) {
			federalReserveRoutingSymbol = routingNumber.substring(0, 4);
		}
		return federalReserveRoutingSymbol;
	};
	
	this.getABAInstitutionIdentifier = function() {
		var abaInstitutionIdentifier = '';
		if (routingNumber && routingNumber.length == 9 && !isNaN(routingNumber)) {
			abaInstitutionIdentifier = routingNumber.substring(4, 8);
		}
		return abaInstitutionIdentifier;
	};
	
	this.getCheckDigit = function() {
		var checkDigit = '';
		if (routingNumber && routingNumber.length == 9 && !isNaN(routingNumber)) {
			var checkDigitCompVal = (7 * (parseInt(routingNumber.charAt(0), 10) + parseInt(routingNumber.charAt(3), 10) + parseInt(routingNumber.charAt(6), 10)) + 
			3 * (parseInt(routingNumber.charAt(1), 10) + parseInt(routingNumber.charAt(4), 10) + parseInt(routingNumber.charAt(7), 10)) +
			9 * (parseInt(routingNumber.charAt(2), 10) + parseInt(routingNumber.charAt(5), 10))) % 10;
			checkDigit = new String(checkDigitCompVal);
		}
		return checkDigit;
	};
};

//Currency class
_2663.Currency = function() {
	var multiCurrencyOn = isMultiCurrency();
	var baseCurrencyId = '1';
	var currencies = {};
	
    // Only when Multiple Currencies is enabled are the ff. possible:
    // - nlapiSearchRecord('currency')
    // - nlapiLoadConfiguration('companyinformation').getFieldValue('basecurrency')
    if (multiCurrencyOn) {
        var columns = [new nlobjSearchColumn('symbol'), new nlobjSearchColumn('name')];
        (nlapiSearchRecord('currency', null, null, columns) || []).forEach(function(res){
        	if (!currencies[res.getId()]) {
        		currencies[res.getId()] = {};
        		currencies[res.getId()].symbol = res.getValue('symbol');
        		currencies[res.getId()].name = res.getValue('name');
        	}
        });
    } else {
    	var baseCurrency = nlapiLoadRecord('currency', baseCurrencyId);
    	if (!currencies[baseCurrencyId]) {
    		currencies[baseCurrencyId] = {};
    		currencies[baseCurrencyId].symbol = baseCurrency.getFieldValue('symbol');
    		currencies[baseCurrencyId].name = baseCurrency.getFieldValue('name');
    	}
    }
    
    //Defaults to currency based on locale (Company Information) if not multi-currency
    this.getValue = function(id, fldName) {
    	if (!multiCurrencyOn) {
    		id = baseCurrencyId;
        }
    	return currencies[id] && currencies[id][fldName];
    };
};

//-----------------------
//NZAccountNumber Util
//-- used by ASB (EFT), Westpac - Deskbank
_2663.NZAccountNumber = function(accountNumber){
	accountNumber = new String(accountNumber);
	
	var isNZAccountNumber = (accountNumber.length == 15 || accountNumber.length == 16);
	
	this.getBankNumber = function() {
		var bankNumber = '';
		if (accountNumber && isNZAccountNumber && !isNaN(accountNumber)) {
			bankNumber = accountNumber.substring(0, 2);
		}
		return bankNumber;
	};
	
	this.getBranchNumber = function() {
		var branchNumber = '';
		if (accountNumber && isNZAccountNumber && !isNaN(accountNumber)) {
			branchNumber = accountNumber.substring(2, 6);
		}
		return branchNumber;
	};
	
	this.getUniqueAccountNumber = function() {
		var uniqueAccountNumber = '';
		if (accountNumber && isNZAccountNumber && !isNaN(accountNumber)) {
			uniqueAccountNumber = accountNumber.substring(6, 13);
		}
		return uniqueAccountNumber;
	};
	
	this.getBankAccountSuffix = function() {
		var bankAccountSuffix = '';
		if (accountNumber && isNZAccountNumber && !isNaN(accountNumber)) {
			bankAccountSuffix = accountNumber.substring(13);
		}
		return bankAccountSuffix;
	};
};

//--------------------------

/*
    http://www.JSON.org/json2.js
    2011-01-18

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html


    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.


    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, strict: false, regexp: false */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

var JSON;
if (!JSON) {
    JSON = {};
}

(function () {
    "use strict";

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf()) ?
                this.getUTCFullYear()     + '-' +
                f(this.getUTCMonth() + 1) + '-' +
                f(this.getUTCDate())      + 'T' +
                f(this.getUTCHours())     + ':' +
                f(this.getUTCMinutes())   + ':' +
                f(this.getUTCSeconds())   + 'Z' : null;
        };

        String.prototype.toJSON      =
            Number.prototype.toJSON  =
            Boolean.prototype.toJSON = function (key) {
                return this.valueOf();
            };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string' ? c :
                '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0 ? '[]' : gap ?
                    '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' :
                    '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    k = rep[i];
                    if (typeof k === 'string') {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0 ? '{}' : gap ?
                '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' :
                '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ?
                    walk({'': j}, '') : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());

/**
 * @author alaurito
 *
 * include list : 2663_lib.js
 */

/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2011/08/11  203530         1.06.2011.07.29.03      Initial version - Fix for 1000 record limit 
 * 2011/08/15  203530         1.07.2011.08.11.01      Include journal entry lines with entity
 *                                                    (don't filter for entity = @NONE@)
 *             203636         1.07.2011.08.11.01      Check if array is empty before assigning as
 *                                                    filter; don't search for transactions when there
 *                                                    are no entities that match the EFT/DD format
 * 2011/08/17  203739         1.07.2011.08.18.02      Don't filter for entity = @NONE@ for 
 *                                                    journal lines with customer as entity
 *             203853                                 Parallel runs for payment scheduled script
 * 2011/08/25  204366         1.08.2011.08.25.02      Get entity text instead of company name to 
 *                                                    get correct string
 *             204379         1.08.2011.08.25.02      Support for large volume processing
 * 2011/08/31  204379         1.08                    Add pages to sublist - can select number of 
 *                                                    transactions per page; Divide selected lines
 *                                                    by pages and place them on different fields
 *                                                    to avoid field length error 
 * 2011/09/01  204697         1.09                    Change posting period dummy field to longtext
 * 2011/09/05  204716         1.09                    Aggregation processing
 * 2011/09/06  204888         1.11.2011.09.08.02      Set the values updated from web page after
 *                                                    applying filters
 *             204890                                 Get only active aggregation methods
 * 2011/09/12  205257         1.11.2011.09.15.01      Restrict subsidiary view based on user's
 *                                                    permissions
 * 2011/09/15  203845         1.07.2011.08.18.02      Search with markers to allow filter to reach
 *                                                    entities after the 1000 record index; also
 *                                                    fixes performance problems
 *             205014         1.10                    Set flags for making DCL fields mandatory
 * 2011/09/28  206319         1.15.2011.09.29.2       Include expense reports with status: 
 *                                                    Approved (Overridden) by Accounting
 *                                                    in search results
 * 2011/11/04  208589         1.15.2011.11.10.2       Add timestamp field to form; set timestamp field
 *                                                    value based on submitted form          
 * 2011/11/29  210004         1.17.2011.11.24.1       Use field groups to organize the forms
 * 2011/12/15  210788         1.17.2011.12.08.3       Use searchRecord to get DCL lists
 * 2011/12/19  211435         1.18.2011.12.22.1       Add "On Hold" filter for account id's and exclude 
 *                                                    bills with "Hold Payment" = T when it is set to T
 * 2012/01/16  212885         1.19.2012.01.05.1       Do not include inactive DCL in dropdown lists
 * 2012/01/31  213961         1.19.2012.01.12.1       Rollback Positive Pay code
 * 2012/02/02  212974         1.19.2012.01.19.1       Support for Positive Pay file option
 *             213421         1.19.2012.01.19.4       Change "to" label to "Date to" in Positive Pay form
 *             213420         1.19.2012.01.19.4       Set the Cheque From and Cheque To as blank when
 *                                                    all cheques for Bank account were processed
 * 2012/02/20  215424         1.19.1                  Workaround for large data performance issue of
 *                                                    nlapiSearchRecord
 * 2012/03/08  217041         GPM Beta - 1.19.2       Support edition control
 *             216225         1.19.2.2012.03.01.1     Do not sort name column for DCL to correctly perform
 *                                                    search by batch
 * 2012/03/13  216909         1.19.2.2012.03.01.2     Select only up to set maximum lines for file format
 * 2012/03/16  217709         1.20.1.2012.03.15.3     Check if search result is null before accessing array
 * 2012/03/30  218903         1.20.1.2012.03.29.3     Additional filter implementation (initially for Groupon)
 * 2012/05/01  220840         1.22                    Add ID for Groupon/KU changes
 * 2012/05/11  221286         1.22.1                  Add dev account for Groupon
 * 2012/05/22  222193         2.00.0.2012.05.24.3     Assign queue priority during PFA creation
 * 2012/05/25  222391         1.22.2                  Change warning message when transaction list loads
 *                                                    with error
 * 2012/05/28  222494         1.22.2                  Remove "Redemption" as default Payment Type for Groupon
 * 2012/08/01  227867         2.01.1                  Refactor for payment portal functionality
 * 2012/08/03  227868         2.01.1                  Changes based on FRD
 * 2012/08/13  227867         2.01.1                  Add support for customer refund 
 * 2012/08/16  227868         2.01.1                  Change banks to be listed to those with
 *                                                    closed batches; do not list transactions from closed batches
 * 2012/08/30  227867         2.01.1                  Update for test automation
 * 2012/09/10  227868         2.01.1                  Add Show Summarized field
 * 2012/09/12  227868         2.01.1                  Add trans id and trans amount from batch only when Show Summarized Data is T
 * 2012/09/17  227868         2.01.1                  Changes to user interface 
 * 2012/09/21  231393         2.01.1                  Modifications for test automation scripts
 * 2012/10/25  233946         2.01.1                  Set the AP as inline field, include
 *                                                    auto-AP setting of bank as hidden field
 * 2012/11/22  237873         2.00.3                  Add currency and exchange rate related fields
 * 2012/12/17  238204         2.00.5                  Add new Groupon filter fields
 * 2012/12/19  238204         2.00.5                  Add QA account in GrouponIds for testing
 * 2012/01/04  239395         2.00.6                  Add checking to set currency fields only when paymentType is eft or dd
 * 2013/01/07  239363         2.00.6                  Get exchange rates by calling function psg_ep.GetExchangeRates
 * 2013/01/08  239643         2.00.6                  Add checking on Include All Currencies field of Payment File Format record
 * 2013/01/15  238208         2.00.5                  Add Exclude Cleared Checks field
 * 2013/02/12  241335         2.00.5                  Check if element in this.Fields has value
 * 2013/02/12  242626         2.00.5                  Add function psg_ep.isGrouponAccount
 * 2013/03/04  244623         2.00                    Update to support Reversal and Notifications
 * 2013/03/11  245306         2.00.10                 Add custpage_2663_ppclosed to psg_ep.CommonFormFields 
 * 													  and set value in function preparePostingPeriodField()
 * 2013/03/11  245309         2.00.10                 Set maxLength of field custpage_2663_reason to 1000 
 * 													  and set maxLength of added field if maxLength has value
 * 2013/03/18  246362         2.00.10                 Update field helps of new fields
 * 2013/03/19  245309         2.00.10                 Set maxLength of field custpage_2663_reason to 999
 * 2013/03/19  246505         2.00.10                 Add 3579761 in psg_ep.GrouponIds
 * 2013/04/15  248888   	  2.00.12				  Use updated getBatches function and use batch detail name
 * 2013/04/17  249111   	  2.00.13				  Add support for auto-numbering for batches
 * 2013/06/18  254321   	  2.00.18				  Add new Dev account in GrouponIds
 * 2013/07/18  257505 		  2.00.18				  Display error message when time limit is reached
 * 2013/07/15  245723		  2.00.10				  Add support for commission transactions
 * 2013/08/29  261772 		  2.00.25.2013.08.29.3	  Added checking for employee commission feature
 * 2013/09/23  255091		  2.00.18				  Added setup of direct debit type
 *													  Exposed aggregate method, aggregate check box
 * 2013/09/24  263190		  3.00.00				  Add handling for approval routing
 * 2013/09/25  263190 		  3.00.00				  Hide Approval Amount field
 * 2013/09/19  263190 		  3.00.00				  Fix totals when transactions are hidden
 * 2013/09/27  263190 		  3.00.00				  Add edit mode
 * 2013/10/01  264897 		  3.00.00				  Add summary transaction and amount fields when summarized
 * 2013/10/03  265147 		  3.00.0.2013.10.03.6	  Make direct debit type field mandatory
 * 2013/10/05  265406 		  3.00.00				  Refactor code to use Payment File Administration record instead of Payment Batch
 * 2013/10/07  265406 		  3.00.00				  Remove reference to other batch status constants
 * 2013/10/10  265592         3.00.0.2013.10.10.5     Show direct debit type field only for SEPA Direct Debit (Germany)
 * 2013/11/07  268727  		  3.00.00				  Add QA accounts to psg_ep.GrouponIds and psg_ep.HoldPaymentAcctIds
 * 2013/11/26  270825         3.01.00				  Add email template field
 *                                                    Add method to put email template names under a select field
 * 2013/11/26  270825         3.01.00				  Add email subject field
 * 2013/12/04  269082         3.01.00				  Add QA accounts to psg_ep.GrouponIds and psg_ep.HoldPaymentAcctIds
 * 2013/12/12  256855 		  3.00.00     			  Add support for SEPA Direct Debit (CBI)
 * 2013/12/26  272459    	  3.00.1    			  Set default email templates
 * 2013/12/27  272910    	  3.00.2    			  Add Busy Bees Account ID to list with hold payments filter
 * 2014/01/24  273463 		  3.00.10				  Always load transactions as selected for payment batches
 * 2014/01/29  276961 		  3.01.1 				  Set email preference to blank by default and handle case with no EP preference page
 * 2014/04/29  201833								  Add support for EP plugin
 */

var psg_ep;

if (!psg_ep)
    psg_ep = {};

/**
 * Accounts with "On Hold" filter
 */
psg_ep.HoldPaymentAcctIds = [
    '1012538',     // KU - Global Production
    '1012538_SB2', // KU - Dev(Asia&EU)
    '1012538_SB1', // KU - Staging Global
    '1012538_SB3', // KU - Dev(US)
    '3456150',     // KU - US Production
    '3777555',     // KU - Busy Bees
    '3365849',     // PSG - Dev Test Account
    '3358508',     // PSG - QA bundle Single Instance
    '3396051',     // PSG - QA bundle OW
    '3370994',     // PSG - QA Pre-release   
    '3418114',     // PSG - QA Post-release
    '3521181',     // PSG - QA 2.00
    '3647765',	   // PSG - QA 3.00
    '3783647',	   // PSG - QA 3.01
    '3830566',	   // PSG - QA 3.01
    '3989504',     // PSG - QA
    '4008003',     // PSG - QA
    '4016617',     // PSG - QA
    '4022487',     // PSG - Dev
    '4026284',     // PSG - QA
    '3821407',     // PSG - QA
    '3708764',     // PSG - QA
    '4051631',     // PSG - QA
    '4098197',     // PSG - QA
    '4105427',     // PSG - QA
];

/**
 * Accounts for Groupon
 */
psg_ep.GrouponIds = [
    '4004600',     // Groupon account (NEW)	
    '1202613',     // Groupon account (International)
    '3579761',     // Groupon account (US Goods)
    '3490367',     // PSG - Dev Test Account
    '3521998',     // PSG - QA Test Account
    '3522323',     // PSG - QA Test Account
    '3522322',     // PSG - QA Test Account
    '3541180',     // PSG - QA 2.00
    '3554443',	   // PSG - QA 2.00	
    '3544795',     // PSG - Dev 2.00
    '3563761',     // PSG - Dev 2.00
    '3671131',     // PSG - Dev 2.00
    '3681459',	   // PSG - QA 3.00	
    '3783648',	   // PSG - QA 3.01
    '3826104',	   // PSG - QA 3.01
    '3989504',     // PSG - QA
    '4008003',     // PSG - QA
    '4016617',     // PSG - QA
    '4022487',     // PSG - Dev
    '4026284',     // PSG - QA
    '3821407',     // PSG - QA
    '3708764',     // PSG - QA
    '4051631',     // PSG - QA
    '4098197',     // PSG - QA
    '4105427',     // PSG - QA
];

/**
 * Form field groups
 */
psg_ep.FormFieldGroups = {
    'custpage_2663_filtergrp' : {
        label: 'Search Filters'
    },
    'custpage_2663_payinfogrp' : {
        label: 'Payment Information'
    },
    'custpage_2663_classgrp' : {
        label: 'Classification'
    },
    'custpage_2663_grp_transfilters': {
        label: 'Additional Transaction Filters'
    },
    'custpage_2663_grp_entityfilters': {
        label: 'Additional Entity Filters'
    },
    'custpage_2663_batchinfo' : {
        label: 'Batch Information'
    },
    'custpage_2663_je_info_grp' : {
        label: 'Journal Entry Information'
    },
    'custpage_2663_custom_transfilters': {
        label: 'Custom Transaction Filters'
    }
};

/**
 * Fields common to forms
 */
psg_ep.CommonFormFields = {
    'custpage_2663_paymenttype' : {
        type: 'text',
        label: 'Payment Type',
        displayType: 'hidden'
    },
    'custpage_2663_refresh_page' : {
        type: 'text',
        label: 'Refresh Page',
        displayType: 'hidden',
        defaultValue: 'F'
    },
    'custpage_2663_bank_account' : {
        type: 'select',
        label: 'Bank account',
        helpText: 'Defines bank to which payments will be made.\n\n' + 
            'Only transactions from customers that have primary bank details with the same file format as the selected bank will be listed for payment.',
        defaultValue: ''
    },
    'custpage_2663_subsidiary' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_SUBSIDIARY'),
        source: 'subsidiary',
        helpText: 'Selected bank account\'s subsidiary.',
        displayType: 'inline',
        defaultValue: ''
    },
    'custpage_2663_format_display' : {
        type: 'select',
        label: 'Bank account format',
        source: 'customrecord_2663_payment_file_format',
        helpText: 'Selected bank account\'s file format.',
        displayType: 'inline',
        defaultValue: ''
    },
    'custpage_2663_max_lines_sel' : {
        type: 'integer',
        label: 'Maximum payments in file',
        helpText: 'Selected bank account\'s file format\'s maximum number of lines in a payment file.',
        displayType: 'inline',
        defaultValue: ''
    },
    'custpage_2663_date_from' : {
        type: 'date',
        label: 'Due date from',
        helpText: 'If entered, only transactions greater than or equal to this date will be listed.',
        defaultValue: ''
    },        
    'custpage_2663_date_to' : {
        type: 'date',
        label: 'to',
        helpText: 'If entered, only transactions less than or equal to this date will be listed.',
        defaultValue: ''
    },
    'custpage_2663_transtype' : {
        type: 'select',
        label: 'Transaction Type',
        helpText: 'If entered, only transactions of the selected type will be listed.',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: ''
    },
    'custpage_2663_process_date' : {
        type: 'date',
        label: 'Date to be processed',
        helpText: 'Defines the date on which the bank will process the file.',
        layoutType: 'normal',
        breakType: 'startcol',
        mandatory: true,
        defaultValue: ''
    },
    'custpage_2663_postingperiod' : {
        type: 'select',
        label: 'Posting period',
        helpText: 'Defines the accounting period in which selected invoice will be posted.',
        mandatory: true,
        defaultValue: ''
    },
    'custpage_2663_ppstart' : {
        type: 'longtext',
        label: 'Posting period start dates',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_ppend' : {
        type: 'select',
        label: 'Posting period end dates',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_ppclosed' : {
        type: 'longtext',
        label: 'Posting period closed value',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_payment_lines' : {
        type: 'integer',
        label: 'Number of Transactions',
        helpText: 'The count of the total selected transactions',
        displayType: 'inline',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: '0'
    },
    'custpage_2663_total_amount' : {
        type: 'currency',
        label: 'Total Payment Amount',
        helpText: 'The sum of the payment amount of selected transactions',
        displayType: 'inline',
        defaultValue: nlapiFormatCurrency(0)
    },
    'custpage_2663_total_payees' : {
        type: 'integer',
        label: 'Number of Payees',
        helpText: 'Number of payees for selected transactions',
        displayType: 'inline',
        defaultValue: '0'
    },
    'custpage_2663_department' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_DEPARTMENT'),
        helpText: 'Defines the default ' + nlapiGetContext().getPreference('NAMING_DEPARTMENT') + ' for the newly created payment(s).',
        mandatory: isDeptMandatory(),
        defaultValue: ''
    },
    'custpage_2663_classification' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_CLASS'),
        helpText: 'Defines the default ' + nlapiGetContext().getPreference('NAMING_CLASS') + ' for the newly created payment(s).',
        mandatory: isClassMandatory(),
        defaultValue: ''
    },
    'custpage_2663_location' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_LOCATION'),
        helpText: 'Defines the default ' + nlapiGetContext().getPreference('NAMING_LOCATION') + ' for the newly created payment(s).',
        mandatory: isLocMandatory(),
        defaultValue: ''
    },
    'custpage_2663_file_creation_timestamp': {
        type: 'datetimetz',
        label: 'Payment File Creation Timestamp',
        displayType: 'hidden'
    },
    'custpage_2663_trans_marked': {
        type: 'text',
        label: 'Transactions Marked',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_portal_batch_flag': {
        type: 'text',
        label: 'Portal Batch Flag',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_bank_currency': {
        type: 'select',
        label: 'Bank account currency',
        displayType: 'inline',
        source: 'Currency',
        helpText: 'GL bank account currency associated with selected Company Bank Details.'
    },
    'custpage_2663_format_currency': {
        type: 'longtext',
        label: 'Format currencies',
        displayType: 'hidden',
        source: 'Currency'
    },
    'custpage_2663_exchange_rates': {
        type: 'longtext',
        label: 'Exchange rates',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_base_currency': {
        type: 'select',
        label: 'Base currency',
        displayType: 'inline',
        source: 'Currency',
        helpText: 'Base currency of company/subsidiary. This will be used as the target currency when for the exchange rate.'
    },
    'custpage_2663_file_id' : {
        type: 'text',
        label: 'PFA Id',
        helpText: 'Internal id of Payment File Admin record.',
        displayType: 'hidden',
    },
    'custpage_2663_file_name' : {
        type: 'text',
        label: 'File Id',
        displayType: 'inline',
        defaultValue: ''
    },
    'custpage_2663_total_lines' : {
        type: 'integer',
        label: 'Total Lines',
        helpText: 'The count of the total transactions',
        displayType: 'inline',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: '0'
    },
    'custpage_2663_approval_amount' : {
        type: 'currency',
        label: 'Amount for Approval',
        helpText: 'This field displays the highest single or combined total amount among all transactions within the payment batch. For more information, see the Processing Bills and Expenses in Batches topic in the Help Center.',
        displayType: 'hidden',
        defaultValue: nlapiFormatCurrency(0)
    },
    'custpage_2663_approval_routing' : {
        type: 'checkbox',
        label: 'Approval Routing',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_edit_mode': {
    	type: 'checkbox',
        label: 'Edit Mode',
        displayType: 'hidden',
        defaultValue: 'F'
    },
    'custpage_2663_subject' : {
        type: 'text',
        label: 'Subject',
        helpText: 'Subject that will be included in the e-mail notification',
        mandatory: true,
        defaultValue: 'Payment Notification'
    }
};

/**
 * Fields for the EFT form
 */
psg_ep.EFTFormFields = {
    'custpage_2663_ap_account' : {
        type: 'select',
        label: 'A/P account',
        helpText: 'Defines the AP Account filter which will be used in the retrieval of available transactions for payment.',
        defaultValue: ''
    },
    'custpage_2663_vendor' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_VENDOR'),
        helpText: 'Select to filter available bills only for that ' + nlapiGetContext().getPreference('NAMING_VENDOR') + '.',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: ''
    },
    'custpage_2663_employee' : {
        type: 'select',
        label: 'Employee',
        helpText: 'Select to filter available expense reports only for that Employee.',
        defaultValue: ''
    },
    'custpage_2663_partner' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_PARTNER'),
        helpText: 'Select to filter available bills only for that ' + nlapiGetContext().getPreference('NAMING_PARTNER') + '.',
        defaultValue: ''
    },
    'custpage_2663_onhold' : {
        type: 'checkbox',
        label: 'Include Bills On Hold',
        helpText: 'Check this box to include bills that are on hold.',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: 'F'
    },
    'custpage_2663_payment_ref' : {
        type: 'text',
        label: 'EFT file reference note',
        helpText: 'Defines the EFT file reference note. The reference note appears within the EFT file.',
        mandatory: true,
        defaultValue: ''
    },
    'custpage_2663_aggregate' : {
        type: 'checkbox',
        label: 'Aggregate by Payee',
        helpText: 'Check this box to aggregate payments based on method selected. Checking this box and setting the Aggregate Method to blank will aggregate payments by entity.',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: 'T'
    },
    'custpage_2663_agg_method' : {
        type: 'select',
        label: 'And',
        helpText: 'Available aggregation methods. Setting this as blank will default to aggregation of payments by entity. \nTo add a new method, go to Setup > Payment Aggregation > New.',
        defaultValue: ''
    }
};

/**
 * Fields for the DD form
 */
psg_ep.DDFormFields = {
    'custpage_2663_ar_account' : {
        type: 'select',
        label: 'A/R account',
        helpText: 'Defines the AR Account filter which will be used in the retrieval of available transactions.',
        defaultValue: ''
    },
    'custpage_2663_customer' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_CUSTOMER'),
        helpText: 'Select to filter available bills only for that ' + nlapiGetContext().getPreference('NAMING_CUSTOMER') + '.',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: ''
    },
    'custpage_2663_payment_ref' : {
        type: 'text',
        label: 'Direct Debit file reference note',
        helpText: 'Defines the Direct Debit file reference note. The reference note appears within the Direct Debit file.',
        mandatory: true,
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: ''
    },
    'custpage_2663_dd_type' : {
        type: 'select',
        label: 'Direct Debit type',
        helpText: 'Defines the Direct Debit type. The type appears within the Direct Debit file.',
        mandatory: true,
        layoutType: 'normal',
        defaultValue: ''
    },
    'custpage_2663_dept_filter' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_DEPARTMENT') + ' Filter',
        helpText: 'Filter for invoices by ' + nlapiGetContext().getPreference('NAMING_DEPARTMENT') + '.',
        defaultValue: ''
    },
    'custpage_2663_class_filter' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_CLASS') + ' Filter',
        helpText: 'Filter for invoices by ' + nlapiGetContext().getPreference('NAMING_CLASS') + '.',
        defaultValue: ''
    },
    'custpage_2663_loc_filter' : {
        type: 'select',
        label: nlapiGetContext().getPreference('NAMING_LOCATION') + ' Filter',
        helpText: 'Filter for invoices by ' + nlapiGetContext().getPreference('NAMING_LOCATION') + '.',
        defaultValue: ''
    },
    'custpage_2663_aggregate' : {
        type: 'checkbox',
        label: 'Aggregate by Payee',
        helpText: 'Check this box to aggregate payments based on method selected. Checking this box and setting the Aggregate Method to blank will aggregate payments by entity.',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: 'T'
    },
    'custpage_2663_agg_method' : {
        type: 'select',
        label: 'And',
        helpText: 'Available aggregation methods. Setting this as blank will default to aggregation of payments by entity. \nTo add a new method, go to Setup > Payment Aggregation > New.',
        defaultValue: ''        
    }
};

/**
 * Fields for the PP form
 */
psg_ep.PPFormFields = {
    'custpage_2663_first_check_no' : {
        type: 'integer',
        label: 'Cheque From',
        helpText: 'Filters for check numbers between the values indicated for Cheque From and To numbers.',
        layoutType: 'normal',
        breakType: 'startcol',
        mandatory: true,
        defaultValue: ''
    },
    'custpage_2663_last_check_no' : {
        type: 'integer',
        label: 'Cheque To',
        helpText: 'Filters for check numbers between the values indicated for Cheque From and To numbers.',
        mandatory: true,
        defaultValue: ''
    },
    'custpage_2663_fcn_hidden' : {
        type: 'text',
        label: 'First Cheque Number',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_lcn_hidden' : {
        type: 'text',
        label: 'Last Cheque Number',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_void' : {
        type: 'checkbox',
        label: 'Include Void Cheques',
        helpText: 'Check this box to include checks that have been voided through a reversal journal entry.',
        defaultValue: 'T'
    },
    'custpage_2663_exclude_cleared' : {
        type: 'checkbox',
        label: 'Exclude Cleared Cheques',
        helpText: 'Check this box to exclude checks that were marked cleared in your bank register.',
        defaultValue: 'T'
    },
    'custpage_2663_void_payment_lines' : {
        type: 'integer',
        label: 'Void Transactions Selected',
        helpText: 'The sum of the payment amount of selected void transactions',
        displayType: 'inline',
        defaultValue: '0'
    },
    'custpage_2663_void_total_amount' : {
        type: 'currency',
        label: 'Total Void Payment Amount',
        helpText: 'The sum of the payment amount of selected void transactions',
        displayType: 'inline',
        defaultValue: nlapiFormatCurrency(0)
    }
};

/**
 * Fields for the Reversal form
 */
psg_ep.ReversalFormFields = {
    'custpage_2663_total_amount_paid' : {
        type: 'currency',
        label: 'Total Amount Paid',
        helpText: 'The sum of the payment amount of all transactions',
        displayType: 'inline',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: '0'
    },
    'custpage_2663_reason' : {
        type: 'text',
        label: 'Reversal Reason',
        helpText: 'Enter a note describing the reason behind the payment reversal. This note will be displayed on the memo field of the payment transactions that were marked for reversal, and added to the memo field of the payment file administration system notes.',
        maxLength: 999,
        defaultValue: ''
    }
};

/**
 * Fields for the Notification form
 */
psg_ep.NotificationFormFields = {
    'custpage_2663_email_body_def' : {
        type: 'longtext',
        label: 'E-mail Notes',
        helpText: 'Text that will be included in the e-mail notification',
    },
    'custpage_2663_email_templates' : {
        type: 'select',        
        label: 'Template'        
    }
};


/**
 * Custom fields holder
 */
psg_ep.CustomFields = {
    'custpage_2663_custom_flds_transfilters' : {
        type: 'longtext',
        label: 'Custom Transaction Filter Fields',
        displayType: 'hidden'
    },
    'custpage_2663_remove_flds_transfilters' : {
        type: 'longtext',
        label: 'Removed Transaction Filter Fields',
        displayType: 'hidden'
    },
    'custpage_2663_remove_flds_trans' : {
        type: 'longtext',
        label: 'Removed Transaction Fields',
        displayType: 'hidden'
    }
};

/**
 * Custom transaction filters fields
 */
psg_ep.CustomTransactionFilterFields = {
		
};


/**
 * Custom transaction filters
 */
psg_ep.CustomTransactionFilters = {
		
};

/**
 * Custom search filters
 */
psg_ep.CustomSearchFilters = {
		
};

/**
 * Custom transaction columns
 */
psg_ep.CustomTransactionColumns = {
		
};

/**
 * Groupon transaction filter fields
 */
psg_ep.GrouponTransactionFilterFields = {
    'custpage_2663_tranid' : {
        type: 'text',
        label: 'Invoice Number',
        helpText: 'Invoice Number',
        defaultValue: ''
    },
    'custpage_2663_custbody_payment_type' : {
        type: 'multiselect',
        label: 'Payment Type',
        source: 'customlist_payment_type',
        helpText: 'Payment Type',
        defaultValue: ''
    },
    'custpage_2663_custbody_deal_id' : {
        type: 'text',
        label: 'Geo ID/Deal ID',
        helpText: 'Geo ID/Deal ID',
        defaultValue: ''
    },
    'custpage_2663_custbody_document_number' : {
        type: 'text',
        label: 'Document Number',
        helpText: 'Document Number',
        defaultValue: ''
    },
    'custpage_2663_custbody_hold_payment_bill' : {
        type: 'checkbox',
        label: 'Hold Payment',
        helpText: 'Hold Payment',
        defaultValue: 'F'
    },    
    'custpage_2663_af_flds_transfilters' : {
        type: 'longtext',
        label: 'Additional Transaction Filter Fields',
        displayType: 'hidden',
        defaultValue: JSON.stringify([ 
            'custpage_2663_tranid', 
            'custpage_2663_custbody_payment_type', 
            'custpage_2663_custbody_deal_id', 
            'custpage_2663_custbody_document_number',
            'custpage_2663_custbody_hold_payment_bill'
        ])
    }
};

/**
 * Groupon entity filter fields
 */
psg_ep.GrouponEntityFilterFields = {
    'custpage_2663_custentity_v_salesforce_account_id' : {
        type: 'text',
        label: 'SalesForce ID',
        helpText: 'SalesForce ID',
        defaultValue: ''
    },
    'custpage_2663_af_flds_entityfilters' : {
        type: 'longtext',
        label: 'Additional Entity Filter Fields',
        displayType: 'hidden',
        defaultValue: JSON.stringify([ 
            'custpage_2663_custentity_v_salesforce_account_id' ])
    }
};

/**
 * Batch info fields
 */
psg_ep.BatchInfoFields = {
    'custpage_2663_batchid': {
        type: 'text',
        label: 'Payment Batch',
        helpText: 'Payment Batch record id.',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_batch_name': {
        type: 'text',
        label: 'Name',
        helpText: 'Payment Batch name.',
        layoutType: 'normal',
        displayType: 'inline',
        breakType: 'startcol',
        defaultValue: ''
    },
    'custpage_2663_action' : {
        type: 'text',
        label: 'Batch Action',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_summarized': {
        type: 'checkbox',
        label: 'Hide Transactions',
        helpText: 'When this field is checked, only the summary for the selected batch will be displayed. When this field is unchecked, the transactions in the batch will be displayed.',
        layoutType: 'normal',
        breakType: 'startcol',
        defaultValue: ''
    },
    'custpage_2663_summarized_placeholder': {
        type: 'text',
        label: 'Show Summarized Placeholder',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_trans_id': {
        type: 'longtext',
        label: 'Batch Transaction Ids',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_trans_amount': {
        type: 'longtext',
        label: 'Batch Transaction Amounts',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_auto_ap': {
        type: 'text',
        label: 'Bank Auto AP',
        displayType: 'hidden',
        defaultValue: ''
    },
    'custpage_2663_batch': {
        type: 'checkbox',
        label: 'Batch',
        displayType: 'hidden',
        defaultValue: 'T'
    },
    'custpage_2663_batch_status': {
        type: 'select',
        source: 'customlist_2663_batch_status',
        label: 'Status',
        displayType: 'inline'
    },
    'custpage_2663_batch_approver': {
        type: 'multiselect',
        source: 'Employee',
        label: 'Approver',
        displayType: 'inline'
    }
};

/**
 * Buttons
 */
psg_ep.FormButtons = {
    'custpage_submitter' : {
        label: 'Submit',
        script: 'ep_SubmitForm()'
    },
    'custpage_cancel' : {
        label: 'Cancel',
        // TODO: change to load to dashboard
        script: 'ep_Cancel()'
    },
    'custpage_approve' : {
        label: 'Approve',
        script: 'ep_Approve()'
    },
    'custpage_save' : {
        label: 'Save',
        script: 'ep_Save()'
    },
    'custpage_edit' : {
        label: 'Edit',
        script: 'ep_Edit()'
    },
    'custpage_reject' : {
        label: 'Reject',
        script: 'ep_Reject()'
    },
};

/**
 * Checks if companyId is a Groupon account
 * 
 * @param {String} companyId
 * 
 * @returns {boolean}
 */
psg_ep.isGrouponAccount = function isGrouponAccount(companyId) {
	if (psg_ep.GrouponIds.indexOf(companyId) > -1) {
		return true;
	}
	for (var i = 0, ii = psg_ep.GrouponIds.length; i < ii; i++) {
		if (companyId.indexOf(psg_ep.GrouponIds[i] + '_') > -1) {
			return true;
		}
	}
	return false;
}; 

/**
 * Static class with utility functions to create objects in a form
 */
psg_ep.FormUtils = new function() {
    /**
     * Creates a form field based on object with properties
     * 
     * @param {nlobjForm} form
     * @param {String} name
     * @param {} fieldObj
     * @param {String} fieldGroup
     * 
     * @returns {nlobjField}
     */
    this.CreateFieldFromObject = function(form, name, fieldObj, fieldGroup) {
        var fld;
        if (form && name && fieldObj && fieldObj.type && fieldObj.label) {
            fld = form.addField(name, fieldObj.type, fieldObj.label, fieldObj.source, fieldGroup == 'none' ? null : fieldGroup);
            if (fieldObj.helpText) {
                fld.setHelpText(fieldObj.helpText);
            }
            if (fieldObj.displayType) {
                fld.setDisplayType(fieldObj.displayType);
            }
            if (fieldObj.layoutType && fieldObj.breakType) {
                fld.setLayoutType(fieldObj.layoutType, fieldObj.breakType);
            }
            if (fieldObj.defaultValue) {
                fld.setDefaultValue(fieldObj.defaultValue);
            }
            if (fieldObj.mandatory) {
                fld.setMandatory(fieldObj.mandatory);
            }
            if (fieldObj.maxLength) {
                fld.setMaxLength(fieldObj.maxLength);
            }
        }
        return fld;
    };

    /**
     * Creates a form field based on object with properties
     * 
     * @param {nlobjForm} form
     * @param {String} fieldGroupId
     * 
     * @returns {nlobjFieldGroup}
     */
    this.CreateFieldGroupFromObject = function(form, fieldGroupId) {
        var fieldGroup;
        if (form && fieldGroupId && fieldGroupId != 'none') {
            var fieldGroupObj = psg_ep.FormFieldGroups[fieldGroupId];
            if (fieldGroupObj) {
                fieldGroup = form.addFieldGroup(fieldGroupId, fieldGroupObj.label);
                fieldGroup.setSingleColumn(false);
                fieldGroup.setShowBorder(true);
            }
        }
        return fieldGroup;
    };
    
    /**
     * Creates a button based on object with properties
     * 
     * @param {nlobjForm} form
     * @param {String} buttonId
     * 
     * @returns {nlobjButton}
     */
    this.CreateButtonFromObject = function(form, buttonId) {
        var button;
        if (form && buttonId) {
            var buttonObj = psg_ep.FormButtons[buttonId];
            if (buttonObj) {
                button = form.addButton(buttonId, buttonObj.label, buttonObj.script);
            }
        }
        return button;
    };
};

/**
 * Parent class for payment selection form
 * 
 * @param {psg_ep.FormUIBuilderInterface} uiBuilderInterface
 * @returns {psg_ep.PaymentSelectionForm}
 */
psg_ep.PaymentSelectionForm = function(uiBuilderInterface) {
    if (!uiBuilderInterface) {
        throw nlapiCreateError('EP_INTERFACE_OBJ_NULL', 'Cannot create object without interface for UIBuilder', true);
    }
    
    this.StartTime = (new Date()).getTime();
    this.Form = null;
    this.Fields = {};
    this.ButtonIds = [];
    this.Sublist = null;
    this.RequestParams = {};
    this.DataFromSearch = {};
    this.Entities = [];
    this.UIBuilder = new psg_ep.FormUIBuilder(this, uiBuilderInterface);
    this.FieldGroups = [];
    this.FieldInitializer = new psg_ep.FormFieldInitializer(this);

    ////////////////////////////////////////////////////////////////////////////////////
    // Common Functions (Framework) - Start
    /**
     * Builds the form UI based on form title and client script id. 
     * Uses the UIBuilder object and its interface to customize fields to add to form.
     * 
     * @params {String} formTitle
     * @params {String} clientScriptId
     */
    function buildUI(formTitle, clientScriptId) {
        if (formTitle && clientScriptId) {
            this.Form = nlapiCreateForm(formTitle);
            this.Form.setScript(clientScriptId);
            
            for (var i = 0; i < this.FieldGroups.length; i++) {
                this.UIBuilder.BuildFieldGroup(this.FieldGroups[i]);
            }
    
            this.UIBuilder.BuildSublist();
            
            // create buttons
            for (var i = 0; i < this.ButtonIds.length; i++) {
                psg_ep.FormUtils.CreateButtonFromObject(this.Form, this.ButtonIds[i]);
            }
        }
    }
    
    /**
     * Set the field values
     */
    function setFieldValuesFromParams() {
        for (var i in this.RequestParams) {
        	if (this.Fields[i]) {
        		this.Fields[i].setDefaultValue(this.RequestParams[i]);	
        	}
        }
    }
    
    /**
     * Set the sublist data
     * 
     * @param {} sublistData
     * @param {nlobjRequest} request
     */
    function setSublistData(sublistData, showMarkAllFlag) {
    	if (sublistData && sublistData.transactionList && sublistData.errorFlag != undefined) {
            var sublistLines = sublistData.transactionList;
            var errorFlag = sublistData.errorFlag;
            
            showMarkAllFlag = showMarkAllFlag || false;
            if (showMarkAllFlag == true && sublistLines.length > 0) {
                // add buttons
                this.Sublist.AddButton('mark_all', 'Mark All', 'ep_MarkAll()');
                this.Sublist.AddButton('unmark_all', 'Unmark All', 'ep_UnmarkAll()');
            }

            this.Sublist.SetSublistData(sublistLines);
            if (sublistData.entitiesSearchError) {
            	var errorMessage = 'Notice: time limit reached during entities search.';
                this.Sublist.SetErrorMessage('[ep] PaymentSelectionForm:setSublistData', errorMessage);
            }
            else if (errorFlag == true) {
                var errorMessage = 'Notice: additional transactions match your criteria and will be available for display after this selection is processed.';
                this.Sublist.SetErrorMessage('[ep] PaymentSelectionForm:setSublistData', errorMessage);
            }
        }
    }
    
    // Common Functions (Framework) - End
    ////////////////////////////////////////////////////////////////////////////////////
    
    this._buildUI = buildUI;
    this._setFieldValuesFromParams = setFieldValuesFromParams;
    this._setSublistData = setSublistData;
};

/**
 * Interface for form builder
 * 
 * @returns {psg_ep.FormUIBuilderInterface}
 */
psg_ep.FormUIBuilderInterface = function() {
    this.GetFieldsForFieldGroup = null;
    this.SublistClass = null;
};

/**
 * Builds the form components based on payment type
 * 
 * @param {psg_ep.PaymentSelectionForm} uiObj
 * @param {psg_ep.FormUIBuilderInterface} uiBuilderInterface
 * @returns {psg_ep.FormUIBuilder}
 */
psg_ep.FormUIBuilder = function(uiObj, uiBuilderInterface) {
    if (!uiObj) {
        throw nlapiCreateError('EP_UI_OBJ_NULL', 'Cannot create data object without ui object', true);
    }
    if (!uiBuilderInterface) {
        throw nlapiCreateError('EP_INTERFACE_OBJ_NULL', 'Cannot create object without interface', true);
    }
    
    // assign argument objects
    this._uiObj = uiObj;
    this._uiBuilderInterface = uiBuilderInterface;
    
    /**
     * Builds the field group based on the field group ids. The ui builder interface should be defined.
     * 
     * @param {String} fieldGroupId
     */
    function buildFieldGroup(fieldGroupId) {
        if (fieldGroupId && verifyInterface(this._uiBuilderInterface) == true) {
            var fieldGroupObjects = uiBuilderInterface.GetFieldsForFieldGroup(fieldGroupId);
            if (fieldGroupObjects) {
                psg_ep.FormUtils.CreateFieldGroupFromObject(this._uiObj.Form, fieldGroupId);
                for (var i in fieldGroupObjects) {
                    this._uiObj.Fields[i] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, i, fieldGroupObjects[i], fieldGroupId);            
                }
            }
        }
    }
    
    /**
     * Builds the sublist object. The ui builder interface should be defined.
     */
    function buildSublist() {
        if (verifyInterface(this._uiBuilderInterface) == true) {
            this._uiObj.Form.addSubTab('custpage_subtab', 'Select Transactions');
            this._uiObj.Sublist = new uiBuilderInterface.SublistClass(this._uiObj.Form, 'custpage_2663_sublist', 'list', 'Select Transactions', 'custpage_subtab');
            this._uiObj.Sublist.BuildUI();
        }
    }
    
    /**
     * Function to check the validity of interface object before making the object functions accessible
     * 
     * @param {psg_ep.FormUIBuilderInterface} interfaceObj
     * @returns {Boolean}
     */
    function verifyInterface(interfaceObj) {
        return (interfaceObj != null &&
                interfaceObj.GetFieldsForFieldGroup != null && 
                typeof interfaceObj.GetFieldsForFieldGroup == 'function' && 
                interfaceObj.SublistClass != null);
    }
    
    this.BuildFieldGroup = buildFieldGroup;
    this.BuildSublist = buildSublist;
};

/**
 * Functions to initialize the fields in the form
 * 
 * @param {psg_ep.PaymentSelectionForm} uiObj
 * @returns {psg_ep.FormFieldInitializer}
 */
psg_ep.FormFieldInitializer = function(uiObj) {
	var logTitle = '[ep] FormFieldInitializer'; 
	var logger = new _2663.Logger(logTitle);
    if (!uiObj) {
        throw nlapiCreateError('EP_UI_OBJ_NULL', 'Cannot create data object without ui object', true);
    }
    
    // assign argument objects
    this._uiObj = uiObj;

    // data provider
    this._dataProvider = new psg_ep.FormFieldDataProvider();
    
    /**
     * Prepare bank accounts and corresponding data
     * 
     * @param {String} paymentType
     * @param {Boolean} batchFlag
     */
    function prepareBankAccountField(paymentType, batchFlag) {
        if (paymentType == 'eft' || paymentType == 'dd' || paymentType == 'pp') {
            var bankAccountField = this._uiObj.Fields['custpage_2663_bank_account'];
            batchFlag = batchFlag || false;
            if (batchFlag == true) {
                this._uiObj.DataFromSearch.BankAccounts = this._dataProvider.GetBatchBankAccount(paymentType, this._uiObj.RequestParams['custpage_2663_batchid']);
            } else {
            	bankAccountField.addSelectOption('', '');
                this._uiObj.DataFromSearch.BankAccounts = this._dataProvider.GetBankAccounts(paymentType);
                for (var i = 0; i < this._uiObj.DataFromSearch.BankAccounts.length; i++) {
                    bankAccountField.addSelectOption(this._uiObj.DataFromSearch.BankAccounts[i].getId(), this._uiObj.DataFromSearch.BankAccounts[i].getValue('name'));
                }
            }
        }
    }
    
    /**
     * Prepare the posting period list and the posting period start/end date hidden objects
     */
    function preparePostingPeriodField() {
        this._uiObj.DataFromSearch.PostingPeriods = this._dataProvider.GetPostingPeriods();
        var postingPeriodField = this._uiObj.Fields['custpage_2663_postingperiod'];
        var ppStartField = this._uiObj.Fields['custpage_2663_ppstart'];
        var ppEndField = this._uiObj.Fields['custpage_2663_ppend'];
        var ppClosedField = this._uiObj.Fields['custpage_2663_ppclosed'];
        var startDateObj = {};
        var endDateObj = {};
        var closedObj = {};
        if (postingPeriodField) {
        	for (var i = 0; i < this._uiObj.DataFromSearch.PostingPeriods.length; i++) {
            	var postingPeriodId = this._uiObj.DataFromSearch.PostingPeriods[i].getId();
            	if (postingPeriodId) {
            		//Check if period is closed
                    if (this._uiObj.DataFromSearch.PostingPeriods[i].getValue('closed') == 'F') {
                    	// add period to dropdown
                        postingPeriodField.addSelectOption(postingPeriodId, this._uiObj.DataFromSearch.PostingPeriods[i].getValue('periodname'));
                    }
                    // set the start and end date objects
                    startDateObj[postingPeriodId] = this._uiObj.DataFromSearch.PostingPeriods[i].getValue('startdate');
                    endDateObj[postingPeriodId] = this._uiObj.DataFromSearch.PostingPeriods[i].getValue('enddate');	
                    closedObj[postingPeriodId] = this._uiObj.DataFromSearch.PostingPeriods[i].getValue('closed');
            	}
            }	
        }
        
        // set hidden fields for the start and end date references
        if (ppStartField) {
        	ppStartField.setDefaultValue(JSON.stringify(startDateObj));	
        }
        if (ppEndField) {
        	ppEndField.setDefaultValue(JSON.stringify(endDateObj));	
        }
        if (ppClosedField) {
        	ppClosedField.setDefaultValue(JSON.stringify(closedObj));	
        }
    } 
    
    /**
     * Prepare the aggregation field
     */
    function prepareAggregationField() {
        var aggField = this._uiObj.Fields['custpage_2663_aggregate'];
        if (aggField && this._uiObj.RequestParams['custpage_2663_refresh_page'] == 'T') {
            var isChecked = this._uiObj.RequestParams['custpage_2663_aggregate'] || 'F';
            aggField.setDefaultValue(isChecked);
        }
    }
    
    /**
     * Prepare the aggregation method list
     */
    function prepareAggregateMethodField() {
        this._uiObj.DataFromSearch.AggMethods = this._dataProvider.GetAggregationMethods();
        var aggMethodField = this._uiObj.Fields['custpage_2663_agg_method'];
        if (aggMethodField) {
        	var isAggChecked = this._uiObj.RequestParams['custpage_2663_aggregate'];
            if (isAggChecked == 'F') {
                aggMethodField.setDisplayType('disabled');
            }
            aggMethodField.addSelectOption('', '');
            for (var i = 0; i < this._uiObj.DataFromSearch.AggMethods.length; i++) {
                aggMethodField.addSelectOption(this._uiObj.DataFromSearch.AggMethods[i].getValue('custrecord_2663_agg_field_id'), this._uiObj.DataFromSearch.AggMethods[i].getValue('name'));
            }	
        }
    }
    
    /**
     * Prepare transaction type filters
     * 
     * @param {String} paymentType
     */
    function prepareTransactionTypeField(paymentType) {
        if (paymentType == 'eft' || paymentType == 'dd') {
            var transTypes = {};
            if (paymentType == 'eft') {
                transTypes['VendBill'] = 'Bill';
                transTypes['ExpRept'] = 'Expense Report';
                
                if (isCommissionEnabled() || isEmployeeCommissionEnabled()) {
                	transTypes['Commissn'] = 'Commission';
                }
            }
            else {
                transTypes['CustInvc'] = 'Invoice';
            }
            transTypes['Journal'] = 'Journal';

            var transTypeField = this._uiObj.Fields['custpage_2663_transtype'];
            transTypeField.addSelectOption('', '');
            for (var i in transTypes) {
                transTypeField.addSelectOption(i, transTypes[i]);
            }
        }
    }
    
    /**
     * Prepare bank account related fields and information (format, subsidiary, max lines, dcl)
     */
    function prepareBankAccountRelatedInfo(batchFlag) {
    	logger.setTitle([logTitle, 'prepareBankAccountRelatedInfo'].join(':'));
        var paymentType = this._uiObj.RequestParams['custpage_2663_paymenttype'];
        if (paymentType == 'eft' || paymentType == 'dd' || paymentType == 'pp' || paymentType == 'custref') {
            batchFlag = batchFlag || false;
            // customer refund type behaves the same as eft
            if (paymentType == 'custref') {
                paymentType = 'eft';
            }
            
            // Bank account format, Maximum payments in file, Subsidiary
            // Find selected bank info
            var selectedBank = this._uiObj.RequestParams['custpage_2663_bank_account'];
            var selectedBankInfo;
            for (var i = 0; i < this._uiObj.DataFromSearch.BankAccounts.length; i++) {
                var currBank = this._uiObj.DataFromSearch.BankAccounts[i].getId();
                if (currBank == selectedBank) {
                    selectedBankInfo = this._uiObj.DataFromSearch.BankAccounts[i];
                    this._uiObj.DataFromSearch.SelectedBank = selectedBankInfo;
                    break;
                }
            }
            if (selectedBankInfo) {
                var templateFieldId = 'custrecord_2663_' + paymentType + '_template';
                var batchFlagFieldId = 'custrecord_2663_' + paymentType + '_batch';
                // get values for fields that are based on selected bank
                var formatValue = selectedBankInfo.getValue(templateFieldId);
                var currencyValue = selectedBankInfo.getValue('custrecord_2663_currency');
                var maxLinesValue = selectedBankInfo.getValue('custrecord_2663_max_lines', templateFieldId);
                if (batchFlag == true && (paymentType == 'eft' || paymentType == 'dd')) {
                    maxLinesValue = nlapiLookupField('customrecord_2663_payment_file_format', formatValue, 'custrecord_2663_max_lines');
                }
                var portalBatchFlagValue = selectedBankInfo.getValue(batchFlagFieldId) || 'F';
    
                var formatObj = psg_ep.CommonFormFields['custpage_2663_format_display'];
                formatObj.defaultValue = formatValue;
                var maxLinesObj = psg_ep.CommonFormFields['custpage_2663_max_lines_sel'];
                maxLinesObj.defaultValue = maxLinesValue;
                
                // set the fields to be added
                var bankAccountFieldObjects = {
                    'custpage_2663_format_display': formatObj,
                    'custpage_2663_max_lines_sel': maxLinesObj,
                };
                if (paymentType == 'eft' || paymentType == 'dd') {
                    var portalBatchFlagObj = psg_ep.CommonFormFields['custpage_2663_portal_batch_flag'];
                    portalBatchFlagObj.defaultValue = portalBatchFlagValue;
                    bankAccountFieldObjects['custpage_2663_portal_batch_flag'] = portalBatchFlagObj;
                }
                
                // only add subsidiary for one world accounts
                var subsidiaryValue;
                if (isOneWorld()) {
                    subsidiaryValue = selectedBankInfo.getValue('custrecord_2663_subsidiary');
                    var subsidiaryObj = psg_ep.CommonFormFields['custpage_2663_subsidiary'];
                    subsidiaryObj.defaultValue = subsidiaryValue;
                    bankAccountFieldObjects['custpage_2663_subsidiary'] = subsidiaryObj;
                }
                
                // add currency objects
                if (isMultiCurrency() && ['eft', 'dd'].indexOf(paymentType) > -1) {
                    var currencyObj = psg_ep.CommonFormFields['custpage_2663_bank_currency'];
                    currencyObj.defaultValue = currencyValue;
                    bankAccountFieldObjects['custpage_2663_bank_currency'] = currencyObj;
                    var baseCurrencyObj = psg_ep.CommonFormFields['custpage_2663_base_currency'];
                    var baseCurrencyValue = psg_ep.GetBaseCurrency(subsidiaryValue);
                    baseCurrencyObj.defaultValue = baseCurrencyValue;
                    bankAccountFieldObjects['custpage_2663_base_currency'] = baseCurrencyObj;
                }
                
                // add new fields
                var insertBeforeFld;
                if (paymentType == 'eft') {
                    insertBeforeFld = 'custpage_2663_ap_account';
                }
                else if (paymentType == 'dd') {
                    insertBeforeFld = 'custpage_2663_ar_account';
                }
                else if (paymentType == 'pp') {
                    insertBeforeFld = 'custpage_2663_date_from';
                }
                
                for (var i in bankAccountFieldObjects) {
                    this._uiObj.Fields[i] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, i, bankAccountFieldObjects[i], 'custpage_2663_filtergrp');
                    this._uiObj.Form.insertField(this._uiObj.Fields[i], insertBeforeFld);
                    this._uiObj.RequestParams[i] = bankAccountFieldObjects[i].defaultValue;
                }
                
                if (paymentType == 'eft' || paymentType == 'dd') {
                    // set the dcl if needed based on bank
                    if (isDepartment() && this._uiObj.Fields['custpage_2663_department'] && !this._uiObj.RequestParams['custpage_2663_department']) {
                        this._uiObj.RequestParams['custpage_2663_department'] = selectedBankInfo.getValue('custrecord_2663_bank_department') || '';
                    }
                    if (isClass() && this._uiObj.Fields['custpage_2663_classification'] && !this._uiObj.RequestParams['custpage_2663_classification']) {
                        this._uiObj.RequestParams['custpage_2663_classification'] = selectedBankInfo.getValue('custrecord_2663_bank_class') || '';
                    }
                    if (isLocation() && this._uiObj.Fields['custpage_2663_location'] && !this._uiObj.RequestParams['custpage_2663_location']) {
                        this._uiObj.RequestParams['custpage_2663_location'] = selectedBankInfo.getValue('custrecord_2663_bank_location') || '';
                    }
                    
                    // set transactions marked field
                    var transMarkedValue = batchFlag ? 'T' : (selectedBankInfo.getValue('custrecord_2663_trans_marked') || 'F');
                    if (this._uiObj.RequestParams['custpage_2663_trans_marked']) {
                        transMarkedValue = this._uiObj.RequestParams['custpage_2663_trans_marked'];
                    }
                    else {
                        this._uiObj.RequestParams['custpage_2663_trans_marked'] = transMarkedValue;
                    }
                    var transMarkedValueField = this._uiObj.Fields['custpage_2663_trans_marked'];
                    transMarkedValueField.setDefaultValue(transMarkedValue);
                }
            }
        }
    }

    /**
     * Prepare format (payment file template) related fields and information
     *
     * @param   {String}    formatId
     */
    function prepareFormatRelatedInfo(formatId) {
        if (formatId){
            var formatName = nlapiLookupField('customrecord_2663_payment_file_format', formatId, 'name');
            switch (formatName){
            	case 'SEPA Direct Debit (CBI)':
                case 'SEPA Direct Debit (Germany)':{
                
                    //add direct debit type
                    this._uiObj.Fields['custpage_2663_dd_type'] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, 'custpage_2663_dd_type', psg_ep.DDFormFields['custpage_2663_dd_type'], 'custpage_2663_payinfogrp');                    
                    this._uiObj.Form.insertField(this._uiObj.Fields['custpage_2663_dd_type'], 'custpage_2663_aggregate');

                    this._uiObj.DataFromSearch.DirectDebitTypes = this._dataProvider.GetDirectDebitTypes();
                    var ddTypeField = this._uiObj.Fields['custpage_2663_dd_type'];
                    
                    if (ddTypeField) {
                        ddTypeField.addSelectOption('', '');
                        for (var i = 0; i < this._uiObj.DataFromSearch.DirectDebitTypes.length; i++) {
                            ddTypeField.addSelectOption(this._uiObj.DataFromSearch.DirectDebitTypes[i].getValue('internalid'), this._uiObj.DataFromSearch.DirectDebitTypes[i].getValue('name'));
                        }
                    }

                    break;
                }
            }
        }
    }
  
    
    /**
     * Prepare accounts payable/receivable field. If there is only one account, set it as default. 
     * If format is given, do not include ap that were set for auto-processing in list.
     * 
     * @param format
     */
    function prepareAccountField(format) {
        var paymentType = this._uiObj.RequestParams['custpage_2663_paymenttype'];
        if (paymentType == 'eft' || paymentType == 'dd') {
            var accountType = paymentType == 'eft' ? 'AcctPay' : 'AcctRec';
            var fieldId = paymentType == 'eft' ? 'custpage_2663_ap_account' : 'custpage_2663_ar_account';
            var subsidiary = '';
            if (isOneWorld()) {
                subsidiary = this._uiObj.RequestParams['custpage_2663_subsidiary'];
            }
            this._uiObj.DataFromSearch.Accounts = this._dataProvider.GetAccounts(subsidiary, accountType);
            // get auto process accts to be excluded only if payment type is eft and format is given
            var autoProcessAccts = [];
            if (paymentType == 'eft' && format) {
                var autoApList = this._dataProvider.GetAutoAPAccounts(format);
                for (var i = 0; i < autoApList.length; i++) {
                    autoProcessAccts.push(autoApList[i].getValue('custrecord_2663_auto_ap'));
                }
            }
            var accountField = this._uiObj.Fields[fieldId];
            accountField.addSelectOption('', '');
            var finalAPList = [];
            for (var i = 0; i < this._uiObj.DataFromSearch.Accounts.length; i++) {
                if (autoProcessAccts.indexOf(this._uiObj.DataFromSearch.Accounts[i].getId()) == -1) {
                    accountField.addSelectOption(this._uiObj.DataFromSearch.Accounts[i].getId(), this._uiObj.DataFromSearch.Accounts[i].getValue('name'));
                    finalAPList.push(this._uiObj.DataFromSearch.Accounts[i].getId());
                }
            }
            if (finalAPList.length == 1) {
                // if there is only 1 account, set it as the default account 
                this._uiObj.RequestParams[fieldId] = finalAPList[0];
            }
        }

    }
    
    /**
     * Prepare department field
     * 
     * @param {String} fieldId
     */
    function prepareDepartmentField(fieldId) {
        if (fieldId && isDepartment()) {
            var departmentField = this._uiObj.Fields[fieldId];
            if (departmentField) {
                var subsidiary = '';
                if (isOneWorld()) {
                    subsidiary = this._uiObj.RequestParams['custpage_2663_subsidiary'];
                }
                if (!this._uiObj.DataFromSearch.Departments) {
                    this._uiObj.DataFromSearch.Departments = this._dataProvider.GetDepartments(subsidiary);
                }
                departmentField.addSelectOption('', '');
                for (var i = 0; i < this._uiObj.DataFromSearch.Departments.length; i++) {
                    departmentField.addSelectOption(this._uiObj.DataFromSearch.Departments[i].getId(), this._uiObj.DataFromSearch.Departments[i].getValue('name'));
                }
            }
        }
    }
    
    /**
     * Prepare class field
     * 
     * @param {String} fieldId
     */
    function prepareClassField(fieldId) {
        if (fieldId && isClass()) {
            var classField = this._uiObj.Fields[fieldId];
            if (classField) {
                var subsidiary = '';
                if (isOneWorld()) {
                    subsidiary = this._uiObj.RequestParams['custpage_2663_subsidiary'];
                }
                if (!this._uiObj.DataFromSearch.Classes) {
                    this._uiObj.DataFromSearch.Classes = this._dataProvider.GetClasses(subsidiary);
                }
                classField.addSelectOption('', '');
                for (var i = 0; i < this._uiObj.DataFromSearch.Classes.length; i++) {
                    classField.addSelectOption(this._uiObj.DataFromSearch.Classes[i].getId(), this._uiObj.DataFromSearch.Classes[i].getValue('name'));
                }
            }
        }
    }
    
    /**
     * Prepare location field
     * 
     * @param {String} fieldId
     */
    function prepareLocationField(fieldId) {
        if (fieldId && isLocation()) {
            var locationField = this._uiObj.Fields[fieldId];
            if (locationField) {
                var subsidiary = '';
                if (isOneWorld()) {
                    subsidiary = this._uiObj.RequestParams['custpage_2663_subsidiary'];
                }
                if (!this._uiObj.DataFromSearch.Locations) {
                    this._uiObj.DataFromSearch.Locations = this._dataProvider.GetLocations(subsidiary);
                }
                locationField.addSelectOption('', '');
                for (var i = 0; i < this._uiObj.DataFromSearch.Locations.length; i++) {
                    locationField.addSelectOption(this._uiObj.DataFromSearch.Locations[i].getId(), this._uiObj.DataFromSearch.Locations[i].getValue('name'));
                }
            }
        }
    }

    /**
     * Prepare entity fields
     */
    function prepareEntityFields() {
        var entityInfoForPaymentType = {
            'eft': [
            {
                fieldId: 'custpage_2663_vendor',
                dataFromSearchObj: this._uiObj.DataFromSearch.Vendors,
                recordFieldId: 'custrecord_2663_parent_vendor'
            },
            {
                fieldId: 'custpage_2663_employee',
                dataFromSearchObj: this._uiObj.DataFromSearch.Employees,
                recordFieldId: 'custrecord_2663_parent_employee'
            },
            {
                fieldId: 'custpage_2663_partner',
                dataFromSearchObj: this._uiObj.DataFromSearch.Partners,
                recordFieldId: 'custrecord_2663_parent_partner'
            }],
            'dd': [
            {
                fieldId: 'custpage_2663_customer',
                dataFromSearchObj: this._uiObj.DataFromSearch.Customers,
                recordFieldId: 'custrecord_2663_parent_customer'
            }],
            'custref': [
            {
                fieldId: 'custpage_2663_customer',
                dataFromSearchObj: this._uiObj.DataFromSearch.Customers,
                recordFieldId: 'custrecord_2663_parent_cust_ref'
            }]
        };
        var paymentType = this._uiObj.RequestParams['custpage_2663_paymenttype'];
        if (entityInfoForPaymentType[paymentType]) {
            var entities = this._dataProvider.GetEntities(paymentType, this._uiObj.RequestParams);

            var entityInfo = entityInfoForPaymentType[paymentType];
            
            // initialize data from search object and blank field
            var entitiesInitFlag = this._uiObj.Entities.length > 0;
            for (var i = 0; i < entityInfo.length; i++) {
                var entityField = this._uiObj.Fields[entityInfo[i].fieldId];
                entityField.addSelectOption('', '');
                entityInfo[i].dataFromSearchObj = [];
            }
            
            for (var i = 0; i < entities.length; i++) {
                var entityFieldId;
                var recordFieldToAdd;
                var dataFromSearchObj;
                for (var j = 0; j < entityInfo.length; j++) {
                    if (entities[i].getValue(entityInfo[j].recordFieldId)) {
                        entityFieldId = entityInfo[j].fieldId;
                        recordFieldToAdd = entityInfo[j].recordFieldId;
                        dataFromSearchObj = entityInfo[j].dataFromSearchObj;
                        break;
                    }
                }
                
                if (entityFieldId && recordFieldToAdd && dataFromSearchObj) {
                    var entityField = this._uiObj.Fields[entityFieldId];
                    entityField.addSelectOption(entities[i].getValue(recordFieldToAdd), entities[i].getText(recordFieldToAdd));
                    dataFromSearchObj.push(entities[i]);
                    if (entitiesInitFlag == false) {
                        this._uiObj.Entities.push(entities[i].getValue(recordFieldToAdd));
                    }
                }
            }
        }
    }
    
    /**
     * Prepare the void cheques field
     */
    function prepareVoidField() {
        var voidField = this._uiObj.Fields['custpage_2663_void'];
        if (this._uiObj.RequestParams['custpage_2663_refresh_page'] == 'T') {
            var isChecked = this._uiObj.RequestParams['custpage_2663_void'] || 'F';
            voidField.setDefaultValue(isChecked);
        }
    }
    
    /**
     * Prepare the exclude cleared cheques field
     */
    function prepareExcludeClearedField() {
        var excludeClearedField = this._uiObj.Fields['custpage_2663_exclude_cleared'];
        if (this._uiObj.RequestParams['custpage_2663_refresh_page'] == 'T') {
            var isChecked = this._uiObj.RequestParams['custpage_2663_exclude_cleared'] || 'F';
            excludeClearedField.setDefaultValue(isChecked);
        }
    }
    
    /**
     * Prepare the cheque from field
     */
    function prepareChequeFromField() {
        if (this._uiObj.RequestParams['custpage_2663_bank_account']) {
            var bankAccount = this._uiObj.RequestParams['custpage_2663_bank_account'];
            var checkFromField = this._uiObj.Fields['custpage_2663_first_check_no'];
            // get max check number for format
            var maxCheckNoForFormat = this._dataProvider.GetLastCheckNumber(bankAccount);
            if (maxCheckNoForFormat && !isNaN(maxCheckNoForFormat)) {
                // add 1 to set as default if there is existing check number, otherwise set to blank
                checkFromField.setDefaultValue(new Number(maxCheckNoForFormat) + 1);
            }
        }
    }

    /**
     * Prepare payment batches to be added to the list
     */
    function preparePaymentBatches() {
        var batchField = this._uiObj.Fields['custpage_2663_batchid'];
        if (batchField) {
            var bankAccount = this._uiObj.RequestParams['custpage_2663_bank_account'];
            // get closed batches for bank account
            if (bankAccount) {
            	this._uiObj.DataFromSearch.PaymentBatches = this._dataProvider.GetBatches(bankAccount, null, [BATCH_OPEN, BATCH_PENDINGAPPROVAL]);
                batchField.addSelectOption('', '');
                var dateFormat = nlapiGetContext().getPreference('DATEFORMAT').replace(/\/|,|-|\.| |/g, '').replace(/on|ONTH/g, 'M');
                var dtUtil = new psg_ep.DateUtil();
                for (var i = 0; i < this._uiObj.DataFromSearch.PaymentBatches.length; i++) {
                    var currBatch = this._uiObj.DataFromSearch.PaymentBatches[i];
                    var dateTime = nlapiStringToDate(currBatch.getValue('custrecord_2663_time_stamp'), 'datetimetz');
                    var batchNumber = (currBatch.getValue('custrecord_2663_number') || 0) * 1 || [dtUtil.formatDateTime(dateTime, dateFormat), dtUtil.formatDateTime(dateTime, 'HHmmss')].join('-');
                    var batchName = [currBatch.getText('custrecord_2663_batch_detail') || nlapiLookupField('customrecord_2663_bank_details', bankAccount, 'name') || currBatch.getId() || '', batchNumber].join('-');
                    
                    batchField.addSelectOption(this._uiObj.DataFromSearch.PaymentBatches[i].getId(), batchName);
                }
            }
        }
    }
    
    /**
     * Prepare the summarized check box if it is displayed (bill payment batches suitelet)
     */
    function prepareSummarizedField() {
        if (this._uiObj.DataFromSearch.SelectedBank && this._uiObj.RequestParams['custpage_2663_batchid']) {
            var summarizedFieldValue = '';
            if (this._uiObj.RequestParams['custpage_2663_summarized_placeholder']) {
                summarizedFieldValue = this._uiObj.RequestParams['custpage_2663_summarized_placeholder'];
            }
            else {
            	summarizedFieldValue = this._uiObj.DataFromSearch.SelectedBank.getValue('custrecord_2663_summarized');
            }
            this._uiObj.RequestParams['custpage_2663_summarized_placeholder'] = summarizedFieldValue;
            this._uiObj.RequestParams['custpage_2663_summarized'] = summarizedFieldValue;
        }
    }
    
    /**
     * Prepare transaction id fields for submitting transactions from batch
     */
    function prepareTransIdFields() {
        if (this._uiObj.RequestParams['custpage_2663_summarized_placeholder'] == 'T' && this._uiObj.RequestParams['custpage_2663_batchid']) {
	        var batch = new _2663.PaymentBatch();
	        batch.load(this._uiObj.RequestParams['custpage_2663_batchid']);
            // add fields
            this._uiObj.Fields['custpage_2663_trans_id'] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, 'custpage_2663_trans_id', psg_ep.BatchInfoFields['custpage_2663_trans_id'], 'custpage_2663_payinfogrp');
            this._uiObj.Fields['custpage_2663_trans_amount'] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, 'custpage_2663_trans_amount', psg_ep.BatchInfoFields['custpage_2663_trans_amount'], 'custpage_2663_payinfogrp');
            this._uiObj.Form.insertField(this._uiObj.Fields['custpage_2663_trans_id'], 'custpage_2663_summarized_placeholder');
            this._uiObj.Form.insertField(this._uiObj.Fields['custpage_2663_trans_amount'], 'custpage_2663_summarized_placeholder');
            // set transaction ids and amounts
            this._uiObj.Fields['custpage_2663_trans_id'].setDefaultValue(JSON.stringify(batch.getTransactionKeys()));
            this._uiObj.Fields['custpage_2663_trans_amount'].setDefaultValue(JSON.stringify(batch.getTransactionAmounts()));
        }            
    }
    
    /**
     * Prepare the accounts payable account for a batch (bill payment batches suitelet)
     */
    function prepareBatchAPAccount() {
        if (this._uiObj.DataFromSearch.SelectedBank && this._uiObj.RequestParams['custpage_2663_batchid'] && this._uiObj.DataFromSearch.PaymentBatches && this._uiObj.DataFromSearch.PaymentBatches.length > 0) {
            this._uiObj.Fields['custpage_2663_auto_ap'] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, 'custpage_2663_auto_ap', psg_ep.BatchInfoFields['custpage_2663_auto_ap'], 'custpage_2663_payinfogrp');
            this._uiObj.Fields['custpage_2663_auto_ap'].setDefaultValue(this._uiObj.DataFromSearch.SelectedBank.getValue('custrecord_2663_auto_ap'));
            for (var i = 0; i < this._uiObj.DataFromSearch.PaymentBatches.length; i++) {
                var currBatch = this._uiObj.DataFromSearch.PaymentBatches[i];
                if (currBatch.getId() == this._uiObj.RequestParams['custpage_2663_batchid']) {
                    // set the account for the batch
                    this._uiObj.Fields['custpage_2663_ap_account'].setDefaultValue(currBatch.getValue('custrecord_2663_account'));
                    this._uiObj.RequestParams['custpage_2663_ap_account'] = currBatch.getValue('custrecord_2663_account');
                    // exit loop after finding selected batch
                    break;
                }
            }
        }
    }
    
    /**
     * Prepare format currency and exchange rate field if it is not yet initialized
     */
    function prepareExchangeRateField(batchFlag) {
        var paymentType = this._uiObj.RequestParams['custpage_2663_paymenttype'];
        var formatCurrencies = this._uiObj.RequestParams['custpage_2663_format_currency'] || '';
        var baseCurrencyValue = this._uiObj.RequestParams['custpage_2663_base_currency'] || '';
        var exchangeRates = this._uiObj.RequestParams['custpage_2663_exchange_rates'] || '';
        var formatValue = this._uiObj.RequestParams['custpage_2663_format_display'] || '';
        if (isMultiCurrency() && this._uiObj.DataFromSearch.SelectedBank && (paymentType == 'eft' || paymentType == 'dd' || paymentType == 'custref') && baseCurrencyValue && formatValue && !formatCurrencies && !exchangeRates) {
            batchFlag = batchFlag || false;
            if (paymentType == 'custref') {
                paymentType = 'eft';
            }
            
            // get format currencies
            var templateFieldId = 'custrecord_2663_' + paymentType + '_template';
            var includeAllCurrencies = this._uiObj.DataFromSearch.SelectedBank.getValue('custrecord_2663_include_all_currencies', templateFieldId) == 'T';
            var formatCurrencyValue = this._uiObj.DataFromSearch.SelectedBank.getValue('custrecord_2663_format_currency', templateFieldId) || '';
            if (batchFlag == true) {
            	includeAllCurrencies = nlapiLookupField('customrecord_2663_payment_file_format', formatValue, 'custrecord_2663_include_all_currencies') == 'T';
            	if (!includeAllCurrencies) {
            		formatCurrencyValue = nlapiLookupField('customrecord_2663_payment_file_format', formatValue, 'custrecord_2663_format_currency') || '';	
            	}
            }
            // set exchange rates and format currencies (if existing)
            var exchangeRateObj = {};
            if (includeAllCurrencies || formatCurrencyValue) {
                var selectedCurrencyArr = includeAllCurrencies ? [] : JSON.parse(formatCurrencyValue);
                var currencyMap = psg_ep.BuildCurrencyMap(selectedCurrencyArr);
                formatCurrencies = JSON.stringify(currencyMap);
                exchangeRateObj = psg_ep.GetExchangeRates(baseCurrencyValue, Object.keys(currencyMap));
            }
            else {
                exchangeRateObj[baseCurrencyValue] = 1;
            }
            exchangeRates = JSON.stringify(exchangeRateObj);
        }
        // create fields
        var insertBeforeFld;
        if (paymentType == 'eft') {
            insertBeforeFld = 'custpage_2663_ap_account';
        }
        else if (paymentType == 'dd') {
            insertBeforeFld = 'custpage_2663_ar_account';
        }
        var formatCurrencyObj = psg_ep.CommonFormFields['custpage_2663_format_currency'];
        formatCurrencyObj.defaultValue = formatCurrencies;
        this._uiObj.Fields['custpage_2663_format_currency'] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, 'custpage_2663_format_currency', formatCurrencyObj, 'custpage_2663_filtergrp');
        this._uiObj.Form.insertField(this._uiObj.Fields['custpage_2663_format_currency'], insertBeforeFld);
        this._uiObj.RequestParams['custpage_2663_format_currency'] = formatCurrencyObj.defaultValue;

        var exchangeRateFieldObj = psg_ep.CommonFormFields['custpage_2663_exchange_rates'];
        exchangeRateFieldObj.defaultValue = exchangeRates;
        this._uiObj.Fields['custpage_2663_exchange_rates'] = psg_ep.FormUtils.CreateFieldFromObject(this._uiObj.Form, 'custpage_2663_exchange_rates', exchangeRateFieldObj, 'custpage_2663_filtergrp');
        this._uiObj.Form.insertField(this._uiObj.Fields['custpage_2663_exchange_rates'], insertBeforeFld);
        this._uiObj.RequestParams['custpage_2663_exchange_rates'] = exchangeRateFieldObj.defaultValue;
    }

    /**
     * Prepare field containing email templates
     *
     */
    function prepareEmailTemplates() {
        
        // get preferred email templates
        var emailTemplate = '';
        var paymentType = nlapiLookupField('customrecord_2663_file_admin', this._uiObj.RequestParams['custpage_2663_file_id'], 'custrecord_2663_payment_type', true) || '';
        var arrColumns = [];
        arrColumns.push(new nlobjSearchColumn('custrecord_ep_eft_email_template'));
        arrColumns.push(new nlobjSearchColumn('custrecord_ep_dd_email_template'));
        var arrRes = nlapiSearchRecord('customrecord_ep_preference', null, null, arrColumns);        
        if (arrRes){
            var recEpPref = arrRes[0];
            emailTemplate = (paymentType == 'EFT') ? recEpPref.getValue('custrecord_ep_eft_email_template') || '' : recEpPref.getValue('custrecord_ep_dd_email_template') || '';
        }

        // populate email template select field
        var emailTemplateField = this._uiObj.Fields['custpage_2663_email_templates'];        
        var arrEmailTemplateObj = this._dataProvider.GetEmailTemplates() || [];        
        emailTemplateField.addSelectOption('','');
        for (var i in arrEmailTemplateObj){        
            var objEmailTemplate = JSON.parse(arrEmailTemplateObj[i]);
            emailTemplateField.addSelectOption(objEmailTemplate.id, objEmailTemplate.name, emailTemplate == objEmailTemplate.name);
        }

    }
     
    
    this.PrepareBankAccountField = prepareBankAccountField;
    this.PreparePostingPeriodField = preparePostingPeriodField;
    this.PrepareAggregationField = prepareAggregationField;
    this.PrepareAggregateMethodField = prepareAggregateMethodField;
    this.PrepareTransactionTypeField = prepareTransactionTypeField;
    this.PrepareBankAccountRelatedInfo = prepareBankAccountRelatedInfo;
    this.PrepareFormatRelatedInfo = prepareFormatRelatedInfo;
    this.PrepareAccountField = prepareAccountField;
    this.PrepareDepartmentField = prepareDepartmentField;
    this.PrepareClassField = prepareClassField;
    this.PrepareLocationField = prepareLocationField;
    this.PrepareEntityFields = prepareEntityFields;
    this.PrepareVoidField = prepareVoidField;
    this.PrepareExcludeClearedField = prepareExcludeClearedField;
    this.PrepareChequeFromField = prepareChequeFromField;
    this.PreparePaymentBatches = preparePaymentBatches;
    this.PrepareSummarizedField = prepareSummarizedField;
    this.PrepareTransIdFields = prepareTransIdFields;
    this.PrepareBatchAPAccount = prepareBatchAPAccount;
    this.PrepareExchangeRateField = prepareExchangeRateField;
    this.PrepareEmailTemplates = prepareEmailTemplates;
};
/**
 * @author alaurito
 *
 * include list : 2663_lib.js
 */

/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2012/08/01  227867         2.01.1                  Refactor for payment portal functionality
 * 2012/09/21  231393         2.01.1                  Modifications for test automation scripts
 * 2013/03/04  244623         2.01.1                  Update markPropDisp with value from marked line
 * 2013/03/05  244623         2.01.1                  Fix typo error
 * 2013/03/11  245309         2.00.10                 Add maxLength property to psg_ep.SublistColumn
 * 2013/04/15  248888   	  2.00.12				  Save added sublist fields into sublistFields obj
 * 2013/03/04  277907   	  2.00				  	  Remove code to alter sublist length if account contains "SB" 
 */

var psg_ep;

if (!psg_ep)
    psg_ep = {};

/**
 * Interface to implement for sublist object
 * 
 * @returns {psg_ep.SublistInterface}
 */
psg_ep.SublistInterface = function() {
    this.GetColumns = null;
};

/**
 * Parent class for sublist
 * 
 * @param {nlobjForm} form
 * @param {String} name
 * @param {String} type
 * @param {String} label
 * @param {String} tab
 * @param {psg_ep.SublistInterface} sublistInterfaceObj
 * @returns {psg_ep.Sublist}
 */
psg_ep.Sublist = function(form, name, type, label, tab, sublistInterfaceObj) {
    // check form
    if (!form) {
        throw nlapiCreateError('EP_FORM_IS_NULL', 'Form should be provided to create a sublist object.', true);
    }

    // check name
    if (!name) {
        throw nlapiCreateError('EP_NAME_IS_NULL', 'Name should be provided to create a sublist object.', true);
    }

    // check type
    if (!type) {
        throw nlapiCreateError('EP_TYPE_IS_NULL', 'Type should be provided to create a sublist object.', true);
    }
    var supportedTypes = ['list'];
    if (supportedTypes.indexOf(type) == -1) {
        throw nlapiCreateError('EP_TYPE_NOT_SUPPORTED', 'Type is not supported.');
    }

    // check label
    if (!label) {
        throw nlapiCreateError('EP_LABEL_IS_NULL', 'Label should be provided to create a sublist object.', true);
    }
    
    // check subtab
    if (!tab) {
        throw nlapiCreateError('EP_SUBTAB_IS_NULL', 'Subtab should be provided to create a sublist object.', true);
    }
    
    // check sublist interface
    if (!sublistInterfaceObj) {
        throw nlapiCreateError('EP_INTERFACE_OBJ_NULL', 'Cannot create object without interface for Sublist', true);
    }
    
    // assign argument objects
    this._form = form;
    this._sublistId = name;
    this._sublistType = type;
    this._sublistLabel = label;
    this._subtab = tab;
    this._sublistInterfaceObj = sublistInterfaceObj;
    
    // sublist properties
    nlapiLogExecution('debug', '[ep] Sublist', 'id: ' + this._sublistId + ', type: ' + this._sublistType + ', label: ' + this._sublistLabel + ', tab: ' + this._subtab);
    this._sublistObj = form.addSubList(this._sublistId, this._sublistType, this._sublistLabel, this._subtab);
    this._fields = {};
    this._sublistFields = {};
    this._requestParams = {};
    this._columns = {};
    this._markKey = '';
    this._markCol = '';
    this._markProperty = '';
    this._markPropertyDisplay = '';
    this._maxSelect = 5000;
    this._maxInSublist = nlapiGetContext().getSetting('script', 'custscript_2663_max_lines_in_sublist') || 50000;
    
    var hiddenPageInfoFields = {};
    hiddenPageInfoFields[this._sublistId + '_maxinpage'] = {
        type: 'integer',
        label: 'Max lines in page',
        displayType: 'hidden',
        defaultValue: '50'
    };
    hiddenPageInfoFields[this._sublistId + '_currpage'] = {
        type: 'integer',
        label: 'Current page',
        displayType: 'hidden',
        defaultValue: '1'
    };
    hiddenPageInfoFields[this._sublistId + '_numpages'] = {
        type: 'integer',
        label: 'Number of pages',
        displayType: 'hidden',
        defaultValue: '1'
    };
    
    var hiddenFields = {};
    hiddenFields['custpage_sublist_ep_name'] = {
        type: 'text',
        label: 'Sublist Id',
        displayType: 'hidden',
        defaultValue: name
    };
    hiddenFields[this._sublistId + '_mark_all'] = {
        type: 'text',
        label: 'Mark all',
        displayType: 'hidden',
        defaultValue: ''
    };
    hiddenFields[this._sublistId + '_columns'] = {
        type: 'longtext',
        label: 'Sublist columns',
        displayType: 'hidden',
        defaultValue: ''
    };
    hiddenFields[this._sublistId + '_markcolumn'] = {
        type: 'text',
        label: 'Sublist mark column',
        displayType: 'hidden',
        defaultValue: ''
    };
    
    hiddenFields[this._sublistId + '_markkey'] = {
        type: 'text',
        label: 'Sublist mark key',
        displayType: 'hidden',
        defaultValue: ''
    };
    hiddenFields[this._sublistId + '_markobjprop'] = {
        type: 'text',
        label: 'Sublist mark obj property',
        displayType: 'hidden',
        defaultValue: ''
    };
    hiddenFields[this._sublistId + '_markobjdispprop'] = {
        type: 'text',
        label: 'Sublist mark obj display property',
        displayType: 'hidden',
        defaultValue: ''
    };
    hiddenFields[this._sublistId + '_maxinsublist'] = {
        type: 'integer',
        label: 'Max in sublist',
        displayType: 'hidden',
        defaultValue: nlapiGetContext().getSetting('script', 'custscript_2663_max_lines_in_sublist') || 50000
    };
    hiddenFields[this._sublistId + '_maxselect'] = {
        type: 'integer',
        label: 'Max to select',
        displayType: 'hidden',
        defaultValue: '5000'
    };

    /**
     * Builds the sublist UI
     */
    function buildUI() {
        if (verifyInterface(this._sublistInterfaceObj) == true) {
            // add hidden fields
            nlapiLogExecution('debug', '[ep] Sublist:buildUI', 'Adding the hidden fields...');
            for (var i in hiddenFields) {
                this._fields[i] = psg_ep.FormUtils.CreateFieldFromObject(this._form, i, hiddenFields[i], 'main');
            }
            
            // add the columns
            this._columns = this._sublistInterfaceObj.GetColumns();
            if (this._columns) {
                nlapiLogExecution('debug', '[ep] Sublist:buildUI', 'Add column list object...');
                var columnsField = this._fields[this._sublistId + '_columns'];
                columnsField.setDefaultValue(JSON.stringify(this._columns));
                
                nlapiLogExecution('debug', '[ep] Sublist:buildUI', 'Adding the columns to sublist...');
                for (var i in this._columns) {
                    if (this._columns[i] instanceof psg_ep.SublistColumn) {
                        var fld = this._sublistObj.addField(this._columns[i].name, this._columns[i].type, this._columns[i].label);
                        this._sublistFields[this._columns[i].name] = fld;
                        fld.setDisplayType(this._columns[i].displayType);
                        fld.setDefaultValue(this._columns[i].defaultValue);
                        if (this._columns[i].maxLength) {
                        	fld.setMaxLength(this._columns[i].maxLength);
                        }
                        
                        if (this._columns[i].markCol == true && !this._markCol) {
                            var markColField = this._fields[this._sublistId + '_markcolumn'];
                            markColField.setDefaultValue(this._columns[i].name);
                            this._markCol = this._columns[i].name;
                        } 
                        else if (this._columns[i].markKey == true && !this._markKey) {
                            var markKeyField = this._fields[this._sublistId + '_markkey'];
                            markKeyField.setDefaultValue(this._columns[i].name);
                            this._markKey = this._columns[i].name;
                        }
                        else if (this._columns[i].markObjectProperty == true && !this._markProperty) {
                            var markObjPropField = this._fields[this._sublistId + '_markobjprop'];
                            markObjPropField.setDefaultValue(this._columns[i].name);
                            this._markProperty = this._columns[i].name;
                        }
                        else if (this._columns[i].markObjectPropertyDisplay == true && !this._markPropertyDisplay) {
                            var markObjPropDispField = this._fields[this._sublistId + '_markobjdispprop'];
                            markObjPropDispField.setDefaultValue(this._columns[i].name);
                            this._markPropertyDisplay = this._columns[i].name;
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Initializes the request parameters based from last refresh
     * 
     * @param {nlobjRequest} request
     */
    function initializeRequestParameters(request) {
        if (request) {
            for (var i in this._fields) {
                if (request.getParameter(i)) {
                    this._requestParams[i] = request.getParameter(i);
                }
            }
            for (var i in hiddenPageInfoFields) {
                if (request.getParameter(i)) {
                    this._requestParams[i] = request.getParameter(i);
                }
            }
            var sublistId = this._requestParams['custpage_sublist_ep_name'];
            var numPagesStr = this._requestParams[sublistId + '_numpages'];
            var numPages = numPagesStr != null ? new Number(numPagesStr) : 0;
            for (var i = 1; i <= numPages; i++) {
                if (request.getParameter(sublistId + '_markdata' + i)) {
                    this._requestParams[sublistId + '_markdata' + i] = request.getParameter(sublistId + '_markdata' + i);
                }
            }
            nlapiLogExecution('debug', '[ep] Sublist:initializeRequestParameters', 'request params: ' + JSON.stringify(this._requestParams));
        }
    }
    
    /**
     * 
     * @param {Array} data
     */
    function setSublistData(data) {
        if (data && data.length > 0) {
            var sublistPageInfo = updateSublistPageInfo(this, data);
            
            this._sublistObj.setLineItemValues(sublistPageInfo.sublistDisplayData);
            
            // add sublist page selectors
            addSublistBrowser(this, sublistPageInfo);
            
        }
    }
    
    /**
     * Sets the error message in the sublist
     * 
     * @param {String} source
     * @param {String} errorMessage
     */
    function setErrorMessage(source, errorMessage) {
        if (source && errorMessage) {
            nlapiLogExecution('error', source, errorMessage);
            var errorFlagField = this._form.addField(this._sublistId + '_load_errmsg', 'text', '', null, this._subtab);
            errorFlagField.setDisplayType('inline');
            errorFlagField.setDefaultValue('<font color="red">' + errorMessage + '</font>');
        }
    }
    
    /**
     * Add button to sublist
     * 
     * @param {String} buttonId
     * @param {String} label
     * @param {String} func
     */
    function addButton(buttonId, label, func) {
        if (buttonId && label && func) {
            this._sublistObj.addButton(this._sublistId + '_' + buttonId, label, func);
        }
    }
    
    /**
     * Updates the sublist page info and marked lines
     * 
     * @param thisObj
     * @param data
     * @returns {}
     */
    function updateSublistPageInfo(thisObj, data) {
        var currPage = 1;
        var newNumPages = 1;
        var maxLinesInPage = 50;
        var sublistLength = 0;
        var sublistDisplayData = [];
        var markedDataPages = {};
        if (thisObj && data && data.length > 0) {
            var sublistPageInfo = getSublistPageInfo(thisObj, data);
            sublistLength = sublistPageInfo.sublistLength;
            newNumPages = sublistPageInfo.newNumPages;
            currPage = sublistPageInfo.currPage;
            maxLinesInPage = sublistPageInfo.maxLinesInPage;
            
            // create empty marked object lists
            for (var i = 1; i <= sublistPageInfo.newNumPages; i++) {
                markedDataPages[thisObj._sublistId + '_markdata' + i] = {};
            } 
            
            // get marked objects parameters
            var markCol = thisObj._markCol;
            var markKey = thisObj._markKey;
            var markProp = thisObj._markProperty;;
            var markPropDisp = thisObj._markPropertyDisplay;
            var markAllFlag = thisObj._requestParams[thisObj._sublistId + '_mark_all'];
            var maxSelect = thisObj._maxSelect;
            var sublistMarkedMaxLength = data.length;
            if (maxSelect && !isNaN(maxSelect) && maxSelect < data.length) {
                sublistMarkedMaxLength = maxSelect;
            }
            nlapiLogExecution('debug', '[ep] Sublist:updateSublistPageInfo', 'mark col: ' + markCol + ', mark key: ' + markKey + ', mark property: ' + markProp + ', mark all flag: ' + markAllFlag + ', max select length: ' + sublistMarkedMaxLength + ', mark display prop: ' + markPropDisp);
            
            // get data to be displayed in sublist's start and end index
            var startLine = (currPage - 1) * maxLinesInPage;
            var endLine = sublistPageInfo.sublistLength < currPage * maxLinesInPage ? sublistPageInfo.sublistLength : currPage * maxLinesInPage;
            nlapiLogExecution('debug', '[ep] Sublist:updateSublistPageInfo', 'display in sublist - idx: ' + startLine + ' to ' + endLine);
            
            var fieldsWithDefaultValue = [];
            for (var i in thisObj._columns) {
                if (thisObj._columns[i].defaultValue && thisObj._columns[i].markObjectProperty == false) {
                    fieldsWithDefaultValue.push(thisObj._columns[i].name);
                }
            }
            
            var markedLines = 0;
            for (var i = 0; i < sublistPageInfo.sublistLength; i++) {
                // set default values (if not set)
                for (var j = 0; j < fieldsWithDefaultValue.length; j++) {
                    if (!data[i][fieldsWithDefaultValue[j]]) {
                        data[i][fieldsWithDefaultValue[j]] = thisObj._columns[fieldsWithDefaultValue[j]].defaultValue; 
                    }
                }
                // set marked data
                if (markCol && markKey && markProp) {
                    var markKeyVal = data[i][markKey];
                    if (markAllFlag == 'T' || sublistPageInfo.currMarkedData[markKeyVal]) {
                        if (markedLines >= sublistMarkedMaxLength) {
                            nlapiLogExecution('debug', '[ep] Sublist:updateSublistPageInfo', 'Maximum number of marked lines reached: ' + sublistMarkedMaxLength);
                            break;
                        }
                        var pageNum = Math.floor(i / maxLinesInPage) + 1;
                        var markPageNumId = thisObj._sublistId + '_markdata' + pageNum;
                        if (markAllFlag == 'T') {
                            // set from property
                            markedDataPages[markPageNumId][markKeyVal] = data[i][markProp];
                        }
                        else {
                            // set from marked data
                            markedDataPages[markPageNumId][markKeyVal] = sublistPageInfo.currMarkedData[markKeyVal];
                            data[i][markProp] = sublistPageInfo.currMarkedData[markKeyVal];
                        }
                        if (markPropDisp) {
                        	var markedPropObj = JSON.parse(sublistPageInfo.currMarkedData[markKeyVal] || '{}');
                        	var markedPropName = markPropDisp == 'custpage_payment' ? 'amount' : markPropDisp;
                        	if (markedPropObj[[markedPropName]]) {
                        		markedDataPages[markPageNumId][markKeyVal] = sublistPageInfo.currMarkedData[markKeyVal];
                        		data[i][markPropDisp] = markedPropObj[markedPropName];
                        	}
                        }
                        
                        // set as checked
                        data[i][markCol] = 'T';
                        // increment lines
                        markedLines++;
                    }
                    else {
                        if (thisObj._columns[markProp].defaultValue) {
                            data[i][markProp] = thisObj._columns[markProp].defaultValue;
                        }
                        if (markPropDisp && thisObj._columns[markPropDisp].defaultValue) {
                            data[i][markPropDisp] = thisObj._columns[markPropDisp].defaultValue;
                        }
                    }
                }
                
                // add sublist line
                if (i >= startLine && i < endLine) {
                    sublistDisplayData.push(data[i]);
                }
            }
            
            // update sublist page info
            nlapiLogExecution('debug', '[ep] Sublist:buildUI', 'Adding the hidden page info fields...');
            var maxLinesFieldObj = hiddenPageInfoFields[thisObj._sublistId + '_maxinpage'];
            maxLinesFieldObj.defaultValue = maxLinesInPage;
            thisObj._fields[thisObj._sublistId + '_maxinpage'] = psg_ep.FormUtils.CreateFieldFromObject(thisObj._form, thisObj._sublistId + '_maxinpage', maxLinesFieldObj, 'main');
            var currPageFieldObj = hiddenPageInfoFields[thisObj._sublistId + '_currpage'];
            currPageFieldObj.defaultValue = currPage;
            thisObj._fields[thisObj._sublistId + '_currpage'] = psg_ep.FormUtils.CreateFieldFromObject(thisObj._form, thisObj._sublistId + '_currpage', currPageFieldObj, 'main');
            var numPagesFieldObj = hiddenPageInfoFields[thisObj._sublistId + '_numpages'];
            numPagesFieldObj.defaultValue = newNumPages;
            thisObj._fields[thisObj._sublistId + '_numpages'] = psg_ep.FormUtils.CreateFieldFromObject(thisObj._form, thisObj._sublistId + '_numpages', numPagesFieldObj, 'main');

            // update marked data fields
            for (var i in markedDataPages) {
                var hiddenMarkedField = {};
                hiddenMarkedField[i] = {
                    type: 'longtext',
                    label: 'Marked Data - ' + i,
                    displayType: 'hidden',
                    defaultValue: JSON.stringify(markedDataPages[i])
                };
                thisObj._fields[i] = psg_ep.FormUtils.CreateFieldFromObject(thisObj._form, i, hiddenMarkedField[i], 'main');
            }
            
            // update mark all field
            var markAllField = thisObj._fields[thisObj._sublistId + '_mark_all'];
            markAllField.setDefaultValue(markAllFlag);
        }
        
        return {
            maxLinesInPage: maxLinesInPage,
            currPage: currPage,
            newNumPages: newNumPages,
            sublistLength: sublistLength,
            sublistDisplayData: sublistDisplayData,
            markedDataPages: markedDataPages
        };
    }
    
    /**
     * Gets the sublist page info object from the request parameters
     * 
     * @param thisObj
     * @param data
     * @returns {}
     */
    function getSublistPageInfo(thisObj, data) {
        var maxLinesInPage = 50;
        var currPage = 1;
        var newNumPages = 1;
        var currMarkedData = {};
        var sublistLength = 0;
        if (thisObj && data && data.length > 0) {
            nlapiLogExecution('debug', '[ep] Sublist:getSublistPageInfo', 'Getting page info from parameters...');
            maxLinesInPage = thisObj._requestParams[thisObj._sublistId + '_maxinpage'] || maxLinesInPage;
            currPage = thisObj._requestParams[thisObj._sublistId + '_currpage'] || currPage;
            var numPages = thisObj._requestParams[thisObj._sublistId + '_numpages'];
            if (numPages) {
                for (var i = 1; i <= numPages; i++) {
                    var markedDataStr = thisObj._requestParams[thisObj._sublistId + '_markdata' + i] || '{}';
                    var markedDataObjects = JSON.parse(markedDataStr);
                    for (var j in markedDataObjects) {
                        currMarkedData[j] = markedDataObjects[j];
                    }
                }
            }
            
            sublistLength = data.length;
            var maxInSublist = thisObj._maxInSublist;
            if (maxInSublist < data.length) {
                nlapiLogExecution('debug', '[ep] Sublist:getSublistPageInfo', 'Data length: '+ data.length + ' is greater than allowed in sublist: ' + thisObj._maxInSublist);
                sublistLength = maxInSublist;
            }
            newNumPages = Math.ceil(sublistLength / maxLinesInPage);
            currPage = numPages != newNumPages ? 1 : currPage;
            
            nlapiLogExecution('debug', '[ep] Sublist:getSublistPageInfo', 'Sublist page info: Sublist length = ' + sublistLength +', MaxLinesInPage = ' + maxLinesInPage + ', CurrPage = ' + currPage + ', NewNumPages = ' + newNumPages);
        }
        return {
            maxLinesInPage: maxLinesInPage,
            currPage: currPage,
            newNumPages: newNumPages,
            currMarkedData: currMarkedData,
            sublistLength: sublistLength
        };
    }
    
    /**
     * Adds the sublist browser
     * 
     * @param thisObj
     * @param sublistPageInfo
     */
    function addSublistBrowser(thisObj, sublistPageInfo) {
        if (thisObj && sublistPageInfo) {
            // initialize page info
            var currentPage = sublistPageInfo.currPage || 1;
            var maxLinesInPage = sublistPageInfo.maxLinesInPage || 50;
            var numPages = sublistPageInfo.newNumPages || 1;
            var markedTransactions = sublistPageInfo.markedDataPages || {};
            var sublistLength = sublistPageInfo.sublistLength;
            
            // add page dropdown field in subtab
            var fld = thisObj._form.addField(thisObj._sublistId + '_page', 'select', 'Select Page', null, thisObj._subtab);
            var helpText = 'Select the sublist page.';
            helpText += markedTransactions != null ? ' Pages with marked transactions are indicated by "(*)"' : '';
            fld.setHelpText(helpText);
            fld.setLayoutType('midrow', 'none');
            
            for (var i = 1; i <= numPages; i++) {
                var startNum = maxLinesInPage * (i - 1) + 1;
                var endNum = ((maxLinesInPage * i) > sublistLength) ? sublistLength : (maxLinesInPage * i);
                var markedTransactionId = thisObj._sublistId + '_markdata' + i;
                var markedLineIndicator = JSON.stringify(markedTransactions[markedTransactionId]) != '{}' ? ' (*)' : '';
                fld.addSelectOption(i, startNum + ' to ' + endNum + ' of ' + sublistLength + markedLineIndicator, false);
            }
            fld.setDefaultValue(currentPage);
            
            // add max transactions dropdown field in subtab
            fld = thisObj._form.addField(thisObj._sublistId + '_max', 'select', 'Transactions per Page', null, thisObj._subtab);
            fld.setHelpText('Select the number of transactions to be displayed on the page. Maximum is 500 transactions per page.');
            var maxTransactions = [50, 100, 200, 300, 400, 500];
            for (var i = 0; i < maxTransactions.length; i++) {
                fld.addSelectOption(maxTransactions[i], maxTransactions[i]);
            }
            fld.setDefaultValue(maxLinesInPage);
            fld.setLayoutType('midrow', 'none');
        }
    }

    
    /**
     * Function to check the validity of interface object before making the object functions accessible
     * 
     * @param {psg_ep.SublistInterface} interfaceObj
     * @returns {Boolean}
     */
    function verifyInterface(interfaceObj) {
        return (interfaceObj &&
                interfaceObj.GetColumns && 
                typeof interfaceObj.GetColumns == 'function'); 
    }
    
    this._buildUI = buildUI;
    this._setSublistData = setSublistData;
    this._setErrorMessage = setErrorMessage;
    this._addButton = addButton;
    this._initializeRequestParameters = initializeRequestParameters;
};

/**
 * Class for the sublist column
 * 
 * @param {String} name
 * @param {String} type
 * @param {String} label
 * @param {String} displayType
 * @param {String} defaultValue
 * @param {Boolean} markCol
 * @param {Boolean} markKey
 * @param {Boolean} markObjectProperty
 * @param {Boolean} markObjectPropertyDisplay
 * @returns {psg_ep.SublistColumn}
 */
psg_ep.SublistColumn = function(name, type, label, displayType, defaultValue, markCol, markKey, markObjectProperty, markObjectPropertyDisplay, maxLength) {
    if (!name && !type && !label) {
        throw nlapiCreateError('EP_INCOMPLETE_SUBLIST_COL_PARAMS', 'Incomplete sublist column parameters.', true);
    }
    this.name = name;
    this.type = type;
    this.label = label;
    this.displayType = displayType || 'normal';
    this.defaultValue = defaultValue || '';
    this.maxLength = maxLength;
    this.markCol = markCol || false;
    this.markKey = markKey || false;
    this.markObjectProperty = markObjectProperty || false;
    this.markObjectPropertyDisplay = markObjectPropertyDisplay || false;
};
/**
 * @author alaurito
 *
 * include list : 2663_lib.js
 */

/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2012/08/01  227867         2.01.1                  Refactor for payment portal functionality
 * 2012/08/03  227868         2.01.1                  Changes based on FRD
 * 2012/08/13  227867         2.01.1                  Add support for customer refund 
 * 2012/08/16  227868         2.01.1                  Change banks to be listed to those with
 *                                                    closed batches
 * 2012/08/23  227868         2.01.1                  Do not show the Deferred batch option when 
 *                                                    Process Bills Automatically flag is false 
 * 2012/09/10  227868         2.01.1                  Add search for summarized data
 * 2012/09/21  231393         2.01.1                  Modifications for test automation scripts
 * 2012/09/28  231712         2.01.1                  Fixes for unit test automation scripts
 * 2012/10/08  232986         2.00.2.2012.10.11.2     Search for transactions by batches of entities
 * 2012/10/25  233946         2.01.1                  Modify search to include the auto-AP for bank;
 *                                                    get all closed batches of a bank regardless of
 *                                                    AP; get auto-ap list for a given format
 * 2012/11/14  235650         2.00.2                  Also include ap filter in search for closed
 *                                                    batches if it is specified       
 * 2012/11/15  235654         2.00.3                  Change max entity in batch to 1000; pass request
 *                                                    params when converting results to sublist data 
 * 2012/11/27  236622         2.00.3                  Use saved searches in order to allow performance
 *                                                    team to add hints (only for Bill Payment suitelet)
 * 2012/11/22  237873         2.00.3                  Add columns in for currency related fields when
 *                                                    retrieving company bank details
 * 2013/01/08  239643         2.00.6                  Add Include All Currencies column
 * 2013/01/15  238208         2.00.5                  Add sorting for Positive Pay
 * 2013/02/12  242626         2.00.5                  Use function psg_ep.isGrouponAccount to check if account is Groupon
 * 2013/02/15  243221         2.00.5                  Add search filter for custentity_hold_payment_vendor for Groupon
 * 2013/03/11  245306         2.00.10                 Remove closed search filter
 * 2013/04/15  248888   	  2.00.12				  Use batchid when searching for Batch transactions
 * 2013/04/17  249111   	  2.00.13				  Add support for auto-numbering for batches
 * 2013/04/18  249273   	  2.00.13				  Add status filter only when status parameter is not null and length > 0
 * 2013/05/23  254321   	  2.00.18				  Use optimized transaction search
 * 2013/06/26  255234   	  2.00.18				  Use saved search in entity search
 * 2013/07/17  256768   	  2.00.18				  Apply entity formula filters if saved search is customsearch_ep_ap_trans_search
 * 2013/07/18  257505 		  2.00.18				  Add time limit handling on function getFormatEntities
 * 2013/07/15  245723		  2.00.10				  Add support for commission transactions
 * 2013/08/29  261772 		  2.00.25.2013.08.29.3	  Added checking for employee commission feature
 * 2013/09/23  255091		  2.00.18				  Added setup of direct debit type dropdown fields
 *													  Added filter to not include invoices from an entity bank with final payment date
 * 2013/09/24  263190		  3.00.00				  Add handling for approval routing
 * 2013/09/25  263190 		  3.00.00				  Use _2663.Search in transaction and entity search
 * 2013/09/26  264504 		  3.00.0.2013.09.26.5	  Fix for undefined variable startTime during entity search
 * 2013/09/26  263190 		  3.00.00				  Rename variables with Portal to Payment
 * 2013/09/27  263190 		  3.00.00				  Add BATCH_OPEN status when searching for bank accounts
 * 2013/10/01  255091		  2.00.18				  Use additional entity bank filter only for SEPA Direct Debit (Germany)
 * 2013/10/05  265406 		  3.00.00				  Refactor code to use Payment File Administration record instead of Payment Batch
 * 2013/10/07  265406 		  3.00.00				  Remove reference to other batch status constants
 * 2013/11/07  267720  		  3.00.00				  Add option to use old transaction search
 * 2013/11/07  267718  		  3.00.00				  Add transaction internalid filters to make search faster 
 * 2013/11/26                 3.00.00				  Add method to retrieve email templates
 * 2013/12/12  256855 		  3.00.00     			  Add support for SEPA Direct Debit (CBI)
 * 2014/01/07  274281  		  3.00.00				  Use old search when getting format entities
 */

var psg_ep;

if (!psg_ep)
    psg_ep = {};

/**
 * Parent class that provides the data for the form fields
 * 
 * @returns {psg_ep.FormFieldDataProvider}
 */
psg_ep.FormFieldDataProvider = function() {
	this.MaxTime = 240000;
	
    /**
     * Searches for available bank accounts based on payment type (eft/dd/pp)
     * 
     * @param {String} type
     * @returns {Array}
     */
    function getBankAccounts(type) {
        var searchResults;
        if (type == 'eft' || type == 'dd' || type == 'pp') {
            // Get bank accounts based on available formats for license
            var typeText = {'eft': 'EFT', 'dd' : 'DD', 'pp' : 'Positive Pay'};
            var allowedFormats = (new _2663.FormatRestrictor()).GetAllowedFormats(typeText[type]);
            var formatFilter = [];
            for (var i in allowedFormats) {
                formatFilter.push(i);
            }
            
            // Search only when there are allowed formats for the edition
            if (formatFilter.length > 0) {
                var filters = [];
                filters.push(new nlobjSearchFilter('custrecord_2663_' + type + '_template', null, 'anyof', formatFilter));
                
                // If OW, search for accounts under available subsidiaries
                if (isOneWorld()) {
                    var subsidiaries = [];
                    var dummyForm = nlapiCreateForm('dummy form');
                    var dummySubField = dummyForm.addField('custpage_2663_dummy_subsidiary', 'select', 'Dummy Sub', 'subsidiary');
                    var subOptions = dummySubField.getSelectOptions();
                    if (subOptions) {
                        for (var i = 0; i < subOptions.length; i++) {
                            subsidiaries.push(subOptions[i].getId());
                        }
                    }
                    
                    if (subsidiaries.length > 0) {
                        filters.push(new nlobjSearchFilter('custrecord_2663_subsidiary', null, 'anyof', subsidiaries.join()));
                    }
                }
                    
                
                // only get active records
                filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
                
                var columns = [];
                columns.push(new nlobjSearchColumn('name').setSort());
                columns.push(new nlobjSearchColumn('custrecord_2663_' + type + '_template'));
                columns.push(new nlobjSearchColumn('custrecord_2663_max_lines', 'custrecord_2663_' + type + '_template'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_department'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_class'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_location'));
                columns.push(new nlobjSearchColumn('custrecord_2663_subsidiary'));
                columns.push(new nlobjSearchColumn('custrecord_2663_trans_marked'));
                columns.push(new nlobjSearchColumn('custrecord_2663_currency'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_approval_type'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_payment_limit'));
                columns.push(new nlobjSearchColumn('custrecord_2663_format_currency', 'custrecord_2663_' + type + '_template'));
                columns.push(new nlobjSearchColumn('custrecord_2663_include_all_currencies', 'custrecord_2663_' + type + '_template'));
                if (type == 'eft' || type == 'dd') {
                    columns.push(new nlobjSearchColumn('custrecord_2663_' + type + '_batch'));
                }
                
                searchResults = nlapiSearchRecord('customrecord_2663_bank_details', null, filters, columns);
            }
        }
    
        return searchResults || [];
    }
    
    /**
     * Returns the list of posting periods
     * 
     * @returns {Array}
     */
    function getPostingPeriods(){
        // Set a list of open posting periods
        var filters = [];
        var columns = [];
        
        filters.push(new nlobjSearchFilter('isadjust', null, 'is', 'F'));
        filters.push(new nlobjSearchFilter('isquarter', null, 'is', 'F'));
        filters.push(new nlobjSearchFilter('isyear', null, 'is', 'F'));
        //filters.push(new nlobjSearchFilter('closed', null, 'is', 'F'));

        columns.push(new nlobjSearchColumn('periodname'));
        columns.push(new nlobjSearchColumn('startdate').setSort());
        columns.push(new nlobjSearchColumn('enddate'));
        columns.push(new nlobjSearchColumn('closed'));
        
        var searchResults = nlapiSearchRecord('accountingperiod', null, filters, columns);
        
        return searchResults || [];
    }
    
    /**
     * Get the aggregation methods set by the user
     * 
     * @returns {Array}
     */
    function getAggregationMethods() {
        // Only get active records
        var filters = [];
        filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
        
        var columns = [];
        
        columns.push(new nlobjSearchColumn('custrecord_2663_agg_field_id'));
        columns.push(new nlobjSearchColumn('name').setSort());
        
        return nlapiSearchRecord('customrecord_2663_payment_aggregation', null, filters, columns) || [];
    }

    /**
     * Get the direct debit types
     * 
     * @returns {Array}
     */
    function getDirectDebitTypes() {
        // Only get active records
        var filters = [];
        filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
        
        var columns = [];
        
        columns.push(new nlobjSearchColumn('internalid'));
        columns.push(new nlobjSearchColumn('name').setSort());
        
        return nlapiSearchRecord('customlist_2663_direct_debit_type', null, filters, columns) || [];
    }
    
    /**
     * Get list of accounts based on subsidiary and account type
     * 
     * @param {String} subsidiary
     * @param {String} acctType
     * @returns {Array}
     */
    function getAccounts(subsidiary, acctType) {
        
        var searchResults;
        if ((!isOneWorld() || (isOneWorld() && subsidiary)) && acctType) {
            var filters = [];
            var columns = [];

            // accounts for subsidiary
            if (isOneWorld() && subsidiary) {
                filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));
            }
            
            // get accounts that are of given type
            filters.push(new nlobjSearchFilter('type', null, 'anyof', acctType));
            
            // do not get inactive accounts
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            
            columns.push(new nlobjSearchColumn('name').setSort());
            
            searchResults = nlapiSearchRecord('account', null, filters, columns);
        }

        return searchResults || [];
    }
    
    /**
     * Get list of departments
     * 
     * @param {String} subsidiary
     * @returns {Array}
     */
    function getDepartments(subsidiary) {
        var searchResults;
        if (!isOneWorld() || (isOneWorld() && subsidiary)) {
            var filters = [];
            var columns = [];
            
            // accounts for subsidiary
            if (isOneWorld() && subsidiary) {
                filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));
            }
            
            // do not get inactive accounts
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            
            columns.push(new nlobjSearchColumn('internalid'));
            columns.push(new nlobjSearchColumn('name'));
            
            searchResults = searchRecord('department', null, filters, columns);
        }
        
        return searchResults || [];
    }

    /**
     * Get list of classes
     * 
     * @param {String} subsidiary
     * @returns {Array}
     */
    function getClasses(subsidiary) {
        var searchResults;
        if (!isOneWorld() || (isOneWorld() && subsidiary)) {
            var filters = [];
            var columns = [];
            
            // accounts for subsidiary
            if (isOneWorld() && subsidiary) {
                filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));
            }
            
            // do not get inactive accounts
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            
            columns.push(new nlobjSearchColumn('internalid'));
            columns.push(new nlobjSearchColumn('name'));
            
            searchResults = searchRecord('classification', null, filters, columns);
        }
        
        return searchResults || [];
    }
    
    /**
     * Get list of locations
     * 
     * @param {String} subsidiary
     * @returns {Array}
     */
    function getLocations(subsidiary) {
        var searchResults;
        if (!isOneWorld() || (isOneWorld() && subsidiary)) {
            var filters = [];
            var columns = [];
            
            // accounts for subsidiary
            if (isOneWorld() && subsidiary) {
                filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));
            }
            
            // do not get inactive accounts
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            
            columns.push(new nlobjSearchColumn('internalid'));
            columns.push(new nlobjSearchColumn('name'));
            
            searchResults = searchRecord('location', null, filters, columns);
        }
        
        return searchResults || [];
    }
    
    /**
     * Gets the entities for the vendor/employee/customer filter
     * 
     * @param {String} paymentType
     * @param {Array} requestParams
     * 
     * @returns {Array} 
     */
    function getEntities(paymentType, requestParams) {
        var searchResults;

        var format = '';
        var subsidiary = '';
        
        if (requestParams) {
            format = requestParams['custpage_2663_format_display'];
            if (isOneWorld()) {
                subsidiary = requestParams['custpage_2663_subsidiary'];
            }
        }
        
        if ((paymentType == 'eft' || paymentType == 'dd' || paymentType == 'custref') && format && (!isOneWorld() || (isOneWorld() && subsidiary))) {
            var entityFilters = [];
            var entityColumns = [];
            
            // only get primary details - for dd and custref (for eft, use saved search)
//            entityFilters.push(new nlobjSearchFilter('custrecord_2663_entity_bank_type', null, 'anyof', '1'));

            // check format
            entityFilters.push(new nlobjSearchFilter('custrecord_2663_entity_file_format', null, 'anyof', format));
            
            var savedSearch = null;
            
            if (paymentType == 'eft') {
                // only entities of current subsidiary (if OW)
                if (isOneWorld() && subsidiary) {
                    var vendorSub = new nlobjSearchFilter('subsidiary', 'custrecord_2663_parent_vendor', 'anyof', subsidiary);
                    vendorSub.setLeftParens(1);
                    vendorSub.setOr(true);
                    entityFilters.push(vendorSub);
                    
                    if (isCommissionEnabled()) {
                    	var partnerSub = new nlobjSearchFilter('subsidiary', 'custrecord_2663_parent_partner', 'anyof', subsidiary);
                        partnerSub.setOr(true);
                        entityFilters.push(partnerSub);
                    }
                    
                    var employeeSub = new nlobjSearchFilter('subsidiary', 'custrecord_2663_parent_employee', 'anyof', subsidiary);
                    employeeSub.setRightParens(1);
                    entityFilters.push(employeeSub);
                }
                
                // don't get customer entity details
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_customer', null, 'anyof', '@NONE@'));
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_cust_ref', null, 'anyof', '@NONE@'));

                // check if eft = true for vendor
                var vendorEftFilter = new nlobjSearchFilter('custentity_2663_payment_method', 'custrecord_2663_parent_vendor', 'is', 'T');
                vendorEftFilter.setLeftParens(2);
                
                // check if vendor is inactive
                var vendorInactiveFilter = new nlobjSearchFilter('isinactive', 'custrecord_2663_parent_vendor', 'is', 'F');
                vendorInactiveFilter.setRightParens(1);
                vendorInactiveFilter.setOr(true);
                
                // check if eft = true for employee
                var employeeEftFilter = new nlobjSearchFilter('custentity_2663_payment_method', 'custrecord_2663_parent_employee', 'is', 'T');
                employeeEftFilter.setLeftParens(1);
                
                // add filter expression
                entityFilters.push(vendorEftFilter);
                entityFilters.push(vendorInactiveFilter);
                
                if (isCommissionEnabled()) {
                	// check if eft = true for partner
                    var partnerEftFilter = new nlobjSearchFilter('custentity_2663_payment_method', 'custrecord_2663_parent_partner', 'is', 'T');
                    partnerEftFilter.setLeftParens(1);
                    
                    // check if partner is inactive
                    var partnerInactiveFilter = new nlobjSearchFilter('isinactive', 'custrecord_2663_parent_partner', 'is', 'F');
                   
                    // check if partner is eligible for commission
                    var partnerCommissionFilter = new nlobjSearchFilter('eligibleforcommission', 'custrecord_2663_parent_partner', 'is', 'T');
                    partnerCommissionFilter.setRightParens(1);
                    partnerCommissionFilter.setOr(true);
                    
                    // add filter expression
                    entityFilters.push(partnerEftFilter);
                    entityFilters.push(partnerInactiveFilter);
                    entityFilters.push(partnerCommissionFilter);
                }
                
                // check if employee is inactive
                var employeeInactiveFilter = new nlobjSearchFilter('isinactive', 'custrecord_2663_parent_employee', 'is', 'F');
                employeeInactiveFilter.setRightParens(2);
                
                // add filter expression
                entityFilters.push(employeeEftFilter);
                entityFilters.push(employeeInactiveFilter);
                
                // add groupon filters
                if (psg_ep.isGrouponAccount(nlapiGetContext().getCompany())) {
                	entityFilters.push(new nlobjSearchFilter('custentity_hold_payment_vendor', 'custrecord_2663_parent_vendor', 'is', 'F'));
                    var salesForceId = requestParams['custpage_2663_custentity_v_salesforce_account_id'];
                    if (salesForceId) {
                        entityFilters.push(new nlobjSearchFilter('custentity_v_salesforce_account_id', 'custrecord_2663_parent_vendor', 'contains', salesForceId));
                    }
                }
                
                // get the entity columns
                entityColumns.push(new nlobjSearchColumn('custrecord_2663_parent_vendor'));
                entityColumns.push(new nlobjSearchColumn('custrecord_2663_parent_employee'));
                entityColumns.push(new nlobjSearchColumn('custrecord_2663_parent_partner'));
                
                savedSearch = 'customsearch_ep_entity_search';
            }
            else if (paymentType == 'dd') {
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_entity_bank_type', null, 'anyof', '1'));
                
                // only entities of current subsidiary (if OW)
                if (isOneWorld() && subsidiary) {
                    var customerSub = new nlobjSearchFilter('subsidiary', 'custrecord_2663_parent_customer', 'anyof', subsidiary);
                    entityFilters.push(customerSub);
                }
                
                // don't get vendor/employee entity details
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_vendor', null, 'anyof', '@NONE@'));
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_employee', null, 'anyof', '@NONE@'));
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_cust_ref', null, 'anyof', '@NONE@'));
                
                // check if dd = true
                entityFilters.push(new nlobjSearchFilter('custentity_2663_direct_debit', 'custrecord_2663_parent_customer', 'is', 'T'));
                
                // check if customer is inactive
                entityFilters.push(new nlobjSearchFilter('isinactive', 'custrecord_2663_parent_customer', 'is', 'F'));
                
                var formatName = nlapiLookupField('customrecord_2663_payment_file_format',format,'name');
                if (['SEPA Direct Debit (Germany)', 'SEPA Direct Debit (CBI)'].indexOf(formatName) > -1){                    
                    // include only entity banks without Final Payment Date
                    entityFilters.push(new nlobjSearchFilter('custrecord_2663_final_pay_date', null, 'isempty'));                
                }
                
                // get the entity column
                entityColumns.push(new nlobjSearchColumn('custrecord_2663_parent_customer'));
            }
            else if (paymentType == 'custref') {
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_entity_bank_type', null, 'anyof', '1'));
                
                // only entities of current subsidiary (if OW)
                if (isOneWorld() && subsidiary) {
                    var custRefSub = new nlobjSearchFilter('subsidiary', 'custrecord_2663_parent_cust_ref', 'anyof', subsidiary);
                    entityFilters.push(custRefSub);
                }
                
                // don't get other entity details
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_vendor', null, 'anyof', '@NONE@'));
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_employee', null, 'anyof', '@NONE@'));
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_customer', null, 'anyof', '@NONE@'));
                
                // check if dd = true
                entityFilters.push(new nlobjSearchFilter('custentity_2663_customer_refund', 'custrecord_2663_parent_cust_ref', 'is', 'T'));
                
                // check if customer is inactive
                entityFilters.push(new nlobjSearchFilter('isinactive', 'custrecord_2663_parent_cust_ref', 'is', 'F'));
                
                // get the entity column
                entityColumns.push(new nlobjSearchColumn('custrecord_2663_parent_cust_ref'));
            }
            
            var entitySearch = new _2663.Search('customrecord_2663_entity_bank_details');
            entitySearch.setSavedSearch(savedSearch);
            entitySearch.addFilters(entityFilters);
            entitySearch.addColumns(entityColumns);
            searchResults = entitySearch.getAllResults(100);
            
            if (searchResults && searchResults.length > 0) {
                nlapiLogExecution('debug', '[ep] FormFieldDataProvider:getEntities', 'entities for format and sub (if ow): ' + searchResults.length);
                searchResults.sort(function(a, b) {
                    var aValue = '';
                    var bValue = '';
                    if (paymentType == 'eft') {
                        aValue = a.getText('custrecord_2663_parent_vendor') || a.getText('custrecord_2663_parent_employee') || a.getText('custrecord_2663_parent_partner');
                        bValue = b.getText('custrecord_2663_parent_vendor') || b.getText('custrecord_2663_parent_employee') || b.getText('custrecord_2663_parent_partner');
                    }
                    else if (paymentType == 'dd') {
                        aValue = a.getText('custrecord_2663_parent_customer');
                        bValue = b.getText('custrecord_2663_parent_customer');
                    }
                    else if (paymentType == 'custref') {
                        aValue = a.getText('custrecord_2663_parent_cust_ref');
                        bValue = b.getText('custrecord_2663_parent_cust_ref');
                    }
                    
                    aValue = new String(aValue);
                    bValue = new String(bValue);
                    
                    if (aValue.toLowerCase() < bValue.toLowerCase()) {
                        return -1;
                    }
                    else if (aValue.toLowerCase() > bValue.toLowerCase()) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                });
            }
        }
        
        return searchResults || [];
    }
    
    /**
     * Gets the vendor/employee entities
     * 
     * @param {String} paymentType
     * @param {Array} requestParams
     * @param {Date} startTime
     * 
     * @returns {Array} 
     */
    function getFormatEntities(paymentType, requestParams, startTime) {
    	var entitySearch = new _2663.Search('entity');
        var searchResults;
        var format = '';
        var subsidiary = '';
        var savedSearch = null;
        
        if (requestParams) {
            format = requestParams['custpage_2663_format_display'];
            if (isOneWorld()) {
                subsidiary = requestParams['custpage_2663_subsidiary'];
            }
        }
        
        if (paymentType == 'eft' && format && (!isOneWorld() || (isOneWorld() && subsidiary))) {
        	entitySearch.addColumn('entityid');
            
            // check format
        	entitySearch.addFilter('custentity_2663_eft_file_format', null, 'anyof', format);
            
            // EFT only for now
            if (paymentType == 'eft') {
                // only entities of current subsidiary (if OW)
                if (isOneWorld() && subsidiary) {
                	entitySearch.addFilter('subsidiary', null, 'anyof', subsidiary);
                }
                // check if eft = true
                entitySearch.addFilter('custentity_2663_payment_method', null, 'is', 'T');
                
                // check if inactive
                entitySearch.addFilter('isinactive', null, 'is', 'F');
                
                // add groupon filters
                if (psg_ep.isGrouponAccount(nlapiGetContext().getCompany())) {
                	entitySearch.addFilter('custentity_hold_payment_vendor', null, 'is', 'F');
                    var salesForceId = requestParams['custpage_2663_custentity_v_salesforce_account_id'];
                    if (salesForceId) {
                    	entitySearch.addFilter('custentity_v_salesforce_account_id', null, 'contains', salesForceId);
                    }
                }
                savedSearch = 'customsearch_ep_active_entity_search';
            }
            
            // Set saved search id
            entitySearch.setSavedSearch(savedSearch);
            
            searchResults = entitySearch.getAllResults(100, startTime, this.MaxTime, true);
            if (searchResults && searchResults.length > 0) {
                nlapiLogExecution('debug', '[ep] FormFieldDataProvider:getEntities', 'entities for format and sub (if ow): ' + searchResults.length);
                searchResults.sort(function(a, b) {
                    var aValue = '';
                    var bValue = '';
                    if (paymentType == 'eft') {
                        aValue = a.getValue('entityid') + '';
                        bValue = b.getValue('entityid') + '';
                    }
                    
                    if (aValue.toLowerCase() < bValue.toLowerCase()) {
                        return -1;
                    }
                    else if (aValue.toLowerCase() > bValue.toLowerCase()) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                });
            }
        }
        
        return searchResults || [];
    }
    
    /**
     * Get the last check number set to a processed payment file for a specified bank account
     * 
     * @param {String} bankAccount
     * @returns {String}
     */
    function getLastCheckNumber(bankAccount) {
        var maxCheckNumber = '';
        
        if (bankAccount) {
            var filters = [];
            var columns = [];
            
            filters.push(new nlobjSearchFilter('custrecord_2663_bank_account', null, 'anyof', bankAccount));
            filters.push(new nlobjSearchFilter('custrecord_2663_file_processed', null, 'anyof', PAYPROCESSED));
            columns.push(new nlobjSearchColumn('custrecord_2663_last_check_no', null, 'max'));
            
            var searchRes = nlapiSearchRecord('customrecord_2663_file_admin', null, filters, columns);
            
            if (searchRes) {
                maxCheckNumber = searchRes[0].getValue('custrecord_2663_last_check_no', null, 'max');
            }
        }
        
        return maxCheckNumber;
    }
    
    /**
     * Retrieves the closed batches based on bank account and account
     *  
     * @param {String} bankAccount
     * @param {String} apAccount
     * @param {Array} status
     * @param {Boolean} excludeDeferred
     * @returns {Array} 
     */
    function getBatches(bankAccount, apAccount, status, excludeDeferred) {
        var searchResults;
        if (bankAccount) {
            // get proc bills auto flag
            var autoProcBillFlag = nlapiLookupField('customrecord_2663_bank_details', bankAccount, 'custrecord_2663_eft_batch');
            nlapiLogExecution('debug', '[ep] FormFieldDataProvider:getClosedBatches', 'Process Bills Automatically flag: ' + autoProcBillFlag);

            var filters = [];
            var columns = [];
            
            filters.push(new nlobjSearchFilter('custrecord_2663_bank_account', null, 'anyof', bankAccount));
            // add ap account filter if specified
            if (apAccount) {
                filters.push(new nlobjSearchFilter('custrecord_2663_account', null, 'anyof', apAccount));
            }
            var batchStatus = status || [];
            if (batchStatus.length > 0) {
            	filters.push(new nlobjSearchFilter('custrecord_2663_status', null, 'anyof', batchStatus));	
            }
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            
            columns.push(new nlobjSearchColumn('custrecord_2663_status').setSort());
            columns.push(new nlobjSearchColumn('internalid').setSort());
            columns.push(new nlobjSearchColumn('custrecord_2663_batch_detail'));
            columns.push(new nlobjSearchColumn('custrecord_2663_bank_account'));
            columns.push(new nlobjSearchColumn('custrecord_2663_account'));
            columns.push(new nlobjSearchColumn('custrecord_2663_time_stamp'));
            columns.push(new nlobjSearchColumn('custrecord_2663_payments_for_process_id'));
            columns.push(new nlobjSearchColumn('custrecord_2663_journal_keys'));
            columns.push(new nlobjSearchColumn('custrecord_2663_payments_for_process_amt'));
            columns.push(new nlobjSearchColumn('custrecord_2663_number'));
            
            searchResults = nlapiSearchRecord('customrecord_2663_file_admin', null, filters, columns);
        }
        return searchResults || [];
    }
    
    /**
     * Gets list of bank accounts with closed batches
     * 
     * @returns {Array}
     */
    function getBatchBankAccount(type, batchId) {
        var searchResults;
        if (batchId && (type == 'eft' || type == 'dd')) {
        	var bankAccount = nlapiLookupField('customrecord_2663_file_admin', batchId, 'custrecord_2663_bank_account');
        	if (bankAccount) {
        		var filters = [];
                // only get active records
                filters.push(new nlobjSearchFilter('internalid', null, 'is', bankAccount));
       
                var columns = [];
                columns.push(new nlobjSearchColumn('name').setSort());
                columns.push(new nlobjSearchColumn('custrecord_2663_' + type + '_template'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_department'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_class'));
                columns.push(new nlobjSearchColumn('custrecord_2663_bank_location'));
                columns.push(new nlobjSearchColumn('custrecord_2663_subsidiary'));
                columns.push(new nlobjSearchColumn('custrecord_2663_trans_marked'));
                columns.push(new nlobjSearchColumn('custrecord_2663_' + type + '_batch'));
                columns.push(new nlobjSearchColumn('custrecord_2663_summarized'));
                columns.push(new nlobjSearchColumn('custrecord_2663_auto_ap'));
                columns.push(new nlobjSearchColumn('custrecord_2663_currency'));

                searchResults = nlapiSearchRecord('customrecord_2663_bank_details', null, filters, columns);	
        	}
        }        
        return searchResults || [];
    }
    
    /**
     * Searches for available auto-ap values set to a format
     * 
     * @param {String} format
     * @returns {Array}
     */
    function getAutoAPAccounts(format) {
        var searchResults;
        if (format && (new _2663.EditionControl()).IsLicensed() == true) {
            var filters = [];
            
            filters.push(new nlobjSearchFilter('custrecord_2663_eft_template', null, 'anyof', format));
            filters.push(new nlobjSearchFilter('custrecord_2663_eft_batch', null, 'is', 'T'));
            filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
            
            var columns = [];
            columns.push(new nlobjSearchColumn('custrecord_2663_auto_ap'));
            
            searchResults = nlapiSearchRecord('customrecord_2663_bank_details', null, filters, columns);
        }
    
        return searchResults || [];
    }

    /**
     * Get the email templates
     * 
     * @returns {Array} - elements are objects with id and name
     */
    function getEmailTemplates() {        
        
        var arrFilters = [];
        var arrColumns = [];
        var arrResults = [];
        
        // search for the file that serves as mark for the Email Templates folder
        arrFilters.push(new nlobjSearchFilter('name',null,'is',EP_UUID_EMAIL_TEMPLATES));
        arrResults = nlapiSearchRecord('file',null,arrFilters,null);
        if (!arrResults){
            throw nlapiCreateError('EP_EMAIL_TEMPLATE_UUID_NULL', 'Cannot find UUID file for the Email Templates', true);
        }
        else if (arrResults.length > 1){
            throw nlapiCreateError('EP_EMAIL_TEMPLATE_UUID_DUPLICATE', 'UUID file for the Email Templates is not unique', true);
        }
        var uuidFile = arrResults[0];
        var fileObj = nlapiLoadFile(uuidFile.getId());
        
        // search for files under the Email Templates folder
        var folderId = fileObj.getFolder();
        arrFilters.length = 0;
        arrFilters.push(new nlobjSearchFilter('folder',null,'is',folderId));
        arrFilters.push(new nlobjSearchFilter('name',null,'isnot',EP_UUID_EMAIL_TEMPLATES));
        arrColumns.length = 0;        
        arrColumns.push(new nlobjSearchColumn('name',null));
        arrResults = nlapiSearchRecord('file',null,arrFilters,arrColumns) || [];
        
        // build the return value containing array of email template objects
        var arrEmailTemplateObj = [];
        for (var i in arrResults){            
            var objEmailTemplate = {};
            objEmailTemplate.id = arrResults[i].getId();
            objEmailTemplate.name = arrResults[i].getValue('name');
            arrEmailTemplateObj.push(JSON.stringify(objEmailTemplate));
        }

        return arrEmailTemplateObj;
    }
        
    
    this.GetBankAccounts = getBankAccounts;
    this.GetPostingPeriods = getPostingPeriods;
    this.GetAggregationMethods = getAggregationMethods;
    this.GetDirectDebitTypes = getDirectDebitTypes;
    this.GetAccounts = getAccounts;
    this.GetDepartments = getDepartments;
    this.GetClasses = getClasses;
    this.GetLocations = getLocations;
    this.GetEntities = getEntities;
    this.GetFormatEntities = getFormatEntities;
    this.GetLastCheckNumber = getLastCheckNumber;
    this.GetBatches = getBatches;
    this.GetBatchBankAccount = getBatchBankAccount;
    this.GetAutoAPAccounts = getAutoAPAccounts;
    this.GetEmailTemplates = getEmailTemplates;
};


/**
 * Interface to retrieve data that would be used for the sublist
 * 
 * @returns {psg_ep.TransactionDataInterface}
 */
psg_ep.TransactionDataInterface = function() {
    this.BuildFilters = null;
    this.BuildColumns = null;
    this.ConvertTransactionSearchResultToObjectArray = null;
    this.RequestParams = null;
    this.Entities = null;
    this.StartTime = null;
    this.PortalBatchProperties = null;
    this.SavedSearch = null;
};

/**
 * Parent class that runs searches based on child classes filters and columns
 * 
 * @param {psg_ep.TransactionDataInterface} tranDataInterface
 * @returns {psg_ep.TransactionData}
 */
psg_ep.TransactionData = function(tranDataInterface) {
    if (!tranDataInterface) {
        throw nlapiCreateError('EP_INTERFACE_OBJ_NULL', 'Cannot create parent data object without interface object', true);
    }

    // assign argument objects
    this._tranDataInterface = tranDataInterface;
    
    // entity batch size
    this.EntityBatchSize = 1000;
    var maxEntityParam = nlapiGetContext().getSetting('script', 'custscript_2663_max_entities_in_batch');
    if (maxEntityParam) {
        this.EntityBatchSize = new Number(maxEntityParam);
    }
    
    // portal batch size
    this.PaymentBatchSize = 500;
    
    // 4 minutes for data retrieval (1 minute buffer)
    this.MaxTime = 240000;
    
    this.TransDataUtil = new psg_ep.TransactionDataUtils();
    
    /**
     * Gets the transactions based on interface filters and columns
     * 
     * @returns {___anonymous73660_73714}
     */
    function getTransactions() {
        var transactions = [];
        var errorFlag = false;
        if (verifyInterface(this._tranDataInterface) == true) {
            var filters = this._tranDataInterface.BuildFilters(this._tranDataInterface.RequestParams);
            var columns = this._tranDataInterface.BuildColumns();
    
            // search for transactions
            var transactionsObj = this.TransDataUtil.SearchTransactions(filters, columns, this._tranDataInterface.StartTime, this.MaxTime, this._tranDataInterface.SavedSearch);
            var searchResults = transactionsObj.transactions;
            errorFlag = transactionsObj.errorFlag;
            
            nlapiLogExecution('debug', '[ep] TransactionData:getTransactions', 'number of transactions found: ' + searchResults.length);
            if (searchResults && searchResults.length > 0) {
                // sort transactions and convert to object array
                transactions = this._tranDataInterface.ConvertTransactionSearchResultToObjectArray(searchResults, this._tranDataInterface.RequestParams);
                transactions.sort(this._tranDataInterface.SortTransactions || sortTransactions);
            }
        }
        else {
            errorFlag = true;
        }
        
        return { transactionList: transactions, errorFlag: errorFlag };
    }
    
    /**
     * Gets the transactions based on interface filters and columns by entity batches
     * 
     * @returns {___anonymous73660_73714}
     */
    function getTransactionsByBatch() {
        var transactions = [];
        var errorFlag = false;
        
        if (verifyInterface(this._tranDataInterface) == true) {
            var entityFilterBatches = buildEntityFilters(this);
            var columns = this._tranDataInterface.BuildColumns();
            
            // search for transactions
            var searchResults = [];
            for (var i = 0; i < entityFilterBatches.length; i++) {
                var transactionsObj = this.TransDataUtil.SearchTransactions(entityFilterBatches[i], columns, this._tranDataInterface.StartTime, this.MaxTime, this._tranDataInterface.SavedSearch);
                searchResults = searchResults.concat(transactionsObj.transactions);
                if (transactionsObj.errorFlag == true) {
                    errorFlag = transactionsObj.errorFlag;
                    break;
                }
            }
            
            nlapiLogExecution('debug', '[ep] TransactionData:getTransactions', 'number of transactions found: ' + searchResults.length);
            if (searchResults && searchResults.length > 0) {
                // convert to object array and sort
                transactions = this._tranDataInterface.ConvertTransactionSearchResultToObjectArray(searchResults, this._tranDataInterface.RequestParams);
                transactions.sort(sortTransactions);
            }
        }
        else {
            errorFlag = true;
        }
        
        return { transactionList: transactions, errorFlag: errorFlag };
    }
    
    /**
     * Gets the transactions based on portal batch record
     * 
     * @returns {___anonymous26958_27012}
     */
    function getPortalBatchTransactions() {
        var transactions = [];
        var errorFlag = false;
        
        if (verifyInterface(this._tranDataInterface) == true) {
            // get batch information
            var batchInfo = this._tranDataInterface.PortalBatchProperties;
            
            // parse the transactions
            if (batchInfo) {
                var searchResults = [];
                var columns = this._tranDataInterface.BuildColumns();
                
                // if successful parse and there are transaction keys
                if (!errorFlag && batchInfo.transactionKeys && batchInfo.transactionKeys.length > 0) {
                    // get batch filters
                    var filters = buildPortalTransactionFilters(this, batchInfo.transactionKeys);
                    
                    // search for transactions based on batch id's
                    var transactionsObj = this.TransDataUtil.SearchTransactions(filters, columns, this._tranDataInterface.StartTime, this.MaxTime);
                    if (transactionsObj.transactions && transactionsObj.transactions.length > 0) {
                        nlapiLogExecution('debug', '[ep] TransactionData:getPortalBatchTransactions', 'number of transactions found from keys: ' + transactionsObj.transactions.length);
                        searchResults = searchResults.concat(transactionsObj.transactions);
                    }
                    
                    errorFlag = transactionsObj.errorFlag;
                }
                
                nlapiLogExecution('debug', '[ep] TransactionData:getPortalBatchTransactions', 'searchResults.length: ' + searchResults.length);
                if (searchResults.length > 0) {
                    // convert to object array and sort
                    transactions = this._tranDataInterface.ConvertTransactionSearchResultToObjectArray(searchResults);
                    transactions.sort(sortTransactions);
                }
            }
            else {
                nlapiLogExecution('error', '[ep] TransactionData:getPortalBatchTransactions', 'Portal batch properties undefined');
                errorFlag = true;
            }
        }
        else {
            nlapiLogExecution('error', '[ep] TransactionData:getPortalBatchTransactions', 'Invalid interface');
            errorFlag = true;
        }
        
        return { transactionList: transactions, errorFlag: errorFlag };
    }
    
    /**
     * Builds the filters with entity batches
     * 
     * @param {psg_ep.TransactionData} thisObj
     * @returns {Array}
     */
    function buildEntityFilters(thisObj) {
        // return filters for all entity batches
        var entityBatchFilters = [];
        
        // build filter for each entity batch
        var entityFilters = thisObj._tranDataInterface.Entities;
        if (entityFilters && entityFilters.length > 0) {
            nlapiLogExecution('debug', '[ep] TransactionData:buildEntityFilters', 'number of entities: ' + entityFilters.length);
            
            // build filters that do not use entities based on request parameters
            var nonEntityFilters = thisObj._tranDataInterface.BuildFilters(thisObj._tranDataInterface.RequestParams);

            // create filter for each batch of entities
            var numBatches = Math.ceil(entityFilters.length / new Number(thisObj.EntityBatchSize));
            nlapiLogExecution('debug', '[ep] TransactionData:buildEntityFilters', 'number of batches: ' + numBatches);
            for (var i = 0; i < numBatches; i++) {
                // add non entity filters
                var filters = [];
                filters = filters.concat(nonEntityFilters);
                
                // create and add filter for entity batch
                var startIdx = i * thisObj.EntityBatchSize;
                var endIdx = (i + 1) * thisObj.EntityBatchSize < entityFilters.length ? (i + 1) * thisObj.EntityBatchSize : entityFilters.length ;
                nlapiLogExecution('debug', '[ep] TransactionData:buildEntityFilters', 'batch: ' + i + ', start: ' + startIdx + ', end: ' + endIdx);
                var entityBatch = entityFilters.slice(startIdx, endIdx);
                var newEntityFilter = new nlobjSearchFilter('entity', null, 'anyof', entityBatch);
                filters.push(newEntityFilter);
                
                // add to list of filters for search
                entityBatchFilters.push(filters);
            }
        }

        return entityBatchFilters;
    }
    
    /**
     * Builds the portal transaction filters
     * 
     * @param {psg_ep.TransactionData} thisObj
     * @param {Array} portalBatchTransactionKeys
     * @returns {Array}
     */
    function buildPortalTransactionFilters(thisObj, portalBatchTransactionKeys) {
        // return filters for all entity batches
        var filters = [];
        
        // build filter for each portal batch
        if (portalBatchTransactionKeys && portalBatchTransactionKeys.length > 0) {
            nlapiLogExecution('debug', '[ep] TransactionData:buildPortalTransactionFilters', 'number of transactions in portal batch: ' + portalBatchTransactionKeys.length);
            
            // build filters that do not use entities based on request parameters
            var transFilters = thisObj._tranDataInterface.BuildFilters(thisObj._tranDataInterface.RequestParams);
            
            // add to filters
            filters = filters.concat(transFilters);
            
            // get only internal ids
            var portalTransactionIds = [];
            for (var i = 0; i < portalBatchTransactionKeys.length; i++) {
                var key = new String(portalBatchTransactionKeys[i]);
                var internalId = key.substring(0, key.indexOf('-'));
                if (internalId) {
                    portalTransactionIds.push(internalId);
                }
            }
            nlapiLogExecution('debug', '[ep] TransactionData:buildPortalTransactionFilters', 'number of transaction ids: ' + portalTransactionIds.length);
            
            // create filter for each batch of entities
            var numBatches = Math.ceil(portalTransactionIds.length / new Number(thisObj.PaymentBatchSize));
            nlapiLogExecution('debug', '[ep] TransactionData:buildPortalTransactionFilters', 'number of batches: ' + numBatches);
            for (var i = 0; i < numBatches; i++) {
                // create portal batch filter
                var startIdx = i * thisObj.PaymentBatchSize;
                var endIdx = (i + 1) * thisObj.PaymentBatchSize < portalTransactionIds.length ? (i + 1) * thisObj.PaymentBatchSize : portalTransactionIds.length ;
                nlapiLogExecution('debug', '[ep] TransactionData:buildPortalTransactionFilters', 'batch: ' + i + ', start: ' + startIdx + ', end: ' + endIdx);
                var portalTransactionIdFilters = portalTransactionIds.slice(startIdx, endIdx);
                // create filter for entity batch
                var newPortalTransFilter = new nlobjSearchFilter('internalid', null, 'anyof', portalTransactionIdFilters);
                if (numBatches > 1) {
                    if (i == 0) {
                        // set left paren for first batch
                        newPortalTransFilter.setLeftParens(2);
                        // set or as true
                        newPortalTransFilter.setOr(true);
                    }
                    else if (i == numBatches - 1) {
                        // set right paren for last batch
                        newPortalTransFilter.setRightParens(2);
                    }
                    else {
                        // set or as true
                        newPortalTransFilter.setOr(true);
                    }
                }
                filters.push(newPortalTransFilter);
            }
        }

        return filters;
    }
    
    /**
     * Builds the portal transaction filters for journal search
     * 
     * @param {psg_ep.TransactionData} thisObj
     * @param {Array} journalKeys
     * @returns {Array}
     */
    function buildPortalJournalTransactionFilters(thisObj, journalKeys) {
        // return filters for all entity batches
        var filters = [];
        
        // build filters for journal transactions of portal batch
        if (thisObj && journalKeys && journalKeys.length > 0) {
        	// get request parameter filters
        	var format = thisObj._tranDataInterface.RequestParams['custpage_2663_format_display'];
            var apAccount = thisObj._tranDataInterface.RequestParams['custpage_2663_ap_account'] || '@NONE@';
            
            //get journal ids
            var journalIds = [];
            
            for (var i = 0, ii = journalKeys.length; i < ii; i++) {
            	var journalId = (journalKeys[i].match(/\d{1,}/) || [])[0];
            	if (journalIds.indexOf(journalId) < 0) {
            		journalIds.push(journalId);
            	} 
            }
            
            //internal id
            filters.push(new nlobjSearchFilter('internalid', null, 'anyof', journalIds));
            
            // ap account  
            if (apAccount) {
            	filters.push(new nlobjSearchFilter('account', null, 'anyof', apAccount));	
            }
            
            // payment file format
            if (format) {
            	filters.push(new nlobjSearchFilter('custcol_2663_eft_file_format', null, 'is', format));	
            }
            
            //transaction type
            filters.push(new nlobjSearchFilter('type', null, 'anyof', ['Journal']));
            
            //transaction status
            filters.push(new nlobjSearchFilter('status', null, 'anyof', ['Journal:B']));
            
            // don't get negative values (for journal entries)
            filters.push(new nlobjSearchFilter('creditamount', null, 'greaterthan', 0));
            
            // only get values with amount remaining
            filters.push(new nlobjSearchFilter('amountremaining', null, 'greaterthan', 0));
            
            // does not have reversal date
            filters.push(new nlobjSearchFilter('reversaldate', null, 'isempty', 'T'));
            
            // don't get memorized transactions
            filters.push(new nlobjSearchFilter('memorized', null, 'is', 'F'));
        }

        return filters;
    }
    
    /**
     * Function to sort transactions
     * 
     * @param {nlobjSearchResult} a
     * @param {nlobjSearchResult} b
     * @returns {Number}
     */
    function sortTransactions(a, b) {
        var aString = new String(a.custpage_entity);
        var bString = new String(b.custpage_entity);
        aString = aString.toLowerCase();
        bString = bString.toLowerCase();
        if (aString < bString) {
            return -1;
        }
        else if (aString > bString) {
            return 1;
        }
        else {
            if (a.custpage_type < b.custpage_type) {
                return -1;
            }
            else if (a.custpage_type > b.custpage_type) {
                return 1;
            }
            else {
                var aNum = !isNaN(a.custpage_number) ? parseInt(a.custpage_number, 10) : '';
                var bNum = !isNaN(b.custpage_number) ? parseInt(b.custpage_number, 10) : '';
                if (aNum && bNum) {
                    if (aNum < bNum) {
                        return -1;
                    }
                    else if (aNum > bNum) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
                else {
                    return 0;
                }
            }
        }
    }
    
    /**
     * Function to check the validity of interface object before making the object functions accessible
     * 
     * @param {psg_ep.TransactionDataInterface} interfaceObj
     * @returns {Boolean}
     */
    function verifyInterface(interfaceObj) {
        return (interfaceObj &&
                interfaceObj.BuildFilters && 
                typeof interfaceObj.BuildFilters == 'function' && 
                interfaceObj.BuildColumns && 
                typeof interfaceObj.BuildColumns == 'function' && 
                interfaceObj.ConvertTransactionSearchResultToObjectArray &&
                typeof interfaceObj.ConvertTransactionSearchResultToObjectArray == 'function');
    }
    
    this._getTransactions = getTransactions;
    this._getTransactionsByBatch = getTransactionsByBatch;
    this._getPortalBatchTransactions = getPortalBatchTransactions;
};

/**
 * Transaction data utilities for search
 * 
 * @returns {psg_ep.TransactionDataUtils}
 */
psg_ep.TransactionDataUtils = function() {
	var context = nlapiGetContext();
	var logTitle = 'psg_ep.TransactionDataUtils';
	var logger = new _2663.Logger(logTitle);
	var useOldSearch = context.getSetting('script', 'custscript_2663_use_old_search') == 'T' || context.getSetting('script', 'custscript_2663_batch_use_old_search') == 'T';
	logger.debug('useOldSearch: ' + useOldSearch);
    /**
     * Checks for timeout
     * 
     * @param {Number} startTime
     * @param {Number} maxTime
     * @returns {Boolean}
     */
    function hasTimedOut(startTime, maxTime) {
        var timeOutFlag = false;

        if (startTime && maxTime) {
            var currentTime = (new Date()).getTime();
            var timeElapsed = currentTime - startTime;
            if (timeElapsed >= maxTime) {
                timeOutFlag = true;
            }
        }
        
        return timeOutFlag;
    }
    
    /**
     * Retrieves transaction records
     * 
     * @param {Array} filters
     * @param {Array} columns
     * @param {Number} startTime
     * @param {Number} maxTime
     * @returns {}
     */
    function searchTransactions(filters, columns, startTime, maxTime, savedSearch) {
    	logger.setTitle(logTitle + ':searchTransactions');
    	var transactions = [];
    	var errorFlag = false;
    	
    	if (useOldSearch) {
    		var markerObj = {};
            markerObj.name = '';
            markerObj.lastIndex = 1;
            var searchResults = {};
            while (searchResults) {
                try {
                    // search with marker
                    searchResults = searchTransactionsWithMarker(filters, columns, markerObj, startTime, maxTime, savedSearch);
                } catch(ex) {
                    logger.error('Error during transaction search with marker', ex);
                    errorFlag = true;
                    break;
                }
                
                // concatenate to results to return
                if (searchResults) {
                    transactions = transactions.concat(searchResults);
                }
            }
    	} else {
    		var transactionSearch = new _2663.Search('transaction');
        	transactionSearch.setSavedSearch(savedSearch);
        	transactionSearch.addFilters(filters);
        	transactionSearch.addColumns(columns);
        	transactionSearch.addColumn('internalid');
        	transactionSearch.addColumn('line');
        	transactionSearch.addColumn('entity');
        	
        	transactions = transactionSearch.getAllResults(100, startTime, maxTime);
        	errorFlag = transactions.errorFlag || false;	
    	}
    	
    	logger.debug('transactions.length: ' + transactions.length);
    	
        return {transactions: transactions, errorFlag: errorFlag} ;
    }
    
    /**
     * Gets succeeding search results from the given internal id and line id (if line == true)
     * - Use the markerObj to store the state. For the initial search, set markerObj.lastIndex = 1.
     * - Calls to this function must handle the Script Execution Limit and Script API 
     *   Governance exceptions
     * 
     * @param {Array} filters
     * @param {Array} columns
     * @param {} markerObj
     * @param {Number} startTime
     * @param {Number} maxTime
     * @param {String} savedSearch
     */
    function searchTransactionsWithMarker(filters, columns, markerObj, startTime, maxTime, savedSearch) {
        var searchResults = [];

        if (markerObj && startTime && maxTime) {
            if (governanceReached()) {
                throw nlapiCreateError('EP_GOVERNANCE_REACHED', 'Governance reached.', true);
            }

            if (hasTimedOut(startTime, maxTime) == true) {
                throw nlapiCreateError('EP_SEARCH_TIME_EXCEEDED', 'Search time exceeded.', true);
            }
            
            if (!columns) {
                columns = [];
            }
            if (!markerObj.lastInternalId) {
                columns.push(new nlobjSearchColumn('internalid').setSort());
                columns.push(new nlobjSearchColumn('line').setSort());
                columns.push(new nlobjSearchColumn('entity'));
            }
            var limitFilters = [];
            if (filters) {
                limitFilters = limitFilters.concat(filters);
            }
            if (markerObj.lastInternalId) {
            	if (savedSearch == 'customsearch_ep_ap_trans_search') {
            		var nameFormulaFilter = new nlobjSearchFilter('formulanumeric', null, 'equalto', 1);
                	nameFormulaFilter.setFormula("CASE WHEN {name} >= '" + markerObj.name + "' THEN 1 ELSE 0 END");
                	limitFilters.push(nameFormulaFilter);
                	
                	var name_id_FormulaFilter = new nlobjSearchFilter('formulanumeric', null, 'equalto', 1);
                	name_id_FormulaFilter.setFormula("CASE WHEN {name} = '" + markerObj.name + "' AND {internalid} <= " + markerObj.lastInternalId + " THEN 0 ELSE 1 END");
                	limitFilters.push(name_id_FormulaFilter);
                	
                    var formula = "CASE WHEN {name} = '"+ markerObj.name; 
                    formula += "' AND {internalid} = " + markerObj.lastInternalId;
                    formula += " AND {line} <= " + markerObj.lastLineNum;
                    formula += " THEN 0 ELSE 1 END";
                    var name_id_line_FormulaFilter = new nlobjSearchFilter('formulanumeric', null, 'equalto', 1);
                    name_id_line_FormulaFilter.setFormula(formula);
                    limitFilters.push(name_id_line_FormulaFilter);
            	} else {
            		limitFilters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthanorequalto', markerObj.lastInternalId));
                    var formula = 'CASE WHEN {internalid} = ' + markerObj.lastInternalId;
                    formula += ' AND {line} <= ' + markerObj.lastLineNum;
                    formula += ' THEN 0 ELSE 1 END';
                    var formulaFilter = new nlobjSearchFilter('formulanumeric', null, 'equalto', 1);
                    formulaFilter.setFormula(formula);
                    limitFilters.push(formulaFilter);	
            	}
            }
            
            var searchResults = nlapiSearchRecord('transaction', savedSearch, limitFilters, columns);
            
            if (searchResults) {
                nlapiLogExecution('debug', '[ep] TransactionData:searchTransactionsWithMarker', 'searchWithMarker results: ' + searchResults.length);
                markerObj.lastIndex = searchResults.length - 1;
                markerObj.lastInternalId = searchResults[markerObj.lastIndex].getId();
                markerObj.lastLineNum = searchResults[markerObj.lastIndex].getValue('line');
                markerObj.name = searchResults[markerObj.lastIndex].getText('entity');
            }
            else {
                markerObj.lastIndex = 0;
            }
        }
        
        return searchResults;
    }

    this.SearchTransactions = searchTransactions;
};

/**
 * Functions to retrieve portal batch properties
 * 
 * @returns {psg_ep.PortalBatchDataProvider}
 */
psg_ep.PortalBatchDataProvider = function() {
    /**
     * Gets the portal orphan batch properties based on batch id
     * 
     * @param {String} batchId
     * @returns {}
     */
    function getPortalBatchPropertiesById(batchId) {
        nlapiLogExecution('debug', '[ep] TransactionData:getPortalBatchProperties', 'batch id: ' + batchId);
        var portalBatch; 
        if (batchId) {
        	var batchSearch = new _2663.Search('customrecord_2663_file_admin');
        	batchSearch.addFilter('internalid', null, 'anyof', batchId);
        	batchSearch.addColumn('altname');
        	batchSearch.addColumn('custrecord_2663_payment_type');
            batchSearch.addColumn('custrecord_2663_bank_account');
            batchSearch.addColumn('custrecord_2663_account');
            batchSearch.addColumn('custrecord_2663_status');
            batchSearch.addColumn('custrecord_2663_payments_for_process_id');
            batchSearch.addColumn('custrecord_2663_journal_keys');
            batchSearch.addColumn('custrecord_2663_approval_level');
            batchSearch.addColumn('custrecord_2663_ref_note');
            batchSearch.addColumn('custrecord_2663_process_date');
            batchSearch.addColumn('custrecord_2663_posting_period');
            batchSearch.addColumn('custrecord_2663_aggregate');
            batchSearch.addColumn('custrecord_2663_agg_method');
            
            var searchResults = batchSearch.getResults();
            if (searchResults && searchResults.length > 0) {
            	var batch = searchResults[0];
            	var bankAccount = batch.getValue('custrecord_2663_bank_account');
            	var approvalLevel = batch.getValue('custrecord_2663_approval_level');
            	var approvers = getApprovers(bankAccount, approvalLevel);
                var paymentTypeText = new String(searchResults[0].getText('custrecord_2663_payment_type')).toLowerCase();
                var isBatchProc = nlapiLookupField('customrecord_2663_bank_details', searchResults[0].getValue('custrecord_2663_bank_account'), 'custrecord_2663_' + paymentTypeText + '_batch');
                nlapiLogExecution('debug', '[ep] PortalBatchDataProvider:getPortalBatchProperties', 'portal batch search res length: ' + searchResults.length);
                portalBatch = {
                    batchId: batch.getId(),
                    name: batch.getValue('altname'),
                    paymentType: batch.getValue('custrecord_2663_payment_type'),
                    bankAccount: bankAccount,
                    account: batch.getValue('custrecord_2663_account'),
                    status: batch.getValue('custrecord_2663_status'),
                    transactionKeys: JSON.parse(batch.getValue('custrecord_2663_payments_for_process_id') || '[]'),
                    journalKeys: JSON.parse(batch.getValue('custrecord_2663_journal_keys') || '[]'),
                    isBatchProc: isBatchProc == 'T' ? true : false,
            		refNote: batch.getValue('custrecord_2663_ref_note') || '',
                    processDate: batch.getValue('custrecord_2663_process_date') || '',
                    postingPeriod: batch.getValue('custrecord_2663_posting_period') || '', 
                    aggregate: batch.getValue('custrecord_2663_aggregate') || 'T',
                    aggMethod: batch.getValue('custrecord_2663_agg_method') || '',
                    approvers: approvers
                };
            } 
        }
        
        return portalBatch;
    }
    
    function getApprovers(bankAccount, approvalLevel) {
    	var approvers = [];
    	if (bankAccount && approvalLevel) {
    		var approvalRoutingSearch = new _2663.Search('customrecord_2663_approval_routing');
        	approvalRoutingSearch.addFilter('custrecord_2663_ar_bank_acct', null, 'is', bankAccount);
        	approvalRoutingSearch.addFilter('custrecord_2663_ar_level', null, 'is', approvalLevel);
        	approvalRoutingSearch.addColumn('custrecord_2663_ar_approver');
        	
        	var res = approvalRoutingSearch.getResults();
        	if (res && res.length > 0) {
        		var strApprovers = res[0].getValue('custrecord_2663_ar_approver');
        		if (strApprovers) {
        			approvers = strApprovers.split(',');	
        		}
        	}	
    	}
    	
    	return approvers; 
    }
    
    this.GetPortalBatchPropertiesById = getPortalBatchPropertiesById;
};
/**
 * @author alaurito
 */

/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2012/11/20  237873         2.00.3                  Initial version, functions for multicurrency
 * 2012/12/20  238697         2.00.5                  Get base currency from admin data loader suitelet
 * 2013/01/07  239363         2.00.6                  Added function psg_ep.GetExchangeRates 
 * 2013/01/07  238977         2.00.6                  Hide currency field if multiple currencies feature is not enabled
 * 2013/01/08  239643         2.00.6                  Add checking on Include All Currencies field of Payment File Format record
 * 2013/01/08  239660         2.00.6                  Set Include All Currencies to disabled on edit of custom format or
 * 													  if payment type is not eft or dd
 * 
 */


var psg_ep;

if (!psg_ep) 
    psg_ep = {};


/**
 * Function to display multicurrency field
 * 
 * @param {String} type
 * @param {nlobjForm} form
 * @param {String} selectedCurrencies
 */
psg_ep.DisplayMulticurrencyField = function(type, form, selectedCurrencies, formatType) {
    if (form) {
    	var isMultiCurrency = nlapiGetContext().getSetting('FEATURE', 'MULTICURRENCY') == 'T';
    	var disableFlag = (formatType != '1' && formatType != '2');
		// hide original field
        var currLongTextFieldObj = form.getField('custrecord_2663_format_currency');
        if (currLongTextFieldObj) {
            currLongTextFieldObj.setDisplayType('hidden');
        }
        var includeAllCurrencies = nlapiGetFieldValue('custrecord_2663_include_all_currencies') == 'T';
        var includeAllCurrLongTextFieldObj = form.getField('custrecord_2663_include_all_currencies');
        if (includeAllCurrLongTextFieldObj) {
        	if (!isMultiCurrency) {
            	includeAllCurrLongTextFieldObj.setDisplayType('hidden');
            } else if (nlapiGetRecordId() || disableFlag) {
        		includeAllCurrLongTextFieldObj.setDisplayType('disabled');	
            }
        }
        
        // display the multicurrency field if multiple currencies feature is enabled
        if (isMultiCurrency && !includeAllCurrencies) {
            // insert new field
            var currMultiselectFieldObj = form.addField('custpage_2663_format_currency', 'multiselect', 'Currency', 'currency');
            if (currMultiselectFieldObj) {
                form.insertField(currMultiselectFieldObj, 'custrecord_2663_file_fields');
            }
            
            // only for EFT and DD
            if (formatType == '1' || formatType == '2') {
                // set display type as inline for when currency was already set before
                if (selectedCurrencies) {
                    // set the value from currency text field to the multiselect field
                    var selectedCurrencyArr;
                    try {
                        selectedCurrencyArr = JSON.parse(selectedCurrencies);
                    }
                    catch(ex) {
                        nlapiLogExecution('error', '[ep] DisplayMulticurrencyField', 'Error in parsing selected currency string: ' + selectedCurrencies);
                    }
                    
                    if (selectedCurrencyArr && selectedCurrencyArr.length > 0) {
                        var currencyMap = psg_ep.BuildCurrencyMap(selectedCurrencyArr);
                        var currToSet = [];
                        for (var i in currencyMap) {
                            currToSet.push(i); 
                        }
                        currMultiselectFieldObj.setDefaultValue(currToSet);
                        
                        // set display type as inline for when currency was already set before for currency and file type
                        currMultiselectFieldObj.setDisplayType('inline');
                        form.getField('custrecord_2663_payment_file_type').setDisplayType('inline');
                    }
                }
                else {
                    nlapiLogExecution('debug', '[ep] DisplayMulticurrencyField', 'Currency is not yet set');
                }
            }
            else {
                currMultiselectFieldObj.setDisplayType('disabled');
            }
        }
	}
    
};

/**
 * Build the currency map
 * 
 * @returns {Array} selectedCurrencyArr
 */
psg_ep.BuildCurrencyMap = function(selectedCurrencyArr) {
    var currencyMap = {};
    
    var filters = [];
    var columns = [];
    if (selectedCurrencyArr && selectedCurrencyArr.length > 0) {
        for (var i = 0; i < selectedCurrencyArr.length; i++) {
            var currSymbolFilter = new nlobjSearchFilter('symbol', null, 'is', selectedCurrencyArr[i]);
            if (selectedCurrencyArr.length > 1 && i < selectedCurrencyArr.length - 1) {
                currSymbolFilter.setOr(true);
            }
            filters.push(currSymbolFilter);
        }
    }
    
    columns.push(new nlobjSearchColumn('name'));
    columns.push(new nlobjSearchColumn('symbol'));
    
    var searchResults = nlapiSearchRecord('currency', null, filters, columns);
    
    if (searchResults) {
        for (var i = 0; i < searchResults.length; i++) {
            currencyMap[searchResults[i].getId() + ''] = {
                name: searchResults[i].getValue('name'),
                symbol: searchResults[i].getValue('symbol')
            };
        }
    }
    
    return currencyMap;
};

/**
 * Class for exchange rate sublist
 *
 * @param {nlobjForm} form
 * @param {String} tab
 * @returns {psg_ep.Sublist_ExchangeRate}
 */
psg_ep.Sublist_ExchangeRate = function(form, tab, displayOnly) {
    if (!form) {
        throw nlapiCreateError('EP_FORM_NULL', 'Cannot create sublist object if form is null');
    }
    
    this.ExchangeRateColumns = {
        'custpage_currency_id': {
            type: 'integer',
            label: 'ID',
            displayType: 'hidden'
        },
        'custpage_currency_name': {
            type: 'text',
            label: 'Currency'
        },
        'custpage_currency_symbol': {
            type: 'text',
            label: 'Symbol'
        },
        'custpage_currency_exchange': {
            type: 'float',
            label: 'Exchange Rate',
            displayType: displayOnly ? 'normal' : 'entry',
            width: 25
        }
    };
    
    if (!tab) {
        tab = tab || 'custpage_exchange_rate_subtab';
        form.addSubTab(tab, 'Exchange Rates');
    }
    this._sublistObj = form.addSubList('custpage_exchange_rate', 'list', 'Exchange Rates', tab);
    
    /**
     * Builds the sublist UI
     */
    function buildUI() {
        for (var i in this.ExchangeRateColumns) {
            var fld = this._sublistObj.addField(i, this.ExchangeRateColumns[i].type, this.ExchangeRateColumns[i].label);
            if (this.ExchangeRateColumns[i].displayType) {
                fld.setDisplayType(this.ExchangeRateColumns[i].displayType);
            }
            if (this.ExchangeRateColumns[i].width) {
                fld.setDisplaySize(this.ExchangeRateColumns[i].width);
            }
        }
    }
    
    /**
     * Sets the sublist data
     */
    function setSublistData(formatCurrencies, exchangeRates, bankCurrency, baseCurrency) {
        nlapiLogExecution('debug', '[ep] Sublist_ExchangeRate:setSublistData', 'Currencies: ' + formatCurrencies + ' , exchange rate: ' + exchangeRates + ', bankCurrency: ' + bankCurrency + ', baseCurrency: ' + baseCurrency);
        if (formatCurrencies && exchangeRates && bankCurrency) {
            var currenciesObj;
            try {
                currenciesObj = typeof formatCurrencies == 'string' ? JSON.parse(formatCurrencies) : formatCurrencies;
                var exchangeRatesObj = JSON.parse(exchangeRates);
                for (var i in currenciesObj) {
                    var exchangeRate = exchangeRatesObj[i] || 1;
                    currenciesObj[i].exchangeRate = exchangeRate;
                }
            }
            catch(ex) {
                nlapiLogExecution('error', '[ep] Sublist_ExchangeRate:setSublistData', 'Error in parsing currencies/exchange rates');
            }
            
            if (currenciesObj) {
                var currencyRows = [];
                // map currency fields to row lines
                for (var i in currenciesObj) {
                    var currencyRow = {
                        custpage_currency_id: i,
                        custpage_currency_name: currenciesObj[i].name,
                        custpage_currency_symbol: currenciesObj[i].symbol,
                        custpage_currency_exchange: currenciesObj[i].exchangeRate,
                    };
                    if (i != baseCurrency && (baseCurrency == bankCurrency || i == bankCurrency)) {
                        currencyRows.push(currencyRow);
                    }
                }
                this._sublistObj.setLineItemValues(currencyRows);
            }
        }
    }
    
    /**
     * Adds the button to the sublist tab
     */
    function addButton(buttonId, label, func) {
        this._sublistObj.addButton(buttonId, label, func);
    }
    
    this.BuildUI = buildUI;
    this.SetSublistData = setSublistData;
    this.AddButton = addButton;
};

/**
 * Get the base currency
 * 
 * @param {subsidiary}
 */
psg_ep.GetBaseCurrency = function(subsidiary) {
	
	function getConfigurationObject() {
	    var companyInfoConfigurationObject = {};
	    try {
	        var adminFieldsUrl = nlapiResolveURL('SUITELET', 'customscript_ep_admin_data_loader_s', 'customdeploy_ep_admin_data_loader_s', true);
	        var params = {};
	        params['custparam_ep_language'] = nlapiGetContext().getPreference('LANGUAGE');
	        var adminFields = nlapiRequestURL(adminFieldsUrl, params);
	        var adminFieldsResult = adminFields.getBody();
	        companyInfoConfigurationObject = JSON.parse(adminFieldsResult);
	    }
	    catch(ex) {
	        nlapiLogExecution('error', '[ep]Payment Batch Processing: getConfigurationObject', 'Error occurred while loading the configuration: ' + ex.getCode() + '\n' + ex.getDetails());
	    }
	    return companyInfoConfigurationObject;
	}
	
    var baseCurrency;
    // get base currency
    if (isOneWorld() && subsidiary) {
        baseCurrency = nlapiLookupField('subsidiary', subsidiary, 'currency');
    }
    else if (!isOneWorld()){
    	var config = getConfigurationObject();
    	if (config && config.companyinformation && config.companyinformation.basecurrency) {
			baseCurrency = config.companyinformation.basecurrency + '';
		} 
    }
    return baseCurrency;
};


/**
 * Function to retrieve exchange rates per currency based on unput base currency
 * 
 * @param {String} baseCurrency
 * @param {Array} currencies
 */

psg_ep.GetExchangeRates = function(baseCurrency, currencies) {
	var exchangeRates = {};
	if (nlapiGetContext().getSetting('FEATURE', 'MULTICURRENCY') == 'T') {
		if (!baseCurrency) {
			throw nlapiCreateError('EP_BASECURRENCY_NULL', 'Cannot get exchange rates without base currency.');        
		}
		if (!currencies) {
			throw nlapiCreateError('EP_TARGETCURRENCIES_NULL', 'Cannot get exchange rates withour target currencies.');        
		}
		var url = nlapiResolveURL('suitelet', 'customscript_2663_exchangerates_loader_s', 'customdeploy_2663_exchangerates_loader_s', true);
		var currenciesBatch = [];
		var currenciesLength = currencies.length; 
		if (currenciesLength > 90) {
			var numOfBatches = Math.ceil(currenciesLength / 90);
			for (var i = 0; i < numOfBatches; i++) {
				currenciesBatch.push(currencies.slice(i * 90, (i + 1) * 90));
			}
		} else {
			currenciesBatch.push(currencies);
		}
		currenciesBatch.forEach(function getExchangeRates(tranCurrencies) {
			var response = nlapiRequestURL(url, {base_currency: baseCurrency, transaction_currencies: JSON.stringify(tranCurrencies)});
			var tranExchangeRates = JSON.parse(response.getBody());
			Object.keys(tranExchangeRates).forEach(function addExchangeRates(id) {
				exchangeRates[id] = tranExchangeRates[id];
			});
		});
	}
	
	return exchangeRates;
};/**
 * @author mcadano
/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2011/07/10  250004         2.00.19      			  Initital version
 * 2013/09/23  255091		  2.00.18				  Added Billing Sequence Types
 * 2013/09/24  263190		  3.00.00				  Add Approval Routing constants
 * 2013/09/26  263190 		  3.00.00				  Add Schedule Event type constants and MAXTIMELIMIT
 * 2013/11/07  267045 		  3.00.00				  Add Reference Amendment constants
 * 2013/11/26                 3.00.00				  Add UUID for file under email templates folder
 * 2014/03/21   		       			              Add country source object
 * 2014/05/19  273487         3.00.3       			  Add constants for batch processing scripts, process and status
 */

//Mutli-select separator char
var MULTISELECT_SEPARATOR = String.fromCharCode(5);

//Maximum time limit
var MAXTIMELIMIT = 240000;	//4 mins 

//Payment Type
var EP_EFT = '1';
var EP_DD = '2';
var EP_PP = '3';


// Payment Status's
var PAYQUEUED = 1;
var PAYMARKPAYMENTS = 5;
var PAYPROCESSING = 2;
var PAYCREATINGFILE = 3;
var PAYPROCESSED = 4;
var PAYREVERSAL = 6;
var PAYNOTIFICATION = 7;
var PAYERROR = 8;
var PAYFAILED = 9;
var PAYCANCELLED = 10;
var PAYDELETINGFILE = 11;
var REQUEUED = 12;
var UPDATINGBATCH = 13;

//Payment Batch status
var BATCH_OPEN = '1';
var BATCH_UPDATING = '2';
var BATCH_PENDINGAPPROVAL = '3';
var BATCH_SUBMITTED = '4';
var BATCH_REJECTED = '5';

//Approval Routing Level
var APPROVAL_LEVEL1 = 1;
var APPROVAL_LEVEL2 = 2;
var APPROVAL_LEVEL3 = 3;

//Approval Type
var APPROVALTYPE_BILLPAYMENT = '1';
var APPROVALTYPE_VENDORPAYMENT = '2';
var APPROVALTYPE_BATCHPAYMENT = '3';

//Schedule Event Type
var EVENTTYPE_CLOSING = '1';
var EVENTTYPE_CREATION = '2';

var VALID_COUNTRY_CODES = [
	'AF', 'AX', 'AL', 'DZ', 'AS', 'AD', 'AO', 'AI', 'AQ', 'AG', 'AR', 'AM', 'AW', 'AU', 'AT', 'AZ', 'BS', 'BH', 'BD', 'BB', 'BY', 'BE',
	'BZ', 'BJ', 'BM', 'BT', 'BO', 'BQ', 'BA', 'BW', 'BV', 'BR', 'IO', 'BN', 'BG', 'BF', 'BI', 'KH', 'CM', 'CA', 'IC', 'CV', 'KY', 'CF',
	'EA', 'TD', 'CL', 'CN', 'CX', 'CC', 'CO', 'KM', 'CD', 'CG', 'CK', 'CR', 'CI', 'HR', 'CU', 'CW', 'CY', 'CZ', 'DK', 'DJ', 'DM', 'DO',
	'TP', 'EC', 'EG', 'SV', 'GQ', 'ER', 'EE', 'ET', 'FK', 'FO', 'FJ', 'FI', 'FR', 'GF', 'PF', 'TF', 'GA', 'GM', 'GE', 'DE', 'GH', 'GI',
	'GR', 'GL', 'GD', 'GP', 'GU', 'GT', 'GG', 'GN', 'GW', 'GY', 'HT', 'HM', 'VA', 'HN', 'HK', 'HU', 'IS', 'IN', 'ID', 'IR', 'IQ', 'IE',
	'IM', 'IL', 'IT', 'JM', 'JP', 'JE', 'JO', 'KZ', 'KE', 'KI', 'KP', 'KR', 'KW', 'KG', 'LA', 'LV', 'LB', 'LS', 'LR', 'LY', 'LI', 'LT',
	'LU', 'MO', 'MK', 'MG', 'MW', 'MY', 'MV', 'ML', 'MT', 'MH', 'MQ', 'MR', 'MU', 'YT', 'MX', 'FM', 'MD', 'MC', 'MN', 'ME', 'MS', 'MA',
	'MZ', 'MM', 'NA', 'NR', 'NP', 'NL', 'AN', 'NC', 'NZ', 'NI', 'NE', 'NG', 'NU', 'NF', 'MP', 'NO', 'OM', 'PK', 'PW', 'PS', 'PA', 'PG',
	'PY', 'PE', 'PH', 'PN', 'PL', 'PT', 'PR', 'QA', 'RE', 'RO', 'RU', 'RW', 'BL', 'SH', 'KN', 'LC', 'MF', 'VC', 'WS', 'SM', 'ST', 'SA',
	'SN', 'RS', 'CS', 'SC', 'SL', 'SG', 'SX', 'SK', 'SI', 'SB', 'SO', 'ZA', 'GS', 'SS', 'ES', 'LK', 'PM', 'SD', 'SR', 'SJ', 'SZ', 'SE',
	'CH', 'SY', 'TW', 'TJ', 'TZ', 'TH', 'TG', 'TK', 'TO', 'TT', 'TN', 'TR', 'TM', 'TC', 'TV', 'UG', 'UA', 'AE', 'GB', 'US', 'UY', 'UM',
	'UZ', 'VU', 'VE', 'VN', 'VG', 'VI', 'WF', 'EH', 'YE', 'ZM', 'ZW'
];

var COUNTRY_SOURCE = {
    "Andorra"                               : { code: "AD" },
    "United Arab Emirates"                  : { code: "AE" },
    "Afghanistan"                           : { code: "AF" },
    "Antigua and Barbuda"                   : { code: "AG" },
    "Anguilla"                              : { code: "AI" },
    "Albania"                               : { code: "AL" },
    "Armenia"                               : { code: "AM" },
    "Netherlands Antilles"                  : { code: "AN" },
    "Angola"                                : { code: "AO" },
    "Antarctica"                            : { code: "AQ" },
    "Argentina"                             : { code: "AR" },
    "American Samoa"                        : { code: "AS" },
    "Austria"                               : { code: "AT" },
    "Australia"                             : { code: "AU" },
    "Aruba"                                 : { code: "AW" },
    "Aland Islands"                         : { code: "AX" },
    "Azerbaijan"                            : { code: "AZ" },
    "Bosnia and Herzegovina"                : { code: "BA" },
    "Barbados"                              : { code: "BB" },
    "Bangladesh"                            : { code: "BD" },
    "Belgium"                               : { code: "BE" },
    "Burkina Faso"                          : { code: "BF" },
    "Bulgaria"                              : { code: "BG" },
    "Bahrain"                               : { code: "BH" },
    "Burundi"                               : { code: "BI" },
    "Benin"                                 : { code: "BJ" },
    "Saint Barth�lemy"                      : { code: "BL" },
    "Bermuda"                               : { code: "BM" },
    "Brunei Darrussalam"                    : { code: "BN" },
    "Bolivia"                               : { code: "BO" },
    "Brazil"                                : { code: "BR" },
    "Bahamas"                               : { code: "BS" },
    "Bhutan"                                : { code: "BT" },
    "Bouvet Island"                         : { code: "BV" },
    "Botswana"                              : { code: "BW" },
    "Belarus"                               : { code: "BY" },
    "Belize"                                : { code: "BZ" },
    "Canada"                                : { code: "CA" },
    "Cocos (Keeling) Islands"               : { code: "CC" },
    "Congo, Democratic People's Republic"   : { code: "CD" },
    "Central African Republic"              : { code: "CF" },
    "Congo, Republic of"                    : { code: "CG" },
    "Switzerland"                           : { code: "CH" },
    "Cote d'Ivoire"                         : { code: "CI" },
    "Cook Islands"                          : { code: "CK" },
    "Chile"                                 : { code: "CL" },
    "Cameroon"                              : { code: "CM" },
    "China"                                 : { code: "CN" },
    "Colombia"                              : { code: "CO" },
    "Costa Rica"                            : { code: "CR" },
    "Serbia and Montenegro (Deprecated)"    : { code: "CS" },
    "Cuba"                                  : { code: "CU" },
    "Cape Verde"                            : { code: "CV" },
    "Christmas Island"                      : { code: "CX" },
    "Cyprus"                                : { code: "CY" },
    "Czech Republic"                        : { code: "CZ" },
    "Germany"                               : { code: "DE" },
    "Djibouti"                              : { code: "DJ" },
    "Denmark"                               : { code: "DK" },
    "Dominica"                              : { code: "DM" },
    "Dominican Republic"                    : { code: "DO" },
    "Algeria"                               : { code: "DZ" },
    "Ceuta and Melilla"                     : { code: "EA" },
    "Ecuador"                               : { code: "EC" },
    "Estonia"                               : { code: "EE" },
    "Egypt"                                 : { code: "EG" },
    "Western Sahara"                        : { code: "EH" },
    "Eritrea"                               : { code: "ER" },
    "Spain"                                 : { code: "ES" },
    "Ethiopia"                              : { code: "ET" },
    "Finland"                               : { code: "FI" },
    "Fiji"                                  : { code: "FJ" },
    "Falkland Islands"                      : { code: "FK" },
    "Micronesia, Federal State of"          : { code: "FM" },
    "Faroe Islands"                         : { code: "FO" },
    "France"                                : { code: "FR" },
    "Gabon"                                 : { code: "GA" },
    "United Kingdom (GB)"                   : { code: "GB" },
    "Grenada"                               : { code: "GD" },
    "Georgia"                               : { code: "GE" },
    "French Guiana"                         : { code: "GF" },
    "Guernsey"                              : { code: "GG" },
    "Ghana"                                 : { code: "GH" },
    "Gibraltar"                             : { code: "GI" },
    "Greenland"                             : { code: "GL" },
    "Gambia"                                : { code: "GM" },
    "Guinea"                                : { code: "GN" },
    "Guadeloupe"                            : { code: "GP" },
    "Equatorial Guinea"                     : { code: "GQ" },
    "Greece"                                : { code: "GR" },
    "South Georgia"                         : { code: "GS" },
    "Guatemala"                             : { code: "GT" },
    "Guam"                                  : { code: "GU" },
    "Guinea-Bissau"                         : { code: "GW" },
    "Guyana"                                : { code: "GY" },
    "Hong Kong"                             : { code: "HK" },
    "Heard and McDonald Islands"            : { code: "HM" },
    "Honduras"                              : { code: "HN" },
    "Croatia/Hrvatska"                      : { code: "HR" },
    "Haiti"                                 : { code: "HT" },
    "Hungary"                               : { code: "HU" },
    "Canary Islands"                        : { code: "IC" },
    "Indonesia"                             : { code: "ID" },
    "Ireland"                               : { code: "IE" },
    "Israel"                                : { code: "IL" },
    "Isle of Man"                           : { code: "IM" },
    "India"                                 : { code: "IN" },
    "British Indian Ocean Territory"        : { code: "IO" },
    "Iraq"                                  : { code: "IQ" },
    "Iran (Islamic Republic of)"            : { code: "IR" },
    "Iceland"                               : { code: "IS" },
    "Italy"                                 : { code: "IT" },
    "Jersey"                                : { code: "JE" },
    "Jamaica"                               : { code: "JM" },
    "Jordan"                                : { code: "JO" },
    "Japan"                                 : { code: "JP" },
    "Kenya"                                 : { code: "KE" },
    "Kyrgyzstan"                            : { code: "KG" },
    "Cambodia"                              : { code: "KH" },
    "Kiribati"                              : { code: "KI" },
    "Comoros"                               : { code: "KM" },
    "Saint Kitts and Nevis"                 : { code: "KN" },
    "Korea, Democratic People's Republic"   : { code: "KP" },
    "Korea, Republic of"                    : { code: "KR" },
    "Kuwait"                                : { code: "KW" },
    "Cayman Islands"                        : { code: "KY" },
    "Kazakhstan"                            : { code: "KZ" },
    "Lao People's Democratic Republic"      : { code: "LA" },
    "Lebanon"                               : { code: "LB" },
    "Saint Lucia"                           : { code: "LC" },
    "Liechtenstein"                         : { code: "LI" },
    "Sri Lanka"                             : { code: "LK" },
    "Liberia"                               : { code: "LR" },
    "Lesotho"                               : { code: "LS" },
    "Lithuania"                             : { code: "LT" },
    "Luxembourg"                            : { code: "LU" },
    "Latvia"                                : { code: "LV" },
    "Libyan Arab Jamahiriya"                : { code: "LY" },
    "Morocco"                               : { code: "MA" },
    "Monaco"                                : { code: "MC" },
    "Moldova, Republic of"                  : { code: "MD" },
    "Montenegro"                            : { code: "ME" },
    "Saint Martin"                          : { code: "MF" },
    "Madagascar"                            : { code: "MG" },
    "Marshall Islands"                      : { code: "MH" },
    "Macedonia"                             : { code: "MK" },
    "Mali"                                  : { code: "ML" },
    "Myanmar"                               : { code: "MM" },
    "Mongolia"                              : { code: "MN" },
    "Macau"                                 : { code: "MO" },
    "Northern Mariana Islands"              : { code: "MP" },
    "Martinique"                            : { code: "MQ" },
    "Mauritania"                            : { code: "MR" },
    "Montserrat"                            : { code: "MS" },
    "Malta"                                 : { code: "MT" },
    "Mauritius"                             : { code: "MU" },
    "Maldives"                              : { code: "MV" },
    "Malawi"                                : { code: "MW" },
    "Mexico"                                : { code: "MX" },
    "Malaysia"                              : { code: "MY" },
    "Mozambique"                            : { code: "MZ" },
    "Namibia"                               : { code: "NA" },
    "New Caledonia"                         : { code: "NC" },
    "Niger"                                 : { code: "NE" },
    "Norfolk Island"                        : { code: "NF" },
    "Nigeria"                               : { code: "NG" },
    "Nicaragua"                             : { code: "NI" },
    "Netherlands"                           : { code: "NL" },
    "Norway"                                : { code: "NO" },
    "Nepal"                                 : { code: "NP" },
    "Nauru"                                 : { code: "NR" },
    "Niue"                                  : { code: "NU" },
    "New Zealand"                           : { code: "NZ" },
    "Oman"                                  : { code: "OM" },
    "Panama"                                : { code: "PA" },
    "Peru"                                  : { code: "PE" },
    "French Polynesia"                      : { code: "PF" },
    "Papua New Guinea"                      : { code: "PG" },
    "Philippines"                           : { code: "PH" },
    "Pakistan"                              : { code: "PK" },
    "Poland"                                : { code: "PL" },
    "St. Pierre and Miquelon"               : { code: "PM" },
    "Pitcairn Island"                       : { code: "PN" },
    "Puerto Rico"                           : { code: "PR" },
    "Palestinian Territories"               : { code: "PS" },
    "Portugal"                              : { code: "PT" },
    "Palau"                                 : { code: "PW" },
    "Paraguay"                              : { code: "PY" },
    "Qatar"                                 : { code: "QA" },
    "Reunion Island"                        : { code: "RE" },
    "Romania"                               : { code: "RO" },
    "Serbia"                                : { code: "RS" },
    "Russian Federation"                    : { code: "RU" },
    "Rwanda"                                : { code: "RW" },
    "Saudi Arabia"                          : { code: "SA" },
    "Solomon Islands"                       : { code: "SB" },
    "Seychelles"                            : { code: "SC" },
    "Sudan"                                 : { code: "SD" },
    "Sweden"                                : { code: "SE" },
    "Singapore"                             : { code: "SG" },
    "Saint Helena"                          : { code: "SH" },
    "Slovenia"                              : { code: "SI" },
    "Svalbard and Jan Mayen Islands"        : { code: "SJ" },
    "Slovak Republic"                       : { code: "SK" },
    "Sierra Leone"                          : { code: "SL" },
    "San Marino"                            : { code: "SM" },
    "Senegal"                               : { code: "SN" },
    "Somalia"                               : { code: "SO" },
    "Suriname"                              : { code: "SR" },
    "Sao Tome and Principe"                 : { code: "ST" },
    "El Salvador"                           : { code: "SV" },
    "Syrian Arab Republic"                  : { code: "SY" },
    "Swaziland"                             : { code: "SZ" },
    "Turks and Caicos Islands"              : { code: "TC" },
    "Chad"                                  : { code: "TD" },
    "French Southern Territories"           : { code: "TF" },
    "Togo"                                  : { code: "TG" },
    "Thailand"                              : { code: "TH" },
    "Tajikistan"                            : { code: "TJ" },
    "Tokelau"                               : { code: "TK" },
    "Turkmenistan"                          : { code: "TM" },
    "Tunisia"                               : { code: "TN" },
    "Tonga"                                 : { code: "TO" },
    "East Timor"                            : { code: "TP" },
    "Turkey"                                : { code: "TR" },
    "Trinidad and Tobago"                   : { code: "TT" },
    "Tuvalu"                                : { code: "TV" },
    "Taiwan"                                : { code: "TW" },
    "Tanzania"                              : { code: "TZ" },
    "Ukraine"                               : { code: "UA" },
    "Uganda"                                : { code: "UG" },
    "US Minor Outlying Islands"             : { code: "UM" },
    "United States"                         : { code: "US" },
    "Uruguay"                               : { code: "UY" },
    "Uzbekistan"                            : { code: "UZ" },
    "Holy See (City Vatican State)"         : { code: "VA" },
    "Saint Vincent and the Grenadines"      : { code: "VC" },
    "Venezuela"                             : { code: "VE" },
    "Virgin Islands (British)"              : { code: "VG" },
    "Virgin Islands (USA)"                  : { code: "VI" },
    "Vietnam"                               : { code: "VN" },
    "Vanuatu"                               : { code: "VU" },
    "Wallis and Futuna Islands"             : { code: "WF" },
    "Samoa"                                 : { code: "WS" },
    "Yemen"                                 : { code: "YE" },
    "Mayotte"                               : { code: "YT" },
    "South Africa"                          : { code: "ZA" },
    "Zambia"                                : { code: "ZM" },
    "Zimbabwe"                              : { code: "ZW" }
};

var VALID_SCHEDULE_STATUSES = ['QUEUED', 'INQUEUE', 'INPROGRESS'];
var TERMINAL_DEPLOYMENT_STATUSES = ['COMPLETED', 'NOTSCHEDULED'];

// EP Processes
var EP_PROCESSPAYMENTS = '1';
var EP_REPROCESS = '2';
var EP_ROLLBACK = '3';
var EP_REVERSEPAYMENTS = '4';
var EP_EMAILNOTIFICATION = '5';
var EP_CREATEFILE = '6';
var EP_CREATEBATCH = '7';
var EP_CLOSEBATCH = '8';

//bank account types
var BANK_ACCT_TYPE_CHECKING = 1;
var BANK_ACCT_TYPE_SAVINGS = 2;

//billing sequence types
var BILL_SEQ_FRST = '1';
var BILL_SEQ_RCUR = '2';
var BILL_SEQ_FNAL = '3';
var BILL_SEQ_OOFF = '4';

//reference amendments
var AMEND_NONE = '1';
var AMEND_MANDATE_ID = '2';
var AMEND_DEBTOR_ACCT = '3';
var AMEND_DEBTOR_AGENT = '4';
var AMEND_CREDITOR_ID = '5';

//Scheduled Script Ids
var EP_PAYMENTPROCESSING_SCRIPT = 'customscript_2663_payment_processing_ss';
var EP_PAYMENTCREATOR_SCRIPT = 'customscript_ep_payment_creator_ss';
var EP_ROLLBACK_SCRIPT = 'customscript_2663_rollback_ss';
var EP_REVERSEPAYMENTS_SCRIPT = 'customscript_2663_reverse_payments_ss';
var EP_EMAILNOTIFICATION_SCRIPT = 'customscript_2663_notify_payments_ss';
var EP_BATCHPROCESSING_SCRIPT = 'customscript_2663_batch_processing_ss';
var EP_ONDEMANDBATCH_SCRIPT = 'customscript_2663_on_demand_batch_ss';
var EP_SS_SCRIPT_IDS = [
	EP_PAYMENTPROCESSING_SCRIPT,
	EP_PAYMENTCREATOR_SCRIPT,
	EP_ROLLBACK_SCRIPT,
	EP_REVERSEPAYMENTS_SCRIPT,
	EP_EMAILNOTIFICATION_SCRIPT,
	EP_BATCHPROCESSING_SCRIPT,
	EP_ONDEMANDBATCH_SCRIPT
];

//Scheduled Script Deployment Ids
var EP_PAYMENTPROCESSING_DEPLOY = 'customdeploy_2663_payment_processing_ss';
var EP_PAYMENTCREATOR_DEPLOY = 'customdeploy_ep_payment_creator_ss';
var EP_ROLLBACK_DEPLOY = 'customdeploy_2663_rollback_ss';
var EP_REVERSEPAYMENTS_DEPLOY = 'customdeploy_2663_reverse_payments_ss';
var EP_EMAILNOTIFICATION_DEPLOY = 'customdeploy_2663_notify_payments_ss';
var EP_BATCHPROCESSING_DEPLOY = 'customdeploy_2663_batch_processing_ss';
var EP_ONDEMANDBATCH_DEPLOY = 'customdeploy_2663_on_demand_batch_ss';

//UUID for folder locations
var EP_UUID_EMAIL_TEMPLATES = 'dd874a40-535a-11e3-8f96-0800200c9a66';
/**
 * @author alaurito
 */

/**
 * Revision History:
 * 
 * Date        Fixed Issue    Broken in QA Bundle     Issue Fix Summary
 * =============================================================================================
 * 2011/06/08  199331         1.00.2011.05.27.01      Filter bank details to show only banks
 *                                                    with EFT format
 * 2011/06/17  199847         1.01.2011.06.17.02      Do not include inactive bank accounts in
 *                                                    bank account dropdown list
 * 2011/06/21  199847         1.01.2011.06.24.01      Do not include inactive AP accounts in
 *                                                    dropdown list
 * 2011/06/24  200164         1.01.2011.06.24.02      Include journal entries in list of
 *                                                    bills for selection even if due date
 *                                                    filters are set
 * 2011/07/26  202323         1.05                    Sort bills by Payee Name
 * 2011/07/27  202422         1.06.2011.07.29.01      Set custom labels from Rename Records
 *             202323         1.05                    Ignore case when sorting by Payee Name
 * 2011/08/12  203530         1.07.2011.08.11.01      Transfer processing to library
 *                                                    Fix for 1000 record limit
 * 2011/08/17  203853         1.07.2011.08.18.02      Parallel runs for payment scheduled script
 * 2011/09/12  205257         1.11.2011.09.15.01      Restrict subsidiary view based on user's
 *                                                    permissions
 * 2011/09/19  203845         1.07.2011.08.18.02      Edit error codes
 * 2012/08/01  227867         2.01.1                  Refactor for payment portal functionality
 * 2012/08/03  227868         2.01.1                  Changes based on FRD
 * 2012/08/16  227868         2.01.1                  Do not list transactions from closed batches
 * 2012/08/30  227867         2.01.1                  Update for test automation
 * 2012/09/17  227868         2.01.1                  Changes to user interface 
 * 2012/09/28  231712         2.01.1                  Fixes for unit test automation scripts
 * 2012/10/17  233546         2.01.1                  Apply fix to include bills for vendors with
 *                                                    zero/negative balance
 * 2012/10/25  233946         2.01.1                  Do not display AP assigned as auto-AP for
 *                                                    format       
 * 2012/11/14  235650         2.00.2                  Do not perform search for transactions for
 *                                                    entities without balance if exception was
 *                                                    already encountered
 * 2012/11/15  235654         2.00.3                  Do not use noneof filter when excluding
 *                                                    transactions from batch; get all id's then 
 *                                                    compare in array instead
 * 2012/11/27  236622         2.00.3                  Use saved searches in order to allow performance
 *                                                    team to add hints (only for Bill Payment suitelet)
 * 2012/11/28  236622         2.00.3                  Additional fix to load vendors without balance
 *                                                    through saved search
 * 2012/11/22  237873         2.00.3                  Add currency and exchange rate related fields;
 *                                                    retrieve transactions for format currencies
 * 2012/12/17  238204         2.00.5                  Add search filters for new Groupon filter fields
 * 2012/12/20  238204         2.00.5                  Fix Hold Payments filter to show all bills when checked                                                    
 * 2013/02/12  242626         2.00.5                  Use function psg_ep.isGrouponAccount to check if account is Groupon
 * 2013/03/11  245306         2.00.10                 Add field custpage_2663_ppclosed
 * 2013/04/16  248888   	  2.00.12				  Change GetClosedBatches to GetBatches
 * 2013/05/23  250917   	  2.00.15				  Add error handling when searching for entities without balance times out
 * 2013/06/17  254321   	  2.00.18				  Use optimized transaction search
 * 2013/06/18  254321   	  2.00.18				  Add search filters for Groupon's entity filters
 * 2013/07/15  257010   	  2.00.19				  Add subsidiary filter for transaction search
 * 2013/07/18  257505 		  2.00.18				  Add error handling when governance or time limit is reached during entities search
 * 2013/07/15  245723		  2.00.10				  Add support for commission transactions 
 * 2013/08/14  260267		  2.00.23.2013.08.15.1    Add checking for commission feature before adding partners to partner filter
 * 2013/08/29  261772 		  2.00.25.2013.08.29.3	  Added checking for employee commission feature
 * 2013/09/19  263190 		  3.00.00				  Use _2663.Logger and add handling for approval routing
 * 2013/10/05  265406 		  3.00.00				  Refactor code to use Payment File Administration record instead of Payment Batch
 * 2013/12/16  272776 		  3.00.02				  Add batch filters only when approval routing is enabled 
 * 2013/12/20  272780         3.00.00     	  		  Add sublist columns custpage_entityid and custpage_trantype
 * 2014/01/24  273463 		  3.00.10				  Include partially paid bills from EP when approval routing is enabled
 * 2014/03/27  275808		  3.00.00				  Add workaround for duplicate bills
 * 2014/04/25  287659		  3.02.4				  Include name column in transaction sublist
 * 2014/04/29  201833								  Add support for EP plugin
 */

var psg_ep;

if (!psg_ep)
    psg_ep = {};

/**
 * Loads EFT form
 *
 * @param {Object} request
 * @param {Object} response
 */
function main(request, response){
    psg_ep.PaymentSelectionFormMain_EFT(request, response);
}

/**
 * Main function to load EFT form
 * 
 * @param {nlobjRequest} request
 * @param {nlobjResponse} response
 */
psg_ep.PaymentSelectionFormMain_EFT = function(request, response) {
	var logger = new _2663.Logger('[ep] PaymentSelectionFormMain_EFT');
    var eftUIObj = new psg_ep.PaymentSelectionForm_EFT();
    logger.debug('Building UI...');
    eftUIObj.BuildUI();
    logger.debug('Initializing request parameters...');
    eftUIObj.InitializeRequestParameters(request);
    logger.debug('Populating form fields...');
    eftUIObj.InitializeFieldData();
    logger.debug('Checking for approval routing settings...');
    eftUIObj.CheckApprovalRouting();   
    logger.debug('Loading sublist...');
    eftUIObj.LoadSublist(request);
    logger.debug('Writing page...');
    response.writePage(eftUIObj._uiObj.Form);
    logger.debug('Done.');
};

/**
 * Class that handles EFT form creation
 * 
 * @returns {psg_ep.PaymentSelectionForm_EFT}
 */
psg_ep.PaymentSelectionForm_EFT = function() {
	var logTitle = '[ep] PaymentSelectionForm_EFT';
	var logger = new _2663.Logger(logTitle);
    // parent class for framework
    this._uiBuilderInterface = new psg_ep.FormUIBuilderInterface();
    this._uiObj = new psg_ep.PaymentSelectionForm(this._uiBuilderInterface);

    ////////////////////////////////////////////////////////////////////////////////////
    // Framework Functions - Start
    /**
     * Builds the basic form UI
     */
    function buildUI() {
        // set the field groups
        var fieldGroups = this._uiObj.FieldGroups;
        fieldGroups.push('custpage_2663_filtergrp');
        fieldGroups.push('custpage_2663_payinfogrp');
        if (isDepartment() || isClass() || isLocation()) {
            fieldGroups.push('custpage_2663_classgrp');
        }
        if (psg_ep.isGrouponAccount(nlapiGetContext().getCompany())) {
            fieldGroups.push('custpage_2663_grp_transfilters');
            fieldGroups.push('custpage_2663_grp_entityfilters');
        }
        
        if(psg_ep.CustomTransactionFilterFields){
        	fieldGroups.push('custpage_2663_custom_transfilters');
        }

        this._uiBuilderInterface.GetFieldsForFieldGroup = getFieldsForFieldGroup_EFT;
        
        // set the sublist class
        this._uiBuilderInterface.SublistClass = psg_ep.Sublist_EFT;
        
        // add the buttons
        this._uiObj.ButtonIds.push('custpage_submitter');
        this._uiObj.ButtonIds.push('custpage_cancel');
        
        // build the form
        this._uiObj._buildUI('EFT - Bill Payments', 'customscript_ep_selection_cs');
        
    }
    
    /**
     * Returns the fields based on field group
     * 
     * @returns {}
     */
    function getFieldsForFieldGroup_EFT(fieldGroup) {
        var fieldGroupObjects = {};
        if (fieldGroup == 'custpage_2663_filtergrp') {
        	var batchIdObj = psg_ep.BatchInfoFields['custpage_2663_batchid'];
        	batchIdObj.displayType = 'hidden';
        	fieldGroupObjects['custpage_2663_batchid'] = batchIdObj;
        	
            var paymentTypeObj = psg_ep.CommonFormFields['custpage_2663_paymenttype'];
            paymentTypeObj.defaultValue = 'eft';
            fieldGroupObjects['custpage_2663_paymenttype'] = paymentTypeObj;
            
            var approvalRoutingObj = psg_ep.CommonFormFields['custpage_2663_approval_routing'];
            approvalRoutingObj.defaultValue = _2663.isApprovalRoutingEnabled() ? 'T' : 'F';
            fieldGroupObjects['custpage_2663_approval_routing'] = approvalRoutingObj;
            
            fieldGroupObjects['custpage_2663_refresh_page'] = psg_ep.CommonFormFields['custpage_2663_refresh_page'];
            fieldGroupObjects['custpage_2663_file_creation_timestamp'] = psg_ep.CommonFormFields['custpage_2663_file_creation_timestamp'];
            fieldGroupObjects['custpage_2663_trans_marked'] = psg_ep.CommonFormFields['custpage_2663_trans_marked'];
            fieldGroupObjects['custpage_2663_bank_account'] = psg_ep.CommonFormFields['custpage_2663_bank_account'];
            fieldGroupObjects['custpage_2663_ap_account'] = psg_ep.EFTFormFields['custpage_2663_ap_account'];
            fieldGroupObjects['custpage_2663_transtype'] = psg_ep.CommonFormFields['custpage_2663_transtype'];
            fieldGroupObjects['custpage_2663_date_from'] = psg_ep.CommonFormFields['custpage_2663_date_from'];
            fieldGroupObjects['custpage_2663_date_to'] = psg_ep.CommonFormFields['custpage_2663_date_to'];
            fieldGroupObjects['custpage_2663_vendor'] = psg_ep.EFTFormFields['custpage_2663_vendor'];
            fieldGroupObjects['custpage_2663_employee'] = psg_ep.EFTFormFields['custpage_2663_employee'];
            
            if (isCommissionEnabled()) {
            	fieldGroupObjects['custpage_2663_partner'] = psg_ep.EFTFormFields['custpage_2663_partner'];
            }
            
            if (psg_ep.HoldPaymentAcctIds.indexOf(nlapiGetContext().getCompany()) != -1) {
                fieldGroupObjects['custpage_2663_onhold'] = psg_ep.EFTFormFields['custpage_2663_onhold'];
            }
            
            if(psg_ep.CustomSearchFilters){
	        	for (var i in psg_ep.CustomSearchFilters) {
	                fieldGroupObjects[i] = psg_ep.CustomSearchFilters[i];            
	            }
	        	if(Object.keys(fieldGroupObjects).indexOf('custpage_2663_custom_flds_transfilters') < 0){
	        		fieldGroupObjects['custpage_2663_custom_flds_transfilters'] = psg_ep.CustomFields['custpage_2663_custom_flds_transfilters'];
	        	}
            	
            }
            
            fieldGroupObjects['custpage_2663_remove_flds_transfilters'] = psg_ep.CustomFields['custpage_2663_remove_flds_transfilters'];
                        
            var removeEntityFieldsStr = psg_ep.CustomFields['custpage_2663_remove_flds_transfilters'].defaultValue;
            
            var removeFields = [];
            if (removeEntityFieldsStr) {
                var removeEntityFields = JSON.parse(removeEntityFieldsStr);
                removeFields = removeFields.concat(removeEntityFields);
            }
            
            // check if the field name is to be removed
            for (var i = 0; i < removeFields.length; i++) {
            	fieldGroupObjects[removeFields[i]].displayType = 'hidden';   
            }

        } else if (fieldGroup == 'custpage_2663_payinfogrp') {
            fieldGroupObjects['custpage_2663_process_date'] = psg_ep.CommonFormFields['custpage_2663_process_date'];
            fieldGroupObjects['custpage_2663_postingperiod'] = psg_ep.CommonFormFields['custpage_2663_postingperiod'];
            fieldGroupObjects['custpage_2663_ppstart'] = psg_ep.CommonFormFields['custpage_2663_ppstart'];
            fieldGroupObjects['custpage_2663_ppend'] = psg_ep.CommonFormFields['custpage_2663_ppend'];
            fieldGroupObjects['custpage_2663_ppclosed'] = psg_ep.CommonFormFields['custpage_2663_ppclosed'];
            fieldGroupObjects['custpage_2663_payment_ref'] = psg_ep.EFTFormFields['custpage_2663_payment_ref'];
            fieldGroupObjects['custpage_2663_aggregate'] = psg_ep.EFTFormFields['custpage_2663_aggregate'];
            fieldGroupObjects['custpage_2663_agg_method'] = psg_ep.EFTFormFields['custpage_2663_agg_method'];
            fieldGroupObjects['custpage_2663_payment_lines'] = psg_ep.CommonFormFields['custpage_2663_payment_lines'];
            fieldGroupObjects['custpage_2663_total_amount'] = psg_ep.CommonFormFields['custpage_2663_total_amount'];
            fieldGroupObjects['custpage_2663_approval_amount'] = psg_ep.CommonFormFields['custpage_2663_approval_amount'];
            fieldGroupObjects['custpage_2663_remove_flds_trans'] = psg_ep.CustomFields['custpage_2663_remove_flds_trans'];
            
            var removeFieldsStr = psg_ep.CustomFields['custpage_2663_remove_flds_trans'].defaultValue;
            
            var removeFields = [];
            if (removeFieldsStr) {
                var removeEntityFields = JSON.parse(removeFieldsStr);
                removeFields = removeFields.concat(removeEntityFields);
            }
            
            // check if the field name is to be removed
            for (var i = 0; i < removeFields.length; i++) {
            	fieldGroupObjects[removeFields[i]].displayType = 'hidden';   
            }

        } else if (fieldGroup == 'custpage_2663_classgrp') {
            if (isDepartment()) {
                fieldGroupObjects['custpage_2663_department'] = psg_ep.CommonFormFields['custpage_2663_department'];
            }
            if (isClass()) {
                fieldGroupObjects['custpage_2663_classification'] = psg_ep.CommonFormFields['custpage_2663_classification'];
            }
            if (isLocation()) {
                fieldGroupObjects['custpage_2663_location'] = psg_ep.CommonFormFields['custpage_2663_location'];
            }
        } else if (fieldGroup == 'custpage_2663_grp_transfilters') {
            if (psg_ep.isGrouponAccount(nlapiGetContext().getCompany())) {
                for (var i in psg_ep.GrouponTransactionFilterFields) {
                    fieldGroupObjects[i] = psg_ep.GrouponTransactionFilterFields[i];            
                }
            }
        } else if (fieldGroup == 'custpage_2663_grp_entityfilters') {
            if (psg_ep.isGrouponAccount(nlapiGetContext().getCompany())) {
                for (var i in psg_ep.GrouponEntityFilterFields) {
                    fieldGroupObjects[i] = psg_ep.GrouponEntityFilterFields[i];            
                }
            }
        } else if (fieldGroup == 'custpage_2663_custom_transfilters') {
        	
            for (var i in psg_ep.CustomTransactionFilterFields) {
            	if(Object.keys(fieldGroupObjects).indexOf(i) < 0){
            		fieldGroupObjects[i] = psg_ep.CustomTransactionFilterFields[i];
            	}
            }
        }
        return fieldGroupObjects;
    }
    
    /**
     * Initializes the request parameters based from last refresh
     * 
     * @param {nlobjRequest} request
     */
    function initializeRequestParameters(request) {
    	logger.setTitle(logTitle + ':initializeRequestParameters');
        if (request) {
            for (var i in this._uiObj.Fields) {
                if (request.getParameter(i)) {
                    this._uiObj.RequestParams[i] = request.getParameter(i);
                    // set entity filters if provided
                    if (i == 'custpage_2663_vendor' || i == 'custpage_2663_employee') {
                        this._uiObj.Entities.push(request.getParameter(i));
                    }
                }
            }
            
            // initialize currency parameters
            if (request.getParameter('custpage_2663_format_currency')) {
                this._uiObj.RequestParams['custpage_2663_format_currency'] = request.getParameter('custpage_2663_format_currency');
            }
            if (request.getParameter('custpage_2663_exchange_rates')) {
                this._uiObj.RequestParams['custpage_2663_exchange_rates'] = request.getParameter('custpage_2663_exchange_rates');
            }
            if (request.getParameter('custpage_2663_bank_currency')) {
                this._uiObj.RequestParams['custpage_2663_bank_currency'] = request.getParameter('custpage_2663_bank_currency');
            }
            
            logger.debug('request params: ' + JSON.stringify(this._uiObj.RequestParams));
            logger.debug('entity id filter: ' + JSON.stringify(this._uiObj.Entities));
            
            // initialize sublist parameters
            this._uiObj.Sublist.InitializeRequestParameters(request);
        }
    }
    
    /**
     * Prepare select fields data
     */
    function initializeFieldData() {
        // prepare data for fields that always have the same value
        this._uiObj.FieldInitializer.PrepareBankAccountField('eft');
        this._uiObj.FieldInitializer.PreparePostingPeriodField();
        this._uiObj.FieldInitializer.PrepareAggregationField();
        this._uiObj.FieldInitializer.PrepareAggregateMethodField();
        this._uiObj.FieldInitializer.PrepareTransactionTypeField('eft');
        
        // prepare data for fields based on bank account and subsidiary
        if (this._uiObj.RequestParams['custpage_2663_bank_account']) {
            this._uiObj.FieldInitializer.PrepareBankAccountRelatedInfo();
            this._uiObj.FieldInitializer.PrepareExchangeRateField();
            var format = this._uiObj.RequestParams['custpage_2663_format_display'];
            this._uiObj.FieldInitializer.PrepareAccountField(format);
            this._uiObj.FieldInitializer.PrepareDepartmentField('custpage_2663_department');
            this._uiObj.FieldInitializer.PrepareClassField('custpage_2663_classification');
            this._uiObj.FieldInitializer.PrepareLocationField('custpage_2663_location');
        }
        
        // set the field values
        this._uiObj._setFieldValuesFromParams();
        
    }
    
    function checkApprovalRouting() {
    	var bankAccount = this._uiObj.RequestParams['custpage_2663_bank_account'];
    	if (bankAccount && this._uiObj.RequestParams['custpage_2663_approval_routing'] == 'T') {
    		var approvalRoutingSearch = new _2663.Search('customrecord_2663_approval_routing');
    		approvalRoutingSearch.addFilter('custrecord_2663_ar_bank_acct', null, 'is', bankAccount);
    		if ((approvalRoutingSearch.getResults()).length < 1) {
    			throw nlapiCreateError('EP_INVALID_APPROVAL_ROUTING', 'You must complete the approval routing setup process for this bank record. To do so, follow the instructions at Payments > Setup > Bank Details.', true);
    		}
    	}
    }
    
    /**
     * Loads the sublist based on request parameters
     */
    function loadSublist() {
    	logger.setTitle(logTitle + ':loadSublist');
        if (this._uiObj.RequestParams['custpage_2663_bank_account'] && this._uiObj.RequestParams['custpage_2663_ap_account']) {
            // initialize entity builder
            logger.debug('initialize entity data...');
            var entityFieldBuilder = new psg_ep.EntityFieldBuilder(this._uiObj, this._uiObj.StartTime);
            entityFieldBuilder.InitializeEntityData();
            
            //////////////////////////////////////////////
            // prepare data
            // get entities
            logger.debug('get entities ...');
            var entities = entityFieldBuilder.BuildEntities();
            logger.debug('entities: ' + entities.length);
            //Check if governance or time limit reached during entities search
            var entitiesSearchTime = new Date() - this._uiObj.StartTime;
            var govLimitReached = governanceReached();
            var entitiesSearchError = false;
            logger.debug('governance reached: ' + govLimitReached + '<br>' + 'entities search time: ' + entitiesSearchTime);
            if (entitiesSearchTime > this._uiObj.FieldInitializer._dataProvider.MaxTime) {
            	logger.error('Time limit exceeded during entities search.');
            	entitiesSearchError = true;
            }
            if (govLimitReached) {
            	logger.error('Governance limit reached during entities search.');
            	entitiesSearchError = true;
            }
            
            // add to filters if entities were not in request
            var entitiesForSearch = [];
            var entitiesLength = this._uiObj.Entities.length;
            if (this._uiObj.Entities.length > 0) {
                for (var i = 0; i < this._uiObj.Entities.length; i++) {
                    if (entities.indexOf(this._uiObj.Entities[i]) > -1) {
                        entitiesForSearch.push(this._uiObj.Entities[i]);
                    } else {
                        logger.debug('selected entity filter not in entities with balance: ' + this._uiObj.Entities[i]);
                    }
                }
            }
            
            // perform transaction search
            var transactionListData = new psg_ep.TransactionData_EFT(this._uiObj.RequestParams, entitiesForSearch, this._uiObj.StartTime);
            var sublistData = transactionListData.GetTransactions();
            logger.debug('data with balance error flag: ' + sublistData.errorFlag);
            
            //////////////////////////////////////////////
            // display data
            // initialize entity fields
            entityFieldBuilder.InitializeEntityFields();
            
            //set entities search error flag
            sublistData.entitiesSearchError = entitiesSearchError;
            
            // sort data
            sublistData.transactionList.sort(sortTransactions);
            
            // set max selected
            this._uiObj.Sublist._sublist._maxSelect = this._uiObj.RequestParams['custpage_2663_max_lines_sel'] || 5000;
            
            // set mark all flag if bank mark transaction flag is true
            var markAllFlag = this._uiObj.Sublist._sublist._requestParams[this._uiObj.Sublist._sublist._sublistId + '_mark_all'] || 'undefined';
            if (this._uiObj.RequestParams['custpage_2663_trans_marked'] == 'T' && markAllFlag == 'undefined') {
                this._uiObj.Sublist._sublist._requestParams[this._uiObj.Sublist._sublist._sublistId + '_mark_all'] = 'T';
            }
            
            // set sublist data
            this._uiObj._setSublistData(sublistData, true);
            
            if (sublistData.transactionList.length > 0) {
                logger.debug('Building exchange rate sublist...');
                var formatCurrencies = this._uiObj.RequestParams['custpage_2663_format_currency'];
                var exchangeRates = this._uiObj.RequestParams['custpage_2663_exchange_rates'];
                var bankCurrency = this._uiObj.RequestParams['custpage_2663_bank_currency'];
                var baseCurrency = this._uiObj.RequestParams['custpage_2663_base_currency'];
                buildExchangeRateSublist(this._uiObj.Form, formatCurrencies, exchangeRates, bankCurrency, baseCurrency);
            }
        }
    }
    // Framework Functions - End
    ////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Function to sort transactions
     * 
     * @param {nlobjSearchResult} a
     * @param {nlobjSearchResult} b
     * @returns {Number}
     */
    function sortTransactions(a, b) {
        var aString = new String(a.custpage_entity);
        var bString = new String(b.custpage_entity);
        aString = aString.toLowerCase();
        bString = bString.toLowerCase();
        if (aString < bString) {
            return -1;
        } else if (aString > bString) {
            return 1;
        } else {
            if (a.custpage_type < b.custpage_type) {
                return -1;
            } else if (a.custpage_type > b.custpage_type) {
                return 1;
            } else {
                var aNum = !isNaN(a.custpage_number) ? parseInt(a.custpage_number, 10) : '';
                var bNum = !isNaN(b.custpage_number) ? parseInt(b.custpage_number, 10) : '';
                if (aNum && bNum) {
                    if (aNum < bNum) {
                        return -1;
                    } else if (aNum > bNum) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            }
        }
    }

    /**
     * Build the exchange rate sublist when the format currencies and exchange rates fields are available
     * 
     * @param {nlobjForm} form
     * @param {String} formatCurrencies
     * @param {String} exchangeRates
     * @param {String} bankCurrency
     * @param {String} baseCurrency
     */
    function buildExchangeRateSublist(form, formatCurrencies, exchangeRates, bankCurrency, baseCurrency) {
        if (form && formatCurrencies && exchangeRates && bankCurrency) {
            // set the exchange rate sublist class
            var exchangeListSublist = new psg_ep.Sublist_ExchangeRate(form);
            exchangeListSublist.BuildUI();
            exchangeListSublist.SetSublistData(formatCurrencies, exchangeRates, bankCurrency, baseCurrency);
            exchangeListSublist.AddButton('custpage_recalc', 'Refresh Total Amount', 'ep_RecalcLines()');
        }
    }
    
    this.BuildUI = buildUI;
    this.InitializeRequestParameters = initializeRequestParameters;
    this.InitializeFieldData = initializeFieldData;
    this.LoadSublist = loadSublist;
    this.CheckApprovalRouting = checkApprovalRouting;
};

/**
 * Class for EFT sublist
 *
 * @param {nlobjForm} form
 * @param {String} name
 * @param {String} type
 * @param {String} label
 * @param {String} tab
 * @returns {psg_ep.Sublist_EFT}
 */
psg_ep.Sublist_EFT = function(form, name, type, label, tab) {
	var objEPPreferences = _2663.getEPPreferences() || {};
    this._sublistInterface = new psg_ep.SublistInterface();
    this._sublist = new psg_ep.Sublist(form, name, type, label, tab, this._sublistInterface);
    
    /**
     * Builds the sublist UI
     */
    function buildUI() {
        this._sublistInterface.GetColumns = getColumns_EFT;
        this._sublist._buildUI();
    }
    
    /**
     * Returns the columns for the sublist
     */
    function getColumns_EFT() {
        var sublistColumns = {};
        sublistColumns['custpage_internalid'] = new psg_ep.SublistColumn('custpage_internalid', 'integer', 'ID', 'hidden');
        sublistColumns['custpage_line'] = new psg_ep.SublistColumn('custpage_line', 'integer', 'Line Number', 'hidden');
        sublistColumns['custpage_mark_key'] = new psg_ep.SublistColumn('custpage_mark_key', 'text', 'Mark ID', 'hidden', null, null, true);
        sublistColumns['custpage_pay'] = new psg_ep.SublistColumn('custpage_pay', 'checkbox', 'Pay', null, 'F', true);
        sublistColumns['custpage_entity'] = new psg_ep.SublistColumn('custpage_entity', 'text', 'Payee');
        if (objEPPreferences.include_name) {
        	sublistColumns['custpage_name'] = new psg_ep.SublistColumn('custpage_name', 'text', 'Name');
        }
        sublistColumns['custpage_type'] = new psg_ep.SublistColumn('custpage_type', 'text', 'Type');
        sublistColumns['custpage_tranid'] = new psg_ep.SublistColumn('custpage_tranid', 'text', 'Reference Number');
        sublistColumns['custpage_trandate'] = new psg_ep.SublistColumn('custpage_trandate', 'date', 'Date');
        sublistColumns['custpage_duedate'] = new psg_ep.SublistColumn('custpage_duedate', 'date', 'Due Date');
        if (isMultiCurrency()) {
            sublistColumns['custpage_currency'] = new psg_ep.SublistColumn('custpage_currency', 'text', 'Currency');
            sublistColumns['custpage_currencyhdn'] = new psg_ep.SublistColumn('custpage_currencyhdn', 'text', 'Currency', 'hidden');
        }
        sublistColumns['custpage_postingperiod'] = new psg_ep.SublistColumn('custpage_postingperiod', 'text', 'Period');
        sublistColumns['custpage_amount'] = new psg_ep.SublistColumn('custpage_amount', 'currency', 'Amount');
        sublistColumns['custpage_amountremaining'] = new psg_ep.SublistColumn('custpage_amountremaining', 'currency', 'Amount Remaining');
        sublistColumns['custpage_amountremaininghdn'] = new psg_ep.SublistColumn('custpage_amountremaininghdn', 'currency', 'Amount Remaining', 'hidden', nlapiFormatCurrency(0));
        sublistColumns['custpage_linemarkdata'] = new psg_ep.SublistColumn('custpage_linemarkdata', 'text', 'Mark Column', 'hidden', null, null, null, true);
        sublistColumns['custpage_payment'] = new psg_ep.SublistColumn('custpage_payment', 'currency', 'Payment Amount', 'entry', nlapiFormatCurrency(0), null, null, null, true);
        sublistColumns['custpage_entityid'] = new psg_ep.SublistColumn('custpage_entityid', 'text', 'Entity Internal Id', 'hidden');
        sublistColumns['custpage_trantype'] = new psg_ep.SublistColumn('custpage_trantype', 'text', 'Transaction Type', 'hidden');

        if(psg_ep.CustomTransactionColumns){
            for (var i in psg_ep.CustomTransactionColumns) {
            	sublistColumns[i] = new psg_ep.SublistColumn(i, psg_ep.CustomTransactionColumns[i].type, psg_ep.CustomTransactionColumns[i].name, psg_ep.CustomTransactionColumns[i].displayType, psg_ep.CustomTransactionColumns[i].defaultValue);
            }
        }
        
        return sublistColumns;
    }
    
    /**
     * Initializes the request parameters
     */
    function initializeRequestParameters(request) {
        this._sublist._initializeRequestParameters(request);
    }
  
    /**
     * Sets the sublist data
     */
    function setSublistData(data) {
        this._sublist._setSublistData(data);
    }
    
    /**
     * Adds the button to the sublist tab
     */
    function addButton(buttonId, label, func) {
        this._sublist._addButton(buttonId, label, func);
    }
    
    /**
     * Sets the error message
     */
    function setErrorMessage(source, errorMessage) {
        this._sublist._setErrorMessage(source, errorMessage);
    }
    
    this.BuildUI = buildUI;
    this.InitializeRequestParameters = initializeRequestParameters;
    this.SetSublistData = setSublistData;
    this.AddButton = addButton;
    this.SetErrorMessage = setErrorMessage;
};

/**
 * Transaction search object for EFT
 * 
 * @param {Array} requestParams
 * @param {Array} entities
 * @param {Date} startTime
 * @returns {psg_ep.TransactionData_EFT}
 */
psg_ep.TransactionData_EFT = function(requestParams, entities, startTime) {
	var objEPPreferences = _2663.getEPPreferences() || {}; 
	var logTitle = '[ep] TransactionData_EFT';
	var logger = new _2663.Logger(logTitle);
    if (!requestParams) {
        throw nlapiCreateError('EP_REQUEST_PARAMS_NULL', 'Cannot create data object without request params', true);
    }
    if (!entities) {
        throw nlapiCreateError('EP_ENTITIES_NULL', 'Cannot create data object without entities', true);
    }
    if (!startTime) {
        throw nlapiCreateError('EP_START_TIME_NULL', 'Cannot create data object without start time', true);
    }
        
    this._tranDataInterface = new psg_ep.TransactionDataInterface(); 
    this._tranDataInterface.StartTime = startTime;
    this._tranDataInterface.RequestParams = requestParams;
    this._tranDataInterface.Entities = entities;
    this._tranDataInterface.SavedSearch = 'customsearch_ep_ap_trans_search';
    this._tranDataObj = new psg_ep.TransactionData(this._tranDataInterface);
    
    /**
     * Returns transactions for sublist
     * 
     * @returns {Array}
     */
    function getTransactions() {
        this._tranDataInterface.BuildFilters = buildFilters_EFT;
        this._tranDataInterface.BuildColumns = buildColumns_EFT;
        this._tranDataInterface.ConvertTransactionSearchResultToObjectArray = convertTransactionSearchResultToObjectArray_EFT;
        return this._tranDataObj._getTransactions();
    }
    
    ////////////////////////////////////////////////////////////////////////////////////
    // Data Framework Functions - Start
    
    /**
     * Get non entity filters
     *
     * @param requestParams
     * @returns {Array}
     */
    function buildFilters_EFT(requestParams) {
    	logger.setTitle(logTitle + ':buildFilters_EFT');
        // get request parameter filters
    	var format = requestParams['custpage_2663_format_display'];
    	var subsidiary = isOneWorld() ? requestParams['custpage_2663_subsidiary'] : '';
    	var bankAccount = requestParams['custpage_2663_bank_account'];
        var apAccount = requestParams['custpage_2663_ap_account'] || '@NONE@';
        var dateFrom = requestParams['custpage_2663_date_from'];
        var dateTo = requestParams['custpage_2663_date_to'];
        var transType = requestParams['custpage_2663_transtype'];
        var currencies = requestParams['custpage_2663_format_currency'];
        
        logger.debug([
            'file format: ' + format, 
            'subsidiary: ' + subsidiary, 
            'bank account: ' + bankAccount, 
            'ap account: ' + apAccount, 
            'date from: ' + dateFrom, 
            'date to: ' + dateTo, 
            'transaction type: ' + transType
        ].join('<br>'));

        var filters = [];
        
        // payment file format
        filters.push(new nlobjSearchFilter('custcol_2663_eft_file_format', null, 'is', format));
        
        // subsidiary
        if (subsidiary) {
        	filters.push(new nlobjSearchFilter('subsidiary', null, 'is', subsidiary));
        }
        
        // ap account  
        filters.push(new nlobjSearchFilter('account', null, 'is', apAccount));
        
        //set entity filters
        if (entities && entities.length > 0) {
        	logger.debug('Filter entities: ' + entities.length);
        	filters.push(new nlobjSearchFilter('entity', null, 'anyof', entities));
        }
        
        // set payables to be included:  Expense Report, Journal
        var payables = ['ExpRept', 'Journal'];

        // set payables' status filter: Expense Report - Approved, Expense Report - Approved (Overridden) by Accounting, Journal - Approved
        var status = ['ExpRept:F', 'ExpRept:G', 'Journal:B'];

        var billFilterType = ['VendBill', 'ExpRept'];
        
        if (isCommissionEnabled() || isEmployeeCommissionEnabled()) {
            payables.push('Commissn');
            status.push('Commissn:A');
            billFilterType.push('Commissn');
        }
        
        // overwrite the status when the transaction type filter is set
        if (transType) {
        	// set specific transaction selected            
            filters.push(new nlobjSearchFilter('type', null, 'anyof', transType));
            
            if (transType == 'VendBill') {
                status = 'VendBill:A';
                filters.push(new nlobjSearchFilter('paymenthold', null, 'is', 'F'));
                
            } else if (transType == 'ExpRept') {
                status = ['ExpRept:F', 'ExpRept:G'];
            } else if (transType == 'Journal') {
                status = ['Journal:B'];
            } else if (transType == 'Commissn') {
            	status = ['Commissn:A'];
            }
            
            filters.push(new nlobjSearchFilter('status', null, 'anyof', status));
        }
        else{
            // set record type and status filters
            filter = new nlobjSearchFilter('type', null, 'anyof', payables);
            filter.setLeftParens(2);
            filters.push(filter);
            
            filter = new nlobjSearchFilter('status', null, 'anyof', status);
            filter.setRightParens(1);
            filter.setOr(true);
            filters.push(filter);
            
            filter = new nlobjSearchFilter('type', null, 'is', 'VendBill');			
            filter.setLeftParens(1);
            filters.push(filter);

            filter = new nlobjSearchFilter('status', null, 'is','VendBill:A');
            filters.push(filter);
             
            filter = new nlobjSearchFilter('paymenthold', null, 'is', 'F');
            filter.setRightParens(2);
            filters.push(filter);
            
        }
        
        // currency (based on bank account or on format currencies)
        if (isMultiCurrency() && bankAccount) {
            // do not initialize currency
            var currency;

            // get the bank currency
            var bankCurrency = nlapiLookupField('customrecord_2663_bank_details', bankAccount, 'custrecord_2663_currency');
            
            // if multicurrency format
            if (currencies) {
                // get currency info
                var currencyMap = JSON.parse(currencies);

                // only get all currencies for filter if bank and base currency are equal
                var baseCurrency = requestParams['custpage_2663_base_currency'];
                if (bankCurrency == baseCurrency) {
                    currency = [];
                    for (var i in currencyMap) {
                        currency.push(i);
                    }
                // if not the same, check if in map and set the bank currency as filter
                } else if (currencyMap.hasOwnProperty(bankCurrency)) {
                    currency = bankCurrency;
                }
            // single currency format
            } else {
                currency = bankCurrency;
            }
            
            // set filter to "@NONE@" if it is undefined to avoid errors
            currency = currency || '@NONE@';
            filters.push(new nlobjSearchFilter('currency', null, 'anyof', currency));
        }
        
        // add from due date filter, skip for journal entries
        if (dateFrom) {
            var journalFilter = new nlobjSearchFilter('type', null, 'anyof', 'Journal');
            journalFilter.setLeftParens(2);
            journalFilter.setRightParens(1);
            journalFilter.setOr(true);
            var billFilter = new nlobjSearchFilter('type', null, 'anyof', billFilterType);
            billFilter.setLeftParens(1);
            var dueDateFilter = new nlobjSearchFilter('duedate', null, 'onorafter', dateFrom);
            dueDateFilter.setRightParens(2);
            filters.push(journalFilter);
            filters.push(billFilter);
            filters.push(dueDateFilter);
        }
        
        // add to due date filter, skip for journal entries
        if (dateTo) {
            var journalFilter = new nlobjSearchFilter('type', null, 'anyof', 'Journal');
            journalFilter.setLeftParens(2);
            journalFilter.setRightParens(1);
            journalFilter.setOr(true);
            var billFilter = new nlobjSearchFilter('type', null, 'anyof', billFilterType);
            billFilter.setLeftParens(1);
            var dueDateFilter = new nlobjSearchFilter('duedate', null, 'onorbefore', dateTo);
            dueDateFilter.setRightParens(2);
            filters.push(journalFilter);
            filters.push(billFilter);
            filters.push(dueDateFilter);
        }
        
        // get KU filters
        filters = filters.concat(buildKUFilters(requestParams));
        
        // get groupon filters
        filters = filters.concat(buildGrouponFilters(requestParams));
        
        // get custom filters
        filters = filters.concat(buildCustomFilters(requestParams));
        
        
        // Only add filters when Approval Routing is enabled
        if (_2663.isApprovalRoutingEnabled()) {
        	//add filters for handling batches
            var batchFilter = new nlobjSearchFilter('custrecord_2663_eft_file_id', 'custrecord_2663_parent_bill', 'anyof', '@NONE@');
            batchFilter.setLeftParens(1);
            batchFilter.setOr(true);
            var processingStatusFilter = new nlobjSearchFilter('custrecord_2663_eft_payment_processed', 'custrecord_2663_parent_bill', 'is', PAYPROCESSED);
            processingStatusFilter.setOr(true);
            var batchJournalFilter = new nlobjSearchFilter('type', null, 'is', 'Journal');
            batchJournalFilter.setRightParens(1);
            
            filters.push(batchFilter);
            filters.push(processingStatusFilter);
            filters.push(batchJournalFilter);	
        }
        
        return filters;
    }

    /**
     * Builds columns for search
     * 
     * @returns {Array}
     */
    function buildColumns_EFT() {
        var columns = [];
        columns.push(new nlobjSearchColumn('name'));
        columns.push(new nlobjSearchColumn('type'));
        columns.push(new nlobjSearchColumn('number'));
        columns.push(new nlobjSearchColumn('trandate'));
        columns.push(new nlobjSearchColumn('duedate'));
        columns.push(new nlobjSearchColumn('entity'));
        
        // Display currency only for multi-currency clients. 
        if (isMultiCurrency()) {
            columns.push(new nlobjSearchColumn('currency'));
        }
        columns.push(new nlobjSearchColumn('postingperiod'));
        if (isMultiCurrency()) {
            columns.push(new nlobjSearchColumn('fxamount'));
            columns.push(new nlobjSearchColumn('fxamountremaining'));
        } else {
            columns.push(new nlobjSearchColumn('amount'));
            columns.push(new nlobjSearchColumn('amountremaining'));
        }
        
        if (objEPPreferences.include_name) {
        	columns.push(new nlobjSearchColumn('custcol_2663_isperson'));	
        	columns.push(new nlobjSearchColumn('custcol_2663_companyname'));
        	columns.push(new nlobjSearchColumn('custcol_2663_firstname'));
        	columns.push(new nlobjSearchColumn('custcol_2663_lastname'));
        }
        
        if(psg_ep.CustomTransactionColumns){
            for (var i in psg_ep.CustomTransactionColumns) {
            	columns.push(new nlobjSearchColumn(psg_ep.CustomTransactionColumns[i].reference, psg_ep.CustomTransactionColumns[i].join));
            }
        }

        
        return columns;
    }
    
    /**
     * Convert the search result array to object array
     * 
     * @param {Array} entityBatchTransactions
     * @param {Object} requestParams
     * @returns {Array}
     */
    function convertTransactionSearchResultToObjectArray_EFT(entityBatchTransactions, requestParams) {
    	logger.setTitle(logTitle + ':convertTransactionSearchResultToObjectArray_EFT');
        var transactions = [];
        
        var amtColumn = 'amount';
        var amtRemainingColumn = 'amountremaining';
        
        var currencyFlag = false;
        if (isMultiCurrency()) {
            amtColumn = 'fxamount';
            amtRemainingColumn = 'fxamountremaining';
            currencyFlag = true;
        }

        var batchTransactionsForExclusion = buildBatchTransactionsForExclusion(requestParams);
        var uniqueKeys = [];
        logger.debug('excluded length: ' + batchTransactionsForExclusion.length);
        logger.debug('excluded entries: ' + batchTransactionsForExclusion.join());
        for (var i = 0; i < entityBatchTransactions.length; i++) {
        	var transaction = entityBatchTransactions[i];
            var sublistLineObj = {
                custpage_internalid: transaction.getValue('internalid'),
                custpage_line: transaction.getValue('line'),
                custpage_mark_key: transaction.getValue('internalid') + '-' + transaction.getValue('line'),
                custpage_entity: transaction.getText('entity'),
                custpage_type: transaction.getText('type'),
                custpage_tranid: transaction.getValue('number'),
                custpage_trandate: transaction.getValue('trandate'),
                custpage_duedate: transaction.getValue('duedate'),
                custpage_postingperiod: transaction.getText('postingperiod'),
                custpage_amount: nlapiFormatCurrency(transaction.getValue(amtColumn)),
                custpage_amountremaining: nlapiFormatCurrency(transaction.getValue(amtRemainingColumn)),
                custpage_payment: nlapiFormatCurrency(transaction.getValue(amtRemainingColumn)),
                custpage_entityid: transaction.getValue('entity'),
                custpage_trantype: transaction.getRecordType()
            };
            
            if (objEPPreferences.include_name) {
                if ((transaction.getValue('custcol_2663_isperson') || 'T') == 'T') {  // if custcol_2663_isperson == 'T' or if not defined (Employee) 
                	sublistLineObj.custpage_name = (transaction.getValue('custcol_2663_firstname') || '') + ' ' + (transaction.getValue('custcol_2663_lastname') || '');
                } else {
                	sublistLineObj.custpage_name = transaction.getValue('custcol_2663_companyname');
                }
                sublistLineObj.custpage_name.trim();
                sublistLineObj.custpage_name = sublistLineObj.custpage_name || transaction.getText('entity');
            }
            
            var lineMarkData = {amount: sublistLineObj.custpage_payment, entity: sublistLineObj.custpage_entityid, type: sublistLineObj.custpage_trantype};
            if (currencyFlag) {
                sublistLineObj.custpage_currency = transaction.getText('currency');
                sublistLineObj.custpage_currencyhdn = transaction.getValue('currency');
                lineMarkData.currency = sublistLineObj.custpage_currencyhdn;
            }
            sublistLineObj.custpage_linemarkdata = JSON.stringify(lineMarkData);

            if(psg_ep.CustomTransactionColumns){
                for (var j in psg_ep.CustomTransactionColumns) {
                	if(psg_ep.CustomTransactionColumns[j].isText){
                		sublistLineObj[j] = transaction.getText(psg_ep.CustomTransactionColumns[j].reference, psg_ep.CustomTransactionColumns[j].join);
                	} else{
                		sublistLineObj[j] = transaction.getValue(psg_ep.CustomTransactionColumns[j].reference, psg_ep.CustomTransactionColumns[j].join);
                	}
                }
            }
            
            if (batchTransactionsForExclusion.indexOf(sublistLineObj.custpage_mark_key) < 0 && uniqueKeys.indexOf(sublistLineObj.custpage_mark_key) < 0) {
            	uniqueKeys.push(sublistLineObj.custpage_mark_key);
            	transactions.push(sublistLineObj);
            } else {
                logger.debug('excluded: ' + sublistLineObj.custpage_mark_key);
            }
        }
        
        return transactions;
    }
    
    
    // Data Framework Functions - End
    ////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Builds additional transaction filters if the account is for KU (on hold)
     * 
     * @param {} requestParams
     * @returns {Array}
     */
    function buildKUFilters(requestParams) {
    	logger.setTitle(logTitle + ':buildKUFilters');
        var kuFilters = [];
        if (requestParams && psg_ep.HoldPaymentAcctIds.indexOf(nlapiGetContext().getCompany()) != -1) {
            var onHold = requestParams['custpage_2663_onhold'] || 'F';
            logger.debug('on hold: ' + onHold);
            // add on hold only when it is unchecked
            if (onHold == 'F') {
                try {
                    kuFilters.push(new nlobjSearchFilter('custbody_holdpayment', null, 'is', onHold));
                } catch(ex) {
                	logger.error('Hold Payment field (custbody_holdpayment) does not exist', ex);
                }
            }
        }
        
        return kuFilters;
    }
    
    /**
     * Builds additional transaction filters if the account is for Groupon
     * 
     * @param {} requestParams
     * @returns {Array}
     */
    function buildGrouponFilters(requestParams) {
    	logger.setTitle(logTitle + ':buildGrouponFilters');
        var grouponFilters = [];
        if (requestParams && psg_ep.isGrouponAccount(nlapiGetContext().getCompany())) {
            var grouponTransFilters = {
                'tranid': {
                    type: 'text',
                    join: null,
                    operator: 'contains'
                },
                'custbody_payment_type': {
                    type: 'multiselect',
                    join: null,
                    operator: 'anyof'
                },
                'custbody_deal_id': {
                    type: 'text',
                    join: null,
                    operator: 'contains'
                },
                'custbody_document_number': {
                    type: 'text',
                    join: null,
                    operator: 'contains'
                },
                'custbody_hold_payment_bill': {
                    type: 'checkbox',
                    join: null,
                    operator: 'is'
                },
                'custcol_hold_payment_vendor': {
                    type: 'checkbox',
                    join: null,
                    operator: 'is',
                    value: 'F'
                },
                'custcol_v_salesforce_account_id': {
                    type: 'text',
                    join: null,
                    operator: 'contains'
                }
            };
            
            for (var i in grouponTransFilters) {
                var fieldName = 'custpage_2663_' + i;
                var requestParamVal = grouponTransFilters[i].value || requestParams[fieldName];
                if (i == 'custbody_hold_payment_bill') {
                	if (!requestParamVal) {
                		requestParamVal = 'F';	
                	} else if (requestParamVal == 'T') {
                		requestParamVal = '';
                	}
                }
                if (i == 'custcol_v_salesforce_account_id') {
                	requestParamVal = requestParams['custpage_2663_custentity_v_salesforce_account_id'];
                }
                if (requestParamVal) {
                    logger.debug(fieldName + ': ' + requestParamVal);
                    try {
                    	requestParamVal = i == 'custbody_payment_type' ? (requestParamVal.match(/\d{1,}/g) || []) : requestParamVal;
                        grouponFilters.push(new nlobjSearchFilter(i, grouponTransFilters[i].join, grouponTransFilters[i].operator, requestParamVal));
                    } catch(ex) {
                        logger.error(i + ' field does not exist', ex);
                    }
                }
            }
        }
        
        return grouponFilters;
    }

    /**
     * Builds additional transaction custom filters 
     * 
     * @param {} requestParams
     * @returns {Array}
     */
    function buildCustomFilters(requestParams) {
    	logger.setTitle(logTitle + ':buildCustomFilters');
        var customFilters = [];
        if (requestParams && psg_ep.CustomTransactionFilters) {
        	
            for (var i in psg_ep.CustomTransactionFilters) {
                var fieldName = 'custpage_2663_' + i;
                var requestParamVal = psg_ep.CustomTransactionFilters[i].value || requestParams[fieldName];
                
                if (requestParamVal) {
                    logger.debug(fieldName + ': ' + requestParamVal);
                    try {
                    	customFilters.push(new nlobjSearchFilter(i, psg_ep.CustomTransactionFilters[i].join, psg_ep.CustomTransactionFilters[i].operator, requestParamVal));
                    } catch(ex) {
                        logger.error(i + ' field does not exist', ex);
                    }
                }
            }
        }
        
        return customFilters;
    }
    
    /**
     * Build filters for transactions to be excluded
     * 
     * @param requestParams
     * @returns {Array}
     */
    function buildBatchTransactionsForExclusion(requestParams) {
        var bankAccount = requestParams['custpage_2663_bank_account'];
        var apAccount = requestParams['custpage_2663_ap_account'] || '@NONE@';
        
        var batchTransactionsForExclusion = [];
        if (bankAccount && apAccount) {
            var closedBatches = (new psg_ep.FormFieldDataProvider()).GetBatches(bankAccount, apAccount, [BATCH_PENDINGAPPROVAL, BATCH_OPEN, BATCH_UPDATING]);
            if (closedBatches && closedBatches.length > 0) {
                for (var i = 0; i < closedBatches.length; i++) {
                    var tranArray = JSON.parse(closedBatches[i].getValue('custrecord_2663_payments_for_process_id'));
                    batchTransactionsForExclusion = batchTransactionsForExclusion.concat(tranArray);
                }
            }
        }
        return batchTransactionsForExclusion;
    }
    
    this.GetTransactions = getTransactions;
};

/**
 * Builds entity fields and its contents
 * 
 * @param {psg_ep.psg_ep.PaymentSelectionForm} uiObj
 * @param {Object} startTime
 */
psg_ep.EntityFieldBuilder = function(uiObj, startTime) {
    if (!uiObj) {
        throw nlapiCreateError('EP_UI_OBJ_NULL', 'Cannot create EntityFieldBuilder object without UI object', true);
    }
    
    this._uiObj = uiObj;
    this._startTime = startTime || new Date();
    
    /**
     * Initialize data for search objects
     */
    function initializeEntityData() {
        this._uiObj.DataFromSearch.Vendors = [];
        this._uiObj.DataFromSearch.Employees = [];
        this._uiObj.DataFromSearch.Partners = [];
    }
    
    /**
     * Initialize fields with select options
     */
    function initializeEntityFields() {
        function sortFunc(a, b) {
            var aValue = new String(a.text);
            var bValue = new String(b.text);
            
            if (aValue.toLowerCase() < bValue.toLowerCase()) {
                return -1;
            } else if (aValue.toLowerCase() > bValue.toLowerCase()) {
                return 1;
            } else {
                return 0;
            }
        }
        // vendor
        if (this._uiObj.DataFromSearch.Vendors && this._uiObj.DataFromSearch.Vendors.length > 0) {
            // sort values
            this._uiObj.DataFromSearch.Vendors.sort(sortFunc);
            
            // add to dropdown
            var vendorField = this._uiObj.Fields['custpage_2663_vendor'];
            vendorField.addSelectOption('', '');
            for (var j = 0; j < this._uiObj.DataFromSearch.Vendors.length; j++) {
                vendorField.addSelectOption(this._uiObj.DataFromSearch.Vendors[j].id, this._uiObj.DataFromSearch.Vendors[j].text);
            }
        }
        
        // employee
        if (this._uiObj.DataFromSearch.Employees && this._uiObj.DataFromSearch.Employees.length > 0) {
            // sort values
            this._uiObj.DataFromSearch.Employees.sort(sortFunc);
            
            // add to dropdown
            var employeeField = this._uiObj.Fields['custpage_2663_employee'];
            employeeField.addSelectOption('', '');
            for (var j = 0; j < this._uiObj.DataFromSearch.Employees.length; j++) {
                employeeField.addSelectOption(this._uiObj.DataFromSearch.Employees[j].id, this._uiObj.DataFromSearch.Employees[j].text);
            }
        }
        
        // partners
        if (this._uiObj.DataFromSearch.Partners && this._uiObj.DataFromSearch.Partners.length > 0) {
            // sort values
            this._uiObj.DataFromSearch.Partners.sort(sortFunc);
            
            // add to dropdown
            var partnerField = this._uiObj.Fields['custpage_2663_partner'];
            partnerField.addSelectOption('', '');
            for (var j = 0; j < this._uiObj.DataFromSearch.Partners.length; j++) {
            	partnerField.addSelectOption(this._uiObj.DataFromSearch.Partners[j].id, this._uiObj.DataFromSearch.Partners[j].text);
            }
        }
    }
    
    /**
     * Builds the entities with balance and returns their ids
     * 
     * @returns {Array}
     */
    function buildEntities() {
        var entities = [];
        var isPartnerCommission = isCommissionEnabled();
        var searchResults = this._uiObj.FieldInitializer._dataProvider.GetFormatEntities('eft', this._uiObj.RequestParams, this._startTime);
        for (var i = 0, ii = searchResults.length; i < ii; i++) {
        	var entity = searchResults[i];
        	var recType = entity.getRecordType();
        	var recId = entity.getId();
        	if (recType == 'vendor') {
        		this._uiObj.DataFromSearch.Vendors.push({
                    id: recId, 
                    text: entity.getValue('entityid')
                });
        	} else if (recType == 'employee') {
        		this._uiObj.DataFromSearch.Employees.push({
                    id: recId, 
                    text: entity.getValue('entityid')
                });
        	} else if (isPartnerCommission && recType == 'partner') {
        		this._uiObj.DataFromSearch.Partners.push({
                    id: recId, 
                    text: entity.getValue('entityid')
                });
        	} else {
        		nlapiLogExecution('error', 'buildEntities', ['Entity:', recId, 'is of type', recType].join(' '));
        		continue;
        	}
            entities.push(searchResults[i].getId());	
        }
        return entities;
    }

    /**
     * Builds the entities without balance and returns their ids
     * 
     * @returns {Array}
     */
    function buildEntitiesWithoutBalance() {
        var entitiesWithoutBalance = [];
        var account = this._uiObj.RequestParams['custpage_2663_ap_account'];
        var subsidiary = this._uiObj.RequestParams['custpage_2663_subsidiary'];
        var format = this._uiObj.RequestParams['custpage_2663_format_display'];
        var eftEntitiesNoBalance = new psg_ep.EFTEntitiesNoBalance();
        
        try {
        	var presearchResults = eftEntitiesNoBalance.GetEntitiesWithoutBalance(account, subsidiary, this._startTime);
        	if (presearchResults && presearchResults.length > 0) {
            	var searchResults = eftEntitiesNoBalance.GetEntitiesForFormat_AP(format, presearchResults, 500, this._startTime);
                for (var i in searchResults) {
                    this._uiObj.DataFromSearch.Vendors.push({
                        id: i, 
                        text: searchResults[i]
                    });
                    entitiesWithoutBalance.push(i);
                }	
            }
        } catch (ex) {
        	var errorMessage = 'Error occurred while getting entities without balance';
	    	if (ex instanceof nlobjError) {
	    		errorMessage += ': ' + ex.getCode() + '\n' + ex.getDetails() + '\n' + ex.getStackTrace();
	    	} else {
	    		errorMessage += ': ' + ex.toString();
	    	}
	    	nlapiLogExecution('error', '[ep] psg_ep.EntityFieldBuilder: buildEntitiesWithoutBalance', errorMessage);
        }
        return entitiesWithoutBalance;
    }
    
    this.BuildEntities = buildEntities;
    this.BuildEntitiesWithoutBalance = buildEntitiesWithoutBalance;
    this.InitializeEntityData = initializeEntityData;
    this.InitializeEntityFields = initializeEntityFields;
};

/**
 * Class that handles search for vendors with zero or negative balance
 * 
 * @returns {psg_ep.EFTEntitiesNoBalance}
 */
psg_ep.EFTEntitiesNoBalance = function() {
    this.MaxTime = 240000;
    
    /**
     * Gets entities without balance based on account and subsidiary (if OW)
     * 
     * @param account
     * @param subsidiary
     * @param startTime
     * @returns {Array}
     */
    function getEntitiesWithoutBalance(account, subsidiary, startTime) {
        var entities = [];
        if (account && (!isOneWorld() || isOneWorld() && subsidiary)) {
            nlapiLogExecution('debug', 'EFTEntitiesNoBalance:getEntitiesWithoutBalance', 'account: ' + account + ', sub: ' + subsidiary);            
            var filters = [];
            if (isOneWorld()) {
                filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));
            }
            filters.push(new nlobjSearchFilter('account', null, 'anyof', account));
            var search = nlapiLoadSearch('transaction', 'customsearch_ep_vendor_nobal_search');
            search.addFilters(filters);
            var searchResultSet = search.runSearch();
            // search by page
            var startIdx = 0;
            var endIdx = 1000;
            do {
                if (governanceReached()) {
                    throw nlapiCreateError('EP_GOVERNANCE_REACHED', 'Governance reached.', true);
                }

                if (startTime) {
                    var currentTime = (new Date()).getTime();
                    var timeElapsed = currentTime - startTime;
                    if (timeElapsed >= this.MaxTime) {
                        throw nlapiCreateError('EP_TIME_LIMIT_REACHED', 'Time limit reached.', true);
                    }
                }
                
                var searchResults = searchResultSet.getResults(startIdx, endIdx);
                if (searchResults) {
                    for (var i = 0; i < searchResults.length; i++) {
                        nlapiLogExecution('debug', 'EFTEntitiesNoBalance:getEntitiesWithoutBalance', searchResults[i].getText('name', null, 'group') + '-' + searchResults[i].getValue('internalid', null, 'count'));
                        entities.push(searchResults[i].getValue('name', null, 'group'));
                    }
                    startIdx += 1000;
                    endIdx += 1000;
                }
            }
            while(searchResults && searchResults.length > 0);
        }
        return entities;
    }

    /**
     * Gets entity file format details based on format and entities from previous search
     *  
     * @param format
     * @param entities
     * @param entityBatchSize
     * @param startTime
     * @returns {___anonymous47288_47289}
     */
    function getEntitiesForFormat_AP(format, entities, entityBatchSize, startTime) {
        entityBatchSize = entityBatchSize && entityBatchSize <= 500 ? entityBatchSize : 500;
        var entitiesForFormat = {};
        if (format && entities && entities.length > 0) {
            var vendorEntityFilters = buildEntityFilterBatch('internalid', 'custrecord_2663_parent_vendor', entities, entityBatchSize);
            
            for (var i = 0; i < vendorEntityFilters.length; i++) {
                var entityFilters = [];
                var entityColumns = [];
                
                // check format
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_entity_file_format', null, 'anyof', format));
                
                // don't get customer entity details
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_customer', null, 'anyof', '@NONE@'));
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_employee', null, 'anyof', '@NONE@'));
                entityFilters.push(new nlobjSearchFilter('custrecord_2663_parent_cust_ref', null, 'anyof', '@NONE@'));
                
                // check if eft = true for vendor
                var vendorEftFilter = new nlobjSearchFilter('custentity_2663_payment_method', 'custrecord_2663_parent_vendor', 'is', 'T');
                
                // check if vendor ap balance is less than 0
                var vendorAPBalanceFilter = new nlobjSearchFilter('balance', 'custrecord_2663_parent_vendor', 'lessthanorequalto', 0);
                
                // check if vendor is inactive
                var vendorInactiveFilter = new nlobjSearchFilter('isinactive', 'custrecord_2663_parent_vendor', 'is', 'F');
                
                // add filter expression
                entityFilters.push(vendorEftFilter);
                entityFilters.push(vendorAPBalanceFilter);
                entityFilters.push(vendorInactiveFilter);
                entityFilters = entityFilters.concat(vendorEntityFilters[i]);
                
                // get the entity columns
                entityColumns.push(new nlobjSearchColumn('custrecord_2663_parent_vendor'));
                
                // search by page
                var search = nlapiLoadSearch('customrecord_2663_entity_bank_details', 'customsearch_ep_entity_search');
                search.addFilters(entityFilters);
                search.addColumns(entityColumns);
                var searchResultSet = search.runSearch();
                var startIdx = 0;
                var endIdx = 1000;
                do {
                    if (governanceReached()) {
                        throw nlapiCreateError('EP_GOVERNANCE_REACHED', 'Governance reached.', true);
                    }

                    if (startTime) {
                        var currentTime = (new Date()).getTime();
                        var timeElapsed = currentTime - startTime;
                        if (timeElapsed >= this.MaxTime) {
                            throw nlapiCreateError('EP_TIME_LIMIT_REACHED', 'Time limit reached.', true);
                        }
                    }
                    
                    var searchResults = searchResultSet.getResults(startIdx, endIdx);
                    if (searchResults) {
                        for (var i = 0; i < searchResults.length; i++) {
                            entitiesForFormat[searchResults[i].getValue('custrecord_2663_parent_vendor')] = searchResults[i].getText('custrecord_2663_parent_vendor');
                            nlapiLogExecution('debug', 'EFTEntitiesNoBalance:getEntitiesForFormat_AP', searchResults[i].getText('custrecord_2663_parent_vendor'));
                        }
                        startIdx += 1000;
                        endIdx += 1000;
                    }
                }
                while(searchResults && searchResults.length > 0);
            }
        }
        return entitiesForFormat;
    }
    
    /**
     * Builds the entity filter by batch
     * @param fieldName
     * @param join
     * @param entities
     * @param entityBatchSize
     * @returns {Array}
     */
    function buildEntityFilterBatch(fieldName, join, entities, entityBatchSize) {
        entityBatchSize = entityBatchSize && entityBatchSize <= 500 ? entityBatchSize : 500;
        join = join || null;
        var entityBatchInSearch = 5;
        var filterBatches = [];
        
        if (fieldName && entities) {
            var numBatches = Math.ceil(entities.length / entityBatchSize); 
            nlapiLogExecution('debug', '[ep] EFTEntitiesNoBalance:buildEntityFilterBatch', 'number of batches: ' + numBatches);
            var entityFilterGroups;
            for (var i = 0; i < numBatches; i++) {
                // create and add filter for entity batch
                var startIdx = i * entityBatchSize;
                var endIdx = (i + 1) * entityBatchSize < entities.length ? (i + 1) * entityBatchSize : entities.length ;
                nlapiLogExecution('debug', '[ep] EFTEntitiesNoBalance:buildEntityFilterBatch', 'batch: ' + i + ', start: ' + startIdx + ', end: ' + endIdx);
                var entityBatch = entities.slice(startIdx, endIdx);
                var newEntityFilter = new nlobjSearchFilter(fieldName, join, 'anyof', entityBatch);
                // create filter for each batch of entities (500 in anyof * 5 per batch)
                if (numBatches > 1) {
                    if (i == 0 || i % entityBatchInSearch == 0) {
                        entityFilterGroups = [];
                        newEntityFilter.setLeftParens(1);
                    }
                    if (i == numBatches - 1  || i % entityBatchInSearch == (entityBatchInSearch - 1)) {
                        newEntityFilter.setRightParens(1);
                    } else {
                        newEntityFilter.setOr(true);
                    }
                } else {
                    entityFilterGroups = [];
                }
                entityFilterGroups.push(newEntityFilter);
                if (i == numBatches - 1  || i % entityBatchInSearch == (entityBatchInSearch - 1)) {
                    filterBatches.push(entityFilterGroups);
                }
            }
        }
        return filterBatches;
    }
    
    this.GetEntitiesWithoutBalance = getEntitiesWithoutBalance;
    this.GetEntitiesForFormat_AP = getEntitiesForFormat_AP;
};


	//end classes
	
	 var transfilters = [];
	 var transfilterstoremove = [];
	 var transfieldstoremove = [];
	 
	 var typeValidValuesFilter = ['text', 'select', 'integer', 'date', 'currency', 'longtext', 'checkbox', 'multiselect'];
	 var displayTypeValidValuesFilter = ['hidden', 'inline', 'normal', 'disabled'];
	 
	 var typeValidValuesColumn = ['text', 'integer', 'date', 'currency'];
	 var displayTypeValidValuesColumn = ['hidden', 'normal'];
	 
	 var filtersToBeRemoved = ['custpage_2663_transtype', 'custpage_2663_date_from', 'custpage_2663_date_to', 'custpage_2663_vendor', 'custpage_2663_employee', 'custpage_2663_partner'];
	 var fieldsToBeRemoved = ['custpage_2663_payment_ref', 'custpage_2663_aggregate', 'custpage_2663_agg_method'];
	 
	 var tOrF = ['T', 'F'];
	 var trueOrFalse = [true, false];
	 
	 var eftUIObj = new psg_ep.PaymentSelectionForm_EFT();
	 
	function getForm(){
		return eftUIObj._uiObj.Form; 
	}
	
	function addFilter(position, reference, type, label, displayType, defaultValue, helpText, source, maxLength){
	
		if(position !== undefined && reference && type && label){
			var id = 'custpage_2663_' + reference;
			
			if(trueOrFalse.indexOf(position) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for position parameter. Valid values are true or false.', true);
			} else if(typeValidValuesFilter.indexOf(type) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for type parameter.', true);
			} else if(displayType && displayTypeValidValuesFilter.indexOf(displayType) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for displayType parameter.', true);
			} else if(type == 'checkbox' && defaultValue && tOrF.indexOf(defaultValue) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for defaultValue parameter. Valid values for type checkbox are true or false.', true);
			} else if(maxLength && isNaN(parseInt(maxLength))){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for maxLength parameter. Valid values are integers only.', true);
			} else{
			
				if(position){
					psg_ep.CustomSearchFilters[id] = { type: type,
																		label: label,
																		displayType: displayType || '',
																		defaultValue: defaultValue || '',
																		helpText: helpText || '',
																		source: source || '',
																		maxLength: maxLength || ''};
				} else{
					psg_ep.CustomTransactionFilterFields[id] = { type: type,
																					  label: label,
																					  displayType: displayType || '',
																					  defaultValue: defaultValue || '',
																					  helpText: helpText || '',
																					  source: source || '',
																					  maxLength: maxLength || ''};
				}
	
				if(type == 'text'){
					addFilterField(reference, null, 'contains', defaultValue);
				} else if(type == 'multiselect'){
					addFilterField(reference, null, 'anyof', defaultValue);
				} else if(type == 'checkbox'){
					addFilterField(reference, null, 'is', defaultValue);
				} else if(type == 'select'){
					addFilterField(reference, null, 'is', defaultValue);
				} 
				transfilters.push(id);
			}
		} else{
			throw nlapiCreateError('EP_MISSING_FIELDS', 'Please enter required filter parameters: isDefaultFilter, reference, type, label', true);
		}
	}
	
	function addFilterField(id, join, operator, value){
	
		psg_ep.CustomTransactionFilters[id] = { join: join || '',
																   operator: operator || '',
																   value: value || ''};
	
	}
	
	function addColumn(type, name, reference, isText, displayType, defaultValue, join){
	
		if(type && name && reference){
			if(typeValidValuesColumn.indexOf(type) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for type parameter.', true);
			} else if(isText && trueOrFalse.indexOf(isText) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for isText parameter. Valid values are true or false.', true);
			} else if(displayType && displayTypeValidValuesColumn.indexOf(displayType) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Please enter a valid value for displayType parameter.', true);
			} else{
				var id = 'custpage_' + reference;
				psg_ep.CustomTransactionColumns[id] = {type: type,
																			  name: name,
																			  reference: reference,
																			  isText: isText || '',
																			  displayType: displayType || '',
																			  defaultValue: defaultValue || null,
																			  join: join || ''
				};
			}
		} else{
			throw nlapiCreateError('EP_MISSING_FIELDS', 'Please enter required column parameters: type, name, reference.', true);
		}
	}
	
	function removeFilter(id){
		if(id){
			if(filtersToBeRemoved.indexOf(id) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Filter ' + id + ' cannot be deleted', true);
			} else{
				transfilterstoremove.push(id);
			}
		} else{
			throw nlapiCreateError('EP_MISSING_FIELDS', 'Please enter required id of filter to be removed.', true);
		}
	}
	
	function removeField(id){
		if(id){
			if(fieldsToBeRemoved.indexOf(id) < 0){
				throw nlapiCreateError('EP_INVALID_VALUE', 'Field ' + id + ' cannot be deleted', true);
			} else if(id == 'custpage_2663_aggregate'){
				transfieldstoremove.push(id);
				transfieldstoremove.push('custpage_2663_agg_method');
			} else{
				transfieldstoremove.push(id);
			}
		} else{
			throw nlapiCreateError('EP_MISSING_FIELDS', 'Please enter required id of field to be removed.', true);
		}
	}
	
	function buildUI(){
		
		if((new _2663.EditionControl()).IsLicensed()){
			if(transfilters){
				psg_ep.CustomFields['custpage_2663_custom_flds_transfilters'].defaultValue = JSON.stringify(transfilters);
			}
			
			if(transfilterstoremove){
				psg_ep.CustomFields['custpage_2663_remove_flds_transfilters'].defaultValue = JSON.stringify(transfilterstoremove);
			}
			
			if(transfieldstoremove){
				psg_ep.CustomFields['custpage_2663_remove_flds_trans'].defaultValue = JSON.stringify(transfieldstoremove);
			}
	
			eftUIObj.BuildUI();
			eftUIObj.InitializeRequestParameters(request);    
			eftUIObj.InitializeFieldData();    
			eftUIObj.CheckApprovalRouting();    
			eftUIObj.LoadSublist(request);    
	
		} else {
	    	throw nlapiCreateError('IPM_NOT_LICENSED', 'This edition does not support addition and removal of Filters or Columns.', true);	
	    }
	}
    
    this.BuildUI = buildUI;
    this.RemoveField = removeField;
    this.RemoveFilter = removeFilter;
    this.AddColumn = addColumn;
    this.AddFilter = addFilter;
    this.GetForm = getForm;
}