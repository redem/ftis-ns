/**
 * © 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 */

define(['../../app/9997_PaymentFlagProcessor'],

function(flagProcessorClass) {

    var TYPE_CUSTOMER_PAYMENT = 'customerpayment';
    var CONFIRM_CLEAR_OTHER_PAYMENT_METHOD = 'Checking this box automatically clears entries for other payment option such as Credit Card Information, Check #, CC Approved or Charge Credit Card. Click OK if you want to proceed.';
    var isEntitySourcingOngoing = false;
    var sourcedEPFlagValue = false;

    function pageInit(context) {
        if (context.mode == 'create') {
            var record = context.currentRecord;
            var type = record.type;

            var flagProcessor = flagProcessorClass.create(type, record);

            if (flagProcessor.isValidPayment() && flagProcessor.isForEP()){
                flagProcessor.clearOtherPaymentMethods();
            }
        }
    }

    function fieldChanged(context) {
        var record = context.currentRecord;
        var type = record.type;
        var fieldId = context.fieldId;

        var flagProcessor = flagProcessorClass.create(type, record);

        if (flagProcessor.isValidPayment()) {

            if (isCustomerPayment(type) && isEntitySourcingOngoing && flagProcessor.isFieldForEP(fieldId)) {
                sourcedEPFlagValue = flagProcessor.isForEP();
            }

            if (flagProcessor.isFieldForEP(fieldId) && flagProcessor.isForEP()) {
                var clearOtherPayment = true;

                if (isCustomerPayment(type) && !isEntitySourcingOngoing) {
                    clearOtherPayment = confirm(CONFIRM_CLEAR_OTHER_PAYMENT_METHOD);
                }

                if (clearOtherPayment) {
                    flagProcessor.clearOtherPaymentMethods();
                } else {
                    flagProcessor.inactivateEPFlag();
                }

            } else if (flagProcessor.isFieldForOtherPaymentMethod(fieldId) && flagProcessor.isValueForTypeTrue(fieldId, record.getValue(fieldId))) {
                flagProcessor.inactivateEPFlag();
            } else if (isCustomerPayment(type) && fieldId == 'customer') {
                isEntitySourcingOngoing = true;
            }

        }
    }

    function postSourcing(context) {
        var record = context.currentRecord;
        var type = record.type;
        var fieldId = context.fieldId;

        var flagProcessor = flagProcessorClass.create(type, record);

        if (isCustomerPayment(type) && isEntitySourcingOngoing && fieldId == 'paymentmethod' && sourcedEPFlagValue) {
            flagProcessor.activateEPFlag();
            flagProcessor.clearOtherPaymentMethods();
        } else if (isCustomerPayment(type) && isEntitySourcingOngoing && fieldId == 'customer') {
            isEntitySourcingOngoing = false;
        }
    }
    
    function isCustomerPayment(paymentType) {
        return paymentType == TYPE_CUSTOMER_PAYMENT;
    }

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        postSourcing: postSourcing
    };
});
