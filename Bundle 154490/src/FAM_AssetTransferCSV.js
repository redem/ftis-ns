/**
 * � 2014 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
**/

var FAM;
if (!FAM) { FAM = {}; }

/**
 * Starter for Background Processing function for Bulk Asset Transfer via CSV file
 *
 * Parameters:
 *     BGP {FAM.BGProcess} - Process Instance Record for this background process
 * Returns:
 *     true {boolean} - processing should be requeued
**/
function famAssetTransferCSV(BGP) {
    var assetTransferCSV = new FAM.AssetTransferCSV(BGP);

    try {
        assetTransferCSV.run();
        return true;
    }
    catch (e) {
        assetTransferCSV.logObj.printLog();
        throw e;
    }
}

/**
 * Class for validating CSV for Bulk Asset Transfer
 *
 * Constructor Parameters:
 *     procInsRec {FAM.BGProcess} - Process Instance Record for this background process
**/
FAM.AssetTransferCSV = function (procInsRec) {
    this.procInsRec = procInsRec;

    this.transferValues = [];

    this.totalRecords = +this.procInsRec.getFieldValue('rec_total') || 0;
    this.recsProcessed = +this.procInsRec.getFieldValue('rec_count') || 0;
    this.recsFailed = +this.procInsRec.getFieldValue('rec_failed') || 0;
    this.transferBGP = +this.procInsRec.stateValues.TransferBGP || 0;

    this.logObj = new printLogObj('debug');
    this.perfTimer = new FAM.Timer();

    this.headers = {
        asset : 'Asset',
        assetType : 'Asset Type',
        subsidiary : 'Subsidiary',
        classFld : 'Class',
        department : 'Department',
        location : 'Location'
    };
    this.stateHeaders = {
        asset : 'id',
        assetType : 'a',
        subsidiary : 's',
        classFld : 'c',
        department : 'd',
        location : 'l'
    };

    this.records = {
         asset : 'customrecord_ncfar_asset',
         assetType : 'customrecord_ncfar_assettype',
         subsidiary : 'subsidiary',
         classFld : 'classification',
         department : 'department',
         location : 'location'
    };

    this.assetFields = {
            asset : 'internalid',
            assetType : 'custrecord_assettype',
            subsidiary : 'custrecord_assetsubsidiary',
            classFld : 'custrecord_assetclass',
            department : 'custrecord_assetdepartment',
            location : 'custrecord_assetlocation'
    };

    this.features = {
         subsidiary : 'SUBSIDIARIES',
         classFld : 'CLASSES',
         department : 'DEPARTMENTS',
         location : 'LOCATIONS'
    };

    this.lists = {};
    this.logMessage = {
        LOG_SUBSIDIARYBOOKID : 'message_subsidiarybookid',
        LOG_SUBSIDIARYMETHOD : 'message_subsidiarymethod',
        LOG_EMPTYSUBATFIELD : 'message_emptysubat',
        LOG_NOCHANGE : 'message_nochange',
        LOG_NOTRANSFERACCT : 'message_notransferacct',
        LOG_SUBDEPT : 'message_subdept_notmatch',
        LOG_SUBLOC : 'message_subloc_notmatch',
        LOG_SUBCLASS : 'message_subclass_notmatch',
        LOG_AT_ASSETACCT : 'message_atassetacct',
        LOG_AT_DEPRACCT : 'message_atdepracct',
        LOG_AT_CDL_MAND : 'message_cdl_mandatory',
        LOG_INVALID_ASSET : 'message_invalidasset',
        LOG_ONETRANSFERTYPE : 'message_onetransfertype',
        LOG_NOEXIST : 'message_nonexistentfld'
    };
    this.perfTimer.start();
};

FAM.AssetTransferCSV.prototype.execLimit = 500;
FAM.AssetTransferCSV.prototype.timeLimit = 30 * 60 * 1000; // 30 minutes
FAM.AssetTransferCSV.prototype.bgpLimit = 99950; //100k - journal permission string

/**
 * Main function for this class
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.AssetTransferCSV.prototype.run = function () {
    this.logObj.startMethod('FAM.AssetTransferCSV.run');

    var headerError, csvValues = FAM_Util.parseCSV(this.loadFileContents(), { header : true });

    if (this.totalRecords === 0) {
        this.totalRecords = csvValues.data.length;
    }
    if (this.totalRecords === 0) {
        this.updateProcIns({ status : FAM.BGProcessStatus.Completed });
        return;
    }
    headerError = this.checkHeaders(csvValues.meta.fields);
    if (headerError) {
        this.updateProcIns({
            status : FAM.BGProcessStatus.Failed,
            message : headerError
        });
        return;
    }
    if (this.transferBGP) {
        this.retrieveWorkingSet();
    }

    this.iterateThroughCSV(csvValues);

    this.logObj.endMethod();
};

/**
 * Retrieves the contents of the user-input CSV File
 *
 * Parameters:
 *     none
 * Returns:
 *     {string} - file contents
**/
FAM.AssetTransferCSV.prototype.loadFileContents = function () {
    this.logObj.startMethod('FAM.AssetTransferCSV.loadFileContents');

    var contents, fileObj = nlapiLoadFile(this.procInsRec.stateValues.FileId);

    if (fileObj) {
        contents = fileObj.getValue() || '';
        contents = contents.replace(/^[\r\n]+|\.|[\r\n]+$/g, ""); //Trim down leading and trailing blank lines
    }
    else {
        throw FAM.resourceManager.GetString('message_errorfile', 'assettransfercsv_ss');
    }

    this.logObj.endMethod();
    return contents;
};

/**
 * Validates the headers used by the user
 *
 * Parameters:
 *     headers {Array} - headers in the CSV File
 * Returns:
 *     {String} - error message or null
**/
FAM.AssetTransferCSV.prototype.checkHeaders = function (headers) {
    this.logObj.startMethod('FAM.AssetTransferCSV.checkHeaders');

    var i, j, expectedHeaders, existingHeaders = [], message = null;
    if (headers.length < 2) {
        message = FAM.resourceManager.GetString('message_headertoofew', 'assettransfercsv_ss');
    }
    else if (headers.indexOf(this.headers.asset) === -1) {
        message = FAM.resourceManager.GetString('message_headerassetmissing',
            'assettransfercsv_ss');
    }
    else {
        expectedHeaders = Object.keys(this.headers);
        for (j = 0; j < expectedHeaders.length; j++) {
            expectedHeaders[j] = this.headers[expectedHeaders[j]];
        }

        for (i = 0; i < headers.length; i++) {
            if (expectedHeaders.indexOf(headers[i]) === -1) {
                message = FAM.resourceManager.GetString('message_headernotallowed',
                    'assettransfercsv_ss', null, [headers[i]]);
                break;
            }
            else if (existingHeaders.indexOf(headers[i]) !== -1) {
                message = FAM.resourceManager.GetString('message_duplicateheader',
                    'assettransfercsv_ss', null, [headers[i]]);
                break;
            }
            else { existingHeaders.push(headers[i]); }
        }
    }

    this.logObj.endMethod();
    return message;
};

/**
 * Retrieves worked upon data from BGP record
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.AssetTransferCSV.prototype.retrieveWorkingSet = function () {
    this.logObj.startMethod('FAM.AssetTransferCSV.retrieveWorkingSet');

    var transferBGP = new FAM.BGProcess();

    transferBGP.loadRecord(this.transferBGP);
    transferBGP.parseStateValues();

    this.transferValues = transferBGP.stateValues;

    this.logObj.endMethod();
};

/**
 * Converts user inputs into machine-readable ids
 *
 * Parameters:
 *     csvValues {Object} - parsed csv values from file
 * Returns:
 *     void
**/
FAM.AssetTransferCSV.prototype.iterateThroughCSV = function (csvValues) {
    this.logObj.startMethod('FAM.AssetTransferCSV.iterateThroughCSV');

    var i, updatedValues, procFields = null, blnToRequeue = false, timer = new FAM.Timer(),
        lastId = +this.procInsRec.getScriptParam('lowerLimit') || 0;

    for (i = lastId; i < csvValues.data.length; i++) {
        // Re-initialize logObj for each summary to prevent flooding of log data
        this.logObj = new printLogObj('debug');
        this.logObj.startMethod('FAM.AssetTransferCSV.iterateThroughCSV');

        lastId = i;
        if (this.hasExceededLimit()) {
            blnToRequeue = true;
            break;
        }

        timer.start();
        updatedValues = this.validateAndConvert(csvValues.data[i], csvValues.meta.fields);
        this.checkResults(updatedValues, i + 2, timer.getReadableElapsedTime());
    }

    if (blnToRequeue) {
        this.logObj.logExecution('Execution Limit | Remaining Usage: ' +
            FAM.Context.getRemainingUsage() + ' | Time Elapsed: ' +
            this.perfTimer.getReadableElapsedTime());
        this.procInsRec.setScriptParams({ lowerLimit : lastId });
    }
    else {
        procFields = { status : FAM.BGProcessStatus.Completed };
    }

    if (this.transferValues.length > 0) {
        this.savetoBGP();
    }
    this.updateProcIns(procFields);

    this.logObj.endMethod();
};

/**
 * Determines if the Execution Limit or Time Limit has exceeded
 *
 * Parameters:
 *     none
 * Returns:
 *     true {boolean} - Execution Limit or Time Limit has exceeded
 *     false {boolean} - Execution Limit or Time Limit has not exceeded
**/
FAM.AssetTransferCSV.prototype.hasExceededLimit = function () {
    this.logObj.startMethod('FAM.AssetTransferCSV.hasExceededLimit');

    var ret = FAM.Context.getRemainingUsage() < this.execLimit ||
        this.perfTimer.getElapsedTime() > this.timeLimit;

    this.logObj.endMethod();
    return ret;
};

/**
 * Validates and Converts user inputs into machine-readable ids
 *
 * Parameters:
 *     lineData {Object} - one row/line of the user input CSV File
 *     headers {Array} - user-defined fields of the CSV File
 * Returns:
 *     {Object} - the specific row/line which values were converted to their respective ids
**/
FAM.AssetTransferCSV.prototype.validateAndConvert = function (lineData, headers) {
    this.logObj.startMethod('FAM.AssetTransferCSV.validateAndConvert');

    var i, fieldCount = 0, ret = {};

    try {
        if (lineData.__parsed_extra && lineData.__parsed_extra.length > 0) {
            throw nlapiCreateError('USER_ERROR', FAM.resourceManager.GetString(
                'message_toomanyfields', 'assettransfercsv_ss'));
        }

        if (!lineData[this.headers.asset]) {
            throw nlapiCreateError('USER_ERROR', FAM.resourceManager.GetString(
                'custpage_fields_missing', null, null, [this.headers.asset]));
        }

        for (i in this.headers) {
            if (lineData[this.headers[i]] !== undefined) {
                ret[this.stateHeaders[i]] = lineData[this.headers[i]] || 'unset';
                fieldCount++;
            }
        }
        if (fieldCount !== headers.length) {
            throw nlapiCreateError('USER_ERROR', FAM.resourceManager.GetString(
                'message_toofewfields', 'assettransfercsv_ss'));
        }

        if (this.transferValues.filter(this.checkDuplicates, lineData).length > 0) {
            throw nlapiCreateError('USER_ERROR', FAM.resourceManager.GetString(
                'message_duplicateasset', 'assettransfercsv_ss', null,
                [lineData[this.headers.asset]]));
        }

        ret = this.validateTranferValues(ret);
    }
    catch (e) {
        this.logObj.pushMsg('Unhandled Exception: ' + FAM_Util.printStackTrace(e), 'ERROR');
        this.logObj.printLog();

        ret.error = e;
    }

    this.logObj.endMethod();
    return ret;
};

/**
 * To be used as a callback function for Array.filter
 * Filters asset collection for a given asset id
 *
 * Parameters:
 *     element {Object} - state value object for asset transfer
 * Returns:
 *     {boolean} - element is for the same asset
**/
FAM.AssetTransferCSV.prototype.checkDuplicates = function (element) {
    return element.id == this.Asset;
};

/**
 * Checks result object, updates success/fail count, and writes to Process Log
 *
 * Parameters:
 *     resultObj {object} - return object of validateAndConvert
 *     lineNum {number} - line number on csv file
 *     elapsedTime {string} - elapsed time for processing, for logging purposes
 * Returns:
 *     void
**/
FAM.AssetTransferCSV.prototype.checkResults = function (resultObj, lineNum, elapsedTime) {
    this.logObj.startMethod('FAM.AssetTransferCSV.checkResults');

    if (resultObj.error) {
        this.recsFailed++;
        this.procInsRec.writeToProcessLog('Processing Failed: ' + resultObj.error, 'Error',
            'Line Number: ' + lineNum);
        this.logObj.pushMsg('Failed processing line: ' + lineNum + ', Elapsed Time: '
            + elapsedTime + ', Failed: ' + this.recsFailed, 'error');
        this.logObj.printLog();
    }
    else {

        if (resultObj.message) {
            this.addMsgToProcessLog(resultObj.message, lineNum);
            delete resultObj.message;
        }
        this.recsProcessed++;
        this.transferValues.push(resultObj);
        this.logObj.logExecution('Successful processing for line: ' + lineNum +
            ', Elapsed Time: ' + elapsedTime + ', Success: ' + this.recsProcessed);
        if (JSON.stringify(this.transferValues).length +
            JSON.stringify(resultObj).length > this.bgpLimit) {
            this.savetoBGP();
            this.transferBGP = 0;
            this.transferValues = [];
        }
    }

    this.logObj.endMethod();
};

/**
 * Saves process progress to Asset Transfer BGP
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.AssetTransferCSV.prototype.savetoBGP = function () {
    this.logObj.startMethod('FAM.AssetTransferCSV.savetoBGP');

    var transferBGP = new FAM.BGProcess(), newBGPStateVal = {};
    
    newBGPStateVal.JrnPermit    = this.procInsRec.stateValues.JrnPermit; //add Journal Permission
    newBGPStateVal.transferVals = JSON.stringify(this.transferValues);

    if (this.transferBGP) {
        transferBGP.recordId = this.transferBGP;
        transferBGP.submitField({ state : JSON.stringify(newBGPStateVal)});
    }
    else {
        this.logObj.pushMsg(FAM.BGProcessStatus.Queued);
        this.logObj.pushMsg(FAM.BGPActivityType.Direct);
        this.logObj.pushMsg(this.procInsRec.getFieldValue('user'));
        this.logObj.pushMsg(JSON.stringify(this.transferValues));
        transferBGP.createRecord({
            status : FAM.BGProcessStatus.Queued,
            recordid : -1,
            activity_type : FAM.BGPActivityType.Direct,
            func_name : 'famTransferAsset',
            proc_name : 'Asset Transfer',
            user : this.procInsRec.getFieldValue('user'),
            state : JSON.stringify(newBGPStateVal)
        });
        this.transferBGP = transferBGP.submitRecord();
    }
    this.logObj.endMethod();
};

/**
 * Updates Process Instance Record
 *
 * Parameters:
 *     procFields {object} - container for Process Instance Changed Fields
 * Returns:
 *     void
**/
FAM.AssetTransferCSV.prototype.updateProcIns = function (procFields) {
    this.logObj.startMethod('FAM.AssetTransferCSV.updateProcIns');

    var totalPercent;

    procFields = procFields || {};

    if (procFields.status === FAM.BGProcessStatus.Failed) {
        totalPercent = 100;
        this.recsFailed = this.totalRecords - this.recsProcessed;
    }
    else {
        totalPercent = FAM_Util.round(((this.recsProcessed + this.recsFailed) / this.totalRecords) *
            100);
    }
    totalPercent = (isNaN(totalPercent) || totalPercent === Infinity) ?  0 : totalPercent;

    FAM.Context.setPercentComplete(totalPercent);

    this.procInsRec.stateValues.TransferBGP = this.transferBGP;

    procFields.rec_total = this.totalRecords;
    procFields.rec_count = this.recsProcessed;
    procFields.rec_failed = this.recsFailed;
    procFields.state = JSON.stringify(this.procInsRec.stateValues);

    if (!procFields.message) {
        if (this.totalRecords === 0) {
            procFields.message = 'No records found.';
        }
        else {
            procFields.message = 'Processing CSV Lines | ' + totalPercent +
                '% Complete | ' + this.totalRecords + ' total | ' + this.recsProcessed +
                ' processed | ' + this.recsFailed + ' failed';
        }
    }

    if (procFields.status === FAM.BGProcessStatus.Completed && this.recsFailed > 0) {
        procFields.status = FAM.BGProcessStatus.CompletedError;
    }

    this.procInsRec.submitField(procFields);

    this.logObj.endMethod();
};

/**
 * Validates the pre-processed line values read from the csv file and returns
 * an object containing either state values or error messages
 *
 * Parameters:
 *     transferValues {Object} - e.g. {id:'10',a:'asset type',s:'Parent Company',c:'class',d:'dept',l:'loc'}
 * Returns:
 *     {Object} - e.g. {id:10,a:1,s:2,c:3,d:4,l:5} or {error:'error message'}
**/
FAM.AssetTransferCSV.prototype.validateTranferValues = function(transferValues) {

    var originalValues = this.getOriginalAssetValues(transferValues[this.stateHeaders.asset]);

    if (!originalValues) {
        return(this.createErrorObj(this.logMessage.LOG_INVALID_ASSET));
    }

    //existence check and convert to IDs
    var newValues = this.getTransferValues(transferValues);
    if (newValues.error) {
        return(this.createErrorObj(this.logMessage.LOG_NOEXIST, newValues.error));
    }

    //retrieve transferValues Ids
    var assetId = newValues[this.stateHeaders.asset],
    origType = originalValues.custrecord_assettype,
    origSub = originalValues.custrecord_assetsubsidiary,
    origClass = originalValues.custrecord_assetclass,
    origDept = originalValues.custrecord_assetdepartment,
    origLoc = originalValues.custrecord_assetlocation,
    newType = newValues[this.stateHeaders.assetType],
    newSub = newValues[this.stateHeaders.subsidiary],
    newClass = newValues[this.stateHeaders.classFld],
    newDept = newValues[this.stateHeaders.department],
    newLoc = newValues[this.stateHeaders.location],
    changedType = this.changedValue(origType, newType),
    changedSub = this.changedValue(origSub, newSub),
    changedClass = this.changedValue(origClass, newClass),
    changedDept = this.changedValue(origDept, newDept),
    changedLoc = this.changedValue(origLoc, newLoc),
    unsetType = this.isUnset(newType),
    unsetSub = this.isUnset(newSub),
    unsetClass = this.isUnset(newClass),
    unsetDept = this.isUnset(newDept),
    unsetLoc = this.isUnset(newLoc);

    var TRANSFER_SCDL = 1, TRANSFER_ASSETTYPE = 2;
    var warningMsg = [];

    // Asset Type must not be 'unset'
    if (unsetType) {
        return(this.createErrorObj(this.logMessage.LOG_EMPTYSUBATFIELD));
    }

    //check if all values are the same as the as the original
    if (changedSub || changedClass || changedDept || changedLoc) {
        transferType = TRANSFER_SCDL;

        if (changedType) {
            // Allowed transfers: (a) Asset Type only XOR (b) Subsidiary, Class, Department and/or Location
            return(this.createErrorObj(this.logMessage.LOG_ONETRANSFERTYPE));
        }
    }
    else if (changedType) {
        transferType = TRANSFER_ASSETTYPE;
    }
    else {
        return(this.createErrorObj(this.logMessage.LOG_NOCHANGE));
    }

    newValues = this.removeUnchangedValues(originalValues, newValues);

    // check Asset Type Asset Account and Depreciation Account
    if (transferType == TRANSFER_ASSETTYPE) {
        assetTypeAccounts = nlapiLookupField('customrecord_ncfar_assettype', newType,
            ['custrecord_assettypeassetacc', 'custrecord_assettypedepracc']);

        if (!assetTypeAccounts.custrecord_assettypeassetacc) {
            return(this.createErrorObj(this.logMessage.LOG_AT_ASSETACCT));
        }

        if (!assetTypeAccounts.custrecord_assettypedepracc) {
            return(this.createErrorObj(this.logMessage.LOG_AT_DEPRACCT));
        }
    }

    if (FAM.Context.blnOneWorld) {
        //Subsidiary must have a value
        if (unsetSub || (!changedSub && !origSub)) {
            return(this.createErrorObj(this.logMessage.LOG_EMPTYSUBATFIELD));
        }

        // CDL must not be unset if mandatory
        if((unsetDept && FAM.Context.getPreference('deptmandatory') === 'T')||
           (unsetClass && FAM.Context.getPreference('classmandatory') === 'T')||
           (unsetLoc && FAM.Context.getPreference('locmandatory') === 'T')){

            return(this.createErrorObj(this.logMessage.LOG_AT_CDL_MAND));
        }

        // Check C/D/L + Subsidiary Combination Validation
        var subToCheck = changedSub ? newSub : origSub,
            classToCheck = changedClass ? newClass : origClass,
            deptToCheck = changedDept ? newDept : origDept,
            locToCheck = changedLoc ? newLoc : origLoc;

        if (deptToCheck && (changedSub || changedDept) && !unsetDept &&
            !FAM.compareCDLSubsidiary('department', deptToCheck, subToCheck)) {

            return(this.createErrorObj(this.logMessage.LOG_SUBDEPT));
        }

        if (classToCheck && (changedSub || changedClass) && !unsetClass &&
            !FAM.compareCDLSubsidiary('classification', classToCheck, subToCheck)) {

            return(this.createErrorObj(this.logMessage.LOG_SUBCLASS));
        }

        if (locToCheck && (changedSub || changedLoc) && !unsetLoc &&
                !FAM.compareCDLSubsidiary('location', locToCheck, subToCheck)) {

            return(this.createErrorObj(this.logMessage.LOG_SUBLOC));
        }

        if (changedSub) {
            // Check if Transfer Account exists
            if (!FAM.Util_Shared.getTransferAccounts(origSub, newSub)) {
                return(this.createErrorObj(this.logMessage.LOG_NOTRANSFERACCT));
            }

            // Check Subsidiary + Accounting Books combination
            bookIds = FAM.Util_Shared.getTaxMethodBookIds(assetId);
            if (!FAM.Util_Shared.isValidSubsidiaryBooks(newSub, bookIds)) {
                warningMsg.push(this.logMessage.LOG_SUBSIDIARYBOOKID);
            }

            // Check Subsidiary + Alternate Methods combination
            if (!FAM.Util_Shared.compatibleSubAndMethods(assetId, newSub)) {
                warningMsg.push(this.logMessage.LOG_SUBSIDIARYMETHOD);
            }
        }
    }

    if (warningMsg.length > 0) {
        newValues.message = warningMsg;
    }
    return newValues;
};

/**
 * Returns the internal id of a transfer value. Also stores retrieved values in the
 * 'lists' array to minimize searches on the succeeding validations.
 *
 * Parameters:
 *     value {String} - the value of a field; could also be 'unset' or undefined
 *     record {String} - the id of the record; e.g. 'subsidiary'
 * Returns:
 *     {number} - the id of the value if found, otherwise it returns null
**/
FAM.AssetTransferCSV.prototype.checkExistence = function (value, record) {
    var ret = null;
    if (!value || !record) {
        return null;
    }

    if (!this.lists[record]) {
        this.lists[record] = {};
        var filters = [new nlobjSearchFilter('isinactive', null, 'is', 'F')];

        var columns = [
            new nlobjSearchColumn('name'),
            new nlobjSearchColumn('internalid').setSort(true)
        ];
        var res = nlapiSearchRecord(record, null, filters, columns);

        if (res) {
            for (var i=0; i < res.length; i++) {
                this.lists[record][res[i].getValue('name')] = res[i].getId();
            }

            ret = this.lists[record][value];
            if (ret) {
                return +ret;
            }
            else if (record === 'subsidiary') {
                var subId = this.searchSubsidiaryMatch(value);
                if (subId) {
                    this.lists[record][value] = +subId;
                    return +subId;
                }

            }
        }
    }
    else {
        var ret = this.lists[record][value];
        if (ret) {
            return +ret;
        }
        else if (record === 'subsidiary') {
            var subId = this.searchSubsidiaryMatch(value);
            if (subId) {
                this.lists[record][value] = +subId;
                return +subId;
            }
        }
        else{
            var filter = [new nlobjSearchFilter('isinactive', null, 'is', 'F'),
                          new nlobjSearchFilter('name', null, 'is', value)];

            var column = new nlobjSearchColumn('internalid');
            var res = nlapiSearchRecord(record, null, filter, column);

            if (res) {
                ret = res[0].getId();
                this.lists[record][value] = res[0].getId();
                return +ret;
            }
        }
    }

    return null;
};

/**
 * Returns true if the field is unset
 *
 * Parameters:
 *     value {number or String} - current value; internal id other possible values are: 'unset' or undefined
 * Returns:
 *     {boolean}
**/
FAM.AssetTransferCSV.prototype.isUnset = function (value) {
    return value === 'unset';
};

/**
 * Returns true if the field has been changed
 *
 * Parameters:
 *     origValue {number or String} - current value; internal id other possible values are: 'unset' or undefined
 *     newValue {number or String} - value from csv; internal id other possible values are: 'unset' or undefined
 * Returns:
 *     {boolean} - true if the value changed
**/
FAM.AssetTransferCSV.prototype.changedValue = function (origValue, newValue) {
    if (!newValue || (this.isUnset(newValue) && !origValue)) {
        return false;
    }
    return newValue != origValue;
};

/**
 * Retrieves the Asset's current values and stores them in an object.
 *
 * Parameters:
 *     assetId {number} - asset internal id
 * Returns:
 *     {Object} - contains the assets values (IDs)
**/
FAM.AssetTransferCSV.prototype.getOriginalAssetValues = function (assetId) {
    var ret = null;
    if (assetId) {
        var fields = ['internalid',
                      'custrecord_assettype',
                      'custrecord_assetsubsidiary',
                      'custrecord_assetclass',
                      'custrecord_assetdepartment',
                      'custrecord_assetlocation',
                      'isinactive'];
        try {
            ret = nlapiLookupField('customrecord_ncfar_asset', assetId, fields);
            if (ret && ret.isinactive == 'T') {
                return null;
            }
        } catch (ex) {
            this.logObj.logExecution(ex.toString());
            return null;
        }
    }
    return ret;
};

/**
 * Returns an object containing the IDs; returns an object with an error property containing the field with the error.
 *
 * Parameters:
 *     transferValues {Object} - contains the pre-processed line values from the csv file
 * Returns:
 *     {Object} - contains the line values converted to record IDs (except for 'unset')
**/
FAM.AssetTransferCSV.prototype.getTransferValues = function (transferValues) {
    var newValues = {};
    for (var fld in this.stateHeaders) {
        var value = transferValues[this.stateHeaders[fld]];
        if (fld == 'asset') {
            newValues[this.stateHeaders[fld]] = +value;
        }
        else {
            if (value) {
                if(fld != 'assetType' && !FAM.Context.isFeatureEnabled(this.features[fld])) {
                    continue;
                }

                if(!this.isUnset(value) && !(value = this.checkExistence(value, this.records[fld]))) {
                    return {error: this.headers[fld]};
                }
                newValues[this.stateHeaders[fld]] = value;
            }
        }
    }
    return newValues;
};

/**
 * Creates an object that contains the error message for the BG Process Log
 *
 * Parameters:
 *     log {String} - the 'name' attribute in the resource file
 *     column {String} - the column name; e.g. Subsidiary, Class
 * Returns:
 *     {Object} - e.g. {error: '<error message>'}
**/
FAM.AssetTransferCSV.prototype.createErrorObj = function (log, column) {
    log = !log ? 'ERROR' : FAM.resourceManager.GetString(log,'assettransfercsv_ss');
    if (column) {
        log = log + ' : ' + column;
    }
    return {error: log};
};

/**
 * Adds messages to the process log (one log per message).
 * Parameters:
 *      msgArray {String[]} string of names for the resource manager e.g. ['message_subsidiarybookid']
 *      lineNum {number} the line being validated on the csv file
 * Returns:
 *      void
 */
FAM.AssetTransferCSV.prototype.addMsgToProcessLog = function (msgArray, lineNum) {
    if (msgArray instanceof Array) {
        for (var i = 0; i < msgArray.length; i++) {
            if(msgArray[i]) {
                this.procInsRec.writeToProcessLog('Warning: ' + FAM.resourceManager.GetString(msgArray[i], 'assettransfercsv_ss'),
                        'Message', 'Line Number: ' + lineNum);
            }
        }
    }
};

/**
 * Returns the internal id of the subsidiary given the name
 * Parameters:
 *      value {String} - subsidiary name could be a subsidiary tree, tree section or name. In cases where name is used and there are
 *                          subsidiaries with similar names, the one with the lowesr internal id will be returned.
 * Returns:
 *      {number} - the internal id of the subsidiary
 */
FAM.AssetTransferCSV.prototype.searchSubsidiaryMatch = function (value) {
    if (!value) {
        return null;
    }

    var formula = "REGEXP_SUBSTR({name},'^" + value + "$|: " + value + "$')";

    var filters = [
        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
        new nlobjSearchFilter('formulatext', null, 'isnotempty').setFormula(formula)
    ];

    var columns = [new nlobjSearchColumn('internalid').setSort()];

    var res = nlapiSearchRecord('subsidiary', null, filters, columns);

    if (res) {
        return res[0].getId();
    }

    return null;
};

/**
 * Returns an object whose values values have changed
 * Parameters:
 *  origValues {Object} - original values where the field names are asset record fields (see assetFields)
 *  newValues {Object} - new values where the field names are specified in stateHeaders
 * Returns:
 *  {Object} - with field names specified in stateHeaders
 */
FAM.AssetTransferCSV.prototype.removeUnchangedValues = function (origValues, newValues) {
    var finalValues = {};

    for (var fld in this.stateHeaders) {
        if (fld === 'asset' || this.changedValue(origValues[this.assetFields[fld]], newValues[this.stateHeaders[fld]])) {
            finalValues[this.stateHeaders[fld]] = newValues[this.stateHeaders[fld]];
        }
    }
    return finalValues;
};



