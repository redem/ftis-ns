/**
 * � 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var FAM;
if (!FAM) { FAM = {}; }
FAM.DefaultAltDepr_CS = new function () {
    this.alertMessage = {};
    this.bookStatus = {};

    this.pageInit = function(type){

        this.alertMessage = FetchMessageObj({
            ALERT_ALREADY_USED          : 'client_defaultaltdepr_alreadyused',
            ALERT_ASSET_AL_WARNING      : 'client_altdepr_assetlifetimewarning',
            ALERT_ASSET_AL_ERROR        : 'client_altdepr_assetlifetimeerror',
            ALERT_IMPROPER_CONVENTION   : 'client_altmethod_improperconvention',
            ALERT_ADD_NOT_ALLOWED       : 'client_defaultaltdepr_addnotallowed',
            ALERT_MULTIBOOK_DISABLED    : 'client_multibookdisabled',
            ALERT_PRIMARY_BOOK          : 'client_primarybook',
            ALERT_PENDING_BOOK          : 'client_pendingbook'});

        this.disableFields();
        this.displaySubsidiary();
        //Cache the Accounting Book Status
        this.bookStatus = new FAM.FieldCache('accountingbook');
    };

    this.saveRecord = function(type){

        if(!nlapiGetFieldValue('custrecord_altdeprdef_assettype')){
            alert(this.alertMessage.ALERT_ADD_NOT_ALLOWED);
            return false;
        }

        //check duplicate Alternate Method
        var hasDupAltMeth = FAM.Util_Shared.checkDupAltDep('assetType',
                                        nlapiGetFieldValue('custrecord_altdeprdef_assettype'),
                                        nlapiGetFieldValue('custrecord_altdeprdef_altmethod'),
                                        nlapiGetFieldValue('custrecord_altdeprdef_accountingbook'),
                                        nlapiGetRecordId());
        if(hasDupAltMeth){
            var printMessage = InjectMessageParameter(this.alertMessage.ALERT_ALREADY_USED,new Array(nlapiGetFieldText('custrecord_altdeprdef_altmethod')));
            alert(printMessage);
            return false;
        }

        //Check DefaultAlternate Method AL against AssetType AL
        var alternateLifeTime = nlapiGetFieldValue('custrecord_altdeprdef_lifetime');
        alternateLifeTime = parseInt(alternateLifeTime, 10) || 0;

        var searchFilter = [new nlobjSearchFilter('internalid',
                             null,
                             'anyof',
                             nlapiGetFieldValue('custrecord_altdeprdef_assettype'))];
        var searchColumn = [new nlobjSearchColumn('custrecord_assettypelifetime')];
        var searchResults = nlapiSearchRecord('customrecord_ncfar_assettype', null, searchFilter, searchColumn);

        if (searchResults != null && searchResults.length > 0) {
            var assetTypeLifeTime = searchResults[0].getValue('custrecord_assettypelifetime');
            assetTypeLifeTime = parseInt(assetTypeLifeTime, 10) || 0;
            if (nlapiGetFieldValue('custrecord_altdeprdef_depreciationperiod') == DEPR_PERIOD_ANNUALLY) {
                //Convert AL to monthly units
                alternateLifeTime = alternateLifeTime * 12;
            }
            if (alternateLifeTime > assetTypeLifeTime) {
                if (FAM_Util.isAllowGreaterAl()) {
                    //notify only as warning since user perference allows it
                    alert(this.alertMessage.ALERT_ASSET_AL_WARNING);
                } else {
                    //validation error
                    alert(this.alertMessage.ALERT_ASSET_AL_ERROR);
                    return false;
                }
            }
        }

        this.setDefaults();
        return true;
    };

    this.validateField = function(type, name){
      //check convention against depreciation period
        if(name == 'custrecord_altdeprdef_deprmethod' || name == 'custrecord_altdeprdef_convention')
        {
          if(!FAM_Util.checkConvention(nlapiGetFieldValue('custrecord_altdeprdef_deprmethod'), nlapiGetFieldValue('custrecord_altdeprdef_convention')))
          {
              alert (this.alertMessage.ALERT_IMPROPER_CONVENTION);
              return false;
          }
        }
        else if(name == 'custrecord_altdeprdef_accountingbook') {
            var aBookId = nlapiGetFieldValue('custrecord_altdeprdef_accountingbook');
            if(aBookId) {
                if(!FAM.Context.blnMultiBook) {
                    alert(this.alertMessage.ALERT_MULTIBOOK_DISABLED);
                    return false;
                }
                else if(this.bookStatus.fieldValue(aBookId,'isprimary') == 'T') {
                    alert(this.alertMessage.ALERT_PRIMARY_BOOK);
                    return false;
                }
                else if(this.bookStatus.fieldValue(aBookId, 'status') != FAM.AccountingBookStatus.Active) {
                    alert(this.alertMessage.ALERT_PENDING_BOOK);
                    return false;
                }
            }
        }
        return true;
    };

    this.postSourcing = function(type, name){
        this.setDefaults(name);
        this.disableFields();
    };

    this.disableFields = function(){
        var bool = true;
        if (nlapiGetFieldValue('custrecord_altdeprdef_override') == 'T') {
            bool = false;
        }
        nlapiDisableField('custrecord_altdeprdef_deprmethod', bool);
        nlapiDisableField('custrecord_altdeprdef_convention', bool);
        nlapiDisableField('custrecord_altdeprdef_lifetime', bool);
        nlapiDisableField('custrecord_altdeprdef_financialyear', bool);
        nlapiDisableField('custrecord_altdeprdef_periodconvention', bool);
    };

    this.setDefaults = function(name){
        if (nlapiGetFieldValue('custrecord_altdeprdef_financialyear') == '') {
            nlapiSetFieldValue('custrecord_altdeprdef_financialyear', 1);
        }

        if (nlapiGetFieldValue('custrecord_altdeprdef_periodconvention') == '') {
            nlapiSetFieldValue('custrecord_altdeprdef_periodconvention', 1);
        }

        if (name=='custrecord_altdeprdef_altmethod') {
            this.displaySubsidiary();
        }
    };

    this.displaySubsidiary = function(){
        if (nlapiGetContext().getSetting('FEATURE','SUBSIDIARIES') != 'T') {
            return;
        }

        var altmethod = nlapiGetFieldValue('custrecord_altdeprdef_altmethod');
        if (altmethod == '') {
            nlapiSetFieldValues('custrecord_altdeprdef_subsidiary', []);
        } else {
            var sub = nlapiLookupField('customrecord_ncfar_altmethods', altmethod, 'custrecord_altmethodsubsidiary');
            nlapiSetFieldValues('custrecord_altdeprdef_subsidiary', sub.split(','));
        }
    };
};

