/**
 * © 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @NScriptName FAM Form Utility
 * @NScriptId _fam_util_form
 * @NApiVersion 2.0
*/

define(['../adapter/fam_adapter_ui_serverWidget'],
        
function (ui){
    var fieldTypeMap  = {},
        layoutTypeMap = {},
        breakTypeMap  = {},
        displayType   = {},
        subListType   = {},
        propertyMap   = {};
    
    var createField = function(form, objFld){
        objFld.options.type = fieldTypeMap[objFld.options.type]; //convert to serverWidget fieldtype enum
        var element = form.addField(objFld.options);
        for(var p in objFld){
            if(objFld && propertyMap[p]){
                element[propertyMap[p]] = objFld[p];
            }
        }
        if(objFld.help){
            element.setHelpText({help : objFld.help});
        }
        if(objFld.breakType){
            element.updateBreakType({breakType : breakTypeMap[objFld.breakType]});
        }
        if(objFld.displayType){
            element.updateDisplayType({displayType: displayType[objFld.displayType]});
        }
        if(objFld.layoutType){
            element.updateLayoutType({layoutType : layoutTypeMap[objFld.layoutType]});
        }
    };
    
    function insertLink(msg, link){
        if(!msg || !link) return null;
        var linkLabel   = msg.slice((msg.indexOf('(') + 1),+msg.indexOf(')')),
            linkHtml    = '<a href=' + link + '>' + linkLabel + '</a>';
        return msg.replace('(','').replace(')','').replace(linkLabel, linkHtml);
    }
    
    return {
        /**
         * Creates UI form
         * Parameters:
         *     objForm {obj} - Object collection of form entitities
         *     
         * Returns:
         *     form {serverWidget.Form}
         */
        createForm : function (objForm){
            var form;
            this.assignMapValues();
            form = ui.createForm(objForm.form);
            for(var grp in objForm.groups){
                form.addFieldGroup(objForm.groups[grp]);
            }
            for(var tab in objForm.tabs){
                form.addTab(objForm.tabs[tab]);
            }
            for(var fld in objForm.fields){
                createField(form, objForm.fields[fld]);
            }
            for(var sl in objForm.sublists){
                var sub;
                objForm.sublists[sl].type = subListType[objForm.sublists[sl].type];
                sub = form.addSublist(objForm.sublists[sl]);
                for(col in objForm.sublists[sl].cols){
                    createField(sub, objForm.sublists[sl].cols[col]);
                }
            }
            for(var btn in objForm.buttons){
                form.addButton(objForm.buttons[btn]);
            }
            for(var subBtn in objForm.submits){
                form.addSubmitButton(objForm.submits[subBtn]);
            }
            if(objForm.csscript !== undefined){
                form.clientScriptFileId = objForm.csscript;
            }
            return form;
        },
        
        insertLink: insertLink,
        
        assignMapValues : function(){
            fieldTypeMap = {
                checkbox    : ui.FieldType.CHECKBOX,
                currency    : ui.FieldType.CURRENCY,
                date        : ui.FieldType.DATE,
                datetimetz  : ui.FieldType.DATETIMETZ,
                email       : ui.FieldType.EMAIL,
                file        : ui.FieldType.FILE,
                float       : ui.FieldType.FLOAT,
                help        : ui.FieldType.HELP,
                inlinehtml  : ui.FieldType.INLINEHTML,
                integer     : ui.FieldType.INTEGER,
                image       : ui.FieldType.IMAGE,
                label       : ui.FieldType.LABEL,
                longtext    : ui.FieldType.LONGTEXT,
                multiselect : ui.FieldType.MULTISELECT,
                passport    : ui.FieldType.PASSPORT,
                percent     : ui.FieldType.PERCENT,
                phone       : ui.FieldType.PHONE,
                select      : ui.FieldType.SELECT,
                radio       : ui.FieldType.RADIO,
                richtext    : ui.FieldType.RICHTEXT,
                text        : ui.FieldType.TEXT,
                textarea    : ui.FieldType.TEXTAREA,
                timeofday   : ui.FieldType.TIMEOFDAY,
                url         : ui.FieldType.URL
            };
            layoutTypeMap = {
                endrow          : ui.FieldLayoutType.ENDROW,
                normal          : ui.FieldLayoutType.NORMAL,
                midrow          : ui.FieldLayoutType.MIDROW,
                outside         : ui.FieldLayoutType.OUTSIDE,
                outsidebelow    : ui.FieldLayoutType.OUTSIDEBELOW,
                outsideabove    : ui.FieldLayoutType.OUTSIDEABOVE,
                startrow        : ui.FieldLayoutType.STARTROW,

            };
            breakTypeMap = {
                none        : ui.FieldBreakType.NONE,
                startcol    : ui.FieldBreakType.STARTCOL,
                startrow    : ui.FieldBreakType.STARTROW
            };
            displayType = {
                disabled    : ui.FieldDisplayType.DISABLED,
                entry       : ui.FieldDisplayType.ENTRY,
                hidden      : ui.FieldDisplayType.HIDDEN,
                inline      : ui.FieldDisplayType.INLINE,
                normal      : ui.FieldDisplayType.NORMAL,
                readonly    : ui.FieldDisplayType.READONLY
            };
            subListType = ui.SublistType;
            propertyMap = {
                value       : 'defaultValue',
                mandatory   : 'isMandatory',
                linkText    : 'linkText'
            };
        }
    };
});