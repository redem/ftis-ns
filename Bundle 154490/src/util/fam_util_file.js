/**
 * © 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 * @NScriptName FAM File Utility
 * @NScriptId _fam_util_file
 * @NApiVersion 2.x
*/

define(['../adapter/fam_adapter_search'],

function (search){
    function getFileId(filename){
        if (!filename) {
            return null;
        }
        
        var searchObj, res;
        
        searchObj = search.create({
            type    : 'file',
            filters : [['name', 'is', filename]],
            columns : ['internalid']
        });
        res = searchObj.run().getRange(0,2);
        
        if(res.length){
            if(res.length>1){
                log.audit('getFileId', 'Found more than 1 file with filename: ' + filename + '. Returning first file found');
            }
            
            return res[0].getValue('internalid');
        }
        else{
            log.audit('getFileId', 'File: ' + filename + ' not found.');
            return null;
        }
    }
    
    return {
        getFileId : getFileId
    };
});