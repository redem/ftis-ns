/**
 * © 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 * 
*/

define(['../adapter/fam_adapter_record',
        './fam_util_record'],
function (record, fRecUtil){
    return {
        getPrecision : function getPrecision(currencyId) {
            if (fRecUtil.isRecordExisting('currency', currencyId)){
                var currency = record.load({ type : 'currency', id : currencyId });
                return currency.getValue({fieldId : 'currencyprecision' });
            }
            return 2;
            
        }
    };
});