/**
 * � 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */


var FAM;
if (!FAM) { FAM = {}; }

FAM.BulkTransfer_CS = new function () {
    /**
     * pageInit event type of client scripts
    **/
    this.pageInit = function (type) {   
        this.alertMessage = FAM.Util_CS.fetchMessageObj({
            ALERT_BULKBGPEXISTS : 'client_assettransfer_bulkbgpexists',
        });
    };
    /**
     * saveRecord event type of client scripts
    **/
    this.saveRecord = function () {
        // Check if an active Bulk Asset Transfer BGP instance exists
        if(FAM.Util_CS.hasExistingBulkTransfer()){
            alert(this.alertMessage.ALERT_BULKBGPEXISTS);
            return false;
        }
        return true;
    };
};