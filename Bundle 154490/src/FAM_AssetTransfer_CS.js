/**
 * © 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var FAM;
if (!FAM) { FAM = {}; }

FAM.AssetTransfer_CS = new function () {
    this.alertMessage = {};
    this.isAssetCompound = false;
    
    /**
     * pageInit event type of client scripts
    **/
    this.pageInit = function (type) {
        this.alertMessage = FAM.Util_CS.fetchMessageObj({
            EXTJS_MESSAGE_TITLE : 'extjs_message_title',
            EXTJS_PROCESSING : 'extjs_processing',
            EXTJS_READING_ASSET : 'extjs_reading_asset',
            EXTJS_CLEARING_DATA : 'extjs_clearing_data',
            EXTJS_LOADING_DATA : 'extjs_loading_data',
            EXTJS_REFRESHING_DATA : 'extjs_refreshing_data',
            ALERT_SUBSIDIARYBOOKID : 'client_assettransfer_subsidiarybookid',
            ALERT_SUBSIDIARYMETHOD : 'client_assettransfer_subsidiarymethod',
            ALERT_EMPTYSUBATFIELD : 'client_assettransfer_emptysubat',
            ALERT_NOCHANGE : 'client_assettransfer_nochange',
            ALERT_NOTRANSFERACCT : 'client_assettransfer_notransferacct',
            ALERT_BGPEXISTS : 'client_assettransfer_bgpexists',
            ALERT_SUBDEPT : 'client_assettransfer_subdept_notmatch',
            ALERT_SUBLOC : 'client_assettransfer_subloc_notmatch',
            ALERT_SUBCLASS : 'client_assettransfer_subclass_notmatch',
            ALERT_AT_ASSETACCT : 'client_assettransfer_atassetacct',
            ALERT_ASSETACCT_INACTIVE : 'client_assettransfer_atassetacct_inactive',
            ALERT_AT_DEPRACCT : 'client_assettransfer_atdepracct',
            ALERT_DEPRACCT_INACTIVE : 'client_assettransfer_atdepracct_inactive',
            ALERT_AT_CDL_MAND : 'client_assettransfer_cdl_mandatory',
            ALERT_BULKBGPEXISTS : 'client_assettransfer_bulkbgpexists',
            ALERT_WARNBGPEXISTS : 'client_assettransfer_warnbgpexists',
            ALERT_COMPONENTSELECTED : 'client_assettransfer_componentselected'
        });
        
        if (!nlapiGetFieldValue('custpage_assetid')) {
            this.disableFields(true);
        };
        
        // Check if an active Bulk Asset Transfer BGP instance exists
        if(FAM.Util_CS.hasExistingBulkTransfer()){
            alert(this.alertMessage.ALERT_WARNBGPEXISTS);
        }
    };

    /**
     * saveRecord event type of client scripts
    **/
    this.saveRecord = function () {
        var assetId = nlapiGetFieldValue('custpage_assetid'),
            excludeCompound = true, obj = {}, oldVal = {}, newVal = {};
        
        newVal.type = nlapiGetFieldValue('custpage_newassettype'),
        newVal.dept = nlapiGetFieldValue('custpage_newdepartment'),
        newVal.cls = nlapiGetFieldValue('custpage_newclass'),
        newVal.loc = nlapiGetFieldValue('custpage_newlocation'),
        newVal.sub = nlapiGetFieldValue('custpage_newsubsidiary');
        oldVal.type = nlapiGetFieldValue('custpage_assettype');
        oldVal.dept = nlapiGetFieldValue('custpage_assetdepartment'),
        oldVal.cls = nlapiGetFieldValue('custpage_assetclass'),
        oldVal.loc = nlapiGetFieldValue('custpage_assetlocation'),
        oldVal.sub = nlapiGetFieldValue('custpage_assetsubsidiary');
        
        //Asset Type must not be null
        if (!newVal.type) {
            alert(this.alertMessage.ALERT_EMPTYSUBATFIELD);
            return false;
        }
        
        // Check if all values are the same as the original
        if (!this.changedField('custpage_newsubsidiary') &&
            !this.changedField('custpage_newassettype') &&
            !this.changedField('custpage_newclass') &&
            !this.changedField('custpage_newdepartment') &&
            !this.changedField('custpage_newlocation')) {
            
            alert(this.alertMessage.ALERT_NOCHANGE);
            return false;
        }
        
        // Check if Asset Transfer BGP instance for this asset already exists
        if (this.isAssetToBeTransferred(assetId)) {
            alert(this.alertMessage.ALERT_BGPEXISTS);
            return false;
        }
        
        // Check if an active Bulk Asset Transfer BGP instance exists
        if(FAM.Util_CS.hasExistingBulkTransfer()){
            alert(this.alertMessage.ALERT_BULKBGPEXISTS);
            return false;
        }
        
        // Check Asset Type Asset Account and Depreciation Account
        if (oldVal.type !== newVal.type) {
            var assetTypeAccounts = nlapiLookupField('customrecord_ncfar_assettype', newVal.type,
                ['custrecord_assettypeassetacc', 'custrecord_assettypedepracc',
                 'custrecord_assettypeassetacc.isinactive', 'custrecord_assettypedepracc.isinactive']);
            
            if (!assetTypeAccounts.custrecord_assettypeassetacc) {
                alert(this.alertMessage.ALERT_AT_ASSETACCT);
                return false;
            }
            
            if (assetTypeAccounts['custrecord_assettypeassetacc.isinactive'] === 'T') {
                alert(this.alertMessage.ALERT_ASSETACCT_INACTIVE);
                return false;
            }
            
            if (!assetTypeAccounts.custrecord_assettypedepracc) {
                alert(this.alertMessage.ALERT_AT_DEPRACCT);
                return false;
            }
            
            if (assetTypeAccounts['custrecord_assettypedepracc.isinactive'] === 'T') {
                alert(this.alertMessage.ALERT_DEPRACCT_INACTIVE);
                return false;
            }
        }
        
        // Check normal asset (includes main compound asset)
        if (!this.validateFields(assetId, oldVal, newVal, this.isAssetCompound)) {
            return false;
        }
        
        // Check compound assets
        if (this.isAssetCompound) {
            var arrParams = [
                'name',
                'altname',
                'custrecord_assettype',
                'custrecord_assetdepartment',
                'custrecord_assetclass',
                'custrecord_assetlocation',
                'custrecord_assetsubsidiary'];
            obj = FAM.Util_Shared.getDescendants([assetId],
                arrParams, null, excludeCompound);
            
            if (!this.validateComponents(obj, oldVal, newVal)) {
                return false;
            }
        }
        return true;
    };
    
    this.validateComponents = function (obj, oldVal, newVal) {
        var cmpVal = {}, name = '';
        for (key in obj) {
            var cmpKey = obj[key];
            cmpVal.type = cmpKey['custrecord_assettype'];
            cmpVal.dept = cmpKey['custrecord_assetdepartment'];
            cmpVal.cls = cmpKey['custrecord_assetclass'];
            cmpVal.loc = cmpKey['custrecord_assetlocation'];
            cmpVal.sub = cmpKey['custrecord_assetsubsidiary'];
            cmpVal.name = cmpKey['name'] +' '+ cmpKey['altname'];
            
            if (!this.validateFields(key, oldVal, newVal, !this.isAssetCompound, cmpVal)) {
                return false;
            }
        }
        return true;
    };
    
    this.addComponentName = function (cmpName, msg) {
        return cmpName ? cmpName +': '+ msg : msg;        
    };
    
    this.validateFields = function (assetId, oldVal, newVal, checkCompound, cmpVal) {
        var bookIds,
            cmpName = cmpVal ? cmpVal.name : '';
            isDeptMandatory = FAM.Context.getPreference('deptmandatory') === 'T',
            isClassMandatory = FAM.Context.getPreference('classmandatory') === 'T',
            isLocMandatory = FAM.Context.getPreference('locmandatory') === 'T';
        
        if (FAM.Context.blnOneWorld) {
            //Subsidiary must not be null
            if (!newVal.sub) {
                alert(this.addComponentName(cmpName, this.alertMessage.ALERT_EMPTYSUBATFIELD));
                return false;
            }
            
            //Check C/D/L + Subsidiary Combination Validation
            //if component asset
            if (cmpVal) {
                if ((!cmpVal.dept && isDeptMandatory)||
                    (!cmpVal.cls && isClassMandatory)||
                    (!cmpVal.loc && isLocMandatory)){
                    
                    alert(this.addComponentName(cmpName, this.alertMessage.ALERT_AT_CDL_MAND));
                    return false;
                }
                
                var sub = (oldVal.sub !== newVal.sub) ? newVal.sub : cmpVal.sub,
                    dept = (newVal.dept && oldVal.dept !== newVal.dept) ? newVal.dept : cmpVal.dept,
                    cls = (newVal.cls && oldVal.cls !== newVal.cls) ? newVal.cls : cmpVal.cls,
                    loc = (newVal.loc && oldVal.loc !== newVal.loc) ? newVal.loc : cmpVal.loc;

                if (dept && !FAM.compareCDLSubsidiary('department', dept, sub)) {
                    alert(this.addComponentName(cmpName, this.alertMessage.ALERT_SUBDEPT));
                    return false;
                }
                
                if (cls && !FAM.compareCDLSubsidiary('classification', cls, sub)) {
                    alert(this.addComponentName(cmpName, this.alertMessage.ALERT_SUBCLASS));
                    return false;
                }
                
                if (loc && !FAM.compareCDLSubsidiary('location', loc, sub)) {
                    alert(this.addComponentName(cmpName, this.alertMessage.ALERT_SUBLOC));
                    return false;
                }
            }
            else {
                //if compound asset
                if (checkCompound) {
                    if ((oldVal.dept !== newVal.dept) && !newVal.dept && isDeptMandatory) {
                        alert(this.alertMessage.ALERT_AT_CDL_MAND);
                        return false;
                    }
                    
                    if ((oldVal.cls !== newVal.cls) && !newVal.cls && isClassMandatory) {
                        alert(this.alertMessage.ALERT_AT_CDL_MAND);
                        return false;
                    }
                    
                    if ((oldVal.loc !== newVal.loc) && !newVal.loc && isLocMandatory) {
                        alert(this.alertMessage.ALERT_AT_CDL_MAND);
                        return false;
                    }
                }
                //if simple asset
                else {
                    if ((!newVal.dept && isDeptMandatory)||
                       (!newVal.cls && isClassMandatory)||
                       (!newVal.loc && isLocMandatory)){
                        alert(this.alertMessage.ALERT_AT_CDL_MAND);
                        return false;
                    }
                }
                
                if ((oldVal.sub !== newVal.sub || newVal.dept !== oldVal.dept) && newVal.dept &&
                    !FAM.compareCDLSubsidiary('department', newVal.dept, newVal.sub)) {
                    
                    alert(this.alertMessage.ALERT_SUBDEPT);
                    return false;
                }
                
                if ((oldVal.sub !== newVal.sub || newVal.cls !== oldVal.cls) && newVal.cls &&
                    !FAM.compareCDLSubsidiary('classification', newVal.cls, newVal.sub)) {
                      
                    alert(this.alertMessage.ALERT_SUBCLASS);
                    return false;
                }
                
                if ((oldVal.sub !== newVal.sub || newVal.loc !== oldVal.loc) && newVal.loc &&
                    !FAM.compareCDLSubsidiary('location', newVal.loc, newVal.sub)) {
                       
                    alert(this.alertMessage.ALERT_SUBLOC);
                    return false;
                }
            }
            
            if (oldVal.sub !== newVal.sub) {
                //Check if Transfer Account exists
                if (!FAM.Util_Shared.getTransferAccounts(oldVal.sub, newVal.sub)) {
                    alert(this.alertMessage.ALERT_NOTRANSFERACCT);
                    return false;
                }
                
                //Check Subsidiary + Accounting Books combination
                bookIds = FAM.Util_Shared.getTaxMethodBookIds(assetId);
                if (!FAM.Util_Shared.isValidSubsidiaryBooks(newVal.sub, bookIds)) {
                    alert(this.addComponentName(cmpName, this.alertMessage.ALERT_SUBSIDIARYBOOKID));
                    return false;
                }
                
                //Check Subsidiary + Alternate Methods combination
                if (!FAM.Util_Shared.compatibleSubAndMethods(assetId, newVal.sub)) {
                    alert(this.addComponentName(cmpName, this.alertMessage.ALERT_SUBSIDIARYMETHOD));
                    return false;
                }
            }
        }
        return true;
    };
    
    /**
     * fieldChanged event type of client scripts
    **/
    this.fieldChanged = function (type, value) {
        var fld, disableSCDL, disableAT, dispField, progress, fldCount, pBreakdown, assetId, assetRec;
        
        if (value == 'custpage_assetid') {
            try {
                progress = 0;
                pBreakdown = { loading : 0.1, reading : 0.8, refresh : 0.1 };
                dispField = {
                    'custpage_assettype'        : 'custrecord_assettype',
                    'custpage_assetsubsidiary'  : 'custrecord_assetsubsidiary',
                    'custpage_assetclass'       : 'custrecord_assetclass',
                    'custpage_assetdepartment'  : 'custrecord_assetdepartment',
                    'custpage_assetlocation'    : 'custrecord_assetlocation',
                    'custpage_assetdescription' : 'custrecord_assetdescr',
                    'custpage_assetserialno'    : 'custrecord_assetserialno',
                    'custpage_assetalternateno' : 'custrecord_assetalternateno',
                    'custpage_assetorigcost'    : 'custrecord_assetcost',
                    'custpage_assetcurrcost'    : 'custrecord_assetcurrentcost',
                    'custpage_assetlifetime'    : 'custrecord_assetlifetime',
                    'custpage_assetlastdepr'    : 'custrecord_assetcurrentage',
                    'custpage_assetstatus'      : 'custrecord_assetstatus',
                    'custpage_assetcurrency'    : 'custrecord_assetcurrency',
                    'custpage_newassettype'     : 'custrecord_assettype',
                    'custpage_newsubsidiary'    : 'custrecord_assetsubsidiary',
                    'custpage_newclass'         : 'custrecord_assetclass',
                    'custpage_newdepartment'    : 'custrecord_assetdepartment',
                    'custpage_newlocation'      : 'custrecord_assetlocation'
                };
                
                fldCount = Object.keys(dispField).length;
                
                Ext.MessageBox.minWidth = 150;
                Ext.MessageBox.show({
                    title : this.alertMessage.EXTJS_MESSAGE_TITLE,
                    msg : this.alertMessage.EXTJS_PROCESSING,
                    progress : true
                });
                
                Ext.MessageBox.updateProgress(progress, this.alertMessage.EXTJS_CLEARING_DATA);
                
                for (fld in dispField) {
                    nlapiSetFieldValue(fld, '', false);
                }
                
                progress += pBreakdown.loading;
                Ext.MessageBox.updateProgress(progress, this.alertMessage.EXTJS_LOADING_DATA);

                assetId = nlapiGetFieldValue('custpage_assetid');
                if (assetId) {
                    assetRec = nlapiLoadRecord('customrecord_ncfar_asset', assetId);
                    var isComponent = !FAM.Util_Shared.isNullUndefinedOrEmpty(assetRec.getFieldValue('custrecord_componentof'));
                    
                    for (fld in dispField) {
                        progress += (pBreakdown.reading / fldCount);
                        Ext.MessageBox.updateProgress(progress, this.alertMessage.EXTJS_READING_ASSET);
                        nlapiSetFieldValue(fld, assetRec.getFieldValue(dispField[fld]), false);
                    }
                    
                    if(isComponent){
                        alert(this.alertMessage.ALERT_COMPONENTSELECTED);
                    }
                    this.isAssetCompound = FAM.Util_Shared.isAssetCompound(assetId);
                    this.disableFields(false, isComponent);
                    
                }
                else {
                    this.disableFields(true);
                }
                
                Ext.MessageBox.updateProgress(progress + pBreakdown.refresh,
                    this.alertMessage.EXTJS_REFRESHING_DATA);
                Ext.MessageBox.hide();
                
            }
            catch (e) {
                this.disableFields(true);
                nlapiLogExecution('ERROR', 'Field Change', e.toString());
                Ext.MessageBox.hide();
                throw e;
            }
        }
        else if (value == 'custpage_newsubsidiary' ||
                value == 'custpage_newclass' ||
                value == 'custpage_newdepartment' ||
                value == 'custpage_newslocation') {
                   
            disableAT = this.changedField('custpage_newsubsidiary') ||
                        this.changedField('custpage_newclass') ||
                        this.changedField('custpage_newdepartment') ||
                        this.changedField('custpage_newlocation');
            
            nlapiDisableField('custpage_newassettype', disableAT);
        }
        else if (value == 'custpage_newassettype') {
            disableSCDL = this.changedField('custpage_newassettype');
            this.disableSCDLFields(disableSCDL);
        }
    };

    this.disableFields = function (disable, isComponent) {
        if(isComponent){
            nlapiDisableField('custpage_newassettype', true);
            nlapiDisableField('custpage_newsubsidiary', true);
        }else{
            nlapiDisableField('custpage_newassettype', disable);
            nlapiDisableField('custpage_newsubsidiary', disable);
        }
        
        nlapiDisableField('custpage_newclass', disable);
        nlapiDisableField('custpage_newdepartment', disable);
        nlapiDisableField('custpage_newlocation', disable);
    };

    this.disableSCDLFields = function (disable) {
        nlapiDisableField('custpage_newsubsidiary', disable);
        nlapiDisableField('custpage_newclass', disable);
        nlapiDisableField('custpage_newdepartment', disable);
        nlapiDisableField('custpage_newlocation', disable);
    };
    
    /**
     * If SCDL are all empty, enable the Asset Type dropdown field.
     * Otherwise, disable the Asset Type dropdown.
     *
     * Parameters:
     *     none
     * Returns:
     *     null
    **/
    this.toggleAssetTypeField = function () {
        var newSubsidiary = nlapiGetFieldValue('custpage_newsubsidiary'),
            newClass = nlapiGetFieldValue('custpage_newclass'),
            newDepartment = nlapiGetFieldValue('custpage_newdepartment'),
            newLocation = nlapiGetFieldValue('custpage_newlocation'),
            disableFlag = newSubsidiary || newClass || newDepartment || newLocation ? true : false;
            
        nlapiDisableField('custpage_newassettype', disableFlag);
    };
    
    /**
     * Checks if there is a BG - Process Instance record where Process Name: 'Asset Transfer',
     * Process Status: 'Queued' or 'In Progress' and the Asset Id (from Process State) is the Asset
     * Id on the form.
     *
     * Parameters:
     *     assetId {number} - internal id of the asset     
     * Returns:
     *     boolean
    **/
    this.isAssetToBeTransferred = function (assetId) {
        if (!assetId) {
            return false;
        }

        var fil = [
            new nlobjSearchFilter('custrecord_far_proins_functionname', null, 'is',
                'famTransferAsset'),
            new nlobjSearchFilter('custrecord_far_proins_recordid', null, 'equalto', assetId),
            new nlobjSearchFilter('custrecord_far_proins_procstatus', null, 'anyof',
                [FAM.BGProcessStatus.InProgress, FAM.BGProcessStatus.Queued]),
            new nlobjSearchFilter('isinactive', null, 'is', 'F')
        ];

        if (nlapiSearchRecord('customrecord_bg_procinstance', null, fil)) {
            return true;
        }

        return false;
    };
    
    this.changedField = function (fld) {
        var fieldMap = {'custpage_newassettype'     : 'custpage_assettype',
                        'custpage_newsubsidiary'    : 'custpage_assetsubsidiary',
                        'custpage_newclass'         : 'custpage_assetclass',
                        'custpage_newdepartment'    : 'custpage_assetdepartment',
                        'custpage_newlocation'      : 'custpage_assetlocation'};
        return nlapiGetFieldValue(fld) != nlapiGetFieldValue(fieldMap[fld]);
    };
};