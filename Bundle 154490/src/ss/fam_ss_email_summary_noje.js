/**
 * Copyright © 2017, 2017 Oracle and/or its affiliates. All rights reserved.
 *
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 */

define([
    'N/email',
    'N/search',
    '../util/fam_util_translator',
], 
function emailSummaryWithoutJE(email, search, utilTranslator) {
    
    function execute(context) {
        if (hasSummaryWithoutJE()){
            var recipients = getRecipients();
            
            if (recipients.length > 0){
                var sender = recipients[0];
                
                try{
                    if (hasJournalWritingBGP()){
                        log.audit('Will not send Summary without JE email', 'Has on-going Journal Writing BGP');
                        return;
                    }
                    email.send({
                        author: sender,
                        recipients: recipients,
                        subject: utilTranslator.getString('email_summary_noje_subject', 'summarywithnoje'), 
                        body: utilTranslator.getString('email_summary_noje_body', 'summarywithnoje') 
                    });
                    
                    log.audit('Summary without JE email sent', 'Final recipients: ' + recipients);
                }
                catch(e){
                    log.error('Unable to send Summary without JE email', e);
                }
            }
            else{
                log.error('Unable to send Summary without JE email', 'No active admin employee');
            }
        }
        else{
            log.audit('Will not send Summary without JE email', 'No Summaries without JE');
        }
    }    
    
    function hasSummaryWithoutJE(searchObj){
        var searchObj = search.load('customsearch_fam_summarytowrite'),
            pagedData = searchObj.runPaged({ pageSize : 1000 });
        
        return pagedData.count > 0;
    }
    
    function getRecipients(){
        var accountAdmins = [],
            recipients = [];

        var searchObj = search.load('customsearch_fam_admin_email_17_1'),       
            pagedData = searchObj.runPaged({ pageSize : 1000 });
        
        pagedData.pageRanges.forEach(function (pageRange) {
            var page = pagedData.fetch({ index : pageRange.index });
            page.data.forEach(function(searchRes) {
                accountAdmins.push({
                    id : searchRes.id,
                    email : searchRes.getValue('email')
                });
            });
        });
        
        accountAdmins = accountAdmins.slice(0,10);
        log.debug('recipients', accountAdmins);
        
        // Get only the IDs
        recipients = accountAdmins.map(function(value){return +value.id;});
        // Sort IDs
        recipients = recipients.sort(function sortNumber(a,b) {
            return a - b;
        });
        
        return recipients;
    }
    
    function hasJournalWritingBGP(){
        var filters = [
            search.createFilter({
                name: 'custrecord_far_proins_procstatus', operator: 'anyof', values: [1,5] // In-progress, Queued
            }),
            search.createFilter({
                name: 'custrecord_far_proins_functionname', operator: 'is', values: 'famJournalWriting'
            }),
            search.createFilter({
                name: 'isinactive', operator: 'is', values: false
            })
        ];
        var searchObj = search.create({
            type : 'customrecord_bg_procinstance',
            filters : filters,
            columns : 'internalid'
        });
        
        pagedData = searchObj.runPaged({ pageSize : 1000 });
        return pagedData.count > 0;
    }
    
    return {
        execute : execute
    };
});
