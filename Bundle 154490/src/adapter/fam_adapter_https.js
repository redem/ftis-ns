/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

define(["N/https"],
    famAdapterHttps);

function famAdapterHttps(https) {
    var module = {};
    module["SecureString"] = https.SecureString;
    module["ClientResponse"] = https.ClientResponse;
    module["ServerRequest"] = https.ServerRequest;
    module["ServerResponse"] = https.ServerResponse;
    
    module["createSecureKey"] = function(options) {
        return https.createSecureKey(options);
    };
    module["createSecureString"] = function(options) {
        return https.createSecureString(options);
    };
    module["delete"] = function(options) {
        return https.delete(options);
    };
    module["get"] = function(options) {
        return https.get(options);
    };
    module["post"] = function(options) {
        return https.post(options);
    };
    module["put"] = function(options) {
        return https.put(options);
    };
    module["request"] = function(options) {
        return https.request(options);
    };
    
    module["CacheDuration"] = https.CacheDuration;
    module["Method"] = https.Method;
    module["Encoding"] = https.Encoding;
    return module;
};