/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

define(["N/task",
        ],
    famAdapterTask);

function famAdapterTask(task) {
    
    return {
        TaskType : task.TaskType,
        create : function(options) {
            return task.create(options);
        }
    }
}