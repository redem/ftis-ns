/**
 * @NModuleScope Public
 */

define(["N/config"],
    famAdapterConfig);

function famAdapterConfig(config) {
    
    return {
        Type : config.Type,
        load : function(options) {
            return config.load(options);
        }
    }
}