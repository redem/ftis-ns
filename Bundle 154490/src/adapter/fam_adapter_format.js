/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

define(["N/format"],
    famAdapterFormat);

function famAdapterFormat(format) {
    
    return {
        Type  : format.Type,
        parse : function(options) {
            return format.parse(options);
        },
        format : function(options) {
            return format.format(options);
        }
    }
}