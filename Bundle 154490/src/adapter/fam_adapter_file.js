/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

define(["N/file"],
    famAdapterFile);

function famAdapterFile(file) {
    
    return {
        Type : file.Type,
        load : function(options) {
            return file.load(options);
        }
    }
}