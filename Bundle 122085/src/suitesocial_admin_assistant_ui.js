// object for storing global variables
var ssobjGlobal = ssobjGlobal || {};
ssobjGlobal.module = 'Assistant > client code';

var JSON = JSON || {};
// // implement JSON.stringify serialization
JSON.stringify = JSON.stringify || Ext.encode;
JSON.parse = JSON.parse || Ext.decode;

function uiConfirmCreateDefaultAutoSubscribe(btn) {
    // Issue: 214566 [Suitesocial] assistant > enable record > autosubscrib
    if (btn == 'no') {
        return;
    }
    ssapiWait();
    var message = uiSuiteletProcess('createdefaultautosubtypes', 'dummy');

    if (message.plainText === '') {
        // no error message
        window.location.reload();
    } else {
        Ext.Msg.hide();
        // for chrome, display plain text (bug info
        // http://productforums.google.com/forum/#!category-topic/chrome/report-a-problem-and-get-troubleshooting-help/EjAWhXJk0fY)
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if (is_chrome) {
            alert(message.plainText);
            ssapiWait();
            window.location.reload();
        } else {
            // display intial message with option to display details
            Ext.Msg.show({
                title : 'SuiteSocial'.tl(),
                msg : message.html,
                buttons : Ext.MessageBox.OK,
                minWidth : 480,
                icon : Ext.MessageBox.INFO,
                fn : function() {
                    ssapiWait();
                    window.location.reload();
                }
            });
        }
    }
}

function uiCreateDefaultAutoSubscribe() {

    var results = retrieveAllDefaultAutoSubTypes();// retrieveResults(REC_DEFAULT_AUTOSUB_TYPE,
    // [FLD_INTERNAL_ID], null);
    if (results === null) {
        ssapiWait();
        // confirm
        var defaults = [];
        var recordName = uiSuiteletProcess('ssapiGetRecordNameById', '-304') /* Case */;
        var item = {};
        item['Record Subscription Name'.tl()] = 'My'.tl() + ' ' + ssapiPluralize(recordName);
        item['Record'.tl()] = recordName;
        item['Field'.tl()] = 'Assigned'.tl();
        item['Role'.tl()] = 'Support Person'.tl();
        defaults.push(item);

        recordName = uiSuiteletProcess('ssapiGetRecordNameById', '-203') /* Customer */;
        item = {};
        item['Record Subscription Name'.tl()] = 'My'.tl() + ' ' + ssapiPluralize(recordName);
        item['Record'.tl()] = recordName;
        item['Field'.tl()] = 'Sales Rep'.tl();
        item['Role'.tl()] = 'Sales Person'.tl();
        defaults.push(item);

        recordName = uiSuiteletProcess('ssapiGetRecordNameById', '-308') /* Issue */;
        item = {};
        item['Record Subscription Name'.tl()] = 'My'.tl() + ' ' + ssapiPluralize(recordName);
        item['Record'.tl()] = recordName;
        item['Field'.tl()] = 'Assigned'.tl();
        item['Role'.tl()] = 'Support Person'.tl();
        defaults.push(item);

        recordName = uiSuiteletProcess('ssapiGetRecordNameById', '-204') /* Lead */;
        item = {};
        item['Record Subscription Name'.tl()] = 'My'.tl() + ' ' + ssapiPluralize(recordName);
        item['Record'.tl()] = recordName;
        item['Field'.tl()] = 'Sales Rep'.tl();
        item['Role'.tl()] = 'Sales Person'.tl();
        defaults.push(item);

        recordName = uiSuiteletProcess('ssapiGetRecordNameById', '-101') /* Opportunity */;
        item = {};
        item['Record Subscription Name'.tl()] = 'My'.tl() + ' ' + ssapiPluralize(recordName);
        item['Record'.tl()] = recordName;
        item['Field'.tl()] = 'Sales Rep'.tl();
        item['Role'.tl()] = 'Sales Person'.tl();
        defaults.push(item);

        var question = 'SuiteSocial can automate the saving of the following Default Record Subscriptions.'.tl();
        question += ' Do you want to save them?'.tl();
        question = '<span style="font: normal 14px Open Sans, Helvetica, sans-serif;">' + question + '</span>';
        question += '<br />&nbsp;<br />&nbsp;';
        question += createTableHtml(defaults);
        Ext.Msg.show({
            title : 'SuiteSocial',
            msg : question,
            buttons : {
                yes : 'Yes'.tl(),
                no : 'No'.tl()
            },
            icon : Ext.MessageBox.QUESTION,
            fn : uiConfirmCreateDefaultAutoSubscribe,
            minWidth : 600
        });
        // HACK: center buttons
        Ext.select('.x-window-br').setStyle({
            'padding-left' : '17px'
        });
    }
}

//
// /*
// * this needs to be implemented in the client since doing it in server causes
// usage limit error
// */
// function displayRecordLineItemSummary(){
// ssapiWait('Getting summary...');
// var logger = new ssobjLogger('hi');
// for (var u = 1; u <= nlapiGetLineItemCount(REC_RECORD); u++) {
// ssapiWait('Getting summary for line item ' + u + '...' + firefoxMsg);
// var context = nlapiGetContext();
// logger.log('getRemainingUsage=' + context.getRemainingUsage());
// scriptedRecordId = nlapiGetLineItemValue(REC_RECORD, FLD_INTERNAL_ID, u)
// // scriptedRecordId = scriptedRecordId * -1;
// logger.log('scriptedRecordId=' + scriptedRecordId);
// var summary = '';
// // tracked fields
// var countResults = retrieveResults(REC_AUTOPOST_FIELD, [FLD_INTERNAL_ID],
// [new nlobjSearchFilter(FLD_AUTOPOST_FLD_REC, null, OP_ANYOF,
// scriptedRecordId)])
// if (countResults != null) {
// summary = 'Tracked Fields: ' + countResults.length.toString();
// }
// else {
// summary = 'Tracked Fields: 0';
// }
//        
// // autosub types
// var countResults = retrieveResults(REC_AUTOSUB_TYPE, [FLD_INTERNAL_ID], [new
// nlobjSearchFilter(FLD_AUTOSUB_TYPE_REC, null, OP_ANYOF, scriptedRecordId)])
// if (countResults != null) {
// summary += ' AutoSub Types: ' + countResults.length.toString();
// }
// else {
// summary += ' AutoSub Types: 0';
// }
//        
// // Scheduled Posts
// var countResults = retrieveResults(REC_AUTOPOST, [FLD_INTERNAL_ID], [new
// nlobjSearchFilter(FLD_AUTOPOST_RECORD, null, OP_ANYOF, scriptedRecordId)])
// if (countResults != null) {
// summary += ' Scheduled Posts: ' + countResults.length.toString();
// }
// else {
// summary += ' Scheduled Posts: 0';
// }
// logger.log(summary);
// nlapiSetLineItemValue(REC_RECORD, 'summary', u, summary);
// }
// nlapiRefreshLineItems(REC_RECORD);
// nlapiSelectLineItem(REC_RECORD, 1);
//    
// Ext.Msg.hide();
// }

// Issue: 212985 [SuiteSocial] Confirm deletions in assistant sublists
// batchDeletion is changed to true when doing batch delete such as when
// changing the value in the role dropdown
var batchDeletion = false;
/**
 * @param {String}
 *        type Sublist internal id
 * @return {Boolean} True to continue line item delete, false to abort delete
 */
function validateDelete(type) {
    var logger = new ssobjLogger(arguments);
    // not working on assistant sublist
    // alert(nlapiGetCurrentLineItemIndex(type))
    // alert('validateDelete type='+type);
    // return false;
    if (batchDeletion) {
        return true;
    }

    if (confirm('Continue with delete'.tl() + '?') === false) {
        return false;
    }

    if (nlapiGetFieldValue('step') == 'recordtypestep' && nlapiGetFieldValue('nosavedrecords') == 'T') {
        // if there are no SS records yet, no need to save
        return true;
    }

    if (nlapiGetFieldValue('step') == 'permissionstep') {
        // since permissions are saved in a different way
        return true;
    }

    // Issue: 217052 [SuiteSocial] For steps 'Enable Channels' and 'Enable
    // check if there are pending changes
    if (window.NS.form.isChanged()) {
        alert('Pending changes will be saved before the deletion.'.tl());

        ssobjGlobal.doNotAcceptLastEdit = true;
        // try saving existing rows.
        if (uiSave() === false) {
            ssobjGlobal.doNotAcceptLastEdit = null;
            return false;
        }
        ssobjGlobal.doNotAcceptLastEdit = null;

    }

    ssapiWait('Checking for dependent records'.tl() + '...');
    // note that at this point, the record is not yet deleted in the sublist,
    // so submit another save but do it after a certain time so that it will be
    // called after the row is removed from the sublist
    setTimeout('uiSave();', 500);
    return true;
}

var gRoleOptions = [];
function uiAddAllRoles() {
    // var logger = new ssobjLogger('uiAddAllRoles');
    // uiClearSublist('permission');
    //    
    // var roleStr = nlapiGetFieldValue('role');
    // var options = JSON.parse(roleStr);
    // gRoleOptions = options;

    // Issue: 213206 [Suite Social]Assistant > Review Permission > enable
    NS.form.setChanged(false);
    window.location.href = window.location.href + '&all=T';
    // setInterval('addPermissionAsync2()', 500);
    // addPermissionAsync2(options);
    //  
    return;
    // alert('Your browser might freeze for about 20 seconds. Just wait for the
    // process to complete.');
    // ssapiWait();
    // var options = fld.getSelectOptions();
    // var addinterval = 500;
    // for (var i = 0; i < options.length; i++) {
    // logger.log(parseInt(i + 1, 10) * addinterval)
    // var timeout = parseInt(i + 1, 10) * addinterval
    // setTimeout('addPermissionAsync(' + options[i].id + ')', timeout);
    // // nlapiSelectNewLineItem('permission');
    // // nlapiSetCurrentLineItemValue('permission', 'id', options[i].id, false,
    // false);
    // // nlapiCommitLineItem('permission');
    // }
    // Ext.Msg.hide();
}

//
// function addPermissionAsync(id){
// nlapiSelectNewLineItem('permission');
// nlapiSetCurrentLineItemValue('permission', 'id', id, false, true);
// nlapiCommitLineItem('permission');
// }
//
// function addPermissionAsync2(){
// if(gRoleOptions.length === 0){
// return;
// }
//  
// //alert('');
// var el = gRoleOptions.pop();
// var id = el.id;
//  
// nlapiSelectNewLineItem('permission');
// //nlapiSetCurrentLineItemValue('permission', 'id', id, false, false);
// nlapiSetCurrentLineItemText('permission', 'id', el.name, false, true);
// nlapiCommitLineItem('permission');
//  
// // addPermissionAsync2(options)
// }

function uiEditDefaultAutoSubType(internalId) {
    // alert(internalId)
    uiOpenPopup(internalId, REC_AUTOSUB_TYPE);
    return;

    // Ext.Msg.wait('...', 'Please wait');
    // var url = nlapiResolveURL('RECORD', REC_DEFAULT_AUTOSUB_TYPE, internalId,
    // 'edit');
    // url += "&popup=T";
    // var width = screen.width - 200;
    // var height = screen.height - 300;
    // var left = (screen.width - width) / 2;
    // var top = (screen.height - height) / 2;
    // var params = 'width=' + width + ', height=' + height;
    // params += ', top=' + top + ', left=' + left;
    // params += ', directories=no';
    // // params += ', location=no';
    // params += ', menubar=no';
    // params += ', resizable=no';
    // params += ', scrollbars=no';
    // params += ', status=no';
    // params += ', toolbar=no';
    //    
    // window.open(url, "mywindow", params);
    // Ext.Msg.hide();
    //    
    //    
}

function uiDeleteDefaultAutoSubType(internalId) {
    if (confirm('Delete this record'.tl() + '?', 'Delete'.tl()) === false) {
        return;
    }

    ssapiWait();

    deleteAutoSubscribeType(internalId);
    // // delete roles
    // var roleResults = retrieveDefaultAutoSubTypeRoles(internalId);
    // if (roleResults !== null) {
    // for (var i = 0; i < roleResults.length; i++) {
    // nlapiDeleteRecord(REC_DEFAULT_AUTOSUB_ROLE,
    // roleResults[i].getValue(FLD_INTERNAL_ID));
    // }
    //        
    // }
    // nlapiDeleteRecord(REC_DEFAULT_AUTOSUB_TYPE, internalId);
    window.location.reload();
}

function uiAddDefaultAutoSubType() {
    // Issue: 213593 [SuiteSocial] Create of autosubscribe type from assistant
    // st
    uiOpenPopup(null, REC_AUTOSUB_TYPE);
    //    
    // var url;
    // // if (recordId == REC_RECORD) {
    // // url = nlapiResolveURL('suitelet',
    // 'customscript_suitesocial_admin_record_sl',
    // 'customdeploy_suitesocial_admin_record_sl') + '&suiteSocialRecordTypeId='
    // + internalid;
    // // }
    // // else {
    // url = nlapiResolveURL('record', recordId, internalid, 'edit');
    // // }
    // url += '&popup=T';
    //    
    // var width = screen.width - 200;
    // var height = screen.height - 300;
    // var left = (screen.width - width) / 2;
    // var top = (screen.height - height) / 2;
    // var params = 'width=' + width + ', height=' + height;
    // params += ', top=' + top + ', left=' + left;
    // params += ', directories=no';
    // params += ', location=no';
    // params += ', menubar=no';
    // params += ', resizable=no';
    // params += ', scrollbars=no';
    // params += ', status=no';
    // params += ', toolbar=no';
    //    
    // ssapiWait('Opening popup window...');
    // // IN SS record popup for example, when a row is deleted, NetSuite
    // redirects to a listing instead of showing the form of the parent SS Form.
    // We track the original
    // // url so that when we detected the listing, we change the location to
    // this URL
    // originalUrl = url;
    // popupWindow = window.open(url, "mywindow", params);
    // ssapiWait('Screen is disabled until the popup window is closed.');
    // uiMonitorPopupIntervalId = setInterval('uiMonitorPopup();', 1000);

    // Ext.Msg.wait('...', 'Please wait');
    // var url = nlapiResolveURL('RECORD', REC_DEFAULT_AUTOSUB_TYPE, null,
    // 'edit');
    // url += "&popup=T";
    // var width = screen.width - 200;
    // var height = screen.height - 300;
    // var left = (screen.width - width) / 2;
    // var top = (screen.height - height) / 2;
    // var params = 'width=' + width + ', height=' + height;
    // params += ', top=' + top + ', left=' + left;
    // params += ', directories=no';
    // // params += ', location=no';
    // params += ', menubar=no';
    // params += ', resizable=no';
    // params += ', scrollbars=no';
    // params += ', status=no';
    // params += ', toolbar=no';
    //    
    // window.open(url, "mywindow", params);
    // Ext.Msg.hide();
}

function showCreateProfileStatus() {

    var el = document.getElementById('profile_progress');
    var logger = new ssobjLogger('showCreateProfileStatus');
    var counts = getPercentageOfProfileAndEmployee();
    logger.log('counts.profileCount=' + counts.profileCount);
    var bar = null;
    if (counts.profileCount == counts.employeeCount) {
        el.innerHTML = '<br />' + 'All profiles have been created.'.tl();
        bar = new Ext.ProgressBar({
            height : 20,
            width : 200,
            renderTo : el,
            value : (100 / 100)
        });
        return;
    }
    //
    var percentage = 100 * counts.profileCount / counts.employeeCount;
    percentage = parseInt(percentage, 10);
    logger.log('percentage=' + percentage);
    // var percentageText = counts.profileCount + ' of ' + counts.employeeCount
    // + ' employee profiles have been created';
    var innerHTML = '';
    // var innerHTML = '<br /><b>Creating profiles...</b><br />';
    // innerHTML += '<br />';
    // innerHTML += '<br />';
    // innerHTML += 'While the profiles are being created, you can minimize this
    // popup and proceed with other setup tasks.';
    // innerHTML += '<br />';
    // innerHTML += '<br />';
    // innerHTML += 'Status: ' + percentageText;
    innerHTML += '<span style="width: 300; text-align: center">' + 'Entities with Profile'.tl() + ': ' + percentage + '%</span>';
    // innerHTML += '<br />';
    el.innerHTML = innerHTML;
    bar = new Ext.ProgressBar({
        height : 20,
        width : 300,
        renderTo : el,
        value : (percentage / 100)
    });
}

function editChannel(channelId) {
    uiOpenPopup(channelId, REC_NEWSFEED_CHANNEL);
}

function editRecord(id) {
    uiOpenPopup(id, REC_RECORD);
}

function editAutoPostField(id) {
    uiOpenPopup(id, REC_AUTOPOST_FIELD);
}

function editAutoPost(id) {
    uiOpenPopup(id, REC_AUTOPOST);
}

function editSubscriptionLink(id) {
    uiOpenPopup(id, 'customrecord_ss_subscription_link');
}

function editAutoSubType(id) {
    uiOpenPopup(id, REC_AUTOSUB_TYPE);
}

function editSavedSearch(id) {
    uiOpenPopup(id, 'customrecord_ss_saved_search_subs');
}

function uiCreateLinks(sublistId, columnIndex) {
    var logger = new ssobjLogger(arguments, true);

    // if (sublistId == REC_RECORD) {
    // document.getElementById('customrecord_suitesocial_record_headerrow').children[4].innerHTML
    // = 'Delete';
    // document.getElementById('customrecord_suitesocial_record_headerrow').children[4].style.width
    // = '1000';
    // document.getElementById('customrecord_suitesocial_record_headerrow').children[0].style.width
    // = '300px';
    // document.getElementById('customrecord_suitesocial_record_headerrow').children[1].style.width
    // = '150px';
    // document.getElementById('customrecord_suitesocial_record_headerrow').children[2].style.width
    // = '600px';
    // document.getElementById('customrecord_suitesocial_record_headerrow').children[3].style.width
    // = '600px';
    // }

    var count = nlapiGetLineItemCount(sublistId);
    for (var line = 1; line <= count; line++) {
        var rowid = sublistId + '_row_' + line;
        var rowEl = document.getElementById(rowid);
        // var columnCount = rowEl.children.length;
        var internalid = nlapiGetLineItemValue(sublistId, FLD_INTERNAL_ID, line);
        // for (var ctrCol = 2; ctrCol < 3; ctrCol++) {
        var ctrCol = columnIndex;
        var rowHtml = rowEl.children[ctrCol].innerHTML;
        if (rowHtml.indexOf('Edit'.tl()) > -1 && rowHtml.indexOf('javascript') == -1) {
            if (ssapiHasNoValue(internalid) === false) {
                // alert(rowEl.children[ctrCol].innerHTML);
                rowEl.children[ctrCol].innerHTML = '<a href="javascript:uiOpenPopup(' + internalid + ', \'' + sublistId + '\')">' + 'Edit'.tl() + '</a>';
            } else {
                rowEl.children[ctrCol].innerHTML = '<i>' + 'You need to save new records before editing other settings.'.tl() + '</i>';
            }
        }
    }
}

// function uiCreateLinksRecord(sublistId){
// var count = nlapiGetLineItemCount(sublistId);
//    
// // get the suitelet url
// var url = nlapiResolveURL('suitelet', 'customscript_social_admin_profile_sl',
// 'customdeploy_social_admin_profile_sl') + '&suiteSocialRecordTypeId=';
// for (var line = 1; line <= count; line++) {
// var rowid = sublistId + '_row_' + line;
// var rowEl = document.getElementById(rowid);
// var columnCount = rowEl.children.length;
// var internalid = nlapiGetLineItemValue(sublistId, FLD_INTERNAL_ID, line);
// //document.getElementById('customrecord_autopost_row_1')
// for (var ctrCol = 0; ctrCol < columnCount; ctrCol++) {
// var rowHtml = rowEl.children[ctrCol].innerHTML;
// if (rowHtml.indexOf('Edit') > -1) {
// if (ssapiHasNoValue(internalid) === false) {
// rowEl.children[ctrCol].innerHTML = '<a href="javascript:uiOpenPopup(' +
// internalid + ', \'' + sublistId + '\')">Edit</a>';
// }
// else {
// rowEl.children[ctrCol].innerHTML = '<i>You need to save new records before
// editing other settings.</i>';
// }
// }
// }
// }
// }

var popupWindow;
var uiMonitorPopupIntervalId;
var originalUrl;
var isPopupChanged = false;
function uiOpenPopup(internalid, recordId) {
    // Issue: 212776 [SuiteSocial] Assistant > Step 3. Inline editing the chan
    // current changes should be saved before allowing display of popup because
    // after save of popup, this page will be refreshed
    // if (nlapiGetFieldValue('step') == 'recordtypestep') {
    if (uiAcceptLastEdit(recordId) === false) {
        return;
    }
    // }
    if (NS.form.isChanged() === true) {
        uiShowError('Current changes needs to be saved before editing the record.'.tl());
        return;
    }

    var url;
    // if (recordId == REC_RECORD) {
    // url = nlapiResolveURL('suitelet',
    // 'customscript_suitesocial_admin_record_sl',
    // 'customdeploy_suitesocial_admin_record_sl') + '&suiteSocialRecordTypeId='
    // + internalid;
    // }
    // else {
    url = nlapiResolveURL('record', recordId, internalid, 'edit');
    // }
    url += '&popup=T';

    var width = screen.width - 200;
    var height = screen.height - 300;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;
    top = top - 50; // position window slightly above so that we can see the
    // status bar
    var params = 'width=' + width + ', height=' + height;
    params += ', top=' + top + ', left=' + left;
    params += ', directories=no';
    params += ', location=no';
    params += ', menubar=no';
    params += ', resizable=no';
    params += ', scrollbars=yes';
    params += ', status=no';
    params += ', toolbar=no';

    if (recordId == REC_RECORD) {
        // ssapiWait('Updating related records fields...');
        var sw = new ssobjStopWatch();
        uiSuiteletProcessAsync('updaterelatedrecordsfields', 'anyvalue');
        // alert(sw.stop());
        // ssapiWait('Updating related records...');
        // uiSuiteletProcess('SaveRelatedRecords', 'anyvalue');
    }

    ssapiWait('Opening popup window'.tl() + '...');
    // IN SS record popup for example, when a row is deleted, NetSuite redirects
    // to a listing instead of showing the form of the parent SS Form. We track
    // the original
    // url so that when we detected the listing, we change the location to this
    // URL
    originalUrl = url;
    isPopupChanged = false;
    popupWindow = window.open(url, "mywindow", params);
    ssapiWait('Screen is disabled until the popup window is closed.'.tl());
    // Issue: 212776 [SuiteSocial] Assistant > Step 3. Inline editing the chan
    // delay by 6 seconds to allow the popup to render/open
    setTimeout("uiMonitorPopupIntervalId = setInterval('uiMonitorPopup();', 2000);", 6000);
}

// Issue: 212776 [SuiteSocial] Assistant > Step 3. Inline editing the chan
function uiMonitorPopup() {
    var logger = new ssobjLogger('uiMonitorPopup');
    logger.log('closed=' + popupWindow.closed);
    // logger.log(ssapiHasNoValue(popupWindow));
    if (popupWindow.closed) {

        // if (isPopupChanged === false) {
        // Ext.Msg.hide();
        // clearInterval(uiMonitorPopupIntervalId);
        // popupWindow = null;
        // return;
        // }

        // Issue: 214594 [SuiteSocial] assistant > enable records > summary do
        // added reload method call
        ssapiWait();
        window.location.reload();
        clearInterval(uiMonitorPopupIntervalId);
        // Ext.Msg.hide();
        return;
    }

    // check if the url of the popup has become a listing, if yes, set to
    // original URL
    // logger.log('popupWindow.location.href='+popupWindow.location.href);
    // logger.log('indexOf='+popupWindow.location.href.indexOf('&id='));
    if (popupWindow.location.href.indexOf('custrecordentry.nl') == -1) {
        // logger.log(popupWindow.title);
        // Issue: 213210 [SuiteSocial] assistant> setup channels > setup channe
        // removed next line
        // if (popupWindow.document.title.indexOf('SuiteSocial Setup Assistant')
        // > -1 ) {
        popupWindow.close();

        // if (isPopupChanged === false) {
        // Ext.Msg.hide();
        // clearInterval(uiMonitorPopupIntervalId);
        // popupWindow = null;
        // return;
        // }
        window.location.reload();
        return;
        //  
        // }
        // popupWindow.location.href = originalUrl;
    }

    // Issue: 213593 [SuiteSocial] Create of autosubscribe type from assistant
    // st
    // if a default autosubscribe type has been added, close

    if (ssapiHasValue(popupWindow.Ext)) {
        // there is only one element with class pt_title. This is the title of
        // the custom record listing
        var els = popupWindow.Ext.select('.pt_title');
        if (ssapiHasValue(els)) {
            if (ssapiHasValue(els.elements[0])) {
                // Issue: 212776 [SuiteSocial] Assistant > Step 3. Inline
                // editing the chan
                // Issue: 213418 [SuiteSocial] Assistant > Setup Channel> edit
                // then sav
                var titles = [ 'Default AutoSubscribe Type', 'SuiteSocial Channel' ];
                if (uiArrayIndexOf(titles, els.elements[0].innerHTML) > -1) {
                    popupWindow.close();

                    // if (isPopupChanged === false) {
                    // Ext.Msg.hide();
                    // clearInterval(uiMonitorPopupIntervalId);
                    // popupWindow = null;
                    // return;
                    // }
                    window.location.reload();
                    return;
                }
            }
        }
    }

}

/*
 * returns true if a value is undefined, null or empty @param {object} value
 * @return {boolean}
 */
function isNullOrEmpty(value) {
    if (typeof value == 'undefined') {
        return true;
    }
    if (value === null) {
        return true;
    }
    if (value === '') {
        return true;
    }
    return false;
}

/*
 * A shortcut method for nlapiSearchRecord @param {string} recordId Id of the
 * record type @param {string[]} columnIds array of column ids returned in the
 * search @param {nlobjSearchFilter[]} searchFilters Search filters @return
 * (nlobjSearchResult[]} result of the search
 */
function uiRetrieveResults(recordId, columnIds, searchFilters) {

    // try {
    var logger = new ssobjLogger('uiRetrieveResults');
    logger.log('recordId=' + recordId);
    var searchColumns = [];

    for (var i = 0; i < columnIds.length; i++) {
        searchColumns.push(new nlobjSearchColumn(columnIds[i]));
    }
    var results = nlapiSearchRecord(recordId, null, searchFilters, searchColumns);
    if (results !== null) {
        logger.log('recordId=' + recordId + ' results.length=' + results.length);
    } else {
        logger.log('recordId=' + recordId + ' results.length=0');
    }
    return results;

    // }
    // catch (e) {
    // uiShowError(uiGetErrorDetails(e));
    // }
}

/*
 * An entity here is composed of multiple name-value pairs. Example entity:
 * {name: 'teddy', age: 33} @param {nlobjSubList} sublist. Not sure what the API
 * requires this @param {string} sublistId @param {string[]} columnIds An array
 * of column ids from the sublist @return {object[]} An array of entities
 */
function uiConvertSublistItemsToEntities(sublistId, columnIds) {
    var logger = new ssobjLogger('uiConvertSublistItemsToEntities');
    var lineCount = nlapiGetLineItemCount(sublistId);
    var entities = [];
    logger.log('lineCount=' + lineCount);
    if (lineCount === 0) {
        return null;
    }
    for (var i = 1; i <= lineCount; i++) {
        var entity = {};
        for (var j = 0; j < columnIds.length; j++) {
            var columnId = columnIds[j];
            entity[columnId] = nlapiGetLineItemValue(sublistId, columnId, i);
        }
        entities.push(entity);
    }
    logger.log('entities.length=' + entities.length);
    return entities;
}

function uiValidateChannels() {
    // Issue: 211710 [SuiteSocial] I was able to create another Private Messages
    var logger = new ssobjLogger('uiValidateChannels');
    // perform validation on channels name
    var builtinChannelNames = [];
    builtinChannelNames.push('private messages');
    builtinChannelNames.push('record changes');
    builtinChannelNames.push('status changes');
    var sublistId = REC_NEWSFEED_CHANNEL;

    var lineCount = nlapiGetLineItemCount(sublistId);
    // get list of channels in the client
    // var existingChannelNames = [];
    for (var i = 1; i <= lineCount; i++) {
        var channelName = nlapiGetLineItemValue(sublistId, FLD_NEWSFEED_CHANNEL_NAME, i);
        var channelNameLower = channelName.toLowerCase().trim();

        for (var k = 1; k <= lineCount; k++) {
            // validate built in channels
            if (uiArrayIndexOf(builtinChannelNames, channelNameLower) > -1) {
                uiShowError('Validation Error: The channel name "'.tl() + channelName + '" cannot be used because it is a built-in channel name.'.tl());
                return false;
            }

            // compare with other user defined
            if (k == i) {
                // same line so dont compare
                continue;
            }
            var channelNameOther = nlapiGetLineItemValue(sublistId, FLD_NEWSFEED_CHANNEL_NAME, k);
            channelNameOther = channelNameOther.toLowerCase().trim();
            logger.log(channelNameLower + ' ' + channelNameOther);
            if (channelNameLower == channelNameOther) {
                uiShowError('Validation Error: Duplicate channel name found ('.tl() + channelName + '). Note that channel names are case-insensitive.'.tl());
                return false;
            }
        }
    }
}

/*
 * returns true if the existing edit is accepted by netsuite
 */
function uiAcceptLastEdit(sublistid) {
    var logger = new ssobjLogger('uiAcceptLastEdit');
    logger.log('ssapiHasValue(ssobjGlobal.doNotAcceptLastEdit)=' + ssapiHasValue(ssobjGlobal.doNotAcceptLastEdit));
    // alert(ssapiHasValue(ssobjGlobal.doNotAcceptLastEdit))
    if (ssapiHasValue(ssobjGlobal.doNotAcceptLastEdit)) {
        // initial call came from recalc or validatedelete event so this means
        // the current line is already valid
        return true;
    }
    var lineCount = nlapiGetLineItemCount(sublistid);
    // make sure there are no pending changes, no pending changes if the current
    // line is the empty line
    var currentLineIndex = nlapiGetCurrentLineItemIndex(sublistid);
    if (lineCount > 0) {
        if (currentLineIndex <= lineCount) {
            // try ling 1st
            nlapiCommitLineItem(sublistid);
            // if commit fails, index wont changed
            if (currentLineIndex == nlapiGetCurrentLineItemIndex(sublistid)) {
                // platform displays the validation error, so just return
                return false;
            } else {
                // set to previous line
                nlapiSelectLineItem(sublistid, currentLineIndex);
            }
        }
    }
    return true;
}

var restletSubmittedCount = 0;
var restletCompletedCount = 0;
var recordAdded = false;
var goToNext = false;
/*
 * saves the data in the sublist to the database if the internalid is null, then
 * add the row if the internalid is not in the db, delete it else update @param
 * {string} recordId The id of the record type. The recordId and the sublist id
 * should be the same @param {string[]} columnIds The array of column ids to be
 * copied from the sublist to the record @param {nlobjSearcFilter[]}
 * searchFilters The array of filters that can be used if not all rows were
 * retrieved from the database. For example, in the retrieval of channels,
 * hidden channels are not available for update.
 */
function uiSaveSublist(recordId, columnIds, searchFilters) {
    var logger = new ssobjLogger(arguments);
    recordAdded = false;
    // note that sublist id must be the same as the record id
    var sublistid = recordId;

    var lineCount = nlapiGetLineItemCount(sublistid);
    // make sure there are no pending changes, no pending changes if the current
    // line is the empty line
    var currentLineIndex = nlapiGetCurrentLineItemIndex(recordId);
    if (lineCount > 0 && ssapiHasNoValue(ssobjGlobal.doNotAcceptLastEdit)) {
        // ssapiHasValue(ssobjGlobal.doNotAcceptLastEdit) = true - initial call
        // came from recalc eventso this means the current line is already valid

        if (currentLineIndex <= lineCount) {
            // try ling 1st
            nlapiCommitLineItem(recordId);
            // if commit fails, index wont changed
            if (currentLineIndex == nlapiGetCurrentLineItemIndex(recordId)) {
                // platform displays the validation error, so just return
                return false;
            }
        }
    }

    ssapiWait();

    // get data in client
    logger.log('getting entitiesNew');
    var entitiesNew = uiConvertSublistItemsToEntities(recordId, columnIds);
    var dataTableNew = new ssobjDataTable();
    dataTableNew.addMany(entitiesNew);

    // get data in db
    logger.log('getting resultsInDb');
    var resultsInDb = uiRetrieveResults(recordId, columnIds, searchFilters);
    var entitiesDb = ssapiConvertResultsToEntities(resultsInDb);
    var dataTableDb = new ssobjDataTable();
    dataTableDb.addMany(entitiesDb);

    // If an error occurs, log it but continue with the next insert, update or
    // delete. Then in the end, report errors (if any).
    var errorMessage = '';

    // start with inserts
    var internalid;
    var columnId;
    var value;
    // perform validation
    if (sublistid == REC_NEWSFEED_CHANNEL) {
        if (uiValidateChannels() === false) {
            return false;
        }
    }
    //    
    for (var i = 1; i <= lineCount; i++) {
        // Issue: 214249 [SuiteSocial] unresponsive script error when adding 23
        // recor
        // ssapiWait('Processing record ' + i + '...' + firefoxMsg);

        var context = nlapiGetContext();
        // logger.log('getRemainingUsage=' + context.getRemainingUsage());

        // var name = request.getLineItemValue(sublistid, 'name', i);
        internalid = nlapiGetLineItemValue(sublistid, FLD_INTERNAL_ID, i);
        // logger.log('internalid=' + internalid);

        // if (!isNullOrEmpty(internalid)) {
        // internalIdExisting += internalid + ',';
        // }
        var values = {};
        if (ssapiHasNoValue(internalid)) {
            // add
            values = {};
            for (var iCols = 0; iCols < columnIds.length; iCols++) {
                columnId = columnIds[iCols];
                // skip internal id for insert
                if (columnId == FLD_INTERNAL_ID) {
                    continue;
                }
                var fld = nlapiGetLineItemField(recordId, columnId, i);
                if (fld.getType() == 'select') {
                    value = nlapiGetLineItemText(sublistid, columnId, i);
                } else {
                    value = nlapiGetLineItemValue(sublistid, columnId, i);
                }
                values[columnId] = value;
            }
            values.lineNumber = i;

            if (recordId == REC_RECORD) {
                restletSubmittedCount++;
                recordAdded = true;
                uiSuiteletProcessAsync(recordId + '_add', values);
            } else {
                var resp = uiSuiteletProcess(recordId + '_add', values);
                logger.log('resp=' + JSON.parse(resp));
                var newInternalId = JSON.parse(resp);
                nlapiSelectLineItem(recordId, i);
                nlapiSetCurrentLineItemValue(recordId, FLD_INTERNAL_ID, newInternalId);
                nlapiCommitLineItem(recordId);
            }
        } else {
            // update
            var entity = uiConvertSublistItemToEntity(recordId, columnIds, i);
            var hasChanged = dataTableDb.hasChanged(internalid, entity);
            if (hasChanged) {
                logger.log('hasChanged update internalid=' + internalid);
                values = {};
                for (iCols = 0; iCols < columnIds.length; iCols++) {
                    columnId = columnIds[iCols];
                    // skip FLD_AUTOPOST_FLD_REC
                    if (columnId == FLD_AUTOPOST_FLD_REC) {
                        continue;
                    }
                    logger.log('columnId=' + columnId);
                    fld = nlapiGetLineItemField(sublistid, columnId, i);
                    if (fld === null) {
                        logger.log('sublistid=' + sublistid + ' columnId=' + columnId);
                        throw 'fld === null';
                        // return false;
                    }
                    if (fld.getType() == 'select') {
                        value = nlapiGetLineItemText(sublistid, columnId, i);
                    } else {
                        value = nlapiGetLineItemValue(sublistid, columnId, i);
                        // nlapiLogExecution('debug', 'value='+value,'')
                        // existingRecord.setFieldValue(columnId, value);
                    }
                    values[columnId] = value;
                }
                uiSuiteletProcess(recordId + '_update', values);// uiRestletProcess
            }
        }
    }
    // now perform deletion by looping thru existing records,
    // if not found in the sublist, delete it

    // var existingInternalIdList = nlapiGetFieldValue(recordId +
    // '_existing_internal_ids');
    // logger.log('existingInternalIdList=' + existingInternalIdList);
    // var existingInternalIds = existingInternalIdList.split(',');
    // logger.log('existingInternalIds.length=' + existingInternalIds.length);

    // logger.log('dataTableNew.toString()=' + dataTableNew.toString());
    // logger.log('dataTableNew.length=' + dataTableNew.length);
    if (entitiesDb !== null) {
        for (var ctrIds = 0; ctrIds < entitiesDb.length; ctrIds++) {
            entity = entitiesDb[ctrIds];
            var internalId = entity.internalid;
            if (dataTableNew.contains(internalId) === false) {
                // for deletion
                logger.log('<b>recordTypeId=' + recordId + ' internalId=' + internalId + ' DELETE</b>');
                try {
                    nlapiDeleteRecord(recordId, internalId);
                } catch (e) {
                    errorMessage += 'Error delete<br />record id=' + recordId + '<br />internalid=' + internalId + '<br />' + uiGetErrorDetails(e) + '<br /><br />';
                    // return false;
                }
            }
        }
    }

    if (errorMessage !== '') {
        Ext.Msg.hide();
        // Issue: 213426 [SuiteSocial] assistant>setup channel> make the dialog
        var deletErrorMsg = "The following error/s were encountered.".tl() + ' ';
        deletErrorMsg += "The most likely reason is the record/s being deleted failed because it is being used by other records.".tl() + ' ';
        deletErrorMsg += "<br /><br />" + "To delete the record and its dependent data,".tl() + " <br />" + "1) Click 'Edit' link".tl() + " <br />" + "2) In the displayed popup window, click 'More Actions' > 'Delete Including Dependent Records' link.".tl() + ' ';
        // Ext.Msg.alert('Error/s encountered', deletErrorMsg + " <br /><br /><a
        // href='#'
        // onclick='document.getElementById(\"technical_message\").style.display=\"block\"'>Technical
        // Details >></a><br /><div id=technical_message style='display: none'>"
        // + errorMessage + '</div>');
        // alert(deletErrorMsg, 'Error/s encountered');

        // Issue: 213943 [Suitesocial]assistant>setupchannel> record being dele
        Ext.Msg.show({
            title : 'Delete Failed',
            msg : deletErrorMsg,
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            fn : function() {
                ssapiWait('Restoring record...');
                window.NS.form.setChanged(false);
                window.location.reload();
            }
        });
        return false;
    }
    if (recordAdded === false) {
        Ext.Msg.hide();
    }

    return true;
}

function uiDeleteSuiteSocialRecord() {
    var logger = new ssobjLogger('uiDeleteSuiteSocialRecord');
    var socialRecordText = nlapiGetFieldText(FLD_AUTOPOST_FLD_REC);
    Ext.Msg.show({
        title : 'Admin Setup Assistant'.tl(),
        msg : 'This will delete SuiteSocial Record '.tl() + socialRecordText + ' and all its related settings. Continue?'.tl(),
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.Msg.QUESTION,
        fn : function(btn, text) {
            if (btn == 'no') {
                return false;
            }
            var count = nlapiGetLineItemCount(REC_AUTOPOST_FIELD);
            var toBeDeleted = '';
            var ctr = 0;
            while (count > 0) {
                ctr++;
                if (ctr > 20) {
                    alert('infinite loop. toBeDeleted=' + toBeDeleted);
                    return;
                }
                var internalid = nlapiGetLineItemValue(REC_AUTOPOST_FIELD, FLD_INTERNAL_ID, 1);
                if (ssapiHasNoValue(internalid)) {
                    break;
                }
                toBeDeleted += internalid + ', ';
                logger.log('for delete internalid=' + internalid);
                nlapiSelectLineItem(REC_AUTOPOST_FIELD, 1);
                nlapiRemoveLineItem(REC_AUTOPOST_FIELD, 1);
                nlapiCommitLineItem(REC_AUTOPOST_FIELD);
                nlapiSelectLineItem(REC_AUTOPOST_FIELD, 1);
                count = nlapiGetLineItemCount(REC_AUTOPOST_FIELD);
                // nlapiDeleteRecord(internalid);
            }
            alert('delete code commented for now. toBeDeleted=' + toBeDeleted);
        }

    });

}

// Issue: 216565 [SuiteSocial] Assistant > enable record > add suite so
// code moved to the before load user event of the subscription link
// function uiCreateRelatedRecordsAll(){
// var logger = new ssobjLogger('uiCreateRelatedRecordsAll');
// // we will try to link new record with existing record (including itself, for
// example an Issue can have a field 'Duplicate Of' which is of type Issue). so,
// get list of ss records
// var recordResults = ssapiGetResults(REC_RECORD, [FLD_INTERNAL_ID,
// FLD_RECORD_NAME], [new nlobjSearchFilter(FLD_ISINACTIVE, null, OP_IS, 'F')]);
// if (recordResults === null) {
// return;
// }
// for (var x = 0; x < recordResults.length; x++) {
// var param = x;
// uiSuiteletProcess('CreateRelatedRecordsPerRecord', param);
// }
// logger.log('DONE');
// return true;
// }

var isProcessing = false;

/*
 * saves the suite social records
 */
function uiSaveSuiteSocialRecords() {

    var logger = new ssobjLogger('uiSaveSuiteSocialRecords');

    if (uiAcceptLastEdit(REC_RECORD) === false) {
        return false;
    }
    logger.log('record type:' + REC_RECORD);
    var searchFilters = [];
    searchFilters.push(new nlobjSearchFilter(FLD_ISINACTIVE, null, OP_IS, 'F'));
    // Issue 212854 [SuiteSocial]Assistant > save buttons in assis
    ssapiWait();
    isProcessing = true;
    if (uiSaveSublist(REC_RECORD, [ FLD_INTERNAL_ID, FLD_RECORD_TYPE, FLD_RECORD_AUTOPOST ], searchFilters) === false) {
        return false;
    }
    isProcessing = false;
    // refresh the related records
    // ssapiWait('Updating related records...');
    // Issue: 215767 [SuiteSocial] Create code for entry of Subscription links
    // Issue: 216565 [SuiteSocial] Assistant > enable record > add suite so
    // Issue: 217066 [SuiteSocial] Assistant > enable records > edit > S

    // var param = {};
    // param.command = 'SaveRelatedRecords';
    // uiSuiteletProcess('SaveRelatedRecords', 'anyvalue');

    // ssapiWait('Updating related records ...');
    // uiSuiteletProcess('updaterelatedrecordsfields', 'anyvalue');
    if (recordAdded === false) {
        window.NS.form.setChanged(false);
        ssapiSaveOk();
    }

    return true;
}

/*
 * saves channels
 */
function uiSaveChannels() {

    if (uiAcceptLastEdit(REC_NEWSFEED_CHANNEL) === false) {
        return false;
    }

    ssapiWait();

    // var logger = new ssobjLogger('uiSaveChannels');

    // uiImplementMissingFunctions();

    var searchFilters = [];
    var filter = new nlobjSearchFilter(FLD_NEWSFEED_CHANNEL_HIDDEN, null, 'is', 'F');
    searchFilters.push(filter);
    searchFilters.push(new nlobjSearchFilter(FLD_ISINACTIVE, null, OP_IS, 'F'));
    // filter private messages
    searchFilters.push(new nlobjSearchFilter(FLD_INTERNAL_ID, null, 'noneof', '1'));
    // alert(FLD_INTERNAL_ID+ ' '+ FLD_NEWSFEED_CHANNEL_NAME)
    // Issue 212854 [SuiteSocial]Assistant > save buttons in assis
    if (uiSaveSublist(REC_NEWSFEED_CHANNEL, [ FLD_INTERNAL_ID, FLD_NEWSFEED_CHANNEL_NAME ], searchFilters) === false) {
        // Ext.Msg.hide();
        return false;
    }
    window.NS.form.setChanged(false);

    Ext.Msg.hide();
    ssapiSaveOk();

    return true;

}

function uiSavePreCanned() {
    var logger = new ssobjLogger(arguments);
    ssapiWait('Scheduling process'.tl() + '...');
    var resp = uiSuiteletProcess('subscribeEmployees', 'anyvalue');
    logger.log('resp=' + resp);

    window.NS.form.setChanged(false);
    Ext.Msg.hide();

    uiShowInfo('You will be notified via email upon completion of adding of Record Subscriptions to employee profiles.'.tl());
    // ssapiSaveOk();

    return true;
    // logger.log('record type:' + REC_RECORD);
    // var resp;
    // // // save SS records
    // // var columnIds = [FLD_INTERNAL_ID, FLD_RECORD_TYPE,
    // FLD_RECORD_AUTOPOST];
    // // var entities = uiConvertSublistItemsToEntitiesText(REC_RECORD,
    // columnIds);
    // // resp = uiSuiteletProcess('addprecannedrecords', entities);
    // // logger.log('resp=' + JSON.parse(resp));
    //    
    // // save autopost type
    // //
    // // var my = mySubscriptions;
    //    
    // var autoSubTypeNames = [];
    // var defaultRoleIds = [];
    // var results = retrieveDefaultAutoSubTypes();
    // for (var i = 0; i < results.length; i++) {
    // var result = results[i];
    // var autoSubTypeName = result.getValue(FLD_AUTOSUB_TYPE_NAME);
    // autoSubTypeNames.push(autoSubTypeName);
    // logger.log('autoSubTypeName=' + autoSubTypeName);
    //        
    // // var recordName = result.getText(FLD_AUTOSUB_TYPE_REC);
    // // var fld = result.getText(FLD_AUTOSUB_TYPE_FLD);
    //        
    // // // check if autosub type is already in db, if not yet, add
    // // var internalid = getRecordId(REC_AUTOSUB_TYPE, FLD_AUTOSUB_TYPE_NAME,
    // 'is', autoSubTypeName);
    // // if (internalid === null) {
    // // var entity = {};
    // // entity.name = autoSubTypeName;
    // // // get ss record id
    // // // var ssRecordId = getRecordId(REC_RECORD, FLD_RECORD_TYPE, 'is',
    // recordTypeId);
    // // entity[FLD_AUTOSUB_TYPE_REC] = recordName;
    // // entity[FLD_AUTOSUB_TYPE_FLD] = fld;
    // // resp = uiSuiteletProcess(REC_AUTOSUB_TYPE + '_add', entity);
    // // internalid = JSON.parse(resp);
    // // logger.log(REC_AUTOSUB_TYPE + '_add resp=' + resp);
    // // }
    //        
    // // get list of roles
    // var roleIds = [];
    // var defaultAutoSubTypeInternalId = result.getValue(FLD_INTERNAL_ID);
    // var roleResults =
    // retrieveDefaultAutoSubTypeRoles(defaultAutoSubTypeInternalId);
    // if (roleResults !== null) {
    // for (var r = 0; r < roleResults.length; r++) {
    // var roleId = roleResults[r].getValue(R_SS_RECORD_SUBSCRIPTION_ROLE.ROLE);
    // logger.log('roleId=' + roleId);
    // roleIds.push(roleId);
    // }
    // defaultRoleIds.push(roleIds);
    // }
    // }
    // var param = {};
    // // alert(autoSubTypeNames)
    // // alert(defaultRoleIds)
    // // alert(defaultRoleIds.length)
    // param.autoSubTypeNames = autoSubTypeNames;
    // param.defaultRoleIds = defaultRoleIds;

    // for (var q in my) {
    // var sublistId = 'my' + my[q].toLowerCase();
    // logger.log('sublistId=' + sublistId);
    // var autoSubTypeName = my[q];
    // logger.log('autoSubTypeName=' + autoSubTypeName);
    // // if has no roles, do not save
    // var count = nlapiGetLineItemCount(sublistId);
    // if (count === 0) {
    // continue;
    // }
    // autoSubTypeNames.push(autoSubTypeName);
    // // check if autosub type is already in db, if yes do not add
    // var internalid = getRecordId(REC_AUTOSUB_TYPE, FLD_AUTOSUB_TYPE_NAME,
    // 'is', autoSubTypeName);
    // if (internalid === null) {
    // var entity = {};
    // entity.name = autoSubTypeName;
    // entity[FLD_AUTOSUB_TYPE_REC] = autoSubTypeRecord[my[q]];
    // entity[FLD_AUTOSUB_TYPE_FLD] = autoSubTypeField[my[q]][0];
    // resp = uiSuiteletProcess(REC_AUTOSUB_TYPE + '_add', entity);
    // internalid = JSON.parse(resp);
    // logger.log(REC_AUTOSUB_TYPE + '_add resp=' + resp);
    // }
    //        
    // // var filters = [];
    // // filters.push(new nlobjSearchFilter(FLD_AUTOSUB_TYPE_NAME, null, 'is',
    // autoSubTypeName))
    // // var results = nlapiSearchRecord(REC_AUTOSUB_TYPE, null, filters, [new
    // nlobjSearchColumn('internalid')]);
    // // var internalid;
    // // if (results !== null) {
    // // internalid = results[0].getValue('internalid');
    // // }
    // // else {
    // // var entity = {};
    // // entity.name = autoSubTypeName;
    // // entity[FLD_AUTOSUB_TYPE_REC] = autoSubTypeRecord[my[q]];
    // // entity[FLD_AUTOSUB_TYPE_FLD] = autoSubTypeField[my[q]][0];
    // // var resp = uiSuiteletProcess(REC_AUTOSUB_TYPE + '_add', entity);
    // // internalid = JSON.parse(resp);
    // // logger.log('resp=' + resp);
    // // }
    // // get list of roles
    // var roleIds = [];
    // for (var line = 1; line <= count; line++) {
    // // console.log('roleid='+nlapiGetLineItemValue(sublistId, 'roleid',
    // line));
    // roleIds.push(nlapiGetLineItemValue(sublistId, 'roleid', line));
    // }
    // chosenRoleIds.push(roleIds);
    // }
    // get all employees with this role and subscribe them to the autosub type

}

/*
 * @param {boolean} isNextStep Whether to move to next step if save succeeds
 */
function uiSaveAndEditAnother(isNextStep) {
    var logger = new ssobjLogger('uiSaveAndEditAnother');
    if (ssapiHasNoValue(isNextStep)) {
        // this will happen if called from the 'Save and Edit Another' button
        isNextStep = false;
    }
    if (window.NS.form.isChanged() === false) {
        if (isNextStep) {
            return true;
        }
        Ext.Msg.show({
            title : 'Admin Setup Assistant'.tl(),
            msg : 'There are no pending changes'.tl() + '.',
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.Msg.INFO
        });
        return true;
    }

    // fields
    logger.log('record type:' + FLD_AUTOPOST_FLD_REC);
    var searchFilters = [];
    var searchFilter = new nlobjSearchFilter(FLD_AUTOPOST_FLD_REC, null, 'anyof', nlapiGetFieldValue(FLD_AUTOPOST_FLD_REC));
    searchFilters.push(searchFilter);
    if (uiSaveSublist(REC_AUTOPOST_FIELD, [ FLD_INTERNAL_ID, FLD_AUTOPOST_FLD_REC, FLD_AUTOPOST_FLD_FLD ], searchFilters) === false) {
        return false;
    }

    // saved searches
    logger.log('record type:' + FLD_AUTOPOST_FLD_REC);
    var suiteSocialRecordTypeId = nlapiGetFieldValue(FLD_AUTOPOST_FLD_REC);
    var scriptedRecordTypeId = nlapiLookupField(REC_RECORD, suiteSocialRecordTypeId, FLD_RECORD_TYPE);
    searchFilters = [];
    searchFilters.push(new nlobjSearchFilter(FLD_AUTOPOST_RECORDTYPE, null, 'anyof', scriptedRecordTypeId));
    var searchColumns = [ FLD_AUTOPOST_CHANNEL, FLD_AUTOPOST_RECORDTYPE, FLD_AUTOPOST_SEARCH, FLD_AUTOPOST_WEEKENDS, FLD_AUTOPOST_ICON, FLD_AUTOPOST_MESSAGE, FLD_AUTOPOST_RECIPIENT, FLD_INTERNAL_ID ];
    if (uiSaveSublist(REC_AUTOPOST, searchColumns, searchFilters) === false) {
        return false;
    }

    // auto subscribe
    logger.log('record type:' + FLD_AUTOSUB_TYPE_REC);
    searchFilters = [];
    searchFilters.push(new nlobjSearchFilter(FLD_AUTOSUB_TYPE_REC, null, 'anyof', suiteSocialRecordTypeId));
    searchColumns = [ FLD_AUTOSUB_TYPE_REC, FLD_AUTOSUB_TYPE_FLD, FLD_INTERNAL_ID ];
    if (uiSaveSublist(REC_AUTOSUB_TYPE, searchColumns, searchFilters) === false) {
        return false;
    }

    window.NS.form.setChanged(false);
    return true;
}

function unload() {
    alert('unload');
    var createProfile = nlapiGetFieldValue('create_profile');
    if (ssapiHasNoValue(createProfile)) {
        return;
    }
    if (createProfile == 'auto') {
        window.open('http://google.com');
    }
}

function setRemoveHeaderLabel() {
    var header;
    switch (nlapiGetFieldValue('step')) {
    case 'precannedstep':
        header = document.getElementById(REC_RECORD + '_headerrow');
        if (header !== null) {
            header.children[2].innerHTML = 'Delete'.tl();
        }
        break;
    case 'channelstep':
        header = document.getElementById(REC_NEWSFEED_CHANNEL + '_headerrow');
        if (header !== null) {
            header.children[2].innerHTML = 'Delete'.tl();
        }
        break;
    default:
        // set the Remove header label
        // header = document.getElementById(REC_NEWSFEED_CHANNEL +
        // '_headerrow');
        // if (header !== null) {
        // header.children[3].innerHTML = 'Remove';
        // }

        header = document.getElementById(REC_RECORD + '_headerrow');
        if (header !== null) {
            header.children[4].innerHTML = 'Delete'.tl();
        }
    }

}

function openProfilePopup() {
    Ext.Msg.wait('...', 'Please wait'.tl());
    var url = nlapiResolveURL('suitelet', 'customscript_social_admin_profile_sl', 'customdeploy_social_admin_profile_sl');
    url += "&popup=T";
    var width = screen.width - 200;
    var height = screen.height - 300;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;
    var params = 'width=' + width + ', height=' + height;
    params += ', top=' + top + ', left=' + left;
    params += ', directories=no';
    params += ', location=no';
    params += ', menubar=no';
    params += ', resizable=no';
    params += ', scrollbars=no';
    params += ', status=no';
    params += ', toolbar=no';

    window.open(url, "mywindow", params);
    Ext.Msg.hide();

    return false;
}

/*
 * profile step is displayed depending on whether there are profiles that need
 * to be created. this returns true if profile step exixts in the assistant
 */
function profileStepExists() {
    var anchors = document.getElementsByTagName('a');
    for (var i = 0; i < anchors.length; i++) {
        if (anchors[i].innerHTML.indexOf('Create&nbsp;Profiles') > -1) {
            return true;
        }
    }

    return false;
}

function uiSave() {
    var logger = new ssobjLogger('uiSave');
    try {
        restletSubmittedCount = 0;
        restletCompletedCount = 0;

        // alert('uiSave');
        switch (nlapiGetFieldValue('step')) {
        case 'sendersstep':
            // Issue: 214351 [SuiteSocial] Have the setting of Company Custom
            // preferences such
            ssapiWait('Saving'.tl() + '...');
            var digestsender = nlapiGetFieldValue('custscript_suitesocial_digest_sender');
            var autopostsender = nlapiGetFieldValue('custscript_suitesocial_autopost_sender');
            var errrorMessageRecipient = nlapiGetFieldValue('custscript_ss_error_message_recipient');
            // company-wide language
            var emailLanguageId = nlapiGetFieldValue('custscript_ss_email_language');
            // Issue: 248093 [SuiteSocial] Admin Setup > Set Email Preference >
            // Error occurs when no Email Language is supplied and Save and Next
            // is clicked
            if (ssapiHasNoValue(emailLanguageId)) {
                uiShowError('Provide a value for Email Language.'.tl());
                return false;
            }
            var enableImagePreview = nlapiGetFieldValue('custscript_ss_enable_image_preview');
            var allowCustomerUpload = nlapiGetFieldValue('custscript_ss_allow_customer_upload');
            var emailCaptureAddr = nlapiGetFieldValue('custscript_ss_email_capture_address');
            var enableCreateEditChannelOnNF = nlapiGetFieldValue('custscript_ss_create_edit_channel_nf');
            var showColleaguePaneForCust = nlapiGetFieldValue('custscript_ss_show_colleague_pane_for_cu');
            var kudosIntegration = nlapiGetFieldValue('custscript_ss_kudos_integration');
            var showUserRestrictionDetails = nlapiGetFieldValue('custscript_ss_show_user_restriction');

            var resp = uiSuiteletProcess('savesenders', {
                custscript_suitesocial_digest_sender : digestsender,
                custscript_suitesocial_autopost_sender : autopostsender,
                custscript_ss_error_message_recipient : errrorMessageRecipient,
                custscript_ss_email_language : emailLanguageId,
                custscript_ss_enable_image_preview : enableImagePreview,
                custscript_ss_allow_customer_upload : allowCustomerUpload,
                custscript_ss_email_capture_address : emailCaptureAddr,
                custscript_ss_create_edit_channel_nf : enableCreateEditChannelOnNF,
                custscript_ss_show_colleague_pane_for_cu : showColleaguePaneForCust,
                custscript_ss_kudos_integration : kudosIntegration,
                custscript_ss_show_user_restriction : showUserRestrictionDetails
            });
            if (!resp) {
                uiShowError("Error in uiSuiteletProcess('savesenders')");
                return false;
            }
            window.NS.form.setChanged(false);
            Ext.Msg.hide();
            ssapiSaveOk();
            break;
        case 'profilestep':
            uiSuiteletProcess('createprofile', 'dummyparam');
            alert('You will be notified via email upon completion of SuiteSocial profile creation.'.tl(), 'SuiteSocial'.tl());
            break;
        case 'channelstep':
            if (uiSaveChannels() === false) {
                return false;
            }
            break;
        case 'permissionstep':

            if (uiAcceptLastEdit('permission') === false) {
                return false;
            }
            ssapiWait('Saving'.tl() + '...');
            if (goToNext) {
                if (uiSavePermissions() === false) {
                    return false;
                }
            } else {
                // set a delay to allow wait message to render in some browsers
                setTimeout(function() {
                    uiSavePermissions();
                }, 100);
            }
            break;
        case 'recordtypestep':
            if (uiSaveSuiteSocialRecords() === false) {
                return false;
            }
            break;
        case 'precannedstep':
            if (uiSavePreCanned() === false) {
                return false;
            }
            break;
        default:
            alert('uiSave(): ' + nlapiGetFieldValue('step') + ' not found');
        }
        return true;
    } catch (e) {
        uiShowError('An error occurred.'.tl() + '<br /><br /> ' + 'Technical details:'.tl() + '<br />' + uiGetErrorDetails(e), 'Unexpected Error'.tl());
    }

}

function uiSavePermissions() {
    // get array of roles
    // Issue: 244607 [SuiteSocial] SuiteSocial Admin Setup> Grant Permissions
    var roleCount = nlapiGetLineItemCount('permission');
    if (roleCount === 0) {
        uiShowError('You need to choose at least one role.'.tl());
        return false;
    }
    var newRoleIds = [];
    for (var line = 1; line <= roleCount; line++) {
        var roleId = nlapiGetLineItemValue('permission', 'id', line);
        newRoleIds.push(roleId);
    }
    var returnValue = uiSuiteletProcess('savepermissions', newRoleIds);
    Ext.Msg.hide();
    if (returnValue == 'scheduled') {
        var msg = 'You will receive an email once role permissions have been successfully granted.'.tl();
        if (goToNext) {
            alert(msg, 'Grant Permissions'.tl());
        } else {
            uiShowInfo(msg, 'Grant Permissions'.tl());
        }
    } else {
        uiShowInfo('Permissions were successfully saved'.tl(), 'Grant Permissions'.tl());
    }

    window.NS.form.setChanged(false);

    return;
}

function uiGotoNext() {
    var stepNumber = 0;

    switch (nlapiGetFieldValue('step')) {
    case 'profilestep':
        stepNumber = 2;
        break;
    case 'permissionstep':
        stepNumber = 3;
        break;
    case 'channelstep':
        stepNumber = 4;
        break;
    case 'recordtypestep':
        stepNumber = 5;
        break;
    case 'precannedstep':
        stepNumber = 6;
        break;
    case 'sendersstep':
        // Issue: 214351 [SuiteSocial] Have the setting of Company Custom
        // preferences such
        stepNumber = 7;
        break;
    default:
        alert('uiSaveAndNext() ' + nlapiGetFieldValue('step') + ' not found');
    }

    // alert(nlapiGetFieldValue('step'))
    if (nlapiGetFieldValue('step') !== 'profilestep' && profileStepExists() === false) {
        stepNumber = parseInt(stepNumber, 10) - 1;
    }
    // alert(stepNumber);
    navigateToStep(stepNumber);

}

function uiSaveAndNext() {
    // alert('uiSaveAndNext');
    goToNext = true;
    if (uiSave() === false) {
        return false;
    }

    if (nlapiGetFieldValue('step') == 'recordtypestep' && recordAdded) {
        // the record step is handled differently for user experience reasons
        return true;
    }
    uiGotoNext();

    return true;
    // var stepNumber = 0;
    //    
    // switch (nlapiGetFieldValue('step')) {
    // case 'profilestep':
    // stepNumber = 2;
    // break;
    // case 'permissionstep':
    // stepNumber = 3;
    // break;
    // case 'channelstep':
    // stepNumber = 4;
    // break;
    // case 'recordtypestep':
    // stepNumber = 5;
    // break;
    // case 'precannedstep':
    // stepNumber = 6;
    // break;
    // case 'sendersstep':
    // // Issue: 214351 [SuiteSocial] Have the setting of Company Custom
    // preferences such
    // stepNumber = 7;
    // break;
    // default:
    // alert('uiSaveAndNext() ' + nlapiGetFieldValue('step') + ' not found');
    // }
    //    
    // // alert(nlapiGetFieldValue('step'))
    // if (nlapiGetFieldValue('step') !== 'profilestep' && profileStepExists()
    // === false) {
    // stepNumber = parseInt(stepNumber, 10) - 1;
    // }
    // // alert(stepNumber);
    // navigateToStep(stepNumber);
    //    
    //    

}

/*
 * this is used in inserting buttons in the default list of buttons in the
 * assistant. it also involves changing the default behavior of the buttons.
 */
function editAssistantButtons() {
    var logger = new ssobjLogger(arguments);
    if (nlapiGetFieldValue('step') == 'summary') {
        return;
    }
    var nextTable = document.getElementById('tbl_secondarynext') || document.getElementById('tbl_next');
    var nextTd = nextTable.parentNode;

    var spacer = nextTd.parentNode.nextSibling || nextTd.nextSibling;
    spacer.style.display = 'none';

    var nextHTML = outerHTML(nextTable);
    nextHTML = nextHTML.replace('submit', 'button');
    // remove default onclick event
    nextHTML = nextHTML.replace('onclick', 'onclickX');

    // alert(nextHTML)
    var saveHTML;
    if (nextHTML.indexOf('secondarynext') > -1) {
        saveHTML = nextHTML.replace(/secondarynext/gi, 'secondarysave');
        saveHTML = saveHTML.replace(/id="next"/gi, 'id=secondarysave');
        saveHTML = saveHTML.replace(/id=next/gi, 'id=secondarysave');
    } else {
        saveHTML = nextHTML.replace(/id=next/gi, 'id=secondarysave');
        saveHTML = saveHTML.replace(/id="next"/gi, 'id=secondarysave');
    }

    nextTd.innerHTML = '<table><tr><td>' + nextHTML + '</td><td>' + saveHTML + '</td></tr></table>';
    // save and next button
    var nextButton = document.getElementById('secondarynext') || document.getElementById('next');
    nextButton.value = 'Save and Next'.tl() + ' >';
    // override the click event function
    Ext.get(nextButton).on('click', function() {
        uiSaveAndNext();
        return false;
    });
    // save button
    var saveButton = document.getElementById('secondarysave');
    saveButton.value = 'Save'.tl();
    Ext.get('secondarysave').on('click', function() {
        uiSave();
        return true;
    });

}

/**
 * @param {String}
 *        type Access mode: create,a copy, edit
 * @return {void}
 */
function clientPageInit(type) {
    var logger = new ssobjLogger(arguments);
    // alert(window.opener);
    // // alert(window.opener.closed);
    // if (ssapiHasValue(window.opener)) {
    // // Issue: 212776 [SuiteSocial] Assistant > Step 3. Inline editing the
    // chan
    // window.opener.location.reload();
    // window.close();
    // return;
    // }
    //    
    // show message when page is refresshing
    Ext.select(window).on({
        'beforeunload' : function() {
            ssapiWait('Page is loading...'.tl());
            // Issue: 214589 [SuiteSocial] Assistant > status dialog is not
            // completing
            setTimeout('Ext.Msg.hide()', 3000);
        }
    });

    editAssistantButtons();

    setRemoveHeaderLabel();
    // setTimeout('setRemoveHeaderLabel();', 5000);

    window.tcForceClose = false;

    uiCreateLinks(REC_NEWSFEED_CHANNEL, 1);
    uiCreateLinks(REC_RECORD, 3);
    setRemoveHeaderLabel();

    setInterval('uiCreateLinks(REC_NEWSFEED_CHANNEL, 1);', 2000);
    setInterval('uiCreateLinks(REC_RECORD, 3);', 2000);
    setInterval('setRemoveHeaderLabel();', 2000);

    switch (nlapiGetFieldValue('step')) {
    case 'precannedstep':
        // makes the sublist non editable (no extra add row and no buttons )
        setInterval("Ext.select('#customrecord_suitesocial_record_layer .machineButtonRow').setStyle({display: 'none'});", 50);
        setInterval("Ext.select('#customrecord_suitesocial_record_layer .listtextnonedit').setStyle({display: 'none'});", 50);
        // Issue: 214566 [Suitesocial] assistant > enable record > autosubscrib
        uiCreateDefaultAutoSubscribe();
        break;
    default:
        // default_statement;
    }
    var el = document.getElementById('profile_progress');
    if (el !== null) {
        showCreateProfileStatus();
        setInterval('showCreateProfileStatus();', 5000);
    }

}

/**
 * @return {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {
    // if (confirm('clientSaveRecord') === false) {
    // return false;
    // }
    return true;
}

/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {Boolean} True to continue changing field value, false to abort value
 *         change
 */
function clientValidateField(type, name, linenum) {
    if (nlapiGetFieldValue('step') == 'recordtypestep') {
        if (type == REC_RECORD && name == FLD_RECORD_TYPE && nlapiGetLineItemValue(REC_RECORD, FLD_INTERNAL_ID, linenum) !== null) {
            alert('Editing of field "Record Type" is not allowed. Either delete the record or make it inactive.'.tl());
            return false;
        }

    }
    addTrimFunctions();
    if (type === null && name == FLD_AUTOPOST_FLD_REC) {
        if (window.NS.form.isChanged()) {
            var question = 'This page is asking you to confirm that you want to leave - data you have entered may not be saved. Do you want to leave?'.tl();
            if (confirm(question)) {
                window.tcForceClose = true;
            } else {
                return false;
            }
        } else {
            window.tcForceClose = true;
        }
    }
    return true;
}

/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {void}
 */
function clientFieldChanged(type, name, linenum) {

    var logger = new ssobjLogger('clientFieldChanged');
    logger.log('type=' + type + '; name=' + name + '; linenum=' + linenum + ' ;step=' + nlapiGetFieldValue('step'));
    switch (nlapiGetFieldValue('step')) {
    case 'permissionstep':
        if (name == 'permission_option') {
            switch (nlapiGetFieldValue('permission_option')) {
            case 'All Roles':
                uiAddAllRoles();
                // statement_1
                break;
            case 'Only Selected Roles':
                uiClearSublist('permission');
                break;
            default:
                // default_statement;
            }
        }
        break;

    case 'recordtypestep':
        break;

    default:
        // default_statement;
    }

    switch (name) {
    case 'create_profile':
        // if (nlapiGetFieldValue(name) == 'auto') {
        // Ext.get('buttonCreateProfile').fadeIn();
        // }
        // else {
        // Ext.get('buttonCreateProfile').fadeOut();
        // }
        NS.form.setChanged(false);
        break;
    case 'custscript_suitesocial_digest_sender':
        // setup email preferences step
        if (ssapiHasNoValue(nlapiGetFieldValue('custscript_suitesocial_autopost_sender'))) {
            nlapiSetFieldValue('custscript_suitesocial_autopost_sender', nlapiGetFieldValue('custscript_suitesocial_digest_sender'));
        }
        break;
    default:
        // default_statement;
    }

    if (window.tcForceClose) {
        NS.form.setChanged(false);
    }
    // alert('clientFieldChanged() type=' + type + '; name=' + name + ';
    // linenum=' + linenum);
    // //logger.log('clientFieldChanged() type=' + type + '; name=' + name + ';
    // linenum=' + linenum);

    if (type === null && name == FLD_AUTOPOST_FLD_REC) {
        var NEW_OPTION_TEXT = '';
        // alert(nlapiGetFieldText(FLD_AUTOPOST_FLD_REC) )
        // alert(nlapiGetFieldValue(FLD_AUTOPOST_FLD_REC) )
        if (nlapiGetFieldText(FLD_AUTOPOST_FLD_REC) == NEW_OPTION_TEXT) {
            // new record being created
            return true;
        }
        // var ADMIN_ASSISTANT_SCRIPT_ID = 'customdeploy_test_assistant';
        // var url = nlapiResolveURL('suitelet', ADMIN_ASSISTANT_SCRIPT_ID);
        // alert(url);
        var suiteSocialRecordTypeId = nlapiGetFieldValue(name);
        var url = window.location.href;
        if (url.indexOf('suiteSocialRecordTypeId') == -1) {
            url = url + '&suiteSocialRecordTypeId=' + suiteSocialRecordTypeId;
        } else {
            url = url.substr(0, url.lastIndexOf('suiteSocialRecordTypeId')) + 'suiteSocialRecordTypeId=' + suiteSocialRecordTypeId;
        }
        Ext.Msg.wait('Please wait'.tl() + '...', 'Processing');
        window.location.href = url;
    }
}

/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @return {void}
 */
function clientPostSourcing(type, name) {

}

/*
 * To prevent the browser from freezing, save as often as possible. This
 * function forces saving when many rows are already added by the admin
 */
function ssapiSaveWhenManyRowsAdded() {

    var logger = new ssobjLogger('ssapiSaveWhenManyRowsAdded');
    return true;
    // if (isProcessing) {
    // // don't trigger when processing
    // return true;
    // }
    //    
    // // Issue: 216854 [SuiteSocial] Assistant > enable records > unresponsiv
    // // count number of new rows, if more than 5, inform admin to save
    // var MANY_ROWS = 4;
    // var sublistId = REC_RECORD;
    // var lineCount = nlapiGetLineItemCount(sublistId);
    // logger.log('lineCount=' + lineCount);
    // if (lineCount === 0) {
    // return;
    // }
    // var newRows = 0;
    // for (var i = 1; i <= lineCount; i++) {
    // // logger.log('FLD_INTERNAL_ID' + nlapiGetLineItemValue(sublistId,
    // FLD_INTERNAL_ID, i));
    // if (ssapiHasNoValue(nlapiGetLineItemValue(sublistId, FLD_INTERNAL_ID,
    // i))) {
    // newRows++;
    // }
    // }
    // if (newRows >= MANY_ROWS) {
    //    
    // Ext.MessageBox.show({
    // title: 'SuiteSocial'.tl(),
    // msg: 'You already added a couple of items. To avoid losing unsaved
    // changes (like due to browser crashing), save will be performed
    // now.'.tl(),
    // buttons: Ext.MessageBox.OK,
    // icon: Ext.MessageBox.INFO,
    // fn: function(){
    // uiSave();
    // }
    // });
    //        
    // // alert('You already added a couple of rows. To avoid losing unsaved
    // changes (like due to browser crashing), save will be now be performed.');
    // // uiSave();
    // }
}

/**
 * @param {String}
 *        type Sublist internal id
 * @return {void}
 */
function clientLineInit(type) {
    var logger = new ssobjLogger('clientLineInit');
    logger.log('clientLineInit type=' + type + '; CurrentLineItemIndex=' + nlapiGetCurrentLineItemIndex(type));
}

/**
 * @param {String}
 *        type Sublist internal id
 * @return {Boolean} True to save line item, false to abort save
 */
function clientValidateLine(type) {
    var logger = new ssobjLogger('clientValidateLine');
    logger.log('type=' + type);

    return true;
}

/**
 * @param {String}
 *        type Sublist internal id
 * @return {void}
 */
function clientRecalc(type) {
    var logger = new ssobjLogger('clientRecalc');
    logger.log('type=' + type);
    // Issue: 216854 [SuiteSocial] Assistant > enable records > unresponsiv
    // calling ssapiSaveWhenManyRowsAdded will call functions that calls
    // commitlineItem which triggers this event again
    // creating a loop. To prevent that, create a variable that marks the call
    // came from this event
    ssobjGlobal.doNotAcceptLastEdit = true;
    ssapiSaveWhenManyRowsAdded();
    ssobjGlobal.doNotAcceptLastEdit = null;
}

/**
 * @param {String}
 *        type Sublist internal id
 * @return {Boolean} True to continue line item insert, false to abort insert
 */
function clientValidateInsert(type) {
    var logger = new ssobjLogger('clientValidateInsert');
    logger.log('type=' + type);

    // ssobjGlobal
    return true;
}

/**
 * this is called in the suite social admin assistant particularly when an
 * autopost field is added.
 */
/*
 * changes the current location to the add autopost field page
 */
function uiAddAutopost() {
    // get url from a hidden field
    window.location.href = nlapiGetFieldValue('new_autopost').toString();
}

/*
 * deletes a autopost field @param {internalId} integer Deletes the row in the
 * record type REC_AUTOPOST_FIELD @param {integer} internalId the internal id of
 * the record
 */
function uiDeleteAutopostField(internalId) {
    if (confirm('Delete Autopost field?') === false) {
        return;
    }
    try {
        Ext.MessageBox.wait("Please wait".tl(), 'Processing...');
        nlapiDeleteRecord(REC_AUTOPOST_FIELD, internalId);
    } catch (e) {
        Ext.MessageBox.hide();
        alert('Delete failed. It is likely that the record being deleted is being used by other records.'.tl());
        return;
    }
    // deleted, reload page
    window.location.reload();
}
