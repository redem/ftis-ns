/**
 * Module Description Here You are using the default templates which should be
 * customized to your needs. You can change your user name under
 * Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version Date Author Remarks 1.00 02 Jan 2012 tcaguioa
 * 
 */
function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    deleteSubscriptionLink(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

    if (typeof window != 'undefined') {
        window.location.href = window.opener.originalUrl;
    }

}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: Employee SuiteSocial Profile - Related Record Subscriptions. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a Related Record Subscription inactive.'.tl() + ' ';
    inactivateQuestion += 'While a Related Record Subscription is inactive,'.tl() + ' ';
    inactivateQuestion += 'posts related to it will not be created.'.tl() + ' ';
    inactivateQuestion += 'Do you want to make this Related Record Subscription inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'SuiteSocial',
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {

    try {
        var logger = new ssobjLogger('clientSaveRecord');
        // check mandatory fields
        if (ssapiHasNoValue(nlapiGetFieldValue('name'))) {
            uiShowError('Provide a value for "Name" field.'.tl());
            return false;
        }
        // check name
        var cols = [];
        cols.push({
            columnId : 'name',
            operator : 'is',
            value : nlapiGetFieldValue('name')
        });
        var internalId = nlapiGetFieldValue('id');
        if (ssapiHasDuplicates('customrecord_ss_subscription_link', cols, internalId)) {
            uiShowError('The name already exists in the database.'.tl());
            return false;
        }
        // 
        if (ssapiHasNoValue(nlapiGetFieldValue('custrecord_sssl_ss_rec'))) {
            uiShowError('Provide a value for "Related Record" field.'.tl());
            return false;
        }
        if (ssapiHasNoValue(nlapiGetFieldValue('custrecord_sssl_field'))) {
            uiShowError('Provide a value for "Related Record Field" field.'.tl());
            return false;
        }

        // Issue: 217397 [SuiteSocial] Subscription Link > New Subscription link
        // code for updating related records is now in the client page init of
        // SS record
        // the list of field options for a selected record display other fields
        // from other records.
        // add checking to reject those invalid field options
        // var relatedRecordScriptId =
        // nlapiGetFieldValue('custrecord_sssl_ss_rec_scrrectype_si');
        // logger.log('relatedRecordScriptId=' + relatedRecordScriptId);
        // var record = nlapiCreateRecord(relatedRecordScriptId);
        // var fields = record.getAllFields();
        // var fieldId =
        // nlapiGetFieldValue('custrecord_sssl_field_scriptid').toLowerCase();
        // logger.log('fieldId=' + fieldId);
        // var found = false;
        // for (var i = 0; i < fields.length; i++) {
        // logger.log('fields[i]=' + fields[i]);
        // if (fields[i].toLowerCase() == fieldId) {
        // found = true;
        // break;
        // }
        // }
        // if (found === false) {
        // uiShowError('The field "' +
        // nlapiGetFieldText('custrecord_sssl_field') + '" does not belong to
        // Related Record "' + nlapiGetFieldText('custrecord_sssl_ss_rec') +
        // '".');
        // return false;
        // }

        cols = [];
        cols.push({
            columnId : 'custrecord_sssl_ss_rec',
            operator : OP_ANYOF,
            value : nlapiGetFieldValue('custrecord_sssl_ss_rec')
        });
        cols.push({
            columnId : 'custrecord_sssl_rel_ss_rec',
            operator : OP_ANYOF,
            value : nlapiGetFieldValue('custrecord_sssl_rel_ss_rec')
        });
        cols.push({
            columnId : 'custrecord_sssl_field',
            operator : OP_ANYOF,
            value : nlapiGetFieldValue('custrecord_sssl_field')
        });

        if (ssapiHasDuplicates('customrecord_ss_subscription_link', cols, internalId)) {
            uiShowError('The combination of "SuiteSocial Record", "Related Record" and "Related Record Field" already exists in the database.'.tl());
            return false;
        }

        return true;

    } catch (e) {
        ssapiHandleError(e);

        return false;
    }
}

function pageInit() {
    // var logger = new ssobjLogger('pageInit');
    // nlapiSetFieldDisplay('customform', false);
    //    
    //    
    // var id = nlapiGetFieldValue('custrecord_sssl_rel_ss_rec_srcrectype_re')
    // var name = nlapiGetFieldText('custrecord_sssl_rel_ss_rec_srcrectype_re');
    // logger.log("name" + name);
    // logger.log("id')=" + id);
    // // get record id
    // ssapiCreateRelatedRecordsPerRecord(id, name);
    setInterval('ssapiHideNewInOptionList();', 300);
    Ext.get('custrecord_sssl_ss_rec_popup_new').dom.style.display = 'none';
    Ext.get('custrecord_sssl_record_field_popup_new').dom.style.display = 'none';
}

// /**
// * @param {String} type Sublist internal id
// * @param {String} name Field internal id
// * @return {void}
// */
function clientPostSourcing(type, name) {
    // var logger = new ssobjLogger('clientPostSourcing');
    // logger.log('name=' + name);
    // // get the number of options in the Field field. If there is only one,
    // set it
    // if (name == 'custrecord_sssl_field') {
    // var fld = nlapiGetField(name);
    // var options = fld.getSelectOptions();
    // if (options.length == 1) {
    // nlapiSetFieldValue(name, options[0].getId());
    // }
    // }
}

/**
 * This is used in translating the menu item Delete Including Dependent Records
 * It needs to be called from an interval since the menu item is created on
 * hover
 */
function ssapiTranslateDeleteIncludingDependentRecords() {
    var link = jQuery('span:contains("Delete Including Dependent Records")')[0];
    if (link) {
        link.innerHTML = link.innerHTML.tl();
        clearInterval(ssobjGlobal.translateDeleteIncludingDependentRecords);
    }
}
ssobjGlobal.translateDeleteIncludingDependentRecords = setInterval('ssapiTranslateDeleteIncludingDependentRecords();', 500);
