
// ******************** START of auto-generated constants
R_SS_RECORD_SUBSCRIPTION_ROLE = {
    RECORD_SUBSCRIPTION: 'custrecord_ssrsr_record_subscription',
    ROLE: 'custrecord_ssrsr_role'
};

R_SS_FIELD = {
    RECORD: 'custrecord_ssf_record',
    NAME: 'name',
    RECORD_FIELD: 'custrecord_ssf_record_field',
    SCRIPT_ID: 'custrecord_ssf_script_id',
    DATA_TYPE_NAME: 'custrecord_ssf_data_type_name',
    SELECT_LIST_OR_RECORD: 'custrecord_ssf_select_list_or_record'
};

R_SS_RELATED_RECORD = {
    RECORD_TYPE: 'custrecord_ssrr_record_type',
    RELATED_SS_RECORD: 'custrecord_ssrr_related_ss_record',
    RELATED_SS_RECORD_TYPE: 'custrecord_ssrr_related_ss_record_type',
    SS_RECORD: 'custrecord_ssrr_ss_record',
    NAME: 'name'
};

R_SS_INTERNAL_SETTING = {
    COMMENT: 'custrecord_ss_internal_setting_comment',
    VALUE: 'custrecord_ss_internal_setting_value',
    NAME: 'name'
};

R_SS_SCRIPTED_RECORD = {
    FIELD_SCRIPT_ID_LIST: 'custrecord_ssr_field_script_id_list',
    SCRIPT_ID: 'custrecord_ssr_script_id',
    SCRIPTED_RECORD: 'custrecord_ssr_scripted_record'
};


R = {
    SS_FIELD: 'customrecord_suitesocial_field',
    SS_RELATED_RECORD: 'customrecord_ssr_ss_related_record',
    SS_INTERNAL_SETTING: 'customrecord_ss_internal_setting',
    SS_SCRIPTED_RECORD: 'customrecord_suitesocial_scripted_record',
    SS_RECORD_SUBSCRIPTION_ROLE: 'customrecord_ss_record_subscription_role'
};
// ******************** END of auto-generated constants

function ssapiSearchInternalSetting(id, filters, columns){
//	throw filters[0].toString();
    return ssapiSearchRecord(R.SS_INTERNAL_SETTING, id, filters, columns);
}

function ssapiSearchDefaultRecordSubscription(id, filters, columns){
    return ssapiSearchRecord(REC_DEFAULT_AUTOSUB_TYPE, id, filters, columns);
}

function ssapiSearchDefaultRecordSubscriptionRole(id, filters, columns){
    return ssapiSearchRecord(REC_DEFAULT_AUTOSUB_ROLE, id, filters, columns);
}

function ssapiSearchRecordSubscription(id, filters, columns){
    return ssapiSearchRecord(REC_AUTOSUB_TYPE, id, filters, columns);
}

function ssapiSearchRecordSubscriptionRole(id, filters, columns){
    return ssapiSearchRecord(R.SS_RECORD_SUBSCRIPTION_ROLE, id, filters, columns);
}


// ******************** START of auto-generated wrapper codes

function SsRecordSubscriptionRole(){
    // record                                                                                        
    var _record;
    // long record name or actual record name                                                                                         
    var _longRecordId = 'customrecord_ss_record_subscription_role';
    _record = nlapiCreateRecord('customrecord_ss_record_subscription_role', {
        recordmode: 'dynamic'
    });
    this.getType = function(fieldId){
        return this.fields[fieldId].fieldtype;
    };
    this.getRecordType = function(){
        return _longRecordId;
    };
    this.createRecord = function(){
        _record = nlapiCreateRecord('customrecord_ss_record_subscription_role', {
            recordmode: 'dynamic'
        });
        return _record;
    };
    this.getRecord = function(){
        return _record;
    };
    this.loadRecord = function(id){
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };
    
    this.setRecord = function(record){
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };
    
    this.submitRecord = function(){
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return nlapiSubmitRecord(_record);
    };
    this.deleteRecord = function(){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        nlapiDeleteRecord('customrecord_ss_record_subscription_role', _record.getId());
        // TODO: delete also children                                                                                                                
        _record = null;
    };
    // private getter and setter methods                                                                                              
    this.getFieldValue = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldValue(fieldId);
    };
    this.setFieldValue = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldValue(fieldId, value);
    };
    this.getFieldText = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldText(fieldId);
    };
    this.setFieldText = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldText(fieldId, value);
    };
    
    // start of record type specific getter and setter methods                                                                        
    
    // @param {SELECT} v Role null                                        
    this.setRoleValue = function(value){
        this.setFieldValue('custrecord_ssrsr_role', value);
    };
    this.getRoleValue = function(){
        return this.getFieldValue('custrecord_ssrsr_role');
    };
    // @return {string}                                        
    this.getRoleText = function(){
        return this.getFieldText('custrecord_ssrsr_role');
    };
    this.setRoleText = function(value){
        this.setFieldText('custrecord_ssrsr_role', value);
    };
    
    // @param {SELECT} v Record Subscription null                                        
    this.setRecordSubscriptionValue = function(value){
        this.setFieldValue('custrecord_ssrsr_record_subscription', value);
    };
    this.getRecordSubscriptionValue = function(){
        return this.getFieldValue('custrecord_ssrsr_record_subscription');
    };
    // @return {string}                                        
    this.getRecordSubscriptionText = function(){
        return this.getFieldText('custrecord_ssrsr_record_subscription');
    };
    this.setRecordSubscriptionText = function(value){
        this.setFieldText('custrecord_ssrsr_record_subscription', value);
    };
}

function SsScriptedRecord(){
    // record                                                                                        
    var _record;
    // long record name or actual record name                                                                                         
    var _longRecordId = 'customrecord_suitesocial_scripted_record';
    _record = nlapiCreateRecord('customrecord_suitesocial_scripted_record', {
        recordmode: 'dynamic'
    });
    this.getType = function(fieldId){
        return this.fields[fieldId].fieldtype;
    };
    this.getRecordType = function(){
        return _longRecordId;
    };
    this.createRecord = function(){
        _record = nlapiCreateRecord('customrecord_suitesocial_scripted_record', {
            recordmode: 'dynamic'
        });
        return _record;
    };
    this.getRecord = function(){
        return _record;
    };
    this.loadRecord = function(id){
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };
    
    this.setRecord = function(record){
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };
    
    this.submitRecord = function(){
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return nlapiSubmitRecord(_record);
    };
    this.deleteRecord = function(){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        nlapiDeleteRecord('customrecord_suitesocial_scripted_record', _record.getId());
        // TODO: delete also children                                                                                                                
        _record = null;
    };
    // private getter and setter methods                                                                                              
    this.getFieldValue = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldValue(fieldId);
    };
    this.setFieldValue = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldValue(fieldId, value);
    };
    this.getFieldText = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldText(fieldId);
    };
    this.setFieldText = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldText(fieldId, value);
    };
    
    // start of record type specific getter and setter methods                                                                        
    
    // @param {freeform} v Name Name                                        
    this.setScriptedRecordValue = function(value){
        this.setFieldValue('custrecord_ssr_scripted_record', value);
    };
    this.getScriptedRecordValue = function(){
        return this.getFieldValue('custrecord_ssr_scripted_record');
    };
    
    this.setScriptedRecordText = function(value){
        this.setFieldText('custrecord_ssr_scripted_record', value);
    };
    this.getScriptedRecordText = function(){
        return this.getFieldText('custrecord_ssr_scripted_record');
    };
    
    // @param {TEXT} v Field Script Id List Comma-separated list of script id of the scripted record                                        
    this.setFieldScriptIdListValue = function(value){
        this.setFieldValue('custrecord_ssr_field_script_id_list', value);
    };
    this.getFieldScriptIdListValue = function(){
        return this.getFieldValue('custrecord_ssr_field_script_id_list');
    };
    
    // @param {TEXT} v Script Id null                                        
    this.setScriptIdValue = function(value){
        this.setFieldValue('custrecord_ssr_script_id', value);
    };
    this.getScriptIdValue = function(){
        return this.getFieldValue('custrecord_ssr_script_id');
    };
}


function SsInternalSetting(){
    // record                                                                                        
    var _record;
    // long record name or actual record name                                                                                         
    var _longRecordId = 'customrecord_ss_internal_setting';
    _record = nlapiCreateRecord('customrecord_ss_internal_setting', {
        recordmode: 'dynamic'
    });
    
    this.getType = function(fieldId){
        return this.fields[fieldId].fieldtype;
    };
    this.getRecordType = function(){
        return _longRecordId;
    };
    this.createRecord = function(){
        _record = nlapiCreateRecord('customrecord_ss_internal_setting', {
            recordmode: 'dynamic'
        });
        return _record;
    };
    this.getRecord = function(){
        return _record;
    };
    this.loadRecord = function(id){
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };
    
    this.setRecord = function(record){
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };
    
    this.submitRecord = function(){
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return nlapiSubmitRecord(_record);
    };
    this.deleteRecord = function(){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        nlapiDeleteRecord('customrecord_ss_internal_setting', _record.getId());
        // TODO: delete also children                                                                                                                
        _record = null;
    };
    // private getter and setter methods                                                                                              
    this.getFieldValue = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldValue(fieldId);
    };
    this.setFieldValue = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldValue(fieldId, value);
    };
    this.getFieldText = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldText(fieldId);
    };
    this.setFieldText = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldText(fieldId, value);
    };
    
    // start of record type specific getter and setter methods                                                                        
    
    // @param {TEXT} v Comment null                                        
    this.setCommentValue = function(value){
        this.setFieldValue('custrecord_ss_internal_setting_comment', value);
    };
    this.getCommentValue = function(){
        return this.getFieldValue('custrecord_ss_internal_setting_comment');
    };
    
    // @param {freeform} v Name Name                                        
    this.setNameValue = function(value){
        this.setFieldValue('name', value);
    };
    this.getNameValue = function(){
        return this.getFieldValue('name');
    };
    
    // @param {CLOBTEXT} v Setting Value null                                        
    this.setValueValue = function(value){
        this.setFieldValue('custrecord_ss_internal_setting_value', value);
    };
    this.getValueValue = function(){
        return this.getFieldValue('custrecord_ss_internal_setting_value');
    };
}


function SsRelatedRecord(){
    // record                                                                                        
    var _record;
    // long record name or actual record name                                                                                         
    var _longRecordId = 'customrecord_ssr_ss_related_record';
    _record = nlapiCreateRecord('customrecord_ssr_ss_related_record', {
        recordmode: 'dynamic'
    });
    //    this.fields = {
    //        "name": {
    //            "recordid": "401",
    //            "scriptid": "name",
    //            "fieldtype": "Free-Form Text",
    //            "label": "Name"
    //        },
    //        "custrecord_ssrr_ss_record": {
    //            "scriptid": "custrecord_ssrr_ss_record",
    //            "fieldtype": "List/Record",
    //            "label": "SuiteSocial Record",
    //            "recordid": "401",
    //            "selectrecordname": "SuiteSocial Record",
    //            "selectrecordscriptid": "customrecord_suitesocial_record",
    //            "selectrecordid": "419"
    //        },
    //        "custrecord_ssrr_related_ss_record": {
    //            "scriptid": "custrecord_ssrr_related_ss_record",
    //            "fieldtype": "List/Record",
    //            "label": "Related SuiteSocial Record",
    //            "recordid": "401",
    //            "selectrecordname": "SuiteSocial Record",
    //            "selectrecordscriptid": "customrecord_suitesocial_record",
    //            "selectrecordid": "419"
    //        },
    //        "custrecord_ssrr_related_ss_record_type": {
    //            "scriptid": "custrecord_ssrr_related_ss_record_type",
    //            "fieldtype": "List/Record",
    //            "label": "Related SuiteSocial Record Type",
    //            "recordid": "401",
    //            "selectrecordname": "Scripted Record Type",
    //            "selectrecordscriptid": null,
    //            "selectrecordid": -125
    //        },
    //        "custrecord_ssrr_record_type": {
    //            "scriptid": "custrecord_ssrr_record_type",
    //            "fieldtype": "List/Record",
    //            "label": "SuiteSocial Record Type",
    //            "recordid": "401",
    //            "selectrecordname": "Scripted Record Type",
    //            "selectrecordscriptid": null,
    //            "selectrecordid": -125
    //        }
    //    };
    this.getType = function(fieldId){
        return this.fields[fieldId].fieldtype;
    };
    this.getRecordType = function(){
        return _longRecordId;
    };
    this.createRecord = function(){
        _record = nlapiCreateRecord('customrecord_ssr_ss_related_record', {
            recordmode: 'dynamic'
        });
        return _record;
    };
    this.getRecord = function(){
        return _record;
    };
    this.loadRecord = function(id){
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };
    
    this.setRecord = function(record){
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };
    
    this.submitRecord = function(){
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return nlapiSubmitRecord(_record);
    };
    this.deleteRecord = function(){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        nlapiDeleteRecord('customrecord_ssr_ss_related_record', _record.getId());
        // TODO: delete also children                                                                                                                
        _record = null;
    };
    // private getter and setter methods                                                                                              
    this.getFieldValue = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldValue(fieldId);
    };
    this.setFieldValue = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldValue(fieldId, value);
    };
    this.getFieldText = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldText(fieldId);
    };
    this.setFieldText = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldText(fieldId, value);
    };
    
    // start of record type specific getter and setter methods                                                                        
    
    // @param {freeform} v Name Name                                        
    this.setNameValue = function(value){
        this.setFieldValue('name', value);
    };
    this.getNameValue = function(){
        return this.getFieldValue('name');
    };
    
    // @param {SELECT} v SuiteSocial Record Type null                                        
    this.setRecordTypeValue = function(value){
        this.setFieldValue('custrecord_ssrr_record_type', value);
    };
    this.getRecordTypeValue = function(){
        return this.getFieldValue('custrecord_ssrr_record_type');
    };
    // @return {string}                                        
    this.getRecordTypeText = function(){
        return this.getFieldText('custrecord_ssrr_record_type');
    };
    this.setRecordTypeText = function(value){
        this.setFieldText('custrecord_ssrr_record_type', value);
    };
    
    // @param {SELECT} v Related SuiteSocial Record Related record is the parent record                                        
    this.setRelatedSsRecordValue = function(value){
        this.setFieldValue('custrecord_ssrr_related_ss_record', value);
    };
    this.getRelatedSsRecordValue = function(){
        return this.getFieldValue('custrecord_ssrr_related_ss_record');
    };
    // @return {string}                                        
    this.getRelatedSsRecordText = function(){
        return this.getFieldText('custrecord_ssrr_related_ss_record');
    };
    this.setRelatedSsRecordText = function(value){
        this.setFieldText('custrecord_ssrr_related_ss_record', value);
    };
    
    // @param {SELECT} v Related SuiteSocial Record Type null                                        
    this.setRelatedSsRecordTypeValue = function(value){
        this.setFieldValue('custrecord_ssrr_related_ss_record_type', value);
    };
    this.getRelatedSsRecordTypeValue = function(){
        return this.getFieldValue('custrecord_ssrr_related_ss_record_type');
    };
    // @return {string}                                        
    this.getRelatedSsRecordTypeText = function(){
        return this.getFieldText('custrecord_ssrr_related_ss_record_type');
    };
    this.setRelatedSsRecordTypeText = function(value){
        this.setFieldText('custrecord_ssrr_related_ss_record_type', value);
    };
    
    // @param {SELECT} v SuiteSocial Record null                                        
    this.setSsRecordValue = function(value){
        this.setFieldValue('custrecord_ssrr_ss_record', value);
    };
    this.getSsRecordValue = function(){
        return this.getFieldValue('custrecord_ssrr_ss_record');
    };
    // @return {string}                                        
    this.getSsRecordText = function(){
        return this.getFieldText('custrecord_ssrr_ss_record');
    };
    this.setSsRecordText = function(value){
        this.setFieldText('custrecord_ssrr_ss_record', value);
    };
}

function SsField(){
    // record                                                                                        
    var _record;
    // long record name or actual record name                                                                                         
    var _longRecordId = 'customrecord_suitesocial_field';
    _record = nlapiCreateRecord('customrecord_suitesocial_field', {
        recordmode: 'dynamic'
    });
    //    this.fields = {
    //        "name": {
    //            "recordid": "425",
    //            "scriptid": "name",
    //            "fieldtype": "Free-Form Text",
    //            "label": "Name"
    //        },
    //        "custrecord_ssf_record_field": {
    //            "scriptid": "custrecord_ssf_record_field",
    //            "fieldtype": "List/Record",
    //            "label": "Record Field",
    //            "recordid": "425",
    //            "selectrecordname": "Field",
    //            "selectrecordscriptid": "customfield",
    //            "selectrecordid": -124
    //        },
    //        "custrecord_ssf_record": {
    //            "scriptid": "custrecord_ssf_record",
    //            "fieldtype": "List/Record",
    //            "label": "Record",
    //            "recordid": "425",
    //            "selectrecordname": "Scripted Record Type",
    //            "selectrecordscriptid": null,
    //            "selectrecordid": -125
    //        },
    //        "custrecord_ssf_data_type_name": {
    //            "scriptid": "custrecord_ssf_data_type_name",
    //            "fieldtype": "Free-Form Text",
    //            "label": "Data Type Name",
    //            "recordid": "425"
    //        },
    //        "custrecord_ssf_select_list_or_record": {
    //            "scriptid": "custrecord_ssf_select_list_or_record",
    //            "fieldtype": "List/Record",
    //            "label": "Select List or Record",
    //            "recordid": "425",
    //            "selectrecordname": "Record Type",
    //            "selectrecordscriptid": null,
    //            "selectrecordid": -123
    //        }
    //    };
    this.getType = function(fieldId){
        return this.fields[fieldId].fieldtype;
    };
    this.getRecordType = function(){
        return _longRecordId;
    };
    this.createRecord = function(){
        _record = nlapiCreateRecord('customrecord_suitesocial_field', {
            recordmode: 'dynamic'
        });
        return _record;
    };
    this.getRecord = function(){
        return _record;
    };
    this.loadRecord = function(id){
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };
    
    this.setRecord = function(record){
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };
    
    this.submitRecord = function(){
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return nlapiSubmitRecord(_record);
    };
    this.deleteRecord = function(){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        nlapiDeleteRecord('customrecord_suitesocial_field', _record.getId());
        // TODO: delete also children                                                                                                                
        _record = null;
    };
    // private getter and setter methods                                                                                              
    this.getFieldValue = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldValue(fieldId);
    };
    this.setFieldValue = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldValue(fieldId, value);
    };
    this.getFieldText = function(fieldId){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        return _record.getFieldText(fieldId);
    };
    this.setFieldText = function(fieldId, value){
        // there should be a nlobjrecord                                                                                                 
        if (_record === null) {
            throw 'RECORD IS NULL';
        }
        _record.setFieldText(fieldId, value);
    };
    
    // start of record type specific getter and setter methods                                                                        
    
    // @param {TEXT} v Data Type Name null                                        
    this.setDataTypeNameValue = function(value){
        this.setFieldValue('custrecord_ssf_data_type_name', value);
    };
    this.getDataTypeNameValue = function(){
        return this.getFieldValue('custrecord_ssf_data_type_name');
    };
    
    // @param {freeform} v Name Name                                        
    this.setNameValue = function(value){
        this.setFieldValue('name', value);
    };
    this.getNameValue = function(){
        return this.getFieldValue('name');
    };
    
    // @param {SELECT} v Select List or Record null                                        
    this.setSelectListOrRecordValue = function(value){
        this.setFieldValue('custrecord_ssf_select_list_or_record', value);
    };
    this.getSelectListOrRecordValue = function(){
        return this.getFieldValue('custrecord_ssf_select_list_or_record');
    };
    // @return {string}                                        
    this.getSelectListOrRecordText = function(){
        return this.getFieldText('custrecord_ssf_select_list_or_record');
    };
    this.setSelectListOrRecordText = function(value){
        this.setFieldText('custrecord_ssf_select_list_or_record', value);
    };
    
    // @param {SELECT} v Record Field null                                        
    this.setRecordFieldValue = function(value){
        this.setFieldValue('custrecord_ssf_record_field', value);
    };
    this.getRecordFieldValue = function(){
        return this.getFieldValue('custrecord_ssf_record_field');
    };
    // @return {string}                                        
    this.getRecordFieldText = function(){
        return this.getFieldText('custrecord_ssf_record_field');
    };
    this.setRecordFieldText = function(value){
        this.setFieldText('custrecord_ssf_record_field', value);
    };
    
    // @param {SELECT} v Record null                                        
    this.setRecordValue = function(value){
        this.setFieldValue('custrecord_ssf_record', value);
    };
    this.getRecordValue = function(){
        return this.getFieldValue('custrecord_ssf_record');
    };
    // @return {string}                                        
    this.getRecordText = function(){
        return this.getFieldText('custrecord_ssf_record');
    };
    this.setRecordText = function(value){
        this.setFieldText('custrecord_ssf_record', value);
    };
}

function apiCreateDataAccessObject(recordId){
    if (recordId == "customrecord_suitesocial_field") {
        return new SsField();
    }
    throw "not found. recordId=" + recordId;
}

// ******************** END of auto-generated wrapper codes
// 959
////////////////////////////////////////////////
// lists
var ROLE_LIST_ID = '-118';


// common
var FLD_INTERNAL_ID = 'internalid';
var FLD_ISINACTIVE = 'isinactive';

var REC_FIELD = 'customrecord_suitesocial_field';
var FLD_FIELD_FIELD = 'custrecord_ssf_field';
var FLD_FIELD_RECORD = 'custrecord_ssf_record';
var FLD_FIELD_DATA_TYPE_NAME = 'custrecord_ssf_data_type_name';
var FLD_FIELD_SELECT_RECORD_TYPE = 'custrecord_ssf_select_record_type';

var REC_RELATED_RECORD = 'customrecord_ssr_ss_related_record';
var REC_SUBSCRIPTION_LINK = 'customrecord_ss_subscription_link';

var REC_ADDED_AUTOSUB = 'customrecord_added_autosub';
var FLD_ADDED_AUTOSUB_PROFILE_ID = 'custrecord_added_autosub_profile_id';
var FLD_ADDED_AUTOSUB_TYPE_ID = 'custrecord_added_autosub_id';


var REC_DEFAULT_AUTOSUB_ROLE = 'customrecord_ss_def_autosub_role';
var FLD_DEFAULT_AUTOSUB_ROLE_PARENT = 'custrecord_ss_def_autosub_role_parent';
var FLD_DEFAULT_AUTOSUB_ROLE_ID = 'custrecord_ss_def_autosub_role_id';
// Issue: 212125 The error below is displayed when going to assistant step 
var REC_DEFAULT_AUTOSUB_TYPE = 'customrecord_ss_def_autosub_type';
var FLD_DEFAULT_AUTOSUB_TYPE_NAME = 'name';
var FLD_DEFAULT_AUTOSUB_TYPE_RT = 'custrecord_ss_def_autosub_type_rt';
var FLD_DEFAULT_AUTOSUB_TYPE_RSI = 'custrecord_ss_def_autosub_type_rsi';
var FLD_DEFAULT_AUTOSUB_TYPE_FLD = 'custrecord_ss_def_autosub_type_fld';




var REC_RECORD_FIELD_DEF = 'customrecord_record_fld_definition';
var FLD_RECORD_FIELD_DEF_SCRIPTED_RECORD = 'custrecord_rtfd_scripted_rec';
var FLD_RECORD_FIELD_DEF_SCRIPTED_RECORD_BASE_TYPE = 'custrecord_rtfd_scripted_rec_base_type';
var FLD_RECORD_FIELD_DEF_FIELD = 'custrecord_rtfd_fld';
var FLD_RECORD_FIELD_DEF_FIELD_SCRIPT_ID = 'custrecord_rtfd_fld_script_id';
var FLD_RECORD_FIELD_DEF_FIELD_DATA_TYPE = 'custrecord_rtfd_fld_data_type';
var FLD_RECORD_FIELD_DEF_FIELD_RECORD_TYPE = 'custrecord_rtfd_fld_rec_type';
var FLD_RECORD_FIELD_DEF_SELECT_FIELD = 'custrecord_rtfd_select_fld';


var REC_ROLE = 'customrecord_suitesocial_role';
var FLD_ROLE_ID = 'custrecord_suitesocial_role_id';


var REC_SETUP_TASK = 'customrecord_setup_task';
var FLD_SETUP_TASK_STATUS = 'custrecord_setup_task_status';
var FLD_SETUP_TASK_LAST_MODIFIED = 'custrecord_setup_task_last_modified';
var FLD_SETUP_TASK_SORT_ORDER = 'custrecord_setup_task_sort_order';


var REC_AUTOPOST = 'customrecord_autopost';
// Issue: 212124 Admin assistant should display current setup information in the S
var FLD_AUTOPOST_RECORD = 'custrecord_autopost_record';
var FLD_AUTOPOST_CHANNEL = 'custrecord_autopost_channel';
var FLD_AUTOPOST_ICON = 'custrecord_autopost_icon';
var FLD_AUTOPOST_MESSAGE = 'custrecord_autopost_message';
var FLD_AUTOPOST_RECIPIENT = 'custrecord_autopost_recipient';
var FLD_AUTOPOST_RECORDTYPE = 'custrecord_autopost_recordtype';
var FLD_AUTOPOST_SCRIPTTYPE = 'custrecord_autopost_scripttype';
var FLD_AUTOPOST_SEARCH = 'custrecord_autopost_search';
var FLD_AUTOPOST_WEEKENDS = 'custrecord_autopost_weekends';
var FLD_AUTOPOST_NAME = 'name';




var REC_AUTOPOST_SCHEDULE = 'customrecord_autopost_schedule';
var FLD_AUTOPOST_SCHEDULE_AUTOPOST_SCHED_AUTOPOST = 'custrecord_autopost_sched_autopost';
var FLD_AUTOPOST_SCHEDULE_AUTOPOST_SCHED_STOD = 'custrecord_autopost_sched_stod';
var FLD_AUTOPOST_SCHEDULE_AUTOPOST_SCHED_TOD = 'custrecord_autopost_sched_tod';


var REC_DISCUSSION = 'customrecord_discussion';
var FLD_DISCUSSION_CHANNEL = 'custrecord_discussion_channel';
var FLD_DISCUSSION_DESCRIPTION = 'custrecord_discussion_description';
var FLD_DISCUSSION_NAME = 'name';


var REC_NEWSFEED = 'customrecord_newsfeed';
var FLD_NEWSFEED_AUTOPOST = 'custrecord_newsfeed_autopost';
var FLD_NEWSFEED_AUTOPOST_FIELD = 'custrecord_newsfeed_autopost_field';
var FLD_NEWSFEED_AUTOPOST_RECORD = 'custrecord_newsfeed_autopost_record';
var FLD_NEWSFEED_CHANNEL = 'custrecord_newsfeed_channel';
var FLD_NEWSFEED_CONTACT = 'custrecord_newsfeed_contact';
var FLD_NEWSFEED_CUSTOMER = 'custrecord_newsfeed_customer';
var FLD_NEWSFEED_CUSTOMRECORD = 'custrecord_newsfeed_customrecord';
var FLD_NEWSFEED_CUSTOMTYPE = 'custrecord_newsfeed_customtype';
var FLD_NEWSFEED_EMPLOYEE = 'custrecord_newsfeed_employee';
var FLD_NEWSFEED_ICON = 'custrecord_newsfeed_icon';
var FLD_NEWSFEED_ISSUE = 'custrecord_newsfeed_issue';
var FLD_NEWSFEED_JOB = 'custrecord_newsfeed_job';
var FLD_NEWSFEED_MESSAGE = 'custrecord_newsfeed_message';
var FLD_NEWSFEED_OPPORTUNITY = 'custrecord_newsfeed_opportunity';
var FLD_NEWSFEED_PARENT = 'custrecord_newsfeed_parent';
var FLD_NEWSFEED_PARTNER = 'custrecord_newsfeed_partner';
var FLD_NEWSFEED_RECIPIENT = 'custrecord_newsfeed_recipient';
var FLD_NEWSFEED_SUPPORTCASE = 'custrecord_newsfeed_supportcase';
var FLD_NEWSFEED_TRANSACTION = 'custrecord_newsfeed_transaction';
var FLD_NEWSFEED_VENDOR = 'custrecord_newsfeed_vendor';
var FLD_NEWSFEED_WORKFLOW = 'custrecord_newsfeed_workflow';


var REC_NEWSFEED_CHANNEL = 'customrecord_newsfeed_channel';
var FLD_NEWSFEED_CHANNEL_AUTO = 'custrecord_newsfeed_channel_auto';
var FLD_NEWSFEED_CHANNEL_COMMENTS = 'custrecord_newsfeed_channel_comments';
var FLD_NEWSFEED_CHANNEL_DEPT = 'custrecord_newsfeed_channel_dept';
var FLD_NEWSFEED_CHANNEL_EMP = 'custrecord_newsfeed_channel_emp';
var FLD_NEWSFEED_CHANNEL_HIDDEN = 'custrecord_newsfeed_channel_hidden';
var FLD_NEWSFEED_CHANNEL_LOC = 'custrecord_newsfeed_channel_loc';
var FLD_NEWSFEED_CHANNEL_POSTERS = 'custrecord_newsfeed_channel_posters';
var FLD_NEWSFEED_CHANNEL_ROLE = 'custrecord_newsfeed_channel_role';
var FLD_NEWSFEED_CHANNEL_NAME = 'name';


var REC_NEWSFEED_ICON = 'customrecord_newsfeed_icon';
var FLD_NEWSFEED_ICON_ICON_IMAGE = 'custrecord_icon_image';
var FLD_NEWSFEED_ICON_NAME = 'name';


var REC_AUTOPOST_FIELD = 'customrecord_suitesocial_autopost_field';
var FLD_AUTOPOST_FLD_DT = 'custrecord_suitesocial_autopost_fld_dt';
var FLD_AUTOPOST_FLD_FLD = 'custrecord_suitesocial_autopost_fld_fld';
var FLD_AUTOPOST_FLD_FT = 'custrecord_suitesocial_autopost_fld_ft';
var FLD_AUTOPOST_FLD_ID = 'custrecord_suitesocial_autopost_fld_id';
var FLD_AUTOPOST_FLD_REC = 'custrecord_suitesocial_autopost_fld_rec';
var FLD_AUTOPOST_FLD_RT = 'custrecord_suitesocial_autopost_fld_rt';


var REC_AUTOSUBSCRIBE = 'customrecord_suitesocial_autosubscribe';
var FLD_AUTOSUBSCRIBE_SUB = 'custrecord_suitesocial_autosubscribe_sub';
var FLD_AUTOSUBSCRIBE_TYP = 'custrecord_suitesocial_autosubscribe_typ';


var REC_AUTOSUB_TYPE = 'customrecord_suitesocial_autosub_type';
var FLD_AUTOSUB_TYPE_FID = 'custrecord_suitesocial_autosub_type_fid';
var FLD_AUTOSUB_TYPE_FLD = 'custrecord_suitesocial_autosub_type_fld';
var FLD_AUTOSUB_TYPE_FT = 'custrecord_suitesocial_autosub_type_ft';
var FLD_AUTOSUB_TYPE_REC = 'custrecord_suitesocial_autosub_type_rec';
var FLD_AUTOSUB_TYPE_RSI = 'custrecord_suitesocial_autosub_type_rsi';
var FLD_AUTOSUB_TYPE_RT = 'custrecord_suitesocial_autosub_type_rt';
var FLD_AUTOSUB_TYPE_NAME = 'name';


var REC_COLLEAGUE = 'customrecord_suitesocial_colleague';
var FLD_COLLEAGUE_ALRT = 'custrecord_suitesocial_colleague_alrt';
var FLD_COLLEAGUE_EMP = 'custrecord_suitesocial_colleague_emp';
var FLD_COLLEAGUE_PROFILE = 'custrecord_suitesocial_colleague_profile';
var FLD_COLLEAGUE_SUB = 'custrecord_suitesocial_colleague_sub';


var REC_DIGEST_SCHEDULE = 'customrecord_suitesocial_digest_schedule';
var FLD_DIGEST_SCHED_PROF = 'custrecord_suitesocial_digest_sched_prof';
var FLD_DIGEST_SCHED_STOD = 'custrecord_suitesocial_digest_sched_stod';
var FLD_DIGEST_SCHED_TOD = 'custrecord_suitesocial_digest_sched_tod';


var REC_NF_POST_LINK = 'customrecord_suitesocial_nf_post_link';
var FLD_NEWSFEED_POST_LINK_POST = 'custrecord_newsfeed_post_link_post';
var FLD_NEWSFEED_POST_LINK_RECIPIENT = 'custrecord_newsfeed_post_link_recipient';


var REC_PROFILE = 'customrecord_suitesocial_profile';
var FLD_PROFILE_ALRT_CMNT = 'custrecord_suitesocial_profile_alrt_cmnt';
var FLD_PROFILE_ALRT_DRCT = 'custrecord_suitesocial_profile_alrt_drct';
var FLD_PROFILE_ALRT_RCRD = 'custrecord_suitesocial_profile_alrt_rcrd';
var FLD_PROFILE_ALRT_STAT = 'custrecord_suitesocial_profile_alrt_stat';
var FLD_PROFILE_DIGEST = 'custrecord_suitesocial_profile_digest';
var FLD_PROFILE_EMP = 'custrecord_suitesocial_profile_emp';
var FLD_PROFILE_IMAGE = 'custrecord_suitesocial_profile_image';
var FLD_PROFILE_IMGHELP = 'custrecord_suitesocial_profile_imghelp';
var FLD_PROFILE_WKND_DGST = 'custrecord_suitesocial_profile_wknd_dgst';


var REC_PROFILE_CHANNEL = 'customrecord_suitesocial_profile_channel';
var FLD_PROFILE_CH_ALRT = 'custrecord_suitesocial_profile_ch_alrt';
var FLD_PROFILE_CH_CH = 'custrecord_suitesocial_profile_ch_ch';
var FLD_PROFILE_CH_DGST = 'custrecord_suitesocial_profile_ch_dgst';
var FLD_PROFILE_CH_PROF = 'custrecord_suitesocial_profile_ch_prof';
var FLD_PROFILE_CH_SUB = 'custrecord_suitesocial_profile_ch_sub';


var REC_RECORD = 'customrecord_suitesocial_record';
var FLD_RECORD_AUTOPOST = 'custrecord_suitesocial_record_autopost';
var FLD_RECORD_SCRIPTID = 'custrecord_suitesocial_record_scriptid';
var FLD_RECORD_TYPE = 'custrecord_suitesocial_record_type';
var FLD_RECORD_NAME = 'name';


var REC_STATUS = 'customrecord_suitesocial_status';
var FLD_STATUS_COMMENT = 'custrecord_suitesocial_status_comment';
var FLD_STATUS_EMP = 'custrecord_suitesocial_status_emp';
var FLD_STATUS_STATUS = 'custrecord_suitesocial_status_status';


var REC_STATUS_TYPE = 'customrecord_suitesocial_status_type';
var FLD_STATUS_TYPE_SUITESOCIAL_STATUS_ICON = 'custrecord_suitesocial_status_icon';
var FLD_STATUS_TYPE_NAME = 'name';


var REC_SUBSCRIPTION = 'customrecord_suitesocial_subscription';
var FLD_SUB_CUSTOMER = 'custrecord_suitesocial_sub_customer';
var FLD_SUB_CUSTOMRECORD = 'custrecord_suitesocial_sub_customrecord';
var FLD_SUB_CUSTOMTYPE = 'custrecord_suitesocial_sub_customtype';
var FLD_SUB_EMPLOYEE = 'custrecord_suitesocial_sub_employee';
var FLD_SUB_ISSUE = 'custrecord_suitesocial_sub_issue';
var FLD_SUB_JOB = 'custrecord_suitesocial_sub_job';
var FLD_SUB_OPPORTUNITY = 'custrecord_suitesocial_sub_opportunity';
var FLD_SUB_PARTNER = 'custrecord_suitesocial_sub_partner';
var FLD_SUB_SUBSCRIBER = 'custrecord_suitesocial_sub_subscriber';
var FLD_SUB_SUPPORTCASE = 'custrecord_suitesocial_sub_supportcase';
var FLD_SUB_TRANSACTION = 'custrecord_suitesocial_sub_transaction';
var FLD_SUB_VENDOR = 'custrecord_suitesocial_sub_vendor';




//operator constants
OP_HASKEYWORDS = 'haskeywords';
OP_AFTER = 'after';
OP_ALLOF = 'allof';
OP_ANY = 'any';
OP_ANYOF = 'anyof';
OP_BEFORE = 'before';
OP_BETWEEN = 'between';
OP_CONTAINS = 'contains';
OP_DOESNOTCONTAIN = 'doesnotcontain';
OP_DOESNOTSTARTWITH = 'doesnotstartwith';
OP_EQUALTO = 'equalto';
OP_GREATERTHAN = 'greaterthan';
OP_GREATERTHANOREQUALTO = 'greaterthanorequalto';
OP_IS = 'is';
OP_ISEMPTY = 'isempty';
OP_ISNOT = 'isnot';
OP_ISNOTEMPTY = 'isnotempty';
OP_LESSTHAN = 'lessthan';
OP_LESSTHANOREQUALTO = 'lessthanorequalto';
OP_NONEOF = 'noneof';
OP_NOTAFTER = 'notafter';
OP_NOTALLOF = 'notallof';
OP_NOTBEFORE = 'notbefore';
OP_NOTBETWEEN = 'notbetween';
OP_NOTEMPTY = 'notempty';
OP_NOTEQUALTO = 'notequalto';
OP_NOTGREATERTHAN = 'notgreaterthan';
OP_NOTGREATERTHANOREQUALTO = 'notgreaterthanorequalto';
OP_NOTLESSTHAN = 'notlessthan';
OP_NOTLESSTHANOREQUALTO = 'notlessthanorequalto';



