function socialPageInit() {
    // alert('socialPageInit');
    socialRenderFollowButtonMarkup();

}

function socialRenderFollowButtonMarkup() {
    try {
        socialSuiteletProcessAsync('socialGetFollowButtonMarkup', {
            recordTypeScriptId : nlapiGetRecordType(),
            recordId : nlapiGetRecordId()
        }, function(markup) {
            Ext.select('#_suitesocial_subscription').remove();
            Ext.get(Ext.select('.uir-page-title-secondline').elements[0]).insertHtml('beforeend', markup);
        });
    } catch (e) {
        ssapiHandleError(e);
    }
}

Ext.onReady(function() {
    // try {
    socialPageInit();
    // stickSetFieldLinks();
    // } catch (e) {
    // // uncomment next line in production
    // throw e;
    //
    // }
});