/**
 * @author tcaguioa
 */
// object for storing global variables
var ssobjGlobal = ssobjGlobal || {};

// No access SuiteSocial
var EXTERNAL_ROLES = [ 'Advanced Partner Center', 'Partner Center', 'Vendor Center' ];

// these settings apply to the user only (employee)
var readOnlyScriptIds = [ 'customrecord_suitesocial_autopost_field', 'customrecord_ss_saved_search_subs', REC_AUTOSUB_TYPE, REC_STATUS_TYPE, REC_NEWSFEED_CHANNEL, 'customrecord_ss_subscription_link', REC_RECORD, 'customrecord_ss_internal_setting' ];
var fullScriptIds = [ 'customrecord_ss_my_related_transaction_s', 'customrecord_exclude_track_field', 'customrecord_ss_my_saved_search_sub', REC_PROFILE, REC_PROFILE_CHANNEL, REC_COLLEAGUE, REC_AUTOSUBSCRIBE, REC_SUBSCRIPTION, REC_DIGEST_SCHEDULE, 'customrecord_ss_subscription_link_sub' ];
// customer roles will not have permission to the custom records below
var customerNoPermissionScriptIds = [ 'customrecord_suitesocial_colleague', 'customrecord_suitesocial_autopost_field', 'customrecord_exclude_track_field', 'customrecord_suitesocial_autosub_type', 'customrecord_suitesocial_autosubscribe', 'customrecord_ss_subscription_link', 'customrecord_ss_subscription_link_sub', 'customrecord_ss_my_related_transaction_s', 'customrecord_ss_saved_search_subs', 'customrecord_ss_my_saved_search_sub' ];

/**
 * Completes the rows in the custom record SuiteSocial Scripted Record. The list
 * preferably should be complete since it will be used in quickly getting the
 * scripted record type id of custom records
 */
function ssapiAddMissingScriptedRecordTypes() {
    var logger = new ssobjLogger(arguments);
    var columns = [ new nlobjSearchColumn('custrecord_ssr_script_id'), new nlobjSearchColumn('name') ];
    var results = ssapiSearchAllRecords('customrecord_suitesocial_scripted_record', null, null, columns);
    var scriptids = [];
    if (results !== null) {
        for (var i = 0; i < results.length; i++) {
            scriptids.push(results[i].getValue('custrecord_ssr_script_id'));
        }
    }

    var records = ssapiGetRecordTypes();
    for (i = 0; i < records.length; i++) {
        if (nlapiGetContext().getRemainingUsage() < 500) {
            // exit to prevent the assistant from getting execution usage error
            logger.error('exiting after ' + i + '...');
            return;
        }
        var record = records[i];
        var scriptid = record.scriptid;
        var name = record.name;

        // skip if already saved
        if (scriptids.indexOf(scriptid) > -1) {
            continue;
        }

        // skip if not custom record and not supported standard record type
        if (scriptid.indexOf('customrecord') == -1 && suitesocial.Helper.getSupportedRecordScriptIds().indexOf(scriptid) == -1) {
            continue;
        }

        // add
        var r = new SuiteSocialScriptedRecord();
        r.setNameValue(name);
        r.setScriptedRecordText(name);
        try {
            r.submitRecord();
        } catch (e) {
            // see Issue: 236260
            ssapiHandleError(e, 'ssapiAddMissingScriptedRecordTypes(). Unable to submitRecord record: ' + JSON.stringify(record));
        }
    }
}

/**
 * Returns an array of supported record names
 * 
 * @return {Array}
 */
function ssapiGetSupportedRecordsName() {
    var names = [];
    for (var i = 0; i < SUPPORTED_RECORDS_IDS.length; i++) {
        names.push(ssapiGetRecordNameById(SUPPORTED_RECORDS_IDS[i]));
    }
    return names;
}

/**
 * This is used in getting a setting. We expect at most a single row returned.
 * Returns null if no match is found
 * 
 * @param {Object}
 *        settingName
 */
function ssapiGetInternalSetting(settingName) {
    var logger = new ssobjLogger(arguments);
    var filters = [];
    filters.push(new ssobjSearchFilter(R_SS_INTERNAL_SETTING.NAME, null, OP_IS, settingName));
    var results = ssapiSearchInternalSetting(null, filters);
    if (ssapiHasNoValue(results)) {
        logger.log('ssapiHasNoValue(results)');
        return null;
    }
    var result = results[0];
    var columns = result.getAllColumns();
    var prop = {};
    logger.log('columns.length=' + columns.length);
    for (var i = 0; i < columns.length; i++) {
        var column = columns[i];
        var columnName = column.getName();
        prop[columnName] = result.getValue(column);
        prop[columnName + 'Text'] = result.getText(column);
    }
    return prop;
}

function ssapiSetInternalSetting(settingName, value) {
    var logger = new ssobjLogger(arguments);
    var prop = ssapiGetInternalSetting(settingName);
    var setting = new SsInternalSetting();
    if (prop === null) {
        // create
        logger.log('create');
        setting.setNameValue(settingName);
    } else {
        // update
        setting.loadRecord(prop[FLD_INTERNAL_ID]);
    }
    setting.setValueValue(value);
    return setting.submitRecord();
}

/**
 * After bundle install/update, the list of fields need to be saved so that when
 * SS records are added, processing will be faster. Process only suggested and
 * supported standard record types
 */
function ssapiPrepopulateFields() {
    var logger = new ssobjLogger(arguments);
    // process first suggested ss records
    var defaultRecordIds = [ '-204' /* lead */, '-203' /* Customer */, '-304' /* Case */, '-101' /* Opportunity */, '-308' /* Issue */, '-208' /* Project */, '-104' /* Invoice */, '-102' /*
                                                                                                                                                                                             * Sales
                                                                                                                                                                                             * Order
                                                                                                                                                                                             */, '-209' /* Partner */, '-103' /* Estimate */];
    // get their names
    var defaultRecordNames = [];
    for (var i = 0; i < defaultRecordIds.length; i++) {
        defaultRecordNames.push(ssapiGetRecordNameById(defaultRecordIds[i]));
    }
    var options = ssapiGetOptionsRecord();
    var allRecordNames = [];
    for (i = 0; i < options.length; i++) {
        allRecordNames.push(options[i].getText());
    }
    var param = {};
    for (i = 0; i < defaultRecordNames.length; i++) {
        // exclude not supported
        var recordName = defaultRecordNames[i];
        if (allRecordNames.indexOf(recordName) == -1) {
            logger.warn('NOT suggested. recordName=' + recordName);
            continue;
        }
        logger.ok('SUGGESTED. recordName=' + recordName);
        param = {};
        param.command = 'SaveSuiteSocialFieldsAfterInstall';
        param.recordName = defaultRecordNames[i];
        ssapiQueue(param);
    }
    // process the supported standard record types
    var supportedRecordNames = ssapiGetSupportedRecordsName();
    for (i = 0; i < supportedRecordNames.length; i++) {
        if (defaultRecordNames.indexOf(supportedRecordNames[i]) > -1) {
            continue;
        }
        recordName = supportedRecordNames[i];
        if (allRecordNames.indexOf(recordName) == -1) {
            logger.warn('NOT supported. recordName=' + recordName);
            continue;
        }
        logger.ok('SUPPORTED. recordName=' + recordName);
        param = {};
        param.command = 'SaveSuiteSocialFieldsAfterInstall';
        param.recordName = supportedRecordNames[i];
        ssapiQueue(param);
    }
}

/*
 * returns the list of active SS Records
 */
function ssapiGetActiveSSRecords() {
    // Issue: 218158 [SuiteSocial] Assistant > Enable Records - Please enter
    // value(s) for: Related Record Field error is emailed
    var logger = new ssobjLogger(arguments);
    if (ssapiHasNoValue(ssobjGlobal.ssapiGetActiveSSRecords)) {
        logger.warn('getting from db');
        var columnIds = [ FLD_RECORD_SCRIPTID, FLD_RECORD_NAME, FLD_INTERNAL_ID ];
        var filters = [ new nlobjSearchFilter(FLD_ISINACTIVE, null, 'is', 'F') ];
        ssobjGlobal.ssapiGetActiveSSRecords = ssapiGetResults(REC_RECORD, columnIds, filters);
    }
    logger.log('DONE');
    return ssobjGlobal.ssapiGetActiveSSRecords;
}

/*
 * gets the list of scripted records saved in s custom record
 */
function ssapiGetSavedScriptedRecords() {
    var logger = new ssobjLogger(arguments);
    if (ssapiHasNoValue(ssobjGlobal.ssapiGetSavedScriptedRecords)) {
        logger.warn('getting from db');
        // sort by script id then internal id
        var columnIds = [ R_SS_SCRIPTED_RECORD.SCRIPT_ID, FLD_INTERNAL_ID, R_SS_SCRIPTED_RECORD.SCRIPTED_RECORD, R_SS_SCRIPTED_RECORD.FIELD_SCRIPT_ID_LIST, 'name' ];
        ssobjGlobal.ssapiGetSavedScriptedRecords = ssapiSearchAllRecords(R.SS_SCRIPTED_RECORD, null, null, ssapiGetSearchColumns(columnIds));
    } else {
        // logger.ok('ssobjGlobal.ssapiGetSavedScriptedRecords found');
    }
    logger.log('DONE');
    return ssobjGlobal.ssapiGetSavedScriptedRecords;
}

/*
 * gets the list of roles with SuiteSocial access @return {object[]} object has
 * the ff properties: id, name, level
 */
function ssapiGetSuiteSocialRoles() {
    // Issue: 217291 [SuiteSocial] Add only default auto subscriptions to
    // employees if
    return ssapiRetrieveRolesofRecordType(REC_PROFILE);
}

/**
 * Used in setting the access and custom forms used by the assistant
 */
function ssapiSetAdminForms() {
    // S3 Issue 227112 : [SuiteSocial] Error encountered in bundle update:
    // afterUpdate. Error in ssapiSetAdminForms. role not found.
    // roleName=Administrator
    var logger = new ssobjLogger(arguments);
    var adminRoleid;
    if (ssapiIsRoleEnabled('Administrator')) {
        adminRoleid = getRoleId('Administrator');
    } else {
        // the role being used by bundle installation is admin?
        adminRoleid = nlapiGetContext().getRole();
    }

    ssapiGrantAccessToCustomRecordType(REC_NEWSFEED_CHANNEL, adminRoleid, 'Full');
    ssapiGrantAccessToCustomRecordType(REC_RECORD, adminRoleid, 'Full');
    ssapiGrantAccessToCustomRecordType(REC_AUTOSUB_TYPE, adminRoleid, 'Full');
    ssapiGrantAccessToCustomRecordType(REC_AUTOPOST, adminRoleid, 'Full');
    ssapiGrantAccessToCustomRecordType(REC_AUTOPOST_FIELD, adminRoleid, 'Full');
    ssapiGrantAccessToCustomRecordType(REC_DEFAULT_AUTOSUB_TYPE, adminRoleid, 'Full');
    ssapiGrantAccessToCustomRecordType('customrecord_ss_subscription_link', adminRoleid, 'Full');
    ssapiGrantAccessToCustomRecordType('customrecord_ss_saved_search_subs', adminRoleid, 'Full');
    logger.log('DONE');
    return true;
}

/*
 * applies the necessary security for each record to the passed array of roles.
 * The rows in the permissions sublist are removed and then the passed role are
 * added. @param {integer[]} roleIds. Array of role ids that should be given
 * access
 */
function ssapiGrantAccessToSSRecordTypes(roleIds) {
    // Issue: 216595 [SuiteSocial] Make the saving of assistant step 'Enable
    // Roles .
    // try {

    var logger = new ssobjLogger('ssapiGrantAccessToSSRecordTypes');
    logger.log('roleIds.length=' + roleIds.length);
    var group = 'permissions';
    var scriptId = null;
    var roleId = null;
    var adminRoleId = '3';
    var RESTRICT_VIEWING_AND_EDITING = '1';
    var RESTRICT_EDITING_ONLY = '3';

    // initialize SuiteSocial technical roles
    var intReadRoleId = ssapiGetRoleId("SuiteSocial Integration Read Access");
    var intFullRoleId = ssapiGetRoleId("SuiteSocial Integration Full Access");
    if (roleIds.indexOf(intReadRoleId) == -1) {
        roleIds.push(intReadRoleId);
    }
    if (roleIds.indexOf(intFullRoleId) == -1) {
        roleIds.push(intFullRoleId);
    }

    // add SuiteSocial Debugger role to "read only" custom records only
    var debuggerRoleId = ssapiGetRoleId("SuiteSocial Debugger");
    if (roleIds.indexOf(debuggerRoleId) == -1) {
        roleIds.push(debuggerRoleId);
    }

    // read only (except for Admin and Full Debugger, which are always Full)
    var access = 'View';
    for (var r = 0; r < readOnlyScriptIds.length; r++) {
        var startMilliSeconds = (new Date()).getTime();
        scriptId = readOnlyScriptIds[r];
        logger.log('grant READ access to scriptId=' + scriptId);
        var recordTypeInternalId = getCustomRecordTypeInternalId(scriptId);
        var customrecord = nlapiLoadRecord('customrecordtype', recordTypeInternalId, {
            recordmode : 'dynamic'
        });
        if (customrecord === null) {
            nlapiCreateError('customrecord === null', 'customrecord === null');
        }
        // remove all rows
        var count = customrecord.getLineItemCount(group);
        while (count > 0) {
            customrecord.removeLineItem(group, 1);
            count = customrecord.getLineItemCount(group);
        }
        for (var i = 0; i < roleIds.length; i++) {
            roleId = roleIds[i];

            // dont give permission for customer for the specified custom record
            var isCustRole = ssapiIsCustomerRole(roleId);
            if (isCustRole && (customerNoPermissionScriptIds.indexOf(scriptId) >= 0)) {
                continue;
            }

            // add role permission
            customrecord.selectNewLineItem(group);
            customrecord.setCurrentLineItemValue(group, 'permittedrole', roleId);
            customrecord.setCurrentLineItemText(group, 'permittedlevel', (roleId == adminRoleId || roleId == intFullRoleId) ? 'Full' : 'View');

            if ([ adminRoleId, intReadRoleId, intFullRoleId, debuggerRoleId ].indexOf(roleId) == -1) {
                customrecord.setCurrentLineItemValue(group, 'restrictform', 'T');
                customrecord.setCurrentLineItemValue(group, 'restriction', RESTRICT_EDITING_ONLY);
            }

            switch (scriptId) {
            case REC_NEWSFEED_CHANNEL:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Channel Form');
                break;
            case REC_RECORD:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Record Form');
                break;
            case REC_AUTOSUB_TYPE:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial AutoSubscribe Type Form');
                break;
            case REC_AUTOPOST:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Auto Post Form');
                break;
            case REC_AUTOPOST_FIELD:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Auto Post Field Form');
                break;
            case REC_PROFILE:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Profile Form');
                break;
            case 'customrecord_ss_subscription_link':
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Subscription Link Form');
                break;
            case REC_DEFAULT_AUTOSUB_TYPE:
                logger.log('set custom form REC_DEFAULT_AUTOSUB_TYPE');
                // Issue: 212785 [SuiteSocial] Allow the use of inactive and
                // delete.
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom Default AutoSubscribe Type Form');
                break;
            case 'customrecord_ss_saved_search_subs':
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom Saved Search Subscription Form');
                break;
            default:
                // default_statement;
            }
            customrecord.setFieldText('accesstype', 'Use Permission List');
            customrecord.commitLineItem(group);
        }
        // logger.log('customrecord=' + customrecord.toString());
        nlapiSubmitRecord(customrecord);

        if (nlapiGetContext().getExecutionContext() != 'scheduled') {
            // check if loading and saving is slow
            var endMilliSeconds = (new Date()).getTime();
            var EXECUTION_TIME_LIMIT = 5000;// in milli seconds
            var loadingAndSavingDuration = endMilliSeconds - startMilliSeconds;
            logger.log('loadingAndSavingDuration=' + loadingAndSavingDuration);
            if (loadingAndSavingDuration > EXECUTION_TIME_LIMIT) {
                // if time to load and save is greater than
                // EXECUTION_TIME_LIMIT, execute it as sked script
                var param = {};
                param.command = 'ssapiGrantAccessToSSRecordTypes';
                param.roleIds = roleIds;
                param.userId = nlapiGetContext().getUser();
                ssapiQueue(param);
                return 'scheduled';
            }
        }

    }

    // do not add SuiteSocial Debugger role to "full" custom records
    var index = roleIds.indexOf(debuggerRoleId);
    roleIds.splice(index, 1);

    // full access
    access = 'Full';
    for (var r = 0; r < fullScriptIds.length; r++) {
        logger.log('grant FULL access to scriptId=' + scriptId);
        scriptId = fullScriptIds[r];
        var recordTypeInternalId = getCustomRecordTypeInternalId(scriptId);
        var customrecord = nlapiLoadRecord('customrecordtype', recordTypeInternalId, {
            recordmode : 'dynamic'
        });
        if (customrecord === null) {
            nlapiCreateError('customrecord === null', 'customrecord === null');
        }
        // remove all rows
        var count = customrecord.getLineItemCount(group);
        while (count > 0) {
            customrecord.removeLineItem(group, 1);
            count = customrecord.getLineItemCount(group);
        }

        var roleId = null;
        for (var i = 0; i < roleIds.length; i++) {
            roleId = roleIds[i];

            // dont give permission for customer for the specified custom record
            var isCustRole = ssapiIsCustomerRole(roleId);
            if (isCustRole && (customerNoPermissionScriptIds.indexOf(scriptId) >= 0)) {
                continue;
            }

            // add role permission
            customrecord.selectNewLineItem(group);
            customrecord.setCurrentLineItemValue(group, 'permittedrole', roleId);
            customrecord.setCurrentLineItemText(group, 'permittedlevel', roleId == intReadRoleId ? 'View' : 'Full');

            if ([ adminRoleId, intReadRoleId, intFullRoleId ].indexOf(roleId) == -1) {
                customrecord.setCurrentLineItemValue(group, 'restrictform', 'T');
                customrecord.setCurrentLineItemValue(group, 'restriction', RESTRICT_VIEWING_AND_EDITING);
            }

            switch (scriptId) {
            case REC_NEWSFEED_CHANNEL:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Channel Form');
                break;
            case REC_RECORD:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Record Form');
                break;
            case REC_AUTOSUB_TYPE:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial AutoSubscribe Type Form');
                break;
            case REC_AUTOPOST:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Auto Post Form');
                break;
            case REC_AUTOPOST_FIELD:
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Auto Post Field Form');
                break;
            case REC_PROFILE:
                // if role is customer, set the customer form
                customrecord.setCurrentLineItemText(group, 'defaultform', isCustRole ? 'Customer SuiteSocial Profile Form' : 'Custom SuiteSocial Profile Form');
                break;
            case 'customrecord_ss_subscription_link':
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom SuiteSocial Subscription Link Form');
                break;
            case REC_DEFAULT_AUTOSUB_TYPE:
                logger.log('set custom form REC_DEFAULT_AUTOSUB_TYPE');
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom Default AutoSubscribe Type Form');
                break;
            case 'customrecord_ss_saved_search_subs':
                customrecord.setCurrentLineItemText(group, 'defaultform', 'Custom Saved Search Subscription Form');
                break;
            default:
                // default_statement;
            }
            customrecord.setFieldText('accesstype', 'Use Permission List');
            customrecord.commitLineItem(group);
        }
        // logger.log('customrecord=' + customrecord.toString());
        nlapiSubmitRecord(customrecord);
    }

    // apply row for Admin
    ssapiSetAdminForms();

    logger.log('DONE');
    return true;
    // }
    // catch (e) {
    // ssapiHandleServerError(e);
    // return false;
    // }
}

/**
 * Gets the SuiteSocial roles that are used only for technical purposes, such as
 * integration and debugging.
 * 
 * @returns {[]} - array of internal IDs of the roles
 */
function ssapiGetSuiteSocialTechnicalRoles() {
    var roleIds = [];
    // get integration roles
    roleIds.push(ssapiGetRoleId("SuiteSocial Integration Read Access"));
    roleIds.push(ssapiGetRoleId("SuiteSocial Integration Full Access"));
    roleIds.push(ssapiGetRoleId("SuiteSocial Debugger"));
    return roleIds;
}

// 2012-01-11-Issue 212652 [SuiteSocial] ReferenceError savePreCannedSingle i
function savePreCanned() {
    var logger = new ssobjLogger('savePreCanned');
    // ssapiWait();
    logger.log('record type:' + REC_RECORD);
    var resp;

    var autoSubTypeNames = [];
    var defaultRoleIds = [];
    var results = retrieveDefaultAutoSubTypes();
    // S3 Issue 212458 : [SuiteSocial] "Cannot read property "length" from null
    // (suitesocial_admin_assistant.js#
    if (results === null) {
        return;
    }
    for (var i = 0; i < results.length; i++) {
        var result = results[i];
        var autoSubTypeName = result.getValue(FLD_AUTOSUB_TYPE_NAME);
        autoSubTypeNames.push(autoSubTypeName);

        // var recordName = result.getText(FLD_AUTOSUB_TYPE_REC);
        // var fld = result.getText(FLD_AUTOSUB_TYPE_FLD);
        //        
        // // check if autosub type is already in db, if not yet, add
        // var internalid = getRecordId(REC_AUTOSUB_TYPE, FLD_AUTOSUB_TYPE_NAME,
        // 'is', autoSubTypeName);
        // if (internalid === null) {
        // var entity = {};
        // entity.name = autoSubTypeName;
        // // get ss record id
        // // var ssRecordId = getRecordId(REC_RECORD, FLD_RECORD_TYPE, 'is',
        // recordTypeId);
        // entity[FLD_AUTOSUB_TYPE_REC] = recordName;
        // entity[FLD_AUTOSUB_TYPE_FLD] = fld;
        //            
        // var newid = addAutoSubType(entity);
        // // resp = uiSuiteletProcess(REC_AUTOSUB_TYPE + '_add', entity);
        // // internalid = JSON.parse(resp);
        // logger.log('newid=' + newid);
        // }

        // get list of roles
        var roleIds = [];
        var defaultAutoSubTypeInternalId = result.getText(FLD_INTERNAL_ID);
        var roleResults = retrieveDefaultAutoSubTypeRoles(defaultAutoSubTypeInternalId);
        if (roleResults !== null) {
            for (var r = 0; r < roleResults.length; r++) {
                roleIds.push(roleResults[r].getValue(R_SS_RECORD_SUBSCRIPTION_ROLE.ROLE));
            }
            defaultRoleIds.push(roleIds);
        }
    }

    var param = {};

    param.autoSubTypeNames = autoSubTypeNames;
    param.defaultRoleIds = defaultRoleIds;
    // resp = uiSuiteletProcess('subscribeEmployees', param);
    // logger.log('resp=' + resp);

    var params = {};
    // logger.log('defaultRoleIds.length=' +
    // datain.values.defaultRoleIds.length);
    // logger.log('autoSubTypeNames.length=' +
    // datain.values.autoSubTypeNames.length);
    params.custscript_auto_subscribe_name_list = JSON.stringify(autoSubTypeNames);
    params.custscript_role_id_list = JSON.stringify(defaultRoleIds);
    params.custscript_batch_start_employee_id = getMinimumEmployeeId();

    // get email of the current user
    var context = nlapiGetContext();
    var userId = context.getUser();
    var email = nlapiLookupField('employee', userId, 'email');

    params.custscript_ss_setup_subscribe_emp_email = email;

    var scheduleReturn = nlapiScheduleScript('customscript_ss_setup_subscribe_emp', 'customdeploy_ss_setup_subscribe_emp', params);
    logger.log('scheduleReturn=' + scheduleReturn);
    // response.write(JSON.stringify(true));

    return scheduleReturn;
}

function retrieveDefaultAutoSubTypes() {
    var logger = new ssobjLogger(arguments);
    // get list of rec subs in rec subs role
    var filters = [ new nlobjSearchFilter('isinactive', null, 'is', 'F') ];
    var recordSubscriptionWithRolesResults = ssapiGetResults(R.SS_RECORD_SUBSCRIPTION_ROLE, [ R_SS_RECORD_SUBSCRIPTION_ROLE.RECORD_SUBSCRIPTION ], filters);
    var recordSubscriptionWithRoles = [];
    if (ssapiHasNoValue(recordSubscriptionWithRolesResults)) {
        logger.log('recordSubscriptionWithRolesResults === null');
        return null;
    }
    logger.log('recordSubscriptionWithRolesResults.length=' + recordSubscriptionWithRolesResults.length);
    for (var i = 0; i < recordSubscriptionWithRolesResults.length; i++) {
        recordSubscriptionWithRoles.push(recordSubscriptionWithRolesResults[i].getValue(R_SS_RECORD_SUBSCRIPTION_ROLE.RECORD_SUBSCRIPTION));
    }
    filters = [ new nlobjSearchFilter('isinactive', null, 'is', 'F') ];
    filters.push(new nlobjSearchFilter(FLD_INTERNAL_ID, null, OP_ANYOF, recordSubscriptionWithRoles));
    var columnIds = [ FLD_AUTOSUB_TYPE_NAME, FLD_AUTOSUB_TYPE_REC, FLD_AUTOSUB_TYPE_FLD, FLD_INTERNAL_ID ];
    return ssapiGetResults(REC_AUTOSUB_TYPE, columnIds, filters);
}

function retrieveDefaultAutoSubTypesBak() {
    var filters = [ new nlobjSearchFilter('isinactive', null, 'is', 'F') ];
    return retrieveResults(REC_DEFAULT_AUTOSUB_TYPE, [ FLD_DEFAULT_AUTOSUB_TYPE_NAME, FLD_DEFAULT_AUTOSUB_TYPE_RT, FLD_DEFAULT_AUTOSUB_TYPE_FLD, FLD_INTERNAL_ID ], filters)
}

// function retrieveAllDefaultAutoSubTypes(){
// return retrieveResults(REC_DEFAULT_AUTOSUB_TYPE,
// [FLD_DEFAULT_AUTOSUB_TYPE_NAME, FLD_DEFAULT_AUTOSUB_TYPE_RT,
// FLD_DEFAULT_AUTOSUB_TYPE_FLD, FLD_INTERNAL_ID])
// }

function retrieveAllDefaultAutoSubTypes() {
    var logger = new ssobjLogger(arguments);
    // get list of rec subs in rec subs role
    var filters = [ new nlobjSearchFilter('isinactive', null, 'is', 'F') ];
    var recordSubscriptionWithRolesResults = ssapiGetResults(R.SS_RECORD_SUBSCRIPTION_ROLE, [ R_SS_RECORD_SUBSCRIPTION_ROLE.RECORD_SUBSCRIPTION ], filters);
    var recordSubscriptionWithRoles = [];
    if (recordSubscriptionWithRolesResults === null) {
        logger.log('recordSubscriptionWithRolesResults === null');
        return null;
    }
    for (var i = 0; i < recordSubscriptionWithRolesResults.length; i++) {
        recordSubscriptionWithRoles.push(recordSubscriptionWithRolesResults[i].getValue(R_SS_RECORD_SUBSCRIPTION_ROLE.RECORD_SUBSCRIPTION));
    }
    filters = [];
    filters.push(new nlobjSearchFilter(FLD_INTERNAL_ID, null, OP_ANYOF, recordSubscriptionWithRoles));
    return retrieveResults(REC_AUTOSUB_TYPE, [ 'isinactive', FLD_AUTOSUB_TYPE_NAME, FLD_AUTOSUB_TYPE_REC, FLD_AUTOSUB_TYPE_FLD, FLD_INTERNAL_ID ], filters);

}

function retrieveDefaultAutoSubTypeRoles(parentId) {
    var logger = new ssobjLogger(arguments);
    logger.log('parentId=' + parentId);
    var filters = [ new nlobjSearchFilter('isinactive', null, 'is', 'F') ];
    filters.push(new nlobjSearchFilter(R_SS_RECORD_SUBSCRIPTION_ROLE.RECORD_SUBSCRIPTION, null, 'is', parentId));
    return ssapiGetResults(R.SS_RECORD_SUBSCRIPTION_ROLE, [ R_SS_RECORD_SUBSCRIPTION_ROLE.ROLE ], filters);
}

function addTrimFunctions() {

    // add trim functions if browser has no support
    if (typeof String.trim == 'undefined') {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, "");
        };
    }

    if (typeof String.ltrim == 'undefined') {
        String.prototype.ltrim = function() {
            return this.replace(/^\s+/, "");
        };
    }

    if (typeof String.rtrim == 'undefined') {
        String.prototype.rtrim = function() {
            return this.replace(/\s+$/, "");
        };
    }
}

addTrimFunctions();

/*
 * get the percentage of employees with profile
 */
function getPercentageOfProfileAndEmployee() {
    var SD = new _SocialData();

    return {
        profileCount : SD.Profiles.GetProfileCount(),
        employeeCount : SD.Entities.GetEntityCount()
    };
}

/*
 * Gets the maximum id in the employee table. This is usually used in scheduled
 * scripts
 */
function getMaximumEmployeeId() {
    var columns = [ new nlobjSearchColumn(FLD_INTERNAL_ID, null, 'max') ];
    var results = nlapiSearchRecord('employee', null, null, columns);
    return results[0].getValue(columns[0]);
}

/*
 * Gets the minimum id in the employee table. This is usually used as start
 * number in scheduled scripts
 */
function getMinimumEmployeeId() {
    var columns = [ new nlobjSearchColumn(FLD_INTERNAL_ID, null, 'min') ];
    var results = nlapiSearchRecord('employee', null, null, columns);
    return results[0].getValue(columns[0]);
}
