/**
 * Module Description Here
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 *
 * Version    Date            Author           Remarks
 * 1.00       02 Jan 2012     tcaguioa
 *
 */
/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {void}
 */
function fieldChanged(type, name, linenum) {
    if (name == 'custpage_savedsearch') {
        nlapiSetFieldValue('custrecord_suitesocial_autosub_type_ss', nlapiGetFieldValue('custpage_savedsearch'));
    }
}

function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    deleteAutoSubscribeType(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

    if (typeof window != 'undefined') {
        window.location.href = window.opener.originalUrl;
    }

}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: Employee Auto-subscriptions. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        width : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a Record Subscription inactive.'.tl() + ' ';
    inactivateQuestion += 'While a record subscription is inactive,'.tl() + ' ';
    inactivateQuestion += 'posts related to the record subscription will not be created. User cannot subscribe to that record subscription.'.tl() + ' ';
    inactivateQuestion += 'Do you want to make this record subscription inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?'.tl(),
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        width : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(type) {

    // check name
    var cols = [];
    var subscriptionName = nlapiGetFieldValue('name');
    if (ssapiHasNoValue(subscriptionName)) {
        uiShowError('Provide a value for Name.'.tl());
        return false;
    }
    var field = nlapiGetFieldValue('custrecord_suitesocial_autosub_type_rf');
    if (ssapiHasNoValue(field)) {
        uiShowError('Provide a value for Field.'.tl());
        return false;
    }

    cols.push({
        columnId : 'name',
        operator : OP_IS,
        value : subscriptionName
    });
    if (ssapiHasDuplicates(REC_AUTOSUB_TYPE, cols, nlapiGetFieldValue('id'))) {
        uiShowError(subscriptionName + ' already exists in the database.'.tl());
        return false;
    }

    // check combination of fields
    cols = [];
    cols.push({
        columnId : FLD_AUTOSUB_TYPE_REC,
        operator : OP_ANYOF,
        value : nlapiGetFieldValue(FLD_AUTOSUB_TYPE_REC)
    });
    cols.push({
        columnId : FLD_AUTOSUB_TYPE_FLD,
        operator : OP_ANYOF,
        value : nlapiGetFieldValue(FLD_AUTOSUB_TYPE_FLD)
    });
    cols.push({
        columnId : 'custrecord_suitesocial_autosub_type_ss',
        operator : OP_ANYOF,
        value : nlapiGetFieldValue('custrecord_suitesocial_autosub_type_ss')
    });

    if (ssapiHasDuplicatesAllowEmptyValues(REC_AUTOSUB_TYPE, cols, nlapiGetFieldValue('id'))) {
        uiShowError('The combination of Field "'.tl() + nlapiGetFieldText(FLD_AUTOSUB_TYPE_FLD) + '" ' + 'and Saved Search'.tl() + ' "' + nlapiGetFieldText('custrecord_suitesocial_autosub_type_ss') + '" for this record type already exists in the database.'.tl());
        return false;
    }

    return true;
}

function pageInit() {
    try {
        nlapiSetFieldDisplay('customform', false);

    } catch (e) {
        // in case custom form is not existing, just supress error
    }

    // upgrade code
    if (ssapiHasNoValue(nlapiGetFieldValue('custrecord_suitesocial_autosub_type_rf'))) {
        nlapiSetFieldText('custrecord_suitesocial_autosub_type_rf', nlapiGetFieldText('custrecord_suitesocial_autosub_type_fld'));
    }

    setInterval('ssapiHideNewInOptionList();', 500);
    Ext.get('custrecord_suitesocial_autosub_type_rf_popup_new').dom.style.display = 'none';
}

/**
 * @param {String}
 *        type Sublist internal id
 * @return {Boolean} True to continue line item delete, false to abort delete
 */
function clientValidateDelete(type) {
    // alert('delete' + type)
    // return false
    return true;
}

/**
 * This is used in translating the menu item Delete Including Dependent Records
 * It needs to be called from an interval since the menu item is created on
 * hover
 */
function ssapiTranslateDeleteIncludingDependentRecords() {
    var link = jQuery('span:contains("Delete Including Dependent Records")')[0];
    if (link) {
        link.innerHTML = link.innerHTML.tl();
        clearInterval(ssobjGlobal.translateDeleteIncludingDependentRecords);
    }
}
ssobjGlobal.translateDeleteIncludingDependentRecords = setInterval('ssapiTranslateDeleteIncludingDependentRecords();', 500);
