/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {void}
 */
function clientFieldChanged(type, name, linenum) {
    // var logger = new ssobjLogger(arguments);
    // if (name == 'custpage_savedsearch') {
    // // nlapiSetFieldText('custrecord_sss_saved_search',
    // nlapiGetFieldText('custpage_savedsearch'));
    // // nlapiSetFieldValue('name', nlapiGetFieldText('custpage_savedsearch'));
    // }
}

function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    ssapiDeleteSavedSearchSubscription(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

    if (typeof window != 'undefined') {
        window.location.href = window.opener.originalUrl;
    }

}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: My Saved Search Subscriptions. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a Saved Search Subscription inactive.'.tl() + ' ';
    inactivateQuestion += 'While a Saved Search Subscription is inactive,'.tl() + ' ';
    inactivateQuestion += 'posts related to the Saved Search Subscription will not be created.'.tl() + ' ';
    inactivateQuestion += 'Do you want to make this Saved Search Subscription inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?',
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

function saveRecord() {
    var savedsearchid = nlapiGetFieldValue('custpage_savedsearch');
    if (ssapiHasNoValue(savedsearchid)) {
        uiShowError('Provide a value for "Saved Search".'.tl());
        return false;
    }

    var cols = [];
    cols.push({
        columnId : R_SAVED_SEARCH_SUBSCRIPTION.SAVED_SEARCH,
        operator : OP_ANYOF,
        value : savedsearchid
    });
    if (ssapiHasDuplicates(REC.SAVED_SEARCH_SUBSCRIPTION, cols, nlapiGetFieldValue('id'))) {
        uiShowError('Saved Search '.tl() + ' ' + nlapiGetFieldText('custpage_savedsearch') + ' ' + 'already exists in the database.'.tl());
        return false;
    }
    return true;
}

/**
 * This is used in translating the menu item Delete Including Dependent Records
 * It needs to be called from an interval since the menu item is created on
 * hover
 */
function ssapiTranslateDeleteIncludingDependentRecords() {
    var link = jQuery('span:contains("Delete Including Dependent Records")')[0];
    if (link) {
        link.innerHTML = link.innerHTML.tl();
        clearInterval(ssobjGlobal.translateDeleteIncludingDependentRecords);
    }
}
ssobjGlobal.translateDeleteIncludingDependentRecords = setInterval('ssapiTranslateDeleteIncludingDependentRecords();', 500);
