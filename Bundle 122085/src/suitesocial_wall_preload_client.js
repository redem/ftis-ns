/**
 * this is called before all other scripts to immediately do processing like
 * hiding of some UI elements
 */
if (top !== self) {
    // inside iframe
    // do a delay to prevent the menu bar from displaying
    setTimeout('socialShowIframe();', 1000);
}

function socialShowIframe() {
    if (parent.document.getElementById('suitesocialwall')) {
        // applies to the tab only
        parent.document.getElementById('suitesocialwall').style.display = '';
        parent.document.getElementById('suitesocialwallmessage').style.display = 'none';
    }
}
