//This is a client-side script.
//TODO: This code should be merged to with suitesocial_profile_CS.js in the future



function pageInit() {
}





function page_init_profile() {

    // add autosubscribed channels if not already added
    var autosubscribedChannels = nlapiGetFieldValue('custpage_ss_prof_ch_rem_autosubs');
    autosubscribedChannels = ssapiHasValue(autosubscribedChannels) ? JSON.parse(autosubscribedChannels) : [];
    for (var i = 0; i < autosubscribedChannels.length; i++) {
        var sublist = 'recmachcustrecord_suitesocial_profile_ch_prof';
        nlapiSelectNewLineItem(sublist);
        nlapiSetCurrentLineItemValue(sublist, 'custpage_ss_prof_ch_ch', autosubscribedChannels[i].internalid);
        nlapiSetCurrentLineItemValue(sublist, 'custpage_ss_prof_ch_auto_subs', 'T');
        nlapiCommitLineItem(sublist);
    }

    var Yammer_Token = nlapiGetFieldValue('custrecord_yammer_profile_token');
    var encryptURL = nlapiResolveURL('SUITELET', 'customscript_suitesocial_encryption_ss', 'customdeploy_suitesocial_encryption_ss', true);
    var postdata = {};
    postdata['custscript_action'] = "dec";
    postdata['custscript_text'] = Yammer_Token;
    var decrypted = nlapiRequestURL(encryptURL, postdata);
    nlapiSetFieldValue('custrecord_yammer_authentication_token', decrypted.getBody());
}





function save_record_profile() {
    var Yammer_Token = nlapiGetFieldValue('custrecord_yammer_authentication_token');
    var encryptURL = nlapiResolveURL('SUITELET', 'customscript_suitesocial_encryption_ss', 'customdeploy_suitesocial_encryption_ss', true);
    var postdata = {};
    postdata['custscript_action'] = "enc";
    postdata['custscript_text'] = Yammer_Token;
    var encrypted = nlapiRequestURL(encryptURL, postdata);
    nlapiSetFieldValue('custrecord_yammer_profile_token', encrypted.getBody());

    return true;
}





/**
 * This function captures the event when a line item is removed from the My
 * Channels sublist. When the channel on the line item is an autosubscribed
 * channel, the removal will be blocked, and a warning message will be shown.
 * 
 * @param type -
 *        sublist id
 * @returns {Boolean}
 */
function validate_delete_line_profile(type) {
    if (type == 'recmachcustrecord_suitesocial_profile_ch_prof') {
        var isAutoSubs = nlapiGetCurrentLineItemValue(type, 'custpage_ss_prof_ch_auto_subs');
        if (isAutoSubs == 'T') {
            var channelName = nlapiGetCurrentLineItemText(type, 'custpage_ss_prof_ch_ch');
            Ext.MessageBox.show({
                title : 'SuiteSocial',
                msg : '"' + channelName + '" is an autosubscribed channel, and cannot be removed.',
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.WARNING
            });
            
            return false;
        }
    }
    
    return true;
}
