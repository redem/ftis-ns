/**
 * Module Description Here You are using the default templates which should be
 * customized to your needs. You can change your user name under
 * Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version Date Author Remarks 1.00 02 Jan 2012 tcaguioa
 * 
 */
function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    deleteAutoPost(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: News Feed. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a Auto Post inactive.'.tl();
    inactivateQuestion += ' While an auto post is inactive, '.tl();
    inactivateQuestion += 'posts related to the auto post will not be created. '.tl();
    inactivateQuestion += 'Do you want to make this auto post inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?'.tl(),
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {
    var logger = new ssobjLogger('clientSaveRecord');
    var cols = [];
    cols.push({
        columnId : 'custrecord_sssl_ss_rec',
        operator : OP_ANYOF,
        value : nlapiGetFieldValue('custrecord_sssl_ss_rec')
    });
    cols.push({
        columnId : 'custrecord_sssl_rel_ss_rec',
        operator : OP_ANYOF,
        value : nlapiGetFieldValue('custrecord_sssl_rel_ss_rec')
    });
    cols.push({
        columnId : 'custrecord_sssl_field',
        operator : OP_ANYOF,
        value : nlapiGetFieldValue('custrecord_sssl_field')
    });

    var internalId = nlapiGetFieldValue('id');
    if (hasDuplicates('customrecord_ss_subscription_link', cols, internalId)) {
        uiShowError('The combination of Related Record "'.tl() + nlapiGetFieldText('custrecord_sssl_rel_ss_rec') + '" and ' + 'Field'.tl() + ' "' + nlapiGetFieldText('custrecord_sssl_field') + '" already exists in the record.'.tl());
        return false;
    }

    cols = [];
    cols.push({
        columnId : 'name',
        operator : 'is',
        value : nlapiGetFieldValue('name')
    });
    if (hasDuplicates('customrecord_ss_subscription_link', cols, internalId)) {
        uiShowError('The name already exists in the record.'.tl());
        return false;
    }

    return true;
}

function pageInit() {
    nlapiSetFieldDisplay('customform', false);

    Ext.get('inpt_custrecord_sssl_ss_rec1').on('focus', function(e) {
        ssapiHideNewInOptionList();
    });

    Ext.get('inpt_custrecord_sssl_record_field2').on('focus', function(e) {
        ssapiHideNewInOptionList();
    });

    Ext.get('custrecord_sssl_ss_rec_popup_new').dom.style.display = 'none';
    Ext.get('custrecord_sssl_record_field_popup_new').dom.style.display = 'none';
}

// /**
// * @param {String} type Sublist internal id
// * @param {String} name Field internal id
// * @return {void}
// */
function clientPostSourcing(type, name) {
    // var logger = new ssobjLogger('clientPostSourcing');
    // logger.log('name=' + name);
    // // get the number of options in the Field field. If there is only one,
    // set it
    // if (name == 'custrecord_sssl_field') {
    // var fld = nlapiGetField(name);
    // var options = fld.getSelectOptions();
    // if (options.length == 1) {
    // nlapiSetFieldValue(name, options[0].getId());
    // }
    // }
}
