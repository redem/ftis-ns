var ssobjGlobal = ssobjGlobal || {};
var TRANSLATION_SESSION_KEY = 'translationMap';
var LANGUAGES_WITH_TRANSLATION = [
    'cs_cz',
    'da_dk',
    'de_de',
    'es_ar',
    'es_es',
    'fr_ca',
    'fr_fr',
    'it_it',
    'ja_jp',
    'ko_kr',
    'nl_nl',
    'pt_br',
    'ru_ru',
    'sv_se',
    'th_th',
    'zh_cn',
    'zh_tw'
];

ssapiLoadTranslationMap();
ssapiAddTranslateFunction();





/**
 * Returns a hashMap/Table of the translations. The key is the XML encoded value of the string.
 */
function ssapiGetTranslationMap()
{
    try {
        var filePath = 'SuiteBundles/Bundle ' + nlapiGetContext().getBundleId() + '/src/i18n/' + ssobjGlobal.language + '.resx.xml';
        var file = nlapiLoadFile(filePath);
        var xmlString = file.getValue();
        var xmlDoc = nlapiStringToXML(xmlString);
        var nodes = xmlDoc.getElementsByTagName('data');
        var values = xmlDoc.getElementsByTagName('value');
        
        if (nodes.length != values.length)
        {
            throw 'nodes.length != values.length; nodes.length=' + nodes.length + '; values.length' + values.length + '; check for empty data tags';
        }
        
        var map = {};
        for (var i = 0; i < nodes.length; i++)
        {
            var node = nodes.item(i);
            var value = values.item(i);
            var key = ssapiCreateKey(node.getAttribute('name'));

            if (map[key])
            {
                nlapiLogExecution('error', 'Translation', 'Language: ' + ssobjGlobal.language + '; Duplicate translation: ' + key);
                throw 'Translation: Language: ' + ssobjGlobal.language + '; Duplicate translation: ' + key;
            }
            
            map[key] = value.getTextContent();
        }
        
        var mapStringified = JSON.stringify(map);
        nlapiGetContext().setSessionObject(TRANSLATION_SESSION_KEY, mapStringified);
        
        return mapStringified;
    } 
    catch (e)
    {
        nlapiLogExecution('error', 'SuiteSocial Error', 'Error in ssapiGetTranslationMap(); e=' + e);
    }
}





/**
 * Executes a suitelet synchronously and returns the response as object.
 */
function ssapiGetTranslationMapViaSuitelet()
{
    try
    {
        if (document.getElementById("beforeLoadProgress") !== null)
        {
            document.getElementById("beforeLoadProgress").innerHTML = '<img src=https://system.netsuite.com/images/setup/hourglass.gif />' + ' Loading translations.';
        }
        
        var data = {};
        data.action = 'getTranslationMap';
        data.values = 'any';
        ssobjGlobal = ssobjGlobal || {};
        ssobjGlobal.socialSuiteletProcessAsyncUrl = nlapiResolveURL('SUITELET', 'customscript_suitewall_generic_suitelet', 'customdeploy_suitewall_generic_suitelet');
        var url = ssobjGlobal.socialSuiteletProcessAsyncUrl;
        
        //parameters
        var xmlRequest = new XMLHttpRequest();
        xmlRequest.open('POST', url, false /* async*/);
        xmlRequest.setRequestHeader("Content-Type", "application/json");
        xmlRequest.send(JSON.stringify(data));
        if (xmlRequest.status !== 200)
        {
            // error
            nlapiLogExecution('debug', 'ssapiGetTranslationMapViaSuitelet', 'ssapiGetTranslationMapViaSuitelet() An unexpected error occurred. xmlRequest.status=' + xmlRequest.status);
            return false;
        }

        if (xmlRequest.responseText.indexOf('ERROR:') > -1)
        {
			nlapiLogExecution('debug', 'ssapiGetTranslationMapViaSuitelet', 'ssapiGetTranslationMapViaSuitelet() ' + JSON.parse(xmlRequest.responseText));
            return false;
        }
        
        //<!--
        /**
         * For some reasons, the returned string sometimes contains debug information from core.
         * This debugging information start with <!--. If this string is found, process only the string before this.
         */
        var responseText = xmlRequest.responseText;
        var position = responseText.indexOf('<!--');
        if (position > -1)
        {
            responseText = responseText.substr(0, position);
        }
        
        var returnObject = JSON.parse(responseText);

        if (document.getElementById("beforeLoadProgress") !== null)
        {
            document.getElementById("beforeLoadProgress").innerHTML = '<img src=https://system.netsuite.com/images/setup/hourglass.gif />' + ' Loading translations.';
        }
        
        return returnObject;
        
    } 
    catch (e) {
        //alert('ssapiGetTranslationMapViaSuitelet e=' + e.toString() + '\n\n\nxmlRequest.responseText=' + xmlRequest.responseText);
		nlapiLogExecution('debug', 'ssapiGetTranslationMapViaSuitelet', 'ssapiGetTranslationMapViaSuitelet e=' + e.toString());
		return {};
    }
}





/**
 * Upon load of script, get the all list of translation and cache it in a global variable .
 */
function ssapiLoadTranslationMap()
{
    try
    {
        var executionContext = nlapiGetContext().getExecutionContext();
        var deploymentId = nlapiGetContext().getDeploymentId();

        if (executionContext == 'scheduled' || deploymentId == 'customdeploy_suitesocial_autopost')
        {
            // get company level since we cannot get employee setting from sked script
            var companypreferences = nlapiLoadConfiguration('companypreferences');
            ssobjGlobal.language = companypreferences.getFieldValue('custscript_ss_email_language');
            ssobjGlobal.language = ssobjGlobal.language || '';
            if (ssobjGlobal.language === '') {
                // no translation
                return;
            }
        }
        else
        {
            // get user's
            ssobjGlobal.language = nlapiGetContext().getPreference('LANGUAGE');
        }


        if (LANGUAGES_WITH_TRANSLATION.indexOf(ssobjGlobal.language.toLowerCase()) == -1)
        {
            // no translation
            return;
        }
        
        
        // get translation map. Use restlet for client-side code
        // determine if running in client or server
        var isUI = typeof nlapiCreateForm == 'undefined';
        if (isUI)
        {
            // we cannot use restlet since we need admin rights 
            ssobjGlobal.map = ssapiGetTranslationMapViaSuitelet();
        }
        else
        {
            // via server
            ssobjGlobal.map = JSON.parse(ssapiGetTranslationMap());
        }
    } 
    catch (e) {
        nlapiLogExecution('error', 'SuiteSocial Error: PSG-i18n.js', 'Error in ssapiLoadTranslationMap(); e=' + e);
    }
}





/**
 * This is used in creating keys from a string compatible inside XML tags.
 * @param {Object} stringValue
 */
function ssapiCreateKey(stringValue)
{
    // the use of " or ' in the key causes errors so remove them
    // error: TypeError: Attribute name "record" associated with an element type "data" must be followed by the ' = ' character. (PSG-tools-common.js#749)
    stringValue = stringValue.replace(/"/g, '');
    stringValue = stringValue.replace(/'/g, '');
    // excel converts ... automatically to � , so just remove
    stringValue = stringValue.replace('...', '');
    
    stringValue = stringValue.replace('&quot;', '');
    stringValue = stringValue.replace('&quot;', '');
    stringValue = stringValue.replace('&quot;', '');
    stringValue = stringValue.replace('&quot;', '');
    stringValue = stringValue.replace('&quot;', '');
    stringValue = stringValue.replace('&quot;', '');
    stringValue = stringValue.replace('&quot;', '');
    
    return nlapiEscapeXML(stringValue);
}





/**
 * Returns an array of missing translations per language
 */
function ssapiGetMissingTranslationStrings()
{
    if (ssobjGlobal.missingTranslationStrings)
    {
        return ssobjGlobal.missingTranslationStrings;
    }
    
    ssobjGlobal.missingTranslationStrings = [];
    
    var columns = [new nlobjSearchColumn('name'), new nlobjSearchColumn('custrecord_ssmt_language')];
    ssobjGlobal.language = nlapiGetContext().getPreference('LANGUAGE');
    
    var filters = ['custrecord_ssmt_language', 'is', ssobjGlobal.language];
    var results = nlapiSearchRecord('customrecord_ss_missing_translation', null, filters, columns);

    if (results !== null)
    {
        for (var i = 0; i < results.length; i++)
        {
            var key = results[i].getValue('name'); //ssapiCreateKey(results[i].getValue('name'));
        
            // if string now has an entry, delete it
            if (ssobjGlobal.map[key]) {
                nlapiDeleteRecord('customrecord_ss_missing_translation', results[i].getId());
            }
            else
            {
                // still no translation
                var keyAndLanguage = key + '--' + results[i].getValue('custrecord_ssmt_language');
                ssobjGlobal.missingTranslationStrings.push(keyAndLanguage);
            }
        }
    }
    
    return ssobjGlobal.missingTranslationStrings;
}





/**
 * Adds the tl() function to String object
 */
function ssapiAddTranslateFunction()
{
    String.prototype.tl = function(key)
    {
        try
        {
            if (LANGUAGES_WITH_TRANSLATION.indexOf(ssobjGlobal.language.toLowerCase()) == -1)
            {
                // no translation
                return this.toString();
            }

            if (typeof key == 'undefined')
            {
                key = ssapiCreateKey(this.toString());
            }
            var translated;
            
            try
            {
                translated = ssobjGlobal.map[key];
            }
            catch (e)
            {
                nlapiLogExecution('error', 'ssapiAddTranslateFunction', e);
                return this.toString();
            }

            if (typeof translated == 'undefined')
            {
                if (typeof console != 'undefined')
                {
                    console.warn('WARNING: Missing translation: ' + key);
                }
                
                var isUI = typeof nlapiCreateForm == 'undefined';
                if (isUI === false)
                {
                    nlapiLogExecution('debug', 'missing translation', key);
                }

                if (ssapiIsSaveMissing())
                {
                    // log missing term
                    if (!ssobjGlobal.missingTranslationStrings)
                    {
                        ssapiGetMissingTranslationStrings();
                    }
                    var keyAndLanguage = key + '--' + ssobjGlobal.language;
                    if (ssobjGlobal.missingTranslationStrings.indexOf(keyAndLanguage) == -1)
                    {
                        // add
                        var r = nlapiCreateRecord('customrecord_ss_missing_translation');
                        if (ssapiHasValue(key))
                        {
                            nlapiLogExecution('debug', 'missing translation', key);
                            r.setFieldValue('name', key);
                            r.setFieldValue('custrecord_ssmt_string', key);
                            r.setFieldValue('custrecord_ssmt_language', ssobjGlobal.language);
                            var module = '';
                            module += ssobjGlobal.module || '';
                            module += ': ' + (ssobjGlobal.func || '');
                            
                            if (ssapiHasValue(ssobjGlobal.request))
                            {
                                module += '<br />request.method=' + ssobjGlobal.request.getMethod();
                                
                                var url = ssobjGlobal.request.getURL() + '?';
                                var params = ssobjGlobal.request.getAllParameters();
                                for (var param in params)
                                {
                                    url += param + '=' + params[param] + '&';
                                }
                                
                                module += '<br />request.url=<a href=' + url + '>' + url + '</a>';
                            }
                            
                            r.setFieldValue('custrecord_ssmt_module', module);
                            
                            nlapiSubmitRecord(r);
                            
                            ssobjGlobal.missingTranslationStrings.push(keyAndLanguage);
                        }
                    }
                }
                
                return this.toString();
            }
        }
        catch (e)
        {
            nlapiLogExecution('error', 'ssapiAddTranslateFunction', e);
            return this.toString();
        }
        
        return translated;
    };
}





/**
 * Returns true if missing translations are saved in a custom record.
 * This can be set to true by adding a row in customrecord_ss_internal_setting; setting name to recordMissingTranslation and value to T
 */
function ssapiIsSaveMissing()
{
    //	// TODO: uncomment next 2 lines when released
    //	ssobjGlobal.isSaveMissing = false;
    //	return ssobjGlobal.isSaveMissing;

    if (ssapiHasValue(ssobjGlobal.isSaveMissing))
    {
        return ssobjGlobal.isSaveMissing;
    }
    
    var logger = new ssobjLogger(arguments);
    var filters = [new nlobjSearchFilter('name', null, 'is', 'recordMissingTranslation')];
    var columns = [new nlobjSearchColumn('custrecord_ss_internal_setting_value')];
    
    var results = nlapiSearchRecord('customrecord_ss_internal_setting', null, filters, columns);
    if (results === null)
    {
        logger.log('ssapiHasNoValue(results)');
        ssobjGlobal.isSaveMissing = false;
        return ssobjGlobal.isSaveMissing;
    }

    ssobjGlobal.isSaveMissing = results[0].getValue('custrecord_ss_internal_setting_value') == 'T';
    
    return ssobjGlobal.isSaveMissing;
}
