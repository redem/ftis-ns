// ******************** START of auto-generated ORM code

var REC = {
    DEFAULT_RECORD_SUBSCRIPTION : 'customrecord_ss_def_autosub_type',
    DEFAULT_SUBSCRIPTION_ROLE : 'customrecord_ss_def_autosub_role',
    MY_RELATED_RECORD_SUBSCRIPTION : 'customrecord_ss_subscription_link_sub',
    MY_SAVED_SEARCH_SUBSCRIPTION : 'customrecord_ss_my_saved_search_sub',
    NEWSFEED_POST_LINK : 'customrecord_suitesocial_nf_post_link',
    RECORD_SUBSCRIPTION : 'customrecord_suitesocial_autosub_type',
    RECORD_SUBSCRIPTION_ROLE : 'customrecord_ss_record_subscription_role',
    RELATED_RECORD_SUBSCRIPTION : 'customrecord_ss_subscription_link',
    SAVED_SEARCH_SUBSCRIPTION : 'customrecord_ss_saved_search_subs',
    SUITESOCIAL_AUTO_SUBSCRIBE : 'customrecord_suitesocial_autosubscribe',
    SUITESOCIAL_COLLEAGUE : 'customrecord_suitesocial_colleague',
    SUITESOCIAL_DIGEST_SCHEDULE : 'customrecord_suitesocial_digest_schedule',
    SUITESOCIAL_FIELD : 'customrecord_suitesocial_field',
    SUITESOCIAL_INTERNAL_SETTING : 'customrecord_ss_internal_setting',
    SUITESOCIAL_PROFILE : 'customrecord_suitesocial_profile',
    SUITESOCIAL_PROFILE_CHANNEL : 'customrecord_suitesocial_profile_channel',
    SUITESOCIAL_RECORD : 'customrecord_suitesocial_record',
    SUITESOCIAL_RELATED_RECORD : 'customrecord_ssr_ss_related_record',
    SUITESOCIAL_ROLE : 'customrecord_suitesocial_role',
    SUITESOCIAL_SCRIPTED_RECORD : 'customrecord_suitesocial_scripted_record',
    SUITESOCIAL_STATUS : 'customrecord_suitesocial_status',
    SUITESOCIAL_STATUS_TYPE : 'customrecord_suitesocial_status_type',
    SUITESOCIAL_SUBSCRIPTION : 'customrecord_suitesocial_subscription',
    TRACKED_FIELD : 'customrecord_suitesocial_autopost_field'
};

function apiCreateDataAccessObject(recordId) {

    if (recordId == "customrecord_ss_my_related_transaction_s") {
        return new MyRelatedTransactionSubs();
    }

    if (recordId == "customrecord_ss_def_autosub_type") {
        return new DefaultRecordSubscription();
    }
    if (recordId == "customrecord_ss_def_autosub_role") {
        return new DefaultSubscriptionRole();
    }
    if (recordId == "customrecord_ss_subscription_link_sub") {
        return new MyRelatedRecordSubscription();
    }
    if (recordId == "customrecord_ss_my_saved_search_sub") {
        return new MySavedSearchSubscription();
    }
    if (recordId == "customrecord_suitesocial_nf_post_link") {
        return new NewsfeedPostLink();
    }
    if (recordId == "customrecord_suitesocial_autosub_type") {
        return new RecordSubscription();
    }
    if (recordId == "customrecord_ss_record_subscription_role") {
        return new RecordSubscriptionRole();
    }
    if (recordId == "customrecord_ss_subscription_link") {
        return new RelatedRecordSubscription();
    }
    if (recordId == "customrecord_ss_saved_search_subs") {
        return new SavedSearchSubscription();
    }
    if (recordId == "customrecord_suitesocial_autosubscribe") {
        return new SuiteSocialAutoSubscribe();
    }
    if (recordId == "customrecord_suitesocial_colleague") {
        return new SuiteSocialColleague();
    }
    if (recordId == "customrecord_suitesocial_digest_schedule") {
        return new SuiteSocialDigestSchedule();
    }
    if (recordId == "customrecord_suitesocial_field") {
        return new SuiteSocialField();
    }
    if (recordId == "customrecord_ss_internal_setting") {
        return new SuiteSocialInternalSetting();
    }
    if (recordId == "customrecord_suitesocial_profile") {
        return new SuiteSocialProfile();
    }
    if (recordId == "customrecord_suitesocial_profile_channel") {
        return new SuiteSocialProfileChannel();
    }
    if (recordId == "customrecord_suitesocial_record") {
        return new SuiteSocialRecord();
    }
    if (recordId == "customrecord_ssr_ss_related_record") {
        return new SuiteSocialRelatedRecord();
    }
    if (recordId == "customrecord_suitesocial_role") {
        return new SuiteSocialRole();
    }
    if (recordId == "customrecord_suitesocial_scripted_record") {
        return new SuiteSocialScriptedRecord();
    }
    if (recordId == "customrecord_suitesocial_subscription") {
        return new SuiteSocialSubscription();
    }
    if (recordId == "customrecord_suitesocial_autopost_field") {
        return new TrackedField();
    }

    return null;
}

/**
 * ORM for record type customrecord_ss_my_related_transaction_s
 */
function MyRelatedTransactionSubs() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_my_related_transaction_s';
    _record = nlapiCreateRecord('customrecord_ss_my_related_transaction_s', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_my_related_transaction_s', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_my_related_transaction_s
     * nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {
        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_my_related_transaction_s', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {MyRelatedTransactionSubs}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {MyRelatedTransactionSubs}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the SuiteSocial Profile
     * 
     * @return {Select}
     */
    this.getSuiteSocialProfileValue = function() {
        return this.getFieldValue('custrecord_ss_mrts_profile');
    };
    /**
     * Returns the text of select field SuiteSocial Profile
     * 
     * @return {string}
     */
    this.getSuiteSocialProfileText = function() {
        return this.getFieldText('custrecord_ss_mrts_profile');
    };

    /**
     * Returns the SuiteSocial Record
     * 
     * @return {Select}
     */
    this.getSuiteSocialRecordValue = function() {
        return this.getFieldValue('custrecord_ss_mrts_ss_record');
    };
    /**
     * Returns the text of select field SuiteSocial Record
     * 
     * @return {string}
     */
    this.getSuiteSocialRecordText = function() {
        return this.getFieldText('custrecord_ss_mrts_ss_record');
    };

    /**
     * Sets the SuiteSocial Profile
     * 
     * @param {Select}
     *        value SuiteSocial Profile
     * @return {MyRelatedTransactionSubs}
     */
    this.setSuiteSocialProfileValue = function(value) {
        this.setFieldValue('custrecord_ss_mrts_profile', value);
        return this;
    };
    /**
     * Sets the text of select field SuiteSocial Profile
     * 
     * @param {string}
     *        text
     * @return {MyRelatedTransactionSubs}
     */
    this.setSuiteSocialProfileText = function(text) {
        this.setFieldText('custrecord_ss_mrts_profile', text);
        return this;
    };

    /**
     * Sets the SuiteSocial Record
     * 
     * @param {Select}
     *        value SuiteSocial Record
     * @return {MyRelatedTransactionSubs}
     */
    this.setSuiteSocialRecordValue = function(value) {
        this.setFieldValue('custrecord_ss_mrts_ss_record', value);
        return this;
    };
    /**
     * Sets the text of select field SuiteSocial Record
     * 
     * @param {string}
     *        text
     * @return {MyRelatedTransactionSubs}
     */
    this.setSuiteSocialRecordText = function(text) {
        this.setFieldText('custrecord_ss_mrts_ss_record', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {MyRelatedTransactionSubs}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {MyRelatedTransactionSubs}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ss_mrts_profile : {
            "name" : "SuiteSocial Profile",
            "internalId" : "6458",
            "scriptId" : "custrecord_ss_mrts_profile",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "919",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        },
        custrecord_ss_mrts_ss_record : {
            "name" : "SuiteSocial Record",
            "internalId" : "6459",
            "scriptId" : "custrecord_ss_mrts_ss_record",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "928",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        }
    };
}

var R_MY_RELATED_TRANSACTION_SUBS = {
    IS_INACTIVE : 'isinactive',
    SUITESOCIAL_PROFILE : 'custrecord_ss_mrts_profile',
    SUITESOCIAL_RECORD : 'custrecord_ss_mrts_ss_record'
};

/**
 * ORM for record type customrecord_ss_def_autosub_type
 */
function DefaultRecordSubscription() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_def_autosub_type';
    _record = nlapiCreateRecord('customrecord_ss_def_autosub_type', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_def_autosub_type', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_def_autosub_type nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_def_autosub_type', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {DefaultRecordSubscription}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {DefaultRecordSubscription}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Internal Field Type
     * 
     * @return {String}
     */
    this.getInternalFieldTypeValue = function() {
        return this.getFieldValue('custrecord_ss_def_autosub_type_ft');
    };

    /**
     * Returns the Internal Record Script ID
     * 
     * @return {String}
     */
    this.getInternalRecordScriptIDValue = function() {
        return this.getFieldValue('custrecord_ss_def_autosub_type_rsi');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {DefaultRecordSubscription}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Field
     * 
     * @return {Select}
     */
    this.getFieldFieldValue = function() {
        return this.getFieldValue('custrecord_ss_def_autosub_type_fld');
    };
    /**
     * Returns the text of select field Field
     * 
     * @return {string}
     */
    this.getFieldFieldText = function() {
        return this.getFieldText('custrecord_ss_def_autosub_type_fld');
    };

    /**
     * Returns the Scripted Record Type
     * 
     * @return {Select}
     */
    this.getScriptedRecordTypeValue = function() {
        return this.getFieldValue('custrecord_ss_def_autosub_type_rt');
    };
    /**
     * Returns the text of select field Scripted Record Type
     * 
     * @return {string}
     */
    this.getScriptedRecordTypeText = function() {
        return this.getFieldText('custrecord_ss_def_autosub_type_rt');
    };

    /**
     * Sets the Field
     * 
     * @param {Select}
     *        value Field
     * @return {DefaultRecordSubscription}
     */
    this.setFieldFieldValue = function(value) {
        this.setFieldValue('custrecord_ss_def_autosub_type_fld', value);
        return this;
    };
    /**
     * Sets the text of select field Field
     * 
     * @param {string}
     *        text
     * @return {DefaultRecordSubscription}
     */
    this.setFieldFieldText = function(text) {
        this.setFieldText('custrecord_ss_def_autosub_type_fld', text);
        return this;
    };

    /**
     * Sets the Scripted Record Type
     * 
     * @param {Select}
     *        value Scripted Record Type
     * @return {DefaultRecordSubscription}
     */
    this.setScriptedRecordTypeValue = function(value) {
        this.setFieldValue('custrecord_ss_def_autosub_type_rt', value);
        return this;
    };
    /**
     * Sets the text of select field Scripted Record Type
     * 
     * @param {string}
     *        text
     * @return {DefaultRecordSubscription}
     */
    this.setScriptedRecordTypeText = function(text) {
        this.setFieldText('custrecord_ss_def_autosub_type_rt', text);
        return this;
    };
    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {DefaultRecordSubscription}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {DefaultRecordSubscription}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ss_def_autosub_type_fld : {
            "name" : "Field",
            "internalId" : "4805",
            "scriptId" : "custrecord_ss_def_autosub_type_fld",
            "dataType" : "select",
            "selectRecordTypeName" : "Field",
            "selectRecordTypeInternalId" : "-124",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_ss_def_autosub_type_ft : {
            "name" : "Internal Field Type",
            "internalId" : "4804",
            "scriptId" : "custrecord_ss_def_autosub_type_ft",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Base Type"
        },
        custrecord_ss_def_autosub_type_rsi : {
            "name" : "Internal Record Script ID",
            "internalId" : "4803",
            "scriptId" : "custrecord_ss_def_autosub_type_rsi",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Script ID"
        },
        custrecord_ss_def_autosub_type_rt : {
            "name" : "Scripted Record Type",
            "internalId" : "4802",
            "scriptId" : "custrecord_ss_def_autosub_type_rt",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_DEFAULT_RECORD_SUBSCRIPTION = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    FIELDFIELD : 'custrecord_ss_def_autosub_type_fld',
    INTERNAL_FIELD_TYPE : 'custrecord_ss_def_autosub_type_ft',
    INTERNAL_RECORD_SCRIPT_ID : 'custrecord_ss_def_autosub_type_rsi',
    SCRIPTED_RECORD_TYPE : 'custrecord_ss_def_autosub_type_rt'
};

/**
 * ORM for record type customrecord_ss_def_autosub_role
 */
function DefaultSubscriptionRole() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_def_autosub_role';
    _record = nlapiCreateRecord('customrecord_ss_def_autosub_role', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_def_autosub_role', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_def_autosub_role nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_def_autosub_role', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {DefaultSubscriptionRole}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {DefaultSubscriptionRole}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Default AutoSubscribe Type
     * 
     * @return {Select}
     */
    this.getDefaultAutoSubscribeTypeValue = function() {
        return this.getFieldValue('custrecord_ss_def_autosub_role_parent');
    };
    /**
     * Returns the text of select field Default AutoSubscribe Type
     * 
     * @return {string}
     */
    this.getDefaultAutoSubscribeTypeText = function() {
        return this.getFieldText('custrecord_ss_def_autosub_role_parent');
    };

    /**
     * Returns the Role
     * 
     * @return {Select}
     */
    this.getRoleValue = function() {
        return this.getFieldValue('custrecord_ss_def_autosub_role_id');
    };
    /**
     * Returns the text of select field Role
     * 
     * @return {string}
     */
    this.getRoleText = function() {
        return this.getFieldText('custrecord_ss_def_autosub_role_id');
    };

    /**
     * Sets the Default AutoSubscribe Type
     * 
     * @param {Select}
     *        value Default AutoSubscribe Type
     * @return {DefaultSubscriptionRole}
     */
    this.setDefaultAutoSubscribeTypeValue = function(value) {
        this.setFieldValue('custrecord_ss_def_autosub_role_parent', value);
        return this;
    };
    /**
     * Sets the text of select field Default AutoSubscribe Type
     * 
     * @param {string}
     *        text
     * @return {DefaultSubscriptionRole}
     */
    this.setDefaultAutoSubscribeTypeText = function(text) {
        this.setFieldText('custrecord_ss_def_autosub_role_parent', text);
        return this;
    };

    /**
     * Sets the Role
     * 
     * @param {Select}
     *        value Role
     * @return {DefaultSubscriptionRole}
     */
    this.setRoleValue = function(value) {
        this.setFieldValue('custrecord_ss_def_autosub_role_id', value);
        return this;
    };
    /**
     * Sets the text of select field Role
     * 
     * @param {string}
     *        text
     * @return {DefaultSubscriptionRole}
     */
    this.setRoleText = function(text) {
        this.setFieldText('custrecord_ss_def_autosub_role_id', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {DefaultSubscriptionRole}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {DefaultSubscriptionRole}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ss_def_autosub_role_parent : {
            "name" : "Default AutoSubscribe Type",
            "internalId" : "4678",
            "scriptId" : "custrecord_ss_def_autosub_role_parent",
            "dataType" : "select",
            "selectRecordTypeName" : "Default Record Subscription",
            "selectRecordTypeInternalId" : "687",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_ss_def_autosub_type",
            "selectRecordTypeShortName" : "DefaultRecordSubscription"
        },
        custrecord_ss_def_autosub_role_id : {
            "name" : "Role",
            "internalId" : "4679",
            "scriptId" : "custrecord_ss_def_autosub_role_id",
            "dataType" : "select",
            "selectRecordTypeName" : "Role",
            "selectRecordTypeInternalId" : "-118",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_DEFAULT_SUBSCRIPTION_ROLE = {
    IS_INACTIVE : 'isinactive',
    DEFAULT_AUTOSUBSCRIBE_TYPE : 'custrecord_ss_def_autosub_role_parent',
    ROLE : 'custrecord_ss_def_autosub_role_id'
};

/**
 * ORM for record type customrecord_ss_subscription_link_sub
 */
function MyRelatedRecordSubscription() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_subscription_link_sub';
    _record = nlapiCreateRecord('customrecord_ss_subscription_link_sub', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_subscription_link_sub', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_subscription_link_sub nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_subscription_link_sub', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {MyRelatedRecordSubscription}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {MyRelatedRecordSubscription}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Related Record Subscription
     * 
     * @return {Select}
     */
    this.getRelatedRecordSubscriptionValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sublink_link');
    };
    /**
     * Returns the text of select field Related Record Subscription
     * 
     * @return {string}
     */
    this.getRelatedRecordSubscriptionText = function() {
        return this.getFieldText('custrecord_suitesocial_sublink_link');
    };

    /**
     * Returns the Subscriber
     * 
     * @return {Select}
     */
    this.getSubscriberValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sublink_sub');
    };
    /**
     * Returns the text of select field Subscriber
     * 
     * @return {string}
     */
    this.getSubscriberText = function() {
        return this.getFieldText('custrecord_suitesocial_sublink_sub');
    };

    /**
     * Sets the Related Record Subscription
     * 
     * @param {Select}
     *        value Related Record Subscription
     * @return {MyRelatedRecordSubscription}
     */
    this.setRelatedRecordSubscriptionValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sublink_link', value);
        return this;
    };
    /**
     * Sets the text of select field Related Record Subscription
     * 
     * @param {string}
     *        text
     * @return {MyRelatedRecordSubscription}
     */
    this.setRelatedRecordSubscriptionText = function(text) {
        this.setFieldText('custrecord_suitesocial_sublink_link', text);
        return this;
    };

    /**
     * Sets the Subscriber
     * 
     * @param {Select}
     *        value Subscriber
     * @return {MyRelatedRecordSubscription}
     */
    this.setSubscriberValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sublink_sub', value);
        return this;
    };
    /**
     * Sets the text of select field Subscriber
     * 
     * @param {string}
     *        text
     * @return {MyRelatedRecordSubscription}
     */
    this.setSubscriberText = function(text) {
        this.setFieldText('custrecord_suitesocial_sublink_sub', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {MyRelatedRecordSubscription}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {MyRelatedRecordSubscription}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_sublink_link : {
            "name" : "Related Record Subscription",
            "internalId" : "4715",
            "scriptId" : "custrecord_suitesocial_sublink_link",
            "dataType" : "select",
            "selectRecordTypeName" : "Related Record Subscription",
            "selectRecordTypeInternalId" : "661",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_ss_subscription_link",
            "selectRecordTypeShortName" : "RelatedRecordSubscription"
        },
        custrecord_suitesocial_sublink_sub : {
            "name" : "Subscriber",
            "internalId" : "4714",
            "scriptId" : "custrecord_suitesocial_sublink_sub",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "683",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        }
    };
}

var R_MY_RELATED_RECORD_SUBSCRIPTION = {
    IS_INACTIVE : 'isinactive',
    RELATED_RECORD_SUBSCRIPTION : 'custrecord_suitesocial_sublink_link',
    SUBSCRIBER : 'custrecord_suitesocial_sublink_sub'
};

/**
 * ORM for record type customrecord_ss_my_saved_search_sub
 */
function MySavedSearchSubscription() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_my_saved_search_sub';
    _record = nlapiCreateRecord('customrecord_ss_my_saved_search_sub', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_my_saved_search_sub', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_my_saved_search_sub nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_my_saved_search_sub', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {MySavedSearchSubscription}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {MySavedSearchSubscription}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Profile
     * 
     * @return {Select}
     */
    this.getProfileValue = function() {
        return this.getFieldValue('custrecord_ss_msss_profile');
    };
    /**
     * Returns the text of select field Profile
     * 
     * @return {string}
     */
    this.getProfileText = function() {
        return this.getFieldText('custrecord_ss_msss_profile');
    };

    /**
     * Returns the Saved Search
     * 
     * @return {Select}
     */
    this.getSavedSearchValue = function() {
        return this.getFieldValue('custrecord_ss_msss_saved_search');
    };
    /**
     * Returns the text of select field Saved Search
     * 
     * @return {string}
     */
    this.getSavedSearchText = function() {
        return this.getFieldText('custrecord_ss_msss_saved_search');
    };

    /**
     * Sets the Profile
     * 
     * @param {Select}
     *        value Profile
     * @return {MySavedSearchSubscription}
     */
    this.setProfileValue = function(value) {
        this.setFieldValue('custrecord_ss_msss_profile', value);
        return this;
    };
    /**
     * Sets the text of select field Profile
     * 
     * @param {string}
     *        text
     * @return {MySavedSearchSubscription}
     */
    this.setProfileText = function(text) {
        this.setFieldText('custrecord_ss_msss_profile', text);
        return this;
    };

    /**
     * Sets the Saved Search
     * 
     * @param {Select}
     *        value Saved Search
     * @return {MySavedSearchSubscription}
     */
    this.setSavedSearchValue = function(value) {
        this.setFieldValue('custrecord_ss_msss_saved_search', value);
        return this;
    };
    /**
     * Sets the text of select field Saved Search
     * 
     * @param {string}
     *        text
     * @return {MySavedSearchSubscription}
     */
    this.setSavedSearchText = function(text) {
        this.setFieldText('custrecord_ss_msss_saved_search', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {MySavedSearchSubscription}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {MySavedSearchSubscription}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ss_msss_profile : {
            "name" : "Profile",
            "internalId" : "4834",
            "scriptId" : "custrecord_ss_msss_profile",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "683",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        },
        custrecord_ss_msss_saved_search : {
            "name" : "Saved Search",
            "internalId" : "4835",
            "scriptId" : "custrecord_ss_msss_saved_search",
            "dataType" : "select",
            "selectRecordTypeName" : "Saved Search Subscription",
            "selectRecordTypeInternalId" : "693",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_ss_saved_search_subs",
            "selectRecordTypeShortName" : "SavedSearchSubscription"
        }
    };
}

var R_MY_SAVED_SEARCH_SUBSCRIPTION = {
    IS_INACTIVE : 'isinactive',
    PROFILE : 'custrecord_ss_msss_profile',
    SAVED_SEARCH : 'custrecord_ss_msss_saved_search'
};

/**
 * ORM for record type customrecord_suitesocial_nf_post_link
 */
function NewsfeedPostLink() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_nf_post_link';
    _record = nlapiCreateRecord('customrecord_suitesocial_nf_post_link', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_nf_post_link', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_nf_post_link nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_nf_post_link', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {NewsfeedPostLink}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {NewsfeedPostLink}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Post
     * 
     * @return {Select}
     */
    this.getPostValue = function() {
        return this.getFieldValue('custrecord_newsfeed_post_link_post');
    };
    /**
     * Returns the text of select field Post
     * 
     * @return {string}
     */
    this.getPostText = function() {
        return this.getFieldText('custrecord_newsfeed_post_link_post');
    };

    /**
     * Returns the Recipient
     * 
     * @return {Select}
     */
    this.getRecipientValue = function() {
        return this.getFieldValue('custrecord_newsfeed_post_link_recipient');
    };
    /**
     * Returns the text of select field Recipient
     * 
     * @return {string}
     */
    this.getRecipientText = function() {
        return this.getFieldText('custrecord_newsfeed_post_link_recipient');
    };

    /**
     * Sets the Post
     * 
     * @param {Select}
     *        value Post
     * @return {NewsfeedPostLink}
     */
    this.setPostValue = function(value) {
        this.setFieldValue('custrecord_newsfeed_post_link_post', value);
        return this;
    };
    /**
     * Sets the text of select field Post
     * 
     * @param {string}
     *        text
     * @return {NewsfeedPostLink}
     */
    this.setPostText = function(text) {
        this.setFieldText('custrecord_newsfeed_post_link_post', text);
        return this;
    };

    /**
     * Sets the Recipient
     * 
     * @param {Select}
     *        value Recipient
     * @return {NewsfeedPostLink}
     */
    this.setRecipientValue = function(value) {
        this.setFieldValue('custrecord_newsfeed_post_link_recipient', value);
        return this;
    };
    /**
     * Sets the text of select field Recipient
     * 
     * @param {string}
     *        text
     * @return {NewsfeedPostLink}
     */
    this.setRecipientText = function(text) {
        this.setFieldText('custrecord_newsfeed_post_link_recipient', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {NewsfeedPostLink}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {NewsfeedPostLink}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_newsfeed_post_link_post : {
            "name" : "Post",
            "internalId" : "4680",
            "scriptId" : "custrecord_newsfeed_post_link_post",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial News Feed",
            "selectRecordTypeInternalId" : "678",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_newsfeed",
            "selectRecordTypeShortName" : "SuiteSocialNewsFeed"
        },
        custrecord_newsfeed_post_link_recipient : {
            "name" : "Recipient",
            "internalId" : "4681",
            "scriptId" : "custrecord_newsfeed_post_link_recipient",
            "dataType" : "select",
            "selectRecordTypeName" : "Employee",
            "selectRecordTypeInternalId" : "-4",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "employee",
            "selectRecordTypeShortName" : "Employee"
        }
    };
}

var R_NEWSFEED_POST_LINK = {
    IS_INACTIVE : 'isinactive',
    POST : 'custrecord_newsfeed_post_link_post',
    RECIPIENT : 'custrecord_newsfeed_post_link_recipient'
};

/**
 * ORM for record type customrecord_suitesocial_autosub_type
 */
function RecordSubscription() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_autosub_type';
    _record = nlapiCreateRecord('customrecord_suitesocial_autosub_type', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_autosub_type', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_autosub_type nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_autosub_type', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {RecordSubscription}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {RecordSubscription}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Field ID
     * 
     * @return {String}
     */
    this.getFieldIDValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_fid');
    };

    /**
     * Returns the Internal Field Type
     * 
     * @return {String}
     */
    this.getInternalFieldTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_ft');
    };

    /**
     * Returns the Internal Record Script ID
     * 
     * @return {String}
     */
    this.getInternalRecordScriptIDValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_rsi');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {RecordSubscription}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Field
     * 
     * @return {Select}
     */
    this.getFieldFieldValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_rf');
    };
    /**
     * Returns the text of select field Field
     * 
     * @return {string}
     */
    this.getFieldFieldText = function() {
        return this.getFieldText('custrecord_suitesocial_autosub_type_rf');
    };

    /**
     * Returns the Field (hidden)
     * 
     * @return {Select}
     */
    this.getFieldHiddenValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_fld');
    };
    /**
     * Returns the text of select field Field (hidden)
     * 
     * @return {string}
     */
    this.getFieldHiddenText = function() {
        return this.getFieldText('custrecord_suitesocial_autosub_type_fld');
    };

    /**
     * Returns the Internal Record Type
     * 
     * @return {Select}
     */
    this.getInternalRecordTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_rt');
    };
    /**
     * Returns the text of select field Internal Record Type
     * 
     * @return {string}
     */
    this.getInternalRecordTypeText = function() {
        return this.getFieldText('custrecord_suitesocial_autosub_type_rt');
    };

    /**
     * Returns the Saved Search
     * 
     * @return {Select}
     */
    this.getSavedSearchValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_ss');
    };
    /**
     * Returns the text of select field Saved Search
     * 
     * @return {string}
     */
    this.getSavedSearchText = function() {
        return this.getFieldText('custrecord_suitesocial_autosub_type_ss');
    };

    /**
     * Returns the SuiteSocial Record Type
     * 
     * @return {Select}
     */
    this.getSuiteSocialRecordTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosub_type_rec');
    };
    /**
     * Returns the text of select field SuiteSocial Record Type
     * 
     * @return {string}
     */
    this.getSuiteSocialRecordTypeText = function() {
        return this.getFieldText('custrecord_suitesocial_autosub_type_rec');
    };

    /**
     * Sets the Field
     * 
     * @param {Select}
     *        value Field
     * @return {RecordSubscription}
     */
    this.setFieldFieldValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_autosub_type_rf', value);
        return this;
    };
    /**
     * Sets the text of select field Field
     * 
     * @param {string}
     *        text
     * @return {RecordSubscription}
     */
    this.setFieldFieldText = function(text) {
        this.setFieldText('custrecord_suitesocial_autosub_type_rf', text);
        return this;
    };

    /**
     * Sets the Saved Search
     * 
     * @param {Select}
     *        value Saved Search
     * @return {RecordSubscription}
     */
    this.setSavedSearchValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_autosub_type_ss', value);
        return this;
    };
    /**
     * Sets the text of select field Saved Search
     * 
     * @param {string}
     *        text
     * @return {RecordSubscription}
     */
    this.setSavedSearchText = function(text) {
        this.setFieldText('custrecord_suitesocial_autosub_type_ss', text);
        return this;
    };

    /**
     * Sets the SuiteSocial Record Type
     * 
     * @param {Select}
     *        value SuiteSocial Record Type
     * @return {RecordSubscription}
     */
    this.setSuiteSocialRecordTypeValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_autosub_type_rec', value);
        return this;
    };
    /**
     * Sets the text of select field SuiteSocial Record Type
     * 
     * @param {string}
     *        text
     * @return {RecordSubscription}
     */
    this.setSuiteSocialRecordTypeText = function(text) {
        this.setFieldText('custrecord_suitesocial_autosub_type_rec', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {RecordSubscription}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {RecordSubscription}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_autosub_type_rf : {
            "name" : "Field",
            "internalId" : "4815",
            "scriptId" : "custrecord_suitesocial_autosub_type_rf",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Field",
            "selectRecordTypeInternalId" : "660",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_field",
            "selectRecordTypeShortName" : "SuiteSocialField"
        },
        custrecord_suitesocial_autosub_type_fld : {
            "name" : "Field (hidden)",
            "internalId" : "4809",
            "scriptId" : "custrecord_suitesocial_autosub_type_fld",
            "dataType" : "select",
            "selectRecordTypeName" : "Field",
            "selectRecordTypeInternalId" : "-124",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Record Field"
        },
        custrecord_suitesocial_autosub_type_fid : {
            "name" : "Field ID",
            "internalId" : "4810",
            "scriptId" : "custrecord_suitesocial_autosub_type_fid",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Script Id"
        },
        custrecord_suitesocial_autosub_type_ft : {
            "name" : "Internal Field Type",
            "internalId" : "4811",
            "scriptId" : "custrecord_suitesocial_autosub_type_ft",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Hidden",
            "sourcefrom" : "Base Type"
        },
        custrecord_suitesocial_autosub_type_rsi : {
            "name" : "Internal Record Script ID",
            "internalId" : "4812",
            "scriptId" : "custrecord_suitesocial_autosub_type_rsi",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Script ID"
        },
        custrecord_suitesocial_autosub_type_rt : {
            "name" : "Internal Record Type",
            "internalId" : "4813",
            "scriptId" : "custrecord_suitesocial_autosub_type_rt",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Hidden",
            "sourcefrom" : "Type"
        },
        custrecord_suitesocial_autosub_type_ss : {
            "name" : "Saved Search",
            "internalId" : "4833",
            "scriptId" : "custrecord_suitesocial_autosub_type_ss",
            "dataType" : "select",
            "selectRecordTypeName" : "Saved Search",
            "selectRecordTypeInternalId" : "-119",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_autosub_type_rec : {
            "name" : "SuiteSocial Record Type",
            "internalId" : "4814",
            "scriptId" : "custrecord_suitesocial_autosub_type_rec",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "686",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        }
    };
}

var R_RECORD_SUBSCRIPTION = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    FIELDFIELD : 'custrecord_suitesocial_autosub_type_rf',
    FIELD_HIDDEN : 'custrecord_suitesocial_autosub_type_fld',
    FIELD_ID : 'custrecord_suitesocial_autosub_type_fid',
    INTERNAL_FIELD_TYPE : 'custrecord_suitesocial_autosub_type_ft',
    INTERNAL_RECORD_SCRIPT_ID : 'custrecord_suitesocial_autosub_type_rsi',
    INTERNAL_RECORD_TYPE : 'custrecord_suitesocial_autosub_type_rt',
    SAVED_SEARCH : 'custrecord_suitesocial_autosub_type_ss',
    SUITESOCIAL_RECORD_TYPE : 'custrecord_suitesocial_autosub_type_rec'
};

/**
 * ORM for record type customrecord_ss_record_subscription_role
 */
function RecordSubscriptionRole() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_record_subscription_role';
    _record = nlapiCreateRecord('customrecord_ss_record_subscription_role', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_record_subscription_role', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_record_subscription_role
     * nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_record_subscription_role', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {RecordSubscriptionRole}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {RecordSubscriptionRole}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Record Subscription
     * 
     * @return {Select}
     */
    this.getRecordSubscriptionValue = function() {
        return this.getFieldValue('custrecord_ssrsr_record_subscription');
    };
    /**
     * Returns the text of select field Record Subscription
     * 
     * @return {string}
     */
    this.getRecordSubscriptionText = function() {
        return this.getFieldText('custrecord_ssrsr_record_subscription');
    };

    /**
     * Returns the Role
     * 
     * @return {Select}
     */
    this.getRoleValue = function() {
        return this.getFieldValue('custrecord_ssrsr_role');
    };
    /**
     * Returns the text of select field Role
     * 
     * @return {string}
     */
    this.getRoleText = function() {
        return this.getFieldText('custrecord_ssrsr_role');
    };

    /**
     * Sets the Record Subscription
     * 
     * @param {Select}
     *        value Record Subscription
     * @return {RecordSubscriptionRole}
     */
    this.setRecordSubscriptionValue = function(value) {
        this.setFieldValue('custrecord_ssrsr_record_subscription', value);
        return this;
    };
    /**
     * Sets the text of select field Record Subscription
     * 
     * @param {string}
     *        text
     * @return {RecordSubscriptionRole}
     */
    this.setRecordSubscriptionText = function(text) {
        this.setFieldText('custrecord_ssrsr_record_subscription', text);
        return this;
    };

    /**
     * Sets the Role
     * 
     * @param {Select}
     *        value Role
     * @return {RecordSubscriptionRole}
     */
    this.setRoleValue = function(value) {
        this.setFieldValue('custrecord_ssrsr_role', value);
        return this;
    };
    /**
     * Sets the text of select field Role
     * 
     * @param {string}
     *        text
     * @return {RecordSubscriptionRole}
     */
    this.setRoleText = function(text) {
        this.setFieldText('custrecord_ssrsr_role', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {RecordSubscriptionRole}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {RecordSubscriptionRole}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ssrsr_record_subscription : {
            "name" : "Record Subscription",
            "internalId" : "4797",
            "scriptId" : "custrecord_ssrsr_record_subscription",
            "dataType" : "select",
            "selectRecordTypeName" : "Record Subscription",
            "selectRecordTypeInternalId" : "689",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_autosub_type",
            "selectRecordTypeShortName" : "RecordSubscription"
        },
        custrecord_ssrsr_role : {
            "name" : "Role",
            "internalId" : "4798",
            "scriptId" : "custrecord_ssrsr_role",
            "dataType" : "select",
            "selectRecordTypeName" : "Role",
            "selectRecordTypeInternalId" : "-118",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_RECORD_SUBSCRIPTION_ROLE = {
    IS_INACTIVE : 'isinactive',
    RECORD_SUBSCRIPTION : 'custrecord_ssrsr_record_subscription',
    ROLE : 'custrecord_ssrsr_role'
};

/**
 * ORM for record type customrecord_ss_subscription_link
 */
function RelatedRecordSubscription() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_subscription_link';
    _record = nlapiCreateRecord('customrecord_ss_subscription_link', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_subscription_link', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_subscription_link nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_subscription_link', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {RelatedRecordSubscription}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {RelatedRecordSubscription}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Field DataTypeName
     * 
     * @return {String}
     */
    this.getFieldDataTypeNameValue = function() {
        return this.getFieldValue('custrecord_sssl_field_datatypename');
    };

    /**
     * Returns the Field ScriptId
     * 
     * @return {String}
     */
    this.getFieldScriptIdValue = function() {
        return this.getFieldValue('custrecord_sssl_field_scriptid');
    };

    /**
     * Returns the Field Type
     * 
     * @return {String}
     */
    this.getFieldTypeValue = function() {
        return this.getFieldValue('custrecord_sssl_field_fieldtype');
    };

    /**
     * Returns the Rel SS Record ScrRecType Script Id
     * 
     * @return {String}
     */
    this.getRelSSRecordScrRecTypeScriptIdValue = function() {
        return this.getFieldValue('custrecord_sssl_rel_ss_rec_scrrectype_si');
    };

    /**
     * Returns the Rel SS Record ScrRecType Text
     * 
     * @return {String}
     */
    this.getRelSSRecordScrRecTypeTextValue = function() {
        return this.getFieldValue('custrecord_sssl_rel_ss_rec_srcrectype_te');
    };

    /**
     * Returns the SS Record ScrRecType BaseType
     * 
     * @return {String}
     */
    this.getSSRecordScrRecTypeBaseTypeValue = function() {
        return this.getFieldValue('custrecord_sssl_ss_rec_scrrectype_basety');
    };

    /**
     * Returns the SS Record ScrRecType Script Id
     * 
     * @return {String}
     */
    this.getSSRecordScrRecTypeScriptIdValue = function() {
        return this.getFieldValue('custrecord_sssl_ss_rec_scrrectype_si');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {RelatedRecordSubscription}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Field RecType
     * 
     * @return {Select}
     */
    this.getFieldRecTypeValue = function() {
        return this.getFieldValue('custrecord_sssl_field_rectype');
    };
    /**
     * Returns the text of select field Field RecType
     * 
     * @return {string}
     */
    this.getFieldRecTypeText = function() {
        return this.getFieldText('custrecord_sssl_field_rectype');
    };

    /**
     * Returns the Rel SS Record ScrRecType
     * 
     * @return {Select}
     */
    this.getRelSSRecordScrRecTypeValue = function() {
        return this.getFieldValue('custrecord_sssl_rel_ss_rec_srcrectype');
    };
    /**
     * Returns the text of select field Rel SS Record ScrRecType
     * 
     * @return {string}
     */
    this.getRelSSRecordScrRecTypeText = function() {
        return this.getFieldText('custrecord_sssl_rel_ss_rec_srcrectype');
    };

    /**
     * Returns the Rel SS Record ScrRecType RecType
     * 
     * @return {Select}
     */
    this.getRelSSRecordScrRecTypeRecTypeValue = function() {
        return this.getFieldValue('custrecord_sssl_rel_ss_rec_srcrectype_re');
    };
    /**
     * Returns the text of select field Rel SS Record ScrRecType RecType
     * 
     * @return {string}
     */
    this.getRelSSRecordScrRecTypeRecTypeText = function() {
        return this.getFieldText('custrecord_sssl_rel_ss_rec_srcrectype_re');
    };

    /**
     * Returns the Related Record
     * 
     * @return {Select}
     */
    this.getRelatedRecordValue = function() {
        return this.getFieldValue('custrecord_sssl_ss_rec');
    };
    /**
     * Returns the text of select field Related Record
     * 
     * @return {string}
     */
    this.getRelatedRecordText = function() {
        return this.getFieldText('custrecord_sssl_ss_rec');
    };

    /**
     * Returns the Related Record Field
     * 
     * @return {Select}
     */
    this.getRelatedRecordFieldValue = function() {
        return this.getFieldValue('custrecord_sssl_record_field');
    };
    /**
     * Returns the text of select field Related Record Field
     * 
     * @return {string}
     */
    this.getRelatedRecordFieldText = function() {
        return this.getFieldText('custrecord_sssl_record_field');
    };

    /**
     * Returns the Related Record Field (Hidden)
     * 
     * @return {Select}
     */
    this.getRelatedRecordFieldHiddenValue = function() {
        return this.getFieldValue('custrecord_sssl_field');
    };
    /**
     * Returns the text of select field Related Record Field (Hidden)
     * 
     * @return {string}
     */
    this.getRelatedRecordFieldHiddenText = function() {
        return this.getFieldText('custrecord_sssl_field');
    };

    /**
     * Returns the Related Record SS Record
     * 
     * @return {Select}
     */
    this.getRelatedRecordSSRecordValue = function() {
        return this.getFieldValue('custrecord_sssl_ss_rec_ss_rec');
    };
    /**
     * Returns the text of select field Related Record SS Record
     * 
     * @return {string}
     */
    this.getRelatedRecordSSRecordText = function() {
        return this.getFieldText('custrecord_sssl_ss_rec_ss_rec');
    };

    /**
     * Returns the SS Record ScrRecType
     * 
     * @return {Select}
     */
    this.getSSRecordScrRecTypeValue = function() {
        return this.getFieldValue('custrecord_sssl_ss_rec_scrrectype');
    };
    /**
     * Returns the text of select field SS Record ScrRecType
     * 
     * @return {string}
     */
    this.getSSRecordScrRecTypeText = function() {
        return this.getFieldText('custrecord_sssl_ss_rec_scrrectype');
    };

    /**
     * Returns the SS Record ScrRecType RecType
     * 
     * @return {Select}
     */
    this.getSSRecordScrRecTypeRecTypeValue = function() {
        return this.getFieldValue('custrecord_sssl_ss_rec_scrrectype_rectyp');
    };
    /**
     * Returns the text of select field SS Record ScrRecType RecType
     * 
     * @return {string}
     */
    this.getSSRecordScrRecTypeRecTypeText = function() {
        return this.getFieldText('custrecord_sssl_ss_rec_scrrectype_rectyp');
    };

    /**
     * Returns the SuiteSocial Record
     * 
     * @return {Select}
     */
    this.getSuiteSocialRecordValue = function() {
        return this.getFieldValue('custrecord_sssl_rel_ss_rec');
    };
    /**
     * Returns the text of select field SuiteSocial Record
     * 
     * @return {string}
     */
    this.getSuiteSocialRecordText = function() {
        return this.getFieldText('custrecord_sssl_rel_ss_rec');
    };

    /**
     * Sets the Related Record
     * 
     * @param {Select}
     *        value Related Record
     * @return {RelatedRecordSubscription}
     */
    this.setRelatedRecordValue = function(value) {
        this.setFieldValue('custrecord_sssl_ss_rec', value);
        return this;
    };
    /**
     * Sets the text of select field Related Record
     * 
     * @param {string}
     *        text
     * @return {RelatedRecordSubscription}
     */
    this.setRelatedRecordText = function(text) {
        this.setFieldText('custrecord_sssl_ss_rec', text);
        return this;
    };

    /**
     * Sets the Related Record Field
     * 
     * @param {Select}
     *        value Related Record Field
     * @return {RelatedRecordSubscription}
     */
    this.setRelatedRecordFieldValue = function(value) {
        this.setFieldValue('custrecord_sssl_record_field', value);
        return this;
    };
    /**
     * Sets the text of select field Related Record Field
     * 
     * @param {string}
     *        text
     * @return {RelatedRecordSubscription}
     */
    this.setRelatedRecordFieldText = function(text) {
        this.setFieldText('custrecord_sssl_record_field', text);
        return this;
    };

    /**
     * Sets the SuiteSocial Record
     * 
     * @param {Select}
     *        value SuiteSocial Record
     * @return {RelatedRecordSubscription}
     */
    this.setSuiteSocialRecordValue = function(value) {
        this.setFieldValue('custrecord_sssl_rel_ss_rec', value);
        return this;
    };
    /**
     * Sets the text of select field SuiteSocial Record
     * 
     * @param {string}
     *        text
     * @return {RelatedRecordSubscription}
     */
    this.setSuiteSocialRecordText = function(text) {
        this.setFieldText('custrecord_sssl_rel_ss_rec', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {RelatedRecordSubscription}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {RelatedRecordSubscription}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_sssl_field_datatypename : {
            "name" : "Field DataTypeName",
            "internalId" : "4661",
            "scriptId" : "custrecord_sssl_field_datatypename",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Data Type (Name)"
        },
        custrecord_sssl_field_rectype : {
            "name" : "Field RecType",
            "internalId" : "4666",
            "scriptId" : "custrecord_sssl_field_rectype",
            "dataType" : "select",
            "selectRecordTypeName" : "Record Type",
            "selectRecordTypeInternalId" : "-123",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "List/Record"
        },
        custrecord_sssl_field_scriptid : {
            "name" : "Field ScriptId",
            "internalId" : "4663",
            "scriptId" : "custrecord_sssl_field_scriptid",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Script ID"
        },
        custrecord_sssl_field_fieldtype : {
            "name" : "Field Type",
            "internalId" : "4665",
            "scriptId" : "custrecord_sssl_field_fieldtype",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Type"
        },
        custrecord_sssl_rel_ss_rec_srcrectype : {
            "name" : "Rel SS Record ScrRecType",
            "internalId" : "4655",
            "scriptId" : "custrecord_sssl_rel_ss_rec_srcrectype",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Type"
        },
        custrecord_sssl_rel_ss_rec_srcrectype_re : {
            "name" : "Rel SS Record ScrRecType RecType",
            "internalId" : "4656",
            "scriptId" : "custrecord_sssl_rel_ss_rec_srcrectype_re",
            "dataType" : "select",
            "selectRecordTypeName" : "Record Type",
            "selectRecordTypeInternalId" : "-123",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "List/Record"
        },
        custrecord_sssl_rel_ss_rec_scrrectype_si : {
            "name" : "Rel SS Record ScrRecType Script Id",
            "internalId" : "4654",
            "scriptId" : "custrecord_sssl_rel_ss_rec_scrrectype_si",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Script ID"
        },
        custrecord_sssl_rel_ss_rec_srcrectype_te : {
            "name" : "Rel SS Record ScrRecType Text",
            "internalId" : "4662",
            "scriptId" : "custrecord_sssl_rel_ss_rec_srcrectype_te",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "List/Record"
        },
        custrecord_sssl_ss_rec : {
            "name" : "Related Record",
            "internalId" : "4652",
            "scriptId" : "custrecord_sssl_ss_rec",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Related Record",
            "selectRecordTypeInternalId" : "665",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_ssr_ss_related_record",
            "selectRecordTypeShortName" : "SuiteSocialRelatedRecord"
        },
        custrecord_sssl_record_field : {
            "name" : "Related Record Field",
            "internalId" : "4668",
            "scriptId" : "custrecord_sssl_record_field",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Field",
            "selectRecordTypeInternalId" : "660",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_field",
            "selectRecordTypeShortName" : "SuiteSocialField"
        },
        custrecord_sssl_field : {
            "name" : "Related Record Field (Hidden)",
            "internalId" : "4664",
            "scriptId" : "custrecord_sssl_field",
            "dataType" : "select",
            "selectRecordTypeName" : "Field",
            "selectRecordTypeInternalId" : "-124",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Record Field"
        },
        custrecord_sssl_ss_rec_ss_rec : {
            "name" : "Related Record SS Record",
            "internalId" : "4667",
            "scriptId" : "custrecord_sssl_ss_rec_ss_rec",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "686",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Related SuiteSocial Record",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        },
        custrecord_sssl_ss_rec_scrrectype : {
            "name" : "SS Record ScrRecType",
            "internalId" : "4657",
            "scriptId" : "custrecord_sssl_ss_rec_scrrectype",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "SuiteSocial Record Type"
        },
        custrecord_sssl_ss_rec_scrrectype_basety : {
            "name" : "SS Record ScrRecType BaseType",
            "internalId" : "4659",
            "scriptId" : "custrecord_sssl_ss_rec_scrrectype_basety",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Base Type"
        },
        custrecord_sssl_ss_rec_scrrectype_rectyp : {
            "name" : "SS Record ScrRecType RecType",
            "internalId" : "4658",
            "scriptId" : "custrecord_sssl_ss_rec_scrrectype_rectyp",
            "dataType" : "select",
            "selectRecordTypeName" : "Record Type",
            "selectRecordTypeInternalId" : "-123",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "List/Record"
        },
        custrecord_sssl_ss_rec_scrrectype_si : {
            "name" : "SS Record ScrRecType Script Id",
            "internalId" : "4653",
            "scriptId" : "custrecord_sssl_ss_rec_scrrectype_si",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Script ID"
        },
        custrecord_sssl_rel_ss_rec : {
            "name" : "SuiteSocial Record",
            "internalId" : "4660",
            "scriptId" : "custrecord_sssl_rel_ss_rec",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "686",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Disabled",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        }
    };
}

var R_RELATED_RECORD_SUBSCRIPTION = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    FIELD_DATATYPENAME : 'custrecord_sssl_field_datatypename',
    FIELD_RECTYPE : 'custrecord_sssl_field_rectype',
    FIELD_SCRIPTID : 'custrecord_sssl_field_scriptid',
    FIELD_TYPE : 'custrecord_sssl_field_fieldtype',
    REL_SS_RECORD_SCRRECTYPE : 'custrecord_sssl_rel_ss_rec_srcrectype',
    REL_SS_RECORD_SCRRECTYPE_RECTYPE : 'custrecord_sssl_rel_ss_rec_srcrectype_re',
    REL_SS_RECORD_SCRRECTYPE_SCRIPT_ID : 'custrecord_sssl_rel_ss_rec_scrrectype_si',
    REL_SS_RECORD_SCRRECTYPE_TEXT : 'custrecord_sssl_rel_ss_rec_srcrectype_te',
    RELATED_RECORD : 'custrecord_sssl_ss_rec',
    RELATED_RECORD_FIELD : 'custrecord_sssl_record_field',
    RELATED_RECORD_FIELD_HIDDEN : 'custrecord_sssl_field',
    RELATED_RECORD_SS_RECORD : 'custrecord_sssl_ss_rec_ss_rec',
    SS_RECORD_SCRRECTYPE : 'custrecord_sssl_ss_rec_scrrectype',
    SS_RECORD_SCRRECTYPE_BASETYPE : 'custrecord_sssl_ss_rec_scrrectype_basety',
    SS_RECORD_SCRRECTYPE_RECTYPE : 'custrecord_sssl_ss_rec_scrrectype_rectyp',
    SS_RECORD_SCRRECTYPE_SCRIPT_ID : 'custrecord_sssl_ss_rec_scrrectype_si',
    SUITESOCIAL_RECORD : 'custrecord_sssl_rel_ss_rec'
};

/**
 * ORM for record type customrecord_ss_saved_search_subs
 */
function SavedSearchSubscription() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_saved_search_subs';
    _record = nlapiCreateRecord('customrecord_ss_saved_search_subs', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_saved_search_subs', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_saved_search_subs nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_saved_search_subs', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SavedSearchSubscription}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SavedSearchSubscription}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Record Script Id
     * 
     * @return {String}
     */
    this.getRecordScriptIdValue = function() {
        return this.getFieldValue('custrecord_sss_record_script_id');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SavedSearchSubscription}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Record Type
     * 
     * @return {Select}
     */
    this.getRecordTypeValue = function() {
        return this.getFieldValue('custrecord_sss_record_type');
    };
    /**
     * Returns the text of select field Record Type
     * 
     * @return {string}
     */
    this.getRecordTypeText = function() {
        return this.getFieldText('custrecord_sss_record_type');
    };

    /**
     * Returns the Saved Search
     * 
     * @return {Select}
     */
    this.getSavedSearchValue = function() {
        return this.getFieldValue('custrecord_sss_saved_search');
    };
    /**
     * Returns the text of select field Saved Search
     * 
     * @return {string}
     */
    this.getSavedSearchText = function() {
        return this.getFieldText('custrecord_sss_saved_search');
    };

    /**
     * Returns the Scripted Record Type
     * 
     * @return {Select}
     */
    this.getScriptedRecordTypeValue = function() {
        return this.getFieldValue('custrecord_sss_scripted_record_type');
    };
    /**
     * Returns the text of select field Scripted Record Type
     * 
     * @return {string}
     */
    this.getScriptedRecordTypeText = function() {
        return this.getFieldText('custrecord_sss_scripted_record_type');
    };

    /**
     * Sets the Record Type
     * 
     * @param {Select}
     *        value Record Type
     * @return {SavedSearchSubscription}
     */
    this.setRecordTypeValue = function(value) {
        this.setFieldValue('custrecord_sss_record_type', value);
        return this;
    };
    /**
     * Sets the text of select field Record Type
     * 
     * @param {string}
     *        text
     * @return {SavedSearchSubscription}
     */
    this.setRecordTypeText = function(text) {
        this.setFieldText('custrecord_sss_record_type', text);
        return this;
    };

    /**
     * Sets the Saved Search
     * 
     * @param {Select}
     *        value Saved Search
     * @return {SavedSearchSubscription}
     */
    this.setSavedSearchValue = function(value) {
        this.setFieldValue('custrecord_sss_saved_search', value);
        return this;
    };
    /**
     * Sets the text of select field Saved Search
     * 
     * @param {string}
     *        text
     * @return {SavedSearchSubscription}
     */
    this.setSavedSearchText = function(text) {
        this.setFieldText('custrecord_sss_saved_search', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SavedSearchSubscription}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SavedSearchSubscription}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_sss_record_script_id : {
            "name" : "Record Script Id",
            "internalId" : "4829",
            "scriptId" : "custrecord_sss_record_script_id",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Script ID"
        },
        custrecord_sss_record_type : {
            "name" : "Record Type",
            "internalId" : "4828",
            "scriptId" : "custrecord_sss_record_type",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "686",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        },
        custrecord_sss_saved_search : {
            "name" : "Saved Search",
            "internalId" : "4827",
            "scriptId" : "custrecord_sss_saved_search",
            "dataType" : "select",
            "selectRecordTypeName" : "Saved Search",
            "selectRecordTypeInternalId" : "-119",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : ""
        },
        custrecord_sss_scripted_record_type : {
            "name" : "Scripted Record Type",
            "internalId" : "4830",
            "scriptId" : "custrecord_sss_scripted_record_type",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Type"
        }
    };
}

var R_SAVED_SEARCH_SUBSCRIPTION = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    RECORD_SCRIPT_ID : 'custrecord_sss_record_script_id',
    RECORD_TYPE : 'custrecord_sss_record_type',
    SAVED_SEARCH : 'custrecord_sss_saved_search',
    SCRIPTED_RECORD_TYPE : 'custrecord_sss_scripted_record_type'
};

/**
 * ORM for record type customrecord_suitesocial_autosubscribe
 */
function SuiteSocialAutoSubscribe() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_autosubscribe';
    _record = nlapiCreateRecord('customrecord_suitesocial_autosubscribe', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_autosubscribe', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_autosubscribe
     * nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_autosubscribe', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Record Subscription
     * 
     * @return {Select}
     */
    this.getRecordSubscriptionValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosubscribe_typ');
    };
    /**
     * Returns the text of select field Record Subscription
     * 
     * @return {string}
     */
    this.getRecordSubscriptionText = function() {
        return this.getFieldText('custrecord_suitesocial_autosubscribe_typ');
    };

    /**
     * Returns the Subscriber
     * 
     * @return {Select}
     */
    this.getSubscriberValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autosubscribe_sub');
    };
    /**
     * Returns the text of select field Subscriber
     * 
     * @return {string}
     */
    this.getSubscriberText = function() {
        return this.getFieldText('custrecord_suitesocial_autosubscribe_sub');
    };

    /**
     * Sets the Record Subscription
     * 
     * @param {Select}
     *        value Record Subscription
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setRecordSubscriptionValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_autosubscribe_typ', value);
        return this;
    };
    /**
     * Sets the text of select field Record Subscription
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setRecordSubscriptionText = function(text) {
        this.setFieldText('custrecord_suitesocial_autosubscribe_typ', text);
        return this;
    };

    /**
     * Sets the Subscriber
     * 
     * @param {Select}
     *        value Subscriber
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setSubscriberValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_autosubscribe_sub', value);
        return this;
    };
    /**
     * Sets the text of select field Subscriber
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setSubscriberText = function(text) {
        this.setFieldText('custrecord_suitesocial_autosubscribe_sub', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialAutoSubscribe}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_autosubscribe_typ : {
            "name" : "Record Subscription",
            "internalId" : "4700",
            "scriptId" : "custrecord_suitesocial_autosubscribe_typ",
            "dataType" : "select",
            "selectRecordTypeName" : "Record Subscription",
            "selectRecordTypeInternalId" : "689",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_autosub_type",
            "selectRecordTypeShortName" : "RecordSubscription"
        },
        custrecord_suitesocial_autosubscribe_sub : {
            "name" : "Subscriber",
            "internalId" : "4699",
            "scriptId" : "custrecord_suitesocial_autosubscribe_sub",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "683",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        }
    };
}

var R_SUITESOCIAL_AUTO_SUBSCRIBE = {
    IS_INACTIVE : 'isinactive',
    RECORD_SUBSCRIPTION : 'custrecord_suitesocial_autosubscribe_typ',
    SUBSCRIBER : 'custrecord_suitesocial_autosubscribe_sub'
};

/**
 * ORM for record type customrecord_suitesocial_colleague
 */
function SuiteSocialColleague() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_colleague';
    _record = nlapiCreateRecord('customrecord_suitesocial_colleague', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_colleague', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_colleague nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_colleague', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialColleague}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialColleague}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Colleague
     * 
     * @return {Select}
     */
    this.getColleagueValue = function() {
        return this.getFieldValue('custrecord_suitesocial_colleague_sub_pro');
    };
    /**
     * Returns the text of select field Colleague
     * 
     * @return {string}
     */
    this.getColleagueText = function() {
        return this.getFieldText('custrecord_suitesocial_colleague_sub_pro');
    };

    /**
     * Returns the Employee
     * 
     * @return {Select}
     */
    this.getEmployeeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_colleague_emp');
    };
    /**
     * Returns the text of select field Employee
     * 
     * @return {string}
     */
    this.getEmployeeText = function() {
        return this.getFieldText('custrecord_suitesocial_colleague_emp');
    };

    /**
     * Returns the Profile
     * 
     * @return {Select}
     */
    this.getProfileValue = function() {
        return this.getFieldValue('custrecord_suitesocial_colleague_profile');
    };
    /**
     * Returns the text of select field Profile
     * 
     * @return {string}
     */
    this.getProfileText = function() {
        return this.getFieldText('custrecord_suitesocial_colleague_profile');
    };

    /**
     * Returns the Subscriber
     * 
     * @return {Select}
     */
    this.getSubscriberValue = function() {
        return this.getFieldValue('custrecord_suitesocial_colleague_sub');
    };
    /**
     * Returns the text of select field Subscriber
     * 
     * @return {string}
     */
    this.getSubscriberText = function() {
        return this.getFieldText('custrecord_suitesocial_colleague_sub');
    };

    /**
     * Sets the Colleague
     * 
     * @param {Select}
     *        value Colleague
     * @return {SuiteSocialColleague}
     */
    this.setColleagueValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_colleague_sub_pro', value);
        return this;
    };
    /**
     * Sets the text of select field Colleague
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialColleague}
     */
    this.setColleagueText = function(text) {
        this.setFieldText('custrecord_suitesocial_colleague_sub_pro', text);
        return this;
    };

    /**
     * Sets the Profile
     * 
     * @param {Select}
     *        value Profile
     * @return {SuiteSocialColleague}
     */
    this.setProfileValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_colleague_profile', value);
        return this;
    };
    /**
     * Sets the text of select field Profile
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialColleague}
     */
    this.setProfileText = function(text) {
        this.setFieldText('custrecord_suitesocial_colleague_profile', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialColleague}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialColleague}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    /**
     * If Email Alerts is true
     * 
     * @return {booelan}
     */
    this.isEmailAlertsTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_colleague_alrt') == 'T';
    };
    /**
     * If Email Alerts is false
     * 
     * @return {booelan}
     */
    this.isEmailAlertsFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_colleague_alrt') != 'T';
    };
    /**
     * Sets Email Alerts to true
     * 
     * @return {SuiteSocialColleague}
     */
    this.setEmailAlertsToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_colleague_alrt', 'T');
        return this;
    };
    /**
     * Sets Email Alerts to false
     * 
     * @return {SuiteSocialColleague}
     */
    this.setEmailAlertsToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_colleague_alrt', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_colleague_sub_pro : {
            "name" : "Colleague",
            "internalId" : "4816",
            "scriptId" : "custrecord_suitesocial_colleague_sub_pro",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "683",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        },
        custrecord_suitesocial_colleague_alrt : {
            "name" : "Email Alerts",
            "internalId" : "4729",
            "scriptId" : "custrecord_suitesocial_colleague_alrt",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_colleague_emp : {
            "name" : "Employee",
            "internalId" : "4730",
            "scriptId" : "custrecord_suitesocial_colleague_emp",
            "dataType" : "select",
            "selectRecordTypeName" : "Employee",
            "selectRecordTypeInternalId" : "-4",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "employee",
            "selectRecordTypeShortName" : "Employee"
        },
        custrecord_suitesocial_colleague_profile : {
            "name" : "Profile",
            "internalId" : "4731",
            "scriptId" : "custrecord_suitesocial_colleague_profile",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "683",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        },
        custrecord_suitesocial_colleague_sub : {
            "name" : "Subscriber",
            "internalId" : "4732",
            "scriptId" : "custrecord_suitesocial_colleague_sub",
            "dataType" : "select",
            "selectRecordTypeName" : "Employee",
            "selectRecordTypeInternalId" : "-4",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Normal",
            "sourcefrom" : "Employee",
            "selectRecordTypeScriptId" : "employee",
            "selectRecordTypeShortName" : "Employee"
        }
    };
}

var R_SUITESOCIAL_COLLEAGUE = {
    IS_INACTIVE : 'isinactive',
    COLLEAGUE : 'custrecord_suitesocial_colleague_sub_pro',
    EMAIL_ALERTS : 'custrecord_suitesocial_colleague_alrt',
    EMPLOYEE : 'custrecord_suitesocial_colleague_emp',
    PROFILE : 'custrecord_suitesocial_colleague_profile',
    SUBSCRIBER : 'custrecord_suitesocial_colleague_sub'
};

/**
 * ORM for record type customrecord_suitesocial_digest_schedule
 */
function SuiteSocialDigestSchedule() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_digest_schedule';
    _record = nlapiCreateRecord('customrecord_suitesocial_digest_schedule', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_digest_schedule', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_digest_schedule
     * nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_digest_schedule', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialDigestSchedule}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialDigestSchedule}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Server Time Of Day
     * 
     * @return {Timeofday}
     */
    this.getServerTimeOfDayValue = function() {
        return this.getFieldValue('custrecord_suitesocial_digest_sched_stod');
    };

    /**
     * Returns the Time of Day
     * 
     * @return {Timeofday}
     */
    this.getTimeOfDayValue = function() {
        return this.getFieldValue('custrecord_suitesocial_digest_sched_tod');
    };

    /**
     * Sets the Server Time Of Day
     * 
     * @param {Timeofday}
     *        value Server Time Of Day
     * @return {SuiteSocialDigestSchedule}
     */
    this.setServerTimeOfDayValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_digest_sched_stod', value);
        return this;
    };

    /**
     * Sets the Time of Day
     * 
     * @param {Timeofday}
     *        value Time of Day
     * @return {SuiteSocialDigestSchedule}
     */
    this.setTimeOfDayValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_digest_sched_tod', value);
        return this;
    };

    /**
     * Returns the Profile
     * 
     * @return {Select}
     */
    this.getProfileValue = function() {
        return this.getFieldValue('custrecord_suitesocial_digest_sched_prof');
    };
    /**
     * Returns the text of select field Profile
     * 
     * @return {string}
     */
    this.getProfileText = function() {
        return this.getFieldText('custrecord_suitesocial_digest_sched_prof');
    };

    /**
     * Sets the Profile
     * 
     * @param {Select}
     *        value Profile
     * @return {SuiteSocialDigestSchedule}
     */
    this.setProfileValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_digest_sched_prof', value);
        return this;
    };
    /**
     * Sets the text of select field Profile
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialDigestSchedule}
     */
    this.setProfileText = function(text) {
        this.setFieldText('custrecord_suitesocial_digest_sched_prof', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialDigestSchedule}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialDigestSchedule}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_digest_sched_prof : {
            "name" : "Profile",
            "internalId" : "4808",
            "scriptId" : "custrecord_suitesocial_digest_sched_prof",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "683",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        },
        custrecord_suitesocial_digest_sched_stod : {
            "name" : "Server Time Of Day",
            "internalId" : "4806",
            "scriptId" : "custrecord_suitesocial_digest_sched_stod",
            "dataType" : "timeofday",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_digest_sched_tod : {
            "name" : "Time of Day",
            "internalId" : "4807",
            "scriptId" : "custrecord_suitesocial_digest_sched_tod",
            "dataType" : "timeofday",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_SUITESOCIAL_DIGEST_SCHEDULE = {
    IS_INACTIVE : 'isinactive',
    PROFILE : 'custrecord_suitesocial_digest_sched_prof',
    SERVER_TIME_OF_DAY : 'custrecord_suitesocial_digest_sched_stod',
    TIME_OF_DAY : 'custrecord_suitesocial_digest_sched_tod'
};

/**
 * ORM for record type customrecord_suitesocial_field
 */
function SuiteSocialField() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_field';
    _record = nlapiCreateRecord('customrecord_suitesocial_field', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_field', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_field nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_field', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialField}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialField}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Data Type Name
     * 
     * @return {String}
     */
    this.getDataTypeNameValue = function() {
        return this.getFieldValue('custrecord_ssf_data_type_name');
    };

    /**
     * Returns the Script Id
     * 
     * @return {String}
     */
    this.getScriptIdValue = function() {
        return this.getFieldValue('custrecord_ssf_script_id');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SuiteSocialField}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Record
     * 
     * @return {Select}
     */
    this.getRecordValue = function() {
        return this.getFieldValue('custrecord_ssf_record');
    };
    /**
     * Returns the text of select field Record
     * 
     * @return {string}
     */
    this.getRecordText = function() {
        return this.getFieldText('custrecord_ssf_record');
    };

    /**
     * Returns the Record Field
     * 
     * @return {Select}
     */
    this.getRecordFieldValue = function() {
        return this.getFieldValue('custrecord_ssf_record_field');
    };
    /**
     * Returns the text of select field Record Field
     * 
     * @return {string}
     */
    this.getRecordFieldText = function() {
        return this.getFieldText('custrecord_ssf_record_field');
    };

    /**
     * Returns the Select List or Record
     * 
     * @return {Select}
     */
    this.getSelectListOrRecordValue = function() {
        return this.getFieldValue('custrecord_ssf_select_list_or_record');
    };
    /**
     * Returns the text of select field Select List or Record
     * 
     * @return {string}
     */
    this.getSelectListOrRecordText = function() {
        return this.getFieldText('custrecord_ssf_select_list_or_record');
    };

    /**
     * Sets the Record
     * 
     * @param {Select}
     *        value Record
     * @return {SuiteSocialField}
     */
    this.setRecordValue = function(value) {
        this.setFieldValue('custrecord_ssf_record', value);
        return this;
    };
    /**
     * Sets the text of select field Record
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialField}
     */
    this.setRecordText = function(text) {
        this.setFieldText('custrecord_ssf_record', text);
        return this;
    };

    /**
     * Sets the Record Field
     * 
     * @param {Select}
     *        value Record Field
     * @return {SuiteSocialField}
     */
    this.setRecordFieldValue = function(value) {
        this.setFieldValue('custrecord_ssf_record_field', value);
        return this;
    };
    /**
     * Sets the text of select field Record Field
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialField}
     */
    this.setRecordFieldText = function(text) {
        this.setFieldText('custrecord_ssf_record_field', text);
        return this;
    };
    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialField}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialField}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ssf_data_type_name : {
            "name" : "Data Type Name",
            "internalId" : "4651",
            "scriptId" : "custrecord_ssf_data_type_name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Data Type (Name)"
        },
        custrecord_ssf_record : {
            "name" : "Record",
            "internalId" : "4650",
            "scriptId" : "custrecord_ssf_record",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_ssf_record_field : {
            "name" : "Record Field",
            "internalId" : "4649",
            "scriptId" : "custrecord_ssf_record_field",
            "dataType" : "select",
            "selectRecordTypeName" : "Field",
            "selectRecordTypeInternalId" : "-124",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_ssf_script_id : {
            "name" : "Script Id",
            "internalId" : "4648",
            "scriptId" : "custrecord_ssf_script_id",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Script ID"
        },
        custrecord_ssf_select_list_or_record : {
            "name" : "Select List or Record",
            "internalId" : "4647",
            "scriptId" : "custrecord_ssf_select_list_or_record",
            "dataType" : "select",
            "selectRecordTypeName" : "Record Type",
            "selectRecordTypeInternalId" : "-123",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "List/Record"
        }
    };
}

var R_SUITESOCIAL_FIELD = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    DATA_TYPE_NAME : 'custrecord_ssf_data_type_name',
    RECORD : 'custrecord_ssf_record',
    RECORD_FIELD : 'custrecord_ssf_record_field',
    SCRIPT_ID : 'custrecord_ssf_script_id',
    SELECT_LIST_OR_RECORD : 'custrecord_ssf_select_list_or_record'
};

/**
 * ORM for record type customrecord_ss_internal_setting
 */
function SuiteSocialInternalSetting() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ss_internal_setting';
    _record = nlapiCreateRecord('customrecord_ss_internal_setting', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ss_internal_setting', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ss_internal_setting nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ss_internal_setting', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialInternalSetting}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Comment
     * 
     * @return {String}
     */
    this.getCommentValue = function() {
        return this.getFieldValue('custrecord_ss_internal_setting_comment');
    };

    /**
     * Returns the Setting Value
     * 
     * @return {Clobtext}
     */
    this.getSettingValueValue = function() {
        return this.getFieldValue('custrecord_ss_internal_setting_value');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SuiteSocialInternalSetting}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Sets the Comment
     * 
     * @param {String}
     *        value Comment
     * @return {SuiteSocialInternalSetting}
     */
    this.setCommentValue = function(value) {
        this.setFieldValue('custrecord_ss_internal_setting_comment', value);
        return this;
    };

    /**
     * Sets the Setting Value
     * 
     * @param {Clobtext}
     *        value Setting Value
     * @return {SuiteSocialInternalSetting}
     */
    this.setSettingValueValue = function(value) {
        this.setFieldValue('custrecord_ss_internal_setting_value', value);
        return this;
    };
    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialInternalSetting}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialInternalSetting}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ss_internal_setting_comment : {
            "name" : "Comment",
            "internalId" : "4765",
            "scriptId" : "custrecord_ss_internal_setting_comment",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_ss_internal_setting_value : {
            "name" : "Setting Value",
            "internalId" : "4764",
            "scriptId" : "custrecord_ss_internal_setting_value",
            "dataType" : "clobtext",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_SUITESOCIAL_INTERNAL_SETTING = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    COMMENT : 'custrecord_ss_internal_setting_comment',
    SETTING_VALUE : 'custrecord_ss_internal_setting_value'
};

/**
 * ORM for record type customrecord_suitesocial_profile
 */
function SuiteSocialProfile() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_profile';
    _record = nlapiCreateRecord('customrecord_suitesocial_profile', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_profile', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_profile nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_profile', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialProfile}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialProfile}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Followed Records List
     * 
     * @return {Inlinehtml}
     */
    this.getFollowedRecordsListValue = function() {
        return this.getFieldValue('custrecord_followed_records');
    };

    /**
     * Returns the Image
     * 
     * @return {Image}
     */
    this.getImageValue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_image');
    };

    /**
     * Returns the Image Help
     * 
     * @return {Help}
     */
    this.getImageHelpValue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_imghelp');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SuiteSocialProfile}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Sets the Followed Records List
     * 
     * @param {Inlinehtml}
     *        value Followed Records List
     * @return {SuiteSocialProfile}
     */
    this.setFollowedRecordsListValue = function(value) {
        this.setFieldValue('custrecord_followed_records', value);
        return this;
    };

    /**
     * Sets the Image
     * 
     * @param {Image}
     *        value Image
     * @return {SuiteSocialProfile}
     */
    this.setImageValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_profile_image', value);
        return this;
    };

    /**
     * Sets the Image Help
     * 
     * @param {Help}
     *        value Image Help
     * @return {SuiteSocialProfile}
     */
    this.setImageHelpValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_profile_imghelp', value);
        return this;
    };

    /**
     * Returns the Employee
     * 
     * @return {Select}
     */
    this.getEmployeeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_emp');
    };
    /**
     * Returns the text of select field Employee
     * 
     * @return {string}
     */
    this.getEmployeeText = function() {
        return this.getFieldText('custrecord_suitesocial_profile_emp');
    };

    /**
     * Sets the Employee
     * 
     * @param {Select}
     *        value Employee
     * @return {SuiteSocialProfile}
     */
    this.setEmployeeValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_profile_emp', value);
        return this;
    };
    /**
     * Sets the text of select field Employee
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialProfile}
     */
    this.setEmployeeText = function(text) {
        this.setFieldText('custrecord_suitesocial_profile_emp', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    /**
     * If Alert on Comment is true
     * 
     * @return {booelan}
     */
    this.isAlertOnCommentTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_cmnt') == 'T';
    };
    /**
     * If Alert on Comment is false
     * 
     * @return {booelan}
     */
    this.isAlertOnCommentFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_cmnt') != 'T';
    };
    /**
     * Sets Alert on Comment to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnCommentToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_cmnt', 'T');
        return this;
    };
    /**
     * Sets Alert on Comment to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnCommentToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_cmnt', 'F');
        return this;
    };
    /**
     * If Alert on Private Message is true
     * 
     * @return {booelan}
     */
    this.isAlertOnDirectMessageTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_drct') == 'T';
    };
    /**
     * If Alert on Private Message is false
     * 
     * @return {booelan}
     */
    this.isAlertOnDirectMessageFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_drct') != 'T';
    };
    /**
     * Sets Alert on Private Message to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnDirectMessageToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_drct', 'T');
        return this;
    };
    /**
     * Sets Alert on Private Message to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnDirectMessageToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_drct', 'F');
        return this;
    };
    /**
     * If Alert on Status Change is true
     * 
     * @return {booelan}
     */
    this.isAlertOnStatusChangeTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_stat') == 'T';
    };
    /**
     * If Alert on Status Change is false
     * 
     * @return {booelan}
     */
    this.isAlertOnStatusChangeFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_stat') != 'T';
    };
    /**
     * Sets Alert on Status Change to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnStatusChangeToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_stat', 'T');
        return this;
    };
    /**
     * Sets Alert on Status Change to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnStatusChangeToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_stat', 'F');
        return this;
    };
    /**
     * If Alert on Subscribed Record is true
     * 
     * @return {booelan}
     */
    this.isAlertOnSubscribedRecordTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_rcrd') == 'T';
    };
    /**
     * If Alert on Subscribed Record is false
     * 
     * @return {booelan}
     */
    this.isAlertOnSubscribedRecordFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_alrt_rcrd') != 'T';
    };
    /**
     * Sets Alert on Subscribed Record to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnSubscribedRecordToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_rcrd', 'T');
        return this;
    };
    /**
     * Sets Alert on Subscribed Record to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setAlertOnSubscribedRecordToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_alrt_rcrd', 'F');
        return this;
    };
    /**
     * If Integrate Posts with Yammer Account is true
     * 
     * @return {booelan}
     */
    this.isIntegratePostsWithYammerAccountTrue = function() {
        return this.getFieldValue('custrecord_yammer_integration_enabled') == 'T';
    };
    /**
     * If Integrate Posts with Yammer Account is false
     * 
     * @return {booelan}
     */
    this.isIntegratePostsWithYammerAccountFalse = function() {
        return this.getFieldValue('custrecord_yammer_integration_enabled') != 'T';
    };
    /**
     * Sets Integrate Posts with Yammer Account to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setIntegratePostsWithYammerAccountToTrue = function() {
        this.setFieldValue('custrecord_yammer_integration_enabled', 'T');
        return this;
    };
    /**
     * Sets Integrate Posts with Yammer Account to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setIntegratePostsWithYammerAccountToFalse = function() {
        this.setFieldValue('custrecord_yammer_integration_enabled', 'F');
        return this;
    };
    /**
     * If Send Digest is true
     * 
     * @return {booelan}
     */
    this.isSendDigestTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_digest') == 'T';
    };
    /**
     * If Send Digest is false
     * 
     * @return {booelan}
     */
    this.isSendDigestFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_digest') != 'T';
    };
    /**
     * Sets Send Digest to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setSendDigestToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_digest', 'T');
        return this;
    };
    /**
     * Sets Send Digest to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setSendDigestToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_digest', 'F');
        return this;
    };
    /**
     * If Weekend Digests is true
     * 
     * @return {booelan}
     */
    this.isWeekendDigestsTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_wknd_dgst') == 'T';
    };
    /**
     * If Weekend Digests is false
     * 
     * @return {booelan}
     */
    this.isWeekendDigestsFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_wknd_dgst') != 'T';
    };
    /**
     * Sets Weekend Digests to true
     * 
     * @return {SuiteSocialProfile}
     */
    this.setWeekendDigestsToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_wknd_dgst', 'T');
        return this;
    };
    /**
     * Sets Weekend Digests to false
     * 
     * @return {SuiteSocialProfile}
     */
    this.setWeekendDigestsToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_wknd_dgst', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_profile_alrt_cmnt : {
            "name" : "Alert on Comment",
            "internalId" : "4786",
            "scriptId" : "custrecord_suitesocial_profile_alrt_cmnt",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_alrt_drct : {
            "name" : "Alert on Private Message",
            "internalId" : "4787",
            "scriptId" : "custrecord_suitesocial_profile_alrt_drct",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_alrt_stat : {
            "name" : "Alert on Status Change",
            "internalId" : "4788",
            "scriptId" : "custrecord_suitesocial_profile_alrt_stat",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_alrt_rcrd : {
            "name" : "Alert on Subscribed Record",
            "internalId" : "4789",
            "scriptId" : "custrecord_suitesocial_profile_alrt_rcrd",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_emp : {
            "name" : "Employee",
            "internalId" : "4790",
            "scriptId" : "custrecord_suitesocial_profile_emp",
            "dataType" : "select",
            "selectRecordTypeName" : "Employee",
            "selectRecordTypeInternalId" : "-4",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Inline Text",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "employee",
            "selectRecordTypeShortName" : "Employee"
        },
        custrecord_followed_records : {
            "name" : "Followed Records List",
            "internalId" : "4795",
            "scriptId" : "custrecord_followed_records",
            "dataType" : "inlinehtml",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Inline Text",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_image : {
            "name" : "Image",
            "internalId" : "4791",
            "scriptId" : "custrecord_suitesocial_profile_image",
            "dataType" : "image",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_imghelp : {
            "name" : "Image Help",
            "internalId" : "4792",
            "scriptId" : "custrecord_suitesocial_profile_imghelp",
            "dataType" : "help",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Inline Text",
            "sourcefrom" : ""
        },
        custrecord_yammer_integration_enabled : {
            "name" : "Integrate Posts with Yammer Account",
            "internalId" : "4785",
            "scriptId" : "custrecord_yammer_integration_enabled",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : "Integrate autoposts feeds in Yammer activity stream",
            "storevalue" : "T",
            "displaytype" : "Disabled",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_digest : {
            "name" : "Send Digest",
            "internalId" : "4793",
            "scriptId" : "custrecord_suitesocial_profile_digest",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_wknd_dgst : {
            "name" : "Weekend Digests",
            "internalId" : "4794",
            "scriptId" : "custrecord_suitesocial_profile_wknd_dgst",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_SUITESOCIAL_PROFILE = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    ALERT_ON_COMMENT : 'custrecord_suitesocial_profile_alrt_cmnt',
    ALERT_ON_DIRECT_MESSAGE : 'custrecord_suitesocial_profile_alrt_drct',
    ALERT_ON_STATUS_CHANGE : 'custrecord_suitesocial_profile_alrt_stat',
    ALERT_ON_SUBSCRIBED_RECORD : 'custrecord_suitesocial_profile_alrt_rcrd',
    EMPLOYEE : 'custrecord_suitesocial_profile_emp',
    FOLLOWED_RECORDS_LIST : 'custrecord_followed_records',
    IMAGE : 'custrecord_suitesocial_profile_image',
    IMAGE_HELP : 'custrecord_suitesocial_profile_imghelp',
    INTEGRATE_POSTS_WITH_YAMMER_ACCOUNT : 'custrecord_yammer_integration_enabled',
    SEND_DIGEST : 'custrecord_suitesocial_profile_digest',
    WEEKEND_DIGESTS : 'custrecord_suitesocial_profile_wknd_dgst'
};

/**
 * ORM for record type customrecord_suitesocial_profile_channel
 */
function SuiteSocialProfileChannel() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_profile_channel';
    _record = nlapiCreateRecord('customrecord_suitesocial_profile_channel', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_profile_channel', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_profile_channel
     * nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_profile_channel', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Channel
     * 
     * @return {Select}
     */
    this.getChannelValue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_ch_ch');
    };
    /**
     * Returns the text of select field Channel
     * 
     * @return {string}
     */
    this.getChannelText = function() {
        return this.getFieldText('custrecord_suitesocial_profile_ch_ch');
    };

    /**
     * Returns the Profile
     * 
     * @return {Select}
     */
    this.getProfileValue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_ch_prof');
    };
    /**
     * Returns the text of select field Profile
     * 
     * @return {string}
     */
    this.getProfileText = function() {
        return this.getFieldText('custrecord_suitesocial_profile_ch_prof');
    };

    /**
     * Returns the Subscriber
     * 
     * @return {Select}
     */
    this.getSubscriberValue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_ch_sub');
    };
    /**
     * Returns the text of select field Subscriber
     * 
     * @return {string}
     */
    this.getSubscriberText = function() {
        return this.getFieldText('custrecord_suitesocial_profile_ch_sub');
    };

    /**
     * Sets the Channel
     * 
     * @param {Select}
     *        value Channel
     * @return {SuiteSocialProfileChannel}
     */
    this.setChannelValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_profile_ch_ch', value);
        return this;
    };
    /**
     * Sets the text of select field Channel
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialProfileChannel}
     */
    this.setChannelText = function(text) {
        this.setFieldText('custrecord_suitesocial_profile_ch_ch', text);
        return this;
    };

    /**
     * Sets the Profile
     * 
     * @param {Select}
     *        value Profile
     * @return {SuiteSocialProfileChannel}
     */
    this.setProfileValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_profile_ch_prof', value);
        return this;
    };
    /**
     * Sets the text of select field Profile
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialProfileChannel}
     */
    this.setProfileText = function(text) {
        this.setFieldText('custrecord_suitesocial_profile_ch_prof', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    /**
     * If Email Alerts is true
     * 
     * @return {booelan}
     */
    this.isEmailAlertsTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_ch_alrt') == 'T';
    };
    /**
     * If Email Alerts is false
     * 
     * @return {booelan}
     */
    this.isEmailAlertsFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_ch_alrt') != 'T';
    };
    /**
     * Sets Email Alerts to true
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setEmailAlertsToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_ch_alrt', 'T');
        return this;
    };
    /**
     * Sets Email Alerts to false
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setEmailAlertsToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_ch_alrt', 'F');
        return this;
    };
    /**
     * If Email Digest is true
     * 
     * @return {booelan}
     */
    this.isEmailDigestTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_ch_dgst') == 'T';
    };
    /**
     * If Email Digest is false
     * 
     * @return {booelan}
     */
    this.isEmailDigestFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_profile_ch_dgst') != 'T';
    };
    /**
     * Sets Email Digest to true
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setEmailDigestToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_profile_ch_dgst', 'T');
        return this;
    };
    /**
     * Sets Email Digest to false
     * 
     * @return {SuiteSocialProfileChannel}
     */
    this.setEmailDigestToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_profile_ch_dgst', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_profile_ch_ch : {
            "name" : "Channel",
            "internalId" : "4724",
            "scriptId" : "custrecord_suitesocial_profile_ch_ch",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Channel",
            "selectRecordTypeInternalId" : "669",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_newsfeed_channel",
            "selectRecordTypeShortName" : "SuiteSocialChannel"
        },
        custrecord_suitesocial_profile_ch_alrt : {
            "name" : "Email Alerts",
            "internalId" : "4725",
            "scriptId" : "custrecord_suitesocial_profile_ch_alrt",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_ch_dgst : {
            "name" : "Email Digest",
            "internalId" : "4726",
            "scriptId" : "custrecord_suitesocial_profile_ch_dgst",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_profile_ch_prof : {
            "name" : "Profile",
            "internalId" : "4727",
            "scriptId" : "custrecord_suitesocial_profile_ch_prof",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Profile",
            "selectRecordTypeInternalId" : "683",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_profile",
            "selectRecordTypeShortName" : "SuiteSocialProfile"
        },
        custrecord_suitesocial_profile_ch_sub : {
            "name" : "Subscriber",
            "internalId" : "4728",
            "scriptId" : "custrecord_suitesocial_profile_ch_sub",
            "dataType" : "select",
            "selectRecordTypeName" : "Employee",
            "selectRecordTypeInternalId" : "-4",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Normal",
            "sourcefrom" : "Employee",
            "selectRecordTypeScriptId" : "employee",
            "selectRecordTypeShortName" : "Employee"
        }
    };
}

var R_SUITESOCIAL_PROFILE_CHANNEL = {
    IS_INACTIVE : 'isinactive',
    CHANNEL : 'custrecord_suitesocial_profile_ch_ch',
    EMAIL_ALERTS : 'custrecord_suitesocial_profile_ch_alrt',
    EMAIL_DIGEST : 'custrecord_suitesocial_profile_ch_dgst',
    PROFILE : 'custrecord_suitesocial_profile_ch_prof',
    SUBSCRIBER : 'custrecord_suitesocial_profile_ch_sub'
};

/**
 * ORM for record type customrecord_suitesocial_record
 */
function SuiteSocialRecord() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_record';
    _record = nlapiCreateRecord('customrecord_suitesocial_record', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_record', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_record nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_record', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialRecord}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialRecord}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Script ID
     * 
     * @return {String}
     */
    this.getScriptIDValue = function() {
        return this.getFieldValue('custrecord_suitesocial_record_scriptid');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SuiteSocialRecord}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Type
     * 
     * @return {Select}
     */
    this.getTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_record_type');
    };
    /**
     * Returns the text of select field Type
     * 
     * @return {string}
     */
    this.getTypeText = function() {
        return this.getFieldText('custrecord_suitesocial_record_type');
    };

    /**
     * Sets the Type
     * 
     * @param {Select}
     *        value Type
     * @return {SuiteSocialRecord}
     */
    this.setTypeValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_record_type', value);
        return this;
    };
    /**
     * Sets the text of select field Type
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialRecord}
     */
    this.setTypeText = function(text) {
        this.setFieldText('custrecord_suitesocial_record_type', text);
        return this;
    };
    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialRecord}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialRecord}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    /**
     * If Autopost on create is true
     * 
     * @return {booelan}
     */
    this.isAutopostOnCreateTrue = function() {
        return this.getFieldValue('custrecord_suitesocial_record_autopost') == 'T';
    };
    /**
     * If Autopost on create is false
     * 
     * @return {booelan}
     */
    this.isAutopostOnCreateFalse = function() {
        return this.getFieldValue('custrecord_suitesocial_record_autopost') != 'T';
    };
    /**
     * Sets Autopost on create to true
     * 
     * @return {SuiteSocialRecord}
     */
    this.setAutopostOnCreateToTrue = function() {
        this.setFieldValue('custrecord_suitesocial_record_autopost', 'T');
        return this;
    };
    /**
     * Sets Autopost on create to false
     * 
     * @return {SuiteSocialRecord}
     */
    this.setAutopostOnCreateToFalse = function() {
        this.setFieldValue('custrecord_suitesocial_record_autopost', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_record_autopost : {
            "name" : "Autopost on create",
            "internalId" : "4799",
            "scriptId" : "custrecord_suitesocial_record_autopost",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_record_scriptid : {
            "name" : "Script ID",
            "internalId" : "4800",
            "scriptId" : "custrecord_suitesocial_record_scriptid",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Hidden",
            "sourcefrom" : "Script ID"
        },
        custrecord_suitesocial_record_type : {
            "name" : "Type",
            "internalId" : "4801",
            "scriptId" : "custrecord_suitesocial_record_type",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_SUITESOCIAL_RECORD = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    AUTOPOST_ON_CREATE : 'custrecord_suitesocial_record_autopost',
    SCRIPT_ID : 'custrecord_suitesocial_record_scriptid',
    TYPE : 'custrecord_suitesocial_record_type'
};

/**
 * ORM for record type customrecord_ssr_ss_related_record
 */
function SuiteSocialRelatedRecord() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_ssr_ss_related_record';
    _record = nlapiCreateRecord('customrecord_ssr_ss_related_record', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_ssr_ss_related_record', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_ssr_ss_related_record nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_ssr_ss_related_record', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialRelatedRecord}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialRelatedRecord}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SuiteSocialRelatedRecord}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Related SuiteSocial Record
     * 
     * @return {Select}
     */
    this.getRelatedSuiteSocialRecordValue = function() {
        return this.getFieldValue('custrecord_ssrr_related_ss_record');
    };
    /**
     * Returns the text of select field Related SuiteSocial Record
     * 
     * @return {string}
     */
    this.getRelatedSuiteSocialRecordText = function() {
        return this.getFieldText('custrecord_ssrr_related_ss_record');
    };

    /**
     * Returns the Related SuiteSocial Record Type
     * 
     * @return {Select}
     */
    this.getRelatedSuiteSocialRecordTypeValue = function() {
        return this.getFieldValue('custrecord_ssrr_related_ss_record_type');
    };
    /**
     * Returns the text of select field Related SuiteSocial Record Type
     * 
     * @return {string}
     */
    this.getRelatedSuiteSocialRecordTypeText = function() {
        return this.getFieldText('custrecord_ssrr_related_ss_record_type');
    };

    /**
     * Returns the SuiteSocial Record
     * 
     * @return {Select}
     */
    this.getSuiteSocialRecordValue = function() {
        return this.getFieldValue('custrecord_ssrr_ss_record');
    };
    /**
     * Returns the text of select field SuiteSocial Record
     * 
     * @return {string}
     */
    this.getSuiteSocialRecordText = function() {
        return this.getFieldText('custrecord_ssrr_ss_record');
    };

    /**
     * Returns the SuiteSocial Record Type
     * 
     * @return {Select}
     */
    this.getSuiteSocialRecordTypeValue = function() {
        return this.getFieldValue('custrecord_ssrr_record_type');
    };
    /**
     * Returns the text of select field SuiteSocial Record Type
     * 
     * @return {string}
     */
    this.getSuiteSocialRecordTypeText = function() {
        return this.getFieldText('custrecord_ssrr_record_type');
    };

    /**
     * Sets the Related SuiteSocial Record
     * 
     * @param {Select}
     *        value Related SuiteSocial Record
     * @return {SuiteSocialRelatedRecord}
     */
    this.setRelatedSuiteSocialRecordValue = function(value) {
        this.setFieldValue('custrecord_ssrr_related_ss_record', value);
        return this;
    };
    /**
     * Sets the text of select field Related SuiteSocial Record
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialRelatedRecord}
     */
    this.setRelatedSuiteSocialRecordText = function(text) {
        this.setFieldText('custrecord_ssrr_related_ss_record', text);
        return this;
    };

    /**
     * Sets the SuiteSocial Record
     * 
     * @param {Select}
     *        value SuiteSocial Record
     * @return {SuiteSocialRelatedRecord}
     */
    this.setSuiteSocialRecordValue = function(value) {
        this.setFieldValue('custrecord_ssrr_ss_record', value);
        return this;
    };
    /**
     * Sets the text of select field SuiteSocial Record
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialRelatedRecord}
     */
    this.setSuiteSocialRecordText = function(text) {
        this.setFieldText('custrecord_ssrr_ss_record', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialRelatedRecord}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialRelatedRecord}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ssrr_related_ss_record : {
            "name" : "Related SuiteSocial Record",
            "internalId" : "4675",
            "scriptId" : "custrecord_ssrr_related_ss_record",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "686",
            "ismandatory" : "T",
            "description" : "Related record is the parent record",
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        },
        custrecord_ssrr_related_ss_record_type : {
            "name" : "Related SuiteSocial Record Type",
            "internalId" : "4676",
            "scriptId" : "custrecord_ssrr_related_ss_record_type",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Type"
        },
        custrecord_ssrr_ss_record : {
            "name" : "SuiteSocial Record",
            "internalId" : "4677",
            "scriptId" : "custrecord_ssrr_ss_record",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "686",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        },
        custrecord_ssrr_record_type : {
            "name" : "SuiteSocial Record Type",
            "internalId" : "4674",
            "scriptId" : "custrecord_ssrr_record_type",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Type"
        }
    };
}

var R_SUITESOCIAL_RELATED_RECORD = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    RELATED_SUITESOCIAL_RECORD : 'custrecord_ssrr_related_ss_record',
    RELATED_SUITESOCIAL_RECORD_TYPE : 'custrecord_ssrr_related_ss_record_type',
    SUITESOCIAL_RECORD : 'custrecord_ssrr_ss_record',
    SUITESOCIAL_RECORD_TYPE : 'custrecord_ssrr_record_type'
};

/**
 * ORM for record type customrecord_suitesocial_role
 */
function SuiteSocialRole() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_role';
    _record = nlapiCreateRecord('customrecord_suitesocial_role', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_role', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_role nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_role', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialRole}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialRole}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SuiteSocialRole}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Returns the Role Field
     * 
     * @return {Select}
     */
    this.getRoleFieldValue = function() {
        return this.getFieldValue('custrecord_suitesocial_role_field');
    };
    /**
     * Returns the text of select field Role Field
     * 
     * @return {string}
     */
    this.getRoleFieldText = function() {
        return this.getFieldText('custrecord_suitesocial_role_field');
    };

    /**
     * Sets the Role Field
     * 
     * @param {Select}
     *        value Role Field
     * @return {SuiteSocialRole}
     */
    this.setRoleFieldValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_role_field', value);
        return this;
    };
    /**
     * Sets the text of select field Role Field
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialRole}
     */
    this.setRoleFieldText = function(text) {
        this.setFieldText('custrecord_suitesocial_role_field', text);
        return this;
    };
    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialRole}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialRole}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_role_field : {
            "name" : "Role Field",
            "internalId" : "4669",
            "scriptId" : "custrecord_suitesocial_role_field",
            "dataType" : "select",
            "selectRecordTypeName" : "Role",
            "selectRecordTypeInternalId" : "-118",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_SUITESOCIAL_ROLE = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    ROLE_FIELD : 'custrecord_suitesocial_role_field'
};

/**
 * ORM for record type customrecord_suitesocial_scripted_record
 */
function SuiteSocialScriptedRecord() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_scripted_record';
    _record = nlapiCreateRecord('customrecord_suitesocial_scripted_record', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_scripted_record', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_scripted_record
     * nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_scripted_record', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialScriptedRecord}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialScriptedRecord}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Name
     * 
     * @return {String}
     */
    this.getNameValue = function() {
        return this.getFieldValue('name');
    };

    /**
     * Returns the Field Script Id List
     * 
     * @return {Clobtext}
     */
    this.getFieldScriptIdListValue = function() {
        return this.getFieldValue('custrecord_ssr_field_script_id_list');
    };

    /**
     * Returns the Script Id
     * 
     * @return {String}
     */
    this.getScriptIdValue = function() {
        return this.getFieldValue('custrecord_ssr_script_id');
    };

    /**
     * Sets the Name
     * 
     * @param {String}
     *        value Name
     * @return {SuiteSocialScriptedRecord}
     */
    this.setNameValue = function(value) {
        this.setFieldValue('name', value);
        return this;
    };

    /**
     * Sets the Field Script Id List
     * 
     * @param {Clobtext}
     *        value Field Script Id List
     * @return {SuiteSocialScriptedRecord}
     */
    this.setFieldScriptIdListValue = function(value) {
        this.setFieldValue('custrecord_ssr_field_script_id_list', value);
        return this;
    };

    /**
     * Returns the Scripted Record
     * 
     * @return {Select}
     */
    this.getScriptedRecordValue = function() {
        return this.getFieldValue('custrecord_ssr_scripted_record');
    };
    /**
     * Returns the text of select field Scripted Record
     * 
     * @return {string}
     */
    this.getScriptedRecordText = function() {
        return this.getFieldText('custrecord_ssr_scripted_record');
    };

    /**
     * Sets the Scripted Record
     * 
     * @param {Select}
     *        value Scripted Record
     * @return {SuiteSocialScriptedRecord}
     */
    this.setScriptedRecordValue = function(value) {
        this.setFieldValue('custrecord_ssr_scripted_record', value);
        return this;
    };
    /**
     * Sets the text of select field Scripted Record
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialScriptedRecord}
     */
    this.setScriptedRecordText = function(text) {
        this.setFieldText('custrecord_ssr_scripted_record', text);
        return this;
    };
    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialScriptedRecord}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialScriptedRecord}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        name : {
            "name" : "Name",
            "internalId" : 0,
            "scriptId" : "name",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_ssr_field_script_id_list : {
            "name" : "Field Script Id List",
            "internalId" : "4672",
            "scriptId" : "custrecord_ssr_field_script_id_list",
            "dataType" : "clobtext",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : "Comma-separated list of script id of the scripted record",
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_ssr_script_id : {
            "name" : "Script Id",
            "internalId" : "4671",
            "scriptId" : "custrecord_ssr_script_id",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Script ID"
        },
        custrecord_ssr_scripted_record : {
            "name" : "Scripted Record",
            "internalId" : "4673",
            "scriptId" : "custrecord_ssr_scripted_record",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        }
    };
}

var R_SUITESOCIAL_SCRIPTED_RECORD = {
    NAME : 'name',
    IS_INACTIVE : 'isinactive',
    FIELD_SCRIPT_ID_LIST : 'custrecord_ssr_field_script_id_list',
    SCRIPT_ID : 'custrecord_ssr_script_id',
    SCRIPTED_RECORD : 'custrecord_ssr_scripted_record'
};

/**
 * ORM for record type customrecord_suitesocial_subscription
 */
function SuiteSocialSubscription() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_subscription';
    _record = nlapiCreateRecord('customrecord_suitesocial_subscription', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_subscription', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_subscription nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_subscription', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {SuiteSocialSubscription}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {SuiteSocialSubscription}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Custom Record
     * 
     * @return {Integer}
     */
    this.getCustomRecordValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_customrecord');
    };

    /**
     * Returns the Custom Type
     * 
     * @return {String}
     */
    this.getCustomTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_customtype');
    };

    /**
     * Sets the Custom Record
     * 
     * @param {Integer}
     *        value Custom Record
     * @return {SuiteSocialSubscription}
     */
    this.setCustomRecordValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_customrecord', value);
        return this;
    };

    /**
     * Sets the Custom Type
     * 
     * @param {String}
     *        value Custom Type
     * @return {SuiteSocialSubscription}
     */
    this.setCustomTypeValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_customtype', value);
        return this;
    };

    /**
     * Returns the Call
     * 
     * @return {Select}
     */
    this.getCallValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_phonecall');
    };
    /**
     * Returns the text of select field Call
     * 
     * @return {string}
     */
    this.getCallText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_phonecall');
    };

    /**
     * Returns the Campaign
     * 
     * @return {Select}
     */
    this.getCampaignValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_campaign');
    };
    /**
     * Returns the text of select field Campaign
     * 
     * @return {string}
     */
    this.getCampaignText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_campaign');
    };

    /**
     * Returns the Case
     * 
     * @return {Select}
     */
    this.getCaseValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_supportcase');
    };
    /**
     * Returns the text of select field Case
     * 
     * @return {string}
     */
    this.getCaseText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_supportcase');
    };

    /**
     * Returns the Contact
     * 
     * @return {Select}
     */
    this.getContactValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_contact');
    };
    /**
     * Returns the text of select field Contact
     * 
     * @return {string}
     */
    this.getContactText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_contact');
    };

    /**
     * Returns the Customer
     * 
     * @return {Select}
     */
    this.getCustomerValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_customer');
    };
    /**
     * Returns the text of select field Customer
     * 
     * @return {string}
     */
    this.getCustomerText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_customer');
    };

    /**
     * Returns the Employee
     * 
     * @return {Select}
     */
    this.getEmployeeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_employee');
    };
    /**
     * Returns the text of select field Employee
     * 
     * @return {string}
     */
    this.getEmployeeText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_employee');
    };

    /**
     * Returns the Event
     * 
     * @return {Select}
     */
    this.getEventValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_calendarevent');
    };
    /**
     * Returns the text of select field Event
     * 
     * @return {string}
     */
    this.getEventText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_calendarevent');
    };

    /**
     * Returns the Issue
     * 
     * @return {Select}
     */
    this.getIssueValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_issue');
    };
    /**
     * Returns the text of select field Issue
     * 
     * @return {string}
     */
    this.getIssueText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_issue');
    };

    /**
     * Returns the Opportunity
     * 
     * @return {Select}
     */
    this.getOpportunityValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_opportunity');
    };
    /**
     * Returns the text of select field Opportunity
     * 
     * @return {string}
     */
    this.getOpportunityText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_opportunity');
    };

    /**
     * Returns the Partner
     * 
     * @return {Select}
     */
    this.getPartnerValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_partner');
    };
    /**
     * Returns the text of select field Partner
     * 
     * @return {string}
     */
    this.getPartnerText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_partner');
    };

    /**
     * Returns the Project
     * 
     * @return {Select}
     */
    this.getProjectValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_job');
    };
    /**
     * Returns the text of select field Project
     * 
     * @return {string}
     */
    this.getProjectText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_job');
    };

    /**
     * Returns the Subscriber
     * 
     * @return {Select}
     */
    this.getSubscriberValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_subscriber');
    };
    /**
     * Returns the text of select field Subscriber
     * 
     * @return {string}
     */
    this.getSubscriberText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_subscriber');
    };

    /**
     * Returns the Task
     * 
     * @return {Select}
     */
    this.getTaskValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_task');
    };
    /**
     * Returns the text of select field Task
     * 
     * @return {string}
     */
    this.getTaskText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_task');
    };

    /**
     * Returns the Transaction
     * 
     * @return {Select}
     */
    this.getTransactionValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_transaction');
    };
    /**
     * Returns the text of select field Transaction
     * 
     * @return {string}
     */
    this.getTransactionText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_transaction');
    };

    /**
     * Returns the Vendor
     * 
     * @return {Select}
     */
    this.getVendorValue = function() {
        return this.getFieldValue('custrecord_suitesocial_sub_vendor');
    };
    /**
     * Returns the text of select field Vendor
     * 
     * @return {string}
     */
    this.getVendorText = function() {
        return this.getFieldText('custrecord_suitesocial_sub_vendor');
    };

    /**
     * Sets the Call
     * 
     * @param {Select}
     *        value Call
     * @return {SuiteSocialSubscription}
     */
    this.setCallValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_phonecall', value);
        return this;
    };
    /**
     * Sets the text of select field Call
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setCallText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_phonecall', text);
        return this;
    };

    /**
     * Sets the Campaign
     * 
     * @param {Select}
     *        value Campaign
     * @return {SuiteSocialSubscription}
     */
    this.setCampaignValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_campaign', value);
        return this;
    };
    /**
     * Sets the text of select field Campaign
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setCampaignText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_campaign', text);
        return this;
    };

    /**
     * Sets the Case
     * 
     * @param {Select}
     *        value Case
     * @return {SuiteSocialSubscription}
     */
    this.setCaseValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_supportcase', value);
        return this;
    };
    /**
     * Sets the text of select field Case
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setCaseText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_supportcase', text);
        return this;
    };

    /**
     * Sets the Contact
     * 
     * @param {Select}
     *        value Contact
     * @return {SuiteSocialSubscription}
     */
    this.setContactValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_contact', value);
        return this;
    };
    /**
     * Sets the text of select field Contact
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setContactText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_contact', text);
        return this;
    };

    /**
     * Sets the Customer
     * 
     * @param {Select}
     *        value Customer
     * @return {SuiteSocialSubscription}
     */
    this.setCustomerValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_customer', value);
        return this;
    };
    /**
     * Sets the text of select field Customer
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setCustomerText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_customer', text);
        return this;
    };

    /**
     * Sets the Employee
     * 
     * @param {Select}
     *        value Employee
     * @return {SuiteSocialSubscription}
     */
    this.setEmployeeValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_employee', value);
        return this;
    };
    /**
     * Sets the text of select field Employee
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setEmployeeText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_employee', text);
        return this;
    };

    /**
     * Sets the Event
     * 
     * @param {Select}
     *        value Event
     * @return {SuiteSocialSubscription}
     */
    this.setEventValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_calendarevent', value);
        return this;
    };
    /**
     * Sets the text of select field Event
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setEventText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_calendarevent', text);
        return this;
    };

    /**
     * Sets the Issue
     * 
     * @param {Select}
     *        value Issue
     * @return {SuiteSocialSubscription}
     */
    this.setIssueValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_issue', value);
        return this;
    };
    /**
     * Sets the text of select field Issue
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setIssueText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_issue', text);
        return this;
    };

    /**
     * Sets the Opportunity
     * 
     * @param {Select}
     *        value Opportunity
     * @return {SuiteSocialSubscription}
     */
    this.setOpportunityValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_opportunity', value);
        return this;
    };
    /**
     * Sets the text of select field Opportunity
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setOpportunityText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_opportunity', text);
        return this;
    };

    /**
     * Sets the Partner
     * 
     * @param {Select}
     *        value Partner
     * @return {SuiteSocialSubscription}
     */
    this.setPartnerValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_partner', value);
        return this;
    };
    /**
     * Sets the text of select field Partner
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setPartnerText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_partner', text);
        return this;
    };

    /**
     * Sets the Project
     * 
     * @param {Select}
     *        value Project
     * @return {SuiteSocialSubscription}
     */
    this.setProjectValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_job', value);
        return this;
    };
    /**
     * Sets the text of select field Project
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setProjectText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_job', text);
        return this;
    };

    /**
     * Sets the Subscriber
     * 
     * @param {Select}
     *        value Subscriber
     * @return {SuiteSocialSubscription}
     */
    this.setSubscriberValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_subscriber', value);
        return this;
    };
    /**
     * Sets the text of select field Subscriber
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setSubscriberText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_subscriber', text);
        return this;
    };

    /**
     * Sets the Task
     * 
     * @param {Select}
     *        value Task
     * @return {SuiteSocialSubscription}
     */
    this.setTaskValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_task', value);
        return this;
    };
    /**
     * Sets the text of select field Task
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setTaskText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_task', text);
        return this;
    };

    /**
     * Sets the Transaction
     * 
     * @param {Select}
     *        value Transaction
     * @return {SuiteSocialSubscription}
     */
    this.setTransactionValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_transaction', value);
        return this;
    };
    /**
     * Sets the text of select field Transaction
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setTransactionText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_transaction', text);
        return this;
    };

    /**
     * Sets the Vendor
     * 
     * @param {Select}
     *        value Vendor
     * @return {SuiteSocialSubscription}
     */
    this.setVendorValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_sub_vendor', value);
        return this;
    };
    /**
     * Sets the text of select field Vendor
     * 
     * @param {string}
     *        text
     * @return {SuiteSocialSubscription}
     */
    this.setVendorText = function(text) {
        this.setFieldText('custrecord_suitesocial_sub_vendor', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {SuiteSocialSubscription}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {SuiteSocialSubscription}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_sub_phonecall : {
            "name" : "Call",
            "internalId" : "4782",
            "scriptId" : "custrecord_suitesocial_sub_phonecall",
            "dataType" : "select",
            "selectRecordTypeName" : "Call",
            "selectRecordTypeInternalId" : "-22",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_sub_campaign : {
            "name" : "Campaign",
            "internalId" : "4784",
            "scriptId" : "custrecord_suitesocial_sub_campaign",
            "dataType" : "select",
            "selectRecordTypeName" : "Campaign",
            "selectRecordTypeInternalId" : "-24",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "campaign",
            "selectRecordTypeShortName" : "Campaign"
        },
        custrecord_suitesocial_sub_supportcase : {
            "name" : "Case",
            "internalId" : "4773",
            "scriptId" : "custrecord_suitesocial_sub_supportcase",
            "dataType" : "select",
            "selectRecordTypeName" : "Case",
            "selectRecordTypeInternalId" : "-23",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "supportcase",
            "selectRecordTypeShortName" : "Case"
        },
        custrecord_suitesocial_sub_contact : {
            "name" : "Contact",
            "internalId" : "4768",
            "scriptId" : "custrecord_suitesocial_sub_contact",
            "dataType" : "select",
            "selectRecordTypeName" : "Contact",
            "selectRecordTypeInternalId" : "-6",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "contact",
            "selectRecordTypeShortName" : "Contact"
        },
        custrecord_suitesocial_sub_customrecord : {
            "name" : "Custom Record",
            "internalId" : "4774",
            "scriptId" : "custrecord_suitesocial_sub_customrecord",
            "dataType" : "integer",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_sub_customtype : {
            "name" : "Custom Type",
            "internalId" : "4775",
            "scriptId" : "custrecord_suitesocial_sub_customtype",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_sub_customer : {
            "name" : "Customer",
            "internalId" : "4776",
            "scriptId" : "custrecord_suitesocial_sub_customer",
            "dataType" : "select",
            "selectRecordTypeName" : "Customer",
            "selectRecordTypeInternalId" : "-2",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customer",
            "selectRecordTypeShortName" : "Customer"
        },
        custrecord_suitesocial_sub_employee : {
            "name" : "Employee",
            "internalId" : "4769",
            "scriptId" : "custrecord_suitesocial_sub_employee",
            "dataType" : "select",
            "selectRecordTypeName" : "Employee",
            "selectRecordTypeInternalId" : "-4",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "employee",
            "selectRecordTypeShortName" : "Employee"
        },
        custrecord_suitesocial_sub_calendarevent : {
            "name" : "Event",
            "internalId" : "4781",
            "scriptId" : "custrecord_suitesocial_sub_calendarevent",
            "dataType" : "select",
            "selectRecordTypeName" : "Event",
            "selectRecordTypeInternalId" : "-20",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "calendarevent",
            "selectRecordTypeShortName" : "Event"
        },
        custrecord_suitesocial_sub_issue : {
            "name" : "Issue",
            "internalId" : "4777",
            "scriptId" : "custrecord_suitesocial_sub_issue",
            "dataType" : "select",
            "selectRecordTypeName" : "Issue",
            "selectRecordTypeInternalId" : "-26",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "issue",
            "selectRecordTypeShortName" : "Issue"
        },
        custrecord_suitesocial_sub_opportunity : {
            "name" : "Opportunity",
            "internalId" : "4778",
            "scriptId" : "custrecord_suitesocial_sub_opportunity",
            "dataType" : "select",
            "selectRecordTypeName" : "Opportunity",
            "selectRecordTypeInternalId" : "-31",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "opportunity",
            "selectRecordTypeShortName" : "Opportunity"
        },
        custrecord_suitesocial_sub_partner : {
            "name" : "Partner",
            "internalId" : "4779",
            "scriptId" : "custrecord_suitesocial_sub_partner",
            "dataType" : "select",
            "selectRecordTypeName" : "Partner",
            "selectRecordTypeInternalId" : "-5",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "partner",
            "selectRecordTypeShortName" : "Partner"
        },
        custrecord_suitesocial_sub_job : {
            "name" : "Project",
            "internalId" : "4780",
            "scriptId" : "custrecord_suitesocial_sub_job",
            "dataType" : "select",
            "selectRecordTypeName" : "Project",
            "selectRecordTypeInternalId" : "-7",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "job",
            "selectRecordTypeShortName" : "Project"
        },
        custrecord_suitesocial_sub_subscriber : {
            "name" : "Subscriber",
            "internalId" : "4770",
            "scriptId" : "custrecord_suitesocial_sub_subscriber",
            "dataType" : "select",
            "selectRecordTypeName" : "Employee",
            "selectRecordTypeInternalId" : "-4",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "employee",
            "selectRecordTypeShortName" : "Employee"
        },
        custrecord_suitesocial_sub_task : {
            "name" : "Task",
            "internalId" : "4783",
            "scriptId" : "custrecord_suitesocial_sub_task",
            "dataType" : "select",
            "selectRecordTypeName" : "Task",
            "selectRecordTypeInternalId" : "-21",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "task",
            "selectRecordTypeShortName" : "Task"
        },
        custrecord_suitesocial_sub_transaction : {
            "name" : "Transaction",
            "internalId" : "4771",
            "scriptId" : "custrecord_suitesocial_sub_transaction",
            "dataType" : "select",
            "selectRecordTypeName" : "Transaction",
            "selectRecordTypeInternalId" : "-30",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : ""
        },
        custrecord_suitesocial_sub_vendor : {
            "name" : "Vendor",
            "internalId" : "4772",
            "scriptId" : "custrecord_suitesocial_sub_vendor",
            "dataType" : "select",
            "selectRecordTypeName" : "Vendor",
            "selectRecordTypeInternalId" : "-3",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "vendor",
            "selectRecordTypeShortName" : "Vendor"
        }
    };
}

var R_SUITESOCIAL_SUBSCRIPTION = {
    IS_INACTIVE : 'isinactive',
    CALL : 'custrecord_suitesocial_sub_phonecall',
    CAMPAIGN : 'custrecord_suitesocial_sub_campaign',
    CASE : 'custrecord_suitesocial_sub_supportcase',
    CONTACT : 'custrecord_suitesocial_sub_contact',
    CUSTOM_RECORD : 'custrecord_suitesocial_sub_customrecord',
    CUSTOM_TYPE : 'custrecord_suitesocial_sub_customtype',
    CUSTOMER : 'custrecord_suitesocial_sub_customer',
    EMPLOYEE : 'custrecord_suitesocial_sub_employee',
    EVENT : 'custrecord_suitesocial_sub_calendarevent',
    ISSUE : 'custrecord_suitesocial_sub_issue',
    OPPORTUNITY : 'custrecord_suitesocial_sub_opportunity',
    PARTNER : 'custrecord_suitesocial_sub_partner',
    PROJECT : 'custrecord_suitesocial_sub_job',
    SUBSCRIBER : 'custrecord_suitesocial_sub_subscriber',
    TASK : 'custrecord_suitesocial_sub_task',
    TRANSACTION : 'custrecord_suitesocial_sub_transaction',
    VENDOR : 'custrecord_suitesocial_sub_vendor'
};

/**
 * ORM for record type customrecord_suitesocial_autopost_field
 */
function TrackedField() {
    // record
    var _record;
    // long record name or actual record name
    var _longRecordId = 'customrecord_suitesocial_autopost_field';
    _record = nlapiCreateRecord('customrecord_suitesocial_autopost_field', {
        recordmode : 'dynamic'
    });
    /**
     * Returns the script id of this record type
     * 
     * @return {string}
     */
    this.getScriptId = function() {
        return _longRecordId;
    };
    /**
     * Returns the id of this record
     * 
     * @return {integer}
     */
    this.getId = function() {
        return _record.getId();
    };
    this.createRecord = function() {
        _record = nlapiCreateRecord('customrecord_suitesocial_autopost_field', {
            recordmode : 'dynamic'
        });
        return _record;
    };
    /**
     * Returns the underlying nlobjRecord
     * 
     * @return {nlobjRecord}
     */
    this.getRecord = function() {
        return _record;
    };
    /**
     * Loads an existing record
     * 
     * @param {string}
     *        id The internal id of the record to be loaded
     * @return {nlobjRecord}
     */
    this.loadRecord = function(id) {
        _record = nlapiLoadRecord(_longRecordId, id);
        return _record;
    };

    /**
     * Sets a previously loadedcustomrecord_suitesocial_autopost_field
     * nlobjRecord
     * 
     * @param {nlobjRecord}
     *        record The underlying nlobjRecord
     */
    this.setRecord = function(record) {
        if (record.getRecordType() != _longRecordId) {
            throw 'Record type mismatch. ' + record.getRecordType() + ' passed but ' + _longRecordId + ' is expected.';
        }
        _record = record;
    };

    /**
     * Submits the underlying nlobjRecord and returns the internal id of the
     * record
     * 
     * @return {string}
     */
    this.submitRecord = function() {

        return nlapiSubmitRecord(_record);
    };
    /**
     * Deletes the underlying nlobjRecord
     */
    this.deleteRecord = function() {
        // there should be a nlobjrecord

        nlapiDeleteRecord('customrecord_suitesocial_autopost_field', _record.getId());
        // TODO: delete also children
        _record = null;
    };
    /**
     * Works like nlobjRecord.getFieldValue()
     */
    this.getFieldValue = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldValue(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldValue()
     * 
     * @return {TrackedField}
     */
    this.setFieldValue = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldValue(fieldId, value);
        return this;
    };
    /**
     * Works like nlobjRecord.getFieldText()
     */
    this.getFieldText = function(fieldId) {
        // there should be a nlobjrecord

        return _record.getFieldText(fieldId);
    };
    /**
     * Works like nlobjRecord.setFieldText()
     * 
     * @return {TrackedField}
     */
    this.setFieldText = function(fieldId, value) {
        // there should be a nlobjrecord

        _record.setFieldText(fieldId, value);
        return this;
    };

    // start of record type specific getter and setter methods

    /**
     * Returns the Field Data Type
     * 
     * @return {String}
     */
    this.getFieldDataTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autopost_fld_dt');
    };

    /**
     * Returns the Field ID
     * 
     * @return {String}
     */
    this.getFieldIDValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autopost_fld_id');
    };

    /**
     * Returns the Field Type
     * 
     * @return {String}
     */
    this.getFieldTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autopost_fld_ft');
    };

    /**
     * Returns the Field
     * 
     * @return {Select}
     */
    this.getFieldFieldValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autopost_fld_rf');
    };
    /**
     * Returns the text of select field Field
     * 
     * @return {string}
     */
    this.getFieldFieldText = function() {
        return this.getFieldText('custrecord_suitesocial_autopost_fld_rf');
    };

    /**
     * Returns the Field (hidden)
     * 
     * @return {Select}
     */
    this.getFieldHiddenValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autopost_fld_fld');
    };
    /**
     * Returns the text of select field Field (hidden)
     * 
     * @return {string}
     */
    this.getFieldHiddenText = function() {
        return this.getFieldText('custrecord_suitesocial_autopost_fld_fld');
    };

    /**
     * Returns the Internal Record Type
     * 
     * @return {Select}
     */
    this.getInternalRecordTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autopost_fld_rt');
    };
    /**
     * Returns the text of select field Internal Record Type
     * 
     * @return {string}
     */
    this.getInternalRecordTypeText = function() {
        return this.getFieldText('custrecord_suitesocial_autopost_fld_rt');
    };

    /**
     * Returns the Record Type
     * 
     * @return {Select}
     */
    this.getRecordTypeValue = function() {
        return this.getFieldValue('custrecord_suitesocial_autopost_fld_rec');
    };
    /**
     * Returns the text of select field Record Type
     * 
     * @return {string}
     */
    this.getRecordTypeText = function() {
        return this.getFieldText('custrecord_suitesocial_autopost_fld_rec');
    };

    /**
     * Sets the Field
     * 
     * @param {Select}
     *        value Field
     * @return {TrackedField}
     */
    this.setFieldFieldValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_autopost_fld_rf', value);
        return this;
    };
    /**
     * Sets the text of select field Field
     * 
     * @param {string}
     *        text
     * @return {TrackedField}
     */
    this.setFieldFieldText = function(text) {
        this.setFieldText('custrecord_suitesocial_autopost_fld_rf', text);
        return this;
    };

    /**
     * Sets the Record Type
     * 
     * @param {Select}
     *        value Record Type
     * @return {TrackedField}
     */
    this.setRecordTypeValue = function(value) {
        this.setFieldValue('custrecord_suitesocial_autopost_fld_rec', value);
        return this;
    };
    /**
     * Sets the text of select field Record Type
     * 
     * @param {string}
     *        text
     * @return {TrackedField}
     */
    this.setRecordTypeText = function(text) {
        this.setFieldText('custrecord_suitesocial_autopost_fld_rec', text);
        return this;
    };

    /**
     * If Is Inactive is true
     * 
     * @return {booelan}
     */
    this.isIsInactiveTrue = function() {
        return this.getFieldValue('isinactive') == 'T';
    };
    /**
     * If Is Inactive is false
     * 
     * @return {booelan}
     */
    this.isIsInactiveFalse = function() {
        return this.getFieldValue('isinactive') != 'T';
    };
    /**
     * Sets Is Inactive to true
     * 
     * @return {TrackedField}
     */
    this.setIsInactiveToTrue = function() {
        this.setFieldValue('isinactive', 'T');
        return this;
    };
    /**
     * Sets Is Inactive to false
     * 
     * @return {TrackedField}
     */
    this.setIsInactiveToFalse = function() {
        this.setFieldValue('isinactive', 'F');
        return this;
    };
    this.FIELD_META = {
        id : {
            "name" : "Id",
            "internalId" : 0,
            "scriptId" : "id",
            "dataType" : "identifier",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        isinactive : {
            "name" : "Is Inactive",
            "internalId" : 0,
            "scriptId" : "isinactive",
            "dataType" : "checkbox",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null
        },
        custrecord_suitesocial_autopost_fld_rf : {
            "name" : "Field",
            "internalId" : "4688",
            "scriptId" : "custrecord_suitesocial_autopost_fld_rf",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Field",
            "selectRecordTypeInternalId" : "660",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_field",
            "selectRecordTypeShortName" : "SuiteSocialField"
        },
        custrecord_suitesocial_autopost_fld_fld : {
            "name" : "Field (hidden)",
            "internalId" : "4682",
            "scriptId" : "custrecord_suitesocial_autopost_fld_fld",
            "dataType" : "select",
            "selectRecordTypeName" : "Field",
            "selectRecordTypeInternalId" : "-124",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "Record Field"
        },
        custrecord_suitesocial_autopost_fld_dt : {
            "name" : "Field Data Type",
            "internalId" : "4683",
            "scriptId" : "custrecord_suitesocial_autopost_fld_dt",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Data Type Name"
        },
        custrecord_suitesocial_autopost_fld_id : {
            "name" : "Field ID",
            "internalId" : "4684",
            "scriptId" : "custrecord_suitesocial_autopost_fld_id",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Hidden",
            "sourcefrom" : "Script Id"
        },
        custrecord_suitesocial_autopost_fld_ft : {
            "name" : "Field Type",
            "internalId" : "4685",
            "scriptId" : "custrecord_suitesocial_autopost_fld_ft",
            "dataType" : "text",
            "selectRecordTypeName" : null,
            "selectRecordTypeInternalId" : null,
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Hidden",
            "sourcefrom" : "Base Type"
        },
        custrecord_suitesocial_autopost_fld_rt : {
            "name" : "Internal Record Type",
            "internalId" : "4686",
            "scriptId" : "custrecord_suitesocial_autopost_fld_rt",
            "dataType" : "select",
            "selectRecordTypeName" : "Scripted Record Type",
            "selectRecordTypeInternalId" : "-125",
            "ismandatory" : "F",
            "description" : null,
            "storevalue" : "F",
            "displaytype" : "Hidden",
            "sourcefrom" : "Type"
        },
        custrecord_suitesocial_autopost_fld_rec : {
            "name" : "Record Type",
            "internalId" : "4687",
            "scriptId" : "custrecord_suitesocial_autopost_fld_rec",
            "dataType" : "select",
            "selectRecordTypeName" : "SuiteSocial Record",
            "selectRecordTypeInternalId" : "686",
            "ismandatory" : "T",
            "description" : null,
            "storevalue" : "T",
            "displaytype" : "Normal",
            "sourcefrom" : "",
            "selectRecordTypeScriptId" : "customrecord_suitesocial_record",
            "selectRecordTypeShortName" : "SuiteSocialRecord"
        }
    };
}

var R_TRACKED_FIELD = {
    IS_INACTIVE : 'isinactive',
    FIELDFIELD : 'custrecord_suitesocial_autopost_fld_rf',
    FIELD_HIDDEN : 'custrecord_suitesocial_autopost_fld_fld',
    FIELD_DATA_TYPE : 'custrecord_suitesocial_autopost_fld_dt',
    FIELD_ID : 'custrecord_suitesocial_autopost_fld_id',
    FIELD_TYPE : 'custrecord_suitesocial_autopost_fld_ft',
    INTERNAL_RECORD_TYPE : 'custrecord_suitesocial_autopost_fld_rt',
    RECORD_TYPE : 'custrecord_suitesocial_autopost_fld_rec'
};

// ******************** END of auto-generated wrapper codes
// 151
