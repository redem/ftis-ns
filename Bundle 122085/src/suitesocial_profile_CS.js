var ssProflieCS = new _SS_Profile_CS();

SS_Profile_CS = {
    OnPageInit: function _OnPageInit()
    {
        ssProflieCS.OnPageInit();
    }
};



function _SS_Profile_CS()
{
    //Interface
    this.OnPageInit = _OnPageInit;
    this.OnImageFileBrowse = _OnImageFileBrowse;
    this.OnManualImageFileChange = _OnManualImageFileChange;

    //Attributes
    var CONTEXT = nlapiGetContext();



    function _OnPageInit()
    {
        if (CONTEXT.getRoleCenter() == "CUSTOMER")
        {
            if (Ext.get("manualFileUpload2") == null)
            {
                var widget = Ext.get("custrecord_suitesocial_profile_image_fs_tooltipMenu");
                if (widget != null)
                {
                    //html element insertions
                    var sUpload = '<input type="file" name="manualFileUpload2" id="manualFileUpload2" hidden="hidden" onchange="ssProflieCS.OnManualImageFileChange();">';
                    Ext.DomHelper.insertAfter(widget, sUpload);

                    var sImg = '<a data-helperbuttontype="new" id="custrecord_suitesocial_profile_image_upload" class="smalltextnolink field_widget" title="New" href=\'javascript:void("custrecord_suitesocial_profile_image_new")\' onclick="ssProflieCS.OnImageFileBrowse();"><img src="https://system.netsuite.com/uirefresh/img/field/add.png" /></a>';
                    Ext.DomHelper.insertAfter(widget, sImg);
                }
            }
        }
    }



    function _OnImageFileBrowse()
    {
        document.getElementById("manualFileUpload2").click();
    }



    function _OnManualImageFileChange()
    {
        var elFile = document.getElementById("manualFileUpload2");
        var file = elFile.files[0];
        var origFileName = file.name;
        var iExtIndex = origFileName.lastIndexOf(".");
        var extName = origFileName.substr(iExtIndex).toLowerCase();

        if ([".png", ".jpg", ".jpeg"].indexOf(extName.toLowerCase()) > -1)
        {
            var responseText = _UploadFile(file);

            var objResponse = JSON.parse(responseText);

            if (objResponse.error !== undefined)
            {
                alert(objResponse.error);
            }
            else
            {
                if (objResponse.fileId !== undefined)
                {
                    nlapiSetFieldValue("custrecord_suitesocial_profile_image", objResponse.fileId);
                }
            }
        }
    }




    function _UploadFile(file)
    {
        var formData = new FormData();
        formData.append("file", file);
        formData.append("targetId", 0);
        formData.append("fileData", "");

        var xhttp = new XMLHttpRequest();
        var url = nlapiResolveURL('SUITELET', 'customscript_ss_dnd_generic_suitelet', 'customdeploy_ss_dnd_generic_suitelet');
        xhttp.open("POST", url, false);
        xhttp.send(formData);

        return xhttp.responseText;
    }
}


