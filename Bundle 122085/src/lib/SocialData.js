function _SocialData()
{
    //Singleton
    if (this.constructor.prototype.instance != null)
    {
        return this.constructor.prototype.instance;
    }
    this.constructor.prototype.instance = this;



    //Dependencies
    var SuiteScript = System.Components.Use("SuiteScript");
    
    
    
    //Interface
    var _SS = this;
    var _Channels = _SS.Channels = new _ChannelData();
    var _Entities = _SS.Entities = new _EntityData();
    var _Profiles = _SS.Profiles = new _ProfileData();
    
    
    
    //Privates
    var _Util = new _Utilities();
    var _Customers = new _CustomerData();
    var _Employees = new _EmployeeData();

    var _SUPPORTED_ENTITY_TYPES = {
        Employee: "employee",
        CustJob: "customer"
    };
    
    
    
    
    
    
    
    
    
    
    function _ChannelData()
    {
        //Interface
        var _THIS = this;
        _THIS.GetUserReadable = _GetUserReadable;
        
        
        var _RECORD = {
            ID: "customrecord_newsfeed_channel",
            FIELDS: {
                NAME: "name",
                OWNER: "owner",
                IS_HIDDEN: "custrecord_newsfeed_channel_hidden",
                IS_COMMENTALLOWED: "custrecord_newsfeed_channel_comments",
                IS_AUTOSUBSCRIBE: "custrecord_newsfeed_channel_auto",
                
                SUBSIDIARIES: "custrecord_newsfeed_channel_sub",
                DEPARTMENTS: "custrecord_newsfeed_channel_dept",
                LOCATIONS: "custrecord_newsfeed_channel_loc",
                ROLES: "custrecord_newsfeed_channel_role",
                ENTITIES: "custrecord_newsfeed_channel_emp",
                
                ALLOWEDPOSTERS: "custrecord_newsfeed_channel_posters",
                SUBSIDIARYSELECTION: "custrecord_suitesocial_channel_sub_sel",
                CREATEDBYADMIN: "custrecord_newsfeed_channel_isadmin"
            }
        };





        function _GetUserReadable(user)
        {
            var filters = [
                new SuiteScript.nlobjSearchFilter("isinactive", null, "is", "F"),
                new SuiteScript.nlobjSearchFilter("name", null, "isnot", "Private Messages"),
                new SuiteScript.nlobjSearchFilter("name", null, "isnot", "Status Changes"),
                new SuiteScript.nlobjSearchFilter("name", null, "isnot", "Record Changes"),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.DEPARTMENTS, null, "anyof", "@NONE@", user.Department),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.LOCATIONS, null, "anyof", "@NONE@", user.Location),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.ROLES, null, "anyof", user.Roles),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.ENTITIES, null, "anyof", "@NONE@", user.Id)
            ];
            
            var columns = _Util.ToSearchColumns(_RECORD, _RECORD.FIELDS.NAME);

            var sr = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, filters, columns);
            if (sr == null)
            {
                return [];
            }

            
            var channels = [];
            for(var i = 0; i < sr.length; ++i)
            {
                channels.push(_ToObj(sr[i]));
            }
            
            return channels;
        }
        


        
        
        function _ToObj(row)
        {
            return {
                Id: row.getId(),
                Name: row.getValue(_RECORD.FIELDS.NAME),
                IsHidden: _Util.ToBoolean(row.getValue(_RECORD.FIELDS.IS_HIDDEN)),
                IsCommentAllowed: _Util.ToBoolean(row.getValue(_RECORD.FIELDS.IS_COMMENTALLOWED)),
                IsAutoSubscribe: _Util.ToBoolean(row.getValue(_RECORD.FIELDS.IS_AUTOSUBSCRIBE)),
                Subsidiaries: _Util.ToArray(row.getValue(_RECORD.FIELDS.SUBSIDIARIES)),
                Departments: _Util.ToArray(row.getValue(_RECORD.FIELDS.DEPARTMENTS)),
                Locations: _Util.ToArray(row.getValue(_RECORD.FIELDS.LOCATIONS)),
                Roles: _Util.ToArray(row.getText(_RECORD.FIELDS.ROLES)),
                Entities: _Util.ToArray(row.getValue(_RECORD.FIELDS.ENTITIES)),
                AllowedPosters: _Util.ToArray(row.getValue(_RECORD.FIELDS.ALLOWEDPOSTERS)),
                SubsidiarySelection: _Util.ToArray(row.getValue(_RECORD.FIELDS.SUBSIDIARYSELECTION)),
                Owner: row.getValue(_RECORD.FIELDS.OWNER),
                CreatedByAdmin: row.getValue(_RECORD.FIELDS.CREATEDBYADMIN),
                
                //For backward compatibility with existing code.
                //This is wrong since subsidiaries is multi-select and can return more than one value (comma delimeted)
                SubsidiaryId: row.getValue(_RECORD.FIELDS.SUBSIDIARIES),
                SubsidiaryName: row.getText(_RECORD.FIELDS.SUBSIDIARIES) == null ? "" : row.getText(_RECORD.FIELDS.SUBSIDIARIES)
            };
        }

    }  //_ChannelData










    function _EntityData()
    {
        //Interface
        var _THIS = this;
        _THIS.IsActive = _IsActive;
        _THIS.GetName = _GetName;
        _THIS.GetEntityCount = _GetEntityCount;
        _THIS.SearchEligibleEntities = _SearchEligibleEntities;



        var _RECORD = {
            ID: "entity",
            FIELDS: {
                _iid: "internalid",
                _iidnumber: "internalidnumber",
                _giveaccess: "giveaccess",
                _isinactive: "isinactive",
                Type: "type",
                Name: "entityid"
            }
        };





        function _IsActive(entityId)
        {
            if (entityId == null || entityId == "")
            {
                return false;
            }


            var filters = [
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS._iid, null, "anyof", entityId),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS._isinactive, null, "is", "F")
            ];

            var sr = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, filters);
            
            return sr != null;
        }


        
        
        
        function _GetName(entityId)
        {
            var rec = SuiteScript.nlapiLookupField(_RECORD.ID, entityId, [_RECORD.FIELDS.Type, _RECORD.FIELDS.Name]);

            if (_Util.IsEmpty(rec))
            {
                return null;
            }

            if (rec[_RECORD.FIELDS.Type] == "Employee")
            {
                return _Employees.GetName(entityId);
            }

            if (rec[_RECORD.FIELDS.Type] == "CustJob")
            {
                return _Customers.GetName(entityId);
            }

            return rec[_RECORD.FIELDS.Name];
        }





        function _GetEntityCount()
        {
            var entityTypes = [];
            for (var i in _SUPPORTED_ENTITY_TYPES)
            {
                entityTypes.push(i)
            }
        
            var filters = [
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS._giveaccess, null, "is", "T"),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS._isinactive, null, "is", "F"),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.Type, null, "anyof", entityTypes)
            ];
            
            var columns = [
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS._iid, null, "COUNT")
            ];

            var sr = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, filters, columns);

            return sr[0].getValue("internalid", null, "COUNT");
        }





        function _SearchEligibleEntities(lastProfileId)
        {
            var entityTypes = [];
            for (var i in _SUPPORTED_ENTITY_TYPES)
            {
                entityTypes.push(i)
            }
            
            var filters = [
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS._giveaccess, null, "is", "T"),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS._isinactive, null, "is", "F"),
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.Type, null, "anyof", entityTypes)
            ];

            if (lastProfileId != null && lastProfileId != "")
            {
                filters.push(new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS._iidnumber, null, "greaterthan", lastProfileId));
            }

            var columns = [
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS._iid).setSort()
            ];

            var rs = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, filters, columns);
            if (rs == null)
            {
                return [];
            }


            var profiles = [];
            for (var i = 0; i < rs.length; ++i)
            {
                profiles.push({ Id: rs[i].getId() });
            }

            return profiles;
        }

    }  //EntityData










    function _ProfileData()
    {
        //Interface
        var _THIS = this;
        _THIS.GetEntityProfile = _GetEntityProfile;
        _THIS.GetEntityProfiles = _GetEntityProfiles;
        _THIS.RequireProfile = _RequireProfile;
        _THIS.UpdateEntityStatus = _UpdateEntityStatus;
        _THIS.GetProfilesWithImage = _GetProfilesWithImage;
        _THIS.GetProfileEntityId = _GetProfileEntityId;
        _THIS.GetProfileCount = _GetProfileCount;
        _THIS.GetProfileDigestInfo = _GetProfileDigestInfo;



        var _RECORD = {
            ID: "customrecord_suitesocial_profile",
            FIELDS: {
                Name: "name",
                IsInactive: "isinactive",
                Owner: "owner",
                EntityId: "custrecord_suitesocial_profile_emp",
                Status: "custrecord_ss_profile_last_status",
                Image: "custrecord_suitesocial_profile_image",
                IsYammerUser: "custrecord_yammer_integration_enabled",
                AlertComment: "custrecord_suitesocial_profile_alrt_cmnt",
                AlertPrivateMessage: "custrecord_suitesocial_profile_alrt_drct",
                AlertStatusChange: "custrecord_suitesocial_profile_alrt_stat",
                AlertRecord: "custrecord_suitesocial_profile_alrt_rcrd",
                AlertMention: "custrecord_suitesocial_profile_alrt_ment",
                SendWeekendDigest : "custrecord_suitesocial_profile_wknd_dgst",
                SendDigest : "custrecord_suitesocial_profile_digest"
            }
        };
        
        var _IS_DEPT = SuiteScript.nlapiGetContext().getSetting("FEATURE", "departments") == "T";




        /*obj = {
            EntityId: {number},
            Name: {string} optional,
        }*/
        function _CreateProfile(obj)
        {
            var rec = SuiteScript.nlapiCreateRecord(_RECORD.ID);

            if(obj.EntityId == null)
            {
                return null;
            }

            var entityName = _Util.IfNull(obj.Name, _Entities.GetName(obj.EntityId));
            if (entityName == null)  //Assume that if no name, entity(customer or employee rec) does not exist
            {
                return null;
            }
            
            rec.setFieldValue(_RECORD.FIELDS.EntityId, obj.EntityId);
            rec.setFieldValue(_RECORD.FIELDS.Name, entityName);
            rec.setFieldValue(_RECORD.FIELDS.Owner, obj.EntityId);
            rec.setFieldValue(_RECORD.FIELDS.IsYammerUser, "T");

            var profile = _ToObj(rec);
            profile.Id = SuiteScript.nlapiSubmitRecord(rec);

            return profile;
        }





        function _GetStandardColumns()
        {
            var columns = [
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.EntityId),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.Name),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.Image),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.Status),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.IsYammerUser),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.AlertComment),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.AlertPrivateMessage),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.AlertStatusChange),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.AlertRecord),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.AlertMention)
            ];
            
            return columns;
        }





        function _ToObj(row)
        {
            var getValue = "getFieldValue" in row ? "getFieldValue" : "getValue";
            var getText = "getFieldText" in row ? "getFieldText" : "getText";

            return {
                Id: row.getId(),
                EntityId: row[getValue](_RECORD.FIELDS.EntityId),
                Name: row[getValue](_RECORD.FIELDS.Name),
                Image: row[getValue](_RECORD.FIELDS.Image),
                Status: row[getValue](_RECORD.FIELDS.Status),
                IsYammerUser : row[getValue](_RECORD.FIELDS.IsYammerUser),
                AlertComment : row[getValue](_RECORD.FIELDS.AlertComment),
                AlertPrivateMessage : row[getValue](_RECORD.FIELDS.AlertPrivateMessage),
                AlertStatusChange : row[getValue](_RECORD.FIELDS.AlertStatusChange),
                AlertRecord : row[getValue](_RECORD.FIELDS.AlertRecord),
                AlertMention : row[getValue](_RECORD.FIELDS.AlertMention)
            };
        }





        //Load profile given entityId.  Return null if none exist.
        function _GetEntityProfile(entityId)
        {
            var filters = [
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.EntityId, null, "is", entityId)
            ];

            var sr = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, filters, _GetStandardColumns());
            
            return sr == null ? null : _ToObj(sr[0]);
        }





        function _GetEntityProfiles(entityIds)
        {
            if (entityIds.length == 0)
            {
                return [];
            }
            
            var filters = [
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.EntityId, null, "anyof", entityIds)
            ];

            var sr = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, filters, _GetStandardColumns());
            if (sr == null)
            {
                return [];
            }

            var profiles = [];
            for (var i = 0; i < sr.length; ++i)
            {
                profiles.push(_ToObj(sr[i]));
            }

            return profiles;
        }





        //Load profile rec given entityId, if none exist then create one
        function _RequireProfile(entityId)
        {
            var profile = _GetEntityProfile(entityId);
            if (profile == null)
            {
                profile = _CreateProfile({ EntityId: entityId });
            }

            return profile;
        }





        function _UpdateEntityStatus(entityId, status)
        {
            var profile = _RequireProfile(entityId);

            _UpdateProfileStatus(profile.Id, status);

            return profile;
        }





        function _UpdateProfileStatus(profileId, status)
        {
            if (profileId != null)
            {
                SuiteScript.nlapiSubmitField(_RECORD.ID, profileId, _RECORD.FIELDS.Status, status);
            }
        }





        function _GetProfilesWithImage()
        {
            var filters = [
                new SuiteScript.nlobjSearchFilter(_RECORD.FIELDS.Image, null, "noneof", ["@NONE@"])
            ];

            var columns = [
                new SuiteScript.nlobjSearchColumn("internalid").setSort(),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.EntityId),
                new SuiteScript.nlobjSearchColumn(_RECORD.FIELDS.Image)
            ];


            var profiles = [];
            var lastIid = 0;   
            do
            {
                var pageFilters = filters.concat([
                    new SuiteScript.nlobjSearchFilter("internalidnumber", null, "greaterthan", lastIid)
                ]);
            
                var sr = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, pageFilters, columns);
                if (sr != null)
                {
                    for (var i = 0; i < sr.length; ++i)
                    {
                        profiles.push(_ToObj(sr[i]));
                    }

                    lastIid = sr[sr.length - 1].getId();
                }
                
            } while(sr != null && sr.length >= 1000);

            return profiles;
        }





        function _GetProfileEntityId(profileId)
        {
            if (profileId == null)
            {
                return null;
            }
            
            return SuiteScript.nlapiLookupField(_RECORD.ID, profileId, _RECORD.FIELDS.EntityId);
        }





        function _GetProfileCount()
        {
            var entityTypes = [];
            for (var i in _SUPPORTED_ENTITY_TYPES)
            {
                entityTypes.push(i)
            }
            
            var filters = [
                new SuiteScript.nlobjSearchFilter("giveaccess", _RECORD.FIELDS.EntityId, "is", "T"),
                new SuiteScript.nlobjSearchFilter("isinactive", _RECORD.FIELDS.EntityId, "is", "F"),
                new SuiteScript.nlobjSearchFilter("type", _RECORD.FIELDS.EntityId, "anyof", entityTypes)
            ];

            var columns = [
                new SuiteScript.nlobjSearchColumn("internalid", null, "COUNT")
            ];

            var sr = SuiteScript.nlapiSearchRecord(_RECORD.ID, null, filters, columns);

            return sr[0].getValue("internalid", null, "COUNT");
        }





        function _GetProfileDigestInfo(profileId)
        {
            if (profileId == undefined || profileId == "")
            {
                return null;
            }

            return SuiteScript.nlapiLookupField(_RECORD.ID, profileId, [_RECORD.FIELDS.EntityId, _RECORD.FIELDS.Image, _RECORD.FIELDS.SendWeekendDigest, _RECORD.FIELDS.SendDigest ]);
        }

    }  //ProfileData










    function _Utilities()
    {
        this.ToSearchColumns = _ToSearchColumns;
        this.ToBoolean = _ToBoolean;
        this.ToArray = _ToArray;
        this.IsEmpty = _IsEmpty;
        this.IfNull = _IfNull;
        
        
        
        
        
        function _ToSearchColumns(recDef, sortColumn)
        {
            var columns = [];
            for(var i in recDef.FIELDS)
            {
                var col = new SuiteScript.nlobjSearchColumn(recDef.FIELDS[i]);
                if(recDef.FIELDS[i] == sortColumn)
                {
                    col.setSort();
                }
                
                columns.push(col);
            }
            
            return columns;
        }
        
        
        
        
        
        function _ToBoolean(sValue)
        {
            var s = sValue == null? "": sValue.toUpperCase();
            return s == "T";
        }





        function _ToArray(sValue)
        {
            return sValue == null || sValue == ""? []: sValue.split(",");
        }


        
        
        
        function _IsEmpty(value)
        {
            return value == null || value == "";
        }





        function _IfNull(value, defaultValue)
        {
            return _IsEmpty(value) ? defaultValue : value;
        }

    }  //Utilities
    
    
    
    
    





    function _EmployeeData()
    {
        //Interface
        var _THIS = this;
        _THIS.GetName = _GetName;



        var _RECORD = {
            ID: "employee",
            FIELDS: {
                _iid: "internalid",
                FirstName: "firstname",
                LastName: "lastname"
            }
        };

        
        
        
        
        function _GetName(empId)
        {
            var emp = SuiteScript.nlapiLookupField(_RECORD.ID, empId, [
                _RECORD.FIELDS.FirstName,
                _RECORD.FIELDS.LastName
            ]);

            if (_Util.IsEmpty(emp))
            {
                return undefined;
            }
            
            var name = _Util.IfNull(emp[_RECORD.FIELDS.FirstName], "");
            name += _Util.IsEmpty(emp[_RECORD.FIELDS.FirstName]) || _Util.IsEmpty(emp[_RECORD.FIELDS.LastName]) ? "" : " ";
            name += _Util.IfNull(emp[_RECORD.FIELDS.LastName], "");

            return name;
        }
    }  //EmployeeData
    
    
    
    
    
    
    
    
    
    
    function _CustomerData()
    {
        //Interface
        var _THIS = this;
        _THIS.GetName = _GetName;



        var _RECORD = {
            ID: "customer",
            FIELDS: {
                _iid: "internalid",
                IsPerson: "isperson",
                FirstName: "firstname",
                LastName: "lastname",
                CompanyName: "companyname"
            }
        };

        
        
        
        
        function _GetName(empId)
        {
            var cust = SuiteScript.nlapiLookupField(_RECORD.ID, empId, [
                _RECORD.FIELDS.IsPerson,
                _RECORD.FIELDS.FirstName,
                _RECORD.FIELDS.LastName,
                _RECORD.FIELDS.CompanyName,
            ]);

            if (_Util.IsEmpty(cust))
            {
                return undefined;
            }
            
            if(cust[_RECORD.FIELDS.IsPerson] == "F")  //Is customer a company?
            {
                return cust[_RECORD.FIELDS.CompanyName];
            }

            //Customer is an individual
            var name = _Util.IfNull(cust[_RECORD.FIELDS.FirstName], "");
            name += _Util.IsEmpty(cust[_RECORD.FIELDS.FirstName]) || _Util.IsEmpty(cust[_RECORD.FIELDS.LastName]) ? "" : " ";
            name += _Util.IfNull(cust[_RECORD.FIELDS.LastName], "");

            return name;
        }
    }  //CustomerData
}
