/**
 * Module Description Here
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 *
 * Version    Date            Author           Remarks
 * 1.00       02 Jan 2012     tcaguioa
 *
 */
/**
 * Hides the options Record Changes and Status Changes
 */
function ssapiHideRecordAndStatusChanges() {

    var logger = new ssobjLogger(arguments, true);
    // drop down item
    var els = document.getElementsByTagName('div');
    for (var i = 0; i < els.length; i++) {
        if (els[i].innerHTML == 'Record Changes') {
            els[i].style.display = 'none';
        }
        if (els[i].innerHTML == 'Status Changes') {
            els[i].style.display = 'none';
        }
    }

    // // link
    // els = document.getElementsByTagName('a');
    // for (i = 0; i < els.length; i++) {
    // if (els[i].innerHTML.indexOf('&nbsp;New') > -1) {
    // // els[i].style.display = 'none';
    // }
    // }

    logger.log('DONE');
}

function ssapiShowOrHideRecipients() {
    var channelName = nlapiGetFieldText('custrecord_autopost_channel');
    if (channelName == 'Private Messages') {
        nlapiSetFieldDisplay('custrecord_autopost_recipient', true);
    } else {
        nlapiSetFieldDisplay('custrecord_autopost_recipient', false);
    }
}

/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {void}
 */
function clientFieldChanged(type, name, linenum) {
    // Issue: 218045 [SuiteSocial] Assistant > Enable Records > Scheduled P
    var logger = new ssobjLogger(arguments);
    if (name == 'custrecord_autopost_channel') {
        ssapiShowOrHideRecipients();
    }

    if (name == 'custpage_savedsearch') {
        nlapiSetFieldValue(FLD_AUTOPOST_SEARCH, nlapiGetFieldValue('custpage_savedsearch'));
    }
    logger.log('DONE');
}

function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    deleteAutoPost(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

    if (typeof window != 'undefined') {
        window.location.href = window.opener.originalUrl;
    }

    // if (ssapiHasValue(window.opener)) {
    // window.opener.location.reload();
    // window.close();
    // return;
    // }

}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: News Feed. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a Scheduled Post subscription inactive.'.tl() + ' ';
    inactivateQuestion += 'While a scheduled post is inactive,'.tl() + ' ';
    inactivateQuestion += 'posts related to the scheduled post will not be created.'.tl() + ' ';
    inactivateQuestion += 'Do you want to make this scheduled post inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?'.tl(),
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {
    // var logger = new ssobjLogger('clientSaveRecord');
    // var cols = [];
    // cols.push({
    // columnId: FLD_AUTOPOST_FLD_REC,
    // operator: OP_ANYOF,
    // value: nlapiGetFieldValue(FLD_AUTOPOST_FLD_REC)
    // });
    // cols.push({
    // columnId: FLD_AUTOPOST_FLD_FLD,
    // operator: OP_ANYOF,
    // value: nlapiGetFieldValue(FLD_AUTOPOST_FLD_FLD)
    // });
    //    
    // var internalId = nlapiGetFieldValue('id');
    // if (hasDuplicates(REC_AUTOPOST_FIELD, cols, internalId)) {
    // uiShowError('Field "' + nlapiGetFieldText(FLD_AUTOPOST_FLD_FLD) + '"
    // already exists in the database.');
    // return false;
    // }
    //    
    return true;
}

function pageInit(type) {

    var logger = new ssobjLogger(arguments);

    nlapiSetFieldDisplay('customform', false);

    // Issue: 218045 [SuiteSocial] Assistant > Enable Records > Scheduled P
    ssapiShowOrHideRecipients();
    // Issue: 217946 [SuiteSocial] Asistant - Scheduled Post. The time format
    // you type
    // Scheduled Post> the time format you type doesn't work as you would
    // expect.
    // For example, entering "3:00 PM" automatically changed to "3:00 am"
    // This is a core issue
    // https://system.netsuite.com/app/crm/support/issuedb/issue.nl?id=5103910&whence=
    // HACK: capture keyup event
    var id = 'custrecord_autopost_sched_tod';
    Ext.get(id).on('keyup', function(e) {
        var value = document.getElementById(id).value;
        if (ssapiHasNoValue(value)) {
            return;
        }
        value = value.replace('P', 'p');
        document.getElementById(id).value = value.replace('M', 'm');
    });

    Ext.get(id).on('onkeyup', function(e) {
        var value = document.getElementById(id).value;
        if (ssapiHasNoValue(value)) {
            return;
        }
        value = value.replace('P', 'p');
        document.getElementById(id).value = value.replace('M', 'm');
    });

    setInterval('ssapiHideRecordAndStatusChanges()', 500);
    setInterval('ssapiHideNewInOptionList();', 500);
    // hide new and popup icons
    Ext.get('custrecord_autopost_channel_popup_new').dom.style.display = 'none';

}

/**
 * This is used in translating the menu item Delete Including Dependent Records
 * It needs to be called from an interval since the menu item is created on
 * hover
 */
function ssapiTranslateDeleteIncludingDependentRecords() {
    var link = jQuery('span:contains("Delete Including Dependent Records")')[0];
    if (link) {
        link.innerHTML = link.innerHTML.tl();
        clearInterval(ssobjGlobal.translateDeleteIncludingDependentRecords);
    }
}
ssobjGlobal.translateDeleteIncludingDependentRecords = setInterval('ssapiTranslateDeleteIncludingDependentRecords();', 500);
