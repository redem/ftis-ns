/**
 * Module Description Here
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 *
 * Version    Date            Author           Remarks
 * 1.00       02 Jan 2012     tcaguioa
 *
 */
/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {void}
 */
function clientFieldChanged(type, name, linenum) {
    var logger = new ssobjLogger('clientFieldChanged');

    logger.log('type=' + type + '; name=' + name + '; linenum=' + linenum);
    if (name == 'custpage_record') {
        // // Issue: 213422 [SuiteSocial] assistant > setup default auto
        // subscribe ty
        // apply the change so that the FIELD field will source
        nlapiSetFieldValue(FLD_DEFAULT_AUTOSUB_TYPE_RT, nlapiGetFieldValue('custpage_record'));
    }
}

function pageInit(type) {
    // alert(type)
    nlapiSetFieldDisplay('customform', false);

    // upgrade code
    if (ssapiHasNoValue(nlapiGetFieldValue('custrecord_ss_def_autosub_type_rf'))) {
        nlapiSetFieldText('custrecord_ss_def_autosub_type_rf', nlapiGetFieldText('custrecord_ss_def_autosub_type_fld'));
    }

    setInterval('ssapiHideNewInOptionList();', 500);

    // since we cannot filter the default dropdown, we create a another
    // dropdown. This dropdown is at the end of the field list.
    // we will move it near the original field and then hide the original field.

    // var parent =
    // document.getElementById('inpt_custpage_record3').parentNode.parentNode.parentNode;
    // var parentTrDisplay = Ext.get('inpt_custpage_record3').findParent('tr');
    // var parentTrHidden =
    // Ext.get('inpt_custrecord_ss_def_autosub_type_rt1').findParent('tr');
    // Ext.get(parentTrHidden).insertSibling(Ext.get(parentTrDisplay))
    // Ext.get(parentTrHidden).setVisible(false);

    // var parent =
    // document.getElementById('inpt_custpage_record3').parentNode.parentNode.parentNode;
    // Ext.get('inpt_custrecord_ss_def_autosub_type_rt1').insertSibling(Ext.get(parent))
}

function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    deleteDefaultAutoSubType(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

    if (ssapiHasValue(window.opener)) {
        window.opener.location.reload();
        window.close();
        return;
    }

}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a Default AutoSubscribe Type, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records:  Default AutoSubscribe Type Roles. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a Default AutoSubscribe Type inactive.'.tl();
    inactivateQuestion += ' While a Default AutoSubscribe Type is inactive, '.tl();
    inactivateQuestion += 'employees will not be automatically subscribed to the Default AutoSubscribe Type. '.tl();
    inactivateQuestion += 'Do you want to make this  Default AutoSubscribe Type inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?'.tl(),
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {
    // Issue: 213592 [SuiteSocial] Able to create duplicate autosubscribe type
    // in
    var logger = new ssobjLogger('clientSaveRecord');

    ssapiWait('Saving...'.tl());
    var autoSubName = nlapiGetFieldValue(FLD_DEFAULT_AUTOSUB_TYPE_NAME);

    var cols = [];
    cols.push({
        columnId : FLD_DEFAULT_AUTOSUB_TYPE_NAME,
        operator : OP_IS,
        value : autoSubName
    });

    var internalId = nlapiGetFieldValue('id');
    if (hasDuplicates(REC_DEFAULT_AUTOSUB_TYPE, cols, internalId)) {
        uiShowError('"' + autoSubName + '" already exists in the custom record.'.tl());
        return false;
    }
    return true;
}
