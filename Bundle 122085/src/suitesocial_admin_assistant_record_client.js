/**
 * Module Description Here You are using the default templates which should be
 * customized to your needs. You can change your user name under
 * Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version Date Author Remarks 1.00 02 Jan 2012 tcaguioa
 * 
 */
/*
 * Used in hiding unnecessary items in the UI such as the customize and attach
 * buttons. This will work only when in edit mode since scripts do not run in
 * view mode.
 */
function uiHideUnnecessaryItems() {
    Ext.select('#tbl_customize').setStyle({
        display : 'none'
    });
    Ext.select('#tbl_attach').setStyle({
        display : 'none'
    });
    var ids = [ 'recmachcustrecord_suitesocial_autopost_fld_rec_searchid_fs_lbl', 'recmachcustrecord_suitesocial_autosub_type_rec_searchid_fs_lbl', 'recmachcustrecord_autopost_record_searchid_fs_lbl', 'recmachcustrecord_sssl_rel_ss_rec_searchid_fs_lbl' ];
    ids.push('recmachcustrecord_sss_record_type_searchid_fs_lbl');
    for (var x = 0; x < ids.length; x++) {
        var els = Ext.select('#' + ids[x]);
        // console.log(els.getCount())

        for (var y = 0; y < els.getCount(); y++) {
            // console.log( els.elements[i] )
            // console.log(
            // els.elements[i].parentNode.parentNode.parentNode.parentNode.style.display
            // = 'none' )
            // console.log(
            // els.elements[i].parentNode.parentNode.parentNode.parentNode.style.display
            // = 'block' )
            Ext.get(els.elements[y]).findParent('table', 10, true).setStyle({
                display : 'none'
            });
        }
    }
}

function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    ssapiDeleteRecordType(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

    // alert(ssapiHasValue(window.opener))
    if (ssapiHasValue(window.opener)) {
        window.opener.location.reload();
        window.close();
        return;
    }

}

/*
 * Makes the record inactive
 */
function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: Auto Post Fields, AutoSubscribe Types and Scheduled Posts. Continue?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a SuiteSocial Record Type inactive.'.tl() + ' ';
    inactivateQuestion += 'While a SuiteSocial record type is inactive,'.tl() + ' ';
    inactivateQuestion += 'posts related to the record type will not be created. User cannot follow that record type.'.tl() + ' ';
    inactivateQuestion += 'Do you want to make this SuiteSocial record type inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?'.tl(),
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
// function clientSaveRecord(){
//  
// var cols = [];
// cols.push({columnId: FLD_AUTOPOST_FLD_REC, operator : OP_ANYOF, value:
// nlapiGetFieldValue(FLD_AUTOPOST_FLD_REC)});
// cols.push({columnId: FLD_AUTOPOST_FLD_FLD, operator : OP_ANYOF, value:
// nlapiGetFieldValue(FLD_AUTOPOST_FLD_FLD)});
//  
// if( hasDuplicates(REC_AUTOPOST_FIELD, cols,
// nlapiGetFieldValue(FLD_INTERNAL_ID))){
// uiShowError('Field "'+nlapiGetFieldText(FLD_AUTOPOST_FLD_FLD)+'" already
// exists inthe database.');
// return false;
// }
//
// return true;
// }
/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {Boolean} True to continue changing field value, false to abort value
 *         change
 */
function clientValidateField(type, name, linenum) {
    var logger = new ssobjLogger('');
    logger.log('type=' + type + '; name=' + name + '; linenum' + linenum);

    return true;
}

function validateDelete() {
    throw 'validateDelete';
}

function recalc() {
    throw 'validateDelete';
}

function fieldChanged() {
    throw 'validateDelete';
}

/*
 * @appliedtorecord employee // temp
 */
function pageInit(type) {

    nlapiSetFieldDisplay('customform', false);

    if (type == 'edit') {
        nlapiDisableField(FLD_RECORD_TYPE, true);
    }

    // Issue: 215871 [SuiteSocial] remove unnecessary items in the Assistant
    uiHideUnnecessaryItems();
    // hideRemoveLinks();
    // setInterval('hideRemoveLinks()', 1000);
}

/**
 * This is used in translating the menu item Delete Including Dependent Records
 * It needs to be called from an interval since the menu item is created on
 * hover
 */
function ssapiTranslateDeleteIncludingDependentRecords() {
    var link = jQuery('span:contains("Delete Including Dependent Records")')[0];
    if (link) {
        link.innerHTML = link.innerHTML.tl();
        clearInterval(ssobjGlobal.translateDeleteIncludingDependentRecords);
    }
}
ssobjGlobal.translateDeleteIncludingDependentRecords = setInterval('ssapiTranslateDeleteIncludingDependentRecords();', 500);

function clientSaveRecord(type) {
    // Issue: 213591 [SuiteSocial] No dialog to refer on the summary to reactive
    if (nlapiGetFieldValue(FLD_ISINACTIVE) == 'T') {
        alert('If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl());
    }
    return true;
    // alert('clientSaveRecord' + type)
    // return false
    // var cols = [];
    // cols.push({
    // columnId: FLD_RECORD_TYPE,
    // operator: OP_ANYOF,
    // value: nlapiGetFieldValue(FLD_RECORD_TYPE)
    // });
    // // cols.push({
    // // columnId: FLD_AUTOSUB_TYPE_FLD,
    // // operator: OP_ANYOF,
    // // value: nlapiGetFieldValue(FLD_AUTOSUB_TYPE_FLD)
    // // });
    //    
    // if (hasDuplicates(REC_RECORD, cols, nlapiGetFieldValue('id'))) {
    // uiShowError('Field "' + nlapiGetFieldText(FLD_RECORD_TYPE) + '" already
    // exists in the database.');
    // return false;
    // }
}

// nlapiDisableField('customform', true);

// alert('wohho');
// var imgs = document.getElementsByTagName('a');
// for (var i = 0; i < imgs.length; i++) {
// if (imgs[i].innerHTML == 'Remove') {
// // imgs[i].style.display = 'none';
// }
// }

/*
 * Removes the Remove link on sublists. This is a hack since there is no option
 * to remove the link with the field remaining as inline
 */
function hideRemoveLinks() {
    var imgs = document.getElementsByTagName('a');
    for (var i = 0; i < imgs.length; i++) {
        var img = imgs[i];
        if (img.innerHTML == 'Remove') {
            if (img.style.display != 'none') {
                img.style.display = 'none';
            }

        }
    }

}
