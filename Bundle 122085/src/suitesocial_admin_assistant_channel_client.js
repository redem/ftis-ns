/**
 * Module Description Here You are using the default templates which should be
 * customized to your needs. You can change your user name under
 * Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version Date Author Remarks 1.00 02 Jan 2012 tcaguioa
 * 
 */
function processDeleteResult(btn) {
    // alert(btn);
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    deleteChannel(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();

    // Issue: 213210 [SuiteSocial] assistant> setup channels > setup channe
    if (ssapiHasValue(window.opener)) {
        window.opener.location.reload();
        window.close();
        return;
    }

}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record'.tl(),
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: News Feed, Profile Channel and Scheduled Posts. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a channel inactive.'.tl() + ' ';
    inactivateQuestion += 'While a channel is inactive, it will not be displayed in the portlet.'.tl() + ' ';
    inactivateQuestion += 'Posts related to the channel will not be created. User cannot subscribe to that channel.'.tl() + ' ';
    inactivateQuestion += 'Do you want to make this channel inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?'.tl(),
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
function channelClientSaveRecord() {
    // added next line
    var channelName = nlapiGetFieldValue(FLD_NEWSFEED_CHANNEL_NAME);
    if (channelName.trim() === '') {
        uiShowError('Please provide a value for channel.'.tl());
        return false;
    }
    var cols = [];
    cols.push({
        columnId : FLD_NEWSFEED_CHANNEL_NAME,
        operator : OP_IS,
        value : channelName
    });

    if (hasDuplicates(REC_NEWSFEED_CHANNEL, cols, nlapiGetFieldValue('id'))) {
        uiShowError('Channel "'.tl() + channelName + '" already exists in the database.'.tl());
        return false;
    }
    return true;
}

function channelPageInit() {

    try {
        nlapiSetFieldDisplay('customform', false);
    } catch (e) {
        // in case custom form is not existing, just supress error
    }// / hide the menu where delete resides
    // Ext.select('.pgm_action_menu').setStyle({
    // display: 'none'
    // })
    // alert('pageInit rrrr');
    // do not allow access to built-in in channels
    var channelName = nlapiGetFieldValue(FLD_NEWSFEED_CHANNEL_NAME);

    if (channelName == 'Private Messages' || channelName == 'Record Changes' || channelName == 'Status Changes') {
        uiShowError('You are not allowed to access this record.'.tl());
        document.body.style.visibility = 'hidden';
        // window.location.href = 'https://system.netsuite.com';
        return false;
    }
}

/**
 * This is used in translating the menu item Delete Including Dependent Records
 * It needs to be called from an interval since the menu item is created on
 * hover
 */
function ssapiTranslateDeleteIncludingDependentRecords() {
    var link = jQuery('span:contains("Delete Including Dependent Records")')[0];
    if (link) {
        link.innerHTML = link.innerHTML.tl();
        clearInterval(ssobjGlobal.translateDeleteIncludingDependentRecords);
    }
}
ssobjGlobal.translateDeleteIncludingDependentRecords = setInterval('ssapiTranslateDeleteIncludingDependentRecords();', 500);
