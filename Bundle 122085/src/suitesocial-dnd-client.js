/**
* � 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
*/

/**
* Module Description
* 
* Version    Date            Author           Remarks
* 1.00       13 Jan 2015     eligon
*
*/

/**
* Constant variables are defined and initialized here
*/
var ssobjGlobal = ssobjGlobal || {};
ssobjGlobal.TITLE = ssobjGlobal.TITLE || "SuiteSocial";
ssobjGlobal.MAXIMUM_IMAGE_SIZE = 5242880;
// 5MB is used here instead of 10MB due to the limitation of file.getValue()
// being able to retrieve only up to 5MB of data despite NetSuite supports
// upload of up to 10MB.

// initialize global variables
ssapiInitDnDGlobalVariables();

/**
* Entry method to enable drag and drop
* 
* @param {Object}
*        dndElements Set of drag-and-drop element IDs
* @param {function}
*        callbackAfterUpload Function to call after file upload is complete
*/
function ssapiEnableDnDOnSuiteSocial(bEnable, dndElements, callbackAfterUpload)
{
    var logger = new ssobjLogger(arguments);

    // check if browser is supported for dnd
    if (!ssapiIsBrowserSupportedForDragAndDrop())
    {
        logger.log("File drag and drop is not supported on this browser.");
        return;
    }

    // save target id to global
    ssobjGlobal.dndElements[dndElements.targetId] = dndElements;
    ssobjGlobal.callbackAfterUpload = callbackAfterUpload;

    // check if dnd should be enabled or disabled
    var isAllowUpload = nlapiGetContext().getRoleCenter() == "CUSTOMER" ? ssapiGetSetting("custscript_ss_allow_customer_upload") : true;
    
    if (bEnable && isAllowUpload)
    {
        // update watermark to show that drag and drop is enabled
        var watermarkText = "";
        if (dndElements.targetWatermarkId == "richEditorWatermark")
            watermarkText = "Write a message or drag and drop a file here";
        else if (dndElements.targetWatermarkId == "richEditorReplyTextBoxWatermarkPrivate")
            watermarkText = "Write a message or drag and drop a file here";
        else
            watermarkText = "Write a comment or drag and drop a file here";
        Ext.get(dndElements.targetWatermarkId).update(watermarkText);

        // enable drag and drop on target
        ssapiAddDnDEvents(Ext.get(dndElements.targetId).dom);
    }
    else
    {
        // update watermark to show that drag and drop is disabled
        var watermarkText = "";
        if (dndElements.targetWatermarkId == "richEditorWatermark")
            watermarkText = "Share something with your colleagues";
        else if (dndElements.targetWatermarkId == "richEditorReplyTextBoxWatermarkPrivate")
            watermarkText = "Write a message";
        else
            watermarkText = "Write a comment";
        Ext.get(dndElements.targetWatermarkId).update(watermarkText);

        // disable drag and drop on target
        ssapiRemoveDnDEvents(Ext.get(dndElements.targetId).dom);
    }
}

/**
* Define global variables
*/
function ssapiInitDnDGlobalVariables()
{
    // flag if drag enter is triggered and drag exit is not yet triggered
    ssobjGlobal.dadIsDragEnter = false;

    // one xmlRequest instance is used for each file
    ssobjGlobal.xmlRequests = [];
    // this is used as current index for ssobjGlobal.xmlRequests
    ssobjGlobal.uploadId = 0;

    // holds the current uploads where each active upload is one property. The
    // key is the upload id.
    ssobjGlobal.currentUploadHash = {};

    // holds the dnd elements for each target element. The key is the target id.
    ssobjGlobal.dndElements = {};

    // holds the pending uploads
    ssobjGlobal.pendingUploads = {};

    // add beforeUnload event handler
    ssapiAddBeforeUnloadEventHandler();
}

/**
* Checks the browser to know if drag and drop is supported
* 
* @returns {Boolean} Returns true if drag and drop is supported, otherwise,
*          false.
*/
function ssapiIsBrowserSupportedForDragAndDrop()
{
    // if chrome, supported
    if (Ext.isChrome)
    {
        return true;
    }
    // if firefox, supported
    if (Ext.isGecko)
    {
        return true;
    }
    // if safari, supported
    if (Ext.isSafari)
    {
        return true;
    }
    // if IE10 or higher, supported
    var ieVersion = ssapiGetIEVersion();
    if (ssapiHasValue(ieVersion))
    {
        if (ieVersion >= 10)
        {
            return true;
        }
    }

    // if others, not supported
    return false;
}

/**
* Attaches an event to an element
* 
* @param {Object}
*        elem Element
* @param {Object}
*        evnt Event
* @param {Object}
*        func Function
*/
function ssapiAddEvent(elem, evnt, func)
{
    if (elem.addEventListener) // W3C DOM
        elem.addEventListener(evnt, func, false);
    else if (elem.attachEvent)
    { // IE DOM
        elem.attachEvent("on" + evnt, func);
    } else
    { // No much to do
        elem[evnt] = func;
    }
}

/**
* Add drag and drop events to an element
* 
* @param {Object}
*        el
*/
function ssapiAddDnDEvents(el)
{
    ssapiAddEvent(el, "drop", ssapiFileDrop);
    ssapiAddEvent(el, "dragenter", ssapiDragEnter);
    ssapiAddEvent(el, "dragover", ssapiNoOperationHandler);
    if (ssapiIsIE())
    {
        // IE
        ssapiAddEvent(el, "dragleave", ssapiDragExit);
    } else if (Ext.isGecko)
    {
        // FF
        ssapiAddEvent(el, "dragexit", ssapiDragExit);
    } else
    {
        // chrome, safari
        ssapiAddEvent(el, "dragleave", ssapiDragExit);
    }
}

/**
* Detaches an event to an element
* 
* @param {Object}
*        elem Element
* @param {Object}
*        evnt Event
* @param {Object}
*        func Function
*/
function ssapiRemoveEvent(elem, evnt, func)
{
    if (elem.removeEventListener) // W3C DOM
        elem.removeEventListener(evnt, func, false);
    else if (elem.detachEvent)
    { // IE DOM
        elem.detachEvent("on" + evnt, func);
    } else
    { // set to null
        elem[evnt] = null;
    }
}

/**
* Remove drag and drop events to an element
* 
* @param {Object}
*        el
*/
function ssapiRemoveDnDEvents(el)
{
    ssapiRemoveEvent(el, "drop", ssapiFileDrop);
    ssapiRemoveEvent(el, "dragenter", ssapiDragEnter);
    ssapiRemoveEvent(el, "dragover", ssapiNoOperationHandler);
    if (ssapiIsIE())
    {
        // IE
        ssapiRemoveEvent(el, "dragleave", ssapiDragExit);
    } else if (Ext.isGecko)
    {
        // FF
        ssapiRemoveEvent(el, "dragexit", ssapiDragExit);
    } else
    {
        // chrome, safari
        ssapiRemoveEvent(el, "dragleave", ssapiDragExit);
    }
}

/**
* Actions to take when a file is dropped into the richtext editor
* 
* @param {Object}
*        evt
*/
function ssapiFileDrop(evt)
{
    var logger = new ssobjLogger(arguments);

    var e = evt || window.event;
    var target = e.target || e.srcElement;

    ssobjGlobal.dadIsDragEnter = false;

    // special check for IE
    if (ssapiIsIE())
    {
        // check if target is HTMLParagraphElement, which is already the content
        // of the text editor
        if (target instanceof HTMLParagraphElement)
        {
            // get the parent node
            target = target.parentNode;
        }
    }

    // get dnd elements
    var dndElements = ssobjGlobal.dndElements[target.id];

    // reset target color
    Ext.get(target).setStyle('borderColor', '#d8d8d8');

    // check if an upload process is currently on-going
    if (ssapiHasValue(ssobjGlobal.ssapiShowUploadProgress))
    {
        Ext.MessageBox.show({
            title: ssobjGlobal.TITLE,
            msg: 'File upload is currently in progress. You can wait for the current upload to finish or cancel the current upload before adding another file.',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING
        });
        return ssapiStopPropagation(evt);
    }

    // get files
    var files = evt.dataTransfer.files;

    // check no. of files dropped
    var count = files.length;
    if (count === 0)
    {
        logger.log('no files');
        return ssapiStopPropagation(evt);
    }
    if (count > 1)
    {
        Ext.MessageBox.show({
            title: ssobjGlobal.TITLE,
            msg: 'You can drop only one file at a time.',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING
        });
        return ssapiStopPropagation(evt);
    }


    // check file size, do not allow more than 5mb
    var file = files[0];
    if (file.size > ssobjGlobal.MAXIMUM_IMAGE_SIZE)
    {
        Ext.MessageBox.show({
            title: ssobjGlobal.TITLE,
            msg: 'The file ' + file.name + ' exceeds the maximum file size of 5MB.',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING
        });
        return ssapiStopPropagation(evt);
    }

    //If customer, check if upload is allowed
    if (nlapiGetContext().getRoleCenter() == "CUSTOMER")
    {
        if (ssapiGetSetting("custscript_ss_allow_customer_upload"))
        {
            var extName = file.name.substr(file.name.lastIndexOf(".")).toLowerCase();
            if (extName == ".zip" || extName == ".rar")
            {
                Ext.MessageBox.show({
                    title: ssobjGlobal.TITLE,
                    msg: "Customers are not allowed to upload zip and archived files.",
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
                return ssapiStopPropagation(evt);
            }
        }
    }

    // check if has pending upload
    if (ssapiHasValue(ssobjGlobal.pendingUploads[target.id]))
    {
        // confirm overwrite uploaded file
        var msg = 'You can attach only one file to each post. Do you want to overwrite the previous file?'.tl();
        Ext.Msg.show({
            title: 'Confirm'.tl(),
            msg: msg,
            buttons: Ext.Msg.YESNOCANCEL,
            fn: function(btn)
            {
                if (btn == 'cancel')
                {
                    return;
                }

                // cancel pending upload
                socialSuiteletProcessAsync('cancelUpload', target.id, function(data)
                {
                    // if return is "ok", then delete file is successful, clear
                    // thumbnail/filename and proceed to upload new file
                    if (data == "ok")
                    {
                        // clear file preview
                        ssapiClearUploadedFilePreview(dndElements);
                        // enable share
                        ssapiEnableShareButton(true, dndElements);

                        // proceed to upload new file
                        ssapiPreProcessFileUpload(file, dndElements);
                    }
                });
            },
            icon: Ext.MessageBox.WARNING,
            buttons: {
                ok: "Yes",
                cancel: "No"
            },
            minWidth: 220
        });
        // HACK: center buttons
        Ext.select('.x-window-br').setStyle({
            'padding-left': '17px'
        });
        return ssapiStopPropagation(evt);
    }

    // proceed to upload
    ssapiPreProcessFileUpload(file, dndElements);
    return ssapiStopPropagation(evt);
}

/**
* Actions to take when a draggable object has entered a dropzone
* 
* @param {Object}
*        e
* @returns {Boolean}
*/
function ssapiDragEnter(e)
{
    var logger = new ssobjLogger(arguments);
    e = e || window.event;
    var target = e.target || e.srcElement;

    ssobjGlobal.dadIsDragEnter = true;

    // highlight drop zone
    Ext.get(target).setStyle('borderColor', '#255599');

    logger.log('end');
    return ssapiStopPropagation(e);
}

/**
* Actions to take when a draggable object has gone out of the dropzone
* 
* @param {Object}
*        e
* @returns {Boolean}
*/
function ssapiDragExit(e)
{
    var logger = new ssobjLogger(arguments);
    e = e || window.event;
    var target = e.target || e.srcElement;

    ssobjGlobal.dadIsDragEnter = false;

    // reset target color
    Ext.get(target).setStyle('borderColor', '#d8d8d8');

    logger.log('end');
    return ssapiStopPropagation(e);
}

/**
* Event handler for other drag-and-drop events. Used for demo purposes.
* 
* @param {Object}
*        evt
*/
function ssapiNoOperationHandler(evt)
{
    return ssapiStopPropagation(evt);
}

/**
* Stops the propagation of an event
* 
* @param {Object}
*        e Event
*/
function ssapiStopPropagation(e)
{
    if (e.preventDefault)
    {
        e.preventDefault();
    }
    if (e.stopPropagation)
    {
        e.stopPropagation();
    }
    return false;
}

/*** Checks if the uploaded file is a supported image or not. If it's a supported* image and it's width is more than 600px, a resized image will be created,* before proceeding to upload. If otherwise, it will just proceed to upload.* * @param {Object}*        file File object* @param {Object}*        dndElements Set of drag-and-drop element IDs*/
function ssapiPreProcessFileUpload(file, dndElements)
{
    // check file extension
    var origFileName = file.name;
    var iExtIndex = origFileName.lastIndexOf(".");
    var extName = origFileName.substr(iExtIndex).toLowerCase();

    if (ssapiIsSupportedImage(extName))
    {
        // pre-process image file
        function render(src)
        {
            var image = new Image();
            image.onload = function()
            {
                var MAX_WIDTH = 600;
                // generate preview image if necessary
                if (image.width > MAX_WIDTH)
                {
                    image.height *= MAX_WIDTH / image.width;
                    image.width = MAX_WIDTH;

                    var canvas = document.getElementById(dndElements.canvasId);
                    var ctx = canvas.getContext("2d");
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    canvas.width = image.width;
                    canvas.height = image.height;
                    ctx.drawImage(image, 0, 0, image.width, image.height);

                    var imgData = encodeURIComponent(canvas.toDataURL("image/jpeg"));
                    ssapiUploadFile(file, imgData, dndElements);
                }
                else {
                    // just draw on the canvas as is
                    var canvas = document.getElementById(dndElements.canvasId);
                    var ctx = canvas.getContext("2d");
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    canvas.width = image.width;
                    canvas.height = image.height;
                    ctx.drawImage(image, 0, 0, image.width, image.height);

                    // just upload original file; no need to generate preview // file
                    ssapiUploadFile(file, "", dndElements);
                }
            };
            image.src = src;
        }

        function loadImage(src)
        {
            // Create our FileReader and run the results through the render
            // function.
            var reader = new FileReader();
            reader.onload = function(e)
            {
                render(e.target.result);
            };
            reader.readAsDataURL(src);
        }

        loadImage(file);
    }
    else {
        // proceed to upload non-image files
        ssapiUploadFile(file, "", dndElements);
    }
}

/**
* Uploads the file(s) to server.
* 
* @param {Object}
*        file File object
* @param {String}
*        fileData Contents of the file (base 64 encoded for binary files)
* @param {Object}
*        dndElements Set of drag-and-drop element IDs
*/
function ssapiUploadFile(file, fileData, dndElements)
{
    var logger = new ssobjLogger(arguments, false);

    // get suitelet url
    if (ssapiHasNoValue(ssobjGlobal.dadUrl))
    {
        ssobjGlobal.dadUrl = nlapiResolveURL('SUITELET', 'customscript_ss_dnd_generic_suitelet', 'customdeploy_ss_dnd_generic_suitelet');
    }

    var xmlRequests = ssobjGlobal.xmlRequests;
    var url = ssobjGlobal.dadUrl;

    // include other info
    var vFD = new FormData();
    vFD.append('file', file);
    vFD.append('fileData', fileData);
    vFD.append('targetId', dndElements.targetId);

    xmlRequests.push(new XMLHttpRequest());

    var uploadId = ssobjGlobal.uploadId;
    logger.log('uploadId=' + uploadId);
    logger.log('xmlRequests.length=' + xmlRequests.length);

    xmlRequests[uploadId].uploadId = uploadId;
    xmlRequests[uploadId].fileName = file.name;
    xmlRequests[uploadId].open('POST', url, true /* async */);
    xmlRequests[uploadId].onreadystatechange = function()
    {

        logger.log('this.readyState=' + this.readyState);
        logger.log('this.status=' + this.status);
        // 'Uploading...';
        if (this.readyState == 4 && this.status != 200)
        {
            // if upload is cancelled or other conditions
            logger.log('ERROR: this.status=' + this.status);
        }

        if (this.readyState == 4 && this.status == 200)
        {
            // upload completed
            var returnValue = ssapiHandleResponse(this);
            logger.log('returnValue=' + returnValue);
            logger.log('returnValue=' + JSON.stringify(returnValue));
            logger.log('this.uploadId=' + this.uploadId);
            delete ssobjGlobal.currentUploadHash[this.uploadId];

            var dadFile = returnValue;
            // check error
            if (ssapiHasValue(dadFile.error))
            {
                uiShowWarning(dadFile.error);
                return;
            }

            // reset and hide progress bar
            ssapiClearUploadProgress(dndElements);

            // show thumbnail/filename of uploaded file
            ssapiShowUploadedFilePreview(dadFile, dndElements);

            // enable share
            ssapiEnableShareButton(true, dndElements);

            // call callback method (i.e. post to newsfeed)
            ssobjGlobal.callbackAfterUpload(dadFile, dndElements);
        }
    };

    // progress bar
    xmlRequests[uploadId].upload.fileName = file.name;
    xmlRequests[uploadId].upload.uploadId = uploadId;
    xmlRequests[uploadId].upload.addEventListener("progress", function(e)
    {
        ssobjGlobal.currentUploadHash[this.uploadId].loaded = e.loaded;
    }, false);

    ssobjGlobal.currentUploadHash[uploadId] = {};
    ssobjGlobal.currentUploadHash[uploadId].fileName = file.name;
    ssobjGlobal.currentUploadHash[uploadId].fileSize = file.size;
    ssobjGlobal.currentUploadHash[uploadId].loaded = 0;
    ssobjGlobal.currentUploadHash[uploadId].startGetTime = null; // (new
    // Date()).getTime();
    ssobjGlobal.currentUploadHash[uploadId].uploadId = uploadId;
    xmlRequests[uploadId].send(vFD);

    // show progress
    if (ssapiHasNoValue(ssobjGlobal.ssapiShowUploadProgress))
    {
        // disable share
        ssapiEnableShareButton(false, dndElements);

        // clear thumbnail/image if any
        ssapiClearUploadedFilePreview(dndElements);

        // reset and show progress bar
        Ext.get(dndElements.progressBarStatusId).update('');
        Ext.get(dndElements.progressBarStatusId).setStyle("color", "black");
        Ext.get(dndElements.progressBarId).setStyle("width", "0%");
        Ext.get(dndElements.progressBarContainerId).setStyle('display', 'block');

        // ensure to pass the appropriate dndElements
        var dndEls = JSON.stringify(dndElements);
        ssobjGlobal.ssapiShowUploadProgress = setInterval("ssapiShowUploadProgress(JSON.parse('" + dndEls + "'));", 500);
    }

    // keep current upload id
    ssobjGlobal.currentUploadId = uploadId;
    // increment upload id for next dnd
    ssobjGlobal.uploadId = uploadId + 1;
}

/**
* Gets the number of files currently uploading. Does not include those in
* waiting.
* 
* @returns {Number}
*/
function ssapiGetUploadingCount()
{
    var hash = ssobjGlobal.currentUploadHash;
    return Object.keys(hash).length;
}

/**
* Checks if currently has an upload in progress.
* 
* @returns {Boolean}
*/
function ssapiHasUploadInProgress()
{
    return (ssapiGetUploadingCount() > 0);
}

/**
* Clears the display of the upload progress.
* 
* @param {Object}
*        dndElements Set of drag-and-drop element IDs
*/
function ssapiClearUploadProgress(dndElements)
{
    // reset and hide progress bar
    if (Ext.get(dndElements.progressBarContainerId))
    {
        Ext.get(dndElements.progressBarContainerId).setStyle('display', 'none');
    }
    if (Ext.get(dndElements.progressBarStatusId))
    {
        Ext.get(dndElements.progressBarStatusId).update('');
        Ext.get(dndElements.progressBarStatusId).setStyle("color", "black");
    }
    if (Ext.get(dndElements.progressBarId))
    {
        Ext.get(dndElements.progressBarId).setStyle("width", "0%");
    }
}

/**
* Shows the upload progress.
* 
* @param {Object}
*        dndElements Set of drag-and-drop element IDs
*/
function ssapiShowUploadProgress(dndElements)
{
    var logger = new ssobjLogger(arguments, true);
    if (ssobjGlobal.dadIsDragEnter)
    {
        logger.log('ssobjGlobal.dadIsDragEnter=' + ssobjGlobal.dadIsDragEnter);
        return;
    }

    if (!ssapiHasUploadInProgress())
    {
        // no more uploads; reset and hide progress bar
        ssapiClearUploadProgress(dndElements);

        clearInterval(ssobjGlobal.ssapiShowUploadProgress);
        delete ssobjGlobal.ssapiShowUploadProgress;
        return;
    }

    // check uploaded percentage
    var currentUploadHash = ssobjGlobal.currentUploadHash;
    for (var p in currentUploadHash)
    {
        var o = currentUploadHash[p];

        // calculate complete percentage
        var completed = Math.round(100 * o.loaded / o.fileSize);
        if (completed >= 99)
            completed = 99;

        // change status color to white if percentage is >= 50%
        if (completed >= 50)
            Ext.get(dndElements.progressBarStatusId).setStyle("color", "white");

        // update status message and percentage
        var width = completed + "%";
        var status = width + " " + o.fileName;
        Ext.get(dndElements.progressBarStatusId).update(status);
        Ext.get(dndElements.progressBarId).setStyle("width", width);
    }
}

/**
* Clears the display of the uploaded file preview.
* 
* @param {Object}
*        dndElements Set of drag-and-drop element IDs
*/
function ssapiClearUploadedFilePreview(dndElements)
{
    // clear thumbnail area
    Ext.get(dndElements.thumbnailContainerId).setStyle('display', 'none');
    Ext.get(dndElements.thumbnailImageId).dom.src = "";

    // clear filename area
    Ext.get(dndElements.filenameContainerId).setStyle('display', 'none');
    Ext.get(dndElements.filenameId).update("");
    Ext.select("#" + dndElements.filenameContainerId + " div.ss_dnd_filename_icon img").setStyle("background", "");

    // clear data
    Ext.get(dndElements.dataId).update("");

    // delete from pending uploads
    if (dndElements.targetId in ssobjGlobal.pendingUploads)
        delete ssobjGlobal.pendingUploads[dndElements.targetId];
}

/**
* Displays a preview of the uploaded file in either thumbnail for supported
* image files, or filename for non-image and non-supported image files.
* 
* @param {Object}
*        dadFile File information of the uploaded file
* @param {Object}
*        dndElements Set of drag-and-drop element IDs
*/
function ssapiShowUploadedFilePreview(dadFile, dndElements)
{
    // check file extension
    var extName = dadFile.extName.toLowerCase();
    if (ssapiIsSupportedImage(extName) && ssapiIsImagePreviewEnabled())
    {
        // process image file
        Ext.get(dndElements.thumbnailImageId).dom.src = Ext.get(dndElements.canvasId).dom.toDataURL("image/jpeg");
        Ext.get(dndElements.thumbnailContainerId).setStyle('display', 'block');
    } else
    {
        // process non-image files
        var bgUrl = ssapiGetFileTypeIcon(extName, ssobjGlobal.imagesUrlsObj['file-type-sprite.png']);
        Ext.select("#" + dndElements.filenameContainerId + " div.ss_dnd_filename_icon img").setStyle("background", bgUrl);
        Ext.get(dndElements.filenameId).update(dadFile.origFileName);
        Ext.get(dndElements.filenameContainerId).setStyle('display', 'block');
    }

    // keep pending uploads to global
    ssobjGlobal.pendingUploads[dndElements.targetId] = dndElements;
}

/**
* Deletes the uploaded file when the user decides not to proceed with sharing
* it.
* 
* @param {String}
*        targetId Element ID of the drag-and-drop target element
*/
function ssapiCancelUpload(targetId)
{
    // confirm cancel upload
    Ext.Msg.show({
        title: ssobjGlobal.TITLE,
        msg: 'Do you want to cancel this upload?'.tl(),
        buttons: Ext.Msg.YESNOCANCEL,
        fn: function(btn)
        {
            if (btn == 'cancel')
            {
                return;
            }

            // cancel upload
            socialSuiteletProcessAsync('cancelUpload', targetId, function(data)
            {
                // if return is "ok", then delete file is successful, clear
                // thumbnail/filename
                if (data == "ok")
                {
                    // get dnd elements
                    var dndElements = ssobjGlobal.dndElements[targetId];

                    ssapiClearUploadedFilePreview(dndElements);

                    // enable share
                    ssapiEnableShareButton(true, dndElements);
                }
            });
        },
        icon: Ext.MessageBox.INFO,
        buttons: {
            ok: "Yes",
            cancel: "No"
        }
    });
}

/**
* Aborts the currently running upload progress.
* 
* @param {Object}
*        evt Event
* @param {String}
*        targetId Element ID of the drag-and-drop target element
*/
function ssapiAbortUpload(evt, targetId)
{
    // check if upload is in progress, then abort upload
    var uploadId = ssobjGlobal.currentUploadId;
    if (ssapiHasValue(ssobjGlobal.currentUploadHash[uploadId]))
    {
        try
        {
            // abort upload
            ssobjGlobal.xmlRequests[uploadId].abort();
            delete ssobjGlobal.currentUploadHash[uploadId];
        } catch (ex)
        {
        }

        // get dnd elements
        var dndElements = ssobjGlobal.dndElements[targetId];

        // clear thumbnail/preview
        ssapiClearUploadedFilePreview(dndElements);

        // enable share
        ssapiEnableShareButton(true, dndElements);
    }

    evt = evt || window.event;
    if (ssapiHasValue(evt))
    {
        ssapiStopPropagation(evt);
    }
}

/**
* Enables/Disables the Share/Send/OK button.
* 
* @param {Boolean}
*        bEnable Set to true to enable, otherwise, set to false
* @param {Object}
*        dndElements Set of drag-and-drop element IDs
*/
function ssapiEnableShareButton(bEnable, dndElements)
{
    // check if buttonId is provided
    if (ssapiHasNoValue(dndElements.buttonId))
        return;

    if (bEnable)
        Ext.get(dndElements.buttonId).removeClass("disabled");
    else
        Ext.get(dndElements.buttonId).addClass("disabled");
}

/**
* Adds a event handler for beforeunload event. The handler prompts a warning
* when leaving page while files are being uploaded.
*/
function ssapiAddBeforeUnloadEventHandler()
{
    // event to trigger before window unloads
    window.addEventListener('beforeunload', function(e)
    {
        try
        {
            // check if has upload in progress
            if (ssapiHasUploadInProgress())
            {
                // display prompt to whether to stay or navigate away from page
                // to user
                // When a non-empty string is assigned to the returnValue Event
                // property, a dialog box appears,
                // asking the users for confirmation to leave the page.
                // When no value is provided, the event is processed silently.

                // from:
                // https://developer.mozilla.org/en-US/docs/Web/Reference/Events/beforeunload
                // For some reasons, Webkit-based browsers don't follow the spec
                // for the dialog box. An almost cross-working example would be
                // close from the below example.
                var confirmationMessage = "Leaving the page will cancel your current file upload progress.";
                e = e || window.event;
                e.returnValue = confirmationMessage; // Gecko + IE
                if (Ext.isChrome)
                {
                    return confirmationMessage; // Webkit, Safari, Chrome etc.
                }
            }

            // get number of pending uploads (already uploaded but haven't
            // posted)
            var pendingUploadsCount = Object.keys(ssobjGlobal.pendingUploads).length;
            if (pendingUploadsCount > 0)
            {
                var confirmationMessage = "You have attached a file to this post. Leaving the page will delete the uploaded file.";
                e = e || window.event;
                e.returnValue = confirmationMessage; // Gecko + IE
                if (Ext.isChrome)
                {
                    return confirmationMessage; // Webkit, Safari, Chrome etc.
                }
            }

            // check if any text editor is "dirty"
            for (var key in ssobjGlobal.dndElements)
            {
                // check editor text
                var msg = socialGetRichEditorText(key);
                if (msg.trim() != '')
                {
                    var confirmationMessage = "Your message has not been posted.";
                    e = e || window.event;
                    e.returnValue = confirmationMessage; // Gecko + IE
                    if (Ext.isChrome)
                    {
                        return confirmationMessage; // Webkit, Safari, Chrome
                        // etc.
                    }
                    break;
                }
            }
        } catch (e)
        {
            // ignore error since we do not know what happens when an error
            // occurs on this event
        }
    }, false);

    // event to trigger when window unloads
    window.addEventListener('unload', function(e)
    {
        // loop through pending uploads; cancel each
        for (var key in ssobjGlobal.pendingUploads)
        {

            // form data to send
            var data = {};
            data.action = 'cancelUpload';
            data.values = key;

            // get generic suitelet url
            var url = "";
            if (ssapiHasNoValue(ssobjGlobal.socialSuiteletProcessAsyncUrl))
            {
                url = nlapiResolveURL('SUITELET', 'customscript_suitewall_generic_suitelet', 'customdeploy_suitewall_generic_suitelet');
                ssobjGlobal.socialSuiteletProcessAsyncUrl = url;
            } else
            {
                url = ssobjGlobal.socialSuiteletProcessAsyncUrl;
            }
            var xhr = new XMLHttpRequest();
            // Need to send synchronously to have the best chance of data
            // getting through to the server
            xhr.open('POST', url, false);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send(JSON.stringify(data));

            // delete from global
            delete ssobjGlobal.pendingUploads[key];
        }
    }, false);
}
