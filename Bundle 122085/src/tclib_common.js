/**
 * @fileOverview Common library used by client and server
 * @author tcaguioa
 */
// 
/**
 * @namespace Global object for storing global variables
 */
var ssobjGlobal = ssobjGlobal || {};
ssobjGlobal.TITLE = ssobjGlobal.TITLE || 'SuiteSocial';
ssobjGlobal.SUPPORTED_IMAGES = [ ".png", ".jpg", ".jpeg", ".PNG", ".JPG", ".JPEG" ];
ssobjGlobal.LABEL_LIKE = 'Like';
ssobjGlobal.LABEL_UNLIKE = 'Unlike';
ssobjGlobal.SYSTEM_ID = '-4';
ssobjGlobal.ACCOUNT_OWNER_ID = '-5';
ssobjGlobal.CENTER_TYPES = {
    ACCOUNTING_CENTER : 'ACCOUNTCENTER',
    CLASSIC_CENTER : 'BASIC',
    CUSTOMER_CENTER : 'CUSTOMER',
    ECOMMERCE_MGMT_CENTER : 'STOREMANAGER',
    EMPLOYEE_CENTER : 'EMPLOYEE',
    ENGINEERING_CENTER : 'ENGINEERCENTER',
    EXECUTIVE_CENTER : 'EXECUTIVE',
    MARKETING_CENTER : 'MARKETCENTER',
    PARTNER_CENTER : 'PARTNER',
    PROJECT_CENTER : 'PROJECTCENTER',
    SUPPORT_CENTER : 'SUPPORTCENTER',
    SALES_CENTER : 'SALESCENTER',
    SHIPPING_CENTER : 'SHIPPINGCENTER',
    SYSTEM_ADMIN_CENTER : 'SYSADMINCENTER',
    VENDOR_CENTER : 'VENDOR'
};

/**
 * A wrapper object of the JSON format of a search result so that getValue() and
 * getText will still work
 * 
 * @param {object}
 *        pObjResult JSON format of a search result
 */
function ssobjResultWrapper(pObjResult) {
    var logger = new ssobjLogger(arguments);
    var objResult = pObjResult;
    // logger.error('objResult=' + JSON.stringify(objResult));
    // logger.error('objResult.columns=' + objResult.columns);
    // logger.error('objResult=' + objResult.columns['internalid'].internalid);
    this.getValue = function(columnScriptId) {
        // logger.error('getValue columnScriptId=' + columnScriptId);
        if (ssapiHasNoValue(objResult.columns[columnScriptId])) {
            return '';
        }
        // if (columnScriptId == 'internalid') {
        // return objResult.internalid;
        // }
        return objResult.columns[columnScriptId].internalid || objResult.columns[columnScriptId];
    };
    this.getText = function(columnScriptId) {
        if (ssapiHasNoValue(objResult.columns[columnScriptId])) {
            return '';
        }
        // logger.error('getText columnScriptId=' + columnScriptId);
        return objResult.columns[columnScriptId].name;
    };
    this.getValues = function(columnScriptId) {
        if (ssapiHasNoValue(objResult.columns[columnScriptId])) {
            return null;
        }
        var result = objResult.columns[columnScriptId];
        var list = objResult.columns[columnScriptId];
        if (!(result instanceof Array)) {
            // only one value selected
            list = [result];
        }
        var values = [];
        for (var i = 0; i < list.length; i++) {
            values.push(list[i].internalid);
        }
        return values;
    };
    this.getTexts = function(columnScriptId) {
        if (ssapiHasNoValue(objResult.columns[columnScriptId])) {
            return null;
        }
        var list = objResult.columns[columnScriptId];
        var result = objResult.columns[columnScriptId];
        if (!(result instanceof Array)) {
            list = [result];
        }
        var texts = [];
        for (var i = 0; i < list.length; i++) {
            texts.push(list[i].name);
        }
        return texts;
    };
}

/**
 * returns true if using the new ui
 * 
 * @returns {Boolean}
 */
function ssapiIsNewUI() {
    return true;
}

/**
 * Returns the "_oldui" suffix if old ui
 * 
 * @returns
 */
function ssapiAddSuffixIfOldUI() {
    return '';
}
// 
/**
 * @description System field script ids
 */
var SYSTEM_FIELDS = [ 'nsapiCT', 'nameorig', 'owner', 'entryformquerystring', '_eml_nkey_', '_multibtnstate_', 'externalid', 'wfSR', 'customwhence', 'wfVF', 'ownerid', 'nsapiSR', 'nsapiFC', 'isinactive', 'customform', 'templatestored', 'selectedtab', 'baserecordtype', 'nsapiRC', 'nsapiPS', 'nldept', 'nlrole', 'nsapiLI', 'nsapiVL', 'wfinstances', 'nsapiVI', 'nsapiVF', 'nsapiVD', 'wfPI', 'whence', 'nsbrowserenv', 'linenumber', 'type', 'wfPS', 'nsapiPI', 'nluser', 'rectype', 'nlsub', 'wfFC',
        'nsapiLC', 'nlloc' ];

ssobjGlobal.LANGUAGES = [ {
    name : 'Chinese (Simplifed)',
    code : 'zh_CN',
    id : 1
}, {
    name : 'Chinese (Traditional)',
    code : 'zh_TW',
    id : 2
}, {
    name : 'Czech',
    code : 'cs_CZ',
    id : 3
}, {
    name : 'Danish',
    code : 'da_DK',
    id : 4
}, {
    name : 'Dutch',
    code : 'nl_NL',
    id : 5
}, {
    name : 'English (International)',
    code : 'en',
    id : 6
}, {
    name : 'English (US)',
    code : 'en_US',
    id : 7
}, {
    name : 'French',
    code : 'fr_FR',
    id : 8
}, {
    name : 'French (Canada)',
    code : 'fr_CA',
    id : 9
}, {
    name : 'German',
    code : 'de_DE',
    id : 10
}, {
    name : 'Italian',
    code : 'it_IT',
    id : 11
}, {
    name : 'Japanese',
    code : 'ja_JP',
    id : 12
}, {
    name : 'Korean',
    code : 'ko_KR',
    id : 13
}, {
    name : 'Portuguese (Brazil)',
    code : 'pt_BR',
    id : 14
}, {
    name : 'Pseudo Translation',
    code : 'xx_US',
    id : 15
}, {
    name : 'Russian',
    code : 'ru_RU',
    id : 16
}, {
    name : 'Spanish (Latin America',
    code : 'es_AR',
    id : 17
}, {
    name : 'Spanish (Spain)',
    code : 'es_ES',
    id : 18
}, {
    name : 'Swedish',
    code : 'sv_SE',
    id : 19
}, {
    name : 'Thai',
    code : 'th_TH',
    id : 20
} ];

/**
 * Returns the Title Case of a string
 */
String.prototype.toTitleCase = function() {
    return this.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};





/**
 * Returns a more user-friendly error message to be displayed to user
 * 
 * @param {Object}
 *        errorMessage
 */
function ssapiGetFriendlyErrorMessage(errorMessage)
{
    if (errorMessage == null)
    {
        return "Unknown error";
    }
    
    if (errorMessage.indexOf('Please provide more detailed keywords so your search does not return too many results') > -1)
    {
        return 'Please provide more detailed keywords so your search does not return too many results';
    }
    
    if (errorMessage.indexOf('Could not determine customer compid') > -1)
    {
        return 'Your session has timed-out. You need to login again.';
    }
    
    // return the same error message
    return errorMessage;
}






function ssapiIsNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Returns an object in an array given a field and its desired value
 * 
 * @param {Object}
 *        jsonArray Array of objects
 * @param {Object}
 *        fieldName
 * @param {Object}
 *        fieldValue
 */
function ssapiGetObjectByFieldValue(objectArray, fieldName, fieldValue) {
    for (var i = 0; i < objectArray.length; i++) {
        var o = objectArray[i];
        if (o[fieldName] == fieldValue) {
            return o;
        }
    }
    return null;
}

/**
 * Sorts an array of JSON objects
 * 
 * @param {Object}
 *        objArray
 * @param {Object}
 *        prop Name of the property
 * @param {Object}
 *        direction Defaults to ascending if no value is supplied
 * @return {Array} Sorted JSON array
 */
// function ssapiSortJsonArrayByProperty(objArray, prop, direction) {
// if (arguments.length < 2)
// throw new Error("sortJsonArrayByProp requires 2 arguments");
// var direct = arguments.length > 2 ? arguments[2] : 1; // Default to
// // ascending
// if (objArray && objArray.constructor === Array) {
// var propPath = (prop.constructor === Array) ? prop : prop.split(".");
// objArray.sort(function(a, b) {
// for ( var p in propPath) {
// if (a[propPath[p]] && b[propPath[p]]) {
// a = a[propPath[p]];
// b = b[propPath[p]];
// }
// }
// // convert numeric strings to integers
// a = a.match(/^\d+$/) ? +a : a;
// b = b.match(/^\d+$/) ? +b : b;
// return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
// });
// }
// }
/**
 * Sorts an array of JSON objects. Alternate implementation of
 * ssapiSortJsonArrayByProperty()
 * 
 * @param {Object}
 *        objArray
 * @param {Object}
 *        prop Name of the property
 * @param {Object}
 *        direction Defaults to ascending if no value is supplied
 * @return {Array} Sorted JSON array
 */
function ssapiSortJsonArrayByProperty2(objArray, prop) {
    var keys = [];
    var hash = {};
    for (var i = 0; i < objArray.length; i++) {
        var item = objArray[i];
        var key = item[prop];
        keys.push(key);
        hash[key] = item;
    }
    keys.sort();
    var newArray = [];
    for (i = 0; i < keys.length; i++) {
        newArray.push(hash[keys[i]]);
    }
    return newArray;
}

/**
 * Makes a word plural
 * 
 * @param {Object}
 *        singular
 */
function ssapiPluralize(singular) {
    var lastChar = singular.substr(singular.length - 1, 1);
    switch (lastChar) {
    case 'y':
        return singular.substr(0, singular.length - 1) + 'ies';
    case 's':
        return singular + 'es';
    default:
        return singular + 's';
    }
}

/**
 * Returns true if the current user has access to the record
 * 
 * @param {Object}
 *        scriptId Script id of the record such as employee, supportcase
 * @return {boolean}
 */
function ssapiHasRecordAccess(scriptId) {
    try {
        nlapiSearchRecord(scriptId, null, null, [ new nlobjSearchColumn('internalid', null, 'count') ]);
        return true;
    } catch (e) {
        // throws error if current user has no access
        return false;
    }

}

/**
 * Returns the profile id of an employee
 * 
 * @param {String}
 *        employeeid
 * @return {String}
 */
function ssapiGetEmployeeProfileId(employeeId) {
    try {
        // Get the profile id for the current user
        var filters = [];
        filters[0] = new nlobjSearchFilter("custrecord_suitesocial_profile_emp", null, "anyof", employeeId);
        var searchresults = nlapiSearchRecord('customrecord_suitesocial_profile', null, filters, null);
        var id = null;
        if (searchresults !== null && searchresults.length > 0) {
            id = searchresults[0].getId();
        } else {
            // create
            var r = nlapiCreateRecord('customrecord_suitesocial_profile');
            r.setFieldValue('custrecord_suitesocial_profile_emp', employeeId);
            r.setFieldValue('name', r.getFieldText('custrecord_suitesocial_profile_emp'));
            r.setFieldValue('custrecord_yammer_integration_enabled', 'T');
            id = nlapiSubmitRecord(r);
        }
        return id;
    } catch (e) // possibly the emp is not of type employee
    {
        ssapiHandleError(e);
        return null; // return null if an error occured while retrieving the
        // profile
    }
}

/**
 * Returns an array of ssobjSearchColumn for all fields of the record type
 * passed
 * 
 * @param {Array}
 *        fieldsWithStoreValueTrue
 * @return {Array}
 */
function ssapiCreateSearchColumnsForAllFields(fieldsWithStoreValueTrue) {
    var logger = new ssobjLogger(arguments);
    // var record = nlapiCreateRecord(recordScriptId);
    // var fieldScriptIds = record.getAllFields();
    var columns = [];
    for (var i = 0; i < fieldsWithStoreValueTrue.length; i++) {
        var fieldScriptId = fieldsWithStoreValueTrue[i];
        columns.push(new ssobjSearchColumn(fieldScriptId));
    }
    logger.log('DONE');
    return columns;
}

/**
 * Returns all nlobjSearchResult of a record search even if they are more than
 * 1000
 * 
 * @param {Object}
 *        type
 * @param {Object}
 *        id
 * @param {Object}
 *        filters
 * @param {Object}
 *        columns
 * @return {Array}
 */
function ssapiSearchAllRecords(type, id, filters, columns)
{
    var logger = new ssobjLogger(arguments);
    var allResults = [];
    // set to lowest employee id value
    var lowerId = -100;
    // add support for 'select all columns' if no columns is supplied
    if (ssapiHasNoValue(columns) || typeof columns == 'string')
    {
        // get all columns
        var allColumns = nlapiCreateRecord(type).getAllFields();
        // include only custom fields
        columns = [];
        for (var i = 0; i < allColumns.length; i++)
        {
            var columnId = allColumns[i];
            // exclude fields whose stored in value is F
            // TODO: check by code this condition
            var excludeNotStoredColumns = ['custrecord_suitesocial_autosub_type_ft', 'custrecord_suitesocial_autopost_fld_ft'];
            if (excludeNotStoredColumns.indexOf(columnId) > -1)
            {
                continue;
            }
            if (columnId.substr(0, 10) == 'custrecord' || columnId == 'name' || columnId == FLD_INTERNAL_ID)
            {
                columns.push(new nlobjSearchColumn(columnId));
            }
        }
    }

    columns.push(new nlobjSearchColumn('internalid'));
    columns[columns.length - 1].setSort(); // sort ascending
    // add filter used in paging
    if (filters === null)
    {
        filters = [];
    }
    filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lowerId));
    var searchCount = 1;
    logger.log('searchCount=' + searchCount);
    var results = nlapiSearchRecord(type, id, filters, columns);
    while (results !== null)
    {
        for (var i = 0; i < results.length; i++)
        {
            allResults.push(results[i]);
        }
        lowerId = results[results.length - 1].getValue('internalid');
        filters[filters.length - 1] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lowerId);
        searchCount = searchCount + 1;
        logger.log('searchCount=' + searchCount);
        results = nlapiSearchRecord(type, id, filters, columns);
    }
    if (allResults.length === 0)
    {
        // consistent with nlapiSearchRecord() where it returns null if no rows
        // passed
        return null;
    }
    return allResults;
}




/**
 * Alias of ssapiSearchAllRecords
 * 
 * @param {Object}
 *        type
 * @param {Object}
 *        id
 * @param {Object}
 *        filters
 * @param {Object}
 *        columns
 */
function ssapiSearchAllRecord(type, id, filters, columns) {
    return ssapiSearchAllRecords(type, id, filters, columns);
}

function ssapiSearchRecordAll(type, id, filters, columns) {
    return ssapiSearchAllRecords(type, id, filters, columns);
}

/**
 * Returns all nlobjSearchResult of a record search even if they are more than
 * 1000; this one is using the filter expression syntax
 * 
 * @param {Object}
 *        type
 * @param {Object}
 *        id
 * @param {Object}
 *        filters
 * @param {Object}
 *        columns
 * @return {Any[]} Array of nlobjSearchResult
 */
function ssapiSearchAllRecords2(type, id, filters, columns)
{
    var allResults = [];

    // set to lowest employee id value
    var lowerId = -100;
    if (ssapiHasNoValue(columns))
    {
        columns = [];
    }

    columns.push(new nlobjSearchColumn('internalid'));
    columns[columns.length - 1].setSort(); // sort ascending
    // add filter used in paging
    if (ssapiHasNoValue(filters))
    {
        filters = [];
    }
    if (filters.length > 0)
    {
        filters.push('and');
    }
    filters.push(['internalidnumber', 'greaterthan', lowerId]);

    var results = nlapiSearchRecord(type, id, filters, columns);
    while (results !== null)
    {
        for (var i = 0; i < results.length; i++)
        {
            allResults.push(results[i]);
        }
        lowerId = results[results.length - 1].getValue('internalid');
        filters[filters.length - 1] = ['internalidnumber', 'greaterthan', lowerId];
        results = nlapiSearchRecord(type, id, filters, columns);
    }
    if (allResults.length === 0)
    {
        // consistent with nlapiSearchRecord() where it returns null if no rows
        // passed
        return null;
    }
    return allResults;
}





/**
 * Shortcut for getting a value of a column given a simple filter
 * 
 * @param {string}
 *        recordTypeId The script id of the record type
 * @param {string}
 *        returnColumnId The column id of the value to be returned
 * @param {string}
 *        filterColumnId The column id where the filtering will be performed
 * @param {string}
 *        operator The operator
 * @param {object}
 *        filterValue The value to be compared against values in filterColumnId
 * @return {object} Value from returnColumnId
 */
function ssapiGetFieldValue(recordTypeId, returnColumnId, filterColumnId, operator, filterValue) {
    var logger = new ssobjLogger(arguments);
    var columns = [ new nlobjSearchColumn(returnColumnId) ];
    var filters = [ new nlobjSearchFilter(filterColumnId, null, operator, filterValue) ];
    var results = nlapiSearchRecord(recordTypeId, null, filters, columns);

    if (ssapiHasNoValue(results)) {
        logger.log('DONE null');
        return null;
    } else {
        logger.log('DONE ' + results[0].getValue(returnColumnId));
        return results[0].getValue(returnColumnId);
    }
}

/**
 * Shortcut for checking if a row exists given a simple filter
 * 
 * @param {string}
 *        recordTypeId The script id of the record type
 * @param {Object}
 *        filterColumnId The script id of the column where the filtering will be
 *        performed.
 * @param {Object}
 *        operator
 * @param {Object}
 *        filterValue
 */
function ssapiRowExists(recordTypeId, filterColumnId, operator, filterValue) {
    var logger = new ssobjLogger(arguments);
    var columns = [ new nlobjSearchColumn(filterColumnId) ];
    var filters = [ new nlobjSearchFilter(filterColumnId, null, operator, filterValue) ];
    var results = nlapiSearchRecord(recordTypeId, null, filters, columns);
    logger.log('DONE');
    return ssapiHasValue(results);
}

/**
 * Shortcut for getting the id of a record type given a simple filter
 * 
 * @param {Object}
 *        recordTypeId
 * @param {Object}
 *        filterColumnId
 * @param {Object}
 *        filterOperator
 * @param {Object}
 *        filterValue
 * @return {Object} Internal id of the record
 */
function getRecordId(recordTypeId, filterColumnId, filterOperator, filterValue) {
    var logger = new ssobjLogger('getRecordId', true);
    return ssapiGetFieldValue(recordTypeId, FLD_INTERNAL_ID, filterColumnId, filterOperator, filterValue);
}

/**
 * Shortcut for getting the id of a record type given a simple filter
 * 
 * @param {Object}
 *        recordTypeId
 * @param {Object}
 *        filterColumnId
 * @param {Object}
 *        filterOperator
 * @param {Object}
 *        filterValue
 * @return {Object} Internal id of the record
 */
function ssapiGetRecordId(recordTypeId, filterColumnId, filterOperator, filterValue) {
    return getRecordId(recordTypeId, filterColumnId, filterOperator, filterValue);
}

/**
 * Similar to nlapiCreateRecord but with additional debugging and additional
 * error handling
 * 
 * @param {string}
 *        recordScriptId
 * @return {nlobjRecord}
 */
function ssapiCreateRecord(recordScriptId, initializeValues) {
    try {
        if (ssapiHasNoValue(recordScriptId)) {
            throw ssapiHasNoValue(recordScriptId);
        }
        var record = nlapiCreateRecord(recordScriptId, initializeValues);
        return record;
    } catch (e) {
        // Issue: 218383 [SuiteSocial] Exception thrown: The feature 'Issue
        // Management
        try {
            ssapiGetErrorDetails(e);
        } catch (e) {
            // suppress
        }

        return null;
    }
}

/**
 * Obtains details about the search column that can be used for debugging
 * purposes
 * 
 * @param {nlobjSearchColumn}
 *        searchColumn
 * @return {string}
 */
// function ssapiGetSearchColumnDetails(searchColumn) {
// var details = '';
// if (ssapiHasNoValue(searchColumn)) {
// throw 'ssapiHasNoValue(searchColumn)';
// }
// details += 'name=' + searchColumn.getName() + '; ';
// details += 'join=' + searchColumn.getJoin() + '; ';
// details += 'sort=' + searchColumn.getSort() + '; ';
// details += 'function=' + searchColumn.getFunction() + '; ';
// details += 'formula=' + searchColumn.getFormula() + '; ';
// details += 'summary=' + searchColumn.getSummary() + '; ';
// details += 'label=' + searchColumn.getLabel() + '; ';
// return details;
// }
// /*
// * @param {nlobjSearchFilter} searchFilter
// * @return {string} Obtains details about the search filter that can be used
// for debugging purposes
// */
// function ssapiGetSearchFilterDetails(searchFilter){
// var details = '';
// if (ssapiHasNoValue(searchFilter)) {
// throw 'ssapiHasNoValue(searchFilter)';
// }
// details += 'name=' + searchColumn.getName() + '; ';
// details += 'join=' + searchColumn.getJoin() + '; ';
// details += 'operator=' + searchColumn.getOperator()() + '; ';
// details += 'formula=' + searchColumn.getFormula() + '; ';
// details += 'summary type=' + searchColumn.getSummaryType() + '; ';
// // details += 'label=' + searchColumn.getLabel() + '; ';
// return details;
// }
/**
 * Creates a span markup given the text and color
 * 
 * @param {Object}
 *        str The text/innerHTML of the span
 * @param {Object}
 *        color the font color
 */
function ssapiColor(str, color) {
    return '<span style="color: ' + color + '">' + str + '</span>';
}

/**
 * Shortcut function for making a html markup bold
 * 
 * @param {Object}
 *        markup The markup that will be surrounded with bold tags
 * @return {string} Markup surrounded with bold tags
 */
function ssapiBold(markup, color) {
    return '<b>' + markup + '</b>';
}

/**
 * Creates a span with green font
 * 
 * @param {Object}
 *        str
 */
function ssapiGreen(str) {
    return ssapiColor(str, 'green');
}

/**
 * Returns a string representation of an object. This is used in debugging.
 * 
 * @param {Object}
 *        obj
 */
function ssapiGetObjectDetailViaJSON(obj) {
    return ssapiGetObjectDetail(obj);
}

/**
 * Returns information about the object.
 * 
 * @param {Object}
 *        obj
 */
function ssapiGetObjectDetail(obj)
{
    if (obj == null)
    {
        return null;
    }
    
    
    // Calling JSON.stringify on some nl objects like nlobjAssistant, nlobjResponse,nlobjRequest and nlobjSelectOptions throws an error even when inside a try catch statement
    var objS = obj.toString();
    if (objS == null)
    {
        return null;
    }
    
    if (['nlobjAssistant'].indexOf(objS) > -1)
    {
        return objS;
    }

    if (objS.indexOf('nlobjResponse') > -1)
    {
        return objS;
    }
    if (objS.indexOf('nlobjRequest') > -1)
    {
        return objS;
    }
    if (objS.indexOf('nlobjForm') > -1)
    {
        return objS;
    }

    if (obj instanceof Array)
    {
        if (obj[0] && obj[0].getId && obj[0].getText)
        {
            var detail = 'nlobjSelectOptions[' + obj.length + ']';
            if (obj.length <= 10)
            {
                // show items if they are not more than 10
                detail += '[';
                var count = obj.length;
                for (var i = 0; i < count; i++)
                {
                    var option = obj[0];
                    detail += '{id: ' + option.getId() + ', text:' + option.getText() + '}';
                }
                detail += ']';
            }
            return detail;
        }
    }

    // Calling JSON.stringify on nlobjSelectOption throws an error
    if (obj && obj.getId && obj.getText)
    {
        var detail = 'nlobjSelectOption';
        detail += '{id: ' + obj.getId() + ', text:' + obj.getText() + '}';
        return detail;
    }

    // for other objects
    try
    {
        detail = JSON.stringify(obj);
    } catch (e)
    {
        return null;
    }
    return detail;
}





/**
 * Obtains debugging information from the built in object arguments
 * 
 * @param {object}
 *        args Built-in arguments object from a function
 * @return {string} The details of a function's arguments
 */
function ssapiGetArgumentDetails(args) {
    var details = '';
    try
    {
        // get the function name and parameters
        var fullFunc = args.callee.toString();
        if (fullFunc == null)
        {
            return "";
        }
        
        var funcAndArgs = fullFunc.substr(0, fullFunc.indexOf('{')).replace('function ', '');
        // get array of argument name
        var paramsStr = funcAndArgs.replace(args.callee.name, '').replace(')', '').replace('(', '');
        var params = paramsStr.split(',');
        details = ssapiGetNewLine(1) + 'Function=' + funcAndArgs.replace('\n', ' ');
        if (args.length > 0) {
            details += ssapiGetNewLine() + 'ARGUMENTS:' + ssapiGetNewLine(1);
            for (var i = 0; i < args.length; i++) {
                var paramName = 'arg' + i;
                if (ssapiHasValue(params[i])) {
                    paramName = params[i].trim();
                }
                var arg = '';
                if (typeof args[i] == 'object') {
                    // is it an array
                    if (args[i] instanceof Array) {
                        for (var x = 0; x < args[i].length; x++) {
                            // // arg += args[i][x];
                            // // arg += JSON.stringify((args[i][x]));
                            // var obj = (args[i])[x];
                            // if (obj instanceof nlobjSearchFilter) {
                            // arg += JSON.stringify(args[i]) ;
                            arg += ssapiGetObjectDetail(args[i][x]);
                        }
                    }

                    else {
                        try {
                            // arg += JSON.stringify(args[i]);
                            arg += args[i];
                        } catch (e) {
                            arg += args[i];
                        }

                        // nlapiLogExecution('debug', 'haller', '8 ' + i);
                    }
                } else {
                    arg = args[i];
                }
                details += paramName + '=' + arg + ';     ';
            }
        }
    }
    catch (e)
    {
        return '';
    }
    
    return details;
}





/**
 * @class Usage is similar to nlobjSearchColumn but with additional
 *        functionality for collecting debugging info
 * @param {Object}
 *        name
 * @param {Object}
 *        join
 * @param {Object}
 *        summary
 */
function ssobjSearchColumn(name, join, summary) {
    var logger = new ssobjLogger(arguments, true);
    var info = 'name=' + name;
    if (ssapiHasValue(join)) {
        info += '; join=' + join;
    }
    if (ssapiHasValue(summary)) {
        info += '; summary=' + summary;
    }
    var netSuiteColumn = new nlobjSearchColumn(name, join, summary);

    /**
     * @public
     * @description Returns the underlying nlobjSearchColumn
     * 
     */
    this.getNetSuiteColumn = function() {
        return netSuiteColumn;
    };

    /**
     * @public
     * @description String representation of the instance
     */
    this.toString = function() {
        return info;
    };
}

/**
 * @class Usage is similar to nlobjSearchFilter but with additional
 *        functionality for collecting debugging info
 * @param {Object}
 *        name
 * @param {Object}
 *        join
 * @param {Object}
 *        operator
 * @param {Object}
 *        value1
 * @param {Object}
 *        value2
 * @return {nlobjSearchFilter}
 */
function ssobjSearchFilter(name, join, operator, value1, value2) {
    var logger = new ssobjLogger(arguments);
    // this.name = name;
    // this.join = join;
    // this.operator = operator;
    // this.value1 = value1;
    // this.value2 = value2;

    var info = 'name=' + name;
    if (ssapiHasValue(join)) {
        info += '; join=' + join;
    }
    if (ssapiHasValue(operator)) {
        info += '; operator=' + operator;
    }
    if (ssapiHasValue(value1)) {
        info += '; value1=' + value1;
    }
    if (ssapiHasValue(value2)) {
        info += '; value2=' + value2;
    }
    // this.ArgumentsDetails = ssapiGetArgumentDetails(arguments);
    var netSuiteFilter = new nlobjSearchFilter(name, join, operator, value1, value2);

    /**
     * @public
     * @description Returns the underlying nlobjSearchFilter
     * @returns {nlobjSearchFilter}
     */
    this.getNetSuiteFilter = function() {
        return netSuiteFilter;
    };

    /**
     * @public
     * @description Calls the setFormula() of the underlying nlobjSearchFilter
     * @param {Object}
     *        formula
     */
    this.setFormula = function(formula) {
        return netSuiteFilter.setFormula(formula);
    };

    /**
     * @public
     * @description Calls the setSummaryType() of the underlying
     *              nlobjSearchFilter
     * @param {Object}
     *        formula
     */
    this.setSummaryType = function(type) {
        return netSuiteFilter.setSummaryType(type);
    };

    /**
     * @public
     * @description Returns the string representation of the filter
     * @param {object}
     *        formula
     * @returns {string}
     */
    this.toString = function() {
        // logger.ok('info=' + info);
        return info;
    };
}

/**
 * Returns the value of the 1st row of the 1st search column;<br />
 * Returns null if searchResults is null or empty
 * 
 * @param {Array}
 *        nlobjSearchResults
 * @param {Object}
 *        nlSearchColumns
 * @return {Object}
 */
// function ssapiGetScalar(searchResults, nlSearchColumns) {
// var logger = new ssobjLogger(arguments);
// if (ssapiHasNoValue(nlSearchColumns)) {
// throw 'ssapiHasNoValue(nlSearchColumns)';
// }
// if (nlSearchColumns.length === 0) {
// throw 'nlSearchColumns.length === 0';
// }
// if (searchResults === null) {
// return null;
// }
// if (searchResults.length === 0) {
// return null;
// }
// logger.log('DONE');
// return searchResults[0].getValue(nlSearchColumns[0]);
// }
/**
 * Returns an HTML representation of the search results
 * 
 * @param {Array}
 *        nlobjSearchResults
 * @param {Object}
 *        nlSearchColumns
 * @return {String} HTML
 */
// function ssapiDisplaySearchResults(searchResults, nlSearchColumns) {
// var logger = new ssobjLogger(arguments, true);
// if (ssapiHasNoValue(nlSearchColumns)) {
// throw 'ssapiHasNoValue(nlSearchColumns)';
// }
// if (nlSearchColumns.length === 0) {
// throw 'nlSearchColumns.length === 0';
// }
// if (searchResults === null) {
// return 'searchResults === null';
// }
// if (searchResults.length === 0) {
// return 'searchResults === 0';
// }
//
// logger.log('nlSearchColumns.length=' + nlSearchColumns.length);
// var rows = [];
// for (var i = 0; i < searchResults.length; i++) {
// var result = searchResults[i];
// var row = {};
// for (var col = 0; col < nlSearchColumns.length; col++) {
// var column = nlSearchColumns[col];
// // logger.log('column.getLabel()='+column.getLabel());
// row[column.getName().replace('custrecord_', '')] = result.getValue(column);
// if (result.getText(column) !== null && result.getText(column) !=
// result.getValue(column)) {
// row[column.getName().replace('custrecord_', '') + 'TEXT'] =
// result.getText(column);
// }
// }
// rows.push(row);
// }
// logger.log('DONE');
// return ssapiCreateTableHtml(rows);
// }
/**
 * Converts an array of ssobjSearchColumns to nlobjSearchColumns
 * 
 * @param {Array}
 *        ssobjSearchColumns
 */
function ssapiGetNetSuiteSearchColumns(tcSearchColumns) {
    var nlSearchColumns = [];
    for (var i = 0; i < tcSearchColumns.length; i++) {
        nlSearchColumns.push(tcSearchColumns[i].getNetSuiteColumn());
    }
    return nlSearchColumns;
}

/**
 * Converts an array of ssobjSearchFilters to nlobjSearchFilters
 * 
 * @param {Array}
 *        ssobjSearchFilters
 */
function ssapiGetNetSuiteSearchFilters(tcSearchFilters) {
    var nlSearchFilters = [];
    for (var i = 0; i < tcSearchFilters.length; i++) {
        nlSearchFilters.push(tcSearchFilters[i].getNetSuiteFilter());
    }
    return nlSearchFilters;
}

/**
 * Usage is similar to nlapiSearchRecord but with additional functionality for
 * collecting debugging info
 * 
 * @param {string}
 *        record script id
 * @param {string}
 *        id
 * @param {nlobjSearchFilter[]}
 *        columns Search filters
 * @param {object}
 *        columns This can be nlobjSearchColumn[], column ids or string literal
 *        'all' which includes all columns of the record
 */
function ssapiSearchRecord(type, id, filters, columns) {
    var logger = new ssobjLogger(arguments);

    var searchDetails = 'type=' + type + ssapiGetNewLine(); // ssapiGetArgumentDetails(arguments);
    if (ssapiHasNoValue(columns) || typeof columns == 'string') {
        logger.log('getting all columns');
        // get all columns
        var allColumns = nlapiCreateRecord(type).getAllFields();
        // include only custom fields
        columns = [];
        // FLD_INTERNAL_ID is not listed in getAllFields() so add manually
        columns.push(new nlobjSearchColumn(FLD_INTERNAL_ID));
        for (var i = 0; i < allColumns.length; i++) {
            var columnId = allColumns[i];
            // exclude fields whose stored in value is F
            // TODO: check by code this condition
            var excludeNotStoredColumns = [ 'custrecord_suitesocial_autosub_type_ft' ];
            if (excludeNotStoredColumns.indexOf(columnId) > -1) {
                continue;
            }
            // logger.log('columnId=' + columnId);
            if (columnId.substr(0, 10) == 'custrecord' || columnId == 'name' || columnId == FLD_INTERNAL_ID) {
                // logger.ok('include columnId=' + columnId);
                columns.push(new ssobjSearchColumn(columnId));
            }
        }
    }

    var nColumns = [];
    for (i = 0; i < columns.length; i++) {
        if (columns[i].getNetSuiteColumn) {
            nColumns.push(columns[i].getNetSuiteColumn());
        } else {
            nColumns.push(columns[i]);
        }
        searchDetails += 'index=' + i + '; ' + ssapiGetObjectDetail(columns[i]) + ssapiGetNewLine();
    }
    // filters
    var nFilters = [];
    searchDetails += 'SEARCH FILTERS: ' + ssapiGetNewLine();
    if (ssapiHasValue(filters)) {
        searchDetails += 'filters.length=' + filters.length + ssapiGetNewLine();
        for (var i = 0; i < filters.length; i++) {
            var filter = filters[i];
            searchDetails += 'index=' + i + '; ' + filter.toString() + ssapiGetNewLine();
            if (filter instanceof ssobjSearchFilter) {
                // logger.log('filter instanceof ssobjSearchFilter');
                nFilters.push(filter.getNetSuiteFilter());
            } else {
                // logger.log('filter NOT instanceof ssobjSearchFilter');
                nFilters.push(filter);
            }
        }
    } else {
        searchDetails += 'no filters';
        nFilters = null;
    }

    logger.log('searchDetails=' + searchDetails);
    try {
        var results = nlapiSearchRecord(type, id, nFilters, nColumns);
        logger.end('DONE');
        return results;
    } catch (e) {
        ssapiHandleError(e, ssapiGetNewLine() + 'SEARCH DETAILS: ' + ssapiGetNewLine() + searchDetails);
        throw 'e=' + ssapiGetErrorDetails(e) + '; ' + ssapiGetNewLine() + 'SEARCH DETAILS: ' + ssapiGetNewLine() + searchDetails;
        // return null;
    }
    // logger.log('DONE');
}

/**
 * Simulates sending of email. This done by adding a row in a custom record
 * customrecord_ss_mock_inbox. You can then search the custom record for the
 * fields from, to, subject and body. This is also used in checking if a runtime
 * error occured since emails are sent when error occurs. This automatically
 * sends the actual email for coding convenience
 * 
 * @param {Object}
 *        from Same as from of nlapiSendEmail
 * @param {Object}
 *        to Same as to of nlapiSendEmail
 * @param {Object}
 *        subject Same as subject of nlapiSendEmail
 * @param {Object}
 *        body Same as body of nlapiSendEmail
 * @return {string} Id of the created row
 */
function ssapiSendMockEmail(from, to, subject, body) {
    // the actual email sending
    nlapiSendEmail(from, to, subject, body);

    // mock
    try {
        var r = nlapiCreateRecord('customrecord_ss_mock_inbox');
        r.setFieldValue('custrecord_smi_from', from);
        r.setFieldValue('custrecord_smi_to', to);
        r.setFieldValue('custrecord_smi_subject', subject);
        r.setFieldValue('custrecord_smi_body', body);
        return nlapiSubmitRecord(r);
    } catch (e) {
        // Do not call error handler here since the error handler calls this
        // function.
        // We do not want an endless loop calls.
        nlapiLogExecution('error', 'suitesocial.Helper.sendMockEmail', e);
        nlapiSendEmail(from, to, 'SuiteSocial runtime error', e);
        return null;
    }
}

/*
 * returns details about a function call. This is used among others in
 * determining cause/source of errors @param {object} args See arguments of
 * javascript
 */
function ssapiGetFunctionCallDetails() {
    var args = ssobjGlobal.args;
    if (ssapiHasNoValue(args)) {
        return '';
    }
    var argumentList = '';
    for (var i = 0; i < args.length; i++) {
        argumentList += ssapiGetNewLine() + 'argument ' + i + '=' + args[i];
    }
    return 'callee.name=' + args.callee.name + '; args.length=' + arguments.length + ssapiGetNewLine() + 'argumentList=' + argumentList;
}

function getErrorDetails(ex) {
    var errorDetails = '';
    try {
        errorDetails = 'ERROR DETAILS' + ssapiGetNewLine();
        errorDetails += 'ex=' + ex.toString() + ssapiGetNewLine();

        if (ex.getDetails) {
            errorDetails += 'Details: ' + ex.getDetails() + ssapiGetNewLine();
        }
        if (ex.getCode) {
            errorDetails += 'Code: ' + ex.getCode() + ssapiGetNewLine();
        }
        if (ex.getId) {
            errorDetails += 'Id: ' + ex.getId() + ssapiGetNewLine();
        }
        if (ex.getStackTrace) {
            errorDetails += 'StackTrace: ' + ex.getStackTrace() + ssapiGetNewLine();
        }
        if (ex.getUserEvent) {
            errorDetails += 'User event: ' + ex.getUserEvent() + ssapiGetNewLine();
        }
        if (ex.getInternalId) {
            errorDetails += 'Internal Id: ' + ex.getInternalId() + ssapiGetNewLine();
        }
        if (ex.rhinoException) {
            errorDetails += 'RhinoException: ' + ex.rhinoException.toString() + ssapiGetNewLine();
        }
        if (ex.stack) {
            errorDetails += 'Stack=' + ex.stack;
        }

        if (ex instanceof nlobjError) {
            errorDetails += 'Type: nlobjError' + ssapiGetNewLine();
        } else if (ssapiHasValue(ex.rhinoException)) {
            errorDetails += 'Type: rhinoException' + ssapiGetNewLine();
        } else {
            errorDetails += 'Type: Generic Error' + ssapiGetNewLine();
        }

    } catch (e) {
        errorDetails += ' Error in ssapiGetErrorDetails=' + e;
    }
    return errorDetails;
}

function ssapiGetErrorDetails(ex) {
    return getErrorDetails(ex);
}

/**
 * Error handling routine
 * 
 * @param {Object}
 *        e Exception
 * @param {Object}
 *        customMessage Any message you want included
 */
function ssapiHandleError(e, customMessage) {
    try {
        // var logger = new ssobjLogger(arguments);
        var fullMessage = 'EXECUTION CONTEXT' + ssapiGetNewLine() + ssapiGetContextDetails() + ssapiGetNewLine(1) + 'customMessage=' + customMessage + ssapiGetNewLine(2);
        var isInBrowser = (typeof document != 'undefined');
        var context = nlapiGetContext();
        if (ssapiHasNoValue(customMessage)) {
            customMessage = '';
        }

        fullMessage += ssapiGetErrorDetails(e);
        // =====================================================================================================
        // client-side stack trace
        // =====================================================================================================
        // logger.log('isInBrowser=' + isInBrowser);
        if (isInBrowser) {
            var clientStackTrace = '';
            if (ssapiHasValue(Error)) {
                var err = new Error();
                clientStackTrace = err.stack;
                fullMessage += 'CLIENT STACK TRACE=' + clientStackTrace + ssapiGetNewLine(2);
            }
            if (typeof console != 'undefined') {
                // logger.log("typeof console != 'undefined'");
                if (typeof console.error != 'undefined') {
                    console.error(ssobjGlobal.TITLE + ' Error: ' + fullMessage);
                    return fullMessage;
                }

                if (typeof console.log != 'undefined') {
                    console.log(ssobjGlobal.TITLE + ' Error: ' + fullMessage);
                    return fullMessage;
                }
            }
            nlapiLogExecution('error', ssobjGlobal.TITLE, 'Error in ssapiHandleError(); fullMessage=' + fullMessage);
            return fullMessage;

        } else {
            // =====================================================================================================
            // server-side stack trace
            // =====================================================================================================
            var html = fullMessage.replace(new RegExp('\n', 'gi'), ';&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
            nlapiLogExecution('error', ssobjGlobal.TITLE + ' Error', html);
        }

        // get recipient of errors
        var companyPrefs = nlapiLoadConfiguration('companypreferences');
        var errorMessageRecipient = companyPrefs.getFieldValue('custscript_ss_error_message_recipient');
        // logger.log('errorMessageRecipient=' + errorMessageRecipient);
        var user = nlapiGetContext().getUser();
        var company = nlapiGetContext().getCompany();
        if (ssapiHasValue(errorMessageRecipient)) {
            if (typeof suitesocial != 'undefined') {
                suitesocial.Helper.sendMockEmail(user, errorMessageRecipient, 'SuiteSocial Runtime Error. Company=' + company + '; ' + e.toString(), fullMessage);
            } else {
                ssapiSendMockEmail(user, errorMessageRecipient, 'SuiteSocial Runtime Error. Company=' + company + '; ' + e.toString(), fullMessage);
            }
            return fullMessage;
        }

        // send to admins
        var filters = [ new nlobjSearchFilter('role', null, 'anyof', 3 /* admin */) ];
        var results = nlapiSearchRecord('employee', null, filters, null);
        for (var i = 0; i < results.length; i++) {
            var result = results[i];
            /*
             * -4 system user
             */
            if (context.getExecutionContext() == 'webstore' || user == '-4') {
                user = results[i].getId();
            }
            if (typeof suitesocial != 'undefined') {
                suitesocial.Helper.sendMockEmail(user, result.getId(), 'SuiteSocial Runtime Error. Company=' + company + '; ' + e.toString(), fullMessage);
            } else {
                ssapiSendMockEmail(user, result.getId(), 'SuiteSocial Runtime Error. Company=' + company + '; ' + e.toString(), fullMessage);
            }
        }

        return fullMessage;
    } catch (e) {
        fullMessage = 'Error in ssapiHandleError(); e=' + e;
        if (typeof document !== 'undefined' && typeof console !== 'undefined') {
            if (console.error) {
                console.error('ssapiHandleError() ' + fullMessage);
            }
        } else {
            nlapiLogExecution('error', 'SuiteSocial Runtime error', 'ssapiHandleError() ' + fullMessage);
        }
        // comment line below when released
        // throw e;
        return fullMessage;
    }

}

/**
 * Returns an array of internal ids of all records even if they are more than
 * 1000
 * 
 * @return {string[]}
 */
function ssapiGetAllIds(recordScriptId, searchFilters) {
    var ids = [];
    var lowerId = -100;
    var searchColumns = [ new nlobjSearchColumn('internalid') ];
    searchColumns[0].setSort(); // sort ascending
    // add filter used in paging
    searchFilters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', lowerId));
    searchFilters[searchFilters.length - 1].setFormula('ROUND({internalid})');

    var results = nlapiSearchRecord(recordScriptId, null, searchFilters, searchColumns);
    while (results !== null) {
        for (var i = 0; i < results.length; i++) {
            ids.push(results[i].getValue('internalid'));
        }
        lowerId = results[results.length - 1].getValue('internalid');
        searchFilters[searchFilters.length - 1] = new nlobjSearchFilter('formulanumeric', null, 'greaterthan', lowerId);
        searchFilters[searchFilters.length - 1].setFormula('ROUND({internalid})');
        results = nlapiSearchRecord(recordScriptId, null, searchFilters, searchColumns);
    }
    return ids;
}

/**
 * @class This is used in measuring execution time in milli-seconds
 */
function ssobjStopWatch() {
    var startMilliSeconds = (new Date()).getTime();
    var lastCallToMeasure = (new Date()).getTime();
    /**
     * @public
     * @description Starts the timer
     */
    this.start = function() {
        startMilliSeconds = (new Date()).getTime();
    };

    /**
     * @public
     * @description Returns the current elapsed time (in ms) and resets the
     *              start time
     * @returns {integer} current elapsed time (in ms)
     */
    this.stop = function() {
        var currentMilliSeconds = (new Date()).getTime();
        var ms = currentMilliSeconds - startMilliSeconds;
        startMilliSeconds = currentMilliSeconds;
        return ms;
    };
    /**
     * @public
     * @description Returns the current elapsed time (in ms) WITHOUT resetting
     *              the start time (from the last call to start or stop)
     */
    this.measure = function() {
        var currentMilliSeconds = (new Date()).getTime();
        var ms = currentMilliSeconds - startMilliSeconds;
        return ms;
    };

    /**
     * Returns the current elapsed time (in ms) from start of the script call
     */
    this.measureFromScript = function() {
        var currentMilliSeconds = (new Date()).getTime();
        var ms = currentMilliSeconds - ssobjGlobal.startOfScriptMilliseconds;
        return ms;
    };

    /**
     * Returns the current elapsed time (in ms) from start of the function call
     */
    this.measureFromFunction = function() {
        var currentMilliSeconds = (new Date()).getTime();
        var ms = currentMilliSeconds - startMilliSeconds;
        return ms;
    };

    /**
     * Returns the current elapsed time (in ms) starting from the last call to
     * measureSegment but WITHOUT resetting the start time (from the last call
     * to start or stop)
     */
    this.measureSegment = function() {
        var currentMilliSeconds = (new Date()).getTime();
        var ms = currentMilliSeconds - lastCallToMeasure;
        lastCallToMeasure = currentMilliSeconds;
        return ms;
    };
}

/**
 * Returns details about the execution context
 * 
 * @return {string}
 */
function ssapiGetContextDetails() {
    // var logger = new ssobjLogger('ssapiGetContextDetails', true);
    var executiondetails = '';
    try {
        var userId = nlapiGetUser();
        var context = nlapiGetContext();
        executiondetails += 'Company: ' + context.getCompany() + ssapiGetNewLine();
        executiondetails += 'User Id: ' + userId + ssapiGetNewLine();
        executiondetails += 'User Name: ' + context.getName() + ssapiGetNewLine();
        executiondetails += 'Role: ' + nlapiGetRole() + ssapiGetNewLine();
        executiondetails += 'Role Center: ' + context.getRoleCenter() + ssapiGetNewLine();
        executiondetails += 'DeploymentId: ' + context.getDeploymentId() + ssapiGetNewLine();
        executiondetails += 'User Email: ' + context.getEmail() + ssapiGetNewLine();
        executiondetails += 'Environment: ' + context.getEnvironment() + ssapiGetNewLine();
        executiondetails += 'ExecutionContext: ' + context.getExecutionContext() + ssapiGetNewLine();
        executiondetails += 'Name: ' + context.getName() + ssapiGetNewLine();
        executiondetails += 'ScriptId: ' + context.getScriptId() + ssapiGetNewLine();
        executiondetails += 'Version: ' + context.getVersion() + ssapiGetNewLine();
        if (context.getFeature('departments'))
            executiondetails += 'Department: ' + nlapiGetDepartment() + ssapiGetNewLine();
        if (context.getFeature('locations'))
            executiondetails += 'Location: ' + nlapiGetLocation() + ssapiGetNewLine();
        if (context.getFeature('subsidiaries'))
            executiondetails += 'Subsidiary: ' + nlapiGetSubsidiary() + ssapiGetNewLine();
    } catch (e) {
    }
    return executiondetails;
}

/**
 * Returns newlines depending on the execution context
 * 
 * @param {integer}
 *        repeat Number of newlines to return
 */
function ssapiGetNewLine(repeat) {
    if (typeof repeat == 'undefined') {
        repeat = 1;
    }
    var newline = '';
    for (var i = 1; i <= repeat; i++) {
        // if (ssapiHasValue(console)) {
        if (typeof console !== 'undefined') {
            newline += '\n';
        } else {
            newline += '\n';
        }
    }
    return newline;
}





/**
 * Browser and server independent implementation of indexOf since IE does not support it
 * 
 * @param {object[]}
 *        arr
 * @param {object}
 *        obj
 */
function ssapiArrayIndexOf(arr, obj)
{
    if (arr == null)
    {
        return -1;
    }

    if (arr.indexOf)
    {
        return arr.indexOf(obj);
    }
    
    // no support
    for (var i = 0; i < arr.length; i++)
    {
        if (arr[i] == obj)
        {
            return i;
        }
    }
    return -1;
}





/**
 * Used in logging
 * 
 * @param {string}
 *        msg
 * @param {string}
 *        otherDetails Not being used?
 * @param {string}
 *        source Not being used?
 */
function ssapiLog(msg, otherDetails, source, type) {
    var completeMsg = msg;// + ': ' + eval(msg);
    if (ssapiHasNoValue(type)) {
        type = 'debug';
    }

    if (!ssapiHasNoValue(otherDetails)) {
        completeMsg = completeMsg + ' otherDetails=' + otherDetails;
    }
    if (!ssapiHasNoValue(source)) {
        completeMsg = source + '() ' + completeMsg;
    }
    if (typeof document !== 'undefined') {
        if (typeof console !== 'undefined') {
            if (console.log) {
                console.log(completeMsg);
            }
        }
    } else {
        completeMsg = '(' + nlapiGetContext().getRemainingUsage() + ') ' + completeMsg;
        nlapiLogExecution(type, 'ssobjLogger', ssapiGetTimeWithMilliSecond() + ' ' + completeMsg);
    }
    return completeMsg;
}

/**
 * Gets the time with the milli-second component
 * 
 * @returns {String}
 */
function ssapiGetTimeWithMilliSecond() {
    var d = new Date();

    var curr_year = d.getFullYear();

    // var curr_month = d.getMonth() + 1; // Months are zero based
    // if (curr_month < 10)
    // curr_month = "0" + curr_month;
    //
    // var curr_date = d.getDate();
    // if (curr_date < 10)
    // curr_date = "0" + curr_date;

    var curr_hour = d.getHours();
    if (curr_hour < 10)
        curr_hour = "0" + curr_hour;

    var curr_min = d.getMinutes();
    if (curr_min < 10)
        curr_min = "0" + curr_min;

    var curr_sec = d.getSeconds();
    if (curr_sec < 10)
        curr_sec = "0" + curr_sec;

    var curr_msec = d.getMilliseconds();
    if (curr_msec < 10)
        curr_msec = "0" + curr_sec;

    var newtimestamp = curr_hour + ":" + curr_min + ":" + curr_sec + ':' + curr_msec;

    return newtimestamp;
}

function ssapiAudit(msg, otherDetails, source, type) {
    var completeMsg = msg;// + ': ' + eval(msg);
    if (ssapiHasNoValue(type)) {
        type = 'audit';
    }

    if (!ssapiHasNoValue(otherDetails)) {
        completeMsg = completeMsg + ' otherDetails=' + otherDetails;
    }
    if (!ssapiHasNoValue(source)) {
        completeMsg = source + '() ' + completeMsg;
    }
    if (typeof document !== 'undefined') {
        if (console.log) {
            console.log(completeMsg);
            return completeMsg;
        }
    } else {
        completeMsg = '(' + nlapiGetContext().getRemainingUsage() + ') ' + completeMsg;
        nlapiLogExecution(type, 'ssobjLogger', ssapiGetTimeWithMilliSecond() + ' ' + completeMsg);
    }
    return completeMsg;
}

/**
 * Logs an error
 * 
 * @param {Object}
 *        msg
 * @param {Object}
 *        otherDetails
 * @param {Object}
 *        source
 */
function ssapiLogError(msg, otherDetails, source) {
    if (typeof document !== 'undefined' && typeof console !== 'undefined') {
        msg = 'ERROR: ' + msg;
        ssapiLog(msg);
    } else {
        var type = ssobjGlobal.enableDebugging ? 'error' : 'debug';
        type = 'error';
        // msg = '<span style="background-color: pink">' + msg + '</span>';
        ssapiLog(msg, otherDetails, source, type);
    }
    return msg;
}

/**
 * Logs a successful activity
 * 
 * @param {Object}
 *        msg
 * @param {Object}
 *        otherDetails
 * @param {Object}
 *        source
 */
function ssapiLogOk(msg, otherDetails, source) {
    if (typeof document !== 'undefined' && typeof console !== 'undefined') {
        msg = 'SUCCESS: ' + msg;
        ssapiLog(msg);
    } else {
        var type = ssobjGlobal.enableDebugging ? 'audit' : 'debug';
        msg = '<span style="background-color: lightgreen">' + msg + '</span>';
        ssapiLog(msg, otherDetails, source, type);
    }
    return msg;
}

/**
 * Logs a warning
 * 
 * @param {Object}
 *        msg
 * @param {Object}
 *        otherDetails
 * @param {Object}
 *        source
 */
function ssapiLogWarn(msg, otherDetails, source) {
    if (typeof document !== 'undefined' && typeof console !== 'undefined') {
        msg = 'WARNING: ' + msg;
        ssapiLog(msg);
    } else {
        var type = ssobjGlobal.enableDebugging ? 'error' : 'debug';
        msg = '<span style="background-color: yellow">' + msg + '</span>';
        ssapiLog(msg, otherDetails, source, type);
    }
    return msg;
}

/**
 * @class Object used in logging. Used in both client and server
 * @example // sample 1 var logger = new ssobjlogger(arguments); // sample 2 var
 *          logger = new ssobjlogger(arguments, false, 'getData()');
 * @param {Object}
 *        args 'arguments' is a built-in object in functions. Pass 'arguments'
 *        always.
 * @param {Boolean}
 *        (optional) isDisabled Set to true to temporarily disable logging.
 *        Default to false.
 * @param {String}
 *        commonLog (optional) All succeeding calls to log will prepend the
 *        commonLog .
 * @returns {void}
 */
function ssobjLogger(args, isDisabled, commonLog) {

    var sw = new ssobjStopWatch();
    // this is an array of commonLog values. If a commonLog is in this list, it
    // is disabled
    var disabledCommonLogs = [ 'ssapiGetFieldPropertiesByText', 'ssapiGetOptionsRecordSelectFieldsByName' ];

    var _disabled = false;
    if (ssapiHasValue(isDisabled)) {
        _disabled = isDisabled;
    }

    /**
     * @deprecated Use the class public function setDisabled instead
     * @description (deprecated) Enable or disables logging
     * @param {Boolean}
     *        isDisabled
     */
    function setDisabled(isDisabled) {
        _disabled = isDisabled;
    }
    var _commonLog;
    var _argumentsDetails = '';
    if (typeof args == 'object') {
        _commonLog = (args.callee.name || commonLog) + '()';
        _argumentsDetails = ssapiGetArgumentDetails(args);
    } else {
        _commonLog = (commonLog || '') + ' ' + args + '()';
        _commonLog = _commonLog.trim();
    }

    if ([ 'ssapiIsSaveMissing()' ].indexOf(_commonLog) == -1) {
        ssobjGlobal = ssobjGlobal || {};
        ssobjGlobal.func = _commonLog;
    }

    this.log = function(msg) {
        if (_disabled === false) {
            if (ssapiArrayIndexOf(disabledCommonLogs, _commonLog) > -1) {
                return;
            }
            ssapiLog(_commonLog + ' ' + sw.measure() + 'ms; ' + msg);
        }
    };

    this.audit = function(msg) {
        if (_disabled === false) {
            if (ssapiArrayIndexOf(disabledCommonLogs, _commonLog) > -1) {
                return;
            }
            ssapiAudit(_commonLog + ' ' + sw.measure() + 'ms; ' + msg);
        }
    };

    this.auditReset = function(msg) {
        sw.measureSegment();
    };

    /**
     * @public
     * @description Logs everytime even if _disabled is true
     * @param {Object}
     *        msg
     */
    this.logAlways = function(msg) {
        ssapiLog(_commonLog + ' ' + sw.measure() + 'ms; ' + msg);
    };

    /**
     * @public
     * @description Logs an error. In NetSuite, error log entries have red
     *              background.
     * @param {Object}
     *        msg
     */
    this.error = function(msg) {
        if (_disabled === false) {
            if (ssapiArrayIndexOf(disabledCommonLogs, _commonLog) > -1) {
                return;
            }
            ssapiLogError(_commonLog + ' ' + sw.measure() + 'ms; ' + msg);
        }
    };
    /**
     * @public
     * @description Logs a warning. In NetSuite, warning log entries have yellow
     *              background.
     * @param {Object}
     *        msg
     */
    this.warn = function(msg) {
        if (_disabled === false) {
            if (ssapiArrayIndexOf(disabledCommonLogs, _commonLog) > -1) {
                return;
            }
            ssapiLogWarn(_commonLog + ' ' + sw.measure() + 'ms; ' + msg);
        }
    };
    /**
     * @public
     * @description Logs a successful activity. In NetSuite, 'successful' log
     *              entries have green background.
     * @param {Object}
     *        msg
     */
    this.ok = function(msg) {
        if (_disabled === false) {
            if (ssapiArrayIndexOf(disabledCommonLogs, _commonLog) > -1) {
                return;
            }
            ssapiLogOk(_commonLog + ' ' + sw.measure() + 'ms; ' + msg);
        }
    };





    this.end = function(msg)
    {
        // var MAX_ARG_LENGTH =70;
        // var shortArgumentsDetails = _argumentsDetails;
        // if(shortArgumentsDetails.length > MAX_ARG_LENGTH){
        // shortArgumentsDetails = shortArgumentsDetails.substr(0,
        // MAX_ARG_LENGTH);
        // }
        var shortArgumentsDetails = '';
        if (ssapiHasValue(msg))
        {
            msg = 'END ' + shortArgumentsDetails + '; ' + msg;
        } else
        {
            msg = 'END ' + shortArgumentsDetails;
        }

        if (_disabled === false)
        {
            this.log(msg);
        }
    };





    /**
     * @public
     * @description Enable or disables logging
     * @param {Boolean}
     *        isDisabled
     */
    this.setDisabled = function(isDisabled) {
        _disabled = isDisabled;
    };

    this.log(_argumentsDetails);

}

/**
 * Logs the id and text of a select option array
 * 
 * @param {nlobjSelectOption[]}
 */
// function ssapiDebugOptions(options) {
// var logger = new ssobjLogger('ssapiDebugOptions', true);
// var debug = '';
// // nice to log caller but arguments.callee.caller is not supported by rhino
// // for performance reasons
// debug += 'options.length=' + options.length + ssapiGetNewLine();
// logger.log('options.length=' + options.length);
// var entities = [];
// for (var i = 0; i < options.length; i++) {
// debug += 'index=' + i + '; Id=' + options[i].getId() + '; Text=' +
// options[i].getText() + ssapiGetNewLine();
// logger.log('index=' + i + '; Id=' + options[i].getId() + '; Text=' +
// options[i].getText());
//
// entities.push({
// Id : options[i].getId(),
// Text : options[i].getText()
// });
// }
// logger.log('DONE');
// return ssapiCreateTableHtml(entities);
// // return debug;
// }
/**
 * Returns the option if the id exists
 * 
 * @param {nlobjSelectOption[]}
 *        select options
 * @return {nlobjSelectOption}
 */
// function ssapiGetOptionById(options, id) {
// var logger = new ssobjLogger('ssapiGetOptionById');
// if (ssapiHasNoValue(id)) {
// throw 'ssapiHasNoValue(id)';
// }
// for (var i = 0; i < options.length; i++) {
// if (options[i].getId() == id) {
// return options[i];
// }
// }
// return null;
// }
/**
 * Returns the option if the name exists. If not, returns null
 * 
 * @param {nlobjSelectOptions}
 *        options Array of nlobjSelectOptions where text will be searched
 * @param {string}
 *        searchText Text to search for
 * @return {nlobjSelectOption}
 */
function ssapiGetOptionByText(options, searchText) {
    try {
        var logger = new ssobjLogger('ssapiGetOptionByText');
        if (ssapiHasNoValue(options)) {
            throw 'ssapiHasNoValue(options)';
        }
        if (ssapiHasNoValue(options.length)) {
            throw 'ssapiHasNoValue(options.length)';
        }
        if (ssapiHasNoValue(searchText)) {
            throw 'ssapiHasNoValue(searchText)';
        }
        searchText = searchText.toLowerCase();
        // var debug = '';
        for (var i = 0; i < options.length; i++) {
            var option = options[i];
            if (option.getText().toLowerCase() == searchText) {
                return option;
            }
        }
    } catch (e) {
        nlapiLogExecution('debug', 'teddy', 'error in ssapiGetOptionByText');
        return null;
    }
    logger.log('not found, try filtering again');
    // there seems to be a core bug where fld.getSelectOptions(letter,
    // 'startsWith') does not return the options when called repeatedly,
    // so try getting the option by filtering again
    var record = nlapiCreateRecord(REC_RECORD_FIELD_DEF, {
        recordmode : 'dynamic'
    });
    var fld = record.getField(FLD_RECORD_FIELD_DEF_SCRIPTED_RECORD);
    options = fld.getSelectOptions(searchText, 'is');
    logger.log('options.length=' + options.length);
    if (options.length > 0) {
        return options[0];
    }
    return null;
}

/*
 * Delete all rows in a record
 * 
 * @deprecated @param {string} recordScriptId @return {int} number of records
 *             deleted
 */
// function ssapiDeleteAllX(recordScriptId) {
// var logger = new ssobjLogger('ssapiDeleteAll');
// recordScriptId = recordScriptId.trim();
// var searchColumns = [ new nlobjSearchColumn('internalid') ];
// // sort descending so that records added last will be deleted first.
// // This is needed specially if there is a parent-child relationship for the
// // same record
// searchColumns[0].setSort(true);
// var results = nlapiSearchRecord(recordScriptId, null, null, searchColumns);
// if (results === null) {
// return 0;
// }
// for (var i = 0; i < results.length; i++) {
// logger.log('recordScriptId=' + recordScriptId + '; row= ' + i + ' of ' +
// results.length + '; internal id=' + results[i].getValue(FLD_INTERNAL_ID));
// nlapiDeleteRecord(recordScriptId, results[i].getValue(FLD_INTERNAL_ID));
// }
// return results.length;
// }
/*
 * A shortcut method for nlapiSearchRecord @param {string} recordScriptId Id of
 * the record type @param {string[]} columnIds array of column ids returned in
 * the search @param {nlobjSearchFilter[]} searchFilters Search filters @return
 * {nlobjSearchResult[]} results of the search
 */
// function ssapiGetResultsAllColumns(recordScriptId, searchFilters) {
//
// var logger = new ssobjLogger(arguments);
// logger.log('recordScriptId=' + recordScriptId);
// var columnIds = nlapiCreateRecord(recordScriptId).getAllFields();
// var searchColumns = [];
// for (var i = 0; i < columnIds.length; i++) {
// var column = new nlobjSearchColumn(columnIds[i]);
// searchColumns.push(column);
// column.setSort();
// }
// var results = nlapiSearchRecord(recordScriptId, null, searchFilters,
// searchColumns);
// if (results !== null) {
// logger.log('recordScriptId=' + recordScriptId + ' results.length=' +
// results.length);
// } else {
// logger.log('recordScriptId=' + recordScriptId + ' results.length=0');
// }
// return results;
// }
/**
 * A shortcut method for nlapiSearchRecord
 * 
 * @param {string}
 *        recordScriptId Id of the record type
 * @param {string[]}
 *        columnIds array of column ids returned in the search
 * @param {nlobjSearchFilter[]}
 *        searchFilters Search filters
 * @return {nlobjSearchResult[]} results of the search
 */
function ssapiGetResults(recordScriptId, columnIds, searchFilters) {
    var logger = new ssobjLogger(arguments, true);
    logger.log('recordScriptId=' + recordScriptId);
    var searchColumns = [];
    for (var i = 0; i < columnIds.length; i++) {
        var column = new nlobjSearchColumn(columnIds[i]);
        searchColumns.push(column);
        column.setSort();
    }
    var results = nlapiSearchRecord(recordScriptId, null, searchFilters, searchColumns);
    if (results !== null) {
        logger.log('recordScriptId=' + recordScriptId + ' results.length=' + results.length);
    } else {
        logger.log('recordScriptId=' + recordScriptId + ' results.length=0');
    }
    return results;
}

/**
 * Alias of ssapiGetResults
 * 
 * @deprecated Use ssapiGetResults
 * @param {Object}
 *        recordScriptId
 * @param {Object}
 *        columnIds
 * @param {Object}
 *        searchFilters
 */
function ssapiRetrieveResults(recordScriptId, columnIds, searchFilters) {
    return ssapiGetResults(recordScriptId, columnIds, searchFilters);
}

/**
 * Alias of ssapiGetResults
 * 
 * @deprecated Use ssapiGetResults
 * @param {Object}
 *        recordScriptId
 * @param {Object}
 *        columnIds
 * @param {Object}
 *        searchFilters
 * @returns {array} of nlobjSearchResult
 */
function retrieveResults(recordScriptId, columnIds, searchFilters) {
    return ssapiGetResults(recordScriptId, columnIds, searchFilters);
}

/**
 * Creates an array of nlobjSearchColumn given an array of column ids.
 * 
 * @param {string[]}
 *        columnIds
 * @returns {array} of nlobjSearchColumn
 */
function ssapiGetSearchColumns(columnIds) {
    var searchColumns = [];
    for (var i = 0; i < columnIds.length; i++) {
        var column = new nlobjSearchColumn(columnIds[i]);
        searchColumns.push(column);
        column.setSort();
    }
    return searchColumns;
}

// /////////////////////////////////////////////////////////

var STANDARD_ROLES = [ 'A/P Clerk', 'A/R Clerk', 'Accountant', 'Accountant (Reviewer)', 'Administrator', 'Bookkeeper', 'CEO', 'CEO(Hands Off)', 'CFO', 'Customer Center', 'Employee Center', 'Engineer', 'Engineering Manager', 'Full Access', 'Intranet Manager', 'Issue Administrator', 'Marketing Administrator', 'Marketing Assistant', 'Marketing Manager', 'Partner Center', 'PM Manager', 'Product Manager', 'QA Engineer', 'QA Manager', 'Retail Clerk', 'Retail Clerk (Web Services Only)',
        'Sales Administrator', 'Sales Manager', 'Sales Person', 'Sales Vice President', 'Store Manager', 'Support Administrator', 'Support Manager', 'Support Person', 'System Administrator', 'Vendor Center' ];

/**
 * @deprecated Use ssapiCreateNetSuiteLookingButtonMarkup()
 * @param {Object}
 *        id
 * @param {Object}
 *        label
 * @param {Object}
 *        onClickFunction
 */
function createNetSuiteLookingButtonMarkup(id, label, onClickFunction) {
    return '<table class="uir-button" onclick="javascript:' + onClickFunction + ';" cellspacing="0" cellpadding="0" border="0" style="margin-right:6px;cursor:hand;" id="tbl_' + id + '"><tbody><tr class="pgBntG" id="tr_' + id + '"><td id="tdleftcap_' + id
            + '"><img width="3" height="50%" border="0" class="bntLT" src="/images/nav/ns_x.gif"><img width="3" height="50%" border="0" class="bntLB" src="/images/nav/ns_x.gif"></td><td valign="top" nowrap="" height="20" class="bntBgB"> <input type="button" id="' + id + '" value="' + label + '"  class="rndbuttoninpt bntBgT"></td><td id="tdrightcap_' + id
            + '"><img width="3" height="50%" border="0" class="bntRT" src="/images/nav/ns_x.gif"><img width="3" height="50%" border="0" class="bntRB" src="/images/nav/ns_x.gif"></td></tr></tbody></table>';
}

/**
 * Creates the markup for a NetSuite looking-button
 * 
 * @param {Object}
 *        id
 * @param {Object}
 *        label
 * @param {Object}
 *        onClickFunction
 * @return {string}
 */
function ssapiCreateNetSuiteLookingButtonMarkup(id, label, onClickFunction) {
    return createNetSuiteLookingButtonMarkup(id, label, onClickFunction);
}

/**
 * @deprecated Use ssapiRetrieveChildren()
 * @param {Object}
 *        parentId
 * @param {Object}
 *        childTypeId
 * @param {Object}
 *        childFieldId
 */
function retrieveChildren(parentId, childTypeId, childFieldId) {
    // var logger = new ssobjLogger('retrieveChildren');
    var filters = [ [ childFieldId, OP_ANYOF, parentId ] ];
    var childResults = retrieveResults(childTypeId, [ FLD_INTERNAL_ID ], filters);
    if (childResults === null) {
        return null;
    }
    return childResults;
}

/**
 * Retrieves the children of a record
 * 
 * @param {string}
 *        parentId
 * @param {string}
 *        childTypeId
 * @param {string}
 *        childFieldId
 * @return {array} of nlobjSearchResult
 */
function ssapiRetrieveChildren(parentId, childTypeId, childFieldId) {
    return retrieveChildren(parentId, childTypeId, childFieldId);
}

// function retrieveChildren(parentId, childTypeId, childFieldId){
// // var logger = new ssobjLogger('retrieveChildren');
// var filters = [new nlobjSearchFilter(childFieldId, null, OP_IS, parentId)];
// var childResults = retrieveResults(childTypeId, [FLD_INTERNAL_ID], filters);
// if (childResults === null) {
// return null;
// }
// return childResults;
// }

/**
 * Deletes child records of a record
 * 
 * @param {Object}
 *        recordType
 * @param {Object}
 *        internalId
 */
function deleteChildRecords(recordType, internalId) {
    var childRecordTypeIds = [];
    switch (recordType) {
    case REC_AUTOSUB_TYPE:
        childRecordTypeIds = [ REC_AUTOSUBSCRIBE ];
        break;
    default:
        // default_statement;
    }
    // delete child records
    for (var child = 0; child < childRecordTypeIds.length; child++) {
        var childRecordTypeId = childRecordTypeIds[child];
        var searchColumns = [ new nlobjSearchColumn(FLD_INTERNAL_ID) ];
        var filters = [ new nlobjSearchFilter(FLD_AUTOSUBSCRIBE_TYP, null, OP_ANYOF, internalId) ];
        var results = nlapiSearchRecord(childRecordTypeId, null, filters, searchColumns);
        if (results !== null) {
            for (var i = 0; i < results.length; i++) {
                nlapiDeleteRecord(childRecordTypeId, results[i].getValue(FLD_INTERNAL_ID));
            }
        }
    }
}

/**
 * Returns true if the column values have duplicates in the db.
 * 
 * @param {string}
 *        recordTypeId Such as customer, supportcase
 * @param {JSON[]}
 *        entities Array of JSON objects with properties:<br />
 *        columnId, operator, value, func
 * @param {integer}
 *        exceptInternalId Used for existing rows so that it does not find
 *        itself
 * @return {boolean} true if a row is found
 */
function ssapiHasDuplicatesAllowEmptyValues(recordTypeId, entities, exceptInternalId) {
    var logger = new ssobjLogger(arguments);
    if (entities.length === 0) {
        throw 'entities.length ===0';
    }
    // create filter
    var filters = [];
    if (ssapiHasValue(exceptInternalId)) {
        // this is used when excluding the record being passed
        filters.push(new nlobjSearchFilter('internalid', null, OP_NONEOF, exceptInternalId));
    }

    for (var i = 0; i < entities.length; i++) {
        var columnId = entities[i].columnId;
        var operator = entities[i].operator;
        var value = entities[i].value;

        var filter;
        if (ssapiHasValue(entities[i].func)) {
            // value = value.toLowerCase();
            filter = new nlobjSearchFilter('formulatext', null, operator, value);
            filter.setFormula(entities[i].func + '({' + columnId + '})');
        } else {

            if (ssapiHasNoValue(value)) {
                filter = new nlobjSearchFilter(columnId, null, 'isempty');
            } else {
                filter = new nlobjSearchFilter(columnId, null, operator, value);
            }
        }
        filters.push(filter);
    }
    var results = nlapiSearchRecord(recordTypeId, null, filters, [ new nlobjSearchColumn(FLD_INTERNAL_ID) ]);
    return results !== null;
}

/**
 * Returns true if the column values have duplicates in the db.
 * 
 * @param {string}
 *        recordTypeId Such as customer, supportcase
 * @param {JSON[]}
 *        entities Array of json with properties: columnId, operator, value,
 *        func
 * @parama {integer} exceptInternalId Used for existing rows so that it does not
 *         find itself
 * @return {boolean} true if a row is found
 */
function hasDuplicates(recordTypeId, entities, exceptInternalId) {
    // Issue: 212029 Field 'Field' of custom record types Auto Post Field
    // var logger = new ssobjLogger('hasDuplicates');
    if (entities.length === 0) {
        throw 'entities.length ===0';
    }
    // create filter
    var filters = [];
    if (ssapiHasValue(exceptInternalId)) {
        // this is used when excluding the record being passed
        filters.push(new nlobjSearchFilter('internalid', null, OP_NONEOF, exceptInternalId));
    }

    for (var i = 0; i < entities.length; i++) {
        var columnId = entities[i].columnId;
        var operator = entities[i].operator;
        var value = entities[i].value;
        // validate
        if (ssapiHasNoValue(columnId)) {
            throw 'ssapiHasNoValue(columnId)';
        }
        if (ssapiHasNoValue(operator)) {
            throw 'ssapiHasNoValue(operator)';
        }
        if (ssapiHasNoValue(value)) {
            throw 'ssapiHasNoValue(value)';
        }

        var filter;
        if (ssapiHasValue(entities[i].func)) {
            // value = value.toLowerCase();
            filter = new nlobjSearchFilter('formulatext', null, operator, value);
            filter.setFormula(entities[i].func + '({' + columnId + '})');
        } else {
            filter = new nlobjSearchFilter(columnId, null, operator, value);
        }
        filters.push(filter);
    }
    var results = nlapiSearchRecord(recordTypeId, null, filters, [ new nlobjSearchColumn(FLD_INTERNAL_ID) ]);
    return results !== null;
}

function ssapiHasDuplicates(recordTypeId, entities, exceptInternalId) {
    return hasDuplicates(recordTypeId, entities, exceptInternalId);
}

/**
 * Returns the array of values of a multiple select field. This has to be
 * created because getFieldValues() has a bug.
 * 
 * @param {string}
 *        typeId The id of the record type
 * @param {string}
 *        columnId The id of the multiple select field
 * @param {select}
 *        internalId The internal id of the row to be loaded
 * @return {array} Returns an array similar to the return value of
 *         getFieldValues
 */
// function getMultipleSelectValues(typeId, columnId, internalId) {
// var columns = [ new nlobjSearchColumn(columnId) ];
// var filters = [ new nlobjSearchFilter('internalid', null, 'anyof',
// internalId) ];
// var results = nlapiSearchRecord(typeId, null, filters, columns);
// if (results === null) {
// return null;
// }
// return results[0].getValue(columnId).split();
//
// // Note: use code below below once the bug is fixed
// // var record = nlapiLoadRecord(typeId, columnId);
// // var values = record.getFieldValues(columnId);
// // return values;
// }
/**
 * Returns the HTML markup of a netsuite looking table
 * 
 * @param {Array}
 *        entities An array of JSON objects
 * @param {Object}
 *        noDataMessage The message displayed when there is no data
 * @return {String}
 */
function ssapiCreateTableHtml(entities, noDataMessage) {

    if (entities.length === 0) {

        return noDataMessage || 'No data'.tl() + '.';
    }

    var html = "<div class='subtabblock' style='padding: 4px; margin-left: 20px; margin-right: 20px'>";
    html += '<table width=100% border=0  cellspacing=0 class="smalltextnolink" style="background-colorx: gainsboro;">';

    // create headers
    var firstEntity = entities[0];
    html += '<tr style="font-weight: bold; ' + (ssapiIsNewUI() ? ' font: normal 14px Open Sans, Helvetica, sans-serif;' : '') + '" class="listheadertdwht listheadertextb">';
    for ( var p in firstEntity) {
        html += '<td>';
        html += p;
        html += '</td>';

    }
    html += '</tr>';

    // rows
    var isOdd = true;
    for (var i = 0; i < entities.length; i++) {
        // html += '<tr' + (isOdd ? ' style="background-color: #efefef" ' : '')
        // + '>';
        html += '<tr>';

        var entity = entities[i];
        for (p in firstEntity) {
            html += '<td class="listtexthlwht" style="vertical-align: top; ' + (ssapiIsNewUI() ? ' font: normal 14px Open Sans, Helvetica, sans-serif;' : '') + '">';
            if (ssapiHasValue(entity[p])) {
                html += entity[p];
            } else {
                html += '&nbsp;' + 'none'.tl();
            }

            html += '</td>';
        }
        html += '</tr>';

        isOdd = isOdd === false;
    }

    // close table
    html += '</table>';
    html += '</div>';

    return html;
}

/**
 * Returns the HTML markup of a netsuite looking table
 * 
 * @param {Array}
 *        entities An array of JSON objects
 * @param {Object}
 *        noDataMessage The message displayed when there is no data
 * @return {String}
 */
// function ssapiCreateTableHtml2(entities, noDataMessage) {
//
// if (entities.length === 0) {
// return noDataMessage || 'No data.';
// }
//
// var html = '';
// html += '<table width=100% border=1 cellspacing=0 >';
//
// // create headers
// var firstEntity = entities[0];
// html += '<tr style="font-weight: bold">';
// for ( var p in firstEntity) {
// html += '<td>';
// html += p;
// html += '</td>';
//
// }
// html += '</tr>';
//
// // rows
// var isOdd = true;
// for (var i = 0; i < entities.length; i++) {
// html += '<tr' + (isOdd ? ' style="background-color: #efefef" ' : '') + '>';
// // html += '<tr>';
//
// var entity = entities[i];
// for (p in firstEntity) {
// html += '<td class="listtexthlwht" style="; vertical-align: top">';
// if (ssapiHasValue(entity[p])) {
// html += entity[p];
// } else {
// html += '&nbsp;';
// }
//
// html += '</td>';
// }
// html += '</tr>';
//
// isOdd = isOdd === false;
// }
//
// // close table
// html += '</table>';
// html += '</div>';
//
// return html;
// }
/**
 * Returns the HTML markup of a netsuite looking table
 * 
 * @param {Array}
 *        entities An array of JSON objects
 * @param {Object}
 *        noDataMessage The message displayed when there is no data
 * @return {String}
 */
function createTableHtml(entities, noDataMessage) {
    return ssapiCreateTableHtml(entities, noDataMessage);
}

// /*
// * A shortcut method for nlapiSearchRecord @param {string} recordId Id of the
// * record type @param {string[]} columnIds array of column ids returned in the
// * search @param {nlobjSearchFilter[]} searchFilters Search filters @return
// * (nlobjSearchResult[]} result of the search
// */
// function retrieveResults(recordId, columnIds, searchFilters){
// var searchColumns = [];
// for (var i = 0; i < columnIds.length; i++) {
// var searchColumn = new nlobjSearchColumn(columnIds[i]);
// searchColumn.setSort();
// searchColumns.push(searchColumn);
// }
// return nlapiSearchRecord(recordId, null, searchFilters, searchColumns);
// }

/**
 * Removes html tags from a string
 * 
 * @param {string}
 *        myString
 * @return {string}
 */
// function ssapiRemoveHtmlTags(myString) {
// return myString.replace(/<(?:.|\n)*?>/gm, '');
// }
createJSON();

/**
 * Creates JSON object if it does not exists (like in IE 8 or in test
 * automation)
 */
function createJSON() {
    // // implement JSON.stringify serialization
    if (typeof Ext !== 'undefined') {
        var JSON = JSON || {};
        JSON.stringify = JSON.stringify || Ext.encode;
        JSON.parse = JSON.parse || Ext.decode;
    }

    if (typeof JSON == 'undefined') {
        var JSON = {};
        // implement JSON.stringify serialization
        // implement JSON.stringify serialization
        JSON.stringify = JSON.stringify || function(obj) {
            var t = typeof (obj);
            if (t != "object" || obj === null) {
                // simple data type
                if (t == "string") {
                    obj = '"' + obj + '"';
                }
                return String(obj);
            } else {
                // recurse array or object
                var n, v, json = [], arr = (obj && obj.constructor == Array);
                for (n in obj) {
                    v = obj[n];
                    t = typeof (v);
                    if (t == "string") {
                        v = '"' + v + '"';
                    }

                    else if (t == "object" && v !== null) {
                        v = JSON.stringify(v);
                    }

                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        };
        JSON.parse = function(jsonString) {
            return eval("(" + jsonString + ")");
        };
    }
}

function getRecordIdManyFilters(recordTypeId, columnIds, operators, values) {
    var logger = new ssobjLogger(arguments.callee.name);
    var columns = [ new nlobjSearchColumn('internalid') ];
    var filters = [];
    for (var i = 0; i < columnIds.length; i++) {
        filters.push(new nlobjSearchFilter(columnIds[i], null, operators[i], values[i]));
    }
    var results = nlapiSearchRecord(recordTypeId, null, filters, columns);
    if (results === null) {
        return null;
    } else {
        if (results.length > 1) {
            // we expect only one row
            throw 'results.length > 1';
        }
        return results[0].getValue('internalid');
    }
}

function getCustomRecordTypeInternalId(scriptId) {
    var columns = [];
    columns.push(new nlobjSearchColumn('internalid'));
    var filter = new nlobjSearchFilter('scriptid', null, 'is', scriptId);
    var filters = [];
    filters.push(filter);
    var results = nlapiSearchRecord('customrecordtype', null, filters, columns);
    if (results === null) {
        return null;
    } else {
        return results[0].getValue('internalid');
    }
}

/**
 * Gets the internal id of a record type given the script id (ex.
 * customrecord_car)
 * 
 * @param {Object}
 *        scriptId
 */
function ssapiGetCustomRecordTypeInternalId(scriptId) {
    return getCustomRecordTypeInternalId(scriptId);
}

/**
 * This is used to check whether a column id is part of record. This is used in
 * filtering the fields displayed by sourcing which displays fields not part of
 * the record type
 * 
 * @param {Object}
 *        recordTypeId
 * @param {Object}
 *        columnId
 */
// function isColumnOfRecord(recordTypeId, columnId) {
// var logger = new ssobjLogger('isColumnOfRecord');
// try {
// var searchColumn = new nlobjSearchColumn(columnId);
// var searchColumns = [];
// searchColumns.push(searchColumn);
// nlapiSearchRecord(recordTypeId, null, null, searchColumns);
// return true;
// } catch (e) {
// logger.log('recordTypeId=' + recordTypeId + '; columnId=' + columnId + '; e='
// + e.toString());
// return false;
// }
// }
/**
 * Returns true is the param is undefined or null or empty
 * 
 * @param {any}
 *        param
 * @return {boolean}
 */
function ssapiHasNoValue(param) {
    if (typeof param == 'undefined') {
        return true;
    }
    if (param === null) {
        return true;
    }
    if (param === '') {
        return true;
    }
    return false;
}





/**
 * Returns true is the param is undefined or null or empty
 * 
 * @param {any}
 *        param
 * @return {boolean}
 */
function ssapiHasValue(param)
{
    return !ssapiHasNoValue(param);
}





/**
 * An entity here is composed of multiple name-value pairs. Example entity:
 * {name: 'teddy', age: 33}
 * 
 * @param {nlobjSearchResult[]}
 *        results
 * @return {object[]} An array of entities
 */
function ssapiConvertResultsToEntities(results) {
    var logger = new ssobjLogger('ssapiConvertResultsToEntities', true);

    if (results === null) {
        logger.log('results === null');
        return null;
    }
    var searchColumns = results[0].getAllColumns();
    var columnIds = [];
    for (var m = 0; m < searchColumns.length; m++) {
        columnIds.push(searchColumns[m].getName());
    }
    var entities = [];
    for ( var i in results) {
        if (i == 'indexOf') {
            // TODO: Replace
            // for (var i in results)
            // with
            // for (var i = 0; i < results.length; i++)
            continue;
        }
        var result = results[i];
        var entity = {};
        for (var j = 0; j < columnIds.length; j++) {
            var columnId = columnIds[j];
            entity[columnId] = result.getValue(columnId);
        }
        entities.push(entity);
    }
    logger.log('entities.length=' + entities.length);
    return entities;
}

/**
 * Returns the internal idof a record type given the script id
 * 
 * @param {string}
 *        scriptid
 * @return {string}
 */
function getScriptID(scriptid) {
    // create filter for script id
    var filters = new Array();
    filters.push(new nlobjSearchFilter('scriptid', null, 'is', scriptid));
    var searchResults = nlapiSearchRecord('script', null, filters);

    if (searchResults != null) {
        return searchResults[0].getId();
    }

    return -1;
}

/**
 * Gets the list of selected scripts that are active and saves them in a
 * temporary file
 * 
 * @param {Object}
 *        scripts
 */
function getSuiteSocialScriptActiveStatus(scripts) {
    var strScriptStatus = "";
    if (scripts.length != 0) {
        for (var i = 0; i < scripts.length; i++) {
            var filters = new Array();
            filters.push(new nlobjSearchFilter('scriptid', null, 'is', scripts[i]));
            var columns = new Array();
            columns.push(new nlobjSearchColumn('isinactive'));
            var searchResults = nlapiSearchRecord('script', null, filters, columns);
            if (searchResults.length == 1) {
                strScriptStatus += scripts[i];
                strScriptStatus += ',' + searchResults[0].getValue('isinactive') + ',';
            }
        }
    }
    var state_file = nlapiCreateFile('suitesocial_script_active_status.txt', 'PLAINTEXT', strScriptStatus);
    state_file.setFolder(-16);
    nlapiSubmitFile(state_file);
}

/**
 * Returns an array of scripts status stored in a file
 */
function getScriptActiveStatus() {
    var searchContent = nlapiSearchGlobal('suitesocial_script_active_status');
    var fileContent = "";
    if (searchContent != null && searchContent.length > 0) {
        for (var i = 0; i < searchContent.length; i++) {
            var searchresult = searchContent[i];
            var record = searchresult.getId();
            var rectype = searchresult.getRecordType();
            if (rectype == 'file') {
                var objFile = nlapiLoadFile(record);
                fileContent = objFile.getValue();
                break;
            }
        }
        var script_status = fileContent.split(',');
        nlapiLogExecution('DEBUG', 'script_status', script_status);
        return script_status;
    }
    return null;
}

/**
 * Sets Inactive field value of scripts to Y
 * 
 * @param {Object}
 *        scripts
 * @param {Object}
 *        active_status
 */
function inactivateSuiteSocialScripts(scripts, active_status) {
    if (scripts.length != 0) {
        for (var i = 0; i < scripts.length; i++) {
            // activate/inactivate suitesocial autopost user event (applicable
            // to all records)
            var id = getScriptID(scripts[i]);
            if (id != -1) {
                var autopost_script = nlapiLoadRecord('script', id);
                autopost_script.setFieldValue('isinactive', active_status[i]);
                nlapiSubmitRecord(autopost_script);
            }
        }
    }
}

/**
 * Sets Inactive field value of scripts to N
 * 
 * @param {Object}
 *        script_status
 */
function reactivateSuiteSocialScripts(script_status) {
    var scripts = new Array();
    var active_status = new Array();
    if (script_status.length != 0) {
        for (var i = 0; i < script_status.length - 1; i = i + 2) {
            scripts.push(script_status[i]);
            active_status.push(script_status[i + 1]);
        }
        inactivateSuiteSocialScripts(scripts, active_status);
    }
}

/**
 * Deletes the temporary files used instoring script status information
 */
function deleteSuiteSocialScriptActiveStatusFile() {
    var searchContent = nlapiSearchGlobal('suitesocial_script_active_status');
    if (searchContent != null && searchContent.length > 0) {
        for (var i = 0; i < searchContent.length; i++) {
            var searchresult = searchContent[i];
            var record = searchresult.getId();
            var rectype = searchresult.getRecordType();
            if (rectype == 'file') {
                nlapiDeleteFile(record);
                break;
            }
        }
    }
}

/**
 * Generates a random number for news feed name
 * 
 * @param (Optional)
 *        {Integer} userId
 * @returns {String}
 */
function ssapiGetNewsFeedRandomNumber(userId) {
    // generate random number using datetime, math functions, and user id
    var random = (new Date()).getTime() + Math.floor((Math.random() * 1000) + 1);
    return random.toString() + (userId || nlapiGetUser()).toString() + 'nf';
}

/**
 * Regular expression for ssapiAddLinks method
 */
var ADD_LINKS_RE = new RegExp('(https?://|\\bwww\\.)[-A-Z0-9/~*?.,;:%=_$&\'"@!)(\\]\\[+]+[A-Z0-9/~*%=_$&\'"@(\\]\\[+]+', 'ig');
/**
 * Add links to a message
 * 
 * @param {String}
 *        rawText
 * @returns {String}
 */
function ssapiAddLinks(rawText) {
    var last = 0;
    var buf = [];
    while ((m = ADD_LINKS_RE.exec(rawText)) != null) {
        buf.push(rawText.substring(last, m.index));
        buf.push('<a href="');
        if (m[1][0] == 'w')
            buf.push('http://');
        buf.push(m[0]);
        buf.push('" target="_blank">');
        buf.push(m[0]);
        buf.push('</a>');
        last = ADD_LINKS_RE.lastIndex;
    }
    if (rawText != null)
        buf.push(rawText.substring(last));
    return buf.join('');
}

/**
 * Return the internal record type for this SuiteSocial type and id
 * 
 * @param record_type
 */
function ssapiGetInternalRecordType(baseRecordType, record_id) {
    var recordScriptId = '';
    switch (baseRecordType) {
    case 'customer':
        recordScriptId = nlapiLookupField("customer", record_id, "stage", false);
        break;

    case 'item':
        // check if Serialized Inventory and Lot Tracking are enabled
        var isSerialInvFeatureEnabled = nlapiGetContext().getFeature("serializedinventory");
        var isLotTrackingFeatureEnabled = nlapiGetContext().getFeature("lotnumberedinventory");

        // set column fields to return
        var columns = [];
        // add type
        columns.push("type");
        // add isserialitem if feature is enabled
        if (isSerialInvFeatureEnabled) {
            columns.push("isserialitem");
        }
        // add islotitem if feature is enabled
        if (isLotTrackingFeatureEnabled) {
            columns.push("islotitem");
        }

        var arrItemFlds = nlapiLookupField("item", record_id, columns, false);
        var itemType = arrItemFlds["type"];
        var isSerialItem = isSerialInvFeatureEnabled ? arrItemFlds["isserialitem"] : 'F';
        var isLotItem = isLotTrackingFeatureEnabled ? arrItemFlds["islotitem"] : 'F';

        switch (itemType.toString()) {
        case 'InvtPart':
            // inventory
            recordScriptId = 'inventoryitem';
            if (isSerialItem == 'T')
                recordScriptId = 'serializedinventoryitem';
            else if (isLotItem == 'T')
                recordScriptId = 'lotnumberedinventoryitem';
            break;

        case 'Assembly':
            // assembly
            recordScriptId = 'assemblyitem';
            if (isSerialItem == 'T')
                recordScriptId = 'serializedassemblyitem';
            else if (isLotItem == 'T')
                recordScriptId = 'lotnumberedassemblyitem';
            break;

        case 'Service':
            recordScriptId = 'serviceitem';
            break;

        case 'DwnLdItem':
            recordScriptId = 'downloaditem';
            break;

        case 'NonInvtPart':
            recordScriptId = 'noninventoryitem';
            break;

        case 'OthCharge':
            recordScriptId = 'otherchargeitem';
            break;

        default:
            throw 'specific item type not found itemType=' + itemType;
        }
        break;

    case 'transaction':
        recordScriptId = nlapiLookupField("transaction", record_id, "recordtype", false);
        break;

    default:
        recordScriptId = baseRecordType;
    }
    return (recordScriptId || '').toLowerCase();
}

/**
 * returns the numeric id given the script id of the custom record
 * 
 * @param {String}
 *        record_type The script id of the custom record
 * @return {Integer} The numeric id of the custom record
 */
function ssapiGetCustomTypeInternalId(record_type) {
    var filters = new Array();
    var columns = new Array();
    filters[0] = new nlobjSearchFilter("scriptid", null, "is", record_type);
    columns[0] = new nlobjSearchColumn("internalid");
    return nlapiSearchRecord("customrecordtype", null, filters, columns)[0].getValue(columns[0]);
}





/**
 * Gets the HTML needed to make the quickview work. Note that the onmouseover
 * event markup should be included once the page is rendered. It seems, it will
 * not work if you add the mouseover event dynamically as in Ext templating.
 * 
 * @param {Object}
 *        internal_record_type
 * @param {Object}
 *        feed_entry_record_id
 */
function ssapiGetQuickViewMarkup(baseRecordType, feed_entry_record_id)
{
    // ensure that base record type and record id has value
    if (ssapiHasNoValue(baseRecordType) || ssapiHasNoValue(feed_entry_record_id))
    {
        return '';
    }

    // check if record type is among those with quickview support as in the array below
    baseRecordType = baseRecordType.toLowerCase();
    var recordTypeScriptId = ssapiGetInternalRecordType(baseRecordType, feed_entry_record_id);
    var with_quickview_support = ['assemblybuild', 'assemblyitem', 'assemblyunbuild', 'calendarevent', 'campaign', 'contact', 'customer', 'descriptionitem', 'discountitem', 'employee', 'estimate', 'inventoryitem', 'issue', 'itemgroup', 'kititem', 'lead', 'lotnumberedassemblyitem', 'lotnumberedinventoryitem', 'markupitem', 'noninventoryitem', 'opportunity', 'otherchargeitem', 'partner', 'paymentitem', 'phonecall', 'project', 'promotioncode', 'prospect', 'serializedassemblyitem',
            'serializedinventoryitem', 'serviceitem', 'solution', 'subtotalitem', 'supportcase', 'task', 'timebill', 'vendor'];

    var isSupportedStandard = with_quickview_support.indexOf(recordTypeScriptId) > -1;
    var isCustomRecord = baseRecordType.indexOf('customrecord') === 0; // check if custom record
    
    // if record type is not supported then exit
    if (!(isSupportedStandard || isCustomRecord))
    {
        return "";
    }

    // Substitute type
    var SUBST = {
        inventoryitem: "item",
        noninventoryitem: "item",
        downloaditem: "item",
        customer: "entity",
        competitor: "entity"
    };

    // form the quickview
    var rand = Math.floor(Math.random() * 1000); // make id of quickview
    
    // element unique
    var p = {
        feedEntryRecId: feed_entry_record_id,
        toolTipId: baseRecordType + feed_entry_record_id + '_' + rand,
        quickViewRecordId: isCustomRecord ? ssapiGetCustomTypeInternalId(baseRecordType) : baseRecordType in SUBST ? SUBST[baseRecordType] : baseRecordType
    };

    var htmQVIcon = '<a href="#" id="qsTarget_custom{toolTipId}" class="dottedlink" onmouseover="var win = window; if (typeof win.getExtTooltip != undefined) { var tip = win.getExtTooltip(\'qsTarget_custom{toolTipId}\', \'{quickViewRecordId}\', \'DEFAULT_TEMPLATE\', {feedEntryRecId}, null); } if (tip != undefined) { tip.onTargetOver(event) }"><img width="10px" height="10px" border="0" src="/images/hover/icon_hover.png?v=2012.1.0" alt="" style="vertical-align:middle; padding-left: 2px" /></a>';

    return _renderTemplate(htmQVIcon, p);
}





/**
 * Render string given a template and data object
 * 
 * @param template
 *        {string}
 * @param ds
 *        {object}
 * @returns {string}
 */
function _renderTemplate(template, ds) {
    var content = template;

    for ( var i in ds) {
        var re = new RegExp("\\{" + i + "\\}", "g");
        content = content.replace(re, ds[i]);
    }

    return content;
}

/**
 * Converts message with mentions into html to render links to records mentioned
 * 
 * @param message
 * @param id
 * @param showQuickView
 * @returns {String}
 */
function ssapiConvertMessageWithMentions(message, id, showQuickView) {
    var logger = new ssobjLogger(arguments, false, 'ssapiConvertMessageWithMentions');
    // check if showQuickView has no value
    if (ssapiHasNoValue(showQuickView)) {
        showQuickView = true;
    }

    var regex = /{{(.*?)}}/g;
    var matches = message.match(regex);
    if (matches !== null) {
        for (var m = 0; m < matches.length; m++) {
            var actualValue = '';
            var objOriginalStr = matches[m];
            var objNew = JSON.parse(objOriginalStr.replace('{{', '{').replace('}}', '}'));
            // logger.log('objNew=' + JSON.stringify(objNew));

            // check object type, and do specific action
            if (objNew.objecttype == 'image') {
                var imageUrl = nlapiResolveURL("mediaitem", objNew.id);
                actualValue = '<br />' + '<img src=' + imageUrl + ' />';
                // } else if (objNew.objecttype == 'search' || typeof
                // objNew.objecttype == 'undefined') {
                // var chartId = 'socialChart' + id;
                // actualValue += '<a target="_blank" title="Opens the saved
                // search results in a new page"
                // href=/app/common/search/searchresults.nl?searchid=' +
                // objNew.id + '>' + objNew.name + '</a>';
                // actualValue += '<div id=' + chartId + ' class="socialchart"
                // style="z-index: 2"></div>';
            } else if (objNew.objecttype == 'record') {
                var recordName = objNew.name;// keep for further improvement:
                // ssapiGetRecordNameWall(objNew.recordtype,
                // objNew.id, null,
                // objNew.name);
                actualValue = '<a href="' + objNew.url + '" >' + recordName + '</a>';
                // if set to show, show quick view
                if (showQuickView) {
                    var quickview = ssapiGetQuickViewMarkup(objNew.recordtype, objNew.id);
                    actualValue += quickview;
                }
            }

            // replace tags [br] with <br>
            message = message.replace(/\[br\]/g, '<br>');
            // replace original string with the actual value
            message = message.replace(objOriginalStr, actualValue);
        }
        return message;
    }
    return message;
}

/**
 * Converts message with hashtags into formatted hashtags
 * 
 * @param message
 * @returns {String}
 */
function ssapiConvertMessageWithHashtags(message) {
    var logger = new ssobjLogger(arguments, false, 'ssapiConvertMessageWithHashtags');
    try {
        // newsfeed url
        var newsFeedUrl = ssapiGetDataCenterURL() + nlapiResolveURL('suitelet', 'customscript_suitewall', 'customdeploy_suitewall');

        var regex = /#([a-z0-9]+)/gi;
        var matches = message.match(regex);
        if (matches !== null) {
            for (var m = 0; m < matches.length; m++) {
                var objOriginalStr = matches[m].trim();
                var url = newsFeedUrl + '&hashtag=' + objOriginalStr.substring(1);
                var actualValue = '<a class="ss_hashtag" href="' + url + '" onclick="socialHashtagClicked(\'' + objOriginalStr + '\'); return false;">' + objOriginalStr + '</a>';

                // replace original string with the actual value
                message = message.replace(objOriginalStr, actualValue);
            }
        }
    } catch (e) {
        ssapiHandleError(e);
    }
    return message;
}

/**
 * Format message for wall posting
 * 
 * @param {String}
 *        message
 * @param {String}
 *        id
 * @param (Optional)
 *        {String} showQuickView
 * @returns {String}
 */
function ssapiFormatMessageForPosting(message, id, showQuickView /* Optional */) {
    // replace < and > with &lt; and &gt;
    message = message.replace(new RegExp('<', 'g'), "&lt;");
    message = message.replace(new RegExp('>', 'g'), "&gt;");

    // Add hyperlinks, user and
    // insert HTML newlines
    message = ssapiAddLinks(message).replace(/\r*\n\r*/g, "<br>");

    // replace tags [br] with <br>
    message = message.replace(/\[br\]/g, '<br>');

    // replace objects in message with actual values
    message = ssapiConvertMessageWithMentions(message, id, showQuickView);

    // format hashtags
    message = ssapiConvertMessageWithHashtags(message);

    return message;
}

/**
 * Reformats hashtags for saving into the search field
 * 
 * @param message
 * @returns {String}
 */
function ssapiReformatHashtag(message) {
    var logger = new ssobjLogger(arguments, false, 'ssapiReformatHashtag');

    var regex = /(^#|[^&]#)([a-z0-9]+)/gi;
    var matches = message.match(regex);
    if (matches !== null) {
        for (var m = 0; m < matches.length; m++) {
            var objOriginalStr = matches[m].trim();
            // remove # from text
            var noHashString = objOriginalStr.slice(1);
            // new format to be saved
            var actualValue = objOriginalStr + ' [hash_' + noHashString + '_tag]';

            // replace original string with the actual value
            message = message.replace(objOriginalStr, actualValue);
        }
    }
    return message;
}





/**
 * Gets the base/parent record type such as transaction, customer
 * 
 * @param {Object}
 *        record_type Such as purchaseorder, salesorder
 */
function ssapiGetBaseRecordType(recordScriptId)
{
    if (recordScriptId == null)
    {
        return "";
    }

    // for transaction
    if (recordScriptId == "vendorbill" || recordScriptId == "estimate" || recordScriptId == "salesorder" || recordScriptId == "invoice" || recordScriptId == "cashrefund" || recordScriptId == "cashsale" || recordScriptId == "check" || recordScriptId == "creditmemo" || recordScriptId == "itemfulfillment" || recordScriptId == "itemreceipt" || recordScriptId == "journalentry" || recordScriptId == "paymentitem" || recordScriptId == "purchaseorder" || recordScriptId == "returnauthorization"
            || recordScriptId == "vendorcredit" || recordScriptId == "vendorpayment" || recordScriptId == "vendorreturnauthorization" || recordScriptId == "paymentitem" || recordScriptId == "workorder")
    {
        return "transaction";
    }

    // for item
    if (recordScriptId == "assemblyitem" || recordScriptId == "serviceitem" || recordScriptId == "downloaditem" || recordScriptId == "otherchargeitem" || recordScriptId == "noninventoryitem" || recordScriptId == "inventoryitem" || recordScriptId == "lotnumberedassemblyitem" || recordScriptId == "lotnumberedinventoryitem" || recordScriptId == "serializedassemblyitem" || recordScriptId == "serializedinventoryitem" || recordScriptId == "descriptionitem" || recordScriptId == "discountitem"
            || recordScriptId == "itemgroup" || recordScriptId == "kititem" || recordScriptId == "markupitem" || recordScriptId == "paymentitem" || recordScriptId == "subtotalitem")
    {
        return "item";
    }

    // for customer
    if (recordScriptId == "lead" || recordScriptId == "prospect" || recordScriptId == "customer")
    {
        return "customer";
    }

    // for custom record
    if (recordScriptId.indexOf('customrecord_') === 0)
    {
        return 'customtype';
    }
    
    return recordScriptId;
}





/**
 * Get the actual name of a custom record type
 * 
 * @param record_type
 * @param record_id
 */
function ssapiGetCustomTypeName(record_type) {
    var filters = new Array();
    var columns = new Array();
    filters[0] = new nlobjSearchFilter("scriptid", null, "is", record_type);
    columns[0] = new nlobjSearchColumn("name");
    return nlapiSearchRecord("customrecordtype", null, filters, columns)[0].getValue(columns[0]);
}

ssobjRecordDescriptor = function(type, id, addendum) {
    this.type = type;
    this.id = id;
    this.addendum = addendum;
};





function ssapiGetRecordDescriptor(recordType, recordId)
{
    var logger = new ssobjLogger(arguments, false, 'ssapiGetRecordDescriptor');

    if (recordType == null)
    {
        return "";
    }
    
    try
    {
        var type = recordType.toTitleCase();
        var id;
        var addendum;

        // get base record type for checking on some records
        var baseRecordType = ssapiGetBaseRecordType(recordType);

        // begin checking record type / base record type
        if (recordType == 'competitor')
        {
            addendum = nlapiLookupField(recordType, recordId, "name", false);
        }
        else if (baseRecordType == "item")  // check base record type
        { 
            var rec = nlapiLoadRecord(recordType, recordId);
            type = rec.getFieldValue("itemtypename");
            addendum = rec.getFieldValue("itemid");
        }
        else if (baseRecordType == "transaction" || baseRecordType == "opportunity")  // check base record type
        { 
            var filters = new Array();
            filters[0] = new nlobjSearchFilter("internalid", null, "anyof", recordId);
            filters[1] = new nlobjSearchFilter("mainline", null, "is", "T");
            var columns = new Array();
            columns[0] = new nlobjSearchColumn("type");
            columns[1] = new nlobjSearchColumn("tranid");
            columns[2] = new nlobjSearchColumn("entity");
            var searchresults = nlapiSearchRecord("transaction", null, filters, columns);
            if (searchresults === null)
            {
                return new ssobjRecordDescriptor(recordType, recordId, '');
            }
            type = searchresults[0].getText(columns[0]);
            id = searchresults[0].getValue(columns[1]);
            id = id != null && id.length > 0 ? "#" + id : "";
            id = ' ' + id;
            addendum = searchresults[0].getText(columns[2]);
        }
        else if (baseRecordType == "customer" || baseRecordType == "vendor" || baseRecordType == "partner" || baseRecordType == "job")
        { // check base record type check if base record type = customer, then get stage, otherwise get type
            type = baseRecordType == "customer" ? nlapiLookupField("customer", recordId, "stage", true) : nlapiLookupField("entity", recordId, "type", true);
            var arrFlds = nlapiLookupField(baseRecordType, recordId, ["entityid", "companyname"], false);
            id = arrFlds["entityid"];
            if (baseRecordType != "vendor" && baseRecordType != "partner")
                addendum = arrFlds["companyname"];
        }
        else if (recordType == "contact" || recordType == "employee")
        {
            id = nlapiLookupField(recordType, recordId, "entityid");
        }
        else if (recordType == "issue")
        {
            // type = "Issue";
            var arrFlds = nlapiLookupField(recordType, recordId, ["number", "issueabstract"], false);
            id = "#" + arrFlds["number"] + " : " + arrFlds["issueabstract"];
        }
        else if (recordType == "supportcase")
        {
            type = "Case";
            id = "#" + nlapiLookupField(recordType, recordId, "number", false);
            addendum = nlapiLookupField(recordType, recordId, "company", true);
        }
        else if (recordType == "campaign")  // type = "Campaign";
        {
            var arrFlds = nlapiLookupField(recordType, recordId, ["campaignid", "title"], false);
            id = "#" + arrFlds["campaignid"];
            addendum = arrFlds["title"];
        }
        else if (recordType == "phonecall")
        {
            type = "Phone Call";
            var arrFlds = nlapiLookupField(recordType, recordId, ["internalid", "title"], false);
            id = "#" + arrFlds["internalid"];
            addendum = arrFlds["title"];
        }
        else if (recordType == "calendarevent")
        {
            type = "Event";
            var arrFlds = nlapiLookupField(recordType, recordId, ["internalid", "title"], false);
            id = "#" + arrFlds["internalid"];
            addendum = arrFlds["title"];
            if (id == "#null")
                id = "(permission needed for access)";
        }
        else if (recordType == "task")
        {
            // type = "Task";
            var arrFlds = nlapiLookupField(recordType, recordId, ["internalid", "title"], false);
            id = "#" + arrFlds["internalid"];
            addendum = arrFlds["title"];
        }
        else if (recordType.indexOf("customrecord") === 0)
        {
            type = ssapiGetCustomTypeName(recordType);
            var r = nlapiLoadRecord(recordType, recordId);
            id = ssapiHasValue(r.getFieldValue('name')) ? ' ' + r.getFieldValue('name') : ' with ID ' + r.getId();
        }
        else if (recordType == "search")
        {
            return new ssobjRecordDescriptor("Results", null);
        }

        return new ssobjRecordDescriptor(type, id, addendum);
    }
    catch (e)
    {
        if (e.getCode)
        {
            if (e.getCode() == 'RCRD_DSNT_EXIST')
            {
                // record has been deleted
                return '';
            }
        }
        return '';
    }
}





/**
 * Get the text record name for a particular type and id
 * 
 * @param record_type
 * @param record_id
 */
function ssapiGetRecordNameWall(recordType, recordId, withRecordType, defaultName) {
    var logger = new ssobjLogger(arguments, false, 'ssapiGetRecordNameWall');
    try {
        // S3 Issue 240166 : [SuiteSocial] Display the transaction number
        // instead of the internal id in the UI
        // var baseRecordType = ssapiGetBaseRecordType(recordType);
        // // Issue: 224847 [SuiteSocial] Handle newsfeed entries whose
        // underlying record has been deleted
        var descriptor = ssapiGetRecordDescriptor(recordType, recordId);
        if (descriptor === '') {
            // record has been deleted, or an exception may have occurred
            return ssapiHasValue(defaultName) ? defaultName : recordType + ' ' + recordId;
        }

        var name = (withRecordType ? descriptor.type : '') + (descriptor.id || '' !== '' ? " " + descriptor.id : " ") + (descriptor.addendum || '' !== '' ? ": " + descriptor.addendum : "");
        if (name.trim() === '') {
            name = ssapiGetRecordNameByScriptId(recordType) + ' : ' + recordId;
        }
        return name;

    } catch (e) {
        suitesocial.Helper.handleError(e, '', arguments);
        return '';
    }
}

/**
 * Get the record url for a particular type and id if error occurs, empty url
 * will be returned
 * 
 * @param record_type
 * @param record_id
 */
function ssapiGetRecordUrl(record_type, record_id) {
    var url = '';
    try {
        url = (record_type == "search" ? nlapiResolveURL("TASKLINK", "LIST_SEARCHRESULTS", null, "VIEW") + "?searchid=" + record_id : (record_type != null && record_type.length > 0 ? nlapiResolveURL("RECORD", ssapiGetInternalRecordType(record_type, record_id), record_id, "VIEW") : ""));
    } catch (e) {
        url = '';
    }
    return url;
}

function ssapiGetRecordNameAndLink(record_type, record_id, subscription_type /* Optional */) {
    // initialize object
    var feed_entry = {};
    feed_entry.recordName = "";
    feed_entry.recordNameWithLink = "<a target=\"_top\" href=\"\"></a>";

    // check if record type and id has value or not
    if (ssapiHasValue(record_type) && ssapiHasValue(record_id)) {
        // record name and link
        var recordName = ssapiGetRecordNameWall(record_type, record_id, true);
        var url = ssapiGetRecordUrl(record_type, record_id);
        var quickview = ssapiGetQuickViewMarkup(record_type, record_id);
        feed_entry.url = url;
        feed_entry.recordName = recordName;
        feed_entry.recordNameWithLink = '<a target="_top" href="' + url + '">' + recordName + '</a>' + quickview;

        // check if normal post
        var isNormalPost = (subscription_type || '') != 'savedsearch';
        // Issue: 224847 [SuiteSocial] Handle newsfeed entries whose underlying
        // record has been deleted
        var rowHasBeenDeleted = (recordName === '');
        // internal_record_type is the record script id
        var link = '';
        var internal_record_type = ssapiGetInternalRecordType(record_type, record_id);
        if (ssapiHasValue(internal_record_type)) {
            link = "<a" + " href='" + url + "' target='_parent'  " + '' + ">";
            link += (isNormalPost && !rowHasBeenDeleted) ? recordName : record_type + ' with ID ' + record_id; // EMS:
            // may
            // need
            // to
            // get
            // the
            // descriptive
            // record
            // name
            link += '' + "</a>";
            feed_entry.link = link + quickview;
        }

        // cleanup preceeding code
        if (subscription_type == 'savedsearch') {
            feed_entry.recordName = link;
            feed_entry.recordNameWithLink = link;
        }
    }

    return feed_entry;
}

/**
 * Gets the day of the week (i.e. Monday, Tuesday, ...)
 * 
 * @param {Date}
 *        dt
 * @returns {String}
 */
function ssapiGetDayOfWeek(dt) {
    var days = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    return days[dt.getDay()];
}

/**
 * Gets the locale date time, formatted for SuiteSocial wall post (e.g. Thu
 * 7/31/2014 3:18 pm)
 * 
 * @param {Date}
 *        dt
 * @returns {String}
 */
function ssapiGetLocaleDateTime(dt) {
    // get day of the week
    var strDayOfWeek = ssapiGetDayOfWeek(dt);
    strDayOfWeek = strDayOfWeek.substr(0, 3); // get abbreviated day (Thursday
    // ==> Thu)

    // get date/time (format based on user preferences)
    var strDateTime = nlapiDateToString(dt, "datetime");

    return strDayOfWeek + " " + strDateTime;
}





/**
 * Checks if the file is supported for newsfeed image preview.
 * 
 * @param {String}
 *        extName Extension name of the file
 * @returns {Boolean} Returns true if supported image, otherwise, false
 */
function ssapiIsSupportedImage(extName)
{
    if (ssobjGlobal.SUPPORTED_IMAGES == null)
    {
        ssobjGlobal.SUPPORTED_IMAGES = [];
    }
    
    return (ssobjGlobal.SUPPORTED_IMAGES.indexOf(extName) > -1);
}





/**
 * Gets the File Download URL via the Suitelet.
 * 
 * @returns {String} Returns the url
 */
function ssapiGetFileDownloadUrl() {
    if (ssapiHasNoValue(ssobjGlobal.dndFileDownloadSuiteletUrl)) {
        ssobjGlobal.dndFileDownloadSuiteletUrl = nlapiResolveURL("SUITELET", "customscript_ss_dnd_file_dl_suitelet", "customdeploy_ss_dnd_file_dl_suitelet");
    }
    return ssobjGlobal.dndFileDownloadSuiteletUrl;
}

/**
 * Gets the appropriate HTML to render the file on the newsfeed post.
 * 
 * @param {String}
 *        postId The internal id or the name of the newsfeed post
 * @param {Object}
 *        dadFile File information of the uploaded file
 * @param {Object}
 *        dndElements Set of drag-and-drop element IDs
 * @param {Boolean}
 *        useFileLink If true, file link is returned even if image preview is
 *        enabled
 * @param {String}
 *        parentId The parent's internal id of the post
 * @returns {String} Returns the HTML text
 */
function ssapiGetFileHTMLToRender(postId, dadFile, dndElements, useFileLink, parentId) {
    var url = ssapiGetFileDownloadUrl() + "&custparam_postid=" + postId + (!ssapiIsImagePreviewEnabled() ? "&custparam_orig=T" : "");

    // check if forced to use file link
    if (ssapiHasNoValue(useFileLink)) {
        useFileLink = false;
    }

    // check if client or server side
    var bIsClient = ssapiHasValue(dndElements);

    // form file html based on file extension
    if (ssapiIsSupportedImage(dadFile.extName) && ssapiIsImagePreviewEnabled() && !useFileLink) {
        // get url for orig file
        var urlOrigFile = url + "&custparam_orig=T";
        // if on client side, use the canvas url
        if (bIsClient) {
            url = Ext.get(dndElements.canvasId).dom.toDataURL("image/jpeg");
            return '<br/><div data-file="' + urlOrigFile + '" style="max-width: 600px"><img src="' + url + '" style="max-width: 100%; border: 1px solid #d8d8d8; cursor: pointer;" onClick="socialShowImageGalleryPopup(\'' + urlOrigFile + '\', \'' + postId + '\', ' + parentId + ')"/></div>';
        }
        return '<br/><div data-file="' + urlOrigFile + '" style="max-width: 600px"><img data-processed="false" data-src="' + url + '" style="max-width: 100%; border: 1px solid #d8d8d8; visibility: hidden; cursor: pointer;" onClick="socialShowImageGalleryPopup(\'' + urlOrigFile + '\', \'' + postId + '\', ' + parentId + ')" /></div>';
    } else {
        var bgUrl = "";
        var imgUrl = "";

        // check if useFileLink is true
        if (useFileLink) {
            // use separate image files for file icons
            imgUrl = ssapiGetFileTypeIconForEmail(dadFile.extName);
        } else {
            // use sprite image
            bgUrl = ssapiGetFileTypeIcon(dadFile.extName, ssapiGetSpriteImageUrl(bIsClient));
            imgUrl = ssapiGetTransparentImageUrl(bIsClient);
        }

        var html = '<br/><table cellpadding="0" cellspacing="0"><tr><td>';
        html += '<a href="' + url + '" target="_blank" style="text-decoration: none;">';
        html += '<img src="' + imgUrl + '" style="background: ' + bgUrl + '; height: 22px; width: 22px; vertical-align: middle;" />';
        html += '</a>';
        html += '</td><td>';
        html += '<a href="' + url + '" target="_blank" style="text-decoration: none; height: 22px; padding-top: 2px; color: #666666; font-size: 12px; font-weight: 600; font-family: Open Sans,Helvetica,sans-serif;">';
        html += dadFile.fileName + '</a>';
        html += '</td></tr></table>';
        return html;
    }
}

/**
 * Gets the url of the sprite image of file type icons.
 * 
 * @param {Boolean}
 *        bIsClient Set to true if on client side, otherwise false
 * @returns {String} Returns the url
 */
function ssapiGetSpriteImageUrl(bIsClient) {
    if (bIsClient) {
        return ssobjGlobal.imagesUrlsObj['file-type-sprite.png'];
    } else {
        if (ssapiHasNoValue(ssobjGlobal.bundlePath)) {
            ssobjGlobal.bundlePath = suitesocial.Helper.getBundlePathURL();
        }
        return ssapiGetDataCenterURL() + ssobjGlobal.bundlePath + '/images/file-type-sprite.png';
    }
}

/**
 * Gets the url of the transparent image that is used with sprite images.
 * 
 * @param {Boolean}
 *        bIsClient Set to true if on client side, otherwise false
 * @returns {String} Returns the url
 */
function ssapiGetTransparentImageUrl(bIsClient) {
    if (bIsClient) {
        return ssobjGlobal.imagesUrlsObj['img_trans.gif'];
    } else {
        if (ssapiHasNoValue(ssobjGlobal.bundlePath)) {
            ssobjGlobal.bundlePath = suitesocial.Helper.getBundlePathURL();
        }
        return ssapiGetDataCenterURL() + ssobjGlobal.bundlePath + '/images/img_trans.gif';
    }
}

/**
 * Gets the url of the sprite image with the specified position that identifies
 * the file type icon to display.
 * 
 * @param {String}
 *        extName Extension name of the file
 * @param {String}
 *        spriteImgUrl URL of the sprite image
 * @returns {String} Returns the url
 */
function ssapiGetFileTypeIcon(extName, spriteImgUrl) {
    extName = extName.toLowerCase();
    var bgUrl = "url(" + spriteImgUrl + ") ";

    if (extName == ".htm" || extName == ".html")
        return bgUrl + " -2px -4px";
    else if (extName == ".xml")
        return bgUrl + " -2px -32px";
    else if (extName == ".pdf")
        return bgUrl + " -2px -59px";
    else if (extName == ".zip" || extName == ".rar")
        return bgUrl + " -2px -86px";
    else if (extName == ".xls" || extName == ".xlsx")
        return bgUrl + " -2px -114px";
    else if (extName == ".doc" || extName == ".docx")
        return bgUrl + " -2px -141px";
    else if (extName == ".ppt" || extName == ".pptx")
        return bgUrl + " -2px -169px";
    else if (extName == ".txt")
        return bgUrl + " -2px -224px";
    else if (extName == ".jpg" || extName == ".jpeg" || extName == ".png" || extName == ".bmp")
        return bgUrl + " -2px -251px";
    else
        return bgUrl + " -2px -197px"; // generic
}

/**
 * Gets the url of the specific image for the file type icon to display. This is
 * used in the email notification's file link.
 * 
 * @param {String}
 *        extName Extension name of the file
 * @returns {String} Returns the url
 */
function ssapiGetFileTypeIconForEmail(extName) {
    extName = extName.toLowerCase();

    // get images path
    if (ssapiHasNoValue(ssobjGlobal.bundlePath)) {
        ssobjGlobal.bundlePath = suitesocial.Helper.getBundlePathURL();
    }
    var imagesPath = ssapiGetDataCenterURL() + ssobjGlobal.bundlePath + '/images/';

    // get appropriate image file
    if (extName == ".htm" || extName == ".html")
        return imagesPath + "file_type_html.png";
    else if (extName == ".xml")
        return imagesPath + "file_type_xml.png";
    else if (extName == ".pdf")
        return imagesPath + "file_type_pdf.png";
    else if (extName == ".zip" || extName == ".rar")
        return imagesPath + "file_type_compressed.png";
    else if (extName == ".xls" || extName == ".xlsx")
        return imagesPath + "file_type_excel.png";
    else if (extName == ".doc" || extName == ".docx")
        return imagesPath + "file_type_word.png";
    else if (extName == ".ppt" || extName == ".pptx")
        return imagesPath + "file_type_powerpoint.png";
    else if (extName == ".txt")
        return imagesPath + "file_type_text.png";
    else if (extName == ".jpg" || extName == ".jpeg" || extName == ".png" || extName == ".bmp")
        return imagesPath + "file_type_image.png";
    else
        return imagesPath + "file_type_generic.png"; // generic
}

/**
 * Checks if image preview setting is enabled.
 * 
 * @returns {Boolean} Returns true if enabled, otherwise false. (Not T or F)
 */
function ssapiIsImagePreviewEnabled() {
    // check enable image preview setting
    if (ssapiHasNoValue(ssobjGlobal.imagePreviewEnabled)) {
        ssobjGlobal.imagePreviewEnabled = nlapiGetContext().getSetting("SCRIPT", "custscript_ss_enable_image_preview");
    }
    return ssobjGlobal.imagePreviewEnabled == "T";
}




/**
* Return setting value by id.
* 
* @returns setting value
*/
function ssapiGetSetting(settingId)
{
    ssobjGlobal.Settings = ssobjGlobal.Settings || {};
    
    if (settingId == "custscript_ss_allow_customer_upload")
    {
        ssobjGlobal.Settings[settingId] = nlapiGetContext().getSetting("SCRIPT", settingId) == "T";
        return ssobjGlobal.Settings[settingId];
    }

    if (settingId == "custscript_ss_enable_image_preview")
    {
        ssobjGlobal.Settings[settingId] = nlapiGetContext().getSetting("SCRIPT", settingId) == "T";
        return ssobjGlobal.Settings[settingId];
    }

    if (settingId == "custscript_ss_use_suitesocial_font")
    {
        ssobjGlobal.Settings[settingId] = nlapiGetContext().getSetting("SCRIPT", settingId) == "T";
        return ssobjGlobal.Settings[settingId];
    }

    return undefined;
}





/**
 * Gets the folder id of suitesocial src folder
 * 
 * @returns {number}
 */
function ssapiGetSrcFolderId() {
    var key = arguments.callee.name;
    var folderId = ssobjGlobal[key];
    if (ssapiHasValue(folderId)) {
        return folderId;
    }
    var filters = [];
    var columns = [ new nlobjSearchColumn('folder') ];
    filters.push([ 'name', 'is', '5dc7bc3b-9201-41c2-a0c0-860a01cc22b7.txt' ]);
    var results = nlapiSearchRecord('file', null, filters, columns);
    if (results === null) {
        throw 'results ===null. File unique to SuiteSocial not found';
    }
    var folderId = results[0].getValue('folder');
    ssobjGlobal[key] = folderId;
    return folderId;

}

/**
 * Returns an array with unique results
 * 
 * @param []
 *        results - search results
 * @returns {Array}
 */
function ssapiGetUniqueResults(results) {
    var map = {}, r = [];
    var count = results.length;
    for (var i = 0; i < count; i++) {
        var result = results[i];
        var key = result.getId();
        if (!map[key]) {
            map[key] = true;
            r.push(result);
        }
    }
    return r;
}






/**
 * Remove items in from one array that exists in another array
 * 
 * @param {[]}
 *        mainItems - items where items will be removed
 * @param {[]}
 *        forRemovalItems - array that contains the items to be removed
 * @returns {[]} - items from mainItems after items from forRemovalItems have
 *          been removed
 */
function ssapiRemoveItems(mainItems, forRemovalItems)
{
    return mainItems.filter(function(item)
    {
        if (forRemovalItems == null)
        {
            return true;
        }
        
        return forRemovalItems.indexOf(item) < 0;
    });
}





/**
 * Gets the data center URL (e.g. https://system.netsuite.com).
 * 
 * @returns {String}
 */
function ssapiGetDataCenterURL()
{
    // check if already has value in global
    if (ssapiHasValue(ssobjGlobal.dataCenterUrl))
    {
        return ssobjGlobal.dataCenterUrl;
    }

    // get the external url of the suitelet
    var url = nlapiResolveURL("SUITELET", "customscript_suitewall", "customdeploy_suitewall", true);
    if (url == null)
    {
        return "";
    }

    // get http or https
    var index = url.indexOf("://");
    var http = url.substr(0, index) + "://";

    // index of domain's first character
    var indexStart = index + 3;

    // get data center
    var indexDot = url.indexOf(".", indexStart);
    var indexEnd = url.indexOf("/", indexStart);
    var dataCenter = url.substring(indexDot, indexEnd);
    var dataCenterUrl = http + "system" + dataCenter;
    
    // save to global
    ssobjGlobal.dataCenterUrl = dataCenterUrl;
    
    return dataCenterUrl;
}





/**
 * Gets the position of all occurrences of a search string in a given string.
 * 
 * @param {String}
 *        searchStr
 * @param {String}str
 * @param {Boolean}
 *        caseSensitive
 * @returns {Array} indices - The indices of all occurrences of the search
 *          string
 */
function ssapiGetIndicesOf(searchStr, str, caseSensitive)
{
    if (str == null)
    {
        return [];
    }

    var startIndex = 0;
    var searchStrLen = searchStr.length;
    var index;
    var indices = [];
    
    if (!caseSensitive)
    {
        str = str.toLowerCase();
        searchStr = searchStr.toLowerCase();
    }
    
    while ((index = str.indexOf(searchStr, startIndex)) > -1)
    {
        indices.push(index);
        startIndex = index + searchStrLen;
    }
    
    return indices;
}





/**
 * Gets the external URL of the generic profile photo
 * 
 * @returns {string} Sample:
 *          https://system.netsuite.com/core/media/media.nl?id=15298&c=3684263&h=d75cc6162c0effc844aa
 * 
 */
function ssapiGetGenericPhotoURL() {
    var key = arguments.callee.name + 'Return';
    if (ssapiHasValue(ssobjGlobal[key])) {
        return ssobjGlobal[key];
    }

    var fileName = 'suitesocial_generic_person.jpg';
    var results = nlapiSearchRecord('file', null, [ [ 'name', 'is', fileName ] ]);
    if (results === null) {
        throw 'results ===null; fileName not found; fileName=' + fileName;
    }
    if (results.length > 1) {
        throw 'results.length > 1; fileName=' + fileName;
    }
    ssobjGlobal[key] = ssapiGetDataCenterURL() + nlapiResolveURL('mediaitem', results[0].getId());
    return ssobjGlobal[key];
}

/**
 * Gets the external URL of the system profile photo
 * 
 * @returns {string} Sample:
 *          https://system.netsuite.com/core/media/media.nl?id=15298&c=3684263&h=d75cc6162c0effc844aa
 */
function ssapiGetSystemPhotoURL() {
    var key = arguments.callee.name + 'Return';
    if (ssapiHasValue(ssobjGlobal[key])) {
        return ssobjGlobal[key];
    }

    var fileName = 'suitesocial_gear.jpg';
    var results = nlapiSearchRecord('file', null, [ [ 'name', 'is', fileName ] ]);
    if (results === null) {
        throw 'results ===null; fileName not found; fileName=' + fileName;
    }
    if (results.length > 1) {
        throw 'results.length > 1; fileName=' + fileName;
    }
    ssobjGlobal[key] = ssapiGetDataCenterURL() + nlapiResolveURL('mediaitem', results[0].getId());
    return ssobjGlobal[key];
}

/**
 * Gets the likers and likes count info for display.
 * 
 * @param {integer}
 *        count - number of likers
 * @returns {String}
 */
function ssapiGetLikersDisplay(likes, likeType) {
    // likes count
    var count = likes.length;

    // check if current user is among the likers
    var isUserLiker = false;
    for (var i = 0; i < count; i++) {
        var like = likes[i];
        if (like.id == nlapiGetUser()) {
            isUserLiker = true;
            break;
        }
    }

    // form the display
    var html = "";
    if (count == 1) {
        html = (isUserLiker ? "You like" : likes[0].name + " likes") + " this " + likeType + ".";
    } else if (count == 2) {
        if (isUserLiker) {
            html = "You and " + (likes[0].id == nlapiGetUser() ? likes[1].name : likes[0].name) + " like this " + likeType + ".";
        } else {
            html = likes[0].name + " and " + likes[1].name + " like this " + likeType + ".";
        }
    } else if (count > 2) {
        // form the likers link
        var likersLink = "<span class='sf_nf_post_likers_link' onclick='socialShowLikesList(" + JSON.stringify(likes) + ");'>" + (count - 1) + " others</span>";
        html = (isUserLiker ? "You and " : likes[0].name + " and ") + likersLink + " like this " + likeType + ".";
    }
    return html;
}

/**
 * Gets the role of the current user. Possible values are 'Employee' and 'Customer'
 * @param id (Optional) - User id. If null, gets the role of current user
 */
function ssapiGetRole(id) {
	var entityType = ssapiHasValue(id) ? nlapiLookupField('entity', id, 'type') : null;
	
	if (entityType === 'Employee')
		return 'Employee';
	else if (entityType === 'CustJob') 
		return 'Customer';
	else if (ssapiHasNoValue(entityType)){
		var roleCenter = nlapiGetContext().getRoleCenter();
		if (roleCenter === ssobjGlobal.CENTER_TYPES.CUSTOMER_CENTER) {
			return 'Customer';
		} else if (roleCenter === ssobjGlobal.CENTER_TYPES.PARTNER_CENTER) {
			return 'Partner';
		} else if (roleCenter === ssobjGlobal.CENTER_TYPES.VENDOR_CENTER) {
			return 'Vendor';
		} else {
			return 'Employee';
		}
	}
}
