/**
 * Module Description Here
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 *
 * Version    Date            Author           Remarks
 * 1.00       02 Jan 2012     tcaguioa
 *
 */
/**
 * @param {String}
 *        type Sublist internal id
 * @param {String}
 *        name Field internal id
 * @param {Number}
 *        linenum Optional line item number, starts from 1
 * @return {void}
 */
function fieldChanged(type, name, linenum) {
    // if (name == 'custpage_field') {
    // nlapiSetFieldValue('custrecord_suitesocial_autopost_fld_fld',
    // nlapiGetFieldValue('custpage_field'), true /*firefieldchanged */);
    // }
    //    
    //    
}

function processDeleteResult(btn) {
    if (btn == 'no') {
        return;
    }
    // delete
    ssapiWait();
    deleteAutoPostField(nlapiGetFieldValue('id'), false);
    Ext.Msg.hide();
    if (typeof window != 'undefined') {
        window.location.href = window.opener.originalUrl;
    }
}

function processInactivateResult(btn) {
    // alert(btn);
    if (btn == 'yes') {
        nlapiSetFieldValue('isinactive', 'T');
        //
        Ext.Msg.show({
            title : 'Inactive Record',
            msg : 'If you want to reactivate a record, you can go to the Summary step of the assistant.'.tl(),
            buttons : {
                ok : 'OK'.tl()
            },
            minWidth : 480,
            icon : Ext.MessageBox.INFORMATION
        });
        return;
    }

    // confirm delete
    var deleteQuestion = 'This will also delete the following dependent records: User Excluded Tracked Field. Continue with the delete?'.tl();
    Ext.Msg.show({
        title : 'Delete Including Dependent Records'.tl() + '?',
        msg : deleteQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processDeleteResult
    });

}

function deleteDependentRecords() {
    var inactivateQuestion = 'Instead of deleting, you have the option to make a Tracked Field inactive.'.tl() + ' ';
    inactivateQuestion += 'While a Tracked Field is inactive,'.tl() + ' ';
    inactivateQuestion += 'posts related to the Tracked Field will not be created.'.tl() + ' ';
    inactivateQuestion += 'Do you want to make this Tracked Field inactive instead?'.tl();
    // Show a dialog using config options:
    Ext.Msg.show({
        title : 'Delete Dependent Records?'.tl(),
        msg : inactivateQuestion,
        buttons : {
            yes : 'Yes'.tl(),
            no : 'No'.tl()
        },
        minWidth : 480,
        icon : Ext.MessageBox.QUESTION,
        fn : processInactivateResult
    });
}

// Issue: 212029 Field 'Field' of custom record types Auto Post Field
/**
 * @return {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {
    var logger = new ssobjLogger('clientSaveRecord');
    if (ssapiHasNoValue(nlapiGetFieldValue(FLD_AUTOPOST_FLD_FLD))) {
        uiShowError('Provide a value for Field.'.tl());
        return false;
    }
    var cols = [];
    cols.push({
        columnId : FLD_AUTOPOST_FLD_REC,
        operator : OP_ANYOF,
        value : nlapiGetFieldValue(FLD_AUTOPOST_FLD_REC)
    });
    cols.push({
        columnId : FLD_AUTOPOST_FLD_FLD,
        operator : OP_ANYOF,
        value : nlapiGetFieldValue(FLD_AUTOPOST_FLD_FLD)
    });

    var internalId = nlapiGetFieldValue('id');
    if (hasDuplicates(REC_AUTOPOST_FIELD, cols, internalId)) {
        uiShowError('Field "'.tl() + nlapiGetFieldText(FLD_AUTOPOST_FLD_FLD) + '" already exists in the database.'.tl());
        return false;
    }

    return true;
}

function pageInit() {
    nlapiSetFieldDisplay('customform', false);

    // upgrade code
    if (ssapiHasNoValue(nlapiGetFieldValue('custrecord_suitesocial_autopost_fld_rf'))) {
        nlapiSetFieldText('custrecord_suitesocial_autopost_fld_rf', nlapiGetFieldText('custrecord_suitesocial_autopost_fld_fld'));
    }

    setInterval('ssapiHideNewInOptionList();', 500);
}

/**
 * This is used in translating the menu item Delete Including Dependent Records
 * It needs to be called from an interval since the menu item is created on
 * hover
 */
function ssapiTranslateDeleteIncludingDependentRecords() {
    var link = jQuery('span:contains("Delete Including Dependent Records")')[0];
    if (link) {
        link.innerHTML = link.innerHTML.tl();
        clearInterval(ssobjGlobal.translateDeleteIncludingDependentRecords);
    }
}
ssobjGlobal.translateDeleteIncludingDependentRecords = setInterval('ssapiTranslateDeleteIncludingDependentRecords();', 500);
