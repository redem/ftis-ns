/*
 * @return {boolean} returns true if the current user has access to SuiteSocial
 */
function isCurrentUserHasSuiteSocialAccess(){
    try {
        nlapiSearchRecord('customrecord_suitesocial_profile', null, null, [new nlobjSearchColumn('internalid', null, 'count')]);
        return true;
    } 
    catch (e) {
        return false;
    }
}

function newsfeedPageInit()
{
    setInterval("ssapiChangeLinkTargetOfHoverWindow('View')", 500);
    setInterval("ssapiChangeLinkTargetOfHoverWindow('Edit')", 500);

    // Issue: 213318 [SuiteSocial] Newsfeed and Status portlet should display a messag
	if(isCurrentUserHasSuiteSocialAccess() === false){
        document.body.innerHTML = '<br /><span class=smalltext>&nbsp;' + 'Your current role has no access to SuiteSocial.'.tl() + '</span>';
		return;
	}


    if (window.opener != null)
        document.getElementById('message').form.target='server_commands';
    setInterval('newsfeedRefresh(true)',60000);
    if (nlapiGetField("recipient") != null)
        newsfeedSyncFields();	
}

function newsfeedFieldChanged(type, fldnam)
{
    if (fldnam == "custpage_feed_channel")
    {
        //newsfeedRefresh();
        newsfeedSyncFields();
		newsfeedRefresh(false);
    }
    else if (fldnam == "channel") 
	{
		if (nlapiGetFieldValue("channel") != "1")
		{
	    	nlapiSetFieldValue("recipient","");
			nlapiSetFieldDisplay("recipient",false);		
		}
		else
		{
			nlapiSetFieldDisplay("recipient",true);		
		}

	}
    else if (fldnam == "recipient" && nlapiGetFieldValue("recipient") != null && nlapiGetFieldValue("recipient").length>0)
        nlapiSetFieldValue("channel","1");
 }

function newsfeedRefresh(isOffset)
{
    var channel = nlapiGetFieldValue("custpage_feed_channel");
    var size = nlapiGetFieldValue("custpage_size");
    var offset = nlapiGetFieldValue("custpage_offset");
    var url = nlapiResolveURL('SUITELET','customscript_newsfeed_refresh','customdeploy_newsfeed_refresh')+"&custpage_size="+size+"&custpage_feed_channel="+channel;
    if ( isOffset )
    	url += "&custpage_offset="+offset;
    if (nlapiGetFieldValue("custpage_record_type") != null && nlapiGetFieldValue("custpage_record_type").length > 0)
        url += "&custpage_record_type="+nlapiGetFieldValue("custpage_record_type")+"&custpage_record_id="+nlapiGetFieldValue("custpage_record_id");
    document.getElementById("server_commands").src=url;
 }

// Issue: 210553 [Suite Social]Suite Social News Feed Portlet > Channel Vi

/* 
 * returns the array of values of a multiple select field. 
 * This has to be created because getFieldValues() has a bug. 
 * @param {string} typeId The id of the record type 
 * @param {string} columnId The id of the multiple select field 
 * @param {select} internalId The internal id of the row to be loaded 
 * @return {array} Returns an array similar to the return value of getFieldValues 
 */ 

function getMultipleSelectValues(typeId, columnId, internalId){ 
    var columns = [new nlobjSearchColumn(columnId)]; 
    var filters = [new nlobjSearchFilter('internalid', null, 'anyof', internalId)] 
    var results = nlapiSearchRecord(typeId, null, filters, columns) 
    if (results === null || results[0].getValue(columnId) == '') { 
        return null; 
    } 
    return results[0].getValue(columnId).split(','); 

     
    // Note: use code below below once the bug is fixed 
    // var record = nlapiLoadRecord(typeId, columnId); 
    // var values = record.getFieldValues(columnId); 
    // return values; 
} 

function ssapiChangeLinkTargetOfHoverWindow(label){
    var elements = Ext.select('input[value="' + label + '"]').elements;
    for (var i = 0; i < elements.length; i++) {
        var onclickvalue = elements[i].getAttribute('onclick');
        if (onclickvalue.indexOf('parent') == -1) {
            elements[i].setAttribute('onclick', 'parent.' + onclickvalue);
        }
    }
}

// If the channel is Private Message and there is no recipient employee, an alert should be displayed
function newsfeedSaveRecord()
{
	if (nlapiGetFieldValue("channel") == "1" && 
		(nlapiGetFieldValue("recipient") === null || nlapiGetFieldValue("recipient") === '')  )
	{
          Ext.Msg.show({
              title: 'SuiteSocial Newsfeed'.tl(),
              msg: 'Employee recipient field is required for Private Messages post.'.tl(),
              buttons: {ok: 'OK'.tl()},
              icon: Ext.MessageBox.WARNING
          });
            return false;

	}
	return true;
}

function newsfeedSyncFields()
{
    if (nlapiGetFieldValue("custpage_feed_channel") != null && nlapiGetFieldValue("custpage_feed_channel").length > 0)
    {
		nlapiSetFieldValue("channel",nlapiGetFieldValue("custpage_feed_channel"), false, true); 
		nlapiSetFieldValue("custpage_channel_sync",nlapiGetFieldValue("custpage_feed_channel"), false, true);
		nlapiSetFieldDisplay("channel",false); 
    }
    else
	{
        nlapiSetFieldDisplay("channel",true);
        nlapiSetFieldValue("custpage_channel_sync","", false, true);
	}


    if (nlapiGetFieldValue("custpage_feed_channel") != null && nlapiGetFieldValue("custpage_feed_channel").length > 0 && nlapiGetFieldValue("custpage_feed_channel") != "1")
    {
        nlapiSetFieldValue("recipient","");
        nlapiSetFieldDisplay("recipient",false);
    }
	
    else
        nlapiSetFieldDisplay("recipient",true);

	if (nlapiGetFieldValue("custpage_feed_channel") !== null && (nlapiGetFieldValue("custpage_feed_channel") !== "") &&
	(nlapiGetFieldValue("custpage_feed_channel") != "14" && nlapiGetFieldValue("custpage_feed_channel") != "13")) {
		
		// Issue: 210553 [Suite Social]Suite Social News Feed Portlet > Channel Vi 
		var values = getMultipleSelectValues('customrecord_newsfeed_channel', 'custrecord_newsfeed_channel_posters', nlapiGetFieldValue("custpage_feed_channel") );
//		var rec_channel = nlapiLoadRecord('customrecord_newsfeed_channel', nlapiGetFieldValue("custpage_feed_channel"));
//		var values = rec_channel.getFieldValues('custrecord_newsfeed_channel_posters');
		if ( values !== null ){
        	for (var i = 0; i < values.length; i++) {
        		if (nlapiGetContext().getUser() == values[i]) {  
        			// dom access
					var btn = document.getElementById('submitter');
					if(btn !== null){
						document.getElementById('submitter').disabled = false;
        				document.getElementById('tbl_submitter').style.visibility = 'visible';	
					}
					btn = document.getElementById('secondarysubmitter');
					if(btn !== null){
						document.getElementById('secondarysubmitter').disabled = false;
        				document.getElementById('tbl_secondarysubmitter').style.visibility = 'visible';	
					}
        			
        			nlapiSetFieldDisplay("message", true);
        			nlapiSetFieldDisplay("icon", true);
        			break;
        		}
        		else {
        			// dom access
					btn = document.getElementById('submitter');
					if(btn !== null){
						document.getElementById('submitter').disabled = true;
        				document.getElementById('tbl_submitter').style.visibility = 'hidden';	
					}
					btn = document.getElementById('secondarysubmitter');
					if(btn !== null){
						document.getElementById('secondarysubmitter').disabled = true;
        				document.getElementById('tbl_secondarysubmitter').style.visibility = 'hidden';	
					}
        			nlapiSetFieldDisplay("message", false);
        			nlapiSetFieldDisplay("icon", false);
        		}
        	}			
		}
		else{
			btn = document.getElementById('submitter');
			if(btn !== null){
				document.getElementById('submitter').disabled = false;
				document.getElementById('tbl_submitter').style.visibility = 'visible';	
			}
			btn = document.getElementById('secondarysubmitter');
			if(btn !== null){
				document.getElementById('secondarysubmitter').disabled = false;
				document.getElementById('tbl_secondarysubmitter').style.visibility = 'visible';	
			}			
			nlapiSetFieldDisplay("message", true);
			nlapiSetFieldDisplay("icon", true);			
		}

	}
	else {
		
		if (nlapiGetFieldValue("custpage_feed_channel") === ""){
			// dom access
			btn = document.getElementById('submitter');
			if(btn !== null){
				document.getElementById('submitter').disabled = false;
				document.getElementById('tbl_submitter').style.visibility = 'visible';	
			}
			btn = document.getElementById('secondarysubmitter');
			if(btn !== null){
				document.getElementById('secondarysubmitter').disabled = false;
				document.getElementById('tbl_secondarysubmitter').style.visibility = 'visible';	
			}
			nlapiSetFieldDisplay("message", true);
			nlapiSetFieldDisplay("icon", true);			
		}
		else{
			// dom access
			btn = document.getElementById('submitter');
			if(btn !== null){
				document.getElementById('submitter').disabled = true;
				document.getElementById('tbl_submitter').style.visibility = 'hidden';	
			}
			btn = document.getElementById('secondarysubmitter');
			if(btn !== null){
				document.getElementById('secondarysubmitter').disabled = true;
				document.getElementById('tbl_secondarysubmitter').style.visibility = 'hidden';	
			}
			nlapiSetFieldDisplay("message",false);
			nlapiSetFieldDisplay("icon",false);			
		}
		//nlapiSetFieldDisplay("submitter", false);
		
	}			
			

}