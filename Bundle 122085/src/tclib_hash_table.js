
/**
 * @author user
 */



/*
 * This is used as an in-memory table. This is usually used in in checking what
 * to delete or update from a data entry form. This object holds the copy of the
 * data before it is edited. After the data is edited (say from a sublist), that
 * data is compared against the data in this object.
 */
function ssobjDataTable(){
    var _keys = [];
    var _rows = [];
    
    
    
    /*
     * adds a row/item in the table
     * @param {object} row The object with a number of name: value pairs
     */
    this.add = function(row){
        if (ssapiHasNoValue(row)) {
            return;
        }
        _keys.push(row.internalid);
        _rows['key' + row.internalid] = row;
    };
    
    /*
     * add an array or row/object in the table
     * @param {object[]} rows The objects with a number of name: value pairs
     */
    this.addMany = function(rows){
        if (ssapiHasNoValue(rows)) {
            return;
        }
        for (var i in rows) {
            this.add(rows[i]);
        }
    };
    /*
     * returns the row, given an internal id. returns null if the internal id is
     * not found
     * @param {integer} internalid The id or key of the row
     * @return {object} If it exists, returns the row. If not found, returns null
     */
    this.find = function(internalid){
        var row = _rows['key' + internalid];
        if (typeof row == 'undefined') {
            return null;
        }
        return row;
    };
    
    /*
     * returns true if the row exists with the given internalid
     * @param {integer} internalid The id or key of the row
     */
    this.contains = function(internalid){
        return this.find(internalid) !== null;
    };
    
    /*
     * Returns the number of rows/items in the table
     * @return {integer}
     */
    this.getCount = function(){
        return _keys.length;
    };
    
    /*
     * Checks whether the object being passed has changed.
     * @param {integer} internalid The internal id of the record
     * @param {object} rowExternal This is usually the object that can be modified. This is usually generated
     * from a sublist or other data entry controls
     */
    this.hasChanged = function(internalid, rowExternal){
        //    var logger = new ssobjLogger('hashTable.hasChanged');
        if (internalid === null) {
            throw 'objDataTable.hasChanged() internalid === null';
        }
        if (rowExternal === null) {
            throw 'objDataTable.hasChanged() rowExternal === null';
        }
        //        nlapiLogExecution('debug', 'hasChanged() internalid=' + internalid, '');
        var rowInTable = this.find(internalid);
        if (rowInTable === null) {
            throw 'objDataTable.hasChanged() rowInTable === null';
        }
        
        
        //		logger.log('JSON.stringify(rowInTable)='+JSON.stringify(rowInTable));
        //		logger.log('JSON.stringify(rowExternal)='+JSON.stringify(rowExternal));
        return JSON.stringify(rowInTable) != JSON.stringify(rowExternal);
    };
    
    this.clear = function(){
        _keys = [];
        _rows = [];
    };
    
    
    /*
     * returns a string representation of the datatable
     */
    this.toString = function(){
        var jsonString = '';
        for (var i in _keys) {
            var row = _rows['key' + _keys[i]];
            jsonString += JSON.stringify(row) + '<br />';
        }
        return jsonString;
    };
}


//
//var results = jsUnity.run(assistantTestSuite);
//
//function assistantTestSuite(){
//    function setUp(){
//        this.scope.hi = 'hello';
//        this.scope.teddy = {
//            internalid: 1,
//            name: 'teddy caguioa'
//        };
//        this.scope.kyle = {
//            internalid: 2,
//            name: 'kyle caguioa'
//        };
//        this.scope.baby = {
//            internalid: 2,
//            name: 'kyle caguioa'
//        };
//        this.scope.lyka = {
//            internalid: 3,
//            name: 'lyka caguioa'
//        };
//        this.scope.erlyn = {
//            internalid: 3,
//            name: 'lyka caguioa'
//        };
//        this.scope.createBaby = function(){
//            return {
//                internalid: 0,
//                name: 'new baby'
//            };
//        };
//    }
//    
//    function tearDown(){
//    }
//    
//    function testThis(){
//    }
//    
//    function testA(){
//        jsUnity.assertions.assertTrue(true);
//    }
//    
//    function testB(){
//        jsUnity.assertions.assertEqual(this.hi, 'hello');
//    }
//    
//    function testFind(){
//        var persons = new ssobjDataTable();
//        persons.add(this.teddy);
//        persons.add(this.kyle);
//        // find teddy
//        var item = persons.find(this.teddy.internalid);
//        jsUnity.assertions.assertEqual(item, this.teddy);
//        // find lyka, should not be found
//        item = persons.find(this.lyka.internalid);
//        jsUnity.assertions.assertNull(item);
//        // now add
//        persons.add(this.lyka);
//        item = persons.find(this.lyka.internalid);
//        jsUnity.assertions.assertEqual(item, this.lyka);
//    }
//    function testContains(){
//        var persons = new ssobjDataTable();
//        persons.add(this.teddy);
//        persons.add(this.kyle);
//        // find teddy
//        var isFound = persons.contains(this.teddy.internalid);
//        jsUnity.assertions.assertTrue(isFound);
//        // find lyka, should not be found
//        isFound = persons.contains(this.lyka.internalid);
//        jsUnity.assertions.assertFalse(isFound);
//        // now add
//        persons.add(this.lyka);
//        isFound = persons.contains(this.lyka.internalid);
//        jsUnity.assertions.assertTrue(isFound);
//    }
//    
//    function testHasChanged(){
//        var persons = new ssobjDataTable();
//        var baby = this.createBaby();
//        persons.add(baby);
//        // clone baby 
//        var babyForEdit = JSON.parse(JSON.stringify(baby));
//        // find baby 
//        var isFound = persons.contains(babyForEdit.internalid);
//        jsUnity.assertions.assertTrue(isFound);
//        // test changes, none for now
//        var hasChanged = persons.hasChanged(babyForEdit.internalid, babyForEdit);
//        jsUnity.assertions.assertFalse(hasChanged);
//		// simulate edits
//		babyForEdit.name = 'joseph';
//        // test changes, there should be
//        hasChanged = persons.hasChanged(babyForEdit.internalid, babyForEdit);
//        jsUnity.assertions.assertTrue(hasChanged);
//    }
//    
//}
//
//jsUnity.log = function(s){
//    if (s.indexOf('[PASSED]') > -1) {
//        return;
//    }
//    if (s.indexOf('[FAILED]') > -1) {
//        //        s = '<b>' + s + '</b>';
//    }
//    console.log('jsUnitylog: ' + s);
//};
//
//jsUnity.error = function(s){
//    console.log('jsUnity error: ' + s);
//};
//var results = jsUnity.run(assistantTestSuite);
//
////console.log('suiteName=' + results.suiteName);// == "namedTestSuite"
////console.log('total=' + results.total);//== 2
////console.log('passed=' + results.passed);//== 2 == 1
////console.log('failed:' + results.failed);//== 2== 1
////alert( results.duration );//== 2== ?
//
////alert('hello')
//var teddy = {
//    internalid: 1,
//    name: 'teddy caguioa'
//};
//
//var kyle = {
//    internalid: 2,
//    name: 'kyle caguioa'
//};
//
//var lyka = {
//    internalid: 3,
//    name: 'lyka caguioa'
//};
//
//var erlyn = {
//    internalid: 3,
//    name: 'lyka caguioa'
//};
//
////alert(JSON.stringify(lyka) == JSON.stringify(erlyn));
//
////// alert('erlyn == lyka=' + erlyn == lyka);
////// alert('erlyn === lyka=' + erlyn === lyka);
////
//var persons = new ssobjDataTable();
//persons.add(teddy);
//persons.add(kyle);
//persons.add(lyka);
// alert('persons.contains(3)=' + persons.contains(3));
// lyka.name = 'taduran';
//alert(persons.toString());
