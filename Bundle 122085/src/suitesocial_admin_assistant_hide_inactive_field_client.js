/**
 * Module Description Here
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 *
 * Version    Date            Author           Remarks
 * 1.00       02 Jan 2012     tcaguioa
 *
 */
// Issue: 212029 Field 'Field' of custom record types Auto Post Field 
/**
 * @return {Boolean} True to continue save, false to abort save
 */
//function clientSaveRecord(){
//	
//	var cols = [];
//	cols.push({columnId: FLD_AUTOPOST_FLD_REC, operator : OP_ANYOF, value: nlapiGetFieldValue(FLD_AUTOPOST_FLD_REC)});
//	cols.push({columnId: FLD_AUTOPOST_FLD_FLD, operator : OP_ANYOF, value: nlapiGetFieldValue(FLD_AUTOPOST_FLD_FLD)});
//	
//	if( hasDuplicates(REC_AUTOPOST_FIELD, cols, nlapiGetFieldValue(FLD_INTERNAL_ID))){
//		uiShowError('Field "'+nlapiGetFieldText(FLD_AUTOPOST_FLD_FLD)+'" already exists inthe database.');
//		return false;
//	}
//
//    return true;
//}


function pageInit(){
    nlapiSetFieldDisplay('isinactive', false);
    try {
        nlapiSetFieldDisplay('customform', false);
    } 
    catch (e) {
        // in case custom form is not existing, just supress error
    }
    
}
