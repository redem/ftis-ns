/**
 * @author tcaguioa
 */

/*
 * we want to handle the error but at the same time we want to make use of the notifications in the 'Unhandled Errors' subtab
 * in the script deployment. 
 * In this case, we call a scheduled script passing the error message. 
 * The call to the scheduled script will generate an unhandled error with the passed error message as error message
 */
function logErrorDetails(){
    var errorDetails = nlapiGetContext().getSetting('script', 'custscript_error_details');
    //    nlapiLogExecution('debug', errorDetails, '');
    throw errorDetails;
}

//function handleError(){
//	nlapiScheduleScript(scriptId, deployId, params)
//}

//function test(){
//	var name = 'teddy';
//var person = <person> 
//  <name>{name}</name>  
//  <likes>  
//    <os>Linux</os>  
//    <browser>Firefox</browser>  
//    <language>JavaScript</language>  
//    <language>Python</language>  
//  </likes>  
//</person>;  
//
//  alert(person); // Bob Smith  
//alert(person.name); // Bob Smith  
//alert(person['name']); // Bob Smith  
//alert(person.likes.browser); // Firefox  
//alert(person['likes'].browser); // Firefox 

//}
