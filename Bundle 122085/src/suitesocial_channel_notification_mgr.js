/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 22 Feb 2016 kchua
 * 
 */
function SocialChannelNotificationMgr() {

    this.notifyOnChannel = _notifyOnChannel;

    var DEFAULT_MSG_CREATE = '{username} has created a new channel, [["action":"showChannel", "channelId":"{channelId}", "channelName":"{channelName}"]].  Click the channel selector and Subscribe button to subscribe to the channel. Or click [["action":"subscribeChannel", "channelId":"{channelId}", "channelName":"{channelName}", "linkText":"here"]] to subscribe.';

    function _notifyOnChannel(param) {
        // create post on public channel
        nlapiLogExecution('DEBUG', 'Creating post on public channel for audience', 'channelId : ' + param.channelId);

        var message = DEFAULT_MSG_CREATE;

        if (param.message != undefined && param.message != "") {
            message = param.message;
        }

        message = applyTemplate(message, param);
        var messageLength = message.length;

        if (messageLength > 4000) {
            throw 'Post should not be more than 4000 characters.'.tl() + '<br>' + 'Current post length:'.tl() + ' ' + messageLength;
        }

        if (messageLength >= 4) {

            if (message.substr(messageLength - 4, 4) == '[br]') {

                message = message.substr(0, messageLength - 4);

            }

        }
        nlapiLogExecution('DEBUG', 'message', 'message : ' + message);

        var postParam = {};

        postParam.record_type = null;
        postParam.record_id = null;
        postParam.msg = message;
        postParam.recipient = null;
        postParam.channel = param.notifyChannelId;
        postParam.suppress_alert = true;
        postParam.name = ssapiGetNewsFeedRandomNumber(nlapiGetUser());
        postParam.isGivingRecognition = false;

        // call addPost function to create post
        var newPostId = addPost(postParam);

        nlapiLogExecution('DEBUG', 'newPostId', newPostId);

    }

    function applyTemplate(tempMessage, param) {
        for ( var name in param) {
            tempMessage = tempMessage.replace(new RegExp('{' + name + '}', 'g'), param[name]);
        }

        return tempMessage;
    }

}