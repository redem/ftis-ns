function SuiteSocial_Profile_UES()
{
    //Dependencies
    //var SuiteScript = System.Components.Use("SuiteScript");
    
    
    //Interface
    this.OnBeforeLoad = _OnBeforeLoad;
    this.OnAfterSubmit = _OnAfterSubmit;



    function _OnBeforeLoad(type, form)
    {
        // disallow create on UI
        var context = nlapiGetContext();


        if (type == 'create' && context.getExecutionContext() == 'userinterface')
        {
            throw 'Profile cannot be created from the UI';
        }


        if (context.getRole() != 3 && (type == "create" || ((type == "view" || type == "edit") &&
            nlapiGetFieldValue("custrecord_suitesocial_profile_emp") != context.getUser())))
        {
            var filters = [
                new nlobjSearchFilter("custrecord_suitesocial_profile_emp", null, "anyof", context.getUser())
            ];

            var searchresults = nlapiSearchRecord('customrecord_suitesocial_profile', null, filters, null);
            var id = null;
            if (searchresults != null && searchresults.length > 0)
            {
                id = searchresults[0].getId();
            }

            if (type != 'create' || id != null)
            {
                nlapiSetRedirectURL("RECORD", "customrecord_suitesocial_profile", id, id == null || type == "edit" || type == "create");
            }
        }


        if ((type == 'view' || type == 'edit') &&
            context.getUser() == nlapiGetFieldValue('custrecord_suitesocial_profile_emp'))
        {
            try
            {
                var user_yammer_token = nlapiGetFieldValue('custrecord_yammer_profile_token');
                var fld = nlapiGetField('custrecord_yammer_authentication_token');

                fld.setDisplayType('normal');

                if (type == 'view')
                {
                    nlapiSetFieldValue('custrecord_yammer_authentication_token', suitesocial.SimpleTokenDecoder.Decrypt(user_yammer_token));
                }
            }
            catch (e)
            {
            }
        }
        else
        {
            var yammer_fld = nlapiGetField('custrecord_yammer_authentication_token');
            yammer_fld.setDisplayType('hidden');

            var yammer_get_token_link = nlapiGetField('custrecord_inline_get_authentication');
            yammer_get_token_link.setDisplayType('hidden');
        }


        var fld = nlapiGetField('custrecord_yammer_integration_enabled');
        var yammer_fld = nlapiGetField('custrecord_yammer_authentication_token');
        var yammer_get_token_link = nlapiGetField('custrecord_inline_get_authentication');
        var isCustomer = context.getRoleCenter() == "CUSTOMER";

        try {
            var token = context.getSetting('SCRIPT', 'custscript_activity_stream_token');
            if (token != '' && token != null && !isCustomer)
            {
                fld.setDisplayType('normal');
            }
            else
            {
                fld.setDisplayType('hidden');
                yammer_fld.setDisplayType('hidden');
                yammer_get_token_link.setDisplayType('hidden');
            }
        }
        catch (e)
        {
            fld.setDisplayType('hidden');
            yammer_fld.setDisplayType('hidden');
            yammer_get_token_link.setDisplayType('hidden');
        }

        // load custom fields on My Channels
        if (type == 'edit')
        {
            loadMyChannelsCustomFields(form);
        }
    }



    /**
    * Loads the custom fields on My Channels and displays the appropriate values
    * taken from the standard fields.
    * 
    * @param form
    *        {nlobjForm}
    */
    function loadMyChannelsCustomFields(form)
    {
        // get channels user has read access (exclude built-in ones)
        var channelsOptions = [];
        var autosubscribedChannels = [];
        var channelsReadAccess = ssapiGetChannelsUserHasReadAccess();

        for (var i = 0; i < channelsReadAccess.length; i++)
        {
            var channelObj = channelsReadAccess[i];

            // add to channels if not built-in
            if (BUILT_IN_CHANNEL_IDS.indexOf(parseInt(channelObj.internalid)) == -1 && channelObj.name != 'Recognition')
            {
                channelsOptions.push(channelObj);

                // add to autosubscribed array if autosubscribed channel
                if (channelObj.autoSubscribed)
                {
                    autosubscribedChannels.push(channelObj);
                }
            }
        }

        // my channels sublist
        var sublist = 'recmachcustrecord_suitesocial_profile_ch_prof';
        var myChannels = form.getSubList(sublist);

        // add custom Channel column
        var colChannel = 'custpage_ss_prof_ch_ch';
        var fldChannel = myChannels.addField(colChannel, 'select', 'Channel');
        fldChannel.setMandatory(true);
        fldChannel.addSelectOption('', '');
        for (var i = 0; i < channelsOptions.length; i++)
        {
            var channelObj = channelsOptions[i];
            fldChannel.addSelectOption(channelObj.internalid, channelObj.name);
        }

        // add custom Email Digest column
        var colEmailDigest = 'custpage_ss_prof_ch_dgst';
        myChannels.addField(colEmailDigest, 'checkbox', 'Email Digest');

        // add custom Email Alerts column
        var colEmailAlerts = 'custpage_ss_prof_ch_alrt';
        myChannels.addField(colEmailAlerts, 'checkbox', 'Email Alerts');

        // add custom Autosubscribed column
        var colAutoSubs = 'custpage_ss_prof_ch_auto_subs';
        var fldAutoSubs = myChannels.addField(colAutoSubs, 'checkbox', 'Auto Subscribed');
        fldAutoSubs.setDisplayType('hidden');

        // load my channels values to custom fields
        var count = nlapiGetLineItemCount(sublist);
        for (var i = 1; i <= count; i++)
        {
            var channel = nlapiGetLineItemValue(sublist, 'custrecord_suitesocial_profile_ch_ch', i);
            var digest = nlapiGetLineItemValue(sublist, 'custrecord_suitesocial_profile_ch_dgst', i);
            var alerts = nlapiGetLineItemValue(sublist, 'custrecord_suitesocial_profile_ch_alrt', i);
            nlapiSetLineItemValue(sublist, colChannel, i, channel);
            nlapiSetLineItemValue(sublist, colEmailDigest, i, digest);
            nlapiSetLineItemValue(sublist, colEmailAlerts, i, alerts);

            // if channel is autosubscribed, set autosubscribed column, and remove from the array
            for (var j = 0; j < autosubscribedChannels.length; j++)
            {
                if (autosubscribedChannels[j].internalid == channel)
                {
                    // set autosubscribed column
                    nlapiSetLineItemValue(sublist, colAutoSubs, i, 'T');

                    // remove from array
                    autosubscribedChannels.splice(j, 1);
                    break;
                }
            }
        }

        // keep remaining autosubscribed channels to be populated on the client side
        var fldRemAutoSubs = form.addField('custpage_ss_prof_ch_rem_autosubs', 'textarea', 'Remaining Autosubs');
        fldRemAutoSubs.setDefaultValue(JSON.stringify(autosubscribedChannels));
        fldRemAutoSubs.setDisplayType('hidden');
    }



    function _OnAfterSubmit()
    {
        var logger = new ssobjLogger(arguments);

        // get profile id
        var id = nlapiGetNewRecord().getId();
        
        _SetProfileImage(id);
            

        try
        {
            // search profile-channel records of this profile that has empty subscriber field
            var filters = [
                ['custrecord_suitesocial_profile_ch_prof', 'is', id],
                'and',
                ['custrecord_suitesocial_profile_ch_sub', 'is', '@NONE@']
            ];

            var results = nlapiSearchRecord('customrecord_suitesocial_profile_channel', null, filters);
            if (ssapiHasValue(results))
            {
                // get empId for this profile
                var empId = nlapiGetNewRecord().getFieldValue('custrecord_suitesocial_profile_emp');
                
                // loop through records
                for (var i = 0; i < results.length; i++)
                {
                    var profChanId = results[i].getId();
                    
                    // set subscriber field (employee)
                    nlapiSubmitField('customrecord_suitesocial_profile_channel', profChanId, 'custrecord_suitesocial_profile_ch_sub', empId);
                }
            }

            // get the list of colleagues of this profile
            // for each profile, if the colleague name is not the same as the employee, update the colleague name
            var s = new SuiteSocialColleagueSearch();
            s.addEmployeeColumn().addColleagueColumn().addIdColumn();
            
            var results = s.Profile_ANYOF(id).searchRecord();
            if (results === null)
            {
                return;
            }
            
            
            for (var i = 0; i < results.length; i++)
            {
                var result = results[i];
                if (result.getText(s.EMPLOYEE) != result.getText(s.COLLEAGUE))
                {
                    var empName = result.getText(s.EMPLOYEE);
                    var colleagueId = result.getText(s.ID);
                    var colleague = new SuiteSocialColleague();
                    colleague.loadRecord(colleagueId);
                    
                    var colleagueProfileId = colleague.getColleagueValue();
                    if (ssapiHasNoValue(colleagueProfileId)) {
                        // this will happen only after bundle update when the field custrecord_suitesocial_colleague_sub_pro of record customrecord_suitesocial_colleague has just been added
                        continue;
                    }
                    
                    // update colleague's profile name with that of employee name
                    nlapiSubmitField('customrecord_suitesocial_profile', colleagueProfileId, 'name', empName);
                }
            }
        }

        catch (e)
        {
            ssapiHandleError(e);
        }
    }
    
    
    
    function _SetProfileImage(id)
    {
        // set profile pic to available without login
        var fileId = nlapiGetFieldValue("custrecord_suitesocial_profile_image");
        
        if (ssapiHasValue(fileId))
        {
            var file = nlapiLoadFile(fileId);
            var isPublicFile = file.isOnline();
            
            //Ensure public access
            if (!isPublicFile)
            {
                file.setIsOnline(true);
                nlapiSubmitFile(file);
            }
            
            _ResizeImage({
                ProfileId: id,
                Name: file.getName(),
                Type: file.getType(),
                Url: file.getURL(),
                FileId: fileId
            });
        }
    }



    function _ResizeImage(imgDef)
    {
        var WIDTH = 100;
        var HEIGHT = 100;
        var imagingSvc = _Netsuite_ImagingService;
        
        var publicUrl = _GetDomain() + imgDef.Url;
        var newImg = new imagingSvc().Resize(publicUrl, WIDTH, HEIGHT);
        
        if (!newImg.IsSuccessful)
        {
            nlapiLogExecution("AUDIT", "Image Resizing Failed", "Image may not be available w/o login. [" + newImg.Url + "]");
        }
        else
        {
            var fileId = _SaveToCabinet(imgDef, newImg);

            //Update profile record
            nlapiSubmitField("customrecord_suitesocial_profile", imgDef.ProfileId, "custrecord_suitesocial_profile_image", fileId);

            nlapiLogExecution("AUDIT", "Image Successfully Resized", "Profile [" + imgDef.ProfileId + "]");
        }
    }



    function _SaveToCabinet(imgDef, newImg)
    {
        var a = imgDef.Name.match(/\.[0-9a-z]+$/i);
        var fileName = "{" + imgDef.ProfileId + ":" + imgDef.FileId + "}" + a[0];

        var file = nlapiCreateFile(fileName, imgDef.Type, newImg.Content);
        file.setFolder(_GetProfilesFolderId());
        file.setIsOnline(true);

        return nlapiSubmitFile(file);
    }



    function _GetProfilesFolderId()
    {
        if (_GetProfilesFolderId.Value == null)
        {
            var filters = [
                new nlobjSearchFilter("name", null, "is", "Profiles"),
                new nlobjSearchFilter("parent", null, "is", _GetImagesFolderId())
            ];
            var columns = null;
            var sr = nlapiSearchRecord("folder", null, filters, columns);

            _GetProfilesFolderId.Value = sr == null ? _CreateProfilesFolder() : sr[0].getId();
        }

        return _GetProfilesFolderId.Value;
    }
    
    
    
    function _GetImagesFolderId()
    {
        if(_GetImagesFolderId.Value == null)  //Lazy load
        {
            var filters = [
                new nlobjSearchFilter("name", null, "is", "images"),
                new nlobjSearchFilter("parent", null, "is", _GetBundleFolderId())
            ];
            
            var columns = null;
            
            var sr = nlapiSearchRecord("folder", null, filters, columns);
            
            //Cache value
            _GetImagesFolderId.Value = sr == null ? null : sr[0].getId();
        }
        
        return _GetImagesFolderId.Value;
    }
    
    
    
    function _GetBundleFolderId()
    {
        var APP_GUID = "5dc7bc3b-9201-41c2-a0c0-860a01cc22b7.txt";
        
        if (_GetBundleFolderId.Value == null)  //Lazy load
        {
            //Search src folder id
            var filters = [new nlobjSearchFilter("name", null, "is", APP_GUID)];
            var columns = [new nlobjSearchColumn("folder")];
            var sr = nlapiSearchRecord("file", null, filters, columns);
            
            //Cache value
            _GetBundleFolderId.Value = nlapiLookupField("folder", sr[0].getValue("folder"), "parent");
        }

        return _GetBundleFolderId.Value;
    }



    function _CreateProfilesFolder()
    {
        var folder = nlapiCreateRecord("folder");
        folder.setFieldValue("parent", _GetImagesFolderId());
        folder.setFieldValue("name", "Profiles");

        return nlapiSubmitRecord(folder);
    }
    
    
    
    function _GetDomain()
    {
        if(_GetDomain.Value === undefined)
        {
            var url = nlapiResolveURL("SUITELET", "customscript_suitesocial_encryption_ss", "customdeploy_suitesocial_encryption_ss", true);
            var rawDomain = url.split(/\/\//)[0] + "//" + url.replace('http://','').replace('https://','').split(/[/?#]/)[0];
            
            var a = rawDomain.toString().split("://");
            var b = a[1].split(".");
            b[0] = "system"

            _GetDomain.Value = a[0] + "://" + b.join(".");
        }
        
        return _GetDomain.Value;
    }
    
    
    
    function _Netsuite_ImagingService()
    {
        this.Resize = _Resize;
        
        
        
        function _Resize(imageUrl, width, height)
        {
            var url = imageUrl + "&resizeid=-1"
            var response = nlapiRequestURL(url);
            
            var retVal = {
                IsSuccessful: response.getCode() == "200",
                Url: url,
                Content: response.getBody(),
            };
            
            return retVal;
        }
    }
    
}



SuiteSocial_Profile_UES.OnBeforeLoad = function _OnBeforeLoad(type, form)
{
    new SuiteSocial_Profile_UES().OnBeforeLoad(type, form);
}



SuiteSocial_Profile_UES.OnAfterSubmit = function _OnAfterSubmit()
{
    new SuiteSocial_Profile_UES().OnAfterSubmit();
}















