/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 12 Nov 2015 jvelasquez
 * 
 */

jQuery(function() {

    /**
     * Add Like ui elements and functionality
     */
    jQuery.widget("common_ui.like", {
        options : {
            internalid : 0,
            like_label_style : "",
            like_icon : "",
            like_label : "Like",
            likers_style : "style='display: none;'",
            likers_label : "",
            likes_info : "[]",
            like_type : ""
        },

        _create : function() {
            var _this = this;

            // like links
            var _likeHtml = jQuery("<div id='nfpostlike" + this.options.internalid + "' class='sf_nf_post_like' " + this.options.like_label_style + ">");
            _likeHtml.click(function() {
                _this._socialLikeUnlikePost(_this.options.internalid, _this.options.like_type);
            });
            _likeHtml.append("<img style='width: 15px; height: 15px; display: table; vertical-align: middle;' src='" + this.options.like_icon + "'/>");
            _likeHtml.append("<div style='display: table-cell; vertical-align: middle; padding-left: 5px;'>" + this.options.like_label + "</div>");

            // likers and likes count
            var likersHtml = "<div id='nfpostlikers" + this.options.internalid + "' class='sf_nf_post_likers' " + this.options.likers_style + ">" + this.options.likers_label + "</div>";
            var likersCountHtml = "<div id='nfpostlikesinfo" + this.options.internalid + "' style='display: none;'>" + this.options.likes_info + "</div>";

            jQuery(this.element).after(likersCountHtml).after(likersHtml).after(_likeHtml);
        },

        /**
         * Likes/unlikes a given post by the current user.
         * 
         * @param {integer}
         *        id - internal id of the post
         */
        _socialLikeUnlikePost : function(id, likeType) {
            var _this = this;
            // clear timeout of a previously set timeout just to ensure only the
            // latest
            // is sent to server
            if (ssapiHasValue(ssobjGlobal.lastTimeoutId)) {
                clearTimeout(ssobjGlobal.lastTimeoutId);
            }

            // get like id
            var likeId = "nfpostlike" + id;
            var likersId = "nfpostlikers" + id;
            var likesInfoId = "nfpostlikesinfo" + id;
            var likeIdSelector = "#" + likeId + " div";
            var likeIconSelector = "#" + likeId + " img";

            // check if user liked or unliked the post:
            // if label = Like ==> post is liked
            // if label = Unlike ==> post is unliked
            if (Ext.select(likeIdSelector).elements[0].innerHTML == ssobjGlobal.LABEL_LIKE) {
                // update label to Unlike
                Ext.select(likeIdSelector).update(ssobjGlobal.LABEL_UNLIKE);
                Ext.select(likeIconSelector).elements[0].src = ssobjGlobal.imagesUrlsObj['suitesocial_like.png'];

                // get current likers
                var likes = JSON.parse(Ext.get(likesInfoId).dom.innerHTML);
                // add current user
                likes.push({
                    id : ssobjGlobal.currentUserId,
                    name : ssobjGlobal.currentUserName
                });
                // update likes info
                _this._socialUpdateLikesInfo(likes, likersId, likesInfoId, likeType);
            } else {
                // update label to Like
                Ext.select(likeIdSelector).update(ssobjGlobal.LABEL_LIKE);
                Ext.select(likeIconSelector).elements[0].src = ssobjGlobal.imagesUrlsObj['suitesocial_unlike.png'];

                // get current likers
                var likes = JSON.parse(Ext.get(likesInfoId).dom.innerHTML);
                // remove current user
                for (var i = 0; i < likes.length; i++) {
                    if (likes[i].id == ssobjGlobal.currentUserId) {
                        likes.splice(i, 1);
                        break;
                    }
                }
                // update likes info
                _this._socialUpdateLikesInfo(likes, likersId, likesInfoId, likeType);
            }

            // delay the sending of Like status to server to ensure properly
            // captured
            // Like status
            ssobjGlobal.lastTimeoutId = setTimeout(function() {
                // check if user liked or unliked the post (this time it's the
                // reverse
                // of the above logic as the label is already changed):
                // if label = Like ==> post is unliked
                // if label = Unlike ==> post is liked
                if (Ext.select(likeIdSelector).elements[0].innerHTML == ssobjGlobal.LABEL_LIKE) {
                    // update post/like records
                    var data = {
                        id : id,
                        recType : ssobjGlobal.recordType || null,
                        recId : ssobjGlobal.recordId || null
                    };
                    socialSuiteletProcessAsync('unlikePost', data, function(newLikes) {
                        // check if newLikes has value
                        if (ssapiHasValue(newLikes)) {
                            // successfully unliked
                            _this._socialUpdateLikesInfo(newLikes, likersId, likesInfoId, likeType);
                        } else {
                            // failed removing like; set back label to Unlike
                            Ext.select(likeIdSelector).update(ssobjGlobal.LABEL_UNLIKE);
                            Ext.select(likeIconSelector).elements[0].src = ssobjGlobal.imagesUrlsObj['suitesocial_like.png'];
                        }
                    });
                } else {
                    // update post/like records
                    var data = {
                        id : id,
                        recType : ssobjGlobal.recordType || null,
                        recId : ssobjGlobal.recordId || null
                    };
                    socialSuiteletProcessAsync('likePost', data, function(newLikes) {
                        // check if newLikes has value
                        if (ssapiHasValue(newLikes)) {
                            // successfully liked
                            _this._socialUpdateLikesInfo(newLikes, likersId, likesInfoId, likeType);
                        } else {
                            // failed saving like; set back label to Like
                            Ext.select(likeIdSelector).update(ssobjGlobal.LABEL_LIKE);
                            Ext.select(likeIconSelector).elements[0].src = ssobjGlobal.imagesUrlsObj['suitesocial_unlike.png'];
                        }
                    });
                }
            }, 1000);
        },

        /**
         * Updates the Likes info display and the hidden likers info.
         * 
         * @param {Array}
         *        likes - array of likers object
         * @param {String}
         *        likersId - element id of the Likers display
         * @param {String}
         *        likesInfoId - element id of the hidden Likers info
         */
        _socialUpdateLikesInfo : function(likes, likersId, likesInfoId, likeType) {
            // update likers label
            Ext.get(likersId).update(ssapiGetLikersDisplay(likes, likeType));
            if (likes.length > 0) {
                Ext.get(likersId).setStyle("display", "block");
            } else {
                Ext.get(likersId).setStyle("display", "none");
            }
            // update hidden likers info
            Ext.get(likesInfoId).update(JSON.stringify(likes));
            // trigger likersChanged event, allow other functionalities to 
            // provide handling logic when likers changes
            // No impact if handling for likersChanged not specified
            jQuery('#' + likersId).trigger('likersChanged');
        }
    });

    /**
     * Multi-select UI by chosenJS
     * 
     */
    jQuery.widget("common_ui.chosenselect", {
		options: {
			style: '',
			deselectImgUrl: 'chosen-sprite.png',
			oclass: '',
			ostyle: '',
			showingDropdown: '',
			hidingDropdown: '',
			onChange: ''
		},
		
		_create: function() {
			var _self = this;
			
			if (_self.options.style !== '') {
				_self.options.ostyle = jQuery('#' + _self.element[0].id).attr('style');
				jQuery('#' + _self.element[0].id).attr('style', _self.options.style);
			}
			
			_self.options.oclass = jQuery('#' + _self.element[0].id).attr('class');
			jQuery('#' + _self.element[0].id).attr('class', 'chosen-select');
			
			var config = { '.chosen-select': {},
						   '.chosen-select-deselect':{ allow_single_deselect: true }, 
						   '.chosen-select-no-single': { disable_search_threshold: 10 }, 
						   '.chosen-select-no-results': { no_results_text: 'No Results' }, 
						   '.chosen-select-width': { width: '95%' } };
			
			for (var selector in config) {
				jQuery(selector).chosen(config[selector]); 
			}
			
            jQuery('select').change(function() {
                jQuery('.search-choice-close').css('background-image', 'url(' + _self.options.deselectImgUrl + ')');
                jQuery('.search-choice-close').css('margin-top', '0.5px');
            });
            
            if (_self.options.showingDropdown !== '') {
            	jQuery('#' + _self.element[0].id).on('chosen:showing_dropdown', _self.options.showingDropdown);
            }
            
            if (_self.options.hidingDropdown !== '') {
            	jQuery('#' + _self.element[0].id).on('chosen:hiding_dropdown', _self.options.hidingDropdown);
            }
            
            if (_self.options.onChange !== '') {
            	jQuery('#' + _self.element[0].id).on('change', _self.options.onChange);
            }
		},
		
		_destroy: function() {
			var _self = this;
			jQuery('#' + _self.element[0].id).attr('class', _self.options.oclass);
			jQuery('#' + _self.element[0].id).attr('style', _self.options.ostyle);
			jQuery('#' + _self.element[0].id + ' option').remove();
			jQuery('#' + _self.element[0].id + '_chosen').remove();
		}
	});
    
    jQuery.widget("common_ui.loading", {
		options: {
			id: '',
			imageUrl: '',
			zIndex: ''
		},
		
		_create: function() {
			var _self = this;
			
			var loadingHtml = "<div style='position:absolute;background-color: rgba(0,0,0,0.09);width:100%;height:100%;z-index:" + _self.options.zIndex + ";top:0%;left:0%'>" + 
				"<img id='"+ _self.options.id +"' style='visibility:visible;position: absolute;left: 45%;top:45%;z-index: " + (parseInt(_self.options.zIndex) + 1) +
				"' src='" + _self.options.imageUrl + "' />" + 
			"</div>";
			
			jQuery('#' + _self.element[0].id).append(loadingHtml);
		},
		
		_destroy: function() {
			var _self = this;
			jQuery('#' +  _self.options.id).parent().detach();
			jQuery('#' +  _self.options.id).detach();
		}
	});
});