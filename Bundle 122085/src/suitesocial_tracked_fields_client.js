/**
 * Client Script functions for Tracked Field user level exclusion
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 June 2012     cbulatao
 *
 */
function onSaveExcludeTrackedFields()
{
	var listCount = nlapiGetLineItemCount('custlist_tracked_fields');
	for ( var i = 1; i <= listCount; i++ )
	{
		if ( 'T' ==  nlapiGetLineItemValue('custlist_tracked_fields', 'custfield_is_tracked', i) ){
			return true;
		}
					
	}
	return confirm('Excluding all tracked fields for this record will result to not having any updates for this record type.'.tl() + '\n' + 'Do you still want to continue?'.tl());
	
}