/**
 * @author tcaguioa
 * a common client library that is being used by admin assistant client scripts
 */
// object for storing global variables
var ssobjGlobal = ssobjGlobal || {};

function uiRestletProcess(action, values){
    var data = {};
    data.action = action;
    data.values = values;
    var xmlRequest = new XMLHttpRequest();
    var url = nlapiResolveURL('SUITELET', 'customscript_suitesocial_generic_restlet', 'customdeploy_suitesocial_generic_restlet');
    // parameters
    xmlRequest.open('POST', url, false /* async*/);
    xmlRequest.setRequestHeader("Content-Type", "application/json");
    xmlRequest.send(JSON.stringify(data));
    return xmlRequest.responseText;
}

function uiSuiteletProcess(action, values){
    
    try {
        var logger = new ssobjLogger('uiSuiteletProcess');
        logger.log('action=' + action);
        var data = {};
        data.action = action;
        data.values = values;
        
        if (ssapiHasNoValue(ssobjGlobal.uiSuiteletProcessAsyncUrl)) {
            ssobjGlobal.uiSuiteletProcessAsyncUrl = nlapiResolveURL('SUITELET', 'customscript_social_admin_generic_sl', 'customdeploy_social_admin_generic_sl');
        }
        var url = ssobjGlobal.uiSuiteletProcessAsyncUrl;
		
		// need to place this declaration after the nlapiResolveURL call
		var xmlRequest = new XMLHttpRequest();
        // parameters
        xmlRequest.open('POST', url, false /* async*/);
        xmlRequest.setRequestHeader("Content-Type", "application/json");
        //    xmlRequest.send(data);
        
        
        xmlRequest.send(JSON.stringify(data));
        //        alert(xmlRequest.status)
        if (xmlRequest.status !== 200) {
            // error
            alert('An unexpected error occurred.');
            return false;
        }
        
        if (xmlRequest.responseText.indexOf('ERROR:') > -1) {
            alert(JSON.parse(xmlRequest.responseText));
            return false;
        }
        
        //<!--
        /**
         * For some reasons, the returned string sometimes contains debug information from core.
         * This debugging information start with <!--. If this string is found, process only the string before this.
         */
        var responseText = xmlRequest.responseText;
        var position = responseText.indexOf('<!--');
        if (position > -1) {
            responseText = responseText.substr(0, position);
        }
        var returnObject = JSON.parse(responseText);
        return returnObject;
        
    } 
    catch (e) {
        alert(e.toString() + '\n\n\nxmlRequest.responseText=' + xmlRequest.responseText);
    }
}

var errorMessageSummary = '';
function uiSuiteletProcessAsync(action, values){
    //	Issue: 215767 [SuiteSocial] Create code for entry of Subscription links
   
    try {
        var logger = new ssobjLogger('uiSuiteletProcessAsync');
        var sw = new ssobjStopWatch();
        
        window.status = 'Processing ' + action + '... ';
        logger.log('action=' + action);
        var data = {};
        data.action = action;
        data.values = values;
        if (ssapiHasNoValue(ssobjGlobal.uiSuiteletProcessAsyncUrl)) {
            ssobjGlobal.uiSuiteletProcessAsyncUrl = nlapiResolveURL('SUITELET', 'customscript_social_admin_generic_sl', 'customdeploy_social_admin_generic_sl');
        }
        var url = ssobjGlobal.uiSuiteletProcessAsyncUrl;
        
        if (action == REC_RECORD + '_add') {
            Ext.Msg.progress('Please wait'.tl(), 'Adding records'.tl() + '...');
        }
        //                nlapiSelectLineItem(REC_RECORD, values.lineNumber);
        //                nlapiSetCurrentLineItemValue(REC_RECORD, 'summary', 'Saving...');
        //                nlapiCommitLineItem(REC_RECORD);
        //                break;
        //        }
        var xmlRequest = new XMLHttpRequest();
        xmlRequest.onreadystatechange = function(){
            if (xmlRequest.readyState == 4 && xmlRequest.status == 200) {
                restletCompletedCount++;
                
                window.status = 'Process ' + action + ' succeeded. ms=' + sw.stop();
                //                ssapiWait('success action=' + action + '; values=' + JSON.stringify(values));
                logger.log('success action=' + action + '; values=' + JSON.stringify(values));
                
                var resp = JSON.parse(xmlRequest.responseText);
                //				alert(resp.action);
                //                logger.log('resp=' + JSON.parse(resp));
                switch (resp.action) {
                    //                    case 'updaterelatedrecordsfields':
                    //                        if (errorMessageSummary !== '') {
                    //                            Ext.Msg.hide();
                    //                            alert('The following record types were not saved. Make sure they are supported in this account: ' + errorMessageSummary);
                    //                        }
                    //                        
                    //                        if (goToNext) {
                    //                            uiGotoNext();
                    //                            return;
                    //                        }
                    //                        if (errorMessageSummary == '') {
                    //                            ssapiSaveOk();
                    //                        }
                    //                        break;
                    case REC_RECORD + '_add':
                        //					alert(resp)
                        var recordId = resp.recordId;
                        var newInternalId = resp.newInternalId;
                        var lineNumber = resp.lineNumber;
                        var recordName = resp.recordName;
                        nlapiSelectLineItem(recordId, lineNumber);
                        nlapiSetCurrentLineItemValue(recordId, FLD_INTERNAL_ID, newInternalId);
                        if (newInternalId == 0) {
							errorMessageSummary += ' - ' + recordName + '\n';
							ssobjGlobal.notEnabledRecordTypes = ssobjGlobal.notEnabledRecordTypes || [];
							ssobjGlobal.notEnabledRecordTypes.push(recordName) ;
                        }
                        else {
                            nlapiSetCurrentLineItemValue(recordId, 'summary', 'Tracked Fields'.tl() + ': 0   ' + 'Scheduled Posts'.tl() + ': 0   ' + 'Record Subscriptions'.tl() + ': 0   ' + 'Related Record Subscriptions'.tl() + ': 0');
                        }
                        
                        //                        nlapiSetCurrentLineItemValue(recordId, 'summary', 'Saved');
                        nlapiCommitLineItem(recordId);
                        //						Ext.Msg.hide();
                        Ext.Msg.progress('Please wait'.tl(), 'SuiteSocial' + ' Record'.tl() + ' "' + resp.recordName + '" ' + 'saved'.tl() + '.<br />' + 'Processing others'.tl() + '...');
                        Ext.Msg.updateProgress(restletCompletedCount / restletSubmittedCount);
                        //                        ssapiWait('SuiteSocial Record "' + resp.recordName + '" saved (' + restletCompletedCount + ' of ' + restletSubmittedCount + '). <br />Processing others...');
                        
                        if (restletCompletedCount == restletSubmittedCount) {
                            // all async requests done
                            restletCompletedCount = 0;
                            restletSubmittedCount = 0;
                            
                            //                            ssapiWait('Updating related records ...');
                            //                            uiSuiteletProcessAsync('updaterelatedrecordsfields', 'anyvalue');
                            nlapiSetFieldValue('nosavedrecords', 'F');
                            NS.form.setChanged(false);
                            if (errorMessageSummary !== '') {
                                Ext.Msg.hide();
                                alert('The following record types were not saved and will be removed from the list. Make sure they are supported in this account: '.tl() + '\n' + errorMessageSummary);
								
                                // for each unsupported record name, loop thru the items and remove line item
                                batchDeletion = true; // to prevent display of delete confirmation
                                for (var r = 0; r < ssobjGlobal.notEnabledRecordTypes.length; r++) {
                                    var recordName = ssobjGlobal.notEnabledRecordTypes[r];
                                    for (var lineNumber = 1; lineNumber <= nlapiGetLineItemCount(recordId); lineNumber++) {
                                        if (nlapiGetLineItemText(recordId, FLD_RECORD_TYPE, lineNumber) == recordName) {
                                            nlapiRemoveLineItem(recordId, lineNumber);
                                            break;
                                        }
                                    }
                                }
                                ssobjGlobal.notEnabledRecordTypes = [];
                                batchDeletion = null;
                            }
                            
                            if (goToNext) {
                                uiGotoNext();
                                return;
                            }
                            goToNext = false;
                            if (errorMessageSummary == '') {
                                ssapiSaveOk();
                            }
							errorMessageSummary = '';

                        }
                        
                        break;
                    default:
                    //default_statement;
                }
                
                
                
                
            }
            // xmlRequest.status == 0 happens when the page is navigating to another page
            if (xmlRequest.readyState == 4 && (xmlRequest.status !== 200 && xmlRequest.status !== 0)) {
                //				 restletSubmittedCount = 0;
                // restletCompletedCount++;
                uiShowError('System error: uiSuiteletProcessAsync(). xmlRequest.status=' + xmlRequest.status + '; values=' + JSON.stringify(values) + '; xmlRequest.responseText=' + xmlRequest.responseText);
            }
        };
        
        // parameters
        xmlRequest.open('POST', url, true /* async*/);
        xmlRequest.setRequestHeader("Content-Type", "application/json");
        xmlRequest.send(JSON.stringify(data));
        return;
    } 
    catch (e) {
        uiShowError(e.toString() + '<br />xmlRequest.responseText=' + xmlRequest.responseText);
    }
}

