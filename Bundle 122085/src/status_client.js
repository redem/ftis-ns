/*
 * @return {boolean} returns true if the current user has access to SuiteSocial
 */
function isCurrentUserHasSuiteSocialAccess(){
    try {
        nlapiSearchRecord('customrecord_suitesocial_profile', null, null, [new nlobjSearchColumn('internalid', null, 'count')]);
        return true;
    } 
    catch (e) {
        return false;
    }
}

function statusPageInit()
{
	// Issue: 213318 [SuiteSocial] Newsfeed and Status portlet should display a message
	if(isCurrentUserHasSuiteSocialAccess() === false){
		document.body.innerHTML = '<br /><span class=smalltext>&nbsp;' + 'Your current role has no access to SuiteSocial.'.tl() + '</span>';
		return;
	}
	
 
    if (window.opener != null)
        document.getElementById('comment').form.target='server_commands';
    setInterval('statusClientRefresh()',60000);
}


function statusClientRefresh()
{
    var url = nlapiResolveURL('SUITELET','customscript_suitesocial_status_refresh','customdeploy_suitesocial_status_refresh');
    document.getElementById("server_commands").src=url;
}