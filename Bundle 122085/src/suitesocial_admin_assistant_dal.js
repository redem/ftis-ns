/**
 * @author tcaguioa
 */
// object for storing global variables
var ssobjGlobal = ssobjGlobal || {};

/**
 * Deletes a suitesocial profile record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteProfile(id) {
    var logger = new ssobjLogger(arguments);
    // delete profile channels
    var childTypeId = 'customrecord_suitesocial_profile_channel';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_suitesocial_profile_ch_prof');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteProfileChannel(childId);
        }
    }

    // delete profile colleagues
    var childTypeId = 'customrecord_suitesocial_colleague';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_suitesocial_colleague_profile');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteProfileColleague(childId);
        }
    }

    // delete profile digest skeds
    var childTypeId = 'customrecord_suitesocial_digest_schedule';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_suitesocial_digest_sched_prof');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteProfileDigestSchedule(childId);
        }
    }

    // delete My Related Transaction Subs
    var childTypeId = 'customrecord_ss_my_related_transaction_s';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_ss_mrts_profile');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteMyRelatedTransactionSubs(childId);
        }
    }

    // delete Exclude Tracked Fields
    var childTypeId = 'customrecord_exclude_track_field';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_suitesocial_profile');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteExcludeTrackedField(childId);
        }
    }

    // delete My Saved Search Subscriptions
    var childTypeId = 'customrecord_ss_my_saved_search_sub';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_ss_msss_profile');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteMySavedSearchSubscriptionField(childId);
        }
    }

    // delete SuiteSocial Auto Subscribe (My Record Subcription)
    var childTypeId = 'customrecord_suitesocial_autosubscribe';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_suitesocial_autosubscribe_sub');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteSuiteSocialAutoSubscribe(childId);
        }
    }

    // delete My Related Record Subscriptions
    var childTypeId = 'customrecord_ss_subscription_link_sub';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_suitesocial_sublink_sub');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiMyRelatedRecordSubscription(childId);
        }
    }

    nlapiDeleteRecord('customrecord_suitesocial_profile', id);

    logger.end();
}

/**
 * Deletes a My Related Record Subscription record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiMyRelatedRecordSubscription(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_ss_subscription_link_sub', id);
    logger.log('DONE');
}

/**
 * Deletes a SuiteSocial Auto Subscribe (My Record Subcription) record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteSuiteSocialAutoSubscribe(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_suitesocial_autosubscribe', id);
    logger.log('DONE');
}

/**
 * Deletes a My Saved Search Subscription record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteMySavedSearchSubscriptionField(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_ss_my_saved_search_sub', id);
    logger.log('DONE');
}

/**
 * Deletes a Exclude Tracked Field record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteExcludeTrackedField(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_exclude_track_field', id);
    logger.log('DONE');
}

/**
 * Deletes a customrecord_ss_my_related_transaction_s record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteMyRelatedTransactionSubs(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_ss_my_related_transaction_s', id);
    logger.log('DONE');
}

/**
 * Deletes a customrecord_suitesocial_digest_schedule record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteProfileDigestSchedule(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_suitesocial_digest_schedule', id);
    logger.log('DONE');
}

/**
 * Deletes a customrecord_suitesocial_colleague record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteProfileColleague(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_suitesocial_colleague', id);
    logger.log('DONE');
}

/**
 * Deletes a customrecord_suitesocial_profile_channel record
 * 
 * @param {integer}
 *        id - internal id
 */
function ssapiDeleteProfileChannel(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_suitesocial_profile_channel', id);
    logger.log('DONE');
}

function ssapiGetRelatedRecords() {
    var logger = new ssobjLogger('ssapiGetRelatedRecords');
    if (ssapiHasValue(ssobjGlobal.GetRelatedRecords)) {
        return ssobjGlobal.GetRelatedRecords;
    }
    // var filters = [new nlobjSearchFilter(FLD_ISINACTIVE, null, 'is', 'F')];
    var columnIds = [ R_SS_RELATED_RECORD.RELATED_SS_RECORD, R_SS_RELATED_RECORD.SS_RECORD ];
    var results = ssapiGetResults(R.SS_RELATED_RECORD, columnIds);
    var mainAndRelatedNames = [];
    if (results === null) {
        return mainAndRelatedNames;
    }
    for (var i = 0; i < results.length; i++) {
        var result = results[i];
        mainAndRelatedNames.push(result.getText(R_SS_RELATED_RECORD.RELATED_SS_RECORD) + '-' + result.getText(R_SS_RELATED_RECORD.SS_RECORD));
    }
    logger.log('mainAndRelatedNames.join=' + mainAndRelatedNames.join(','));
    logger.log('DONE');

    ssobjGlobal.GetRelatedRecords = mainAndRelatedNames;
    return ssobjGlobal.GetRelatedRecords;
}

function ssapiAddRelatedRecord(mainRecordName, relatedRecordName) {
    var logger = new ssobjLogger(arguments);
    var newId;
    try {
        // check if already existing in the db
        var relatedRecords = ssapiGetRelatedRecords();
        var relatedRecordItem = mainRecordName + '-' + relatedRecordName;
        if (relatedRecords.indexOf(relatedRecordItem) > -1) {
            logger.log('existing. relatedRecordItem=' + relatedRecordItem);
            return 0;
        }

        var r = new SsRelatedRecord();
        // check if both record name are in the options list (they should be
        // active)
        var fld = r.getRecord().getField(R_SS_RELATED_RECORD.SS_RECORD);
        var options = fld.getSelectOptions();
        if (ssapiGetOptionByText(options, mainRecordName) === null) {
            logger.warn('ssapiGetOptionByText(options, mainRecordName) === null. Not a SS record. mainRecordName=' + mainRecordName);
            // this will happen if a script is scheduled and afterwards, the
            // record is made inactive
            return;
        }
        if (ssapiGetOptionByText(options, relatedRecordName) === null) {
            // this will happen if a script is scheduled and afterwards, the
            // record is made inactive
            logger.warn('ssapiGetOptionByText(options, relatedRecordName) === null. Not a SS record. relatedRecordName=' + relatedRecordName);
            return;
        }

        // now add
        logger.log('ADDING. relatedRecordItem=' + relatedRecordItem + '****************************************');

        // the next 2 fields are swapped because the original custom record
        // schema were interchanged
        r.setNameValue(relatedRecordName);
        r.setSsRecordText(relatedRecordName);
        r.setRelatedSsRecordText(mainRecordName);
        newId = r.submitRecord();
        // include in the existing related records listaq
        relatedRecords.push(relatedRecordItem);

        logger.log('DONE');

    } catch (e) {
        ssapiHandleError(e, ssapiGetArgumentDetails(arguments));
    }
    return newId;
}

function showWaitMessage() {
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (typeof document == 'undefined') {
        // exit if in the server
        return;
    }
    var firefoxMessage = '';
    if (Ext.isGecko) {
        // browser = "Firefox";
        firefoxMessage = '<br /><br />If a dialog box displays with the message below, check "Don\'t ask me again" and click "Continue" button in the displayed dialog box.<br /><br /><i>"A script on this page may be busy, or it may have stopped responding. You can stop the script now, open the script in the debugger, or let the script continue."</i><br />';
    }
    ssapiWait(firefoxMessage);
}

/*
 * retrieves the immediate newsfeeds (post to a channel, instead of comment to a
 * post) of a channel
 */
function retrieveChannelNewsfeed(parentId, childTypeId, childFieldId) {
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    // Issue: 214352 [SuiteSocial] Delete Including Dependent Records takes a
    // lon ..
    // var logger = new ssobjLogger('ssapiRetrieveChildren');
    var filters = [ new nlobjSearchFilter(childFieldId, null, OP_ANYOF, parentId) ];
    filters.push(new nlobjSearchFilter(FLD_NEWSFEED_PARENT, null, 'anyof', '@NONE@'));
    var childResults = ssapiRetrieveResults(childTypeId, [ FLD_INTERNAL_ID ], filters);
    if (childResults === null) {
        return null;
    }
    return childResults;
}

function deleteDefaultAutoSubTypeRole(id) {
    var logger = new ssobjLogger('deleteDefaultAutoSubTypeRole');

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    logger.log('id=' + id);
    nlapiDeleteRecord(REC_DEFAULT_AUTOSUB_ROLE, id);
}

function deleteDefaultAutoSubType(id, all) {
    var logger = new ssobjLogger(arguments);
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    showWaitMessage();

    logger.log('id=' + id);
    // default roles
    var childTypeId = REC_DEFAULT_AUTOSUB_ROLE;
    var childResults = ssapiRetrieveChildren(id, childTypeId, FLD_DEFAULT_AUTOSUB_ROLE_PARENT);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteDefaultAutoSubTypeRole(childId);
        }
    }
    nlapiDeleteRecord(REC_DEFAULT_AUTOSUB_TYPE, id);
}

function deleteAutoSubscribe(id) {
    var logger = new ssobjLogger('deleteAutoSubscribe');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    logger.log('id=' + id);
    nlapiDeleteRecord(REC_AUTOSUBSCRIBE, id);
}

function deleteRecordSubscriptionRole(id) {
    var logger = new ssobjLogger(arguments);

    logger.log('id=' + id);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_ss_record_subscription_role', id);
}

function deleteAutoSubscribeType(id, all) {
    var logger = new ssobjLogger('deleteAutoSubscribeType');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    showWaitMessage();

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    logger.log('id=' + id);

    // auto subscribe role
    var childTypeId = 'customrecord_ss_record_subscription_role';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_ssrsr_record_subscription');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteRecordSubscriptionRole(childId);
        }
    }

    // auto subscribe
    childTypeId = REC_AUTOSUBSCRIBE;
    childResults = ssapiRetrieveChildren(id, childTypeId, FLD_AUTOSUBSCRIBE_TYP);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteAutoSubscribe(childId);
        }
    }

    // default auto subscribe (handled differently)
    childTypeId = REC_DEFAULT_AUTOSUB_TYPE;
    var record = nlapiLoadRecord(REC_AUTOSUB_TYPE, id);
    var autoSubTypeName = record.getFieldValue(FLD_AUTOSUB_TYPE_NAME);
    var columns = [ new nlobjSearchColumn(FLD_INTERNAL_ID) ];
    var filters = [ new nlobjSearchFilter(FLD_DEFAULT_AUTOSUB_TYPE_NAME, null, OP_IS, autoSubTypeName) ];
    childResults = nlapiSearchRecord(REC_DEFAULT_AUTOSUB_TYPE, null, filters, columns);
    // var childResults = ssapiRetrieveChildren(autoSubTypeName, childTypeId,
    // FLD_DEFAULT_AUTOSUB_TYPE_NAME);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteDefaultAutoSubType(childId);
        }
    }

    // if (all) {
    // now delete
    nlapiDeleteRecord(REC_AUTOSUB_TYPE, id);
    // }
}

function deleteNewsFeedLink(id) {
    var logger = new ssobjLogger('deleteNewsFeedLink');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord(REC_NF_POST_LINK, id);
}

/**
 * Deletes the email capture link of a news feed post.
 * 
 * @param {Number}
 *        id - internal id of news feed post
 */
function deleteEmailCaptureLink(id) {
    var logger = new ssobjLogger('deleteEmailCaptureLink');
    // email capture link
    var childTypeId = 'customrecord_ss_email_capture';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_ss_email_capture_post_id');
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            nlapiDeleteRecord(childTypeId, childId);
        }
    }
}

/**
 * Deletes the child records of a news feed post.
 * 
 * @param {Number}
 *        id - internal id of news feed post
 */
function deleteNewsFeedChildRecords(id) {
    var logger = new ssobjLogger('deleteNewsFeedChildRecords');

    // email capture link
    deleteEmailCaptureLink(id);

    // newfeed link
    var childTypeId = REC_NF_POST_LINK;
    var childResults = ssapiRetrieveChildren(id, childTypeId, FLD_NEWSFEED_POST_LINK_POST);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteNewsFeedLink(childId);
        }
    }

    // mentions
    childTypeId = 'customrecord_suitesocial_post_mention';
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_sspm_news_feed');
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            nlapiDeleteRecord(childTypeId, childId);
        }
    }

    // news feed comments
    childTypeId = REC_NEWSFEED;
    childResults = ssapiRetrieveChildren(id, childTypeId, FLD_NEWSFEED_PARENT);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId + '; id=' + id);

            // check if has file, delete it too
            var dadFile = nlapiLookupField(childTypeId, childId, "custrecord_newsfeed_file");
            if (ssapiHasValue(dadFile)) {
                dadFile = JSON.parse(dadFile);
                // delete main file
                nlapiDeleteFile(dadFile.fileId);
                // delete preview file if any
                if (ssapiHasValue(dadFile.previewFileId))
                    nlapiDeleteFile(dadFile.previewFileId);
            }

            // email capture link
            deleteEmailCaptureLink(childId);

            nlapiDeleteRecord(childTypeId, childId);
        }
    }

    // queue deletion of orphan likes
    var params = {};
    params.command = 'ssapiDeleteOrphanNewsfeedLikes';
    ssapiQueue(params);
}

/**
 * Deletes a news feed post and its child records.
 * 
 * @param {Number}
 *        id - internal id of news feed post
 */
function deleteNewsFeed(id) {
    var logger = new ssobjLogger('deleteNewsFeed');

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    logger.log('id=' + id);

    // set the newsfeed record to inactive first
    nlapiSubmitField(REC_NEWSFEED, id, "isinactive", "T");

    // delete news feed child records first
    deleteNewsFeedChildRecords(id);

    try {
        // now delete
        nlapiDeleteRecord(REC_NEWSFEED, id);
    } catch (e) {
        // handle post whose child records has not been deleted
        if (e.getCode) {
            if (e.getCode() == 'THIS_RECORD_CANNOT_BE_DELETED_BECAUSE_IT_HAS_DEPENDENT_RECORDS') {
                // delete news feed child records
                deleteNewsFeedChildRecords(id);

                // now delete
                nlapiDeleteRecord(REC_NEWSFEED, id);
                return;
            }
        }
        throw e;
    }
}

// Issue: 214562 [SuiteSocial] Delete Including Dependent Records takes a lon
function deleteAutoPostSchedule(id, all) {
    var logger = new ssobjLogger('deleteAutoPostSchedule');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord(REC_AUTOPOST_SCHEDULE, id);
}

function deleteAutoPost(id, all) {
    var logger = new ssobjLogger('deleteAutoPost');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    var childTypeId = REC_NEWSFEED;
    // Issue: 214562 [SuiteSocial] Delete Including Dependent Records takes a
    // long tim
    // changed the linked field
    var childResults = ssapiRetrieveChildren(id, childTypeId, FLD_NEWSFEED_AUTOPOST);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteNewsFeed(childId);
        }
    }
    // Issue: 214562 [SuiteSocial] Delete Including Dependent Records takes a
    // lon
    // auto post schedule
    childTypeId = REC_AUTOPOST_SCHEDULE;
    childResults = ssapiRetrieveChildren(id, childTypeId, FLD_AUTOPOST_SCHEDULE_AUTOPOST_SCHED_AUTOPOST);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteAutoPostSchedule(childId);
        }
    }
    nlapiDeleteRecord(REC_AUTOPOST, id);
}

function deleteProfileChannel(id) {
    var logger = new ssobjLogger('deleteProfileChannel');

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord(REC_PROFILE_CHANNEL, id);
}

function deleteChannel(id, all) {
    var logger = new ssobjLogger('deleteChannel');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    // prevent deletion of built in channels
    if (id == 1 || id == 13 || id == 14) {
        throw 'Built in channels cannot be deleted. id=' + id;
    }

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    showWaitMessage();

    // profile channel
    var childTypeId = REC_PROFILE_CHANNEL;
    var childResults = ssapiRetrieveChildren(id, childTypeId, FLD_PROFILE_CH_CH);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteProfileChannel(childId);
        }
    }
    // auto post
    childTypeId = REC_AUTOPOST;
    childResults = ssapiRetrieveChildren(id, childTypeId, FLD_AUTOPOST_CHANNEL);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteAutoPost(childId);
        }
    }
    // news feed
    childTypeId = REC_NEWSFEED;
    childResults = retrieveChannelNewsfeed(id, childTypeId, FLD_NEWSFEED_CHANNEL);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteNewsFeed(childId);
        }
    }
    // if (all) {
    // now delete
    nlapiDeleteRecord(REC_NEWSFEED_CHANNEL, id);
    // }
}

function deleteExcludeTrackedField(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_exclude_track_field', id);
}

function deleteAutoPostField(id) {
    var logger = new ssobjLogger('deleteAutoPostField');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    // delete child newsfeed records
    // news feed
    var childTypeId = REC_NEWSFEED;
    var childResults = ssapiRetrieveChildren(id, childTypeId, FLD_NEWSFEED_AUTOPOST_FIELD);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteNewsFeed(childId);
        }
    }

    // exlude tracked field
    childTypeId = 'customrecord_exclude_track_field';
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_tracked_field');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteExcludeTrackedField(childId);
        }
    }

    nlapiDeleteRecord(REC_AUTOPOST_FIELD, id);
}

function deleteSubscriptionLink(id) {
    // Issue: 217411 [SuiteSocial] Subscription link > edit > delete > delete
    var logger = new ssobjLogger('deleteSubscriptionLink');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    // news feed
    var childTypeId = 'customrecord_ss_subscription_link_sub';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_suitesocial_sublink_link');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteProfileSubscriptionLink(childId);
        }
    }

    nlapiDeleteRecord(REC_SUBSCRIPTION_LINK, id);
}

function deleteRelatedRecord(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    // 
    var childTypeId = 'customrecord_ss_subscription_link';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_sssl_ss_rec');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteSubscriptionLink(childId);
        }
    }

    nlapiDeleteRecord('customrecord_ssr_ss_related_record', id);
}

function ssapiDeleteProfileSubscriptionLink(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_ss_subscription_link_sub', id);
    logger.log('DONE');
}

function ssapiDeleteMySavedSearchSubscription(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    nlapiDeleteRecord('customrecord_ss_my_saved_search_sub', id);
}

function ssapiDeleteSavedSearchSubscription(id) {
    var logger = new ssobjLogger(arguments);

    if (ssapiIsNumber(id) === false) {
        throw 'ssapiIsNumber(id) = false; id=' + id;
    }

    var childTypeId = 'customrecord_ss_my_saved_search_sub';
    var childResults = ssapiRetrieveChildren(id, childTypeId, R_MY_SAVED_SEARCH_SUBSCRIPTION.SAVED_SEARCH);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteMySavedSearchSubscription(childId);
        }
    }

    nlapiDeleteRecord('customrecord_ss_saved_search_subs', id);
}

function deleteRecordType(id, all) {
    var logger = new ssobjLogger(arguments);
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    showWaitMessage();

    // saved search
    var childTypeId = 'customrecord_ss_saved_search_subs';
    var childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_sss_record_type');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (var q = 0; q < childResults.length; q++) {
            var childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            ssapiDeleteSavedSearchSubscription(childId);
        }
    }

    // news feed
    childTypeId = REC_NEWSFEED;
    childResults = retrieveChannelNewsfeed(id, childTypeId, FLD_NEWSFEED_AUTOPOST_RECORD);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteNewsFeed(childId);
        }
    }

    // auto post field
    ssapiWait('Deleting Auto Post Fields...');
    childTypeId = REC_AUTOPOST_FIELD;
    childResults = ssapiRetrieveChildren(id, childTypeId, FLD_AUTOPOST_FLD_REC);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteAutoPostField(childId, true);
        }
    }

    // auto subscribe type
    ssapiWait('Deleting AutoSubscribe Types...');
    childTypeId = REC_AUTOSUB_TYPE;
    childResults = ssapiRetrieveChildren(id, childTypeId, FLD_AUTOSUB_TYPE_REC);
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteAutoSubscribeType(childId, true);
        }
    }
    // autopost (scheduled post)
    ssapiWait('Deleting Scheduled Posts...');
    childTypeId = REC_AUTOPOST;
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_autopost_record');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteAutoPost(childId, true);
        }
    }
    // subscription links - field 1
    ssapiWait('Deleting Subscription Links...');
    childTypeId = 'customrecord_ss_subscription_link';
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_sssl_rel_ss_rec');
    logger.log('childTypeId=' + childTypeId + '; childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteSubscriptionLink(childId, true);
        }
    }
    // subscription links - field 2
    ssapiWait('Deleting Subscription Links...');
    childTypeId = 'customrecord_ss_subscription_link';
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_sssl_ss_rec_ss_rec');
    logger.log('childTypeId=' + childTypeId + '; childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteSubscriptionLink(childId, true);
        }
    }

    // Issue 215767 [SuiteSocial] Create code for adding and deleting Related
    // Records
    // related ()
    ssapiWait('Deleting Related Records...');
    childTypeId = 'customrecord_ssr_ss_related_record';
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_ssrr_ss_record');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteRelatedRecord(childId, true);
        }
    }
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_ssrr_related_ss_record');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            deleteRelatedRecord(childId, true);
        }
    }

    // related trans

    ssapiWait('Deleting Related Transaction Subscriptions...');
    childTypeId = 'customrecord_ss_my_related_transaction_s';
    childResults = ssapiRetrieveChildren(id, childTypeId, 'custrecord_ss_mrts_ss_record');
    logger.log('childResults=' + childResults);
    if (childResults !== null) {
        for (q = 0; q < childResults.length; q++) {
            childId = childResults[q].getValue(FLD_INTERNAL_ID);
            logger.log('deleting childTypeId=' + childTypeId + '; childId=' + childId);
            nlapiDeleteRecord(childTypeId, childId);
        }
    }

    // now delete
    nlapiDeleteRecord(REC_RECORD, id);
}

function ssapiDeleteRecordType(id, all) {
    deleteRecordType(id, all);
}

function addAddedAutoSub(values) {
    var logger = new ssobjLogger('addAddedAutoSub');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    var newRecord = nlapiCreateRecord(REC_ADDED_AUTOSUB);
    // for (var p in values) {
    // logger.log('p=' + p);
    // logger.log('values[p]=' + values[p]);
    // newRecord.setFieldValue(p, values[p]);
    // }
    newRecord.setFieldValue(FLD_ADDED_AUTOSUB_PROFILE_ID, parseInt(values[FLD_ADDED_AUTOSUB_PROFILE_ID], 10));
    newRecord.setFieldValue(FLD_ADDED_AUTOSUB_TYPE_ID, parseInt(values[FLD_ADDED_AUTOSUB_TYPE_ID], 10));
    var retValue = nlapiSubmitRecord(newRecord);
    return retValue;
}

// function addChannel(){
// var logger = new ssobjLogger('Dal.addChannel');
// newRecord = nlapiCreateRecord(REC_NEWSFEED_CHANNEL, {
// recordmode: 'dynamic'
// });
// newRecord.setFieldValue(FLD_NEWSFEED_CHANNEL_NAME,
// values[FLD_NEWSFEED_CHANNEL_NAME]);
// var error = validateChannel();
//    
//    
//    
//    
// retValue = nlapiSubmitRecord(newRecord, true, true);
// return retValue;
// }

function addAutoSubscribe(values) {
    var logger = new ssobjLogger('Dal.addAutoSubscribe');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    var newRecord = nlapiCreateRecord(REC_AUTOSUBSCRIBE, {
        recordmode : 'dynamic'
    });
    newRecord.setFieldValue(FLD_AUTOSUBSCRIBE_SUB, values[FLD_AUTOSUBSCRIBE_SUB]);
    newRecord.setFieldValue(FLD_AUTOSUBSCRIBE_TYP, values[FLD_AUTOSUBSCRIBE_TYP]);
    var retValue = nlapiSubmitRecord(newRecord, true, true);
    return retValue;
}

function addProfile(values) {
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    // var logger = new ssobjLogger('Dal.addProfile');
    // // check 1st if not existing
    // var columns = [new nlobjSearchColumn('internalid')];
    // var filters = [new nlobjSearchFilter('role', null, 'anyof', roleIds)];
    // var results = piSearchRecord('employee', null, filters, columns);
    // if(results === null){
    // // no employee found
    // return true;
    // }
    //	
    //	
    //	
    var newRecord = nlapiCreateRecord(REC_PROFILE, {
        recordmode : 'dynamic'
    });
    newRecord.setFieldValue(FLD_PROFILE_EMP, values[FLD_PROFILE_EMP]);
    // newRecord.setFieldText(FLD_RECORD_TYPE, values[FLD_RECORD_TYPE]);
    var retValue = nlapiSubmitRecord(newRecord, true, true);
    return retValue;
}

function addRecord(values) {
    var logger = new ssobjLogger(arguments);
    var retValue = null;
    var recordTypeName = values[FLD_RECORD_TYPE];
    logger.log('recordTypeName=' + recordTypeName);
    // check if record name is passed
    if (ssapiHasNoValue(recordTypeName)) {
        throw 'ssapiHasNoValue(recordTypeName)';
    }
    // check if the record name exists
    if (ssapiGetRecordIdByName(recordTypeName) === null) {
        throw 'Invalid record type name. recordTypeName=' + recordTypeName;
    }
    // try running create to make sure record is supported
    var recordScriptId = ssapiGetRecordScriptIdByName(recordTypeName);
    var record = ssapiCreateRecord(recordScriptId);
    if (ssapiHasNoValue(record)) {
        logger.log('Error in nlapiCreateRecord(). recordTypeName=' + recordTypeName);
        return 0;
    }
    var newRecord = nlapiCreateRecord(REC_RECORD, {
        recordmode : 'dynamic'
    });
    newRecord.setFieldValue(FLD_RECORD_NAME, recordTypeName);
    newRecord.setFieldText(FLD_RECORD_TYPE, recordTypeName);
    newRecord.setFieldValue(FLD_RECORD_AUTOPOST, values[FLD_RECORD_AUTOPOST]);
    retValue = nlapiSubmitRecord(newRecord, true, true);

    ssapiSaveSuiteSocialFields(recordTypeName);
    return retValue;
}

function ssapiIsRecordSubscriptionNameExists(autoSubscribeName) {
    var logger = new ssobjLogger(arguments);
    // check if the name already exists
    if (ssapiRowExists(REC_AUTOSUB_TYPE, FLD_AUTOSUB_TYPE_NAME, 'is', autoSubscribeName)) {
        logger.log('Record Subscription Name '.tl() + '"' + autoSubscribeName + '" ' + 'already exists.'.tl());
        // alert('Record Subscription Name "' + autoSubscribeName + '" already
        // exists.');
        return true;
    }
    // not existing
    return false;
}

/**
 * 
 * @param {String}
 *        autoSubscribeRecordName
 * @param {String}
 *        fieldName
 * @param {String}
 *        savedSearchId Internal id of the saved search
 * @return {Boolean} true if the combination already exists
 */
function ssapiIsRecordSubscriptionCombinationExists(autoSubscribeRecordName, fieldName, savedSearchName) {
    var logger = new ssobjLogger(arguments);
    var s = new RecordSubscriptionSearch();
    var results = s.searchRecord();
    if (results === null) {
        return false;
    }
    for (var i = 0; i < results.length; i++) {
        var result = results[i];

        if (result.getText(s.SUITESOCIAL_RECORD_TYPE) == autoSubscribeRecordName && result.getText(s.FIELD) == fieldName && result.getText(s.SAVED_SEARCH) == savedSearchName) {
            return true;
        }
    }
    logger.log('DONE');
    return false;
}

function addAutoSubType(values) {
    var logger = new ssobjLogger('Dal.addAutoSubType');
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    // check first if the record type already exists, if not create
    var filters = [ new nlobjSearchFilter(FLD_RECORD_NAME, null, 'is', values[FLD_AUTOSUB_TYPE_REC]) ];
    var columns = [ new nlobjSearchColumn('internalid') ];
    if (nlapiSearchRecord(REC_RECORD, null, filters, columns) === null) {
        var ssRecordValues = {};
        ssRecordValues[FLD_RECORD_TYPE] = values[FLD_AUTOSUB_TYPE_REC];
        ssRecordValues[FLD_RECORD_AUTOPOST] = 'F';
        if (addRecord(ssRecordValues) === false) {
            return false;
        }
    }

    // now add the auto sub type
    var newRecord = nlapiCreateRecord(REC_AUTOSUB_TYPE, {
        recordmode : 'dynamic'
    });

    // logger.log('values=' + values);
    // for (var p in values) {
    // logger.log('p=' + p);
    // logger.log('value of p=' + values[p]);
    //        
    // }
    // logger.log('values[FLD_AUTOSUB_TYPE_REC]=' +
    // values[FLD_AUTOSUB_TYPE_REC]);

    // / required fields
    var requiredFields = [ FLD_AUTOSUB_TYPE_REC, FLD_AUTOSUB_TYPE_FLD, 'name' ];
    for ( var i in requiredFields) {
        var fieldId = requiredFields[i];
        logger.log('fieldId=' + fieldId);
        if (ssapiHasNoValue(values[fieldId])) {
            throw 'if( ssapiHasNoValue(values[fieldId])){ fieldId=' + fieldId;
        }
    }
    newRecord.setFieldText(FLD_AUTOSUB_TYPE_REC, values[FLD_AUTOSUB_TYPE_REC]);
    newRecord.setFieldText(FLD_AUTOSUB_TYPE_FLD, values[FLD_AUTOSUB_TYPE_FLD]);
    newRecord.setFieldText('custrecord_suitesocial_autosub_type_rf', values[FLD_AUTOSUB_TYPE_FLD]);
    newRecord.setFieldValue('name', values.name);
    var retValue = nlapiSubmitRecord(newRecord, true, true);
    return retValue;
}

/*
 * adds a default auto-subscribe type and role
 */
function addDefaultAutoSubscribeAndRole(autoSubscribeName, autoSubscribeRecordName, fieldName, roleName) {
    var logger = new ssobjLogger(arguments);
    if (ssapiHasNoValue(autoSubscribeName)) {
        throw 'ssapiHasNoValue(autoSubscribeName)';
    }
    if (ssapiHasNoValue(autoSubscribeRecordName)) {
        throw 'ssapiHasNoValue(autoSubscribeRecordName)';
    }
    if (ssapiHasNoValue(fieldName)) {
        throw 'ssapiHasNoValue(fieldName)';
    }
    if (ssapiHasNoValue(roleName)) {
        throw 'ssapiHasNoValue(roleName)';
    }
    if (ssapiIsRecordSubscriptionNameExists(autoSubscribeName)) {
        throw 'ssapiIsRecordSubscriptionNameExists(autoSubscribeName). autoSubscribeName=' + autoSubscribeName;
    }
    if (ssapiIsRecordSubscriptionCombinationExists(autoSubscribeRecordName, fieldName)) {
        throw 'ssapiIsRecordSubscriptionCombinationExists(autoSubscribeRecordName, fieldName. autoSubscribeRecordName=' + autoSubscribeRecordName + '; fieldName=' + fieldName;
    }

    // // check if SS record already exists
    // var ssRecordInternalId = ssapiGetRecordId(REC_RECORD, FLD_RECORD_NAME,
    // 'is', autoSubscribeRecordName);
    // if (ssRecordInternalId === null) {
    // // create
    // var values = {};
    // values[FLD_RECORD_NAME] = autoSubscribeRecordName;
    // ssRecordInternalId = addRecord(values);
    // }

    var param = {};
    param.name = autoSubscribeName;
    param[FLD_AUTOSUB_TYPE_REC] = autoSubscribeRecordName;
    param[FLD_AUTOSUB_TYPE_FLD] = fieldName;
    var newParentId = addAutoSubType(param);

    // child role
    var role = new SsRecordSubscriptionRole();
    role.setRecordSubscriptionText(autoSubscribeName);
    role.setRoleText(roleName);
    role.submitRecord();

    return newParentId;

    // newRecord = nlapiCreateRecord(REC_DEFAULT_AUTOSUB_ROLE, {
    // recordmode: 'dynamic'
    // });
    // newRecord.setFieldValue(FLD_DEFAULT_AUTOSUB_ROLE_PARENT, newParentId);
    // newRecord.setFieldText(FLD_DEFAULT_AUTOSUB_ROLE_ID, roleName);
    // nlapiSubmitRecord(newRecord);

    // newRecord = nlapiCreateRecord(REC_DEFAULT_AUTOSUB_TYPE, {
    // recordmode: 'dynamic'
    // });
    // newRecord.setFieldValue(FLD_DEFAULT_AUTOSUB_TYPE_NAME,
    // autoSubscribeName);
    // // Issue: 214452 [SuiteSocial] Install of bundle > Error in generating th
    // newRecord.setFieldText('custpage_record', autoSubscribeRecordName);
    // newRecord.setFieldText(FLD_DEFAULT_AUTOSUB_TYPE_RT,
    // autoSubscribeRecordName);
    // newRecord.setFieldText(FLD_DEFAULT_AUTOSUB_TYPE_FLD, fieldName);
    // newRecord.setFieldText('custrecord_ss_def_autosub_type_rf', fieldName);
    // newParentId = nlapiSubmitRecord(newRecord);

    // // child role
    // newRecord = nlapiCreateRecord(REC_DEFAULT_AUTOSUB_ROLE, {
    // recordmode: 'dynamic'
    // });
    // newRecord.setFieldValue(FLD_DEFAULT_AUTOSUB_ROLE_PARENT, newParentId);
    // newRecord.setFieldText(FLD_DEFAULT_AUTOSUB_ROLE_ID, roleName);
    // nlapiSubmitRecord(newRecord);
}

function updateAutoSubType(values) {
    var logger = new ssobjLogger(arguments);
    // next line used by ssapiGetFunctionCallDetails
    ssobjGlobal.args = arguments;

    // // check first if the record type already exists, if not create
    // // FLD_AUTOSUB_TYPE_REC is a from a getFieldText
    // var filters = [new nlobjSearchFilter(FLD_RECORD_NAME, null, 'is',
    // values[FLD_AUTOSUB_TYPE_REC])];
    // var columns = [new nlobjSearchColumn(FLD_INTERNAL_ID)];
    // if (nlapiSearchRecord(REC_RECORD, null, filters, columns) === null) {
    // var ssRecordValues = {};
    // ssRecordValues[FLD_RECORD_TYPE] = values[FLD_AUTOSUB_TYPE_REC];
    // ssRecordValues[FLD_RECORD_AUTOPOST] = 'F';
    // if (addRecord(ssRecordValues) == false) {
    // return false;
    // }
    // }

    // now load the auto sub type
    var record = nlapiLoadRecord(REC_AUTOSUB_TYPE, values.internalid, {
        recordmode : 'dynamic'
    });

    // logger.log('values=' + values);
    // for (var p in values) {
    // logger.log('p=' + p);
    // logger.log('value of p=' + values[p]);
    //        
    // }
    // logger.log('values[FLD_AUTOSUB_TYPE_REC]=' +
    // values[FLD_AUTOSUB_TYPE_REC]);

    // / required fields
    var requiredFields = [ FLD_AUTOSUB_TYPE_REC, FLD_AUTOSUB_TYPE_FLD, 'name' ];
    for ( var i in requiredFields) {
        var fieldId = requiredFields[i];
        logger.log('fieldId=' + fieldId);
        if (ssapiHasNoValue(values[fieldId])) {
            throw 'if( ssapiHasNoValue(values[fieldId])){ fieldId=' + fieldId;
        }
    }
    record.setFieldText(FLD_AUTOSUB_TYPE_REC, values[FLD_AUTOSUB_TYPE_REC]);
    record.setFieldText(FLD_AUTOSUB_TYPE_FLD, values[FLD_AUTOSUB_TYPE_FLD]);
    record.setFieldValue('name', values.name);
    var retValue = nlapiSubmitRecord(record, true, true);
    return retValue;
}
