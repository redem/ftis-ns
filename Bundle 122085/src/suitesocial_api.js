/**
 * � 2014 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 * 
 * Module Description: This is an API for integration with SuiteSocial. This is
 * being called by a RESTlet through which the other SuiteApps and third party
 * apps communicate to SuiteSocial.
 * 
 * Version Date Author Remarks 1.00 10 Apr 2015 eligon
 * 
 */

var MAX_SIZE_ALL_EMP = 1000;
var MAX_SIZE_SINGLE_EMP = 25;

/**
 * Main API for integration to SuiteSocial.
 */
var SuiteSocialAPI = (function() {
    var logger = new ssobjLogger("SuiteSocialAPI");

    // private functions definition

    /**
     * Get posts
     */
    var getPostsPrivate = function(data) {
        logger.log("getPosts");

        // check required parameters; viewasuserid
        if (ssapiHasNoValue(data.viewasuserid)) {
            throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "A required parameter 'viewasuserid' is missing.");
        }

        // check viewasuserid parameter
        if (ssapiIsNumber(data.viewasuserid)) {
            if (ssapiHasValue(data.size)) {
                if (!ssapiIsNumber(data.size) || data.size < 1) {
                    throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for property 'size': " + data.size);
                }
            }
        } else if (data.viewasuserid.toLowerCase() == "all") {
            if (ssapiHasValue(data.size)) {
                if (!ssapiIsNumber(data.size) || data.size < 1) {
                    throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for property 'size': " + data.size);
                }
            }
        } else {
            throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for parameter 'viewasuserid': " + data.viewasuserid);
        }

        // If any of these parameters exist, 'filter' parameter is required
        if (ssapiHasNoValue(data.filter) && (ssapiHasValue(data.channelid) || ssapiHasValue(data.authorid) || ssapiHasValue(data.recordtypescriptid) || ssapiHasValue(data.recordid))) {
            throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "Missing Required 'filter' property.");
        }

        // check filter parameter for the valid values
        if (ssapiHasValue(data.filter)) {
            if (data.filter == 'channel') {
                if (ssapiHasNoValue(data.channelid)) {
                    throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "Missing Required 'channelid' property.");
                }

                if (!ssapiIsNumber(data.channelid)) {
                    throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for property 'channelid': " + data.channelid);
                }

            } else if (data.filter == 'author') {
                if (ssapiHasNoValue(data.authorid)) {
                    throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "Missing Required 'authorid' property.");
                }

                if (!ssapiIsNumber(data.authorid)) {
                    throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for property 'authorid': " + data.authorid);
                }

            } else if (data.filter == 'record') {
                if (ssapiHasNoValue(data.recordtypescriptid)) {
                    throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "Missing Required 'recordtypescriptid' property");
                }

                if (ssapiHasNoValue(data.recordid)) {
                    throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "Missing Required 'recordid' property");
                }

                if (!ssapiIsNumber(data.recordid)) {
                    throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for property 'recordid': " + data.recordid);
                }

            } else {
                throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for 'filter' parameter: " + data.filter);
            }
        }

        // check lessThanPostId parameter
        if (ssapiHasValue(data.lessthanpostid)) {
            if (!ssapiIsNumber(data.lessthanpostid)) {
                throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for parameter 'lessThanPostId': " + data.lessthanpostid);
            }
        }

        // Process the parameter size
        var maxSize = MAX_SIZE_SINGLE_EMP;
        if (data.viewasuserid.toLowerCase() == "all") {
            data.viewasuserid = null;
            maxSize = MAX_SIZE_ALL_EMP;
        }

        if (ssapiHasNoValue(data.size) || data.size > maxSize) {
            data.size = maxSize;
        }

        // Process the parameter filter
        if (data.filter == 'channel') {
            data.authorid = null;
            data.recordtypescriptid = null;
            data.recordid = null;
        } else if (data.filter == 'author') {
            data.channelid = null;
            data.recordtypescriptid = null;
            data.recordid = null;
        } else if (data.filter == 'record') {
            data.channelid = null;
            data.authorid = null;
        }

        // get posts
        var feeds = SuiteSocialAPIHelper.getNewsFeedWall(data);
        logger.log("feeds = " + feeds);
        return ssapiHasNoValue(feeds) ? [] : feeds;
    };

    // public functions definition
    return {
        getPosts : function(data) {
            return getPostsPrivate(data);
        },

        getUsers : function() {
            return SuiteSocialAPIHelper.getUsers();
        },

        getChannels : function() {
            return SuiteSocialAPIHelper.getChannels();
        },

        getFollowing : function(data) {
            return SuiteSocialAPIHelper.getFollowing(data);
        },

        getFollowers : function(data) {
            return SuiteSocialAPIHelper.getFollowers(data);
        }
    };

})();

/**
 * Helper class for SuiteSocial API.
 */
var SuiteSocialAPIHelper = (function() {
    // private functions definition

    /**
     * Gets the news feed posts.
     * 
     * @param {Object}
     *        data - filters for getting news feed posts
     * @returns {Any[]} array of news feed post objects
     */
    var getNewsFeedWallPrivate = function(data) {
        var logger = new suitesocial.Helper.logger(arguments, false, 'getNewsFeedWallPrivate');
        logger.log("data = " + JSON.stringify(data));

        // call helper getNewsFeedWall
        return suitesocial.Helper.getNewsFeedWall(data.viewasuserid, 0 /* offset */, data.size, data.channelid, data.recordtypescriptid, data.recordid, null /* parent */, null /* segmentinfo */, null /* starttime */, null /* startid */, null /* internalIds */, data.authorid, null /* showQuickView */, data.lessthanpostid, null /* greaterThanId */, data.searchstring, true /* isApiCall */);
    };

    /**
     * Gets the list of active users.
     * 
     * @returns [user] See https://confluence.corp.netsuite.com/x/tQJpAg
     */
    var getUsersPrivate = function() {
        var filters = [];
        filters.push([ 'istemplate', 'is', 'F' ]);
        filters.push('and');
        filters.push([ 'isinactive', 'is', 'F' ]);

        var columns = [];
        columns.push(new nlobjSearchColumn('internalid'));
        columns[0].setSort();
        columns.push(new nlobjSearchColumn('lastname'));
        columns.push(new nlobjSearchColumn('firstname'));
        columns.push(new nlobjSearchColumn('title'));
        columns.push(new nlobjSearchColumn('email'));
        columns.push(new nlobjSearchColumn('department'));
        var results = ssapiSearchAllRecords2('employee', null, filters, columns);
        var count = results.length;
        var objUsers = [];
        for (var i = 0; i < count; i++) {
            var result = results[i];
            var email = result.getValue('email');
            if (ssapiHasNoValue(email)) {
                email = null;
            }
            var title = result.getValue('title');
            if (ssapiHasNoValue(title)) {
                title = null;
            }
            var objDepartment = null;
            if (ssapiHasValue(result.getValue('department'))) {
                objDepartment = {
                    id : result.getValue('department'),
                    name : result.getText('department')
                };
            }
            var objUser = {
                id : result.getId(),
                lastName : result.getValue('lastname'),
                firstName : result.getValue('firstname'),
                email : email,
                userType : 'employee',
                title : title,
                department : objDepartment
            };
            objUsers.push(objUser);
        }
        return objUsers;
    };

    /**
     * Gets the list of active channels.
     * 
     * @returns {Object[]} - array of channel objects
     */
    var getChannelsPrivate = function() {
        var logger = new suitesocial.Helper.logger(arguments, false, 'getChannelsPrivate');

        var filters = [];
        filters.push([ 'isinactive', 'is', 'F' ]);
        var columns = [];
        columns.push(new nlobjSearchColumn('internalid'));
        columns[0].setSort();
        columns.push(new nlobjSearchColumn('name'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_dept'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_loc'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_role'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_emp'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_posters'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_auto'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_comments'));
        columns.push(new nlobjSearchColumn('custrecord_newsfeed_channel_sub'));
        var results = ssapiSearchAllRecords2('customrecord_newsfeed_channel', null, filters, columns);
        var count = results.length;
        var objChannels = [];
        for (var i = 0; i < count; i++) {
            var result = results[i];
            var channelId = result.getId();
            var name = result.getValue('name');
            logger.log("id = " + channelId + ", name = " + name);

            var objSubsidiary = null;
            if (ssapiHasValue(result.getValue('custrecord_newsfeed_channel_sub'))) {
                var subs = result.getValue('custrecord_newsfeed_channel_sub').split(",");

                // reset global variable hash to ensure list is updated
                resetChannelFieldHash("custrecord_newsfeed_channel_sub");

                // Note: You can select only one subsidiary as a channel filter
                // for this version of SuiteSocial
                objSubsidiary = getChannelFieldRecordObject(channelId, "custrecord_newsfeed_channel_sub", subs[0]);
            }

            var departments = null;
            if (ssapiHasValue(result.getValue('custrecord_newsfeed_channel_dept'))) {
                var depts = result.getValue('custrecord_newsfeed_channel_dept').split(",");

                // reset global variable hash to ensure list is updated
                resetChannelFieldHash("custrecord_newsfeed_channel_dept");

                departments = [];
                for (var j = 0; j < depts.length; j++) {
                    var obj = getChannelFieldRecordObject(channelId, "custrecord_newsfeed_channel_dept", depts[j]);
                    departments.push(obj);
                }
            }

            var locations = null;
            if (ssapiHasValue(result.getValue('custrecord_newsfeed_channel_loc'))) {
                var locs = result.getValue('custrecord_newsfeed_channel_loc').split(",");

                // reset global variable hash to ensure list is updated
                resetChannelFieldHash("custrecord_newsfeed_channel_loc");

                locations = [];
                for (var j = 0; j < locs.length; j++) {
                    var obj = getChannelFieldRecordObject(channelId, "custrecord_newsfeed_channel_loc", locs[j]);
                    locations.push(obj);
                }
            }

            var roles = null;
            if (ssapiHasValue(result.getValue('custrecord_newsfeed_channel_role'))) {
                var arrRoles = result.getValue('custrecord_newsfeed_channel_role').split(",");

                // reset global variable hash to ensure list is updated
                resetChannelFieldHash("custrecord_newsfeed_channel_role");

                roles = [];
                for (var j = 0; j < arrRoles.length; j++) {
                    var obj = getChannelFieldRecordObject(channelId, "custrecord_newsfeed_channel_role", arrRoles[j]);
                    roles.push(obj);
                }
            }

            var employees = null;
            if (ssapiHasValue(result.getValue('custrecord_newsfeed_channel_emp'))) {
                var emps = result.getValue('custrecord_newsfeed_channel_emp').split(",");

                // reset global variable hash to ensure list is updated
                resetChannelFieldHash("custrecord_newsfeed_channel_emp");

                employees = [];
                for (var j = 0; j < emps.length; j++) {
                    var obj = getChannelFieldRecordObject(channelId, "custrecord_newsfeed_channel_emp", emps[j]);
                    employees.push(obj);
                }
            }

            var posters = null;
            if (ssapiHasValue(result.getValue('custrecord_newsfeed_channel_posters'))) {
                var arrPosters = result.getValue('custrecord_newsfeed_channel_posters').split(",");

                // reset global variable hash to ensure list is updated
                resetChannelFieldHash("custrecord_newsfeed_channel_posters");

                posters = [];
                for (var j = 0; j < arrPosters.length; j++) {
                    var obj = getChannelFieldRecordObject(channelId, "custrecord_newsfeed_channel_posters", arrPosters[j]);
                    posters.push(obj);
                }
            }

            var autoSubscribe = result.getValue('custrecord_newsfeed_channel_auto') == "T";
            var allowComments = result.getValue('custrecord_newsfeed_channel_comments') == "T";

            var objChannel = {
                id : channelId,
                name : name,
                subsidiary : objSubsidiary,
                departments : departments,
                locations : locations,
                roles : roles,
                viewPermissionList : employees,
                createPermissionList : posters,
                isAutoSubscribe : autoSubscribe,
                isAllowComments : allowComments
            };
            objChannels.push(objChannel);
        }
        return objChannels;
    };

    /**
     * Gets the id/name object given the value of a multiselect field in the
     * Channel record.
     * 
     * @param {Number}
     *        channelId - internal id of the channel record
     * @param {String}
     *        fieldId - script id of the multiselect field in channel record
     * @param {Number}
     *        id - internal id of a field value
     * @returns {Object} - id/name pair
     */
    var getChannelFieldRecordObject = function(channelId, fieldId, id) {
        var logger = new suitesocial.Helper.logger(arguments, false, 'getChannelFieldRecordObject');

        var fieldHash = {};
        // check if hash is already in global
        var key = fieldId + "ObjectsHash";
        if (ssapiHasValue(ssobjGlobal[key])) {
            // get hash from global
            fieldHash = ssobjGlobal[key];
        } else {
            // get hash from options
            var recChannel = nlapiLoadRecord("customrecord_newsfeed_channel", channelId);
            var fld = recChannel.getField(fieldId);
            var options = fld.getSelectOptions();
            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                fieldHash[option.getId()] = option.getText();
            }
            ssobjGlobal[key] = fieldHash;
        }
        logger.log("fieldHash = " + JSON.stringify(fieldHash));

        // get id/name from hash
        return {
            id : id,
            name : fieldHash[id]
        };
    };

    /**
     * Resets the value of the global variable for a channel field objects hash.
     * 
     * @param {String}
     *        fieldId - script id of the multiselect field in channel record
     */
    var resetChannelFieldHash = function(fieldId) {
        var key = fieldId + "ObjectsHash";
        ssobjGlobal[key] = null;
    };

    /**
     * Gets the list of employees that a given user is following.
     * 
     * @param {Object}
     *        data - contains userId
     * @returns {Object[]} - array of user objects
     */
    var getFollowingPrivate = function(data) {
        var logger = new suitesocial.Helper.logger(arguments, false, 'getFollowingPrivate');

        // check required parameters
        if (ssapiHasNoValue(data.userid)) {
            throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "A required parameter 'userId' is missing.");
        }
        // check user parameter if it's a number
        if (!ssapiIsNumber(data.userid)) {
            throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for parameter 'userId': " + data.userid);
        }

        // call getFollowingList from generic suitelet
        var employees = getFollowingList(data.userid);

        // loop through employees, and format to appropriate object
        var objEmployees = [];
        var count = employees.length;
        for (var i = 0; i < count; i++) {
            var emp = employees[i];
            var objEmployee = {
                id : emp.colleagueEmpId,
                name : emp.colleagueEmpText,
                title : emp.title,
                department : emp.department,
                lastStatus : emp.statusUpdate
            };
            objEmployees.push(objEmployee);
        }
        return objEmployees;
    };

    /**
     * Gets the list of followers of a given user.
     * 
     * @param {Object}
     *        data - contains userId
     * @returns {Object[]} - array of user objects
     */
    var getFollowersPrivate = function(data) {
        var logger = new suitesocial.Helper.logger(arguments, false, 'getFollowersPrivate');

        // check required parameters
        if (ssapiHasNoValue(data.userid)) {
            throw nlapiCreateError("MISSING_REQUIRED_PARAMETER", "A required parameter 'userId' is missing.");
        }
        // check user parameter if it's a number
        if (!ssapiIsNumber(data.userid)) {
            throw nlapiCreateError("INVALID_PARAMETER_VALUE", "Invalid value for parameter 'userId': " + data.userid);
        }

        // call getFollowingList from generic suitelet
        var employees = getFollowersList(data.userid);

        // loop through employees, and format to appropriate object
        var objEmployees = [];
        var count = employees.length;
        for (var i = 0; i < count; i++) {
            var emp = employees[i];
            var objEmployee = {
                id : emp.colleagueEmpId,
                name : emp.colleagueEmpText,
                title : emp.title,
                department : emp.department,
                lastStatus : emp.statusUpdate
            };
            objEmployees.push(objEmployee);
        }
        return objEmployees;
    };

    // public functions definition
    return {
        getNewsFeedWall : getNewsFeedWallPrivate,

        getUsers : getUsersPrivate,

        getChannels : getChannelsPrivate,

        getFollowing : getFollowingPrivate,

        getFollowers : getFollowersPrivate
    };

})();
