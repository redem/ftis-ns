/**
 * Module Description Here
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Mar 2012     calib
 *
 */

/**
 * @return {Boolean} True to continue save, false to abort save
 */

function channelPageInit()
{
	if ( !(nlapiGetContext().getFeature("subsidiaries")) )
	{
		nlapiSetFieldDisplay('custrecord_newsfeed_channel_sub', false);
	}
}

function clientSaveRecord(){
	
	if ( nlapiGetContext().getFeature("subsidiaries") )
		{
			var selected_subs = null;
			
			var channel_sub =  nlapiGetFieldValues('custrecord_newsfeed_channel_sub');
	
			if ( channel_sub != null && channel_sub != "" )
			{
				if ( channel_sub.length > 1 )
				{
					alert("This version can support one subsidiary (consolidated) as channel filter.".tl() + "\n " + "Please select only one subsidiary.".tl());
					return false;
				}
	
				if ( channel_sub.length == 1 )
				{
					selected_subs = new Array();
					selected_subs.push(channel_sub[0]);	
					var subsidiary = nlapiLoadRecord('subsidiary', channel_sub[0]);
					var parent = subsidiary.getFieldValue('parent');
					if ( parent != null && parent != "")
						selected_subs.push(parent);
					while ( parent != null && parent != "" )
						{	
							subsidiary = nlapiLoadRecord('subsidiary', parent);
							parent = subsidiary.getFieldValue('parent');
							if ( parent != null && parent != "")
								selected_subs.push(parent);					
						}
					nlapiSetFieldValues('custrecord_suitesocial_channel_sub_sel', selected_subs);		
				}
				
			}
		
		}
	
    return true;
}
