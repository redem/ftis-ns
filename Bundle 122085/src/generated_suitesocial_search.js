function SuiteSocialNewsFeedSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialNewsFeedSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_newsfeed', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutoPoster_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutoPoster_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutoPoster_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutoPoster_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostField_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_field', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostField_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_field', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostField_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_field', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostField_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_field', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostInfo_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_info', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.AutopostRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_autopost_record', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Call_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_phonecall', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Call_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_phonecall', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Call_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_phonecall', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Call_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_phonecall', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Campaign_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_campaign', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Campaign_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_campaign', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Campaign_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_campaign', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Campaign_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_campaign', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Case_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_supportcase', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Case_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_supportcase', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Case_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_supportcase', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Case_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_supportcase', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Channel_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_channel', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Channel_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_channel', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Channel_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_channel', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Channel_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_channel', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Comments_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_comments', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Competitor_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_competitor', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Competitor_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_competitor', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Competitor_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_competitor', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Competitor_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_competitor', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Contact_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_contact', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Contact_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_contact', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Contact_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_contact', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Contact_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_contact', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_EQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'EQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_GREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'GREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_GREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'GREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_LESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'LESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_LESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'LESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_NOTEQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'NOTEQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_NOTGREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'NOTGREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_NOTGREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'NOTGREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_NOTLESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'NOTLESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_NOTLESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'NOTLESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_BETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'BETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomRecord_NOTBETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customrecord', null, 'NOTBETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.CustomType_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customtype', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Customer_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customer', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Customer_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customer', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Customer_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customer', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Customer_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_customer', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Employee_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_employee', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Employee_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_employee', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Employee_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_employee', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Employee_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_employee', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Event_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_calendarevent', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Event_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_calendarevent', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Event_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_calendarevent', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Event_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_calendarevent', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Icon_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_icon', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Icon_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_icon', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Icon_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_icon', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Icon_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_icon', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Issue_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_issue', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Issue_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_issue', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Issue_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_issue', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Issue_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_issue', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Item_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_item', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Item_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_item', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Item_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_item', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Item_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_item', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Message_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.MessageForSearch_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_message_search', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Opportunity_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_opportunity', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Opportunity_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_opportunity', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Opportunity_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_opportunity', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Opportunity_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_opportunity', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Parent_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_parent', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Parent_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_parent', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Parent_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_parent', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Parent_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_parent', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Partner_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_partner', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Partner_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_partner', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Partner_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_partner', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Partner_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_partner', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Project_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_job', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Project_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_job', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Project_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_job', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Project_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_job', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Recipient_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_recipient', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Recipient_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_recipient', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Recipient_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_recipient', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Recipient_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_recipient', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.RecordInfo_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_record_info', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.SubscriptionType_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_subs_type', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Task_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_task', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Task_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_task', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Task_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_task', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Task_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_task', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Transaction_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_transaction', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Transaction_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_transaction', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Transaction_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_transaction', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Transaction_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_transaction', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Vendor_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_vendor', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Vendor_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_vendor', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Vendor_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_vendor', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Vendor_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_vendor', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Workflow_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_workflow', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Workflow_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_workflow', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Workflow_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_workflow', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.Workflow_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_workflow', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.AUTOPOST_INFO = 'custrecord_newsfeed_autopost_info';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addAutopostInfoColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_autopost_info'));
        return this;
    };

    this.COMMENTS = 'custrecord_newsfeed_comments';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCommentsColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_comments'));
        return this;
    };

    this.CUSTOM_RECORD = 'custrecord_newsfeed_customrecord';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCustomRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_customrecord'));
        return this;
    };

    this.CUSTOM_TYPE = 'custrecord_newsfeed_customtype';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCustomTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_customtype'));
        return this;
    };

    this.MESSAGE = 'custrecord_newsfeed_message';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addMessageColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_message'));
        return this;
    };

    this.MESSAGE_FOR_SEARCH = 'custrecord_newsfeed_message_search';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addMessageForSearchColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_message_search'));
        return this;
    };

    this.RECORD_INFO = 'custrecord_newsfeed_record_info';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addRecordInfoColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_record_info'));
        return this;
    };

    this.SUBSCRIPTION_TYPE = 'custrecord_newsfeed_subs_type';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addSubscriptionTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_subs_type'));
        return this;
    };

    this.AUTO_POSTER = 'custrecord_newsfeed_autopost';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addAutoPosterColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_autopost'));
        return this;
    };

    this.AUTOPOST_FIELD = 'custrecord_newsfeed_autopost_field';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addAutopostFieldColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_autopost_field'));
        return this;
    };

    this.AUTOPOST_RECORD = 'custrecord_newsfeed_autopost_record';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addAutopostRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_autopost_record'));
        return this;
    };

    this.CALL = 'custrecord_newsfeed_phonecall';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCallColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_phonecall'));
        return this;
    };

    this.CAMPAIGN = 'custrecord_newsfeed_campaign';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCampaignColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_campaign'));
        return this;
    };

    this.CASE = 'custrecord_newsfeed_supportcase';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCaseColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_supportcase'));
        return this;
    };

    this.CHANNEL = 'custrecord_newsfeed_channel';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addChannelColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_channel'));
        return this;
    };

    this.COMPETITOR = 'custrecord_newsfeed_competitor';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCompetitorColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_competitor'));
        return this;
    };

    this.CONTACT = 'custrecord_newsfeed_contact';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addContactColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_contact'));
        return this;
    };

    this.CUSTOMER = 'custrecord_newsfeed_customer';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addCustomerColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_customer'));
        return this;
    };

    this.EMPLOYEE = 'custrecord_newsfeed_employee';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addEmployeeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_employee'));
        return this;
    };

    this.EVENT = 'custrecord_newsfeed_calendarevent';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addEventColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_calendarevent'));
        return this;
    };

    this.ICON = 'custrecord_newsfeed_icon';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addIconColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_icon'));
        return this;
    };

    this.ISSUE = 'custrecord_newsfeed_issue';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addIssueColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_issue'));
        return this;
    };

    this.ITEM = 'custrecord_newsfeed_item';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addItemColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_item'));
        return this;
    };

    this.OPPORTUNITY = 'custrecord_newsfeed_opportunity';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addOpportunityColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_opportunity'));
        return this;
    };

    this.PARENT = 'custrecord_newsfeed_parent';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addParentColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_parent'));
        return this;
    };

    this.PARTNER = 'custrecord_newsfeed_partner';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addPartnerColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_partner'));
        return this;
    };

    this.PROJECT = 'custrecord_newsfeed_job';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addProjectColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_job'));
        return this;
    };

    this.RECIPIENT = 'custrecord_newsfeed_recipient';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addRecipientColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_recipient'));
        return this;
    };

    this.TASK = 'custrecord_newsfeed_task';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addTaskColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_task'));
        return this;
    };

    this.TRANSACTION = 'custrecord_newsfeed_transaction';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addTransactionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_transaction'));
        return this;
    };

    this.VENDOR = 'custrecord_newsfeed_vendor';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addVendorColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_vendor'));
        return this;
    };

    this.WORKFLOW = 'custrecord_newsfeed_workflow';
    /**
     * @return {SuiteSocialNewsFeedSearch}
     */
    this.addWorkflowColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_workflow'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_newsfeed_autopost', 'custrecord_newsfeed_autopost_field', 'custrecord_newsfeed_autopost_info', 'custrecord_newsfeed_autopost_record', 'custrecord_newsfeed_phonecall', 'custrecord_newsfeed_campaign', 'custrecord_newsfeed_supportcase', 'custrecord_newsfeed_channel', 'custrecord_newsfeed_comments', 'custrecord_newsfeed_competitor', 'custrecord_newsfeed_contact', 'custrecord_newsfeed_customrecord',
            'custrecord_newsfeed_customtype', 'custrecord_newsfeed_customer', 'custrecord_newsfeed_employee', 'custrecord_newsfeed_calendarevent', 'custrecord_newsfeed_icon', 'custrecord_newsfeed_issue', 'custrecord_newsfeed_item', 'custrecord_newsfeed_message', 'custrecord_newsfeed_message_search', 'custrecord_newsfeed_opportunity', 'custrecord_newsfeed_parent', 'custrecord_newsfeed_partner', 'custrecord_newsfeed_job', 'custrecord_newsfeed_recipient', 'custrecord_newsfeed_record_info',
            'custrecord_newsfeed_subs_type', 'custrecord_newsfeed_task', 'custrecord_newsfeed_transaction', 'custrecord_newsfeed_vendor', 'custrecord_newsfeed_workflow' ];
}

function MyRelatedTransactionSubsSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new MyRelatedTransactionSubsSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ss_my_related_transaction_s', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialProfile_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_profile', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialProfile_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_profile', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialProfile_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_profile', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialProfile_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_profile', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_ss_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_ss_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_ss_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.SuiteSocialRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_mrts_ss_record', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.SUITESOCIAL_PROFILE = 'custrecord_ss_mrts_profile';
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.addSuiteSocialProfileColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ss_mrts_profile'));
        return this;
    };

    this.SUITESOCIAL_RECORD = 'custrecord_ss_mrts_ss_record';
    /**
     * @return {MyRelatedTransactionSubsSearch}
     */
    this.addSuiteSocialRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ss_mrts_ss_record'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_ss_mrts_profile', 'custrecord_ss_mrts_ss_record' ];
}
function MyRelatedRecordSubscriptionSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new MyRelatedRecordSubscriptionSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ss_subscription_link_sub', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSubscription_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_link', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSubscription_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_link', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSubscription_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_link', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSubscription_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_link', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Subscriber_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_sub', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Subscriber_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_sub', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Subscriber_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_sub', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.Subscriber_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sublink_sub', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.RELATED_RECORD_SUBSCRIPTION = 'custrecord_suitesocial_sublink_link';
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.addRelatedRecordSubscriptionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sublink_link'));
        return this;
    };

    this.SUBSCRIBER = 'custrecord_suitesocial_sublink_sub';
    /**
     * @return {MyRelatedRecordSubscriptionSearch}
     */
    this.addSubscriberColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sublink_sub'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_suitesocial_sublink_link', 'custrecord_suitesocial_sublink_sub' ];
}

function MySavedSearchSubscriptionSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new MySavedSearchSubscriptionSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ss_my_saved_search_sub', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Profile_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_profile', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Profile_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_profile', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Profile_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_profile', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.Profile_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_profile', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.SavedSearch_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_saved_search', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.SavedSearch_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_saved_search', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.SavedSearch_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_saved_search', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.SavedSearch_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_msss_saved_search', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.PROFILE = 'custrecord_ss_msss_profile';
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.addProfileColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ss_msss_profile'));
        return this;
    };

    this.SAVED_SEARCH = 'custrecord_ss_msss_saved_search';
    /**
     * @return {MySavedSearchSubscriptionSearch}
     */
    this.addSavedSearchColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ss_msss_saved_search'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_ss_msss_profile', 'custrecord_ss_msss_saved_search' ];
}

function NewsfeedPostLinkSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new NewsfeedPostLinkSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {NewsfeedPostLinkSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {NewsfeedPostLinkSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_nf_post_link', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Post_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_post', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Post_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_post', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Post_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_post', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Post_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_post', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Recipient_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_recipient', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Recipient_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_recipient', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Recipient_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_recipient', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.Recipient_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_newsfeed_post_link_recipient', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.POST = 'custrecord_newsfeed_post_link_post';
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.addPostColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_post_link_post'));
        return this;
    };

    this.RECIPIENT = 'custrecord_newsfeed_post_link_recipient';
    /**
     * @return {NewsfeedPostLinkSearch}
     */
    this.addRecipientColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_newsfeed_post_link_recipient'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_newsfeed_post_link_post', 'custrecord_newsfeed_post_link_recipient' ];
}

function RecordSubscriptionSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new RecordSubscriptionSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {RecordSubscriptionSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {RecordSubscriptionSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_autosub_type', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'IS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Description_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_desc', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Field_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rf', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Field_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rf', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Field_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rf', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.Field_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rf', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldHidden_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fld', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldHidden_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fld', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldHidden_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fld', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldHidden_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fld', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'IS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.FieldID_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_fid', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'IS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.InternalRecordScriptID_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rsi', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SavedSearch_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_ss', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SavedSearch_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_ss', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SavedSearch_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_ss', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SavedSearch_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_ss', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SuiteSocialRecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rec', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SuiteSocialRecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rec', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SuiteSocialRecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rec', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.SuiteSocialRecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosub_type_rec', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.DESCRIPTION = 'custrecord_suitesocial_autosub_type_desc';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addDescriptionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosub_type_desc'));
        return this;
    };

    this.FIELD_ID = 'custrecord_suitesocial_autosub_type_fid';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addFieldIDColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosub_type_fid'));
        return this;
    };

    this.INTERNAL_RECORD_SCRIPT_ID = 'custrecord_suitesocial_autosub_type_rsi';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addInternalRecordScriptIDColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosub_type_rsi'));
        return this;
    };

    this.FIELD = 'custrecord_suitesocial_autosub_type_rf';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addFieldColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosub_type_rf'));
        return this;
    };

    this.FIELD_HIDDEN = 'custrecord_suitesocial_autosub_type_fld';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addFieldHiddenColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosub_type_fld'));
        return this;
    };

    this.SAVED_SEARCH = 'custrecord_suitesocial_autosub_type_ss';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addSavedSearchColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosub_type_ss'));
        return this;
    };

    this.SUITESOCIAL_RECORD_TYPE = 'custrecord_suitesocial_autosub_type_rec';
    /**
     * @return {RecordSubscriptionSearch}
     */
    this.addSuiteSocialRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosub_type_rec'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_suitesocial_autosub_type_desc', 'custrecord_suitesocial_autosub_type_rf', 'custrecord_suitesocial_autosub_type_fld', 'custrecord_suitesocial_autosub_type_fid', 'custrecord_suitesocial_autosub_type_rsi', 'custrecord_suitesocial_autosub_type_ss', 'custrecord_suitesocial_autosub_type_rec' ];
}

function RecordSubscriptionRoleSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new RecordSubscriptionRoleSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {RecordSubscriptionRoleSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {RecordSubscriptionRoleSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ss_record_subscription_role', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.RecordSubscription_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_record_subscription', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.RecordSubscription_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_record_subscription', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.RecordSubscription_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_record_subscription', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.RecordSubscription_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_record_subscription', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Role_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_role', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Role_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_role', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Role_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_role', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.Role_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrsr_role', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.RECORD_SUBSCRIPTION = 'custrecord_ssrsr_record_subscription';
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.addRecordSubscriptionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssrsr_record_subscription'));
        return this;
    };

    this.ROLE = 'custrecord_ssrsr_role';
    /**
     * @return {RecordSubscriptionRoleSearch}
     */
    this.addRoleColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssrsr_role'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_ssrsr_record_subscription', 'custrecord_ssrsr_role' ];
}

function RelatedRecordSubscriptionSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new RelatedRecordSubscriptionSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ss_subscription_link', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.Description_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_desc', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldDataTypeName_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_datatypename', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldRecType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_rectype', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldRecType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_rectype', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldRecType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_rectype', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldRecType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_rectype', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldScriptId_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_scriptid', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.FieldType_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field_fieldtype', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeRecType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_re', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeRecType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_re', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeRecType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_re', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeRecType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_re', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeScriptId_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_scrrectype_si', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelSSRecordScrRecTypeText_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec_srcrectype_te', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordField_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_record_field', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordField_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_record_field', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordField_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_record_field', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordField_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_record_field', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordFieldHidden_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordFieldHidden_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordFieldHidden_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordFieldHidden_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_field', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSSRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_ss_rec', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSSRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_ss_rec', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSSRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_ss_rec', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.RelatedRecordSSRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_ss_rec', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeBaseType_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_basety', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeRecType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_rectyp', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeRecType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_rectyp', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeRecType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_rectyp', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeRecType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_rectyp', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'ANY', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'IS', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SSRecordScrRecTypeScriptId_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_ss_rec_scrrectype_si', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SuiteSocialRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SuiteSocialRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SuiteSocialRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.SuiteSocialRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sssl_rel_ss_rec', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.DESCRIPTION = 'custrecord_sssl_desc';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addDescriptionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_desc'));
        return this;
    };

    this.FIELD_DATATYPENAME = 'custrecord_sssl_field_datatypename';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addFieldDataTypeNameColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_field_datatypename'));
        return this;
    };

    this.FIELD_SCRIPTID = 'custrecord_sssl_field_scriptid';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addFieldScriptIdColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_field_scriptid'));
        return this;
    };

    this.FIELD_TYPE = 'custrecord_sssl_field_fieldtype';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addFieldTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_field_fieldtype'));
        return this;
    };

    this.REL_SS_RECORD_SCRRECTYPE_SCRIPT_ID = 'custrecord_sssl_rel_ss_rec_scrrectype_si';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelSSRecordScrRecTypeScriptIdColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_rel_ss_rec_scrrectype_si'));
        return this;
    };

    this.REL_SS_RECORD_SCRRECTYPE_TEXT = 'custrecord_sssl_rel_ss_rec_srcrectype_te';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelSSRecordScrRecTypeTextColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_rel_ss_rec_srcrectype_te'));
        return this;
    };

    this.SS_RECORD_SCRRECTYPE_BASETYPE = 'custrecord_sssl_ss_rec_scrrectype_basety';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addSSRecordScrRecTypeBaseTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_ss_rec_scrrectype_basety'));
        return this;
    };

    this.SS_RECORD_SCRRECTYPE_SCRIPT_ID = 'custrecord_sssl_ss_rec_scrrectype_si';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addSSRecordScrRecTypeScriptIdColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_ss_rec_scrrectype_si'));
        return this;
    };

    this.FIELD_RECTYPE = 'custrecord_sssl_field_rectype';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addFieldRecTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_field_rectype'));
        return this;
    };

    this.REL_SS_RECORD_SCRRECTYPE = 'custrecord_sssl_rel_ss_rec_srcrectype';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelSSRecordScrRecTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_rel_ss_rec_srcrectype'));
        return this;
    };

    this.REL_SS_RECORD_SCRRECTYPE_RECTYPE = 'custrecord_sssl_rel_ss_rec_srcrectype_re';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelSSRecordScrRecTypeRecTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_rel_ss_rec_srcrectype_re'));
        return this;
    };

    this.RELATED_RECORD = 'custrecord_sssl_ss_rec';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelatedRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_ss_rec'));
        return this;
    };

    this.RELATED_RECORD_FIELD = 'custrecord_sssl_record_field';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelatedRecordFieldColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_record_field'));
        return this;
    };

    this.RELATED_RECORD_FIELD_HIDDEN = 'custrecord_sssl_field';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelatedRecordFieldHiddenColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_field'));
        return this;
    };

    this.RELATED_RECORD_SS_RECORD = 'custrecord_sssl_ss_rec_ss_rec';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addRelatedRecordSSRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_ss_rec_ss_rec'));
        return this;
    };

    this.SS_RECORD_SCRRECTYPE = 'custrecord_sssl_ss_rec_scrrectype';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addSSRecordScrRecTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_ss_rec_scrrectype'));
        return this;
    };

    this.SS_RECORD_SCRRECTYPE_RECTYPE = 'custrecord_sssl_ss_rec_scrrectype_rectyp';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addSSRecordScrRecTypeRecTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_ss_rec_scrrectype_rectyp'));
        return this;
    };

    this.SUITESOCIAL_RECORD = 'custrecord_sssl_rel_ss_rec';
    /**
     * @return {RelatedRecordSubscriptionSearch}
     */
    this.addSuiteSocialRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sssl_rel_ss_rec'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_sssl_desc', 'custrecord_sssl_field_datatypename', 'custrecord_sssl_field_rectype', 'custrecord_sssl_field_scriptid', 'custrecord_sssl_field_fieldtype', 'custrecord_sssl_rel_ss_rec_srcrectype', 'custrecord_sssl_rel_ss_rec_srcrectype_re', 'custrecord_sssl_rel_ss_rec_scrrectype_si', 'custrecord_sssl_rel_ss_rec_srcrectype_te', 'custrecord_sssl_ss_rec', 'custrecord_sssl_record_field', 'custrecord_sssl_field',
            'custrecord_sssl_ss_rec_ss_rec', 'custrecord_sssl_ss_rec_scrrectype', 'custrecord_sssl_ss_rec_scrrectype_basety', 'custrecord_sssl_ss_rec_scrrectype_rectyp', 'custrecord_sssl_ss_rec_scrrectype_si', 'custrecord_sssl_rel_ss_rec' ];
}

function SavedSearchSubscriptionSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SavedSearchSubscriptionSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ss_saved_search_subs', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'IS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.Description_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_desc', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'IS', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordScriptID_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_script_id', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_type', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_type', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_type', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.RecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_record_type', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.SavedSearch_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_saved_search', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.SavedSearch_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_saved_search', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.SavedSearch_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_saved_search', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.SavedSearch_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_saved_search', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.ScriptedRecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_scripted_record_type', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.ScriptedRecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_sss_scripted_record_type', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.ScriptedRecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_scripted_record_type', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.ScriptedRecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_sss_scripted_record_type', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.DESCRIPTION = 'custrecord_sss_desc';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addDescriptionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sss_desc'));
        return this;
    };

    this.RECORD_SCRIPT_ID = 'custrecord_sss_record_script_id';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addRecordScriptIDColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sss_record_script_id'));
        return this;
    };

    this.RECORD_TYPE = 'custrecord_sss_record_type';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sss_record_type'));
        return this;
    };

    this.SAVED_SEARCH = 'custrecord_sss_saved_search';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addSavedSearchColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sss_saved_search'));
        return this;
    };

    this.SCRIPTED_RECORD_TYPE = 'custrecord_sss_scripted_record_type';
    /**
     * @return {SavedSearchSubscriptionSearch}
     */
    this.addScriptedRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_sss_scripted_record_type'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_sss_desc', 'custrecord_sss_record_script_id', 'custrecord_sss_record_type', 'custrecord_sss_saved_search', 'custrecord_sss_scripted_record_type' ];
}

function ScheduledPostSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new ScheduledPostSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {ScheduledPostSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {ScheduledPostSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_autopost', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Channel_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_channel', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Channel_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_channel', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Channel_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_channel', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Channel_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_channel', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Icon_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_icon', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Icon_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_icon', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Icon_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_icon', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Icon_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_icon', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'ANY', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'IS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.Message_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_message', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.PostOnWeekends_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_weekends', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.PostOnWeekends_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_weekends', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.PostOnWeekends_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_weekends', null, 'IS', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecipientField_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recipient', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecipientField_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recipient', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecipientField_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recipient', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecipientField_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recipient', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecipientField_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recipient', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecipientField_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recipient', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recordtype', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recordtype', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recordtype', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.RecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_recordtype', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearch_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_search', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearch_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_search', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearch_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_search', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearch_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_search', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearchRecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_saved_search_rt', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearchRecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_saved_search_rt', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearchRecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_saved_search_rt', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SavedSearchRecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_saved_search_rt', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SuiteSocialRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SuiteSocialRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SuiteSocialRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {ScheduledPostSearch}
     */
    this.SuiteSocialRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_record', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.MESSAGE = 'custrecord_autopost_message';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addMessageColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_message'));
        return this;
    };

    this.POST_ON_WEEKENDS = 'custrecord_autopost_weekends';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addPostOnWeekendsColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_weekends'));
        return this;
    };

    this.RECIPIENT_FIELD = 'custrecord_autopost_recipient';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addRecipientFieldColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_recipient'));
        return this;
    };

    this.CHANNEL = 'custrecord_autopost_channel';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addChannelColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_channel'));
        return this;
    };

    this.ICON = 'custrecord_autopost_icon';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addIconColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_icon'));
        return this;
    };

    this.RECORD_TYPE = 'custrecord_autopost_recordtype';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_recordtype'));
        return this;
    };

    this.SAVED_SEARCH = 'custrecord_autopost_search';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addSavedSearchColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_search'));
        return this;
    };

    this.SAVED_SEARCH_RECORD_TYPE = 'custrecord_autopost_saved_search_rt';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addSavedSearchRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_saved_search_rt'));
        return this;
    };

    this.SUITESOCIAL_RECORD = 'custrecord_autopost_record';
    /**
     * @return {ScheduledPostSearch}
     */
    this.addSuiteSocialRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_record'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_autopost_channel', 'custrecord_autopost_icon', 'custrecord_autopost_message', 'custrecord_autopost_weekends', 'custrecord_autopost_recipient', 'custrecord_autopost_recordtype', 'custrecord_autopost_search', 'custrecord_autopost_saved_search_rt', 'custrecord_autopost_record' ];
}

function SuiteSocialAutoPostScheduleSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialAutoPostScheduleSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_autopost_schedule', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.AutoPost_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_autopost', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.AutoPost_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_autopost', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.AutoPost_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_autopost', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.AutoPost_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_autopost', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_EQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'EQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_GREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'GREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_GREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'GREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_LESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'LESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_LESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'LESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_NOTEQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'NOTEQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_NOTGREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'NOTGREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_NOTGREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'NOTGREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_NOTLESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'NOTLESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_NOTLESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'NOTLESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_BETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'BETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.ServerTimeOfDay_NOTBETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_stod', null, 'NOTBETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_EQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'EQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_GREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'GREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_GREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'GREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_LESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'LESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_LESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'LESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_NOTEQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'NOTEQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_NOTGREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'NOTGREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_NOTGREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'NOTGREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_NOTLESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'NOTLESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_NOTLESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'NOTLESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_BETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'BETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.TimeOfDay_NOTBETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_autopost_sched_tod', null, 'NOTBETWEEN', value1, value2));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.SERVER_TIME_OF_DAY = 'custrecord_autopost_sched_stod';
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.addServerTimeOfDayColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_sched_stod'));
        return this;
    };

    this.TIME_OF_DAY = 'custrecord_autopost_sched_tod';
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.addTimeOfDayColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_sched_tod'));
        return this;
    };

    this.AUTO_POST = 'custrecord_autopost_sched_autopost';
    /**
     * @return {SuiteSocialAutoPostScheduleSearch}
     */
    this.addAutoPostColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_autopost_sched_autopost'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_autopost_sched_autopost', 'custrecord_autopost_sched_stod', 'custrecord_autopost_sched_tod' ];
}

function SuiteSocialAutoSubscribeSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialAutoSubscribeSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_autosubscribe', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.RecordSubscription_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_typ', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.RecordSubscription_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_typ', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.RecordSubscription_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_typ', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.RecordSubscription_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_typ', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Subscriber_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_sub', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Subscriber_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_sub', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Subscriber_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_sub', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.Subscriber_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autosubscribe_sub', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.RECORD_SUBSCRIPTION = 'custrecord_suitesocial_autosubscribe_typ';
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.addRecordSubscriptionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosubscribe_typ'));
        return this;
    };

    this.SUBSCRIBER = 'custrecord_suitesocial_autosubscribe_sub';
    /**
     * @return {SuiteSocialAutoSubscribeSearch}
     */
    this.addSubscriberColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autosubscribe_sub'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_suitesocial_autosubscribe_typ', 'custrecord_suitesocial_autosubscribe_sub' ];
}

function SuiteSocialColleagueSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialColleagueSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialColleagueSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialColleagueSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_colleague', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Colleague_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub_pro', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Colleague_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub_pro', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Colleague_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub_pro', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Colleague_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub_pro', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.EmailAlerts_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_alrt', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.EmailAlerts_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_alrt', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.EmailAlerts_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_alrt', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Employee_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_emp', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Employee_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_emp', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Employee_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_emp', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Employee_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_emp', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Profile_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_profile', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Profile_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_profile', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Profile_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_profile', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Profile_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_profile', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Subscriber_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Subscriber_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Subscriber_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.Subscriber_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_colleague_sub', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.EMAIL_ALERTS = 'custrecord_suitesocial_colleague_alrt';
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.addEmailAlertsColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_colleague_alrt'));
        return this;
    };

    this.COLLEAGUE = 'custrecord_suitesocial_colleague_sub_pro';
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.addColleagueColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_colleague_sub_pro'));
        return this;
    };

    this.EMPLOYEE = 'custrecord_suitesocial_colleague_emp';
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.addEmployeeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_colleague_emp'));
        return this;
    };

    this.PROFILE = 'custrecord_suitesocial_colleague_profile';
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.addProfileColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_colleague_profile'));
        return this;
    };

    this.SUBSCRIBER = 'custrecord_suitesocial_colleague_sub';
    /**
     * @return {SuiteSocialColleagueSearch}
     */
    this.addSubscriberColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_colleague_sub'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_suitesocial_colleague_sub_pro', 'custrecord_suitesocial_colleague_alrt', 'custrecord_suitesocial_colleague_emp', 'custrecord_suitesocial_colleague_profile', 'custrecord_suitesocial_colleague_sub' ];
}

function SuiteSocialDigestScheduleSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialDigestScheduleSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_digest_schedule', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Profile_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_prof', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Profile_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_prof', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Profile_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_prof', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.Profile_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_prof', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_EQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'EQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_GREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'GREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_GREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'GREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_LESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'LESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_LESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'LESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_NOTEQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'NOTEQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_NOTGREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'NOTGREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_NOTGREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'NOTGREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_NOTLESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'NOTLESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_NOTLESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'NOTLESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_BETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'BETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.ServerTimeOfDay_NOTBETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_stod', null, 'NOTBETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_EQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'EQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_GREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'GREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_GREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'GREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_LESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'LESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_LESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'LESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_NOTEQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'NOTEQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_NOTGREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'NOTGREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_NOTGREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'NOTGREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_NOTLESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'NOTLESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_NOTLESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'NOTLESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_BETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'BETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.TimeOfDay_NOTBETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_digest_sched_tod', null, 'NOTBETWEEN', value1, value2));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.SERVER_TIME_OF_DAY = 'custrecord_suitesocial_digest_sched_stod';
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.addServerTimeOfDayColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_digest_sched_stod'));
        return this;
    };

    this.TIME_OF_DAY = 'custrecord_suitesocial_digest_sched_tod';
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.addTimeOfDayColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_digest_sched_tod'));
        return this;
    };

    this.PROFILE = 'custrecord_suitesocial_digest_sched_prof';
    /**
     * @return {SuiteSocialDigestScheduleSearch}
     */
    this.addProfileColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_digest_sched_prof'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_suitesocial_digest_sched_prof', 'custrecord_suitesocial_digest_sched_stod', 'custrecord_suitesocial_digest_sched_tod' ];
}

function SuiteSocialFieldSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialFieldSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialFieldSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialFieldSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_field', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.DataTypeName_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_data_type_name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Record_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Record_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Record_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.Record_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.RecordField_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record_field', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.RecordField_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record_field', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.RecordField_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record_field', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.RecordField_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_record_field', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.ScriptId_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_script_id', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.SelectListOrRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_select_list_or_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.SelectListOrRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_select_list_or_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.SelectListOrRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_select_list_or_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.SelectListOrRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssf_select_list_or_record', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.DATA_TYPE_NAME = 'custrecord_ssf_data_type_name';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addDataTypeNameColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssf_data_type_name'));
        return this;
    };

    this.SCRIPT_ID = 'custrecord_ssf_script_id';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addScriptIdColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssf_script_id'));
        return this;
    };

    this.RECORD = 'custrecord_ssf_record';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssf_record'));
        return this;
    };

    this.RECORD_FIELD = 'custrecord_ssf_record_field';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addRecordFieldColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssf_record_field'));
        return this;
    };

    this.SELECT_LIST_OR_RECORD = 'custrecord_ssf_select_list_or_record';
    /**
     * @return {SuiteSocialFieldSearch}
     */
    this.addSelectListOrRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssf_select_list_or_record'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_ssf_data_type_name', 'custrecord_ssf_record', 'custrecord_ssf_record_field', 'custrecord_ssf_script_id', 'custrecord_ssf_select_list_or_record' ];
}

function SuiteSocialInternalSettingSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialInternalSettingSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ss_internal_setting', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.Comment_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_comment', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.SettingValue_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_internal_setting_value', null, 'STARTSWITH', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.COMMENT = 'custrecord_ss_internal_setting_comment';
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.addCommentColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ss_internal_setting_comment'));
        return this;
    };

    this.SETTING_VALUE = 'custrecord_ss_internal_setting_value';
    /**
     * @return {SuiteSocialInternalSettingSearch}
     */
    this.addSettingValueColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ss_internal_setting_value'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_ss_internal_setting_comment', 'custrecord_ss_internal_setting_value' ];
}

function SuiteSocialProfileSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialProfileSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialProfileSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialProfileSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_profile', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnComment_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_cmnt', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnComment_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_cmnt', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnComment_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_cmnt', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnMention_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_ment', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnMention_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_ment', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnMention_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_ment', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnPrivateMessage_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_drct', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnPrivateMessage_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_drct', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnPrivateMessage_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_drct', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnStatusChange_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_stat', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnStatusChange_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_stat', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnStatusChange_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_stat', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnSubscribedRecord_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_rcrd', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnSubscribedRecord_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_rcrd', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.AlertOnSubscribedRecord_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_alrt_rcrd', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Employee_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_emp', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Employee_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_emp', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Employee_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_emp', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Employee_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_emp', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Image_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_image', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Image_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_image', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Image_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_image', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.Image_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_image', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.IntegratePostsWithYammerAccount_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_integration_enabled', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.IntegratePostsWithYammerAccount_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_integration_enabled', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.IntegratePostsWithYammerAccount_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_integration_enabled', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.LastStatus_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ss_profile_last_status', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.SendDigest_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_digest', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.SendDigest_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_digest', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.SendDigest_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_digest', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.WeekendDigests_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_wknd_dgst', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.WeekendDigests_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_wknd_dgst', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.WeekendDigests_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_wknd_dgst', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.YammerProfileTokenHidden_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_yammer_profile_token', null, 'STARTSWITH', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.ALERT_ON_COMMENT = 'custrecord_suitesocial_profile_alrt_cmnt';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addAlertOnCommentColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_alrt_cmnt'));
        return this;
    };

    this.ALERT_ON_MENTION = 'custrecord_suitesocial_profile_alrt_ment';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addAlertOnMentionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_alrt_ment'));
        return this;
    };

    this.ALERT_ON_PRIVATE_MESSAGE = 'custrecord_suitesocial_profile_alrt_drct';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addAlertOnPrivateMessageColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_alrt_drct'));
        return this;
    };

    this.ALERT_ON_STATUS_CHANGE = 'custrecord_suitesocial_profile_alrt_stat';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addAlertOnStatusChangeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_alrt_stat'));
        return this;
    };

    this.ALERT_ON_SUBSCRIBED_RECORD = 'custrecord_suitesocial_profile_alrt_rcrd';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addAlertOnSubscribedRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_alrt_rcrd'));
        return this;
    };

    this.IMAGE = 'custrecord_suitesocial_profile_image';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addImageColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_image'));
        return this;
    };

    this.INTEGRATE_POSTS_WITH_YAMMER_ACCOUNT = 'custrecord_yammer_integration_enabled';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addIntegratePostsWithYammerAccountColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_yammer_integration_enabled'));
        return this;
    };

    this.LAST_STATUS = 'custrecord_ss_profile_last_status';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addLastStatusColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ss_profile_last_status'));
        return this;
    };

    this.SEND_DIGEST = 'custrecord_suitesocial_profile_digest';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addSendDigestColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_digest'));
        return this;
    };

    this.WEEKEND_DIGESTS = 'custrecord_suitesocial_profile_wknd_dgst';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addWeekendDigestsColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_wknd_dgst'));
        return this;
    };

    this.YAMMER_PROFILE_TOKEN_HIDDEN = 'custrecord_yammer_profile_token';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addYammerProfileTokenHiddenColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_yammer_profile_token'));
        return this;
    };

    this.EMPLOYEE = 'custrecord_suitesocial_profile_emp';
    /**
     * @return {SuiteSocialProfileSearch}
     */
    this.addEmployeeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_emp'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_suitesocial_profile_alrt_cmnt', 'custrecord_suitesocial_profile_alrt_ment', 'custrecord_suitesocial_profile_alrt_drct', 'custrecord_suitesocial_profile_alrt_stat', 'custrecord_suitesocial_profile_alrt_rcrd', 'custrecord_suitesocial_profile_emp', 'custrecord_suitesocial_profile_image', 'custrecord_yammer_integration_enabled', 'custrecord_ss_profile_last_status', 'custrecord_suitesocial_profile_digest',
            'custrecord_suitesocial_profile_wknd_dgst', 'custrecord_yammer_profile_token' ];
}

function SuiteSocialProfileChannelSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialProfileChannelSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_profile_channel', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Channel_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_ch', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Channel_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_ch', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Channel_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_ch', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Channel_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_ch', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.EmailAlerts_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_alrt', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.EmailAlerts_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_alrt', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.EmailAlerts_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_alrt', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.EmailDigest_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_dgst', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.EmailDigest_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_dgst', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.EmailDigest_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_dgst', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Profile_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_prof', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Profile_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_prof', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Profile_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_prof', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.Profile_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_profile_ch_prof', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.EMAIL_ALERTS = 'custrecord_suitesocial_profile_ch_alrt';
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addEmailAlertsColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_ch_alrt'));
        return this;
    };

    this.EMAIL_DIGEST = 'custrecord_suitesocial_profile_ch_dgst';
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addEmailDigestColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_ch_dgst'));
        return this;
    };

    this.CHANNEL = 'custrecord_suitesocial_profile_ch_ch';
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addChannelColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_ch_ch'));
        return this;
    };

    this.PROFILE = 'custrecord_suitesocial_profile_ch_prof';
    /**
     * @return {SuiteSocialProfileChannelSearch}
     */
    this.addProfileColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_profile_ch_prof'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_suitesocial_profile_ch_ch', 'custrecord_suitesocial_profile_ch_alrt', 'custrecord_suitesocial_profile_ch_dgst', 'custrecord_suitesocial_profile_ch_prof' ];
}

function SuiteSocialRecordSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialRecordSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialRecordSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialRecordSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_record', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.AutopostOnCreate_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_record_autopost', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.AutopostOnCreate_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_record_autopost', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.AutopostOnCreate_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_record_autopost', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Type_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_record_type', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Type_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_record_type', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Type_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_record_type', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.Type_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_record_type', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.AUTOPOST_ON_CREATE = 'custrecord_suitesocial_record_autopost';
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.addAutopostOnCreateColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_record_autopost'));
        return this;
    };

    this.TYPE = 'custrecord_suitesocial_record_type';
    /**
     * @return {SuiteSocialRecordSearch}
     */
    this.addTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_record_type'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_suitesocial_record_autopost', 'custrecord_suitesocial_record_type' ];
}

function SuiteSocialRelatedRecordSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialRelatedRecordSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_ssr_ss_related_record', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record_type', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record_type', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record_type', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.RelatedSuiteSocialRecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_related_ss_record_type', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_ss_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_ss_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_ss_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_ss_record', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_record_type', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_record_type', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_record_type', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.SuiteSocialRecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssrr_record_type', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.RELATED_SUITESOCIAL_RECORD = 'custrecord_ssrr_related_ss_record';
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addRelatedSuiteSocialRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssrr_related_ss_record'));
        return this;
    };

    this.RELATED_SUITESOCIAL_RECORD_TYPE = 'custrecord_ssrr_related_ss_record_type';
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addRelatedSuiteSocialRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssrr_related_ss_record_type'));
        return this;
    };

    this.SUITESOCIAL_RECORD = 'custrecord_ssrr_ss_record';
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addSuiteSocialRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssrr_ss_record'));
        return this;
    };

    this.SUITESOCIAL_RECORD_TYPE = 'custrecord_ssrr_record_type';
    /**
     * @return {SuiteSocialRelatedRecordSearch}
     */
    this.addSuiteSocialRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssrr_record_type'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_ssrr_related_ss_record', 'custrecord_ssrr_related_ss_record_type', 'custrecord_ssrr_ss_record', 'custrecord_ssrr_record_type' ];
}

function SuiteSocialRoleSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialRoleSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialRoleSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialRoleSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_role', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.RoleField_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_role_field', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.RoleField_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_role_field', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.RoleField_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_role_field', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.RoleField_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_role_field', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.ROLE_FIELD = 'custrecord_suitesocial_role_field';
    /**
     * @return {SuiteSocialRoleSearch}
     */
    this.addRoleFieldColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_role_field'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_suitesocial_role_field' ];
}

function SuiteSocialScriptedRecordSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialScriptedRecordSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_scripted_record', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_IS = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.Name_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('name', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.FieldScriptIdList_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_field_script_id_list', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptId_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_script_id', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptedRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_scripted_record', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptedRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_scripted_record', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptedRecord_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_scripted_record', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.ScriptedRecord_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_ssr_scripted_record', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.NAME = 'name';
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addNameColumn = function() {
        _columns.push(new ssobjSearchColumn('name'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.FIELD_SCRIPT_ID_LIST = 'custrecord_ssr_field_script_id_list';
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addFieldScriptIdListColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssr_field_script_id_list'));
        return this;
    };

    this.SCRIPT_ID = 'custrecord_ssr_script_id';
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addScriptIdColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssr_script_id'));
        return this;
    };

    this.SCRIPTED_RECORD = 'custrecord_ssr_scripted_record';
    /**
     * @return {SuiteSocialScriptedRecordSearch}
     */
    this.addScriptedRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_ssr_scripted_record'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'name', 'isinactive', 'custrecord_ssr_field_script_id_list', 'custrecord_ssr_script_id', 'custrecord_ssr_scripted_record' ];
}

function SuiteSocialSubscriptionSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new SuiteSocialSubscriptionSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_subscription', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Call_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_phonecall', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Call_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_phonecall', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Call_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_phonecall', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Call_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_phonecall', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Campaign_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_campaign', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Campaign_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_campaign', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Campaign_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_campaign', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Campaign_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_campaign', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Case_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_supportcase', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Case_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_supportcase', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Case_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_supportcase', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Case_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_supportcase', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Competitor_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_competitor', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Competitor_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_competitor', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Competitor_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_competitor', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Competitor_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_competitor', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Contact_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_contact', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Contact_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_contact', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Contact_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_contact', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Contact_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_contact', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_EQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'EQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_GREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'GREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_GREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'GREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_LESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'LESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_LESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'LESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_NOTEQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'NOTEQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_NOTGREATERTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'NOTGREATERTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_NOTGREATERTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'NOTGREATERTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_NOTLESSTHAN = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'NOTLESSTHAN', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_NOTLESSTHANOREQUALTO = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'NOTLESSTHANOREQUALTO', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_BETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'BETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomRecord_NOTBETWEEN = function(value1, value2) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customrecord', null, 'NOTBETWEEN', value1, value2));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'ANY', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'IS', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.CustomType_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customtype', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Customer_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customer', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Customer_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customer', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Customer_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customer', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Customer_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_customer', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Employee_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_employee', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Employee_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_employee', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Employee_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_employee', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Employee_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_employee', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Event_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_calendarevent', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Event_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_calendarevent', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Event_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_calendarevent', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Event_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_calendarevent', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Issue_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_issue', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Issue_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_issue', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Issue_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_issue', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Issue_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_issue', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Item_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_item', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Item_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_item', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Item_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_item', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Item_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_item', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Opportunity_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_opportunity', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Opportunity_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_opportunity', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Opportunity_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_opportunity', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Opportunity_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_opportunity', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Partner_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_partner', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Partner_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_partner', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Partner_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_partner', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Partner_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_partner', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Project_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_job', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Project_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_job', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Project_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_job', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Project_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_job', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Subscriber_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_subscriber', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Subscriber_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_subscriber', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Subscriber_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_subscriber', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Subscriber_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_subscriber', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Task_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_task', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Task_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_task', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Task_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_task', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Task_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_task', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Transaction_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Transaction_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Transaction_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Transaction_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.TransactionType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction_t', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.TransactionType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction_t', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.TransactionType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction_t', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.TransactionType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_transaction_t', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Vendor_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_vendor', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Vendor_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_vendor', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Vendor_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_vendor', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.Vendor_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_sub_vendor', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.CUSTOM_RECORD = 'custrecord_suitesocial_sub_customrecord';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addCustomRecordColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_customrecord'));
        return this;
    };

    this.CUSTOM_TYPE = 'custrecord_suitesocial_sub_customtype';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addCustomTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_customtype'));
        return this;
    };

    this.CALL = 'custrecord_suitesocial_sub_phonecall';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addCallColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_phonecall'));
        return this;
    };

    this.CAMPAIGN = 'custrecord_suitesocial_sub_campaign';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addCampaignColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_campaign'));
        return this;
    };

    this.CASE = 'custrecord_suitesocial_sub_supportcase';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addCaseColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_supportcase'));
        return this;
    };

    this.COMPETITOR = 'custrecord_suitesocial_sub_competitor';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addCompetitorColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_competitor'));
        return this;
    };

    this.CONTACT = 'custrecord_suitesocial_sub_contact';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addContactColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_contact'));
        return this;
    };

    this.CUSTOMER = 'custrecord_suitesocial_sub_customer';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addCustomerColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_customer'));
        return this;
    };

    this.EMPLOYEE = 'custrecord_suitesocial_sub_employee';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addEmployeeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_employee'));
        return this;
    };

    this.EVENT = 'custrecord_suitesocial_sub_calendarevent';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addEventColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_calendarevent'));
        return this;
    };

    this.ISSUE = 'custrecord_suitesocial_sub_issue';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addIssueColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_issue'));
        return this;
    };

    this.ITEM = 'custrecord_suitesocial_sub_item';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addItemColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_item'));
        return this;
    };

    this.OPPORTUNITY = 'custrecord_suitesocial_sub_opportunity';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addOpportunityColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_opportunity'));
        return this;
    };

    this.PARTNER = 'custrecord_suitesocial_sub_partner';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addPartnerColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_partner'));
        return this;
    };

    this.PROJECT = 'custrecord_suitesocial_sub_job';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addProjectColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_job'));
        return this;
    };

    this.SUBSCRIBER = 'custrecord_suitesocial_sub_subscriber';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addSubscriberColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_subscriber'));
        return this;
    };

    this.TASK = 'custrecord_suitesocial_sub_task';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addTaskColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_task'));
        return this;
    };

    this.TRANSACTION = 'custrecord_suitesocial_sub_transaction';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addTransactionColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_transaction'));
        return this;
    };

    this.TRANSACTION_TYPE = 'custrecord_suitesocial_sub_transaction_t';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addTransactionTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_transaction_t'));
        return this;
    };

    this.VENDOR = 'custrecord_suitesocial_sub_vendor';
    /**
     * @return {SuiteSocialSubscriptionSearch}
     */
    this.addVendorColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_sub_vendor'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_suitesocial_sub_phonecall', 'custrecord_suitesocial_sub_campaign', 'custrecord_suitesocial_sub_supportcase', 'custrecord_suitesocial_sub_competitor', 'custrecord_suitesocial_sub_contact', 'custrecord_suitesocial_sub_customrecord', 'custrecord_suitesocial_sub_customtype', 'custrecord_suitesocial_sub_customer', 'custrecord_suitesocial_sub_employee', 'custrecord_suitesocial_sub_calendarevent',
            'custrecord_suitesocial_sub_issue', 'custrecord_suitesocial_sub_item', 'custrecord_suitesocial_sub_opportunity', 'custrecord_suitesocial_sub_partner', 'custrecord_suitesocial_sub_job', 'custrecord_suitesocial_sub_subscriber', 'custrecord_suitesocial_sub_task', 'custrecord_suitesocial_sub_transaction', 'custrecord_suitesocial_sub_transaction_t', 'custrecord_suitesocial_sub_vendor' ];
}

function TrackedFieldSearch() {
    if (this == window || this.constructor.name == 'Object') {
        return new TrackedFieldSearch();
    }
    var _results = null;
    // ssobjSearchColumns
    var _columns = [];
    // ssobjSearchFilters
    var _filters = [];
    this.resetSearch = function() {
        _columns = [];
        _filters = [];
        _results = null;
    };

    /**
     * Adds a search column to the internal array of search columns
     * 
     * @param {ssobjSearchColumn}
     *        searchColumn
     * @return {TrackedFieldSearch}
     */
    this.addSearchColumn = function(searchColumn) {
        _columns.push(searchColumn);
        return this;
    };
    /**
     * Adds a search filter to the internal array of search filters
     * 
     * @param {ssobjSearchFilter}
     *        searchFilter
     * @return {TrackedFieldSearch}
     */
    this.addSearchFilter = function(searchFilter) {
        _filters.push(searchFilter);
        return this;
    };
    /**
     * Returns array of ssobjSearchResult. Call this once you have defined the
     * search columns and filters
     * 
     * @return {Array}
     */
    this.searchRecord = function() {
        if (_columns.length === 0) {
            _columns = ssapiCreateSearchColumnsForAllFields(this.FIELDS_WITH_STORE_VALUE_TRUE);
        }
        _results = ssapiSearchRecord('customrecord_suitesocial_autopost_field', null, _filters, _columns);
        return _results;
    };
    /**
     * Returns an array of nlobjSearchColumn.
     * 
     * @return {Array}
     */
    this.getColumns = function() {
        return ssapiGetNetSuiteSearchColumns(_columns);
    };
    /**
     * Returns an array of nlobjSearchFilter.
     * 
     * @return {Array}
     */
    this.getFilters = function() {
        return ssapiGetNetSuiteSearchFilters(_filters);
    };
    /**
     * Return the last item in the array of nlobjSearchColumns.
     * 
     * @return {nlobjSearchColumn}
     */
    this.getLastNetSuiteColumn = function() {
        if (_columns.length === 0) {
            return null;
        }
        return _columns[_columns.length - 1].getNetSuiteColumn();
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Id_ALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ALLOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Id_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Id_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Id_NOTALLOF = function(value) {
        _filters.push(new ssobjSearchFilter('internalid', null, 'NOTALLOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.IsInactive_ISTRUE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'T'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.IsInactive_ISFALSE = function() {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'is', 'F'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.IsInactive_IS = function(value) {
        _filters.push(new ssobjSearchFilter('isinactive', null, 'IS', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Field_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rf', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Field_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rf', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Field_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rf', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.Field_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rf', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldHidden_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_fld', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldHidden_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_fld', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldHidden_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_fld', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldHidden_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_fld', null, 'NONEOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'ANY', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'IS', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldDataType_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_dt', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_ANY = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'ANY', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_CONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'CONTAINS', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_DOESNOTCONTAINS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'DOESNOTCONTAINS', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_DOESNOTSTARTWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'DOESNOTSTARTWITH', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_IS = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'IS', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_ISNOT = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'ISNOT', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.FieldID_STARTSWITH = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_id', null, 'STARTSWITH', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.RecordType_ISEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rec', null, 'ISEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.RecordType_ISNOTEMPTY = function() {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rec', null, 'ISNOTEMPTY'));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.RecordType_ANYOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rec', null, 'ANYOF', value));
        return this;
    };
    /**
     * @return {TrackedFieldSearch}
     */
    this.RecordType_NONEOF = function(value) {
        _filters.push(new ssobjSearchFilter('custrecord_suitesocial_autopost_fld_rec', null, 'NONEOF', value));
        return this;
    };

    this.ID = 'internalid';
    /**
     * @return {TrackedFieldSearch}
     */
    this.addIdColumn = function() {
        _columns.push(new ssobjSearchColumn('internalid'));
        return this;
    };

    this.IS_INACTIVE = 'isinactive';
    /**
     * @return {TrackedFieldSearch}
     */
    this.addIsInactiveColumn = function() {
        _columns.push(new ssobjSearchColumn('isinactive'));
        return this;
    };

    this.FIELD_DATA_TYPE = 'custrecord_suitesocial_autopost_fld_dt';
    /**
     * @return {TrackedFieldSearch}
     */
    this.addFieldDataTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autopost_fld_dt'));
        return this;
    };

    this.FIELD_ID = 'custrecord_suitesocial_autopost_fld_id';
    /**
     * @return {TrackedFieldSearch}
     */
    this.addFieldIDColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autopost_fld_id'));
        return this;
    };

    this.FIELD = 'custrecord_suitesocial_autopost_fld_rf';
    /**
     * @return {TrackedFieldSearch}
     */
    this.addFieldColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autopost_fld_rf'));
        return this;
    };

    this.FIELD_HIDDEN = 'custrecord_suitesocial_autopost_fld_fld';
    /**
     * @return {TrackedFieldSearch}
     */
    this.addFieldHiddenColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autopost_fld_fld'));
        return this;
    };

    this.RECORD_TYPE = 'custrecord_suitesocial_autopost_fld_rec';
    /**
     * @return {TrackedFieldSearch}
     */
    this.addRecordTypeColumn = function() {
        _columns.push(new ssobjSearchColumn('custrecord_suitesocial_autopost_fld_rec'));
        return this;
    };
    this.FIELDS_WITH_STORE_VALUE_TRUE = [ 'internalid', 'isinactive', 'custrecord_suitesocial_autopost_fld_rf', 'custrecord_suitesocial_autopost_fld_fld', 'custrecord_suitesocial_autopost_fld_dt', 'custrecord_suitesocial_autopost_fld_id', 'custrecord_suitesocial_autopost_fld_rec' ];
}

// 183
