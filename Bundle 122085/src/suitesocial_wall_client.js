/**
 * 
 * © 2014 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * 
 * otherwise make available this code.
 * 
 * 
 * 
 * @fileOverview Client functions for the News Feed Wall/UI
 * 
 * 
 * 
 * @author tcaguioa
 * 
 */

var ssobjGlobal = ssobjGlobal || {};

// if a preview image is loading

ssobjGlobal.isPreviewImageLoading = false;

ssobjGlobal.channelEditorEnabled = {};

/**
 * 
 * Add indexOf() for Array object since not all browsers support it
 * 
 */

function socialAddIndexOfFunction() {

    if (!Array.prototype.indexOf) {

        Array.prototype.indexOf = function(obj, start) {

            for (var i = (start || 0), j = this.length; i < j; i++) {

                if (this[i] === obj) {

                    return i;

                }

            }

            return -1;

        };

    }

}

/**
 * 
 * Adds trim functions if browser has no support
 * 
 */

function socialAddTrimFunctions() {

    if (typeof String.trim == 'undefined') {

        String.prototype.trim = function() {

            return this.replace(/^\s+|\s+$/g, "");

        };

    }

    if (typeof String.ltrim == 'undefined') {

        String.prototype.ltrim = function() {

            return this.replace(/^\s+/, "");

        };

    }

    if (typeof String.rtrim == 'undefined') {

        String.prototype.rtrim = function() {

            return this.replace(/\s+$/, "");

        };

    }

}

// call functions to run on load here

socialAddIndexOfFunction();

socialAddTrimFunctions();

// define constants
var CONTEXT = nlapiGetContext();
var PUBLIC_CHANNEL_NAME = CONTEXT.getRoleCenter() == "CUSTOMER" ? "All Posts" : "Public".tl();

var ENTER_KEYCODE = 13;

var UP_ARROW = 38;

var DOWN_ARROW = 40;

var ENTER_RIGHT_ARROW = 38;

var ENTER_LEFT_ARROW = 37;

var ESC_KEYCODE = 27;

var BACKSPACE_KEYCODE = 8;

var DOWN_ARROW_MARKUP = '<span style="color: silver; font-size: 7pt">&#9660; </span>';

var UP_ARROW_MARKUP = '<span style="color: silver; font-size: 7pt">&#9650; </span>';

var RIGHT_ARROW_MARKUP = '<span style="color: silver; font-size: 7pt">&#9658; </span>';

var RIGHT_ARROW_MARKUP_FOR_SAFARI = '<span style="color: silver; font-size: 9pt">&#9658; </span>';

var HOURGLASS = '<img src="/images/setup/hourglass.gif" />';

var DIRECT_MESSAGES_ID = '1';

var STATUS_CHANGES_ID = '13';

var RECORD_CHANGES_ID = '14';

var FADEIN_SPEED = 500;

var LABEL_EXPAND_CHANNELS = 'Expand Channels'.tl();

var LABEL_COLLAPSE_CHANNELS = 'Collapse Channels'.tl();

// ******* start of UI functions ******* //

/* define functions that run on init */

function socialRemoveTitleBar() {

    // remove title bar

    var titleElement = Ext.select('.uir-page-title');

    if (titleElement)

        titleElement.remove();

    // remove box of buttons (above the newsfeed)

    var el = Ext.select('.uir-header-buttons').elements[0];

    if (el)

        Ext.get(Ext.get(el).findParent('table')).remove();

}

function socialAdjustUIForPortlet() {

    ssobjGlobal.isPortlet = ssobjGlobal.urlParams["nfportlet"] == 'T';

    if (ssobjGlobal.isPortlet) {

        Ext.get('titlebar').setStyle('display', 'none');

        Ext.select('.ss_column_left').setStyle('padding-left', '0px');

        Ext.get('mainpane').setStyle('margin-top', '0px');

        Ext.get('maincontainer').setStyle('padding-left', '0px');

        Ext.select('div#mainpane div.ss_portlet.ns-portlet-wrapper').setStyle('margin', '0px');

        jQuery('#maincontainer').closest('#div__body').css('cssText', 'padding-top: 0px !important;');

        jQuery('.ss_portlet_hdr.ns-portlet-header').css('cssText', 'background-color: rgb(139, 157, 182) !important');

    }

}

function socialGetTabContentTopProperty() {

    return ssobjGlobal.tabContentTop || 269; // 269 = actual top value

}

function socialSetMainColumnLeft() {

    // no need to do if on record page

    if (ssapiHasValue(ssobjGlobal.recordType)) {

        return;

    }

    // if on normal mode (not responsive), set left of mainpane; otherwise dont

    if (!socialIsResponsiveMode()) {

        // get width of left pane

        var leftColWidth = Ext.get("leftpane").getWidth();

        // set left of main pane

        Ext.get("mainpane").setLeft(leftColWidth);

        // remove style of leftpane; to let global style work

        jQuery("#leftpane").css("visibility", "");

        // remove style of mainpane; to let global style work

        jQuery("#mainpane").css("width", "");

    } else {

        // check if left pane is not visible, then set left of main pane to

        // fixed

        if (!Ext.get("leftpane").isVisible()) {

            Ext.get("mainpane").setLeft(0);

            // set width of main pane

            var iBodyWidth = Ext.get("maincontainer").getWidth() - ((ssapiIsMobile() && !ssobjGlobal.isPortlet) ? 8 : 20);

            // add 20 if on small portlet

            iBodyWidth += ssobjGlobal.isPortlet ? 20 : 0;

            Ext.get("mainpane").setWidth(iBodyWidth);

            // set visual arrow to right

            if (ssapiHasValue(ssobjGlobal.imagesUrlsObj)) {

                Ext.select("#visualarrow img").set({

                    style : 'background-image: url("' + ssobjGlobal.imagesUrlsObj['visual-arrow-right.png'] + '"); background-repeat: no-repeat;"'

                });

            }

        }

        // set position of visual arrow

        socialSetVisualIndicatorPosition();

        // hide horizontal scrollbar

        Ext.getBody().setStyle("overflow-x", "hidden");

    }

}

function socialSetLeftColumnHeight() {

    // no need to do if on record page

    if (ssapiHasValue(ssobjGlobal.recordType)) {

        return;

    }

    // get height of body, and top of tab content

    var iBodyHeight = Ext.getBody().getViewSize().height;

    var iTabContentTop = socialGetTabContentTopProperty();

    // calculate and set height of tab content

    var iHeight = iBodyHeight - iTabContentTop - 15;

    // calculate height this way if on iframe (portlet)

    if (parent.document.getElementById('suitesocialwall'))

        iHeight = iBodyHeight - 155;

    Ext.select(".ss_tab_content").setStyle("height", iHeight + "px");

    try {

        // Issue 327979 [SuiteSocial] Visual indicator graphic > The button

        // looks out of place without its left border for feeds with no posts

        var networkHeight = Ext.get('network').getHeight();

        var el = Ext.select('#mainpane .ns-portlet-wrapper').elements[0];

        jQuery(el).css({

            'min-height' : networkHeight + 30

        });

    } catch (e) {

        // TODO: handle exception

        // no handler for now since it might inject error

    }

}

/**
 * Calculates and sets the min-height of main pane.
 */
function socialSetMainColumnMinHeight() {
    // get height of body, and top of tab content
    var iBodyHeight = Ext.getBody().getViewSize().height;
    var iMainTop = Ext.get("mainpane").getTop();
    // calculate and set height of main pane
    var iHeight = iBodyHeight - iMainTop - 20;
    Ext.select("#mainpane .ss_portlet").setStyle("min-height", iHeight + "px");
    // set min-height of body
    iBodyHeight = iHeight + 55;
    Ext.get("body").setStyle("min-height", iBodyHeight + "px");
}

/**
 * 
 * Calculates and sets the position of the visual indicator.
 * 
 */

function socialSetVisualIndicatorPosition() {

    // set height of visual arrow

    var iLeftPaneHeight = Ext.get("leftpane").getHeight() - 20;

    Ext.get("visualarrow").setHeight(iLeftPaneHeight);

}

function socialOnResizeWindow() {

    // if customer, check if colleague pane is to be shown
    if (nlapiGetContext().rolecenter == ssobjGlobal.CENTER_TYPES.CUSTOMER_CENTER && !ssobjGlobal.isShowColleaguePane) {
        // set min-height of main pane
        socialSetMainColumnMinHeight();
        return;
    }

    // set height of left pane

    socialSetLeftColumnHeight();

    // set left of main pane to adjust with left pane width

    socialSetMainColumnLeft();

    // code to fix the shrinking of height in sp2

    var el = Ext.select("form#main_form > div > ul > li");

    if (el) {

        var height = Ext.get("maincontainer").getHeight();

        height = height + "px";

        el.setStyle("height", height);

    }

    // set width of shareto dropdown

    if (!ssapiIsMobile()) {

        var iWidth = Ext.get("sharetolist").getWidth();

        Ext.get("sharetodropdown").setWidth(iWidth);

    }

    // show hide New Message label on maincontainer
    var mainContainerWidth = jQuery('#mainpane').width();
    if (mainContainerWidth < 500) {
        jQuery('#newPrivateMessageLabel').hide();
    } else {
        jQuery('#newPrivateMessageLabel').show();
    }

}

/* call functions here */

socialRemoveTitleBar();

window.onresize = function(event) {

    // do stuff on resize window

    socialOnResizeWindow(event);

};

/**
 * 
 * On-click event on the body to hide open menus and tooltips
 * 
 */

Ext.getBody().on("click", function(e) {

    socialHideOpenMenus(e);

    socialHideAllTooltips();

});

/**
 * 
 * On-click event to display colleagues in either tile or list view
 * 
 */

Ext.select("div#actionnetworkviewlist.ss_portlet_network_hdr_action_list ul li").on("click", function(e, t) {

    socialToggleView(e);

});

/**
 * 
 * On-click event to show the colleague menu (tile/list)
 * 
 */

Ext.get("actionnetworkview").on("click", function(e) {

    // stop propagation

    e.stopPropagation();

    // proceed to show menu

    socialShowActionMenu("actionnetworkviewlist");

});

/**
 * 
 * On-click event to show the Share To dropdown menu
 * 
 */

Ext.get("sharetodropdown").on("click", function(e) {

    // stop propagation

    e.stopPropagation();

    // proceed to show menu

    socialShowActionMenu("sharetolist");

});

/**
 * 
 * On-scroll event to carry the colleague balloon while scrolling
 * 
 */

Ext.select(".ss_tab_content").on("scroll", function() {

    // get tooltip object; check if has value; skip if none

    var tooltip = ssobjGlobal.lastTooltip;

    if (ssapiHasNoValue(tooltip))

        return;

    // check if target is colleague list; skip if not

    if (tooltip.target.id.indexOf("colleaguePhoto") == -1)

        return;

    // set top of tooltip while scrolling

    var iTop = tooltip.target.getTop();

    var iScrollTop = Ext.getBody().getScroll().top;

    iTop = iTop - iScrollTop;

    // check anchor if bottom, adjust some more

    if (tooltip.anchor == "bottom") {

        var iHeight = Ext.get("labsTutorialTooltip").getHeight();

        iTop = iTop - iHeight - 10;

    }

    Ext.get("labsTutorialTooltip").setTop(iTop);

    Ext.select(".x-shadow").setTop(iTop);

    Ext.select(".x-ie-shadow").setTop(iTop);

});

/**
 * 
 * On-input event on richEditor to trigger onRichEditorChange
 * 
 */

Ext.get("richEditor").on("input", function(e) {

    onRichEditorChange(Ext.get("richEditor").dom);

});

/* define functions that run on demand */

function socialShowActionMenu(menuId) {

    if (Ext.get(menuId).isVisible())

        Ext.get(menuId).hide();

    else

        Ext.get(menuId).show();

}

function socialHideOpenMenus(e) {

    var e = e || {};

    var target = e.target || {};

    var id = target.id || "";

    // check if action buttons are hit or not

    if (id != "actionchannels") {

        // hide channels menu

        if (Ext.get("actionchannelslist")) {

            if (Ext.get("actionchannelslist").isVisible())

                Ext.get("actionchannelslist").hide();

        }

    }

    if (id != "actionnetworkview") {

        // hide network view menu

        if (Ext.get("actionnetworkviewlist")) {

            if (Ext.get("actionnetworkviewlist").isVisible())

                Ext.get("actionnetworkviewlist").hide();

        }

    }

    if (id != "sharetodropdown") {

        // hide share dropdown menu

        if (Ext.get("sharetolist")) {

            if (Ext.get("sharetolist").isVisible())

                Ext.get("sharetolist").hide();

        }

    }

    if (id != "searchColleaguesTextBox") {

        // hide colleagues search result list

        if (Ext.get("placeHolderColleagueOthersContainer")) {

            if (Ext.get("placeHolderColleagueOthersContainer").isVisible())

                Ext.get("placeHolderColleagueOthersContainer").hide();

        }

    }

}

function socialShowActiveTab(tabId) {

    // get target tab

    var targetTab = Ext.get(tabId);

    var isActive = targetTab.hasClass("active");

    // if already active, then nothing to do

    if (!isActive) {

        // get the other tab, and tab content elements

        var otherTabId = "";

        var targetTabContentId = "";

        var otherTabContentId = "";

        if (tabId == "tabfollowing") {

            otherTabId = "tabfollowers";

            targetTabContentId = "tabcontentfollowing";

            otherTabContentId = "tabcontentfollowers";

        } else {

            otherTabId = "tabfollowing";

            targetTabContentId = "tabcontentfollowers";

            otherTabContentId = "tabcontentfollowing";

        }

        // remove active class from other tab and tab content

        Ext.get(otherTabId).removeClass("active");

        Ext.get(otherTabContentId).removeClass("active");

        // add active class to target tab and tab content

        targetTab.addClass("active");

        Ext.get(targetTabContentId).addClass("active");

    }

}

function socialToggleView(e) {

    // skip if active view

    if (e.target.className.trim() == "active") {

        e.stopEvent();

        return;

    }

    // set view mode

    var id = e.target.id;

    if (id == "actionnetworkviewtilemode") {

        // set to tile view

        socialSetColleagueTileView("T");

    } else if (id == "actionnetworkviewlistmode") {

        // set to list view

        socialSetColleagueTileView("F");

    }

}

/**
 * Gets the selected channel from the channels menu, and displays the newsfeed
 * posts for that channel
 * 
 * @param e
 */
function socialToggleChannel(e) {
    if (e.target.nodeName === 'A') {// edit link is clicked
        return;
    }

    // check if parent li
    var el = e.target;
    if (el.hasAttribute("data-channelId") == false) {
        el = el.offsetParent;
    }

    // skip if active view
    if (Ext.get(el).hasClass("active")) {
        e.stopEvent();
        return;
    }

    // get channel id
    var channelId = el.getAttribute("data-channelId");

    // set channel view
    socialSetChannelView(channelId);
}

/**
 * 
 * Gets the selected channel from the Share To dropdown, and displays the
 * 
 * newsfeed posts for that channel
 * 
 * 
 * 
 * @param e
 * 
 */

function socialShareToChannel(e) {

    // skip if active view

    if (e.target.className.trim() == "active" || e.target.className.trim() == "disabled") {

        e.stopEvent();

        return;

    }

    // check if default share button

    if (e.target.id == "btnsharedefault") {

        // check container if disabled

        if (Ext.get("btnsharecontainer").hasClass("disabled")) {

            e.stopEvent();

            return;

        }

        // add post

        socialAddPost();

    } else {

        // get channel id

        var channelId = e.target.getAttribute("data-channelId");

        // set channel view

        socialSetChannelView(channelId);

        // add post to channel

        socialAddPost();

    }

}

function socialSetChannelView(channelId) {

    // remove active class from other channel

    if (Ext.select("div#actionchannelslist ul li.active").elements.length > 0)

        Ext.select("div#actionchannelslist ul li.active").removeClass("active");

    if (Ext.select("div#sharetolist ul li.active").elements.length > 0)

        Ext.select("div#sharetolist ul li.active").removeClass("active");

    // add active class from target channel

    var id = "channel-" + channelId;

    if (Ext.get(id))

        Ext.get(id).addClass("active");

    var idSelect = "channelselect-" + channelId;

    if (Ext.get(idSelect))

        Ext.get(idSelect).addClass("active");

    // set channel view

    socialShowPostsByChannel(Ext.get(id)/* .dom */, channelId);

}

/**
 * 
 * Shows/Hides left pane.
 * 
 */

function socialShowHideLeftPane() {

    // visual indicator left-offset

    var viLeftOffset = (ssapiIsMobile() && !ssobjGlobal.isPortlet) ? 8 : 20;

    // less 20 if on small portlet

    viLeftOffset -= ssobjGlobal.isPortlet ? 20 : 0;

    // show leftpane, if not shown yet

    if (!socialIsLeftPaneShown()) {

        // change visual arrow icon

        Ext.select("#visualarrow img").set({

            style : 'background-image: url("' + ssobjGlobal.imagesUrlsObj['visual-arrow-left.png'] + '"); background-repeat: no-repeat;"'

        });

        // show left pane

        Ext.get("leftpane").setStyle("visibility", "visible");

        // set left of main pane

        jQuery("#mainpane").animate({

            left : 290

        }, {

            duration : 500,

            step : function(now, fx) {

                jQuery("#visualarrow").css("left", now + viLeftOffset);

            }

        });

    } else {

        // change visual arrow icon

        Ext.select("#visualarrow img").set({

            style : 'background-image: url("' + ssobjGlobal.imagesUrlsObj['visual-arrow-right.png'] + '"); background-repeat: no-repeat;"'

        });

        // set left of main pane

        jQuery("#mainpane").animate({

            left : 0

        }, {

            duration : 500,

            step : function(now, fx) {

                jQuery("#visualarrow").css("left", now + viLeftOffset);

            },

            complete : function() {

                // hide left pane; remove style

                jQuery("#leftpane").css("visibility", "");

            }

        });

    }

}

/**
 * 
 * Checks if the left pane is currently shown
 * 
 * 
 * 
 * @returns {Boolean}
 * 
 */

function socialIsLeftPaneShown() {

    // check left of mainpane to know if leftpane is shown or not

    var left = Ext.get("mainpane").getStyle("left");

    left = parseInt(left.replace("px", ""));

    // if mainpane's left style is > 0, then left pane is shown

    return (left > 0);

}

/**
 * 
 * Check if currently on responsive mode
 * 
 * 
 * 
 * @returns {Boolean}
 * 
 */

function socialIsResponsiveMode() {

    // check z-index of left pane to know if responsive design is active at the

    // moment

    var leftZIndex = Ext.get("leftpane").getStyle("z-index");

    // if z-index is 0, then it's on responsive mode

    return (leftZIndex == 0);

}

// ******* start of Client Script functions ******* //

function pageInit() {

    socialPageInitNewsFeed();

}

/**
 * 
 * This is the client script of the SuiteWall.<br>
 * 
 * This function is the alias of pageInit.<br>
 * 
 * pageInit is referenced in customscript_suitewall_client.<br>
 * 
 */

function socialPageInitNewsFeed() {

    var logger = new ssobjLogger(arguments);

    // Correct the document title

    document.title = 'SuiteSocial News Feed - ' + document.title;

    // initialize global variables

    socialInitGlobalVariables();

    // check if user has access

    if (!socialCheckUserHasAccess())
        return;

    // begin loading news feed

    try {

        // show content area

        Ext.get('contentarea').setStyle('display', 'block');

        // see if this is a record page

        socialCheckIfOnRecordPage();

        // initialize some DOM elements

        socialInitDOMElements();

        // get news feed settings

        socialLoadNewsFeedSettings();

        // get UI data, and begin loading channels

        socialLoadUIData(logger);

        // begin loading colleagues

        socialLoadColleagues();

        logger.log('end');

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialInitGlobalVariables() {

    // initialize global variables

    ssobjGlobal.module = 'News feed UI';

    ssobjGlobal.yourChannels = [];

    ssobjGlobal.latestDisplayedPostId = 0;

    ssobjGlobal.offset = 0;

    // ssobjGlobal.size = 5;

    ssobjGlobal.channel = null;

    ssobjGlobal.isNewFeedProcessing = false;

    ssobjGlobal.tooltips = {};

    // holds colleagues with id as the key

    ssobjGlobal.colleagues = {};

    ssobjGlobal.followingEmpIds = [];

    ssobjGlobal.colleagueEmpId = null;

    // set to true if @tagging search is currently being performed

    ssobjGlobal.globalSearchIsProcessing = false;

    // initialize flag that tells if news feed has loaded

    ssobjGlobal.isLoaded = false;

    ssobjGlobal.isNewsFeedPostsLoaded = false;

    ssobjGlobal.isFollowingColleaguesLoaded = false;

    ssobjGlobal.isFollowersColleaguesLoaded = false;

    ssobjGlobal.isChannelsLoaded = false;

    ssobjGlobal.isColleaguesDisplayed = false;

    // get url parameters

    ssobjGlobal.urlParams = ssapiGetUrlParameters();

    // richEditor is similar to the fb post box where the mentions are

    // highlighted

    // as compared to the plain text version similar to yammer

    // there might be a need to support clients that do not support the

    // richEditor

    // for now, all clients will use the richEditor

    ssobjGlobal.isUseRichEditor = true;

    ssobjGlobal.isShowColleaguePane = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_show_colleague_pane_for_cu') == 'T';

    // performance logger

    ssobjGlobal.logger = new ssobjLogger("onPageLoad");

}

function socialInitDOMElements() {
    // set default target to _top
    Ext.getHead().insertHtml('beforeEnd', '<base target="_top">');

    // adjust UI margin, padding, etc for portlet
    socialAdjustUIForPortlet();

    // get initial value of top property of tab content
    ssobjGlobal.tabContentTop = Ext.get("tabcontentfollowing").getTop();

    // check if on record page or not
    if (ssapiHasNoValue(ssobjGlobal.recordType)) {

        // if customer, check if colleague pane is to be shown
        if (nlapiGetContext().rolecenter == ssobjGlobal.CENTER_TYPES.CUSTOMER_CENTER && !ssobjGlobal.isShowColleaguePane) {
            jQuery('#leftpane div.ss_portlet').hide();
            jQuery('#leftpane').css('padding-left', '0px');
            jQuery('#mainpane').css({
                left : '0px',
                'z-index' : '1',
                width : '100%'
            });
            if (!ssobjGlobal.isPortlet) {
                jQuery('#maincontainer').css('padding-left', '20px');
            }

            // set min-height of main pane
            socialSetMainColumnMinHeight();
        } else {
            // set height of left pane
            socialSetLeftColumnHeight();
            // set left of main pane to adjust with left pane width
            socialSetMainColumnLeft();
        }

        // On-click event to show the channels menu
        Ext.get("actionchannels").on("click", function(e) {
            // stop propagation
            e.stopPropagation();

            // form the list before displaying
            socialFormChannelsMenu();

            // proceed to show menu
            socialShowActionMenu("actionchannelslist");
        });

        Ext.get("actionaddchannel").on("click", function(e, t) {
            showAdhocChannelPopupWindow(e, t);
        });

        // On-click event to expand/collapse the channels menu
        Ext.get("actionchannelsdisplay").on("click", function(e) {
            // stop propagation
            e.stopPropagation();

            // check if expanded or collapsed
            if (Ext.get("actionchannelsdisplay").dom.innerHTML == LABEL_EXPAND_CHANNELS) {
                // proceed to expand channels
                Ext.get("actionchannelsdisplay").update(LABEL_COLLAPSE_CHANNELS);
                socialExpandChannel(true);
            } else {
                // proceed to collapse channels
                Ext.get("actionchannelsdisplay").update(LABEL_EXPAND_CHANNELS);
                socialExpandChannel(false);
            }
        });

        // if on mobile, set the following
        if (ssapiIsMobile()) {
            // set smaller margin
            Ext.select(".ss_main_container").setStyle("padding-left", "8px");
            Ext.select(".ss_portlet").setStyle("margin", "0px 8px 0px 0px");
            Ext.select(".ss_column_left").setStyle("padding-left", "0px");

            // show home icon, hide help icon, hide share-to label
            Ext.get("homelink").setStyle("display", "inline");
            Ext.get("ssHelpLink").setStyle("display", "none");
            Ext.get("sharetolabel").setStyle("display", "none");

            // align share-to dropdown and share btn, and set fixed width on
            // dropdown
            Ext.select(".ss_btn_container_bar").setStyle("padding-left", "15px");
            Ext.get("sharetodropdown").setWidth(200);

            // remove some unnecessary margins
            Ext.get("sharetocontainer").setStyle("margin-bottom", "0px");
            Ext.get("btnsharecontainer").setStyle("margin-bottom", "0px");
        }
    } else {
        // special check for FF, when on record page
        if (Ext.isGecko) {
            Ext.get("contentarea").setStyle("display", "block");
        }

        // channels menu is disabled/hidden if on record page
        Ext.get("actionchannels").setStyle("cursor", "auto");
        Ext.get("actionchannelsarrow").setStyle("display", "none");
        Ext.get("actionaddchannelplus").setStyle("display", "none");

        // left pane is hidden if on record page
        Ext.get("leftpane").setStyle("display", "none");

        // main pane must consume full width
        Ext.get("mainpane").setStyle("left", "0px");
        Ext.get("mainpane").setStyle("width", "100%");
        Ext.get("mainpane").setStyle("margin-top", "0px");
        Ext.select("#mainpane div.ss_portlet").setStyle("margin", "0px");
    }

    // set space and styles for mobile
    if (ssapiIsMobile()) {
        Ext.select(".ss_profile_pic").setStyle("width", "46px").setStyle("height", "46px");
        Ext.select(".ss_btn_container_bar").setStyle("padding-left", "12px");
        Ext.select(".ss_nf_post_container").setStyle("margin-left", "12px");
        Ext.select(".sf_nf_post_comments").setStyle("margin-right", "0px").setStyle("margin-left", "15px");
    }

}

function socialUpdateStatusMessage(message) {

    // update status message

    Ext.get('statusmessage').update(message);

}

function socialShowStatusMessage(bShow) {

    Ext.get('statusbar').setDisplayed(bShow ? '' : 'none');

}

function socialCheckUserHasAccess() {

    // check if user has access

    try {

        // update status message

        socialUpdateStatusMessage('Checking access'.tl() + '...');

        // do a search to check if user has access to the record

        nlapiSearchRecord('customrecord_suitesocial_profile', null, null, [ new nlobjSearchColumn('internalid', null, 'count') ]);

        // has access

        ssobjGlobal.hasAccess = true;

        return true;

    } catch (e) {

        ssobjGlobal.hasAccess = false;

        socialUpdateStatusMessage('You have no access to SuiteSocial.'.tl());

        return false;

    }

}

function socialCheckIfOnRecordPage() {

    // see if this is a record page

    ssobjGlobal.recordType = null;

    ssobjGlobal.recordId = null;

    if (parent !== null) {

        // it is in an iframe

        if (parent.nlapiGetRecordType) {

            if (parent.nlapiGetRecordType() !== null) {

                // record tab

                ssobjGlobal.recordType = parent.nlapiGetRecordType();

                ssobjGlobal.recordId = parent.nlapiGetRecordId();

            }

        }

    }

}

function socialLoadNewsFeedSettings() {

    // get news feed settings, and set to global variable

    ssobjGlobal.newsFeedSettings = ssapiGetNewsFeedSettings();

    // set news feed size to global size variable

    ssobjGlobal.size = ssobjGlobal.newsFeedSettings.size;

    // set tile/list view

    socialSetColleagueTileView(ssobjGlobal.newsFeedSettings.showColleagueAsTiled);

}

function socialSetColleagueTileView(bTile) {

    if (bTile == "T") {

        // remove active class from list view mode

        Ext.get("actionnetworkviewlistmode").removeClass("active");

        // add active class to tile view mode

        Ext.get("actionnetworkviewtilemode").addClass("active");

        // change list view to tile view

        Ext.select(".ss_colleagues_list").removeClass("ss_colleagues_list").addClass("ss_colleagues_tile");

    } else {

        // remove active class from tile view mode

        Ext.get("actionnetworkviewtilemode").removeClass("active");

        // add active class to list view mode

        Ext.get("actionnetworkviewlistmode").addClass("active");

        // change tile view to list view

        Ext.select(".ss_colleagues_tile").removeClass("ss_colleagues_tile").addClass("ss_colleagues_list");

    }

    // make setting sticky

    socialSuiteletProcessAsyncUser('setTileColleagues', bTile, function(results) {

        // should be ok

    });

}

function ssapiGetNewsFeedSettings() {

    var settings = {};

    settings.showColleagueAsTiled = nlapiGetContext().getSetting("SCRIPT", "custscript_ss_show_colleagues_as_tiled");

    settings.collapseChannels = nlapiGetContext().getSetting("SCRIPT", "custscript_ss_collapse_channels");

    settings.collapseFollowing = nlapiGetContext().getSetting("SCRIPT", "custscript_ss_collapse_following");

    settings.collapseFollowers = nlapiGetContext().getSetting("SCRIPT", "custscript_ss_collapse_followers");

    settings.size = nlapiGetContext().getSetting("SCRIPT", "custscript_newsfeed_suitelet_size");

    return settings;

}

function socialLoadUIData(logger) {
    logger.log('Processing getUiData');

    socialSuiteletProcessAsync('getUiData', 'anyvalue', function(dataSet) {
        logger.log('DONE getUiData');

        for ( var p in dataSet) {
            logger.log('p=' + p);
            ssobjGlobal[p] = dataSet[p];
        }

        // set the appropriate images
        Ext.get('hourglass').dom.src = ssobjGlobal.imagesUrlsObj['loading-ani.gif'];
        ssobjGlobal.loadingImageMarkup = '<img src="' + ssobjGlobal.imagesUrlsObj['loading-ani.gif'] + '" />';
        ssobjGlobal.loadingImageMarkupSmall = '<img class="ss_loading_ani_small" src="' + ssobjGlobal.imagesUrlsObj['loading-ani.gif'] + '" />';
        ssobjGlobal.loadingImageMarkupXSmall = '<img class="ss_loading_ani_xsmall" src="' + ssobjGlobal.imagesUrlsObj['loading-ani.gif'] + '" />';
        ssobjGlobal.loadingImageLiteGrayMarkupXSmall = '<img class="ss_loading_ani_xsmall" src="' + ssobjGlobal.imagesUrlsObj['loading-ani-lite-gray.gif'] + '" />';
        ssobjGlobal.TOOLTIP_HELP_ICON = ssobjGlobal.imagesUrlsObj['ss-image-tooltip-help.png'];
        ssobjGlobal.TOOLTIP_MESSAGE_ICON = ssapiIsMobile() ? ssobjGlobal.imagesUrlsObj['message_N.png'] : ssobjGlobal.imagesUrlsObj['message24.png'];
        ssobjGlobal.TOOLTIP_POSTS_ICON = ssapiIsMobile() ? ssobjGlobal.imagesUrlsObj['posts_N.png'] : ssobjGlobal.imagesUrlsObj['post24.png'];
        ssobjGlobal.TOOLTIP_FOLLOW_ICON = ssapiIsMobile() ? ssobjGlobal.imagesUrlsObj['follow_N.png'] : ssobjGlobal.imagesUrlsObj['follow24.png'];
        ssobjGlobal.TOOLTIP_UNFOLLOW_ICON = ssapiIsMobile() ? ssobjGlobal.imagesUrlsObj['unfollow_N.png'] : ssobjGlobal.imagesUrlsObj['unfollow24.png'];
        ssobjGlobal.TOOLTIP_KUDOS_ICON = ssobjGlobal.imagesUrlsObj['kudos_icon.png'];
        ssobjGlobal.clsTooltipLink = ssapiIsMobile() ? "ss_colleague_bubble_action_link_mobile" : "ss_colleague_bubble_action_link";

        Ext.get('dndprogressbarcancelicon').dom.src = ssobjGlobal.imagesUrlsObj['cancel-sml.png'];
        Ext.get('dndthumbnailcancelicon').dom.src = ssobjGlobal.imagesUrlsObj['cancel-sml.png'];
        Ext.get('dndfilenamecancelicon').dom.src = ssobjGlobal.imagesUrlsObj['cancel-sml.png'];
        Ext.get('actionnetworkviewarrow').dom.src = ssobjGlobal.imagesUrlsObj['menu_arrow_white.png'];
        Ext.get('actionchannelsarrow').dom.src = ssobjGlobal.imagesUrlsObj['menu_arrow_white.png'];
        Ext.get('actionaddchannelplus').dom.src = ssobjGlobal.imagesUrlsObj['channel-add.png'];

        Ext.get('newPrivateMessageIconImage').dom.src = ssobjGlobal.TOOLTIP_MESSAGE_ICON;

        Ext.get("editChannelIco").setStyle({
            'width' : '26px',
            'background' : 'url(' + ssobjGlobal.imagesUrlsObj['selectcolumn.png'] + ')',
            'background-repeat' : 'no-repeat',
            'background-position' : 'center'
        });

        Ext.get('fileUploadIcon').dom.src = ssobjGlobal.imagesUrlsObj['file.png'];
        Ext.get('fileUploadIconMessage').dom.src = ssobjGlobal.imagesUrlsObj['file.png'];

        Ext.select(".ss_dnd_filename_icon img").set({
            src : ssobjGlobal.imagesUrlsObj['img_trans.gif']
        });

        Ext.select("#visualarrow img").set({
            src : ssobjGlobal.imagesUrlsObj['img_trans.gif']
        });

        Ext.select("#homelink img").set({
            src : ssobjGlobal.imagesUrlsObj['home-ss-sml.png']
        });

        Ext.select(".ss_search_input_clear").set({
            src : ssobjGlobal.imagesUrlsObj['icon_clear_default.png']
        });

        // begin loading news feed
        socialLoadNewsFeed();

        // begin loading channels
        if (ssapiHasNoValue(ssobjGlobal.recordType)) {
            socialShowChannels();

            // display private message icon
            if (!ssapiIsMobile()) {
                Ext.get('newPrivateMessageContainer').setStyle('display', 'block');
            }
        }

        // get current user's profile
        socialGetCurrentUserAvatar();

        // set to global all channels id and name
        ssobjGlobal.allChannelsObj = dataSet.allChannelsObj;
    });
}

/**
 * 
 * Displays channels
 * 
 */

function socialShowChannels() {

    try {

        // performance logger for channels

        ssobjGlobal.channelsLogger = new ssobjLogger("socialShowChannels");

        if (!ssobjGlobal.isLoaded && !ssobjGlobal.recordType) {

            socialUpdateStatusMessage('Loading channels'.tl() + '...');

            socialShowChannelsHandler(ssobjGlobal.channelsWhereUserIsSubscribed);

        } else {

            socialSuiteletProcessAsync('ssapiGetChannelsUserIsSubscribed', 'any', socialShowChannelsHandler);

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowChannelsHandler(data) {
    try {
        var isCreateEditAllowedInNewsfeed = nlapiGetContext().getSetting("SCRIPT", "custscript_ss_create_edit_channel_nf") === 'T' && ssapiGetRole() === 'Employee';

        for (var i = 0; i < data.length; i++) {
            var channelId = data[i].internalid;
            data[i].channelId = channelId;
            var channelName = data[i].name;

            if ([ DIRECT_MESSAGES_ID, RECORD_CHANGES_ID, STATUS_CHANGES_ID ].indexOf(channelId) > -1 || channelName === 'Recognition') {
                data[i].channelName = channelName.tl();
                data[i].subscribedClass = "";
                data[i].canedit = false;
                data[i].editLink = "";
                data[i].hidden = 'hidden';
            } else {
                data[i].channelName = channelName;
                data[i].subscribedClass = data[i].autoSubscribed ? "" : "subscribed";
                data[i].canedit = data[i].isOwner || nlapiGetContext().getRole() === '3';
                data[i].editText = 'Edit';
                data[i].hidden = data[i].canedit && isCreateEditAllowedInNewsfeed ? 'visible' : 'hidden';
                data[i].createdByAdmin = data[i].createdByAdmin === 'T';
            }
        }

        // insert Public channel in the beginning
        data.splice(0, 0, {
            channelId : "0",
            channelName : PUBLIC_CHANNEL_NAME,
            className : "",
            subscribedClass : "",
            editLink : "",
            hidden : 'hidden'
        });

        ssobjGlobal.yourChannels = data;

        // start to get other channels
        socialShowChannelsOthers();

        // show creation of channels on newsfeed
        if (isCreateEditAllowedInNewsfeed === true && !(ssapiIsMobile())) {
            Ext.get('actionaddchannel').setStyle('display', 'block');
        } else {
            Ext.get('actionaddchannel').setStyle('display', 'none');
        }
    } catch (e) {
        ssapiHandleError(e);
    }
}

/*
 * 
 * Displays other channels
 * 
 */

function socialShowChannelsOthers() {

    try {

        if (!ssobjGlobal.isLoaded && !ssobjGlobal.recordType) {

            socialShowChannelsOthersHandler(ssobjGlobal.otherChannels);

        } else {

            socialSuiteletProcessAsync('getOtherChannels', false, socialShowChannelsOthersHandler);

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowChannelsOthersHandler(data) {
    try {
        var isCreateEditAllowedInNewsfeed = nlapiGetContext().getSetting("SCRIPT", "custscript_ss_create_edit_channel_nf") === 'T' && ssapiGetRole() === 'Employee';

        for (var i = 0; i < data.length; i++) {
            data[i].channelId = data[i].internalid;
            data[i].channelName = data[i].name;
            data[i].subscribedClass = "unsubscribed";

            data[i].canedit = data[i].isOwner || nlapiGetContext().getRole() === '3';
            data[i].editText = 'Edit';
            data[i].createdByAdmin = data[i].createdByAdmin === 'T';
            data[i].hidden = data[i].canedit && isCreateEditAllowedInNewsfeed ? 'visible' : 'hidden';
        }

        ssobjGlobal.otherChannels = data;

        // update writeable channels list
        ssobjGlobal.writeableChannelIds = ssobjGlobal.channelsWhereUserIsAllowedPoster;

        socialAppendChannelSelectionRows();

        // update active channel
        if (ssobjGlobal.isLoaded && ssapiHasValue(ssobjGlobal.channel)) {
            // add active class from target channel
            var channelId = ssobjGlobal.channel;

            var id = "channel-" + channelId;

            if (Ext.get(id))
                Ext.get(id).addClass("active");

            var idSelect = "channelselect-" + channelId;

            if (Ext.get(idSelect))
                Ext.get(idSelect).addClass("active");
        }

        // log end time
        ssobjGlobal.logger.log("*********************************** END: Loading Channels ***********************************");
        ssobjGlobal.channelsLogger.log("*********************************** END: Loading Channels ***********************************");

        // set channels has loaded
        ssobjGlobal.isChannelsLoaded = true;

        // check if page has fully loaded
        if (ssobjGlobal.isNewsFeedPostsLoaded && ssobjGlobal.isFollowingColleaguesLoaded && ssobjGlobal.isFollowersColleaguesLoaded && ssobjGlobal.isChannelsLoaded) {
            ssobjGlobal.isLoaded = true;
            socialShowStatusMessage(false);

        } else {
            if (!ssobjGlobal.isNewsFeedPostsLoaded)
                socialUpdateStatusMessage('Loading posts'.tl() + '...');
            else if (!ssobjGlobal.isFollowingColleaguesLoaded || !ssobjGlobal.isFollowersColleaguesLoaded)
                socialUpdateStatusMessage('Loading colleagues'.tl() + '...');
        }
    } catch (e) {
        ssapiHandleError(e);
    }
}

/**
 * 
 * Displays the channels menu
 * 
 */
function socialFormChannelsMenu() {
    try {
        // ensure default action for Expand/Collapse Channel is expand
        Ext.get("actionchannelsdisplay").update(LABEL_EXPAND_CHANNELS);

        // get subscribed channels
        var data = ssobjGlobal.yourChannels;

        var tmpPost = document.getElementById('tmpChannel').innerHTML;
        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');
        myTplPost.overwrite(document.getElementById('actionchannelslistul'), data, false);

        // get unsubscribed channels
        var data = ssobjGlobal.otherChannels;
        var tmpPost = document.getElementById('tmpChannel').innerHTML;
        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');
        myTplPost.append(document.getElementById('actionchannelslistul'), data, false);

        // show active channel if there is NO colleague is currently selected
        if (ssapiHasNoValue(ssobjGlobal.colleagueEmpId)) {
            // no selected colleague, so set current channel to active
            var channelId = ssobjGlobal.channel;

            if (ssapiHasNoValue(channelId)) {
                channelId = "0";
            }

            // add active class from target channel
            var id = "channel-" + channelId;
            if (Ext.get(id)) {
                Ext.get(id).addClass("active");
            }
        }

        // apply code on click
        Ext.select("div#actionchannelslist.ss_portlet_hdr_action_list ul li").on("click", function(e, t) {
            if (t && t.id === 'editChannelIco')
                return;

            socialToggleChannel(e);
        });

        // apply code on click
        Ext.select("div#actionchannelslist.ss_portlet_hdr_action_list ul li div div a").on("click", function(e, t) {
            showAdhocChannelPopupWindow(e, t);
        });

        // set transparent image to src
        Ext.select(".ss_portlet_hdr_action_list ul li div img").set({
            src : ssobjGlobal.imagesUrlsObj["img_trans.gif"]
        });

        // set toggle switch url
        socialSetToggleSwitchStyle();

        // set to collapsed by default
        socialExpandChannel(false);

        // apply code on click of toggle switch
        Ext.select(".ss_portlet_hdr_action_list ul li div img#imgSubOnOff").on("click", function(e, t) {
            // get parent li
            var el = e.target;
            var li = el.offsetParent;

            // check if subscribed/unsubscribed
            if (Ext.get(li).hasClass("subscribed")) {
                // update toggle switch icon
                Ext.get(li).removeClass("subscribed");
                Ext.get(li).addClass("unsubscribed");

                socialSetToggleSwitchStyle();

                // get channel id
                var channelId = li.getAttribute("data-channelId");

                // unsubscribe to channel
                socialUnsubscribeChannel(channelId);
            } else if (Ext.get(li).hasClass("unsubscribed")) {
                // update toggle switch icon
                Ext.get(li).removeClass("unsubscribed");
                Ext.get(li).addClass("subscribed");

                socialSetToggleSwitchStyle();

                // get channel id
                var channelId = li.getAttribute("data-channelId");

                // subscribe to channel
                socialSubscribeChannel(channelId);
            } else {
                // do nothing; dont stop event
                return;
            }

            // prevent other events
            e.stopEvent();
        });
    } catch (e) {
        ssapiHandleError(e);
    }
}

/**
 * 
 * Updates the toggle switch display and font color of the channel menu
 * 
 */
function socialSetToggleSwitchStyle() {
    var bgUrl = "url(" + ssobjGlobal.imagesUrlsObj["on-off.png"] + ") ";

    // set toggle switch url
    Ext.select(".ss_portlet_hdr_action_list ul li.subscribed div img#imgSubOnOff").setStyle("background", bgUrl + " -3px -3px");
    Ext.select(".ss_portlet_hdr_action_list ul li.unsubscribed div img#imgSubOnOff").setStyle("background", bgUrl + " -3px -28px");

    // set font color
    Ext.select(".ss_portlet_hdr_action_list ul li").setStyle("color", "#255599");
    Ext.select(".ss_portlet_hdr_action_list ul li.unsubscribed").setStyle("color", "#666666");
    Ext.select(".ss_portlet_hdr_action_list ul li.active").setStyle("color", "#333333");
}

/**
 * 
 * Expands/Collapses the channels menu
 * 
 * 
 * 
 * @param {Boolean}
 * 
 * bExpand Set to true to expand; false to collapse
 * 
 */

function socialExpandChannel(bExpand) {
    if (bExpand) {
        Ext.select(".ss_portlet_hdr_action_list ul li div img").setStyle("display", "");
        Ext.select(".ss_portlet_hdr_action_list ul li.unsubscribed").setStyle("display", "");

        if (ssapiIsMobile()) {
            Ext.select(".ss_portlet_hdr_action_list ul li div div").setStyle("display", "none");
        } else {
            Ext.select(".ss_portlet_hdr_action_list ul li div div").setStyle("display", "");
        }

        // special codes for IE
        if (Ext.isIE) {
            // set div styles
            Ext.select(".ss_portlet_hdr_action_list ul li div").setStyle("white-space", "nowrap");
            Ext.select(".ss_portlet_hdr_action_list ul li div").setStyle("display", "inline");

            // get longest div
            var maxWidth = 0;
            var elem = null;
            var els = Ext.select(".ss_portlet_hdr_action_list ul li div").elements;

            for (var i = 0; i < els.length; i++) {
                var el = els[i];
                var width = Ext.get(el).getWidth();
                if (width > maxWidth) {
                    maxWidth = width;
                    elem = el;
                }
            }

            // set longest div's display
            if (elem != null)
                Ext.get(elem).setStyle("display", "inline-block");
        }
    } else {
        Ext.select(".ss_portlet_hdr_action_list ul li div img").setStyle("display", "none");
        Ext.select(".ss_portlet_hdr_action_list ul li.unsubscribed").setStyle("display", "none");
        Ext.select(".ss_portlet_hdr_action_list ul li div div").setStyle("display", "none");
    }
}

/**
 * 
 * Appends the channels in the channel selector
 * 
 */

function socialAppendChannelSelectionRows() {

    try {

        var logger = new ssobjLogger(arguments);

        // your channels

        var tmpPost = document.getElementById('tmpChannelSelection').innerHTML;

        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');

        var data = [];

        var writeableChannelIds = ssobjGlobal.writeableChannelIds;

        var yourChannels = ssobjGlobal.yourChannels;

        // logger.log('writeableChannelIds=' + writeableChannelIds);

        data.push({

            internalid : '0',

            channelId : '0',

            channelName : PUBLIC_CHANNEL_NAME,

            className : (!ssobjGlobal.isLoaded) ? 'active' : ''

        });

        for (var i = 0; i < yourChannels.length; i++) {

            if (writeableChannelIds.indexOf(yourChannels[i].channelId) > -1) {

                // except Private Messages

                if (yourChannels[i].channelId == DIRECT_MESSAGES_ID) {

                    continue;

                }

                // display on channels where user is allowed to post

                data.push(ssobjGlobal.yourChannels[i]);

            }

        }

        // append data to list

        myTplPost.overwrite(document.getElementById('sharetolistul'), data, false);

        // apply code on click

        Ext.select("div#sharetolist.ss_share_to_list ul li").on("click", function(e, t) {

            socialToggleChannel(e);

        });

        // set current channel to active

        var channelId = ssobjGlobal.channel;

        if (ssapiHasNoValue(channelId)) {

            channelId = "0";

        }

        // add active class from target channel

        var idSelect = "channelselect-" + channelId;

        if (Ext.get(idSelect))

            Ext.get(idSelect).addClass("active");

        // set name on share dropdown

        var channelName = Ext.select("div#sharetolist ul li.active").elements[0].innerHTML;

        Ext.select("#sharetodropdown div").update(channelName);

        // set width of shareto dropdown

        if (!ssapiIsMobile()) {

            var iWidth = Ext.get("sharetolist").getWidth();

            Ext.get("sharetodropdown").setWidth(iWidth);

        }

        logger.log('END');

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialGetCurrentUserAvatar() {

    try {

        // get current user id

        ssobjGlobal.currentUserId = nlapiGetContext().getUser();

        // get avatar of current user

        ssobjGlobal.currentUserAvatar = '';

        var columns = [ new nlobjSearchColumn('custrecord_suitesocial_profile_image') ];

        var filters = [ new nlobjSearchFilter('custrecord_suitesocial_profile_emp', null, 'is', ssobjGlobal.currentUserId) ];

        var results = nlapiSearchRecord('customrecord_suitesocial_profile', null, filters, columns);

        if (results !== null) {

            var image = results[0].getValue('custrecord_suitesocial_profile_image') || '';

            if (image !== '')

                ssobjGlobal.currentUserAvatar = nlapiResolveURL("mediaitem", image);

        }

        // check if no profile pic, use generic image

        if (ssapiHasNoValue(ssobjGlobal.currentUserAvatar))

            ssobjGlobal.currentUserAvatar = ssobjGlobal.imagesUrlsObj['generic_person.jpg'];

        // set user avatar

        Ext.get('profilepic').dom.src = ssobjGlobal.currentUserAvatar;

        // get current user's name; set name in UI

        ssobjGlobal.currentUserName = nlapiGetContext().getName();

        Ext.get('profilename').update(ssobjGlobal.currentUserName);

        // set edit profile icon

        Ext.get('editprofile').dom.title = "Edit Profile".tl();

        Ext.get('editprofileicon').dom.src = ssobjGlobal.imagesUrlsObj['img_trans.gif'];

        Ext.get('editprofileicon').dom.style.background = 'url("' + ssobjGlobal.imagesUrlsObj['searchnprofile.png'] + '") 0 -30px';

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialLoadNewsFeed() {

    // begin loading news feed posts

    socialUpdateStatusMessage('Loading posts'.tl() + '...');

    // get postId param

    var postId = ssobjGlobal.urlParams["postid"];

    // get hashtag param

    var hashtag = ssobjGlobal.urlParams["hashtag"];

    if (ssapiHasValue(hashtag)) {

        // simulate clicking hashtag

        socialHashtagClicked('#' + hashtag);

    } else {

        // show posts

        socialShowPosts(postId);

        // set translated labels

        // socialSetTranslatedLabels();

    }

}

/**
 * 
 * Displays posts
 * 
 */

function socialShowPosts(postId) {

    try {

        // performance logger for news feed post

        ssobjGlobal.newsFeedLogger = new ssobjLogger("socialShowPosts");

        var logger = new ssobjLogger(arguments);

        if (ssobjGlobal.isNewFeedProcessing) {

            // an earlier getNewsFeed request has not completed yet

            logger.log('an earlier getNewsFeed request has not completed yet');

            return;

        }

        ssobjGlobal.isNewFeedProcessing = true;

        if (!ssobjGlobal.isLoaded) {

            socialUpdateStatusMessage('Loading posts'.tl() + '...');

        }

        if (ssobjGlobal.offset == 0) {

            // need to reset flag on first load like when user chooses another

            // channel or colleague, or when user performs a search

            ssobjGlobal.isPreviewImageLoading = false;

        }

        // clear info/error message under post box if there's any

        Ext.get('processing').update('');

        // show Show More link

        Ext.get('showMore').dom.style.display = 'block';

        Ext.get('showMore').dom.innerHTML = ssobjGlobal.loadingImageMarkupSmall;

        document.getElementById("noMorePosts").style.display = 'none';

        var MAX_PAGE_SIZE = 25;

        if (ssobjGlobal.size > MAX_PAGE_SIZE) {

            ssobjGlobal.size = MAX_PAGE_SIZE;

        }

        // get search string

        var searchString = Ext.get('searchNewsFeed').dom.value.trim();

        // form data to get posts

        var value = {

            "offset" : ssobjGlobal.offset,

            "size" : parseInt(ssobjGlobal.size, 10) + 1,

            "channel" : ssobjGlobal.channel,

            "record_type" : ssobjGlobal.recordType,

            "record_id" : ssobjGlobal.recordId,

            "parent" : null,

            "segmentinfo" : [ 0, 1 ],

            "starttime" : null,

            "startid" : null,

            "colleagueEmpId" : ssobjGlobal.colleagueEmpId,

            "lessThanId" : ssobjGlobal.lessThanId,

            "searchString" : searchString
        };

        // check if getting a single post only

        if (ssapiHasValue(postId)) {

            value.internalIds = postId;

            ssobjGlobal.showSinglePost = true;

        } else {

            ssobjGlobal.showSinglePost = false;

        }

        // call getNewsFeed

        socialSuiteletProcessAsync('getNewsFeed', value, socialShowPostsHandler);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowPostsHandler(data) {

    try {

        var logger = new ssobjLogger(arguments);

        // try {

        // alert(JSON.stringify(data));

        // /////////////

        var resultsCount = data.length;

        var tmpPost = document.getElementById('tmpPost').innerHTML;

        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');

        logger.log('resultsCount=' + resultsCount);

        var rowsToLoop = Math.min(ssobjGlobal.size, resultsCount);

        if (resultsCount > 0) {

            // var intOldestDisplayedPostId = data[rowsToLoop - 1].internalid;

            // set the latest displayed post id

            if (parseInt(data[0].id, 10) > ssobjGlobal.latestDisplayedPostId) {

                ssobjGlobal.latestDisplayedPostId = parseInt(data[0].id, 10);

            }

            ssobjGlobal.lessThanId = data[rowsToLoop - 1].id;

        } else {

            document.getElementById("noMorePosts").style.display = 'block';

            document.getElementById("showMore").style.display = 'none';

        }

        logger.log('get items to be displayed');

        logger.log('rowsToLoop=' + rowsToLoop);

        var itemsToBeDisplayed = [];

        var selectedChannelName = '';

        var selectedChannelId = '';

        var channelElements = Ext.select('#actionchannelslistul .active').elements;

        if (channelElements.length > 0) {

            selectedChannelId = channelElements[0].getAttribute('data-channelId');

            // set channel name on newsfeed title bar

            selectedChannelName = Ext.select('#actionchannelslistul .active div span').elements[0].innerHTML;

            Ext.get("channelhdrlabel").update("Channel:".tl());

            Ext.get("channelname").update(selectedChannelName);

        }

        for (var i = 0; i < rowsToLoop; i++) {

            // alert(JSON.stringify(data[i]));

            itemsToBeDisplayed.push(data[i]);

            var msg = itemsToBeDisplayed[i].message;

            msg = socialAddTags(msg);

            msg = socialAddChannelLink(msg);

            itemsToBeDisplayed[i].message = msg;

            itemsToBeDisplayed[i].image = itemsToBeDisplayed[i].image.replace('/suitebundle/', '/suitebundle26198/');

            itemsToBeDisplayed[i].internalid = itemsToBeDisplayed[i].id;

            // Issue 249551 Defect SuiteSocial>Private Messages channel>mouse

            // over channel text should be "Private Messages" instead of "Direct

            // Messages"


            if (itemsToBeDisplayed[i].channel.length > 0) {
                if (itemsToBeDisplayed[i].channel == selectedChannelName && selectedChannelId == itemsToBeDisplayed[i].channel_id) {
                    itemsToBeDisplayed[i].channel_label = '';
                } else {
                    itemsToBeDisplayed[i].channel_label = ' &rarr; ' + itemsToBeDisplayed[i].channel;
                }
            }

            if (typeof itemsToBeDisplayed[i].recipient == "object" && itemsToBeDisplayed[i].recipient.length > 0) {
                itemsToBeDisplayed[i].recipient = socialGetRecipientsDisplay(itemsToBeDisplayed[i].recipient, itemsToBeDisplayed[i].internalid);
            } else {
                itemsToBeDisplayed[i].recipient = '';
            }


            if (ssapiHasValue(itemsToBeDisplayed[i].orgValues)) {
                itemsToBeDisplayed[i].orgValues = socialGetOrgValuesDisplay(itemsToBeDisplayed[i].orgValues);
            }

            itemsToBeDisplayed[i].currentUserAvatar = ssobjGlobal.currentUserAvatar;

            itemsToBeDisplayed[i].hourglass = ssobjGlobal.imagesUrlsObj['loading-ani.gif'];

            itemsToBeDisplayed[i].fileIcon = ssobjGlobal.imagesUrlsObj['file.png'];

        }

        myTplPost.append(document.getElementById('nfposts'), itemsToBeDisplayed, false);

        for ( var itemsIdx in itemsToBeDisplayed) {

            jQuery('#nfpostmessage' + itemsToBeDisplayed[itemsIdx].internalid).like({

                internalid : itemsToBeDisplayed[itemsIdx].internalid,

                like_label_style : itemsToBeDisplayed[itemsIdx].like_label_style,

                like_icon : itemsToBeDisplayed[itemsIdx].like_icon,

                like_label : itemsToBeDisplayed[itemsIdx].like_label,

                likers_style : itemsToBeDisplayed[itemsIdx].likers_style,

                likers_label : itemsToBeDisplayed[itemsIdx].likers_label,

                likes_info : itemsToBeDisplayed[itemsIdx].likes_info,

                like_type : 'post'

            });

        }

        // set the src of cancel icon for all comment box dnd elements

        Ext.select(".ss_dnd_progress_bar_cancel img, .ss_dnd_thumbnail_cancel img, .ss_dnd_filename_cancel img").set({

            src : ssobjGlobal.imagesUrlsObj['cancel-sml.png']

        });

        // display charts

        logger.log('display charts');

        for (i = 0; i < itemsToBeDisplayed.length; i++) {

            var feed = itemsToBeDisplayed[i];

            var chartId = 'socialChart' + feed.id;

            if (ssapiHasValue(feed.chartdata)) {

                // EMS: socialShowChart(feed.chartdata, chartId, 'Chart',

                // false);

            }

        }

        // no more to display?

        if (data.length <= rowsToLoop) {

            document.getElementById("noMorePosts").style.display = 'block';

            document.getElementById("showMore").style.display = 'none';

        } else {

            document.getElementById("noMorePosts").style.display = 'none';

            document.getElementById("showMore").style.display = 'block';

            document.getElementById("showMore").innerHTML = 'More'.tl();

        }

        // Ext.Msg.show('Retrieving replies...', 'Processing');

        for (i = 0; i < rowsToLoop; i++) {

            // enable dnd on comment boxes

            var internalId = itemsToBeDisplayed[i].internalid;

            var dndElements = {};

            dndElements.targetId = "nfpostcommenteditor" + internalId;

            dndElements.targetWatermarkId = "nfpostcommenteditorWatermark" + internalId;

            dndElements.progressBarContainerId = "replydndprogressbarcontainer" + internalId;

            dndElements.progressBarId = "replydndprogressbar" + internalId;

            dndElements.progressBarStatusId = "replydndprogressbarstatus" + internalId;

            dndElements.thumbnailContainerId = "replydndthumbnailcontainer" + internalId;

            dndElements.thumbnailImageId = "replydndthumbnailimage" + internalId;

            dndElements.filenameContainerId = "replydndfilenamecontainer" + internalId;

            dndElements.filenameId = "replydndfilename" + internalId;

            dndElements.canvasId = "replydndcanvas" + internalId;

            dndElements.dataId = "replydnddata" + internalId;

            dndElements.buttonId = "btnreplycontainer" + internalId;

            ssapiEnableDnDOnSuiteSocial(true, dndElements, socialFileUploadHandler);

            // check if post has replies

            if (ssapiHasNoValue(itemsToBeDisplayed[i].comments)) {

                continue;

            }

            var parentId = itemsToBeDisplayed[i].id;

            var template = '<tpl for=".">' + document.getElementById('tmpreply').innerHTML + '</tpl>';

            template = template.replace(/{internalid}/gi, '{id}');

            var myTplReply = new Ext.XTemplate(template);

            var replies = data[i].comments;

            myTplReply.append(document.getElementById('nfpostcommentslist' + parentId), replies);

            // Append like to reply

            for ( var replyIdx in replies) {

                jQuery('#nfpostcommentmsg' + replies[replyIdx].internalid).like({

                    internalid : replies[replyIdx].internalid,

                    like_label_style : replies[replyIdx].like_label_style,

                    like_icon : replies[replyIdx].like_icon,

                    like_label : replies[replyIdx].like_label,

                    likers_style : replies[replyIdx].likers_style,

                    likers_label : replies[replyIdx].likers_label,

                    likes_info : replies[replyIdx].likes_info,

                    like_type : 'comment'

                });

            }

            if (Ext.select('.replyBatch2.parent' + parentId).elements.length > 0) {

                // show showMoreReplies link only when there is more than 1

                // batch/page

                Ext.get('nfpostshowmorecommentcontainer' + parentId).fadeIn(FADEIN_SPEED);

            }

        }

        // if showing single post, get channel of post, and later set channel to

        // be selected in the UI

        ssobjGlobal.singlePostChannelId = ssobjGlobal.showSinglePost ? (itemsToBeDisplayed.length > 0 ? (itemsToBeDisplayed[0].channel_id || '0') : null) : null;

        // apply rich editor on key up functionality

        Ext.select('#nfposts .newpost .replyEditor').on('keyup', function(event) {

            // show/hide watermark

            if (ssapiIsIE()) {

                onRichEditorChange(event.target);

            }

            var logger = new ssobjLogger(arguments);

            logger.log('replyEditor keyup');

            var e = event || window.event;

            onRichEditorKeyup(e);

        });

        Ext.select('#nfposts .newpost .replyEditor').on('input', function(e) {

            onRichEditorChange(e.target);

        });

        // EMS: socialSetReplyOverAndOutEvents();

        ssobjGlobal.offset = ssobjGlobal.offset + rowsToLoop;

        Ext.select('.newreply.replyBatch1').fadeIn(FADEIN_SPEED);

        Ext.select('.newpost').fadeIn(FADEIN_SPEED);

        if (!Ext.isIE) {

            Ext.select('.newpost').animate({

                backgroundColor : {

                    from : '#EEEEBB',

                    to : '#ffffff'

                }

            }, 2.0, null, 'easeOut', 'color');

            Ext.select('.newreply.replyBatch1').animate({

                backgroundColor : {

                    from : '#EEEEBB',

                    to : 'white'

                }

            }, 2.0, null, 'easeOut', 'color');

        } else {

            Ext.select('.newpost').setStyle('backgroundColor', '#ffffff');

        }

        Ext.select('#nfposts .newreply.replyBatch1').removeClass('newreply');

        Ext.select('#nfposts .newpost').removeClass('newpost');

        // EMS: socialSetPostOverAndOutEvents();

        // EMS: socialShowPhotos();

        if (!ssobjGlobal.isLoaded) {

            ssobjGlobal.isLoaded = true;

            socialShowStatusMessage(false);

            // perform one time loads here

            socialPerformFirstLoad();

        }

        // Issue: 247711 [SuiteSocial] The employee name in record changes posts

        // shou

        // this will make the 'System' user in displayed posts unclickable and

        // dimgary

        // EMS: may not need already

        /*
         * 
         * var selector = '.profilename[data-channel-id=14]';
         * 
         * Ext.select(selector).setStyle('color', 'dimgray').setStyle('cursor',
         * 
         * 'text').set({ onclick : 'return false;' });
         * 
         * Ext.select(selector).setStyle('cursor', 'text').set({ onclick :
         * 
         * 'return false;' });
         * 
         */

        // this is used in loading preview images sequentially xxx
        jQuery('img[data-processed="false"]').on('load', function() {

            // run script when the image is completely loaded

            logger.log('loaded, calling socialLoadImagesSequentially()');

            // uiShowInfo('load');

            ssobjGlobal.isPreviewImageLoading = false;

            socialLoadImagesSequentially();

        });

        socialLoadImagesSequentially();

        // EMS: socialMakeCurrentUserLinkUnclickable();

        ssobjGlobal.isNewFeedProcessing = false;

        // log end time

        ssobjGlobal.logger.log("*********************************** END: Loading News Feed Posts ***********************************");

        ssobjGlobal.newsFeedLogger.log("*********************************** END: Loading News Feed Posts ***********************************");

        // set news feed posts has loaded

        ssobjGlobal.isNewsFeedPostsLoaded = true;

        // check if page has fully loaded

        if (ssobjGlobal.isNewsFeedPostsLoaded && ssobjGlobal.isFollowingColleaguesLoaded && ssobjGlobal.isFollowersColleaguesLoaded && ssobjGlobal.isChannelsLoaded) {

            ssobjGlobal.isLoaded = true;

            socialShowStatusMessage(false);

        } else {

            if (!ssobjGlobal.isFollowingColleaguesLoaded || !ssobjGlobal.isFollowersColleaguesLoaded)

                socialUpdateStatusMessage('Loading colleagues'.tl() + '...');

            else if (!ssobjGlobal.isChannelsLoaded)

                socialUpdateStatusMessage('Loading channels'.tl() + '...');

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialGetRecipientsDisplay(recipientList, postId) {
    if (ssapiHasNoValue(recipientList)) {
        return '';
    }

    if (ssobjGlobal.recipientList == undefined) {
        ssobjGlobal.recipientList = {};
    }
    ssobjGlobal.recipientList[postId] = recipientList;

    // if only one recipient
    var recipientHtml = '';
    var noOfRecipients = recipientList.length;
    if (noOfRecipients == 1) {
        recipientHtml = ' to ' + recipientList[0].name;
        return recipientHtml;
    }

    recipientHtml = ' to ' + '<a href="#" onclick="socialShowRecipientList(\'' + postId + '\')">' + recipientList[0].name + ' and ' + (noOfRecipients - 1) + ' other';
    if (noOfRecipients > 2) {
        // set other word to plural form
        recipientHtml += 's';
    }
    recipientHtml += '</a>';

    return recipientHtml;

}

function socialShowRecipientList(postId) {
    var recipientList = ssobjGlobal.recipientList[postId];
    var logger = new ssobjLogger(arguments);

    if (ssapiHasNoValue(recipientList)) {
        logger.log("Error : Recipient List is empty");
        return;
    }

    var html = Ext.get('tmpRecipientList').dom.innerHTML.replace(/{internalid}/g, 'PrivateMessageRecipients');
    var recipientHtml = '';
    var recipientHtmlTemplate = Ext.get('tmpRecipient').dom.innerHTML;

    for (var i = 0; i < recipientList.length; i++) {
        recipientHtml += recipientHtmlTemplate.replace(/{internalid}/g, recipientList[i].id).replace(/{recipientEmpId}/g, recipientList[i].id).replace(/{recipientImageUrl}/g, recipientList[i].imageUrl).replace(/{recipientName}/g, recipientList[i].name);
    }

    html = html.replace(/{recipientList}/g, recipientHtml);

    ssobjGlobal.recipientListWin = new Ext.Window({

        title : 'Message Recipients'.tl(),

        width : '300px',

        autoHeight : true,

        resizable : false,

        modal : true,

        html : html
    }).show();

    jQuery('#recipientListContainerPrivateMessageRecipients').parents('.x-window-mc').css('padding', '10px').css('padding-right', '0px');
    jQuery('.x-panel-nofooter').css('display', 'none');
    jQuery('.x-shadow').css('display', 'none');

    var defaultMaxHeight = '264px';
    var maxHeight = jQuery('#recipientListContainerPrivateMessageRecipients .ss_recipient_list ul').attr('max-height') || defaultMaxHeight;
    maxHeight = maxHeight.substring(0, maxHeight.indexOf('px'));

    var minHeight = 108;
    var listHeight = jQuery('#recipientListContainerPrivateMessageRecipients .ss_recipient_list ul').height() || minHeight;
    if (maxHeight - listHeight > 0) {
        jQuery('#recipientListContainerPrivateMessageRecipients .ss_recipient_list ul').css('overflow-y', 'hidden');
    }

}

function socialGetOrgValuesDisplay(orgValues) {
    if (orgValues == undefined || orgValues.length == 0) {
        return '';
    }

    var orgValuesHtmlTemplate = Ext.get('tmpOrgValues').dom.innerHTML;
    var orgValueHtml = '<div id="orgValueContainer" class="social-org-value-container">';

    for (var i = 0; i < orgValues.length; i++) {

        orgValueHtml += orgValuesHtmlTemplate.replace(/{orgValueImageUrl}/g, orgValues[i].imageUrl).replace(/{orgValueName}/g, orgValues[i].name);

    }
    orgValueHtml += '</div>';

    return orgValueHtml;
}

function socialPerformFirstLoad() {

    try {

        // hide netsuite menu if inside iframe

        if (top !== self) {

            // inside iframe

            Ext.select('#div__header').remove();

            // Ext.get('viewOnItsOwnPage').dom.style.display = '';

            /*
             * 
             * Ext.get('viewOnItsOwnPage').on('click', function(e) {
             * 
             * window.top.location = window.location; return false; });
             * 
             */

        }

        // tooltip

        new Ext.ToolTip({

            target : 'ssHelpIcon',

            html : 'Show help about this page.'.tl()

        });

        // EMS: show/adjust editor container here if on a record page

        // if (ssapiHasValue(ssobjGlobal.recordType))

        // socialAdjustEditorContainer();

        window.onscroll = function(event) {

            // set position of visual arrow

            socialSetVisualIndicatorPosition();

            // automatically load more posts if the More Post becomes within

            // view

            socialLoadMorePosts();

        };

        socialSuiteletProcessAsync('getProfileEditUrl', 'anyvalue', function(profileEditUrl) {

            document.getElementById('editprofile').setAttribute('href', profileEditUrl);

        });

        Ext.get('btnsharedefault').on('click', function(e) {

            socialShareToChannel(e);

        });

        Ext.get('searchColleaguesTextBox').on('keyup', function(e) {

            onSearchColleaguesTextBoxKeyup(e);

        });

        Ext.get('searchColleaguesTextBox').on('focus', function(e) {

            Ext.get('placeHolderColleagueOthersContainer').show();

            if (Ext.get('placeHolderColleagueOthers').dom.children.length === 0) {

                Ext.get('placeHolderColleagueOthers').dom.innerHTML = 'Search using global search syntax. You can omit the "emp:" record type prefix.'.tl();

            }

        });

        // news feed search

        var searchNewsFeedX = Ext.get('searchNewsFeed');

        searchNewsFeedX.on('keyup', function(e) {

            onSearchNewsFeedKeyup(e);

        });

        /*
         * 
         * Ext.get('selectDropDownListRich').on('keyup', function(e) {
         * 
         * onSelectDropDownListRichKeyup(e); });
         * 
         */

        Ext.get('richEditor').on('keypress', function(e) {

            var logger = new ssobjLogger(arguments);

            logger.log('richEditor keypress');

            onRichEditorKeypress(e);

        });

        Ext.select('.replyEditor').on('keypress', function(e) {

            var logger = new ssobjLogger(arguments);

            logger.log('richEditor keypress');

            onRichEditorKeypress(e);

        });

        Ext.get('richEditor').on('keyup', function(event) {

            // show/hide watermark

            if (ssapiIsIE()) {

                onRichEditorChange(Ext.get("richEditor").dom);

            }

            var logger = new ssobjLogger(arguments);

            logger.log('richEditor keyup');

            var e = event || window.event;

            onRichEditorKeyup(e);

        });

        // we need to set padding but this will increase the width,

        // so we decrease it by the same number of pixel after adding the

        // padding

        /*
         * 
         * var x = Ext.get('richEditor'); x.setStyle('width', '100%');
         * 
         * x.setStyle('padding', '5'); x.setWidth(x.getWidth() - 10);
         * 
         */

        // ui adjustment code for special browser ie
        /*
         * 
         * if (Ext.isIE) { Ext.select('.postouter').setStyle('width', '100%');
         * 
         * Ext.get('richEditor').setStyle('width', '100%');
         * 
         * Ext.select('.replyContainer').setStyle('display', 'none');
         * 
         * Ext.get('channel-0').setStyle('backgroundImage', 'url("' +
         * 
         * ssobjGlobal.imagesUrlsObj['channel-background-ie.png'] + '")');
         * 
         * Ext.get('placeHolderFollowers').dom.style.width = '98%';
         * 
         * Ext.get('placeHolderFollowing').dom.style.width = '98%';
         * 
         * Ext.get('placeHolderColleagueOthers').dom.style.width = '98%'; }
         * 
         */

        // EMS: may not need already
        // socialPositionPatcher();
        //
        // Ext.select('.ss_colleaguestatus').setStyle('width', '210');
        if (ssapiHasNoValue(ssobjGlobal.recordType)) {

            socialSetEditorLookAndChannel(null /* Ext.get('channel-0').dom */, ssobjGlobal.singlePostChannelId);

        } else {

            // set portlet title

            var channelName = "Current Record".tl();

            Ext.get("channelhdrlabel").update("Channel:".tl());

            Ext.get('channelname').update(channelName);

            // for some reason, safari does not show this label on first load

            if (Ext.isSafari) {

                setTimeout(function() {

                    // for some reason, it requires an update of other text

                    Ext.get('channelname').update(" ");

                    Ext.get('channelname').update(channelName);

                }, 500);

            }

            // hide share to dropdown

            Ext.get("sharetolabel").dom.style.display = 'none';

            Ext.get("sharetocontainer").dom.style.display = 'none';

        }

        /*
         * 
         * var HEIGHT_OF_GRADIENT = 800; if (Ext.get('maintable').getHeight() <
         * 
         * HEIGHT_OF_GRADIENT) {
         * 
         * Ext.get('maintable').setHeight(HEIGHT_OF_GRADIENT); }
         * 
         */

        // EMS: socialShowSubscribersOfRecord();
        // Issue: 248091 [SuiteSocial] News Feed UI: Fix various UI errors
        // prevent default enter/submit on textboxes
        if (Ext.isIE) {

            Ext.get('main_form').on('submit', function(e) {

                if (e.preventDefault) {

                    // for IE

                    e.preventDefault();

                }

                return false;

            });

        } else {

            Ext.get('main_form').dom.setAttribute('onsubmit', 'return false;');

        }

        socialOnResizeWindow();

        // load roles
        socialSuiteletProcessAsync('ssapiRetrieveAllRoles', {}, function(res) {
            ssobjGlobal.roles = JSON.parse(decodeEntities(JSON.stringify(res)));
        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Retrieves and displays more comments
 * 
 * 
 * 
 * @param {Object}
 * 
 * parentId
 * 
 */

function socialShowMoreComments(parentId) {

    try {

        var elements = Ext.select('.parent' + parentId).elements;

        for (var i = elements.length - 1; i >= 0; i--) {

            var el = elements[i];

            // if (el.style.display == 'none') {

            if (el.style.visibility != 'visible') {

                var batch = el.getAttribute('data-batch');

                var selector = '.parent' + parentId + '.replyBatch' + batch;

                Ext.select(selector).fadeIn(FADEIN_SPEED);

                // EMS: Ext.select(selector).removeClass('newreply');

                Ext.select(selector).animate({// animation control object

                    backgroundColor : {

                        from : '#EEEEBB',

                        to : '#ffffff'

                    }

                }, 2.0, // animation duration

                null, // callback

                'easeOut', // easing method

                'color' // animation type ('run','color','motion','scroll')}

                );

                // get the last hidden batch

                var lastBatch = elements[0].getAttribute('data-batch');

                if (lastBatch == batch) {

                    Ext.get('nfpostshowmorecommentcontainer' + parentId).fadeOut(FADEIN_SPEED);

                    Ext.get('nfpostshowmorecommentcontainer' + parentId).dom.style.display = 'none';

                }

                return;

            }

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialLoadColleagues() {

    // begin loading colleagues

    if (ssapiHasNoValue(ssobjGlobal.recordType)) {

        // update status message

        socialUpdateStatusMessage('Loading colleagues'.tl() + '...');

        // performance logger for following colleagues

        ssobjGlobal.followingLogger = new ssobjLogger("socialShowFollowing");

        // performance logger for followers colleagues

        ssobjGlobal.followersLogger = new ssobjLogger("socialShowFollowers");

        // load following

        socialSuiteletProcessAsync('getFollowingList', false, socialShowFollowingHandler);

        // load followers

        socialSuiteletProcessAsync('getFollowersList', false, socialShowFollowersHandler);

    }

}

/**
 * 
 * Displays user's colleagues
 * 
 * 
 * 
 * @param {Object}
 * 
 * callback
 * 
 */

function socialShowFollowing(callback) {

    try {

        // performance logger for following colleagues

        ssobjGlobal.followingLogger = new ssobjLogger("socialShowFollowing");

        if (!ssobjGlobal.isLoaded && !ssobjGlobal.recordType) {

            socialUpdateStatusMessage('Loading colleagues'.tl() + '...');

            socialShowFollowingHandler(ssobjGlobal.followingList, callback);

        } else {

            ssobjGlobal.followingEmpIds = [];

            ssobjGlobal.colleagueEmpId = null;

            socialSuiteletProcessAsync('getFollowingList', false, function(colleagues) {

                socialShowFollowingHandler(colleagues, callback);

            });

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowFollowingHandler(colleagues, callback) {

    try {

        for (var i = 0; i < colleagues.length; i++) {

            colleagues[i].statusUpdate = colleagues[i].statusUpdate + '&nbsp;';

            colleagues[i].from = 'following';

            ssobjGlobal.colleagues[colleagues[i].colleagueEmpId] = colleagues[i];

        }

        var tmpPost = document.getElementById('tmpColleague').innerHTML;

        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');

        myTplPost.overwrite(document.getElementById('colleaguefollowing'), colleagues, false);

        // update count

        Ext.get("tabfollowing").update("Following".tl() + " (" + colleagues.length + ")");

        // EMS: socialShowPhotos();

        socialHideAllTooltips();

        if (callback) {

            callback();

        }

        // log end time

        ssobjGlobal.logger.log("*********************************** END: Loading Following ***********************************");

        ssobjGlobal.followingLogger.log("*********************************** END: Loading Following ***********************************");

        // set following colleagues has loaded

        ssobjGlobal.isFollowingColleaguesLoaded = true;

        // check if page has fully loaded

        if (ssobjGlobal.isNewsFeedPostsLoaded && ssobjGlobal.isFollowingColleaguesLoaded && ssobjGlobal.isFollowersColleaguesLoaded && ssobjGlobal.isChannelsLoaded) {

            ssobjGlobal.isLoaded = true;

            socialShowStatusMessage(false);

        } else {

            if (!ssobjGlobal.isNewsFeedPostsLoaded)

                socialUpdateStatusMessage('Loading posts'.tl() + '...');

            else if (!ssobjGlobal.isFollowersColleaguesLoaded)

                socialUpdateStatusMessage('Loading colleagues'.tl() + '...');

            else if (!ssobjGlobal.isChannelsLoaded)

                socialUpdateStatusMessage('Loading channels'.tl() + '...');

        }

        // check if colleagues are loaded, then display

        // socialDisplayColleagues();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays the user's followers
 * 
 */

function socialShowFollowers() {

    try {

        // performance logger for followers colleagues

        ssobjGlobal.followersLogger = new ssobjLogger("socialShowFollowers");

        if (!ssobjGlobal.isLoaded && !ssobjGlobal.recordType) {

            socialUpdateStatusMessage('Loading colleagues'.tl() + '...');

            socialShowFollowersHandler(ssobjGlobal.followersList);

        } else {

            socialSuiteletProcessAsync('getFollowersList', false, socialShowFollowersHandler);

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowFollowersHandler(colleagues) {

    try {

        for (var i = 0; i < colleagues.length; i++) {

            colleagues[i].from = 'followers';

            ssobjGlobal.colleagues[colleagues[i].colleagueEmpId] = colleagues[i];

        }

        socialTemplateAppend('tmpColleague', 'colleaguefollowers', colleagues);

        // update count

        Ext.get("tabfollowers").update("Followers".tl() + " (" + colleagues.length + ")");

        /*
         * 
         * var ElId = null; if (ssobjGlobal.colleague !== null) { ElId =
         * 
         * 'colleague-' + ssobjGlobal.colleague; var exjsEl = Ext.get(ElId); if
         * 
         * (ssobjGlobal.isLoaded) { socialShowPostsByColleague(exjsEl.dom,
         * 
         * ssobjGlobal.colleague); } }
         * 
         */

        // EMS: socialShowPhotos();
        // log end time
        ssobjGlobal.logger.log("*********************************** END: Loading Followers ***********************************");

        ssobjGlobal.followersLogger.log("*********************************** END: Loading Followers ***********************************");

        // set followers colleagues has loaded

        ssobjGlobal.isFollowersColleaguesLoaded = true;

        // check if page has fully loaded

        if (ssobjGlobal.isNewsFeedPostsLoaded && ssobjGlobal.isFollowingColleaguesLoaded && ssobjGlobal.isFollowersColleaguesLoaded && ssobjGlobal.isChannelsLoaded) {

            ssobjGlobal.isLoaded = true;

            socialShowStatusMessage(false);

        } else {

            if (!ssobjGlobal.isNewsFeedPostsLoaded)

                socialUpdateStatusMessage('Loading posts'.tl() + '...');

            else if (!ssobjGlobal.isFollowingColleaguesLoaded)

                socialUpdateStatusMessage('Loading colleagues'.tl() + '...');

            else if (!ssobjGlobal.isChannelsLoaded)

                socialUpdateStatusMessage('Loading channels'.tl() + '...');

        }

        // check if colleagues are loaded, then display

        // socialDisplayColleagues();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/*
 * 
 * function socialDisplayColleagues() { // check if colleagues are loaded, then
 * 
 * display if (ssobjGlobal.isFollowingColleaguesLoaded &&
 * 
 * ssobjGlobal.isFollowersColleaguesLoaded &&
 * 
 * !ssobjGlobal.isColleaguesDisplayed) { ssobjGlobal.isColleaguesDisplayed =
 * 
 * true; // set tile view of colleagues if set on preferences if
 * 
 * (ssobjGlobal.newsFeedSettings.showColleagueAsTiled == 'T')
 * 
 * ssapiTriggerEvent('socialTileColleagues', 'click'); // show following
 * 
 * colleagues Ext.get('placeHolderFollowing').dom.style.display = ''; //
 * 
 * collapse following if set on preferences if
 * 
 * (ssobjGlobal.newsFeedSettings.collapseFollowing == 'T')
 * 
 * ssapiTriggerEvent('socialFollowingHeader', 'click'); // show followers
 * 
 * colleagues Ext.get('placeHolderFollowers').dom.style.display = ''; //
 * 
 * collapse followers if set on preferences if
 * 
 * (ssobjGlobal.newsFeedSettings.collapseFollowers == 'T')
 * 
 * ssapiTriggerEvent('socialFollowersHeader', 'click'); } }
 * 
 */

/**
 * 
 * Displays links/actions for a colleague in a custom quickview
 * 
 * 
 * 
 * @param {Object}
 * 
 * event
 * 
 * @param {Object}
 * 
 * el
 * 
 * @param {Object}
 * 
 * id
 * 
 * @param {Object}
 * 
 * name
 * 
 * @param {Object}
 * 
 * tooltipTarget
 * 
 * @param {Object}
 * 
 * from
 * 
 */

function socialShowColleagueActions(event, el, id, name, tooltipTarget, from) {

    try {

        var logger = new ssobjLogger(arguments);

        var e = event || window.event;

        // dont show popup menu for current user and system messages

        if (id == nlapiGetUser() || id == "-4" || name == "System".tl())

            return false;

        var param = {};

        param.recordTypeScriptId = 'entity';

        param.recordId = id;

        socialSuiteletProcessAsync('isInactive', param, function(isInactive) {

            logger.log('isInactive=' + isInactive);

            if (ssapiHasNoValue(name)) {

                name = e.target.getAttribute('data-name');

            }

            // check if on mobile or not

            if (ssapiIsMobile()) {

                // prepare html content of colleague popup

                var html = Ext.get('tmpColleaguePopup').dom.innerHTML.replace(/{internalid}/g, id);

                html = html.replace(/{name}/g, name);

                // send message

                html = html.replace(/{label_message}/g, 'Send message'.tl());

                html = html.replace(/{image_msg}/g, ssobjGlobal.TOOLTIP_MESSAGE_ICON);

                var displayStyleMsg = (ssobjGlobal.recordType === null && isInactive == 'F') ? "" : "display: none";

                html = html.replace(/{msg_display}/g, displayStyleMsg);

                // show posts

                html = html.replace(/{label_posts}/g, 'Show posts'.tl());

                html = html.replace(/{image_posts}/g, ssobjGlobal.TOOLTIP_POSTS_ICON);

                html = html.replace(/{from}/g, from);

                var displayStylePosts = ssobjGlobal.recordType === null ? "" : "display: none";

                html = html.replace(/{posts_display}/g, displayStylePosts);

                // follow/unfollow

                var isBeingFollowed = Ext.select('#colleaguefollowing li[data-colleagueEmpId=' + id + ']').elements.length > 0;

                var onclick = "Ext.get(this).dom.innerHTML = Ext.get(this).dom.innerHTML + ssobjGlobal.loadingImageLiteGrayMarkupXSmall; ";

                var followLabel = "";

                var followImage = "";

                if (isBeingFollowed) {

                    onclick += "return socialUnsubscribeFollowing(" + id + ");";

                    followLabel = 'Unfollow'.tl();

                    followImage = ssobjGlobal.TOOLTIP_UNFOLLOW_ICON;

                } else {

                    onclick += "return socialSubscribeFollowing(" + id + ");";

                    followLabel = 'Follow'.tl();

                    followImage = ssobjGlobal.TOOLTIP_FOLLOW_ICON;

                }

                html = html.replace(/{follow_onclick}/g, onclick);

                html = html.replace(/{label_follow}/g, followLabel);

                html = html.replace(/{image_follow}/g, followImage);

                var displayStyleFollow = isInactive == 'F' ? "" : "display: none";

                html = html.replace(/{follow_display}/g, displayStyleFollow);

                // cancel

                html = html.replace(/{cancel_onclick}/g, "ssobjGlobal.collWin.close();");

                html = html.replace(/{label_cancel}/g, 'Cancel'.tl());

                // initialize Ext.Window

                var iWidth = 304;

                ssobjGlobal.collWin = new Ext.Window({

                    width : iWidth,

                    modal : true,

                    html : html,

                    header : false

                });

                // show window

                ssobjGlobal.collWin.show(this);

                // sync width of inner body (for some reason, it's having a 40px

                // margin)

                Ext.select(".x-window-body").setStyle("width", iWidth + "px");

                // remove default padding

                Ext.select(".x-window-mc").setStyle("padding", "0px");

                // remove header

                Ext.select(".x-window-tl").setStyle("display", "none");

                // remove extra line at the bottom

                Ext.select(".x-shadow div.xsb").setStyle("display", "none");

                Ext.select("div .x-window-bl.x-panel-nofooter").setStyle("display", "none");

            } else {

                // prepare html content of colleague balloon

                var html = '<b class="ss_colleague_bubble_name">' + name + '</b>';

                html += '<br />';

                // title and department

                var titleAndDepartment = el.getAttribute('data-titleAndDepartment');

                if (ssapiHasValue(titleAndDepartment)) {

                    html += titleAndDepartment || '';

                    html += '<br />';

                }

                // status

                var status = el.getAttribute('data-statusUpdate');

                if (ssapiHasValue(status)) {

                    html += 'Status'.tl() + ': ' + status + '<br />';

                }

                // html += '<div style="border-top: 1px solid silver;

                // margin-top:

                // 10px"></div>';

                var spacer = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                if (ssobjGlobal.recordType === null) {

                    // do not show these if in the tab

                    if (isInactive == 'F') {

                        // Send message

                        html += '<a title="' + 'Send this colleague a private message'.tl() + '" class="dottedlink" href=# onclick="return socialShowPrivateMessageBox(' + id + ', \'' + name + '\');" style="border-bottom: none;"><div class="' + ssobjGlobal.clsTooltipLink + '">';

                        html += '<img src="' + ssobjGlobal.TOOLTIP_MESSAGE_ICON + '" />';

                        html += '<div>' + 'Message'.tl() + '</div>';

                        html += '</div></a>';

                        html += spacer;

                        if (isKudosIntegrationEnabled() && ssapiGetRole() === 'Employee' && ssapiGetRole(id) === 'Employee') {
                            html += '<a title="' + 'Give this colleague a kudos recognition'.tl() + '" class="dottedlink" href=# onclick="return socialShowKudosPopup(\'' + id + '\', \'' + name + '\');" style="border-bottom: none;"><div class="' + ssobjGlobal.clsTooltipLink + '">';

                            html += '<img src="' + ssobjGlobal.TOOLTIP_KUDOS_ICON + '" />';

                            html += '<div>' + 'Give Kudos'.tl() + '</div>';

                            html += '</div></a>';

                            html += spacer;
                        }

                    }

                    // Show previous posts

                    html += '<a target="_self" title="' + 'View previous posts this colleague has created, including private messages sent to you'.tl() + '" class="dottedlink" href="#" onclick="socialShowColleagueMessages(' + id + ', \'' + name + '\', \'' + from + '\'); return false;" style="border-bottom: none;"><div class="' + ssobjGlobal.clsTooltipLink + '">';

                    html += '<img src="' + ssobjGlobal.TOOLTIP_POSTS_ICON + '" />';

                    html += '<div>' + 'Show posts'.tl() + '</div>';

                    html += '</div></a>';

                    html += spacer;

                }

                if (isInactive == 'F' && ssapiGetRole(id) === 'Employee' && ssapiGetRole() === 'Employee') {
                    html += ' <a id="followOrUnfollow" class="dottedlink" href=# onclick="Ext.get(this).dom.innerHTML = Ext.get(this).dom.innerHTML + ssobjGlobal.loadingImageLiteGrayMarkupXSmall; socialStopPropagation(event || window.event); ';

                    // check if the employee is being followed

                    var isBeingFollowed = Ext.select('#colleaguefollowing li[data-colleagueEmpId=' + id + ']').elements.length > 0;

                    if (isBeingFollowed) {

                        html += 'return socialUnsubscribeFollowing(' + id + ');" title="' + 'Unfollow colleague'.tl() + ' ' + name + '" style="border-bottom: none;"><div class="' + ssobjGlobal.clsTooltipLink + '">';

                        html += '<img src="' + ssobjGlobal.TOOLTIP_UNFOLLOW_ICON + '" />';

                        html += '<div>' + 'Unfollow'.tl() + '</div>';

                    } else {

                        html += 'return socialSubscribeFollowing(' + id + ');" title="' + 'Follow colleague'.tl() + ' ' + name + '" style="border-bottom: none;"><div class="' + ssobjGlobal.clsTooltipLink + '">';

                        html += '<img src="' + ssobjGlobal.TOOLTIP_FOLLOW_ICON + '" />';

                        html += '<div>' + 'Follow'.tl() + '</div>';

                    }

                    html += '</div></a>';

                }

                var tooltipId = "labsTutorialTooltip";

                // if (ssapiHasNoValue(tooltipTarget)) {

                // tooltipId = e.target.id || (new Date()).getTime() +

                // 'Tooltip';

                // tooltipTarget = e.target;

                // }

                logger.log('tooltipId=' + tooltipId);

                if (typeof tooltipTarget == 'string') {

                    tooltipTarget = Ext.get(tooltipTarget).dom;

                }

                logger.log('tooltipTarget=' + tooltipTarget);

                var isAutoWidth = true;

                var toolTipWidth = 'auto';

                var anchorDirection = 'left';

                if (Ext.isIE) {

                    isAutoWidth = false;

                    toolTipWidth = 350;

                }

                if (socialIsResponsiveMode()) {

                    anchorDirection = 'top';

                    isAutoWidth = false;

                    toolTipWidth = Ext.get("maincontainer").getWidth() - 75;

                    if (toolTipWidth >= 390)

                        toolTipWidth = 390;

                }

                logger.log('isAutoWidth=' + isAutoWidth);

                logger.log('toolTipWidth=' + toolTipWidth);

                var tooltip = (new Ext.ToolTip({

                    title : '',

                    id : tooltipId,

                    target : tooltipTarget,

                    anchor : anchorDirection,

                    html : html,

                    autoWidth : isAutoWidth,

                    width : toolTipWidth,

                    autoHide : false,

                    closable : false,

                    autoShow : false,

                    hideDelay : 10000,

                    dismissDelay : 0,

                    anchorOffset : 0,

                    listeners : {

                        'hide' : function() {

                            this.destroy();

                        },

                        'renderX' : function() {

                            this.header.on('click', function(e) {

                                e.stopEvent();

                                Ext.Msg.alert('Link', 'Link to something interesting.');

                            }, this, {

                                delegate : 'a'

                            });

                        }

                    }

                }));

                // hide other tooltips

                socialHideAllTooltips();

                // now show

                tooltip.show();

                if (socialIsResponsiveMode())

                    Ext.select(".x-tip-body").setStyle("width", (toolTipWidth - 2) + "px");

                socialApplyCSSToTooltip();

                // scroll tooltip with target element on colleagues list

                socialScrollTooltipWithTarget(tooltip, from);

                // logger.log(tooltipId);

                // ssobjGlobal.tooltips[tooltipId] = tooltip;

                ssobjGlobal.lastTooltip = tooltip;

                socialStopPropagation(e);

                return false;

            }

        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialApplyCSSToTooltip() {
    // apply tooltip help icon
    if (ssapiHasValue(Ext.select('.ssImgTooltipHelp'))) {
        Ext.select('.ssImgTooltipHelp').set({
            'src' : ssobjGlobal.TOOLTIP_HELP_ICON,
            'style' : {
                'vertical-align' : 'middle'
            }
        });
    }

    socialAdjustTooltipAnchor();

    // if likers list box is shown, use its z-index
    var zIndex = ssapiHasValue(Ext.get('likersListBox')) ? Ext.get('likersListBox').getStyle('z-index') : 5;

    // if imgPopup is visible, use its zindex + 1, else use zIndex
    var imgPopupZindex = ssapiHasValue(Ext.get('imageGalleryPopupBox')) ? Number(Ext.get('imageGalleryPopupBox').getStyle('z-index')) + 1 : zIndex;

    // lower down the z-index of tooltip
    Ext.select(".x-tip").setStyle("z-index", imgPopupZindex);
    Ext.select(".x-shadow").setStyle("display", "none");
    Ext.select(".x-ie-shadow").setStyle("display", "none"); // for IE
    Ext.select(".ext-shim").setStyle("z-index", imgPopupZindex); // for IE
}

function socialAdjustTooltipAnchor() {

    var logger = new ssobjLogger(arguments);

    // fix bottom anchor top

    var els = jQuery(".x-tip-anchor.x-tip-anchor-bottom");

    if (els.length) {

        var iTop = els.css("top");

        logger.log("Old: iTop = " + iTop);

        iTop = parseInt(iTop.replace("px", "")) + 1;

        iTop = iTop + "px";

        logger.log("New: iTop = " + iTop);

        els.css("top", iTop);

    }

}

/**
 * 
 * Scrolls the tooltip with the colleagues list element. It also controls the
 * 
 * tooltip from being scrolled with the news feed pane.
 * 
 * 
 * 
 * @param tooltip
 * 
 * @param from
 * 
 */

function socialScrollTooltipWithTarget(tooltip, from) {

    // scroll tooltip with target element on colleagues list

    if (ssapiHasValue(from)) {

        // set position to fixed

        Ext.get("labsTutorialTooltip").setStyle("position", "fixed");

        Ext.select(".x-shadow").setStyle("position", "fixed");

        Ext.select(".x-ie-shadow").setStyle("position", "fixed");

        // set top of tooltip while scrolling

        var iTop = tooltip.target.getTop();

        var iScrollTop = Ext.getBody().getScroll().top;

        iTop = iTop - iScrollTop;

        // check anchor if bottom, adjust some more

        if (tooltip.anchor == "bottom") {

            var iHeight = Ext.get("labsTutorialTooltip").getHeight();

            iTop = iTop - iHeight - 10;

        }

        Ext.get("labsTutorialTooltip").setTop(iTop);

        Ext.select(".x-shadow").setTop(iTop);

        Ext.select(".x-ie-shadow").setTop(iTop);

    } else {

        // set position to fixed

        Ext.get("labsTutorialTooltip").setStyle("position", "absolute");

        Ext.select(".x-shadow").setStyle("position", "absolute");

        Ext.select(".x-ie-shadow").setStyle("position", "absolute");

    }

}

function socialStopPropagation(e) {

    var logger = new ssobjLogger(arguments);

    if (e.preventDefault) {

        e.preventDefault();

    }

    if (Ext.isIE) {

        e.cancelBubble = true;

        e.returnValue = false;

    } else {

        if (e.stopPropagation) {

            e.stopPropagation();

        }

    }

    return false;

}

// ******* start of library functions

// *****************************************************

/**
 * 
 * Used in removing removing markup when something is pasted in the rich editor
 * 
 * 
 * 
 * @param {Object}
 * 
 * elem
 * 
 * @param {Object}
 * 
 * e
 * 
 */

function handlepaste(elem, e) {

    var logger = new ssobjLogger(arguments);

    e = e || window.event;

    var savedcontent = elem.innerHTML;

    if (e && e.clipboardData && e.clipboardData.getData) {

        logger.log('Webkit - get data from clipboard, put into editdiv, cleanup, then cancel event');

        var clipboardDataTypesString = JSON.stringify(e.clipboardData.types);

        logger.log('clipboardDataTypesString=' + clipboardDataTypesString);

        if (/text\/html/.test(e.clipboardData.types) || clipboardDataTypesString.indexOf('text/html') > -1) {

            logger.log('text/html');

            elem.innerHTML = e.clipboardData.getData('text/html');

        } else if (/text\/plain/.test(e.clipboardData.types) || clipboardDataTypesString.indexOf('text/plain') > -1) {

            logger.log('text/plain');

            elem.innerHTML = e.clipboardData.getData('text/plain');

        } else {

            logger.log('elem.innerHTML = ""');

            elem.innerHTML = "";

        }

        waitforpastedata(elem, savedcontent);

        if (e.preventDefault) {

            e.stopPropagation();

            e.preventDefault();

        }

        socialSetCaretAtEnd(elem);

        return false;

    } else {

        logger.log('Everything else - empty editdiv and allow browser to paste content into it, then cleanup');

        elem.innerHTML = "";

        waitforpastedata(elem, savedcontent);

        return true;

    }

}

/**
 * 
 * Used in removing removing markup when something is pasted in the rich editor
 * 
 * 
 * 
 * @param {Object}
 * 
 * elem
 * 
 * @param {Object}
 * 
 * savedcontent
 * 
 */

function waitforpastedata(elem, savedcontent) {

    if (elem.childNodes && elem.childNodes.length > 0) {

        processpaste(elem, savedcontent);

    } else {

        var that = {

            e : elem,

            s : savedcontent

        };

        that.callself = function() {

            waitforpastedata(that.e, that.s);

        };

        setTimeout(that.callself, 20);

    }

}

/**
 * 
 * Used in removing removing markup when something is pasted in the rich editor
 * 
 * 
 * 
 * @param {Object}
 * 
 * elem
 * 
 * @param {Object}
 * 
 * savedcontent
 * 
 */

function processpaste(elem, savedcontent) {

    var logger = new ssobjLogger(arguments);

    var pasteddata = elem.innerHTML;

    logger.log('pasteddata=' + pasteddata);

    // ^^Alternatively loop through dom (elem.childNodes or

    // elem.getElementsByTagName) here

    // elem.innerHTML = savedcontent;

    // Do whatever with gathered data;

    pasteddata = pasteddata.replace(/<\/p>/g, '</p>[br]');

    Ext.get('forpaste').update(pasteddata);

    // now get the text value

    // var filteredpasteddata = pasteddata.replace(/(<([^>]+)>)/ig, "");

    var text = socialGetTextContent(Ext.get('forpaste').dom);

    text = text.replace(/\[br\]/g, '<br>');

    text = socialRemoveDoubleSpaces(text);

    text = text.replace(/\n/g, '<br>');

    elem.innerHTML = savedcontent + ' ' + text;

    // alert(pasteddata);

}

/**
 * 
 * Removes double spaces to make post messages look better
 * 
 * 
 * 
 * @param {Object}
 * 
 * str
 * 
 */

function socialRemoveDoubleSpaces(str) {

    return str.replace(/(\s\s)/g, ' ').replace(/(\s\s)/g, ' ').replace(/(\s\s)/g, ' ').replace(/(\s\s)/g, ' ').replace(/(\s\s)/g, ' ').replace(/(\s\s)/g, ' ');

}

/**
 * 
 * This can be used with SuiTags. This is currently being used for demo purposes
 * 
 * only.
 * 
 * 
 * 
 * @param {Object}
 * 
 * msg
 * 
 */

function socialAddTags(msg) {

    return msg;

    var words = msg.split(' ');

    for (var i = 0; i < words.length; i++) {

        var word = words[i].toString();

        if (word.length <= 1) {

            continue;

        }

        var firstChar = word.substr(0, 1);

        // 2nd char check is to make sure we donot tag ids of issues or sales

        // orders for example

        var secondChar = word.substr(1, 2);

        if (firstChar == '#' && socialIsNumber(secondChar) === false) {

            msg = msg.replace(word, "<a id=st" + (new Date()).getTime() + " href=# data-tagname='" + word.replace('#', 's:') + "' onclick='return stCloudShowTip(this);'>" + word + "</a>");

        }

    }

    return msg;

}

/**
 * Convert channel information in the message to link so that user can click on
 * it and be transferred to that channel.
 * 
 * @param message
 * @returns
 */
function socialAddChannelLink(message) {
    var regex = /\[\[(.*?)\]\]/g;
    var matches = message.match(regex);
    var actualValue = '';

    for ( var m in matches) {
        var objOriginalStr = matches[m].trim();
        var objNew = JSON.parse(objOriginalStr.replace('[[', '{').replace(']]', '}'));
        if (objNew.action == "showChannel") {
            actualValue = '<a href="#" onclick="navigateToChannel(' + objNew.channelId + ', \'' + objNew.channelName + '\')">' + objNew.channelName + '</a>';
        } else if (objNew.action == "subscribeChannel") {
            actualValue = '<a href="#" onclick="subscribeFromChannelLink(' + objNew.channelId + ', \'' + objNew.channelName + '\')">' + objNew.linkText + '</a>';
        }
        // replace original string with the actual value
        message = message.replace(objOriginalStr, actualValue);
    }

    return message;
}

/**
 * navigate to the channel specified by the parameter
 * 
 * @param channelId -
 *        id of channel to navigate to.
 */
function navigateToChannel(channelId, channelName) {
    // click the channel selector twice to load the channels. Channels are not
    // loaded on initial load of newsfeed.
    jQuery('#actionchannelsarrow').click().click();
    // click the channel link
    var channelSelector = jQuery('#channel-' + channelId);
    if (channelSelector.length == 0) {
        Ext.Msg.alert('SuiteSocial', 'You do not have access to channel ' + channelName);
    } else {
        channelSelector.click();
    }

}

/**
 * Subscribe the user to the channel through the channel link on the post.
 * 
 * @param channelId
 * @param channelName
 */
function subscribeFromChannelLink(channelId, channelName) {

    // check if shown on ui.
    jQuery('#actionchannelsarrow').click().click();
    var channelElement = jQuery('#channel-' + channelId);
    if (channelElement.length > 0 && channelElement.hasClass('subscribed')) {
        Ext.Msg.alert('SuiteSocial', 'You are already subscribed to channel ' + channelName);
        return;
    }

    // check if user has access to channel
    var userHasAccess = false;
    if (ssapiHasValue(ssobjGlobal.otherChannels)) {
        for (var i = 0; i < ssobjGlobal.otherChannels.length; i++) {
            var otherChannel = ssobjGlobal.otherChannels[i];
            if (otherChannel.internalid == channelId) {
                // user has access
                userHasAccess = true;
                break;
            }
        }
    }

    if (!userHasAccess) {
        Ext.Msg.alert('SuiteSocial', 'You cannot subscribe to channel ' + channelName + ' because you do not have access.');
        return;
    }

    socialSubscribeChannel(channelId);
    socialSuiteletProcessAsync('ssapiGetChannelsUserIsSubscribed', 'any', socialShowChannelsHandler);

    Ext.Msg.alert('SuiteSocial', 'You are currently being subscribed to channel ' + channelName);

    if (channelElement.length == 0 || channelElement.hasClass('subscribed')) {
        return;
    }

    channelElement.removeClass("unsubscribed");
    channelElement.addClass("subscribed");

    socialSetToggleSwitchStyle();
}

function utf8_to_b64(str) {

    return window.btoa(unescape(encodeURIComponent(str)));

}

/**
 * 
 * Removes the image preview of an image attachment. This is currently being
 * 
 * used for demo purposes only.
 * 
 * 
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function socialRemoveFilePreview(event) {

    Ext.get('imagePost').fadeOut({

        callback : function() {

            Ext.get('imagePost').dom.style.display = 'none';

        }

    });

    Ext.get('processing').dom.innerHTML = '';

    ssobjGlobal.imageFile = null;

    return false;

}

/**
 * 
 * Action to take after a file is uploaded to the NetSuite cabinet
 * 
 * 
 * 
 * @param {Object}
 * 
 * data
 * 
 */

function socialAfterUploadFile(data) {

    var logger = new ssobjLogger(arguments);

    var id = data.toString().replace('.0', '');

    logger.log('id=' + id);

    var fileUrl = nlapiResolveURL("mediaitem", id);

    logger.log('fileUrl=' + fileUrl);

    document.getElementById("processing").innerHTML = '';

    // make sure to call next line before calling socialAddPostCommon

    socialRemoveFilePreview();

    document.getElementById("processing").innerHTML = 'File uploaded. Posting message...';

    socialAddPostCommon(ssobjGlobal.msg + ' {{"objecttype": "image", "id": "' + id + '", "filetype": ""}}');

}

/**
 * 
 * Uploads a file to the cabinet
 * 
 * 
 * 
 * @param {Object}
 * 
 * callback
 * 
 */

function socialUploadFile(callback) {

    var logger = new ssobjLogger(arguments);

    if (ssobjGlobal.imageFile === null) {

        return false;

    }

    document.getElementById("processing").innerHTML = 'Uploading...';

    Ext.get("hourglass").show();

    // get form data for POSTing

    // var vFD = document.getElementById('upload_formx').getFormData(); // for

    // FF3

    Ext.get('main_form').dom.setAttribute('enctype', 'multipart/form-data');

    var vFD = new FormData(document.getElementById('main_form'));

    vFD.append('image_file', ssobjGlobal.imageFile);

    // create XMLHttpRequest object, adding few event listeners, and POSTing our

    // data

    var xmlRequest = new XMLHttpRequest();

    // oXHR.upload.addEventListener('progress', uploadProgress, false);

    // oXHR.addEventListener('load', uploadFinish, false);

    // oXHR.addEventListener('error', uploadError, false);

    // oXHR.addEventListener('abort', uploadAbort, false);

    var url = nlapiResolveURL('SUITELET', 'customscript_suitewall_generic_suitelet', 'customdeploy_suitewall_generic_suitelet');

    document.getElementById('main_form').action = url;

    xmlRequest.open('POST', url, true /* async */);

    xmlRequest.onreadystatechange = function() {

        document.getElementById("processing").innerHTML = 'Uploading...';

        if (xmlRequest.readyState == 4 && xmlRequest.status == 200) {

            var returnValue = socialHandleResponse(xmlRequest);

            callback(returnValue);

            window.status = 'Ready';

        }

    };

    xmlRequest.send(vFD);

}

/**
 * 
 * Actions to take when a file is dropped into the richtext editor
 * 
 * 
 * 
 * @param {Object}
 * 
 * evt
 * 
 */

function socialFileDrop(evt) {

    var logger = new ssobjLogger(arguments);

    evt.stopPropagation();

    evt.preventDefault();

    // logger.log('evt.target='+evt.target.innerHTML);

    // logger.log("evt.dataTransfer.getData('Text')="+evt.dataTransfer.getData('Text'));

    // logger.log("evt.dataTransfer.getData('html')="+evt.dataTransfer.getData('html'));

    // logger.log("evt.dataTransfer.getData('Text')="+JSON.stringify(evt.dataTransfer));

    var files = evt.dataTransfer.files;

    var count = files.length;

    // Only call the handler if 1 or more files was dropped.

    // logger.log(count);

    if (count > 0) {

        var file = files[0];

        ssobjGlobal.imageFile = file;

        for ( var p in file) {

            logger.log(p + '=' + file[p]);

        }

        logger.log('file=' + JSON.stringify(file));

        // if (confirm('Do you want to set ' + file.name + ' as your profile

        // picture?') === false) {

        // return;

        // }

        // check file size, do not allow more than 500kb

        var MAXIMUM_IMAGE_SIZE = 500000;

        if (file.size > MAXIMUM_IMAGE_SIZE) {

            Ext.MessageBox.show({

                title : 'SuiteSocial',

                msg : 'The size of the dropped file exceeds the maximum file size which is ' + MAXIMUM_IMAGE_SIZE / 1000 + ' kb.',

                buttons : Ext.MessageBox.OK,

                icon : Ext.MessageBox.WARNING

            });

            return false;

        }

        // Ext.get('imagePost').dom.setAttribute('src', evt.target.result);

        var ALLOWED_FILE_TYPES = [ 'image/jpeg', 'image/png' ];

        if (ALLOWED_FILE_TYPES.indexOf(file.type) == -1) {

            Ext.MessageBox.show({

                title : 'SuiteSocial',

                msg : 'The type of the file is not allowed. The supported file types are:<br /> - ' + ALLOWED_FILE_TYPES.join('<br /> - '),

                buttons : Ext.MessageBox.OK,

                icon : Ext.MessageBox.WARNING

            });

            return false;

        }

        // document.getElementById("processing").innerHTML = "Loading " +

        // file.name + '...';

        var reader = new FileReader();

        // init the reader event handlers

        reader.onload = function(evt) {

            // e.target.result holds the DataURL which

            // can be used as a source of the image:

            var image = Ext.get('imagePost').dom;

            image.setAttribute('src', evt.target.result);

            Ext.get(image).fadeIn();

            document.getElementById("processing").innerHTML = "Preview of " + file.name + ' &nbsp;&nbsp;Size: ' + file.size / 1000 + ' KB &nbsp;&nbsp;Type: ' + file.type + ' &middot; <a href=# onclick="return socialRemoveFilePreview(event)" title="Removes file attachment">Remove</a>';

        };

        // begin the read operation

        reader.readAsDataURL(file);

        // set focus to editor

        setTimeout('socialGetEditorEl().focus()', 500);

        // // Ext.get('image_file').dom.files[0] = file;

        //

        // // get form data for POSTing

        //

        // // var vFD = document.getElementById('upload_formx').getFormData();

        // // for FF3

        // Ext.get('main_form').dom.setAttribute('enctype',

        // 'multipart/form-data');

        // var vFD = new FormData(document.getElementById('main_form'));

        // vFD.append('image_file', file);

        // // create XMLHttpRequest object, adding few event listeners, and

        // POSTing our data

        // var oXHR = new XMLHttpRequest();

        // // oXHR.upload.addEventListener('progress', uploadProgress, false);

        // // oXHR.addEventListener('load', uploadFinish, false);

        // // oXHR.addEventListener('error', uploadError, false);

        // // oXHR.addEventListener('abort', uploadAbort, false);

        // var url = nlapiResolveURL('SUITELET',

        // 'customscript_suitewall_generic_suitelet',

        // 'customdeploy_suitewall_generic_suitelet');

        // document.getElementById('main_form').action = url;

        // oXHR.open('POST', url);

        // oXHR.send(vFD);

        // // oXHR.send(utf8_to_b64(file));

    }

}

/**
 * 
 * Event handler for other drag-and-drop events. Used for demo purposes.
 * 
 * 
 * 
 * @param {Object}
 * 
 * evt
 * 
 */

function noopHandler(evt) {

    evt.stopPropagation();

    evt.preventDefault();

}

/**
 * 
 * Expands or collapses the list of channel, followers, following
 * 
 * 
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function socialExpandOrCollapsePanel(event) {

    try {

        var logger = new ssobjLogger(arguments);

        var animationDuration = 0.5;

        if (ssobjGlobal.isLoaded === false) {

            // this is set to small value since we do not want animation upon

            // page load

            animationDuration = 0.01;

        }

        var e = event || window.event;

        var target = e.target || e.srcElement;

        if (ssapiHasNoValue(target.id)) {

            // must be the down or left arrow so get the parent

            target = e.target.parentNode;

        }

        var id = target.id;

        var headerDom = Ext.get(id).dom;

        var panel = id + 'Panel';

        var panelDom = Ext.get(panel).dom;

        logger.log('panelDom=' + panelDom);

        var label = headerDom.getAttribute('data-label').tl();

        logger.log('label=' + label);

        var count = headerDom.getAttribute('data-count');

        logger.log('panelDom.style.display=' + panelDom.style.display);

        var setCollapseChannels = 'F';

        if (panelDom.style.display === '') {

            setCollapseChannels = 'T';

            logger.log('after panelDom.style.display');

            // animate

            panelDom.style.overflow = 'hidden';

            panelDom.setAttribute('data-originalheight', Ext.get(panelDom).getHeight());

            var props = {

                height : {

                    from : Ext.get(panelDom).getHeight(),

                    to : 1

                }

            };

            logger.log('before animate()');

            var headerTitle = 'Expands the list'.tl();

            Ext.get(panelDom).animate(props, animationDuration, function() {

                panelDom.style.display = 'none';

                if (Ext.isSafari) {

                    headerDom.innerHTML = RIGHT_ARROW_MARKUP_FOR_SAFARI + label;

                } else {

                    // not sure why the text looks smaller in safari when this

                    // code is used

                    headerDom.innerHTML = RIGHT_ARROW_MARKUP + label;

                }

                if (ssapiHasValue(count)) {

                    headerDom.innerHTML = headerDom.innerHTML + ' (' + count + ')';

                }

                // calling line below causes firefox to hang

                // Expands the list.tl();

                headerDom.title = headerTitle;

                socialPositionPatcher();

            }, 'easeOut', 'run');

            logger.log('after animate()');

        } else {

            var originalheight = panelDom.getAttribute('data-originalheight');

            logger.log('originalheight=' + originalheight);

            panelDom.style.display = '';

            props = {

                height : {

                    from : 1,

                    to : originalheight

                }

            };

            headerTitle = 'Collapses the list'.tl();

            Ext.get(panelDom).animate(props, animationDuration, function() {

                headerDom.innerHTML = DOWN_ARROW_MARKUP + label;

                if (ssapiHasValue(count)) {

                    headerDom.innerHTML = headerDom.innerHTML + ' (' + count + ')';

                }

                headerDom.title = headerTitle;

                panelDom.style.overflow = 'visible';

                panelDom.style.height = 'auto';

                socialPositionPatcher();

            }, 'easeOut', 'run');

        }

        if (ssobjGlobal.isLoaded) {

            // make setting sticky

            var command = '';

            switch (panelDom.id) {

            case 'socialChannelsHeaderPanel':

                command = 'setCollapseChannels';

                break;

            case 'socialFollowingHeaderPanel':

                command = 'setCollapseFollowing';

                break;

            case 'socialFollowersHeaderPanel':

                command = 'setCollapseFollowers';

                break;

            default:

                // default_statement;

            }

            socialSuiteletProcessAsyncUser(command, setCollapseChannels, function(results) {

                // should be ok

            });

        }

        // socialPositionPatcher();

        socialStopPropagation(e);

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Makes the editor enabled or not
 * 
 * 
 * 
 * @param {Object}
 * 
 * isEnabled
 * 
 */
function socialSetEditorEnabled(isEnabled) {
    try {
        // enable/disable share button
        // but do not change button status, if a file is currently uploading
        if (!ssapiHasUploadInProgress()) {
            isEnabled ? Ext.get('btnsharecontainer').removeClass("disabled") : Ext.get('btnsharecontainer').addClass("disabled");
        }

        // enable/disable text editor
        var el = socialGetEditorEl();

        if (isEnabled) {
            if (ssobjGlobal.isUseRichEditor) {
                el.setAttribute('contentEditable', true);
            } else {
                el.disabled = false;
            }

            // watermark div
            el.previousElementSibling.style.backgroundColor = 'white';
        } else {
            if (ssobjGlobal.isUseRichEditor) {
                el.setAttribute('contentEditable', false);
            } else {
                el.disabled = true;
            }

            // watermark div
            el.previousElementSibling.style.backgroundColor = '#efefef';
        }

        // enable/disable drag n drop for main text area
        var dndElements = {
            targetId : "richEditor",
            targetWatermarkId : "richEditorWatermark",
            progressBarContainerId : "dndprogressbarcontainer",
            progressBarId : "dndprogressbar",
            progressBarStatusId : "dndprogressbarstatus",
            thumbnailContainerId : "dndthumbnailcontainer",
            thumbnailImageId : "dndthumbnailimage",
            filenameContainerId : "dndfilenamecontainer",
            filenameId : "dndfilename",
            canvasId : "dndcanvas",
            dataId : "dnddata",
            buttonId : "btnsharecontainer"
        };
        ssapiEnableDnDOnSuiteSocial(isEnabled, dndElements, socialFileUploadHandler);
        
        // enable/disable manual upload
        jQuery('#fileUpload').css('display', isEnabled ? '' : 'none');
        
    } catch (e) {
        ssapiHandleError(e);
    }
}

function socialGetEditorEx() {

    if (ssobjGlobal.isUseRichEditor) {

        return Ext.get('richEditor');

    } else {

        // return Ext.get('msgTextBox');

    }

}

function socialGetEditorEl() {

    return socialGetEditorEx().dom;

}

function socialGetEditorText() {

    if (ssobjGlobal.isUseRichEditor) {

        return socialGetRichEditorText(socialGetEditorEl());

    } else {

        return socialGetEditorEl().value;

    }
}

// Show or hide suggestion box
function setSuggestionBoxVisibility(isVisible // boolean: true to show, false
// to hide
) {
    var action = isVisible === true ? "show" : "hide";

    Ext.get("divDropDownListRich")[action]();
    if (Ext.isIE) {
        Ext.get("selectDropDownListRich")[action]();
    }
}

/**
 * @event
 * @param {Object}
 *        element
 */
function onSelectDropDownListRichClick(element) {
    try {

        var select = element;

        var index = select.selectedIndex;

        if (index == -1) {

            return;

        }

        var name = select.options[index].text;

        // var id = select.options[index].value;

        // var messageTextBox = Ext.get('msgTextBox').dom;

        // var wholeMessage = messageTextBox.value;

        // var word = socialGetCurrentWord(messageTextBox);

        var newWord = name;

        // // the name is in the format RECORDTYPE name, strip the RECORDTYPE

        // newWord = newWord.substr(newWord.indexOf(' ') + 1);

        // check if the item selected is a saved search

        var key = '@' + newWord.replace(/ /g, '-');

        // logger.log(key);

        var resultJson = ssobjGlobal.recordKeys[key];

        var objectType = 'record';

        ssobjGlobal.lastSearchData = null;

        // // store then we will replace with id later
        // var newMessage = wholeMessage.replace(word, newWord);
        // messageTextBox.value = newMessage;

        setSuggestionBoxVisibility(false);

        var el = ssobjGlobal.currentRichEditor;
        var recordType = resultJson.recordtype;
        var recordId = resultJson.id;
        socialInsertMention(el, newWord, objectType, recordType, recordId);

        // socialStopPropagation(e)

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * Displays the list of results from the global search. Note that 2 global
 * searches are performed: 1 for the employee which uses admin rights and
 * another search for the rest of the record types.
 * 
 * @param {Object}
 *        str
 */
function socialShowMessageGlobalSearchRich(str, srcTarget, srcWord) {
    try {
        ssobjGlobal.globalSearchIsProcessing = true;
        /**
         * Search for employees using admin rghts
         * 
         * @param {Object}
         *        employeeResults
         */
        socialSuiteletProcessAsync('employeeGlobalSearch', str, function(employeeResults) {
            /**
             * Search for the rest of the record types using the current user's
             * credentials
             * 
             * @param {Object}
             *        userHasAccessResults
             */
            socialSuiteletProcessAsyncUser('globalSearch', str, function(userHasAccessResults) {
                var logger = new ssobjLogger(arguments);
                var mergedResults = userHasAccessResults.concat(employeeResults);
                // hide hourglass as applicable
                Ext.get('hourglass').dom.style.display = 'none';
                Ext.select('.replyHourGlass').setStyle("display", "none");
                if (Ext.get('hourGlassPrivate')) {
                    Ext.get('hourGlassPrivate').dom.style.display = 'none';
                }
                // clear items in select
                document.getElementById('selectDropDownListRich').options.length = 0;
                var select = document.getElementById("selectDropDownListRich");
                // do global search
                logger.log('mergedResults.length=' + mergedResults.length);
                if (mergedResults.length === 0) {
                    Ext.get('noresultsfoundrich').dom.style.display = '';
                } else {
                    Ext.get('noresultsfoundrich').dom.style.display = 'none';
                    // sort by type and name
                    for (var i = 0; i < mergedResults.length; i++) {
                        mergedResults[i].displayName = mergedResults[i].columns.type.toUpperCase() + ' ' + mergedResults[i].columns.name;
                    }
                    var results = ssapiSortJsonArrayByProperty2(mergedResults, 'displayName');
                    if (results === null) {
                        ssobjGlobal.globalSearchIsProcessing = false;
                        throw results === null;
                    }
                    ssobjGlobal.recordKeys = ssobjGlobal.recordKeys || {};
                    var previousKey = '';
                    for (i = 0; i < results.length; i++) {
                        var option = document.createElement("option");
                        var EXCLUDED_RECORD_TYPES = [ 'file', 'customrecord_ssr_ss_related_record', 'customrecord_newsfeed' ];
                        if (results[i].recordtype === null || EXCLUDED_RECORD_TYPES.indexOf(results[i].recordtype) > -1) {
                            // disable saved search tagging for now
                            // if (results[i].columns.type != 'Search') {
                            continue;
                            // }
                        }
                        var EXCLUDED_TYPES = [ 'NSBUNDLEPROJECT' ];
                        var type = results[i].columns.type || '';
                        type = type.toUpperCase();
                        if (EXCLUDED_TYPES.indexOf(type) > -1) {
                            continue;
                        }
                        option.value = results[i].id;
                        var name = results[i].columns.name;
                        if (socialIsNumber(name)) {
                            name = '#' + name + ': ' + results[i].columns.info1;
                        }
                        option.text = type + ' ' + name;
                        var key = '@' + option.text.replace(/ /g, '-');
                        if (key == previousKey) {
                            // do not add duplicates
                            continue;
                        }
                        ssobjGlobal.recordKeys[key] = results[i];
                        previousKey = key;
                        try {
                            // for IE earlier than version 8
                            select.add(option, select.options[null]);
                        } catch (e) {
                            select.add(option, null);
                        }
                    }
                    if (select.options.length === 0) {
                        Ext.get('noresultsfoundrich').dom.style.display = '';
                    } else {
                        Ext.get('selectDropDownListRich').dom.style.display = '';
                        if (select.options.length > 10) {
                            select.size = 10;
                        } else if (select.options.length == 1) {
                            select.size = 2; // so that it will not become a
                            // dropdown
                        } else {
                            select.size = select.options.length;
                        }
                    }
                }

                var currentWord = socialGetCurrentWordRich(srcTarget);
                currentWord = ssapiHasValue(currentWord) ? trim(currentWord) : currentWord;
                if (currentWord == srcWord) {
                    showSuggestions(srcTarget);
                }

                ssobjGlobal.globalSearchIsProcessing = false;

            }); // end of user credential GlobalSearch
        }); // end of employeeGlobalSearch
    }

    catch (e) {
        ssapiHandleError(e);
    }

    // Set position and show suggestion box
    // Declared inline to prevent pollution of global namespace
    function showSuggestions(srcTarget // text edit element which text is the
    // subject of suggestions
    ) {
        var xRichEditor = Ext.get(srcTarget);

        // get offset for position dropdown
        var offset = xRichEditor.id == 'richEditor' ? 13 : xRichEditor.id == 'richEditorReplyTextBoxPrivate' ? 13 : 5;

        // set position of dropdown
        var top = xRichEditor.getTop() - Ext.get('mainpane').getTop() + offset;
        var xDivDropDownList = Ext.get('divDropDownListRich');
        xDivDropDownList.setTop(top);

        var left = xRichEditor.getLeft() - Ext.get('mainpane').getLeft();
        xDivDropDownList.setLeft(left);

        var width = xRichEditor.getWidth();
        xDivDropDownList.setWidth(width);

        setSuggestionBoxVisibility(true);
    }
}

/**
 * 
 * Returns the word in the current position of the caret
 * 
 */

function socialGetCurrentWordRich(target) {

    var logger = new ssobjLogger(arguments);

    var targetId = target.id;

    var currentMessage = socialGetRichEditorText(targetId);

    var pos = socialGetCaretPositionEditable(targetId);

    // logger.log('pos=' + pos);

    var currentWord = socialGetWordAtPosition(currentMessage, pos);

    if (ssapiHasValue(currentWord)) {

        // for some reasons, a newline is being added, so remove it

        currentWord = currentWord.replace('\n', '');

    }

    logger.log('currentWord=' + currentWord);

    return currentWord;

}

function debugLoopChildNodes() {

    // console.clear();

    var el = Ext.get('richEditor').dom;

    var childNodes = el.childNodes;

    // logger.logog('el.innerHTML=' + el.innerHTML);

    if (childNodes === null) {

        // //logger.log('no children');

        return;

    }

    for (var i = 0; i < childNodes.length; i++) {

        var n = childNodes[i];

        // //logger.log('i=' + i + ' nodeValue=' + n.nodeValue + '; nodeType=' +

        // n.nodeType + '; innerHTML=' + n.innerHTML);

    }

}

/**
 * 
 * Fires when the arrow down arror is pressed in the rich editor
 * 
 */

function socialRichEditorDownArrowPressed() {

    var divDropDownList = Ext.get('divDropDownListRich').dom;

    if (divDropDownList.style.visibility == 'hidden') {

        return true;

    }

    var selectDropDownList = Ext.get('selectDropDownListRich').dom;

    selectDropDownList.focus();

    selectDropDownList.selectedIndex = 0;

}

function onRichEditorChange(el) {

    try {

        // watermark element

        var watermarkId = "";

        // check if post editor or comment editor

        if (el.id == "richEditor") {

            watermarkId = "richEditorWatermark";

        } else if (el.id == "richEditorReplyTextBoxPrivate") {

            watermarkId = "richEditorReplyTextBoxWatermarkPrivate";

        } else {

            watermarkId = "nfpostcommenteditorWatermark" + el.id.replace("nfpostcommenteditor", "");

        }

        // show watermark on entry; hide when no more characters

        var msg = el.innerHTML;

        if (msg == "" || msg == "<br>" || msg == "<div><br></div>")

            Ext.get(watermarkId).show();

        else

            Ext.get(watermarkId).hide();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * @event
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function onRichEditorKeypress(event) {

    var logger = new ssobjLogger(arguments);

    return true;

}

/**
 * @event
 * @param {Object}
 *        event
 */
function onRichEditorKeyup(event) {
    try {
        var logger = new ssobjLogger(arguments, true);
        var e = event || window.event;
        var target = e.target;
        var code = e.charCode || e.keyCode;

        switch (code) {
        case ESC_KEYCODE:
            setSuggestionBoxVisibility(false);
            break;

        case DOWN_ARROW:
            socialRichEditorDownArrowPressed();
            socialStopPropagation(e);
            return false;

        case BACKSPACE_KEYCODE:
            var nodeWrapper = socialGetNodeBeforeCaret(target.id);
            if (ssapiHasValue(nodeWrapper)) {
                if (nodeWrapper.cls == 'mention') {
                    Ext.get(nodeWrapper.id).remove();
                    socialStopPropagation(e);
                    return false;
                }
            }
            break;

        case ENTER_KEYCODE:
            if (e.ctrlKey) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                if (target.id == 'richEditor') {
                    // new posts
                    socialAddPost();
                } else if (target.id == 'richEditorReplyTextBoxPrivate') {
                    // new private message
                    Ext.Msg.hide();
                    socialSendPrivateMessage('ok');
                } else {
                    // reply
                    var postid = target.getAttribute('id').replace('nfpostcommenteditor', '');
                    socialAddReply(postid);
                }
                return false;
            }
            return true;

        default:
            // intentionally nothing
            break;
        }

        // do not allow global search on status updates?
        if (ssobjGlobal.channel == STATUS_CHANGES_ID) {
            return true;
        }

        // store old text so that no need to process if text didnt changed
        var currentMessage = socialGetRichEditorText(target);
        currentMessage = ssapiHasValue(currentMessage) ? trim(currentMessage) : currentMessage;
        if (currentMessage == "") {
            setSuggestionBoxVisibility(false);
        }

        socialRemoveMentions(target);
        ssobjGlobal.lastMessage = currentMessage;
        if (target.childNodes.length === 0) {
            return true;
        }

        /**
         * start of suggestions
         */
        var word = socialGetCurrentWordRich(target);
        word = ssapiHasValue(word) ? trim(word) : word;
        if (word == ssobjGlobal.lastWord) {
            logger.log('word == ssobjGlobal.lastWord');
            return true;
        }
        ssobjGlobal.lastWord = word;

        // previous global search is still running?
        if (ssobjGlobal.globalSearchIsProcessing === true) {
            // console.warn('ssobjGlobal.globalSearchIsProcessing === true
            // busyyyyyyyyyyyyyyyyyyyyyyy');
            return;
        }

        Ext.get('noresultsfoundrich').dom.style.display = 'none';
        Ext.get('selectDropDownListRich').dom.style.display = 'none';
        // IE hack: store the caretPosition since the caret position is lost
        // when the focus is removed from the richEditor
        var nbsp = String.fromCharCode(160);

        if (word !== null) {
            word = word.replace(nbsp, '');
        }

        ssobjGlobal.currentWordRich = word;
        ssobjGlobal.nodeAtCaret = socialGetNodeAtCaret(target.id);
        if (ssapiHasNoValue(ssobjGlobal.nodeAtCaret)) {
            return true;
        }

        var realNode = ssobjGlobal.nodeAtCaret.node;
        ssobjGlobal.nodeTextContent = socialGetTextContent(realNode);
        logger.logAlways('nodeTextContent=' + ssobjGlobal.nodeTextContent);
        ssobjGlobal.caretPos = socialGetCaretPositionEditable(target.id);
        logger.logAlways('word=' + word);

        if (word === null) {
            logger.log('word === null');
            setSuggestionBoxVisibility(false);
            return;
        }

        if (word.indexOf('@') !== 0) {
            // not a mention
            logger.logAlways('not a mention');
            setSuggestionBoxVisibility(false);
            return;
        }

        var keywordLength = word.length - 1; // exclude the @
        if (word.indexOf(':') != -1) {
            keywordLength -= word.indexOf(':'); // exclude the prefix
        }

        // the keyword only
        var MINIMUM_GLOBAL_SEARCH_LENGTH = 3;
        if (keywordLength < MINIMUM_GLOBAL_SEARCH_LENGTH) {
            logger.logAlways('keywordLength < MINIMUM_GLOBAL_SEARCH_LENGTH');
            setSuggestionBoxVisibility(false);
            return;
        }

        // perform search 1 second after the user stopped typing
        if (ssobjGlobal.messageSearchId) {
            clearTimeout(ssobjGlobal.messageSearchId);
        }

        var str = word.replace('@', '');
        logger.logAlways('str=' + str);
        if (target.id == 'richEditor') {
            Ext.get('hourglass').dom.style.display = 'inline';
        } else if (target.id == 'richEditorReplyTextBoxPrivate') {
            Ext.get('hourGlassPrivate').dom.style.display = 'inline';
        } else {
            Ext.get(target.id.replace('nfpostcommenteditor', 'hourGlass')).dom.style.display = 'inline';
        }

        var MILLISECONDS_BEFORE_SEARCHING = 1000; // 1 second
        ssobjGlobal.currentRichEditor = target;
        ssobjGlobal.messageSearchId = setTimeout(socialShowMessageGlobalSearchRich, MILLISECONDS_BEFORE_SEARCHING, str, target, word);

    } catch (e) {
        ssapiHandleError(e);
    }
}

/**
 * 
 * Returns the text (instead of the markup) of the contents of the richEditor.
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 * @return {string}
 * 
 */

function socialGetRichEditorText(el) {

    var logger = new ssobjLogger(arguments);

    el = socialGetElement(el);

    var txt = '';

    var childNodes = el.childNodes;

    // logger.log('el.innerHTML=' + el.innerHTML);

    if (childNodes === null) {

        // logger.log('no children');

        return '';

    }

    for (var i = 0; i < childNodes.length; i++) {

        var n = childNodes[i];

        var value = socialGetTextContent(n) || (n.getAttribute != undefined ? n.getAttribute('originaltext') : '');

        if (ssapiHasNoValue(value)) {

            // append space if tag is BR

            if (ssapiHasValue(n.tagName)) {

                if (n.tagName.toUpperCase() == 'BR') {

                    txt += ' ';

                }

            }

        } else {

            // prepend space to value if tag is P or DIV

            if (ssapiHasValue(n.tagName)) {

                if (n.tagName.toUpperCase() == 'P' || n.tagName.toUpperCase() == 'DIV') {

                    txt += ' ';

                }

            }

            txt += value;

        }

        logger.log('i=' + i + ' nodeValue=' + n.nodeValue + '; nodeType=' + n.nodeType + '; textContent=' + n.textContent);

    }

    var NBSP = String.fromCharCode(160);

    txt = txt.replace(new RegExp(NBSP, 'g'), ' ');

    return txt;

}

function socialGetRichEditorTextForPosting(el) {

    try {

        var logger = new ssobjLogger(arguments);

        el = socialGetElement(el);

        var txt = '';

        var childNodes = el.childNodes;

        // logger.log('el.innerHTML=' + el.innerHTML);

        if (childNodes === null) {

            // logger.log('no children');

            return '';

        }

        for (var i = 0; i < childNodes.length; i++) {

            var n = childNodes[i];

            var value = n.nodeValue || socialGetTextContent(n);

            if (ssapiHasNoValue(value)) {

                // most likely newline <br>

                txt += '[b]';

            } else {

                txt += value;

            }

            logger.log('i=' + i + ' nodeValue=' + n.nodeValue + '; nodeType=' + n.nodeType + '; textContent=' + n.textContent);

        }

        var NBSP = String.fromCharCode(160);

        txt = txt.replace(new RegExp(NBSP, 'g'), ' ');

        return txt;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialGetElement(el) {

    if (typeof el == 'string') {

        el = Ext.get(el).dom;

    } else {

        if (el.update) {

            // an extjs so get dom

            el = el.dom;

        }

    }

    return el;

}

/**
 * 
 * Sets the caret position before the supplied child
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 * @param {Object}
 * 
 * child
 * 
 */

function socialSetCaretRich(el, child) {

    try {

        var logger = new ssobjLogger(arguments);

        el = socialGetElement(el);

        if (document.selection) {

            // TODO: code for IE

            // var range = window.getSelection().getRangeAt(0);

            var sel = document.selection.createRange();

            // sel.moveStart('character', char);

            // sel.select();

        } else {

            var range = document.createRange();

            sel = window.getSelection();

            range.setStart(child, 0);

            range.collapse(true);

            sel.removeAllRanges();

            sel.addRange(range);

        }

        el.focus();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Sets the caret position at the end
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 */

function socialSetCaretAtEnd(el) {

    try {

        el = socialGetElement(el);

        el.focus();

        if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {

            var range = document.createRange();

            range.selectNodeContents(el);

            range.collapse(false);

            var sel = window.getSelection();

            sel.removeAllRanges();

            sel.addRange(range);

        } else if (typeof document.body.createTextRange != "undefined") {

            var textRange = document.body.createTextRange();

            textRange.moveToElementText(el);

            textRange.collapse(false);

            textRange.select();

        }

        el.focus();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Inserts a mention into the editor
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 * @param {Object}
 * 
 * text
 * 
 * @param {Object}
 * 
 * objectType
 * 
 * @param {Object}
 * 
 * recordType
 * 
 * @param {Object}
 * 
 * recordId
 * 
 */

function socialInsertMention(el, text, objectType, recordType, recordId) {

    try {

        var logger = new ssobjLogger(arguments);

        el = socialGetElement(el);

        // create mention (span)

        // var newSpan = document.createElement('span');

        // newSpan.innerHTML = text;

        // newSpan.setAttribute('id', (new Date()).getTime());

        // newSpan.setAttribute('originaltext', text);

        // newSpan.setAttribute('class', 'mention');

        // newSpan.setAttribute('contentEditable', 'false');

        // newSpan.setAttribute('objecttype', objectType);

        // newSpan.setAttribute('recordtype', recordType);

        // newSpan.setAttribute('recordid', recordId);

        // if (ssobjGlobal.lastSearchData !== null) {

        // newSpan.setAttribute('data',

        // JSON.stringify(ssobjGlobal.lastSearchData));

        // }

        var id = (new Date()).getTime();

        var markupToBeInserted = '<a isMention=true contentEditable=false class=mention id="' + id + '" originaltext="' + text + '" objecttype="' + objectType + '" recordtype="' + recordType + '" recordid="' + recordId + '">' + text + '</a>';

        // var markupToBeInserted = '<img class=mention alt="' + text + '"

        // originaltext="' + text + '" objecttype="' + objectType + '"

        // recordtype="' + recordType + '" recordid="' + recordId + '"/>';

        // add a space

        // if (Ext.isChrome || Ext.isSafari) {

        // markupToBeInserted += ' &nbsp;<span>&#00;</span>';

        // }

        // else {

        // markupToBeInserted += ' ';

        markupToBeInserted += '&nbsp;';

        // }

        logger.log('markupToBeInserted=' + markupToBeInserted);

        var word = ssobjGlobal.currentWordRich;

        logger.log('word=' + word);

        // logger.log('el.innerHTML=' + el.innerHTML);

        var nbsp = String.fromCharCode(160);

        word = word.replace(nbsp, ' ');

        // el.innerHTML = el.innerHTML.replace('&nbsp;', ' ');

        // socialInsertElementAtCursor(newSpan);

        // logger.log('el.innerHTML=' + el.innerHTML);

        // logger.log('el.id=' + el.id);

        el.innerHTML = el.innerHTML.replace(word, markupToBeInserted);

        // loop thru the child nodes and find the newly added mention

        for (var i = 0; i < el.childNodes.length; i++) {

            var node = el.childNodes[i];

            if (node.getAttribute) {

                if (node.getAttribute('id') == id) {

                    // caret should be after the inserted span so we set the

                    // caret before the next node after the inserted span

                    // if (Ext.isChrome || Ext.isSafari) {

                    // socialSetCaretRich(el, el.childNodes[i + 2]);

                    // }

                    // else {

                    if (i + 1 == el.childNodes.length) {

                        // last element

                        logger.log('last element');

                        socialSetCaretAtEnd(el);

                    } else {

                        socialSetCaretRich(el, el.childNodes[i + 1]);

                    }

                    return;

                }

            }

        }

        return true;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Removes mentions from the editor when the original text is no longer the same
 * 
 * the displayed text
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 */

function socialRemoveMentions(el) {

    try {

        var logger = new ssobjLogger(arguments);

        // logger.setDisabled(true);

        if (typeof el == 'string') {

            el = Ext.get(el).dom;

        } else {

            if (el.update) {

                // an extjs so get dom

                el = el.dom;

            }

        }

        var nodes = socialGetChildNodes(el);

        // var retainNodes = [];

        for (var i = 0; i < nodes.length; i++) {

            var node = nodes[i];

            logger.log('node.id=' + node.id);

            logger.log('node.isLiteral=' + node.isLiteral);

            if (ssapiHasNoValue(node.originaltext)) {

                // ignore literals

                continue;

            }

            logger.log('node.originaltext=' + node.originaltext);

            logger.log('node.text=' + node.text);

            if (node.originaltext != node.text) {

                var orig = node.originaltext;

                var newText = node.text;

                if (orig.length == newText.length - 1) {

                    // he must be trying to type after the tag, so change

                    node.text = orig;

                    // logger.log('node.id='+node.id);

                    // logger.log('JSON.stringify(node.id)='+JSON.stringify(node.id));

                    Ext.get(node.value.id).update(Ext.get(node.value.id).dom.innerHTML + '&nbsp;hi');

                }

                logger.log('REMOVE. node.text=' + node.text);

                // Ext.removeNode(node.node);

            }

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialGetNodeAtCaret(el) {

    var logger = new ssobjLogger(arguments);

    el = socialGetElement(el);

    // var node = document.getSelection().anchorNode;

    // if (node.id == 'richEitor') {

    // // end of the editor

    // return null;

    // }else{

    // return node;

    // }

    if (el.childNodes.length === 0) {

        return null;

    }

    // get caret positions

    var caretPos = socialGetCaretPositionEditable(el);

    logger.log('caretPos=' + caretPos);

    // get list of nodes and find where the caret is within

    var nodes = socialGetChildNodes(el);

    // if caret position is after the last node, the node is the last

    var lastNode = nodes[nodes.length - 1];

    if (caretPos > lastNode.endPos) {

        // if (lastNode.node.outerHTML === '<br>') {

        // // 2nd to the last node

        // return nodes[nodes.length - 2];

        // }

        return lastNode;

    }

    for (var i = 0; i < nodes.length; i++) {

        var node = nodes[i];

        if (caretPos >= node.startPos && caretPos <= node.endPos) {

            // if the node is the last and it is a <br>, set the preceeding node

            // as current node

            // not sure why <br> is being appended automatically

            // if (i == nodes.length - 1 && node.node.outerHTML === '<br>') {

            // // 2nd to the last node

            // return nodes[nodes.length - 2];

            // }

            return node;

        }

    }

    throw 'not found. caretPos=' + caretPos;

}

function socialGetNodeBeforeCaret(el) {

    try {

        var logger = new ssobjLogger(arguments);

        el = socialGetElement(el);

        // var node = document.getSelection().anchorNode;

        // if (node.id == 'richEitor') {

        // // end of the editor

        // return null;

        // }else{

        // return node;

        // }

        if (el.childNodes.length === 0) {

            return null;

        }

        // get caret positions

        var caretPos = socialGetCaretPositionEditable(el);

        logger.log('caretPos=' + caretPos);

        if (caretPos == 0) {

            return null;

        }

        // get list of nodes and find where the caret is within

        var nodes = socialGetChildNodes(el);

        // if caret position is after the last node, the node is the last

        var lastNode = nodes[nodes.length - 1];

        // if (caretPos > lastNode.endPos) {

        // return lastNode;

        // }

        for (var i = 0; i < nodes.length; i++) {

            var node = nodes[i];

            if ((caretPos - 1) >= node.startPos && (caretPos - 1) <= node.endPos) {

                return node;

            }

        }

    } catch (e) {

        ssapiHandleError(e);

    }

    throw 'not found. caretPos=' + caretPos + '; nodeHash=' + JSON.stringify(nodes);

}

function socialGetChildNodes(el) {

    try {

        var logger = new ssobjLogger(arguments);

        if (typeof el == 'string') {

            el = Ext.get(el).dom;

        } else {

            if (el.update) {

                // an extjs so get dom

                el = el.dom;

            }

        }

        logger.log('el.innerHTML=' + el.innerHTML);

        logger.log('el.childNodes.length=' + el.childNodes.length);

        // ensure to get nodes from within <P>s and <DIV>s

        var allNodes = [];

        for (var i = 0; i < el.childNodes.length; i++) {

            var node = el.childNodes[i];

            logger.log(i + '; node.tagName=' + node.tagName + '; node.nodeValue=' + node.nodeValue + '; node.textContent=' + node.textContent + ' node.outerHTML=' + node.outerHTML);

            // if node is P or DIV, get childnodes and add to array

            if (ssapiHasValue(node.tagName)) {

                if (node.tagName.toUpperCase() == "P" || node.tagName.toUpperCase() == "DIV") {

                    // first, prepend br if necessary, and only from 2nd line

                    if (i > 0) {

                        if (node.tagName.toUpperCase() == "P") {

                            allNodes.push(document.createElement("br"));

                        } else if (node.tagName.toUpperCase() == "DIV") {

                            // if node before this node is nbsp, replace by br

                            if (allNodes[allNodes.length - 1].nodeValue == String.fromCharCode(160)) {

                                allNodes[allNodes.length - 1] = document.createElement("br");

                            }

                        }

                    }

                    // loop through childnodes

                    for (var j = 0; j < node.childNodes.length; j++) {

                        allNodes.push(node.childNodes[j]);

                    }

                    continue;

                }

            }

            // add all other nodes

            allNodes.push(node);

        }

        // loop through all nodes

        var nodes = [];

        var startPos = 0;

        var endPos = 0;

        logger.log('allNodes.length=' + allNodes.length);

        for (var i = 0; i < allNodes.length; i++) {

            var node = allNodes[i];

            logger.log(i + '; node.nodeType=' + node.nodeType + '; node.nodeValue=' + node.nodeValue + '; node.textContent=' + node.textContent + ' node.outerHTML=' + node.outerHTML);

            var text, value;

            var isLiteral;

            if (node.nodeValue) {

                // literal

                text = node.nodeValue;

                value = node.nodeValue;

                isLiteral = true;

            } else {

                // span, a, etc

                text = socialGetTextContent(node);

                value = node.outerHTML;

                isLiteral = false;

            }

            if (text.length > 0) {

                endPos = startPos + text.length - 1;

            } else {

                startPos = endPos;

            }

            logger.log('text=' + text);

            var originaltext = null;

            var id = null;

            var cls = null;

            if (node.getAttribute) {

                originaltext = node.getAttribute('originaltext');

                id = node.getAttribute('id');

                cls = node.getAttribute('class');

            }

            nodes.push({

                id : id,

                text : text,

                startPos : startPos,

                endPos : endPos,

                value : value,

                originaltext : originaltext,

                node : node,

                isLiteral : isLiteral,

                cls : cls

            });

            // set startPos for next node

            startPos = endPos + 1;

        }

        return nodes;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Returns the current caret position
 * 
 * 
 * 
 * @param {Object}
 * 
 * element. Can be an extjs or dom
 * 
 * @return {int}
 * 
 */

function socialGetCaretPositionEditable(element) {

    try {

        var logger = new ssobjLogger(arguments);

        element = socialGetElement(element);

        var caretOffset = 0;

        if (typeof window.getSelection != "undefined") {

            var range = window.getSelection().getRangeAt(0);

            var preCaretRange = range.cloneRange();

            preCaretRange.selectNodeContents(element);

            preCaretRange.setEnd(range.endContainer, range.endOffset);

            caretOffset = preCaretRange.toString().length;

        } else if (typeof document.selection != "undefined" && document.selection.type != "Control") {

            var textRange = document.selection.createRange();

            var preCaretTextRange = document.body.createTextRange();

            preCaretTextRange.moveToElementText(element);

            preCaretTextRange.setEndPoint("EndToEnd", textRange);

            caretOffset = preCaretTextRange.text.length;

        }

        return caretOffset;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays a chart. Used for demo purposes.
 * 
 * 
 * 
 * @param {Object}
 * 
 * data
 * 
 * @param {Object}
 * 
 * renderTo
 * 
 * @param {Object}
 * 
 * title
 * 
 * @param {Object}
 * 
 * isShowCloseButton
 * 
 * @param {Object}
 * 
 * width
 * 
 */

function socialShowChart(data, renderTo, title, isShowCloseButton, width) {

    var logger = new ssobjLogger(arguments);

    if (ssapiHasNoValue(data)) {

        logger.error('ssapiHasNoValue(data)');

        return;

    }

    Ext.get('chartPost').dom.innerHTML = '';

    var buttonMarkup = '<a href=# onclick="Ext.get(\'chartPost\').dom.innerHTML = \'\'; return false;">Remove</a><br />';

    Ext.chart.Chart.CHART_URL = 'http://dev.sencha.com/deploy/ext-3.3.1/resources/charts.swf';

    var first = data[0];

    var ctr = 0;

    for ( var p in first) {

        if (ctr === 0) {

            var category = p;

        }

        if (ctr == 1) {

            var value = p;

        }

        ctr++;

    }

    var store = new Ext.data.JsonStore({

        fields : [ category, value ],

        data : data

    });

    var tools = null;

    if (isShowCloseButton) {

        tools = [ {

            id : 'close',

            handler : function(e, toolEl, panel, tc) {

                Ext.get('chartPost').dom.innerHTML = '';

            },

            qtip : 'Remove the chart'

        } ];

    }

    if (!width) {

        width = 578;

    }

    new Ext.Panel({

        title : title,

        renderTo : renderTo,

        width : width,

        height : 300,

        layout : 'fit',

        tools : tools,

        items : {

            xAxis : new Ext.chart.NumericAxis({

                title : value

            }),

            yAxis : new Ext.chart.CategoryAxis({

                title : ''

            }),

            xtype : 'barchart',

            store : store,

            xField : value,

            yField : category,

            extraStyle : {

                yAxisX : {

                    labelRotation : 90

                }

            },

            listeners : {

                itemclickX : function(o) {

                    // var rec = store.getAt(o.index);

                    // Ext.example.msg('Item Selected', 'You chose {0}.',

                    // rec.get('name'));

                }

            }

        }

    });

}

function socialTemplateAppend(tmpId, targetId, data) {

    try {

        var tmpPost = document.getElementById(tmpId).innerHTML;

        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');

        myTplPost.append(document.getElementById(targetId), data, false);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function onClearSearchColleagues() {

    // clear input value

    Ext.get('searchColleaguesTextBox').dom.value = '';

    // call search colleagues keyup event; mock event object to pass the input

    // as target element

    onSearchColleaguesTextBoxKeyup({

        target : Ext.get('searchColleaguesTextBox').dom

    });

}

/**
 * 
 * @event
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function onSearchColleaguesTextBoxKeyup(event) {

    try {

        Ext.get('placeHolderColleagueOthers').dom.innerHTML = '<img style="display: block; margin-left: auto; margin-right: auto; margin-top: 50px; margin-bottom: 50px;" src="' + ssobjGlobal.imagesUrlsObj['loading-ani.gif'] + '" />';

        var e = event || window.event;

        // Standard or IE model

        var target = e.target || e.srcElement;

        var str = target.value;

        str = str.replace('@', '');

        str = str.replace('emp:', '');

        // str = 'emp:' + str;

        if (ssobjGlobal.employeeSearchId) {

            clearTimeout(ssobjGlobal.employeeSearchId);

        }

        // hide/show search icon; show/hide clear icon

        if (str.length > 0) {

            Ext.get(target).setStyle('background-image', 'none');

            Ext.select('.ss_search_network_container .ss_search_input_clear').show();

        } else {

            Ext.get(target).setStyle('background-image', '');

            Ext.select('.ss_search_network_container .ss_search_input_clear').hide();

        }

        // perform search 1 second after the user stopped typing

        var MILLISECONDS_BEFORE_SEARCHING = 1000;

        // 1 second

        ssobjGlobal.employeeSearchId = setTimeout('socialShowEmployeeSearch("' + str + '")', MILLISECONDS_BEFORE_SEARCHING);

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Unfollows a colleague
 * 
 * 
 * 
 * @param {string}
 * 
 * employeeId
 * 
 */

function socialUnsubscribeFollowing(employeeId) {

    if (ssobjGlobal.colleagueEmpId !== null) {

        Ext.get('hourglass').show();

    }

    socialSuiteletProcessAsync('unsubscribeFollowing', employeeId, function() {

        socialShowFollowing(function(employeeId) {

            socialHideAllTooltips();

            if (ssobjGlobal.colleagueEmpId !== null) {

                ssobjGlobal.colleagueEmpId = null;

                socialShowPostsByChannel(Ext.get('channel-0'), '0');

            }

            // socialSetHeightOfColleagueContainers();

            Ext.get('hourglass').hide();

            // close colleague popup window

            if (ssapiHasValue(ssobjGlobal.collWin)) {

                ssobjGlobal.collWin.close();

            }

        });

    });

    return false;

}

/**
 * 
 * Follows a colleague
 * 
 * 
 * 
 * @param {Object}
 * 
 * employeeId
 * 
 * @param {Object}
 * 
 * callback
 * 
 */

function socialSubscribeFollowing(employeeId, callback) {
    if (ssapiGetRole(employeeId) === 'Customer' || ssapiGetRole() === 'Customer')
        return;

    try {

        if (ssobjGlobal.colleagueEmpId !== null) {

            Ext.get('hourglass').show();

        }

        socialSuiteletProcessAsync('subscribeFollowing', employeeId, function() {

            socialShowFollowing(function() {

                var logger = new ssobjLogger(arguments, false, 'subscribeFollowing');

                if (callback) {

                    try {

                        callback();

                    } catch (e) {

                        alert('Unexpected error: ' + e);

                    }

                }

                var selector = '#colleaguefollowing li[data-colleague-empid=' + employeeId + ']';

                logger.log('Ext.select(selector).elements.length=' + Ext.select(selector).elements.length);

                if (Ext.select(selector).elements.length != 1) {

                    logger.log('selector=' + selector);

                    throw 'Ext.select(selector).elements.length > 1; selector=' + selector + '; Ext.select(selector).elements.length =' + Ext.select(selector).elements.length;

                }

                var newEl = Ext.select(selector).elements[0];

                // socialSetHeightOfColleagueContainers();

                Ext.get(newEl).scrollIntoView('colleaguefollowing');

                Ext.get(newEl).highlight();

                Ext.get('hourglass').hide();

                // close colleague popup window

                if (ssapiHasValue(ssobjGlobal.collWin)) {

                    ssobjGlobal.collWin.close();

                }

            });

            // socialShowPosts();

        });

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Sets the text to Follow or Unfollow depending on the clicked employee is on
 * 
 * your colleagues list
 * 
 * 
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function socialFollowOrUnfollow(event) {

    try {

        var logger = new ssobjLogger(arguments);

        var e = event || window.event;

        var target = e.target || e.srcElement;

        var id = target.getAttribute('colleagueEmpId');

        if (ssapiHasNoValue(id)) {

            throw 'ssapiHasNoValue(id) target.innerHTML=' + target.innerHTML;

        }

        logger.log('id=' + id);

        socialStopPropagation(e);

        logger.log('target.innerHTML=' + target.innerHTML);

        // alert( target.title.toString().replace('Follow colleague'.tl(),

        // 'Unfollow colleague'.tl() ))

        if (target.innerHTML == 'Unfollow'.tl()) {

            logger.log("target.innerHTML == 'Unfollow'");

            socialUnsubscribeFollowing(id);

            target.innerHTML = 'Follow'.tl();

            var newTitle = target.title.toString().replace('Unfollow colleague'.tl(), 'Follow colleague'.tl());

            target.title = newTitle;

            // next line should be last since it seems the refernce to target is

            // lost after the next line

            target.outerHTML = target.outerHTML.replace('socialUnsubscribeFollowing', 'socialSubscribeFollowing');

        } else {

            target.innerHTML = target.innerHTML + ssobjGlobal.loadingImageMarkupXSmall;

            socialSubscribeFollowing(id, function() {

                target.innerHTML = 'Unfollow'.tl();

                newTitle = target.title.toString().replace('Follow colleague'.tl(), 'Unfollow colleague'.tl());

                target.title = newTitle;

                target.outerHTML = target.outerHTML.replace('socialSubscribeFollowing', 'socialUnsubscribeFollowing').replace(ssobjGlobal.loadingImageMarkupXSmall, '');

                // not sure why if the line below is in a separate line, a a

                // modification not allowed error in encountered in chrome

                // target.outerHTML = target.outerHTML.replace(HOURGLASS, '');

            });

        }

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Clears the colleague search
 * 
 */

function socialClearSearch() {

    try {

        var d = Ext.get('placeHolderColleagueOthers').dom;

        d.innerHTML = '';

        d.style.overflow = 'visible';

        d.style.height = 'auto';

        //

        d = Ext.get('searchColleaguesTextBox').dom;

        d.value = '';

        d.focus();

        d.style.color = 'black';

        Ext.get('socialColleagueSearchHeader').dom.innerHTML = 'Search Colleagues'.tl();

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays the results of the employee search
 * 
 * 
 * 
 * @param {Object}
 * 
 * str
 * 
 */

function socialShowEmployeeSearch(str) {

    try {

        // var logger = new ssobjLogger(arguments);

        // Issue: 247704 [SuiteSocial] News Feed UI: Results in Search

        // Colleagues box still shows results even when emptied

        if (str.length < 2) {

            // Issue: 247704 [SuiteSocial] News Feed UI: Results in Search

            // Colleagues box still shows results even when emptied

            // EMS: Ext.get('socialColleagueSearchHeader').dom.innerHTML =

            // 'Search Colleagues'.tl();

            Ext.get('placeHolderColleagueOthers').dom.innerHTML = 'Type at least 2 characters.'.tl();

            return false;

        }

        socialSuiteletProcessAsync('getEmployeeSearch', {
            searchString : str
        }, function(colleagues) {

            var logger = new ssobjLogger(arguments, false, 'getEmployeeSearch');

            logger.log('colleagues.length=' + colleagues.length);

            ssobjGlobal.employeeSearchId = null;

            Ext.get('placeHolderColleagueOthers').dom.innerHTML = '';

            // EMS: Ext.get('socialColleagueSearchHeader').dom.innerHTML =

            // 'Search Colleagues'.tl() + ' (' + colleagues.length + ')';

            if (colleagues.length === 0) {

                Ext.get('placeHolderColleagueOthers').dom.innerHTML = 'No results found'.tl();

            }

            for (var i = 0; i < colleagues.length; i++) {

                // show only first 100 to prevent browser from freezing

                if (i == 99) {

                    // Ext.get('socialColleagueSearchHeader').dom.innerHTML =

                    // 'Search'.tl() + ' (' + colleagues.length + ')';

                    // break;

                }

                colleagues[i].from = 'search';

                colleagues[i].statusUpdate = colleagues[i].statusUpdate + '&nbsp;';

                // check if the employee is being followed

                var id = colleagues[i].colleagueEmpId;

                // var name = data[i].colleagueEmpText;

                var followOrUnfollowLink = "";

                if (ssapiGetRole() === 'Employee' && ssapiGetRole(id) === 'Employee') {
                    var isBeingFollowed = Ext.select('#colleaguefollowing li[data-colleagueEmpId=' + id + ']').elements.length > 0;
                    if (isBeingFollowed) {
                        followOrUnfollowLink = '<span class="separator">|</span><span class="link" colleagueEmpId=' + id + ' onclick="return socialFollowOrUnfollow(event);" title="Unfollow colleague ' + colleagues[i].colleagueEmpText + '">' + 'Unfollow'.tl() + '</span>';
                    } else {
                        followOrUnfollowLink = '<span class="separator">|</span><span class="link" colleagueEmpId=' + id + ' onclick="return socialFollowOrUnfollow(event);" title="Follow colleague ' + colleagues[i].colleagueEmpText + '">' + 'Follow'.tl() + '</span>';
                    }
                }

                colleagues[i].followOrUnfollowLink = followOrUnfollowLink;
                colleagues[i].kudosDisplayStyle = (ssapiGetRole() === 'Employee' && ssapiGetRole(id) === 'Employee' && isKudosIntegrationEnabled()) ? 'inline' : 'none';
            }

            socialTemplateAppend('tmpColleagueSearch', 'placeHolderColleagueOthers', colleagues);

            // EMS: socialShowPhotos();

            // set maximum height

            /*
             * 
             * var x = Ext.get('placeHolderColleagueOthers');
             * 
             * x.setHeight('auto'); if (x.getHeight() > 250) { x.setHeight(250); }
             * 
             * else { // need to add 10 to remove the vertical scrollbar, dunno
             * 
             * why x.setHeight(x.getHeight() + 10); }
             * 
             */

            // socialColleagueSearchPanel
            // not sure why it does not work without the timeout
            // EMS: setTimeout('socialConvertTipsToExtTips();', 500);
        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Sets the maximum height of an element. This is used for IE only since IE does
 * 
 * not support the max-height css directive.
 * 
 */

function socialIeSetToMaxHeight() {

    return;

}

ssobjGlobal.isList = true;

/**
 * 
 * Toggles the colleague view from Tile or List
 * 
 * 
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function socialToggleColleagueView(event) {

    try {

        var e = event || window.event;

        var target = e.target || e.srcElement;

        var animationDuration = 1;

        if (ssobjGlobal.isLoaded == false) {

            animationDuration = 0.01;

        }

        var isList = ssobjGlobal.isList;

        isList = !isList;

        var setTileColleagues = 'T';

        if (isList) {

            ssobjGlobal.isList = isList;

            setTileColleagues = 'F';

            // list

            // dont MAKE mouseover work

            Ext.select('.colleaguepic').each(function(el) {

                el.dom.setAttribute('onmouseover', 'return false;');

            });

            Ext.select('.colleague').setStyle('display', 'block');

            Ext.select('.colleagueDetails').setStyle('display', 'block');

            Ext.select('.colleague').setStyle('height', 'auto');

            var els = Ext.select('.colleague').elements;

            Ext.select('.colleague').removeClass('tiled');

            if (Ext.isIE) {

                var compEls = Ext.select('.colleague');

                compEls.setStyle('padding-bottom', '0px');

                compEls.setStyle('padding-right', '0px');

                compEls.setStyle('padding-top', '2px');

                compEls.setStyle('padding-left', '5px');

                compEls.setStyle('width', '330px');

                socialSetHeightOfColleagueContainers();

                socialPositionPatcher();

            } else {

                for (var i = 0; i < els.length; i++) {

                    // animate increase in width

                    var el = els[i];

                    var props = {

                        width : {

                            from : Ext.get(el).getWidth(),

                            to : 300

                        }

                    };

                    Ext.get(el).animate(props, animationDuration, function() {

                        Ext.select('.colleagueDetails').setStyle('display', 'block');

                        Ext.select('.colleague').setStyle('border-radius', '10px');

                        Ext.select('.colleague').setStyle('margin', '1px');

                        Ext.select('.colleague').setStyle('padding', '6px');

                        socialSetHeightOfColleagueContainers();

                        socialPositionPatcher();

                    }, 'easeOut', 'run');

                }

            }

            target.innerHTML = 'Tile'.tl();

        } else {

            // tile

            ssobjGlobal.isList = isList;

            // set make mouseover work

            Ext.select('#placeHolderFollowing .colleaguepic, #placeHolderFollowers .colleaguepic').each(function(el) {

                el.dom.setAttribute('onmouseover', el.dom.getAttribute('onclick'));

            });

            if (Ext.isIE) {

                compEls = Ext.select('.colleague');

                compEls.setStyle('width', '36px');

                compEls.setStyle('height', '36px');

                compEls.setStyle('padding', '2px');

                compEls.addClass('tiled');

                if (ssapiIsNewUI()) {

                    compEls.setStyle('display', 'inline-block');

                } else {

                    compEls.setStyle('display', 'inline');

                }

                compEls.setStyle('border-radius', '0');

                Ext.select('.colleagueDetails').setStyle('display', 'none');

                // for some reasons, we need to remove the left and right

                // margins

                // to get the desired 2px in between images

                Ext.select('.colleague').setStyle('margin', '1px');

                socialSetHeightOfColleagueContainers();

                socialPositionPatcher();

            } else {

                els = Ext.select('.colleague').elements;

                for (i = 0; i < els.length; i++) {

                    el = els[i];

                    // animate decrease in width

                    props = {

                        width : {

                            from : Ext.get(el).getWidth(),

                            to : ssapiIsNewUI() ? 42 : 32

                        }

                    };

                    Ext.get(el).animate(props, animationDuration, function() {

                        Ext.select('.colleague').addClass('tiled');

                        Ext.select('.colleagueDetails').setStyle('display', 'none');

                        Ext.select('.colleague').setStyle('display', 'inline-block');

                        Ext.select('.colleague').setStyle('height', '32');

                        Ext.select('.colleague').setStyle('border-radius', '0');

                        Ext.select('.colleague').setStyle('padding', '4px');

                        // for some reasons, we need to remove the left and

                        // right margins

                        // to get the desired 2px in between images

                        Ext.select('.colleague').setStyle('margin-left', '0px');

                        Ext.select('.colleague').setStyle('margin-right', '0px');

                        Ext.select('.colleague').setStyle('margin-top', '1px');

                        Ext.select('.colleague').setStyle('margin-bottom', '1px');

                        socialSetHeightOfColleagueContainers();

                        socialPositionPatcher();

                    }, 'easeOut', 'run');

                }

            }

            target.innerHTML = 'List'.tl();

        }

        if (ssobjGlobal.isLoaded) {

            // make setting sticky

            socialSuiteletProcessAsyncUser('setTileColleagues', setTileColleagues, function(results) {

                // should be ok

            });

        }

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Hides all tooltips.
 * 
 */

function socialHideAllTooltips() {

    // var tooltips = ssobjGlobal.tooltips;

    // for(var t in tooltips) {

    if (ssobjGlobal.lastTooltip) {

        try {

            ssobjGlobal.lastTooltip.hide();

        } catch (e) {

        }

        ssobjGlobal.lastTooltip = null;

    }

    // }

}

/**
 * 
 * Displays the posts sent and received by the selected employee
 * 
 * 
 * 
 * @param {Object}
 * 
 * empId
 * 
 * @param {Object}
 * 
 * empName
 * 
 * @param {Object}
 * 
 * from
 * 
 */

function socialShowColleagueMessages(empId, empName, from) {

    try {

        // var tooltipId = 'colleague-name-' + empId + 'Tooltip';

        socialHideAllTooltips();

        socialCloseCurrentModalBox();

        var container = null;

        switch (from) {

        case 'following':

            container = '#colleaguefollowing';

            break;

        case 'followers':

            container = '#colleaguefollowers';

            break;

        case 'search':

            container = '#placeHolderColleagueOthers';

            break;

        default:

            // default_statement;

        }

        ssobjGlobal.colleagueEmpId = empId;

        document.getElementById('nfposts').innerHTML = '';

        var selector = container + ' li[data-colleague-empid=' + empId + ']';

        var el = Ext.select(selector).elements[0];

        socialShowPostsByColleague(el, empId, empName, from);

        socialSetEditorEnabled(false);

        Ext.get("channelhdrlabel").update("Colleague:".tl());

        Ext.get("channelname").update(empName);

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays the private message dialog box used in entering the message.
 * 
 * 
 * 
 * @param {Object}
 * 
 * empId
 * 
 * @param {Object}
 * 
 * empName
 * 
 */

function socialShowPrivateMessageBox(empId, empName) {

    try {

        socialHideAllTooltips();

        socialCloseCurrentModalBox();

        // check browser width

        var iWidth = Ext.get("maincontainer").getWidth();

        iWidth = iWidth > 400 ? 400 : iWidth;

        // prepare html content of pm

        var html = Ext.get('tmpPrivateMessageEditor').dom.innerHTML.replace(/{internalid}/g, 'Private');

        html = html.replace(/{hourglass}/g, ssobjGlobal.imagesUrlsObj['loading-ani.gif']);

        // initialize Ext.Window

        var searchId = null;

        ssobjGlobal.pmWin = new Ext.Window({

            title : 'Private Message'.tl(),

            width : iWidth,

            autoHeight : true,

            resizable : true,

            modal : true,

            html : html,

            listeners : {
                show : function() {
                    var select = jQuery('#socialpPrivateMessageRecipientSelectPrivate');

                    if (ssapiHasValue(empId) && ssapiHasValue(empName)) {
                        // add selected employee to recipient list
                        jQuery('#socialpPrivateMessageRecipientSelectPrivate optgroup[label="Employees"]').append(jQuery('<option>', {
                            value : empId,
                            text : empName,
                            selected : true
                        }));
                    }

                    select.chosenselect({
                        style : 'width:316px',
                        deselectImgUrl : ssobjGlobal.imagesUrlsObj['chosen-sprite.png']
                    });

                    select.change(addEntityIdToChosenDropDownOptions);

                    // Fix issue with display of close image of selected item
                    jQuery('.search-choice-close').css('background-image', 'url(' + ssobjGlobal.imagesUrlsObj['chosen-sprite.png'] + ')');

                    // adjust the search result container size.
                    var searchResultContainerHeight = 80;
                    jQuery('#pmRecipientPrivate .chosen-drop').height((searchResultContainerHeight + 1) + 'px');
                    jQuery('#pmRecipientPrivate .chosen-results').height(searchResultContainerHeight + 'px');

                    jQuery('.ss_private_message_recipient input').on('keyup', function(e) {
                        searchId = onSocialPrivateMessageSearchTextBoxKeyup(e, searchId);
                    });

                }

            }

        });

        // show window

        ssobjGlobal.pmWin.show(this);

        // remove extra line at the bottom

        Ext.select(".x-shadow div.xsb").setStyle("display", "none");

        Ext.select("div .x-window-bl.x-panel-nofooter").setStyle("display", "none");

        var pmzIndex = jQuery('.x-resizable-pinned').css('z-index') - 1;
        jQuery('.x-resizable-proxy').css('z-index', pmzIndex);

        // capture before close event

        ssobjGlobal.pmWin.on('beforeclose', function() {

            // if has upload in progress, prompt user

            if (ssapiHasUploadInProgress()) {

                // confirm cancel with user

                var msg = 'File upload is currently in progress. Canceling the message or closing the window will stop your upload progress. Do you want to continue?'.tl();

                socialConfirmCancelPrivateMessage(msg, function() {

                    // abort upload

                    ssapiAbortUpload(null, "richEditorReplyTextBoxPrivate");

                    // in case the upload completed before cancelling, check

                    // if has pending upload

                    if (ssapiHasValue(ssobjGlobal.pendingUploads["richEditorReplyTextBoxPrivate"])) {

                        // cancel the upload

                        socialSuiteletProcessAsync('cancelUpload', "richEditorReplyTextBoxPrivate", function(data) {

                            // no need to take action

                        });

                        // delete from global

                        delete ssobjGlobal.pendingUploads["richEditorReplyTextBoxPrivate"];

                    }

                    // close pm window

                    ssobjGlobal.pmWin.close();

                });

                // stop window from closing

                return false;

            }

            // if has pending upload, prompt user

            if (ssapiHasValue(ssobjGlobal.pendingUploads["richEditorReplyTextBoxPrivate"])) {

                // confirm cancel with user

                var msg = 'You have attached a file to this message. Canceling the message or closing the window will delete the uploaded file. Do you want to continue?'.tl();

                socialConfirmCancelPrivateMessage(msg, function() {

                    // cancel the upload

                    socialSuiteletProcessAsync('cancelUpload', "richEditorReplyTextBoxPrivate", function(data) {

                        // no need to take action

                    });

                    // delete from global

                    delete ssobjGlobal.pendingUploads["richEditorReplyTextBoxPrivate"];

                    // close pm window

                    ssobjGlobal.pmWin.close();

                });

                // stop window from closing

                return false;

            }

            ssobjGlobal.recipient = null;

            return true;

        });

        // initialize events

        var privateMsgEditorId = "richEditorReplyTextBoxPrivate";

        Ext.get(privateMsgEditorId).update('');

        Ext.get(privateMsgEditorId).on('keyup', function(event) {

            // show/hide watermark

            if (ssapiIsIE()) {

                onRichEditorChange(Ext.get(privateMsgEditorId).dom);

            }

            var logger = new ssobjLogger(arguments);

            logger.log('private message editor keyup');

            var e = event || window.event;

            onRichEditorKeyup(e);

        });

        Ext.get(privateMsgEditorId).on("input", function(e) {

            onRichEditorChange(Ext.get(privateMsgEditorId).dom);

        });

        // enable drag n drop for private message text area

        var dndElements = {};

        dndElements.targetId = "richEditorReplyTextBoxPrivate";

        dndElements.targetWatermarkId = "richEditorReplyTextBoxWatermarkPrivate";

        dndElements.progressBarContainerId = "pmdndprogressbarcontainerPrivate";

        dndElements.progressBarId = "pmdndprogressbarPrivate";

        dndElements.progressBarStatusId = "pmdndprogressbarstatusPrivate";

        dndElements.thumbnailContainerId = "pmdndthumbnailcontainerPrivate";

        dndElements.thumbnailImageId = "pmdndthumbnailimagePrivate";

        dndElements.filenameContainerId = "pmdndfilenamecontainerPrivate";

        dndElements.filenameId = "pmdndfilenamePrivate";

        dndElements.canvasId = "pmdndcanvasPrivate";

        dndElements.dataId = "pmdnddataPrivate";

        dndElements.buttonId = "pmBtnSendContainerPrivate";

        ssapiEnableDnDOnSuiteSocial(true, dndElements, socialFileUploadHandler);

        // add delay to allow time for rendering

        setTimeout("Ext.get('" + privateMsgEditorId + "').dom.focus();", 300);

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialPrivateMessageShowEmployeeSearch(searchString) {

    if (searchString.length < 2) {
        jQuery('#pmRecipientPrivate .chosen-drop .no-results').html("Type at least 2 characters.");
        return false;
    }

    var selectId = 'socialpPrivateMessageRecipientSelectPrivate';
    var groups = [ 'Employees', 'Customers' ];
    var selectedEntities = jQuery('#' + selectId).val();
    if (ssapiHasNoValue(selectedEntities)) {
        selectedEntities = [];
    }

    var selectedOptions = getSelectedOptions(selectId, groups);

    emptyChosenSelect(selectId, groups);

    // Gets employee id and entity id from server that matches inputted
    // string str
    socialSuiteletProcessAsync('getEntitiesSearch', {
        searchString : searchString,
        isEmployeesOnly : 'F',
        allowSearchCurrentUser : 'F'
    }, function(entities) {

        if (ssapiHasNoValue(entities) || isSearchResultForSelectEmpty(entities, groups)) {
            jQuery('#pmRecipientPrivate .chosen-drop .no-results').html("No results found");
            appendSelectedOptions(selectId, groups, selectedOptions);
            return;
        }

        // ===============
        // add options
        // ===============
        for (var idx = 0; idx < groups.length; idx++) {
            var searchResult = entities[groups[idx]];
            var entityGroup = jQuery('#' + selectId + ' optgroup[label=' + groups[idx] + ']');
            if (ssapiHasValue(searchResult)) {
                var html = '';
                for (var i = 0; i < searchResult.length; i++) {
                    // show only first 100 to prevent browser from freezing
                    if (i == 99) {
                        break;
                    }

                    // avoid duplicate display of currently selected options
                    if (selectedEntities.indexOf(searchResult[i]._id) > -1) {
                        continue;
                    }

                    html += '<option value="' + searchResult[i]._id + '">' + searchResult[i].entityName + '</option>';

                }

                entityGroup.append(html);
            }

        }

        appendSelectedOptions(selectId, groups, selectedOptions);

        jQuery('#' + selectId).trigger('chosen:updated');
        jQuery('#' + selectId).change();

        addEntityIdToChosenDropDownOptions();

    });
}

function onSocialPrivateMessageSearchTextBoxKeyup(event, searchId) {

    // Ext.get('placeHolderColleagueOthers').dom.innerHTML = HOURGLASS;
    var e = event || window.event;
    // Standard or IE model
    var target = e.target || e.srcElement;
    var content = target.value;

    content = content.replace('@', '').replace('emp:', '');

    if (searchId) {
        clearTimeout(searchId);
    }

    // perform search 1 second after the user stopped typing
    var MILLISECONDS_BEFORE_SEARCHING = 1000;
    // 1 second
    jQuery('#pmRecipientPrivate .chosen-drop .no-results').html("Searching...");
    searchId = setTimeout('socialPrivateMessageShowEmployeeSearch("' + content + '")', MILLISECONDS_BEFORE_SEARCHING);
    return searchId;
}

function getSelectedOptions(selectId, groups) {

    // get the options that were not selected
    var selectedEntities = jQuery('#' + selectId).val();

    if (ssapiHasNoValue(selectedEntities)) {
        return {};
    }

    var selectedOptions = [];
    if (ssapiHasValue(groups)) {
        // loop through each group
        var selectedOptionsByGroup = {};
        for (var grpIndex = 0; grpIndex < groups.length; grpIndex++) {
            var options = jQuery('#' + selectId + ' optgroup[label=' + groups[grpIndex] + '] option');
            selectedOptions = [];
            for (var i = 0; i < options.length; i++) {
                if (selectedEntities.indexOf(options[i].value) > -1) {
                    selectedOptions.push(options[i]);
                }
            }
            selectedOptionsByGroup[groups[grpIndex]] = selectedOptions;

        }

        return selectedOptionsByGroup;
    } else {

        var options = jQuery('#' + selectId + ' option');

        for (var i = 0; i < options.length; i++) {
            if (selectedEntities.indexOf(options[i].value) > -1) {
                selectedOptions.push(options[i]);
            }
        }

        return selectedOptions;
    }

}

function emptyChosenSelect(selectId, groups) {

    if (ssapiHasValue(groups)) {
        for (var idx = 0; idx < groups.length; idx++) {
            jQuery('#' + selectId + ' optgroup[label=' + groups[idx] + ']').empty();
        }
    } else {
        jQuery('#' + selectId).empty();
    }

}

function appendSelectedOptions(selectId, groups, selectedOptions) {
    if (ssapiHasValue(groups)) {
        for (var idx = 0; idx < groups.length; idx++) {
            jQuery('#' + selectId + ' optgroup[label=' + groups[idx] + ']').append(selectedOptions[groups[idx]]);
        }
    } else {
        jQuery('#' + selectId).append(selectedOptions);
    }
}

function isSearchResultForSelectEmpty(searchResult, groups) {
    if (ssapiHasNoValue(groups)) {
        return ssapiHasNoValue(searchResult);
    }

    for (var i = 0; i < groups.length; i++) {
        if (ssapiHasValue(searchResult[groups[i]])) {
            return false;
        }
    }
}

function addEntityIdToChosenDropDownOptions() {
    var groups = [ 'Employees', 'Customers' ];

    // do for each group
    for ( var grpIdx in groups) {
        var group = groups[grpIdx];
        var optionValues = [];
        jQuery('#socialpPrivateMessageRecipientSelectPrivate optgroup[label="' + group + '"] option').each(function() {
            optionValues.push({
                value : jQuery(this).val(),
                text : jQuery(this).text()
            });
        });

        var groupFound = false;
        var optionValuesIdx = 0;

        jQuery('#socialpPrivateMessageRecipientSelectPrivate_chosen ul.chosen-results li').each(function() {
            if (groupFound) {
                jQuery(this).attr('data-entityid', optionValues[optionValuesIdx].value);
                optionValuesIdx++;
                if (optionValuesIdx >= optionValues.length) {
                    return false;
                }
            }

            if (!groupFound && jQuery(this).hasClass('group-result') && jQuery(this).text() == group) {
                groupFound = true;
            }

        });
    }
}

/**
 * 
 * Shows a prompt to ask user to confirm cancel of private message
 * 
 * 
 * 
 * @param {String}
 * 
 * msg Message to ask on the user
 * 
 * @param {Function}
 * 
 * callback Callback method that will execute when user proceeds
 * 
 */

function socialConfirmCancelPrivateMessage(msg, callback) {

    // prompt user

    Ext.Msg.show({

        title : 'Confirm'.tl(),

        msg : msg,

        buttons : Ext.Msg.YESNOCANCEL,

        fn : function(btn) {

            if (btn == 'cancel') {

                return;

            }

            // callback method when user proceeds

            if (callback) {

                callback();

            }

        },

        icon : Ext.MessageBox.WARNING,

        buttons : {

            ok : "Yes",

            cancel : "No"

        },

        minWidth : 220

    });

    // HACK: center buttons

    Ext.select('.x-window-br').setStyle({

        'padding-left' : '17px'

    });

}

/**
 * 
 * Client-part of creating a private message or post.
 * 
 * 
 * 
 * @param {String}
 * 
 * buttonId Identify the clicked button id
 * 
 */

function socialSendPrivateMessage(buttonId) {

    try {

        // performance logger

        var logger = new ssobjLogger("socialSendPrivateMessage");

        if (buttonId == 'cancel') {

            // close pm window

            ssobjGlobal.pmWin.close();

            return;

        }

        // check if button is disabled

        if (Ext.get("pmBtnSendContainerPrivate").hasClass("disabled")) {

            return;

        }

        // check if has msg

        var target = Ext.get('richEditorReplyTextBoxPrivate').dom;

        var msg = socialGetRichEditorText(target);

        if (msg.trim() === '') {

            socialShowWarning('Please provide private message.'.tl(), 'SuiteSocial');

            return;

        }

        // check msg if there are curly braces

        if (msg.indexOf('{{') > -1 || msg.indexOf('}}') > -1) {

            socialShowWarning('Double curly braces are not allowed.'.tl(), 'SuiteSocial');

            return;

        }

        var msgRich = socialSocialConvertRecordNamesToObjectRich('richEditorReplyTextBoxPrivate');

        if (msgRich.length > 4000) {

            socialShowInfo('Post should not be more than 4000 characters.'.tl() + '<br>' + 'Current post length:'.tl() + ' ' + msgRich.length, 'SuiteSocial');

            return;

        }

        var recipients = jQuery('#socialpPrivateMessageRecipientSelectPrivate').val();
        if (ssapiHasNoValue(recipients)) {
            socialShowWarning('Please select at least one recipient.'.tl(), 'SuiteSocial');

            return;
        }

        socialAddPostPrivate(msgRich);

        // close pm window

        ssobjGlobal.pmWin.close();

        // log end time

        logger.log("*********************************** END: Adding Private Message ***********************************");

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Action a user clicks a channel to post under to
 * 
 */

function socialSelectChannel(channelId, channelName) {

    // var targetChannelLabel = Ext.get('targetChannelLabel');

    // targetChannelLabel.dom.innerHTML = channelName;

    Ext.get('selectChannel').dom.style.visibility = 'hidden';

    // ssobjGlobal.channel = channelId;

    // Ext.get('msgTextBox').dom.focus();

    socialSetEditorLookAndChannel(null /* el */, channelId, true /* fromsocialSelectChannel */);

    return false;

}

/**
 * 
 * Displays the list of channels where the user will choose where the post will
 * 
 * be posted
 * 
 */

function socialShowOrHideSelectChannel(event) {

    try {

        var logger = new ssobjLogger(arguments);

        var selectChannel = Ext.get('selectChannel');

        var e = event || window.event;

        if (selectChannel.dom.style.visibility == 'visible') {

            // hide it

            selectChannel.hide();

            // selectChannel.setStyle('display', 'none');

            Ext.get('dropdownOtherChannelsLinkContainer').hide();

            socialStopPropagation(e);

            logger.log("selectChannel.dom.style.visibility == 'visible'");

            return false;

        }

        Ext.get('dropdownOtherChannelsLinkContainer').show();

        Ext.get('dropdownYourChannels').update('');

        socialAppendChannelSelectionRows();

        selectChannel.setHeight('auto');

        // display

        var selectChannelLinkArrow = Ext.get('selectChannelLinkArrow');

        // set maximum height

        logger.log('selectChannel.getHeight()=' + selectChannel.getHeight());

        if (selectChannel.getHeight() > 400) {

            selectChannel.setHeight(400);

        }

        // position the dropdown so that its right border is aligned to the down

        // arrow

        var topOfLinkArrow = selectChannelLinkArrow.getTop();

        var leftOfLinkArrow = selectChannelLinkArrow.getLeft();

        var heightOfButton = selectChannelLinkArrow.getHeight();

        logger.log('topOfLinkArrow=' + topOfLinkArrow);

        logger.log('leftOfLinkArrow=' + leftOfLinkArrow);

        logger.log('heightOfButton=' + heightOfButton);

        // selectChannel.dom.style.visibility = 'hidden';

        var widthOfButton = selectChannelLinkArrow.getWidth();

        logger.log('widthOfButton=' + widthOfButton);

        // selectChannel.dom.style.visibility = 'visible';

        var ALLOWANCE = 2;

        selectChannel.setTop(topOfLinkArrow + heightOfButton + ALLOWANCE);

        var dropDownWidth = selectChannel.getWidth();

        // // in ie, the width of the div does not fit in the content, so set to

        // fixed width

        logger.log('dropDownWidth=' + dropDownWidth);

        var MAX_DROPDOWN_CHANNEL_WIDTH = 340;

        if (dropDownWidth > MAX_DROPDOWN_CHANNEL_WIDTH) {

            dropDownWidth = MAX_DROPDOWN_CHANNEL_WIDTH;

            selectChannel.setWidth(dropDownWidth);

        }

        var dropDownLeft = leftOfLinkArrow + widthOfButton - dropDownWidth;

        logger.log('dropDownLeft=' + dropDownLeft);

        selectChannel.setLeft(dropDownLeft);

        socialHideChannelSelectorMoreIfNoRows();

        // show

        selectChannel.show();

        socialStopPropagation(e);

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Shows or hides other channels
 * 
 */

function socialShowMoreOrLessChannels(otherChannelsLinkContainerId, otherChannelsListId) {

    try {

        var logger = new ssobjLogger(arguments);

        logger.log('otherChannelsListId=' + otherChannelsListId);

        var otherChannelsLinkContainerDom = Ext.get(otherChannelsLinkContainerId).dom;

        var otherChannelsListDom = Ext.get(otherChannelsListId).dom;

        logger.log('otherChannelsListDom.style.display=' + otherChannelsListDom.style.display);

        if (otherChannelsListDom.style.display == 'none') {

            // show more channels

            otherChannelsLinkContainerDom.innerHTML = '<a href="#" onclick="return socialShowMoreOrLessChannels( \'' + otherChannelsLinkContainerId + '\', \'' + otherChannelsListId + '\');">' + 'Less'.tl() + ' <span style="color: dimgray; font-size: 7pt">&#9650;</span></a>';

            // otherChannelsListDom.style.display = '';

            Ext.get(otherChannelsListDom).fadeIn();

        } else {

            otherChannelsLinkContainerDom.innerHTML = '<a href="#" onclick="return socialShowMoreOrLessChannels( \'' + otherChannelsLinkContainerId + '\', \'' + otherChannelsListId + '\');">' + 'More'.tl() + ' <span style="color: dimgray; font-size: 7pt">&#9660;</span></a>';

            otherChannelsListDom.style.display = 'none';

            // Ext.get(otherChannelsListDom).fadeOut().dom;

        }

        // var e = event || window.event;

        // socialStopPropagation(e);

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Hides the More link in the channels selector dropdown if there are no more
 * 
 * other channels
 * 
 */

function socialHideChannelSelectorMoreIfNoRows() {

    try {

        // get other channels

        var data = ssobjGlobal.otherChannels;

        // remove channels where user not allowed to post

        var otherChannelsWritable = [];

        for (var i = 0; i < data.length; i++) {

            if (ssobjGlobal.writeableChannelIds.indexOf(data[i].internalid) > -1) {

                otherChannelsWritable.push(data[i]);

            }

        }

        if (otherChannelsWritable.length === 0) {

            Ext.get('dropdownOtherChannelsLinkContainer').setStyle('display', 'none');

        } else {

            Ext.get('dropdownOtherChannelsLinkContainer').setStyle('display', '');

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays other channels in the channel dropdown list
 * 
 * 
 * 
 * @param {Object}
 * 
 * event
 * 
 * @param {Object}
 * 
 * otherChannelsLinkContainerId
 * 
 */

function socialShowMoreChannelsInDropDown(event, otherChannelsLinkContainerId) {

    try {

        // var e = event || window.event;

        // var target = e.target || e.srcElement;

        // append other channels

        var data = ssobjGlobal.otherChannels;

        var tmpPost = document.getElementById('tmpChannelSelection').innerHTML;

        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');

        // remove channels where user not allowed to post

        var otherChannelsWritable = [];

        for (var i = 0; i < data.length; i++) {

            if (ssobjGlobal.writeableChannelIds.indexOf(data[i].internalid) > -1) {

                otherChannelsWritable.push(data[i]);

            }

        }

        myTplPost.append(document.getElementById('dropdownYourChannels'), otherChannelsWritable, false);

        // set max height of dropdown list

        var selectChannel = Ext.get('selectChannel');

        if (selectChannel.getHeight() > 400) {

            selectChannel.setHeight(400);

        }

        // hide More link

        var otherChannelsLinkContainerDom = Ext.get(otherChannelsLinkContainerId).dom;

        otherChannelsLinkContainerDom.style.display = 'none';

        // see if there is a scrollbar, if yes add it to the width

        var d = Ext.get('selectChannel');

        if (d.dom.scrollHeight != d.getHeight()) {

            d.setWidth(d.getWidth() + 20);

        }

        var e = event || window.event;

        socialStopPropagation(e);

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays the NetSuite quickview
 * 
 * 
 * 
 * @param {Object}
 * 
 * event
 * 
 * @param {Object}
 * 
 * recordScriptId
 * 
 * @param {Object}
 * 
 * rowId
 * 
 * @param {Object}
 * 
 * quickViewId
 * 
 */

function socialShowQuickView(event, recordScriptId, rowId, quickViewId) {

    try {

        recordScriptId = 'employee';

        rowId = 1308;

        event = event || window.event;

        // <a id="qsTarget_custom2438_7583_0" onmouseover=" var win =

        // window.isOLC ? window : parent.getExtTooltip ? parent : window; win =

        // window; if (typeof win.getExtTooltip != undefined) var tip =

        // win.getExtTooltip('qsTarget_custom2438_7583_0', 'employee',

        // 'DEFAULT_TEMPLATE', 2438,null); if(tip != undefined)

        // {tip.onTargetOver(event)}; " target="_parent"

        // href="/app/common/entity/employee.nl?id=2438"> first 5666 renamed

        // last 5666 <span style="padding-left:3px"></span> <img width="10px"

        // height="10px" border="0"

        // src="/images/hover/icon_hover.png?v=2012.1.0" alt=""

        // style="vertical-align:middle;"> </a>

        var win = window.isOLC ? window : parent.getExtTooltip ? parent : window;

        win = window;

        var tip = null;

        if (typeof win.getExtTooltip != 'undefined') {

            tip = win.getExtTooltip('qsTarget_custom_' + rowId, recordScriptId, 'DEFAULT_TEMPLATE', rowId, null);

        }

        // logger.log('tip=' + tip);

        if (tip !== null) {

            tip.onTargetOver(event);

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Returns the position of the caret in a control such as textbox
 * 
 * 
 * 
 * @param {Object}
 * 
 * control
 * 
 * @return {int}
 * 
 */

function socialGetCaretPosition(control) {

    try {

        var caretPos = 0;

        // IE Support

        if (document.selection) {

            control.focus();

            var sel = document.selection.createRange();

            var sel2 = sel.duplicate();

            sel2.moveToElementText(control);

            // var CaretPos = 0;

            var charactersAdded = 1;

            while (sel2.inRange(sel)) {

                // old socialGetCaretPosition always counts 1 for

                // linetermination

                // fixed

                if (sel2.htmlText.substr(0, 2) == '\r\n') {

                    caretPos += 2;

                    charactersAdded = 2;

                } else {

                    caretPos++;

                    charactersAdded = 1;

                }

                sel2.moveStart('character');

            }

            caretPos -= charactersAdded;

        }

        // Firefox support

        else if (control.selectionStart || control.selectionStart == '0') {

            caretPos = control.selectionStart;

        }

        return caretPos;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Returns the current word where the caret is within or just before the caret
 * 
 * on a textbox
 * 
 * 
 * 
 * @param {Object}
 * 
 * control
 * 
 * @return {string}
 * 
 */

function socialGetCurrentWord(control) {

    try {

        var logger = new ssobjLogger(arguments);

        var caretPosition = socialGetCaretPosition(control);

        var text = control.value;

        // loop thru all words, if word is within or just before the caret,

        // check if it starts with caret

        var word = '';

        var nextChar = null;

        var startOfWord = 0, endOfWord;

        var isLastChar = false;

        for (var i = 0; i < text.length; i++) {

            var currentChar = text.substr(i, 1);

            if (i === 0 && currentChar !== ' ') {

                word = text.substr(0, 1);

                continue;

            }

            if (currentChar == ' ') {

                word = '';

                startOfWord = i + 1;

                continue;

            }

            word += currentChar;

            if (i == text.length - 1) {

                isLastChar = true;

            } else {

                nextChar = text.substr(i + 1, 1);

            }

            if (nextChar == ' ' || isLastChar) {

                // we got a whole word, so see if it is within or just before

                // the caret

                if (word.length > 0 && word.substr(0, 1) != '@') {

                    if (typeof console != 'undefined') {

                        logger.log('WHOLE WORD NOT keyed=' + word);

                    }

                    continue;

                }

                endOfWord = i;

                logger.log('WHOLE WORD KEYED=' + word + '; startOfWord=' + startOfWord + '; endOfWord=' + endOfWord + '; caretPosition=' + caretPosition);

                if (caretPosition >= startOfWord && caretPosition <= endOfWord + 1) {

                    return word;

                }

            }

        }

        return null;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Returns the word at the current caret position
 * 
 * 
 * 
 * @param {Object}
 * 
 * text
 * 
 * @param {Object}
 * 
 * position
 * 
 * @return {string}
 * 
 */

function socialGetWordAtPosition(text, position) {

    try {

        var logger = new ssobjLogger(arguments, true);

        // loop thru all words, if word is within or just before the caret,

        // check if it starts with caret

        var word = '';

        var nextChar = null;

        var startOfWord = 0, endOfWord;

        var isLastChar = false;

        var NBSP = String.fromCharCode(160);

        if (text === null) {

            return null;

        }

        // edge case where the text has length of 1

        if (text.length == 1 && (position === 0 || position == 1)) {

            return text;

        }

        for (var i = 0; i < text.length; i++) {

            var currentChar = text.substr(i, 1);

            if (i === 0 && currentChar !== ' ' && currentChar != NBSP) {

                word = text.substr(0, 1);

                continue;

            }

            if (currentChar == ' ' || currentChar == NBSP) {

                word = '';

                startOfWord = i + 1;

                continue;

            }

            word += currentChar;

            if (i == text.length - 1) {

                isLastChar = true;

            } else {

                nextChar = text.substr(i + 1, 1);

            }

            if (nextChar == ' ' || currentChar == NBSP || isLastChar) {

                // we got a whole word, so see if it is within or just before

                // the caret

                endOfWord = i;

                // logger.log('WHOLE WORD=' + word + '; startOfWord=' +

                // startOfWord + '; endOfWord=' + endOfWord + '; position=' +

                // position);

                if (position >= startOfWord && position <= endOfWord + 1) {

                    return word;

                }

            }

        }

        return null;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialIsNumber(n) {

    return !isNaN(parseFloat(n)) && isFinite(n);

}

function socialSocialConvertRecordNamesToObject(text) {

    try {

        for ( var p in ssobjGlobal.recordKeys) {

            // see if not existing

            if (text.indexOf(p) == -1) {

                // not existing

                continue;

            }

            var recordObj = ssobjGlobal.recordKeys[p];

            var replacementObj = {};

            replacementObj.name = recordObj.columns.name;

            replacementObj.id = recordObj.id;

            // set objecttype

            if (recordObj.recordtype) {

                if (recordObj.recordtype == 'customrecord_suitesocial_workflow_tag') {

                    replacementObj.objecttype = 'workflow';

                } else {

                    replacementObj.objecttype = 'record';

                    replacementObj.url = nlapiResolveURL('record', recordObj.recordtype, recordObj.id, 'view');

                    replacementObj.recordtype = recordObj.recordtype;

                }

            }

            if (recordObj.columns.type == 'Search') {

                replacementObj.objecttype = 'search';

                replacementObj.data = ssobjGlobal.lastSearchData;

            }

            text = text.replace(new RegExp(p, 'gi'), '{' + JSON.stringify(replacementObj) + '}');

        }

        return text;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * http://stackoverflow.com/questions/512528/set-cursor-position-in-html-textbox
 * 
 * 
 * 
 * @param {Object}
 * 
 * elemId
 * 
 * @param {Object}
 * 
 * caretPos
 * 
 */

function socialSetCaretPosition(elemId, caretPos) {

    try {

        var elem = document.getElementById(elemId);

        if (elem !== null) {

            if (elem.createTextRange) {

                var range = elem.createTextRange();

                range.move('character', caretPos);

                range.select();

            } else {

                if (elem.selectionStart) {

                    elem.focus();

                    elem.setSelectionRange(caretPos, caretPos);

                } else {

                    elem.focus();

                }

            }

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Given a array of objects, converts instance of space in the name to '-'
 * 
 * 
 * 
 * @param {Object}
 * 
 * jsonArray
 * 
 */

function socialRemoveSpacesInName(jsonArray) {

    try {

        var newItems = [];

        for (var i = 0; i < jsonArray.length; i++) {

            var newItem = {};

            var item = jsonArray[i];

            for ( var p in item) {

                newItem[p.replace(/ /g, '-')] = item[p];

            }

            newItems.push(newItem);

        }

        return newItems;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialSetCaretPositionOfElement(elem, caretPos) {

    try {

        if (elem !== null) {

            if (elem.createTextRange) {

                var range = elem.createTextRange();

                range.move('character', caretPos);

                range.select();

            } else {

                if (elem.selectionStart) {

                    elem.focus();

                    elem.setSelectionRange(caretPos, caretPos);

                } else {

                    elem.focus();

                }

            }

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Subscribes to a channel
 * 
 * 
 * 
 * @param {Object}
 * 
 * channelId
 * 
 */

function socialSubscribeChannel(channelId) {

    try {

        socialSuiteletProcessAsyncUser('subscribeChannel', channelId, function() {

            socialShowChannels();

        });

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Unsubscribes to a channel
 * 
 * 
 * 
 * @param {Object}
 * 
 * channelId
 * 
 */

function socialUnsubscribeChannel(channelId) {

    try {

        socialSuiteletProcessAsync('unsubscribeChannel', channelId, function() {

            socialShowChannels();

        });

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * For some reasons, the src attribute is not being filled with correct values
 * 
 * but data-src is being filled properly, so copy the attribute from data-src to
 * 
 * src
 * 
 */

function socialShowPhotos() {

    try {

        var selector = 'img[data-src]';

        var els = Ext.select(selector).elements;

        var count = els.length;

        for (var i = 0; i < count; i++) {

            var el = els[i];

            el.setAttribute('src', el.getAttribute('data-src'));

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialSetHeightOfColleagueContainers() {

    try {

        var followingCount = Ext.select('#placeHolderFollowing .colleague').elements.length;

        var dPlaceHolderFollowing = Ext.get('placeHolderFollowing').dom;

        var followersCount = Ext.select('#placeHolderFollowers .colleague').elements.length;

        var dPlaceHolderFollowers = Ext.get('placeHolderFollowers').dom;

        // show set max height to the total height of 5 colleagues

        if (ssobjGlobal.isList) {

            /**
             * 
             * following
             * 
             */

            var DISPLAYED_ITEMS_IN_LIST_VIEW = 5;

            if (followingCount > DISPLAYED_ITEMS_IN_LIST_VIEW) {

                var elColleague = Ext.select('#placeHolderFollowing .colleague').elements[0];

                dPlaceHolderFollowing.style.height = Ext.get(elColleague).getHeight() * 5 + 5;

            } else {

                dPlaceHolderFollowing.style.height = 'auto';

            }

            /**
             * 
             * followers
             * 
             */

            // show set max height to the total height of 5 colleagues
            if (followersCount > DISPLAYED_ITEMS_IN_LIST_VIEW) {

                var elColleague = Ext.select('#placeHolderFollowers .colleague').elements[0];

                dPlaceHolderFollowers.style.height = Ext.get(elColleague).getHeight() * 5 + 5;

            } else {

                dPlaceHolderFollowers.style.height = 'auto';

            }

        } else {

            /**
             * 
             * following
             * 
             */

            // for tiled, set maximum of 3 rows. note that there are 6 columns.
            var MAXIMUM_LINES_IN_TILED_VIEW = 2;

            if (followingCount > 12) {

                var elColleague = Ext.select('#placeHolderFollowing .colleague').elements[0];

                dPlaceHolderFollowing.style.height = Ext.get(elColleague).getHeight() * MAXIMUM_LINES_IN_TILED_VIEW + 5;

            } else {

                dPlaceHolderFollowing.style.height = 'auto';

            }

            /**
             * 
             * followers
             * 
             */

            if (followersCount > 12) {

                var elColleague = Ext.select('#placeHolderFollowers .colleague').elements[0];

                dPlaceHolderFollowers.style.height = Ext.get(elColleague).getHeight() * MAXIMUM_LINES_IN_TILED_VIEW + 5;

            } else {

                dPlaceHolderFollowers.style.height = 'auto';

            }

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays followers of a record; This is used only on record pages in the
 * 
 * SuitesOCIAL tab
 * 
 */

function socialShowSubscribersOfRecord() {

    try {

        if (ssobjGlobal.recordType === null) {

            return;

        }

        socialSuiteletProcessAsync('getSubscribersOfRecord', {

            recordType : ssobjGlobal.recordType,

            recordId : ssobjGlobal.recordId

        }, socialShowSubscribersOfRecordHandler);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowSubscribersOfRecordHandler(employees) {

    try {

        if (ssapiHasNoValue(ssobjGlobal.recordType)) {

            return;

        }

        // Ext.get('socialFollowersHeader').dom.innerHTML = DOWN_ARROW_MARKUP +

        // 'Followers'.tl() + ' (' + colleagues.length + ')';

        // Ext.get('socialFollowersHeader').dom.setAttribute('data-count',

        // colleagues.length);

        // for (var i = 0; i < employees.length; i++) {

        // ssobjGlobal.colleagues[colleagues[i].colleagueEmpId] = colleagues[i];

        // }

        // alert( JSON.stringify( colleagues));

        // var tmpPost = document.getElementById('tmpColleague').innerHTML;

        // var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost +

        // '</tpl>');

        // myTplPost.append(document.getElementById('placeHolderFollowers'),

        // colleagues, false);

        socialTemplateAppend('tmpColleague', 'placeHolderSubscribersOfRecord', employees);

        // Ext.get('maintable').setWidth(Ext.get('leftColumn').getWidth() +

        // Ext.get('wall').getWidth() + Ext.get('rightColumn').getWidth());

        // alert(employees.length)

        Ext.get('rightColumn').fadeIn();

        // to show the right column

        Ext.get('maintable').setWidth(2000);

        // Ext.get('rightColumn').dom.style.display = '';

        // var ElId = null;

        // if (ssobjGlobal.colleague !== null) {

        // ElId = 'colleague-' + ssobjGlobal.colleague;

        // var exjsEl = Ext.get(ElId);

        // if (ssobjGlobal.isLoaded) {

        // socialShowPostsByColleague(exjsEl.dom, ssobjGlobal.colleague);

        // }

        // }

        socialShowPhotos();

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialGetChannelObj(channelId) {

    try {

        if (channelId == '0') {
            return {
                internalid : '0',
                channelName : PUBLIC_CHANNEL_NAME,
                isAutoSubscribed : true,
                isSubscribed : true
            };
        }

        for (var i = 0; i < ssobjGlobal.yourChannels.length; i++) {
            if (ssobjGlobal.yourChannels[i].internalid == channelId) {
                return ssobjGlobal.yourChannels[i];
            }
        }

        for (i = 0; i < ssobjGlobal.otherChannels.length; i++) {
            if (ssobjGlobal.otherChannels[i].internalid == channelId) {
                return ssobjGlobal.otherChannels[i];
            }
        }

        return null;

    } catch (e) {
        ssapiHandleError(e);
    }

}

/**
 * 
 * Changes the look of the message text box when the active channel changes
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 * @param {Object}
 * 
 * channelId
 * 
 * @param {boolean}
 * 
 * fromsocialSelectChannel Whether this function was called from the
 * 
 * channel selector below the post text box
 * 
 */

function socialSetEditorLookAndChannel(el, channelId, fromsocialSelectChannel) {

    try {

        var logger = new ssobjLogger(arguments);

        ssobjGlobal.channel = channelId;

        ssobjGlobal.colleagueEmpId = null;

        var channelObj = null;

        var isSubscribed = true;

        var isAutoSubscribed = false;

        var channelName = '';

        var allowPost = null;

        if (channelId === null || channelId == '0') {

            channelName = PUBLIC_CHANNEL_NAME;
            allowPost = nlapiGetContext().getRoleCenter() != "CUSTOMER";

            // set channel global variable to null for Public
            ssobjGlobal.channel = null;

            ssobjGlobal.channelEditorEnabled['0'] = allowPost;

        } else {
            channelObj = socialGetChannelObj(channelId);

            channelName = channelObj.name;

            var selector = '#actionchannelslistul li[data-channelId="' + channelId + '"].subscribed';

            isSubscribed = Ext.select(selector).elements.length > 0;

            isAutoSubscribed = channelObj.autoSubscribed;

            if (channelId == STATUS_CHANGES_ID) {

                allowPost = true;

            }

            if (channelId == DIRECT_MESSAGES_ID || channelName == 'Recognition') {

                // since need to select a recipient before you can send PM

                allowPost = false;

            }

            if ([ STATUS_CHANGES_ID, RECORD_CHANGES_ID, DIRECT_MESSAGES_ID ].indexOf(channelId) > -1) {

                channelName = channelName.tl();

            }

        }

        document.getElementById('justPosted').innerHTML = '';

        if (allowPost === null) {
            allowPost = ssobjGlobal.writeableChannelIds.indexOf(channelId) > -1;
        }

        // check if user is allowed to post
        // disallow posting for record changes
        // var message = '';

        socialSetEditorEnabled(allowPost);

        if (ssapiHasValue(channelId)) {
            ssobjGlobal.channelEditorEnabled[channelId] = allowPost;
        }

        if (ssapiHasNoValue(ssobjGlobal.recordType)) {

            // Ext.get('targetChannelLabel').dom.innerHTML = channelName;

            Ext.get("channelhdrlabel").update("Channel:".tl());

            Ext.get('channelname').update(channelName);

            // for some reason, safari does not show this label on first load

            if (Ext.isSafari) {

                setTimeout(function() {

                    // for some reason, it requires an update of other text

                    Ext.get('channelname').update(" ");

                    Ext.get('channelname').update(channelName);

                }, 500);

            }

            // set name on share dropdown

            Ext.select("#sharetodropdown div").update(channelName);

        }

        // clear editor if @mention is found

        if (channelId == STATUS_CHANGES_ID) {

            if (Ext.get('richEditor').dom.innerHTML.indexOf('recordtype') > -1) {

                Ext.get('richEditor').update('');

            }

        }

        Ext.get('richEditor').focus();

        // Ext.get('info').update(message);

        // Ext.get('info').fadeIn(500);

        // socialAdjustEditorContainer();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Positions a small div so that it hides the right border the selected channel
 * 
 * or colleague
 * 
 */

function socialPositionPatcher() {

    try {

        var logger = new ssobjLogger(arguments, true);

        // this is done to hide the right border of the selected channel

        var selected;

        var patcherX = Ext.get('social_patcher');

        if (Ext.select('.channelselected').elements.length > 0) {

            selected = Ext.select('.channelselected').elements[0];

        } else {

            selected = Ext.select('.colleagueselected').elements[0];

            if (ssobjGlobal.isList === false) {

                // no need to patch since colleague view is tile

                patcherX.hide();

                return;

            }

        }

        var selectedX = Ext.get(selected);

        if (selectedX === null) {

            // this will happen if show posts of a colleague was done on the

            // news feed

            return;

        }

        if (selectedX.getWidth() === 0) {

            // it is hidden

            patcherX.hide();

            return;

        } else {

            patcherX.show();

        }

        if (Ext.isIE) {

            patcherX.setTop(selectedX.getTop() - Ext.get('wall').getTop() + 5);

            patcherX.setHeight(selectedX.getHeight() - 5);

        } else {

            patcherX.setTop(selectedX.getTop() - Ext.get('wall').getTop() + 1);

            patcherX.setHeight(selectedX.getHeight());

        }

        patcherX.setLeft(Ext.get('leftcolumn').getWidth() - 1);

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Retrieves and displays posts under a channel
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 * @param {Object}
 * 
 * channelId
 * 
 */

function socialShowPostsByChannel(el, channelId) {

    try {

        var logger = new ssobjLogger(arguments);

        if (ssobjGlobal.isNewFeedProcessing) {

            // in case the use is too fast in clicking channels

            // abort currently running xmlrequest

            socialSuiteletAbortNewsFeedRequest();

            ssobjGlobal.isNewFeedProcessing = false;

        }

        document.getElementById('nfposts').innerHTML = '';

        ssobjGlobal.offset = 0;

        ssobjGlobal.lessThanId = null;

        socialSetEditorLookAndChannel(el, channelId);

        // show posts by channel

        socialShowPosts();

        // this is done to hide the right border of the selected channel

        // socialPositionPatcher();

        return false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Retrieves and displays posts of a colleague
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 * @param {Object}
 * 
 * colleagueEmpId
 * 
 * @param {Object}
 * 
 * colleagueEmpText
 * 
 * @param {Object}
 * 
 * from
 * 
 */

function socialShowPostsByColleague(el, colleagueEmpId, colleagueEmpText, from) {

    try {

        var logger = new ssobjLogger(arguments);

        if (ssobjGlobal.isNewFeedProcessing) {

            // in case the use is too fast in clicking channels

            Ext.Msg.alert('SuiteSocial', 'Please wait until the previous colleague news feed has finished loading.'.tl());

            return;

        }

        ssobjGlobal.colleagueEmpId = colleagueEmpId;

        ssobjGlobal.channel = null;

        ssobjGlobal.offset = 0;

        ssobjGlobal.lessThanId = null;

        // ssobjGlobal.recordType = null;

        // ssobjGlobal.recordId = null;

        // unselect all channels

        // remove active class from other channel

        Ext.select("div#actionchannelslist ul li.active").removeClass("active");

        Ext.select("div#sharetolist ul li.active").removeClass("active");

        document.getElementById('justPosted').innerHTML = '';

        /*
         * 
         * if (Ext.select('.colleagueselected').elements.length !== 0) { if
         * 
         * (Ext.isIE) {
         * 
         * Ext.select('.colleagueselected').setStyle('backgroundImage', '');
         * 
         * Ext.select('.colleagueselected').setStyle('backgroundColor',
         * 
         * 'transparent'); }
         * 
         * Ext.select('.colleagueselected').removeClass('colleagueselected').addClass('colleague'); }
         * 
         */

        var container = null;

        switch (from) {

        case 'following':

            container = '#colleaguefollowing';

            break;

        case 'followers':

            container = '#colleaguefollowers';

            break;

        case 'search':

            container = '#placeHolderColleagueOthers';

            break;

        default:

            // default_statement;

        }

        // Ext.select('.colleagueDivider').show();

        var selected = Ext.select(container + ' li[data-colleague-empid=' + colleagueEmpId + ']').elements[0];

        if (ssapiHasNoValue(selected)) {

            // this will happen if the employee menu is diplsyed from the

            // newsfeed and not from the colleague or search list

        } else {

            /*
             * 
             * Ext.get(selected).addClass('colleagueselected'); if (Ext.isIE) {
             * 
             * Ext.get(selected).setStyle('backgroundImage', 'url("' +
             * 
             * ssobjGlobal.imagesUrlsObj['colleague-background-ie.png'] + '")'); }
             * 
             * Ext.select('#' + selected.id + ' .colleagueDivider').hide();
             * 
             */

        }

        document.getElementById('nfposts').innerHTML = '';

        // socialAdjustEditorContainer();

        // this is done to hide the right border of the selected colleague

        // socialPositionPatcher();

        socialShowPosts();

    } catch (e) {

        ssapiHandleError(e);

    }

}

ssobjGlobal.replyInputId = null;

function socialChangeProcessingState() {

    var dots = Ext.get('showMore').dom.innerHTML;

    switch (dots) {

    case '.&nbsp;&nbsp;':

        Ext.get('showMore').dom.innerHTML = '..&nbsp;';

        break;

    case '..&nbsp;':

        Ext.get('showMore').dom.innerHTML = '...';

        break;

    case '...':

        Ext.get('showMore').dom.innerHTML = '.&nbsp;&nbsp;';

        break;

    default:

        // default_statement;

    }

    var element = Ext.get('btnPost').dom;

    dots = element.value;

    switch (dots) {

    case '.  ':

        element.value = '.. ';

        break;

    case '.. ':

        element.value = '...';

        break;

    case '...':

        element.value = '.  ';

        break;

    default:

        // default_statement;

    }

    // for the reply

    if (Ext.get(ssobjGlobal.replyInputId) === null) {

        return;

    }

    element = Ext.get(ssobjGlobal.replyInputId).dom;

    dots = element.value;

    switch (dots) {

    case '.  ':

        element.value = '.. ';

        break;

    case '.. ':

        element.value = '...';

        break;

    case '...':

        element.value = '.  ';

        break;

    default:

        // default_statement;

    }

}

// EMS: setInterval('socialChangeProcessingState()', 500);

function socialMakeCurrentUserLinkUnclickable() {

    try {

        // make the name of the current user unclickable in the newsfeed

        selector = '.profilename[colleagueempid=' + nlapiGetUser() + ']';

        Ext.select(selector).setStyle('color', 'dimgray').setStyle('cursor', 'text').set({

            onclick : 'return false;'

        });

        Ext.select(selector).setStyle('cursor', 'text').set({

            onclick : 'return false;'

        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Gets the new posts after the last retrieval. Future enhancement.
 * 
 */

function socialCheckForNewPosts() {

    if (ssobjGlobal.isNewFeedProcessing) {

        return;

    }

    var value = {

        "offset" : 0,

        "size" : 25,

        "channel" : null,

        "record_type" : null,

        "record_id" : null,

        "parent" : null,

        "segmentinfo" : [ 0, 1 ],

        "starttime" : null,

        "startid" : null,

        "colleagueEmpId" : null,

        "lessThanId" : null,

        "greaterThanId" : ssobjGlobal.latestDisplayedPostId

    };

    socialSuiteletProcessAsync('getNewsFeed', value, function(feeds) {

        // alert(feeds.length)

        if (feeds.length > 0) {

            var message = '';

            if (feeds.length == 1) {

                message = 'Show ' + feeds.length + ' new post';

            } else {

                message = 'Show ' + feeds.length + ' new posts';

            }

            if (Ext.get('newPostLink').dom.innerHTML == message) {

                return;

            }

            // position at the bottom right

            Ext.get('newPostLink').update(message);

            Ext.get('newPosts').setTop(Ext.getBody().dom.clientHeight - 30);

            Ext.get('newPosts').setLeft(Ext.getBody().dom.clientWidth - Ext.get('newPosts').getWidth() - 10);

            Ext.get('newPosts').show();

            Ext.get('newPostLink').highlight();

        } else {

            Ext.get('newPosts').hide();

        }

    });

}

/**
 * 
 * Client code in deleting a a post
 * 
 * 
 * 
 * @param {string}
 * 
 * id
 * 
 */

function socialDeletePost(id) {
    try {
        // check if deletion is done in popup window
        var popupWindow = socialGetPopupWindow();

        Ext.Msg.show({
            title : 'Confirm'.tl(),
            msg : 'Delete this post?'.tl(),
            buttons : Ext.Msg.YESNOCANCEL,
            fn : function(btn) {
                if (btn == 'cancel') {
                    if (popupWindow.isVisible)
                        popupWindow.showModalWindow();

                    return;
                }

                socialAnimate(Ext.get('nfpostcontainer' + id), function() {
                    Ext.get('nfpostcontainer' + id).dom.style.display = 'none';
                });

                Ext.get('processing').update('<span style="color: dimgray;">' + 'Deleting'.tl() + '...</span>');
                Ext.get('hourglass').show();

                socialSuiteletProcessAsync('deletePost', id, function() {
                    window.status = 'Ready'.tl();
                    Ext.get('hourglass').hide();
                    Ext.get('processing').update('');
                });
            },
            icon : Ext.MessageBox.INFO,
            buttons : {
                ok : "Yes",
                cancel : "No"
            },
            minWidth : 220
        });

        // HACK: center buttons
        Ext.select('.x-window-br').setStyle({
            'padding-left' : '17px'
        });
    } catch (e) {
        ssapiHandleError(e);
    }
}

/**
 * 
 * Deletes a post reply/comment
 * 
 * 
 * 
 * @param {Object}
 * 
 * replyId
 * 
 * @param {Object}
 * 
 * parentId
 * 
 */

function socialDeleteReply(replyId, parentId) {
    try {
        // check if deletion is done in popup window
        var popupWindow = socialGetPopupWindow();

        Ext.MessageBox.confirm('Confirm'.tl(), 'Delete this comment?'.tl(), function(btn) {
            if (btn == 'no') {
                if (popupWindow.isVisible)
                    popupWindow.showModalWindow();

                return false;
            }

            socialAnimate(Ext.get('nfpostcommentmsgcontainer' + replyId), function() {
                Ext.get('nfpostcommentmsgcontainer' + replyId).dom.style.display = 'none';
            });

            socialSuiteletProcessAsync('deletePost', replyId, function() {
                window.status = 'Ready'.tl();
            });

            if (popupWindow.isVisible)
                popupWindow.showModalWindow();
        });

        // HACK: center buttons
        Ext.select('.x-window-br').setStyle({
            'padding-left' : '17px'
        });

        return false;
    } catch (e) {
        ssapiHandleError(e);
    }
}

function socialInsertTextAtCursor(text) {

    try {

        var sel, range, html;

        if (window.getSelection) {

            sel = window.getSelection();

            if (sel.getRangeAt && sel.rangeCount) {

                range = sel.getRangeAt(0);

                range.deleteContents();

                range.insertNode(document.createTextNode(text));

            }

        } else if (document.selection && document.selection.createRange) {

            document.selection.createRange().text = text;

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialInsertNewlineAtCursor() {

    var newEl = document.createElement('br');

    socialInsertElementAtCursor(newEl);

    // var sel, range, html;

    // if (window.getSelection) {

    // sel = window.getSelection();

    // if (sel.getRangeAt && sel.rangeCount) {

    // range = sel.getRangeAt(0);

    // range.deleteContents();

    // var newEl = document.createElement('br');

    // range.insertNode(newEl);

    //

    // socialSetCaretRich('richEditor', newEl);

    // }

    // }

    // else

    // if (document.selection && document.selection.createRange) {

    // document.selection.createRange().innerHTML = '<br>';

    // }

}

function socialInsertElementAtCursor(newEl) {

    try {

        var sel, range, html;

        if (window.getSelection) {

            sel = window.getSelection();

            if (sel.getRangeAt && sel.rangeCount) {

                range = sel.getRangeAt(0);

                range.deleteContents();

                range.insertNode(newEl);

                socialSetCaretRich('richEditor', newEl);

            }

        } else if (document.selection && document.selection.createRange) {

            document.selection.createRange().innerHTML = '<br>';

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * @event
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function onSelectDropDownListRichKeyup(event) {

    try {

        var logger = new ssobjLogger(arguments);

        // alert('onMsgTextBoxKeypress')

        // Get the event object and the target element target

        var e = event || window.event;

        // Standard or IE model

        var target = e.target || e.srcElement;

        // Standard or IE model

        var text = null;

        // The text that was entered

        // Convert character code into a string

        var code = e.charCode || e.keyCode;

        // if (typeof console != 'undefined')

        // logger.log('onSelectDropDownListRichKeypress code=' + code);

        switch (code) {

        case ENTER_KEYCODE:

            if (e.preventDefault) {

                e.preventDefault();

            }

            onSelectDropDownListRichClick(target);

            return false;

        case UP_ARROW:

            var selectDropDownList = Ext.get('selectDropDownListRich').dom;

            if (selectDropDownList.selectedIndex === 0) {

                if (e.preventDefault) {

                    e.preventDefault();

                }

                Ext.get('richEditor').focus();

                return false;

            }

            break;

        default:

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays an information
 * 
 * 
 * 
 * @param {Object}
 * 
 * info
 * 
 * @param {Object}
 * 
 * title
 * 
 */

function socialShowInfo(info, title) {

    try {

        if (ssapiHasNoValue(title)) {

            title = 'SuiteSocial';

        }

        Ext.MessageBox.show({

            title : title,

            msg : info,

            buttons : Ext.MessageBox.OK,

            icon : Ext.MessageBox.INFO

        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Displays a warning
 * 
 * 
 * 
 * @param {Object}
 * 
 * info
 * 
 * @param {Object}
 * 
 * title
 * 
 * @param {Object}
 * 
 * callback
 * 
 */

function socialShowWarning(info, title, callback) {

    try {

        if (ssapiHasNoValue(title)) {

            title = 'SuiteSocial';

        }

        var param = {

            title : title,

            msg : info,

            buttons : Ext.MessageBox.OK,

            icon : Ext.MessageBox.WARNING

        };

        if (ssapiHasValue(callback)) {

            param.fn = callback;

        }

        Ext.MessageBox.show(param);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function foo(returnValue) {

    alert('foo() ' + JSON.stringify(returnValue));

    // uiShowInfo('foo() ' + JSON.stringify(returnValue));

}

/**
 * 
 * Animates an element by changing the height until it becomes 0
 * 
 * 
 * 
 * @param {Object}
 * 
 * extElement
 * 
 * @param {Object}
 * 
 * callback
 * 
 */

function socialAnimate(extElement, callback) {

    try {

        extElement.setStyle('overflow', 'hidden');

        var props = {

            height : {

                to : 0,

                from : extElement.getHeight()

            }

        };

        extElement.animate(props, 1.0, callback, 'easeOut', 'run');

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Animates an element by changing the height until it becomes 0
 * 
 * 
 * 
 * @param {Object}
 * 
 * extElement
 * 
 */

function socialAnimateHide(extElement) {

    try {

        if (extElement.getHeight() !== 0) {

            extElement.dom.setAttribute('originalHeight', extElement.getHeight());

        }

        extElement.setStyle('overflow', 'hidden');

        var props = {

            height : {

                to : 0,

                from : extElement.getHeight()

            }

        };

        extElement.animate(props, 0.700, function() {

            extElement.hide();

        }, 'easeOut', 'run');

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Animates an element by changing the height from 0 to its original height
 * 
 * 
 * 
 * @param {Object}
 * 
 * extElement
 * 
 * @param {Object}
 * 
 * callback
 * 
 */

function socialAnimateShow(extElement, callback) {

    try {

        // if (Ext.isIE) {

        // extElement.dom.style.display = '';

        // callback();

        // return;

        // }

        if (extElement.getHeight() !== 0) {

            // do not animate show if it is already displayed

            return;

        }

        extElement.show();

        extElement.setStyle('overflow', 'hidden');

        var props = {

            height : {

                from : 0,

                to : extElement.dom.getAttribute('originalHeight')

            }

        };

        extElement.animate(props, 0.700, callback, 'easeOut', 'run');

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialAnimateWidth(extElement, from, to, callback) {

    // if (Ext.isIE) {

    // extElement.dom.style.display = '';

    // callback();

    // return;

    // }

    if (extElement.getHeight() !== 0) {

        // do not animate show if it is already displayed

        return;

    }

    // extElement.show();

    // extElement.setStyle('overflow', 'hidden');

    var props = {

        height : {

            from : 0,

            to : extElement.dom.getAttribute('originalHeight')

        }

    };

    extElement.animate(props, 0.700, callback, 'easeOut', 'run');

}

function socialMouseOverColleague(id) {

    Ext.select('#colleague' + id + ' a').show();

}

function socialSetPostOverAndOutEvents() {

    try {

        /**
         * 
         * @event
         * 
         * @name .postouter on mouseenter
         * 
         * @param {Object}
         * 
         * e
         * 
         */

        Ext.select('.postouter').on('mouseenter', function(e) {

            try {

                if (e.target.id === '') {

                    return;

                }

                Ext.select('.socialdeletepost').hide();

                Ext.select('.ss_recipient').hide();

                Ext.select('#' + e.target.id + ' .socialdeletepost').show();

                Ext.select('#' + e.target.id + ' .ss_recipient').show();

            } catch (e) {

                if (typeof console != 'undefined') {

                    console.error('postouter mouseenter ' + e);

                }

            }

        });

        Ext.select('.postouter').on('mouseleave', function(e) {

            try {

                Ext.select('.socialdeletepost').hide();

                Ext.select('.ss_recipient').hide();

                return true;

            } catch (e) {

                if (typeof console != 'undefined') {

                    console.error('postouter mouseleave ' + e);

                }

            }

        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialSetReplyOverAndOutEvents() {

    try {

        var logger = new ssobjLogger(arguments);

        Ext.select('.replyouter').on('mousemove', function(e) {

            try {

                if (e.target.id === '') {

                    return true;

                }

                var targetId = e.target.id.replace('Td', '');

                Ext.select('.socialdeletereply').each(function(el, c, idx) {

                    if (el.getAttribute('id') != targetId) {

                        Ext.get(el).hide();

                    }

                })

                var selector = '#' + e.target.id.replace('Td', '') + ' .socialdeletereply';

                Ext.select(selector).show();

            } catch (e) {

                logger.log('replyouter mousemove ' + e);

            }

        });

        Ext.select('.replyouter').on('mouseleave', function(e) {

            try {

                Ext.select('.socialdeletereply').hide();

            } catch (e) {

                if (typeof console != 'undefined') {

                    console.error('replyouter mouseleave ' + e);

                }

            }

        });

        return true;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialSetColleaguesTooltip() {

    return;

    // Ext.select('.colleague').on('mouseover', function(e){

    //

    // var relatedId = e.getRelatedTarget().id;

    // if( console ) //logger.log('over e.target.id=' + e.target.id + '

    // ;e.getRelatedTarget().id=' + e.getRelatedTarget().id);

    // if (e.target.id.indexOf('colleague-div-') === 0 && (relatedId === '' ||

    // relatedId.indexOf('colleague') == -1)) {

    // var sendMessageId = e.target.id.replace('colleague-div-',

    // 'colleague-send-message-');

    // var element = document.getElementById(sendMessageId);

    // if( console ) //logger.log('over display=' + element.style.display);

    // if (element.style.display == 'none' || element.style.display.length ===

    // 0) {

    // Ext.get(sendMessageId).dom.style.display = 'inline';

    // }

    // }

    // //

    // // if( console ) //logger.log("over

    // e.getRelatedTarget().getAttribute('data-role')=" +

    // e.getRelatedTarget().getAttribute('data-role'));

    // // if (e.target.id.indexOf('colleague') === 0 && e.getRelatedTarget().id

    // === 'colleagues') {

    // //

    // // }

    //

    //

    //

    // });

    // //

    // Ext.select('.colleague').on('mouseout', function(e){

    // var relatedId = e.getRelatedTarget().id;

    // if( console ) //logger.log('out e.target.id=' + e.target.id + '

    // ;e.getRelatedTarget().id=' + e.getRelatedTarget().id);

    // if (e.target.id.indexOf('colleague-div-') === 0 && (relatedId === '' ||

    // relatedId.indexOf('colleague') == -1)) {

    // var sendMessageId = e.target.id.replace('colleague-div-',

    // 'colleague-send-message-');

    // var element = document.getElementById(sendMessageId);

    // if( console ) //logger.log('out display=' + element.style.display);

    // if (element.style.display !== 'none') {

    // Ext.get(sendMessageId).dom.style.display = 'none';

    // }

    // }

    // });

    //

    // Ext.select('[data-role=sendprivatemessage]').on('mouseover', function(e){

    // socialStopPropagation(e);

    // return false;

    // });

    // Ext.select('[data-role=sendprivatemessage]').on('mouseoout', function(e){

    // socialStopPropagation(e);

    // return false;

    // });

    //

    //

    // console.clear();

    // window.status = 'tooltip ready';

    // return;

    // var elements = document.getElementsByClassName('colleague');

    // if( console ) //logger.log('onReady ' + elements.length);

    // for (var i = 0; i < elements.length; i++) {

    // var element = elements[i];

    // if (element.id == 'colleague{internalid}') {

    // // this is the template

    // continue;

    // }

    // // alert('tip-colleague-' + element.getAttribute('data-colleague-empid'))

    // new Ext.ToolTip({

    // title: '<a href="#">Colleague ' + element.innerHTML + '</a>',

    // id: 'tip-colleague-' + element.getAttribute('data-colleague-empid'),

    // target: element.id,

    // anchor: 'left',

    // html: "<br /><a href='#' onclick='socialSendPrivateMessage(" +

    // element.getAttribute('data-colleague-empid') + ")' style='margin-left:

    // 20px'>Send Private Message</a><br /><br />",

    // autoWidth: true,

    // autoHide: false,

    // closable: false,

    // hideDelay: 1500,

    // dismissDelay: 0,

    // contentElX: 'colleagueMenu', // load content from the page

    // listeners: {

    //

    // 'renderX': function(){

    //

    // this.header.on('click', function(e){

    // e.stopEvent();

    // Ext.Msg.alert('Link', 'Link to something interesting.');

    //

    // }, this, {

    // delegate: 'a'

    // });

    // }

    // }

    // });

    // }

    // console.clear();

    // window.status = 'tooltip ready';

}

function onRichEditorFocus(el) {

    try {

        // show reply button on focus on comment box

        if (el.id.indexOf("nfpostcommenteditor") == 0) {

            setTimeout(function() {

                var id = "btnreplycontainer" + el.id.replace("nfpostcommenteditor", "");

                Ext.get(id).dom.style.display = "";

            }, 400);

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function onRichEditorBlur(el) {

    try {

        // hide reply button on blur from comment box

        if (el.id.indexOf("nfpostcommenteditor") == 0) {

            setTimeout(function() {

                var id = "btnreplycontainer" + el.id.replace("nfpostcommenteditor", "");

                Ext.get(id).dom.style.display = "none";

            }, 400);

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowReplyContainer(id) {

    try {

        // Ext.select('.replyContainer').hide();

        // return;

        var elements = Ext.select('.replyContainer').elements;

        for (var i = 0; i < elements.length; i++) {

            var element = elements[i];

            if (element.id != 'replyContainer' + id) {

                // Ext.get(elements[i]).setStyle('height', '0');

                // socialAnimateHide(Ext.get(elements[i]));

            }

        }

        socialAnimateShow(Ext.get('replyContainer' + id), function() {

            // Ext.get('replyContainer' + id).scrollIntoView();

            // Ext.get('replyContainer'+id).scrollIntoView();

            Ext.get('richEditorReplyTextBox' + id).focus();

            // Ext.get('replyContainer'+id).scrollIntoView();

            // Ext.get('richEditorReplyTextBox'+id).scrollIntoView();

            var elements = Ext.select('.replyContainer').elements;

            for (var i = 0; i < elements.length; i++) {

                var element = elements[i];

                if (element.id != 'replyContainer' + id) {

                    if (Ext.isIE) {

                        // socialAnimateHide(Ext.get(elements[i]));

                        Ext.get(elements[i]).setStyle('display', 'none');

                    } else {

                        Ext.get(elements[i]).setStyle('height', '0');

                    }

                    // socialAnimateHide(Ext.get(elements[i]));

                }

            }

        });

        // hide other reply textboxes

        // socialAnimateHide(Ext.get('replyContainer' + id));

        // Ext.get('replyContainer' + id).fadeIn(1000);

        // Ext.get('replyContainer' + id).show();

    } catch (e) {

        ssapiHandleError(e);

    }

}

function onReplyTextBoxFocus(el) {

    if (el.value.indexOf('Write a comment'.tl()) > -1) {

        el.style.color = 'black';

        el.value = '';

    }

}

function onReplyTextBoxBlur(el) {

    if (el.value === '') {

        el.style.color = 'silver';

        el.value = 'Write a comment'.tl();

    }

}

function onFileBrowse() {
    document.getElementById("manualFileUpload").click();
}

function onManualFileUpload() {
    var file = document.getElementById("manualFileUpload").files;
    if (file.length > 0) {
        processManualFileUpload('manualFileUpload', 0, file[0]);   
    }
}

function onFileBrowseMessage() {
    document.getElementById("manualFileUploadMessage").click();
}

function onManualFileUploadMessage() {
    var file = document.getElementById("manualFileUploadMessage").files;
    if (file.length > 0) {
        processManualFileUpload('manualFileUploadMessage', 0, file[0]);        
    }
}

function onFileBrowseComment(element) {
    document.getElementById(element).click();
}

function onManualFileUploadComment(internalId) {
    var file = document.getElementById("manualFileUploadComment" + internalId).files;
    if (file.length > 0) {
        processManualFileUpload('manualFileUploadComment', internalId, file[0]);   
    }
}

function processManualFileUpload(name, internalId, file) {

    // check if an upload process is currently on-going
    if (ssapiHasValue(ssobjGlobal.ssapiShowUploadProgress)) {
        Ext.MessageBox.show({
            title: ssobjGlobal.TITLE,
            msg: 'File upload is currently in progress. You can wait for the current upload to finish or cancel the current upload before adding another file.',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING
        });
        // reset file upload value
        document.getElementById(name).value = '';
        return;
    }

    // check file size, do not allow more than 5mb
    if (file.size > ssobjGlobal.MAXIMUM_IMAGE_SIZE) {
        Ext.MessageBox.show({
            title : ssobjGlobal.TITLE,
            msg : 'The file ' + file.name + ' exceeds the maximum file size of 5MB.',
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.WARNING
        });
        // reset file upload value
        document.getElementById(name).value = '';
        return;
    }

    // If customer, check if upload is allowed
    if (nlapiGetContext().getRoleCenter() == "CUSTOMER") {
        if (ssapiGetSetting("custscript_ss_allow_customer_upload")) {
            var extName = file.name.substr(file.name.lastIndexOf(".")).toLowerCase();
            if (extName == ".zip" || extName == ".rar") {
                Ext.MessageBox.show({
                    title : ssobjGlobal.TITLE,
                    msg : "Customers are not allowed to upload zip and archived files.",
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.WARNING
                });
                // reset file upload value
                document.getElementById(name).value = '';
                return;
            }
        }
    }

    var dndElements = {};
    if (name == 'manualFileUpload') {
        dndElements.targetId = "richEditor";
        dndElements.targetWatermarkId = "richEditorWatermark";
        dndElements.progressBarContainerId = "dndprogressbarcontainer";
        dndElements.progressBarId = "dndprogressbar";
        dndElements.progressBarStatusId = "dndprogressbarstatus";
        dndElements.thumbnailContainerId = "dndthumbnailcontainer";
        dndElements.thumbnailImageId = "dndthumbnailimage";
        dndElements.filenameContainerId = "dndfilenamecontainer";
        dndElements.filenameId = "dndfilename";
        dndElements.canvasId = "dndcanvas";
        dndElements.dataId = "dnddata";
        dndElements.buttonId = "btnsharecontainer";
    } else if (name == 'manualFileUploadMessage') {
        dndElements.targetId = "richEditorReplyTextBoxPrivate";
        dndElements.targetWatermarkId = "richEditorReplyTextBoxWatermarkPrivate";
        dndElements.progressBarContainerId = "pmdndprogressbarcontainerPrivate";
        dndElements.progressBarId = "pmdndprogressbarPrivate";
        dndElements.progressBarStatusId = "pmdndprogressbarstatusPrivate";
        dndElements.thumbnailContainerId = "pmdndthumbnailcontainerPrivate";
        dndElements.thumbnailImageId = "pmdndthumbnailimagePrivate";
        dndElements.filenameContainerId = "pmdndfilenamecontainerPrivate";
        dndElements.filenameId = "pmdndfilenamePrivate";
        dndElements.canvasId = "pmdndcanvasPrivate";
        dndElements.dataId = "pmdnddataPrivate";
        dndElements.buttonId = "pmBtnSendContainerPrivate";
    } else {
        dndElements.targetId = "nfpostcommenteditor" + internalId;
        dndElements.targetWatermarkId = "nfpostcommenteditorWatermark" + internalId;
        dndElements.progressBarContainerId = "replydndprogressbarcontainer" + internalId;
        dndElements.progressBarId = "replydndprogressbar" + internalId;
        dndElements.progressBarStatusId = "replydndprogressbarstatus" + internalId;
        dndElements.thumbnailContainerId = "replydndthumbnailcontainer" + internalId;
        dndElements.thumbnailImageId = "replydndthumbnailimage" + internalId;
        dndElements.filenameContainerId = "replydndfilenamecontainer" + internalId;
        dndElements.filenameId = "replydndfilename" + internalId;
        dndElements.canvasId = "replydndcanvas" + internalId;
        dndElements.dataId = "replydnddata" + internalId;
        dndElements.buttonId = "btnreplycontainer" + internalId;
    }
    
    // check if has pending upload
    if (ssapiHasValue(ssobjGlobal.pendingUploads[dndElements.targetId])) {
        // confirm overwrite uploaded file
        var msg = 'You can attach only one file to each post. Do you want to overwrite the previous file?'.tl();
        Ext.Msg.show({
            title: 'Confirm'.tl(),
            msg: msg,
            buttons: Ext.Msg.YESNOCANCEL,
            fn: function(btn) {
                
                if (btn == 'cancel') {
                    // reset file upload value
                    document.getElementById(name).value = '';
                    return;
                }

                // cancel pending upload
                socialSuiteletProcessAsync('cancelUpload', dndElements.targetId, function(data) {
                    // if return is "ok", then delete file is successful, clear
                    // thumbnail/filename and proceed to upload new file
                    if (data == "ok")
                    {
                        // clear file preview
                        ssapiClearUploadedFilePreview(dndElements);
                        // enable share
                        ssapiEnableShareButton(true, dndElements);

                        // proceed to upload new file
                        ssapiPreProcessFileUpload(file, dndElements);
                        // reset file upload value
                        document.getElementById(name).value = '';
                    }
                });
            },
            icon: Ext.MessageBox.WARNING,
            buttons: {
                ok: "Yes",
                cancel: "No"
            },
            minWidth: 220
        });
        // HACK: center buttons
        Ext.select('.x-window-br').setStyle({
            'padding-left': '17px'
        });
        return;
    }

    // proceed to upload
    ssapiPreProcessFileUpload(file, dndElements);
    // reset file upload value
    document.getElementById(name).value = '';
}

function socialSetHelp(target, title, help, anchor, anchorOffset) {

    try {

        if (Ext.get(target).getWidth() === 0) {

            // hidden, so do not show tips

            return;

        }

        // add title on html

        help = '<b><img class="ssImgTooltipHelp" src="">&nbsp;&nbsp;' + title + '</b><br><br>' + help;

        if (typeof anchorOffset == 'undefined') {

            anchorOffset = 0;

        }

        var tooltip = (new Ext.ToolTip({

            title : '',

            id : target + 'Tooltip',

            target : target,

            anchor : anchor,

            html : help,

            autoWidth : false,

            maxWidth : 350,

            autoHide : false,

            closable : false,

            autoShow : false,

            hideDelay : 0,

            dismissDelay : 0,

            anchorOffset : anchorOffset,

            style : {

                color : 'red'

            },

            listeners : {

                'hide' : function() {

                    this.destroy();

                },

                'renderX' : function() {

                    this.header.on('click', function(e) {

                        e.stopEvent();

                        Ext.Msg.alert('Link', 'Link to something interesting.');

                    }, this, {

                        delegate : 'a'

                    });

                }

            }

        }));

        tooltip.show();

        socialApplyCSSToTooltip();

        ssobjGlobal.tooltips[target] = tooltip;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialSetHelpAutoHide(target, title, help, anchor, anchorOffset) {

    try {

        var autoWidth = true;

        var width;

        // if (Ext.isIE) {

        width = 400;

        autoWidth = false;

        // }

        (new Ext.ToolTip({

            title : null,

            ids : target + 'TooltipAutoHide',

            target : target,

            anchor : anchor,

            html : help,

            autoWidth : autoWidth,

            width : width,

            maxWidth : 400,

            autoHide : true,

            closable : false,

            autoShow : true,

            hideDelay : 0,

            dismissDelay : 0,

            style : {

                color : 'red'

            },

            bodyStyle : 'padding: 5px; background-color: lightyellow',

            listeners : {

                'hide' : function() {

                    // this.destroy();

                },

                'renderX' : function() {

                    this.header.on('click', function(e) {

                        e.stopEvent();

                        Ext.Msg.alert('Link', 'Link to something interesting.');

                    }, this, {

                        delegate : 'a'

                    });

                }

            }

        }));

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialHideHelp() {

    for ( var p in ssobjGlobal.tooltips) {

        // ssobjGlobal.tooltips[p].hide();

        ssobjGlobal.tooltips[p].destroy();

    }

}

function socialShowHelp() {

    try {

        // scroll to top first

        jQuery('body,html').animate({

            scrollTop : 0

        }, 200);

        // display help popups after a delay

        setTimeout(function() {

            // channels

            var help = '';

            help += '<li>' + 'The Channels menu initially displays your current channel subscriptions.'.tl() + '</li>';

            help += '<li>' + 'Use the Channels menu to subscribe or unsubscribe to a channel and to filter posts displayed on your news feed.'.tl() + '</li>';

            socialSetHelp('actionchannels', 'Channels'.tl(), help, 'bottom');

            // editor

            help = 'Post your message.'.tl();

            help += '<br />';

            help += 'To post, press Ctrl + Enter or click Share.'.tl();

            help += '<br />';

            help += 'Note: You can use the @ character followed by global search syntax to refer to records (for example, @John or @emp:John) and link your post to those records.'.tl();

            help += '<br />';

            help += 'You can reference more than one record per post.'.tl();

            // help += ' - ' + 'You can drag-and-drop a file from your computer

            // to

            // the Post text box to attach a file such as an image.'.tl();

            socialSetHelp('richEditor', 'Post'.tl(), help, 'bottom');

            // profile

            help = 'Click to edit your SuiteSocial profile'.tl();

            help += '<br />';

            help += '(Options include channel subscriptions, colleague subscriptions, record subscriptions, and email preferences).'.tl();

            socialSetHelp('editprofile', 'Edit Profile'.tl(), help, 'right');

            // coleagues

            help = '<li>' + 'The colleague subscription lists displays the people that you follow and those that are following you.'.tl() + '</li>';

            help += '<li>' + 'Click the Colleagues dropdown menu to select between the tile or list view.'.tl() + '</li>';

            socialSetHelp('actionnetworkview', 'Colleagues'.tl(), help, 'bottom');

            // colleague search

            socialSetHelp('searchColleaguesTextBox', 'Search'.tl(), 'Search using global search syntax. You can omit the "emp:" record type prefix.'.tl(), 'top');

        }, 500);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialAdjustEditorContainer() {

    try {

        if (Ext.isIE) {

            // align bottom of the editor to the bottom of the profile picture

            var profilePicBorderHeight = Ext.get('profilePicBorder').getHeight();

            var newHeight = profilePicBorderHeight - Ext.get('currentUserName').getHeight() - 5 /*
                                                                                                 * 
                                                                                                 * for
                                                                                                 * 
                                                                                                 * the
                                                                                                 * 
                                                                                                 * spacing
                                                                                                 * 
                                                                                                 */;

            Ext.get('richEditor').setHeight(newHeight);

        }

        var wallW = Ext.get('wall').getWidth();

        var profileContainerW = Ext.get('profileContainer').getWidth();

        var ALLOWANCE = 30;

        var newWidth = wallW - profileContainerW - ALLOWANCE;

        Ext.get('wholePosting').setWidth(newWidth);

        if (Ext.get('wholePosting').dom.style.visibility != 'visible') {

            Ext.get('wholePosting').fadeIn();

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * This function is called twice, before the merging of template and data and
 * 
 * after that.
 * 
 */

function socialSetTranslatedLabels() {

    try {

        var logger = new ssobjLogger(arguments);

        var translated = [];

        // execute this block only once

        logger.log('ssapiHasNoValue(ssobjGlobal.setTranslatedLabelsHasRan)=' + ssapiHasNoValue(ssobjGlobal.setTranslatedLabelsHasRan));

        if (ssapiHasNoValue(ssobjGlobal.setTranslatedLabelsHasRan)) {

            var elements = Ext.select('.ss_translateTitle').elements;

            logger.log('.ss_translateTitle elements.length=' + elements.length);

            if (elements.length > 0) {

                for (var i = 0; i < elements.length; i++) {

                    var d = elements[i];

                    var s = d.getAttribute('title');

                    if (ssapiHasValue(s)) {

                        d.setAttribute('title', s.tl());

                    }

                    translated.push(d);

                }

            }

            elements = [];

            elements = Ext.select('.ss_translateText').elements;

            logger.log('.ss_translateText elements.length=' + elements.length);

            if (elements.length > 0) {

                for (i = 0; i < elements.length; i++) {

                    d = elements[i];

                    s = d.innerHTML;

                    if (ssapiHasValue(s)) {

                        d.innerHTML = s.tl();

                        logger.log('d.innerHTML=' + d.innerHTML);

                    }

                    translated.push(d);

                }

            }

            // mark as translated

            for (i = 0; i < translated.length; i++) {

                d = translated[i];

                d.setAttribute('data-translated', 'true');

            }

            ssobjGlobal.setTranslatedLabelsHasRan = true;

        }

        // innerHTML

        var ids = [ 'noMorePosts', 'socialTileColleagues', 'socialColleagueSearchHeader', 'aSendPrivateMessage', 'editProfile2', 'socialAllChannel', 'channelSelectorHeader', 'postCommentLink{internalid}', 'aShowMoreComments{internalid}', 'replyTextBox{internalid}' ];

        for (i = 0; i < ids.length; i++) {

            var id = ids[i];

            // logger.log('id=' + id);

            // it is possible for an element to have images or other symbols as

            // part of the label. for translation purposes, we only want to

            // translate the word.

            // to handle this, we specify a child with class ss_label and

            // translate these children only

            if (ssapiHasNoValue(id)) {

                continue;

            }

            var selector = '[id="' + id + '"].ss_label';

            // not sure why the code below fails

            // var selector = '#' + id + '.ss_label';

            try {

                var childLabels = Ext.select(selector).elements;

            } catch (e) {

                if (Ext.isIE) {

                    continue;

                }

                throw e;

                // alert('e=' + e + '; selector=' + selector);

                continue;

            }

            if (childLabels.length > 0) {

                for (var c = 0; c < childLabels.length; c++) {

                    d = childLabels[c];

                    if (d.getAttribute('data-translated') == 'true') {

                        // prevent double translation

                        continue;

                    }

                    var label = d.getAttribute('data-label');

                    if (ssapiHasNoValue(label)) {

                        label = d.innerHTML;

                    }

                    Ext.get(d).update(label.tl());

                    d.setAttribute('data-translated', 'true');

                }

            } else {

                d = Ext.get(id).dom;

                if (d.getAttribute('data-translated') == 'true') {

                    // prevent double translation

                    continue;

                }

                label = d.getAttribute('data-label');

                if (ssapiHasNoValue(label)) {

                    label = d.innerHTML;

                }

                Ext.get(id).update(label.tl());

                d.setAttribute('data-translated', 'true');

            }

        }

        // values

        ids = [ 'searchColleaguesTextBox', 'btnPost' ];

        for (i = 0; i < ids.length; i++) {

            id = ids[i];

            d = Ext.get(id).dom;

            if (d.getAttribute('data-translated') == 'true') {

                // prevent double translation

                continue;

            }

            var value = Ext.get(id).dom.value;

            Ext.get(id).dom.value = value.tl();

            d.setAttribute('data-translated', 'true');

        }

        Ext.get('btnPost').dom.value = '    ' + 'Share'.tl() + '    ';

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialCenterWall(event) {

    try {

        if (ssapiHasValue(ssobjGlobal.recordType)) {

            // do not center when in the tab

            return;

        }

        var bodyWidth = Ext.getBody().getViewSize().width;

        // console.log('bodyWidth='+bodyWidth)

        if (bodyWidth === 0) {

            // this will happen if the page is in a iframe like in the

            // suitesocial tab

            var ext = window.top.Ext || parent.Ext;

            bodyWidth = parseInt(ext.getBody().getViewSize().width, 10) - 100;

        }

        // console.log('mainTableWidth='+mainTableWidth)

        var mainTableWidth = Ext.get('maintable').getStyle('width').replace('px', '');

        var maintableLeft = (bodyWidth - mainTableWidth) / 2;

        if (maintableLeft < 0) {

            // do not let the left edge to go out of the screen.

            // this may happen if the client width is smaller than the news feed

            // width

            maintableLeft = 1;

        }

        Ext.get('maintable').setLeft(maintableLeft);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function onClearSearchNewsFeed() {

    // clear input value

    Ext.get('searchNewsFeed').dom.value = '';

    // call search news feed keyup event; mock event object to pass the input as

    // target element

    onSearchNewsFeedKeyup({

        target : Ext.get('searchNewsFeed').dom

    });

}

/*
 * 
 * Search news feed
 * 
 */

/**
 * 
 * @event
 * 
 * @param {Object}
 * 
 * event
 * 
 */

function onSearchNewsFeedKeyup(event) {

    try {

        // Ext.get('placeHolderColleagueOthers').dom.innerHTML = HOURGLASS;

        var e = event || window.event;

        // Standard or IE model

        var target = e.target || e.srcElement;

        var str = target.value;

        str = str.replace('@', '');

        str = str.replace('emp:', '');

        // hide/show search icon; show/hide clear icon

        if (str.length > 0) {

            Ext.get(target).setStyle('background-image', 'none');

            Ext.select('.ss_search_nf_container .ss_search_input_clear').show();

        } else {

            Ext.get(target).setStyle('background-image', '');

            Ext.select('.ss_search_nf_container .ss_search_input_clear').hide();

        }

        if (str.length > 0 && str.length < 3) {

            // ssobjGlobal.newsFeedSearchQV = new Ext.ToolTip({

            // target: 'searchNewsFeed',

            // html: 'Type at least 3 characters.'.tl(),

            // anchor: 'bottom',

            // dismissDelay: 10000 // auto hide after 10 seconds

            // });

            //

            // ssobjGlobal.newsFeedSearchQV.show();

            Ext.get('searchNewsFeedInfo').dom.innerHTML = 'Type at least 3 characters.'.tl();

            return false;

        }

        // proceed to search news feed

        socialSearchNewsFeed();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Performs search on news feed 1 second after the user stopped typing. It is
 * 
 * also called when a hashtag is clicked.
 * 
 */

function socialSearchNewsFeed() {

    // do UI preps

    Ext.get('searchNewsFeedInfo').dom.innerHTML = '';

    Ext.get('nfposts').update('');

    Ext.get('justPosted').update('');

    Ext.get('mainpane').repaint();

    Ext.get('mainpane').repaint(); // just needed to call twice

    Ext.get('showMore').update(ssobjGlobal.loadingImageMarkupSmall);

    Ext.get('showMore').show();

    Ext.get('noMorePosts').dom.style.display = 'none';

    if (ssobjGlobal.newsFeedFilter) {

        clearTimeout(ssobjGlobal.newsFeedFilter);

    }

    // perform search 1 second after the user stopped typing

    var MILLISECONDS_BEFORE_SEARCHING_FEED = 1000;

    // 1 second

    ssobjGlobal.newsFeedFilter = setTimeout('socialSearchStringChanged();', MILLISECONDS_BEFORE_SEARCHING_FEED);

}

function socialSearchStringChanged() {

    try {

        var logger = new ssobjLogger(arguments);

        Ext.get('searchNewsFeedInfo').dom.innerHTML = '';

        if (ssobjGlobal.isNewFeedProcessing) {

            // in case the use is too fast in clicking channels

            // abort currently running xmlrequest

            socialSuiteletAbortNewsFeedRequest();

            ssobjGlobal.isNewFeedProcessing = false;

        }

        ssobjGlobal.offset = 0;

        ssobjGlobal.lessThanId = null;

        // socialSetEditorLookAndChannel(el, channelId);

        socialShowPosts();

        logger.log('end');

        // Ext.get('msgTextBox').focus();

        // this is done to hide the right border of the selected channel

        // socialPositionPatcher();

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * A more generic way of triggering events since
 * 
 * Ext.get('socialChannelsHeader').dom.click(); does not work on Safari
 * 
 * 
 * 
 * @param {Object}
 * 
 * el
 * 
 * @param {Object}
 * 
 * eventName
 * 
 */

function ssapiTriggerEvent(el, eventName) {

    try {

        if (typeof el == 'string') {

            el = Ext.get(el).dom;

        }

        var evt = document.createEvent("HTMLEvents");

        evt.initEvent(eventName, true, true);

        el.dispatchEvent(evt);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialLoadMorePosts() {

    try {

        var logger = new ssobjLogger(arguments);

        if (Ext.get('showMore').getWidth() === 0) {

            // there are no more posts

            return;

        }

        var showMoreTop = Ext.get('showMore').getTop();

        var visibleHeightStart = Ext.getBody().getScroll().top;

        var visibleHeightEnd = visibleHeightStart + Ext.getBody().dom.clientHeight;

        logger.log('showMoreTop=' + showMoreTop + '; visibleHeightStart=' + visibleHeightStart + '; visibleHeightEnd=' + visibleHeightEnd);

        // higher allowance value will load the next posts faster since the

        // lower limit is increased

        var allowance = 800;

        if (showMoreTop >= visibleHeightStart && showMoreTop <= visibleHeightEnd + allowance) {

            logger.log('displayed');

            socialShowPosts();

        } else {

            logger.log('not');

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialRetrieveRepliesOfPostHandler(data) {

    try {

        // try {

        // if( console ) //logger.log('req.status=' + req.status);

        // // alert(req.responseText);

        // var resp = xmlRequest.responseText;

        // if( console ) //logger.log('resp=' + resp);

        // console.clear();

        var parentId = data.parentId;

        var resultsEntities = data.resultset;

        if (resultsEntities === null) {

            return;

        }

        // if( console ) //logger.log('resultsEntities.length=' +

        // resultsEntities.length);

        // if( console ) //logger.log('parentId=' + parentId + '

        // replies.length=' + replies.length);

        var repliesFlatEntities = ssapiConvertResultsEntitiesToFlatEntities(resultsEntities);

        // if( console ) //logger.log('parentId=' + parentId + '

        // repliesEntities.length=' + repliesEntities.length);

        // if( console ) //logger.log('repliesEntities=' +

        // JSON.stringify(repliesEntities));

        // if( console ) //logger.log('parentId=' + parentId);

        if (ssapiHasNoValue(repliesFlatEntities)) {

            return;

        }

        // socialShowRepliesOfPostHandler(parentId, repliesFlatEntities);

        var myTplReply = new Ext.XTemplate('<tpl for=".">' + document.getElementById('tmpreply').innerHTML + '</tpl>');

        myTplReply.append(document.getElementById('replies' + parentId), repliesFlatEntities);

        Ext.select('.reply').fadeOut(0);

        // if( console ) //logger.log('parentId=' + parentId + ' DONE');

        // }

        // catch (e) {

        // if( console ) //logger.log('socialRetrieveRepliesOfPostHandler()

        // description=' + e.description + ' message=' + e.message);

        // alert('socialRetrieveRepliesOfPostHandler() e=' + e + '; message=' +

        // e.message + ' repliesFlatEntities=' +

        // JSON.stringify(repliesFlatEntities));

        // }

    } catch (e) {

        ssapiHandleError(e);

    }

}

function ssapiHasNoValue(param) {

    if (typeof param == 'undefined') {

        return true;

    }

    if (param === null) {

        return true;

    }

    if (param === '') {

        return true;

    }

    return false;

}

//

function ssapiConvertResultsEntitiesToFlatEntities(resultsEntities) {

    try {

        if (ssapiHasNoValue(resultsEntities)) {

            return null;

        }

        if (resultsEntities.length === 0) {

            return null;

        }

        // get column id array

        var columns = resultsEntities[0].columns;

        var columnIds = [];

        for ( var p in columns) {

            columnIds.push(p);

        }

        //

        var flatEntities = [];

        for ( var i in resultsEntities) {

            var nameValuePairs = {};

            var entity = resultsEntities[i];

            for ( var c in columnIds) {

                var columnId = columnIds[c];

                if (typeof entity.columns[columnId] != 'object') {

                    nameValuePairs[columnId] = entity.columns[columnId];

                } else {

                    // if object, must be a parent, get the id of the parent

                    nameValuePairs[columnId] = (entity.columns[columnId]).internalid;

                }

            }

            flatEntities.push(nameValuePairs);

        }

        return flatEntities;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialShowRepliesOfPostHandler(postid, data) {

    try {

        var myTplReply = new Ext.XTemplate('<tpl for=".">' + document.getElementById('tmpreply').innerHTML + '</tpl>');

        var newEl = myTplReply.append(document.getElementById('replies' + postid), data);

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialGetTextContent(node) {

    return node.textContent || node.innerText || node.wholeText || node.nodeValue || '';

}

function socialSocialConvertRecordNamesToObjectRich(elId) {

    try {

        var msg = '';

        var nodes = socialGetChildNodes(elId);

        for (var i = 0; i < nodes.length; i++) {

            var node = nodes[i];

            var realNode = node.node;

            if (ssapiHasValue(node.originaltext)) {

                var mention = {};

                mention.name = node.text;

                mention.objecttype = realNode.getAttribute('objecttype');

                mention.recordtype = realNode.getAttribute('recordtype');

                mention.id = realNode.getAttribute('recordid');

                if (mention.objecttype == 'record') {

                    mention.url = nlapiResolveURL('record', mention.recordtype, mention.id);

                } else {

                    mention.url = '/app/common/search/searchresults.nl?searchid=' + mention.id;

                    mention.data = ssobjGlobal.lastSearchData;

                }

                msg += '{' + JSON.stringify(mention) + '}';

            } else {

                var outerHTML = realNode.outerHTML;

                outerHTML = outerHTML || '';

                outerHTML = outerHTML.toLowerCase();

                if (outerHTML == '<br>') {

                    msg += '[br]';

                } else {

                    var NBSP_CHAR_CODE = 160;

                    if (node.text == String.fromCharCode(NBSP_CHAR_CODE)) {

                        msg += ' ';

                    } else {

                        var hasDiv = outerHTML.indexOf('<div>') > -1;

                        var hasP = outerHTML.indexOf('<p>') > -1;

                        var hasBr = outerHTML.indexOf('<br>') > -1;

                        if (hasDiv) {

                            msg += '[br]';

                        }

                        msg += node.text || '';

                        if (hasDiv === false && (hasBr || hasP)) {

                            msg += '[br]';

                        }

                    }

                }

            }

        }

        return msg;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialAddPostPrivate(msg) {

    try {

        if (msg.length > 4000) {

            socialShowInfo('Post should not be more than 4000 characters', 'SuiteSocial');

            return;

        }

        document.getElementById("processing").innerHTML = '';

        Ext.get('processing').hide();

        // socialSetEditorEnabled(false);

        // document.getElementById("btnPost").disabled = true;

        Ext.get('hourglass').show();

        Ext.get('processing').fadeIn(0);

        ssobjGlobal.channelBeforePrivateMessage = ssobjGlobal.channel; // save

        // current

        // channel

        ssobjGlobal.channel = '1'; // private message

        ssobjGlobal.recordType = null;

        ssobjGlobal.recordId = null;

        // get selected recipients

        var selectedRecipients = jQuery('#socialpPrivateMessageRecipientSelectPrivate').val();
        var recipients = [];

        jQuery('#socialpPrivateMessageRecipientSelectPrivate option').each(function() {
            var optionValue = jQuery(this).val();
            if (selectedRecipients.indexOf(optionValue) > -1) {
                recipients.push({
                    id : jQuery(this).val(),
                    name : jQuery(this).text()
                });
            }
        });
        ssobjGlobal.recipient = recipients;

        socialAddPostCommon(msg, "richEditorReplyTextBoxPrivate");

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Client part of creating a post via news feed wall. This is the start the
 * 
 * whole process below. <br>
 * 
 * <div class='diagram'> <br>
 * 
 * <br>
 * 
 * User-->Browser: Create post\n socialAddPost() <br>
 * 
 * Browser-->Server: Send post\n addPost action <br>
 * 
 * Server-->Server: Create records\n for subscribers\naddPost() <br>
 * 
 * Server-->Browser: Send internal id and\n markup to render <br>
 * 
 * Browser-->User: Render post\n socialAddPostHandler() <br>
 * 
 * <br>
 * 
 * </div>
 * 
 */

function socialAddPost() {

    try {

        // performance logger

        var logger = new ssobjLogger("socialAddPost");

        // check if button is disabled

        if (Ext.get("btnsharecontainer").hasClass("disabled")) {

            return;

        }

        var msgRich = socialSocialConvertRecordNamesToObjectRich('richEditor');

        if (msgRich.length > 4000) {

            socialShowInfo('Post should not be more than 4000 characters.'.tl() + '<br>' + 'Current post length:'.tl() + ' ' + msgRich.length, 'SuiteSocial');

            return;

        }

        document.getElementById("processing").innerHTML = '';

        var msg = socialGetEditorText();

        if (msg.trim() === '') {

            socialGetEditorEl().focus();

            Ext.get('processing').hide();

            document.getElementById("processing").innerHTML = 'Please provide message';

            Ext.get('processing').fadeIn(1000);

            return;

        }

        // check msg if there are curly braces

        if (msg.indexOf('{{') > -1 || msg.indexOf('}}') > -1) {

            socialShowWarning('Double curly braces are not allowed.'.tl(), 'SuiteSocial');

            return;

        }

        // S3 - Issue 248264 : [SuiteSocial] News feed UI: New lines in posts

        // are not saved.

        // if message has new line at the end, remove it

        var msgLength = msgRich.length;

        if (msgLength >= 4) {

            if (msgRich.substr(msgLength - 4, 4) == '[br]') {

                msgRich = msgRich.substr(0, msgLength - 4);

            }

        }

        // if channel is status update, do not allow new lines

        if (ssobjGlobal.channel == '13' /* status change channel id */&& msgRich.indexOf('[br]') > -1) {

            socialGetEditorEl().focus();

            Ext.get('processing').hide();

            document.getElementById("processing").innerHTML = 'New lines are not allowed for status changes.'.tl();

            Ext.get('processing').fadeIn(1000);

            return;

        }

        Ext.get('processing').hide();

        socialSetEditorEnabled(false);

        // document.getElementById("btnPost").disabled = true;

        Ext.get('hourglass').show();

        // document.getElementById("richEditor").style.backgroundColor =

        // '#efefef';

        Ext.get('processing').fadeIn(0);

        // make sure this is null

        ssobjGlobal.recipient = null;

        socialAddPostCommon(msgRich, "richEditor");

        // log end time

        logger.log("*********************************** END: Adding Post ***********************************");

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialAddPostCommon(msg, targetId, isGivingRecognition) {

    try {

        if (ssapiHasValue(ssobjGlobal.imageFile)) {

            // if there is file attachment

            ssobjGlobal.msg = msg;

            socialUploadFile(socialAfterUploadFile);

            return false;

        }

        var feed = {};

        // S2 Issue 240607 : SuiteSocial>Posting to 'Public' after filtering

        // news feed by channel causes error "You are not allowed to post on

        // this channel. channel=0"

        if (ssobjGlobal.channel == '0') {

            // 0 is also being used to represent no channel.

            // set it to null instead

            ssobjGlobal.channel = null;

        }

        feed.msg = msg;

        feed.record_type = ssobjGlobal.recordType;

        feed.record_id = ssobjGlobal.recordId;

        feed.recipient = ssobjGlobal.recipient;

        feed.orgValues = ssapiHasValue(ssobjGlobal.orgValues) ? ssobjGlobal.orgValues : '';

        /**
         * 
         * @description ssobjGlobal.channel defines if a post is saved under a
         * 
         * channel or not.<br>
         * 
         * This is set to the id of desired channel to post under
         * 
         * that channel.<br>
         * 
         * Set this to null to save the post with no channel.
         * 
         * @name postToChannel_INLINE_COMMENT
         * 
         * @function
         * 
         */

        feed.channel = ssobjGlobal.channel;

        ssobjGlobal.isNewFeedProcessing = true;

        // suppress alert msg on error

        feed.suppress_alert = true;

        // form data for quick posting

        var data = ssapiFormDataForPosting(msg, null /* parentId */);

        feed.name = data.id; // save temp id for the name field in news feed

        // record

        // get dnd file if dnd is enabled

        if (ssapiHasValue(targetId) && ssapiHasValue(ssobjGlobal.dndElements)) {

            // get dnd elements

            var dndElements = ssobjGlobal.dndElements[targetId];

            // get dnd file if any

            var dadFile = socialGetDnDFileData(dndElements);

            if (ssapiHasValue(dadFile)) {

                feed.dadFile = dadFile;

                feed.targetId = targetId;

                data.dadFile = dadFile;

                data.dndElements = dndElements;

                // clear thumbnail/filename preview

                ssapiClearUploadedFilePreview(dndElements);

            }

        }

        // keep post data in the post html

        data.postdata = JSON.stringify(feed);

        // immediately post to wall

        socialAddPostHandler(data);

        feed.isGivingRecognition = isGivingRecognition;

        ssobjGlobal.orgValues = '';

        // then persist to database asynchronously

        socialSuiteletProcessAsync('addPost', feed, function(newId) {

            // check if newId has valid value

            if (ssapiIsNumber(newId)) {

                // successfully saved post; newId = internal id of the newly

                // posted & saved msg

            } else {

                // failed saving post; update look of post to provide a retry

                // functionality

                socialShowPostError(feed.name, true, newId);

            }

            // remove classes: newpost and newreply

            Ext.select('#justPosted .newreply').removeClass('newreply');

            Ext.select('#justPosted .newpost').removeClass('newpost');

        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

function socialAddPostHandler(data) {

    try {

        var logger = new ssobjLogger(arguments);

        // EMS: document.getElementById("chartPost").innerHTML = '';

        // get template for post, and set to Ext object

        var tmpPost = document.getElementById('tmpPost').innerHTML;

        var myTplPost = new Ext.XTemplate('<tpl for=".">' + tmpPost + '</tpl>');

        // set the latest displayed post id

        ssobjGlobal.latestDisplayedPostId = data.id;

        // get selected channel name

        var selectedChannelName = '';

        var selectedChannelId = '';

        var channelElements = Ext.select('#actionchannelslistul .active').elements;

        if (channelElements.length > 0) {

            selectedChannelId = channelElements[0].getAttribute('data-channelId');

            // set channel name on newsfeed title bar

            selectedChannelName = Ext.select('#actionchannelslistul .active div span').elements[0].innerHTML;
        }

        logger.log('selectedChannelName=' + selectedChannelName);

        // set some more info in the data to post

        data.image = data.image.replace('/suitebundle/', '/suitebundle26198/');

        data.internalid = data.id;

        data.currentUserAvatar = ssobjGlobal.currentUserAvatar;

        data.fileIcon = ssobjGlobal.imagesUrlsObj['file.png'];

        if (data.channel !== '') {

            // nothing here

        }

        if (data.channel.length > 0) {
            if (selectedChannelName == data.channel && selectedChannelId == data.channel_id) {
                data.channel_label = '';
            } else {
                data.channel_label = ' &rarr; ' + data.channel;
            }

            if (typeof data.recipient == 'object') {
                data.recipient = socialGetRecipientsDisplay(data.recipient, data.id);
            } else {
                data.recipient = '';
            }

        }

        if (ssapiHasValue(data.orgValues)) {
            data.orgValues = socialGetOrgValuesDisplay(data.orgValues);
        }

        // get dnd file if any

        if (ssapiHasValue(data.dadFile)) {

            data.file = ssapiGetFileHTMLToRender(data.id, data.dadFile, data.dndElements);

        }

        // set hourglass src

        data.hourglass = ssobjGlobal.imagesUrlsObj['loading-ani.gif'];

        // if comment is not allowed in channel, hide comment box

        if (ssapiHasValue(ssobjGlobal.channel)) {

            var channel = socialGetChannelObj(ssobjGlobal.channel);

            if (channel.allowComments === false && channel.id != RECORD_CHANGES_ID) {

                data.allow_comments = "hidden";

            } else {

                data.allow_comments = "";

            }

        } else {

            data.allow_comments = "";

        }

        // set like display

        data.like_label = ssobjGlobal.LABEL_LIKE;

        data.like_icon = ssobjGlobal.imagesUrlsObj['suitesocial_unlike.png'];

        data.like_label_style = '';

        data.likers_style = 'style="display: none;"';

        data.likes_info = "[]";

        // insert new post

        var newEl = myTplPost.insertFirst(document.getElementById('justPosted'), [ data ], false);

        jQuery('#nfpostmessage' + data.id).like({

            internalid : data.id,

            like_label_style : data.like_label_style,

            like_icon : data.like_icon,

            like_label : data.like_label,

            likers_style : data.likers_style,

            likers_label : data.likers_label,

            likes_info : data.likes_info,

            like_type : 'post'

        });

        // EMS: socialShowPhotos();

        var chartId = 'socialChart' + data.id;

        if (data.chartdata) {

            // EMS: socialShowChart(data.chartdata, chartId, 'Title', false);

        }

        // enable dnd on comment box

        var internalId = data.id;// itemsToBeDisplayed[i].internalid;

        var dndElements = {};

        dndElements.targetId = "nfpostcommenteditor" + internalId;

        dndElements.targetWatermarkId = "nfpostcommenteditorWatermark" + internalId;

        dndElements.progressBarContainerId = "replydndprogressbarcontainer" + internalId;

        dndElements.progressBarId = "replydndprogressbar" + internalId;

        dndElements.progressBarStatusId = "replydndprogressbarstatus" + internalId;

        dndElements.thumbnailContainerId = "replydndthumbnailcontainer" + internalId;

        dndElements.thumbnailImageId = "replydndthumbnailimage" + internalId;

        dndElements.filenameContainerId = "replydndfilenamecontainer" + internalId;

        dndElements.filenameId = "replydndfilename" + internalId;

        dndElements.canvasId = "replydndcanvas" + internalId;

        dndElements.dataId = "replydnddata" + internalId;

        dndElements.buttonId = "btnreplycontainer" + internalId;

        ssapiEnableDnDOnSuiteSocial(true, dndElements, socialFileUploadHandler);

        // increment offset

        ssobjGlobal.offset = ssobjGlobal.offset + 1;

        try {

            // show post by fading in

            Ext.select('.newreply').fadeIn(FADEIN_SPEED);

            Ext.select('.newpost').fadeIn(FADEIN_SPEED);

        } catch (e) {

            // ignore error in ie

        }

        // animate post by fading in and fading out the color

        Ext.select('.newpost').animate({// animation control object

            backgroundColor : {

                from : '#EEEEBB',

                to : '#ffffff'

            }

        }, 2.0, null, 'easeOut', 'color');

        /*
         * 
         * Ext.select('.newreply').animate({// animation control object
         * 
         * backgroundColor : { from : '#EEEEBB', to : '#ffffff' } }, 2.0, null,
         * 
         * 'easeOut', 'color');
         * 
         */

        if (ssapiHasNoValue(ssobjGlobal.colleagueEmpId)) {

            // when channel is selected

            socialSetEditorEnabled(true);

            document.getElementById("richEditor").innerHTML = '';

            onRichEditorChange(Ext.get("richEditor").dom);

            // check if not on record page
            if (ssapiHasNoValue(ssobjGlobal.recordType)) {
            
                // restore initial state of editor upon load of channel
                // channel 0 = public channel
                var currentChannel = ssapiHasNoValue(selectedChannelId) ? "0"  : selectedChannelId;
                socialSetEditorEnabled(ssobjGlobal.channelEditorEnabled[currentChannel]);
            }
            // editor always enabled on record page.
        } else {

            // disable when a colleague is selected
            socialSetEditorEnabled(false);
        }

        document.getElementById("processing").innerHTML = '';

        // EMS: socialSetPostOverAndOutEvents();

        // EMS: Ext.MessageBox.hide();

        Ext.get('hourglass').hide();

        // we need to restore the channel after a private message

        if (ssobjGlobal.channel == DIRECT_MESSAGES_ID) {

            ssobjGlobal.channel = ssobjGlobal.channelBeforePrivateMessage;

        }

        // Ext.get('richEditorReplyTextBox' + data.internalid).update('');

        // apply rich editor on key up functionality

        Ext.select('#justPosted .newpost .replyEditor').on('keyup', function(event) {

            // show/hide watermark

            if (ssapiIsIE()) {

                onRichEditorChange(event.target);

            }

            var logger = new ssobjLogger(arguments);

            logger.log('replyEditor keyup');

            var e = event || window.event;

            onRichEditorKeyup(e);

        });

        Ext.select('#justPosted .newpost .replyEditor').on('input', function(e) {

            onRichEditorChange(e.target);

        });

        // EMS: socialMakeCurrentUserLinkUnclickable();

        ssobjGlobal.isNewFeedProcessing = false;

    } catch (e) {

        ssapiHandleError(e);

    }

}

function ssapiFormDataForPosting(msg, parentId /* Optional */) {
    // form data for quick posting
    var idTemp = ssapiGetNewsFeedRandomNumber(ssobjGlobal.currentUserId); // generate

    // temporary
    // id
    var channelInfo = ssobjGlobal.channel == null ? {
        internalid : "",
        channelName : ""
    } : socialGetChannelObj(ssobjGlobal.channel);

    var formattedMsg = ssapiFormatMessageForPosting(msg, idTemp);
    var recordInfo = ssapiGetRecordNameAndLink(ssobjGlobal.recordType, ssobjGlobal.recordId, null /* subscription_type */);

    var data = {};
    data.id = idTemp; // put generated id
    data.date_created = ssapiGetLocaleDateTime(new Date());
    data.sender = ssobjGlobal.currentUserName;
    data.sender_id = ssobjGlobal.currentUserId;
    data.image = ssobjGlobal.currentUserAvatar;
    data.icon = ""; // EMS: custrecord_newsfeed_icon
    data.channel = channelInfo.channelName;
    data.channel_id = channelInfo.internalid;
    data.prefix = ""; // empty string, not related to an auto post

    // field/record
    data.message = formattedMsg;

    // if has recipient or private message channel, it's a private post
    data.private_post = ssapiHasValue(ssobjGlobal.recipient) || (data.channel_id == DIRECT_MESSAGES_ID);

    if (ssapiHasValue(ssobjGlobal.recipient)) {
        var recipients = ssobjGlobal.recipient;

        var list = [];
        for (var i = 0; i < recipients.length; i++) {
            var imageUrl = socialSearchForProfilePicOnUi(recipients[i].id);
            list.push({
                id : recipients[i].id,
                name : recipients[i].name,
                imageUrl : imageUrl
            });
        }
        data.recipient = list;
    } else {
        data.recipient = '';
    }

    if (ssapiHasValue(ssobjGlobal.orgValues)) {
        data.orgValues = ssobjGlobal.orgValues;
    } else {
        data.orgValues = '';
    }

    data.record_type = ssobjGlobal.recordType;
    data.record_id = ssobjGlobal.recordId;
    data.comment_count = 0; // count = 0 since it's a new post or a new comment
    data.subscription_type = ""; // EMS: custrecord_newsfeed_subs_type - will

    // have value for saved search posting
    data.recordName = recordInfo.recordName;
    data.recordNameWithLink = recordInfo.recordNameWithLink;
    data.url = recordInfo.url;
    data.link = recordInfo.link;
    data.color = ssapiHasValue(ssobjGlobal.recipient) ? "green" : "black";

    // check if a wall post or a comment
    if (ssapiHasValue(parentId)) {
        // set other data for comment
        var commentInfo = getNewCommentCtrAndBatch(parentId);

        data.delete_link = '<a id=replyDelete' + idTemp + ' title="Delete" href=# onclick="socialDeleteReply(\'' + idTemp + '\', \'' + parentId + '\'); return false;"><img src="' + ssobjGlobal.imagesUrlsObj['icon_clear_default.png'] + '" /></a>';

        data.ctr = commentInfo.ctr;
        data.batch = commentInfo.batch;
        data.parent = parentId;
        data.internalid = idTemp; // put generated id
    } else {
        // set other data

        data.delete_link = '<a title="Delete" href="#" onclick="socialDeletePost(\'' + idTemp + '\'); return false;"><img src="' + ssobjGlobal.imagesUrlsObj['icon_clear_default.png'] + '" /></a>';

    }

    return data;
}

function socialSearchForProfilePicOnUi(entityId) {
    // search colleague pane
    var imageSource = jQuery('.ss_colleagues_list li[data-colleague-empid="' + entityId + '"] img').attr('src');
    if (imageSource != undefined && imageSource != '') {
        return imageSource;
    }

    // search newsfeed posts
    imageSource = jQuery('.ss_nf_post_container .ss_nf_post_profile_pic[data-sender-entityid="' + entityId + '"]').attr('src');
    if (imageSource != undefined && imageSource != '') {
        return imageSource;
    }

    // search on server
    var filters = [ new nlobjSearchFilter('custrecord_suitesocial_profile_emp', null, 'is', entityId) ];
    var columns = [ new nlobjSearchColumn('custrecord_suitesocial_profile_image') ];
    var results = nlapiSearchRecord('customrecord_suitesocial_profile', null, filters, columns);

    if (results == null) {
        return ssobjGlobal.imagesUrlsObj['generic_person.jpg'];
    }

    var imageFileId = results[0].getValue('custrecord_suitesocial_profile_image');
    if (ssapiHasNoValue(imageFileId)) {
        return ssobjGlobal.imagesUrlsObj['generic_person.jpg'];
    }

    imageSource = nlapiResolveURL("mediaitem", imageFileId);

    return imageSource;
}

function getNewCommentCtrAndBatch(parentId) {

    var iCtr = 1, iBatch = 1;

    var repliesNode = document.getElementById('replies' + parentId);

    if (repliesNode != null) {

        var iCount = repliesNode.children.length - 1; // get actual comments

        // length (subtracted 1

        // because first element

        // is for "Show more

        // comments")

        iCount += 1; // add 1 for the newly posted comment

        // calculate ctr and batch

        var BATCH_COUNT = 5;

        var RADIX = 10;

        var c = iCount - 1;

        iCtr = c + 1;

        iBatch = parseInt((c + BATCH_COUNT) / BATCH_COUNT, RADIX);

    }

    return {

        ctr : iCtr,

        batch : iBatch

    };

}

function retryPost(feedName) {

    // update look of post to hide the error message

    socialShowPostError(feedName, false);

    // retrieve the post data

    var feed = document.getElementById('postdata' + feedName).innerHTML;

    feed = JSON.parse(feed);

    // animate post to indicate retrying/reposting

    Ext.select('#nfpostcontainer' + feedName).animate({// animation control

        // object

        backgroundColor : {

            from : '#EEEEBB',

            to : '#ffffff'

        }

    }, 2.0, null, 'easeOut', 'color');

    // then persist to database asynchronously

    socialSuiteletProcessAsync('addPost', feed, function(newId) {

        // check if newId has valid value

        if (ssapiIsNumber(newId)) {

            // successfully saved post; newId = internal id of the newly posted

            // & saved msg

        } else {

            // failed saving post; update look of post to provide a retry

            // functionality

            socialShowPostError(feedName, true, newId);

        }

    });

}

function socialShowPostError(feedName, bShow, objError) {

    // check if show or hide

    if (bShow) {

        // /////// render the post error message /////////

        // get template for error message, and set to Ext object

        var tmpWarning = document.getElementById('tmpWarningMsg').innerHTML;

        var myTplWarning = new Ext.XTemplate('<tpl for=".">' + tmpWarning + '</tpl>');

        // set default error message

        var strError = "Saving message failed.  Please click Retry button to repost your message.".tl();

        // get returned error message

        if (ssapiHasValue(objError)) {

            strError = objError.error || strError;

            // check session timeout message; change with this more appropriate

            // and doc-reviewed message

            if (strError == "Your session has timed-out. You need to login again.") {

                strError = "Your session has timed out. Log in again to post your message.";

            }

        }

        // insert error message

        var errorData = {};

        errorData.error_message = strError;

        errorData.internalid = feedName;

        errorData.icon_url = ssobjGlobal.imagesUrlsObj['icon_msg_alert.png'];

        myTplWarning.insertFirst(document.getElementById('nfpostcontent' + feedName), errorData, false);

        // add slide in effect

        // Ext.select('#warningMsg' + feedName).slideIn('t', {

        // easing : 'easeOut',

        // duration : 1

        // });

        Ext.select('#warningMsg' + feedName).fadeIn(1000);

    } else {

        // /////// hide the post error message /////////

        // remove the error message node

        var postNode = document.getElementById('nfpostcontent' + feedName);

        var errorNode = document.getElementById('warningMsg' + feedName);

        postNode.removeChild(errorNode);

    }

}

function retryReply(feedName) {

    // update look of post to hide the error message

    socialShowReplyError(feedName, false);

    // retrieve the post data

    var feed = document.getElementById('replydata' + feedName).innerHTML;

    feed = JSON.parse(feed);

    // animate post to indicate retrying/reposting

    Ext.select('#nfpostcommentmsgcontainer' + feedName).fadeIn(FADEIN_SPEED);

    // then persist to database asynchronously

    socialSuiteletProcessAsync('addReply', feed, function(newId) {

        // check if newId has valid value

        if (ssapiIsNumber(newId)) {

            // successfully saved post; newId = internal id of the newly posted

            // & saved msg

        } else {

            // failed saving post; update look of post to provide a retry

            // functionality

            socialShowReplyError(feedName, true, newId);

        }

    });

}

function socialShowReplyError(feedName, bShow, objError) {

    // check if show or hide

    if (bShow) {

        // /////// render the post error message /////////

        // get template for error message, and set to Ext object

        var tmpWarning = document.getElementById('tmpWarningMsgReply').innerHTML;

        var myTplWarning = new Ext.XTemplate('<tpl for=".">' + tmpWarning + '</tpl>');

        // set default error message

        var strError = "Saving message failed.  Please click Retry button to repost your message.".tl();

        // get returned error message

        if (ssapiHasValue(objError)) {

            strError = objError.error || strError;

        }

        // insert error message

        var errorData = {};

        errorData.error_message = strError;

        errorData.internalid = feedName;

        errorData.icon_url = ssobjGlobal.imagesUrlsObj['icon_msg_alert.png'];

        myTplWarning.insertFirst(document.getElementById('nfpostcommentmsgcontainer' + feedName), errorData, false);

        // add slide in effect

        // Ext.select('#warningMsgReply' + feedName).slideIn('t', {

        // easing : 'easeOut',

        // duration : 1

        // });

        Ext.select('#warningMsg' + feedName).fadeIn(1000);

    } else {

        // /////// hide the post error message /////////

        // remove the error message node

        var postNode = document.getElementById('nfpostcommentmsgcontainer' + feedName);

        var errorNode = document.getElementById('warningMsgReply' + feedName);

        postNode.removeChild(errorNode);

    }

}

ssobjGlobal.lastRepliedPost = 0;

/**
 * 
 * Client-part for creating a comment to a post.
 * 
 * 
 * 
 * @param {integer}
 * 
 * parentId The internalid of the post
 * 
 * @returns {Boolean}
 * 
 */

function socialAddReply(parentId) {
    try {
        // performance logger
        var logger = new ssobjLogger("socialAddReply");

        // var replyInputId = 'richEditorReplyTextBox' + parentId;
        var replyInputId = 'nfpostcommenteditor' + parentId;

        // check if button is disabled
        if (Ext.get("btnreplycontainer" + parentId).hasClass("disabled"))
            return false;

        // check if adding reply is done in popup window
        var popupWindow = socialGetPopupWindow();

        var target = Ext.get(replyInputId).dom;
        var msg = socialGetRichEditorText(target);
        if (msg.trim() === '') {
            socialShowWarning('Please provide a reply.'.tl(), null, function() {
                if (popupWindow.isVisible)
                    popupWindow.showModalWindow();

                target.focus();
            });

            return false;
        }

        // check msg if there are curly braces
        if (msg.indexOf('{{') > -1 || msg.indexOf('}}') > -1) {
            socialShowWarning('Double curly braces are not allowed.'.tl(), 'SuiteSocial', function() {
                if (popupWindow.isVisible)
                    popupWindow.showModalWindow();
            });

            return;
        }

        var msg = socialSocialConvertRecordNamesToObjectRich(replyInputId);
        if (msg.length > 4000) {
            socialShowInfo('Reply should not be more than 4000 characters.'.tl() + '<br>' + 'Current reply length:'.tl() + ' ' + msgRich.length, 'SuiteSocial');
            return;
        }

        document.getElementById(replyInputId).style.backgroundColor = '#efefef';
        document.getElementById(replyInputId).disabled = true;

        // EMS: Ext.get('hourGlass' + parentId).show();
        ssobjGlobal.replyInputId = replyInputId;

        // form data for quick posting
        var comment = ssapiFormDataForPosting(msg, parentId);

        // form data for saving
        var data = {
            parentId : parentId,
            message : msg,
            name : comment.id, // save temp id for the name field in news feed
            // record
            suppress_alert : true
        // suppress alert msg on error
        };

        // get dnd file if dnd is enabled
        if (ssapiHasValue(replyInputId) && ssapiHasValue(ssobjGlobal.dndElements)) {
            // get dnd elements
            var dndElements = ssobjGlobal.dndElements[replyInputId];

            // get dnd file if any
            var dadFile = socialGetDnDFileData(dndElements);

            if (ssapiHasValue(dadFile)) {
                comment.dadFile = dadFile;
                comment.dndElements = dndElements;
                data.dadFile = dadFile;
                data.targetId = replyInputId;

                // clear thumbnail/filename preview
                ssapiClearUploadedFilePreview(dndElements);
            }
        }

        // keep post data in the post html
        comment.postdata = JSON.stringify(data);

        // immediately post comment
        socialAddReplyHandler(comment);

        // then save to database
        socialSuiteletProcessAsync('addReply', data, function(newId) {
            // check if newId has valid value
            if (ssapiIsNumber(newId)) {
                // successfully saved post; newId = internal id of the newly
                // posted & saved msg
            } else {
                // failed saving post; update look of post to provide a retry
                // functionality
                socialShowReplyError(data.name, true, newId);
            }
        });

        // log end time
        logger.log("*********************************** END: Adding Reply ***********************************");
    } catch (e) {
        ssapiHandleError(e);
    }
}

function socialAddReplyHandler(data) {

    try {

        // get parent id

        var parentId = data.parent;

        // get the template for reply, and set to Ext object

        var template = '<tpl for=".">' + document.getElementById('tmpreply').innerHTML + '</tpl>';

        template = template.replace(/{internalid}/gi, '{id}');

        var myTplReply = new Ext.XTemplate(template);

        // get dnd file if any

        if (ssapiHasValue(data.dadFile)) {

            data.file = ssapiGetFileHTMLToRender(data.id, data.dadFile, data.dndElements);

        }

        // post comment

        myTplReply.append(document.getElementById('nfpostcommentslist' + parentId), [ data ]);

        // check if displayed on popup window
        var imageGalleryContainer = jQuery('#nfpostcontainer' + parentId).parent('#socialImageGalPostCommentContainer');
        if (imageGalleryContainer.length == 1) {
            // displayed on popup window
            jQuery("#nfpostcontainer" + parentId + " .sf_nf_post_msg img").css('max-width', '380px').css('max-height', '135px');
            jQuery("#nfpostcontainer" + parentId + " .ss_nf_post_comment_msg img").css('max-width', '380px').css('max-height', '135px');
        }

        data.like_label = ssobjGlobal.LABEL_LIKE;

        data.like_icon = ssobjGlobal.imagesUrlsObj['suitesocial_unlike.png'];

        data.like_label_style = data.privatePost ? 'style="display: none;"' : '';

        data.likers_style = 'style="display: none;"';

        data.likes_info = "[]";

        data.likers_label = '';

        jQuery('#nfpostcommentmsg' + data.internalid).like({

            internalid : data.internalid,

            like_label_style : data.like_label_style,

            like_icon : data.like_icon,

            like_label : data.like_label,

            likers_style : data.likers_style,

            likers_label : data.likers_label,

            likes_info : data.likes_info,

            like_type : 'comment'

        });

        // reset reply textbox for later comment

        var richEditorReplyTextBox = document.getElementById('nfpostcommenteditor' + parentId);

        richEditorReplyTextBox.disabled = false;

        richEditorReplyTextBox.innerHTML = '';

        richEditorReplyTextBox.focus();

        onRichEditorChange(richEditorReplyTextBox);

        // EMS: Ext.get('hourGlass' + parentId).hide();

        // do some fading in and animation

        // EMS: socialSetReplyOverAndOutEvents();

        // var selector = '#reply' + data.internalid;

        var selector = '#nfpostcommentmsgcontainer' + data.internalid;

        Ext.select(selector).fadeIn(FADEIN_SPEED).removeClass('newreply');

        Ext.select(selector).animate({// animation control object

            backgroundColor : {

                from : '#EEEEBB',

                to : '#ffffff'

            }

        }, 2.0, null, 'easeOut', 'color');

        Ext.select('#nfposts .newpost').removeClass('newpost');

        ssobjGlobal.replyInputId = null;

        // EMS: socialMakeCurrentUserLinkUnclickable();

    } catch (e) {

        ssapiHandleError(e);

    }

}

function onChannelMouseEnter(event) {

    if (Ext.isIE) {

        var e = event || window.event;

        var target = e.target || e.srcElement;

        Ext.get(target).addClass('channelhover');

        Ext.get(target).setStyle('backgroundImage', 'url("' + ssobjGlobal.imagesUrlsObj['channel-background-ie.png'] + '")');

    }

}

function onChannelMouseLeave(event) {

    if (Ext.isIE) {

        var e = event || window.event;

        var target = e.target || e.srcElement;

        if (target.className.indexOf('channelselected') > -1) {

            // currently selected, so don't remove

            return;

        }

        Ext.get(target).removeClass('channelhover');

        Ext.get(target).setStyle('backgroundImage', '');

    }

}

function onChannelSelectionMouseEnter(event) {

    // if (Ext.isIE) {

    var e = event || window.event;

    var target = e.target || e.srcElement;

    Ext.get(target).addClass('channelselectorrowhover');

    // }

}

function onChannelSelectionMouseLeave(event) {

    var e = event || window.event;

    var target = e.target || e.srcElement;

    Ext.get(target).removeClass('channelselectorrowhover');

}

function onColleagueMouseEnter(event) {

    var e = event || window.event;

    var target = e.target || e.srcElement;

    if (Ext.isIE) {

        Ext.get(target).addClass('colleaguehover');

        Ext.select('#' + target.id + ' .colleagueDivider').hide();

        if (ssobjGlobal.isList) {

            Ext.get(target).setStyle('backgroundImage', 'url("' + ssobjGlobal.imagesUrlsObj['colleague-background-ie.png'] + '")');

        }

    }

}

function onColleagueMouseLeave(event) {

    var e = event || window.event;

    var target = e.target || e.srcElement;

    if (Ext.isIE) {

        if (Ext.get(target).dom.className.indexOf('colleagueselected') > -1) {

            // if selected, do not remove class

            return;

        }

        Ext.get(target).removeClass('colleaguehover');

        Ext.select('#' + target.id + ' .colleagueDivider').show();

        Ext.get(target).setStyle('backgroundImage', '');

    }

}

function socialShowGrid(data, renderTo) {

    if (data.length === 0) {

        return '';

    }

    var store = new Ext.data.JsonStore({

        // store configs

        autoDestroy : true,

        data : data,

        storeId : 'myStore',

        root : '',

        idProperty : 'name'

    });

    var jsonObject = data[0];

    var fields = [];

    var columns = [];

    for ( var p in jsonObject) {

        fields.push(p);

        columns.push({

            text : p,

            flex : 1,

            sortable : true,

            dataIndex : p

        });

    }

    // create the Grid

    var grid = new Ext.grid.GridPanel({

        store : store,

        stateful : true,

        stateId : 'stateGrid',

        columns : columns,

        height : 500,

        width : 600,

        title : '',

        renderTo : renderTo,

        viewConfig : {

            stripeRows : false

        }

    });

}

/**
 * 
 * When the View and Edit buttons of the quick view is clicked, open the record
 * 
 * page in the parent page and not inside the portlet Issue: 240175
 * 
 * [SuiteSocial] Drilling down on some records within the news .
 * 
 */

function socialSetQuickViewLinksLocationToParent() {

    var logger = new ssobjLogger(arguments, true);

    try {

        // check if the quick view is visible

        var x = Ext.get('singleTipextTooltip');

        if (x === null) {

            // not ye initialized

            logger.log('x === null');

            return;

        }

        if (x.dom.style.visibility != 'visible') {

            // hidden

            logger.log("x.dom.style.visibility != 'visible'");

            return;

        }

        if (Ext.isIE) {

            // buttons

            var values = [ 'View', 'Edit' ];

            for (var i = 0; i < values.length; i++) {

                var els = Ext.select('#singleTipextTooltip [value=' + values[i] + ']').elements;

                if (els.length != 1) {

                    // we expect only 1 element

                    throw 'els.length != 1';

                }

                if (els[0].outerHTML.indexOf('parent.') == -1) {

                    els[0].outerHTML = els[0].outerHTML.replace('location', 'parent.location');

                } else {

                    logger.log('processed already');

                }

            }

        } else {

            // selector means get all input whose onclick starts with

            // 'location.'

            var els = Ext.select('#singleTipextTooltip input[onclick^="location."]').elements;

            logger.log('els.length=' + els.length);

            for (var i = 0; i < els.length; i++) {

                var onClickCode = els[i].getAttribute('onclick');

                // this does not work on ie 10

                els[i].setAttribute('onclick', 'parent.' + onClickCode);

            }

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/*
 * 
 * We need to call continuously because we do not know when the quickview is
 * 
 * displayed
 * 
 */

// EMS: setInterval('socialSetQuickViewLinksLocationToParent()', 1000);
function ssapiGetParameterByName(name) {

    try {

        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

        var regexS = "[\\?&]" + name + "=([^&#]*)";

        var regex = new RegExp(regexS);

        var results = regex.exec(window.location.search);

        if (results == null) {

            return "";

        } else {

            return decodeURIComponent(results[1].replace(/\+/g, " "));

        }

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Chrome's tooltip is unable to display other characters such as japanese Use
 * 
 * Ext tooltip then instead
 * 
 */

function socialConvertTipsToExtTips() {

    var els = Ext.select('#maintable [title]').elements;

    for (var i = 0; i < els.length; i++) {

        var el = els[i];

        if (ssapiHasValue(el.title)) {

            var target = el;

            new Ext.ToolTip({

                target : target,

                html : el.title

            });

            el.title = '';

        }

    }

}

function getErrorDetails(ex) {

    var errorDetails = 'ERROR DETAILS' + ssapiGetNewLine();

    errorDetails += ex.toString();

    if (ex.getDetails) {

        errorDetails += 'Details: ' + ex.getDetails() + ssapiGetNewLine();

    }

    if (ex.getCode) {

        errorDetails += 'Code: ' + ex.getCode() + ssapiGetNewLine();

    }

    if (ex.getId) {

        errorDetails += 'Id: ' + ex.getId() + ssapiGetNewLine();

    }

    if (ex.getStackTrace) {

        errorDetails += 'StackTrace: ' + ex.getStackTrace() + ssapiGetNewLine();

    }

    if (ex.getUserEvent) {

        errorDetails += 'User event: ' + ex.getUserEvent() + ssapiGetNewLine();

    }

    if (ex.getInternalId) {

        errorDetails += 'Internal Id: ' + ex.getInternalId() + ssapiGetNewLine();

    }

    if (ex.rhinoException) {

        errorDetails += 'RhinoException: ' + ex.rhinoException.toString() + ssapiGetNewLine();

    }

    if (ex.stack) {

        errorDetails += 'Stack=' + ex.stack;

    }

    if (ex instanceof nlobjError) {

        errorDetails += 'Type: nlobjError' + ssapiGetNewLine();

    } else if (ssapiHasValue(ex.rhinoException)) {

        errorDetails += 'Type: rhinoException' + ssapiGetNewLine();

    } else {

        errorDetails += 'Type: Generic Error' + ssapiGetNewLine();

    }

    errorDetails += ssapiGetFunctionCallDetails() + ssapiGetNewLine();

    return errorDetails;

}

function ssapiGetErrorDetails(ex) {

    return getErrorDetails(ex);

}

// fail safe code in case we miss an action that require application of the ext

// tooltip

// EMS: setInterval('socialConvertTipsToExtTips()', 2000);

function socialFileUploadHandler(dadFile, dndElements) {

    Ext.get(dndElements.dataId).update(JSON.stringify(dadFile));

}

function socialGetDnDFileData(dndElements) {

    // check if dndElements is null/undefined

    if (ssapiHasNoValue(dndElements))

        return "";

    var dadFile = Ext.get(dndElements.dataId).dom.innerHTML;

    if (ssapiHasValue(dadFile)) {

        dadFile = JSON.parse(dadFile);

        return {

            fileName : dadFile.origFileName,

            extName : dadFile.extName,

            fileId : dadFile.fileId,

            previewFileId : dadFile.previewFileId

        };

    }

    return "";

}

/**
 * 
 * Load the images sequentially since they cannot be retrieved at the same time.
 * 
 * This is because the connection of the browser to the netsuite server is
 * 
 * limted to around 5 connections (browser dependent) that when these
 * 
 * connections are already used, future requests are suspended until pending
 * 
 * requests have been completed.
 * 
 */

function socialLoadImagesSequentially() {

    var logger = new ssobjLogger(arguments);

    if (ssobjGlobal.isPreviewImageLoading === true) {

        logger.log('ssobjGlobal.isPreviewImageLoading === true');

        // uiShowInfo('ssobjGlobal.isPreviewImageLoading === true');

        return;

    }

    // get list of unprocessed images

    var els = Ext.select('img[data-processed="false"]').elements;

    if (els.length === 0) {

        logger.log('nothing more to process');

        return;

    }

    // process 1st image

    var el = els[0];

    var jel = jQuery(el);

    var src = jel.attr('data-src');

    // console.error(src);

    ssobjGlobal.isPreviewImageLoading = true;

    jel.attr('data-processed', 'true');

    jel.attr('src', src);

    jel.css('visibility', 'visible');

    logger.log('end');

}

/**
 * Shows the add or edit adhoc channel from newsfeed. This is triggered when
 * Create Channel or Edit Channel icon is clicked
 */
function showAdhocChannelPopupWindow(e, t) {
    if (!e instanceof Ext.EventObjectImpl && e.type !== 'click' && t.nodeName !== 'IMG')
        return;
    
    // check if span or img and parent is actionaddchannel, get the parent node
    if ((t.nodeName == 'SPAN' || t.nodeName == 'IMG') && t.parentNode.id == 'actionaddchannel') {
        t = t.parentNode;
    }

    var channelId = parseInt(t.attributes['data-internalid'].value);
    var isAdmin = nlapiGetContext().getRole() === '3'; // check if user is
    // admin
    var createdByAdmin = channelId > 0 ? JSON.parse(t.attributes['data-createdbyadmin'].value) : (isAdmin ? true : false);

    var channelHtml = prepareAddChannelContainer({
        channelId : channelId,
        isAdmin : isAdmin,
        createdByAdmin : createdByAdmin
    });

    var promiseShow = showAddChannel(channelHtml);

    /**
     * Populates the multi-select fields (Subsidiary, Departments, Location,
     * Roles, Employees) of adhoc channel
     */
    promiseShow.done(function(obj) {
        var roles = ssobjGlobal.roles;

        function loadSelectData(selectId, searchObj) {
            var def = jQuery.Deferred();
            var promise = def.promise();
            var options = null;
            var jsonType = null;

            promise.progress(function() {
                if (options !== null) {
                    for (var j = 0; j < options.length; j++) {
                        jQuery('#' + selectId).append(jQuery('<option>', {
                            value : jsonType ? options[j].id : options[j].getId(),
                            text : jsonType ? options[j].name : options[j].getValue(searchObj.text)
                        }));
                    }

                    jQuery('#' + selectId).trigger('chosen:updated');
                }
            });

            if (searchObj.searchRecord === 'role') {
                jsonType = true;
                options = roles;

                def.notify();
            } else {
                if (searchObj.searchRecord !== 'employee') {

                    options = nlapiSearchRecord(searchObj.searchRecord, null, [ new nlobjSearchFilter('isinactive', null, 'is', 'F') ], [ new nlobjSearchColumn(searchObj.text) ]);
                    def.notify();

                }
            }
        }

        function loadChosenOptions() {
            var def = jQuery.Deferred();

            if (isAdmin && createdByAdmin && jQuery('#adhocAdminRoles').children('option').length === 0) {
                var msel = jQuery('#addAdHocAdminChContainer select');
                for (var i = 0; i < msel.length; i++) {
                    if (jQuery('#' + msel[i].id).parent().css('display') !== 'none') {
                        loadSelectData(msel[i].id, {
                            searchRecord : jQuery('#' + msel[i].id).attr('data-source'),
                            text : jQuery('#' + msel[i].id).attr('data-displayfield')
                        });
                        jQuery('#' + msel[i].id).trigger('chosen:updated');
                    }
                }
            } else {
                var msel = jQuery('#addAdHocNonAdminChContainer select');
                for (var i = 0; i < msel.length; i++) {
                    if (jQuery('#' + msel[i].id).parent().css('display') !== 'none') {
                        loadSelectData(msel[i].id, {
                            searchRecord : jQuery('#' + msel[i].id).attr('data-source'),
                            text : jQuery('#' + msel[i].id).attr('data-displayfield')
                        });
                        jQuery('#' + msel[i].id).trigger('chosen:updated');
                    }
                }
            }
            def.resolve(true);
            return def.promise();
        }

        loadChosenOptions().then(function() {
            var oldChName = populateAdhocFields(obj);
            jQuery('#adhocchanneloldname').html(oldChName);
        }).then(function() { // last to do is to hide the loading image
            jQuery('#addNewChannelPw').loading('destroy');
        });
    });

    /**
     * Gets the appopriate template html to display adhoc cahnnel (admin or non
     * admin)
     */
    function prepareAddChannelContainer(paramObj) {
        var adHocChContainer = {};
        var chTemplate = '';
        var templateHt = 0;

        if ((paramObj.channelId === -1 && paramObj.isAdmin) || (paramObj.channelId > 0 && paramObj.createdByAdmin)) {
            chTemplate = '#tmpAdhocAdminChannel';
            templateHt = jQuery(chTemplate).height();
            adHocChContainer = jQuery(chTemplate).html();
            jQuery('#addAdHocAdminChContainer').detach();
        } else if ((paramObj.channelId === -1 && !paramObj.isAdmin) || (paramObj.channelId > 0 && !paramObj.createdByAdmin)) {
            chTemplate = '#tmpAdhocNonAdminChannel';
            templateHt = jQuery(chTemplate).height();
            adHocChContainer = jQuery(chTemplate).html();
            jQuery('#addAdHocNonAdminChContainer').detach();
        }

        return {
            html : adHocChContainer,
            chId : paramObj.channelId,
            userIsAdmin : paramObj.isAdmin,
            createdByAdmin : createdByAdmin,
            chTemplate : chTemplate,
            height : templateHt
        };
    }

    /**
     * Shows the Add Channel popup window
     */
    function showAddChannel(container) {
        var defCh = jQuery.Deferred();
        var html = "<div id='addChannelWin' style='width: 100%; height: 100%; display: table;'>" + "<div id='popWinAdhocContainer'>" + container.html + "</div>" + "<div id='adhocchannelid' style='visibility: hidden'>" + container.chId + "</div>" + "<div id='adhocchanneloldname' style='visibility: hidden'></div>" + "</div>";

        ssobjGlobal.addAdHocChWin = new Ext.Window({
            id : 'addNewChannelPw',
            title : parseInt(container.chId) === parseInt('-1') ? 'Create New Channel'.tl() : 'Edit Channel',
            width : 550,
            height : (function() {
                if (container.userIsAdmin) {
                    if (container.createdByAdmin)
                        return jQuery('#addAdHocAdminChContainer').height() || 454;
                    else
                        return jQuery('#addAdHocNonAdminChContainer').height() || 250;
                } else
                    return 250;
            })(),
            resizable : container.userIsAdmin && container.createdByAdmin,
            autoScroll : true,
            modal : true,
            html : html,
            listeners : {
                show : function() {
                    jQuery('#addNewChannelPw').loading({
                        id : 'imgAdHocLoading',
                        zIndex : 9004,
                        imageUrl : ssobjGlobal.imagesUrlsObj['loading-ani-trans.gif']
                    });

                    if (container.userIsAdmin && container.createdByAdmin) {
                        if (nlapiGetContext().getSetting("FEATURE", "subsidiaries") === 'T')
                            jQuery('#adhocAdminSubsidiary').parent().show();
                        if (nlapiGetContext().getSetting("FEATURE", "departments") === 'T')
                            jQuery('#adhocAdminDepts').parent().show();
                        if (nlapiGetContext().getSetting("FEATURE", "locations") === 'T')
                            jQuery('#adhocAdminLocs').parent().show();
                        jQuery('#adhocAdminRoles').parent().show();
                        jQuery('#adhocAdminIndividuals').parent().show();
                        jQuery('#adhocAdminAllowedPosters').parent().show();

                        // convert to chosenjs ui
                        var origHeight = {
                            win : jQuery('#addChannelWin').css('height').split('px')[0],
                            pw : jQuery('#addNewChannelPw').css('height').split('px')[0]
                        };

                        jQuery('#addAdHocAdminChContainer select').chosenselect({
                            style : 'width:316px',
                            deselectImgUrl : ssobjGlobal.imagesUrlsObj['chosen-sprite.png']
                        });

                        // adjust the search result container size.
                        var searchResultContainerHeight = 78;
                        jQuery('#adhocAdminIndividuals_chosen .chosen-drop').height((searchResultContainerHeight) + 'px');
                        jQuery('#adhocAdminIndividuals_chosen .chosen-results').height(searchResultContainerHeight + 'px');

                        jQuery('#adhocAdminAllowedPosters_chosen .chosen-drop').height((searchResultContainerHeight) + 'px');
                        jQuery('#adhocAdminAllowedPosters_chosen .chosen-results').height(searchResultContainerHeight + 'px');

                        var individualSearchId = null;
                        jQuery('#adhocAdminIndividuals_chosen input').on('keyup', function(e) {
                            var indvdualParam = {
                                searchId : individualSearchId,
                                selectId : 'adhocAdminIndividuals',
                                searchFunctionName : 'socialSearchEmployees'
                            };
                            individualSearchId = onSocialChosenSearchKeyup(e, indvdualParam);
                        });

                        var posterSearchId = null;
                        jQuery('#adhocAdminAllowedPosters_chosen input').on('keyup', function(e) {
                            var posterParam = {
                                searchId : posterSearchId,
                                selectId : 'adhocAdminAllowedPosters',
                                searchFunctionName : 'socialSearchEmployees'
                            };
                            posterSearchId = onSocialChosenSearchKeyup(e, posterParam);
                        });

                        // adjust height of display area of popup window
                        jQuery('.x-window-body').height(jQuery('.x-window-body').height() + 6);

                    } else {
                        jQuery('#adhocNonAdminIndividuals').parent().show();

                        var nonAdminAdhocContainerHeight = jQuery('#addAdHocNonAdminChContainer').height();
                        nonAdminAdhocContainerHeight = nonAdminAdhocContainerHeight + 50;
                        jQuery('.x-window-body').height(nonAdminAdhocContainerHeight);

                        jQuery('#addAdHocNonAdminChContainer select').chosenselect({
                            style : 'width:316px',
                            deselectImgUrl : ssobjGlobal.imagesUrlsObj['chosen-sprite.png']
                        });

                        // adjust the search result container size.
                        var searchResultContainerHeight = 78;
                        jQuery('#adhocNonAdminIndividuals_chosen .chosen-drop').height((searchResultContainerHeight) + 'px');
                        jQuery('#adhocNonAdminIndividuals_chosen .chosen-results').height(searchResultContainerHeight + 'px');

                        var individualSearchId = null;
                        var userSelectingIndividual = false;

                        jQuery('#adhocNonAdminIndividuals_chosen input').on('keyup', function(e) {
                            var indvdualParam = {
                                searchId : individualSearchId,
                                selectId : 'adhocNonAdminIndividuals',
                                searchFunctionName : 'socialSearchEmployees'
                            };
                            individualSearchId = onSocialChosenSearchKeyup(e, indvdualParam);
                        });

                    }

                    defCh.resolve({
                        channelId : container.chId,
                        isAdmin : container.userIsAdmin,
                        createdByAdmin : container.createdByAdmin
                    });

                    // disable scrolling in newsfeed
                    jQuery('html, body').css({
                        'overflow' : 'hidden',
                        'height' : '100%'
                    });
                },
                resize : function() {
                    if (container.userIsAdmin && container.createdByAdmin && ssobjGlobal.addAdHocChWin !== undefined && ssobjGlobal.addAdHocChWin !== null) {
                        ssobjGlobal.addAdHocChWin.setSize({
                            height : arguments[0].height,
                            width : arguments[0].width
                        });
                    }
                }
            }
        }).show().center();
        jQuery('#addNewChannelPw #btnCreate').children()[0].innerHTML = container.chId > 0 ? 'Update' : 'Create';

        ssobjGlobal.addAdHocChWin.on('beforeclose', function() {
            // restore original state (non-chosen)
            if (container.userIsAdmin && container.createdByAdmin) {
                jQuery('#addAdHocAdminChContainer select').chosenselect('destroy');
            } else {
                jQuery('#adhocNonAdminSubsidiary').text('My Subsidiary ');
                jQuery('#adhocNonAdminDepartment').text('My Department ');
                jQuery('#adhocNonAdminLocation').text('My Location ');
                jQuery('#adhocNonAdminRole').text('My Role');

                jQuery('#addAdHocNonAdminChContainer select').chosenselect('destroy');
            }

            jQuery(container.chTemplate).append(jQuery('#popWinAdhocContainer').html());
            ssobjGlobal.addAdHocChWin = null;

            // restore scrolling
            jQuery('html, body').css({
                'overflow' : 'auto',
                'height' : 'auto'
            });

            return true;
        });

        return defCh.promise();
    }

    /**
     * Populates the adhoc channel fields on edit
     */
    function populateAdhocFields(paramObj) {
        var channelValues = {
            name : ''
        };
        if (paramObj.channelId > 0) {
            channelValues = nlapiLookupField('customrecord_newsfeed_channel', paramObj.channelId, [ "name", "custrecord_newsfeed_channel_sub", "custrecord_newsfeed_channel_dept", "custrecord_newsfeed_channel_loc", "custrecord_newsfeed_channel_role", "custrecord_newsfeed_channel_emp", "custrecord_newsfeed_channel_posters", "custrecord_newsfeed_channel_auto", "custrecord_newsfeed_channel_comments", "owner" ]);
        }

        if (paramObj.isAdmin && paramObj.createdByAdmin) {
            if (paramObj.channelId > 0) {
                var decodedChannelName = decodeEntities(channelValues.name);

                jQuery('#adhocAdminChannelName').val(decodedChannelName);
                jQuery('#adhocAdminAutoSub').prop('checked', channelValues.custrecord_newsfeed_channel_auto === 'T');
                jQuery('#adhocAdminAllowComm').prop('checked', channelValues.custrecord_newsfeed_channel_comments === 'T');
                jQuery('#adhocAdminSubsidiary').val(channelValues.custrecord_newsfeed_channel_sub.split(','));
                jQuery('#adhocAdminDepts').val(channelValues.custrecord_newsfeed_channel_dept.split(','));
                jQuery('#adhocAdminLocs').val(channelValues.custrecord_newsfeed_channel_loc.split(','));
                jQuery('#adhocAdminRoles').val(channelValues.custrecord_newsfeed_channel_role.split(','));

                var individuals = channelValues.custrecord_newsfeed_channel_emp == "" ? null : channelValues.custrecord_newsfeed_channel_emp.split(',');
                addAndSelectCurrentValuesToChosen('adhocAdminIndividuals', individuals);
                var allowedPosters = channelValues.custrecord_newsfeed_channel_posters == "" ? null : channelValues.custrecord_newsfeed_channel_posters.split(',');
                addAndSelectCurrentValuesToChosen('adhocAdminAllowedPosters', allowedPosters);

                jQuery('#addAdHocAdminChContainer select').trigger('chosen:updated');
                jQuery('#addAdHocAdminChContainer select').change();

                return decodedChannelName;
            } else {
                jQuery('#adhocAdminAllowComm').prop('checked', true);
            }
        } else {
            var subsidiaryId = null;
            var departmentId, locationId;
            var restrictionFields = [];

            // check if subsidiaries feature is enabled
            if (nlapiGetContext().getSetting("FEATURE", "subsidiaries") == 'T') {
                jQuery('#adhocNonAdminMySubsidiary').show();
                subsidiaryId = nlapiGetContext().getSubsidiary();
                jQuery("#adhocNonAdminMySubsidiary input").prop('disabled', paramObj.isAdmin && paramObj.channelId > 0);

                restrictionFields.push("subsidiary");
            }

            // check if departments feature is enabled
            if (nlapiGetContext().getSetting("FEATURE", "departments") == 'T') {
                jQuery('#adhocNonAdminMyDepartment').show();
                departmentId = nlapiGetContext().getDepartment();
                if (departmentId === "0") {
                    jQuery("#adhocNonAdminMyDepartment input").change(function() {
                        alert('You cannot restrict channel access to your department, because a department has not been set on this account. Contact your account administrator to set your department.');
                        jQuery('#adhocNonAdminMyDepartment input').prop('checked', false);
                    });
                }

                restrictionFields.push("department");
                jQuery("#adhocNonAdminMyDepartment input").prop('disabled', paramObj.isAdmin && paramObj.channelId > 0);
            }

            // check if locations feature is enabled
            if (nlapiGetContext().getSetting("FEATURE", "locations") == 'T') {
                jQuery('#adhocNonAdminMyLocation').show();
                locationId = nlapiGetContext().getLocation();
                if (locationId === "0") {
                    jQuery("#adhocNonAdminMyLocation input").change(function() {
                        alert('You cannot restrict channel access to your location, because a location has not been set on this account. Contact your account administrator to set your location.');
                        jQuery('#adhocNonAdminMyLocation input').prop('checked', false);
                    });
                }

                restrictionFields.push("location");
                jQuery("#adhocNonAdminMyLocation input").prop('disabled', paramObj.isAdmin && paramObj.channelId > 0);
            }

            restrictionFields.push("role");

            if (channelId > 0 && channelValues.custrecord_newsfeed_channel_emp.length > 0) {
                ssobjGlobal.isNonAdminAdhocUsingRestrictions = false;
            } else {
                ssobjGlobal.isNonAdminAdhocUsingRestrictions = true;
            }

            // remove channel restrictions when user selects an
            // individual
            jQuery('#adhocNonAdminIndividuals_chosen .chosen-drop').click(function() {
                removeChannelRestrictions();
            });

            jQuery("#adhocNonAdminIndividuals_chosen").on("keydown", function(event) {
                if (event.which == 13)
                    removeChannelRestrictions();
            });

            function removeChannelRestrictions() {
                if (ssobjGlobal.isNonAdminAdhocUsingRestrictions) {
                    var confirmSelectIndividuals = confirm('Selecting an individual will remove the channel restrictions that you have defined. Do you want to continue?');
                    if (confirmSelectIndividuals) {
                        jQuery('#adhocNonAdminMySubsidiary input').prop('checked', false);
                        jQuery('#adhocNonAdminMyDepartment input').prop('checked', false);
                        jQuery('#adhocNonAdminMyLocation input').prop('checked', false);
                        jQuery('#adhocNonAdminMyRole input').prop('checked', false);

                        ssobjGlobal.isNonAdminAdhocUsingRestrictions = false;
                    } else {
                        emptyChosenSelect('adhocNonAdminIndividuals');
                        jQuery('#adhocNonAdminIndividuals').trigger('chosen:updated');
                        jQuery('#adhocNonAdminIndividuals').change();
                        ssobjGlobal.isNonAdminAdhocUsingRestrictions = true;
                    }
                }
            }

            // remove individuals when a user checks a channel
            // restriction
            jQuery('#addAdHocNonAdminChContainer :checkbox').click(function() {
                if (!ssobjGlobal.isNonAdminAdhocUsingRestrictions) {
                    var confirmSelectIndividuals = confirm('Selecting a channel restriction will remove the individuals that you have defined. Do you want to continue?');
                    if (confirmSelectIndividuals) {
                        // jQuery('#adhocNonAdminIndividuals_chosen
                        // .search-choice').remove();
                        emptyChosenSelect('adhocNonAdminIndividuals');
                        jQuery('#adhocNonAdminIndividuals').trigger('chosen:updated');
                        jQuery('#adhocNonAdminIndividuals').change();
                        ssobjGlobal.isNonAdminAdhocUsingRestrictions = true;
                    } else {
                        jQuery('#adhocNonAdminMySubsidiary input').prop('checked', false);
                        jQuery('#adhocNonAdminMyDepartment input').prop('checked', false);
                        jQuery('#adhocNonAdminMyLocation input').prop('checked', false);
                        jQuery('#adhocNonAdminMyRole input').prop('checked', false);

                        ssobjGlobal.isNonAdminAdhocUsingRestrictions = false;
                    }
                }
            });

            if (channelId > 0) {
                // set channel name
                var decodedChannelName = decodeEntities(channelValues.name);
                jQuery('#adhocNonAdminChannelName').val(decodedChannelName);

                // set restriction checkboxes if available
                (channelValues.custrecord_newsfeed_channel_sub === subsidiaryId) && jQuery('#adhocNonAdminMySubsidiary').is(':visible') ? jQuery('#adhocNonAdminMySubsidiary input').prop('checked', true) : '';
                (channelValues.custrecord_newsfeed_channel_dept === departmentId) && jQuery('#adhocNonAdminMyDepartment').is(':visible') ? jQuery('#adhocNonAdminMyDepartment input').prop('checked', true) : '';
                (channelValues.custrecord_newsfeed_channel_loc === locationId) && jQuery('#adhocNonAdminMyLocation').is(':visible') ? jQuery('#adhocNonAdminMyLocation input').prop('checked', true) : '';
                (channelValues.custrecord_newsfeed_channel_role !== '') ? jQuery('#adhocNonAdminMyRole input').prop('checked', true) : '';

                jQuery("#adhocNonAdminMyRole input").prop('disabled', paramObj.isAdmin && paramObj.channelId > 0);

                if (paramObj.isAdmin) {
                    // update popup title bar
                    var adhocChannel = nlapiLookupField('customrecord_newsfeed_channel', paramObj.channelId, [ "owner" ], true);
                    ssobjGlobal.addAdHocChWin.setTitle('Edit Channel created by ' + adhocChannel.owner);
                    jQuery('#addNewChannelPw span').first().css({
                        'display' : 'block',
                        'width' : '500px',
                        'overflow' : 'hidden',
                        'text-overflow' : 'ellipsis',
                        'white-space' : 'nowrap'
                    });

                    // update restriction label if administrator
                    jQuery('#adhocNonAdminSubsidiary').text('Subsidiary ');
                    jQuery('#adhocNonAdminDepartment').text('Department ');
                    jQuery('#adhocNonAdminLocation').text('Location ');
                    jQuery('#adhocNonAdminRole').text('Role ');

                    // disable individuals if administrator
                    jQuery('#adhocNonAdminIndividuals').prop('disabled', true).trigger("chosen:updated");
                }

                // set user restriction fields
                var isShowUserRestrictionDetails = (nlapiGetContext().getSetting("SCRIPT", "custscript_ss_show_user_restriction") === 'T');
                if (isShowUserRestrictionDetails) {
                    var param = {};
                    param.userId = channelValues.owner;
                    param.restrictionFields = restrictionFields;
                    applyRestrictionDetails(param);
                }

                // set individuals
                jQuery('#adhocNonAdminIndividuals_chosen .search-choice').remove();
                var individuals = channelValues.custrecord_newsfeed_channel_emp == "" ? null : channelValues.custrecord_newsfeed_channel_emp.split(',');
                addAndSelectCurrentValuesToChosen('adhocNonAdminIndividuals', individuals);

                jQuery('#addAdHocNonAdminChContainer select').trigger('chosen:updated');
                jQuery('#addAdHocNonAdminChContainer select').change();

                return decodedChannelName;
            } else {
                jQuery('#adhocNonAdminMySubsidiary input').prop('checked', true);
                departmentId !== "0" ? jQuery('#adhocNonAdminMyDepartment input').prop('checked', true) : '';
                locationId !== "0" ? jQuery('#adhocNonAdminMyLocation input').prop('checked', true) : '';
                jQuery('#adhocNonAdminMyRole input').prop('checked', true);

                var isShowUserRestrictionDetails = (nlapiGetContext().getSetting("SCRIPT", "custscript_ss_show_user_restriction") === 'T');
                if (isShowUserRestrictionDetails) {
                    var param = {};
                    param.userId = nlapiGetContext().getUser();
                    param.restrictionFields = restrictionFields;
                    applyRestrictionDetails(param);
                }
            }
        }

        return '';
    }
}

/**
 * Append restriction details to non-admin adhoc channel: subsidiary,
 * department, location, role
 */
function applyRestrictionDetails(param) {
    socialSuiteletProcessAsync('getUserRestrictionDetails', param, function(restrictionDetails) {
        jQuery('#adhocNonAdminSubsidiary').text(ssapiHasValue(restrictionDetails.subsidiary) ? jQuery('#adhocNonAdminSubsidiary').text() + ': ' + restrictionDetails.subsidiary : jQuery('#adhocNonAdminSubsidiary').text());
        jQuery('#adhocNonAdminDepartment').text(ssapiHasValue(restrictionDetails.department) ? jQuery('#adhocNonAdminDepartment').text() + ': ' + restrictionDetails.department : jQuery('#adhocNonAdminDepartment').text());
        jQuery('#adhocNonAdminLocation').text(ssapiHasValue(restrictionDetails.location) ? jQuery('#adhocNonAdminLocation').text() + ': ' + restrictionDetails.location : jQuery('#adhocNonAdminLocation').text());
        jQuery('#adhocNonAdminRole').text(ssapiHasValue(restrictionDetails.role) ? jQuery('#adhocNonAdminRole').text() + ': ' + restrictionDetails.role : jQuery('#adhocNonAdminRole').text());
    });

}

function onSocialChosenSearchKeyup(event, param) {
    var e = event || window.event;
    // Standard or IE model
    var target = e.target || e.srcElement;
    var content = target.value;

    var searchId = param.searchId;
    var selectId = param.selectId;
    var chosenSelectId = '#' + selectId + '_chosen';
    var searchFunctionName = param.searchFunctionName;
    var searchFunctionParams = param.searchFuncParams;

    if (ssapiHasNoValue(searchFunctionParams)) {
        searchFunctionParams = {};
    }

    searchFunctionParams.searchString = content;
    searchFunctionParams.selectId = selectId;

    if (searchId) {
        clearTimeout(searchId);
    }

    // perform search 1 second after the user stopped typing
    var MILLISECONDS_BEFORE_SEARCHING = 1000;
    jQuery(chosenSelectId + ' .chosen-drop .no-results').html("Searching...");

    if (content.length < 2) {
        jQuery(chosenSelectId + ' .chosen-drop .no-results').html("Type at least 2 characters.");
        return searchId;
    } else {
        jQuery(chosenSelectId + ' .chosen-drop .no-results').html("Searching...");
    }

    searchId = setTimeout(searchFunctionName + '(' + JSON.stringify(searchFunctionParams) + ')', MILLISECONDS_BEFORE_SEARCHING);
    return searchId;
}

function socialSearchEmployees(param) {

    var searchString = param.searchString;
    var selectId = param.selectId;
    var selectedEntities = jQuery('#' + selectId).val();
    if (ssapiHasNoValue(selectedEntities)) {
        selectedEntities = [];
    }

    var selectedOptions = getSelectedOptions(selectId);

    emptyChosenSelect(selectId);

    socialSuiteletProcessAsync('getEntitiesSearch', {
        searchString : searchString,
        isEmployeesOnly : 'T',
        allowSearchCurrentUser : 'T'
    }, function(entities) {
        var employees = entities.Employees;
        if (ssapiHasNoValue(employees) || employees.length == 0 || isSearchResultForSelectEmpty(employees)) {
            jQuery('#' + selectId + '_chosen .chosen-drop .no-results').html("No results found");

            setTimeout(function() {
                appendSelectedOptions(selectId, null, selectedOptions);

                jQuery('#' + selectId).trigger('chosen:updated');
                jQuery('#' + selectId).change();
            }, 2000);
            return;
        }

        // add options to dropdown
        var select = jQuery('#' + selectId);
        var html = '';
        for (var i = 0; i < employees.length; i++) {
            // show only first 100 to prevent browser from freezing
            if (i == 99) {
                break;
            }

            // avoid duplicate display of currently selected options
            if (selectedEntities.indexOf(employees[i]._id) > -1) {
                continue;
            }

            html += '<option value="' + employees[i]._id + '">' + employees[i].entityName + '</option>';

        }

        select.append(html);

        appendSelectedOptions(selectId, null, selectedOptions);

        jQuery('#' + selectId).trigger('chosen:updated');
        jQuery('#' + selectId).change();

        addIdToChosenDropDownOptions(selectId);

        jQuery('#' + selectId + '_chosen').addClass('chosen-container-active');
        jQuery('#' + selectId + '_chosen input').val(searchString);
        jQuery('#' + selectId + '_chosen input').width('100px');

    });
}

function addIdToChosenDropDownOptions(selectId, groups) {

    var optionValues = [];

    if (ssapiHasNoValue(groups)) {

        jQuery('#' + selectId + ' option').each(function() {
            optionValues.push({
                value : jQuery(this).val(),
                text : jQuery(this).text()
            });
        });

        var optionValuesIdx = 0;
        jQuery('#' + selectId + '_chosen ul.chosen-results li').each(function() {
            jQuery(this).attr('data-entityid', optionValues[optionValuesIdx].value);
            optionValuesIdx++;
            if (optionValuesIdx >= optionValues.length) {
                return;
            }

        });
    } else {
        for ( var grpIdx in groups) {
            var group = groups[grpIdx];
            jQuery('#' + selectId + ' optgroup[label="' + group + '"] option').each(function() {
                optionValues.push({
                    value : jQuery(this).val(),
                    text : jQuery(this).text()
                });
            });

            var groupFound = false;
            var optionValuesIdx = 0;

            jQuery('#' + selectId + '_chosen ul.chosen-results li').each(function() {
                if (groupFound) {
                    jQuery(this).attr('data-entityid', optionValues[optionValuesIdx].value);
                    optionValuesIdx++;
                    if (optionValuesIdx >= optionValues.length) {
                        return;
                    }
                }

                if (!groupFound && jQuery(this).hasClass('group-result') && jQuery(this).text() == group) {
                    groupFound = true;
                }

            });
        }
    }
}

function addAndSelectCurrentValuesToChosen(selectId, currentValues) {

    if (ssapiHasNoValue(currentValues) || currentValues.length == 0) {
        return;
    }

    socialSuiteletProcessAsync('getEntitiesSearch', {
        isEmployeesOnly : 'T',
        allowSearchCurrentUser : 'T',
        employeeIds : currentValues
    }, function(entities) {

        if (ssapiHasNoValue(entities.Employees) || isSearchResultForSelectEmpty(entities.Employees)) {
            console.log('Error : Employees not found' + JSON.stringify(currentValues));
            return;
        }

        var employees = entities.Employees;
        for (var i = 0; i < employees.length; i++) {
            jQuery('#' + selectId).append(jQuery('<option>', {
                value : employees[i]._id,
                text : employees[i].entityName,
                selected : true
            }));
        }

        jQuery('#' + selectId).trigger('chosen:updated');
        jQuery('#' + selectId).change();

        addIdToChosenDropDownOptions(selectId);

    });
}

/**
 * Decodes the HTML string (<, >, &, ')
 */
function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}

/**
 * Validates adhoc channel fields for add or update
 * 
 * @param chFieldsObj -
 *        the object represenation of fields and values
 * @returns {Boolean} - return true if valid, otherwise returns false
 */
function validateChannelFields(chFieldsObj) {
    var chOldName = decodeEntities(jQuery('#adhocchanneloldname').html());

    if (chFieldsObj.chName === '') {
        alert('Channel name is required. Enter a name for the channel.');
        jQuery('#adhocAdminChannelName').val('').focus();

        return false;
    }

    var isValid = true;
    if ([ PUBLIC_CHANNEL_NAME.toLowerCase(), 'private messages', 'recognition', 'status changes', 'record changes' ].indexOf(chFieldsObj.chName.toLowerCase()) > -1) {
        alert('Channel "' + chFieldsObj.chName + '" already exists. Enter another name for the channel.');
        jQuery('#adhocAdminChannelName').val(chOldName).focus();

        isValid = false;
    } else {
        for ( var c in ssobjGlobal.allChannelsObj) {
            var exChName = decodeEntities(ssobjGlobal.allChannelsObj[c].name);
            if (chFieldsObj.chName.toLowerCase() === exChName.toLowerCase()) {
                if (chFieldsObj.chId === -1 || (chFieldsObj.chId !== -1 && chOldName.toLowerCase() !== chFieldsObj.chName.toLowerCase())) {
                    alert('Channel "' + chFieldsObj.chName + '" already exists. Enter another name for the channel.');
                    jQuery('#adhocAdminChannelName').val(chOldName).focus();

                    isValid = false;
                    break;
                }
            }
        }
    }

    if (isValid && chFieldsObj.chSubs !== null && chFieldsObj.chSubs.length > 1) {
        alert("You can select only one subsidiary.");

        isValid = false;
    }

    return isValid;
}

/**
 * Processes the adhoc channel window when Create or Update is clicked
 */
function processAdHocChannel() {
    var chId = parseInt(jQuery('#adhocchannelid').html());
    var chName = jQuery('#adhocAdminChannelName').val().trim() || jQuery('#adhocNonAdminChannelName').val().trim();
    var chSubs = jQuery('#adhocAdminSubsidiary').val();

    var isValid = validateChannelFields({
        chId : chId,
        chName : chName,
        chSubs : chSubs
    });

    if (!isValid)
        return;

    jQuery('#addNewChannelPw').loading({
        id : 'imgAdHocLoading',
        zIndex : 9004,
        imageUrl : ssobjGlobal.imagesUrlsObj['loading-ani-trans.gif']
    });

    var isAdmin = nlapiGetContext().getRole() === '3';
    var adhocChObj = {};

    if (isAdmin && jQuery('#addAdHocAdminChContainer').is(":visible")) {
        adhocChObj = {
            id : chId,
            name : chName,
            custrecord_newsfeed_channel_sub : chSubs,
            custrecord_newsfeed_channel_dept : jQuery('#adhocAdminDepts').val(),
            custrecord_newsfeed_channel_loc : jQuery('#adhocAdminLocs').val(),
            custrecord_newsfeed_channel_role : jQuery('#adhocAdminRoles').val(),
            custrecord_newsfeed_channel_emp : jQuery('#adhocAdminIndividuals').val(),
            custrecord_newsfeed_channel_posters : jQuery('#adhocAdminAllowedPosters').val(),
            custrecord_newsfeed_channel_auto : jQuery('#adhocAdminAutoSub').is(":checked") ? 'T' : 'F',
            custrecord_newsfeed_channel_comments : jQuery('#adhocAdminAllowComm').is(":checked") ? 'T' : 'F',
            auto_subscribe_users : []
        };
        chId === -1 ? adhocChObj.custrecord_newsfeed_channel_isadmin = 'T' : '';
    } else {
        if (isAdmin) {
            adhocChObj = {
                id : chId,
                name : chName,
                auto_subscribe_users : []
            };
        } else {
            var userSubsidiary = [];
            var userDepartment = [];
            var userLocation = [];
            var userRole = [];
            if (nlapiGetContext().getSetting("FEATURE", "subsidiaries") == 'T' && jQuery('#adhocNonAdminMySubsidiary input').prop('checked') === true) {
                userSubsidiary.push(nlapiGetContext().getSubsidiary());
            }

            if (nlapiGetContext().getSetting("FEATURE", "departments") == 'T' && jQuery('#adhocNonAdminMyDepartment input').prop('checked') === true) {
                nlapiGetContext().getDepartment() > 0 ? userDepartment.push(nlapiGetContext().getDepartment()) : '';
            }

            if (nlapiGetContext().getSetting("FEATURE", "locations") == 'T' && jQuery('#adhocNonAdminMyLocation input').prop('checked') === true) {
                nlapiGetContext().getLocation() > 0 ? userLocation.push(nlapiGetContext().getLocation()) : '';
            }
            if (jQuery('#adhocNonAdminMyRole input').prop('checked') === true) {
                userRole.push(nlapiGetContext().getRole());
                userRole.push('3');
            }

            adhocChObj = {
                id : parseInt(jQuery('#adhocchannelid').html()),
                name : chName,
                custrecord_newsfeed_channel_sub : userSubsidiary.length > 0 ? userSubsidiary : null,
                custrecord_newsfeed_channel_dept : userDepartment.length > 0 ? userDepartment : null,
                custrecord_newsfeed_channel_loc : userLocation.length > 0 ? userLocation : null,
                custrecord_newsfeed_channel_role : userRole.length > 0 ? userRole : null,
                custrecord_newsfeed_channel_emp : jQuery('#adhocNonAdminIndividuals').val(),
                custrecord_newsfeed_channel_posters : jQuery('#adhocNonAdminIndividuals').val(),
                custrecord_newsfeed_channel_auto : 'F',
                custrecord_newsfeed_channel_comments : 'T',
                custrecord_newsfeed_channel_isadmin : 'F',
                auto_subscribe_users : !ssobjGlobal.isNonAdminAdhocUsingRestrictions ? jQuery('#adhocNonAdminIndividuals').val() : []
            };
        }
    }

    var action = adhocChObj.id > 0 ? 'updateAdHocChannel' : 'addAdHocChannel';

    socialSuiteletProcessAsync(action, adhocChObj, function(retObj) {
        if (retObj.channelId !== -1)
            window.location.reload();
        else {
            jQuery('#imgAdHocLoading').css('visibility', 'hidden');
            alert(retObj.error.message);
        }
    });
}

/**
 * Closes the add adhoc channel popup window
 */
function closeAddAdHocChannel() {
    ssobjGlobal.addAdHocChWin.close();
}

/**
 * 
 * Shows a list of likers of the post.
 * 
 * 
 * 
 * @param {Array}
 * 
 * likes - array of likers object
 * 
 */

function socialShowLikesList(likes) {

    // disable window scroll

    jQuery('body,html').css('overflow', 'hidden');

    socialCloseCurrentModalBox();

    // check browser width

    var iBaseWidth = 370;

    var iSideGaps = 20;

    var iWidth = Ext.get("maincontainer").getWidth();

    iWidth = iWidth > (iBaseWidth + iSideGaps) ? iBaseWidth : (iWidth - iSideGaps);

    // prepare html content of likers list

    var html = "<div id='likersPopup' style='width: 100%; height: 100%; display: table;'>";

    html += "<div style='display: table-cell; text-align: center; vertical-align: middle;'>";

    html += "<img src='" + ssobjGlobal.imagesUrlsObj['loading-ani.gif'] + "' /></div></div>";

    // initialize Ext.Window

    var likersListId = 'likersListBox';

    ssobjGlobal.likersWin = new Ext.Window({

        id : likersListId,

        title : 'People Who Like This'.tl(),

        width : iWidth,

        height : (iWidth + 6),

        resizable : false,

        modal : true,

        html : html

    });

    // show window, and center it

    ssobjGlobal.likersWin.show(this);

    ssobjGlobal.likersWin.center();

    // remove extra line at the bottom

    Ext.select(".x-shadow div.xsb").setStyle("display", "none");

    Ext.select("div .x-window-bl.x-panel-nofooter").setStyle("display", "none");

    // reset mask

    Ext.select(".ext-el-mask").setStyle("top", window.pageYOffset + "px");

    // enhance border; add bottom lining

    Ext.get(likersListId).setStyle({

        'box-shadow' : '0 0 0px 5px rgba(0,0,0,0.2)',

        'border-bottom' : '5px solid #e8e8e8'

    });

    // decrease padding

    Ext.select('.x-window-mc').setStyle({

        'padding' : '15px 15px 0px 15px'

    });

    // capture before close event

    ssobjGlobal.likersWin.on('beforeclose', function() {

        // enable back window scroll

        jQuery('body,html').css('overflow', '');

        // proceed on closing

        return true;

    });

    // add mouse up event on document to hide the list on click of the body

    // other than the list

    if (ssapiHasNoValue(ssobjGlobal.likersListMouseEventAdded)) {

        jQuery(document).mousedown(function() {

            ssobjGlobal.likersListIsDragging = false;

        }).mousemove(function() {

            ssobjGlobal.likersListIsDragging = true;

        }).mouseup(function(e) {

            // check if was dragging

            if (ssobjGlobal.likersListIsDragging) {

                return;

            }

            ssobjGlobal.likersListIsDragging = false;

            // check if colleague tooltip

            var colleagueTooltip = jQuery('#labsTutorialTooltip');

            var isNotCollTooltip = (colleagueTooltip.length > 0) ? (!colleagueTooltip.is(e.target) && colleagueTooltip.has(e.target).length === 0) : true;

            // get the list box

            var listBox = jQuery('#' + likersListId);

            // check if target is not the list; nor a child of it; nor the

            // colleague tooltip

            if (!listBox.is(e.target) && listBox.has(e.target).length === 0 && ssobjGlobal.likersWin.isVisible() && isNotCollTooltip) {

                // close window

                ssobjGlobal.likersWin.close();

            }

        });

        // set flag

        ssobjGlobal.likersListMouseEventAdded = true;

    }

    // get likers info (i.e. image)

    socialSuiteletProcessAsync('getLikersInfo', likes, function(newLikes) {

        // check if newLikes has value

        if (ssapiHasValue(newLikes)) {

            // update style of likers popup

            Ext.get('likersPopup').setStyle({

                'overflow-y' : 'scroll',

                'position' : 'absolute',

                'display' : '',

                'width' : '371px'

            });

            // successfully retrieve likers info; form the list

            var tmpLiker = document.getElementById('tmpLikersPopup').innerHTML;

            var myTplLiker = new Ext.XTemplate('<tpl for=".">' + tmpLiker + '</tpl>');

            myTplLiker.overwrite(document.getElementById('likersPopup'), newLikes, false);

        } else {

            // failed retrieving likers info

            Ext.get('likersPopup').update('Failed retrieving information.');

        }

    });

}

/**
 * 
 * Sets the hashtag string into the search field and does the search on the news
 * 
 * feed posts with the specified hashtag.
 * 
 * 
 * 
 * @param {String}
 * 
 * hashtag
 * 
 * @returns {Boolean}
 * 
 */

function socialHashtagClicked(hashtag) {

    try {

        // put the hashtag into the search field

        Ext.get('searchNewsFeed').dom.value = hashtag;

        // call search news feed keyup event; mock event object to pass the

        // input as target element

        onSearchNewsFeedKeyup({

            target : Ext.get('searchNewsFeed').dom

        });

    } catch (e) {

        ssapiHandleError(e);

    }

}

/**
 * 
 * Shows the picture gallery with the large version of clicked image
 * 
 * @param {String}
 *        imageSrc - src of the image
 * @param {String}
 *        postId The internal id or the name of the newsfeed post
 * @param {String}
 *        parentId The parent's internal id of the post
 */
function socialShowImageGalleryPopup(imageSrc, postId, parentId) {

    if (ssapiIsMobile()) {
        socialShowImageGalleryPopupMobile(imageSrc, postId, parentId);
        return;
    }

    // Close current modal boxes
    socialCloseCurrentModalBox();

    // disable window scroll
    jQuery('body,html').css('overflow', 'hidden');

    // check browser width
    var defaultWidth = 1024;
    var sideGaps = 61;
    var verticalGaps = 61;
    var defaultHeight = 630;
    var minWidth = 950;
    var minHeight = 630;

    var iWidth = Ext.get("maincontainer").getWidth();
    // get height of left portlet as basis for window height
    var iHeight = jQuery('.ss_column_left .ss_portlet').height();

    // sideGaps * 2 for both left and right side
    iWidth = iWidth > (defaultWidth + (sideGaps * 2)) ? defaultWidth : iWidth - (2 * sideGaps);
    // verticalGaps * 2 for top and bottom
    iHeight = iHeight > (defaultHeight + (verticalGaps * 2)) ? defaultHeight : iHeight - (2 * verticalGaps);

    // prepare html content of image gallery
    var html = '<div id="imageGalleryPopup" style="width: 100%; height: 100%; display: table;">';

    html += '       <div id="socialImageGalPostCommentContainer" class="ss_img_gal_post_comment">' + '        </div>' + '        <div id="socialImageViewer" class="ss_image_viewer">' + '            <div id="displayImageDiv" class="ss_display_image">' + '                <img id="originalImage" src="' + imageSrc + '" />' + '            </div>' + '        </div>' + '        <div id="imageGalleryLoading" >' + '            <div id="imageGalleryLoadingDiv" >' + ssobjGlobal.loadingImageMarkup
            + '            </div>' + '        </div>';

    html += "</div>";

    // initialize Ext.Window
    var imageGalleryPopupId = 'imageGalleryPopupBox';

    // if img viewer is not closed, image is clicked from comments
    // update image source
    if (ssobjGlobal.imageGalleryWin !== undefined && !ssobjGlobal.imageGalleryWin.isDestroyed) {
        jQuery("#originalImage").attr("src", imageSrc);
    } else { // initialize new image viewer object
        ssobjGlobal.imageGalleryWin = new Ext.Window({
            id : imageGalleryPopupId,
            title : 'Image Gallery'.tl(),
            width : iWidth,
            height : iHeight,
            resizable : false,
            modal : true,
            html : html
        });
    }

    // calculate the image display width for container
    var postCommentContainerWidth = 400;
    var popupContainerBoxHeight = 39;
    var imageDispWidth = iWidth - postCommentContainerWidth;
    var padding = 8;

    // Show window
    ssobjGlobal.imageGalleryWin.show(this);

    // center the modal box
    ssobjGlobal.imageGalleryWin.center();

    jQuery('#imageGalleryPopupBox').css('min-height', minHeight);

    jQuery('#imageGalleryPopupBox').css('min-width', minWidth);

    jQuery('.ss_image_viewer').css('min-height', minHeight - popupContainerBoxHeight - padding);

    jQuery('.ss_image_viewer').css('min-width', minWidth - postCommentContainerWidth - padding);

    jQuery('#imageGalleryLoading').css('min-height', minHeight - popupContainerBoxHeight - padding);

    jQuery('#imageGalleryLoading').css('min-width', minWidth - postCommentContainerWidth - padding);

    // remove extra line at the bottom
    Ext.select(".x-shadow div.xsb").setStyle("display", "none");
    Ext.select("div .x-window-bl.x-panel-nofooter").setStyle("display", "none");

    // reset mask
    Ext.select(".ext-el-mask").setStyle("top", window.pageYOffset + "px");

    // enhance border; add bottom lining
    Ext.get(imageGalleryPopupId).setStyle({
        'box-shadow' : '0 0 0px 5px rgba(0,0,0,0.2)',
    // 'border-bottom' : '5px solid #e8e8e8'
    });

    // decrease padding
    Ext.select('.x-window-mc').setStyle({
        'padding' : '0px 0px 0px 0px'
    });

    Ext.select('.x-window-body').setStyle({
        'width' : '100%',
        'height' : '100%'
    });
    // Adjust width of containter display as square area
    jQuery('.ss_image_viewer').css('width', imageDispWidth);

    jQuery('.ss_image_viewer').css('height', imageDispWidth + 2);

    // hide window while image loads
    jQuery('.ss_image_viewer').hide();

    // set width of container of loading image
    jQuery('#imageGalleryLoading').css('width', imageDispWidth - 4).css('height', imageDispWidth - 4);

    // when image is loaded
    jQuery('.ss_display_image img').load(function() {

        // hide loading image
        jQuery('#imageGalleryLoading').hide();
        jQuery('.ss_display_image').css('background-color', 'transparent');

        // set the width of the image to blank to get actual width of image
        jQuery('.ss_display_image img').css('width', '');
        jQuery('.ss_display_image img').css('height', '');

        // Show image
        jQuery('.ss_image_viewer').show();

        // adjust width if image size is bigger than container
        var imageWidth = jQuery('.ss_display_image img').width();
        var imageHeight = jQuery('.ss_display_image img').height();

        if (imageWidth >= imageDispWidth - padding) {

            if (imageHeight >= imageDispWidth - padding) {
                // width and height is greater than container size
                // determine which has the bigger difference and use that as the
                // basis for full length
                var widthDiff = imageWidth - (imageDispWidth - padding);
                var heightDiff = imageHeight - (imageDispWidth - padding);

                if (widthDiff >= heightDiff) {
                    jQuery('.ss_display_image img').css('width', '100%');
                } else {
                    jQuery('.ss_display_image img').css('height', iHeight);
                }

            } else {
                // width is greater than container, height is not greater than
                // container
                // Set width to cover the full length of container and let
                // height adjust automatically
                jQuery('.ss_display_image img').css('width', '100%');
            }

        } else {

            if (imageHeight >= imageDispWidth - padding) {
                // adjusted the height of the image
                jQuery('.ss_display_image img').css('height', iHeight);
            } else {
                // image is smaller than container in both width and height
                // dispaly image as original size

            }

        }

        // get the new image width and height based on adjusted height
        imageWidth = jQuery('.ss_display_image img').width();
        imageHeight = jQuery('.ss_display_image img').height();

        if (imageWidth < minWidth - (postCommentContainerWidth + padding)) {
            // set background image to black when width is smaller than
            // container
            jQuery('.ss_display_image').css('background-color', 'black');
        } else {
            // set containter to fit the image
            jQuery('.ss_image_viewer').css('width', imageWidth);
            jQuery('#imageGalleryPopupBox').css('width', imageWidth + postCommentContainerWidth + padding);
        }

        if (imageHeight < minHeight - popupContainerBoxHeight) {
            // set background image to black when height is smaller than
            // container
            jQuery('.ss_display_image').css('background-color', 'black');
        } else {
            // set containter to fit the image
            jQuery('.ss_image_viewer').css('height', iHeight);
            jQuery('#imageGalleryPopupBox').css('height', iHeight + popupContainerBoxHeight);
        }

        var imageViewerHeight = jQuery('.ss_image_viewer').height() + padding;
        jQuery('#socialImageGalPostCommentContainer').css('height', imageViewerHeight);

        // center the modal box
        ssobjGlobal.imageGalleryWin.center();

        // reset mask
        Ext.select(".ext-el-mask").setStyle("top", window.pageYOffset + "px");

    });

    if (parentId !== undefined)
        postId = parentId;
    else if (parentId === undefined && isNaN(postId)) { // for new posts or
        // comments with temp
        // ids
        var parentContainer = jQuery('#nfpostcommentmsgcontainer' + postId).parent();
        if (parentContainer !== undefined && parentContainer.length != 0) {
            var parentPostId = parentContainer.attr('id').split('nfpostcommentslist');
            if (parentPostId.length > 1)
                postId = parentPostId[1];
        }
    }

    var nextPostId = jQuery("#nfpostcontainer" + postId).next().attr('id');
    // capture before close event
    ssobjGlobal.imageGalleryWin.on('beforeclose', function() {
        // enable back window scroll
        jQuery('body,html').css('overflow', '');

        // attach back to nfpost
        jQuery("#nfpostcomments" + postId).css('margin-left', '45px');
        jQuery("#nfpostcommenteditor" + postId).empty();

        if (nextPostId === undefined)
            jQuery("#nfposts").prepend(jQuery("#nfpostcontainer" + postId).detach());
        else
            jQuery("#" + nextPostId).before(jQuery("#nfpostcontainer" + postId).detach());

        // return attached images to original sizes by removing max width and
        // height
        jQuery("#nfpostcontainer" + postId + " .sf_nf_post_msg img").css('max-width', '').css('max-height', '');
        jQuery("#nfpostcontainer" + postId + " .ss_nf_post_comment_msg img").css('max-width', '').css('max-height', '');

        // proceed on closing
        return true;
    });

    // add mouse up event on document to hide the list on click of the body
    // other than the list
    if (ssapiHasNoValue(ssobjGlobal.imageGalleryMouseEventAdded)) {
        jQuery(document).mousedown(function() {
            ssobjGlobal.imageGalleryIsDragging = false;
        }).mousemove(function() {
            ssobjGlobal.imageGalleryIsDragging = true;
        }).mouseup(function(e) {
            // check if was dragging
            if (ssobjGlobal.imageGalleryIsDragging) {
                return;
            }

            ssobjGlobal.imageGalleryIsDragging = false;

            // check if colleague tooltip
            var colleagueTooltip = jQuery('#labsTutorialTooltip');
            var isNotCollTooltip = (colleagueTooltip.length > 0) ? (!colleagueTooltip.is(e.target) && colleagueTooltip.has(e.target).length === 0) : true;

            // get the list box
            var imageGalleryBox = jQuery('#' + imageGalleryPopupId);

            // check if target is not the list; nor a child of it; nor the
            // colleague tooltip
            if (!imageGalleryBox.is(e.target) && imageGalleryBox.has(e.target).length === 0 && ssobjGlobal.imageGalleryWin.isVisible() && isNotCollTooltip) {
                // close window
                ssobjGlobal.imageGalleryWin.close();
            }
        });

        // set flag
        ssobjGlobal.imageGalleryMouseEventAdded = true;
    }

    ssobjGlobal.imageGalleryWin.imageSrc = imageSrc;
    ssobjGlobal.imageGalleryWin.postId = postId;
    ssobjGlobal.imageGalleryWin.parentId = parentId;

    // attach post to popup comments section
    jQuery("#socialImageGalPostCommentContainer").append(jQuery("#nfpostcontainer" + postId).detach());
    jQuery("#nfpostcontainer" + postId + " .sf_nf_post_msg img").css('max-width', '380px').css('max-height', '135px');
    jQuery("#nfpostcontainer" + postId + " .ss_nf_post_comment_msg img").css('max-width', '380px').css('max-height', '135px');
    jQuery("#nfpostcommenteditor" + postId).empty();
    jQuery("#nfpostcomments" + postId).css('margin-left', '25px');

    // center the modal box
    ssobjGlobal.imageGalleryWin.center();

}

/**
 * Close the currently open modal boxes (i.e. likers list, private message box,
 * view image popup.
 */
function socialCloseCurrentModalBox() {
    // close colleague box
    if (ssapiHasValue(ssobjGlobal.collWin)) {
        if (ssobjGlobal.collWin.isVisible())
            ssobjGlobal.collWin.close();
    }

    // close likers box
    if (ssapiHasValue(ssobjGlobal.likersWin)) {
        if (ssobjGlobal.likersWin.isVisible())
            ssobjGlobal.likersWin.close();
    }

    // close image gallery box
    if (ssapiHasValue(ssobjGlobal.imageGalleryWin)) {
        if (ssobjGlobal.imageGalleryWin.isVisible())
            ssobjGlobal.imageGalleryWin.close();
    }
}

function socialGetPopupWindow() {// checkIfActionIsOnPopupWindow
    // check if deletion is done in popup window
    var imgPopupIsVisible = false;
    var showModalWindow = function() {
    };
    if (ssobjGlobal.imageGalleryWin !== undefined && ssobjGlobal.imageGalleryWin.isVisible()) {
        imgPopupIsVisible = true;

        showModalWindow = function() {
            if (!ssobjGlobal.imageGalleryWin.isVisible()) {
                socialShowImageGalleryPopup(ssobjGlobal.imageGalleryWin.imageSrc, ssobjGlobal.imageGalleryWin.postId, ssobjGlobal.imageGalleryWin.parentId);
            }
        }
    }

    return {
        isVisible : imgPopupIsVisible,
        showModalWindow : showModalWindow
    };
}

function socialShowImageGalleryPopupMobile(imageSrc, postId, parentId) {

    // Close current modal boxes
    socialCloseCurrentModalBox();

    var socialJQuery = jQuery;
    var socialWindow = window;
    var isShownAsPortlet = false;

    // check if inside portlet
    if (parent.jQuery('.ss_portlet').length == 0) {
        // SuiteSocial inside portlet
        socialJQuery = parent.jQuery;
        socialExt = parent.Ext;
        socialWindow = parent.window;
        isShownAsPortlet = true;
    }

    // disable window scroll
    socialJQuery('body,html').css('overflow', 'hidden');

    // check browser width
    var iWidth = socialJQuery('body').width();
    var iHeight = socialWindow.innerHeight;

    var likersText = socialJQuery('#nfpostlikers' + postId).text();

    // prepare html content of image gallery
    var html = '<div id="imageGalleryPopup" style="width: 100%; height: 100%; display: table;">' + '    <div class="socialImageGalleryCloseMobile"><a style="color: white; text-decoration: none; text-align: center;">X</a></div>' + '    <div id="socialImageViewer" class="ss_image_viewer_mobile">' + '        <div id="displayImageDiv" class="ss_display_image">' + '            <img id="originalImage" src="' + imageSrc + '" />' + '        </div>'
            + '        <div id="imageMenuBar" class="social_image_gallery_menu_bar_mobile">' + '            <div id="imageMenuBarLikes">' + likersText + '</div><div id="imageMenuBarComments"><a style="color: white; text-decoration: none; text-align: center;">Comments</a></div>' + '        </div>' + '    </div>' + '    <div id="imageGalleryLoadingMobile" >' + '        <div id="imageGalleryLoadingDiv" >' + ssobjGlobal.loadingImageMarkup + '        </div>' + '    </div>'
            + '    <div id="socialImageGalPostCommentContainer" class="ss_img_gal_post_comment_mobile"></div>' + '</div>';

    if (isShownAsPortlet) {
        html = addMobileCssToHtml(html);
    }

    // if img viewer is not closed, image is clicked from comments
    // update image source
    if (socialJQuery('#imageGalleryPopup:visible').length == 1) {
        socialJQuery("#originalImage").attr("src", imageSrc);
        socialJQuery('#socialImageGalPostCommentContainer').fadeOut();
    } else { // initialize new image viewer object
        socialJQuery('body').append(html);

    }

    if (parentId !== undefined)
        postId = parentId;
    else if (parentId === undefined && isNaN(postId)) { // for new posts or
        // comments with temp
        // ids
        var parentContainer = socialJQuery('#nfpostcommentmsgcontainer' + postId).parent();
        if (parentContainer !== undefined && parentContainer.length != 0) {
            var parentPostId = parentContainer.attr('id').split('nfpostcommentslist');
            if (parentPostId.length > 1)
                postId = parentPostId[1];
        }
    }

    var topPosition = Math.floor(socialJQuery('#nfpostcontainer' + postId).offset().top);
    // scroll body to that position
    socialJQuery('body, html').scrollTop(topPosition);

    var zIndex = socialJQuery('.ss_visual_arrow').css('z-index') + 1;

    socialJQuery('#imageGalleryPopup').css({
        'position' : 'absolute',
        'top' : topPosition,
        'z-index' : zIndex
    });
    socialJQuery('.socialImageGalleryCloseMobile').css('z-index', zIndex + 1);

    // when image is loaded
    socialJQuery('.ss_display_image img').load(function() {

        // hide loading image
        socialJQuery('#imageGalleryLoadingMobile').hide();
        // Show image
        socialJQuery('.ss_image_viewer_mobile').css('display', 'table');
        socialJQuery('.ss_display_image').css('background-color', 'transparent');

        // set the width of the image to blank to get actual width of image
        socialJQuery('.ss_display_image img').css('width', '');
        socialJQuery('.ss_display_image img').css('height', '');

        // socialJQuery('.ss_image_viewer').show();

        // adjust width if image size is bigger than container
        var imageWidth = socialJQuery('.ss_display_image img').width();
        var imageHeight = socialJQuery('.ss_display_image img').height();

        if (imageWidth >= iWidth) {

            if (imageHeight >= iHeight) {
                // width and height is greater than container size
                // determine which has the bigger difference and use that as the
                // basis for full length
                var widthDiff = imageWidth - (iWidth);
                var heightDiff = imageHeight - (iHeight);

                if (widthDiff >= heightDiff) {
                    socialJQuery('.ss_display_image img').css('width', '100%');
                } else {
                    socialJQuery('.ss_display_image img').css('height', iHeight);
                }

            } else {
                // width is greater than container, height is not greater than
                // container
                // Set width to cover the full length of container and let
                // height adjust automatically
                socialJQuery('.ss_display_image img').css('width', '100%');
            }

        } else {

            if (imageHeight >= iWidth) {
                // adjusted the height of the image
                socialJQuery('.ss_display_image img').css('height', iHeight);
            } else {
                // image is smaller than container in both width and height
                // dispaly image as original size

            }

        }

        // get the new image width and height based on adjusted height
        imageWidth = socialJQuery('.ss_display_image img').width();
        imageHeight = socialJQuery('.ss_display_image img').height();

        if (imageWidth < iWidth) {
            // set background image to black when width is smaller than
            // container
            socialJQuery('.ss_image_viewer_mobile').css('background-color', 'black');
        } else {
            // set containter to fit the image
            socialJQuery('ss_image_viewer_mobile').css('width', imageWidth);
        }

        if (imageHeight < iHeight) {
            // set background image to black when height is smaller than
            // container
            socialJQuery('.ss_image_viewer_mobile').css('background-color', 'black');
        } else {
            // set containter to fit the image
            socialJQuery('ss_image_viewer_mobile').css('height', iHeight);
        }

        socialJQuery('.socialImageGalleryCloseMobile').click(function() {
            socialJQuery('#imageGalleryPopup').remove();
        });

        socialJQuery('#imageMenuBarComments').on('click', function() {
            // show post and comments
            socialJQuery('.ss_img_gal_post_comment_mobile').fadeIn(FADEIN_SPEED);
        });

        // set max-height of the post comment container to 80% of image viewer
        var imageViewerHeight = socialJQuery('.ss_image_viewer_mobile').height();
        socialJQuery('.ss_img_gal_post_comment_mobile').css('max-height', imageViewerHeight * 0.8);

        // set height of the post comment container based on content

        // click on the image to close the post comment box
        // add mouse up event on document to hide the list on click of the body
        // other than the list

        socialJQuery('.ss_image_viewer_mobile').mouseup(function(e) {

            // get the list box
            var postCommentBox = socialJQuery('#socialImageGalPostCommentContainer');
            // check if listbox is visible.
            var isPostCommentBoxVisible = socialJQuery('#socialImageGalPostCommentContainer:visible').length === 1;

            // check if target is not the list; nor a child of it; nor the
            // colleague tooltip
            if (!postCommentBox.is(e.target) && postCommentBox.has(e.target).length === 0 && jQuery('#imageGalleryPopup:visible').length == 1 && isPostCommentBoxVisible) {
                // hide post comment box
                postCommentBox.fadeOut();
            }
        });

    });

    var nextPostId = socialJQuery("#nfpostcontainer" + postId).next().attr('id');

    // capture before close event
    socialJQuery('#imageGalleryPopup').on('remove', function() {
        // enable back window scroll
        socialJQuery('body,html').css('overflow', '');

        // attach back to nfpost
        socialJQuery('#nfpostcomments' + postId).css('margin-left', '45px');
        socialJQuery("#nfpostcommenteditor" + postId).empty();

        if (nextPostId === undefined)
            jQuery("#nfposts").prepend(socialJQuery("#nfpostcontainer" + postId).detach());
        else
            jQuery("#" + nextPostId).before(socialJQuery("#nfpostcontainer" + postId).detach());

        // return attached images to original sizes by removing max width and
        // height
        socialJQuery("#nfpostcontainer" + postId + " .sf_nf_post_msg img").css('max-width', '100%').css('max-height', '');
        socialJQuery("#nfpostcontainer" + postId + " .ss_nf_post_comment_msg img").css('max-width', '100%').css('max-height', '');

        // proceed on closing
        return true;
    });

    // attach post to popup comments section
    socialJQuery("#socialImageGalPostCommentContainer").append(jQuery("#nfpostcontainer" + postId).detach());
    socialJQuery("#nfpostcontainer" + postId + " .sf_nf_post_msg img").css('max-width', '380px').css('max-height', '135px');
    socialJQuery("#nfpostcontainer" + postId + " .ss_nf_post_comment_msg img").css('max-width', '380px').css('max-height', '135px');
    socialJQuery("#nfpostcommenteditor" + postId).empty();
    socialJQuery("#nfpostcomments" + postId).css('margin-left', '25px');
    socialJQuery('.ss_img_gal_post_comment_mobile').hide();

    // add event listener for like to change when likers text changes
    socialJQuery('#nfpostlikers' + postId).bind('likersChanged', function() {
        socialJQuery('#imageMenuBarLikes').text(socialJQuery(this).text());
    });

    socialJQuery('#nfpostcommenteditor' + postId).focus(function() {
        setTimeout(function() {
            var position = Math.floor(jQuery('#socialImageGalPostCommentContainer').height() + 20);
            jQuery('#socialImageGalPostCommentContainer').scrollTop(position);
        }, 700);
    });

}

function addMobileCssToHtml(html) {
    var cssContent = jQuery('#suitesocialInlineCss')[0].innerHTML;
    html += '<style type="text/css">' + cssContent + '</style>';

    return html;
}

/**
 * Show give Kudos popup dialog using functionality provided by core.
 * 
 * @param employeeId
 */
function socialShowKudosPopup(employeeId, employeeName) {
    if (!isKudosIntegrationEnabled() || ssapiGetRole() === 'Customer' || ssapiGetRole(employeeId) === 'Customer') {
        return;
    }

    var width = 1024;
    var height = 490;
    var isRecognitionGiven = false;

    nlExtOpenWindow("/app/hcm/perfmgmt/kudospopupservice.nl", "KudosPopup", // title
    width, // Layout is optimized for approx this height/width.
    height, null, // fields
    true, // show scrollbars
    "Kudos",
    // listeners
    {
        "hide" : function(data) {
            // disable window scroll
            jQuery('body,html').css('overflow', '');

            // If kudos has not been saved exit
            if (!isRecognitionGiven) {
                return;
            }

            var frameObjects = getFrameVariables();
            var kudosJQuery = frameObjects['jQuery'];

            var recipients = [];
            kudosJQuery('#ns-kudos-input-employee option').each(function() {
                recipients.push({
                    id : kudosJQuery(this).val(),
                    name : kudosJQuery(this).text()
                });
            });

            var description = kudosJQuery('#ns-kudos-input-message').val();

            var orgValuesList = [];
            kudosJQuery('#ns-kudos-input-org-values-selectable div.ui-selected').each(function() {
                orgValuesList.push({
                    name : kudosJQuery(this).find('span').text(),
                    imageUrl : kudosJQuery(this).find('img').attr('src')
                });
            });

            // call addPost to Post to SuiteSocial Newsfeed
            var message = description;
            ssobjGlobal.imageFile = null;
            var currentChannel = ssobjGlobal.channel;
            ssobjGlobal.channel = getKudosRecognitionChannel();
            ssobjGlobal.recordType = null;
            ssobjGlobal.recordId = null;
            ssobjGlobal.recipient = recipients;
            ssobjGlobal.orgValues = orgValuesList.length == 0 ? '' : orgValuesList;

            socialAddPostCommon(message, null, true);
            ssobjGlobal.channel = currentChannel;
            isRecognitionGiven = false;

            // disable window scroll
            jQuery('body,html').css('overflow', '');
        },
        "show" : function _load(data) {
            isRecognitionGiven = false;
            var def = jQuery.Deferred();

            def.then(function() {
                console.log('kudos iframe found');
                var frameObjects = getFrameVariables();
                var kudosJQuery = frameObjects['jQuery'];

                kudosJQuery('#ns-kudos-input-submit').click(function() {
                    isRecognitionGiven = true;
                });
                kudosJQuery('.selectize-input input').val(employeeName).trigger('keyup');

                var kudosDef = kudosJQuery.Deferred();
                kudosDef.done(function() {
                    console.log('selecting employee');
                    kudosJQuery('.selectize-dropdown-content div[data-value="' + employeeId + '"]').click();
                });

                var kudosRetry = 10; // max 5 second waiting time
                setTimeout(isFoundEmployee, 500);

                function isFoundEmployee() {
                    console.log('checking if employee found... ' + kudosRetry);
                    kudosRetry--;

                    if (kudosJQuery('.selectize-dropdown-content div[data-value="' + employeeId + '"]').length != 0) {
                        kudosDef.resolve();
                        return;
                    }

                    if (kudosRetry == 0) {
                        return;
                    }

                    setTimeout(isFoundEmployee, 500);
                }

            });
            var retries = 10; // max 10 second waiting time
            setTimeout(checkKudosIframe, 1000);

            function checkKudosIframe() {
                console.log('checking for kudos iframe : ' + retries);
                retries--;
                var frameObjects = getFrameVariables();
                var kudosJQuery = frameObjects['jQuery'];
                
                if (kudosJQuery != undefined && typeof kudosJQuery == "function") {
                    var inputBox = kudosJQuery('.selectize-input input');
                    if (inputBox != undefined && inputBox.length > 0) {
                        def.resolve();
                        return;
                    }
                }

                if (retries == 0) {
                    def.reject('Kudos Popup not found');
                    return;
                }

                setTimeout(checkKudosIframe, 1000);
            }

            // disable window scroll
            jQuery('body,html').css('overflow', 'hidden');
        }
    });

}

function getFrameVariables() {

    // iframe DOMDocument
    var frameVariables = {
        doc : getFrameDoc()
    };

    frameVariables.frame = document.getElementById('KudosPopup_frame');

    if (frameVariables.frame === null) {
        return frameVariables;
    }

    // iframe window object
    frameVariables.win = frameVariables.frame.contentWindow;

    if (frameVariables.win) {
        // iframe jQuery
        frameVariables.jQuery = frameVariables.win.jQuery;
    }

    return frameVariables;

}

function getFrameDoc() {

    var oIframe = document.getElementById('nsiframe');

    if (oIframe === null) {

        return null;

    }

    var oDoc = (oIframe.contentWindow || oIframe.contentDocument);

    if (oDoc.document) {

        oDoc = oDoc.document;

    }

    return oDoc;

}

function getKudosRecognitionChannel() {
    if (ssobjGlobal.recognitionChannelId) {
        return ssobjGlobal.recognitionChannelId;
    }

    var filters = [ new nlobjSearchFilter('name', null, 'is', 'Recognition') ];
    var columns = [];
    var records = nlapiSearchRecord('customrecord_newsfeed_channel', null, filters, columns);
    if (records === null) {
        return null;
    }

    // return the first record's internal id
    var channelId = records[0].getId();
    ssobjGlobal.recognitionChannelId = channelId;
    return channelId;
}

function socialCreateEmployeeMentionTag(param) {
    var mention = {
        name : param.name,
        objecttype : 'record',
        recordtype : 'employee',
        id : param.id,
        url : nlapiResolveURL('record', 'employee', param.id)
    };

    return '{' + JSON.stringify(mention) + '}';
}

/**
 * Checks if Kudos feature is enabled in account
 */
function isKudosIntegrationEnabled() {
    // check if kudos feature in core is enabled
    var context = nlapiGetContext();
    var isKudosFeatureEnabled = context.getSetting('FEATURE', 'kudos') == 'T';
    if (isKudosFeatureEnabled) {
        // check setup assistant setting if kudos integration is enabled.
        if (context.getSetting('SCRIPT', 'custscript_ss_kudos_integration') == 'T') {
            return true;
        }
    }
    return false;
}