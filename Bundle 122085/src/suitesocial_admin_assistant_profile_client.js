/**
 * Module Description Here You are using the default templates which should be
 * customized to your needs. You can change your user name under
 * Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version Date Author Remarks 1.00 02 Jan 2012 tcaguioa
 * 
 */
var tree;
var selectedRowClass = 'treeitemselected';// 'portlettexthlctr';
var allAutoSubsChanged = false;

/**
 * Add indexOf() for Array object since not all browsers support it
 */
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
            if (this[i] === obj) {
                return i;
            }
        }
        return -1;
    };
}

function ssapiSaveRecordSubs(profileId) {

    var logger = new ssobjLogger(arguments);
    // delete all user subscriptions then add selected
    // get list of user record subscriptions
    var s = new SuiteSocialAutoSubscribeSearch();
    var userRecordSubs = s.Subscriber_ANYOF(profileId).searchRecord();
    if (userRecordSubs !== null) {
        logger.log('deleting: ' + userRecordSubs.length);
        for (var x = 0; x < userRecordSubs.length; x++) {
            var id = userRecordSubs[x].getValue('internalid');
            nlapiDeleteRecord(REC.SUITESOCIAL_AUTO_SUBSCRIBE, id);
        }
    }

    // var selNodes = tree.getChecked();
    // for some reasons, the line above does not work consistently, so we need
    // to loop thru the nodes and see
    // if it is checked
    var allNodes = tree.getRootNode().childNodes[0].childNodes;
    for (var i = 0; i < allNodes.length; i++) {
        var node = allNodes[i];
        if (node.getUI().isChecked()) {
            var r = new SuiteSocialAutoSubscribe();
            r.setSubscriberValue(profileId).setRecordSubscriptionValue(node.id).submitRecord();
        }

    }
}

function ssapiSaveRelatedRecordSubs(profileId) {
    var logger = new ssobjLogger(arguments);
    // delete all user subscriptions then add selected
    // get list of user record subscriptions
    var s = new MyRelatedRecordSubscriptionSearch();
    var userSubs = s.Subscriber_ANYOF(profileId).searchRecord();
    if (userSubs !== null) {
        logger.log('deleting: ' + userSubs.length);
        for (var x = 0; x < userSubs.length; x++) {
            var id = userSubs[x].getValue('internalid');
            nlapiDeleteRecord(REC.MY_RELATED_RECORD_SUBSCRIPTION, id);
        }
    }
    // var selNodes = tree.getChecked();
    // for some reasons, the line above does not work consistently, so we need
    // to loop thru the nodes and see
    // if it is checked
    var allNodes = tree.getRootNode().childNodes[1].childNodes;
    for (var i = 0; i < allNodes.length; i++) {
        var node = allNodes[i];
        if (node.getUI().isChecked()) {
            var r = new MyRelatedRecordSubscription();
            r.setSubscriberValue(profileId).setRelatedRecordSubscriptionValue(node.id).submitRecord();
        }

    }
    // var selNodes = tree.getChecked();
    // Ext.each(selNodes, function(node){
    // logger.log('adding: ' + node.text);
    // if (node.parentNode.text == 'Related Record Subscriptions') {
    // var r = new MyRelatedRecordSubscription();
    // r.setSubscriberValue(profileId).setRelatedRecordSubscriptionValue(node.id).submitRecord();
    // }
    // });

}

function ssapiSaveRelatedTransactionsSubs(profileId) {

    var logger = new ssobjLogger(arguments);
    // delete all user subscriptions then add selected
    // get list of user record subscriptions
    var s = new MyRelatedTransactionSubsSearch();
    var userRelatedTransactionsSubs = s.SuiteSocialProfile_ANYOF(profileId).searchRecord();
    if (userRelatedTransactionsSubs !== null) {
        logger.log('deleting: ' + userRelatedTransactionsSubs.length);
        for (var x = 0; x < userRelatedTransactionsSubs.length; x++) {
            var id = userRelatedTransactionsSubs[x].getValue('internalid');
            nlapiDeleteRecord('customrecord_ss_my_related_transaction_s', id);
        }
    }

    var allNodes = tree.getRootNode().childNodes[2].childNodes;
    for (var i = 0; i < allNodes.length; i++) {
        var node = allNodes[i];
        if (node.getUI().isChecked()) {
            var r = new MyRelatedTransactionSubs();
            r.setSuiteSocialProfileValue(profileId).setSuiteSocialRecordValue(node.id).submitRecord();
        }

    }
}

function ssapiSaveSavedSearchSubs(profileId) {
    var logger = new ssobjLogger(arguments);
    // delete all user subscriptions then add selected
    // get list of user record subscriptions
    var s = new MySavedSearchSubscriptionSearch();
    var userSubs = s.Profile_ANYOF(profileId).searchRecord();
    if (userSubs !== null) {
        logger.log('deleting: ' + userSubs.length);
        for (var x = 0; x < userSubs.length; x++) {
            var id = userSubs[x].getValue('internalid');
            nlapiDeleteRecord(REC.MY_SAVED_SEARCH_SUBSCRIPTION, id);
        }
    }
    // var selNodes = tree.getChecked();
    // for some reasons, the line above does not work consistently, so we need
    // to loop thru the nodes and see
    // if it is checked
    var allNodes = tree.getRootNode().childNodes[3].childNodes;
    for (var i = 0; i < allNodes.length; i++) {
        var node = allNodes[i];
        if (node.getUI().isChecked()) {
            var r = new MySavedSearchSubscription();
            r.setProfileValue(profileId).setSavedSearchValue(node.id).submitRecord();
        }

    }

    // var selNodes = tree.getChecked();
    // Ext.each(selNodes, function(node){
    // logger.log('adding: ' + node.text);
    // if (node.parentNode.text == 'Saved Search Subscriptions') {
    // var r = new MySavedSearchSubscription();
    // r.setProfileValue(profileId).setSavedSearchValue(node.id).submitRecord();
    // }
    // });
}

function saveRecord() {
    // check dupe channels
    var type = 'recmachcustrecord_suitesocial_profile_ch_prof';
    var count = nlapiGetLineItemCount(type);
    var items = [];
    if (count > 0) {
        for (var i = 1; i <= count; i++) {
            var item = nlapiGetLineItemText(type, 'custpage_ss_prof_ch_ch', i);
            if (items.indexOf(item) > -1) {
                uiShowError(item + ' ' + 'is entered more than once.'.tl());
                return false;
            }
            items.push(item);
        }
    }

    // check dupe colleagues
    type = 'recmachcustrecord_suitesocial_colleague_profile';
    count = nlapiGetLineItemCount(type);
    items = [];
    if (count > 0) {
        for (i = 1; i <= count; i++) {
            item = nlapiGetLineItemText(type, 'custrecord_suitesocial_colleague_sub_pro', i);
            if (items.indexOf(item) > -1) {
                uiShowError(item + ' ' + 'is entered more than once.'.tl());
                return false;
            }
            items.push(item);
        }
    }

    if (allAutoSubsChanged) {
        ssapiWait();
        var empId = nlapiGetContext().getUser();
        var profileId = ssapiGetEmployeeProfileId(empId);
        ssapiSaveRecordSubs(profileId);
        ssapiSaveRelatedRecordSubs(profileId);
        ssapiSaveSavedSearchSubs(profileId);
        ssapiSaveRelatedTransactionsSubs(profileId);
    }
    return true;
}

function ssapiShowRecordSubs(profileId) {
    /**
     * record subscriptions
     */
    // get list of user record subscriptions
    var s = new SuiteSocialAutoSubscribeSearch();
    var userRecordSubs = s.Subscriber_ANYOF(profileId).searchRecord();

    // get the list of subscriptions
    var recordSubs = {
        text : 'Record Subscriptions'.tl(),
        leaf : false
    };
    s = new RecordSubscriptionSearch();
    // sort by name
    s.addNameColumn().addIdColumn().addDescriptionColumn();
    (s.getColumns())[0].setSort();
    var results = s.IsInactive_ISFALSE().searchRecord();
    if (results === null) {
        recordSubs.children = [ {
            text : 'None'.tl(),
            leaf : true
        } ];
    } else {
        recordSubs.children = [];
        for (var i = 0; i < results.length; i++) {
            var item = {
                leaf : true
            };
            item.text = results[i].getValue('name');
            item.id = results[i].getValue('internalid');
            item.qtip = results[i].getValue(s.DESCRIPTION);
            // check if user is subscribed
            var recordSubsId = item.id;
            var found = false;
            if (userRecordSubs !== null) {
                for (var x = 0; x < userRecordSubs.length; x++) {
                    if (userRecordSubs[x].getValue('custrecord_suitesocial_autosubscribe_typ') == recordSubsId) {
                        found = true;
                        break;
                    }
                }
            }
            item.checked = found;
            if (found) {
                item.cls = selectedRowClass;
            }

            recordSubs.children.push(item);
        }
    }

    return recordSubs;
}

function ssapiShowRelatedTransactionsSubs(profileId) {

    /**
     * related transactions subscriptions
     */
    // get list of user transactions subscriptions
    var s = new MyRelatedTransactionSubsSearch();
    var userSubs = s.SuiteSocialProfile_ANYOF(profileId).searchRecord();
    // get list of records with createdfrom field
    var recordsWithCreatedFromField = socialSuiteletProcess('getRecordsWithCreatedFromField', 'any');

    // get the list of subscriptions
    var availableSubs = {
        text : 'Related Transaction Subscriptions'.tl(),
        leaf : false
    };
    s = new SuiteSocialRecordSearch();
    s.addTypeColumn().addIdColumn();
    (s.getColumns())[0].setSort();
    var results = s.IsInactive_ISFALSE().searchRecord();
    if (results === null) {
        availableSubs.children = [ {
            text : 'None',
            leaf : true
        } ];
    } else {
        results.sort(function(a, b) {
            return a.getText(s.TYPE) < b.getText(s.TYPE);
        });
        availableSubs.children = [];
        for (var i = 0; i < results.length; i++) {
            var recordName = results[i].getText(s.TYPE);
            if (recordsWithCreatedFromField.indexOf(recordName) == -1) {
                // has no createdfrom field
                continue;
            }

            var item = {
                leaf : true
            };
            item.text = recordName;
            item.id = results[i].getValue('internalid');
            item.qtip = 'Get notified even if you are only subscribed to the parent transactions of '.tl() + results[i].getText(s.TYPE);
            // check if user is subscribed
            var availableSubsId = item.id;
            var found = false;
            if (userSubs !== null) {
                for (var x = 0; x < userSubs.length; x++) {
                    if (userSubs[x].getValue('custrecord_ss_mrts_ss_record') == availableSubsId) {
                        found = true;
                        break;
                    }
                }
            }
            item.checked = found;
            if (found) {
                item.cls = selectedRowClass;
            }
            availableSubs.children.push(item);
        }
    }

    return availableSubs;
}

function ssapiShowRelatedRecordSubs(profileId) {
    /**
     * related record subscriptions
     */
    // get list of user record subscriptions
    var s = new MyRelatedRecordSubscriptionSearch();
    var userSubs = s.Subscriber_ANYOF(profileId).searchRecord();

    // get the list of subscriptions
    var availableSubs = {
        text : 'Related Record Subscriptions'.tl(),
        leaf : false
    };
    s = new RelatedRecordSubscriptionSearch();
    s.addNameColumn().addIdColumn().addDescriptionColumn();
    (s.getColumns())[0].setSort();
    var results = s.IsInactive_ISFALSE().searchRecord();
    if (results === null) {
        availableSubs.children = [ {
            text : 'None',
            leaf : true
        } ];
    } else {
        availableSubs.children = [];
        for (var i = 0; i < results.length; i++) {
            var item = {
                leaf : true
            };
            item.text = results[i].getValue('name');
            item.id = results[i].getValue('internalid');
            item.qtip = results[i].getValue(s.DESCRIPTION);
            // check if user is subscribed
            var availableSubsId = item.id;
            var found = false;
            if (userSubs !== null) {
                for (var x = 0; x < userSubs.length; x++) {
                    if (userSubs[x].getValue('custrecord_suitesocial_sublink_link') == availableSubsId) {
                        found = true;
                        break;
                    }
                }
            }
            item.checked = found;
            if (found) {
                item.cls = selectedRowClass;
            }
            availableSubs.children.push(item);
        }
    }

    return availableSubs;
}

function ssapiShowSavedSearchSubs(profileId) {
    /**
     * related record subscriptions
     */
    // get list of user record subscriptions
    var s = new MySavedSearchSubscriptionSearch();
    var userSubs = s.Profile_ANYOF(profileId).searchRecord();

    // get the list of subscriptions
    var availableSubs = {
        text : 'Saved Search Subscriptions'.tl(),
        leaf : false
    };
    s = new SavedSearchSubscriptionSearch();
    s.addNameColumn().addIdColumn().addRecordScriptIDColumn().addDescriptionColumn();
    // sort by name
    (s.getColumns())[0].setSort();
    var results = s.IsInactive_ISFALSE().searchRecord();
    if (results === null) {
        availableSubs.children = [ {
            text : 'None',
            leaf : true
        } ];
    } else {
        availableSubs.children = [];
        for (var i = 0; i < results.length; i++) {
            // get script id
            var recordScriptId = results[i].getValue('custrecord_sss_record_script_id');
            if (ssapiHasRecordAccess(recordScriptId) == false) {
                continue;
            }

            var item = {
                leaf : true
            };
            item.text = results[i].getValue('name');
            item.id = results[i].getValue('internalid');
            item.qtip = results[i].getValue(s.DESCRIPTION);
            item.iconCls = 'iconCls';
            // check if user is subscribed
            var availableSubsId = item.id;
            var found = false;
            if (userSubs !== null) {
                for (var x = 0; x < userSubs.length; x++) {
                    if (userSubs[x].getValue('custrecord_ss_msss_saved_search') == availableSubsId) {
                        found = true;
                        break;
                    }
                }
            }
            item.checked = found;
            if (found) {
                item.cls = selectedRowClass;
            }
            availableSubs.children.push(item);
        }
    }

    return availableSubs;
}

function ssapiSubsSearch() {
    var logger = new ssobjLogger(arguments);
    Ext.onReady(function() {
        Ext.get('subsSearch').on('keyup', function(e) {

            var searchString = document.getElementById('subsSearch').value.toLowerCase().trim();

            var root = tree.getRootNode();
            var childNodes = root.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var folder = childNodes[i];
                // alert(folder.childNodes.length);
                for (var j = 0; j < folder.childNodes.length; j++) {
                    var subsNode = folder.childNodes[j];
                    // alert(subsNode.text+ '
                    // '+subsNode.text.indexOf(searchString));
                    if (subsNode.text.toLowerCase().indexOf(searchString) > -1) {
                        // subsNode.getUI().addClass('treeitemselected');
                        subsNode.getUI().show();
                    } else {
                        // subsNode.getUI().removeClass('treeitemselected');
                        subsNode.getUI().hide();
                    }
                    // alert(subsNode.text);
                }
            }
        });
    });
}

function ssapiShowAllAutoSubsEntry() {
    // if customer, skip this
    if (nlapiGetContext().getRoleCenter() == 'CUSTOMER') {
        return;
    }

    //
    ssapiSubsSearch();
    //
    var empId = nlapiGetContext().getUser();
    var profileId = ssapiGetEmployeeProfileId(empId);
    var data = [];
    data.push(ssapiShowRecordSubs(profileId));
    data.push(ssapiShowRelatedRecordSubs(profileId));
    data.push(ssapiShowRelatedTransactionsSubs(profileId));
    data.push(ssapiShowSavedSearchSubs(profileId));

    /* height: 100, */
    // treeAllAutosub was defined in user event beforeload
    Ext.onReady(function() {

        var logger = new ssobjLogger('Ext.onReady');

        Ext.QuickTips.init();
        tree = new Ext.tree.TreePanel({
            renderTo : 'treeAllAutosub',
            width : '100%',
            useArrows : true,
            autoScroll : true,
            animate : true,
            enableDD : false, /* drag and drop */
            containerScroll : true,
            rootVisible : false,
            frame : false,

            root : new Ext.tree.AsyncTreeNode({
                expanded : true,
                children : data
            }),

            listeners : {
                'checkchange' : function(node, checked) {
                    NS.form.setChanged(true);
                    allAutoSubsChanged = true;
                    if (checked) {
                        node.getUI().addClass(selectedRowClass);
                    } else {
                        node.getUI().removeClass(selectedRowClass);
                    }
                },
                'click' : function(node, checked) {

                    // fireEvent('checkchange', !node.checked);
                    // alert('click');
                    // alert(typeof node.checked);
                    // if (typeof node.checked == 'undefined' || node.checked
                    // === false) {
                    // node.checked = true;
                    // }
                    // else {
                    // node.checked = false;
                    // }
                    // node.checked = true;
                    // if (checked) {
                    // alert('checked ' + node.text);
                    // //node.getUI().addClass('complete');
                    // }
                    // else {
                    // alert('UN checked ' + node.text);
                    // //node.getUI().removeClass('complete');
                    // }
                }
            },

            buttons : [ {
                text : 'Get Completed Tasks',
                handler : function() {
                    // var msg = '', selNodes = tree.getChecked();
                    // Ext.each(selNodes, function(node){
                    // if (msg.length > 0) {
                    // msg += ', ';
                    // }
                    // msg += node.text + node.id;
                    // });
                    // Ext.Msg.show({
                    // title: 'Completed Tasks',
                    // msg: msg.length > 0 ? msg : 'None',
                    // icon: Ext.Msg.INFO,
                    // minWidth: 200,
                    // buttons: Ext.Msg.OK
                    // });
                }
            } ]
        });

        tree.getRootNode().expand(true);
        if (ssapiIsNewUI()) {
            Ext.select('#treeAllAutosub span, #custrecord_suitesocial_profile_all_auto_val span').setStyle({
                'font-size' : '14px',
                'font-family' : 'Open Sans,Helvetica,sans-serif'
            });

            Ext.select('#treeAllAutosub .x-panel-body').setStyle({
                'border' : 'none'
            });
        }
        // hide tree node icons (workaround, cant make the treenode cls or
        // iconCls work)
        Ext.select('.x-tree-node-leaf .x-tree-node-icon').setStyle({
            display : 'none'
        });

        if (ssapiIsNewUI() && Ext.isIE) {
            // HACK: For some reasons, clicking the nodes does not toggle the
            // checkbox. The state of the checkbox is toggled back on mouse
            // release.
            // So we need to set the correct value after an interval
            jQuery('#treeAllAutosub .x-tree-node-cb').click(function(evtData, evt) {

                NS.form.setChanged(true);

                var folders = Ext.select('.x-tree-root-node > li').elements;
                logger.log('folders.length=' + folders.length);
                for (var f = 0; f < folders.length; f++) {
                    var folder = folders[f];
                    logger.log('folder ' + f + '; children count=' + Ext.get(folder).select('.x-tree-node-cb').elements.length);
                    var chkboxes = Ext.get(folder).select('.x-tree-node-cb').elements;
                    for (var i = 0; i < chkboxes.length; i++) {
                        if (chkboxes[i] === evtData.target) {
                            logger.log('match f=' + f + ' i=' + i);
                            setTimeout('tree.getRootNode().childNodes[' + f + '].childNodes[' + i + '].getUI().toggleCheck()', 50);
                            return;
                        }
                    }
                }

            });
        }

        NS.form.setChanged(false);
    });
}

//
// [{
// text: 'Record Subscriptions',
// leaf: false,
// children: [{
// text: 'My Employees',
// leaf: true,
// checked: false,
// id: '1'
// }, {
// text: 'My Sales Team',
// leaf: true,
// checked: false,
// id: '2'
// }, {
// text: 'My Issues',
// leaf: true,
// checked: false
// }]
// }, {
// text: 'Related Record Subscriptions',
// leaf: false,
// children: [{
// text: 'Customers Cases',
// leaf: true,
// checked: false
// }, {
// text: 'Subordinates Ussues',
// leaf: true,
// checked: false
// }]
// }, {
// text: 'Saved Search Subscriptions',
// leaf: false,
// children: [{
// text: 'Saved Search 1',
// leaf: true,
// checked: false
// }, {
// text: 'Saved Search 2',
// leaf: true,
// checked: false
// }, {
// text: 'Saved Search 1',
// leaf: true,
// checked: false
// }, {
// text: 'Saved Search 2',
// leaf: true,
// checked: false
// }, {
// text: 'Saved Search 1',
// leaf: true,
// checked: false
// }, {
// text: 'Saved Search 2',
// leaf: true,
// checked: false
// }]
// }]

function ssapiHideAddButtonRowProfile() {
    Ext.select('.tabBnt').setStyle('display', 'none');
    Ext.select('#tbl_submitas, #tbl_submitnew, #tbl_submitcopy, #tbl_delete').setStyle('display', 'none');
    // bottom buttons
    Ext.select('#tbl_secondarysubmitas, #tbl_secondarysubmitnew, #tbl_secondarysubmitcopy, #tbl_secondarydelete').setStyle('display', 'none');
    // var labels = ['New SuiteSocial Profile Channel', 'New SuiteSocial
    // Colleague'];
    // labels.push('New SuiteSocial Digest Schedule');
    // labels.push('New My Saved Search Subscription');
    // // Issue: 217048 [SuiteSocial] In the Profile page, hide 'New' item i .
    // labels.push('New My Related Record Subscription');
    // labels.push('New SuiteSocial Auto Subscribe');
    // for (var i = 0; i < labels.length; i++) {
    // ssapiHideNsButton(labels[i]);
    // }
}

/**
 * hides the New item in option dropdown lists
 */
function ssapiHideNewInOptionListProfile() {

    var logger = new ssobjLogger('ssapiHideNewInOptionList', true);
    // drop down item
    var els = document.getElementsByTagName('div');
    for (var i = 0; i < els.length; i++) {
        var text = els[i].innerHTML;
        if (text.length < 2) {
            continue;
        }
        // for the 'New' options, it is in the format '- New -' where the New
        // changes per language
        if (text.substr(0, 2) == '- ') {
            els[i].style.display = 'none';
        }
    }
    logger.log('DONE');
}

function ssapiShowNewInOptionListProfile() {

    var logger = new ssobjLogger('ssapiHideNewInOptionList', true);
    // drop down item
    var els = document.getElementsByTagName('div');
    for (var i = 0; i < els.length; i++) {
        if (els[i].innerHTML == '- New -') {
            els[i].style.display = 'block';
        }
    }

    // // link
    // els = document.getElementsByTagName('a');
    // for (i = 0; i < els.length; i++) {
    // if (els[i].innerHTML.indexOf('&nbsp;New') > -1) {
    // // els[i].style.display = 'none';
    // }
    // }

    // // hide also the new icon
    // els = Ext.select('.i_createnew');
    // for (i = 0; i < els.elements.length; i++) {
    // els.elements[i].style.display = 'block';
    // }
    logger.log('DONE');
}

function profilePageInit() {

    var logger = new ssobjLogger('profilePageInit');
    if (ssapiIsNewUI()) {
        Ext.select('#custrecord_user_tracked_fields_val *, * custrecord_followed_records_val').setStyle({
            'font-size' : '14px',
            'font-family' : 'Open Sans,Helvetica,sans-serif;'
        });
    }
    if (ssapiHasValue(nlapiGetField('customform'))) {
        nlapiSetFieldDisplay('customform', false);
    }
    nlapiSetFieldDisplay('isinactive', false);

    // hide the add buttons in the sublists
    ssapiHideAddButtonRowProfile();

    // S3 Issue 218895 : [SuiteSocial] Asistant - Scheduled Post. The time
    // format you type doesn't work as you would expect.
    // For example, entering "3:00 PM" automatically changed to "3:00 am"
    // This is a core issue
    // https://system.netsuite.com/app/crm/support/issuedb/issue.nl?id=5103910&whence=
    // HACK: capture keyup event
    var id = 'custrecord_suitesocial_digest_sched_tod';
    Ext.get(id).on('keyup', function(e) {
        var value = document.getElementById(id).value;
        if (ssapiHasNoValue(value)) {
            return;
        }
        value = value.replace('P', 'p');
        document.getElementById(id).value = value.replace('M', 'm');
    });

    Ext.get(id).on('onkeyup', function(e) {
        var value = document.getElementById(id).value;
        if (ssapiHasNoValue(value)) {
            return;
        }
        value = value.replace('P', 'p');
        document.getElementById(id).value = value.replace('M', 'm');
    });
    // Issue: 217048 [SuiteSocial] In the Profile page, hide 'New' item i
    // 'inpt_custrecord_suitesocial_profile_image1',
    // get the ids by using firebug ang getting the id of the textbox when the
    // dropdown is in edit mode/has focus
    var ids = [ 'inpt_custrecord_suitesocial_profile_ch_ch1', 'inpt_custrecord_suitesocial_profile_ch_ch2', 'inpt_custrecord_suitesocial_colleague_sub_pro2', 'inpt_custrecord_suitesocial_colleague_sub_pro3', 'recmachcustrecord_suitesocial_colleague_profile_custrecord_suitesocial_colleague_emp_display', 'inpt_custrecord_suitesocial_autosubscribe_typ3', 'inpt_custrecord_suitesocial_autosubscribe_typ4', 'inpt_custrecord_suitesocial_sublink_link4', 'inpt_custrecord_suitesocial_sublink_link5',
            'inpt_custrecord_ss_msss_saved_search6' ];
    for (var i = 0; i < ids.length; i++) {
        id = ids[i];
        // Issue: 219178 [SuiteSocial] suite social profile > error pop up when
        // ac
        // added checking if the element exists
        if (ssapiHasNoValue(document.getElementById(id))) {
            continue;
        }
        Ext.get(id).on('focus', function(e) {
            ssapiHideNewInOptionListProfile();
        });
        Ext.get(id).on('onfocus', function(e) {
            ssapiHideNewInOptionListProfile();
        });
    }

    // hide the new icon
    var els = Ext.select('.i_createnew');
    for (i = 0; i < els.elements.length; i++) {
        if (ssapiHasNoValue(els.elements[i])) {
            continue;
        }
        els.elements[i].style.display = 'none';
    }

    // hide the menu items (list, search) in the header
    var header = document.getElementById('header_ls');
    if (ssapiHasValue(header)) {
        // in IE, we cannot set the innerHTML of the TABLE and TR tag so we
        // cannot do this: document.getElementById('header_ls').innerHTML =
        // 'SuiteSocial Profile'
        // instead, we use innerHTML for the 1st child (cell) only
        header.children[0].innerHTML = 'SuiteSocial Profile';
        // and hide other children (since other children are actually table so
        // updating innerHTML will throw error)
        for (i = 1; i < 7; i++) {
            if (ssapiHasValue(header.children[i])) {
                header.children[i].style.display = 'none';
            }
        }
    }

    ssapiWait();
    ssapiShowAllAutoSubsEntry();
    Ext.Msg.hide();

    NS.form.setChanged(false);

    logger.log('DONE');
}

/**
 * Field Changed event on SuiteSocial Profile. The standard column fields of My
 * Channels are being set whenever their counterpart custom field is set.
 * 
 * @param type
 *        {String} - sublist id of the modified field
 * @param name
 *        {String} - field id of the modified field
 */
function profileFieldChanged(type, name) {
    // check if custom field of My Channels
    if (type == 'recmachcustrecord_suitesocial_profile_ch_prof' && (name == 'custpage_ss_prof_ch_ch' || name == 'custpage_ss_prof_ch_dgst' || name == 'custpage_ss_prof_ch_alrt')) {
        var fldValue = nlapiGetCurrentLineItemValue(type, name);
        console.log('changed! fldValue=' + fldValue);
        // set the value of the standard field
        var stdField = '';
        switch (name) {
        case 'custpage_ss_prof_ch_ch':
            stdField = 'custrecord_suitesocial_profile_ch_ch';
            break;
        case 'custpage_ss_prof_ch_dgst':
            stdField = 'custrecord_suitesocial_profile_ch_dgst';
            break;
        case 'custpage_ss_prof_ch_alrt':
            stdField = 'custrecord_suitesocial_profile_ch_alrt';
            break;
        }
        nlapiSetCurrentLineItemValue(type, stdField, fldValue);
    }
}
