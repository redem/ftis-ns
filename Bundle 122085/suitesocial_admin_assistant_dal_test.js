/**
 * Automated tests for suitesocial_admin_assistant_dal.js functions
 * @author tcaguioa
 */
function testCreateDefaultAutosubTypes(){

    // tear down
    //    ssapiDeleteAll(R.SS_RELATED_RECORD);
    ssapiDeleteAll(R.SS_RECORD_SUBSCRIPTION_ROLE);
    ssapiDeleteAll(REC_AUTOSUB_TYPE);
    ssapiAssertCount(REC_AUTOSUB_TYPE, 0);
    //    ssapiDeleteAll(R.SS_FIELD);
    //    ssapiDeleteAll(R.SS_SCRIPTED_RECORD);
    ssapiDeleteAllIncludingChildren(REC_RECORD);
    ssapiAssertCount(REC_RECORD, 0);
    
    
    // tests    
    var type = "FEATURE";
    
    var message = '';
    
    var autoSubscribeName, autoSubscribeRecordName, fieldName;
    // add ss record    
    var values = {};
    values[FLD_RECORD_AUTOPOST] = 'F';
    
    // My Cases  (assigned)
    autoSubscribeName = 'My Cases';
    autoSubscribeRecordName = 'Case';
    values[FLD_RECORD_TYPE] = autoSubscribeRecordName;
    addRecord(values);
    
    var fieldName = 'Assigned';
    var roleName = 'Support Person';
    var lastCount = 0;
    var context = nlapiGetContext();
    if (context.getSetting(type, "support") == 'T' && isRoleEnabled(roleName)) {
        if (ssapiIsRecordSubscriptionNameExists(autoSubscribeName)) {
            message += ' - ' + autoSubscribeName + ' was not added because record subscription name ' + autoSubscribeName + ' already exists in the database.<br />';
        }
        else 
            if (ssapiIsRecordSubscriptionCombinationExists(autoSubscribeRecordName, fieldName)) {
                message += ' - ' + autoSubscribeName + ' was not added because the combination of record ' + autoSubscribeRecordName + ' and field ' + fieldName + ' already exists in the database.<br />';
            }
            else {
                addDefaultAutoSubscribeAndRole(autoSubscribeName, autoSubscribeRecordName, fieldName, roleName);
                var actualCount = ssapiGetRecordCount(REC_AUTOSUB_TYPE);
                jsUnity.assertions.assertEqual(actualCount, lastCount + 1);
                
                actualCount = ssapiGetRecordCount(R.SS_RECORD_SUBSCRIPTION_ROLE);
                jsUnity.assertions.assertEqual(actualCount, lastCount + 1);
                
                lastCount = lastCount + 1;
            }
    }
    
    // My Customers  (sales rep)
    autoSubscribeName = 'My Customers';
    autoSubscribeRecordName = 'Customer';
    values[FLD_RECORD_TYPE] = autoSubscribeRecordName;
    addRecord(values);
    
    fieldName = 'Sales Rep';
    roleName = 'Sales Person';
    if (context.getSetting(type, "support") == 'T' && isRoleEnabled(roleName)) {
        if (ssapiIsRecordSubscriptionNameExists(autoSubscribeName)) {
            message += ' - ' + autoSubscribeName + ' was not added because record subscription name ' + autoSubscribeName + ' already exists in the database.<br />';
        }
        else 
            if (ssapiIsRecordSubscriptionCombinationExists(autoSubscribeRecordName, fieldName)) {
                message += ' - ' + autoSubscribeName + ' was not added because the combination of record ' + autoSubscribeRecordName + ' and field ' + fieldName + ' already exists in the database.<br />';
            }
            else {
                addDefaultAutoSubscribeAndRole(autoSubscribeName, autoSubscribeRecordName, fieldName, roleName);
                actualCount = ssapiGetRecordCount(REC_AUTOSUB_TYPE);
                jsUnity.assertions.assertEqual(actualCount, lastCount + 1);
                
                actualCount = ssapiGetRecordCount(R.SS_RECORD_SUBSCRIPTION_ROLE);
                jsUnity.assertions.assertEqual(actualCount, lastCount + 1);
                
                lastCount = lastCount + 1;
            }
    }
    // tear down
    //    ssapiDeleteAll(R.SS_RELATED_RECORD);
//    ssapiDeleteAll(R.SS_RECORD_SUBSCRIPTION_ROLE);
//    ssapiDeleteAll(REC_AUTOSUB_TYPE);
    //    ssapiDeleteAll(R.SS_FIELD);
    //    ssapiDeleteAll(R.SS_SCRIPTED_RECORD);
    ssapiDeleteAllIncludingChildren(REC_RECORD);
}


({
    suiteName: 'SuiteSocial CreateDefaultAutosubTypes 1 Tests',
    
    /* dependencies*/
    dependencies: [{ // a relative location, based on the folder of the current test suite
        fileSet: 'generated_suitesocial_orm.js,generated_suitesocial_search.js,suitesocial_record_field_constants.js,tclib_hash_table.js,tclib_common.js,tclib_server.js,suitesocial_admin_assistant_generic_suitelet.js,suitesocial_admin_assistant_dal.js,suitesocial_admin_assistant_common.js',
        parentFolder: '../src'
    }, { // a relative location, based on the folder of the current test suite
        fileSet: 'ssapi_common_test_automation.js',
        parentFolder: '../test'
    }],
    
    
    setUp: function(){
    },
    
    tearDown: function(){
    },
    
    testCreateDefaultAutosubTypes: testCreateDefaultAutosubTypes

});

