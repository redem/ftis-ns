function updateInvoices()
{
   var context = nlapiGetContext();
   var searchresults = nlapiSearchRecord('invoice', 'customsearch1234');
   
   nlapiLogExecution('Debug','Number of records', searchresults.length);
   
   if ( searchresults == null )
      return;
   
   for ( var i = 0; i < searchresults.length; i++ )
   {
   nlapiLogExecution('Debug','Record', i);
      nlapiLogExecution('Debug','Updating Invoice #', searchresults[i].getId());
    
   var transRec = nlapiLoadRecord('invoice', searchresults[i].getId()); 
   nlapiSubmitRecord(transRec); //resubmits the record
   
   //Reschedule when usage is reaching the limit
      if ( context.getRemainingUsage() <= 100 && (i+1) < searchresults.length )
      {
         var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId())
         if ( status == 'QUEUED' )
            break;     
      }
   }
}
