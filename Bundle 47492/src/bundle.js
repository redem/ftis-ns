/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};

TAF.BundleInstall = new function(){
	
	this.afterInstall = _afterInstall;
	this.afterUpdate = _afterUpdate;
	    
	function _afterInstall() {
		renameRawFiles();
		deleteSCOADeployment();
	}
	
	function _afterUpdate() {
		deleteSCOADeployment();
	}
	
	function renameRawFiles() {
		try {
			var parent_folder = nlapiSearchRecord("file", null, new nlobjSearchFilter("name", null, "is", "0ff667bf-1663-447e-b23c-5282653a6bca"), new nlobjSearchColumn("folder"));
			
			if(parent_folder){
				var filters = [new nlobjSearchFilter("name", null, "is", "Raw Files"),
				               new nlobjSearchFilter("parent", null, "is", parent_folder[0].getValue("folder"))];
				var rs = nlapiSearchRecord("folder", null, filters , [new nlobjSearchColumn("name")]);
	
				if(rs){
					nlapiSubmitField("folder", rs[0].getId(), "name", "Raw Files ("+new Date().toString()+")");
				}
			}
		} catch (ex) {
			nlapiLogExecution('ERROR', 'TAF.BundleInstall.renameRawFiles', ex.toString());
		}
	}
	
	function deleteSCOADeployment() {
		try {
			var isOW = nlapiGetContext().getSetting('FEATURE', 'SUBSIDIARIES') == 'T';
			if (!isOW) {
				var sr = nlapiSearchRecord('scriptdeployment', null, [['scriptid', 'is', 'customdeploy_scoa_s']]);
				if (sr && sr.length > 0) {
					nlapiDeleteRecord('scriptdeployment', sr[0].getId());
				}
			}
		} catch (ex) {
			nlapiLogExecution('ERROR', 'TAF.BundleInstall.deleteSCOADeployment', ex.toString());
		}
	}
};

