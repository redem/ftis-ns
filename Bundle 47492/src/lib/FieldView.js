/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};

TAF.FieldView = function _FieldView(fieldMap) {
    this.context = nlapiGetContext();
};

TAF.FieldView.prototype.displayFields = function _displayFields(fieldMap, properties, isClientScript) {
    var field;
    var property;
    var isDisplayed;
    var nsField;
    var intersection;

    for (var fieldId in fieldMap) {
        isDisplayed = true;
        field = fieldMap[fieldId];
        nsField = nlapiGetField(field.id);

        if (!nsField) {
            continue;
        }

        for (var i = 0; properties && i < properties.length; i++) {
            property = properties[i];
            if (property.value === null || property.value === undefined || !field.hasOwnProperty(property.id)) {
                continue;
            }

            intersection = this.getInterSection(field[property.id], property.value);
            if (intersection.length == 0) {
                isDisplayed = false;
                break;
            }
        }

        if (isDisplayed) {
            isClientScript ? nlapiSetFieldDisplay(field.id, true) : nsField.setDisplayType(field.displayType || 'normal');
        } else {
            isClientScript ? nlapiSetFieldDisplay(field.id, false) : nsField.setDisplayType('hidden');
            nlapiSetFieldValue(field.id, '');
        }
    }
};

TAF.FieldView.prototype.getInterSection = function _getInterSection(array1, array2) {
    return array1.filter(function (n) {
        return array2.indexOf(n) !== -1;
    });
};

TAF.FieldView.prototype.moveFieldsToTaxReportingTab = function _moveFieldsToTaxReportingTab(form, fieldMap, referenceFieldId) {
    if (this.context.getExecutionContext() !== 'userinterface') {
        return;
    }

    if (!form) {
        throw nlapiCreateError('MISSING_PARAMETER', 'TAF.FieldView.displayFields: form is required');
    }

    var field;
    var nsField;

    if (!nlapiGetField(referenceFieldId)) {
        return;
    }

    for (var fieldId in fieldMap) {
        field = fieldMap[fieldId];
        nsField = nlapiGetField(field.id);
        if (nsField) {
            form.insertField(nsField, referenceFieldId);
        }
    }
};
