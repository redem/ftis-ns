/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!TAF) { var TAF = {}; }


TAF.AccountDao = function _AccountDao() {
    var object_cache = {};
    var account_id_cache = [];
    var is_one_world = nlapiGetContext().getSetting('FEATURE', 'SUBSIDIARIES') === 'T';
    var has_account_numbering = nlapiLoadConfiguration('accountingpreferences').getFieldValue('ACCOUNTNUMBERS') == 'T';
    var MAX_RESULTS = 1000;
    var includeSCOA = true;
    var hasAccountingContext = parseFloat(nlapiGetContext().version) >= 2017.1;
    
    this.getList = _GetList;
    this.update = _Update;
    
    function _GetList(filters, pIncludeSCOA) {
    	if (typeof(pIncludeSCOA) != 'undefined') {
    		includeSCOA = pIncludeSCOA;
    	}
    	
        populateCache(filters);
        return object_cache;
    }
    
    
    function populateCache(filters) {
        getAccounts(filters);
        if (is_one_world && includeSCOA) { getStatutoryCOA(filters); }
    }
    
    
    function getAccounts(filters) {
    	try {
            var columns = [
                new nlobjSearchColumn('internalid'),
                new nlobjSearchColumn('name'),
                new nlobjSearchColumn('type'),
                new nlobjSearchColumn('description'),
                new nlobjSearchColumn('custrecord_acct_bank_account_number')
            ];
            
            if (is_one_world) {
                columns.push(new nlobjSearchColumn('subsidiary'));
            }
            
            if (has_account_numbering) {
                columns.push(new nlobjSearchColumn('number'));
            }
            
            if (hasAccountingContext) {
                columns.push(new nlobjSearchColumn('localizedname'));

                if(has_account_numbering) {
                    columns.push(new nlobjSearchColumn('localizednumber'));
                }
                
                if(filters.hasOwnProperty('accountingcontext')) {
                    filters.accountingcontext[1] = filters.accountingcontext[1] || '@NONE@'; 
                    
                    var language = nlapiGetContext().getPreference('LANGUAGE');
                    var srchFilter = [ new nlobjSearchFilter('locale', null, 'is', language) ];
                    var rs = nlapiSearchRecord('account', null, srchFilter,null);
                    if(!rs) {
                        language = '@NONE@';
                    }
                    filters.locale = ['is',language];
                }
            } else if(!hasAccountingContext) {
                delete filters.accountingcontext;
            }

            columns[0].setSort();
            
            var _filters = [];
            
            for (var key in filters) {
                _filters.push(new nlobjSearchFilter(key, null, filters[key][0], filters[key][1]));
            }
            
            var search = nlapiCreateSearch('account', _filters, columns);
            var resultSet = search.runSearch();
            var index = 0;
            do {
                var accounts = resultSet.getResults(index, index + MAX_RESULTS);
                for (var i = 0; accounts && i < accounts.length; i++) {
                    account_id_cache.push(accounts[i].getId());
                    object_cache[accounts[i].getId()] = convertRowToObject(accounts[i]);
                }
                index += MAX_RESULTS;
            } while (accounts && accounts.length >= MAX_RESULTS);
        } catch (ex) {
            nlapiLogExecution('ERROR', 'TAF.AccountDao.getAccounts', ex.toString());
        }
    }
    
    
    function getStatutoryCOA(filters) {
        try {
            var columns = [
                new nlobjSearchColumn('custrecord_scoa_account'),
                new nlobjSearchColumn('custrecord_scoa_subsidiary'),
                new nlobjSearchColumn('custrecord_scoa_number'),
                new nlobjSearchColumn('custrecord_scoa_name')
            ];
            
            var filter = [];
            if (account_id_cache.length > 0) {
                filter.push(new nlobjSearchFilter('custrecord_scoa_account', null, 'anyof', account_id_cache));
                filter.push(new nlobjSearchFilter('custrecord_scoa_subsidiary', null, 'is', filters.subsidiary));
                
                var search = nlapiCreateSearch('customrecord_statutory_coa', filter, columns);
                var resultSet = search.runSearch();
                var index = 0;
                do {
                   var statutory_coa = resultSet.getResults(index, index + MAX_RESULTS);
                   for (var i = 0; statutory_coa && i < statutory_coa.length; i++) {
                        var account = object_cache[statutory_coa[i].getValue('custrecord_scoa_account')];
                        account.setSCOAId(statutory_coa[i].getId());
                        account.setSCOAName(statutory_coa[i].getValue('custrecord_scoa_name'));
                        account.setSCOANumber(statutory_coa[i].getValue('custrecord_scoa_number'));
                    }
                   index += MAX_RESULTS;
                } while (statutory_coa && statutory_coa.length >= MAX_RESULTS);
            }
        } catch (ex) {
            nlapiLogExecution('ERROR', 'TAF.AccountDao.getStatutoryCOA', ex.toString());
        }
    }
    
    
    function convertRowToObject(row) {
        var object = new TAF.Account(row.getId());
        
        object.isOneWorld(is_one_world);
        object.setAccountName(row.getValue('name'));
        object.setType(row.getValue('type'));
        object.setDescription(row.getValue('description'));
        object.setBankNumber(row.getValue('custrecord_acct_bank_account_number'));
        if(hasAccountingContext) {
            object.setLocalizedName(row.getValue('localizedname').trim());
        }
        
        if (has_account_numbering) {
            object.setAccountNumber(row.getValue('number'));
            if(hasAccountingContext) {
                object.setLocalizedNumber(row.getValue('localizednumber'));
            }
        }
        
        if (is_one_world) {
            object.setSubsidiary(row.getValue('subsidiary'));
        }
        
        return object;
    }
    
    
    function _Update(accounts) {
        var message = {
            result: 'pass',
            details: 'SCOA_SAVE_CONFIRMATION_MESSAGE'
        };
        
        try {
            var record = {};
            var scoa_id = '';
            
            for (var account in accounts) {
                scoa_id = accounts[account].getSCOAId();
                if (scoa_id) {
                    record = nlapiLoadRecord('customrecord_statutory_coa', scoa_id);
                    
                    if (accounts[account].getSCOAName() || accounts[account].getSCOANumber()) {
                        record.setFieldValue('custrecord_scoa_name', accounts[account].getSCOAName());
                        record.setFieldValue('custrecord_scoa_number', accounts[account].getSCOANumber());
                        
                        nlapiSubmitRecord(record);
                    } else {
                        nlapiDeleteRecord('customrecord_statutory_coa', scoa_id);
                    }
                } else if (accounts[account].getSCOAName() || accounts[account].getSCOANumber()) {
                    record = nlapiCreateRecord('customrecord_statutory_coa');
                    
                    record.setFieldValue('custrecord_scoa_account', accounts[account].getAccountId());
                    record.setFieldValue('custrecord_scoa_subsidiary', accounts[account].getSubsidiary());
                    record.setFieldValue('custrecord_scoa_name', accounts[account].getSCOAName());
                    record.setFieldValue('custrecord_scoa_number', accounts[account].getSCOANumber());
                    
                    nlapiSubmitRecord(record);
                }
            }
            
            return message;
        } catch(e) {
            message = {
                result: 'fail',
                details: 'SCOA_SAVE_ERROR_MESSAGE'
            };
            
            var errorMsg = e.getCode ? e.getCode() + ': ' + e.getDetails() : 'Error: ' + (e.message ? e.message : e);
            
            return message;
        }
    }
};


TAF.DAO = TAF.DAO || {};
TAF.DAO.AccountDao = TAF.AccountDao;