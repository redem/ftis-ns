/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!TAF) { var TAF = {}; }
TAF.DAO = TAF.DAO || {};

TAF.DAO.MappingDao = function _MappingDao() {
	this.cache = {};
	this.MAX_RESULTS = 1000;
};

TAF.DAO.MappingDao.prototype.getList = function _GetList(filters, daoFilters) {
	var category_id = this.getCategory(filters);
	if (category_id == '') { return {}; }

	var mapping_category_list = new TAF.DAO.MappingCategoryDao().getList();
	var key_dao = mapping_category_list[category_id].dao;
	var daoList = key_dao.split(',');

	for (var i=0; i<daoList.length; i++) {
		var dao = daoList[i].trim();
		this.getKeys(category_id, false, dao, daoFilters);
	}

	this.getMappings(category_id, filters);
	return this.cache;
};

TAF.DAO.MappingDao.prototype.getListWithDefault = function _GetListWithDefault(filters, values) {
	var category_id = this.getCategory(filters);
	if (category_id == '') { return {}; }

	var mapping_category_list = new TAF.DAO.MappingCategoryDao().getList();
	var key_dao = mapping_category_list[category_id].dao;
	var daoFilters = this.getMappingDaoFilters(new TAF.DAO.MappingFilterDao().getMappingFilterByIds(mapping_category_list[category_id].filters), values);
	this.getKeys(category_id, true, key_dao, daoFilters);
	this.getMappings(category_id, filters);
	return this.cache;
};

TAF.DAO.MappingDao.prototype.deleteMapping = function _DeleteMapping(id) {
	try {
		nlapiDeleteRecord('customrecord_mapper_keyvalue', id);
	} catch(e) {
		var errorMsg = e.getCode ? e.getCode() + ': ' + e.getDetails() : 'Error: ' + (e.message ? e.message : e);
		nlapiLogExecution('ERROR', 'TAF.DAO.MappingDao.deleteMapping', errorMsg);
	}
};

TAF.DAO.MappingDao.prototype.getCategory = function _GetCategory(filters) {
	var key = 'custrecord_mapper_keyvalue_category';
	return 	filters && filters[key] && filters[key].length > 1 ?
			filters[key][1]: '';
};

TAF.DAO.MappingDao.prototype.getKeys = function _GetKeys(category_id, with_default, key_dao, daoFilters) {
	var default_value = with_default ? new TAF.DAO.MappingValueDao().getDefaultValue(category_id) : '';
	var key_list = new TAF.DAO[key_dao]().getList(daoFilters);

	if (Object.keys(key_list).length > 0) {
		for (var key in key_list) {
			this.cache[key] = new TAF.DAO.Mapping('');
			this.cache[key].category = category_id;
			this.cache[key].key = key;
			this.cache[key].key_text = key_list[key].getName ? key_list[key].getName() : key_list[key].name;
			this.cache[key].value = default_value ? default_value.id : '';
			this.cache[key].value_text = default_value ? default_value.inreport : '';
			this.cache[key].value_name = default_value ? default_value.name : '';
		}
	}
};

TAF.DAO.MappingDao.prototype.getMappings = function _GetMappings(category_id, filters) {
	if (!category_id) {
		throw nlapiCreateError('INVALID_PARAMETER', 'Category id is null.');
	}

	if (!filters || !filters['custrecord_mapper_keyvalue_category']) {
		throw nlapiCreateError('INVALID_PARAMETER', 'Filter parameter is null or custrecord_mapper_keyvalue_category not defined in Filter parameter.');
	}

	if (!filters['custrecord_mapper_keyvalue_category'][0] || !filters['custrecord_mapper_keyvalue_category'][1]) {
		throw nlapiCreateError('INVALID_PARAMETER', 'Operator is not defined in Filter parameter.');
	}

	var values = new TAF.DAO.MappingValueDao().getList({'custrecord_mapper_value_category': ['anyof', category_id]});

	var searchColumns = [
		new nlobjSearchColumn('custrecord_mapper_keyvalue_category'),
		new nlobjSearchColumn('custrecord_mapper_keyvalue_key'),
		new nlobjSearchColumn('custrecord_mapper_keyvalue_value'),
		new nlobjSearchColumn('custrecord_mapper_keyvalue_inputvalue')
	];
	var searchFilters = [new nlobjSearchFilter('custrecord_mapper_keyvalue_category', null, filters['custrecord_mapper_keyvalue_category'][0], filters['custrecord_mapper_keyvalue_category'][1])];

	var search = nlapiCreateSearch('customrecord_mapper_keyvalue', searchFilters, searchColumns);
	var resultSet = search.runSearch();
	var index = 0;

	do {
		var mappings = resultSet.getResults(index, index + this.MAX_RESULTS);
		this.setMappings(mappings, values);
		index += this.MAX_RESULTS;
	} while (mappings && mappings.length >= this.MAX_RESULTS);
};

TAF.DAO.MappingDao.prototype.setMappings = function _setMappings(mappings, values) {
	for (var i = 0; i < mappings.length; i++) {
		var key = mappings[i].getValue('custrecord_mapper_keyvalue_key');
		var object = this.cache[key];

		if (!object) {
			continue;
		}

		object.id = mappings[i].getId();
		object.category = mappings[i].getValue('custrecord_mapper_keyvalue_category');
		object.value = mappings[i].getValue('custrecord_mapper_keyvalue_value');

		if(object.value){
			object.value_text = values[object.value].inreport;
			object.value_name = values[object.value].name;
		}else{
			var inputvalue = mappings[i].getValue('custrecord_mapper_keyvalue_inputvalue');
			object.value = object.value_text = inputvalue;
			object.value_name = '';
		}
	}
};

TAF.DAO.MappingDao.prototype.update = function _Update(mappings) {
	var message = {result: 'pass'};
	var policyNumCategory = new TAF.DAO.MappingCategoryDao().getByCode('MX_POLICY_NUMBER');
	var valuefield = null;

	try {
		var record = {};
		var mapping_id = '';
		for (var mapping in mappings) {
			mapping_id = mappings[mapping].id;
			if (!mappings[mapping].value) {
				this.deleteMapping(mapping_id);
			} else {
				if (mapping_id) {
					record = nlapiLoadRecord('customrecord_mapper_keyvalue', mapping_id);
				} else {
					record = nlapiCreateRecord('customrecord_mapper_keyvalue');
				}

				record.setFieldValue('custrecord_mapper_keyvalue_category', mappings[mapping].category);
				record.setFieldValue('custrecord_mapper_keyvalue_key', mappings[mapping].key);
				valuefield = valuefield ||
					((mappings[mapping].category == policyNumCategory.id) ?
						'custrecord_mapper_keyvalue_inputvalue' : 'custrecord_mapper_keyvalue_value');
				record.setFieldValue(valuefield, mappings[mapping].value);
				nlapiSubmitRecord(record);
			}
		}
		return message;
	} catch(e) {
		var errorMsg = e.getCode ? e.getCode() + ': ' + e.getDetails() : 'Error: ' + (e.message ? e.message : e);
		nlapiLogExecution('ERROR', 'TAF.DAO.MappingDao.Update', errorMsg);
		message = { result: 'fail', error: errorMsg };
		return message;
	}
};

TAF.DAO.MappingDao.prototype.getMappingDaoFilters = function getMappingDaoFilters(filters, values) {
	var mappingDaoFilters = {};
	for (var filter in filters) {
		var currentFilter = filters[filter];
		if (currentFilter.isUi) {
			continue;
		}

		var mappingFilters =  currentFilter.mappingFilters;
		for (var mf in mappingFilters) {
			mappingDaoFilters[mf] = mappingFilters[mf];
		}
	}

	if (values){
		for (var mdf in mappingDaoFilters) {
			if(values[mdf]) {
				mappingDaoFilters[mdf][1] = values[mdf];
			}
		}
	}

	return mappingDaoFilters;
};
