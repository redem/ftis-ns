/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.CONSTANTS = TAF.CONSTANTS || {};

TAF.CONSTANTS.SUITELET = {
    'MAIN': {
        'SUITELET_ID': 'customscript_4599_main_s',
        'DEPLOYMENT_ID': 'customdeploy_4599_main_s'
    },
    'FILTER': {
        'SUITELET_ID': 'customscript_taf_filter',
        'DEPLOYMENT_ID': 'customdeploy_taf_filter'
    }
};

TAF.CONSTANTS.FIELDS = {
    'REPORT': 'report',
    'ACCOUNTING_PERIOD_FROM': 'accountingperiod_from',
    'ACCOUNTING_PERIOD_TO': 'accountingperiod_to',
    'TAX_PERIOD_FROM': 'taxperiod_from',
    'TAX_PERIOD_TO': 'taxperiod_to',
    'SUBSIDIARY': 'subsidiary',
    'INCLUDE_CHILD_SUBS': 'include_child_subs',
    'ACCOUNTING_BOOK': 'accountingbook',
    'SUBSIDIARIES': 'subsidiaries',
    'ACCOUNTING_BOOKS': 'accountingbooks',
    'ACCOUNTING_CONTEXT': 'accountingcontext',
    'START_DATE': 'start_date',
    'END_DATE': 'end_date'
};
