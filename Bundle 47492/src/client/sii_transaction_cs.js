/**
 * Copyright � 2017, Oracle and/or its affiliates. All rights reserved.
 */

var TAF = TAF || {};
TAF.SII = TAF.SII || {};
TAF.SII.CS = TAF.SII.CS || {};

TAF.SII.CS.OnPageInit = function _OnPageInit() {
    TAF.SII.CS.executionContext = TAF.SII.CS.executionContext || nlapiGetContext().getExecutionContext();
    if (TAF.SII.CS.executionContext !== 'userinterface') {
        return;
    }
    var fieldView = TAF.SII.CS.getFieldView();
    var invoiceTypeId = TAF.SII.CONSTANTS.FIELD_MAP.TRANSACTION.receivedInvoiceType.id;

    fieldView.displayFields(TAF.SII.CONSTANTS.FIELD_MAP.TRANSACTION, TAF.SII.CS.getProperties(), true);
    if (nlapiGetField(invoiceTypeId) && !nlapiGetFieldValue(invoiceTypeId)) {
        TAF.SII.CS.setInvoiceTypeValueToInvoice();
    }
};

TAF.SII.CS.OnFieldChanged = function _OnFieldChanged(type, name, line) {
    TAF.SII.CS.executionContext = TAF.SII.CS.executionContext || nlapiGetContext().getExecutionContext();
    if (TAF.SII.CS.executionContext !== 'userinterface') {
        return;
    }
    if (name === 'subsidiary' || name === 'nexus' || name === 'trantype') {
        var fieldView = TAF.SII.CS.getFieldView();
        fieldView.displayFields(TAF.SII.CONSTANTS.FIELD_MAP.TRANSACTION, TAF.SII.CS.getProperties(), true);
    }
};

TAF.SII.CS.getFieldView = function _getFieldView() {
    if (!TAF.SII.CS.fieldView) {
        TAF.SII.CS.fieldView = new TAF.FieldView();
    }

    return TAF.SII.CS.fieldView;
};

TAF.SII.CS.getProperties = function _getProperties() {
    return [
        { id: 'nexus', value: [nlapiGetFieldValue('nexus_country')] },
        { id: 'recordType', value: [TAF.SII.CS.getRecordType()] }
    ];
};

TAF.SII.CS.getRecordType = function _getRecordType() {
    var recordType = nlapiGetRecordType();

    if (recordType === 'creditcardcharge') {
        recordType += '_' + nlapiGetFieldValue('trantype');
    }

    return recordType;
};

TAF.SII.CS.setInvoiceTypeValueToInvoice = function _setInvoiceTypeValueToInvoice() {
    var defaltInvoiceTypeValue = nlapiGetFieldValue('custpage_sii_temp_received_inv_type');
    nlapiSetFieldValue(TAF.SII.CONSTANTS.FIELD_MAP.TRANSACTION.receivedInvoiceType.id, defaltInvoiceTypeValue);
};

