/**
 * Copyright 2017 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.SII = TAF.SII || {};
TAF.SII.CS = TAF.SII.CS || {};

TAF.SII.CS.OnPageInit = function _OnPageInit() {
    TAF.SII.CS.OnPageInit.isOneWorld = nlapiGetContext().getFeature('SUBSIDIARIES');
    var fieldView = TAF.SII.CS.getFieldView();
    fieldView.displayFields(TAF.SII.CONSTANTS.FIELD_MAP.ENTITY, TAF.SII.CS.getProperties());
};

TAF.SII.CS.OnFieldChanged = function _OnFieldChanged(type, name, line) {
    var fieldView = TAF.SII.CS.getFieldView();
    if (TAF.SII.CS.OnPageInit.isOneWorld) {
        if (name === 'subsidiary') {
            fieldView.displayFields(TAF.SII.CONSTANTS.FIELD_MAP.ENTITY, TAF.SII.CS.getProperties());
        }
    } else if (name === 'defaultaddress' || name === 'defaultbilling') {
        fieldView.displayFields(TAF.SII.CONSTANTS.FIELD_MAP.ENTITY, TAF.SII.CS.getProperties());
    }
};

TAF.SII.CS.getFieldView = function _getFieldView() {
    if (!TAF.SII.CS.fieldView) {
        TAF.SII.CS.fieldView = new TAF.FieldView();
    }

    return TAF.SII.CS.fieldView;
};

TAF.SII.CS.getProperties = function _getProperties() {
    TAF.SII.CS.recordType = TAF.SII.CS.recordType || nlapiGetRecordType();
    var nexuses;
    
    if (TAF.SII.CS.OnPageInit.isOneWorld) {
        nexuses = TAF.SII.CS.getNexus();
    } else {
        nexuses = [nlapiGetFieldValue('billcountry')];
    }
    
    return [
        { id: 'nexus', value: nexuses },
        { id: 'recordType', value: [TAF.SII.CS.recordType] }
    ];
};

TAF.SII.CS.getNexus = function _getNexus() {
    var nexuses = [];
    
    try {
        var subsidiaryId = nlapiGetFieldValue('subsidiary');

        if (subsidiaryId) {
            var record = nlapiLoadRecord('subsidiary', subsidiaryId);
            var nexusCount = record.getLineItemCount('nexus') + 1;

            for (var i = 1; i < nexusCount; i++) {
                nexuses.push(record.getLineItemValue('nexus', 'country', i));
            }
        }
    } catch (ex) {
        var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
        nlapiLogExecution('AUDIT', 'TAF.SII.CS.getNexus', errorMsg);
    }

    return nexuses;
};
