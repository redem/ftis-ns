/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.FR = TAF.FR || {};
TAF.FR.Adapter = TAF.FR.Adapter || {};

// START - SAFT Adapter
TAF.FR.Adapter.SAFT = function _SAFT() {
	return {
		journalCode: '',
		journalLib: '',
		ecritureNum: '',
		ecritureDate: '',
		compteNum: '',
		compteLib: '',
		pieceRef: '',
		pieceDate: '',
		ecritureLib: '',
		debit: 0,
		credit: 0,
		validDate: '',
		montantDevise: '',
		iDevise:'',
        compAuxNum: '',
        compAuxLib: '',
        ecritureLet: '',
        dateLet: ''
	};
};

TAF.FR.Adapter.SAFTAdapter = function _SAFTAdapter(params) {
	this.defaultCurrency = params.currency;
    this.isGLSupported = false;
    this.isSubAccountEnabled = params.isSubAccountEnabled;
    this.accountsMap = params.accountsMap;
    this.txnSequenceNo = 0;
    this.lastInternalId = -1;
    this.resource = params.resource;
    this.accountingBook = params.accountingBook;
    this.usesAccountingContext = params.usesAccountingContext;
};

TAF.FR.Adapter.SAFTAdapter.prototype.getSAFTLine = function _getSAFTLine(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT',
				'TAF.FR.Adapter.SAFTAdapter.prototype.getSAFTLine: Parameter txnObj is null.');
	}
	
	var line = null;
	
	if (txnObj.posting === 'T' && txnObj.accountName.length > 0) {		
		try {
			var row = new TAF.FR.Adapter.SAFT();
		    var tranDate = nlapiStringToDate(txnObj.date);
		    var account = this.getAccountInfo(txnObj);
		    var foreign = this.getForeignValues(txnObj);
		    
		    row.journalCode = 'GL';
		    row.journalLib = this.resource.GetString("GENERAL_LEDGER");
		    row.ecritureNum = this.getEcritureNum(txnObj).toString();
		    row.ecritureDate = tranDate;
		    row.compteNum = account.number;
		    row.compteLib = account.name;
		    row.pieceRef = this.getPieceRef(txnObj);
		    row.pieceDate = this.getDate(txnObj);
		    row.ecritureLib = this.getEcritureLib(txnObj);
		    row.debit = txnObj.debit;
		    row.credit = txnObj.credit;
		    row.validDate = tranDate;
		    row.montantDevise = foreign.amount;
		    row.iDevise = foreign.currency;
		    row.compAuxNum = this.getEntityId(txnObj);
		    row.compAuxLib = this.getEntityName(txnObj);	    
		    
		    line = {};
		    line.convertedRow = row;
		    line.lastInternalId = this.lastInternalId;
		    line.txnSequenceNo = this.txnSequenceNo;
		} catch (ex) {
			nlapiLogExecution('ERROR', 'TAF.FR.Adapter.SAFTAdapter.prototype.getSAFTLine', ex.toString());
			throw nlapiCreateError('DATA_ERROR', 'TAF.FR.Adapter.SAFTAdapter.prototype.getSAFTLine');
		}
	}
	
	return line;
};

TAF.FR.Adapter.SAFTAdapter.prototype.getTxnSequenceNo = function _getTxnSequenceNo(id) {
	if(!id) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getTxnSequenceNo: parameter id is null.');
	}
	
	if (id == this.lastInternalId) {
        return this.txnSequenceNo;
    }
    
    this.lastInternalId = id;
    return ++this.txnSequenceNo;
};

TAF.FR.Adapter.SAFTAdapter.prototype.getDate = function _getDate(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
		
    return nlapiStringToDate(txnObj.documentDate || txnObj.date);
};

TAF.FR.Adapter.SAFTAdapter.prototype.getAccountInfo = function _getAccountInfo(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
	
    var scoa = this.accountsMap[txnObj.account];
    var account = {};

    if (this.accountingBook && !this.accountingBook.isPrimary) {
        account.number = scoa.getAccountNumber();
        account.name = scoa.getAccountName();
    } else if (this.usesAccountingContext) {
        account.number = scoa.getLocalizedNumber() || scoa.getAccountNumber();
        account.name = scoa.getLocalizedName() || scoa.getAccountName();
    } else {
        account.number = scoa.getSCOANumber() || scoa.getAccountNumber();
        account.name = scoa.getSCOAName() || scoa.getAccountName();
    }
    
    return account;
};

TAF.FR.Adapter.SAFTAdapter.prototype.getEcritureNum = function _getEcritureNum(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
	
    return this.isGLSupported ? txnObj.glNumber : this.getTxnSequenceNo(txnObj.internalId);
};

TAF.FR.Adapter.SAFTAdapter.prototype.getPieceRef = function _getPieceRef(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
	
    return txnObj.transactionNumber || txnObj.tranId || txnObj.internalId;
};

TAF.FR.Adapter.SAFTAdapter.prototype.getEcritureLib = function _getEcritureLib(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
	
    var account = this.getAccountInfo(txnObj);
    return txnObj.memo || txnObj.memoMain || account.name;
};

TAF.FR.Adapter.SAFTAdapter.prototype.getForeignValues = function _getForeignValues(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
	
    var foreign = {
        amount : '',
        currency : ''
    };
    
    if (((txnObj.fxAmount) && (txnObj.fxAmount != 0)) &&
    	((txnObj.currency) && (txnObj.currency != this.defaultCurrency))) {
        foreign.amount = txnObj.fxAmount;
        foreign.currency = txnObj.currency || this.defaultCurrency;
    }
    
    return foreign;
};

TAF.FR.Adapter.SAFTAdapter.prototype.getEntityId = function _getEntityId(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
	
    return this.isSubAccountEnabled ? txnObj.entityId || txnObj.customerId || txnObj.vendorId || '' : '';
};

TAF.FR.Adapter.SAFTAdapter.prototype.getEntityName = function _getEntityName(txnObj) {
	if(!txnObj) {
		throw nlapiCreateError('MISSING_ARGUMENT', 
				'TAF.FR.Adapter.SAFTAdapter.prototype.getDate: parameter txnObj is null.');
	}
	
    return this.isSubAccountEnabled ? txnObj.entityName || txnObj.customerName || txnObj.vendorName || '' : '';
};

// END - SAFT Adapter