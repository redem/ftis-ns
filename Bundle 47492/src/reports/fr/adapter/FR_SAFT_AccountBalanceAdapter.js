/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * Adapter class for FR SAF-T's opening balance lines.
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 May 2016     isalen
 */
 
if (!TAF) { var TAF = {}; }
TAF.Report = TAF.Report || {};
TAF.Report.Adapter = TAF.Report.Adapter || {};

TAF.Report.Adapter.AccountBalanceAdapter = function AccountBalanceAdapter(params) {
    this.accountsMap = params.accountsMap;
    this.date = params.date;
    this.resource = params.resource;
    this.sequence = params.sequence || 0;
    this.usesAccountingContext = params.usesAccountingContext;
};

TAF.Report.Adapter.AccountBalanceAdapter.prototype.getBalanceLine = function getBalanceLine(line) {
    var balances = this.getBalances(line);
    
    if (!(balances.debit + balances.credit)) {
        return null;
    }
    
    var account = this.accountsMap[line.internalId];
    var balance = new TAF.FR.Adapter.SAFT();
    
    try {
        balance.journalCode = this.resource.type;
        balance.journalLib = this.resource.description;
        balance.ecritureNum = this.resource.type + (++this.sequence).toString();
        balance.ecritureDate = this.date;
        if(this.usesAccountingContext){
            balance.compteNum = account.getLocalizedNumber() || account.getAccountNumber();
            balance.compteLib = account.getLocalizedName() || account.getAccountName();
        } else {
            balance.compteNum = account.getSCOANumber() || account.getAccountNumber();
            balance.compteLib = account.getSCOAName() || account.getAccountName();
        }
        balance.pieceRef = this.resource.pieceRef;
        balance.pieceDate = this.date;
        balance.ecritureLib = this.resource.longDescription;
        balance.debit = balances.debit;
        balance.credit = balances.credit;
        balance.validDate = this.date;
        balance.montantDevise = '';
        balance.iDevise = '';
        balance.compAuxNum = '';
        balance.compAuxLib = '';
        balance.ecritureLet = '';
        balance.dateLet = '';
    } catch(ex) {
        nlapiLogExecution('ERROR', 'TAF.Report.Adapter.AccountBalanceAdapter.getBalanceLine', ex.toString());
        throw ex;
    }
    
    return balance;
};

TAF.Report.Adapter.AccountBalanceAdapter.prototype.getBalances = function getBalances(line) {
    var balances = {debit: 0, credit: 0};
    var balance = 0;
    
    if (line.isLeftSide === 'T') {
        balance = Number(line.debit) - Number(line.credit);
        balances.debit = balance >= 0 ? balance : 0;
        balances.credit = balance < 0 ? Math.abs(balance) : 0;
    } else {
        balance = Number(line.credit) - Number(line.debit);
        balances.credit = balance >= 0 ? balance : 0;
        balances.debit = balance < 0 ? Math.abs(balance) : 0;
    }
    
    return balances;
};
