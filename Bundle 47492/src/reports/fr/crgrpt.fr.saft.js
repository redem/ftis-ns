/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!TAF) { var TAF = {}; }
TAF.Report = TAF.Report || {};

TAF.Report.FR_SAFT_Report = function FR_SAFT_Report(state, params, output, job) {
    TAF.IReportSection.apply(this, arguments);

    var isOneWorld = nlapiGetContext().getFeature('SUBSIDIARIES');
    
    var acctParams = isOneWorld ? {subsidiary: ['anyof', params.subsidiary]} : {};
    if(params.job_params && params.job_params.hasAccountingContext) {
        acctParams.accountingcontext = ['is', params.accountingContext];
    }
    
    var accountingBook = this.getAccountingBook();
    var isSCOAIncluded = !accountingBook || accountingBook.isPrimary;

    params.scoaAccounts = new TAF.AccountDao().getList(acctParams);
    params.multiBookAccounts = new TAF.AccountDao().getList(acctParams,isSCOAIncluded);
    
    this.GetOutline = function() {
        return {
            'Section': TAF.Report.Section.FR_SAFT_AuditFile.bind(this, state, params, output, job),
            'SubSections': [
                {'Section': TAF.Report.Section.FR_SAFT_OpeningBalance.bind(this, state, params, output, job)},
                {'Section': TAF.Report.Section.FR_SAFT_Ledger.bind(this, state, params, output, job)},
                {'Section': TAF.Report.Section.FR_SAFT_ClosingBalance.bind(this, state, params, output, job)}
            ]
        };
    };
};

TAF.Report.FR_SAFT_Report.prototype = Object.create(TAF.IReportSection.prototype);

var FR_SAFT_TXT_Report = function FR_SAFT_TXT_Report(state, params, output, job) {
    params.formatter = new TAF.FR.Formatter.SAFTFormatter();
    params.filename = ''; //set with actual values in setFileName
    TAF.Report.FR_SAFT_Report.call(this, state, params, output, job);
};
FR_SAFT_TXT_Report.prototype = Object.create(TAF.Report.FR_SAFT_Report.prototype);
FR_SAFT_TXT_Report.IsCRGReport = true;
FR_SAFT_TXT_Report.ReportId = 'FR_SAFT_TXT';
