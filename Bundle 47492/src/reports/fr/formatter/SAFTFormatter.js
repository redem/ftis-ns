/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.FR = TAF.FR || {};
TAF.FR.Formatter = TAF.FR.Formatter || {};

// START - SAFT Formatter
TAF.FR.Formatter = function _SAFTFormatter() {
	TAF.Formatter.ReportFormatter.call(this);
	
	this.fields = {};
	this.TEMPLATE = {};
	
	this.charMap = {
		quote: {regex: /"/g, replaceChar: ''},
		tab: {regex: /\t/g, replaceChar: ' '},
		feed: {regex: /\r\n|\n|\r/g, replaceChar: ' '},
		enq: {regex: //g, replaceChar: ''}
	};

	this.dateFormat = 'yyyyMMdd';
	
	this.columnDelimiter = '|';
	
	this.lineTemplate = '';
};

TAF.FR.Formatter.prototype = Object.create(TAF.Formatter.ReportFormatter.prototype);

TAF.FR.Formatter.prototype.formatHeader = function _formatHeader(templateColumns) {
	var headers = [];

	for (var i = 0; i < templateColumns.length; i++) {
		headers.push(templateColumns[i].columnName);
	}
	
	return headers.join(this.columnDelimiter);
};

TAF.FR.Formatter.prototype.formatLine = function _formatLine(templateColumns) {
	var line = [];

	for (var i = 0; i < templateColumns.length; i++) {
		line.push(templateColumns[i].valueKey);
	}
	
	return line.join(this.columnDelimiter);
};
// END - SAFT Formatter

// START - GL Formatter
TAF.FR.Formatter.SAFTFormatter = function _SAFTFormatter() {
	TAF.FR.Formatter.call(this);
	this.FILE_EXTENSION = '.txt';
	this.FILE_NAME_CONNECTOR = 'FEC';
	
	this.fields = {
		journalCode: {type: TAF.Formatter.FieldTypes.TEXT},
        journalLib: {type: TAF.Formatter.FieldTypes.TEXT},
        ecritureNum: {type: TAF.Formatter.FieldTypes.TEXT},
        ecritureDate: {type: TAF.Formatter.FieldTypes.DATE},
        compteNum: {type: TAF.Formatter.FieldTypes.TEXT},
        compteLib: {type: TAF.Formatter.FieldTypes.TEXT},
        compAuxNum: {type: TAF.Formatter.FieldTypes.TEXT},
        compAuxLib: {type: TAF.Formatter.FieldTypes.TEXT},
        pieceRef: {type: TAF.Formatter.FieldTypes.TEXT},
        pieceDate: {type: TAF.Formatter.FieldTypes.DATE},
        ecritureLib: {type: TAF.Formatter.FieldTypes.TEXT},
        debit: {
        	type: TAF.Formatter.FieldTypes.CURRENCY,
            thousandSign: '',
			decimalSign: ','
        },
        credit: {
            type: TAF.Formatter.FieldTypes.CURRENCY,
            thousandSign: '',
			decimalSign: ','
        },
        ecritureLet: {type: TAF.Formatter.FieldTypes.TEXT},
        dateLet: {type: TAF.Formatter.FieldTypes.DATE},
        validDate: {type: TAF.Formatter.FieldTypes.DATE},
        montantDevise: {
        	type: TAF.Formatter.FieldTypes.CURRENCY,
            thousandSign: '',
			decimalSign: ','
        },
        iDevise: {type: TAF.Formatter.FieldTypes.TEXT} };
	
	this.TEMPLATE.COLUMNS = [];
	this.TEMPLATE.COLUMNS.push({columnName: 'JournalCode', valueKey: '{journalCode}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'JournalLib', valueKey: '{journalLib}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'EcritureNum', valueKey: '{ecritureNum}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'EcritureDate', valueKey: '{ecritureDate}', constantType: SFC.Utilities.Constants.DATE, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'CompteNum', valueKey: '{compteNum}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'CompteLib', valueKey: '{compteLib}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'CompAuxNum', valueKey: '{compAuxNum}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'CompAuxLib', valueKey: '{compAuxLib}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'PieceRef', valueKey: '{pieceRef}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'PieceDate', valueKey: '{pieceDate}', constantType: SFC.Utilities.Constants.DATE, constantDescriptor: SFC.Utilities.Constants.DATEFORMAT});
	this.TEMPLATE.COLUMNS.push({columnName: 'EcritureLib', valueKey: '{ecritureLib}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor:  null});
	this.TEMPLATE.COLUMNS.push({columnName: 'Debit', valueKey: '{debit}', constantType: SFC.Utilities.Constants.NUMERIC, constantDescriptor: SFC.Utilities.Constants.NUMERICACCURACY});
	this.TEMPLATE.COLUMNS.push({columnName: 'Credit', valueKey: '{credit}', constantType: SFC.Utilities.Constants.NUMERIC, constantDescriptor: SFC.Utilities.Constants.NUMERICACCURACY});
	this.TEMPLATE.COLUMNS.push({columnName: 'EcritureLet', valueKey: '{ecritureLet}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	this.TEMPLATE.COLUMNS.push({columnName: 'DateLet', valueKey: '{dateLet}', constantType: SFC.Utilities.Constants.DATE, constantDescriptor: SFC.Utilities.Constants.DATEFORMAT});
	this.TEMPLATE.COLUMNS.push({columnName: 'ValidDate', valueKey: '{validDate}', constantType: SFC.Utilities.Constants.DATE, constantDescriptor: SFC.Utilities.Constants.DATEFORMAT});
	this.TEMPLATE.COLUMNS.push({columnName: 'Montantdevise', valueKey: '{montantDevise}', constantType: SFC.Utilities.Constants.NUMERIC, constantDescriptor: SFC.Utilities.Constants.NUMERICACCURACY});
	this.TEMPLATE.COLUMNS.push({columnName: 'Idevise', valueKey: '{iDevise}', constantType: SFC.Utilities.Constants.ALPHANUMERIC, constantDescriptor: null});
	
	
	if (!this.lineTemplate) {
		this.lineTemplate = this.formatLine(this.TEMPLATE.COLUMNS);
	}
};

TAF.FR.Formatter.SAFTFormatter.prototype = Object.create(TAF.FR.Formatter.prototype);

TAF.FR.Formatter.SAFTFormatter.prototype.formatSAFTHeader = function _formatHeader() {
	return this.formatHeader(this.TEMPLATE.COLUMNS);
};

TAF.FR.Formatter.SAFTFormatter.prototype.formatSAFTLine = function _formatSAFTLine(data) {
	return this.formatElement(data, this.lineTemplate);
};

TAF.FR.Formatter.SAFTFormatter.prototype.formatFileName = function _formatFileName(siren, date) {
    var formattedDate = this.formatDate(date, this.dateFormat);
    return siren + this.FILE_NAME_CONNECTOR + formattedDate + this.FILE_EXTENSION;
};

TAF.FR.Formatter.SAFTFormatter.prototype.formatCurrency = function _formatCurrency(data, thousandSign, decimalSign) {
    if (!data || data == 0) {
        return '';
    }
    else {
        return SFC.Utilities.FormatCurrency(Math.abs(data), thousandSign, decimalSign);
    }
};
// END - GL Formatter