/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.DAO = TAF.ES.DAO || {};

TAF.ES.DAO.IssuedInvoiceDAO = function _IssuedInvoiceDAO(params) {
	TAF.DAO.SearchDAO.call(this, params);
    this.recordType = 'transaction';
    this.savedSearchId = 'customsearch_es_sii_issued_invoices';
    this.hasForeignCurrencyManagement = this.context.getFeature('FOREIGNCURRENCYMANAGEMENT');
};
TAF.ES.DAO.IssuedInvoiceDAO.prototype = Object.create(TAF.DAO.SearchDAO.prototype);

TAF.ES.DAO.IssuedInvoiceDAO.prototype.createSearchColumns = function _createSearchFilters(params) {
    if (params && params.bookId && this.hasForeignCurrencyManagement) {
        this.columns.push(new nlobjSearchColumn('formulacurrency').setFormula('({taxamount} * {accountingtransaction.exchangerate}) / {exchangerate}'));
    } else {
        this.columns.push(new nlobjSearchColumn('taxamount'));
    }

    this.columns.push(new nlobjSearchColumn('netamount', this.multiBookJoinColumn));
};

TAF.ES.DAO.IssuedInvoiceDAO.prototype.createSearchFilters = function _createSearchFilters(params) {
    if (!params) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'params is required.');
    }

    if (params.startDate) {
        this.filters.push(new nlobjSearchFilter('trandate', null, 'onorafter', params.startDate));
    }

    if (params.endDate) {
        this.filters.push(new nlobjSearchFilter('trandate', null, 'onorbefore', params.endDate));
    }

    if (params.subsidiary) {
        this.filters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.subsidiary));
    }

    if (params.nexusId) {
        this.filters.push(new nlobjSearchFilter('nexus', null, 'is', params.nexusId));
    }

    if (params.bookId && this.isMultiBook) {
        this.filters.push(new nlobjSearchFilter('accountingbook', this.multiBookJoinColumn, 'is', params.bookId));
    }

    if (params.internalid) {
        this.filters.push(new nlobjSearchFilter('internalid', null, 'is', params.internalid));
    }

    if (params.registrationStatus) {
        this.filters.push(new nlobjSearchFilter('custbody_sii_registration_status', null, 'anyof', params.registrationStatus));
    } else {
        this.filters.push(new nlobjSearchFilter('custbody_sii_registration_status', null, 'noneof', ['1', '2']));
    }
};

TAF.ES.DAO.IssuedInvoiceDAO.prototype.rowToObject = function _rowToObject(row) {
	if(!row) {
        throw  nlapiCreateError('MISSING_PARAMETER', 'row is required');
    }
    try {
        return {
            internalId: row.getId(),
            tranId: row.getValue('tranid'),
            tranDate: row.getValue('trandate'),
            taxCode: row.getValue('taxitem'),
            netAmount: row.getValue(this.columns[1]),
            taxAmount: row.getValue(this.columns[0]),
            billingCountryCode: row.getValue('billcountrycode'),
            memo: row.getValue('memomain'),
            specialSchemeCode: row.getValue('custbody_sii_spcl_scheme_code_sales'),
            propertyLocation: row.getValue('custbody_sii_property_location'),
            landRegisterRef: row.getValue('custbody_sii_land_register'),
            isIssuedByThirdParty: row.getValue('custbody_sii_is_third_party'),
            exemptionDetails: row.getValue('custbody_sii_exempt_details'),
            isCustomerPerson: row.getValue('isperson', 'customer'),
            customerCompanyName: row.getValue('companyname', 'customer'),
            customerFirstName: row.getValue('firstname', 'customer'),
            customerLastName: row.getValue('lastname', 'customer'),
            customerVatRegNo: row.getValue('vatregnumber', 'customer'),
            defaultBillingCountryCode: row.getValue('billcountrycode', 'customer'),
            customerIdType: row.getValue('custentity_sii_id_type', 'customer'),
            customerId: row.getValue('custentity_sii_id', 'customer')
        };
    } catch (ex) {
        nlapiLogExecution('ERROR', 'Error in TAF.ES.DAO.IssuedInvoiceDAO.rowToObject', ex.toString());
        throw nlapiCreateError('SEARCH_ERROR', 'Error in processing search results.');
    }
};