/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.DAO = TAF.ES.DAO || {};

TAF.ES.DAO.IssuedInvoiceAmendingTxnDAO = function _IssuedInvoiceAmendingTxnDAO(params) {
	TAF.DAO.SearchDAO.call(this, params);
    this.recordType = 'transaction';
    this.savedSearchId = 'customsearch_es_sii_rectify_invoices';
    this.hasForeignCurrencyManagement = this.context.getFeature('FOREIGNCURRENCYMANAGEMENT');
};
TAF.ES.DAO.IssuedInvoiceAmendingTxnDAO.prototype = Object.create(TAF.DAO.SearchDAO.prototype);

TAF.ES.DAO.IssuedInvoiceAmendingTxnDAO.prototype.createSearchColumns = function _createSearchFilters(params) {
    if (params && params.bookId && this.hasForeignCurrencyManagement) {
        this.columns.push(new nlobjSearchColumn('formulacurrency').setFormula('({taxamount} * {accountingtransaction.exchangerate}) / {exchangerate}'));
    } else {
        this.columns.push(new nlobjSearchColumn('taxamount'));
    }

    this.columns.push(new nlobjSearchColumn('netamount', this.multiBookJoinColumn));
};

TAF.ES.DAO.IssuedInvoiceAmendingTxnDAO.prototype.createSearchFilters = function _createSearchFilters(params) {
    if (!params) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'params is required.');
    }

    if (params.startDate) {
        this.filters.push(new nlobjSearchFilter('trandate', null, 'onorafter', params.startDate));
    }

    if (params.endDate) {
        this.filters.push(new nlobjSearchFilter('trandate', null, 'onorbefore', params.endDate));
    }

    if (params.subsidiary) {
        this.filters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.subsidiary));
    }

    if (params.nexusId) {
        this.filters.push(new nlobjSearchFilter('nexus', null, 'is', params.nexusId));
    }

    if (params.bookId && this.isMultiBook) {
        this.filters.push(new nlobjSearchFilter('accountingbook', this.multiBookJoinColumn, 'is', params.bookId));
    }

    this.filters.push(new nlobjSearchFilter('custbody_sii_registration_status', null, 'noneof', TAF.SII.CONSTANTS.TRANSACTION.STATUS.REGISTERED));
    
};

TAF.ES.DAO.IssuedInvoiceAmendingTxnDAO.prototype.rowToObject = function _rowToObject(row) {
	if(!row) {
        throw  nlapiCreateError('MISSING_PARAMETER', 'row is required');
    }
    try {
        return {
            internalId: row.getId(),
            tranId: row.getValue('tranid'),
            tranDate: row.getValue('trandate'),
            taxCode: row.getValue('taxitem'),
            netAmount: row.getValue(this.columns[1]),
            taxAmount: row.getValue(this.columns[0]),
            memo: row.getValue('memomain'),
            origTranId: row.getValue('createdfrom') || row.getValue('custbody_sii_orig_invoice'),
            invoiceType: row.getValue('custbody_sii_correction_type')
        };
    } catch (ex) {
        nlapiLogExecution('ERROR', 'Error in TAF.ES.DAO.IssuedInvoiceAmendingTxnDAO.rowToObject', ex.toString());
        throw nlapiCreateError('SEARCH_ERROR', 'Error in processing search results.');
    }
};
