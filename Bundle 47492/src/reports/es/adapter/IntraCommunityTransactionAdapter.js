/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Adapter = TAF.ES.Adapter || {};

TAF.ES.Adapter.IntraCommunityTransactionAdapter = function _IntraCommunityTransactionAdapter(listValueMap) {
    TAF.ES.Adapter.ESTransactionAdapter.apply(this, arguments);
    this.Name = 'IntraCommunityTransactionAdapter';
};
TAF.ES.Adapter.IntraCommunityTransactionAdapter.prototype = Object.create(TAF.ES.Adapter.ESTransactionAdapter.prototype);

TAF.ES.Adapter.IntraCommunityTransactionAdapter.prototype.createTxnObject = function _createTxnObject(rawLine, companyInfo) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine is required.');
    }
    if (!companyInfo) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'companyInfo is required.');
    }

    var isSales = this.isSalesTxn(rawLine);
    var date = this.getTxnPeriod(rawLine.tranDate);
    var filer = this.getFiler(rawLine, companyInfo);
    var sender = this.getSender(rawLine, companyInfo);
    var txnObject = {
        vatNo: companyInfo.vatNo || '',
        address: this.getAddress(companyInfo),
        tranId: rawLine.tranId,
        month: date.month,
        year: date.year,
        tranDate: rawLine.tranDate,
        description: rawLine.memo || '',
        submissionType: this.getListValueCode(rawLine.intraCommunityTxnType),
        intraCommunityCode: isSales ? 'D' : 'R',
        countryCode: companyInfo.countryCode,
        filerName: filer.companyName,
        filerVatNo: filer.vatNo,
        filerCountryCode: filer.billingCountryCode || filer.countryCode || '',
        filerIdType: filer.idType,
        filerId: filer.id || '',
        senderName: sender.companyName,
        senderVatNo: sender.vatNo,
        senderCountryCode: sender.billingCountryCode || sender.countryCode || '',
        senderIdType: sender.idType,
        senderId: sender.id || ''
    };

    return txnObject;
};

TAF.ES.Adapter.IntraCommunityTransactionAdapter.prototype.isSalesTxn = function _isSalesTxn(rawLine) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine is required.');
    }
    if (!rawLine.type) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine.type is required.');
    }

    var type = rawLine.type.toLowerCase();
    return type === 'custinvc' || type === 'cashsale';
};

TAF.ES.Adapter.IntraCommunityTransactionAdapter.prototype.getFiler = function _getFiler(rawLine, companyInfo) {
    var filer;
    var isSalesTxn = this.isSalesTxn(rawLine);
    
    if (isSalesTxn) {
        // Filer is company/subsidiary
        filer = {
            companyName: companyInfo.companyName,
            vatNo: companyInfo.vatNo
        };
    } else {
        // Filer is vendor
        filer = this.getEntity(rawLine, 'vendor');
    }

    return filer;
};

TAF.ES.Adapter.IntraCommunityTransactionAdapter.prototype.getSender = function _getSender(rawLine, companyInfo) {
    var sender;
    var isSalesTxn = this.isSalesTxn(rawLine);
    
    if (isSalesTxn) {
        // Sender is customer
        sender = this.getEntity(rawLine, 'customer');
    } else {
        // Sender is company/subsidiary
        sender = {
            companyName: companyInfo.companyName,
            vatNo: companyInfo.vatNo
        };
    }

    return sender;
};

TAF.ES.Adapter.IntraCommunityTransactionAdapter.prototype.getAddress = function _getAddress(companyInfo) {
    if (!companyInfo) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'companyInfo is required.');
    }

    var address = '';

    if (companyInfo.address1) {
        address += companyInfo.address1 + ' ';
    }
    if (companyInfo.address2) {
        address += companyInfo.address2 + ' ';
    }
    if (companyInfo.city) {
        address += companyInfo.city + ' ';
    }
    if (companyInfo.state) {
        address += companyInfo.state + ' ';
    }
    if (companyInfo.zip) {
        address += companyInfo.zip + ' ';
    }
    if (companyInfo.country) {
        address += companyInfo.country;
    }

    return address;
};

TAF.ES.Adapter.IntraCommunityTransactionAdapter.prototype.getEntity = function _getEntity(rawLine, entityType) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine is required.');
    }

    if (!entityType) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'entityType is required.');
    }

    var entity = {};
    var isPerson = rawLine['is' + entityType.charAt(0).toUpperCase() + entityType.slice(1) + 'Person'] === 'T';

    if (isPerson) {
        entity.companyName = rawLine[entityType + 'FirstName'] + ' ' + rawLine[entityType + 'LastName'];
    } else {
        entity.companyName = rawLine[entityType + 'CompanyName'];
    }

    var idType = this.getListValueCode(rawLine[entityType + 'IdType']);
    if (idType) {
        entity.idType = idType;

        // NOTE:
        // If idType is '02', country code is not required. Therefore, skip.
        // If idType is '07', country code must be 'ES'.
        if (entity.idType !== TAF.SII.CONSTANTS.ENTITY.ID_TYPE_NIF_VAT) {
            entity.billingCountryCode = rawLine[entityType + 'DefaultBillingCountryCode'];
            entity.id = rawLine[entityType + 'Id'];
        } else {
            entity.id = this.getVATNo(rawLine[entityType + 'Id']);
        }
    } else {
        entity.vatNo = this.getVATNo(rawLine[entityType + 'VatRegNo'], true);
    }
    

    return entity;
};
