/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Adapter = TAF.ES.Adapter || {};

TAF.ES.Adapter.IssuedInvoiceAmendingTxnAdapter = function _IssuedInvoiceAmendingTxnAdapter(listValueMap) {
    TAF.ES.Adapter.ESTransactionAdapter.apply(this, arguments);
    this.Name = 'IssuedInvoiceAmendingTxnAdapter';
};
TAF.ES.Adapter.IssuedInvoiceAmendingTxnAdapter.prototype = Object.create(TAF.ES.Adapter.ESTransactionAdapter.prototype);

TAF.ES.Adapter.IssuedInvoiceAmendingTxnAdapter.prototype.createTxnObject = function _createTxnObject(rawLine, origInvoiceObject, companyInfo) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine is required.');
    }
    if (!origInvoiceObject) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'origInvoiceObject is required.');
    }
    if (!companyInfo) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'companyInfo is required.');
    }
    
    var date = this.getTxnPeriod(rawLine.tranDate);
    var isPerson = origInvoiceObject.isCustomerPerson === 'T';
    var customerName = isPerson ? origInvoiceObject.customerFirstName + ' ' + origInvoiceObject.customerLastName : origInvoiceObject.customerCompanyName;
    var customerIdType = this.getListValueCode(origInvoiceObject.customerIdType);
    var customerVatNo = this.getVATNo(origInvoiceObject.customerVatRegNo, customerIdType === TAF.SII.CONSTANTS.ENTITY.ID_TYPE_NIF_VAT);
    var txnObject = {
        vatNo: this.getVATNo(companyInfo.vatNo),
        tranId: rawLine.tranId,
        month: date.month,
        year: date.year,
        tranDate: rawLine.tranDate,
        description: rawLine.memo || '',
        invoiceType: this.getListValueCode(rawLine.invoiceType),
        specialSchemeCode: this.getListValueCode(origInvoiceObject.specialSchemeCode),
        propertyLocation: this.getListValueCode(origInvoiceObject.propertyLocation),
        landRegistrationNo: origInvoiceObject.landRegisterRef || '',
        isIssuedByThirdParty: origInvoiceObject.isIssuedByThirdParty === 'T'
                ? TAF.SII.CONSTANTS.TRANSACTION.ISSUED_BY_THIRD_PARTY_YES
                : TAF.SII.CONSTANTS.TRANSACTION.ISSUED_BY_THIRD_PARTY_NO,
        exemptionDetails: this.getListValueCode(origInvoiceObject.exemptionDetails),
        exemptionClassification: '',
        customerName: customerName,
        customerVatNo: customerVatNo,
        customerCountryCode: origInvoiceObject.billingCountryCode || origInvoiceObject.defaultBillingCountryCode || '',
        customerIdType: customerIdType,
        customerId: origInvoiceObject.customerId || '',
        total: 0,
        servicesTaxableAmount: 0,
        taxableAmount: 0,
        exemptAmount: 0,
        servicesExemptAmount: 0,
        notSubjectAmount: 0,
        servicesNotSubjectAmount: 0,
        lines: [],
        serviceLines: [],
        isTransactionType: this.isTransactionType(customerIdType, customerVatNo),
        correctedInvoiceType: rawLine.correctedInvoiceType,
        origTranId: origInvoiceObject.tranId,
        origTranDate: origInvoiceObject.tranDate
    };

    return txnObject;
};

TAF.ES.Adapter.IssuedInvoiceAmendingTxnAdapter.prototype.createTxnLineObject = function _createTxnLineObject(rawLine) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawTxnLine is required.');
    }

    var taxCode = SFC.TaxCodes.Load(rawLine.taxCode);
    var netAmount = rawLine.netAmount;
    var taxAmount = netAmount > 0 ? Math.abs(rawLine.taxAmount) : -Math.abs(rawLine.taxAmount);
    var txnLineObject = {
        netAmount: parseFloat(netAmount) || 0,
        taxAmount: parseFloat(taxAmount) || 0,
        taxRate: taxCode ? parseFloat(taxCode.GetRate()) || 0 : 0,
        isReverseCharge: taxCode ? taxCode.IsReverseCharge() : false,
        isService: taxCode ? taxCode.IsService() : false
    };

    return txnLineObject;
};

TAF.ES.Adapter.IssuedInvoiceAmendingTxnAdapter.prototype.isTransactionType = function _isTransactionType(customerIdType, customerVatNo) {
    var result = false;
    var formattedCustomerVatNo = (customerVatNo || '').toLowerCase();

    if (customerIdType || (formattedCustomerVatNo && formattedCustomerVatNo.indexOf('n') === 0)) {
        result = true;
    }

    return result;
};
