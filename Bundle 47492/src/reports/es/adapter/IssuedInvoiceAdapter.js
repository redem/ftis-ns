/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Adapter = TAF.ES.Adapter || {};

TAF.ES.Adapter.IssuedInvoiceAdapter = function _IssuedInvoiceAdapter(listValueMap) {
    TAF.ES.Adapter.ESTransactionAdapter.apply(this, arguments);
    this.Name = 'IssuedInvoiceAdapter';
};
TAF.ES.Adapter.IssuedInvoiceAdapter.prototype = Object.create(TAF.ES.Adapter.ESTransactionAdapter.prototype);

TAF.ES.Adapter.IssuedInvoiceAdapter.prototype.createTxnObject = function _createTxnObject(rawLine, companyInfo) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine is required.');
    }
    if (!companyInfo) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'companyInfo is required.');
    }

    var date = this.getTxnPeriod(rawLine.tranDate);
    var isPerson = rawLine.isCustomerPerson === 'T';
    var customerName = isPerson ? rawLine.customerFirstName + ' ' + rawLine.customerLastName : rawLine.customerCompanyName;
    var customerIdType = this.getListValueCode(rawLine.customerIdType);
    var customerVatNo = this.getVATNo(rawLine.customerVatRegNo, customerIdType === TAF.SII.CONSTANTS.ENTITY.ID_TYPE_NIF_VAT);
    var txnObject = {
        vatNo: this.getVATNo(companyInfo.vatNo),
        tranId: rawLine.tranId,
        month: date.month,
        year: date.year,
        tranDate: rawLine.tranDate,
        description: rawLine.memo || '',
        invoiceType: TAF.SII.CONSTANTS.TRANSACTION.INVOICE,
        specialSchemeCode: this.getListValueCode(rawLine.specialSchemeCode),
        propertyLocation: this.getListValueCode(rawLine.propertyLocation),
        landRegistrationNo: rawLine.landRegisterRef || '',
        isIssuedByThirdParty: rawLine.isIssuedByThirdParty === 'T'
                ? TAF.SII.CONSTANTS.TRANSACTION.ISSUED_BY_THIRD_PARTY_YES
                : TAF.SII.CONSTANTS.TRANSACTION.ISSUED_BY_THIRD_PARTY_NO,
        exemptionDetails: this.getListValueCode(rawLine.exemptionDetails),
        exemptionClassification: '',
        customerName: customerName,
        customerVatNo: customerVatNo,
        customerCountryCode: rawLine.billingCountryCode || rawLine.defaultBillingCountryCode || '',
        customerIdType: customerIdType,
        customerId: rawLine.customerId || '',
        total: 0,
        servicesTaxableAmount: 0,
        taxableAmount: 0,
        exemptAmount: 0,
        servicesExemptAmount: 0,
        notSubjectAmount: 0,
        servicesNotSubjectAmount: 0,
        lines: [],
        serviceLines: [],
        isTransactionType: this.isTransactionType(customerIdType, customerVatNo)
    };

    return txnObject;
};

TAF.ES.Adapter.IssuedInvoiceAdapter.prototype.createTxnLineObject = function _createTxnLineObject(rawLine) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawTxnLine is required.');
    }

    var taxCode = SFC.TaxCodes.Load(rawLine.taxCode);
    var txnLineObject = {
        netAmount: parseFloat(rawLine.netAmount) || 0,
        taxAmount: parseFloat(rawLine.taxAmount) || 0,
        taxRate: taxCode ? parseFloat(taxCode.GetRate()) || 0 : 0,
        isReverseCharge: taxCode ? taxCode.IsReverseCharge() : false,
        isService: taxCode ? taxCode.IsService() : false
    };

    return txnLineObject;
};

TAF.ES.Adapter.IssuedInvoiceAdapter.prototype.isTransactionType = function _isTransactionType(customerIdType, customerVatNo) {
    var result = false;
    var formattedCustomerVatNo = (customerVatNo || '').toLowerCase();

    if (customerIdType || (formattedCustomerVatNo && formattedCustomerVatNo.indexOf('n') === 0)) {
        result = true;
    }

    return result;
};
