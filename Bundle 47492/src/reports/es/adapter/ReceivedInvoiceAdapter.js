/**
 * Copyright � 2017, Oracle and/or its affiliates. All rights reserved.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Adapter = TAF.ES.Adapter || {};

TAF.ES.Adapter.ReceivedInvoiceAdapter = function _ReceivedInvoiceAdapter(listValueMap) {
    TAF.ES.Adapter.ESTransactionAdapter.apply(this, arguments);
    this.Name = 'ReceivedInvoiceAdapter';
};
TAF.ES.Adapter.ReceivedInvoiceAdapter.prototype = Object.create(TAF.ES.Adapter.ESTransactionAdapter.prototype);

TAF.ES.Adapter.ReceivedInvoiceAdapter.prototype.createTxnObject = function _createTxnObject(rawLine, companyInfo) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine is required.');
    }
    if (!companyInfo) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'companyInfo is required.');
    }

    var date = this.getTxnPeriod(rawLine.tranDate);
    var isPerson = rawLine.isVendorPerson === 'T';
    var vendorName = isPerson ? rawLine.vendorFirstName + ' ' + rawLine.vendorLastName : rawLine.vendorCompanyName;
    var vendorIdType = this.getListValueCode(rawLine.vendorIdType);
    var txnObject = {
        vatNo: this.getVATNo(companyInfo.vatNo),
        tranId: rawLine.tranId,
        month: date.month,
        year: date.year,
        tranDate: rawLine.tranDate,
        description: rawLine.memo || '',
        invoiceType: this.getListValueCode(rawLine.invoiceType) || TAF.SII.CONSTANTS.TRANSACTION.INVOICE, // default to F1 if null/empty
        specialSchemeCode: this.getListValueCode(rawLine.specialSchemeCode),
        vendorName: vendorName,
        vendorVatNo: this.getVATNo(rawLine.vendorVatRegNo, vendorIdType === TAF.SII.CONSTANTS.ENTITY.ID_TYPE_NIF_VAT),
        vendorCountryCode: rawLine.billingCountryCode || rawLine.defaultBillingCountryCode || '',
        vendorIdType: vendorIdType,
        vendorId: rawLine.vendorId || '',
        total: 0,
        taxableAmount: 0,
        totalTax: 0,
        lines: []
    };

    return txnObject;
};

TAF.ES.Adapter.ReceivedInvoiceAdapter.prototype.createTxnLineObject = function _createTxnLineObject(rawLine) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawTxnLine is required.');
    }

    var taxCode = SFC.TaxCodes.Load(rawLine.taxCode);
    var txnLineObject = {
        netAmount: parseFloat(rawLine.netAmount) || 0,
        taxAmount: -parseFloat(rawLine.taxAmount) || 0,
        taxRate: taxCode ? parseFloat(taxCode.GetRate()) || 0 : 0,
        isReverseCharge: taxCode ? taxCode.IsReverseCharge() : false,
        isService: taxCode ? taxCode.IsService() : false
    };

    return txnLineObject;
};
