/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Adapter = TAF.ES.Adapter || {};

TAF.ES.Adapter.ESTransactionAdapter = function _ESTransactionAdapter(listValueMap) {
    if (!listValueMap) {
        throw nlapiCreateError('INVALID_PARAMETER', 'listValueMap data is invalid');
    }
    this.listValueMap = listValueMap;
};

TAF.ES.Adapter.ESTransactionAdapter.prototype.createTxnLineObject = function _createTxnLineObject(rawLine) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawTxnLine is required.');
    }

    var taxCode = SFC.TaxCodes.Load(rawLine.taxCode);
    var netAmount = parseFloat(rawLine.netAmount) || 0;
    var taxAmount = parseFloat(rawLine.taxAmount) || 0;
    var taxRate = taxCode ? parseFloat(taxCode.GetRate()) || 0 : 0;

    var txnLineObject = {
        netAmount: netAmount,
        taxAmount: taxAmount,
        taxRate: taxRate
    };

    return txnLineObject;
};

TAF.ES.Adapter.ESTransactionAdapter.prototype.getListValueCode = function _getListValueCode(id) {
    return this.listValueMap[id] ? this.listValueMap[id].code : '';
};

TAF.ES.Adapter.ESTransactionAdapter.prototype.getTxnPeriod = function _getTxnPeriod(dateString) {
    if (!dateString) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'dateString is required.');
    }

    try {
        var date = nlapiStringToDate(dateString);
        return {
            month: date.getMonth() + 1,
            year: date.getFullYear()
        };
    } catch (ex) {
        logException(ex, this.Name + '.getTxnPeriod');
        throw ex;
    }
};

TAF.ES.Adapter.ESTransactionAdapter.prototype.getVATNo = function _getVATNo(vatNo, isRetainPrefix) {
    if (!vatNo) {
        return '';
    }

    vatNo = vatNo.replace(/[^A-Za-z0-9]/g, '');
    return isRetainPrefix ? vatNo : vatNo.replace(/^[A-Z]{2}/i, '');
};
