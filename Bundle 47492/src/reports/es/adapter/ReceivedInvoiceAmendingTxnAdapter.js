/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Adapter = TAF.ES.Adapter || {};

TAF.ES.Adapter.ReceivedInvoiceAmendingTxnAdapter = function _ReceivedInvoiceAmendingTxnAdapter(listValueMap) {
    TAF.ES.Adapter.ESTransactionAdapter.apply(this, arguments);
    this.Name = 'ReceivedInvoiceAmendingTxnAdapter';
};
TAF.ES.Adapter.ReceivedInvoiceAmendingTxnAdapter.prototype = Object.create(TAF.ES.Adapter.ESTransactionAdapter.prototype);

TAF.ES.Adapter.ReceivedInvoiceAmendingTxnAdapter.prototype.createTxnObject = function _createTxnObject(rawLine, origBillObject, companyInfo) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawLine is required.');
    }
    if (!origBillObject) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'origBillObject is required.');
    }
    if (!companyInfo) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'companyInfo is required.');
    }

    var date = this.getTxnPeriod(rawLine.tranDate);
    var vendorData = this.getVendorData(origBillObject);
    var isJournal = rawLine.recordType === 'journalentry';
    var txnObject = {
        vatNo: this.getVATNo(companyInfo.vatNo),
        correctedInvoiceType: TAF.SII.CONSTANTS.TRANSACTION.CORRECTED_INVOICE_TYPE_DIFFERENCE,
        tranId: isJournal ? rawLine.referenceNo : rawLine.tranId,
        month: date.month,
        year: date.year,
        tranDate: rawLine.tranDate,
        description: rawLine.memo || '',
        invoiceType: this.getListValueCode(rawLine.invoiceType),
        specialSchemeCode: this.getListValueCode(origBillObject.specialSchemeCode),
        vendorName: vendorData.name,
        vendorVatNo: vendorData.vatNo,
        vendorCountryCode: vendorData.countryCode,
        vendorIdType: vendorData.idType,
        vendorId: vendorData.id,
        total: 0,
        taxableAmount: 0,
        totalTax: 0,
        lines: [],
        origTranId: origBillObject.tranId,
        origTranDate: origBillObject.tranDate
    };

    return txnObject;
};

TAF.ES.Adapter.ReceivedInvoiceAmendingTxnAdapter.prototype.getVendorData = function _getVendorData(origBillObject) {
    var vendor = {};

    vendor.id = origBillObject.vendorId || '';
    vendor.isPerson = origBillObject.isVendorPerson === 'T';
    vendor.name = vendor.isPerson ? origBillObject.vendorFirstName + ' ' + origBillObject.vendorLastName : origBillObject.vendorCompanyName;
    vendor.idType = this.getListValueCode(origBillObject.vendorIdType);
    vendor.vatNo = this.getVATNo(origBillObject.vendorVatRegNo, vendor.idType === TAF.SII.CONSTANTS.ENTITY.ID_TYPE_NIF_VAT);
    vendor.countryCode = origBillObject.billingCountryCode || origBillObject.defaultBillingCountryCode || '';

    return vendor;
};

TAF.ES.Adapter.ReceivedInvoiceAmendingTxnAdapter.prototype.createTxnLineObject = function _createTxnLineObject(rawLine) {
    if (!rawLine) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'rawTxnLine is required.');
    }

    var taxCode = SFC.TaxCodes.Load(rawLine.taxCode);
    var netAmount = rawLine.netAmount;
    var taxAmount = netAmount < 0 ? Math.abs(rawLine.taxAmount) : -Math.abs(rawLine.taxAmount);
    var txnLineObject = {
        netAmount: -parseFloat(netAmount) || 0,
        taxAmount: parseFloat(taxAmount) || 0,
        taxRate: taxCode ? parseFloat(taxCode.GetRate()) || 0 : 0,
        isReverseCharge: taxCode ? taxCode.IsReverseCharge() : false,
        isService: taxCode ? taxCode.IsService() : false
    };

    return txnLineObject;
};
