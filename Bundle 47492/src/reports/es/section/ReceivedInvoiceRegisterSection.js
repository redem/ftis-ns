/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Section = TAF.ES.Section || {};

TAF.ES.Section.ReceivedInvoiceRegisterSection = function _ReceivedInvoiceRegisterSection(state, params, output, job) {
    TAF.ES.Section.SIIRegisterSection.apply(this, arguments);
    this.Name = 'ReceivedInvoices';
};
TAF.ES.Section.ReceivedInvoiceRegisterSection.prototype = Object.create(TAF.ES.Section.SIIRegisterSection.prototype);

TAF.ES.Section.ReceivedInvoiceRegisterSection.prototype.On_Init = function _On_Init() {
    var listValueMap = new TAF.ES.DAO.ListValuesDAO().search({}).getMap();
    this.dao = new TAF.ES.DAO.ReceivedInvoiceDAO();
    this.adapter = new TAF.ES.Adapter.ReceivedInvoiceAdapter(listValueMap);
    this.formatter = this.params.formatter;

    if (!this.state[this.Name]) {
        this.initState();
    } else if (this.state[this.Name].fileIndex) {
        this.state[this.Name].fileIndex++;
    }

    this.REAGYP_RATES = [12, 10.5];
    this.VALID_RATES = [0, 4, 10, 21];
    this.SURCHARGE_RATES = [5.2, 1.4, 0.5, 1.75];
};

TAF.ES.Section.ReceivedInvoiceRegisterSection.prototype.initState = function _initState(txnObject, searchIndex, txnCount, fileIndex) {
    this.state[this.Name] = {
        fileIndex: fileIndex || null,
        txnCount: txnCount || 0,
        currentTxnLineIndex: 0,
        currentSearchIndex: searchIndex || 0,
        currentTranId: txnObject ? txnObject.tranId : null,
        currentTransaction: txnObject || {},
        currentTxnLineKey: '',
        lineIndexMap: {}
    };
};

TAF.ES.Section.ReceivedInvoiceRegisterSection.prototype.processList = function _processList(lines) {
    if (!lines) {
        throw nlapiCreateError('MISSING_PARAMETER', 'TAF.ES.Section.ReceivedInvoiceRegisterSection.processList: lines is a required parameter');
    }

    var index;
    var length;
    var line;

    for (index = 0, length = lines.length; index < length; index++) {
        this.state.ReceivedInvoices.currentSearchIndex++;
        line = lines[index];
        if (this.state.ReceivedInvoices.currentTranId !== line.tranId) {
            if (this.isValidTransaction(this.state.ReceivedInvoices.currentTransaction)) {
                this.processTransaction('getRegistroLRfacturasRecibidas', this.state.ReceivedInvoices.currentTransaction);
            }
            if (this.isMaxTransactionReached()) {
                return;
            }
            this.state.ReceivedInvoices.currentTranId = line.tranId;
            this.initState(this.adapter.createTxnObject(line, this.state.CompanyInformation),
                this.state.ReceivedInvoices.currentSearchIndex, this.state.ReceivedInvoices.txnCount,
                this.state.ReceivedInvoices.fileIndex);
        }

        this.processLine(line);

        if (this.job.IsThresholdReached()) {
            return;
        }
    }

    // add the last txn to the file
    if (this.isValidTransaction(this.state.ReceivedInvoices.currentTransaction)) {
        this.processTransaction('getRegistroLRfacturasRecibidas', this.state.ReceivedInvoices.currentTransaction);
    }
};

TAF.ES.Section.ReceivedInvoiceRegisterSection.prototype.isValidTransaction = function _isValidTransaction(txnObject) {
    var result = false;

    if (txnObject && JSON.stringify(txnObject) !== '{}' && txnObject.lines && txnObject.lines.length > 0) {
        result = true;
    }

    return result;
};

TAF.ES.Section.ReceivedInvoiceRegisterSection.prototype.processLine = function _processLine(line) {
    if (!line) {
        throw nlapiCreateError('MISSING_PARAMETER', 'TAF.ES.Section.ReceivedInvoiceRegisterSection.processLine: line is a required parameter');
    }

    var txnLineObject = this.adapter.createTxnLineObject(line);
    var currentIndexMap;
    var currentTxnLineKey;
    var index;
    var txnLine;
    var taxObjectKeys;

    currentIndexMap = this.state[this.Name].lineIndexMap;
    this.state[this.Name].currentTxnLineKey = txnLineObject.taxRate;
    currentTxnLineKey = txnLineObject.taxRate;
    taxObjectKeys = this.getTaxObjectKeys(txnLineObject);

    // Do not add line items with unsupported tax rates
    if (taxObjectKeys.taxRateName && taxObjectKeys.taxAmountName) {
        this.state[this.Name].currentTransaction.total += txnLineObject.netAmount + txnLineObject.taxAmount;
        this.state[this.Name].currentTransaction.totalTax += txnLineObject.taxAmount;

        // Add the line to the appropriate list
        if (currentIndexMap[currentTxnLineKey] === undefined) {
            txnLine = { netAmount: 0 };
            txnLine[taxObjectKeys.taxRateName] = txnLineObject.taxRate;
            txnLine[taxObjectKeys.taxAmountName] = 0;
            currentIndexMap[currentTxnLineKey] = this.state[this.Name].currentTransaction.lines.push(txnLine) - 1;
        }

        // Update the amounts of the current transaction line
        index = currentIndexMap[currentTxnLineKey];
        this.state[this.Name].currentTransaction.lines[index].netAmount += txnLineObject.netAmount;
        this.state[this.Name].currentTransaction.lines[index][taxObjectKeys.taxAmountName] += txnLineObject.taxAmount;
    }
    this.processedLineCount++;
};

TAF.ES.Section.ReceivedInvoiceRegisterSection.prototype.getTaxObjectKeys = function _getTaxObjectKeys(txnLineObject) {
    var taxObjectKeys = {};

    // Determine type of tax item line
    // Other tax rates not on REAGYP_RATES, SURCHARGE_RATES and VALID_RATES are ignored
    if (this.REAGYP_RATES.indexOf(txnLineObject.taxRate) > -1 &&
        this.state[this.Name].currentTransaction.specialSchemeCode === TAF.SII.CONSTANTS.TRANSACTION.SPCL_SCHEME_CODE_REAGYP) {
        taxObjectKeys.taxRateName = 'REAGYPTaxRate';
        taxObjectKeys.taxAmountName = 'REAGYPTaxAmount';
    } else if (this.SURCHARGE_RATES.indexOf(txnLineObject.taxRate) > -1) {
        taxObjectKeys.taxRateName = 'surchargeTaxRate';
        taxObjectKeys.taxAmountName = 'surchargeTaxAmount';
    } else if (this.VALID_RATES.indexOf(txnLineObject.taxRate) > -1) {
        taxObjectKeys.taxRateName = 'taxRate';
        taxObjectKeys.taxAmountName = 'taxAmount';
    }

    return taxObjectKeys;
};
