/**
 * Copyright 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.ES = TAF.ES || {};
TAF.ES.Section = TAF.ES.Section || {};

TAF.ES.Section.IssuedInvoiceAmendingTxnSection = function _IssuedInvoiceAmendingTxnSection(state, params, output, job) {
    TAF.ES.Section.IssuedInvoiceRegisterSection.apply(this, arguments);
    this.Name = 'IssuedInvoiceAmendingTxn';
};
TAF.ES.Section.IssuedInvoiceAmendingTxnSection.prototype = Object.create(TAF.ES.Section.IssuedInvoiceRegisterSection.prototype);

TAF.ES.Section.IssuedInvoiceAmendingTxnSection.prototype.On_Init = function _On_Init() {
    var listValueMap = new TAF.ES.DAO.ListValuesDAO().search({}).getMap();
    this.dao = new TAF.ES.DAO.IssuedInvoiceAmendingTxnDAO();
    this.adapter = new TAF.ES.Adapter.IssuedInvoiceAmendingTxnAdapter(listValueMap);
    this.formatter = this.params.formatter;

    if (!this.state[this.Name]) {
        this.initState();
    } else if (this.state[this.Name].fileIndex) {
        this.state[this.Name].fileIndex++;
    }

    this.VALID_RATES = [0, 4, 10, 21];
    this.SURCHARGE_RATES = [5.2, 1.4, 0.5, 1.75];
};

TAF.ES.Section.IssuedInvoiceAmendingTxnSection.prototype.processList = function _processList(lines) {
    if (!lines) {
        throw nlapiCreateError('MISSING_PARAMETER', 'TAF.ES.Section.IssuedInvoiceAmendingTxnSection.processList: lines is a required parameter');
    }

    var index;
    var length;
    var line;

    for (index = 0, length = lines.length; index < length; index++) {
        this.state[this.Name].currentSearchIndex++;
        line = lines[index];
        if (this.state[this.Name].currentTranId !== line.tranId) {
            if (this.isValidTransaction(this.state[this.Name].currentTransaction)) {
                this.processTransaction('getRegistroLRfacturasEmitidas', this.state[this.Name].currentTransaction);
            }
            if (this.isMaxTransactionReached()) {
                return;
            }
            this.state[this.Name].currentTranId = line.tranId;
            line.correctedInvoiceType = TAF.SII.CONSTANTS.TRANSACTION.CORRECTED_INVOICE_TYPE_DIFFERENCE;

            var origInvoiceDAO = new TAF.ES.DAO.IssuedInvoiceDAO();
            var params = {internalid: line.origTranId};
            params.registrationStatus = ['@NONE@'];
            params.registrationStatus.push(TAF.SII.CONSTANTS.TRANSACTION.STATUS.REGISTERED);
            params.registrationStatus.push(TAF.SII.CONSTANTS.TRANSACTION.STATUS.REGISTERED_WITH_ERRORS);
            params.registrationStatus.push(TAF.SII.CONSTANTS.TRANSACTION.STATUS.REJECTED);
            
            origInvoiceDAO.search(params);
            var origTxnLines = origInvoiceDAO.getList();

            this.initState(this.adapter.createTxnObject(line, origTxnLines[0], this.state.CompanyInformation),
                this.state[this.Name].currentSearchIndex, this.state[this.Name].txnCount,
                this.state[this.Name].fileIndex);
        }

        this.processLine(line);

        if (this.job.IsThresholdReached()) {
            return;
        }
    }

    // add the last txn to the file
    if (this.isValidTransaction(this.state[this.Name].currentTransaction)) {
        this.processTransaction('getRegistroLRfacturasEmitidas', this.state[this.Name].currentTransaction);
    }
};

TAF.ES.Section.IssuedInvoiceAmendingTxnSection.prototype.isValidTransaction = function _isValidTransaction(txnObject) {
    var result = false;

    if (txnObject && JSON.stringify(txnObject) !== '{}') {
        var hasLines = (txnObject.lines && txnObject.lines.length > 0) || (txnObject.serviceLines && txnObject.serviceLines.length > 0);
        var hasExemption = txnObject.exemptAmount != 0;
        var isNotSubject = txnObject.servicesNotSubjectAmount != 0 || txnObject.notSubjectAmount != 0;
        if (hasLines || hasExemption || isNotSubject) {
            result = true;
        }
    }

    return result;
};