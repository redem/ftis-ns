/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.SG = TAF.SG || {};
TAF.SG.DAO = TAF.SG.DAO || {};

//Supply Summary

TAF.SG.DAO.SuppDataSummary = function _SuppDataSummary() {
 return {
     supplyTotalSGD : '',
     gstTotalSGD : '',
     transactionCountTotal : ''
 };
};

TAF.SG.DAO.SuppDataSummaryDao = function _SuppDataSummaryDao() {
 this.savedSearch = 'customsearch_sg_iaf_salessummary';
 this.accounts = ['@NONE@', 'OthIncome', 'Income', 'DeferRevenue'];
};

TAF.SG.DAO.SuppDataSummaryDao.prototype.getSummary = function _getSummary(params) {
 try {
     var summary = new TAF.SG.DAO.SuppDataSummary();
     var multiBookJoinColumn = params.bookId ? 'accountingtransaction' : null;
     
     // Columns
     var columns = [new nlobjSearchColumn('netamount', multiBookJoinColumn, 'sum')];
     if (params.bookId) {
         // Unable to join taxamount to multiBookJoinColumn
         var formula = '{accountingtransaction.netamount}*{taxitem.rate}/100';
         columns.push(new nlobjSearchColumn('formulanumeric', null, 'sum').setFormula(formula));
     }     
     
     // Filters
     var filters = [new nlobjSearchFilter('internalid', 'accountingperiod', 'anyof', params.periodIds),
                    new nlobjSearchFilter('accounttype', multiBookJoinColumn, 'anyof', this.accounts)];
     if (params.subIds) {
         filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', params.subIds));
     }
     if (params.bookId) {
         filters.push(new nlobjSearchFilter('accountingbook', multiBookJoinColumn, 'is', params.bookId));
     }
     
     var result = nlapiSearchRecord('transaction', this.savedSearch, filters, columns);
     
     if (result && (result.length == 1)) {
         summary.supplyTotalSGD = result[0].getValue('netamount', multiBookJoinColumn, 'sum') || 0;
         summary.transactionCountTotal = result[0].getValue('formulatext', null, 'count') || 0;
         
         if (params.bookId) {
             summary.gstTotalSGD = result[0].getValue('formulanumeric', null, 'sum');
         } else {
             summary.gstTotalSGD = result[0].getValue('taxamount', null, 'sum');
         }
     }
     
     return summary;
 } catch (ex) {
     nlapiLogExecution('ERROR', 'TAF.SG.DAO.SuppDataSummaryDao.getSummary', ex.toString());
     throw nlapiCreateError('SEARCH_ERROR', 'SuppDataSummaryDao');
 }
};

// Supply Lines

TAF.SG.DAO.SuppData = function _SuppData(id) {
    return {
        id : id,
        isIndividual : '',
        customerFirstName : '',
        customerMiddleName : '',
        customerLastName : '',
        customerCompanyName : '',
        customerName : '',
        customerUEN : '',
        docDate : '',
        tranDate : '',
        number : '',
        lineID : '',
        item : '',
        netAmount : '',
        taxAmount : '',
        taxItem : '',
        shippingCountry : '',
        currency : '',
        exchangeRate : '',
        billingCountry : '',
        account : '',
    };
};

TAF.SG.DAO.SuppDataDao = function _SuppDataDao() {
    TAF.DAO.SearchDAO.call(this);
    
    this.recordType = 'transaction';
    this.savedSearchId = 'customsearch_sg_iaf_saleslines';
    this.accounts = ['@NONE@', 'OthIncome', 'Income', 'DeferRevenue'];
};
TAF.SG.DAO.SuppDataDao.prototype = Object.create(TAF.DAO.SearchDAO.prototype);

TAF.SG.DAO.SuppDataDao.prototype.createSearchFilters = function _createSearchFilters(params) {
    if(!params || !params.periodIds) {
        throw nlapiCreateError('MISSING_PARAMETER', 'params is required');
    }
    
    this.filters.push(new nlobjSearchFilter('postingperiod', null, 'is', params.periodIds));
    this.filters.push(new nlobjSearchFilter('accounttype', this.multiBookJoinColumn, 'anyof', this.accounts));
    
    if (this.isOneWorld) {
        this.filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', params.subIds));
    }
    if (this.isMultiBook) {
        this.filters.push(new nlobjSearchFilter('accountingbook', this.multiBookJoinColumn, 'is', params.bookId));
    }
};

TAF.SG.DAO.SuppDataDao.prototype.createSearchColumns = function _createSearchColumns(params) {
    this.columns.push(new nlobjSearchColumn('account', this.multiBookJoinColumn));
    
    if (this.multicurrency) {
        this.columns.push(new nlobjSearchColumn('currency'));
        this.columns.push(new nlobjSearchColumn('exchangerate'));
        this.columns.push(new nlobjSearchColumn('fxamount'));
    }
    
    if (this.isMultiBook) {
        this.columns.push(new nlobjSearchColumn('netamount', this.multiBookJoinColumn));
        this.columns.push(new nlobjSearchColumn('exchangerate', this.multiBookJoinColumn));
    }
};

TAF.SG.DAO.SuppDataDao.prototype.rowToObject = function _rowToObject(row) {
    if(!row) {
        throw  nlapiCreateError('MISSING_PARAMETER', 'row is required');
    }
    
    var txn = new TAF.SG.DAO.SuppData(row.getId());
    
    txn.isIndividual = row.getValue('isperson','customer') == 'T';
    txn.customerFirstName = row.getValue('firstname', 'customer');
    txn.customerMiddleName = row.getValue('middlename', 'customer');
    txn.customerLastName = row.getValue('lastname', 'customer');
    txn.customerCompanyName = row.getValue('companyname', 'customer');
    txn.customerName = row.getText('entityid');
    txn.customerUEN = row.getValue('custentity_4599_sg_uen', 'customer');
    txn.docDate = row.getValue('custbody_document_date');
    txn.tranDate = row.getValue('trandate');
    txn.number = row.getValue('number');
    txn.lineID = row.getValue('line');
    txn.item = row.getText('item');
    txn.account = row.getText('account', this.multiBookJoinColumn);
    txn.taxItem = row.getText('taxcode');
    txn.shippingCountry = row.getText('shipcountry');
    txn.billingCountry = row.getText('billcountry');
    txn.netAmount = row.getValue('netamount') || 0;
    txn.taxAmount = row.getValue('taxamount') || 0;
    txn.fxAmount = row.getValue('fxamount') || 0;
    txn.taxRate = row.getValue('rate', 'taxitem') || 0;

    if (this.multicurrency) {
        txn.currency = row.getValue('currency');
        txn.exchangeRate = row.getValue('exchangerate') || 1;
    }
    
    if (this.isMultiBook) {
        txn.bookNetAmount = row.getValue('netamount', this.multiBookJoinColumn) || 0;
        txn.bookExchangeRate = row.getValue('exchangerate', this.multiBookJoinColumn) || 1;
    }
    
    return txn;
};
 

// Realized Gain/Loss

TAF.SG.DAO.IAFRglDAO = function IAFRglDAO() {
    TAF.DAO.RglDao.call(this);
    this.name = 'IAFRglDAO';
    this.reportName = 'TAF SG IAF RGL';
};

TAF.SG.DAO.IAFRglDAO.prototype = Object.create(TAF.DAO.RglDao.prototype);
