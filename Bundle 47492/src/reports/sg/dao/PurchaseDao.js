/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.SG = TAF.SG || {};
TAF.SG.DAO = TAF.SG.DAO || {};

// Purchase Summary

TAF.SG.DAO.PurchaseSummary = function _PurchaseSummary() {
    var summary = {
        netAmountSum     : '',
        taxAmountSum     : '',
        formulaLineCount : ''
    };
    return summary;
};

TAF.SG.DAO.PurchaseSummaryDao = function _PurchaseSummaryDao() {
	this.savedSearch = 'customsearch_sg_iaf_purchase_summary';
	this.accounts = ['OthCurrAsset', 'FixedAsset', 'OthAsset', 'COGS', 'Expense', 'OthExpense', 'DeferExpense'];
};

TAF.SG.DAO.PurchaseSummaryDao.prototype.getSummary = function _getSummary(params) {
	try {
	    var summary = new TAF.SG.DAO.PurchaseSummary();
	    var multiBookJoinColumn = params.bookId ? 'accountingtransaction' : null;

	    // Columns
	    var columns = [new nlobjSearchColumn('netamount', multiBookJoinColumn, 'sum')];
	    if (params.bookId) {
	        // Unable to join taxamount to multiBookJoinColumn
	        var formula = '{accountingtransaction.netamount}*{taxitem.rate}/100';
	        columns.push(new nlobjSearchColumn('formulanumeric', null, 'sum').setFormula(formula));
	    }
        
	    // Filters
		var filters = [new nlobjSearchFilter('internalid', 'accountingperiod', 'anyof', params.periodIds),
		               new nlobjSearchFilter('accounttype', multiBookJoinColumn, 'anyof', this.accounts)];
		if (params.subIds) {
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', params.subIds));
		}
        if (params.bookId) {
            filters.push(new nlobjSearchFilter('accountingbook', multiBookJoinColumn, 'is', params.bookId));
        }
        
        // Search
		var result = nlapiSearchRecord('transaction', this.savedSearch, filters, columns);
		if (result && (result.length == 1)) {
			summary.netAmountSum = result[0].getValue('netamount', multiBookJoinColumn, 'sum');
			summary.formulaLineCount = result[0].getValue('formulatext', null, 'count');
			
			if (params.bookId) {
			    summary.taxAmountSum = result[0].getValue('formulanumeric', null, 'sum');
			} else {
	            summary.taxAmountSum = result[0].getValue('taxamount', null, 'sum');
			}
		}
	    
	    return summary;
	} catch (ex) {
		nlapiLogExecution('ERROR', 'TAF.SG.DAO.PurchaseSummaryDao.getSummary', ex.toString());
		throw nlapiCreateError('SEARCH_ERROR', 'PurchaseSummaryDao');
	}
};

// Purchase Lines

TAF.SG.DAO.Purchase = function _Purchase(id) {
    var txn = {
        id                   : id,
        type                 : '',
        entity               : '',
        mainName             : '',
        tranDate             : '',
        sgInvoiceDate        : '',
        sgUen                : '',
        number               : '',
        sgImportPermitNumber : '',
        line                 : '',
        item                 : '',
        account              : '',
        netAmount            : '',
        taxAmount            : '',
        taxCode              : '',
        currency             : '',
        fxAmount             : '',
        exchangeRate         : ''
    };
    return txn;
};

TAF.SG.DAO.PurchaseDao = function _PurchaseDao() {
    TAF.DAO.SearchDAO.call(this);
    
    this.recordType = 'transaction';
    this.savedSearchId = 'customsearch_sg_iaf_purchase';
    this.accounts = ['OthCurrAsset', 'FixedAsset', 'OthAsset', 'COGS', 'Expense', 'OthExpense', 'DeferExpense'];
};
TAF.SG.DAO.PurchaseDao.prototype = Object.create(TAF.DAO.SearchDAO.prototype);

TAF.SG.DAO.PurchaseDao.prototype.createSearchFilters = function _createSearchFilters(params) {
    if(!params || !params.periodIds) {
        throw nlapiCreateError('MISSING_PARAMETER', 'params is required');
    }
    
    this.filters.push(new nlobjSearchFilter('postingperiod', null, 'is', params.periodIds));
    this.filters.push(new nlobjSearchFilter('accounttype', this.multiBookJoinColumn, 'anyof', this.accounts));
    
    if (this.isOneWorld) {
        this.filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', params.subIds));
    }
    if (this.isMultiBook) {
        this.filters.push(new nlobjSearchFilter('accountingbook', this.multiBookJoinColumn, 'is', params.bookId));
    }
};

TAF.SG.DAO.PurchaseDao.prototype.createSearchColumns = function _createSearchColumns(params) {
    this.columns.push(new nlobjSearchColumn('account', this.multiBookJoinColumn));
    this.columns.push(new nlobjSearchColumn('netamount', this.multiBookJoinColumn));
    
    if (this.multicurrency) {
        this.columns.push(new nlobjSearchColumn('currency'));
        this.columns.push(new nlobjSearchColumn('exchangerate'));
        this.columns.push(new nlobjSearchColumn('fxamount'));
    }
    
    if (this.isMultiBook) {
        this.columns.push(new nlobjSearchColumn('netamount', this.multiBookJoinColumn));
        this.columns.push(new nlobjSearchColumn('exchangerate', this.multiBookJoinColumn));
    }
};

TAF.SG.DAO.PurchaseDao.prototype.rowToObject = function _rowToObject(row) {
    if(!row) {
        throw  nlapiCreateError('MISSING_PARAMETER', 'row is required');
    }
    
    var txn = new TAF.SG.DAO.Purchase(row.getId());
    
    txn.type = row.getValue('type');
    txn.entity = row.getText('entity');
    txn.mainName = row.getText('mainname');
    txn.tranDate = row.getValue('trandate');
    txn.sgInvoiceDate = row.getValue('custbody_document_date');
    txn.sgUen = row.getValue('custentity_4599_sg_uen', 'vendor');
    txn.number = row.getValue('number');
    txn.sgImportPermitNumber = row.getValue('custbody_4599_sg_import_permit_num');
    txn.line = row.getValue('line');
    txn.item = row.getText('item');
    txn.account = row.getText('account', this.multiBookJoinColumn);
    txn.taxCode = row.getText('taxcode');

    txn.netAmount = row.getValue('netamount') || 0;
    txn.taxAmount = row.getValue('taxamount') || 0;
    txn.fxAmount = row.getValue('fxamount') || 0;
    txn.taxRate = row.getValue('rate', 'taxitem') || 0;

    if (this.multicurrency) {
        txn.currency = row.getValue('currency');
        txn.exchangeRate = row.getValue('exchangerate') || 1;
    }
    
    if (this.isMultiBook) {
        txn.bookNetAmount = row.getValue('netamount', this.multiBookJoinColumn) || 0;
        txn.bookExchangeRate = row.getValue('exchangerate', this.multiBookJoinColumn) || 1;
    }
    
    return txn;
};
