/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.SG = TAF.SG || {};
TAF.SG.Adapter = TAF.SG.Adapter || {};

TAF.SG.Adapter.SuppDataSummary = function _SuppDataSummary() {
    return {
        supplyTotalSGD : '',
        gstTotalSGD : '',
        transactionCountTotal : ''
    };
};

TAF.SG.Adapter.SuppData = function _SuppData() {
    return {
        customerName : '',
        customerUEN : '',
        invoiceDate : '',
        invoiceNo : '',
        lineNo : '',
        productDescription : '',
        supplyValueSGD : '',
        gstValueSGD : '',
        taxCode : '',
        country : '',
        fcyCode : '',
        supplyFCY : '',
        gstFCY : ''
    };
};

TAF.SG.Adapter.SupplyAdapter = function _SupplyAdapter(params, state) {
    this.DEFAULT = {
        ISO_CURRENCY : 'XXX',
        DATE : '12/31/9999'
    };

    this.isMultibook = params.isMultibook;
    this.isMulticurrency = params.isMulticurrency;
    this.baseCurrency = params.baseCurrency;
    this.currencyMap = params.currencyMap;
    this.companyCountry = params.companyCountry;
    this.isRgl = params.isRgl;
    
    if (state) {
        this.state = state;
    } else {
        this.state = {
            tranId : -1,
            lineNo: 0
        };
    }
};

TAF.SG.Adapter.SupplyAdapter.prototype.convertSuppDataSummary = function _convertSuppDataSummary(summaryObj, realizedObj) {
    var summary = new TAF.SG.Adapter.SuppDataSummary();
    
    try {
        summary.supplyTotalSGD = Number(summaryObj.supplyTotalSGD) + Number(realizedObj.rglAmount);
        summary.gstTotalSGD = Number(summaryObj.gstTotalSGD);
        summary.transactionCountTotal = Number(summaryObj.transactionCountTotal) + Number(realizedObj.count);
    } catch (ex) {
        nlapiLogExecution('ERROR', 'TAF.SG.Adapter.SupplyAdapter.convertSuppDataSummary exception', ex.toString());
        throw nlapiCreateError('DATA_ERROR', 'Error in getting supply section summary');
    }
    
    return summary;
};


TAF.SG.Adapter.SupplyAdapter.prototype.convertSuppData = function _convertSuppData(searchObj) {
    var suppData = new TAF.SG.Adapter.SuppData();
    try {
        suppData.customerName = searchObj.isIndividual ? 
                [searchObj.customerFirstName, searchObj.customerMiddleName, searchObj.customerLastName].join(' ') :
                (searchObj.customerCompanyName || searchObj.customerName || '');
        suppData.customerUEN = searchObj.customerUEN || '';
        suppData.invoiceDate = searchObj.docDate || searchObj.tranDate || this.DEFAULT.DATE;
        suppData.invoiceNo = searchObj.number || '';
        suppData.productDescription = searchObj.item || searchObj.account || '';
        suppData.taxCode = searchObj.taxItem || '';
        suppData.country = (this.companyCountry != (searchObj.shippingCountry || searchObj.billingCountry)) ? 
                searchObj.shippingCountry || searchObj.billingCountry || '' : '';
        this.setLineNo(suppData, searchObj);
        this.setAmounts(suppData, searchObj);
    } catch (ex) {
        nlapiLogExecution('ERROR', 'TAF.SG.Adapter.SupplyAdapter.convertSuppData exception', ex.toString());
    }
    return suppData;
};


TAF.SG.Adapter.SupplyAdapter.prototype.setLineNo = function _setLineNo(suppData, searchObj) {
    if (this.state.tranId == searchObj.id) {
        suppData.lineNo = ++this.state.lineNo;
    } else {
        this.state.tranId = searchObj.id;
        this.state.lineNo = 1;
        suppData.lineNo = this.state.lineNo;
    }
};

TAF.SG.Adapter.SupplyAdapter.prototype.setAmounts = function _setAmounts(suppData, searchObj) {
    var taxRate = (parseFloat(searchObj.taxRate) || 0) / 100;
    this.setSGDValues(suppData, searchObj, taxRate);
    this.setFCYValues(suppData, searchObj, taxRate);
};


TAF.SG.Adapter.SupplyAdapter.prototype.setSGDValues = function _setSGDValues(suppData, searchObj, taxRate) {
    if (this.isMultibook) {
        suppData.supplyValueSGD = searchObj.bookNetAmount;
        suppData.gstValueSGD = searchObj.bookNetAmount * taxRate;
    } else {
        suppData.supplyValueSGD = searchObj.netAmount;
        suppData.gstValueSGD = searchObj.taxAmount;
    }
};

TAF.SG.Adapter.SupplyAdapter.prototype.setFCYValues = function _setFCYValues(suppData, searchObj, taxRate) {
    if ((!this.isMulticurrency) || (this.baseCurrency == searchObj.currency)) {
        suppData.fcyCode = this.DEFAULT.ISO_CURRENCY;
        suppData.supplyFCY = 0;
        suppData.gstFCY = 0;
    } else {
        var exchangeRate = searchObj.bookExchangeRate || searchObj.exchangeRate || 1;
        suppData.supplyFCY = searchObj.fxAmount;
        suppData.gstFCY = suppData.supplyFCY * taxRate;
        suppData.fcyCode = this.currencyMap[searchObj.currency] || '';
    }
};
