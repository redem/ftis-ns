/**
 * Copyright 2015 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var TAF = TAF || {};
TAF.MX = TAF.MX || {};
TAF.MX.Formatter = TAF.MX.Formatter || {};

TAF.MX.Formatter.PSV = function _PSV() {
	TAF.MX.Formatter.SAT.call(this);
    this.isXML = false;
    this.TEMPLATE.DIOT_LINE = '{vendorType}|{operationType}|{RFC}|{nonMXVendorTaxID}|{vendorName}|{nonMXCountry}|{nonMXNationality}|' +
	  						  '{S_net}|{DS_net}|{S_tax}|{R_net}|{DR_net}|{R_tax}|{IS_net}|{IS_tax}|{IR_net}|{IR_tax}|{IE_net}|' +
	  						  '{Z_IZ_net}|{E_net}|{wtax}|{paidDiscount}|';
};
TAF.MX.Formatter.PSV.prototype = Object.create(TAF.MX.Formatter.SAT.prototype);

