/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */
function MX_SAT_TrialBalance_Report(state, params, output, job) {
    params.isOneWorld = SFC.Context.IsOneWorld();
    params.period = SFC.PostingPeriods.Load(params.periodFrom);

    this.outline = {
       "Section": trialBalance
	};
	this.GetOutline = function() { return this.outline; };
	
	function trialBalance() {
		return new MX_SAT_TrialBalance_Section(state, params, output, job);
	}
}

//Report Sections
function MX_SAT_TrialBalance_Section(state, params, output, job) {
    TAF.MX.SAT.ReportSection.apply(this, arguments);
    this.Name         = 'TrialBalance';
    this.SAVED_REPORT = 'TAF Trial Balance';
    this.PROGRESS_PERCENTAGE = {
        HEADER:  10,
        BODY:    95,
        FOOTER:  100
    };

    this.adapter = new TAF.MX.Adapter.TrialBalanceAdapter();
}

MX_SAT_TrialBalance_Section.prototype = Object.create(TAF.MX.SAT.ReportSection.prototype);

MX_SAT_TrialBalance_Section.prototype.On_Init = function() {
    this.state[this.Name] = this.state[this.Name] || {currentIndex: -1};
    this.subsidiary = this.params.isOneWorld ? new TAF.DAO.SubsidiaryDao().getSubsidiaryInfo(this.params) : null;
    var accountingBook = this.getAccountingBook();
    var isSCOAIncluded = !accountingBook || accountingBook.isPrimary;    this.usesAccountingContext = this.params.accountingContext != '';
    var accountParams = this.getAccountParams();

    if (nlapiGetContext().getFeature('MULTIPLECALENDARS')) {
        this.validateAccountingPeriods(this.params.periodFrom, this.subsidiary.getFiscalCalendar());
    }
    
    this.accounts = new TAF.DAO.AccountDao().getList(accountParams, isSCOAIncluded);
    this.mappings = this.getAccountGroupMappings(accountParams);
    this.trialBalance = {};
    
    new TAF.DAO.EnhancedTrialBalanceDAO().getList({
        periodFrom: this.params.periodFrom,
        periodTo: this.params.periodFrom,
        subsidiary: this.params.subsidiary,
        group: this.params.include_child_subs,
        bookId: this.params.bookId
    }).forEach(function(a) {
        this.trialBalance[a.internalId] = a;
    }, this);
};

MX_SAT_TrialBalance_Section.prototype.On_Header = function() {
    var rawData = {
        isOneWorld  : this.params.isOneWorld,
        period      : this.params.period
    };
    
    if (this.params.isOneWorld) {
        rawData.subsidiary = this.subsidiary;
    } else {
        rawData.company = new TAF.DAO.CompanyDao().getInfo();
    }
    
    headerData = this.adapter.getHeaderData(rawData);
    this.output.WriteLine(this.formatter.formatHeader());
    this.output.WriteLine(this.formatter.formatBalanceHeader(headerData));
    
    this.output.SetFileName(this.formatter.formatTrialBalanceFilename(headerData));
    this.output.SetPercent(this.PROGRESS_PERCENTAGE.HEADER);
};

MX_SAT_TrialBalance_Section.prototype.On_Body = function() {
    var accountIds = this.adapter.getSortedAccountIds(this.accounts);
    var index = this.state[this.Name].currentIndex + 1;
    var line = {};
    var accountId = -1;
    
    for (var i = index; i < accountIds.length; i++) {
        accountId = accountIds[i];
        line = this.adapter.getLineData({
            account: this.accounts[accountId],
            balance: this.trialBalance[accountId] || {},
            group: this.mappings[accountId],            usesAccountingContext: this.usesAccountingContext
        });
        
        if (line) {
            this.output.WriteLine(this.formatter.formatBalanceBody(line));
        }
        
        this.state[this.Name].currentIndex = i;
        
        if (this.job.IsThresholdReached()) {
            return;
        }
    }
    
    this.output.SetPercent(this.PROGRESS_PERCENTAGE.BODY);
};

MX_SAT_TrialBalance_Section.prototype.On_Footer = function() {
    this.output.WriteLine(this.formatter.formatBalanceFooter());
    this.output.SetPercent(this.PROGRESS_PERCENTAGE.FOOTER);
};

//CRG Reports
var MX_SAT_TrialBalance_XML_Report = function _MX_SAT_TrialBalance_XML_Report(state, params, output, job) {
	params.formatter = new TAF.MX.Formatter.XML();
	MX_SAT_TrialBalance_Report.call(this, state, params, output, job);
};
MX_SAT_TrialBalance_XML_Report.prototype = Object.create(MX_SAT_TrialBalance_Report.prototype);
MX_SAT_TrialBalance_XML_Report.IsCRGReport = true;
MX_SAT_TrialBalance_XML_Report.ReportId = 'TRIAL_BALANCE_MX_XML';
