/**
 * © 2017 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 */

define(["N/record"],
    adapterRecord);

function adapterRecord(record) {
    return {
        create: function(options) {
            return record.create(options);
        },
        load: function(options){
            return record.load(options);
        },
        submitFields: function(options){
            return record.submitFields(options);
        }, 
        getType: function(options){
            return record.getType(options);
        }
    }    
}