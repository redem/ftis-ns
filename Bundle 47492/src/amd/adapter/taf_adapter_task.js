/**
 * Copyright 2017 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 */

define(['N/task'],
    adapterTask);

function adapterTask(task) {
    return {
        create: function(options) {
            return task.create(options);
        },
        getTaskType: function(taskType) {
            return task.TaskType[taskType];
        }
    };
}