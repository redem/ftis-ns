/**
 * Copyright 2017 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

define(['N/error', '../adapter/taf_adapter_xml'], function(error, xml) {
    function TAFSIIFileParser() {
        this.name = 'TAFSIIFileParser';
        this.XML_SALES_TRANSACTION_NODE = '//env:Envelope/env:Body/siiR:RespuestaLRFacturasEmitidas/siiR:RespuestaLinea';
        this.XML_PURCHASE_TRANSACTION_NODE = '//env:Envelope/env:Body/siiR:RespuestaLRFacturasRecibidas/siiR:RespuestaLinea';
        this.ERROR_CODE_TAG = 'siiR:CodigoErrorRegistro';
        this.TRANSACTION_NUMBER_TAG = 'sii:NumSerieFacturaEmisor';
    }
    
    TAFSIIFileParser.prototype = {
        toXML: function getList(stringXML) {
            if (!stringXML) {
                throw error.create({ name: 'MISSING_PARAMETER', message: 'stringXML parameter is required', notifyOff: true });
            }

            var xmlDocument;
            
            try {
                xmlDocument = xml.getParser().fromString({ text: stringXML });
            } catch (ex) {
                log.error({
                    title: 'SIIFileParser.toXML',
                    details: ex.toString()
                });
                throw error.create({ name: 'INVALID_XML', message: 'XML File is invalid.', notifyOff: true });
            }

            return xmlDocument;
        },
        extractTxnData: function search(xmlDoc) {
            if (!xmlDoc) {
                throw error.create({ name: 'MISSING_PARAMETER', message: 'xmlDoc parameter is required', notifyOff: true });
            }

            var data = {};
            var nodes;
            var errorElement;
            var transactionNumber;
            var isSales = true;

            try {
                nodes = this.getNodes(xmlDoc, this.XML_SALES_TRANSACTION_NODE);
                if (!nodes || nodes.length === 0) {
                    nodes = this.getNodes(xmlDoc, this.XML_PURCHASE_TRANSACTION_NODE);
                    isSales = false;
                }

                for (var i = 0; i < nodes.length; i++) {
                    transactionNumber = nodes[i].getElementsByTagName(this.TRANSACTION_NUMBER_TAG)[0].textContent;
                    data[transactionNumber] = {};

                    errorElement = nodes[i].getElementsByTagName(this.ERROR_CODE_TAG);
                    if (errorElement && errorElement.length > 0) {
                        data[transactionNumber] = errorElement[0].textContent;
                    } else {
                        data[transactionNumber] = 'REG';
                    }
                }
            } catch (ex) {
                log.error({
                    title: 'SIIFileParser.extractTxnData',
                    details: ex.toString()
                });
                throw error.create({ name: 'INVALID_XML', message: 'XML File is invalid.', notifyOff: true });
            }

            return {
                isSales: isSales,
                transactions: data
            };
        },
        getNodes: function(xmlDoc, xpath) {
            if (!xmlDoc) {
                throw error.create({ name: 'MISSING_PARAMETER', message: 'xmlDoc parameter is required', notifyOff: true });
            }
            if (!xpath) {
                throw error.create({ name: 'MISSING_PARAMETER', message: 'xpath parameter is required', notifyOff: true });
            }

            try {
                return xml.getXPath().select({
                    node: xmlDoc,
                    xpath: xpath
                });
            } catch (ex) {
                log.error({
                    title: 'SIIFileParser.getNodes',
                    details: ex.toString()
                });
                throw error.create({ name: 'INVALID_XML', message: 'XML File is invalid.', notifyOff: true });
            }
        }
    };

    return TAFSIIFileParser;
});
