/**
 * Copyright (c) 1998-2010 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

/**
 * Library for Thai VAT Reversal.
 * 
 * $Id$
 * 
 * @author faaliwin
 */

var THAI_TAX_CONFIGURATION_DATA = null;
var THAI_TAX_CONFIG_CUSTOM_RECORD = 'customrecord_ps_thai_tax_config';

var ThaiTaxLib = {};

/**
 * Returns Configuration value for the configuration field saved in custom record
 * @param {Object} configFieldName - Field ID in Configuration
 */
ThaiTaxLib.getConfigurationValue = function(configFieldName) {
	var objConfig = this.getConfigurationObject();
	if(objConfig != null){
		return objConfig[configFieldName] != null ? objConfig[configFieldName] : '';
	}
	return '';
}


ThaiTaxLib.getConfigurationObject = function(){
	if (THAI_TAX_CONFIGURATION_DATA == null) {
		
		var objConfig = {};
		
		try{
			
			// Search for Configuration record
			var results = nlapiSearchRecord(THAI_TAX_CONFIG_CUSTOM_RECORD, null, 
				[new nlobjSearchFilter('isinactive', null, 'is', 'F')],
				[new nlobjSearchColumn('internalid').setSort()]);
			
			if (results != null){
				var configId  = results[0].getId(); // get first result; business-wise should only get one result
				var configRec = nlapiLoadRecord(THAI_TAX_CONFIG_CUSTOM_RECORD, configId, {recordmode:'dynamic'});
				
				var fields = configRec.getAllFields();
				
				for (var i=0; i<fields.length; i++){
					
					var fieldValue = '';
					var fieldName  = fields[i];
					
					// only include custom record fields
					if (fieldName.indexOf('custrecord') == 0) {
						
						var objField = configRec.getField(fieldName);
					
						if (objField != null){
							var fieldType = objField.getType();
							if (fieldType == 'multiselect'){
								var valueArr = configRec.getFieldValue(fieldName);
								nlapiLogExecution('Debug', 'DEBUG', fieldName + ' Multi-select getFieldValue:' + valueArr);
								if (valueArr != null && valueArr != '') {
									fieldValue = valueArr.split('\u0005');
								}
								nlapiLogExecution('Debug', 'DEBUG', fieldName + ' Multi-select is empty:' + (fieldValue == null || fieldValue.length == 0));
								
							}else{
								fieldValue = configRec.getFieldValue(fieldName);
							}
						}
						if (fieldValue != null && fieldValue + ' ' != '' && fieldValue != undefined){
							objConfig[fieldName] = fieldValue;
						}
						
					}
				}
			}
			
			nlapiLogExecution('Debug', 'objConfig', JSON.stringify(objConfig));
		
		}catch(ex){
			var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' : ex.toString();
	        nlapiLogExecution('Error', 'getConfigRec', 'Error encountered while searching for Config record. ' + errorStr);
		}
		
		// copy to variable
		THAI_TAX_CONFIGURATION_DATA = objConfig;
		
	}
	
	return THAI_TAX_CONFIGURATION_DATA;
}


/**
 * Filters the transactions payment is applied to.
 * The following are the criteria for Thai Tax VAT Reversal:
 * 	- Apply Type = 'Invoice' or 'Bill'
 * 	- Invoice Type (custom) = 'Service'
 * 	- Due Date > Transaction Date
 *  - Vendor/Customer is not excluded from Configuration
 *  
 * 
 * Returns list of the filtered transactions internal IDs.
 * @param {Object} recordPayment
 */
ThaiTaxLib.filterApplyTransactions = function (recordPayment) {	

	var arrFilteredTransactions = new Array(); 
	
	if (recordPayment != null) {
		
		// Get list of all Transaction Internal IDs
		var arrInternalIDs = new Array();
		var applyLines = recordPayment.getLineItemCount('apply');
		for (var i = 1; i <= applyLines; i++) {
			// only include those applied
			if (recordPayment.getLineItemValue('apply', 'apply', i) == 'T') {
				arrInternalIDs.push(recordPayment.getLineItemValue('apply', 'doc', i));	
			}
		}
		
		// Filters:
		// - Invoice or Vendor Bill
		// - Invoice Type = 'Service'
		// - Due Date > Transaction Date
		var filters = [
			new nlobjSearchFilter('internalid', null, 'anyof', arrInternalIDs),
			new nlobjSearchFilter('type', null, 'anyof', ['CustInvc', 'VendBill']),
			new nlobjSearchFilter('custbody_invoice_type', null, 'anyof', ThaiTaxLib.getConfigurationValue('custrecord_conf_service_invoice_type')),
			new nlobjSearchFilter('formulanumeric', null, 'equalto', 1).setFormula('CASE WHEN {duedate} > {trandate} THEN 1 ELSE 0 END'),
			new nlobjSearchFilter('mainline', null, 'is', 'T')
		];
		
		// Add Excluded Entities
		var arrExcludedEntities = ThaiTaxLib.getConfigurationValue('custrecord_conf_excluded_entities');
		if (arrExcludedEntities != null && arrExcludedEntities != '' && arrExcludedEntities.length > 0) {
			new nlobjSearchFilter('name', null, 'noneof', arrExcludedEntities);
		}
		
		var results = nlapiSearchRecord('transaction', null, filters);
		
		for (var i in results ) {
			arrFilteredTransactions.push(results[i].getId());
		}
		
	}
	
	nlapiLogExecution('debug', 'DEBUG', 'Filtered Transaction IDs:' + JSON.stringify(arrFilteredTransactions));
	return arrFilteredTransactions;
}



/**
 * Gets the Total Amount Due for each transaction less the Withholding Tax
 * 
 * @param {Object} arrTransactionIds
 */
ThaiTaxLib.getTotalInvoicesDueLessWithholdingTax = function (arrTransactionIds) {
	
	var arrAmountsLessWithholdingTax = {};
	
	if (arrTransactionIds != null && arrTransactionIds.length > 0) {
		
		var filters = [
			new nlobjSearchFilter('internalid', null, 'anyof', arrTransactionIds),
			new nlobjSearchFilter('mainline', null, 'is', 'F'),
			new nlobjSearchFilter('taxline', null, 'is', 'F'),
			new nlobjSearchFilter('shipping', null, 'is', 'F'),
		];
		
		var cols = [
			new nlobjSearchColumn('internalid', null, 'group'),
			new nlobjSearchColumn('custcol_4601_witaxamount', null, 'sum'),
			//new nlobjSearchColumn('totalamount', null, 'max'),
			// take note of foreign currency conversion
			//new nlobjSearchColumn('formulanumeric', null, 'max').setFormula('CASE WHEN {amount} <> 0 THEN ({totalamount} / {amount}) * {fxamount} ELSE 0 END')
		];
		
		var results = nlapiSearchRecord('transaction', null, filters, cols);
		
		var totalWithholdingTax = 0;
		var totalAmount = 0;
		var totalAmountLessWithholdingTax = 0;
		var transactionId = '';
		for (var i in results) {
			
			totalWithholdingTax = results[i].getValue('custcol_4601_witaxamount', null, 'sum');
			transactionId = results[i].getValue('internalid', null, 'group');
			//totalAmount = results[i].getValue('formulanumeric', null, 'max');
			
			// get total amount from body field
			totalAmount = nlapiLookupField('transaction', transactionId, 'fxamount');
			nlapiLogExecution('debug', 'AMOUNT & TAX', 'TOTAL AMOUNT:' + totalAmount + ', WTAX AMOUNT:' + totalWithholdingTax);
			
			if (!isNaN(totalWithholdingTax) && !isNaN(totalAmount)) {
				
				// if negative value (case of Bills) multiply to -1
				if (parseFloat(totalWithholdingTax) < 0) {
					totalWithholdingTax = -1 * totalWithholdingTax;
				}
				
				// compute for amount less withholding tax
				totalAmountLessWithholdingTax = parseFloat(totalAmount) - parseFloat(totalWithholdingTax);
				arrAmountsLessWithholdingTax[transactionId] = totalAmountLessWithholdingTax;
			}
		}
		
	}
	
	nlapiLogExecution('debug', 'DEBUG', 'Amounts Less Withholding Tax:' + JSON.stringify(arrAmountsLessWithholdingTax));
	return arrAmountsLessWithholdingTax;
	
}



/**
 * Gets the VAT amount details for each transaction. Includes:
 * 	- VAT Amount
 * 	- Vat Account
 * 	- VAT Entity
 *  - Transaction Type
 * 
 * @param {Object} arrTransactionIds
 */
ThaiTaxLib.getVatAmountsAndAccounts = function (arrTransactionIds) {
	
	var arrVATAccountsAmounts = {};
	
	// Filters:
	// - VAT Memo
	// - Tax Line
	var filters  = [
		new nlobjSearchFilter('internalid', null, 'anyof', arrTransactionIds),
		new nlobjSearchFilter('memo', null, 'is', 'VAT'),
		new nlobjSearchFilter('taxline', null, 'is', 'T'),
		new nlobjSearchFilter('amount', null, 'greaterthan', '0')
	];
	
	var cols = [
		new nlobjSearchColumn('internalid').setSort(),
		new nlobjSearchColumn('type'),
		new nlobjSearchColumn('account'),
		new nlobjSearchColumn('fxamount'),
		new nlobjSearchColumn('name')
	];
	
	var results = nlapiSearchRecord('transaction', null, filters, cols);
	
	var transactionId = 0;
	
	for (var i in results) { 
		transactionId = results[i].getValue('internalid'); 
		if (arrVATAccountsAmounts[transactionId] == null) {
			arrVATAccountsAmounts[transactionId] = new Array();		
		}
		
		arrVATAccountsAmounts[transactionId].push(
			{account: results[i].getValue('account'), 
			amount: results[i].getValue('fxamount'),
			type: results[i].getValue('type'),
			name: results[i].getValue('name')}
		);
	}
	
	nlapiLogExecution('debug', 'DEBUG', 'VAT Accounts and Amounts:' + JSON.stringify(arrVATAccountsAmounts));
	return arrVATAccountsAmounts;
	
}