/**
 * Copyright (c) 1998-2010 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

/**
 * Reversal of VAT Undue. Applied to Payments.
 * 
 * $Id$
 * 
 * @author faaliwin
 */


function customizeGlImpact(transactionRecord, standardLines, customLines, book){
	
	if (transactionRecord != null) {
			
		nlapiLogExecution('debug', 'DEBUG', 'CREATE JOURNAL THAI TAX:' + transactionRecord.getFieldValue('custbody_create_journal_for_tax'));
		// Check if Create Journal is not checked
		if (transactionRecord.getFieldValue('custbody_create_journal_for_tax') != 'T') {
		
			var configThaiSubsidiary = ThaiTaxLib.getConfigurationValue('custrecord_conf_thai_subsidiary');
			var transSubsidiary = transactionRecord.getFieldValue('subsidiary');
			nlapiLogExecution('debug', 'DEBUG', 'CONFIG THAI SUBSIDIARY:' + configThaiSubsidiary + '  TRANSACTION SUBSIDIARY:' + transSubsidiary);
			
			// Apply only to Thailand Subsidiary
			if (configThaiSubsidiary == transSubsidiary) {
				
				// Get transaction records qualified for Thai Vat Reversal
				var arrFilteredTransactions = ThaiTaxLib.filterApplyTransactions(transactionRecord);
				
				// Post GL Lines for VAT Reversal
				reverseVATOfTransactions(arrFilteredTransactions, transactionRecord, customLines);
			}
		
		}
		
	}
	
}

/**
 * Posts Custom GL Lines to reverse the VAT Amounts for the included transactions
 * @param {Object} arrTransactionIds
 * @param {Object} transactionRecord
 * @param {Object} standardLines
 * @param {Object} customLines
 */
function reverseVATOfTransactions(arrTransactionIds, transactionRecord, customLines) {
	
	if (arrTransactionIds != null && arrTransactionIds.length > 0) {
						
		// Get amounts of transactions less the withholding tax
		var arrAmountsDueLessWithholdingTax = ThaiTaxLib.getTotalInvoicesDueLessWithholdingTax(arrTransactionIds);	
		
		// Get transaction vat amounts and accounts
		var arrTransactionsVATAccountsAmounts = ThaiTaxLib.getVatAmountsAndAccounts(arrTransactionIds);
		
		
		var applyTransactionId = 0;
		var paymentAmount = 0;
		var paymentRate = 0;
		var applyTransactionAmt = 0;
		var arrVATAccountsAmounts= null;
		var vatAmount = 0;
		var vatAccount = 0;
		var transactionType = '';
		
		var reverseVATAmount = 0;
		var transferToVATAccount = 0;
		var vatEntityId = 0;
		var newLine = null;
		
		var applyLines = transactionRecord.getLineItemCount('apply');
		for (var i = 1; i <= applyLines; i++) {
			applyTransactionId = transactionRecord.getLineItemValue('apply', 'doc', i);
			
			// Check if it is included in list of transactions
			if (arrTransactionIds.indexOf(applyTransactionId) >= 0) {
				
				paymentAmount = transactionRecord.getLineItemValue('apply', 'amount', i);
				applyTransactionAmt = arrAmountsDueLessWithholdingTax[applyTransactionId];
				
				// Get payment rate relative to Transaction Total Amount less Withholding Tax
				paymentRate = paymentAmount / applyTransactionAmt;
				nlapiLogExecution('debug', 'DEBUG', 'PAYMENT AMT:' + paymentAmount + ', AMT-WTTAX:' + applyTransactionAmt + ', PAYMENTRATE:' + paymentRate);
				
				arrVATAccountsAmounts = arrTransactionsVATAccountsAmounts[applyTransactionId];
				
				for (var k in arrVATAccountsAmounts) {
					
					// VAT Amount and Account
					vatAmount = arrVATAccountsAmounts[k].amount;
					vatAccount = arrVATAccountsAmounts[k].account;
					transactionType = arrVATAccountsAmounts[k].type;
					vatEntityId = arrVATAccountsAmounts[k].name;
					
					// Reversal VAT Amount will be prorated, depending on how much was paid
					reverseVATAmount = paymentRate * vatAmount;
					
					transferToVATAccount = '';
					if (transactionType == 'CustInvc') {
						transferToVATAccount = ThaiTaxLib.getConfigurationValue('custrecord_conf_invoice_reverse_account');
					} else if (transactionType == 'VendBill') {
						transferToVATAccount = ThaiTaxLib.getConfigurationValue('custrecord_conf_bill_reverse_account');
					}
					
					// Post GL Lines
					if (!PSUtils.isEmpty(transferToVATAccount)) {
						nlapiLogExecution('debug', 'DEBUG', 'ADD REVERSAL FOR LINE VAT AMOUNT DR:' + reverseVATAmount + ', Account: ' + vatAccount + ', Entity ID:' + vatEntityId);
						nlapiLogExecution('debug', 'DEBUG', 'ADD REVERSAL FOR LINE VAT AMOUNT CR:' + reverseVATAmount + ', Account: ' + transferToVATAccount + ', Entity ID:' + vatEntityId);
						
						// round to 2 decimal places
						reverseVATAmount = PSUtils.roundNumber(reverseVATAmount, 2);
						
						// Reversal for VAT Account
						newLine = customLines.addNewLine();
						if (transactionType == 'CustInvc') {
							newLine.setDebitAmount(reverseVATAmount);
						} else {
							newLine.setCreditAmount(reverseVATAmount);
						}
						newLine.setAccountId(parseInt(vatAccount));
						if (vatEntityId != null) {
							//newLine.setEntityId(vatEntityId);	
						}
						
						// Transfer to VAT Account
						newLine = customLines.addNewLine();
						if (transactionType == 'CustInvc') {
							newLine.setCreditAmount(reverseVATAmount);
						} else {
							newLine.setDebitAmount(reverseVATAmount);
						}
						newLine.setAccountId(parseInt(transferToVATAccount));
						if (vatEntityId != null) {
							//newLine.setEntityId(vatEntityId);	
						}
						
					}
					
				}
				
			}
			
		}
		
	}
}

var PSUtils = {};

PSUtils.isEmpty = function(f) {
	return (f == null || f + '' == '' || f == undefined);
};

PSUtils.roundNumber = function(value, places) {
	value = this.isEmpty(value) ? 0 : value;
    var multiplier = Math.pow(10, places);
    return (Math.round(value * multiplier) / multiplier);
};