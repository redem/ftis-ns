/**
 * Copyright (c) 1998-2010 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

/**
 * Reversal of VAT Undue. Applied to Payments.
 * 
 * $Id: ps_ue_thai_tax_payment.js 9493 2016-03-01 05:54:51Z faaliwin@ATHERTON $
 * 
 * @author faaliwin
 */


function afterSubmit_ThaiVATReversal (type) {
	
	if (type == 'create' || type == 'edit') {
		
		if (isEditThaiTaxJournal(type)) {
			applyThaiVatReversal();	
		}
		
		if (isDeleteThaiTaxJournal(type)) {
			// TODO Check if delete allowed
			deleteThaiTaxJournal();
		}
	}
	
}

/**
 * Do Thai VAT Reversal if applicable.
 * 
 */
function applyThaiVatReversal() {
	
	var configThaiSubsidiary = ThaiTaxLib.getConfigurationValue('custrecord_conf_thai_subsidiary');
	var transSubsidiary  = nlapiGetFieldValue('subsidiary');
	nlapiLogExecution('debug', 'DEBUG', 'CONFIG THAI SUBSIDIARY:' + configThaiSubsidiary + '  TRANSACTION SUBSIDIARY:' + transSubsidiary);
	
	// Apply only to Thailand Subsidiary
	if (configThaiSubsidiary == transSubsidiary) {
		
		// Get transaction records qualified for Thai Vat Reversal
		var arrFilteredTransactions = ThaiTaxLib.filterApplyTransactions(nlapiGetNewRecord());
		
		// Proceed if there are qualified transactions
		if (arrFilteredTransactions != null && arrFilteredTransactions.length > 0) {
			
			// Create/update journal entry
			var journalId = createThaiVatReversalJournal(arrFilteredTransactions);
			
			// Save reference to Journal here
			if (!PSUtils.isEmpty(journalId) && nlapiGetFieldValue('custbody_ps_thai_vat_reversal_journal') != journalId) {
				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custbody_ps_thai_vat_reversal_journal', journalId);
			}
			
		}
		
	}
	
}

/**
 * Create VAT Reversal Journal Record
 * 
 * @param {Object} arrFilteredTransactions
 */
function createThaiVatReversalJournal(arrFilteredTransactions) {
	var createdJournalId = '';
	
	var recJournal = getThaiVatReversalJournalRecord();
	
	if (recJournal != null) {
		
		if (!PSUtils.isEmpty(recJournal.getId())) {
			
			// Meaning case of existing record
			// Remove existing journal lines
			var linesCount = recJournal.getLineItemCount('line');
			for (var i = linesCount; i >= 1; i--) {
				recJournal.selectLineItem('line', i);
				recJournal.removeLineItem('line', i);
			}
			
		} else {
			
			// New journal record
			recJournal.setFieldValue('subsidiary', nlapiGetFieldValue('subsidiary'));
			recJournal.setFieldValue('currency', nlapiGetFieldValue('currency'));
			recJournal.setFieldValue('trandate', nlapiGetFieldValue('trandate'));
		}
		
		// Add GL Lines to Journal
		addVATReversalJournalLines(recJournal, arrFilteredTransactions, nlapiGetNewRecord());
		nlapiLogExecution('debug', 'DEBUG', 'Journal Lines: ' + recJournal.getLineItemCount('line'));
		
		if (recJournal.getLineItemCount('line') > 0) {
			createdJournalId = nlapiSubmitRecord(recJournal, false, true);
		}
		
	}
	
	return createdJournalId;
}

/**
 * Adds the GL Lines to the Journal record
 * 
 * @param {Object} recJournal
 * @param {Object} arrTransactionIds
 * @param {Object} transactionRecord
 */
function addVATReversalJournalLines(recJournal, arrTransactionIds, transactionRecord) {
	
	if (recJournal != null && arrTransactionIds != null && arrTransactionIds.length > 0 && transactionRecord != null) {
						
		// Get amounts of transactions less the withholding tax
		var arrAmountsDueLessWithholdingTax = ThaiTaxLib.getTotalInvoicesDueLessWithholdingTax(arrTransactionIds);	
		
		// Get transaction vat amounts and accounts
		var arrTransactionsVATAccountsAmounts = ThaiTaxLib.getVatAmountsAndAccounts(arrTransactionIds);
		
		
		var applyTransactionId = 0;
		var paymentAmount = 0;
		var paymentRate = 0;
		var applyTransactionAmt = 0;
		var arrVATAccountsAmounts= null;
		var vatAmount = 0;
		var vatAccount = 0;
		var transactionType = '';
		
		var reverseVATAmount = 0;
		var transferToVATAccount = 0;
		var vatEntityId = 0;
		var newLine = null;
		
		var applyLines = transactionRecord.getLineItemCount('apply');
		for (var i = 1; i <= applyLines; i++) {
			applyTransactionId = transactionRecord.getLineItemValue('apply', 'doc', i);
			
			// Check if it is included in list of transactions
			if (arrTransactionIds.indexOf(applyTransactionId) >= 0) {
				
				paymentAmount = transactionRecord.getLineItemValue('apply', 'amount', i);
				applyTransactionAmt = arrAmountsDueLessWithholdingTax[applyTransactionId];
				
				// Get payment rate relative to Transaction Total Amount less Withholding Tax
				paymentRate = paymentAmount / applyTransactionAmt;
				
				arrVATAccountsAmounts = arrTransactionsVATAccountsAmounts[applyTransactionId];
				
				for (var k in arrVATAccountsAmounts) {
					
					// VAT Amount and Account
					vatAmount = arrVATAccountsAmounts[k].amount;
					vatAccount = arrVATAccountsAmounts[k].account;
					transactionType = arrVATAccountsAmounts[k].type;
					vatEntityId = arrVATAccountsAmounts[k].name;
					
					// Reversal VAT Amount will be prorated, depending on how much was paid
					reverseVATAmount = paymentRate * vatAmount;
					
					transferToVATAccount = '';
					if (transactionType == 'CustInvc') {
						transferToVATAccount = ThaiTaxLib.getConfigurationValue('custrecord_conf_invoice_reverse_account');
					} else if (transactionType == 'VendBill') {
						transferToVATAccount = ThaiTaxLib.getConfigurationValue('custrecord_conf_bill_reverse_account');
					}
					
					// Post GL Lines
					if (transferToVATAccount != null && transferToVATAccount != '') {
						nlapiLogExecution('debug', 'DEBUG', 'ADD REVERSAL FOR LINE VAT AMOUNT DR:' + reverseVATAmount + ', Account: ' + vatAccount + ', Entity ID:' + vatEntityId);
						nlapiLogExecution('debug', 'DEBUG', 'ADD REVERSAL FOR LINE VAT AMOUNT CR:' + reverseVATAmount + ', Account: ' + transferToVATAccount + ', Entity ID:' + vatEntityId);
						
						// round to 2 decimal places
						reverseVATAmount = PSUtils.roundNumber(reverseVATAmount, 2);
						
						recJournal.selectNewLineItem('line');
						recJournal.setCurrentLineItemValue('line', (transactionType == 'CustInvc') ? 'debit' : 'credit', reverseVATAmount);
						recJournal.setCurrentLineItemValue('line', 'entity', vatEntityId);
						recJournal.setCurrentLineItemValue('line', 'account', vatAccount);
						recJournal.commitLineItem('line');
						
						recJournal.selectNewLineItem('line');
						recJournal.setCurrentLineItemValue('line', (transactionType == 'CustInvc') ? 'credit' : 'debit', reverseVATAmount);
						recJournal.setCurrentLineItemValue('line', 'entity', vatEntityId);
						recJournal.setCurrentLineItemValue('line', 'account', transferToVATAccount);
						recJournal.commitLineItem('line');
						
					}
					
				}
				
			}
			
		}
		
	}
	
	
}

/**
 * Returns new/existing record for VAT Reversal Journal Entry
 * 
 */
function getThaiVatReversalJournalRecord() {
	var journalId = nlapiGetFieldValue('custbody_ps_thai_vat_reversal_journal');
	
	if (PSUtils.isEmpty(journalId)) {
		return nlapiCreateRecord('journalentry', {recordmode: 'dynamic'})
	} else {
		return nlapiLoadRecord('journalentry', journalId, {recordmode: 'dynamic'});
	}
}

function deleteThaiTaxJournal() {
	var journalId = nlapiGetFieldValue('custbody_ps_thai_vat_reversal_journal');
	
	if (!PSUtils.isEmpty(journalId)) {
		nlapiDeleteRecord('journalentry', journalId);
	}
}

function isEditThaiTaxJournal(type) {
	return nlapiGetFieldValue('custbody_create_journal_for_tax') == 'T';
}

function isDeleteThaiTaxJournal(type) {
	if (type == 'edit') {
		
		return nlapiGetOldRecord().getFieldValue('custbody_create_journal_for_tax') == 'T' && 
			nlapiGetNewRecord().getFieldValue('custbody_create_journal_for_tax') != 'T';
		
	}
	
	return false;
}


var PSUtils = {};

PSUtils.isEmpty = function(f) {
	return (f == null || f + '' == '' || f == undefined);
};

PSUtils.roundNumber = function(value, places) {
	value = this.isEmpty(value) ? 0 : value;
    var multiplier = Math.pow(10, places);
    return (Math.round(value * multiplier) / multiplier);
};