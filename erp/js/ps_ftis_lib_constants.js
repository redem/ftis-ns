/**
 * Copyright (c) 1998-2014 NetSuite, Inc. 2955 Campus Drive, Suite 100, San
 * Mateo, CA, USA 94403-2511 All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of NetSuite,
 * Inc. ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with NetSuite.
 */

var CURRENCY_USD = '1';
var CURRENCY_NZD = '7';
var CURRENCY_SGD = '8';

var LOG_ACTION_ADD = '1';
var LOG_ACTION_UPDATE = '2';
var LOG_ACTION_REMOVED = '3';
