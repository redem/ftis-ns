/**
 * Copyright (c) 1998-2014 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

function beforeSubmit_ftlotentry(type) {
	var record = nlapiGetNewRecord();
	if (type=='create' || type=='edit') {
		_setBatchNo(record);
	}
}

function afterSubmit_ftlotentry(type) {
	if (type=='create' || type=='edit') {
		_setInventoryNumberDetails();
	}
}


/** 
 * Makes sure that the Lot# is set on 2 fields on the "FT Lot Entry" record. 
 * Whichever of the 2 fields below is not blank, use its lot# to fill up.
 *  (1) LOT NO. (Batch Entry custom record)
 *  (2) LOT #   (Inventory Number standard record)
 * */
function _setBatchNo(record) 
{
	var lotNo = record.getFieldText('custrecord_nslotno');
	var batchNo = record.getFieldText('custrecord_lotno');
	
	var no = lotNo; // default to Inventory No first
	no = isEmpty(no) ? batchNo : no; // if still blank, use the Batch Entry No instead
	
	if(isEmpty(batchNo) || isEmpty(lotNo)) {
		record.setFieldText('custrecord_lotno', no);
		record.setFieldText('custrecord_nslotno', no);
	}
}


/** Sync the FT Lot Entry details with the Inventory Number record */
function _setInventoryNumberDetails() 
{
	var record = nlapiGetNewRecord();
	var lotId = record.getFieldValue('custrecord_nslotno'); // this is an Inventory Number field
	if(!isEmpty(lotId)) 
	{
		var lotNo = record.getFieldText('custrecord_nslotno');
		var price = record.getFieldValue('custrecord_salesprice');
		var taxcodeId = record.getFieldValue('custrecord_gstcate');
		var subsidiaryId = record.getFieldValue('custrecord_subsidiary');
		var clearanceDate = record.getFieldValue('custrecord_ftlot_customclearancedate');
		var clearanceNo = record.getFieldValue('custrecord_ftlot_customclearancenum');
		var exchangeRate = record.getFieldValue('custrecord_ftlot_customexchangerate');
		var itemNo = record.getFieldValue('custrecord_ftlot_itemname');
		var buyPrice = record.getFieldValue('custrecord_ftlot_buyprice');
		
		nlapiSubmitField('inventorynumber', lotId, 
			['custitemnumber_salesprice',
			 'custitemnumber_gstcate',
			 'custitemnumber_subsidiary',
			 'custitemnumber_customclearancedate',
			 'custitemnumber_customclearancenum',
			 'custitemnumber_ftlot_customexchangerate'],
			[price, taxcodeId, subsidiaryId, clearanceDate, clearanceNo, exchangeRate]
		);
		
		nlapiLogExecution('debug', '_setInventoryNumberDetails', 'Updated inventory# ' + lotNo + ' ['+lotId+']');
	}
}