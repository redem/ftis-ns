

function afterSubmit(type)
{
	try{
		if(type=='create' || type=='edit') 
		{
			var record = nlapiGetNewRecord();
			var oldRecord = nlapiGetOldRecord();
			
			var prices = _getPurchasePrices(record);
			var pricesOld = _getPurchasePrices(oldRecord);
			
			var hasChanged = JSON.stringify(prices) != JSON.stringify(pricesOld);
			if(hasChanged)
			{
				var changesNew = difference(prices, pricesOld);
				var changesOld = difference(pricesOld, prices);
				
				// Something was added or updated
				if(changesNew.length >= changesOld.length) {
					for ( var i = 0; i < changesNew.length; i++) 
					{
						var change = changesNew[i];
						var oldChange = _.where(pricesOld, { v: change.v, c: change.c, s: change.s }); // makes vendor, subsidiary, currency unique        
						var isAdded = !oldChange || oldChange.length==0;
						if(isAdded) {
							createLog(change, null, LOG_ACTION_ADD);
						} else {
							createLog(change, oldChange[0], LOG_ACTION_UPDATE);
						}
						nlapiLogExecution('debug', 'afterSubmit', 'isAdded('+isAdded+') ' + JSON.stringify(change));
					}
				}
				
				// Something was deleted
				if(changesNew.length < changesOld.length) {
					for ( var i = 0; i < changesOld.length; i++) 
					{
						var change = changesOld[i];
						var oldChange = _.where(prices, { v: change.v, c: change.c, s: change.s }); // makes vendor, subsidiary, currency unique
						var isDeleted = !oldChange || oldChange.length==0;
						if(isDeleted) {
							createLog(change, null, LOG_ACTION_REMOVED);
						}
						nlapiLogExecution('debug', 'afterSubmit', 'isDeleted('+isDeleted+') ' + JSON.stringify(change));
					}
				}
			}
			nlapiLogExecution('debug', 'afterSubmit', 'prices => ' + JSON.stringify(prices));
			nlapiLogExecution('debug', 'afterSubmit', 'pricesOld => ' + JSON.stringify(pricesOld));
			nlapiLogExecution('debug', 'afterSubmit', 'hasChanged => ' + hasChanged);
		}
	}catch(erroraft){
		nlapiLogExecution('error', 'afterSubmit error', JSON.stringify(erroraft));
	}
}


function _getPurchasePrices(record) 
{
	var prices = [];
	
	if(record==null) {
		return prices;
	}
	
	// Cache all currencies
	if(this.currencyCache==null) {
		this.currencyCache = {};
		var filters = [ new nlobjSearchFilter('isinactive', null, 'is', 'F')];
		var columns = [ new nlobjSearchColumn('symbol')];
		var results = nlapiSearchRecord('currency', null, filters, columns);
		if(results!=null && results.length>0) {
			for(var i=0; i<results.length; i++) {
				var result = results[i];
				var symbol = result.getValue('symbol');
				this.currencyCache[symbol] = result.getId();
			}
		}
	}
	
	var tab = 'itemvendor';
	var lines = record.getLineItemCount(tab);
	for ( var i = 1; i <= lines; i++) 
	{
		var priceText = record.getLineItemValue(tab, 'vendorprices', i); // example data => THB: 1000.00|USD: 300.00
		var vendorId = record.getLineItemValue(tab, 'vendor', i);
		var subsidiaryId = record.getLineItemValue(tab, 'subsidiary', i);
		var vendorName = record.getLineItemText(tab, 'vendor', i);
		var priceremark = record.getFieldValue('custitem_ft_price_amendment_remark');
		
		nlapiLogExecution('debug', '_getPurchasePrices', 'vendor('+vendorName+') priceText('+priceText+')');
		
		if(!isEmpty(priceText)) 
		{
			var allPrices = priceText.split('|');
			for ( var j = 0; j < allPrices.length; j++) 
			{
				var priceData = allPrices[j].split(':');
				var currency = priceData[0];
				var price = priceData[1].trim();
				
				prices.push({ 
					i: record.getId(), 
					v: vendorId, 
					vn: vendorName, 
					c: this.currencyCache[currency], 
					p: Number(price), 
					s: subsidiaryId,
					priceremark:(priceremark)?priceremark:''
				});
			}
			prices = _.sortBy(prices, function(obj){
				return obj.v + obj.s + obj.c;
			});
		}
			
	}
	
	return prices;
}


function createLog (newChange, oldChange, actionType) 
{	
	nlapiLogExecution('debug', 'createLog', 'newChange => ' + JSON.stringify(newChange));
	nlapiLogExecution('debug', 'createLog', 'oldChange => ' + JSON.stringify(oldChange));
	
	var record = nlapiCreateRecord('customrecord_f_item_vendor_price_log');
	record.setFieldValue('custrecord_ivp_item', newChange.i);
	record.setFieldValue('custrecord_ivp_vendor', newChange.v);
	record.setFieldValue('custrecord_ivp_subsidiary', newChange.s);
	if(actionType==LOG_ACTION_UPDATE) {
		record.setFieldValue('custrecord_ivp_old_price_currency', oldChange.c || '');
		record.setFieldValue('custrecord_ivp_old_price', oldChange.p || '');
	}
	record.setFieldValue('custrecord_ivp_new_price_currency', newChange.c);
	record.setFieldValue('custrecord_ivp_new_price', newChange.p);
	record.setFieldValue('custrecord_ivp_action_type', actionType);
	

	record.setFieldValue('custrecord_price_amend_remark', newChange.priceremark);
	
	
	var logId = nlapiSubmitRecord(record, false, true);
	
	var actionLabel = {
		LOG_ACTION_ADD : 'Add',
		LOG_ACTION_UPDATE : 'Update',
		LOG_ACTION_REMOVED : 'Remove',
	};
	nlapiLogExecution('audit', 'createLog', 'Created log('+logId+') Action: '+ (actionLabel[actionType] || 'None'));
}


/** Performs a deep comparison of JSON objects */
var difference = function(array) {
	var rest = Array.prototype.concat.apply(Array.prototype, Array.prototype.slice.call(arguments, 1));
	var containsEquals = function(obj, target) {
		if (obj == null) {
			return false;
		}
		return _.any(obj, function(value) {
			return _.isEqual(value, target);
		});
	};
	return _.filter(array, function(value) {
		return !containsEquals(rest, value);
	});
};



function main_test(request, response) {
	
	var pricesOld = [{
		"v": "224",
		"vn": "*Hellmann World Wide",
		"c": "THB",
		"p": "700.00"
	}, {
		"v": "16",
		"vn": "*TH1 *Thailand Vendor",
		"c": "THB",
		"p": "1000.00"
	}, {
		"v": "16",
		"vn": "*TH1 *Thailand Vendor",
		"c": "USD",
		"p": "300.00"
	}];
	
	
	var prices = [{
		"v": "224",
		"vn": "*Hellmann World Wide",
		"c": "THB",
		"p": "570.00"
	}, {
		"v": "290",
		"vn": "*Gerry",
		"c": "SGD",
		"p": "20.00"
	}, {
		"v": "16",
		"vn": "*TH1 *Thailand Vendor",
		"c": "THB",
		"p": "1000.00"
	}, {
		"v": "16",
		"vn": "*TH1 *Thailand Vendor",
		"c": "USD",
		"p": "300.00"
	}];
	
	var currencyText = [ 'USD', 'SGD' ];
	
	var ids = [];
	var filters = toSearchFiltersByName('symbol', currencyText);
	var columns = [ new nlobjSearchColumn('name')];
	var results = nlapiSearchRecord('currency', null, filters, columns);
	if(results!=null && results.length>0) {
		for(var i=0; i<results.length; i++) {
			var result = results[i];
			var id = result.getId();
			var name = result.getValue('name');
			ids.push(id);
		}
	}

	response.write(JSON.stringify(ids));
}


