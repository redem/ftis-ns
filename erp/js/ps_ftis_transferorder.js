/**
 * Copyright (c) 1998-2014 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */


function beforeSubmit_transferorder(type) 
{
	var ctx = nlapiGetContext().getExecutionContext();
	if (type == 'create' || type == 'edit') 
	{
		var record = nlapiGetNewRecord();
		_setCustomerPartNoColumn(record) 
		_setItemAndCommercialRate(record);
		_setSellPriceDecimal(record); // Added by AMU 5/5/2016
        _setCustomerPartNoColumnFields(record); // Added by AMU 9/6/2016
	}
}

/**
 * Applied to a TO record, this function does 2 things:
 * (1) set the TO > Line > Item based on the Customer Part No
 * (2) set the TO > Line > Commercial Rate/Amount taken from the Customer record > Item Pricing information.
 *     To find the item pricing, the TO > "Transfer to Customer" and TO > Line > "Item" combination is used.
 * (3) set the TO > Line > Transfer Price taken from the Item record > Locations > Average Cost.
 *     To find the average cost of the item, use the TO > From Location + Line Item combination.    
 */
function _setItemAndCommercialRate(record) 
{
	// ------------------------------------------------------------
	// #1 - Set the TO > Line > Item based on the Customer Part No
	// ------------------------------------------------------------
	var partIds = [];
	var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) {
		var partId = record.getLineItemValue('item', 'custcol_custompartnoto', i);
		if(!isEmpty(partId)) {
			partIds.push(partId);
		}
	}
	nlapiLogExecution('debug', 'beforeSubmit_transferorder', 'partIds => ' + JSON.stringify(partIds));
	
	var partCache = {};
	if(partIds.length>0) {
		var filters = [ new nlobjSearchFilter('internalid', null, 'anyof', partIds) ];
		var columns = [ new nlobjSearchColumn('custrecord_ftis_customeritemref_nsitem') ];
		var results = nlapiSearchRecord('customrecord_ftis_customeritemref', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				var id = result.getId();
				var itemId = result.getValue('custrecord_ftis_customeritemref_nsitem');
				partCache[id] = itemId;
			}
		}
	}
	nlapiLogExecution('debug', 'beforeSubmit_transferorder', 'partCache => ' + JSON.stringify(partCache));
		
	
	var itemIds = [];
	var currencyIds = [];
	var priceLevelIds = [];
	
	var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) 
	{
		// Set the TO > Line > Item based on the customer part no
		var partId = record.getLineItemValue('item', 'custcol_custompartnoto', i);
		
		var currentItemId = record.getLineItemValue('item', 'item', i);
		var itemId = partCache[partId];
		if(!isEmpty(itemId) && itemId!=currentItemId) {
			record.setLineItemValue('item', 'item', i, itemId);
		}
		
		// Gather all item on lines to search for customer pricing data
		var itemId = record.getLineItemValue('item', 'item', i);
		itemIds.push(itemId);
	}
	nlapiLogExecution('debug', 'beforeSubmit_transferorder', 'itemIds => ' + JSON.stringify(itemIds));
	
	
	// -------------------------------------------------------------------------------------
	// #2 - Get the customer item price
	// -------------------------------------------------------------------------------------
	var priceCache = {};
	var customerId = record.getFieldValue('custbody_ftis_transfertocustomer');
	nlapiLogExecution('debug', 'beforeSubmit_transferorder', 'customerId('+customerId+')');
	
	if(!isEmpty(customerId)) {
		
		// Get the Customer > Item Pricing data
		var filters = [ 
            new nlobjSearchFilter('internalid', null, 'anyof', customerId),
            new nlobjSearchFilter('pricingitem', null, 'anyof', itemIds)
        ];
		var columns = [ 
            new nlobjSearchColumn('pricingitem'),
            new nlobjSearchColumn('currency'),
            new nlobjSearchColumn('itempricinglevel'),
            new nlobjSearchColumn('itempricingunitprice')
        ];			
		var results = nlapiSearchRecord('customer', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				var customerId = result.getId();
				var itemId = result.getValue('pricingitem');
				var priceLevel = result.getValue('itempricinglevel');
				var unitPrice = result.getValue('itempricingunitprice');
				var currencyId = result.getValue('currency');
				
				var key = customerId+'_'+itemId;
				priceCache[key] = {
					price: priceLevel,
					currency: currencyId,
					rate: unitPrice
				};
				
				priceLevelIds.push(priceLevel);
				currencyIds.push(currencyId);
			}
		}
	}
	
	
	// --------------------------------------------------------------------------
	// #3 - Get all TO > Transfer Price from Item > From Location > Average Cost
	// --------------------------------------------------------------------------
	var averageCostCache = {};
	var fromLocationId = record.getFieldValue('location');
	var filters = [ 
        new nlobjSearchFilter('internalid', null, 'anyof', itemIds),
        new nlobjSearchFilter('inventorylocation', null, 'anyof', fromLocationId),
    ];
	var columns = [ 
        new nlobjSearchColumn('inventorylocation'),
        new nlobjSearchColumn('locationaveragecost')
    ];
	var results = nlapiSearchRecord('item', null, filters, columns);
	if(results!=null && results.length>0) 
	{
		for(var i=0; i<results.length; i++) {
			var result = results[i];
			var itemId = result.getId();
			var locationId = result.getValue('inventorylocation');
			var averagecost = result.getValue('locationaveragecost') || 0;
			averageCostCache[itemId + '_' + locationId] = averagecost;
		}
	}
	
	
	nlapiLogExecution('debug', 'beforeSubmit_transferorder', 'priceCache => ' + JSON.stringify(priceCache));
	nlapiLogExecution('debug', 'beforeSubmit_transferorder', 'averageCostCache => ' + JSON.stringify(averageCostCache));
	
	
	// -------------------------------------------
	// Set the TO > Line > Commercial Rate/Amount
	// Set the TO > Line > Transfer Price 
	// -------------------------------------------
	var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) 
	{
		var itemId = record.getLineItemValue('item', 'item', i);
		var qty = record.getLineItemValue('item', 'quantity', i);
		 
		var key = customerId+'_'+itemId;
		var priceData = priceCache[key];
		if(priceData!=null) 
		{
			var rate = priceData.rate;
			var amount = rate * qty;
			
			var currentRate = record.getLineItemValue('item', 'custcol_ftis_comminvrate', i);
			var currentAmt = record.getLineItemValue('item', 'custcol_ftis_comminvamt', i);
			
			if(isEmpty(currentRate)) {
				record.setLineItemValue('item', 'custcol_ftis_comminvrate', i, rate);
			}
			if(isEmpty(currentRate)) {
				record.setLineItemValue('item', 'custcol_ftis_comminvamt', i, amount);
			}
		}
		
		var costKey = itemId + '_' + fromLocationId;
		var cost = averageCostCache[costKey] || 0;
		record.setLineItemValue('item', 'rate', i, cost);
	}
	
}


// Added by AMU 5/5/2016
function _setSellPriceDecimal(record) 
{
	var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) {
		var sellprice = record.getLineItemValue('item', 'custcol_ftis_base_price', i);
		if (sellprice != null) {
			record.setLineItemValue('item', 'custcol_ftis_sell_price_decimal', i, sellprice);
		}
	}
}

// Added by AMU 9/6/2016
function _setCustomerPartNoColumnFields(record)
{
var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) {
		var cir = record.getLineItemValue('item', 'custcol_custompartnoto', i);
		if (!isEmpty(cir)) {
			
			var cirFields= nlapiLookupField('customrecord_ftis_customeritemref', cir,  ['custrecord_ftis_customeritemref_custpart', 'custrecord_ftis_cust_rev', 'custrecord_ftis_database_location' ]); 
			
			var cpn = cirFields['custrecord_ftis_customeritemref_custpart'];
			if(cpn==null || cpn == undefined) {cpn='';}
			
			var custrev = cirFields['custrecord_ftis_cust_rev'];
			if(custrev==null || custrev==undefined) {custrev='';}
			
			var dblocation = cirFields['custrecord_ftis_database_location'];
			if(dblocation==null || dblocation==undefined) {dblocation='';}
			
			record.setLineItemValue('item', 'custcol_ftis_cpn', i, cpn);
			record.setLineItemValue('item', 'custcol_ftis_cust_rev', i, custrev );
			record.setLineItemValue('item', 'custcol_ftis_database_location', i, dblocation );
		}
	}


}
