/**
 * Copyright (c) 1998-2014 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */


var ACT_LIST_FULFILLMENTS = '1';
var ACT_CREATE_INVOICES = '2';
var ACT_CREATE_ONE_INVOICE = '3';

var DEFAULT_UI_TITLE = 'Bill Delivery Orders';

function main(request, response) 
{
	var action = request.getParameter('act');
	switch (action) {
		case ACT_CREATE_INVOICES: {
			return _doShowWaitScreen(request, response);
			 // _doCreateInvoices(request, response);
			 // break;
		}
		case ACT_CREATE_ONE_INVOICE: {
			return _doCreateOneInvoice(request, response);
		}
	}
	
	// Request parameters
	var doNo = request.getParameter('dono');
	var orderNo = request.getParameter('doorder');
	var orderDate = request.getParameter('dodate');
	var subsidiaryId = request.getParameter('subsidiary') || nlapiGetSubsidiary();
	
	// Script settings
	var title = nlapiGetContext().getSetting('script', 'custscript_slci_ui_title') || DEFAULT_UI_TITLE;
	var savedSearchId = nlapiGetContext().getSetting('script', 'custscript_slci_fulfill_savedsearch');
	
	// UI Form
	var form = nlapiCreateForm(title);
	var subsidiaryFld = form.addField('subsidiary', 'select', 'Subsidiary', 'subsidiary');
	var doNoFld = form.addField('dono', 'text', 'Filter by DO#');
	var orderDateFld = form.addField('dodate', 'date', 'Delivery Order Date');
	
	// UI form default values
	subsidiaryFld.setDefaultValue(subsidiaryId);
	doNoFld.setDefaultValue(doNo);
	orderDateFld.setDefaultValue(orderDate);
	
	// UI form mandatory fields
	subsidiaryFld.setMandatory(true);
	
	// UI Sublist
	var sublist = form.addSubList('item', 'list', 'Unbilled Order Lines');
	sublist.addField('cbox', 'checkbox', 'Select');
	sublist.addMarkAllButtons();
	_fillSublist(sublist, savedSearchId, doNo, orderNo, orderDate, subsidiaryId);
	
	
	// Embed a hidden action
	var actionFld = form.addField('act', 'text');
	actionFld.setDisplayType('hidden');
	actionFld.setDefaultValue(ACT_CREATE_INVOICES);
	
	// Add submit button
	form.addSubmitButton('Submit');
	form.addButton('custpage_btn_search', 'Search', 'search()');
	
	// Add client script
	form.setScript('customscript_f_cs_invoicing_client');
	
	response.writePage(form);
	
}

/** Renders a custom UI that transforms each selected order into an invoice */
function _doShowWaitScreen(request, response) 
{
	var ctx = nlapiGetContext();
	
	var title = nlapiGetContext().getSetting('script', 'custscript_slci_ui_title') || DEFAULT_UI_TITLE;
	var form = nlapiCreateForm(title);
	
	var fileId = ctx.getSetting('script', 'custscript_slci_ui_template');
	if(isEmpty(fileId)) {
		response.write('No configured HTML template');
		return;
	}
	
	var orderIds = [];
	var orderLineCache = {};
	
	var lines = request.getLineItemCount('item');
	for (var i = 1; i <= lines; i++) 
	{
		var isChecked = request.getLineItemValue('item', 'cbox', i)=='T' ? true : false;
		if(!isChecked) {
			continue;
		}
		
		var closedTaskId = request.getLineItemValue('item', 'taskid', i);
		var orderId = request.getLineItemValue('item', 'orderid', i);
		var orderLineId = request.getLineItemValue('item', 'line', i);
		var qty = request.getLineItemValue('item', 'qty', i);
		var itemId = request.getLineItemValue('item', 'itemid', i);
		var itemName = request.getLineItemValue('item', 'item', i);
		var partNoId = request.getLineItemValue('item', 'partid', i);
		var priceLevel = request.getLineItemValue('item', 'price', i);
		var rate = request.getLineItemValue('item', 'rate', i);
		var taxrate = request.getLineItemValue('item', 'taxrate', i);
		var taxcode = request.getLineItemValue('item', 'taxcode', i);
		var lotprice = request.getLineItemValue('item', 'lotprice', i);
		var lottaxcode = request.getLineItemValue('item', 'lottaxcode', i);
		var lotid = request.getLineItemValue('item', 'lotid', i);
		var batchId = request.getLineItemValue('item', 'batchid', i);
		var locationId = request.getLineItemValue('item', 'location', i);
		var itemType = request.getLineItemValue('item', 'itemtype', i);
		var entityId = request.getLineItemValue('item', 'entity', i);
		var ftlotId = request.getLineItemValue('item', 'ftlotid', i);
		var delivery = request.getLineItemValue('item', 'delivery', i);
		var lineId = request.getLineItemValue('item', 'line', i);
		
		// Group selected rows by Sale Order
		var orderLines = [];
		
		var orderLines = orderLineCache[orderId] || [];
		orderLines.push({
			item: itemId,
			itemname: itemName,
			qty: qty,
			partid: partNoId,
			taskid: closedTaskId,
			price: priceLevel,
			lotid: lotid,
			lotprice: lotprice,
			lottaxcode: lottaxcode,
			batchid: batchId,
			rate: rate,
			order: orderId,
			line: orderLineId,
			taxrate: taxrate,
			taxcode: taxcode,
			location: locationId,
			itemtype: itemType,
			entity: entityId,
			batchid: batchId,
			delivery: delivery,
			ftlotid: ftlotId,
			line: lineId
		});
		orderLineCache[orderId] = orderLines;
	}
	
	nlapiLogExecution('debug', 'xxxx', 'orderLineCache => ' + JSON.stringify(orderLineCache));
	
	var suiteletURL = nlapiResolveURL('suitelet', ctx.getScriptId(), ctx.getDeploymentId());
	
	var doNo = request.getParameter('dono');
	var orderDate = request.getParameter('dodate');
	var subsidiaryId = request.getParameter('subsidiary') || nlapiGetSubsidiary();
	
	// The base URL should carry the filters entered on the UI form
	var suiteletBaseURL =  suiteletURL;
	if(!isEmpty(doNo)) suiteletBaseURL += '&dono='+doNo;
	if(!isEmpty(orderDate)) suiteletBaseURL += '&dodate='+orderDate;
	if(!isEmpty(subsidiaryId)) suiteletBaseURL += '&subsidiary='+subsidiaryId;
	
	// Load the html template (with javascript to submit each invoice individually)
	var template = nlapiLoadFile(fileId).getValue();
	var template = _.template(template);
	var content = template({
		'data': JSON.stringify(orderLineCache),
		'suiteletURL' : suiteletURL + '&act=' + ACT_CREATE_ONE_INVOICE,
		'suiteletBaseURL' : suiteletBaseURL
	});
	
	form.addField('custpage_data', 'inlinehtml').setDefaultValue(content);
	response.writePage(form);
}

function _fillSublist(sublist, savedSearchId, doNo, orderNo, orderDate, subsidiaryId) 
{
	var list = [];
	
	var orderIds = [];
	var fulfillIds = [];
	var batchNos = [];
	var itemIds = [];
	
	var filters = [ 
        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
        new nlobjSearchFilter('custrecord_erp_is_invoiced', null, 'is', 'F')
    ];
	if(!isEmpty(subsidiaryId)) {
		 filters.push( new nlobjSearchFilter('subsidiary', 'custrecord_ebiztask_customer', 'anyof', subsidiaryId));
	}
	if(!isEmpty(doNo)) {
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_wave_no', null, 'startswith', doNo));
	}
	if(!isEmpty(orderNo)) {
		filters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiztask_ebiz_order_no', 'startswith', orderNo));
	}
	if(!isEmpty(orderDate)) {
		 filters.push( new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on', orderDate));
	}
	filters = filters.length>0 ? filters : null;
	
	var columns = [ 
        new nlobjSearchColumn('name'),
        new nlobjSearchColumn('custrecord_ebiztask_act_qty'),
        new nlobjSearchColumn('itemid', 'custrecord_ebiztask_sku'), // item
        new nlobjSearchColumn('itemid', 'custrecord_ebiztask_parent_item_no'), // item 
        new nlobjSearchColumn('type', 'custrecord_ebiztask_parent_item_no'), // item 
        new nlobjSearchColumn('custrecord_ebiz_nsconf_refno_ebiztask'),
        new nlobjSearchColumn('custrecord_ebiztask_batch_no'),
        new nlobjSearchColumn('custrecord_ebiztask_ebiz_wave_no'),
        new nlobjSearchColumn('custrecord_ebiztask_line_no'),
        new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no'),
        new nlobjSearchColumn('tranid', 'custrecord_ebiztask_ebiz_order_no'),
        new nlobjSearchColumn('custrecord_ebiztask_lotno_with_quantity') 
    ];
	
	var results = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', savedSearchId, filters, columns);
	if(results!=null && results.length>0) 
	{
		for(var i=0; i<results.length; i++) 
		{
			var result = results[i];
			
			var id = result.getId();
			var name = result.getValue('name');
			var qty = result.getValue('custrecord_ebiztask_act_qty');
			var parentItemId = result.getValue('itemid', 'custrecord_ebiztask_sku');
			var itemNo = result.getValue('itemid', 'custrecord_ebiztask_parent_item_no');
			var itemId = result.getValue('custrecord_ebiztask_parent_item_no');
			var itemType = result.getValue('type', 'custrecord_ebiztask_parent_item_no');
			var batchNo = result.getValue('custrecord_ebiztask_batch_no');
			var doNo = result.getValue('custrecord_ebiztask_ebiz_wave_no');
			var lineNo = result.getValue('custrecord_ebiztask_line_no');
			var orderNo = result.getValue('tranid', 'custrecord_ebiztask_ebiz_order_no');
			var orderId = result.getValue('custrecord_ebiztask_ebiz_order_no');
			var fulfillId = result.getValue('custrecord_ebiz_nsconf_refno_ebiztask');
			var lotNo = result.getValue('custrecord_ebiztask_lotno_with_quantity');
			
			var orderURL = nlapiResolveURL('record', 'salesorder', orderId);
			var fulfillURL = nlapiResolveURL('record', 'itemfulfillment', fulfillId);
			var closedTaskURL = nlapiResolveURL('record', 'customrecord_ebiznet_trn_ebiztask', id);
			
			list.push({
				id: '<a href="'+closedTaskURL+'" target="_blank">'+id+'</a>',
				taskid: id,
				name: name,
				qty: qty,
				itemid: itemId,
				item: itemNo,
				parentitem: parentItemId,
				batch: batchNo,
				delivery: doNo,
				line: lineNo,
				orderid: orderId,
				order: '<a href="'+orderURL+'" target="_blank">'+orderNo+'</a>',
				fulfillment: '<a href="'+fulfillURL+'" target="_blank">'+fulfillId+'</a>',
				fulfillid: fulfillId,
				itemtype: itemType
			});
			
			orderIds.push(orderId);
			fulfillIds.push(fulfillId);
			batchNos.push(batchNo);
			itemIds.push(itemId);
		}
	}
	
	
	orderIds = _.uniq(orderIds);
	batchNos = _.uniq(batchNos);
	fulfillIds = _.uniq(fulfillIds);
	itemIds = _.uniq(itemIds);
	
	nlapiLogExecution('debug', 'batchNos', JSON.stringify(batchNos));
	
	// Fill up the sales order related information
	if(orderIds.length>0) 
	{
		var orderCache = {};
		
		var filters = [ 
            new nlobjSearchFilter('internalid', null, 'anyof', orderIds),
            new nlobjSearchFilter('mainline', null, 'is', 'F')
            // TODO: insert a status filter (only Pending Billing and Pending Billing/Partially Fulfilled)
        ];
		var columns = [ 
            new nlobjSearchColumn('custcol_ftis_customerpartno'),
            new nlobjSearchColumn('custcol_ftis_cust_po'),
            new nlobjSearchColumn('pricelevel'),
            new nlobjSearchColumn('rate'),
            new nlobjSearchColumn('taxcode'),
            new nlobjSearchColumn('line'),
            new nlobjSearchColumn('location'),
            new nlobjSearchColumn('entity')
        ];
		var results = nlapiSearchRecord('salesorder', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			// Extract the relevant SO information
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				var orderId = result.getId();
				var line = result.getValue('line');
				var partId = result.getValue('custcol_ftis_customerpartno');
				var partNo = result.getText('custcol_ftis_customerpartno');
				var custPO = result.getValue('custcol_ftis_cust_po');
				var priceLevel = result.getValue('pricelevel');
				var rate = result.getValue('rate');
				var taxcode = result.getValue('taxcode');
				var locationId = result.getValue('location');
				var entityId = result.getValue('entity');
				
				var lineKey = orderId+'_'+line;
				orderCache[lineKey] = {
					partno: partNo,
					partid: partId,
					po: custPO,
					price: priceLevel,
					rate: rate,
					taxcode: taxcode,
					location: locationId,
					entity: entityId
				};
			}
		}
		
		// ---------------------------
		// Gets the fulfillment dates
		// ---------------------------
		var fulfillmentCache = {};
		var filters = [ 
            new nlobjSearchFilter('mainline', null, 'is', 'T'),
            new nlobjSearchFilter('internalid', null, 'anyof', fulfillIds)
        ];
		var columns = [ new nlobjSearchColumn('trandate') ];
		var results = nlapiSearchRecord('itemfulfillment', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				var id = result.getId();
				var date = result.getValue('trandate');
				
				fulfillmentCache[id] = { date: date };
			}
		}
		
		
		var itemCache = {};
		var filters = [ new nlobjSearchFilter('internalid', null, 'anyof', itemIds) ];
   		var results = nlapiSearchRecord('item', null, filters);
   		if(results!=null && results.length>0) 
   		{
   			for(var i=0; i<results.length; i++) 
   			{
   				var result = results[i];
   				var id = result.getId();
   				var type = result.getRecordType();
   				itemCache[id] = { type: type };
   			}
   		}
		
   		// nlapiLogExecution('debug', 'xxx', 'itemCache => ' + JSON.stringify(itemCache) );
		
		// Add the sales order info to the to-be invoiced list
		for (var i = 0; i < list.length; i++) 
		{
			var data = list[i];
			
			var fulfillId = data.fulfillid;
			var orderId = data.orderid;
			var line = data.line;
			var lineKey = orderId+'_'+line;
			
			var orderData = orderCache[lineKey];
			if(orderData!=null) 
			{
				data.partno = orderData.partno;
				data.po = orderData.po;
				data.price = orderData.price;
				data.rate = orderData.rate;
				data.taxcode = orderData.taxcode;
				data.location = orderData.location;
				data.entity = orderData.entity;
				data.itemtype = itemCache[data.itemid].type;
				
				var fulfillData = fulfillmentCache[fulfillId];
				if(fulfillData!=null) {
					data.dodate = fulfillData.date;
				}
			}
			
		}
	}
	
	
	// Resolve the Batch Entry ids
	if(batchNos.length>0) 
	{
		var batchIds = [];
		var batchCache = {};
		
		var filters = toSearchFiltersByName('custrecord_ebizlotbatch', batchNos);
		var columns = [ 
            new nlobjSearchColumn('name'),
            new nlobjSearchColumn('custrecord_ebizlotbatch'),
        ];
		var results = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				var id = result.getId();
				var name = result.getValue('name');
				var batchNo = result.getValue('custrecord_ebizlotbatch');
				
				batchCache[name] = {
					id: id,
					batch: batchNo
				};
				batchIds.push(id);
			}
			
			// Add the lot pricing info to the to-be invoiced list
			for (var i = 0; i < list.length; i++) 
			{
				var data = list[i];
				
				var batchNo = data.batch;
				var batchData = batchCache[batchNo];
				if(batchData!=null) {
					data.batchid = batchData.id;
				}
			}
		}
		
		batchIds = _.uniq(batchIds);
		
		nlapiLogExecution('debug', 'xx', 'batchIds = ' + JSON.stringify(batchIds));
		nlapiLogExecution('debug', 'xx', 'batchCache = ' + JSON.stringify(batchCache));
		
		if(batchIds.length>0) 
		{
			var ftLotCache = {};
			
			var filters = [ 
	            new nlobjSearchFilter('isinactive', null, 'is', 'F'),
	            new nlobjSearchFilter('custrecord_lotno', null, 'anyof', batchIds)
	        ];
			var columns = [ 
	            new nlobjSearchColumn('custrecord_ebizlotbatch', 'custrecord_lotno'),
	            new nlobjSearchColumn('custrecord_gstcate'),
	            new nlobjSearchColumn('custrecord_nslotno'),
	            new nlobjSearchColumn('custrecord_subsidiary'),
	            new nlobjSearchColumn('custrecord_salesprice'),
	            new nlobjSearchColumn('custrecord_ftlot_customclearancedate'),
	            new nlobjSearchColumn('custrecord_ftlot_customclearancenum')
	        ];
			var results = nlapiSearchRecord('customrecord_ftlotentry', null, filters, columns);
			if(results!=null && results.length>0) 
			{
				for(var i=0; i<results.length; i++) 
				{
					var result = results[i];
					
					var id = result.getId();
					var batchNo = result.getValue('custrecord_ebizlotbatch', 'custrecord_lotno');
					var lotId = result.getValue('custrecord_nslotno');
					var lotNo = result.getText('custrecord_nslotno');
					var price = result.getValue('custrecord_salesprice');
					var taxcodeId = result.getValue('custrecord_gstcate'); // taxcode
					var subsidiaryId = result.getValue('custrecord_subsidiary');
					var clearanceDate = result.getValue('custrecord_ftlot_customclearancedate');
					var clearanceNo = result.getValue('custrecord_ftlot_customclearancenum');
					
					ftLotCache[batchNo] = {
						id: id,
						lotid: lotId,
						lotno: batchNo,
						price: price,
						taxcode: taxcodeId
					};
				}
				
				// Add the lot pricing info to the to-be invoiced list
				for (var i = 0; i < list.length; i++) 
				{
					var data = list[i];
					
					var batchNo = data.batch;
					var lotData = ftLotCache[batchNo];
					if(lotData!=null) {
						data.ftlotid = lotData.id;
						data.lotid = lotData.lotid;
						data.lotprice = lotData.price;
						data.lotno = lotData.lotno
						data.lottaxcode = lotData.taxcode;
					}
				}
			}
		}
		
	}
	
	// Show the sublist and populate with the list data
	sublist.addField('name', 'text', 'Name');
	sublist.addField('id', 'text', 'ID');
	sublist.addField('delivery', 'text', 'DO#');
	sublist.addField('batch', 'text', 'Batch No');
	sublist.addField('fulfillment', 'text', 'Fulfillment');
	sublist.addField('order', 'text', 'Order');
	sublist.addField('parentitem', 'text', 'Parent Item');
	sublist.addField('item', 'text', 'Item');
	sublist.addField('line', 'integer', 'Line');
	sublist.addField('qty', 'float', 'Actual Quantity');
	sublist.addField('po', 'text', 'Customer PO');
	sublist.addField('partno', 'text', 'Customer Part No');
	sublist.addField('dodate', 'date', 'DO Date');
	// hidden columns
	sublist.addField('taskid', 'text').setDisplayType('hidden');
	sublist.addField('orderid', 'text').setDisplayType('hidden');
	sublist.addField('fulfillid', 'text').setDisplayType('hidden');
	sublist.addField('itemid', 'text', 'Item ID').setDisplayType('inline');
	sublist.addField('price', 'text', 'Price').setDisplayType('inline');
	sublist.addField('rate', 'text', 'Rate').setDisplayType('inline');
	sublist.addField('taxcode', 'text', 'Tax Code').setDisplayType('inline');
	sublist.addField('ftlotid', 'text', 'FT Lot Id').setDisplayType('inline');
	sublist.addField('lotid', 'text', 'Lot Id').setDisplayType('inline');
	sublist.addField('lotno', 'text', 'Lot Number').setDisplayType('inline');
	sublist.addField('lotprice', 'text', 'Lot Price').setDisplayType('inline');
	sublist.addField('lottaxcode', 'text', 'Lot Tax Code').setDisplayType('inline');
	sublist.addField('batchid', 'text', 'Batch ID').setDisplayType('inline');
	sublist.addField('location', 'text', 'Location').setDisplayType('inline');
	sublist.addField('itemtype', 'text', 'Item Type').setDisplayType('inline');
	sublist.addField('entity', 'text', 'Entity').setDisplayType('inline');
	
	sublist.setLineItemValues(list);
}

function _doCreateOneInvoice(request, response) 
{
	var reply = {
		success: false
	};
	
	var orderId = request.getParameter('oid');
	var orderLines = JSON.parse(request.getParameter('lines'));
	var doNumbers = _.pluck(orderLines, 'delivery');
	
	var customFormId = nlapiGetContext().getSetting('script', 'custscript_slci_invoice_customform');
	
	try {
		
		var invoiceId = _transformToInvoice(orderId, orderLines, customFormId);
		reply.success = true;
		
	} catch (ex) {
		reply.success = false;
		reply.message = getErrorMsg(ex);
		reply.tranid = 'SO#' +orderId+' DO#: '+doNumbers.join(', ');
	}
	
	nlapiLogExecution('debug', '_doCreateOneInvoice', 'orderId('+orderId+') doNumbers => ' + JSON.stringify(doNumbers));
	
	response.write(JSON.stringify(reply));
}


/** Not used anymore */
function _doCreateInvoices(request, response) 
{
	var lotIds = [];
	var orderIds = [];
	var orderLineCache = {};
	
	var lines = request.getLineItemCount('item');
	for (var i = 1; i <= lines; i++) 
	{
		var isChecked = request.getLineItemValue('item', 'cbox', i)=='T' ? true : false;
		if(!isChecked) {
			continue;
		}
		
		var closedTaskId = request.getLineItemValue('item', 'taskid', i);
		var orderId = request.getLineItemValue('item', 'orderid', i);
		var orderLineId = request.getLineItemValue('item', 'line', i);
		var qty = request.getLineItemValue('item', 'qty', i);
		var itemId = request.getLineItemValue('item', 'itemid', i);
		var itemName = request.getLineItemValue('item', 'item', i);
		var partNoId = request.getLineItemValue('item', 'partid', i);
		var priceLevel = request.getLineItemValue('item', 'price', i);
		var rate = request.getLineItemValue('item', 'rate', i);
		var taxrate = request.getLineItemValue('item', 'taxrate', i);
		var taxcode = request.getLineItemValue('item', 'taxcode', i);
		var lotprice = request.getLineItemValue('item', 'lotprice', i);
		var lottaxcode = request.getLineItemValue('item', 'lottaxcode', i);
		var lotid = request.getLineItemValue('item', 'lotid', i);
		var batchId = request.getLineItemValue('item', 'batchid', i);
		var locationId = request.getLineItemValue('item', 'location', i);
		var itemType = request.getLineItemValue('item', 'itemtype', i);
		var entityId = request.getLineItemValue('item', 'entity', i);
		var ftlotId = request.getLineItemValue('item', 'ftlotid', i);
		var delivery = request.getLineItemValue('item', 'delivery', i);
		var lineId = request.getLineItemValue('item', 'line', i);
		
		// Group selected rows by Sale Order
		var orderLines = [];
		
		var orderLines = orderLineCache[orderId] || [];
		orderLines.push({
			item: itemId,
			itemname: itemName,
			qty: qty,
			partid: partNoId,
			taskid: closedTaskId,
			price: priceLevel,
			lotid: lotid,
			lotprice: lotprice,
			lottaxcode: lottaxcode,
			batchid: batchId,
			rate: rate,
			order: orderId,
			line: orderLineId,
			taxrate: taxrate,
			taxcode: taxcode,
			location: locationId,
			itemtype: itemType,
			entity: entityId,
			batchid: batchId,
			delivery: delivery,
			ftlotid: ftlotId,
			line: lineId
		});
		orderLineCache[orderId] = orderLines;
		
		if(!isEmpty(lotid)) {
			lotIds.push(lotid);
		}
	}
	
	// Get all Inventory Number information
	var lotData = _getLotData(lotIds);
	nlapiLogExecution('debug', 'xxxx', 'lotNoCache => ' + JSON.stringify(lotData));
	
	var customFormId = nlapiGetContext().getSetting('script', 'custscript_slci_invoice_customform');
	var MIN_USAGE = 40;
	
	// Create each invoice from the sales order
	for (var orderId in orderLineCache)
	{	
		nlapiLogExecution('debug', '_doCreateInvoices', 'orderid = ' + orderId);
		
		try 
		{
			var orderLines = orderLineCache[orderId];
			if(orderLines!=null) 
			{
				// Check the remaining usage
				var usageLeft = nlapiGetContext().getRemainingUsage();
				if(usageLeft > MIN_USAGE) 
				{
					// Create one invoice
					var invoiceId = _transformToInvoice(orderId, orderLines, customFormId);
					
					nlapiLogExecution('audit', '_doCreateInvoices', 
						'Created invoice('+invoiceId+') from salesorder('+orderId+')');
				}
			}
			
		} catch (ex) {
			nlapiLogExecution('error', '_doCreateInvoices', 'Failed to create invoice for order('+orderId+') ::  ' + getErrorMsg(ex) );
			throw ex;
		}
	}
}


function _transformToInvoice(orderId, orderLines, customFormId) 
{
	var invoice = nlapiTransformRecord('salesorder', orderId, 'invoice', {
		recordmode: 'dynamic',
		customform: customFormId
	});
	
	for (var i = 0; i < orderLines.length; i++) {
		var orderLine = orderLines[i];
		if(!isEmpty(orderLine.line)) {
			var lineNo = invoice.findLineItemValue('item', 'line', orderLine.line);
			nlapiLogExecution('debug', '_createOneInvoice', 'orderline='+orderLine.line+' lineNo=' + lineNo);
			
			if(!isEmpty(lineNo)) {
				var tab = 'item';
				invoice.selectLineItem(tab, lineNo);
				invoice.setCurrentLineItemValue(tab, 'custcol_f_line_closed_task', orderLine.taskid);
				invoice.setCurrentLineItemValue(tab, 'custcol_ftis_customerpartno', orderLine.partid);
				invoice.setCurrentLineItemValue(tab, 'custcol_ft_batchno', orderLine.batchid);
				invoice.setCurrentLineItemValue(tab, 'custcol_nslotno', orderLine.lotid);
				invoice.setCurrentLineItemValue(tab, 'custcol_ft_lotno', orderLine.ftlotid);
				invoice.commitLineItem(tab);
			}
		}
	}
	
	var invoiceId = nlapiSubmitRecord(invoice, false, true);
	nlapiLogExecution('audit', '_transformToInvoice', 'Invoice('+invoiceId+') created from Order('+orderId+')');
	
	// Update the invoice to put the lot details
	var linesByLineNo = _.indexBy(orderLines, 'line');
	invoice = loadRecord('invoice', invoiceId);
	var lines = invoice.getLineItemCount('item');
	for (var i = 1; i <= lines; i++) {
		
		var quantity = invoice.getLineItemValue('item', 'quantity', i);
		var rate = invoice.getLineItemValue('item', 'rate', i);
		var amount = invoice.getLineItemValue('item', 'amount', i);
		var itemId = invoice.getLineItemValue('item', 'item', i);
		var lineNo = invoice.getLineItemValue('item', 'line', i);
		
		var tab = 'recmachcustrecord_fild_invoice';
		invoice.selectNewLineItem(tab);
		invoice.setCurrentLineItemValue(tab, 'custrecord_fild_order', orderId);
		invoice.setCurrentLineItemValue(tab, 'custrecord_fild_invoice', invoiceId);
		invoice.setCurrentLineItemValue(tab, 'custrecord_fild_orderline', lineNo);
		invoice.setCurrentLineItemValue(tab, 'custrecord_fild_item', itemId);
		invoice.setCurrentLineItemValue(tab, 'custrecord_fild_quantity', quantity);
		invoice.setCurrentLineItemValue(tab, 'custrecord_fild_rate', rate);
		invoice.setCurrentLineItemValue(tab, 'custrecord_fild_amount', amount);
		
		var orderLine = linesByLineNo[lineNo];
		if(orderLine) {
			invoice.setCurrentLineItemValue(tab, 'custrecord_fild_ft_lotentry', orderLine.ftlotid);
			invoice.setCurrentLineItemValue(tab, 'custrecord_fild_closedtask', orderLine.taskid);
		}
		
		invoice.commitLineItem(tab);
	}
	nlapiSubmitRecord(invoice);
	nlapiLogExecution('audit', '_transformToInvoice', 'Created Invoice Details:  invoice('+invoiceId+')');
	
	return invoiceId;
}


/** NOT USED anymore */
function _createOneInvoice(orderLines, customFormId) 
{
	// Create a new invoice
	var invoice = nlapiCreateRecord('invoice', {
		recordmode: 'dynamic',
		customform: customFormId
	});
	
	// Add new line items for the selected closed tasks
	for (var i = 0; i < orderLines.length; i++) 
	{	
		var orderLine = orderLines[i];
		nlapiLogExecution('debug', '_createOneInvoice', 'orderLine => ' + JSON.stringify(orderLine));
		
		if(i==0) {
			invoice.setFieldValue('entity', orderLine.entity);
			invoice.setFieldValue('location', orderLine.location);
		}
		
		invoice.selectNewLineItem('item');
		invoice.setCurrentLineItemValue('item', 'item', orderLine.item); 
		invoice.setCurrentLineItemValue('item', 'quantity', orderLine.qty);
		invoice.setCurrentLineItemValue('item', 'location', orderLine.location);
		invoice.setCurrentLineItemValue('item', 'custcol_f_line_closed_task', orderLine.taskid);
		invoice.setCurrentLineItemValue('item', 'custcol_ftis_customerpartno', orderLine.partid);
		invoice.setCurrentLineItemValue('item', 'custcol_ft_batchno', orderLine.batchid);
		invoice.setCurrentLineItemValue('item', 'custcol_nslotno', orderLine.lotid);
		invoice.setCurrentLineItemValue('item', 'custcol_ft_lotno', orderLine.ftlotid);
		
		// Resolve price and tax code
		var price = orderLine.price;
		var rate = orderLine.rate;
		if(!isEmpty(orderLine.lotprice)) { 
			price = '-1';
			rate = orderLine.lotprice;
		}
		var taxcode = !isEmpty(orderLine.lottaxcode) ? orderLine.lottaxcode : orderLine.taxcode;
		
		// Set price and tax code
		// invoice.setCurrentLineItemValue('item', 'price', price);
		invoice.setCurrentLineItemValue('item', 'rate', rate);
		invoice.setCurrentLineItemValue('item', 'orderdoc', orderLine.order); // Attach the line to the SO line
		invoice.setCurrentLineItemValue('item', 'orderline', orderLine.line); // Attach the line to the SO line
		
		nlapiLogExecution('debug', '_createOneInvoice', 'orderLine('+ JSON.stringify(orderLine) +')');
		
		// Set the inventory detail subrecord on the line
		if(orderLine.itemtype == 'lotnumberedinventoryitem') 
		{
			var subrecord = invoice.createCurrentLineItemSubrecord('item', 'inventorydetail');
			subrecord.selectNewLineItem('inventoryassignment');
			
			var lotFld = subrecord.getLineItemField('inventoryassignment', 'issueinventorynumber', subrecord.getLineItemCount('inventoryassignment') + 1);
			if(lotFld!=null) 
			{
				var lotOptions = lotFld.getSelectOptions() || [];
				var availableLots = [];
				for ( var j = 0; j < lotOptions.length; j++) {
					availableLots.push( lotOptions[j].getId() );
				}
				nlapiLogExecution('audit', '_createOneInvoice', 
					'Trying to set salesorder('+orderLine.order+') line('+orderLine.line+') item('+orderLine.item+') '+
					'to lot('+orderLine.lotid+') from these available lots '+ JSON.stringify(availableLots));
				
				// Only set the LOT if it is one of the available lot ids
				var foundLot = false;
				for (var j = 0; j < availableLots.length; j++) {
					var lotId = availableLots[j];
					if(lotId==orderLine.lotid) {
						subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', orderLine.lotid);
						break;
					}
				}
				if(foundLot) {
					subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', orderLine.qty);
					subrecord.commitLineItem('inventoryassignment');
					subrecord.commit();
				}
				else
				{
					subrecord.cancel();
				}
			}
			
		}
		
		invoice.commitLineItem('item');
	}
	
	nlapiLogExecution('debug', '_createOneInvoice', 'invoice => ' + JSON.stringify(invoice));
	
	// Save the invoice
	var invoiceId = null;
	
	try {
		var lines = invoice.getLineItemCount('item');
		if(lines>0) {
			invoiceId = nlapiSubmitRecord(invoice, false, true);
		}
	} catch (ex) {
		nlapiLogExecution('error', '_createOneInvoice', 'Failed to create invoice.. ' + getErrorMsg(ex));
		throw ex;
	}
	return invoiceId;
}


function afterSubmit_invoiceFromBillDeliveryOrders() {
	var ctx = nlapiGetContext().getExecutionContext();
	if(type=='create' && ctx=='suitelet') 
	{
		var record = nlapiGetNewRecord();
		var createdFromId = record.getFieldValue('createdfrom');
		
		nlapiLogExecution('debug', 'afterSubmit_invoiceFromBillDeliveryOrders', 
			'Invoice('+record.getId()+') createdfrom('+createdFromId+')');
		
		if(!isEmpty(createdFromId)) {
			
		}
	}
}


function afterSubmit_invoice(type) 
{
	if(type=='create' || type=='edit') 
	{
		var record = nlapiGetNewRecord();
		_updateClosedTask(record);
	}
}

function _updateClosedTask(record) 
{
	var closedTaskIds = [];
	var batchEntryIds = [];
	var lotIds = [];
	
	var lines = record.getLineItemCount('item');
	for (var i = 1; i <= lines; i++) 
	{
		var taskId = record.getLineItemValue('item', 'custcol_f_line_closed_task', i);
		if(!isEmpty(closedTaskIds)) {
			closedTaskIds.push(taskId);
		}
		
		var batchId = record.getLineItemValue('item', 'custcol_ft_batchno', i);
		if(!isEmpty(batchId)) {
			batchEntryIds.push(batchId);
		}
		
		var lotId = record.getLineItemValue('item', 'custcol_nslotno', i);
		if(!isEmpty(lotId)) {
			lotIds.push(lotId);
		}
	}
	
	// Update all "Closed Task" first
	for (var i = 0; i < closedTaskIds.length; i++) {
		var taskId = closedTaskIds[i];
		nlapiSubmitField('customrecord_ebiznet_trn_ebiztask', taskId, 'custrecord_erp_is_invoiced', 'T');
		
		nlapiLogExecution('audit', 'xxxxx', 
			'Updating closed task('+taskId+') for invoice('+record.getId());
	}
}


function _getLotData(lotIds) 
{
	var lotNoCache = {};
	
	nlapiLogExecution('debug', '_getLotData', 'lotIds => ' + JSON.stringify(lotIds));
	if(lotIds.length>0) {
		
		var filters = [ 
	       new nlobjSearchFilter('internalid', null, 'anyof', lotIds)
	    ];
		var columns = [ 
	       new nlobjSearchColumn('inventorynumber'),
	       new nlobjSearchColumn('isonhand'),
	       new nlobjSearchColumn('item'),
	       new nlobjSearchColumn('location'),
	       new nlobjSearchColumn('expirationdate'),
	       new nlobjSearchColumn('custitemnumber_salesprice'),
	       new nlobjSearchColumn('custitemnumber_gstcate'),
	       new nlobjSearchColumn('custitemnumber_subsidiary'),
	       new nlobjSearchColumn('custitemnumber_customclearance'),
	       new nlobjSearchColumn('custitemnumber_customclearancenum'),
	       new nlobjSearchColumn('quantityavailable'),
	       new nlobjSearchColumn('quantityonhand'),
	       new nlobjSearchColumn('quantityintransit'),
	       new nlobjSearchColumn('quantityonorder')
	    ];
		var results = nlapiSearchRecord('inventorynumber', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				
				var id = result.getId();
				var itemId = result.getValue('item');
				var lotNo = result.getValue('inventorynumber');
				var price = result.getValue('custitemnumber_salesprice');
				var taxcodeId = result.getValue('custitemnumber_gstcate'); // taxcode
				var subsidiaryId = result.getValue('custitemnumber_subsidiary');
				var clearanceDate = result.getValue('custitemnumber_customclearance');
				var clearanceNo = result.getValue('custitemnumber_customclearancenum');
				
				lotNoCache[id] = {
					id: id,
					item: itemId,
					number: lotNo,
					price: price,
					taxcode: taxcodeId,
					subsidiary: subsidiaryId
				};
			}
		}
	}
	return lotNoCache;
}


function main_test(request, response) {
	
	var customerId = '22';
	
	var record = nlapiCreateRecord('invoice');
	record.setFieldValue('entity', customerId);
	record.setFieldValue('location', '5');
	
	record.selectNewLineItem('item');
	record.setCurrentLineItemValue('item', 'item', '9314');
	record.setCurrentLineItemValue('item', 'quantity', '1');
	record.setCurrentLineItemValue('item', 'rate', '100');
	
	// Attach the line to the SO line
	//record.setCurrentLineItemValue('item', 'orderdoc', orderLine.order);
	//record.setCurrentLineItemValue('item', 'orderline', orderLine.line);
	
	// Set the inventory detail subrecord on the line
	var subrecord = record.createCurrentLineItemSubrecord('item', 'inventorydetail');
	subrecord.selectNewLineItem('inventoryassignment');
	subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', '12');
	subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
	subrecord.commitLineItem('inventoryassignment');
	subrecord.commit();
	
	record.commitLineItem('item');
	
	nlapiSubmitRecord(record, false, true);
}



