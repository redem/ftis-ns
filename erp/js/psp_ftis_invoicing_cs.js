
var ACT_LIST_FULFILLMENTS = '1';


function search() {
	nlapiSetFieldValue('act', ACT_LIST_FULFILLMENTS);
	setWindowChanged(window, false);
	document.forms['main_form'].submit();
}


function reset() {
	nlapiSetFieldValue('dono', '');
	nlapiSetFieldValue('doorder', '');
	nlapiSetFieldValue('dodate', '');
}

function fieldChanged(type, name) {
	if(name=='dono' || name=='dodate' || name=='subsidiary') {
		search();
	}
}


function saveRecord() {
	return confirm('This will create invoices for selected rows. Are you sure?');
}
