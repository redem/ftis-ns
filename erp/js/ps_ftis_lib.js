
function isEmpty(str) {
    return (!str || 0 === str.length || /^\s*$/.test(str));
}

function loadRecord(type, id) {
	var record = null;
	try {
		record = nlapiLoadRecord(type, id);
	} catch (ex) {}
	return record;
}

function trimToZero(text) {
	var number = !isNaN(text) || !isEmpty(text) ? Number(text) : 0;
	return number;
}

function trimToBlank(text) {
	text = text || '';
	text = text.trim();
	return text;
}

function getErrorMsg(e) {
	if (e instanceof nlobjError) {
		return e.getCode() + '\n' + e.getDetails();
	} else {
		return e.toString();
	}
}

// Searching
function searchRecord(type, callback) {
	searchRecord(type, null, null, null, callback);
}
function searchRecord(type, savedSearchId, callback) {
	searchRecord(type, savedSearchId, null, null, callback);
}
function searchRecord(type, filters, columns, callback) {
	searchRecord(type, null, filters, columns, callback);
}
function searchRecord(type, savedSearchId, filters, columns, callback) 
{
	var search = null;
	if(savedSearchId!=null) {
		search = nlapiLoadSearch(type, savedSearchId);
		if(filters!=null) { search.addFilters(filters); }
		if(columns!=null) { search.addColumns(columns); }
	} else {
		search = nlapiCreateSearch(type, filters, columns);
	}
	
	var results;
	var start = 0;
	var end = 1000;
	var total = 0;
	
	do 
	{
		results = search.runSearch().getResults(start, end);
		for (var i=0; results!=null && i<results.length; i++){
			var result = results[i];
			if (callback && typeof(callback) === "function") {  
		        var continueToNext = callback(result, i);
		        if(continueToNext) {
		        	break;
		        }
		    }  
		}
		start+= 1000;
		end+= 1000;
		
		total+= results.length;
	} 
	while (results.length==1000);
	
	return total;
}


function toSearchFiltersByName(fieldId, values) 
{
	var filters = [];
	values = values || [];
	values = _.uniq(values);
	
	var total = values.length;
	if (total == 1)
	{
		var value = values[0];
		if(!isEmpty(value)) {
			filters.push(new nlobjSearchFilter(fieldId, null, 'is', value));
		}
	}
	else 
	{
		for (var i = 0; i < total; i++) {
			var value = values[i];
			if(!isEmpty(value)) 
			{
				filters.push(new nlobjSearchFilter(fieldId, null, 'is', value));
				filters[i].setOr(true);
				if (i == 0) {
					filters[0].setLeftParens(1);
				} else if (i == total - 1) {
					filters[i].setRightParens(1);
				}
			}
		}
	}
	return filters.length>0 ? filters : null;
}

function getSystemURL() 
{
	var accountId = nlapiGetContext().getCompany();
	
	var systemURL = accountId.startsWith('TST') ? 
		'https://system.na1.netsuite.com' : 
			'https://system.netsuite.com';
	
	var environment = nlapiGetContext().getEnvironment();
	if (environment == 'SANDBOX') {
		systemURL = 'https://system.sandbox.netsuite.com';
	}
	
	return systemURL;
}

function newDate() 
{
	// The current date and time captured within the server is set to PST
	// Create a new date that corresponds to the user's timezone and not in PST
	var conf = nlapiLoadConfiguration('userpreferences');
	var userRawTz = conf.getFieldText('TIMEZONE');
	var userTz = userRawTz.split(' ')[0].replace('(GMT','').replace(')','');
	var userOffset = convertToMinutes(userTz);

	var date = new Date();
	var serverRawOffset = date.getUTCOffset(); // -0800
	var serverOffset = convertToMinutes(serverRawOffset);
	
	var adjDate = date.clone();
	adjDate.addMinutes(serverOffset * -1);
	adjDate.addMinutes(userOffset);
	
	return adjDate;
}


function convertToMinutes(offsetStr) {
    var sign = offsetStr.substring(0, 1);
	var time = offsetStr.substring(1);
	time = time.replace(':', '');
	var hrs = Number(time.substring(0, 2));
	var mins = Number(time.substring(2));
	var offsetMins = (hrs * 60) + mins;
    if(sign=='-') {
        offsetMins = offsetMins * -1; 
    }
    return Number(offsetMins).toFixed(2);
}

if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}

