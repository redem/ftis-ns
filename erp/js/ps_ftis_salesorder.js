/**
 * Copyright (c) 1998-2014 NetSuite, Inc. 2955 Campus Drive, Suite 100, San
 * Mateo, CA, USA 94403-2511 All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of NetSuite,
 * Inc. ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with NetSuite.
 */

function beforeLoad(type, form, request) 
{
	var ctx = nlapiGetContext().getExecutionContext();
	nlapiLogExecution('debug', 'beforeLoad', 'type='+type + ' ctx='+ctx);
	
	if(type=='copy' && ctx=='userinterface') 
	{
		var record = nlapiGetNewRecord();
		var sub = record.getFieldValue('subsidiary');
		if(sub!=7){
		
			var lines = record.getLineItemCount('item');
			for (var i = 1; i <= lines; i++) 
			{
				record.setLineItemValue('item', 'custcol_ft_lotno', i, null);
				record.setLineItemValue('item', 'custcol_ft_batchno', i, null);
				record.setLineItemValue('item', 'custcol_nslotno', i, null);
				record.setLineItemValue('item', 'custcol_customs_exchange_rate', i, null);
				record.setLineItemValue('item', 'custcol_ft_customclearancedoc', i, null);
				record.setLineItemValue('item', 'custcol_ft_customclearancedate', i, null);
			}
		
		}
	}
}

function beforeSubmit(type) 
{
	var ctx = nlapiGetContext().getExecutionContext();
	nlapiLogExecution('debug', 'beforeSubmit', 'type='+type + ' ctx='+ctx);
	
	if(type=='create' || type=='edit') {
		
		var record = nlapiGetNewRecord();
		var sub = record.getFieldValue('subsidiary');
		if(sub!=7){ //added by Noeh 06/06/2018
			nlapiLogExecution('debug', 'beforeSubmit', 'xxxxx');
			
			// Whenever a sales order is saved, set the line item > Customer Part No if it is blank
			_setCustomerPartNoColumn(record);
			_setLotColumnFields(record);
			 _setCustomerPartNoColumnFields(record); // Added by AMU 11/6/2016
		}
	}
}


function _setLotColumnFields(record) 
{
	var ctx = nlapiGetContext().getExecutionContext();
	
	var lotNos = [];
	var partNoIds = [];
	
	// Two things to set:
	// (1) Order > Line > Customer Part No
	// (2) Order > Line > Price (based on lot no) 
	
	var sentError = false;
	var lines = record.getLineItemCount('item');
	for (var i = 1; i <= lines; i++) 
	{
		var itemId = record.getLineItemValue('item', 'item', i);
		var partNo = record.getLineItemValue('item', 'custcol_ftis_customerpartno', i);
		if(!isEmpty(partNo)) {
			partNoIds.push(partNo);
		}
	
		// Get the lot id either from the custom Lot Column OR the line inventory detail
		var lotId = record.getLineItemValue('item', 'custcol_nslotno', i);
		if(!isEmpty(lotId)) {
			lotNos.push(lotId);
		} 
		else 
		{
			var lotNo = null;
			record.selectLineItem('item', i);
			
			try {
				
				var subrecord = record.viewCurrentLineItemSubrecord('item', 'inventorydetail');
				if(subrecord!=null) {
					var sublines = subrecord.getLineItemCount('inventoryassignment');
					if(sublines>0) {
						subrecord.selectLineItem('inventoryassignment', 1);
						var lotNo = subrecord.getCurrentLineItemText('inventoryassignment', 'issueinventorynumber');
						var lotId = subrecord.getCurrentLineItemValue('inventoryassignment', 'issueinventorynumber');
						if(!isEmpty(lotId)) {
							record.setLineItemValue('item', 'custcol_nslotno', i, lotId);
							lotNos.push(lotNo);
						}
					}
				}
				
			} 
			catch(ex) 
			{
				if(!sentError) 
				{
					var userId = nlapiGetUser();
					
					nlapiSendEmail(userId, userId, 
						'Failed to set the lot no on order('+ record.getFieldValue('tranid') +')', 
						'Details: ' + getErrorMsg(ex), 
						null, null, null, null, true);
					
					sentError = true;
				}	
			}
		}
	}
	
	lotNos = _.uniq(lotNos);
	partNoIds = _.uniq(partNoIds);
	
	var subsidiaryId = record.getFieldValue('subsidiary');
	var ftLotCache = _getLotNoFromLotEntry(lotNos, subsidiaryId);
	var partNoCache = _getPartNumbers(partNoIds);
	
	nlapiLogExecution('debug', 'xxx', 'lotNos => ' + JSON.stringify(lotNos));
	nlapiLogExecution('debug', 'xxx', 'partNoIds => ' + JSON.stringify(partNoIds));
	nlapiLogExecution('debug', 'xxx', 'ftLotCache => ' + JSON.stringify(ftLotCache));
	nlapiLogExecution('debug', 'xxx', 'partNoCache => ' + JSON.stringify(partNoCache));

	
	// Apply item id to all lines with a part no
	for (var i = 1; i <= lines; i++)
	{
		// ---------------------------------------
		// Set the Lot related column fields
		// ---------------------------------------
		var lotNo = record.getLineItemValue('item', 'custcol_nslotno', i);
		var ftLotData = ftLotCache[lotNo];
		
		nlapiLogExecution('debug', 'zzzz ctx['+ctx+']', 'lotNo = ' + lotNo);
		nlapiLogExecution('debug', 'zzzz', 'ftLotData => ' + JSON.stringify(ftLotData));
		
		if(ftLotData) 
		{
			var ftLotId = ftLotData.id;
			var batchId = ftLotData.batchid;
			var lotSubsidiaryId = ftLotData.subsidiary;
			var lotId = ftLotData.lotid;
			var price = ftLotData.price;
			var taxcode = ftLotData.taxcode;
			var status = ftLotData.status;
			
			record.setLineItemValue('item', 'custcol_ft_batchno', i, batchId);
			record.setLineItemValue('item', 'custcol_ft_lotno', i, ftLotId);
			if(!isEmpty(price)) {
				record.setLineItemValue('item', 'price', i, '-1');
				record.setLineItemValue('item', 'rate', i, price);
			}
			
			var itemStatus = record.getLineItemValue('item', 'custcol_ebiznet_item_status', i);
			if(isEmpty(itemStatus)) {
				record.setLineItemValue('item', 'custcol_ebiznet_item_status', i, status);
			}
			
			nlapiLogExecution('debug', 'custcol_nslotno', 'lotId = '+lotId);
			nlapiLogExecution('debug', '_setLotColumnFields', 'Found ftLot => ' + JSON.stringify(ftLotData));
			
		} else {
			// throw nlapiCreateError('NO_LOT_FOUND', 'FT Lot No ['+lotNo+'] not found', true);
		}
		
	}
}




/**
 * When the sales order is saved, if there is any sales order line with blank "Customer Part No.", 
 * the script will validate the "Customer Part No." from the "Customer Item Reference" record, 
 * based on the criteria below:
 *  - Customer Item Reference.Customer Name = Sales Order.Customer 
 *  - Customer Item Reference.NS Item Name = Sales Order.Item
 *  - Where Customer Item Reference.Delievery Priority is the smallest value 
 *    among the found Customer Item Reference record which is not inactive
 *    
 *  If no Customer Item Reference record found to match the above criteria, 
 *  the sales order line "Customer Part No." will stay blank after save.
 */
function _setCustomerPartNoColumn(record) 
{
	// Tracks the Group lines
	var groupData = null;
	
	// Contains line data where a part no will be assigned if one is found
	var groupLines = [];
	
	// Contains all item IDs to be searched from the "Customer Item Reference" record.
	// Searching this custom record will give us back the Part No.
	var itemIds = [];
	
	// We will apply the logic on these item types only
	var SUPPORTED_ITEM_TYPES = {
		'InvtPart' : true,
		'Kit' : true,
'Assembly' : true, // 2016/06/06 Added by Amu
'Group' : true // 2016/06/06 Added by Amu
	};
	
	var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) 
	{
		var itemId = record.getLineItemValue('item', 'item', i);
		var itemName = record.getLineItemText('item', 'item', i);
		var itemType = record.getLineItemValue('item', 'itemtype', i);
		var partNoId = record.getLineItemValue('item', 'custcol_ftis_customerpartno', i);
		
		nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'line('+i+') item('+itemName+') itemType('+itemType+')');
		
		if(itemType=='Group') {
			groupData = { parent: itemId, line: i };
			itemIds.push(itemId);
			continue;
		}
		if(itemType=='EndGroup') {
			groupData = null;
			continue;
		}
		
		if(SUPPORTED_ITEM_TYPES[itemType]) 
		{
			nlapiLogExecution('debug', '_setCustomerPartNoColumn', ' partNoId('+partNoId+') groupData => ' + JSON.stringify(groupData));
			
			// Below means the line is not part of a group item
			if(!groupData) {
				// And the line > Part No is blank
				if(isEmpty(partNoId)) {
					itemIds.push(itemId);
				}
			}
			
			// Line could be part of an Item Group or not.. 
			var lineItemData = { 
				part: partNoId, 
				item: itemId,
				line: i
			};
			
			// If line is part of an Item Group, add the parent info
			if(groupData) {
				lineItemData.parentitem = groupData.parent;
				lineItemData.parentline = groupData.line;
			}
			groupLines.push(lineItemData);
		}
	}
	
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'grouplines => ' + JSON.stringify(groupLines));
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'itemIds => ' + JSON.stringify(itemIds));
	
	if(itemIds.length==0) {
		// Nothing to update on the lines
		return;
	}
	
	// Search for the part no given a list of item IDs
	var customerId = record.getFieldValue('entity');
	if(record.getRecordType()=='transferorder') {
		customerId = record.getFieldValue('custbody_ftis_transfertocustomer');
	}
	var partNoCache = _getCustomerPartNo(itemIds, customerId);
	if(itemIds.length != _.keys(partNoCache).length) {
		// throw nlapiCreateError('NO_PART_NO_FOUND', 'Some customer part # are not found for these items['+itemIds.join(',')+']', true);
	}
	
	
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'partNoCache => ' + JSON.stringify(partNoCache));
	
	// Set the part numbers we searched above to the SO > Line > "Customer Part No" column
	for (var i = 0; i < groupLines.length; i++) 
	{
		var child = groupLines[i];
		var parentPartNo = partNoCache[child.parentitem] || [];
		var childPartNo = partNoCache[child.item] || [];
		
		// Only set the Part No if the: 
		// (1) line item > Customer Part No is blank OR
		// (2) for item group lines, if the Part No found is the same as the Item Group > Part No
		if(childPartNo==parentPartNo || isEmpty(child.part) ) 
		{
			// Update the sales order > line item > part no
			record.setLineItemValue('item', 'custcol_ftis_customerpartno', child.line, childPartNo);
			
			nlapiLogExecution('audit', '_setCustomerPartNoColumn', 
				record.getRecordType() + '('+record.getId()+'): Updating line('+child.line+') item('+child.item+') with partno('+childPartNo+')');
		}
	}

}

/** 
 * Given a list of items and customer, search the "Customer Item Reference" custom record
 * to find the active "Customer Part No" that has the least priority for the item and customer
 * combination.
 * 
 * @param Array itemIds
 * @param number customerId 
 * @returns A cache of item and part no combination. Example, 
 * 	{
 * 		<itemid>: <customeritemreference_id>,
 *      <itemid>: <customeritemreference_id>
 * 	}
 */
function _getCustomerPartNo(itemIds, customerId) 
{
	if(itemIds.length==0) {
		return null;
	}
	
	itemIds = _.uniq(itemIds);
	
	nlapiLogExecution('debug', '_getCustomerPartNo', 'customerId('+customerId+') itemIds('+ JSON.stringify(itemIds) +')');
	
	var cache = {};
	
	var filters = [ 
        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
        new nlobjSearchFilter('custrecord_ftis_customeritemref_nsitem', null, 'anyof', itemIds),
        new nlobjSearchFilter('custrecord_ftis_customeritemref_custname', null, 'anyof', customerId)
    ];
	var columns = [
       new nlobjSearchColumn('custrecord_ftis_customeritemref_nsitem'),
       new nlobjSearchColumn('custrecord_ftis_customeritemref_delpri'),
       new nlobjSearchColumn('custrecord_ftis_customeritemref_delpri').setSort() // sorts by priority ascending
    ];
	var results = nlapiSearchRecord('customrecord_ftis_customeritemref', null, filters, columns);
	if(results!=null && results.length>0) 
	{
		for(var i=0; i<results.length; i++) 
		{
			var result = results[i];
			
			var id = result.getId();
			var itemId = result.getValue('custrecord_ftis_customeritemref_nsitem');
			
			if(cache[itemId]==null) {
				cache[itemId] = id;
			}
		}
	}
	nlapiLogExecution('debug', '_getCustomerPartNo', 'cache => ' + JSON.stringify(cache));
	return cache;
}


var ACT_SUBMIT_ORDER = '2';
var DELIMITER = ',';

function main(request, response) 
{
	var orderId = null;
	
	var action = request.getParameter('act');
	switch (action) 
	{
		case ACT_SUBMIT_ORDER: {
			orderId = _import_doSubmitOrder(request, response);
		}
	}
	
	
	var form = nlapiCreateForm('Import One Order');
	var columnFld = form.addField('rows', 'inlinehtml', 'Columns');
	columnFld.setDefaultValue('Document Number, Date, Customer Name, Item, Quantity, Location, Lot No., Shipping Method, Create Fulfilment Line, Currency, Item Rate, PO/Check Number, Memo, Business Model')
	columnFld.setLayoutType('outsideabove', 'startrow');
	
	// Text area containing the data to create the sales order
	var width = 130;
	var height = 20;
	var dataFld = form.addField('data', 'textarea', 'Paste your data here');
	dataFld.setDisplaySize(width, height);
	
	// Hidden action
	var actionFld = form.addField('act', 'text');
	actionFld.setDisplayType('hidden');
	actionFld.setDefaultValue(ACT_SUBMIT_ORDER);
	
	// Display the order when it is created
	if(!isEmpty(orderId)) {
		var orderFld = form.addField('orderid', 'select', 'Success', 'salesorder');
		orderFld.setDisplayType('inline');
		orderFld.setDefaultValue(orderId);
		form.addSubmitButton('Add another one');
	}
	else
	{
		form.addSubmitButton('Submit');
	}
	
	
	form.addResetButton();
	response.writePage(form);
}



function _import_doSubmitOrder(request, response) 
{
	var data = request.getParameter('data');
	var orderId = _import_parseAndCreateOrder(data);
	return orderId;
}

function _import_parseAndCreateOrder(data) 
{
	// Document Number , 
	// Date , 
	// Customer Name , 
	// Item , 
	// Quantity , 
	// Location , 
	// Lot No. , 
	// Shipping Method , 
	// Create Fulfilment Line , 
	// Currency , 
	// Item Rate , 
	// PO/Check Number , 
	// Memo , 
	// Business Model
	
	var record = nlapiCreateRecord('salesorder', { recordmode: 'dynamic' });
	
	// Split to rows
	var rows = data.split('\n');
	
	var itemNames = [];
	var locations = [];
	var lotNos = [];
	var currencySymbols = [];
	
	for (var i = 0; i < rows.length; i++) 
	{
		var row = rows[i];
		if(isEmpty(row)) {
			continue;
		}
		
		var cols = row.split(DELIMITER); // Split to columns
		
		var item = cols[3]; // item name is 4th column
		var location = cols[5]
		var lotno = cols[6];
		var currency = cols[9];
		
		if(!isEmpty(item)) {
			itemNames.push(item);
		}
		if(!isEmpty(location)) {
			locations.push(location);
		}
		if(!isEmpty(lotno)) {
			lotNos.push(lotno);
		}
		if(!isEmpty(currency)) {
			currencySymbols.push(currency);
		}
	}
	
	// Remove duplicates
	itemNames = _.uniq(itemNames);
	locations = _.uniq(locations);
	lotNos = _.uniq(lotNos);
	currencySymbols = _.uniq(currencySymbols);
	
	// Lookup the item, location, lotno
	var itemCache = _getItemsByName(itemNames);
	var locationCache = _getLocationsByName(locations);
	var lotCache = _getLotsByName(lotNos);
	var currencyCache = _getCurrencies(currencySymbols);
	
	nlapiLogExecution('debug', 'itemCache', JSON.stringify(itemCache));
	nlapiLogExecution('debug', 'locationCache', JSON.stringify(locationCache));
	nlapiLogExecution('debug', 'lotCache', JSON.stringify(lotCache));
	
	for (var i = 0; i < rows.length; i++) 
	{
		var row = rows[i];
		
		// Split to columns
		var cols = row.split(DELIMITER);
		
		// 1  Document Number , 
		// 2  Date , 
		// 3  Customer Name , 
		// 4  Item , 
		// 5  Quantity , 
		// 6  Location , 
		// 7  Lot No. , 
		// 8  Shipping Method , 
		// 9  Create Fulfilment Line , 
		// 10 Currency , 
		// 11 Item Rate , 
		// 12 PO/Check Number , 
		// 13 Memo , 
		// 14 Business Model
		
		var tranid = trimToBlank( cols[0] );
		var date = trimToBlank( cols[1] );
		var customer = trimToBlank( cols[2] );
		var item = trimToBlank( cols[3] );
		var qty = trimToBlank( cols[4] );
		var location = trimToBlank( cols[5] );
		var lotno = trimToBlank( cols[6] );
		var shipMethod = trimToBlank( cols[7] );
		var yesNo = trimToBlank(cols[8]).toLowerCase();
		var createFulfillmentLine = yesNo=='yes' || yesNo=='y' || yesNo=='t' || yesNo=='true' ? 'T' : 'F';
		var currency = trimToBlank( cols[9] );
		var rate = trimToBlank( cols[10] );
		var poNo = trimToBlank( cols[11] );
		var memo = trimToBlank( cols[12] );
		var businessModel = trimToBlank( cols[13] );
		
		var itemId = !isEmpty(item) ? itemCache[item] : null;
		var locationId = !isEmpty(location) ? locationCache[location] : null;
		var lotId = !isEmpty(lotno) ? lotCache[lotno] : null;
		var currencyId = !isEmpty(currency) ? currencyCache[currency] : null;
		
		if(i==0) 
		{	
			record.setFieldValue('trandate', date);
			record.setFieldValue('tranid', tranid);
			
			var customerData = _getCustomer(customer);
			record.setFieldText('entity', customer);
			record.setFieldValue('otherrefnum', poNo);
			record.setFieldValue('memo', memo);
			record.setFieldValue('currency', currencyId);
			record.setFieldText('location', location);
			record.setFieldText('custbody_ftis_businessmodel', businessModel);
			record.setFieldText('shipmethod', shipMethod);
		}
		
		record.selectNewLineItem('item');
		record.setCurrentLineItemValue('item', 'item', itemId); 
		record.setCurrentLineItemValue('item', 'quantity', qty);
		record.setCurrentLineItemValue('item', 'location', locationId);
		record.setCurrentLineItemValue('item', 'custcol_create_fulfillment_order', createFulfillmentLine);
		record.setCurrentLineItemValue('item', 'custcol_nslotno', lotId);
		
		record.commitLineItem('item');
	}
	
	var orderId = nlapiSubmitRecord(record, false, true);
	nlapiLogExecution('audit', '_import_parseAndCreateOrder', 'Created salesorder('+orderId+')');
	
	return orderId;
}

function afterSubmit(type) 
{
	if (type == 'create' || type == 'edit') 
	{
		var shouldSave = false;
		var record = loadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		var sub = record.getFieldValue('subsidiary');
		if(sub!=7){
			var lines = record.getLineItemCount('item');
			for (var i = 1; i <= lines; i++) 
			{
				var qty = record.getLineItemValue('item', 'quantity', i);
				var lotId = record.getLineItemValue('item', 'custcol_nslotno', i);
				
				if(!isEmpty(lotId))
				{
					record.selectLineItem('item', i);
					
					var subrecord = record.viewCurrentLineItemSubrecord('item', 'inventorydetail');
					if(subrecord==null) 
					{
						subrecord = record.createCurrentLineItemSubrecord('item', 'inventorydetail');
						subrecord.selectNewLineItem('inventoryassignment');
						subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotId);
						subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
						subrecord.commitLineItem('inventoryassignment');
						subrecord.commit();
						
						shouldSave = true;
					}
				}
				record.commitLineItem('item');
			}
			
			if(shouldSave) {
				nlapiSubmitRecord(record, false, true);
			}
		}
		
	}
}


function _getItemsByName(itemNames) {
	nlapiLogExecution('debug', '_getItemsByName', 'itemNames => ' + JSON.stringify(itemNames));
	var data = {};
	var filters = toSearchFiltersByName('itemid', itemNames);
	var columns = [ new nlobjSearchColumn('itemid')];
	var results = nlapiSearchRecord('item', null, filters, columns);
	if(results!=null && results.length>0) {
		for(var i=0; i<results.length; i++) {
			var result = results[i];
			var itemName = result.getValue('itemid');
			data[itemName] = result.getId();
		}
	}
	return data;
}

function _getLotsByName(lotNos) {
	nlapiLogExecution('debug', '_getLotsByName', 'lotNos => ' + JSON.stringify(lotNos));
	var data = {};
	var filters = toSearchFiltersByName('inventorynumber', lotNos);
	var columns = [ new nlobjSearchColumn('inventorynumber')];
	var results = nlapiSearchRecord('inventorynumber', null, filters, columns);
	if(results!=null && results.length>0) {
		for(var i=0; i<results.length; i++) {
			var result = results[i];
			var number = result.getValue('inventorynumber');
			data[number] = result.getId();
		}
	}
	return data;
}

function _getLocationsByName(locationNames) {
	nlapiLogExecution('debug', '_getLocationsByName', 'locationNames => ' + JSON.stringify(locationNames));
	var data = {};
	var filters = toSearchFiltersByName('name', locationNames);
	var columns = [ new nlobjSearchColumn('name')];
	var results = nlapiSearchRecord('location', null, filters, columns);
	if(results!=null && results.length>0) {
		for(var i=0; i<results.length; i++) {
			var result = results[i];
			var locationName = result.getValue('name');
			data[locationName] = result.getId();
		}
	}
	return data;
}

function _getCurrencies(currencySymbols) 
{
	var data = {};
	var filters = toSearchFiltersByName('symbol', currencySymbols);
	var columns = [ new nlobjSearchColumn('symbol')];
	var results = nlapiSearchRecord('currency', null, filters, columns);
	if(results!=null && results.length>0) {
		for(var i=0; i<results.length; i++) {
			var result = results[i];
			var symbol = result.getValue('symbol');
			data[symbol] = result.getId();
		}
	}
	return data;
}

function _getCustomer(customer) 
{
	var data = {};
	var filters = [ new nlobjSearchFilter('entityid', null, 'is', customer)];
	var columns = [ new nlobjSearchColumn('subsidiary')];
	var results = nlapiSearchRecord('customer', null, filters, columns);
	if(results!=null && results.length>0) 
	{
		for(var i=0; i<results.length; i++) 
		{
			var result = results[i];
			var customerId = result.getId();
			var subsidiaryId = result.getValue('subsidiary');
			
			data.id = customerId;
			data.subsidiary = subsidiaryId;
		}
	}
	
	var results = nlapiSearchRecord('customer', null, filters);
	if(results!=null && results.length>0) {
		return results[0].getId();
	}
	return data;
}


function _getLotNoFromLotEntry(lotNos, subsidiaryId) 
{
	nlapiLogExecution('debug', '_getLotNoFromLotEntry', 'lotNos => '+JSON.stringify(lotNos));
	
	var ftLotCache = {};
	if(lotNos.length>0) 
	{
		var filters = [ 
            new nlobjSearchFilter('custrecord_nslotno', null, 'anyof', lotNos),
            //new nlobjSearchFilter('custrecord_subsidiary', null, 'anyof', subsidiaryId)
        ];
		var columns = [ 
            new nlobjSearchColumn('custrecord_ebizlotbatch', 'custrecord_lotno'),
            new nlobjSearchColumn('custrecord_gstcate'),
            new nlobjSearchColumn('custrecord_nslotno'),
            new nlobjSearchColumn('custrecord_lotno'),
            new nlobjSearchColumn('custrecord_subsidiary'),
            new nlobjSearchColumn('custrecord_salesprice'),
            new nlobjSearchColumn('custrecord_ftlot_customclearancedate'),
            new nlobjSearchColumn('custrecord_ftlot_customclearancenum'),
            new nlobjSearchColumn('custitem_ebizoutbounddefskustatus', 'custrecord_ftlot_itemname')
        ];
		var results = nlapiSearchRecord('customrecord_ftlotentry', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				
				var id = result.getId();
				var lotId = result.getValue('custrecord_nslotno');
				var lotNo = result.getText('custrecord_nslotno');
				var batchId = result.getValue('custrecord_lotno');
				var price = result.getValue('custrecord_salesprice');
				var taxcodeId = result.getValue('custrecord_gstcate'); // taxcode
				var subsidiaryId = result.getValue('custrecord_subsidiary');
				var clearanceDate = result.getValue('custrecord_ftlot_customclearancedate');
				var clearanceNo = result.getValue('custrecord_ftlot_customclearancenum');
				var itemstatus = result.getValue('custitem_ebizoutbounddefskustatus', 'custrecord_ftlot_itemname');
				
				ftLotCache[lotId] = {
					id: id,
					batchid: batchId,
					lotid: lotId,
					lotno: lotNo,
					price: price,
					taxcode: taxcodeId,
					subsidiary: subsidiaryId,
					clearanceno: clearanceNo,
					clearancedate: clearanceDate,
					status: itemstatus
				};
			}
		}
	}
	
	nlapiLogExecution('debug', '_getLotNoFromLotEntry', 'ftLotCache => '+JSON.stringify(ftLotCache));
	return ftLotCache;
}

function _getPartNumbers(partNoIds) 
{
	nlapiLogExecution('debug', '_getPartNumbers', 'partNoIds => '+JSON.stringify(partNoIds));
	var partNoCache = {};
	if(partNoIds.length>0) 
	{
		var filters = [ 
            new nlobjSearchFilter('isinactive', null, 'is', 'F'),
            new nlobjSearchFilter('internalid', null, 'anyof', partNoIds)
        ];
		var columns = [ new nlobjSearchColumn('custrecord_ftis_customeritemref_nsitem') ];
		var results = nlapiSearchRecord('customrecord_ftis_customeritemref', null, filters, columns);
		for(var i=0; results!=null && i<results.length; i++) {
			var result = results[i];
			var id = result.getId();
			
			var itemId = result.getValue('custrecord_ftis_customeritemref_nsitem');
			partNoCache[id] = itemId;
		}
	}
	nlapiLogExecution('debug', '_getPartNumbers', 'partNoCache => '+JSON.stringify(partNoCache));
	return partNoCache;
}


//Added by AMU 11/6/2016
function _setCustomerPartNoColumnFields(record)
{
var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) {
		var cir = record.getLineItemValue('item', 'custcol_ftis_customerpartno', i);
		if (!isEmpty(cir)) {
			var cirFields= nlapiLookupField('customrecord_ftis_customeritemref', cir,  ['custrecord_ftis_customeritemref_custpart', 'custrecord_ftis_cust_rev', 'custrecord_ftis_database_location'] ); 
			var cpn = cirFields['custrecord_ftis_customeritemref_custpart'];
			if(cpn==null || cpn == undefined) {cpn='';}
			
			var custrev = cirFields['custrecord_ftis_cust_rev'];
			if(custrev==null || custrev==undefined) {custrev='';}
			
			var dblocation = cirFields['custrecord_ftis_database_location'];
			if(dblocation==null || dblocation==undefined) {dblocation='';}
			
			record.setLineItemValue('item', 'custcol_ftis_cpn', i, cpn);
			record.setLineItemValue('item', 'custcol_ftis_cust_rev', i, custrev );
			record.setLineItemValue('item', 'custcol_ftis_database_location', i, dblocation );
			
		}
	}


}