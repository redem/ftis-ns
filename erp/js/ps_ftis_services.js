/**
 * Copyright (c) 1998-2014 NetSuite, Inc. 2955 Campus Drive, Suite 100, San
 * Mateo, CA, USA 94403-2511 All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of NetSuite,
 * Inc. ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with NetSuite.
 */

var ACT_CUSTOMER_ITEM_REFERENCE_QUERY = 'custitemref';

function main(request, response) 
{
	var action = request.getParameter('act');
	switch (action) {
		case ACT_CUSTOMER_ITEM_REFERENCE_QUERY: {
			return _doCustomerItemReferenceQuery(request, response);
			
		}
	}
	response.write('Nothing done');
}



function _doCustomerItemReferenceQuery(request, response) 
{
	var reply = {
		success: false
	};
	
	var partNoId = request.getParameter('pid');
	nlapiLogExecution('debug', '_doCustomerItemReferenceQuery', 'partNoId('+partNoId+')');
	
	// Check for invalid inputs
	if(isEmpty(partNoId)) {
		reply.msg = 'Invalid request';
		response.write(JSON.stringify(reply));
		return;
	}
	
	var itemId = 
		nlapiLookupField('customrecord_ftis_customeritemref', 
			partNoId, 'custrecord_ftis_customeritemref_nsitem');
	
	// Item was found
	if(!isEmpty(itemId)) {
		reply.success = true;
		reply.item = itemId;
		response.write(JSON.stringify(reply));
		return;
	} 
	
	// If we get here, nothing found
	reply.msg = 'Nothing found';
	response.write(JSON.stringify(reply));
}