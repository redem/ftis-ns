/**
 * Copyright (c) 1998-2014 NetSuite, Inc. 2955 Campus Drive, Suite 100, San
 * Mateo, CA, USA 94403-2511 All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of NetSuite,
 * Inc. ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with NetSuite.
 */


function wf_calculate_SG_GST(type) 
{
	var record = nlapiGetNewRecord();
	
	switch(record.getRecordType()) 
	{
		case 'journalentry': {
			if(_notVoided(record)) {
				_calculateRate(record, 'line');
			}
			break;
		}
		
		case 'invoice': {
			_calculateRate(record, 'item'); 
			break;
		}
        
        case 'salesorder': {
			_calculateRate(record, 'item'); 
			break;
		}
		
		case 'vendorbill': 
		{
			var totalExpense = _calculateRate(record, 'expense');
			var totalItem = _calculateRate(record, 'item');
			if(totalItem!=null && totalExpense!=null) {
				record.setFieldValue('custbody_ftis_sgd_converted_amount', Number(totalExpense.amount) + Number(totalItem.amount));
				record.setFieldValue('custbody_ftis_sgdgstconvertedamt', Number(totalExpense.gst) + Number(totalItem.gst));
			}
			break;
		}
        
		case 'check': 
		{
			var totalExpense = _calculateRate(record, 'expense');
			var totalItem = _calculateRate(record, 'item');
			if(totalItem!=null && totalExpense!=null) {
				record.setFieldValue('custbody_ftis_sgd_converted_amount', Number(totalExpense.amount) + Number(totalItem.amount));
				record.setFieldValue('custbody_ftis_sgdgstconvertedamt', Number(totalExpense.gst) + Number(totalItem.gst));
			}
			break;
		}
        
		case 'vendorcredit': 
		{
			var totalExpense = _calculateRate(record, 'expense');
			var totalItem = _calculateRate(record, 'item');
			if(totalItem!=null && totalExpense!=null) {
				record.setFieldValue('custbody_ftis_sgd_converted_amount', Number(totalExpense.amount) + Number(totalItem.amount));
				record.setFieldValue('custbody_ftis_sgdgstconvertedamt', Number(totalExpense.gst) + Number(totalItem.gst));
			}
			break;
		}
		
		case 'creditmemo': {
			_calculateRate(record, 'item'); 
			break;
          

		}
	}
}

function _notVoided(record) {
	var isVoid = record.getFieldValue('void')=='T' ? true : false;
	return !isVoid;
}


function _calculateRate(record, tab) 
{
	// Defaults to being calculated
	var calculate = true;
	
	// If Dont Calculate is checked, then do not calculate 
	var dontCalculate = record.getFieldValue('custbody_f_gst_dont_calculate');
	if(dontCalculate=='T') {
		calculate = false;
	}
	
	nlapiLogExecution('debug', '_calculateRate', 'id('+record.getId()+') custbody_f_gst_dont_calculate - '  + record.getFieldValue('custbody_f_gst_dont_calculate'));
    nlapiLogExecution('debug', '_calculateRate', 'calculate - '  + calculate);
	if(calculate==false || calculate==null) {
		return;
	}
	
	var currencyId = record.getFieldValue('currency');
	
	// By default, use the user-entered exchange rate if not blank
	var exchangeRate = record.getFieldValue('custbody_ftis_sgd_exch_rate');;
	
	// Otherwise, do the currency conversion to SGD
	if(isEmpty(exchangeRate)) {
		// For any other currencies
		var effectiveDate = record.getFieldValue('trandate');
		var sourceCurrency = 'SGD';
		var targetCurrency = currencyId;
		exchangeRate = nlapiExchangeRate(sourceCurrency, targetCurrency, effectiveDate);
	}
	
	if(!isEmpty(exchangeRate)) 
	{
		var totalAmount = 0;
		var totalGST = 0;
		
		var lines = record.getLineItemCount(tab);
		for ( var i = 1; i <= lines; i++) 
		{
			
			// ----------------------------
			// Convert the line amount
			// ----------------------------
			var amount = record.getLineItemValue(tab, 'amount', i);
			var taxcode = record.getLineItemValue(tab, 'taxcode', i);
			var isCredit = false;
			var isDebit = false;
			// Special case for journal entries (get from debit/credit if grossamount is blank
			if(record.getRecordType()=='journalentry' && !isEmpty(taxcode)) {
				var debit = record.getLineItemValue(tab, 'debit', i);
				var credit = record.getLineItemValue(tab, 'credit', i) * -1;
				isCredit = !isEmpty(credit) ? true : false;
				isDebit = !isEmpty(debit) ? true : false;
				amount = isCredit ? Number(credit) : Number(debit);	
			}
			var amountConverted = amount / exchangeRate;
			totalAmount += parseFloat(Number(amountConverted).toFixed(2));
			
			
			// ----------------------------
			// Convert the line tax
			// ----------------------------
			var taxAmount = record.getLineItemValue(tab, 'tax1amt', i);
			if(record.getRecordType()=='journalentry') {
				taxAmount = isCredit ? Number(taxAmount) * -1 : Number(taxAmount);	
			}
			var taxAmountConverted = taxAmount / exchangeRate;
			totalGST += parseFloat(Number(taxAmountConverted).toFixed(2));
			
			if(record.getRecordType()=='journalentry' && isCredit) {
				totalGST = totalGST;
			}
			
			// For creditmemo and vendorcredit, amounts should be negative
			if(record.getRecordType()=='creditmemo' || record.getRecordType()=='vendorcredit') {
				amountConverted = amountConverted * -1;
				taxAmountConverted = taxAmountConverted * -1;
			}
			
			// ----------------------------
			// Set line calculations
			// ----------------------------
			record.setLineItemValue(tab, 'custcol_ftis_sgd_exch_rate_line', i, exchangeRate);
			record.setLineItemValue(tab, 'custcol_ftis_sgd_converted_amt_line', i, Number(amountConverted).toFixed(2));
			record.setLineItemValue(tab, 'custcol_ftis_sgdgstconvertedamt_line', i, Number(taxAmountConverted).toFixed(2));
		}
		
		// For credit memo, amounts should be negative
		if(record.getRecordType()=='creditmemo' || record.getRecordType()=='vendorcredit') {
			totalAmount = totalAmount * -1;
			totalGST = totalGST * -1;
		}
		
		nlapiLogExecution('audit', '_calculateRate', 
			'Done calculation => ' + record.getRecordType() + '='+ record.getId() +
			' totalAmount('+totalAmount+') ' + 
			' totalGST('+totalGST+')');
		
		// ----------------------------
		// Set header calculations
		// ----------------------------
		record.setFieldValue('custbody_ftis_sgd_exch_rate', exchangeRate);
		record.setFieldValue('custbody_ftis_sgd_converted_amount', Number(totalAmount).toFixed(2));
		record.setFieldValue('custbody_ftis_sgdgstconvertedamt', Number(totalGST).toFixed(2));
	}
	
	return {
		amount: totalAmount,
		gst: totalGST
	}
}


function massupdate_adhocAdjustAmounts(type, id) 
{
	var record = loadRecord(type, id);
	var doNotCalculate = record.getFieldValue('custbody_f_gst_dont_calculate')=='T' ? true : false;
	
	if(type=='creditmemo') {
		if(!doNotCalculate) {
			record.setFieldValue('custbody_f_gst_dont_calculate', 'T');
		}
		
		// Update header amounts
		var totalAmount = record.getFieldValue('custbody_ftis_sgd_converted_amount');
		var totalGST = record.getFieldValue('custbody_ftis_sgdgstconvertedamt');
		if(Number(totalAmount)>0) {
			record.setFieldValue('custbody_ftis_sgd_converted_amount', totalAmount * -1);
			record.setFieldValue('custbody_ftis_sgdgstconvertedamt', totalGST * -1);
		}
		
		// Update line amounts
		var lines = record.getLineItemCount('item');
		for ( var i = 1; i <= lines; i++) {
			var amountConverted = record.getLineItemValue('item', 'custcol_ftis_sgd_converted_amt_line', i);
			var taxAmountConverted = record.getLineItemValue('item', 'custcol_ftis_sgdgstconvertedamt_line', i);
			var shouldChange = Number(amountConverted)>0;
			if(shouldChange) {
				record.setLineItemValue('item', 'custcol_ftis_sgd_converted_amt_line', i, amountConverted * -1);
				record.setLineItemValue('item', 'custcol_ftis_sgdgstconvertedamt_line', i, taxAmountConverted * -1);
			}
			nlapiLogExecution('debug', 'massupdate_adhocAdjustAmounts', 
				'shouldChange('+shouldChange+') amountConverted('+amountConverted+')');
		}
		
		nlapiSubmitRecord(record, false, true);
		nlapiLogExecution('debug', 'massupdate_adhocAdjustAmounts', 'Updated ' + type + '('+id+')');
		
		if(!doNotCalculate) {
			nlapiSubmitField(type, id, 'custbody_f_gst_dont_calculate', 'F');
		}
	}
	
	if(type=='vendorcredit') {
		if(doNotCalculate) {
			record.setFieldValue('custbody_f_gst_dont_calculate', 'F');
		}
		nlapiSubmitRecord(record, false, true);
		if(doNotCalculate) {
			nlapiSubmitField(type, id, 'custbody_f_gst_dont_calculate', 'T');
		}
	}
	
}

