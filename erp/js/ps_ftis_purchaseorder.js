/**
 * Copyright (c) 1998-2014 NetSuite, Inc. 2955 Campus Drive, Suite 100, San
 * Mateo, CA, USA 94403-2511 All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of NetSuite,
 * Inc. ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with NetSuite.
 */

function beforeSubmit(type) 
{
	var ctx = nlapiGetContext().getExecutionContext();
	nlapiLogExecution('debug', 'beforeSubmit', 'type='+type + ' ctx='+ctx);
	
	if(type=='create' || type=='edit') {
	
		var record = nlapiGetNewRecord();
		
		// On create/edit PO, copy the tax code to a custom taxcode field (GST Category)
		var lines = record.getLineItemCount('item');
		for (var i = 1; i <= lines; i++) 
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i);
			if(itemType=='EndGroup') {
				continue;
			}
			
			var taxcodeId = record.getLineItemValue('item', 'taxcode', i);
		
			// Only set the GST Category if the tax code appears on the list of options
			if(ctx=='userinterface') 
			{
				var categoryFld = record.getLineItemField('item', 'custcol_gstcategory', i);
				if(categoryFld!=null) {
					var options = categoryFld.getSelectOptions();
					for ( var j = 0; j < options.length; j++) {
						var option = options[j];
						var categoryId = option.getId();
						if(taxcodeId==categoryId) {
							record.setLineItemValue('item', 'custcol_gstcategory', i, taxcodeId);
							break;
						}
					}
				}
			}
			else
			{
				record.setLineItemValue('item', 'custcol_gstcategory', i, taxcodeId);
			}	
		
		}
	}
}


function schedule(type) 
{
	nlapiLogExecution('debug', 'schedule', 'start');

	var counter = 0;
	
	var savedSearch = nlapiGetContext().getSetting('script', 'custscript_scpgc_savedsearch');
	if (savedSearch != null && savedSearch != '') {
		var filters = [ 
           new nlobjSearchFilter('mainline', null, 'is', 'F'),
           new nlobjSearchFilter('custcol_gstcategory', null, 'anyof', '@NONE@') 
        ];
		var columns = [ new nlobjSearchColumn('internalid', null, 'group') ];
		var results = nlapiSearchRecord('purchaseorder', savedSearch, filters, columns);
		nlapiLogExecution('audit', 'schedule', 'Found ' + (results != null ? results.length : 0) + ' records.');

		if (results != null && results.length > 0) 
		{
			var minimumUsage = 40;

			for ( var i = 0; i < results.length; i++) 
			{
				var usageLeft = nlapiGetContext().getRemainingUsage();
				if (usageLeft < minimumUsage) {
					nlapiLogExecution('audit', 'schedule', 'Not enough usage left(' + usageLeft + ') .. exiting.');
					break;
				}

				var result = results[i];
				var recordId = result.getValue('internalid', null, 'group');

				try 
				{
					// Edit and save only - rely on beforeSubmit() to set the GST category
					var record = nlapiLoadRecord('purchaseorder', recordId);
					nlapiSubmitRecord(record, false, true);
					
					nlapiLogExecution('audit', 'schedule', 'Processed  PO(' + recordId + ').');
					counter++;
					
				} catch (ex) {
					nlapiLogExecution('error', 'schedule',
						'Error processing PO(' + recordId + '). Reason: ' + getErrorMsg(ex));
				}
			}
		}
	}

	nlapiLogExecution('debug', 'schedule', 'Done. Processed ' + counter + ' records.');

}
