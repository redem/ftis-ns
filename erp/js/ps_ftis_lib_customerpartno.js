

// WARNING: This library file is used the following files.
// 	ps_ftis_salesorder.js
// 	ps_ftis_transferorder.js


/**
 * When the sales order is saved, if there is any sales order line with blank "Customer Part No.", 
 * the script will validate the "Customer Part No." from the "Customer Item Reference" record, 
 * based on the criteria below:
 *  - Customer Item Reference.Customer Name = Sales Order.Customer 
 *  - Customer Item Reference.NS Item Name = Sales Order.Item
 *  - Where Customer Item Reference.Delievery Priority is the smallest value 
 *    among the found Customer Item Reference record which is not inactive
 *    
 *  If no Customer Item Reference record found to match the above criteria, 
 *  the sales order line "Customer Part No." will stay blank after save.
 */
function _setCustomerPartNoColumn(record) 
{
	// Tracks the Group lines
	var groupData = null;
	
	// Contains line data where a part no will be assigned if one is found
	var groupLines = [];
	
	// Contains all item IDs to be searched from the "Customer Item Reference" record.
	// Searching this custom record will give us back the Part No.
	var itemIds = [];
	
	// We will apply the logic on these item types only
	var SUPPORTED_ITEM_TYPES = {
		'InvtPart' : true,
		'Kit' : true
	};
	
	var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) 
	{
		var itemId = record.getLineItemValue('item', 'item', i);
		var itemName = record.getLineItemText('item', 'item', i);
		var itemType = record.getLineItemValue('item', 'itemtype', i);
		
		var partNoFld = record.getRecordType()=='transferorder' ? 
			'custcol_custompartnoto' : 
				'custcol_ftis_customerpartno';
		
		var partNoId = record.getLineItemValue('item', partNoFld, i);
		
		nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'line('+i+') item('+itemName+') itemType('+itemType+')');
		
		if(itemType=='Group') {
			groupData = { parent: itemId, line: i };
			itemIds.push(itemId);
			continue;
		}
		if(itemType=='EndGroup') {
			groupData = null;
			continue;
		}
		
		if(SUPPORTED_ITEM_TYPES[itemType]) 
		{
			nlapiLogExecution('debug', '_setCustomerPartNoColumn', ' partNoId('+partNoId+') groupData => ' + JSON.stringify(groupData));
			
			// Below means the line is not part of a group item
			if(!groupData) {
				// And the line > Part No is blank
				if(isEmpty(partNoId)) {
					itemIds.push(itemId);
				}
			}
			
			// Line could be part of an Item Group or not.. 
			var lineItemData = { 
				part: partNoId, 
				item: itemId,
				line: i
			};
			
			// If line is part of an Item Group, add the parent info
			if(groupData) {
				lineItemData.parentitem = groupData.parent;
				lineItemData.parentline = groupData.line;
			}
			groupLines.push(lineItemData);
		}
	}
	
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'grouplines => ' + JSON.stringify(groupLines));
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'itemIds => ' + JSON.stringify(itemIds));
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'recordtype => ' + record.getRecordType());
	
	if(itemIds.length==0) {
		// Nothing to update on the lines
		return;
	}
	
	// Search for the part no given a list of item IDs
	var customerId = record.getFieldValue('entity');
	if(record.getRecordType()=='transferorder') {
		customerId = record.getFieldValue('custbody_ftis_transfertocustomer');
	}
	var partNoCache = _getCustomerPartNo(itemIds, customerId);
	if(partNoCache==null) {
		return;
	}
	
	if(itemIds.length != _.keys(partNoCache).length) {
		// throw nlapiCreateError('NO_PART_NO_FOUND', 'Some customer part # are not found for these items['+itemIds.join(',')+']', true);
	}
	
	
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'partNoCache => ' + JSON.stringify(partNoCache));
	nlapiLogExecution('debug', '_setCustomerPartNoColumn', 'groupLines => ' + JSON.stringify(groupLines));
	
	// Set the part numbers we searched above to the SO > Line > "Customer Part No" column
	for (var i = 0; i < groupLines.length; i++) 
	{
		var child = groupLines[i];
		var parentPartNo = partNoCache[child.parentitem] || [];
		var childPartNo = partNoCache[child.item] || [];
		
		// Only set the Part No if the:
		// (1) line item > Customer Part No is blank OR
		// (2) for item group lines, if the Part No found is the same as the Item Group > Part No
		if(childPartNo==parentPartNo || isEmpty(child.part) ) 
		{
			// Update the sales order or TO > line item > part no
			record.setLineItemValue('item', partNoFld, child.line, childPartNo);
			
			nlapiLogExecution('audit', '_setCustomerPartNoColumn', 
				record.getRecordType() + '('+record.getId()+'): Updating line('+child.line+') item('+child.item+') with partno('+childPartNo+')');
		}
	}

}


/** 
 * Given a list of items and customer, search the "Customer Item Reference" custom record
 * to find the active "Customer Part No" that has the least priority for the item and customer
 * combination.
 * 
 * @param Array itemIds
 * @param number customerId 
 * @returns A cache of item and part no combination. Example, 
 * 	{
 * 		<itemid>: <customeritemreference_id>,
 *      <itemid>: <customeritemreference_id>
 * 	}
 */
function _getCustomerPartNo(itemIds, customerId) 
{
	if(itemIds.length==0 || isEmpty(customerId)) {
		return null;
	}
	
	itemIds = _.uniq(itemIds);
	
	nlapiLogExecution('debug', '_getCustomerPartNo', 'customerId('+customerId+') itemIds('+ JSON.stringify(itemIds) +')');
	
	var cache = {};
	
	var filters = [ 
        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
        new nlobjSearchFilter('custrecord_ftis_customeritemref_nsitem', null, 'anyof', itemIds),
        new nlobjSearchFilter('custrecord_ftis_customeritemref_custname', null, 'anyof', customerId)
    ];
	var columns = [
       new nlobjSearchColumn('custrecord_ftis_customeritemref_nsitem'),
       new nlobjSearchColumn('custrecord_ftis_customeritemref_delpri'),
       new nlobjSearchColumn('custrecord_ftis_customeritemref_delpri').setSort() // sorts by priority ascending
    ];
	var results = nlapiSearchRecord('customrecord_ftis_customeritemref', null, filters, columns);
	if(results!=null && results.length>0) 
	{
		for(var i=0; i<results.length; i++) 
		{
			var result = results[i];
			
			var id = result.getId();
			var itemId = result.getValue('custrecord_ftis_customeritemref_nsitem');
			
			if(cache[itemId]==null) {
				cache[itemId] = id;
			}
		}
	}
	nlapiLogExecution('debug', '_getCustomerPartNo', 'cache => ' + JSON.stringify(cache));
	return cache;
}