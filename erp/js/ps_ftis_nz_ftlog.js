


function wf_generateInvoice() 
{
	var record = nlapiGetNewRecord();
	
	// Get default items to put on NZ invoice
	var itemGST = nlapiGetContext().getSetting('script', 'custscript_wfgifl_item_gst');
	var itemDuty = nlapiGetContext().getSetting('script', 'custscript_wfgifl_item_duty');
	var itemGeneric = nlapiGetContext().getSetting('script', 'custscript_wfgifl_item_generic');
	var defaultLocation = nlapiGetContext().getSetting('script', 'custscript_wfgifl_location');
	var defaultCurrency = nlapiGetContext().getSetting('script', 'custscript_wfgifl_currency');
	
	// Get the default tax codes
	var taxcodeGST = nlapiGetContext().getSetting('script', 'custscript_wfgifl_taxcode_gst');
	var taxcodeDuty = nlapiGetContext().getSetting('script', 'custscript_wfgifl_taxcode_duty'); // NOT USED
	
	// Convert the SG Invoice amount to NZD
	// - use the SG Invoice > Date
	// - use the SG Invoice total amount
	var totalAmount = record.getFieldValue('total');
	var effectiveDate = record.getFieldValue('trandate');
	var sourceCurrency = 'USD';
	var targetCurrency = 'NZD';
	var exchangeRate = nlapiExchangeRate(sourceCurrency, targetCurrency, effectiveDate);
	var nzTotalAmount = totalAmount * exchangeRate;
	// nlapiLogExecution('debug', 'zzzzz', 'exchangeRate('+exchangeRate+') totalAmount('+totalAmount+') nzTotalAmount('+nzTotalAmount+')');
	
	// Create the actual FTLog Invoice
	var customForm = nlapiGetContext().getSetting('script', 'custscript_wfgifl_customform');
	var customerId = record.getFieldValue('entity');
	var customerData = nlapiLookupField('customer', customerId, ['parent', 'taxitem']);
	var parentCustomerData = nlapiLookupField('customer', customerData['parent'], ['taxitem']);
	
	var customerTaxCode = parentCustomerData['taxitem'];
	
	// Get the Tax Code > Rates
	var taxcodeRates = _getTaxCodeRates([taxcodeGST, customerTaxCode]);
	var taxcodeGSTRate = taxcodeRates[taxcodeGST];
	var customerTaxRate = taxcodeRates[customerTaxCode];
	
	var invoiceData = {
		lines : []
	};
	
		
	// Add the GST line (1st line)
	invoiceData.lines.push({
		item: itemGST,
		description: 'GST PAID ON YOUR BEHALF',
		rate: 0,
		taxcode: taxcodeGST,
		taxrate: Number(nzTotalAmount * taxcodeGSTRate).toFixed(2),
		isgstline: true
	});
	
	// Add the DUTY line (2nd line)
	invoiceData.lines.push({
		item: itemDuty,
		description: 'DUTY PAID ON YOUR BEHALF',
		taxcode: customerTaxCode,
		taxrate: customerTaxRate,
		rate: 0,
		isdutyline: true
	});
	
	
	// Get the fulfillment data from Invoice > Created From (SO) > Item Fulfilment
	var fulfillData = {};
	var salesOrderId = record.getFieldValue('createdfrom');
	var salesOrder = loadRecord('salesorder', salesOrderId);
	if(salesOrder!=null) 
	{
		var filters = [ 
            new nlobjSearchFilter('createdfrom', null, 'anyof', salesOrderId),
            new nlobjSearchFilter('mainline', null, 'is', 'F')
        ];
		var columns = [ 
            new nlobjSearchColumn('createdfrom'),
            new nlobjSearchColumn('tranid'),
            new nlobjSearchColumn('item')
        ];
		var results = nlapiSearchRecord('itemfulfillment', null, filters, columns);
		if(results!=null && results.length>0) 
		{
			for(var i=0; i<results.length; i++) 
			{
				var result = results[i];
				var id = result.getId();
				var orderId = result.getValue('createdfrom');
				var tranId = result.getValue('tranid');
				var itemId = result.getValue('item');
				
				var key = orderId + '_' + itemId;
				fulfillData[key] = {
					id: id,
					tranid: tranId
				};
			}
		}
	}
	
	// nlapiLogExecution('debug', 'xxxx', 'taxcode rates => ' + JSON.stringify(taxcodeRates));
	// nlapiLogExecution('debug', 'xxxx', 'fulfillData => ' + JSON.stringify(fulfillData));
	
	var dutyRateTotal = 0;
	
	// Get the number of lines on SG invoice
	var lines = record.getLineItemCount('item');
	for ( var i = 1; i <= lines; i++) 
	{
		var itemId = record.getLineItemValue('item', 'item', i);
	
		// Get duty rate from item
		var dutyRatePct = record.getLineItemValue('item', 'custcol_f_item_duty_rate', i);
		dutyRatePct = isEmpty(dutyRatePct) ? '0.00%' : dutyRatePct;
		
		// All duty rates greater than zero can be added to NZ invoice
		var dutyRate = Number(parseFloat(dutyRatePct)).toFixed(2);
		if(dutyRate>0) 
		{
			// Get the invoice amount
			var lineAmount = record.getLineItemValue('item', 'amount', i);
			var lineAmountNZ = Number( (lineAmount * exchangeRate * dutyRate) / 100 ).toFixed(2);
			
			var key = salesOrderId + '_' + itemId;
			var fulfillment = fulfillData[key] || {};
			var fulfillNo = fulfillment.tranid || '';
			
			var lineNo = padNo(i, '000');
			invoiceData.lines.push({
				item: itemGeneric,
				description: lineNo + ' @ ' + dutyRate + ' NZ$ '+lineAmountNZ+'  IF# ' + fulfillNo,
				type: 'description'
			});
			
			dutyRateTotal+= Number(lineAmountNZ);
		}
	}
	
	// Set the duty line rate
	for ( var i = 0; i < invoiceData.lines.length; i++) {
		var lineData = invoiceData.lines[i];
		if(lineData.isdutyline) {
			lineData.rate = dutyRateTotal;
		}
	}
	
	nlapiLogExecution('debug', 'xxxx', 'invoiceData => ' + JSON.stringify(invoiceData));
	
	var ftisInvoiceId = record.getFieldValue('custbody_ftlog_invoice');
	var hasExistingInvoice = !isEmpty(ftisInvoiceId);
	
	nlapiLogExecution('debug', 'wf_generateInvoice', 
		'hasExistingInvoice('+hasExistingInvoice+') ftisInvoiceId('+ftisInvoiceId+')');
	
	// If has FTIS invoice already, load it else create a new one
	var invoice = hasExistingInvoice ? 
		loadRecord('invoice', ftisInvoiceId) : 
			nlapiCreateRecord('invoice');
	
	// If has FTIS invoice already, remove all lines
	if(hasExistingInvoice) {
		var lines = invoice.getLineItemCount('item');
		for(var i = lines; i > 0; i--){
	        invoice.removeLineItem('item', i);
	    }
	}
	
	// Add header info
	invoice.setFieldValue('customform', customForm);
	invoice.setFieldValue('entity', customerData['parent']);
	invoice.setFieldValue('trandate', record.getFieldValue('trandate'));
	invoice.setFieldValue('saleseffectivedate', record.getFieldValue('trandate'));
	invoice.setFieldValue('custbody_ftis_usd_inv_total', record.getFieldValue('total'));
	invoice.setFieldValue('custbody_ftis_nz_exch_rate', exchangeRate);
	invoice.setFieldValue('custbody_ftis_nzd_inv_total', nzTotalAmount);
	invoice.setFieldValue('location', defaultLocation);
	invoice.setFieldValue('currency', defaultCurrency);
	
	invoice.setFieldValue('custbody_ftis_nz_exch_rate', exchangeRate);
	invoice.setFieldValue('custbody_ftis_invoice', record.getId());
	invoice.setFieldValue('custbody_ftis_nz_exch_rate', exchangeRate);
	
	// Add line info
	for ( var i = 0; i < invoiceData.lines.length; i++) 
	{
		var lineData = invoiceData.lines[i];
		nlapiLogExecution('debug', 'xxxx', 'line => ' + JSON.stringify(lineData));
		
		if(lineData.type!='description') 
		{
			invoice.selectNewLineItem('item');
			invoice.setCurrentLineItemValue('item', 'item', lineData.item); 
			invoice.setCurrentLineItemValue('item', 'description', lineData.description);
			invoice.setCurrentLineItemValue('item', 'quantity', '1');
			invoice.setCurrentLineItemValue('item', 'price', '-1');
			invoice.setCurrentLineItemValue('item', 'rate', lineData.rate);
			invoice.setCurrentLineItemValue('item', 'amount', lineData.rate);
			invoice.setCurrentLineItemValue('item', 'taxcode', lineData.taxcode);
			if(lineData.isgstline) {
				invoice.setCurrentLineItemValue('item', 'tax1amt', lineData.taxrate);
			}
			
			invoice.commitLineItem('item');   
		} 
		else 
		{
			// Description lines
			invoice.selectNewLineItem('item');
			invoice.setCurrentLineItemValue('item', 'item', lineData.item); 
			invoice.setCurrentLineItemValue('item', 'description', lineData.description);
			invoice.commitLineItem('item');
		}
	}
	
	ftisInvoiceId = nlapiSubmitRecord(invoice, false, true);
	nlapiLogExecution('audit', 'wf_generateInvoice', 'FTLog Invoice: ' + ftisInvoiceId);
	
	return ftisInvoiceId;
}

function _getTaxCodeRates(ids) 
{
	var rates = {};
	
	var filters = [ new nlobjSearchFilter('internalid', null, 'anyof', ids)];
	var columns = [ new nlobjSearchColumn('rate')];
	var results = nlapiSearchRecord('salestaxitem', null, filters, columns);
	if(results!=null && results.length>0) 
	{
		for(var i=0; i<results.length; i++) 
		{
			var result = results[i];
			var id = result.getId();
			var ratePct = result.getValue('rate');
			var rate = parseFloat(ratePct) / 100;
			
			rates[id] = rate;
		}
	}
	
	return rates;
}

function main_test(request, response) {
	wf_generateInvoice();
}



function padNo(text, pad) {
	text = text + '';
	return pad.substring(0, pad.length - text.length) + text;
}