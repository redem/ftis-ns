
/** User event script to show the Print button */
function beforeLoad(type, form, request) 
{
	var ctx = nlapiGetContext().getExecutionContext();
	try{
		if(type=='view' && ctx=='userinterface') 
		{
			var subs = nlapiGetFieldValue('subsidiary');
			if(subs != '7'){
				var record = nlapiGetNewRecord();
				var recordId = record.getId();
				var recordType = record.getRecordType();
				
				var savedSearchIds = [];
				var templates = {};
				var filters = [ 
		            new nlobjSearchFilter('custrecord_cpo_recordtype', null, 'anyof', recordType),
		            new nlobjSearchFilter('isinactive', null, 'is', 'F')
		        ];
				var columns = [ 
				    new nlobjSearchColumn('custrecord_cpo_recordtype'), 
		            new nlobjSearchColumn('custrecord_cpo_buton_label'), 
		            new nlobjSearchColumn('custrecord_cpo_button_savedsearch'), 
		            new nlobjSearchColumn('custrecord_cpo_template'), 
		            new nlobjSearchColumn('custrecord_cpo_savedsearch_1'), 
		            new nlobjSearchColumn('custrecord_cpo_savedsearch_2') 
		        ];
				var results = nlapiSearchRecord('customrecord_f_custom_printout', null, filters, columns);
				if(results!=null && results.length>0) 
				{
					for(var i=0; i<results.length; i++) 
					{
						var result = results[i];
						
						var type = result.getValue('custrecord_cpo_recordtype');
						var btnLabel = result.getValue('custrecord_cpo_buton_label');
						var btnSearch = result.getValue('custrecord_cpo_button_savedsearch');
						var template = result.getValue('custrecord_cpo_template');
						var tplSearch_1 = result.getValue('custrecord_cpo_savedsearch_1');
						var tplSearch_2 = result.getValue('custrecord_cpo_savedsearch_2');
						
						if(!isEmpty(btnSearch) && !isEmpty(template)) 
						{
							templates[btnSearch] = {
								button: btnLabel,
								template: template,
								search1: tplSearch_1,
								search2: tplSearch_2
							};
							
							savedSearchIds.push(btnSearch);
						}
						
					}
					
					for (var i = 0; i < savedSearchIds.length; i++) 
					{
						try 
						{
							var id = savedSearchIds[i];
							var filters = [ new nlobjSearchFilter('internalid', null, 'anyof', recordId)];
							var results = nlapiSearchRecord(recordType, id, filters);
							if(results!=null && results.length>0) 
							{
								// Show the button
								var templateData = templates[id];
								if(templateData!=null) 
								{
									var buttonName = templateData.button;
									var templateFileId = templateData.template;
									var savedSearch1 = templateData.search1 || '';
									var savedSearch2 = templateData.search2 || '';
									
									var printURL = nlapiResolveURL('suitelet', 'customscript_sl_customprint', 'customdeploy_sl_customprint');
									printURL+= '&tpl='+templateFileId + '&rectype=' + recordType + '&recid=' + recordId +
												'&search1='+savedSearch1+'&search2='+savedSearch2;
									
									buttonName = buttonName || 'Custom Print';
									
									form.addButton('custpage_printbtn_'+ i, buttonName, "window.open('"+printURL+"')");
								}
							}
							
						
						} catch (ex) {
							nlapiLogExecution('error', 'beforeLoad', 'Failed on search('+id+'): ' + getErrorMsg(ex));
						}
					}
				}				
			}
		}
	}catch(ex){
		nlapiLogExecution('ERROR', 'beforeLoad ex', ex);
	}
	
	
}


/** Given record info and a PDF template file, generate a PDF */
function main(request, response) 
{
	// Get some parameters needed to generate PDF
	var recordType = request.getParameter('rectype');
	var recordId = request.getParameter('recid');
	var pdfTemplateId = request.getParameter('tpl');
	var savedSearch1 = request.getParameter('search1');
	var savedSearch2 = request.getParameter('search2');
	
	// Check if there is a template id given
	if(isEmpty(pdfTemplateId)) {
		response.write('PDF Template is not specified');
		return;
	}
	
	// Load NetSuite data
	var record = nlapiLoadRecord(recordType, recordId);

	
	// Load some data from saved search results
	var results_1 = executeSavedSearch(savedSearch1);
	var results_2 = executeSavedSearch(savedSearch2);
	
	// Read the template
	var template = nlapiLoadFile(pdfTemplateId).getValue();
	
	// Replace tags on the template
	var renderer = nlapiCreateTemplateRenderer();
	renderer.setTemplate(template);
	renderer.addRecord('record', record);
	
	if(recordType == "itemfulfillment" && pdfTemplateId == '19052'){
		var ifs = nlapiSearchRecord("itemfulfillment",null,
				[
				   ["internalid","anyof",recordId], 
				   "AND", 
				   ["cogs","is","F"]
				], 
				[
				   new nlobjSearchColumn("item",null,null), 
				   new nlobjSearchColumn("custcol_ftis_item_description",null,null),
				   new nlobjSearchColumn("formulatext",null,null).setFormula("{item.custitem_ftis_item_description}"),
				   new nlobjSearchColumn("custcol_ftis_cust_po",null,null), 
				   new nlobjSearchColumn("custcol_ftis_cpn",null,null), 
				   new nlobjSearchColumn("custcol_ftis_cust_rev",null,null), 
				   new nlobjSearchColumn("custcol_country_of_origin",null,null), 
				   new nlobjSearchColumn("custcol_ftis_category",null,null), 
				   new nlobjSearchColumn("inventorynumber","inventoryDetail",null), 
				   new nlobjSearchColumn("quantity","inventoryDetail",null),
				   new nlobjSearchColumn("custcol_ftis_carton_no",null,null),
				   new nlobjSearchColumn("custcol_ftis_gross_weight",null,null),
				   new nlobjSearchColumn("custcol_ftis_pallet_no",null,null),
				   new nlobjSearchColumn("custcol_ftis_pallet_weight",null,null),
				   new nlobjSearchColumn("unit",null,null)
				]
				);
		
		if(ifs){						
			renderer.addSearchResults('results_3', ifs);
		}
	}
	
	
	if(results_1) {
		renderer.addSearchResults('results', results_1);
	}
	if(results_2) {
		renderer.addSearchResults('results_2', results_2);
	}
	var xml = renderer.renderToString();
	
	// Convert the template to PDF and send as response
	var file = nlapiXMLToPDF(xml);
	response.setContentType('PDF', recordType+'_'+recordId+'.pdf', 'inline');
	response.write(file.getValue());
}


function executeSavedSearch(savedSearchId) {
	if(isEmpty(savedSearchId)) {
		return null;
	}
	
	nlapiLogExecution('debug', 'executeSavedSearch', 'savedSearchId('+savedSearchId+')');
	var search = nlapiLoadSearch(null, savedSearchId);
	var searchType = search.getSearchType();
	var results = nlapiSearchRecord(searchType, savedSearchId);
	nlapiLogExecution('debug', 'executeSavedSearch', 'Found '+(results?results.length:0)+' results');
	
	return results;
}

