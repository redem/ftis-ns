/**
 * Copyright (c) 1998-2014 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */


function beforeSubmit(type) 
{
	var record = nlapiGetNewRecord();
	
	if(type=='create' || type=='edit') {
		_checkUniquePriority(record);
	}
}


function _checkUniquePriority(record) 
{
	var id = record.getId();
	var customerId = record.getFieldValue('custrecord_ftis_customeritemref_custname');
	var customerName = record.getFieldText('custrecord_ftis_customeritemref_custname');
	var partId = record.getFieldValue('custrecord_ftis_customeritemref_custpart');
	var priority = record.getFieldValue('custrecord_ftis_customeritemref_delpri');
	
	var found = false;
	if(!isEmpty(customerId) && !isEmpty(partId) && !isEmpty(priority)) 
	{
		var filters = [ 
           new nlobjSearchFilter('custrecord_ftis_customeritemref_custname', null, 'anyof', customerId),
           new nlobjSearchFilter('custrecord_ftis_customeritemref_custpart', null, 'is', partId),
           new nlobjSearchFilter('custrecord_ftis_customeritemref_delpri', null, 'equalto', priority) 
        ];
		if(!isEmpty(id)) {
			filters.push( new nlobjSearchFilter('internalid', null, 'noneof', id) );
		}
   		var results = nlapiSearchRecord('customrecord_ftis_customeritemref', null, filters);
   		if(results!=null && results.length>0) {
   			found = true;
   		}
	}
	if(found) {
		throw nlapiCreateError('INVALID_PRIORITY', 
			'Delivery priority('+priority+') already exists for '+customerName+' and ' + partId, true);
	}
}