/**
 * Copyright (c) 1998-2014 NetSuite, Inc. 2955 Campus Drive, Suite 100, San
 * Mateo, CA, USA 94403-2511 All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of NetSuite,
 * Inc. ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with NetSuite.
 */


function fieldChanged(type, name, line) 
{
	if(type=='item') 
	{
		if(name=='custcol_ftis_customerpartno' || name=='custcol_custompartnoto') 
		{
			var partNoId = nlapiGetCurrentLineItemValue(type, name);
			if(!isEmpty(partNoId)) 
			{
				// Lookup the item belonging to this partNo
				var suiteletURL = nlapiResolveURL('suitelet', 
					'customscript_f_sl_common_services', 
					'customdeploy_f_sl_common_services');
			
				suiteletURL += 
					'&pid='+ partNoId +
					'&act=custitemref';
				
				var jsonReply = nlapiRequestURL(suiteletURL).getBody();
				var json = JSON.parse(jsonReply);
				console.log(jsonReply);
				
				if(!json.success) {
					alert('Failed to find item for part no');
					return;
				}
				
				// If we get here, we found the item. Put the item on the line item.
				nlapiSetCurrentLineItemValue(type, 'item', json.item, false);
			}
		}
	}
}


