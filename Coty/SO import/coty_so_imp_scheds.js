/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(
		[ 'N/file', 'N/task', 'N/http', 'N/https', 'N/record',
				'N/runtime', 'N/search' ],
		/**
		 * @param {file}
		 *            file
		 * @param {task}
		 *            task
		 * @param {http}
		 *            http
		 * @param {https}
		 *            https
		 * @param {record}
		 *            record
		 * @param {runtime}
		 *            runtime
		 * @param {search}
		 *            search
		 */
		function(file, task, http, https, record, runtime, search) {
			//161
			
			/**
			 * Definition of the Scheduled script trigger point.
			 * 
			 * @param {Object}
			 *            scriptContext
			 * @param {string}
			 *            scriptContext.type - The context in which the script
			 *            is executed. It is one of the values from the
			 * @Since 2015.2
			 */
			function execute(scriptContext){
			     var f = runtime.getCurrentScript().getParameter("custscript_c_file");
			    
				if(scriptContext.type == 'ondemand'){

					var sessionObj = runtime.getCurrentSession();
					
					nyapiLog('sessionObj',sessionObj);
					nyapiLog('runtime',runtime);
					var sessionObj = runtime.getCurrentSession();
					
					
				}
				
				var fileObj = file.create({
				    name    : 'test.txt',
				    fileType: file.Type.CSV,
				    contents: f
				    });
				nyapiLog('fileObj',fileObj);
				nyapiLog('f',f);
				
				
				
			
				 var mrTask =task.create({taskType: task.TaskType.CSV_IMPORT});
				 
				 mrTask.importFile = f;
				 mrTask.mappingId = 13;
				
				var csvImportTaskId = mrTask.submit();
				
				var csvTaskStatus = task.checkStatus({
				    taskId: csvImportTaskId
				    });
				
				nyapiLog('csvTaskStatus',csvTaskStatus);
				
			
			}
			
			
			
			function nyapiLog(title_log, details_log) {
				log.error({
					title : title_log,
					details : details_log
				});
			}

			return {
				execute : execute
			};

		});
