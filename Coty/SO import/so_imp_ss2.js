var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE,FORM;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/task'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {task} task
 */
function(file, http, record, render, runtime, search, serverWidget, url, task) {
    RT = runtime;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var params = this.request.parameters;
			
			var form = serverWidget.createForm({
			    title : 'Coty Sales Order Import'
			});
		
			var tcust = form.addField({
				id: 'custpage_import',
				label:'Import CSV',
				type:'file'
			});	
			

			
			form.addSubmitButton({
			    label : 'Submit Button'
			});
			
			this.response.writePage(form);
		};
		
		
		this.POST = function(){
			var params = this.request;
			
	
			var f = params.files["custpage_import"];
		
			/** var mrTask =task.create({taskType: task.TaskType.CSV_IMPORT});
			 
			 mrTask.importFile = f;
			 mrTask.mappingId = 13;
			
			var csvImportTaskId = mrTask.submit();
			
			var csvTaskStatus = task.checkStatus({
			    taskId: csvImportTaskId
			    });
			**/
		
			nyapiLog('f',f);
			
			var fdata = f.getContents();
			nyapiLog('fdata',fdata);
			
			var s = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			s.scriptId = 7;
			s.deploymentId = 'customdeploy_coty_task_sched';
			s.params = {'custscript_c_file': fdata};
			var scriptTaskId = s.submit();
			
			var res = task.checkStatus(scriptTaskId);
			
			this.response.write(JSON.stringify(res));
		
		};
		
    }

    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
	    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
    return {
        onRequest: setGo
    };
    
});