/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(
		[ 'N/runtime', 'N/search', 'N/error', 'N/record' ],
		/**
		 * @param {runtime} runtime
		 * @param {search} search
		 * @param {error} error
		 * @param {record} record
		 */
		function(runtime, search, error, record) {

			/**
			 * Function definition to be triggered before record is loaded.
			 *
			 * @param {Object} scriptContext
			 * @param {Record} scriptContext.newRecord - New record
			 * @param {string} scriptContext.type - Trigger type
			 * @param {Form} scriptContext.form - Current form
			 * @Since 2015.2
			 */
			function beforeLoad(scriptContext) {
				try{
					var newRec = scriptContext.newRecord;
					var form =scriptContext.form ;
					
					var sub = newRec.getValue('subsidiary');
					if(scriptContext.type == 'view'){
						if(sub == '3'){
							//form.clientScriptFileId = '29167';//sandbox 29167  29167
							
							form.addButton({
			       				id : 'custpage_pinvoice',
			       				label : 'Print Invoice',
			       				functionName : 'pinvoice'
			       			});      
						}
					}
				}catch(exx){
					nyapiLog('bf error so',exx);
				}
				
				
			}

			/**
			 * Function definition to be triggered before record is loaded.
			 *
			 * @param {Object} scriptContext
			 * @param {Record} scriptContext.newRecord - New record
			 * @param {Record} scriptContext.oldRecord - Old record
			 * @param {string} scriptContext.type - Trigger type
			 * @Since 2015.2
			 */
			function beforeSubmit(scriptContext) {
				try {
				} catch (ex) {
					nyapiLog('bfs error so',ex);
				}

			}

			/**
			 * Function definition to be triggered before record is loaded.
			 *
			 * @param {Object} scriptContext
			 * @param {Record} scriptContext.newRecord - New record
			 * @param {Record} scriptContext.oldRecord - Old record
			 * @param {string} scriptContext.type - Trigger type
			 * @Since 2015.2
			 */
			function afterSubmit(scriptContext) {
				try{
					
				}catch(exx){
					nyapiLog('aft error so',exx);
				}
				
			}

			function nyapiLog(title_log, details_log) {
				log.debug({
					title : title_log,
					details : details_log
				});
				log.error({
					title : title_log,
					details : details_log
				});
			}

			return {
				beforeLoad:beforeLoad,
				afterSubmit : afterSubmit
			};

		});
