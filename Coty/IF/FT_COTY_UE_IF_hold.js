/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime','N/record','N/search', 'N/url','N/encode','SuiteScripts/Internal/date.js'],
/**
 * @param {runtime} runtime
 * @param {record} record
 * @param {search} search
 * @param {url} url
 * @param {encode} encode
 */
function(runtime, record, search, url, encode) {  
 
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	
    	try{
	       

		}catch(exx){
			nyapiLog('error beofreload po',exx);
		}
       
    }
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	try{
    	
    	}catch(exx){
    		nyapiLog('error before submit',exx);
    	}
		
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	try{
    		if(scriptContext.type != 'delete'){
    			var inrec = scriptContext.newRecord;
        		var subs= inrec.getValue('subsidiary');
        		var idd = inrec.id;
        		if(subs == '7' && idd){
        			var nrec = record.load({
        			    type: record.Type.ITEM_FULFILLMENT, 
        			    id: idd
        			});
            		var status= nrec.getValue('status');
            		nyapiLog('subs', subs);
            		nyapiLog('status', status);
            		
        			if(status == 'Shipped'){
        				var cfrom = nrec.getValue('createdfrom');
        				var rec = record.transform({
        				    fromType: record.Type.SALES_ORDER,
        				    fromId: cfrom,
        				    toType: record.Type.INVOICE,
        				    isDynamic: true,
        				});

        				var shipdate = objRecord.getValue({
        				    fieldId: 'custbody_coty_shipdate'
        				});
        				rec.setValue({
        					fieldId: 'trandate',
    					    value: shipdate,
    					    ignoreFieldChange: true
    					});
        				
        				/**
        				rec.setValue({
        					fieldId: 'tobeemailed',
    					    value: true,
    					    ignoreFieldChange: true
    					});
        				
        				rec.setValue({
        					fieldId: 'tobeemailed',
    					    value: true,
    					    ignoreFieldChange: true
    					});
        				rec.setValue({
        					fieldId: 'email',
    					    value: 'noeh.canizares@ftis.com.sg,arnold.peret@ftis.com.sg',
    					    ignoreFieldChange: true
    					});
        				rec.setValue({
        					fieldId: 'messagesel',
    					    value: '2',
    					    ignoreFieldChange: true
    					});
        				**/
        				var invoiceno = rec.save({
        				    enableSourcing: true,
        				    ignoreMandatoryFields: true
        				});
        				nyapiLog('invoiceno',invoiceno);
        				
        			}
        		}
    		}
    	}catch(exx){
    		nyapiLog('error aft submit', exx);
    	}
    	 	
    }

    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
		
	}
    
   
    
    

    
    return {
    	afterSubmit: afterSubmit
    };
    
});
