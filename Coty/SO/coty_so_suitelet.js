var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/encode','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {encode} encode
 */
function(file, http, record, render, runtime, search, serverWidget, url, encode) {
    RT = runtime;
    ENCODEMODULE = encode ;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			
			var params = this.request.parameters;
			var form = serverWidget.createForm({title:'Coty Sales Order'});
			nyapiLog('params',params);
			
			FORM = form;
			getInjectableForm();
			form.clientScriptFileId = 48344; // sandbox 42616 production 48344 

			
			form.addFieldGroup({
			    id : 'fieldgroupid',
			    label : 'Select Customer'
			});
			

			/**var  inlinehtml = form.addField({
			    id : 'custpage_inlinehtml',
			    type : 'inlinehtml',
			    label : 'Customer',
			    container : 'fieldgroupid'
			});
			inlinehtml.defaultValue = '<label for="square">Customer</label><input id="square" />';
			**/
			
			var customer = form.addField({
			    id : 'custpage_customer',
			    type : serverWidget.FieldType.SELECT,
			    label : 'Customer',
			    source:'customer',
			    container : 'fieldgroupid'
			});
			
			if(params.entity){
				form.addButton({
				    id : 'custpage_btnso',
				    label : 'Create Order',
				    functionName:'createSO()'
				});
				var custload = record.load({
				    type: record.Type.CUSTOMER,
				    id: params.entity}
				);
				
				
				customer.defaultValue = params.entity;
				
				
				var customercategory = form.addField({
				    id : 'custpage_category',
				    type : serverWidget.FieldType.SELECT,
				    label : 'Customer Category',
				    source:'customercategory',
				    container : 'fieldgroupid'
				});
				
				var ccat = custload.getValue({
				    fieldId: 'category'
				});
				nyapiLog('ccat',ccat);
				customercategory.defaultValue = ccat;
				params.cat = ccat;
				
				customercategory.updateDisplayType({
				    displayType : serverWidget.FieldDisplayType.HIDDEN
				});
				
				form.addFieldGroup({
				    id : 'fieldgroupid1',
				    label : 'Classification'
				});

				/**var custpage_shipdate = form.addField({
				    id : 'custpage_shipdate',
				    type : serverWidget.FieldType.DATE,
				    label : 'Ship Date',
				    container:'fieldgroupid1'
				});**/
				
				var custpage_promo = form.addField({
				    id : 'custpage_promo',
				    type : serverWidget.FieldType.SELECT,
				    label : 'SELECT TYPE OF PROMOTION',
				    container:'fieldgroupid1'
				});
				custpage_promo.addSelectOption({
				    value : '',
				    text : 'No Promotion'
				});
				custpage_promo.addSelectOption({
					    value : 1,
					    text : 'Contractual Promo'
					});
				custpage_promo.addSelectOption({
				    value : 2,
				    text : 'Seasonal Promo'
				});
				
				if(params.promo){
					custpage_promo.defaultValue = params.promo;
					
					var promolist = getPromotion(params.cat,params.promo);
					nyapiLog('promolist',promolist);
					
					form.addFieldGroup({
					    id : 'fieldgroupid2',
					    label : 'List of Promotions'
					});
					
					if(params.promo == '2'){
						
						var spromo = form.addField({
						    id : 'custpage_spromo',
						    type : serverWidget.FieldType.SELECT,
						    label : 'SELECT SEASONAL PROMOTION',
						    container:'fieldgroupid2'
						});
						for(var x = 0; x< promolist.length; x++){
							spromo.addSelectOption({
							    value : promolist[x].internalid,
							    text : promolist[x].name
							});
						}
						
					}
					
					if(params.promo == '1'){
						var pg = '';
						for(var x = 0; x< promolist.length; x++){
							 pg += '<p class="list-group-item">'+promolist[x].name+'</p>';
						}
						
						var  inlinehtml = form.addField({
						    id : 'custpage_inlinehtml',
						    type : 'inlinehtml',
						    label : 'listofcontractpromotion',
						    container:'fieldgroupid2'
						});
						inlinehtml.defaultValue = '<br/><div class=""> '+
							'<div class="list-group">'+pg+
							'</div></div>';	
					}	
					
				}
				
				
					
			}
			
			//customer.updateDisplayType({
			//    displayType : serverWidget.FieldDisplayType.HIDDEN
			//});


						
			this.response.writePage(form);
			
		};
		
		this.POST = function(){
			var params = this.request;
			this.response.write(JSON.stringify(params));
		};
		
    }
    
    function getPromotion(cat,type,today){
    	var cont = [];
    	if(cat && type){
    		var psearch = search.create({
    			   type: "promotioncode",
    			   filters:
    			   [
    			      [[["customercategory","anyof",cat],"AND",["custrecord_pro_type","anyof",type],"AND",["isinactive","is","F"]],"OR",[["ispublic","is","T"],"AND",["isinactive","is","F"],"AND",["custrecord_pro_type","anyof",type]]]
    			   ],
    			   columns:
    			   [
    			      search.createColumn({
    			         name: "name",
    			         sort: search.Sort.ASC
    			      }),
    			      "internalid",
    			      "custrecord_pro_type"
    			   ]
    			});
    			var searchResultCount = psearch.runPaged().count;
    			if(searchResultCount){
    				psearch.run().each(function(result){
    	    			   // .run().each has a limit of 4,000 results
    	    				var data = {};
    	    				data.internalid = result.getValue('internalid');
    	    				data.name = result.getValue('name');
    	    				data.ptype = result.getValue('custrecord_pro_type');
    	    				cont.push(data);
    	    			   return true;
    	    			});
    			}
    	}
		
		return cont;
    }
    function getInjectableForm(){
    	
    	var bodyAreaField = FORM.addField({
            id : 'custpage_bodyareafield',
            type : 'inlinehtml',
            label : 'Body Area Field'
        });
     
        //*********** Prepare HTML and scripts to Inject ***********
        //var body = getBody();
        var body = '';
        clientCode = clientCode.replace('$DEPLOYMENT_URL$', DEPLOYMENT_URL);
        var base64ClientCode = toBase64(clientCode);
        var scriptToInject = 'console.log(\'Added bottom script element\');';
        scriptToInject += "eval( atob(\'" + base64ClientCode + "\') );";
     
        //*********** Injecting HTML and scripts into an the Body Area Field ***********
        bodyAreaField.defaultValue = '<script>var script = document.createElement(\'script\');script.setAttribute(\'type\', \'text/javascript\');script.appendChild(document.createTextNode(\"' + scriptToInject + '\" ));document.body.appendChild(script);</script>';
       // return form;
    }
    
    var clientCode  = 'run(); ' + function run() {
        console.log('Running client code');
        
        //*********** GLOBAL VARIABLES ***********
        $ = NS.jQuery;
        DEPLOYMENT_URL = '$DEPLOYMENT_URL$'; 
        THISURL = $(location).attr('href');
        
        //*********** After DOM loads run this: ***********
        $(function() {
            injectHeaderScripts(); //Loads Libraries that will be placed on header (Optional)
            $(window).bind('load', injectHTML); //Replaces Suitelet's body with custom HTML once the window has fully loaded(Required)
            waitForLibraries(['swal'], runCustomFunctions); //Runs additional logic after required libraries have loaded (Optional)            
        });
        
        return;
        //*********** HELPER FUNCTIONS ***********
        
        /**
         * Loads Libraries that will be placed on header (Optional) 
         */
        function injectHeaderScripts(){
            console.log('loadHeaderLibraries START');
            //fancy grid
            //loadjscssfile("/core/media/media.nl?id=43634&c=240300_SB2&h=58b4d7fbdabe9e203656&_xt=.js");
            
            //easyautocomplete
           // loadjscssfile("/core/media/media.nl?id=43939&c=240300_SB2&h=3abc7417bb9ec8f33afc&_xt=.css");
           // loadjscssfile("/core/media/media.nl?id=43839&c=240300_SB2&h=c5cb6ee145d466397aca&_xt=.css");
            //loadjscssfile("/core/media/media.nl?id=43739&c=240300_SB2&h=d4a180fe9af2ae93e33a&_xt=.js");
            
       
           // loadjscssfile("https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css");
           // loadjscssfile("https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js");
           // loadjscssfile("https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js");
            loadjscssfile("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");
            
            //sweetmodal 
            //loadjscssfile("/core/media/media.nl?id=43434&c=240300_SB2&h=1c166ea0be44e957b6c2&_xt=.js");
            //loadjscssfile("/core/media/media.nl?id=43534&c=240300_SB2&h=78c3c91aeee603cdb99f&_xt=.css");       
            
            //loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css");
            //loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css");
            //loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" );
            loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js");
            loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");

            console.log('loadHeaderLibraries END'); 
     
            //*********** HELPER FUNCTION ***********
             function loadjscssfile(filename) {
                var filetype = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
                if (filetype == "js") { //if filename is a external JavaScript file
                    var fileref = document.createElement('script');
                    fileref.setAttribute("type", "text/javascript");
                    fileref.setAttribute("src", filename);
                } else if (filetype == "css") { //if filename is an external CSS file
                    var fileref = document.createElement("link");
                    fileref.setAttribute("rel", "stylesheet");
                    fileref.setAttribute("type", "text/css");
                    fileref.setAttribute("href", filename);
                }
                if (typeof fileref != "undefined"){
                    document.getElementsByTagName("head")[0].appendChild(fileref);
                }
                console.log(filename + ' plugin loaded');
            }
        }
        
        function waitForLibraries(libraries, functionToRun){
            var loadedLibraries = 0;
            for (var i in libraries) {
                var library = libraries[i];
                if(eval('typeof ' + library)!='undefined'){
                    loadedLibraries++;
                }
            }
            
            window.setTimeout(function(){
                if(loadedLibraries != libraries.length){
                    waitForLibraries(libraries, functionToRun);
                } else{
                    console.log(library + ' loaded');
                    functionToRun();
                }
              },500);
        }
        
        function injectHTML(){
        	
        	/**var customerSearch = nlapiSearchRecord("customer",null,
        			[
        			   ["subsidiary","anyof","7"]
        			], 
        			[
        			   new nlobjSearchColumn("internalid"), 
        			   new nlobjSearchColumn("formulatext").setFormula("{entityid} || ' ' ||{altname}")
        			]
        			);
        	
           var cont = [];
           for(var x = 0; x<customerSearch.length;x++){
        	   var data = {};
        	   data.internalid = customerSearch[x].getValue('internalid');
        	   data.name = customerSearch[x].getValue('formulatext');
        	   cont.push(data);
        	   
           }


            var options = {
            			data: cont,
            			getValue: "name",
            			minCharNumber: 0,
            			list: {
            				match: {
            					enabled: true
            				},
            				maxNumberOfElements: 10,
            				hideOnEmptyPhrase: true
            			},
            			theme:'square'
            		};

            $("#square").easyAutocomplete(options);**/
            
        }
        
        
        function runCustomFunctions() {
            console.log('clientFunctions START');
            var DEPLOYMENT_URL = '$DEPLOYMENT_URL$';
        }
    };
    function toBase64(stringInput){
        return ENCODEMODULE.convert({
            string: stringInput,
            inputEncoding: ENCODEMODULE.Encoding.UTF_8,
            outputEncoding: ENCODEMODULE.Encoding.BASE_64
        });
    }
   
    
    
    
   
    
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
	    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
   
    
    
    
    return {
        onRequest: setGo
    };
    
});