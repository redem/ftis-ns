/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define([],

function() {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
    	if (window.onbeforeunload){
 	  	   window.onbeforeunload=function() {null;};
 	  	}
    	
    }
    	
    	
 

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	
    	var durl = window.nlapiResolveURL('SUITELET','customscript_coty_so_ss','customdeploy_coty_so_ss');
    	switch(scriptContext.fieldId){
	    	case 'custpage_promo': //custpage_promo
	    		var cust = window.nlapiGetFieldValue('custpage_customer');
	    		var custcat = window.nlapiGetFieldValue('custpage_category');
	    		var promo = window.nlapiGetFieldValue('custpage_promo');
	    		if(cust){
		    		location = durl + '&entity='+ cust +'&promo='+promo + '&cat='+custcat;
	    		}else{
	    			location = durl;
	    		}
	    		break;
	    	case 'custpage_customer':
	    		var cust = window.nlapiGetFieldValue('custpage_customer');
	    		//var custcat = window.nlapiGetFieldValue('custpage_category');
	    		//var promo = window.nlapiGetFieldValue('custpage_promo');
	    		if(cust){
	    			location = durl + '&entity='+ cust;
		    	}else{
	    			location = durl;
	    		}
	    		break;
    	}
    }

    /**
     * Function to be executed when field is slaved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     *
     * @since 2015.2
     */
    function postSourcing(scriptContext) {

    }

    /**
     * Function to be executed after sublist is inserted, removed, or edited.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function sublistChanged(scriptContext) {

    }

    /**
     * Function to be executed after line is selected.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function lineInit(scriptContext) {

    }

    /**
     * Validation function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @returns {boolean} Return true if field is valid
     *
     * @since 2015.2
     */
    function validateField(scriptContext) {

    }

    /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) {

    }

    /**
     * Validation function to be executed when sublist line is inserted.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateInsert(scriptContext) {

    }

    /**
     * Validation function to be executed when record is deleted.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateDelete(scriptContext) {

    }

    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {

    }
    
    function createSO(){
    	var solink = window.nlapiResolveURL('RECORD', 'salesorder');
    	var cust = window.nlapiGetFieldValue('custpage_customer');
    	var promo = window.nlapiGetFieldValue('custpage_promo');
    	var shipdate = window.nlapiGetFieldValue('custpage_shipdate');
    	var custcat = window.nlapiGetFieldValue('custpage_category');
    	var spromo = window.nlapiGetFieldValue('custpage_spromo');
    	
    	
    	
    	if(cust){
    		promo = (promo)?promo:'';
    		spromo = (spromo)?spromo:'';
    		
    		//location = solink+'&entity='+cust+'&promo='+promo+'&cf=250'+'&shpdate='+shipdate+'&cat='+custcat+'&spromo='+spromo;//sandbox
    		location = solink+'&entity='+cust+'&promo='+promo+'&cf=258'+'&shpdate='+shipdate+'&cat='+custcat+'&spromo='+spromo; //production
    	}
    }
    
    

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        createSO : createSO
    };
    
});
