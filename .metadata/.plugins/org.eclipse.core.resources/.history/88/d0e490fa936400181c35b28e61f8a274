/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 May 2018     noeh.canizares
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
    this.mctx = nlapiGetContext();
    this.request = request;
    this.response = response;
    this.runFunction = function() {
        if (this.request.getMethod() == 'GET') {
            this.GET();
        } else {
            this.POST();
        }
    };
    this.GET = function() {
    	try{
    		var soid = this.request.getParameter('soid');
    		nlapiLogExecution('ERROR', 'soid', soid);
	        var ifid = createItemFufillmentWithLot(soid);
	        this.response.sendRedirect('RECORD', 'itemfulfillment', ifid);
	      }catch(ex){
	    	nlapiLogExecution('ERROR','ex',ex.code);
	    	
	        this.response.write('PLEASE CHECK ITEM AVAILABILITY');
    	}
    };
}

function runForm(request, response) {
    new suitelet(request, response).runFunction();
}

function createItemFufillmentWithLot(id) {
	var nsRecord, lineItems, locations;
	//create an item fulfillment from sales order and auto fill with the lot numbers using LIFO
	//also accommodate preferred bin assuming customization on item record to hold reference to a preferred bin
	nsRecord = nlapiTransformRecord('salesorder', id, 'itemfulfillment');
	lineItems = [];
	lotNumbers = [];
	locations = [];
	
	//build array of transaction item and locations to prepare for batch based searches
	for(var i = 1; i <= nsRecord.getLineItemCount('item'); i++){
		lineItems.push(nsRecord.getLineItemValue('item','item',i));

		if(nsRecord.getLineItemValue('item','location',i))
			locations.push(nsRecord.getLineItemValue('item','location',i));
	}

	
	var stocks = getLotBin(lineItems,locations);
	nlapiLogExecution('ERROR', 'stocks', JSON.stringify(stocks));
	nlapiLogExecution('ERROR','count',nsRecord.getLineItemCount('item'));
	
	//now do the work to set the LOT Number and Preferred Bin on each item
	for(var i = 1; i <= nsRecord.getLineItemCount('item'); i++){
		nsRecord.selectLineItem('item', i);
		var committed = Number(nsRecord.getCurrentLineItemValue('item','quantitycommitted'));
		var itemId = nsRecord.getCurrentLineItemValue('item', 'item');
		var itemtype = nsRecord.getCurrentLineItemValue('item','itemtype');
		
		var qty = Number(nsRecord.getCurrentLineItemValue('item','quantityremaining'));
		qty = (qty <= committed)?qty :committed;
		var tempqty = Number(nsRecord.getCurrentLineItemValue('item','quantityremaining'));
		tempqty = (tempqty <= committed)?tempqty :committed;
		
		var lotitem = _.where(stocks,{item: itemId});
		nlapiLogExecution('ERROR', 'xitem', itemId);
		
		nlapiLogExecution('ERROR', 'x', JSON.stringify(lotitem));
		
		nlapiLogExecution('ERROR', 'xitem', itemtype);
		
		
		//Set Quantity from Available
		nlapiLogExecution('ERROR', 'qty '+itemId+' '+i, qty);
		nsRecord.setCurrentLineItemValue('item', 'quantity', qty);
		

		if(itemtype != 'Kit'){
			var invDetail = nsRecord.createCurrentLineItemSubrecord('item','inventorydetail');
			//check if the item supports assignment
			if(invDetail && invDetail.getLineItemCount('inventoryassignment') <= 0){
				
				var lines = [];
				while(tempqty > 0){
					var noqty;
					for(var s=0;s<lotitem.length;s++){
						var stk = lotitem[s];
						var q = (tempqty>stk.qtya)?stk.qtya:tempqty;
						if(stk.qtya > 0){
							
							var data = {};
							data.lotno = stk.lotno;
							data.qty = q;
							data.bin = stk.bin;
							data.expirydate = stk.expirydate;
							stk.qtya = stk.qtya - q;
							lines.push(data);
							tempqty = tempqty-q;
						}
						nlapiLogExecution('ERROR','q ',q);
						nlapiLogExecution('ERROR','stk.qty ',stk.qtya);
						nlapiLogExecution('ERROR','order ',tempqty);
						if(q <= 0){
							noqty = q
							break;
						}
						
						
					}
					if(tempqty<= 0){
						break;
					}
					
				}
				nlapiLogExecution('ERROR','lines', JSON.stringify(lines));				
				//unload any assignments that may already be there
				var c = invDetail.getLineItemCount('inventoryassignment');
				for(var x = c; x > 0; x--){
					invDetail.removeLineItem('inventoryassignment',x);
				}

				for(var l = 0; l < lines.length; l++){
					
						nlapiLogExecution('Debug', 'INV Detail', invDetail.getLineItemCount('inventoryassignment') + ' : ' + lines[l].number + ' | ' + lines[l].bin + ' | ' + lines[l].qty + ' / ' +qty);
						invDetail.selectNewLineItem('inventoryassignment');
						invDetail.setCurrentLineItemText('inventoryassignment','issueinventorynumber', lines[l].lotno);
						invDetail.setCurrentLineItemValue('inventoryassignment','binnumber', lines[l].bin);
						invDetail.setCurrentLineItemValue('inventoryassignment','quantity', lines[l].qty);
						//invDetail.setCurrentLineItemValue('inventoryassignment','expirydate', lines[l].expirydate);
						invDetail.commitLineItem('inventoryassignment');
					
				}
				invDetail.commit();
				nlapiLogExecution('ERROR', 'invDetail',JSON.stringify(invDetail));				
			}	
		}
		nsRecord.commitLineItem('item');

		
	}
	nlapiLogExecution('ERROR', 'nsrecord', JSON.stringify(nsRecord));
	
	var id = nlapiSubmitRecord(nsRecord,null,true);
	return id;
}




function getLotBin2(itemID,locationid){
	
	
	var s = nlapiSearchRecord("inventorydetail",null,
			[
			   ["location","anyof",locationid], 
			   "AND", 
			   ["item","anyof",itemID], 
			   "AND", 
			   ["binnumber","noneof","@NONE@"]
			], 
			[
			   new nlobjSearchColumn("location",null,"GROUP"), 
			   new nlobjSearchColumn("binnumber",null,"GROUP"), 
			   new nlobjSearchColumn("item","inventoryNumber","GROUP"), 
			   new nlobjSearchColumn("quantityavailable","inventoryNumber","GROUP"), 
			   new nlobjSearchColumn("inventorynumber","inventoryNumber","GROUP"), 
			   new nlobjSearchColumn("expirationdate","inventoryNumber","GROUP").setSort(false)
			]
	);
	
	//build a structure that will later be used to help setup preferred bin and lot numbers based on 
	//transactional dates outside this function

	var cont = [];
	if(s){
		for(var i = 0; i < s.length; i++){
			var data = {};
			data.item = s[i].getValue('item','inventoryNumber','GROUP');
			data.bin = s[i].getValue('binnumber',null,'GROUP');
			data.location = s[i].getValue('location',null,'GROUP');
			data.lotno = s[i].getValue('inventorynumber','inventoryNumber','GROUP');
			data.qtya = Number(s[i].getValue('quantityavailable','inventoryNumber','GROUP'));
			data.expirydate = s[i].getValue('expirationdate','inventoryNumber','GROUP');
			cont.push(data);
			
		}
	}
	return cont;
}

function getLotBin(itemID,locationid){
	var s = nlapiSearchRecord("item",null,
		[
		   ["internalid","anyof",itemID], 
		   "AND", 
		   ["inventorynumberbinonhand.location","anyof",locationid], 
		   "AND", 
		   ["inventorynumberbinonhand.quantityavailable","greaterthan","0"], 
		   "AND", 
		   ["formulanumeric: CASE WHEN {inventorynumberbinonhand.inventorynumber} = {inventorynumber.inventorynumber} THEN 1 ELSE 0 END","greaterthan","0"]
		], 
		[
     	   new nlobjSearchColumn("internalid",null,null), 
		   new nlobjSearchColumn("quantityavailable","inventoryNumberBinOnHand",null), 
		   new nlobjSearchColumn("binnumber","inventoryNumberBinOnHand",null), 
		   new nlobjSearchColumn("inventorynumber","inventoryNumberBinOnHand",null), 
		   new nlobjSearchColumn("location","inventoryNumberBinOnHand",null), 
		   new nlobjSearchColumn("quantityonhand","inventoryNumberBinOnHand",null), 
		   new nlobjSearchColumn("expirationdate","inventoryNumber",null).setSort(false)
		]
		);
	var cont = [];
	if(s){
		for(var i = 0; i < s.length; i++){
			var data = {};
			data.item = s[i].getValue('internalid');
			data.bin = s[i].getValue('binnumber','inventoryNumberBinOnHand');
			data.location = s[i].getValue('location','inventoryNumberBinOnHand');
			data.lotno = s[i].getText('inventorynumber','inventoryNumberBinOnHand');
			data.qtya = Number(s[i].getValue('quantityavailable','inventoryNumberBinOnHand'));
			data.expirydate = s[i].getValue('expirationdate','inventoryNumber');
			cont.push(data);
			
		}
	}
	return cont;
}



