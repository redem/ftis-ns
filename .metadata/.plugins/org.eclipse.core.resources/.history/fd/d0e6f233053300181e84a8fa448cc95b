var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/encode','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {encode} encode
 */
function(file, http, record, render, runtime, search, serverWidget, url, encode) {
    RT = runtime;
    ENCODEMODULE = encode ;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var params = this.request.parameters;
			var form = serverWidget.createForm({title:'Coty Sales Order'});
			FORM = form;
			form.clientScriptFileId = 42616;
			form.addFieldGroup({
			    id : 'fieldgroupid',
			    label : 'Primary Information'
			});
			var fcust = form.addField({
			    id : 'custpage_customer',
			    type : serverWidget.FieldType.SELECT,
			    label : 'Customer',
			    source:'customer',
			    container : 'fieldgroupid'
			});
			if(params.entity){
				fcust.defaultValue = params.entity;
				var custdata = search.lookupFields({
				    type: search.Type.CUSTOMER,
				    id: params.entity,
				    columns: ['custentity_salon_thai_name', 'custentity24', 'salesrep', 'custentity_salon_class','address']
				});
				var custpage_trandate = form.addField({
				    id : 'custpage_trandate',
				    type : serverWidget.FieldType.DATE,
				    label : 'Sales Order Date',
				    container : 'fieldgroupid'
				});
				var custpage_trandate = form.addField({
				    id : 'custpage_shipdate',
				    type : serverWidget.FieldType.DATE,
				    label : 'Ship Date',
				    container : 'fieldgroupid'
				});
				var custpage_salongroup = form.addField({
				    id : 'custpage_custentity24',
				    type : serverWidget.FieldType.SELECT,
				    label : 'Salon Group',
				    source:'customer',
				    container : 'fieldgroupid'
				});
				custpage_salongroup.defaultValue = custdata.custentity24;
				var custpage_salonthainame = form.addField({
				    id : 'custpage_salonthainame',
				    type : serverWidget.FieldType.TEXT,
				    label : 'Salon Thai Name',
				    container : 'fieldgroupid'
				});
				custpage_salonthainame.defaultValue = custdata.custentity_salon_thai_name;
				var custpage_address = form.addField({
				    id : 'custpage_address',
				    type : serverWidget.FieldType.TEXTAREA,
				    label : 'Ship Address',
				    container : 'fieldgroupid'
				});
				custpage_address.defaultValue = custdata.address;
				var custpage_salesrep = form.addField({
				    id : 'custpage_salesrep',
				    type : serverWidget.FieldType.SELECT,
				    label : 'Sales Rep',
				    source:'employee',
				    container : 'fieldgroupid'
				});
				custpage_salesrep.defaultValue = (custdata.salesrep.length)?custdata.salesrep[0].value:'';
				
				var custpage_salon_class = form.addField({
				    id : 'custpage_salon_class',
				    type : serverWidget.FieldType.SELECT,
				    label : 'Salon Classification',
				    source:'customlist_salon_class',
				    container : 'fieldgroupid'
				});
				custpage_salon_class.defaultValue = (custdata.custentity_salon_class.length)?custdata.custentity_salon_class[0].value:'';
				form.addFieldGroup({
				    id : 'fieldgroupid1',
				    label : 'Salon Contract Particulars'
				});
			}

			form.addFieldGroup({
			    id : 'fieldgroupid2',
			    label : 'Item Filter'
			});
			
			var fhouse = form.addField({
			    id : 'custpage_house',
			    type : serverWidget.FieldType.MULTISELECT,
			    label : 'House',
			    source:'customrecord_coty_house',
			    container : 'fieldgroupid2'
			});
			
			var fprodline = form.addField({
			    id : 'custpage_prodline',
			    type : serverWidget.FieldType.MULTISELECT,
			    label : 'Product Line',
			    container : 'fieldgroupid2'
			});
			
			
			var bhouse = (params.vhouse)?params.vhouse.split('\u0005'):[];
			if(params.vhouse){	
				fhouse.defaultValue = bhouse;
				nyapiLog('vhouse',bhouse);
				var pldata = getProdLine(bhouse);
				for(var x=0; x<pldata.length;x++){
					fprodline.addSelectOption({
					    value : pldata[x].internalid,
					    text : pldata[x].name
					});
				}
			}
			
			var bprod = (params.prodline)?params.prodline.split('\u0005'):[];
			
			if(bhouse.length && bprod.length){
				fprodline.defaultValue = bprod;
				var itemdata = getItems(bhouse,bprod);
				nyapiLog('itemdata',itemdata);
				var ilist = form.addSublist({
				    id : 'custpage_solist',
				    type : serverWidget.SublistType.INLINEEDITOR,
				    label : 'Ites'
				});			
				ilist.addField({id : 'custpage_linehouse',type : serverWidget.FieldType.SELECT,label : 'House',source:'customrecord_coty_house'});
				ilist.addField({id : 'custpage_lineprodl',type : serverWidget.FieldType.SELECT,label : 'Product Line',source:'customrecord_coty_prodline'});
				ilist.addField({id : 'cutpage_sitem',type : serverWidget.FieldType.SELECT,label : 'Item',source: 'item'});
				ilist.addField({id : 'cutpage_itemdesc',type : serverWidget.FieldType.TEXT,label : 'Item Descripton'});
				ilist.addField({id : 'custpage_onhand',type : serverWidget.FieldType.FLOAT,label : 'On hand'});	
				ilist.addField({id : 'custpage_qty',type : serverWidget.FieldType.FLOAT,label : 'Quantity'});	
				ilist.addField({id : 'custpage_focqty',type : serverWidget.FieldType.FLOAT,label : 'FOC Quantity'});	
				ilist.addField({id : 'custpage_unitprice',type : serverWidget.FieldType.FLOAT,label : 'Unit Price'});
				
				for(var z = 0; z< itemdata.length; z++){
					/**
					 var data = {};
	    			data.internalid = result.getValue('internalid');
	    			data.custitem_ftss_house = result.getValue('custitem_ftss_house');
	    			data.custitem_coty_prodline_cr = result.getValue('custitem_coty_prodline_cr');
	    			data.itemid = result.getValue('itemid');
	    			data.displayname = result.getValue('displayname');
	    			cont.push(data);
	    			
					 */
					nyapiLog('itemdata',itemdata[z]);
					ilist.setSublistValue({id: 'custpage_linehouse',line: z,value: itemdata[z].custitem_ftss_house});
					ilist.setSublistValue({id: 'custpage_lineprodl',line: z,value: itemdata[z].custitem_coty_prodline_cr});
					ilist.setSublistValue({id: 'custpage_sitem',line: z,value: itemdata[z].internalid});
					ilist.setSublistValue({id: 'custpage_itemdesc',line: z,value: itemdata[z].itemid});
					ilist.setSublistValue({id: 'custpage_unitprice',line: z,value: "121"});
				
				}
			}
			
			
			this.response.writePage(form);
			
		};
		
		this.POST = function(){
			var params = this.request;
			this.response.write(JSON.stringify(params));
		};
		
    }
    
    function getItems(house,prodline){
    	var inventoryitemSearchObj = search.create({
    		   type: "inventoryitem",
    		   filters:
    		   [
    		      ["subsidiary","anyof","7"], 
    		      "AND", 
    		      ["type","anyof","InvtPart"], 
    		      "AND", 
    		      ["custitem_ftss_house","anyof",house], 
    		      "AND", 
    		      ["custitem_coty_prodline_cr","anyof",prodline]
    		   ],
    		   columns:
    		   [
    		      "internalid",
    		      "custitem_ftss_house",
    		      "custitem_coty_prodline_cr",
    		      search.createColumn({
    		         name: "itemid",
    		         sort: search.Sort.ASC
    		      }),
    		      "displayname"
    		   ]
    		});
    		var cont = [];
    		inventoryitemSearchObj.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			var data = {};
    			data.internalid = result.getValue('internalid');
    			data.custitem_ftss_house = result.getValue('custitem_ftss_house');
    			data.custitem_coty_prodline_cr = result.getValue('custitem_coty_prodline_cr');
    			data.itemid = result.getValue('itemid');
    			data.displayname = result.getValue('displayname');
    			cont.push(data);
    			
    		   return true;
    		});
    		return cont;
    		
    }
    
    function getProdLine(house){
    	var customrecord_coty_prodlineSearchObj = search.create({
    		   type: "customrecord_coty_prodline",
    		   filters: [
    		      ["custrecord1","anyof",house]
    		   ],
    		   columns: [
    		      "internalid",
    		      search.createColumn({
    		         name: "name",
    		         sort: search.Sort.ASC
    		      }),
    		      "custrecord1"
    		   ]
    		});
    		var cont = [];
    		customrecord_coty_prodlineSearchObj.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			var data = {};
    			data.internalid = result.getValue('internalid');
    			data.name = result.getValue('name');
    			cont.push(data);
    		   return true;
    		});
    		return cont;
    }
    
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
	    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
   
    
    
    
    return {
        onRequest: setGo
    };
    
});