function getClasses(dataIn) {		
	var currentContext = nlapiGetContext();
	nlapiLogExecution('DEBUG','RESTlet', currentContext.getExecutionContext());
	
	var sType = 'customlist_ftis_brand_list';

	var sColumns = new Array();
	var sFilters = new Array();

	sColumns.push(new nlobjSearchColumn('internalId'));
	sColumns.push(new nlobjSearchColumn('name'));

	var search = nlapiCreateSearch(sType, null , sColumns);
    var resultSet = search.runSearch();
	
	if(resultSet == null)	return '';	
	
	var DataOut = new Array();
	
	var sr = resultSet.getResults(dataIn.fromIndex, dataIn.toIndex);

	if(sr == null || sr.length == 0){
		return '';
	}
	
	for (var i=0; i<sr.length; i+=1) {			
	    var sOut = {};
	    //INTERNAL ID NEW CODE
	    sOut['InternalId'] = sr[i].getValue('internalId');
		sOut['Code'] = sr[i].getValue('internalid');
		sOut['Description'] = sr[i].getValue('name');

		DataOut.push(sOut);		
	}

    var custNumber = {
        IntenralId: 'Customer Number',
        Code: 'Customer Number',
        Description: 'Customer Number'
    }

    DataOut.push(custNumber);

    nlapiLogExecution('DEBUG','RESTlet', currentContext.getRemainingUsage());
	
	return DataOut;
}

