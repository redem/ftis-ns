function workOrderGet(dataIn) { 
	var currentContext = nlapiGetContext();
	//nlapiLogExecution('DEBUG','RESTlet', currentContext.getRemainingUsage());
	
	var sType = 'workorder';
	var fromDate = nlapiStringToDate(dataIn.from.replace(/-/g, "/"));
	var toDate = nlapiStringToDate(dataIn.to.replace(/-/g, "/"));


	var sColumns = new Array();
	var sFilters = new Array();

	sColumns.push(new nlobjSearchColumn('tranid'));
	sColumns.push(new nlobjSearchColumn('line'));
	sColumns.push(new nlobjSearchColumn('mainline'));
	sColumns.push(new nlobjSearchColumn('status'));
	sColumns.push(new nlobjSearchColumn('trandate'));
	sColumns.push(new nlobjSearchColumn('item'));
	sColumns.push(new nlobjSearchColumn('quantity'));
	sColumns.push(new nlobjSearchColumn('enddate'));
	sColumns.push(new nlobjSearchColumn('built')); 
	sColumns.push(new nlobjSearchColumn('actualshipdate'));
	sColumns.push(new nlobjSearchColumn('location'));


	//var sFilterExpression = [ [['applyinglinktype','is',['ShipRcpt']],'OR',['mainline','is','T']] , 'AND', ['trandate', 'onorafter', fromDate], 'AND', ['trandate', 'before', toDate]];
	var sFilterExpression = ['trandate','within',fromDate, toDate] ;
	var search = nlapiCreateSearch(sType, sFilterExpression, sColumns);
    var resultSet = search.runSearch();
	// consider using nlapiCreateSearch if expected searchresult is more than 1000

      if(resultSet == null)	return '';
	
	var DeliveryDate, SOstatus, OrderDate, OrderNum, Location;
	var ItemCode, OrderLine, DeliveryQuantity;
	var ShipDate, ShipQuantity;
	var DataOut = new Array();
	
	var pageSize = 1000;
	var reachedEnd = false;
	var fromIndex = 0;
	while(true){
			var sr = resultSet.getResults(fromIndex, fromIndex + pageSize);
			// Get SalesOrder Data
			//nlapiLogExecution('DEBUG','count', fromIndex);
			if(sr == null || sr.length == 0){
				break;
			}
			
			fromIndex += sr.length;
			for (var iLP=0; iLP<sr.length; iLP++) {
				if (sr[iLP].getValue('mainline').equals('*')) {
					//nlapiLogExecution('DEBUG','Search Result Mainline ', iLP + ' ' + sr[iLP].getValue('mainline'));
					DeliveryDate = sr[iLP].getValue('enddate');
					SOstatus = sr[iLP].getValue('status');
					OrderDate = sr[iLP].getValue('trandate');
					OrderNum = sr[iLP].getValue('tranid');
					Location = sr[iLP].getValue('location');					
				}
				else if (!(sr[iLP].getValue('mainline').equals('*'))) {
				    //	nlapiLogExecution('DEBUG','Search Result Item Line', iLP + ' ' );

					
					ItemCode = sr[iLP].getValue('item');
					OrderLine = sr[iLP].getValue('line');
					DeliveryQuantity = sr[iLP].getValue('quantity');
					ShipDate = sr[iLP].getValue('actualshipdate');
					ShipQuantity = sr[iLP].getValue('built');
					
					var sOut = {};

				    //INTERNAL ID NEW CODE
				    sOut['ProductInternalId'] = ItemCode;
				    sOut['LocationInternalId'] = Location;

					sOut['OrderNumber'] = OrderNum + "-" + OrderLine;
					sOut['Status'] = SOstatus;
					sOut['OrderDate'] = OrderDate;
					sOut['ItemCode'] = ItemCode;
                    sOut['Quantity'] = DeliveryQuantity;
					sOut['DeliveryDate'] = DeliveryDate;
					sOut['ShipDate'] = ShipDate;
					sOut['ShipQuantity'] = ShipQuantity;
					sOut['Location'] = Location;
					
					DataOut.push(sOut);
				}
			}
	}
	nlapiLogExecution('DEBUG','RESTlet', currentContext.getRemainingUsage());
	
	return DataOut;
}
