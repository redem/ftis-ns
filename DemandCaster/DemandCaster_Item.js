function getItems(dataIn) {
    if (dataIn.getCir === 'True') return new Array();
    var currentContext = nlapiGetContext();
    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getExecutionContext());
    var sType = 'inventoryitem';
    var sColumns = columnsDefault();

    //keep in same order as in ItemExtractionRest
    // 1. getAssemblyItems
    // 2. getKit
    // 3. getCir
    if (dataIn.getAssemblyItems === 'True') {
        sType = 'assemblyitem';
    }
    if (dataIn.getKit === 'True') {
        sType = 'kititem';
    }
    if (dataIn.getCir === 'True') {
        sType = 'customrecord_ftis_customeritemref';
        sColumns = columnsCIR();
    }

    var filter = Array();
    if (dataIn.isDifferential === 'True') {
        filter.push(new nlobjSearchFilter('modified', null, 'onOrAfter', dataIn.lastModifiedDate.replace(/-/g, "/")));
    }


    var search = nlapiCreateSearch(sType, ['isinactive', 'is', 'False'], sColumns);
    var resultSet = search.runSearch();

    if (resultSet == null) return '';

    var DataOut = new Array();

    var sr = resultSet.getResults(dataIn.fromIndex, dataIn.toIndex);

    nlapiLogExecution('DEBUG', 'count', dataIn.fromIndex);
    if (sr == null || sr.length == 0) {
        return '';
    }

    for (var i = 0; i < sr.length; i += 1) {
        if (sType === 'customrecord_ftis_customeritemref') {
            DataOut.push(returnArrayCir(sr, i));
        } else if (!containsInternalId(sr[i].getValue('internalId'), DataOut)) {
            DataOut.push(returnArrayDefault(sr, i));
        }
    }

    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getRemainingUsage());

    return DataOut;
}

function containsInternalId(internalId, arrayItems) {
    for (var i = 0; i < arrayItems.length; i++) {
        if (arrayItems[i].InternalId === internalId) {
            return true;
        }
    }

    return false;
}

function columnsDefault() {
    nlapiLogExecution('DEBUG', 'RESTlet', 'columnsDefalut');
    var sColumns = new Array();
    sColumns.push(new nlobjSearchColumn('internalId'));
    sColumns.push(new nlobjSearchColumn('displayname'));
    sColumns.push(new nlobjSearchColumn('itemId'));
    sColumns.push(new nlobjSearchColumn('quantityAvailable'));
    sColumns.push(new nlobjSearchColumn('reorderPoint'));
    sColumns.push(new nlobjSearchColumn('leadTime'));
    sColumns.push(new nlobjSearchColumn('averagecost'));
    sColumns.push(new nlobjSearchColumn('safetyStockLevel'));
    sColumns.push(new nlobjSearchColumn('custitem_ftis_brand'));
    sColumns.push(new nlobjSearchColumn('class'));
    //sColumns.push(new nlobjSearchColumn('internalId', 'vendor'));
    sColumns.push(new nlobjSearchColumn('vendor'));
    sColumns.push(new nlobjSearchColumn('saleUnit'));
    sColumns.push(new nlobjSearchColumn('baseprice'));
    sColumns.push(new nlobjSearchColumn('custitem_ftis_purc_moq'));
    sColumns.push(new nlobjSearchColumn('custitem_ebiznet_item_cube'));
    sColumns.push(new nlobjSearchColumn('custitem_ftis_brand'));
    sColumns.push(new nlobjSearchColumn('created'));
    sColumns.push(new nlobjSearchColumn('custitem_ftis_inner_pack_qty'));

    return sColumns;
}

function columnsCIR() {
    nlapiLogExecution('DEBUG', 'RESTlet', 'colulmnsCIR');
    var sColumns = new Array();
    sColumns.push(new nlobjSearchColumn('internalId'));
    sColumns.push(new nlobjSearchColumn('custrecord_ftis_customeritemref_custpart'));
    sColumns.push(new nlobjSearchColumn('custrecord_ftis_project_status'));
    sColumns.push(new nlobjSearchColumn('custrecord_ftis_customeritemref_nsitem'));

    return sColumns;
}

function returnArrayDefault(sr, i) {
    nlapiLogExecution('DEBUG', 'RESTlet', 'retDefault');
    var sOut = {};
    sOut['InternalId'] = sr[i].getValue('internalId');
    sOut['Code'] = sr[i].getValue('itemid');
    sOut['VendorInternalId'] = sr[i].getValue('vendor');
    sOut['ClassInternalId'] = sr[i].getValue('custitem_ftis_brand');
    sOut['Description'] = sr[i].getValue('displayname');
    sOut['QuantityOnHand'] = sr[i].getValue('quantityAvailable');
    sOut['ReorderTriggerLevelQuantity'] = sr[i].getValue('reorderPoint');
    sOut['LeadTime'] = sr[i].getValue('leadTime');
    sOut['MinimumQuantity'] = sr[i].getValue('custitem_ftis_purc_moq');
    sOut['VendorCode'] = sr[i].getValue('name', 'vendor');
    sOut['UnitOfMeasure'] = sr[i].getText('saleUnit');
    sOut['ClassCode'] = sr[i].getValue('custitem_ftis_brand');
    sOut['UnitCost'] = sr[i].getValue('averagecost');
    sOut['SafetyStockLevel'] = sr[i].getValue('safetyStockLevel');
    sOut['CaseCube'] = sr[i].getValue('custitem_ebiznet_item_cube');
    sOut['CasePack'] = sr[i].getValue('custitem_ftis_inner_pack_qty') == '' ? '1' : sr[i].getValue('custitem_ftis_inner_pack_qty');
    sOut['Price'] = sr[i].getValue('baseprice');
    sOut['Tag1'] = sr[i].getText('class');
    sOut['DateCreated'] = sr[i].getValue('created');

    return sOut;
}

function returnArrayCir(sr, i) {
    nlapiLogExecution('DEBUG', 'RESTlet', 'retCIR');
    var sOut = {};
    sOut['InternalId'] = 'CIR-' + sr[i].getValue('internalId');
    sOut['Code'] = 'CIR-' + sr[i].getValue('custrecord_ftis_customeritemref_custpart');
    sOut['VendorInternalId'] = '';
    sOut['ClassInternalId'] = 'Customer Number';
    sOut['Description'] = sr[i].getValue('custrecord_ftis_customeritemref_nsitem');
    sOut['QuantityOnHand'] = '0';
    sOut['ReorderTriggerLevelQuantity'] = '0';
    sOut['LeadTime'] = '0';
    sOut['MinimumQuantity'] = '0';
    sOut['VendorCode'] = '';
    sOut['UnitOfMeasure'] = 'each';
    sOut['ClassCode'] = 'Customer Number';
    sOut['UnitCost'] = '0';
    sOut['SafetyStockLevel'] = '0';
    sOut['CaseCube'] = 0;
    sOut['CasePack'] = sr[i].getValue('custitem_ftis_inner_pack_qty') == '' ? '1' : sr[i].getValue('custitem_ftis_inner_pack_qty');
    sOut['Price'] = '0';
    sOut['MakeBuy'] = 'M';
    sOut['Tag1'] = sr[i].getText('custrecord_ftis_project_status');

    return sOut;
}