function getItems(dataIn) {
  if (dataIn.getCir == 'True') return new Array();
    var currentContext = nlapiGetContext();
    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getExecutionContext());
    var sType = 'assemblyitem'; 
    var location = dataIn.location;
    var sColumns = columnsDefault(location);
    var search;
    
    if (dataIn.getCir === 'True') {
        sType = 'customrecord_ftis_customeritemref';
        sColumns = columnsCIR();
    }

    var sFilterExpression = [['inventorylocation', 'is', location]];

    if (location != 0 && sType !== 'customrecord_ftis_customeritemref') {
        search = nlapiCreateSearch(sType, sFilterExpression, sColumns);
    } else {
        search = nlapiCreateSearch(sType, null, sColumns);
    }

    var resultSet = search.runSearch();

    if (resultSet == null) return '';

    var DataOut = new Array();

    var sr = resultSet.getResults(dataIn.fromIndex, dataIn.toIndex);

    nlapiLogExecution('DEBUG', 'count', dataIn.fromIndex);
    if (sr == null || sr.length == 0) {
        return '';
    }

    for (var i = 0; i < sr.length; i += 1) {
       
        if (sType == 'customrecord_ftis_customeritemref') {
            DataOut.push(returnArrayCir(sr, i, location));
        } else {
            DataOut.push(returnArrayDefault(sr, i, location));
        }      
    }

    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getRemainingUsage());

    return DataOut;
}

function columnsDefault(location) {
    var columns = new Array();
    columns.push(new nlobjSearchColumn('internalId'));
    columns.push(new nlobjSearchColumn('memberitem'));
    columns.push(new nlobjSearchColumn('memberquantity'));
    if (location != 0) {
        columns.push(new nlobjSearchColumn('inventorylocation'));
    }
    
    return columns;
}

function columnsCIR() {
    var columns = new Array();
    columns.push(new nlobjSearchColumn('internalId'));
    columns.push(new nlobjSearchColumn('internalId','custrecord_ftis_customeritemref_nsitem'));
    
    return columns;
}

function returnArrayDefault(sr, i, location) {
    var sOut = {};
    sOut['ParentInternalId'] = sr[i].getValue('internalId');
    sOut['ChildInternalId'] = sr[i].getValue('memberitem');
    sOut['LocationInternalId'] = '';

    sOut['ParentProductCode'] = sr[i].getValue('internalId');
    sOut['ChildProductCode'] = sr[i].getValue('memberitem');
    sOut['Quantity'] = sr[i].getValue('memberquantity');
    sOut['Location'] = '';
    if (location != 0) {
        sOut['Location'] = sr[i].getValue('inventorylocation');
        sOut['LocationInternalId'] = sr[i].getValue('inventorylocation');
    }
    
    return sOut;              
}

function returnArrayCir(sr, i, location) {
    var sOut = {};

    sOut['ParentInternalId'] = 'CIR-' + sr[i].getValue('internalId');
    sOut['ChildInternalId'] = sr[i].getValue('internalId','custrecord_ftis_customeritemref_nsitem');
    sOut['LocationInternalId'] = '';

    sOut['ParentProductCode'] = 'CIR-' + sr[i].getValue('internalId');
    sOut['ChildProductCode'] = sr[i].getValue('internalId','custrecord_ftis_customeritemref_nsitem');
    sOut['Quantity'] = '1';
    sOut['Location'] = '';
    if (location != 0) {
        sOut['Location'] = location;
        sOut['LocationInternalId'] = location;
    }

    return sOut;
}