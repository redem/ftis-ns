var BillOfDistribution = function (sourceLocation, transferLeadTime) {
    this.sourceLocation = sourceLocation;
    this.transferLeadTime = transferLeadTime;
}

var validDistNetworkLocationValues = ['11', '17', '18'];
var billOfDistributionLocationMappingByLocation = {
    18: new BillOfDistribution('15', '2'),
    17: new BillOfDistribution('16', '14'),
    11: new BillOfDistribution('5', '1')
}

function getItems(dataIn) {
  if (dataIn.getCir == 'True') return new Array();
    var currentContext = nlapiGetContext();
    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getExecutionContext());

    var sType = 'inventoryitem';

    var sColumns = columnsDefault();
    var location = dataIn.location;

    if (dataIn.getAssemblyItems == 'True' && dataIn.getKit == 'False') {
        sType = 'assemblyitem';
    }
    //if (dataIn.getNonInventory == 'True') {
    //    sType = 'noninventoryitem';
    //}
    if (dataIn.getKit == 'True') {
        sType = 'kititem';
    }
    if (dataIn.getCir == 'True') {
        sType = 'customrecord_ftis_customeritemref';
        sColumns = ColumnsCir();
    }

    //var filter = ['inventorylocation', 'is', location];
    //if (sType == 'customrecord_ftis_customeritemref') {

    //    filter = null;
    //}

    var filter = new Array();    
    if (sType !== 'customrecord_ftis_customeritemref') {
        filter.push(new nlobjSearchFilter('inventorylocation', null, 'is', location));
    }
    if (dataIn.isDifferential === 'True') {
        filter.push(new nlobjSearchFilter('modified', null, 'onOrAfter', dataIn.lastModifiedDate.replace(/-/g, "/")));
    }

    var search = nlapiCreateSearch(sType, filter, sColumns);

    var resultSet = search.runSearch();

    if (resultSet == null) return '';

    var DataOut = new Array();

    var sr = resultSet.getResults(dataIn.fromIndex, dataIn.toIndex);

    nlapiLogExecution('DEBUG', 'count', dataIn.fromIndex);

    if (sr == null || sr.length == 0) {
        return '';
    }

    nlapiLogExecution('DEBUG', 'result', sr.length);

    for (var i = 0; i < sr.length; i += 1) {

        if (sType == 'customrecord_ftis_customeritemref') {
            DataOut.push(returnArrayCir(sr, i, location));
        } else {
            DataOut.push(returnArrayDefault(sr, i));
        }

    }

    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getRemainingUsage());

    return DataOut;
}

function columnsDefault() {
    var sColumnsDefault = new Array();
    sColumnsDefault.push(new nlobjSearchColumn('internalId'));
    sColumnsDefault.push(new nlobjSearchColumn('locationquantityavailable'));
    sColumnsDefault.push(new nlobjSearchColumn('locationquantitycommitted'));
    sColumnsDefault.push(new nlobjSearchColumn('inventorylocation'));
    sColumnsDefault.push(new nlobjSearchColumn('locationleadtime'));
    sColumnsDefault.push(new nlobjSearchColumn('leadtime'));
    sColumnsDefault.push(new nlobjSearchColumn('locationfixedlotsize'));
    sColumnsDefault.push(new nlobjSearchColumn('locationaveragecost')); // = location average cost
    //sColumns.push(new nlobjSearchColumn('averagecost')); // = location average cost
    sColumnsDefault.push(new nlobjSearchColumn('locationsafetystocklevel'));
    sColumnsDefault.push(new nlobjSearchColumn('itemId'));
    sColumnsDefault.push(new nlobjSearchColumn('custitem_ftis_distribution_network'));
    sColumnsDefault.push(new nlobjSearchColumn('custitem_ftis_purc_moq'));

    return sColumnsDefault;
}

function ColumnsCir() {
    var sColumnsCir = new Array();
    sColumnsCir.push(new nlobjSearchColumn('internalId'));
    sColumnsCir.push(new nlobjSearchColumn('custrecord_ftis_customeritemref_custpart'));

    return sColumnsCir;
}

function returnArrayDefault(sr, i) {
    var sOut = {};
    //INTERNAL ID NEW CODE
    sOut['ProductInternalId'] = sr[i].getValue('internalId');
    sOut['LocationInternalId'] = sr[i].getValue('inventorylocation');
    sOut['ProductCode'] = sr[i].getValue('internalId');
    sOut['LocationCode'] = sr[i].getValue('inventorylocation');
    sOut['QuantityAvailable'] = sr[i].getValue('locationquantityavailable');
    sOut['QuantityCommitted'] = sr[i].getValue('locationquantitycommitted');
    sOut['ItemId'] = sr[i].getValue('itemId');
    sOut['MinQuantity'] = sr[i].getValue('custitem_ftis_purc_moq');
    sOut['SafetyStockLevel'] = sr[i].getValue('locationsafetystocklevel');
    sOut['LeadTime'] = sr[i].getValue('locationleadtime');
    sOut['UnitCost'] = sr[i].getValue('locationaveragecost');
    sOut['ATPLeadTime'] = sr[i].getValue('locationleadtime');
    sOut['CaseCube'] = sr[i].getValue('locationfixedlotsize');
    if (validDistNetworkLocationValues.indexOf(sr[i].getValue('inventorylocation')) >= 0) {
        sOut['DemandSource'] = billOfDistributionLocationMappingByLocation[sr[i].getValue('inventorylocation')].sourceLocation;
        sOut['TransferLeadTime'] = billOfDistributionLocationMappingByLocation[sr[i].getValue('inventorylocation')].transferLeadTime;
    } else {
        sOut['DemandSource'] = '';
        sOut['TransferLeadTime'] = '0';
    }

    return sOut;
}

function returnArrayCir(sr, i, location) {
    var sOut = {};
    sOut['ProductInternalId'] = 'CIR-' + sr[i].getValue('internalId');
    sOut['LocationInternalId'] = location;
    sOut['ProductCode'] = 'CIR-' + sr[i].getValue('internalId');
    sOut['LocationCode'] = location;
    sOut['QuantityAvailable'] = '0';
    sOut['QuantityCommitted'] = '0';
    sOut['ItemId'] = sr[i].getValue('custrecord_ftis_customeritemref_custpart');
    sOut['MinQuantity'] = '0';
    sOut['SafetyStockLevel'] = '0';
    sOut['LeadTime'] = '0';
    sOut['UnitCost'] = '0';
    sOut['ATPLeadTime'] = '0';
    sOut['CaseCube'] = '0';
    if (validDistNetworkLocationValues.indexOf(location) >= 0) {
        sOut['DemandSource'] = billOfDistributionLocationMappingByLocation[location].sourceLocation;
        sOut['TransferLeadTime'] = billOfDistributionLocationMappingByLocation[location].transferLeadTime;
    } else {
        sOut['DemandSource'] = '';
        sOut['TransferLeadTime'] = '0';
    }

    return sOut;
}