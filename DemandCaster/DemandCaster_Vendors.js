function getVendors(dataIn) {	
	var currentContext = nlapiGetContext();
	nlapiLogExecution('DEBUG','RESTlet', currentContext.getExecutionContext());
		nlapiLogExecution('DEBUG','RESTlet', 'vendors');
	var sType = 'vendor';

	var sColumns = new Array();
	var sFilters = new Array();

	sColumns.push(new nlobjSearchColumn('internalId'));
	sColumns.push(new nlobjSearchColumn('companyname'));
	sColumns.push(new nlobjSearchColumn('firstname'));
	sColumns.push(new nlobjSearchColumn('lastname'));

	var search = nlapiCreateSearch(sType, null , sColumns);
	var resultSet = search.runSearch();
	
	if(resultSet == null)	
		return '';
	
	var DataOut = new Array();
	
	var sr = resultSet.getResults(dataIn.fromIndex, dataIn.toIndex);

	if(sr == null || sr.length == 0){
		return '';
	}
	
	for (var i=0; i<sr.length; i+=1) {
		var sOut = {};
		sOut['InternalId'] = sr[i].getValue('internalid');
		sOut['Code'] = sr[i].getValue('internalid');
		sOut['CompanyName'] = sr[i].getValue('companyname');
		sOut['IndividualName'] = sr[i].getValue('firstname') + ' ' + sr[i].getValue('lastname');

		DataOut.push(sOut);		
	}

	nlapiLogExecution('DEBUG','RESTlet', currentContext.getRemainingUsage());
	
	return DataOut;
}
	

