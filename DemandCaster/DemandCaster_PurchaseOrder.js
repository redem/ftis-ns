function getRESTlet(dataIn) {

    nlapiLogExecution('DEBUG', 'RESTlet', 'User: ' + nlapiGetUser() + ' ROle: ' + nlapiGetRole() + ' Subsidiary ' + nlapiGetSubsidiary());
    var currentContext = nlapiGetContext();
    var sType = '';
    if (dataIn.getTransferOrders == 'True') {
        sType = 'transferorder';
    } else {
        sType = 'purchaseorder';
    }

    var fromDate = nlapiStringToDate(dataIn.from.replace(/-/g, "/"));
    var toDate = nlapiStringToDate(dataIn.to.replace(/-/g, "/"));
    var sColumns = new Array();
    var sFilters = new Array();

    sColumns.push(new nlobjSearchColumn('shipdate'));
    sColumns.push(new nlobjSearchColumn('status'));
    sColumns.push(new nlobjSearchColumn('trandate'));
    sColumns.push(new nlobjSearchColumn('tranid'));
    sColumns.push(new nlobjSearchColumn('item'));
    sColumns.push(new nlobjSearchColumn('entity'));
    sColumns.push(new nlobjSearchColumn('location'));
    sColumns.push(new nlobjSearchColumn('line'));
    sColumns.push(new nlobjSearchColumn('quantity'));
    sColumns.push(new nlobjSearchColumn('mainline'));
    sColumns.push(new nlobjSearchColumn('subsidiary'));
    if (dataIn.getTransferOrders == 'True') {
        sColumns.push(new nlobjSearchColumn('transferlocation'));
        sColumns.push(new nlobjSearchColumn('transferorderquantityreceived'));
    } else {
		    sColumns.push(new nlobjSearchColumn('custcol_ftis_orgreqdate'));
    	sColumns.push(new nlobjSearchColumn('custcol_ftis_etawarehouse'));
    	sColumns.push(new nlobjSearchColumn('custcol_request_date'));
	}
    sColumns.push(new nlobjSearchColumn('location'));
    sColumns.push(new nlobjSearchColumn('quantityshiprecv'));

    sColumns.push(new nlobjSearchColumn('actualshipdate'));
    sColumns.push(new nlobjSearchColumn('expectedReceiptDate'));

    var sFilterExpression = new Array();
    if (dataIn.getTransferOrders == 'True') {
        sFilterExpression = [['trandate', 'onorafter', fromDate], 'AND', ['trandate', 'before', toDate], 'AND', ['status', 'anyof', ['TrnfrOrd:F']]];
    } else {
        sFilterExpression = [['trandate', 'onorafter', fromDate], 'AND', ['trandate', 'before', toDate]];
    }
    
    var search = nlapiCreateSearch(sType, sFilterExpression, sColumns);
    var resultSet = search.runSearch();
    // consider using nlapiCreateSearch if expected searchresult is more than 1000

    if (resultSet == null) return '';

    var DeliveryDate, SOstatus, OrderDate, OrderNum, VendorCode, Location;
    var ItemCode, OrderLine, DeliveryQuantity;
    var ShipDate, ShipQuantity;
    var DataOut = new Array();

    var pageSize = 1000;
    var fromIndex = 0;

    while (true) {
        nlapiLogExecution('DEBUG', 'RESTlet', 'Get Results' + fromIndex + "," + pageSize);
        var sr = resultSet.getResults(fromIndex, fromIndex + pageSize);
        if (dataIn.getTransferOrders == 'True') { nlapiLogExecution('DEBUG', 'RESTlet', 'sr' + JSON.stringify(sr)); }

        if (sr == null || sr.length == 0) {
            break;
        }

        fromIndex += sr.length;
        for (var iLP = 0; iLP < sr.length; iLP++) {
            if (sr[iLP].getValue('mainline').equals('*')) {
                SOstatus = sr[iLP].getValue('status');
                VendorCode = sr[iLP].getValue('entity');
                OrderDate = sr[iLP].getValue('trandate');
                OrderNum = sr[iLP].getValue('tranid');
                if (dataIn.getTransferOrders == 'True') {
                    Location = sr[iLP].getValue('transferlocation');
                } else {
                    Location = sr[iLP].getValue('location');
                }

            }
            else if (!(sr[iLP].getValue('mainline').equals('*'))) {
                ItemCode = sr[iLP].getValue('item');
                OrderLine = sr[iLP].getValue('line');

                DeliveryQuantity = sr[iLP].getValue('quantity');
                if (dataIn.getTransferOrders == 'True') {
                    ShipDate = sr[iLP].getValue('shipdate');
                    ShipQuantity = sr[iLP].getValue('transferorderquantityreceived');
                } else {
                    ShipDate = sr[iLP].getValue('actualshipdate');
                    ShipQuantity = sr[iLP].getValue('quantityshiprecv');
                }

                var sOut = {};

                //INTERNAL ID NEW CODE
                sOut['ProductInternalId'] = ItemCode;
                sOut['LocationInternalId'] = Location;
                sOut['VendorInternalId'] = VendorCode;
                sOut['OrderNumber'] = OrderNum + '-' + OrderLine;

                sOut['Status'] = SOstatus;
                sOut['OrderDate'] = OrderDate;
                sOut['ItemCode'] = ItemCode;
                sOut['VendorCode'] = VendorCode;
                sOut['Quantity'] = DeliveryQuantity;

                sOut['ShipDate'] = ShipDate;
                sOut['ShipQuantity'] = ShipQuantity;
                sOut['Location'] = Location;

                if (dataIn.getTransferOrders == 'True') {
                    DeliveryDate = sr[iLP].getValue('expectedreceiptdate');
                } else if (sr[iLP].getValue('custcol_ftis_etawarehouse') && sr[iLP].getValue('custcol_ftis_etawarehouse') != '') {
                    DeliveryDate = sr[iLP].getValue('custcol_ftis_etawarehouse');
                } else {
                    DeliveryDate = sr[iLP].getValue('custcol_request_date');
                 // nlapiLogExecution('DEBUG', 'RESTlet', OrderNum + '-' + OrderLine + '  reqDate: ' + sr[iLP].getValue('custcol_request_date'));
                    if (sr[iLP].getValue('custcol_request_date') != '') {
                        var dateArray = DeliveryDate.split('/'); // ftis date format 'dd-mm-yyy'
                        var calculatedEstimateDate = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
                        calculatedEstimateDate.setDate(calculatedEstimateDate.getDate() + 90);
                        var mm = calculatedEstimateDate.getMonth() + 1;
                        var dd = calculatedEstimateDate.getDate();
                        var yyyy = calculatedEstimateDate.getFullYear();
                        DeliveryDate = dd + '/' + mm + '/' + yyyy;
                      //nlapiLogExecution('DEBUG', 'RESTlet', OrderNum + '-' + OrderLine + '     finalDeliveryDAste: ' + DeliveryDate);
                    }

                    sOut['Status'] = 'dcplanned';
                }
                sOut['DeliveryDate'] = DeliveryDate;

                DataOut.push(sOut);
            }
        }
    }
    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getRemainingUsage());

    return DataOut;
}

