function getLocations(dataIn) {	
	var currentContext = nlapiGetContext();
	nlapiLogExecution('DEBUG','RESTlet', currentContext.getExecutionContext());
	
	var sType = 'location';

	//13	CONSIGN-SG x
	//3     CONSIGN-TH x
	//15	FT-NZ x
	//16	FT-SG x
	//5     FT-TH x
	//	    JABIL - does not exist ot live
    //      TH-VMI - does not exist on live
	//17	VMI-JABIL x
	//18	VMI-NZ x
	//11	VMI-TH x
    //12	WIP x
    //14    DROPSHIP-SG x
    //24    VMI-TH : BILL-TH x
    //add 1 and 2 because of the old coftis file

	var sColumns = new Array();

    var sFilters = ['internalid', 'anyof', ['1', '2', '3', '5', '11', '12', '13', '14', '15', '16', '17', '18', '24']];

	sColumns.push(new nlobjSearchColumn('internalId'));
	sColumns.push(new nlobjSearchColumn('name'));

	var search = nlapiCreateSearch(sType, sFilters, sColumns);
	var resultSet = search.runSearch();
	
	if(resultSet == null)	
		return '';
	
	var DataOut = new Array();
	
	var sr = resultSet.getResults(dataIn.fromIndex, dataIn.toIndex);

	if(sr == null || sr.length == 0){
		return '';
	}
	
	for (var i=0; i<sr.length; i+=1) {			
	    var sOut = {};


	    //INTERNAL ID NEW CODE
	    sOut['InternalId'] = sr[i].getValue('internalId');

		sOut['Code'] = sr[i].getValue('internalid');
		sOut['Description'] = sr[i].getValue('name');

		DataOut.push(sOut);		
	}

	nlapiLogExecution('DEBUG','RESTlet', currentContext.getRemainingUsage());
	
	return DataOut;
}
	

