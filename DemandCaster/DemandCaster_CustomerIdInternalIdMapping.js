function getCustomers(dataIn) {
    var currentContext = nlapiGetContext();
    var DataOut = new Array();
    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getExecutionContext());

    var sType = 'customer';

    var sColumns = new Array();
    sColumns.push(new nlobjSearchColumn('internalId'));
    sColumns.push(new nlobjSearchColumn('entityid'));
    sColumns.push(new nlobjSearchColumn('companyname'));
    sColumns.push(new nlobjSearchColumn('firstname'));
    sColumns.push(new nlobjSearchColumn('lastname'));

    var search = nlapiCreateSearch(sType, null, sColumns);
    var resultSet = search.runSearch();
    if (resultSet == null) return '';

    var sr = resultSet.getResults(dataIn.fromIndex, dataIn.toIndex);

    if (sr == null || sr.length == 0) {
        return '';
    }

    for (var i = 0; i < sr.length; i += 1) {
        var sOut = {};
        var customerId = sr[i].getValue('companyname');
        if (!customerId.trim()) {
            customerId = sr[i].getValue('firstname') + ' ' + sr[i].getValue('lastname');
        }
        customerId = sr[i].getValue('entityid') + ' ' + customerId;
        sOut['InternalId'] = sr[i].getValue('internalId');
        sOut['CustomerId'] = customerId;
        DataOut.push(sOut);
    }

    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getRemainingUsage());

    return DataOut;
}