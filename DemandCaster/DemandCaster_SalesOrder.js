function salesOrderGet(dataIn) {
    var currentContext = nlapiGetContext();
    var sType = dataIn.type;
    var sColumns = new Array();
    var from = dataIn.from.replace(/-/g, "/");
    var to = dataIn.to.replace(/-/g, "/");

    nlapiLogExecution('DEBUG', 'RESTlet', 'Starting');

    sColumns.push(new nlobjSearchColumn('entityid', 'customer'));
    sColumns.push(new nlobjSearchColumn('companyname', 'customer'));
    sColumns.push(new nlobjSearchColumn('firstname', 'customer'));
    sColumns.push(new nlobjSearchColumn('lastname', 'customer'));
    sColumns.push(new nlobjSearchColumn('entity'));
    sColumns.push(new nlobjSearchColumn('shipdate'));
    sColumns.push(new nlobjSearchColumn('status'));
    sColumns.push(new nlobjSearchColumn('trandate'));
    sColumns.push(new nlobjSearchColumn('tranid'));
    sColumns.push(new nlobjSearchColumn('rate'));
    sColumns.push(new nlobjSearchColumn('item'));
    sColumns.push(new nlobjSearchColumn('line'));
    sColumns.push(new nlobjSearchColumn('quantity'));
    sColumns.push(new nlobjSearchColumn('mainline'));
    sColumns.push(new nlobjSearchColumn('location'));
    sColumns.push(new nlobjSearchColumn('custcol_ftis_crd'));
    sColumns.push(new nlobjSearchColumn('quantity', 'fulfillingtransaction'));
    sColumns.push(new nlobjSearchColumn('trandate', 'fulfillingtransaction'));
    //sColumns.push(new nlobjSearchColumn('custcol_ftis_customerpartno'));

    nlapiLogExecution('DEBUG', 'RESTlet', 'Columns');

    var filter = [['trandate', 'onorafter', from], 'AND', ['trandate', 'before', to]];
    var search = nlapiCreateSearch(sType, filter, sColumns);
    var resultSet = search.runSearch();
    // consider using nlapiCreateSearch if expected searchresult is more than 1000
    if (resultSet == null) return '';

    var SOCustomer, SOendDate, SOstatus, SOtranDate, SOtranId, SOLocation;
    var SOIamount, SOIitem, SOIline, SOIquantity, SOICustPartNo, SOICIRInternalId, SOCustomerInternalId;
    var IFtrandate, IFquantity;
    var DataOut = new Array();

    var pageSize = 1000;
    var reachedEnd = false;
    var fromIndex = 0;
    while (true) {
        var sr = resultSet.getResults(fromIndex, fromIndex + pageSize);
        // Get SalesOrder Data

        if (sr == null || sr.length == 0) {
            break;
        }

        nlapiLogExecution('DEBUG', 'RESTlet', 'In the while');
        fromIndex += pageSize;
        for (var iLP = 0; iLP < sr.length; iLP++) {
            if (sr[iLP].getValue('mainline').equals('*')) {
                SOtranDate = sr[iLP].getValue('trandate');
                SOCustomerInternalId = sr[iLP].getValue('entity');

                var name = sr[iLP].getValue('companyname', 'customer');
                if (!name.trim()) {
                    name = sr[iLP].getValue('firstname', 'customer') + ' ' + sr[iLP].getValue('lastname', 'customer');
                }
                SOCustomer = sr[iLP].getValue('entityid', 'customer') + ' ' + name;
               
                SOstatus = sr[iLP].getValue('status');
                SOtranId = sr[iLP].getValue('tranid');
                SOLocation = sr[iLP].getValue('location');
            }
            else if (!(sr[iLP].getValue('mainline').equals('*'))) {
                SOendDate = sr[iLP].getValue('custcol_ftis_crd');
                SOIamount = sr[iLP].getValue('rate');
                //SOICIRInternalId = sr[iLP].getValue('custcol_ftis_customerpartno');
                SOIitem = sr[iLP].getValue('item');
                //SOICustPartNo = sr[iLP].getValue('custcol_ftis_customerpartno');
                SOIline = sr[iLP].getValue('line');
                SOIquantity = sr[iLP].getValue('quantity');

                IFtranDate = sr[iLP].getValue('trandate', 'fulfillingtransaction');
                IFquantity = sr[iLP].getValue('quantity', 'fulfillingtransaction');


                var sOut = {};

                //INTERNAL ID NEW CODE
                sOut['ProductInternalId'] = SOIitem;
                sOut['LocationInternalId'] = SOLocation;
                sOut['CustomerInternalId'] = SOCustomerInternalId;

                sOut['OrderNumber'] = SOtranId + '_' + SOIline;
                sOut['Status'] = SOstatus;
                sOut['OrderDate'] = SOtranDate;
                sOut['CustomerCode'] = SOCustomer;
                sOut['ProductCode'] = SOIitem;
                sOut['Quantity'] = SOIquantity;
                sOut['DueDate'] = SOendDate;
                sOut['ShipDate'] = IFtranDate;
                sOut['ShipQuantity'] = IFquantity;
                sOut['Price'] = SOIamount;
                sOut['Location'] = SOLocation;

                DataOut.push(sOut);
            }
        }
    }

    return DataOut;
}