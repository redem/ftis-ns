function suggestedOrderUpload(dataIn) {
    var currentContext,
        workOrder,
        purchaseOrder,
        transferOrder,
        id;

    currentContext = nlapiGetContext();
    if (dataIn.NetSuiteOrderType === 1) {
        nlapiLogExecution('DEBUG', 'PO', 1);
        purchaseOrder = nlapiCreateRecord('purchaseorder');
        purchaseOrder.setFieldValue('externalId', dataIn.NsSuggestedOrderId);
        purchaseOrder.setFieldValue('trandate', dataIn.DateString.replace(/-/g, "/"));
        purchaseOrder.setFieldValue('entity', dataIn.VendorCode);
        purchaseOrder.setFieldValue('location', dataIn.Location);
      //  purchaseOrder.setFieldValue('subsidiary', getSubsidiaryInternalId(dataIn.Location));
        dataIn.SuggestedOrderItems.forEach(function (orderItem) {
            purchaseOrder.selectNewLineItem('item');
            purchaseOrder.setCurrentLineItemValue('item', 'item', orderItem.ItemCode);
            purchaseOrder.setCurrentLineItemValue('item', 'quantity', orderItem.Quantity);
            purchaseOrder.setCurrentLineItemValue('item', 'location', orderItem.Location);
            purchaseOrder.setCurrentLineItemValue('item', 'vendorname', dataIn.VendorName);
            purchaseOrder.commitLineItem('item');
        });
        id = nlapiSubmitRecord(purchaseOrder, 'T');
        if (id > 0) {
            return dataIn.NsSuggestedOrderId;
        }
    } else if (dataIn.NetSuiteOrderType === 2) {
       nlapiLogExecution('DEBUG', 'TO', 1);
        transferOrder = nlapiCreateRecord('transferorder');
        transferOrder.setFieldValue('externalId', dataIn.NsSuggestedOrderId);
        transferOrder.setFieldValue('trandate', dataIn.DateString.replace(/-/g, "/"));
        transferOrder.setFieldValue('location', dataIn.SourceLocation);
        transferOrder.setFieldValue('transferlocation', dataIn.DestinationLocation);
        //transferOrder.setFieldValue('subsidiary', getSubsidiaryInternalId(dataIn.SourceLocation));

        dataIn.SuggestedOrderItems.forEach(function (orderItem) {
            transferOrder.selectNewLineItem('item');
            transferOrder.setCurrentLineItemValue('item', 'item', orderItem.ItemCode);
            transferOrder.setCurrentLineItemValue('item', 'quantity', orderItem.Quantity);
            transferOrder.setCurrentLineItemValue('item', 'location', orderItem.Location);
            transferOrder.commitLineItem('item');
        });
        id = nlapiSubmitRecord(transferOrder, 'T');
        if (id > 0) {
            return dataIn.NsSuggestedOrderId;
        }
    } else if (dataIn.NetSuiteOrderType === 3) {
               nlapiLogExecution('DEBUG', 'WO', 1);
        workOrder = nlapiCreateRecord('workorder');
        workOrder.setFieldValue('externalId', dataIn.NsSuggestedOrderId);
        workOrder.setFieldValue('trandate', dataIn.DateString.replace(/-/g, "/"));
        workOrder.setFieldValue('location', dataIn.Location);
        workOrder.setFieldValue('assemblyitem', dataIn.Assembly);
       // workOrder.setFieldValue('subsidiary', getSubsidiaryInternalId(dataIn.Location));
        workOrder.setFieldValue('expandassembly', 'T');
        id = nlapiSubmitRecord(workOrder, 'T');
        if (id > 0) {
            return dataIn.NsSuggestedOrderId;
        }
    }

    return 0;
}

function getSubsidiaryInternalId(locationInternalId) {
    var subtype, subColumns, locColumns, subFilter, subSearch, locationSearch, resultSet, searchResult;

    locColumns = new Array();
    locColumns.push(new nlobjSearchColumn('subsidiary'));

    locationSearch = nlapiCreateSearch('location', ['internalid', 'is', locationInternalId], locColumns);
    resultSet = locationSearch.runSearch();
    searchResult = resultSet.getResults(0, 1);

     nlapiLogExecution('DEBUG', 'RESTlet-locationid', locationInternalId);
    nlapiLogExecution('DEBUG', 'RESTlet', searchResult[0].getValue('subsidiary'));

    subColumns = new Array();
    subColumns.push(new nlobjSearchColumn('internalid'));
    subSearch = nlapiCreateSearch('subsidiary', ['name', 'contains', searchResult[0].getValue('subsidiary')], subColumns);
    resultSet = subSearch.runSearch();
    searchResult = resultSet.getResults(0, 1);

    return searchResult[0].getValue('internalId');
}