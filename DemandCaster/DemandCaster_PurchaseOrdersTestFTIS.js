function getRESTlet(dataIn) {
    var currentContext = nlapiGetContext();
   

    var sType = 'purchaseorder';
    var fromDate = nlapiStringToDate(dataIn.from);
    var toDate = nlapiStringToDate(dataIn.to);


    var sColumns = new Array();
    var sFilters = new Array();

    sColumns.push(new nlobjSearchColumn('shipdate'));
    sColumns.push(new nlobjSearchColumn('status'));
    sColumns.push(new nlobjSearchColumn('trandate'));
    sColumns.push(new nlobjSearchColumn('tranid'));
    sColumns.push(new nlobjSearchColumn('item'));
    sColumns.push(new nlobjSearchColumn('entity'));
    sColumns.push(new nlobjSearchColumn('location'));
    sColumns.push(new nlobjSearchColumn('line'));
    sColumns.push(new nlobjSearchColumn('quantity'));
    sColumns.push(new nlobjSearchColumn('mainline'));
    sColumns.push(new nlobjSearchColumn('quantityshiprecv'));
    sColumns.push(new nlobjSearchColumn('actualshipdate'));
    sColumns.push(new nlobjSearchColumn('expectedReceiptDate'));
    sColumns.push(new nlobjSearchColumn('custcol_ftis_orgreqdate'));
    sColumns.push(new nlobjSearchColumn('custcol_ftis_customerpartno'));

    //var sFilterExpression = [ [['applyinglinktype','is',['ShipRcpt']],'OR',['mainline','is','T']] , 'AND', ['trandate','within',fromDate, toDate]];
    var sFilterExpression = [['trandate', 'onorafter', fromDate], 'AND', ['trandate', 'before', toDate]];
    var search = nlapiCreateSearch(sType, sFilterExpression, sColumns);
    var resultSet = search.runSearch();
    // consider using nlapiCreateSearch if expected searchresult is more than 1000

    if (resultSet == null) return '';

    var DeliveryDate, SOstatus, OrderDate, OrderNum, VendorCode, Location, SOICustPartNo;
    var ItemCode, OrderLine, DeliveryQuantity;
    var ShipDate, ShipQuantity;
    var DataOut = new Array();

    var pageSize = 1000;
    var reachedEnd = false;
    var fromIndex = 0;

    //nlapiLogExecution('DEBUG', 'RESTlet', 'Starting while');
    var openStatusList = 
    [
        "partiallyreceived",
        "pendingbillingpartiallyreceived",
        "pendingreceipt",
        "pendingsupervisorapproval",
        "pendingfulfillment",
        "partiallyfulfilled",
        "pendingreceiptpartfulfilled",
        "pendingsupapproval",
        "pendingbillpartreceived",
        "pendingbilling"
    ];

    while (true) {
        nlapiLogExecution('DEBUG', 'RESTlet', 'Get Results' + fromIndex + "," + pageSize);
        var sr = resultSet.getResults(fromIndex, fromIndex + pageSize);
        
        if (sr == null || sr.length == 0) {
            break;
        }

        fromIndex += sr.length;
        for (var iLP = 0; iLP < sr.length; iLP++) {
            if (sr[iLP].getValue('mainline').equals('*')) {
                //nlapiLogExecution('DEBUG','Search Result Mainline ', iLP + ' ' + sr[iLP].getValue('mainline'));
                //DeliveryDate = sr[iLP].getValue('shipdate');
                SOstatus = sr[iLP].getValue('status');
                VendorCode = sr[iLP].getValue('entity');
                OrderDate = sr[iLP].getValue('trandate');
                OrderNum = sr[iLP].getValue('tranid');
                Location = sr[iLP].getValue('location');
            }
            else if (!(sr[iLP].getValue('mainline').equals('*'))) {
                //	nlapiLogExecution('DEBUG','Search Result Item Line', iLP + ' ' );
SOICustPartNo = sr[iLP].getValue('custcol_ftis_customerpartno');
                ItemCode = sr[iLP].getValue('item');
                OrderLine = sr[iLP].getValue('line');
               
                DeliveryQuantity = sr[iLP].getValue('quantity');
                ShipDate = sr[iLP].getValue('actualshipdate');
                ShipQuantity = sr[iLP].getValue('quantityshiprecv');

                var sOut = {};

                //INTERNAL ID NEW CODE
                sOut['ProductInternalId'] = ItemCode;
                sOut['LocationInternalId'] = Location;
                sOut['VendorInternalId'] = VendorCode;

                sOut['OrderNumber'] = OrderNum + '-' + OrderLine;
                sOut['Status'] = SOstatus;
                sOut['OrderDate'] = OrderDate;
                sOut['ItemCode'] = SOICustPartNo !== '' ? 'CIR-' + SOICustPartNo : ItemCode;
                sOut['VendorCode'] = VendorCode;
                sOut['Quantity'] = DeliveryQuantity;
                
                sOut['ShipDate'] = ShipDate;
                sOut['ShipQuantity'] = ShipQuantity;
                sOut['Location'] = Location;

                if (openStatusList.indexOf(SOstatus.toLowerCase()) > -1) {
                    DeliveryDate = sr[iLP].getValue('expectedReceiptDate');
                } else {
                    DeliveryDate = sr[iLP].getValue('custcol_ftis_orgreqdate');
                }
                sOut['DeliveryDate'] = DeliveryDate;

                DataOut.push(sOut);
            }
        }
    }
    nlapiLogExecution('DEBUG', 'RESTlet', currentContext.getRemainingUsage());

    return DataOut;
}
