/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Aug 2012     ljamolod
 *
 */

if (!_4601) { var _4601 = {}; }

_4601.getResourceManager = (function(language) {
	function getResourceManager(language){
		var fileId = null;
		var file = null;
		
		var fileName = 'wi_tax_resfile';
		var userLanguage = language || nlapiGetContext().getPreference('LANGUAGE');
		var fileExtension = 'json.txt';
		
		var resourceFileName =  fileName +"."+ userLanguage + "."+fileExtension;
		
		var result = nlapiSearchRecord( 'file', null, [new nlobjSearchFilter('name', null, 'is', resourceFileName)], [new nlobjSearchColumn('internalid')]);
		
		var resourceObject = null;
		if (result) {
			fileId = result[0].getValue("internalid");
	        file = nlapiLoadFile(fileId);
		} else {
			userLanguage = 'en_US';
			resourceFileName =  fileName +"."+ userLanguage + "."+fileExtension;
			result = nlapiSearchRecord( 'file', null, [new nlobjSearchFilter('name', null, 'is', resourceFileName)], [new nlobjSearchColumn('internalid')]);
			if (result) {
				fileId = result[0].getValue("internalid");
		        file = nlapiLoadFile(fileId);
			}
		}
		if (file){ resourceObject = getResourceManager.buildResourceObject( JSON.parse(file.getValue()) ); }
		
		return resourceObject;
	}
	
	getResourceManager.buildResourceObject = function buildResourceObject( resourcesArray ){
    	var resourceObject = {};
    	resourcesArray.forEach(function(resource){
    		getResourceManager.checkElements(resource);
    		resourceObject[resource.id] = resource;
    	});
    	return resourceObject;
    };
	
    getResourceManager.checkElements = function checkElements(resource){
    	for (var i in resource){
    		if(resource.hasOwnProperty(i)){
    			// check if element is an array...
    			if (typeof resource[i] === 'object' && resource[i].constructor == Array){
    				var  groupObject = getResourceManager.traverseArray(resource[i]); // array group...
    				resource[i] = groupObject;
    			}
    		}
    	}
    };
    
    getResourceManager.traverseArray = function traverseArray(arrayElements){
    	var memberVariableAsObject = {};
    	arrayElements.forEach(
    			function(arrayElement){
    				for (var i in arrayElement){
    					if(arrayElement.hasOwnProperty(i)){
    						if (typeof arrayElement[i] === 'object' && arrayElement[i].constructor == Array){
    							var  groupObject = traverseArray(arrayElement[i]); // array group...
    							arrayElement[i] = groupObject;
    						}
    					}
    				}
    				memberVariableAsObject[arrayElement.id] = arrayElement;
    			});
    	return memberVariableAsObject;
    };
	
	return getResourceManager;
}());


/**
 * Some languages have different positions when inserting variables
 * Replaces &<variable>
 * e.g. To replace variables in string:
 *   var attributes = {
 *   	 1: refNoContent //generated value 
 *   }
 *   var renderedMsg = _4601.renderTranslation(afterSubmit.resourceObject.TRANS_UE['field_msg'].WTAX_AMT_BILL.message, attributes);
 *   //message = Withholding Tax Amount for Bill &1
 */
_4601.renderTranslation = (function() {
	function renderTranslation(template, ds) {
        var content = template;

        for (var i in ds) {
            var re = new RegExp("\\&" + i, "g");
            content = content.replace(re, ds[i]);
        }

        return content;
	}
	
	return renderTranslation;
}());
	
	
