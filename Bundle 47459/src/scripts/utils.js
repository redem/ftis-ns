/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.context = nlapiGetContext();

_4601.doZeroOutCreditTransactionForWiTax = function doZeroOutCreditTransactionForWiTax(tranIdToVoid, voidingDesc, tranType, resourceObject) {
    var filters = [ new nlobjSearchFilter('custbody_4601_pymnt_ref_id', null, 'is', tranIdToVoid) ];
    var columns = [ new nlobjSearchColumn('internalid',null, 'GROUP')];
    var results = nlapiSearchRecord('transaction', null, filters, columns);
    if(results){
        results.forEach(
                function(result){
                    var recordType = tranType === 'purc' ? 'vendorcredit' : 'creditmemo';
                    
                    try{
                       nlapiVoidTransaction(recordType, result.getValue('internalid',null, 'GROUP'));
                    } catch(ex) {
                        throw nlapiCreateError("WITHHOLDING TAX: Error voiding transaction.");
                    }
                }
        );
    }
};

_4601.getCountry = function getCountry() {
    var nexusId = nlapiGetFieldValue('nexus');
    return nlapiLookupField('nexus', nexusId, 'country');
};

_4601.getWiTaxLineAppropriateVATCodeId = function getWiTaxLineAppropriateVATCodeId(resourceObject, nexus) {
    var isAdvancedTaxesFeatureEnabled = _4601.isAdvancedTaxesFeatureEnabled();
    var country = isAdvancedTaxesFeatureEnabled ? (nexus ? nlapiLookupField('nexus', nexus, 'country') : _4601.getCountry()) : '';
    var filters = [];
    var vatCodeId;

    if(['CA'].indexOf(country) > -1){
        var vatGroupMapper = new _4601.RecordMapper('taxgroup');
        filters = [
                   ['isinactive', 'is', 'F'],
                   ['country', 'is', 'CA']
                   ];

        vatGroupMapper.all({filters: filters}).some(function (mappedVATGroupCode) {
            var vatGroupCode = nlapiLoadRecord('taxgroup', mappedVATGroupCode.id);
            var taxItem1 = vatGroupCode.getFieldValue('taxitem1');
            var taxItem2 = vatGroupCode.getFieldValue('taxitem2');
            var vatCode1 = nlapiLoadRecord('salestaxitem', taxItem1); // nlapiLookupField doesn't work
            var vatCode2 = nlapiLoadRecord('salestaxitem', taxItem2); // nlapiLookupField doesn't work
            var isExcluded;

            isExcluded = vatCode1.getFieldValue('excludefromtaxreports') === 'T' && vatCode1.getFieldValue('rate') === '0.00%' &&
                            vatCode2.getFieldValue('excludefromtaxreports') === 'T' && vatCode2.getFieldValue('rate') === '0.00%';

            if (isExcluded) {
                vatCodeId = mappedVATGroupCode.id;
            }
            return isExcluded;
        });
    } else {
        var vatCodeMapper = new _4601.RecordMapper('salestaxitem')
        .addMapping('isinactive');
        filters = [
                    ['isinactive', 'is', 'F'],
                    ['formulanumeric', 'equalto', 1, null, 'CASE WHEN {rate} = 0 THEN 1 END']
                    ];

        var vatCodeId;

        if (isAdvancedTaxesFeatureEnabled) {
            vatCodeMapper.addMapping('country');
            filters.push(['country', 'is', country]);
        }

        vatCodeMapper.all({filters: filters}).some(function (mappedVATCode) {
            var vatCode = nlapiLoadRecord('salestaxitem', mappedVATCode.id);         // nlapiLookupField doesn't work
            var isExcluded; // with excludefromtaxreports

            if(['US'].indexOf(country) > -1){
                isExcluded = vatCode.getFieldValue('itemid') === '-Not Taxable-'; // For US sub, this must be present.
            } else {
                isExcluded = vatCode.getFieldValue('excludefromtaxreports') === 'T'; // with excludefromtaxreports
            }

            if (isExcluded) {
                vatCodeId = mappedVATCode.id;
            }
            return isExcluded;
        });
    }

    if (vatCodeId) {
        return vatCodeId;
    } else {
        var errorDetails;
        if(['US'].indexOf(country) > -1) {
            errorDetails = [
                            //'Please make sure that there is at least 1 tax code with the following settings for the current country/subsidiary:',
                            //'Rate = 0.00%',
                            //'Tax Name = -Not Taxable-',
                            //'Inactive = false (checkbox should NOT be marked)',
                            resourceObject.TRANS_UE['error_msg'].VAT_CODE_CHECK_US.message
                            ].join('\n');
        } else if(['CA'].indexOf(country) > -1) { 
            errorDetails = [
                            //'Please make sure that there is at least a tax group with GST/PST or HST tax codes with the following settings for the current country/subsidiary',
                            //'Rate = 0.00%',
                            //'Exclude From VAT Reports = true (checkbox should be marked)',
                            //'Inactive = false (checkbox should NOT be marked)',
                            resourceObject.TRANS_UE['error_msg'].VAT_CODE_CHECK_CA.message
                            ].join('\n');
        } else {
            errorDetails = [
                            //'Please make sure that there is at least 1 tax code with the following settings for the current country/subsidiary:',
                            //'Rate = 0.00%',
                            //'Exclude From VAT Reports = true (checkbox should be marked)',
                            //'Inactive = false (checkbox should NOT be marked)',
                            //'',
                            //'HINT: Usually the above settings can be found in a tax code with the name/code containing "UNDEF_"'
                            resourceObject.TRANS_UE['error_msg'].VAT_CODE_CHECK_OTHER_COUNTRIES.message
                            ].join('\n');
        }

        throw nlapiCreateError('WITHHOLDING TAXES: NO APPROPRIATE VAT CODE FOUND', errorDetails);
    }
};    

_4601.getCurrentUserName = function getCurrentUserName() {
    return _4601.context.getName();
};

_4601.isOneWorld = function isOneWorld() {
    return _4601.context.getFeature('SUBSIDIARIES');
};

_4601.isMultiLocationOn = function isMultiLocationOn() {
    return _4601.context.getFeature('MULTILOCINVT');
};

_4601.isMultiCurrencyOn = function isMultiCurrencyOn() {
    return _4601.context.getFeature('MULTICURRENCY');
};

_4601.isClassesFeatureEnabled = function isClassesFeatureEnabled() {
    return _4601.context.getSetting('FEATURE', 'CLASSES') === 'T';
};

_4601.isLocationsFeatureEnabled = function isLocationsFeatureEnabled() {
    return _4601.context.getSetting('FEATURE', 'LOCATIONS') === 'T';
};

_4601.isAdvancedTaxesFeatureEnabled = function isAdvancedTaxesFeatureEnabled() {
    return _4601.context.getFeature('ADVTAXENGINE');
};

_4601.isExternalCall = function isExternalCall() {
    return (['webservices', 'scheduled', 'suitelet', 'debugger', 'client'].indexOf(_4601.context.getExecutionContext()) > -1);
};

var _isUI = null;
_4601.isUI = function isUI() {
    if (_isUI === null) {
        _isUI = _4601.context.getExecutionContext() === 'userinterface';
    }
    return _isUI;
};

_4601.isCSVImport = function isCSVImport() {
    return _4601.context.getExecutionContext() === 'csvimport';
};

_4601.isWebServices = function isWebServices() {
    return _4601.context.getExecutionContext() === 'webservices';
};

_4601.isScheduled = function isScheduled() {
    return _4601.context.getExecutionContext() === 'scheduled';
};

_4601.isUserEvent = function isUserEvent() {
    return _4601.context.getExecutionContext() === 'userevent';
};

_4601.getWiTaxSublistNames = function getWiTaxSublistNames() {
    return ['item', 'expense'].filter(function (sublistName) {
        return nlapiGetSubList(sublistName);
    });
};

_4601.getRecordType = function getRecordType() {
    return nlapiGetRecordType().toLowerCase();
};

_4601.isExpenseSublist = function isExpenseSublist(sublistName) { return sublistName === 'expense'; };
_4601.isItemSublist = function isItemSublist(sublistName) { return sublistName === 'item'; };

_4601.getSublistWiTaxColumnsTotals = function getSublistWiTaxColumnsTotals(sublistName, wiTaxCustomColumnsTotalObject, tranType){
    var lineItemCount = nlapiGetLineItemCount(sublistName);
    var customColumnFieldPrefix = 'custcol_4601_';
    for (var i=1; i <= lineItemCount; i++) {
        var savedBaseAmount = parseFloat(nlapiGetLineItemValue(sublistName, customColumnFieldPrefix+ (_4601.isExpenseSublist(sublistName) ? 'witaxbamt_exp': 'witaxbaseamount'), i )|| 0);
        var savedTaxAmount = parseFloat(nlapiGetLineItemValue(sublistName, customColumnFieldPrefix+ (_4601.isExpenseSublist(sublistName) ? 'witaxamt_exp':'witaxamount'), i )|| 0);

        // retrieve values...
        var lineObject = _4601.getSublistWiTaxColumnValuesForDisplayAndForSave(tranType, {SavedBaseAmount:savedBaseAmount, SavedTaxAmount:savedTaxAmount});
        wiTaxCustomColumnsTotalObject.WiTaxAmountTotal += parseFloat(lineObject.ShownTaxAmount) || 0;
        wiTaxCustomColumnsTotalObject.WiTaxBaseAmountTotal += parseFloat(lineObject.ShownBaseAmount) || 0;
    }
};

_4601.getSublistWiTaxColumnValuesForDisplayAndForSave = function getSublistWiTaxColumnValuesForDisplayAndForSave(tranType, paramObj){
    var lineObject = {};
    if(tranType === 'purc'){
        // computed amounts were passed as parameters
        if(paramObj.hasOwnProperty("WiTaxBaseAmount")){
            lineObject.WiTaxBaseAmount = paramObj.WiTaxBaseAmount;
            lineObject.ShownBaseAmount = paramObj.WiTaxBaseAmount;
            lineObject.SavedBaseAmount = paramObj.WiTaxBaseAmount;
        }
        if(paramObj.hasOwnProperty("WiTaxAmount")){
            lineObject.WiTaxAmount = paramObj.WiTaxAmount;
            lineObject.ShownTaxAmount = paramObj.WiTaxAmount;
            lineObject.SavedTaxAmount = -paramObj.WiTaxAmount;
        }
        // saved amounts were passed as parameters...       
        if(paramObj.hasOwnProperty("SavedBaseAmount")){
            lineObject.SavedBaseAmount = paramObj.SavedBaseAmount; 
            lineObject.WiTaxBaseAmount = paramObj.SavedBaseAmount; 
            lineObject.ShownBaseAmount = paramObj.SavedBaseAmount; 
        }
        if(paramObj.hasOwnProperty("SavedTaxAmount")){
            lineObject.SavedTaxAmount = paramObj.SavedTaxAmount; 
            lineObject.WiTaxAmount = -paramObj.SavedTaxAmount; 
            lineObject.ShownTaxAmount = -paramObj.SavedTaxAmount; 
        }
        // shown amounts were passed as parameters...       
        if(paramObj.hasOwnProperty("ShownBaseAmount")){
            lineObject.ShownBaseAmount = paramObj.ShownBaseAmount; 
            lineObject.WiTaxBaseAmount = paramObj.ShownBaseAmount; 
            lineObject.SavedBaseAmount = paramObj.ShownBaseAmount;
        }
        if(paramObj.hasOwnProperty("ShownTaxAmount")){
            lineObject.ShownTaxAmount = paramObj.ShownTaxAmount; 
            lineObject.WiTaxAmount = paramObj.ShownTaxAmount; 
            lineObject.SavedTaxAmount = -paramObj.ShownTaxAmount;
        }

    }
    else{
        // computed amounts were passed as parameters
        if(paramObj.hasOwnProperty("WiTaxBaseAmount")){
            lineObject.WiTaxBaseAmount = paramObj.WiTaxBaseAmount;
            lineObject.ShownBaseAmount = paramObj.WiTaxBaseAmount;
            lineObject.SavedBaseAmount = -paramObj.WiTaxBaseAmount;
        }
        if(paramObj.hasOwnProperty("WiTaxAmount")){
            lineObject.WiTaxAmount = paramObj.WiTaxAmount;
            lineObject.ShownTaxAmount = -paramObj.WiTaxAmount;
            lineObject.SavedTaxAmount = -paramObj.WiTaxAmount;
        }
        // saved amounts were passed as parameters...       
        if(paramObj.hasOwnProperty("SavedBaseAmount")){
            lineObject.SavedBaseAmount = paramObj.SavedBaseAmount; 
            lineObject.WiTaxBaseAmount = -paramObj.SavedBaseAmount; 
            lineObject.ShownBaseAmount = -paramObj.SavedBaseAmount; 
        }
        if(paramObj.hasOwnProperty("SavedTaxAmount")){
            lineObject.SavedTaxAmount = paramObj.SavedTaxAmount; 
            lineObject.WiTaxAmount = -paramObj.SavedTaxAmount; 
            lineObject.ShownTaxAmount = paramObj.SavedTaxAmount; 
        }
        // shown amounts were passed as parameters...       
        if(paramObj.hasOwnProperty("ShownBaseAmount")){
            lineObject.ShownBaseAmount = paramObj.ShownBaseAmount; 
            lineObject.WiTaxBaseAmount = paramObj.ShownBaseAmount; 
            lineObject.SavedBaseAmount = -paramObj.ShownBaseAmount;
        }
        if(paramObj.hasOwnProperty("ShownTaxAmount")){
            lineObject.ShownTaxAmount = paramObj.ShownTaxAmount; 
            lineObject.WiTaxAmount = -paramObj.ShownTaxAmount; 
            lineObject.SavedTaxAmount = paramObj.ShownTaxAmount;
        }
    }
    return lineObject;
};


/**
 * Creates an array of applicable WTax Groups for the given Nexus.
 * Extracted from payment_ue.js: buildTaxGroupsLookup(paidTransactions)
 */
_4601.buildWiTaxGroupsLookup = function buildWiTaxGroupsLookup (reference) {
    var taxGroupsWithTaxCodes = {};
    var taxCodes = [];

    if (reference.paidTransactions) { // if called from payment_ue.js (nlobjSearchResult containing the transactions that are being paid)
        for (var i in reference.paidTransactions){
            if (reference.paidTransactions[i].getValue('custcol_4601_witaxcode') != ""){
                taxCodes.push(reference.paidTransactions[i].getValue('custcol_4601_witaxcode')); 
            }
        }
    } else if (reference.wiTaxSetup) { // if called from accrual_ue.js (id of the wiTaxSetup applicable to the transaction)
        var wiTaxCodeFilter = [
                               new nlobjSearchFilter("custrecord_4601_wtt_witaxsetup", "custrecord_4601_wtc_witaxtype", "is", reference.wiTaxSetup),
                               new nlobjSearchFilter("custrecord_4601_wtc_istaxgroup", null, "is", "T")
                               ];

        var wiTaxCodeRes = nlapiSearchRecord("customrecord_4601_witaxcode", null, wiTaxCodeFilter);

        if (wiTaxCodeRes && wiTaxCodeRes.length > 0) {
            wiTaxCodeRes.forEach(function (res) {
                taxCodes.push(res.getId());
            });
        }
    }

    // note: the tax group is also a taxcode (same internalid series)
    if (taxCodes.length > 0){
        var filters = [ new nlobjSearchFilter('custrecord_4601_gwtc_group', null, 'anyof', taxCodes) ];
        var columns = [
                       new nlobjSearchColumn('custrecord_4601_gwtc_group'),
                       new nlobjSearchColumn('custrecord_4601_gwtc_code'),
                       new nlobjSearchColumn('custrecord_4601_gwtc_basis'),
                       new nlobjSearchColumn('custrecord_4601_wtc_rate', 'CUSTRECORD_4601_GWTC_CODE'),                         
                       new nlobjSearchColumn('custrecord_4601_wtc_witaxtype', 'CUSTRECORD_4601_GWTC_CODE')
                       ];

        var results = nlapiSearchRecord('customrecord_4601_groupedwitaxcode', null, filters, columns);

        if (results){
            for (var i=0; i< results.length; i++){
                var taxGroupId = results[i].getValue('custrecord_4601_gwtc_group');
                var taxCodeInGroup = results[i].getValue('custrecord_4601_gwtc_code');

                var taxRateOfTaxCode = parseFloat(results[i].getValue('custrecord_4601_wtc_rate', 'CUSTRECORD_4601_GWTC_CODE')) || 0;
                taxRateOfTaxCode = taxRateOfTaxCode / 100;

                var taxBasis = parseFloat(results[i].getValue('custrecord_4601_gwtc_basis')) || 0;
                var taxType = results[i].getValue('custrecord_4601_wtc_witaxtype', 'CUSTRECORD_4601_GWTC_CODE');
                if(!taxGroupsWithTaxCodes[taxGroupId]){ taxGroupsWithTaxCodes[taxGroupId] = []; }
                taxGroupsWithTaxCodes[taxGroupId].push({ TaxGroupId: taxGroupId, TaxCode: taxCodeInGroup, 
                    TaxRate: taxRateOfTaxCode, TaxBasis: taxBasis, TaxType: taxType});
            }
        }
    }

    return taxGroupsWithTaxCodes;
};


_4601.getEntityType = function getEntityType() {
    var recordType = '';
    var entityId = nlapiGetFieldValue('entity');

    if (!!entityId) {
        var filters = new Array();
        var columns = new Array();

        filters.push(new nlobjSearchFilter('internalid', null, 'is', entityId));
        var result = nlapiSearchRecord('entity', null, filters, columns);

        if (result != null)
            recordType = result[0].getRecordType();
    }

    return recordType.toLowerCase();
};

_4601.getWiTaxCodeSubsidiaryFilter = function getWiTaxCodeSubsidiaryFilter(subsidiaryIds, includeChildSubs) {
    if (_4601.isOneWorld()) {
        var subsidiaryMapper = _4601.getSubsidiaryMapper();
        var allSubsidiaries = subsidiaryMapper.all({columns: ['name'], filters: [
                                                                                 ['isinactive', 'is', 'F'],
                                                                                 ['iselimination', 'is', 'F']
                                                                                 ]});
        var subsidiaryIdsIncludingChildren = subsidiaryMapper
        .filterByIds(allSubsidiaries, subsidiaryIds, includeChildSubs)
        .map(function (sub) {
            return sub.id;
        });

        return (function (wiTaxCode) {
            var wiTaxCodeSubIds = subsidiaryMapper
            .filterByIds(allSubsidiaries, wiTaxCode.subsidiaries, wiTaxCode.includechildsubs)
            .map(function (sub) {
                return sub.id;
            });
            return subsidiaryIdsIncludingChildren.some(function (subId) {
                return wiTaxCodeSubIds.indexOf(subId) > -1;
            });
        });
    } else {
        return (function () {
            return true;
        });
    }
};


_4601.getFirstWiTaxCodeInUse = function getFirstWiTaxCodeInUse(wiTaxSublistNames) {
    var wiTaxCodeInUse;

    wiTaxSublistNames.some(function (sublistName) {
        var lineItemCount = nlapiGetLineItemCount(sublistName);
        for (var i = 1; i <= lineItemCount; i++) {
            if (sublistName == 'expense')
                wiTaxCodeInUse = nlapiGetLineItemValue(sublistName, 'custcol_4601_witaxcode_exp', i);
            else
                wiTaxCodeInUse = nlapiGetLineItemValue(sublistName, 'custcol_4601_witaxcode', i);
            if (wiTaxCodeInUse) {
                break;
            }
        }
        return wiTaxCodeInUse
    });

    return wiTaxCodeInUse;
};


_4601.hideField = function hideField(fieldName) {
    var field = nlapiGetField(fieldName);

    if (field) {
        field.setDisplayType('hidden');
    }
};


_4601.hideLineItemField = function hideLineItemField(lineItemFieldName) {
    ['expense', 'item', 'itemcost', 'expcost', 'line'].forEach(function (sublistName) {
        var field = nlapiGetLineItemField(sublistName, lineItemFieldName);

        if (field) {
            field.setDisplayType('hidden');
        }
    });
};

_4601.checkAssocTransactions = function checkAssocTransactions(taxTypeId) {
    var filters = new Array();
    var columns = new Array();
    filters.push(new nlobjSearchFilter('custrecord_4601_wtc_witaxtype', 'custcol_4601_witaxcode', 'is', taxTypeId));
    columns.push(new nlobjSearchColumn('internalid'));
    var resAssocTrans = nlapiSearchRecord('transaction', null, filters, columns);
    return resAssocTrans != null ? resAssocTrans.length : 0;
};

_4601.formatCurrencyProxy = function formatCurrencyProxy(strCurrency, numberformat, negativeformat) {
    var numberformatted;
    strCurrency = ""+ nlapiFormatCurrency(strCurrency);
    x = strCurrency.split('.');
    x1 = x[0];
    var rgx;


    switch(numberformat)
    {
    case "0":
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    case "1":
        x2 = x.length > 1 ? ',' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    case "2":
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    case "3":
        x2 = x.length > 1 ? ',' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    case "4":
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + String.fromCharCode('1548') + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    case "5":
        x2 = x.length > 1 ? ',' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + String.fromCharCode('1548') + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    default:
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    }

    if(negativeformat == '1'){ //if User Preference Negative format is (-123)
        var rgx2 = /^-\d+(?:\.\d+)?$/; //check delimiter
        if(strCurrency.match(rgx2)){ //check if value is negative
            numberformatted = numberformatted.replace("-","(")+")";
        }
    }

    return numberformatted;
}

//for Number formatting, and Negative formatting
_4601.formatCurrency = function formatCurrency(strCurrency) {
    var numberformat = _4601.context.getSetting('SCRIPT','NUMBERFORMAT');
    var negativeformat = _4601.context.getSetting('SCRIPT','NEGATIVE_NUMBER_FORMAT');
    return _4601.formatCurrencyProxy(strCurrency, numberformat, negativeformat);
};

_4601.isUnsupportedItemType = function isUnsupportedItemType (type) {
    return ["subtotal", "endgroup", "description", "group"].indexOf(type.toLowerCase()) > -1;
};

_4601.hasHeaderDiscount = function hasHeaderDiscount () {
    return !!nlapiGetFieldValue("discountrate");
};

_4601.addGLImpactButton = function addGLImpactButton(form, request) {
    var recordType = _4601.getRecordType();
    var tranid = request.getParameter("id");
    var trantype = "";
    var url = request.getURL();
    var lastdawindex = url.lastIndexOf("/") + 1; // look for the last occurrence of the "/"
    var nlindex = url.indexOf(".nl"); // look for the ".nl" position
    trantype = url.substring(lastdawindex, nlindex); // retrieve the value in between...

    // when in view mode, add the GL Impact with WTax button, if and only if, there is an existing wtax-carrying journal entry related to it...
    if (_4601.hasWiTaxJournalEntry(tranid)) {
        var approvalStatus = recordType === "vendorbill" ? nlapiLookupField(recordType, tranid, "approvalstatus") : 2; // 1 - Pending Approval, 2 -  Approved
        if (approvalStatus == 2) {
            var glimpacturl = nlapiResolveURL("SUITELET", "customscript_4601_wh_gl_impact_s", "customdeploy_4601_wh_gl_impact_s");
            glimpacturl += "&tranid=" + tranid + "&trantype=" + trantype;
            var glimpactclick =  "javascript:document.location=\'" + glimpacturl + "\'";
            form.addButton("custpage_gli_prt_btn", "GL Impact with WTax", glimpactclick);
        }
    }
};

_4601.hasWiTaxJournalEntry = function hasWiTaxJournalEntry(id) {
    var filters = [];
    filters.push( new nlobjSearchFilter("custcol_ph4014_src_tranintid", null, "is", id) );
    filters.push( new nlobjSearchFilter("type", null, "is", "Journal") );
    var searchResults = nlapiSearchRecord("transaction", null, filters);

    var result = searchResults && searchResults.length > 0 ? true : false;
    return result;
};

function getUrlParamValue(name) {
    var matches = location.href.match(new RegExp('[?&]' + name + '=([^&]*)'));
    return matches && matches[1];
}

/**
 * Finds a class in a given namespace and returns an instance of that class.
 * @param {Object} parent - The topmost namespace.
 * @param {String} name - The full reference to the class, e.g. 'WTax.WTaxJobDAO'.
 * @param {Object} params - Optional parameter object to be passed to the class's constructor.
 * @returns {Object} - An instance of the class.
 */
function findClass(parent, name, params) {
    if (!name) {
        var error = nlapiCreateError('MISSING_REQ_PARAM', 'A class name is required.');
        WTaxError.throwError(error, 'findClass', WTaxError.LEVEL.ERROR);
    }
    
    if (!parent) {
        var error = nlapiCreateError('MISSING_REQ_PARAM', 'A parent object is required.');
        WTaxError.throwError(error, 'findClass', WTaxError.LEVEL.ERROR);
    }
    
    var Class = parent;
    
    try {
        var path = name.split('.');
        
        for (var i = 1; i < path.length; i++) {
            Class = Class[path[i]];
            
            if (Class == undefined) {
                throw 'CLASS_NOT_FOUND';
            }
        }
        
        if (typeof Class != 'function') {
            throw 'NOT_A_FUNCTION';
        }
        
        return new Class(params);
    } catch(e) {
        if (e == 'CLASS_NOT_FOUND') {
            var error = nlapiCreateError('CLASS_NOT_FOUND', 'Cannot find the class "' + name + '".');
            WTaxError.throwError(error, 'findClass', WTaxError.LEVEL.ERROR);
        } else if (e == 'NOT_A_FUNCTION') {
            var error = nlapiCreateError('NOT_A_FUNCTION', '"' + name + '" is not a function.');
            WTaxError.throwError(error, 'findClass', WTaxError.LEVEL.ERROR);
        } else {
            WTaxError.throwError(e, 'findClass', WTaxError.LEVEL.ERROR);
        }
    }
    
}

/*
 * 
- GUIDE FOR THE COMPUTATION OF THE FIELDS (Shown and saved Base Amount, Tax Amount per transaction type...)

[_4601.getSublistWiTaxColumnValuesForDisplayAndForSave = function getSublistWiTaxColumnValuesForDisplayAndForSave(tranType, paramObj)]

Normal Behavior on UI                   
Field           Transaction Type    Computed Amount    Displayed Amount    Saved Amount 
Base Amount     Purchase            positive           positive            positive 
                Purchase            negative           negative            negative 
                Sale                positive           positive            negative 
                Sale                negative           negative            positive 

Field           Transaction Type    Computed Amount    Displayed Amount    Saved Amount    Saved Based Amount
Tax Amount      Purchase            positive           positive            negative        positive
                Purchase            negative           negative            positive        negative
                Sale                negative           positive            positive        negative
                Sale                positive           negative            negative        positive

Discount Line created (Withholding Tax)     
Field           Transaction Type    Computed Total     Tax Amount          Sign of Amount Displayed  
Amount/Rate     Purchase            negative           negative  
                Purchase            positive           positive  
                Sale                positive           negative  
                Sale                negative           positive  

Extraction     
Field           Transaction Type    Computed Amount    Displayed Amount    Saved Amount 
Base Amount     Purchase            positive           positive            positive 
                Purchase            negative           negative            negative 
                Sale                positive           positive            negative 
                Sale                negative           negative            positive 

Field           Transaction Type    Computed Amount    Displayed Amount    Saved Amount   Saved Based Amount
Tax Amount      Purchase            positive           positive            negative       positive
                Purchase            negative           negative            positive       negative
                Sale                negative           positive            positive       negative
                Sale                positive           negative            negative       positive

 */
