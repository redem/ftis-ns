if (!_4601) { var _4601 = {}; }

_4601.createTransUEWarningMessage = function _createTransUEWarningMessage(resourceObject, recordType, lockType) {
    var message = '';

    if (lockType == 'onpayment') {
    	// Payment process is still in progress.
        message = _4601.getTransUEErrorMessage(resourceObject, 'WTAX_ON_PAYMENT_LOCK');
    } else {
        switch(recordType) {
	        case 'invoice':
	        	// "This invoice already has a payment net of withholding tax applied to it."
	            message = _4601.getTransUEErrorMessage(resourceObject, 'TRAN_LOCK_INVOICE_PAYMENT');
	        	break;
	        case 'vendorbill':
	        	// "This bill already has a payment net of withholding tax applied to it."
	            message = _4601.getTransUEErrorMessage(resourceObject, 'TRAN_LOCK_BILL_PAYMENT');
	        	break;
	        case 'creditmemo':
	        	// "This credit memo reflects the withholding tax amount for the associated invoice."
	            message = _4601.getTransUEErrorMessage(resourceObject, 'TRAN_LOCK_CREDIT_MEMO');
	        	break;
	        case 'vendorcredit':
	        	// "This bill credit reflects the withholding tax amount for the associated bill."
	            message = _4601.getTransUEErrorMessage(resourceObject, 'TRAN_LOCK_BILL_CREDIT');
	        	break;
	        default:
	        	nlapiLogExecution('ERROR', '_4601.createTransUEWarningMessage', 'Unsupported record type: [' + recordType + ']');
        }
    }

    return [
        '<b>', _4601.getTransUEErrorMessage(resourceObject, 'TRAN_LOCK_WARNING'), ":", '</b>',
        message,
        _4601.getTransUEErrorMessage(resourceObject, 'TRAN_LOCK') //" Editing is not allowed for this transaction. For more information contact your NetSuite Administrator.";
    ].join(' ');
};

_4601.getTransUEResource = function _getTransUEResource(resourceObject, resourceName) {
    return resourceObject.TRANS_UE[resourceName];
};

_4601.getTransUEErrorMessage = function _getTransUEErrorMessage(resourceObject, resourceName) {
    var errorMsgResource = _4601.getTransUEResource(resourceObject, 'error_msg');

    if (errorMsgResource) {
        return errorMsgResource[resourceName].message;
    }

    return '';
};
