/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Feb 2013     isalen
 *
 */

if (!_4601) { var _4601 = {}; }

_4601.wiTaxSetupCS = {};

(function () {
    var cs = _4601.wiTaxSetupCS;
    
    
    var purcFields = [
        "custrecord_4601_wts_notonpurcorders",
        "custrecord_4601_wts_purctaxpoint",
        "custrecord_4601_wts_purcappliesto"
    ];
    
    
    var saleFields = [
        "custrecord_4601_wts_notonsaleorders",
        "custrecord_4601_wts_saletaxpoint",
        "custrecord_4601_wts_saleappliesto"
    ];
    
    
    cs.disableField = function disableField(fieldName) {
        nlapiDisableField(fieldName, true);
    };
    
    
    cs.enableField = function enableField(fieldName) {
        nlapiDisableField(fieldName, false);
    };
    
    
    cs.pageInit = function () {
        if (nlapiGetFieldValue("custrecord_4601_wts_onpurcs") === "F") { purcFields.forEach(cs.disableField); }
        if (nlapiGetFieldValue("custrecord_4601_wts_onsales") === "F") { saleFields.forEach(cs.disableField); }
    };
    
    
    cs.fieldChanged = function (sublistName, fieldName, lineNum) {
        if (fieldName === "custrecord_4601_wts_onpurcs") {
            nlapiGetFieldValue(fieldName) === "F" ? purcFields.forEach(cs.disableField) : purcFields.forEach(cs.enableField);
        } else if (fieldName === "custrecord_4601_wts_onsales") {
            nlapiGetFieldValue(fieldName) === "F" ? saleFields.forEach(cs.disableField) : saleFields.forEach(cs.enableField);
        }
    };
})();