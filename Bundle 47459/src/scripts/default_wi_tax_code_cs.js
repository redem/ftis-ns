/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.defaultWiTaxCodeCS = {};

_4601.defaultWiTaxCodeCS.fieldChanged = function fieldChanged(sublistName, fieldName, linenum) {
    if (_4601.isUI() && !sublistName && fieldName === 'custpage_defaultwitaxfield') {
        var value = nlapiGetFieldValue('custpage_defaultwitaxfield');
        nlapiSetFieldValue('custentity_4601_defaultwitaxcode', value);
        nlapiSetFieldValue('custitem_4601_defaultwitaxcode', value);
    }
};
