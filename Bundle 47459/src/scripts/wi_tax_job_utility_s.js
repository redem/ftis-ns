/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

function main(request, response) {
    var actionType = request.getParameter('actionType');
    var util = new WTax.Job.Utility(request, response);
    
    switch(actionType) {
        case 'getJobUrl':
            var result = {};
            result.jobUrl = util.getJobUrl();
            response.write(JSON.stringify(result));
            break;
        case 'runJob':
            util.runJob();
            break;
        default:
            break;
    }
}


if (!WTax) { var WTax = {}; }
WTax.Job = WTax.Job || {};

WTax.Job.Utility = function Utility(request, response) {
    this.request = request;
    this.response = response;
};

WTax.Job.Utility.prototype.getJobUrl = function getJobUrl() {
    try {
        var baseURL = this.request.getURL().split('/app/')[0];
        var jobId = this.request.getParameter('jobId');
        
        return [baseURL, nlapiResolveURL('RECORD', WTax.DAO.WTaxJob.RECORD_TYPE, jobId)].join('');
    } catch(e) {
        WTaxError.throwError(e, 'WTax.Job.Utility.getJobUrl', WTaxError.LEVEL.ERROR);
    }
};


WTax.Job.Utility.prototype.runJob = function runJob() {
    try {
        var jobDao = new WTax.DAO.WTaxJobDao();
        var recordId = this.request.getParameter('recordId');
        var recordType = this.request.getParameter('recordType');
        var jobToBeUpdated = jobDao.getList({id : recordId}).pop();
        
        if (!jobToBeUpdated) {
            var error = nlapiCreateError('RECORD_DOES_NOT_EXIST', 'The job with id[' + recordId + '] and type [' + recordType + '] does not exist.');
            WTaxError.throwError(error, 'WTax.Job.Utility.runJob', WTaxError.LEVEL.ERROR);
        }
        
        var jobManager = new WTax.WTaxJobManager(jobDao);
        
        // Update job
        var job = new WTax.DAO.WTaxJob(jobToBeUpdated.id);
        job.status = WTax.DAO.WTaxJob.STATUS.PENDING;
        job.outputData = {};
        jobManager.updateJob(job);
        
        // Run the scheduled script
        new WTax.WTaxJobScheduler().run();
        
        nlapiSetRedirectURL('RECORD', WTax.DAO.WTaxJob.RECORD_TYPE, recordId);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.Job.Utility.runJob', WTaxError.LEVEL.ERROR);
    }
};
