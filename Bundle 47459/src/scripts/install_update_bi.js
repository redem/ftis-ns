/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.bundleInstall = {};

_4601.bundleInstall.beforeInstall = (function(){
	function beforeInstall() {
		beforeInstall.exceptMsgs = "*** BUNDLE BEFORE INSTALL PROCESS COMPLETED ***";
		beforeInstall.logMsgs = [];
		var withException = false;
		try{ beforeInstall.run(); }
	    catch(ex) { 
	        withException = true; 
		    beforeInstall.exceptMsgs = "*** UNEXPECTED ERROR ***<br/>"+ ex.toString();
	    }

	    nlapiLogExecution("DEBUG", "BUNDLE BEFORE INSTALL LOG ENTRIES",	beforeInstall.exceptMsgs +"<br/>"+
	            															"*********************<br/>"+ 
	            															"Log Trail: <br/>"+beforeInstall.logMsgs.join('<br/>') );

		if (withException){ throw nlapiCreateError("BUNDLE BEFORE INSTALL PROCESS ERROR", beforeInstall.logMsgs.join('<br/>')); }
	}
	
	beforeInstall.run = function run() {
		beforeInstall.checkFeatures();
    };
    
    beforeInstall.checkFeatures = function checkFeatures() {
    	var context = nlapiGetContext();
    	
    	if(!context.getFeature('ADVTAXENGINE')) {
    		beforeInstall.logMsgs.push("Feature validation error - Advanced Taxes<br/>");
    		throw nlapiCreateError('FEATURE VALIDATION ERROR',
					'Advanced Taxes is not enabled. Aborting bundle deprecation process.\n' +
    				'NOTE: To enable this feature, go to Setup > Company > Enable Features > Accounting > Advanced Features then tick the checkbox for Advanced Taxes.');
    	}
    	
    	beforeInstall.logMsgs.push("Completed validation of Features.<br/>");
    	
    };
    
    return beforeInstall;
})();

_4601.bundleInstall.afterInstall = (function(){

	function afterInstall(){
		afterInstall.logMsgs = [];
		afterInstall.exceptMsgs = "*** BUNDLE AFTER INSTALL PROCESS COMPLETED ***";
		
		var withException = false;
		var accountingPreferenceSetupObject = afterInstall.saveAccountingPreferenceSetup();
    	try{ afterInstall.run(); }
        catch(ex)
        { 
            withException = true; 
            if ( ex instanceof nlobjError )
            {
            	afterInstall.exceptMsgs = "*** SYSTEM ERROR ***<br/>"+
                                        "Exception Code: "+ex.getCode() + '<br/>' + 
                                        "Exception Details: "+ex.getDetails() + '<br/>' +
                                        "Exception Stack Trace: "+ex.getStackTrace() + '<br/>';
            }
            else{ afterInstall.exceptMsgs = "*** UNEXPECTED ERROR ***<br/>"+ ex.toString(); }
        }
        afterInstall.restoreAccountingPreferenceSetup( accountingPreferenceSetupObject );

        nlapiLogExecution("DEBUG", "BUNDLE AFTER INSTALL LOG ENTRIES",	afterInstall.exceptMsgs +"<br/>"+
                															"*********************<br/>"+ 
                															"Log Trail: <br/>"+afterInstall.logMsgs.join('<br/>') );
    	if (withException){ 
    		throw nlapiCreateError("BUNDLE AFTER INSTALL PROCESS ERROR", 
    				afterInstall.exceptMsgs +"<br/>"+"*********************<br/>"+ afterInstall.logMsgs.join('<br/>')
    		); 
    	}

	};

	afterInstall.run = function run()
    {
    	var arrayOfWtaxDataFromResource = afterInstall.getWtaxDataInfoFromResource( afterInstall.getSourceFileName() );
    	if (arrayOfWtaxDataFromResource){ arrayOfWtaxDataFromResource.forEach( afterInstall.processNexusSetupEntries ); }
    	else
    	{ 
    		throw nlapiCreateError("ERROR",  afterInstall.logMsgs.push("arrayOfWtaxDataFromResource is empty!<br/>No WTax data source found!<br/>"));
    	}
    };

    afterInstall.processNexusSetupEntries = function processNexusSetupEntries(wiNexusSetupObject){
        var wiNexusMapper = _4601.getNexusMapper();
        var wiNexusMapperWithTaxAgency = _4601.getNexusMapper();
        wiNexusMapperWithTaxAgency.addMapping('taxagency'); // added the tax agency map here... because this field is not searchable...

		if( _4601.isOneWorld()){
			var subsidiaryMapper = _4601.getSubsidiaryMapper();
			afterInstall.subsidiaryIds = subsidiaryMapper.all(
			        {  columns: [], 
			            filters: 
			            [ 
			                ['iselimination', 'is', 'F'], 
			                ['isinactive', 'is', 'F'], 
			                ['country', 'is', wiNexusSetupObject.country] 
			            ] 
			        }).map(function (subsidiary) { return subsidiary.id; });
		}        
        
        // 'nexusObject' is needed for the processing of the tax type and tax code later on, that's why it was made a property of the 'afterInstall' object.
        afterInstall.nexusObject = wiNexusMapper.all({
		filters: [['country', 'is', wiNexusSetupObject.country]],
		columns: [] 
        }).getFirst();

		if(afterInstall.nexusObject)
		{
			afterInstall.nexusObject = wiNexusMapperWithTaxAgency.loadRecord(afterInstall.nexusObject.id); // use the wiNexusMapperWithTaxAgency here because by loading the record, the tax agency field is now available...
			if (afterInstall.createNexusSetupRecord(wiNexusSetupObject))
			{
				// the nexusObject has been saved in the creation of the nexus setup.
				afterInstall.defaultTaxAgency = afterInstall.nexusObject.taxagency != null ? afterInstall.nexusObject.taxagency : afterInstall.assignDefaultTaxAgency();
				var wiTaxTypesOfNexus = wiNexusSetupObject.wiTaxTypes; // retrieve tax types of the nexus object...
				wiTaxTypesOfNexus.forEach( afterInstall.createWiTaxTypeOfNexus );
			}
		}
		else
		{
        	afterInstall.logMsgs.push("Target account doesn't have a nexus with the country #"+wiNexusSetupObject.country+"#. Skipping creation of Withholding tax entries for this nexus...<br/>");        	
		}
		
    };    
    
    afterInstall.assignDefaultTaxAgency = function assignDefaultTaxAgency() {
    	var filters = new Array();
    	var columns = new Array();

    	filters.push(new nlobjSearchFilter('country', null, 'is', 'PH'));

    	columns.push(new nlobjSearchColumn("internalid"));

    	var rs = nlapiSearchRecord("salestaxitem", null, filters, columns);

    	var n = nlapiLoadRecord('salestaxitem', rs[0].getId());

    	return n.getFieldValue('taxagency');
    };

    afterInstall.createNexusSetupRecord = function createNexusSetupRecord(wiTaxSetupObject){
    	var wiNexusSetupRecordCreated = false;
        var wiTaxSetupRecordMapper = _4601.getWiTaxSetupMapper();
        wiTaxSetupObject.nexus = afterInstall.nexusObject.id; // this is the 'nexusObject' property attached to the 'afterInstall' object...
        wiTaxSetupObject.purcappliesto = (wiTaxSetupObject.purcappliesto === "lines")?'F':'T';   // 'lines' or 'total' are the values taken from the resource file
        wiTaxSetupObject.saleappliesto = (wiTaxSetupObject.saleappliesto === "lines")?'F':'T';
        wiTaxSetupObject.id = "new";

        afterInstall.wiNexusSetupObjectId = wiTaxSetupRecordMapper.saveRecord(wiTaxSetupObject); // save the tax setup object id. needed later... 
        if( afterInstall.wiNexusSetupObjectId > 0 ){
        	afterInstall.logMsgs.push("Tax Setup/Nexus of #"+wiTaxSetupObject.country+"#, id:# "+afterInstall.wiNexusSetupObjectId+"# created!<br/>");        	
        	wiNexusSetupRecordCreated = true;
        }
        else{ afterInstall.logMsgs.push("Tax Setup/Nexus of #"+wiTaxSetupObject.country+"# NOT created!<br/>"); }
		return wiNexusSetupRecordCreated;
    };

    afterInstall.createWiTaxTypeOfNexus = function createWiTaxTypeOfNexus(wiTaxTypeObject){
		var recordMapper = _4601.getWiTaxTypeMapper();
		wiTaxTypeObject.id = "new";
		wiTaxTypeObject.taxtypekind = ""; // provide an empty value by default...
		wiTaxTypeObject.witaxsetup = afterInstall.wiNexusSetupObjectId;

		afterInstall.wiTaxTypeObjectId = recordMapper.saveRecord(wiTaxTypeObject); // save the tax type id to the afterInstall object...
		if (afterInstall.wiTaxTypeObjectId > 0)
		{
	    	afterInstall.logMsgs.push("Tax Type #"+wiTaxTypeObject.name+"#, id:#"+afterInstall.wiTaxTypeObjectId+"# created!<br/>");

	    	var wiTaxCodesOfTaxType = wiTaxTypeObject.wiTaxCodes;
	    	wiTaxCodesOfTaxType.forEach( afterInstall.createWiTaxCodeOfTaxType );
		}
        else{ afterInstall.logMsgs.push("Tax Type #"+wiTaxTypeObject.name+"#, id:#"+afterInstall.wiTaxTypeObjectId+"# NOT created!<br/>"); }
    };

    afterInstall.createWiTaxCodeOfTaxType = function createWiTaxCodeOfTaxType(wiTaxCodeObject)
    {
		var recordMapper = _4601.getWiTaxCodeMapper();
		wiTaxCodeObject.id = "new";
		wiTaxCodeObject.witaxtype = afterInstall.wiTaxTypeObjectId; // this was the variable that saved the internal id of the created tax type...
		wiTaxCodeObject.taxagency = afterInstall.defaultTaxAgency;

		if( _4601.isOneWorld()){
			wiTaxCodeObject.subsidiaries = afterInstall.subsidiaryIds;
			wiTaxCodeObject.includechildsubs = true;
		}

		if (wiTaxCodeObject.availableon === 'Both'){ wiTaxCodeObject.availableon = "both"; }
		else if(wiTaxCodeObject.availableon === 'Purchases'){ wiTaxCodeObject.availableon = "onpurcs"; }
		else if(wiTaxCodeObject.availableon === 'Sales'){wiTaxCodeObject.availableon = "onsales"; }

		if (recordMapper.saveRecord(wiTaxCodeObject) > 0 ){afterInstall.logMsgs.push("Tax Code #"+wiTaxCodeObject.name+"# created!<br/>"); }
		else { afterInstall.logMsgs.push("Tax Code #"+wiTaxCodeObject.name+"# NOT created!<br/>"); }
    };    

    afterInstall.getWtaxDataInfoFromResource = function getWtaxDataInfoFromResource(jsonSourceFile)
    {
        var fileId;
        var file;
        var jsonObject=null;

        if( jsonSourceFile ){
            var wtaxXmlFile = jsonSourceFile;
            var result = nlapiSearchRecord( 'file', null,[new nlobjSearchFilter('name', null, 'is', wtaxXmlFile)], [new nlobjSearchColumn('internalid')]);
            if (result){ 
            	fileId = result[0].getValue("internalid");
                file = nlapiLoadFile(fileId);
                if (file)
                {
                    jsonObject = JSON.parse(file.getValue());
                	afterInstall.logMsgs.push("Default Nexuses and Tax codes successfully retrieved. <br/>");
                };
            };
        }
        return jsonObject;
    };

    afterInstall.getSourceFileName = function getSourceFileName(){ return "default_wi_tax_data.json.txt"; };

    afterInstall.saveAccountingPreferenceSetup = function saveAccountingPreferenceSetup()
    {
        var accountingPreferenceObject = nlapiLoadConfiguration('accountingpreferences');
        accountingPreferenceObject.setFieldValue('ASSETCOGSITEMACCTS', 'T'); // make all COAs available in all transactions...
        var useAccountNumbers = (accountingPreferenceObject.getFieldValue('ACCOUNTNUMBERS') != 'T')?false:true; 
        nlapiSubmitConfiguration(accountingPreferenceObject);

        var accountingPreferenceSetupObject = {};
        accountingPreferenceSetupObject.UseAccountNumbers = useAccountNumbers;
        
    	afterInstall.logMsgs.push("Completed retrieval of Accounting Preference Setup information<br/>");
        return accountingPreferenceSetupObject;
    };	

    afterInstall.restoreAccountingPreferenceSetup = function restoreAccountingPreferenceSetup( accountingPreferenceSetupObject )
    {
    	if (accountingPreferenceSetupObject){
            var accountingPreferenceObject = nlapiLoadConfiguration('accountingpreferences');
            accountingPreferenceObject.setFieldValue('ACCOUNTNUMBERS', (accountingPreferenceSetupObject.UseAccountNumbers?'T':'F') );
            nlapiSubmitConfiguration(accountingPreferenceObject);         	
        	afterInstall.logMsgs.push("Completed restore of Accounting Preference Setup information<br/>");
    	}
    };    
	return afterInstall;
})(); 


_4601.bundleUpdate = {};

_4601.bundleUpdate.beforeUpdate = (function(){
	function beforeUpdate() {
		beforeUpdate.exceptMsgs = "*** BUNDLE BEFORE UPDATE PROCESS COMPLETED ***";
		beforeUpdate.logMsgs = [];
		var withException = false;
		try{ beforeUpdate.run(); }
	    catch(ex) { 
	        withException = true; 
	        beforeUpdate.exceptMsgs = "*** UNEXPECTED ERROR ***<br/>"+ ex.toString();
	    }

	    nlapiLogExecution("DEBUG", "BUNDLE UPDATE LOG ENTRIES",	beforeUpdate.exceptMsgs +"<br/>"+
	            															"*********************<br/>"+ 
	            															"Log Trail: <br/>"+beforeUpdate.logMsgs.join('<br/>') );

		if (withException){ throw nlapiCreateError("BUNDLE BEFORE UPDATE PROCESS ERROR", beforeUpdate.logMsgs.join('<br/>')); }
	}
	
	beforeUpdate.run = function run() {
		if (beforeUpdate.searchPhFields()) 
			beforeUpdate.checkTaxType();
		beforeUpdate.checkFeatures();
		beforeUpdate.checkAccountingPref();
    };
    
    beforeUpdate.checkAccountingPref = function checkAccountingPref() {
    	var accountingPreferenceObject = nlapiLoadConfiguration('accountingpreferences');
        if (accountingPreferenceObject.getFieldValue('ASSETCOGSITEMACCTS') == 'F') {
        	nlapiLogExecution('DEBUG', 'Accounting Preferences > Expand Account Lists is not enabled');
        	beforeUpdate.logMsgs.push("Accounting Preferences validation error - Expand Account Lists is not enabled<br/>");
        	throw nlapiCreateError('FEATURE VALIDATION ERROR',
					'Expand Account Lists is not enabled. Aborting bundle deprecation process.\n' +
    				'NOTE: To enable this feature, go to Setup > Accounting > Accounting Preferences then tick the checkbox for Expand Account Lists.');
        }
        
        beforeUpdate.logMsgs.push("Completed validation of Accounting Preferences.<br/>");
    };
    
    beforeUpdate.checkFeatures = function checkFeatures() {
    	var context = nlapiGetContext();
    	
    	if(!context.getFeature('ADVTAXENGINE')) {
    		beforeUpdate.logMsgs.push("Feature validation error - Advanced Taxes<br/>");
    		throw nlapiCreateError('FEATURE VALIDATION ERROR',
					'Advanced Taxes is not enabled. Aborting bundle deprecation process.\n' +
    				'NOTE: To enable this feature, go to Setup > Company > Enable Features > Accounting > Advanced Features then tick the checkbox for Advanced Taxes.');
    	}
    	
    	beforeUpdate.logMsgs.push("Completed validation of Features.<br/>");
    	
    };
    
    beforeUpdate.checkTaxType = function checkTaxType() {
    	var taxTypeFilters = new Array();
    	var taxTypeColumns = new Array();
    	
    	taxTypeColumns.push(new nlobjSearchColumn('custrecord_ph4014_wtax_type_pacct_h'));
    	taxTypeColumns.push(new nlobjSearchColumn('custrecord_ph4014_wtax_type_sacct_h'));
    	taxTypeColumns.push(new nlobjSearchColumn('custrecord_ph4014_wtax_type_name_h'));
    	
    	var rsTaxTypes = nlapiSearchRecord('customrecord_ph4014_wtax_type_h', null, taxTypeFilters, taxTypeColumns);
    	
    	if(rsTaxTypes == null) {
	    	throw nlapiCreateError("TAX TYPE RECORDS UPDATE ERROR",  
	    			                               "No Withholding Tax Type found. Aborting bundle deprecation process." );
		} else {
			for (var i in rsTaxTypes) {
				var wiTaxType = {
					id: rsTaxTypes[i].getId(),
					purcaccount: rsTaxTypes[i].getValue('custrecord_ph4014_wtax_type_pacct_h'),
					saleaccount: rsTaxTypes[i].getValue('custrecord_ph4014_wtax_type_sacct_h'),
					name: rsTaxTypes[i].getValue('custrecord_ph4014_wtax_type_name_h')
				};
				beforeUpdate.validateExistingTaxTypeRecord(wiTaxType);
	    	}
		}
		
		beforeUpdate.logMsgs.push("Completed validation of PH Withholding Tax Types.<br/>");
    };
    
	beforeUpdate.searchPhFields = function searchPhfields() {
		var hasPhFields = false;
		
		var filter = new Array();
		filter.push(new nlobjSearchFilter('scriptid', null, 'is', 'customrecord_ph4014_wtax_code'));

		var column = new Array();
		column.push(new nlobjSearchColumn("internalid"));
		column.push(new nlobjSearchColumn("name"));
		column.push(new nlobjSearchColumn("scriptid"));

		var PhRecord = nlapiSearchRecord('customrecordtype', null, filter, column);
		
		if(PhRecord == null)
			beforeUpdate.logMsgs.push('PH Bundle Check - PH Bundle is NOT available');
		else {
			beforeUpdate.logMsgs.push('PH Bundle Check - PH Bundle is available');
			hasPhFields = true;
		}
		
		return hasPhFields;
	};
	
    beforeUpdate.validateExistingTaxTypeRecord = function validateExistingTaxTypeRecord(wiTaxTypeObject) {
		if(wiTaxTypeObject) {
			nlapiLogExecution('DEBUG', 'Liability/Purchase Tax Account', wiTaxTypeObject.purcaccount);
			nlapiLogExecution('DEBUG', 'Asset/Sales Tax Account', wiTaxTypeObject.saleaccount);
			
			// Validation: Same posting accounts are not allowed
			if (wiTaxTypeObject.purcaccount && wiTaxTypeObject.saleaccount && wiTaxTypeObject.purcaccount == wiTaxTypeObject.saleaccount) {
				beforeUpdate.logMsgs.push(
						"Error validating Withholding Tax Type record #" + wiTaxTypeObject.id + "#<br/>"+
		    			"Tax Type Name is: #" + wiTaxTypeObject.name + "#<br/>");
				throw nlapiCreateError('TAX TYPE VALIDATION ERROR',
						'Liability/Purchase Tax Account and Asset/Sales Tax Account should post to different accounts. Aborting bundle deprecation process.');
				
			// Validation: Account type should be available to discount item 
			} else {
				var listAccountTypes = new Array();
				listAccountTypes.push('OthCurrAsset');
				listAccountTypes.push('FixedAsset');
				listAccountTypes.push('OthAsset');
				listAccountTypes.push('OthCurrLiab');
				listAccountTypes.push('LongTermLiab');
				listAccountTypes.push('Equity');
				listAccountTypes.push('Income');
				listAccountTypes.push('COGS');
				listAccountTypes.push('Expense');
				listAccountTypes.push('OthIncome');
				
				// Blank posting accounts should allow deprecation to continue
				if (wiTaxTypeObject.purcaccount) {
					var objPurcAccount = nlapiLoadRecord('account', wiTaxTypeObject.purcaccount);
					var typePurcAccount = objPurcAccount.getFieldValue('accttype');
					nlapiLogExecution('DEBUG', 'Purc Account Type', typePurcAccount);
					if (listAccountTypes.indexOf(typePurcAccount) == -1) {
						beforeUpdate.logMsgs.push(
								"Error validating Withholding Tax Type record #" + wiTaxTypeObject.id + "#<br/>"+
				    			"Tax Type Name is: #" + wiTaxTypeObject.name + "#<br/>");
						throw nlapiCreateError('TAX TYPE VALIDATION ERROR',
								'Liability/Purchase Tax Account has an invalid account type. Aborting bundle deprecation process.');
					}
				} else if (wiTaxTypeObject.saleaccount) {
					var objSaleAccount = nlapiLoadRecord('account', wiTaxTypeObject.saleaccount);
					var typeSaleAccount = objSaleAccount.getFieldValue('accttype');
					nlapiLogExecution('DEBUG', 'Sale Account Type', typeSaleAccount);
					if (listAccountTypes.indexOf(typeSaleAccount) == -1) {
						beforeUpdate.logMsgs.push(
								"Error validating Withholding Tax Type record #" + wiTaxTypeObject.id + "#<br/>"+
				    			"Tax Type Name is: #" + wiTaxTypeObject.name + "#<br/>");
						throw nlapiCreateError('TAX TYPE VALIDATION ERROR',
								'Asset/Sales Tax Account has an invalid account type. Aborting bundle deprecation process.');
					}
				}
			}
			
			beforeUpdate.logMsgs.push(
					"Completed validation of Withholding Tax Type record #" + wiTaxTypeObject.id + "#<br/>"+
	    			"Tax Type Name is: #" + wiTaxTypeObject.name + "#<br/>");
			
		} else { beforeUpdate.logMsgs.push("Tax Type #" + wiTaxTypeObject.id + "# was not loaded properly.<br/>"); }
    };
    
    return beforeUpdate;
})();

_4601.bundleUpdate.afterUpdate = (function(){
	function afterUpdate(){
		afterUpdate.exceptMsgs = "*** BUNDLE AFTER UPDATE PROCESS COMPLETED ***";
		afterUpdate.deprecateMsg = "*** No need for deprecation process. Skipping deprecation steps... ***<br/><br/>";
		afterUpdate.logMsgs = [];
		var withException = false;
		try{ afterUpdate.run(); }
	    catch(ex)
	    { 
	        withException = true; 
	        if ( ex instanceof nlobjError )
	        {
	        	afterUpdate.exceptMsgs = "*** SYSTEM ERROR ***<br/>"+
	                                    "Exception Code: "+ex.getCode() + '<br/>' + 
	                                    "Exception Details: "+ex.getDetails() + '<br/>' +
	                                    "Exception Stack Trace: "+ex.getStackTrace() + '<br/>';
	        }
	        else{ afterUpdate.exceptMsgs = "*** UNEXPECTED ERROR ***<br/>"+ ex.toString(); }
	    }

	    nlapiLogExecution("DEBUG", "BUNDLE UPDATE LOG ENTRIES",	afterUpdate.exceptMsgs +"<br/>"+
	            															"*********************<br/>"+ 
	            															"Log Trail: <br/>"+afterUpdate.logMsgs.join('<br/>') );

		if (withException){ throw nlapiCreateError("BUNDLE AFTER UPDATE PROCESS ERROR", afterUpdate.logMsgs.join('<br/>')); }
	}
	
	afterUpdate.run = function run()
    {
		// TODO Is it still necessary to check if account has already been deprecated?
		if ( afterUpdate.targetAccountNeedsDeprecation() ){
			
			afterUpdate.wiTaxSetupRecordObject = afterUpdate.updateExistingSingleTaxSetupRecord();
			afterUpdate.updateExistingTaxTypesOfTaxSetupRecord();
			afterUpdate.updateExistingTaxCodes();
			afterUpdate.updateDeprecationFlagOfTaxSetupRecord(afterUpdate.wiTaxSetupRecordObject);
		}
    };
    
    afterUpdate.updateExistingTaxCodes = function updateExistingTaxCodes() {
    	afterUpdate.wiTaxCodeMapper = _4601.getWiTaxCodeMapper();
		var wiTaxCodes = afterUpdate.wiTaxCodeMapper.all({skipColumns: 'T'});
		
		if (wiTaxCodes.length == 0){
	    	throw nlapiCreateError("TAX CODE RECORDS UPDATE ERROR",  
	    			                               "No Withholding Tax Codes found!. Aborting bundle deprecation process." );
		}
		wiTaxCodes.forEach(afterUpdate.updateExistingTaxCodeRecord);
    };
    
    afterUpdate.updateExistingTaxCodeRecord = function updateExistingTaxCodeRecord(wiTaxCodeElement){
		var wiTaxCodeObject = afterUpdate.wiTaxCodeMapper.loadRecord(wiTaxCodeElement.id);
		if(wiTaxCodeObject){
			wiTaxCodeObject.availableon = "both";
			wiTaxCodeObject.id = afterUpdate.wiTaxCodeMapper.saveRecord(wiTaxCodeObject);
			if (wiTaxCodeObject.id > 0){
				afterUpdate.logMsgs.push(
						"Completed update of Withholding Tax Code record #"+wiTaxCodeObject.id+"#<br/>");
			}
		}
		else{ afterUpdate.logMsgs.push("Tax Code #"+wiTaxCodeObject.id+"# was not loaded properly.<br/>"); }
	};

	afterUpdate.updateDeprecationFlagOfTaxSetupRecord = function(wiTaxSetupRecordObject){
		var userOverrideId = 'custrecord_ph4014_wtax_useroverride';
        var wiTaxSetupRecordMapper = _4601.getWiTaxSetupMapper();

        wiTaxSetupRecordMapper.addMapping('useroverride', {type: 'checkbox', field: userOverrideId});
        wiTaxSetupRecordObject.useroverride = true;
        wiTaxSetupRecordObject.id = wiTaxSetupRecordMapper.saveRecord(wiTaxSetupRecordObject);
        if( wiTaxSetupRecordObject.id > 0){
        	afterUpdate.logMsgs.push("Tax Setup Record #"+wiTaxSetupRecordObject.id+"# was saved properly.<br/>");	
        }
	};    
    
	afterUpdate.updateExistingTaxTypeRecord = function updateExistingTaxTypeRecord(wiTaxTypeElement){
		var wiTaxTypeObject = afterUpdate.wiTaxTypeMapper.loadRecord(wiTaxTypeElement.id);
		if(wiTaxTypeObject){
			wiTaxTypeObject.witaxsetup = afterUpdate.wiTaxSetupRecordObject.id;
			wiTaxTypeObject.witaxbase = "amount";
			wiTaxTypeObject.id = afterUpdate.wiTaxTypeMapper.saveRecord(wiTaxTypeObject);
			if (wiTaxTypeObject.id > 0){
				afterUpdate.logMsgs.push(
						"Completed update of Tax Setup field of Withholding Tax Type record #"+wiTaxTypeObject.id+"#<br/>"+
						"Tax Setup field is: #"+wiTaxTypeObject.witaxsetup+"#<br/>"+
						"Tax Type field is: #"+wiTaxTypeObject.name+"#<br/>");
			}
		}
		else{ afterUpdate.logMsgs.push("Tax Type #"+wiTaxTypeElement.id+"# was not loaded properly.<br/>"); }
	};

	afterUpdate.updateExistingTaxTypesOfTaxSetupRecord = function updateExistingTaxTypesOfTaxSetupRecord(){
		afterUpdate.wiTaxTypeMapper = _4601.getWiTaxTypeMapper();
		var wiTaxTypes = afterUpdate.wiTaxTypeMapper.all({skipColumns: 'T'});
		
		if (wiTaxTypes.length == 0){
	    	throw nlapiCreateError("TAX TYPE RECORDS UPDATE ERROR",  
	    			                               "No Withholding Tax Types found for Tax Setup Record #"+afterUpdate.wiTaxSetupRecordObject.id+"# found!. Aborting bundle deprecation process." );
		}
		wiTaxTypes.forEach(afterUpdate.updateExistingTaxTypeRecord);
		
		// Consider the importance of the Withholding Tax Type Kind Reference. For now, retain this field...
		// 
		// 1. Update the value of the 'Withholding Tax Setup' field with the value from the sole Setup record that were created by the PH bundle... 
		// Use the existing "User Override" ('custrecord_ph4014_wtax_useroverride') field to track if deprecation has been applied already...
		// if it's already marked to 'T'. The afterUpdate was already triggered. No need to do any afterUpdate process anymore...
		//
		// The following updates will happen only if the "User Override" field is not yet set to 'T'
		//
		// For the Tax Type custom record, the following activities must done.
		// 
		//  1. Provide a value for the field "Withholding Tax Setup" ('custrecord_4601_wtt_witaxsetup') by getting the internal id of the sole Tax Setup record.
		//  2. Must retain the original posting accounts and discount items already available when the bundle update/deprecation happened. 
		//  3. Provide a value for the field "Withholding Tax Base" ('custrecord_4601_wtt_witaxbase'). By default put the value 'amount'.
	};    
    
	afterUpdate.updateExistingSingleTaxSetupRecord = function(){
        var wiTaxSetupRecordMapper = _4601.getWiTaxSetupMapper();
        var wiTaxSetupObject = wiTaxSetupRecordMapper.all().getFirst();
        
    	// should never happen, but might as well test for this...
        if (!wiTaxSetupObject){
        	throw nlapiCreateError("TAX SETUP RECORD UPDATE ERROR",  "No Withholding Tax Setup Record found!. Aborting bundle deprecation process." );
        }
        var wiNexusMapper = _4601.getNexusMapper();
        var nexusObject = wiNexusMapper.all({filters: [['country', 'is', "PH"]], columns: ['country'] }).getFirst();
        if (!nexusObject){
        	throw nlapiCreateError("TAX SETUP RECORD UPDATE ERROR",  "No Nexus with country equal to PH found!. Aborting bundle deprecation process." );
        }
        
    	wiTaxSetupObject = wiTaxSetupRecordMapper.loadRecord(wiTaxSetupObject.id);
    	wiTaxSetupObject.nexus = nexusObject.id;
    	wiTaxSetupObject.lookup = 'T';
    	wiTaxSetupObject.onpurcs = 'T';
    	wiTaxSetupObject.notonpurcorders = 'T';
    	wiTaxSetupObject.purctaxpoint = 'onaccrual';
    	wiTaxSetupObject.purcappliesto = wiTaxSetupObject.applyontaxline == 'T' ? 'T' : 'F';
    	wiTaxSetupObject.onsales = 'T';
    	wiTaxSetupObject.notonsaleorders = 'T';
    	wiTaxSetupObject.saletaxpoint = 'onpayment';
    	wiTaxSetupObject.saleappliesto = wiTaxSetupObject.applyontaxline == 'T' ? 'T' : 'F';
    	wiTaxSetupObject.id = wiTaxSetupRecordMapper.saveRecord(wiTaxSetupObject);

    	afterUpdate.logMsgs.push(
    			"Completed update of Nexus field of Withholding Tax Setup record #"+wiTaxSetupObject.id+"#<br/>"+
    			"Nexus ID is: #"+nexusObject.id+"#<br/>"+
    			"Nexus Country is: #"+nexusObject.country+"#<br/>");

		// For the Tax Setup custom record, the following activities must done.

		// For the new fields...
		// 1.  Provide a value for the 'Nexus' field derived from the 'Subsidiary'... - MANDATORY - SET THIS TO PH!
		// 2. Provide a value 'Enable Tax Lookup on Transactions' field - OPTIONAL
		
		// 3. Ignore 'Purchase Withholding Tax Point' - NO VALUE NECESSARY
		// 4. Ignore 'Purchase Withholding Tax Applies To' - NO VALUE NECESSARY
		// 5. Ignore 'Sale Withholding Tax Point' - NO VALUE NECESSARY
		// 6. Ignore 'Sale Withholding Tax Applies To' - NO VALUE NECESSARY
		// 7. Ignore 'Purchase Withholding Tax Applies To' - NO VALUE NECESSARY
		
		// Main activity here is...
		// 1. Use the existing Subsidiary from the Setup Record and find the appropriate Nexus associated to it and set the Nexus field of the custom record 'Withholding Tax Setup'
    	return wiTaxSetupObject;

	};    
    
    afterUpdate.searchWiTaxSetupRecord = function searchWiTaxSetupRecord(){
		// if the "custrecord_ph4014_wtax_useroverride" field is no longer available, this means that the search attempt below will throw an error. 
		// this field must have been already removed from the target account. this removal will happen on succeeding bundle updates after the
		// deprecation process.
    	var accountNeedsDeprecation = false;
		var result = nlapiSearchRecord("customrecord_4601_witaxsetup", null, null, new nlobjSearchColumn("custrecord_ph4014_wtax_useroverride"));
        if(result)
        {
        	
    		// When first update of the PH Bundle is done through the deprecation process, the target account must only have 1 entry for the custom record "customrecord_4601_witaxsetup".
    		// Deprecation is done when the new Generic WTax Bundle is pushed to an existing PH Bundle.
    		// If the target account's custom record "customrecord_4601_witaxsetup" has more than 1 entry, this means that there are new 'tax setup' already added by the user.
    		// Adding of new 'tax setup' records is only available to the Generic WTax Bundle. If this is the case, it means the this update attempt is no longer the first update attempt.
    		// The deprecation process were done before!!!
	    	
        	if (result.length > 1){
        		afterUpdate.deprecateMsg += "New Tax Setup accounts were already added. Generic Withholding Tax functionality is already in place.";
        	}
        	else
        	{
        		if (result[0].getValue("custrecord_ph4014_wtax_useroverride") === 'T'){
        			afterUpdate.deprecateMsg += "Deprecation process was already completed for this account.";	        			
        		}
        		else {
        			var wiNexusMapper = _4601.getNexusMapper();
        	        var nexusObject = wiNexusMapper.all({filters: [['country', 'is', "PH"]], columns: ['country'] }).getFirst();
        	        if (!nexusObject){
        	        	afterUpdate.deprecateMsg += "No Nexus with country equal to PH found!.";
        	        } else
        	        	accountNeedsDeprecation = true;
        		}
        	}

        	// just throw an nlapiCreateError to create a log for debugging purposes...
//        	if (!accountNeedsDeprecation){ throw nlapiCreateError("IGNORE",  afterUpdate.deprecateMsg ); }
        }
        
        return accountNeedsDeprecation;
    };
    
	afterUpdate.targetAccountNeedsDeprecation = function targetAccountNeedsDeprecation(){
		var accountNeedsDeprecation = false;
		try { accountNeedsDeprecation = afterUpdate.searchWiTaxSetupRecord(); }
		catch(ex)
		{
			// re-throw other error encountered... 
			if ( ex instanceof nlobjError ){ throw nlapiCreateError("ERROR",  ex ); }
			else 
			{
				afterUpdate.logMsgs.push( "*** INFORMATION MESSAGE ONLY ***<br/>"+
                        "Deprecation process was already done for this account. <br/>"+
                        "It will no longer be triggered. Proceeding with normal bundle update. <br/><br/>"+
                        "Error message shown below is for debugging purposes only.<br/>"+ afterUpdate.deprecateMsg);
			};
		}
		return accountNeedsDeprecation;
	};    
	
	return afterUpdate;
})(); 