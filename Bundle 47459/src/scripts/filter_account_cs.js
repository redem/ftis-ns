/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 May 2012     mmoya
 *
 */

if (!_4601) { var _4601 = {}; }

_4601.filterAccountCS = {};

(function () {
    var cs = _4601.filterAccountCS;

    cs.fieldChanged = function fieldChanged(type, fieldName) {
    	if (fieldName == 'custrecord_4601_wtt_witaxsetup') {
       	 var wiTaxSetupId = nlapiGetFieldValue('custrecord_4601_wtt_witaxsetup');
       	 
       	 if (wiTaxSetupId) {
       		 cs.fieldChanged.reload(wiTaxSetupId);
       	 }
       	 
        }  
    };
    
    cs.fieldChanged.reload = function reload(wiTaxSetupId) {
    	cs.resourceObject = JSON.parse(nlapiGetFieldValue('custpage_resobj'));
        if (!cs.fieldChanged.reload.reloading) {
            alert([
				cs.resourceObject.TRANS_CS['form_reload'].RELOADMSG2.message // 'The form needs to be reloaded.',
																			// 'Changes made will not be saved.'

            ].join(' '));

            var params = {
            		custrecord_4601_wtt_witaxsetup: wiTaxSetupId,
            		custrecord_4601_wtt_name: nlapiGetFieldValue('custrecord_4601_wtt_name'),
            		custrecord_4601_wtt_description: escape(nlapiGetFieldValue('custrecord_4601_wtt_description'))            		
            };
            
            nlapiChangeCall(params);
            cs.fieldChanged.reload.reloading = true;
        }
    };
}());
