/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }

WTax.WTaxOnPaymentProcessor = function WTaxOnPaymentProcessor(params) {
    if (!params || !params.job) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A job is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor', WTaxError.LEVEL.ERROR);
    }
    
    this.job = params.job;
    this.monitors = params.monitors || [];
    this.result = {};
};

WTax.WTaxOnPaymentProcessor.prototype.run = function run() {
    try {
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.PROCESSING);
        this.tranType = this.job.recordType == 'vendorpayment' ? 'purc' : 'sale';
        this.resourceObject = _4601.getResourceManager();
        this.job.state.processedTransactions = this.job.state.processedTransactions || {};
        this.job.state.existingCreditRecords = this.job.state.existingCreditRecords || {};
        this.job.state.wtaxGroupsMap = this.job.state.wtaxGroupsMap || this.buildWTaxGroupsMap();
        this.job.state.wtaxTypesMap = this.job.state.wtaxTypesMap || this.buildWTaxTypesMap();
        this.job.state.vatCode = this.job.state.vatCode || _4601.getWiTaxLineAppropriateVATCodeId(this.resourceObject, this.job.inputData.nexus);
        
        var rawPaidLines = {};
        if (this.job.appliedLines.length > 0) {
            rawPaidLines = new WTax.DAO.WTaxOnPaymentTransactionDAO().getList({
                id: this.job.appliedLines,
                type: ['CustInvc', 'VendBill']
            });
        }
        
        this.job.state.paidLines = this.getPaidLines(rawPaidLines, this.job.inputData.appliedLines);
        var creditRecordDao = new WTax.DAO.WTaxCreditRecordDAO();
        
        if (Object.keys(this.job.state.paidLines).length > 0) {
            switch (this.job.inputData.mode) {
                case 'create':
                case 'paybills':
                    this.createCreditRecords(this.job.state.paidLines, creditRecordDao);
                    break;
                case 'edit':
                    if (this.isVoided(this.job.inputData.paymentId)) {
                        this.job.state.voidedCreditRecords = this.job.state.voidedCreditRecords || {};
                        this.voidCreditRecords(this.job.inputData.paymentId, creditRecordDao);
                    } else {
                        this.editCreditRecords(this.job.state.paidLines, creditRecordDao);
                    }
                    
                    break;
                case 'delete':
                    this.deleteCreditRecords(this.job.inputData.associatedCredits);
                    break;
                default:
                    break;
            }
        } else {
            this.cancelCreditRecords({}, creditRecordDao);
            this.setJobResult(WTax.DAO.WTaxJob.STATUS.DONE);
        }
    } catch(e) {
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.FAILED, WTaxError.toString(e));
        WTaxError.logError(e, 'WTax.WTaxOnPaymentProcessor.run', WTaxError.LEVEL.ERROR);
    }
    
    this.setJobOutput();
    return this.result;
};

WTax.WTaxOnPaymentProcessor.prototype.createCreditRecords = function createCreditRecords(transactions, creditRecordDao) {
    if (!transactions) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A transactions object is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.createCreditRecords', WTaxError.LEVEL.ERROR);
    }
    
    if (!creditRecordDao) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A Credit Record DAO is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.createCreditRecords', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var record = null;
        var tran = null;
        var creditId = null;
        
        for (var t in transactions) {
            tran = transactions[t];
            creditId = '';
            
            //skip the transaction if it has been processed already or if it has no WTax amount
            if (this.job.state.processedTransactions[tran.id] != undefined || tran.lines.length == 0) {
                this.job.state.processedTransactions[tran.id] = this.job.state.processedTransactions[tran.id] || '';
                continue;
            }
            
            record = new WTax.DAO.WTaxCreditRecord();
            this.setCreditRecordValues(record, tran);
            this.addCreditRecordItems(record, tran);
            creditId = creditRecordDao.create(record);
            
            this.job.state.processedTransactions[tran.id] = this.job.state.processedTransactions[tran.id] || creditId;
            
            if (this.isThresholdReached()) {
                this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING)
                return;
            }
        }
        
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.DONE);
    } catch(e) {
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.FAILED, WTaxError.toString(e));
        WTaxError.logError(e, 'WTax.WTaxOnPaymentProcessor.createCreditRecords', WTaxError.LEVEL.ERROR);
    }
};

WTax.WTaxOnPaymentProcessor.prototype.editCreditRecords = function editCreditRecords(transactions, creditRecordDao) {
    if (!transactions) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A transactions object is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.editCreditRecords', WTaxError.LEVEL.ERROR);
    }
    
    if (!creditRecordDao) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A Credit Record DAO is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.editCreditRecords', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var tran = null;
        var record = null;
        var creditId = null;
        
        this.cancelCreditRecords(transactions, creditRecordDao);
        
        for (var t in transactions) {
            tran = transactions[t];
            creditId = '';
            
            //skip the transaction if it has been processed already or if it has no WTax amount
            if (this.job.state.processedTransactions[tran.id] != undefined || tran.lines.length == 0) {
                this.job.state.processedTransactions[tran.id] = this.job.state.processedTransactions[tran.id] || '';
                continue;
            }
            
            record = new WTax.DAO.WTaxCreditRecord();
            record.internalId = this.job.state.existingCreditRecords[tran.id] ? this.job.state.existingCreditRecords[tran.id].internalId : '';
            this.setCreditRecordValues(record, tran);
            this.addCreditRecordItems(record, tran);
            creditId = record.internalId ? creditRecordDao.update(record) : creditRecordDao.create(record);
            
            this.job.state.processedTransactions[tran.id] = this.job.state.processedTransactions[tran.id] || creditId;
            
            if (this.isThresholdReached()) {
                this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING);
                return;
            }
        }
        
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.DONE);
    } catch(e) {
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.FAILED, WTaxError.toString(e));
        WTaxError.logError(e, 'WTax.WTaxOnPaymentProcessor.editCreditRecords', WTaxError.LEVEL.ERROR);
    }
};

WTax.WTaxOnPaymentProcessor.prototype.voidCreditRecords = function voidCreditRecords(paymentId, creditRecordDao) {
    try {
        var credits = creditRecordDao.getList({paymentRefId: paymentId});
        
        if (this.isThresholdReached()) {
            this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING);
            return;
        }
        
        for (var i = 0; i < credits.length; i++) {
            if (this.job.state.processedTransactions[credits[i].docRefId] == undefined) {
                try {
                    nlapiVoidTransaction(credits[i].recordType, credits[i].internalId);
                    this.job.state.processedTransactions[credits[i].docRefId] = this.job.state.processedTransactions[credits[i].docRefId] || credits[i].internalId;
                    
                    if (this.isThresholdReached()) {
                        this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING);
                        return;
                    }
                } catch(e) {
                    WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.voidCreditRecords', WTaxError.LEVEL.ERROR);
                }
            }
        }
        
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.DONE);
    } catch(e) {
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.FAILED, WTaxError.toString(e));
        WTaxError.logError(e, 'WTax.WTaxOnPaymentProcessor.voidCreditRecords', WTaxError.LEVEL.ERROR);
    }
};

WTax.WTaxOnPaymentProcessor.prototype.deleteCreditRecords = function deleteCreditRecords(credits) {
    try {
        if (this.isThresholdReached()) {
            this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING);
            return;
        }
        
        for (var i = 0; i < credits.length; i++) {
            if (this.job.state.processedTransactions[credits[i].docRefId] == undefined) {
                try {
                    nlapiDeleteRecord(credits[i].recordType, credits[i].internalId);
                    this.job.state.processedTransactions[credits[i].docRefId] = this.job.state.processedTransactions[credits[i].docRefId] || credits[i].internalId;
                    
                    if (this.isThresholdReached()) {
                        this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING);
                        return;
                    }
                } catch(e) {
                    WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.voidCreditRecords', WTaxError.LEVEL.ERROR);
                }
            }
        }
        
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.DONE);
    } catch(e) {
        this.setJobResult(WTax.DAO.WTaxJob.STATUS.FAILED, WTaxError.toString(e));
        WTaxError.logError(e, 'WTax.WTaxOnPaymentProcessor.deleteCreditRecords', WTaxError.LEVEL.ERROR);
    }
};

WTax.WTaxOnPaymentProcessor.prototype.cancelCreditRecords = function cancelCreditRecords(transactions, creditRecordDao) {
    if (!transactions) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A transactions object is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.cancelCreditRecords', WTaxError.LEVEL.ERROR);
    }
    
    if (!creditRecordDao) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A Credit Record DAO is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.cancelCreditRecords', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var creditRecords = creditRecordDao.getList({paymentRefId: this.job.inputData.paymentId, hasAmount: true});
        
        if (this.isThresholdReached()) {
            this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING);
            return;
        }
        
        var tran = null;
        
        for (var c = 0; c < creditRecords.length; c++) {
            tran = creditRecords[c];
            
            if (this.job.state.processedTransactions[tran.docRefId] != undefined) {
                continue;
            }
            
            if (!transactions[tran.docRefId]) {
                tran.memo = '**' + (this.tranType == 'purc' ?
                    _4601.renderTranslation(this.resourceObject.TRANS_UE['field_msg'].CANCEL_WTAX_BILL_CREDIT.message, {1: ''}) :
                        _4601.renderTranslation(this.resourceObject.TRANS_UE['field_msg'].CANCEL_WTAX_CREDIT_MEMO.message, {1: ''})
                ) + ' ** [' + tran.memo + ']';
                
                creditRecordDao.cancel(tran);
                this.job.state.processedTransactions[tran.docRefId] = this.job.state.processedTransactions[tran.docRefId] || tran.internalId;
                
                if (this.isThresholdReached()) {
                    this.setJobResult(WTax.DAO.WTaxJob.STATUS.RESCHEDULING);
                    return;
                }
            } else {
                this.job.state.existingCreditRecords[tran.docRefId] = tran;
            }
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.cancelCreditRecords', WTaxError.LEVEL.ERROR);
    }
};

WTax.WTaxOnPaymentProcessor.prototype.getPaidLines = function getPaidLines(paidLines, appliedLines) {
    if (!paidLines) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'An array of paid lines is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.getPaidLines', WTaxError.LEVEL.ERROR);
    }
    
    if (!appliedLines) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'An array of applied lines is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.getPaidLines', WTaxError.LEVEL.ERROR);
    }
    
    var result = {};
    
    try {
        var tran = null;
        for (var id in paidLines) {
            tran = {};
            tran.id = paidLines[id].id;
            tran.entity = paidLines[id].entity;
            tran.grossAmount = paidLines[id].grossAmount;
            tran.department = paidLines[id].department;
            tran.classification = paidLines[id].classification;
            tran.location = paidLines[id].location;
            tran.refNum = paidLines[id].tranType + ' #' + (paidLines[id].tranId || paidLines[id].transactionNumber);
            tran.paidAmount = appliedLines[id].paidAmount;
            tran.discountAmount = appliedLines[id].discountAmount;
            tran.dueAmount = appliedLines[id].dueAmount;
            tran.lines = paidLines[id].lines;
            tran.wtaxTypeAmounts = {};
            tran.wtaxTotal = 0;
            
            var group = null;
            for (var i = 0; i < paidLines[id].lines.length; i++) {
                tran.wtaxTotal += parseFloat(paidLines[id].lines[i].wtaxAmount);
                
                if (this.job.state.wtaxGroupsMap[paidLines[id].lines[i].wtaxCode] == undefined) {
                    tran.wtaxTypeAmounts[paidLines[id].lines[i].wtaxType] = tran.wtaxTypeAmounts[paidLines[id].lines[i].wtaxType] || 0;
                    tran.wtaxTypeAmounts[paidLines[id].lines[i].wtaxType] += parseFloat(paidLines[id].lines[i].wtaxAmount);
                } else {
                    group = this.job.state.wtaxGroupsMap[paidLines[id].lines[i].wtaxCode];
                    
                    for (var j = 0; j < group.length; j++) {
                        tran.wtaxTypeAmounts[group[j].wtaxType] = tran.wtaxTypeAmounts[group[j].wtaxType] || 0;
                        tran.wtaxTypeAmounts[group[j].wtaxType] -= (parseFloat(paidLines[id].lines[i].wtaxBase) * group[j].effectiveRate);
                    }
                }
            }
            
            tran.paidToGrossRatio = (tran.paidAmount + tran.discountAmount) / (tran.grossAmount - Math.abs(tran.wtaxTotal));
            result[id] = tran;
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.getPaidLines', WTaxError.LEVEL.ERROR);
    }
    
    return result;
};

WTax.WTaxOnPaymentProcessor.prototype.setCreditRecordValues = function setCreditRecordValues(record, tran) {
    if (!record) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A record object is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.setCreditRecordValues', WTaxError.LEVEL.ERROR);
    }
    
    if (!tran) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A transaction object is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.setCreditRecordValues', WTaxError.LEVEL.ERROR);
    }
    
    try {
        record.recordType = this.tranType == 'purc' ? 'vendorcredit' : 'creditmemo';
        record.entity = tran.entity;
        record.subsidiary = this.job.inputData.subsidiary;
        record.tranDate = this.job.inputData.tranDate;
        record.postingPeriod = this.job.inputData.postingPeriod;
        record.account = this.job.inputData.account;
        record.classification = tran.classification;
        record.department = tran.department;
        record.location = tran.location;
        record.paymentRefId = Number(this.job.inputData.paymentId) || '';
        record.docRefId = tran.id;
        record.currency = this.job.inputData.currency;
        record.exchangeRate = this.job.inputData.exchangeRate;
        record.memo = _4601.renderTranslation(this.resourceObject.TRANS_UE['field_msg'].WTAX_AMT.message, {1: tran.refNum});
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.setCreditRecordValues', WTaxError.LEVEL.ERROR);
    }
};

WTax.WTaxOnPaymentProcessor.prototype.addCreditRecordItems = function addCreditRecordItems(record, tran) {
    if (!record) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A record object is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.addCreditRecordItems', WTaxError.LEVEL.ERROR);
    }
    
    if (!tran) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A transaction object is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.addCreditRecordItems', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var discountItemField = this.tranType == 'purc' ? 'purchaseTaxItem' : 'salesTaxItem';
        var wtaxAmountPortion = 0;
        
        for (var taxType in tran.wtaxTypeAmounts) {
            wtaxAmountPortion = tran.wtaxTypeAmounts[taxType] * tran.paidToGrossRatio;
            record.totalWtaxAmount += wtaxAmountPortion;
            
            record.items.push({
                item: this.job.state.wtaxTypesMap[taxType][discountItemField],
                rate: Math.abs(wtaxAmountPortion),
                vatCode: this.job.state.vatCode,
                location: tran.location
            });
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.addCreditRecordItems', WTaxError.LEVEL.ERROR);
    }
};

WTax.WTaxOnPaymentProcessor.prototype.buildWTaxGroupsMap = function buildWTaxGroupsMap() {
    var map = {};
    
    try {
        var wtaxGroupMembers = new WTax.DAO.GroupedWTaxCodeDao().getList();
        var member = null;
        
        for (var j = 0; wtaxGroupMembers && j < wtaxGroupMembers.length; j++) {
            member = wtaxGroupMembers[j];
            
            if (map[member.group] == undefined) {
                map[member.group] = [];
            }
            
            map[member.group].push({
                wtaxCode: member.code,
                rate: parseFloat(member.rate) / 100,
                basis: parseFloat(member.basis) / 100,
                effectiveRate: parseFloat(member.rate) * parseFloat(member.basis) / 10000,
                wtaxType: member.taxType
            });
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.buildWTaxGroupsMap', WTaxError.LEVEL.ERROR);
    }
    
    return map;
};

WTax.WTaxOnPaymentProcessor.prototype.buildWTaxTypesMap = function buildWTaxTypesMap() {
    var map = {};
    
    try {
        map = new WTax.DAO.WTaxTypeDao().getList();
        map = new _4601.OrderedHash('id', map);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.buildWTaxTypesMap', WTaxError.LEVEL.ERROR);
    }
    
    return map;
};

WTax.WTaxOnPaymentProcessor.prototype.isVoided = function isVoided(id) {
    if (!id) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A transaction ID is required.');
        WTaxError.throwError(error, 'WTax.WTaxOnPaymentProcessor.isVoided', WTaxError.LEVEL.ERROR);
    }
    
    return Object.keys(new WTax.DAO.WTaxOnPaymentTransactionDAO().getList({
        id: id,
        voided: 'T'
    })).length > 0;
};

WTax.WTaxOnPaymentProcessor.prototype.isThresholdReached = function isThresholdReached() {
    for (var i = 0; i < this.monitors.length; i++) {
        if (this.monitors[i].isThresholdReached()) {
            return true;
        }
    }
    
    return false;
};

WTax.WTaxOnPaymentProcessor.prototype.setJobResult = function setJobResult(status, message) {
    this.result.job = this.job;
    this.result.status = status;
    this.result.message = message;
};

WTax.WTaxOnPaymentProcessor.prototype.setJobOutput = function setJobOutput() {
    try {
        if ([WTax.DAO.WTaxJob.STATUS.DONE, WTax.DAO.WTaxJob.STATUS.FAILED].indexOf(this.result.status) > -1) {
            this.job.outputData = {
                status: this.result.status,
                sendEmail: false
            };
            
            if (this.result.status == WTax.DAO.WTaxJob.STATUS.FAILED) {
                this.job.outputData.sendEmail = true;
                
                var tranList = [];
                for (var tran in this.job.state.paidLines) {
                    if (this.job.state.processedTransactions[tran] == undefined && this.job.state.paidLines[tran].lines.length > 0) {
                        tranList.push(' - ' + this.job.state.paidLines[tran].refNum);
                    }
                }
                
                var refNo = Number(this.job.inputData.paymentId) ? ' #' + this.job.inputData.paymentRefNo : '';
                var jobURL = this.job.inputData.baseUrl + nlapiResolveURL('RECORD', WTax.DAO.WTaxJob.RECORD_TYPE, this.job.id);
                
                this.job.outputData.message = {
                    subject: _4601.renderTranslation(this.resourceObject.TRANS_UE['email'].WTAX_ON_PAYMENT_FAILED_SUBJECT.message, {1: refNo}),
                    body: _4601.renderTranslation(this.resourceObject.TRANS_UE['email'].WTAX_ON_PAYMENT_FAILED_BODY.message, {1: tranList.join('\n'), 2: this.result.message, 3: jobURL}),
                    user: this.job.inputData.user
                };
            }
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxOnPaymentProcessor.setJobOutput', WTaxError.LEVEL.ERROR);
    }
};
