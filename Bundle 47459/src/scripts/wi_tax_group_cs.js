/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.wiTaxGroupCS = {};

(function () {
    var cs = _4601.wiTaxGroupCS;


    cs.pageInit = function pageInit() {
        cs.wiTaxCodes = JSON.parse(nlapiGetFieldValue('witaxcodes'));
        cs.wiTaxGroupMapper = _4601.getWiTaxGroupMapper();
        cs.recalc();
    };


    cs.fieldChanged = function fieldChanged(sublistName, fieldName, lineNum) {
        if (sublistName === 'groupedwitaxcodes' && fieldName === 'code') {
            cs.fieldChanged.setTaxCodeRateAndTaxType();
        } else if (['availableon', 'subsidiaries', 'includechildsubs', 'witaxtype'].map(function (name) {
            var mapping = cs.wiTaxGroupMapper.mappings[name];
            return mapping && mapping.field;
        }).indexOf(fieldName) > -1) {
            cs.fieldChanged.reload();
        }
    };


    cs.fieldChanged.setTaxCodeRateAndTaxType = function setTaxCodeRateAndTaxType() {
        var wiTaxCode = cs.wiTaxCodes[nlapiGetCurrentLineItemValue('groupedwitaxcodes', 'code')] || {rate: 0, witaxtype_name: ''};
        nlapiSetCurrentLineItemValue('groupedwitaxcodes', 'rate', wiTaxCode.rate);
        nlapiSetCurrentLineItemValue('groupedwitaxcodes', 'witaxtype', wiTaxCode.witaxtype_name);
        nlapiSetCurrentLineItemValue('groupedwitaxcodes', 'witaxbase', wiTaxCode.witaxbase);
    };


    cs.fieldChanged.reload = function reload() {
        var params = [
            'name',
            'description',
            'rate',
            'subsidiaries',
            'includechildsubs',
            'witaxtype',
            'availableon',
            'inactive'
        ].reduce(function (params, name) {
            var mapping = cs.wiTaxGroupMapper.mappings[name];
            var value = mapping && nlapiGetFieldValue(mapping.field);
            if (value) {
                params[mapping.field] = cs.fieldChanged.encodeURI(value);
            }
            return params;
        }, {});
        nlapiChangeCall(params);
    };
    
    
    cs.fieldChanged.encodeURI = function convertToHex(value) {
    	var strHex = '';
    	for (var i = 0; i < value.length; i++) {
    		var character = value.charAt(i);
    		if (character == '%')
    			strHex += ''+encodeURI(character);
    		else
    			strHex += character;
    	}
    	return strHex;
    };


    cs.recalc = function recalc() {
        var effectiveRate = 0;
        var lineItemCount = nlapiGetLineItemCount('groupedwitaxcodes');

        for (var i = 1; i <= lineItemCount; i++) {
            var rate = parseFloat(nlapiGetLineItemValue('groupedwitaxcodes', 'rate', i));
            var basis = parseFloat(nlapiGetLineItemValue('groupedwitaxcodes', 'basis', i));
            effectiveRate += rate * basis / 100;
        }

        nlapiSetFieldValue(cs.wiTaxGroupMapper.mappings.rate.field, effectiveRate + '%');
    };


}());
