/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.WTaxSetup = function _WTaxSetup(id) {
    return {
        id: id,
        nexus: '',
        autoApply: '',
        enableTaxLookup : '',
        availableOnPurchases: '',
        availableOnPurchaseOrders: '',
        purchaseTaxPoint: '',
        purchaseAppliesTo: '',
        availableOnSales: '',
        availableOnSalesOrders: '',
        salesTaxPoint: '',
        salesAppliesTo: ''
    };
};


WTax.DAO.WTaxSetupDao = function _WTaxSetupDao() {
    try {
        var searchColumns = [
            'custrecord_4601_wts_nexus',
            'custrecord_4601_wts_autoapply',
            'custrecord_4601_wts_lookup',
            'custrecord_4601_wts_onpurcs',
            'custrecord_4601_wts_notonpurcorders',
            'custrecord_4601_wts_purctaxpoint',
            'custrecord_4601_wts_purcappliesto',
            'custrecord_4601_wts_onsales',
            'custrecord_4601_wts_notonsaleorders',
            'custrecord_4601_wts_saletaxpoint',
            'custrecord_4601_wts_saleappliesto'
        ];
        
        var columns = [];
        for (var d = 0; d < searchColumns.length; d++){
            columns.push(new nlobjSearchColumn(searchColumns[d]));
        }
        
        this.resultSet = nlapiSearchRecord('customrecord_4601_witaxsetup', null, null, columns);          
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxSetupDao', WTaxError.LEVEL.ERROR);
    }
};
WTax.DAO.WTaxSetupDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);


WTax.DAO.WTaxSetupDao.prototype.getList = function _getList(startIndex, endIndex) {
    var list = [];

    try {
        var rawList = this.resultSet;
        var rawLength = rawList ? rawList.length : 0;
        for (var i = 0; i < rawLength; i++) {
        	list.push(this.convertToObject(rawList[i]));
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxSetupDao.getList', WTaxError.LEVEL.ERROR);
    }
    
    return list;
};


WTax.DAO.WTaxSetupDao.prototype.getByNexus = function _getByNexus(nexusId) {
    var setup = null;
    var list = this.getList(0, 1000);
    
    for (var i = 0; list && i < list.length; i++) {
        if (list[i].nexus == nexusId) {
            setup = list[i];
            break;
        }
    }
    
    return setup;
};


WTax.DAO.WTaxSetupDao.prototype.convertToObject = function _convertToObject(row) {
    var setup = new WTax.DAO.WTaxSetup();
    
    try {
    	setup.id = row.getId();
        setup.nexus = row.getValue('custrecord_4601_wts_nexus');
        setup.autoApply = row.getValue('custrecord_4601_wts_autoapply');
        setup.enableTaxLookup = row.getValue('custrecord_4601_wts_lookup');
        setup.availableOnPurchases = row.getValue('custrecord_4601_wts_onpurcs');
        setup.availableOnPurchaseOrders = row.getValue('custrecord_4601_wts_notonpurcorders');
        setup.purchaseTaxPoint = row.getValue('custrecord_4601_wts_purctaxpoint');
        setup.purchaseAppliesTo = row.getValue('custrecord_4601_wts_purcappliesto');
        setup.availableOnSales = row.getValue('custrecord_4601_wts_onsales');
        setup.availableOnSalesOrders = row.getValue('custrecord_4601_wts_notonsaleorders');
        setup.salesTaxPoint = row.getValue('custrecord_4601_wts_saletaxpoint');
        setup.salesAppliesTo = row.getValue('custrecord_4601_wts_saleappliesto');
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxSetupDao.convertToObject', WTaxError.LEVEL.ERROR);
    }
    
    return setup;
};
