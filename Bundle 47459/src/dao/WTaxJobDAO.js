/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.WTaxJob = function WTaxJob(id) {
    return {
        id : id,
        recordType: '',
        recordId: '',
        processor: '',
        inputData: '',
        outputData: '',
        state: '',
        status: '',
        appliedLines: ''
    };
};

WTax.DAO.WTaxJob.STATUS = {
    PENDING: 'pending',
    PROCESSING: 'processing',
    RESCHEDULING: 'rescheduling',
    FAILED: 'failed',
    DONE: 'done'
};

WTax.DAO.WTaxJob.RECORD_TYPE = 'customrecord_wtax_job';

WTax.DAO.WTaxJobDao = function WTaxJobDao() {
};

WTax.DAO.WTaxJobDao.prototype.getList = function _getList(params) {
    var list = [];
    
    try {
        var filters = [];
        var columns = [
            new nlobjSearchColumn('internalid').setSort(),
            new nlobjSearchColumn('custrecord_wtax_job_record_type'),
            new nlobjSearchColumn('custrecord_wtax_job_record_id'),
            new nlobjSearchColumn('custrecord_wtax_job_processor'),
            new nlobjSearchColumn('custrecord_wtax_job_input_data'),
            new nlobjSearchColumn('custrecord_wtax_job_output_data'),
            new nlobjSearchColumn('custrecord_wtax_job_state'),
            new nlobjSearchColumn('custrecord_wtax_job_status'),
            new nlobjSearchColumn('custrecord_wtax_job_applied_lines')
        ];
        
        if (params && params.status) {
            filters.push(new nlobjSearchFilter('custrecord_wtax_job_status', null, 'is', params.status));
        }
        
        if (params && params.id) {
            filters.push(new nlobjSearchFilter('internalid', null, 'is', params.id));
        }
        
        if (params && params.recordId) {
            filters.push(new nlobjSearchFilter('custrecord_wtax_job_record_id', null, 'is', params.recordId));
        }
        
        if (params && params.appliedLines) {
            filters.push(new nlobjSearchFilter('custrecord_wtax_job_applied_lines', null, 'anyof', params.appliedLines));
        }
        
        var resultSet = nlapiSearchRecord(WTax.DAO.WTaxJob.RECORD_TYPE, null, filters, columns); 

        for (var i = 0; resultSet && i < resultSet.length; i++) {
            list.push(this.convertToObject(resultSet[i]));
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxJobDao.getList', WTaxError.LEVEL.ERROR);
    }

    return list;
};

WTax.DAO.WTaxJobDao.prototype.convertToObject = function _convertToObject(nlObject) {
    var code = new WTax.DAO.WTaxJob();
    
    try {
        code.id = nlObject.getValue('internalid');
        code.recordType = nlObject.getValue('custrecord_wtax_job_record_type');
        code.recordId = nlObject.getValue('custrecord_wtax_job_record_id');
        code.processor = nlObject.getValue('custrecord_wtax_job_processor');
        code.inputData = JSON.parse(nlObject.getValue('custrecord_wtax_job_input_data') || '{}');
        code.outputData = JSON.parse(nlObject.getValue('custrecord_wtax_job_output_data') || '{}');
        code.state = JSON.parse(nlObject.getValue('custrecord_wtax_job_state') || '{}');
        code.status = nlObject.getValue('custrecord_wtax_job_status');
        
        var appliedLines = nlObject.getValue('custrecord_wtax_job_applied_lines');
        code.appliedLines = appliedLines ? appliedLines.split(',') : [];
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxJobDao.convertToObject', WTaxError.LEVEL.ERROR);
    }
    
    return code;
};

WTax.DAO.WTaxJobDao.prototype.create = function _create(job) {
	if(!job) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A Job is required.');
		WTaxError.throwError(error, 'WTax.DAO.WTaxJobDao.create', WTaxError.LEVEL.ERROR);
	}

    var record = nlapiCreateRecord(WTax.DAO.WTaxJob.RECORD_TYPE, {recordmode: 'dynamic'});
    record.setFieldValue('custrecord_wtax_job_record_type', job.recordType);
    record.setFieldValue('custrecord_wtax_job_record_id', job.recordId);
    record.setFieldValue('custrecord_wtax_job_processor', job.processor);
    record.setFieldValue('custrecord_wtax_job_input_data', JSON.stringify(job.inputData || {}));
    record.setFieldValue('custrecord_wtax_job_output_data', JSON.stringify(job.outputData || {}));
    record.setFieldValue('custrecord_wtax_job_state', JSON.stringify(job.state || {}));
    record.setFieldValue('custrecord_wtax_job_status', job.status);
    record.setFieldValues('custrecord_wtax_job_applied_lines', job.appliedLines);

    job.id = nlapiSubmitRecord(record);

    return job;
};

WTax.DAO.WTaxJobDao.prototype.update = function _update(job) {
    if (!job || !job.id) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A Job is required.');
		WTaxError.throwError(error, 'WTax.DAO.WTaxJobDao.update', WTaxError.LEVEL.ERROR);
    }
    
    var record = nlapiLoadRecord(WTax.DAO.WTaxJob.RECORD_TYPE, job.id, {recordmode: 'dynamic'});

    if (job.outputData) {
        record.setFieldValue('custrecord_wtax_job_output_data', JSON.stringify(job.outputData));
    }

    if (job.state) {
        record.setFieldValue('custrecord_wtax_job_state', JSON.stringify(job.state));
    }

    if (job.status) {
        record.setFieldValue('custrecord_wtax_job_status', job.status);
    }
    
    nlapiSubmitRecord(record);

    return job;
};

WTax.DAO.WTaxJobDao.prototype.getJobByRecordType = function _getJobByRecordType(params) {
    if (!params) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A params is required.');
		WTaxError.throwError(error, 'WTax.DAO.WTaxJobDao.getJobByRecordType', WTaxError.LEVEL.ERROR);
    }
    if (!params.recordType) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A recordType is required.');
		WTaxError.throwError(error, 'WTax.DAO.WTaxJobDao.getJobByRecordType', WTaxError.LEVEL.ERROR);
    }
    if (!params.recordId) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A recordId is required.');
		WTaxError.throwError(error, 'WTax.DAO.WTaxJobDao.getJobByRecordType', WTaxError.LEVEL.ERROR);
    }
	var daoParams = {};

    switch(params.recordType) {
	    case 'vendorbill':
	    case 'invoice':
	    	daoParams = {
	            appliedLines: [params.recordId]
	        };
	        break;
	    case 'vendorpayment':
	    case 'customerpayment':
	    	daoParams = {
	            recordId: params.recordId
	        };
	        break;
	    default:
			var error = nlapiCreateError('UNSUPPORTED_RECORD_TYPE', 'The recordType ' + params.recordType + ' is not supported.');
	    	WTaxError.throwError(error, 'WTax.DAO.WTaxJobDao.getJobByRecordType', WTaxError.LEVEL.ERROR);
    }
    
    return this.getList(daoParams).pop();
};
