/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this wtaxType.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.WTaxType = function _WTaxType(id) {
    var wtaxType = {
        id : id,
        countryCode : '',
        name : '',
        description : '',
        purchaseTaxAccount : '',
        salesTaxAccount : '',
        countryName : '',
        legacyPurchaseDiscountItem : '',
        legacySalesDiscountItem : '',
        wtaxTypeKindReference : '',
        wtaxSetup : '',
        wtaxBase : '',
        includeDiscounts : '',
        purchaseTaxItem : '',
        salesTaxItem : ''
    };
    return wtaxType;
};


WTax.DAO.WTaxTypeDao = function _WTaxTypeDao(params) {
    try {
        var searchColumns = [
            'custrecord_ph4014_wtax_type_country_h',
            'custrecord_4601_wtt_name',
            'custrecord_4601_wtt_description',
            'custrecord_4601_wtt_purcaccount',
            'custrecord_4601_wtt_saleaccount',
            'custrecord_ph4014_wtax_type_dcountry_h',
            'custrecord_ph4014_wtax_discitem_p',
            'custrecord_ph4014_wtax_discitem_s',
            'custrecord_ph4014_wtax_type_kind_ref',
            'custrecord_4601_wtt_witaxsetup',
            'custrecord_4601_wtt_witaxbase',
            'custrecord_4601_wtt_includediscounts',
            'custrecord_4601_wtt_purcitem',
            'custrecord_4601_wtt_saleitem'
        ];
        
        var colLength = searchColumns.length;
        var columns = [];
        for(var d = 0; d < colLength; d++){
            columns.push(new nlobjSearchColumn(searchColumns[d]));
        }
        
        this.resultSet = nlapiSearchRecord('customrecord_4601_witaxtype', null, null, columns);  
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxTypeDao', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxTypeDao.prototype.getList = function _getList() {
    var list = [];

    try {
        var rawList = this.resultSet;
        var rawLength = rawList ? rawList.length : 0;
        for (var i = 0; i < rawLength; i++) {
            list.push(this.convertToObject(rawList[i]));
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxTypeDao.getList', WTaxError.LEVEL.ERROR);
    }
    
    return list;
};


WTax.DAO.WTaxTypeDao.prototype.convertToObject = function _convertToObject(nlObject) {
    var wtaxType = new WTax.DAO.WTaxType();
    
    try {
        wtaxType.id = nlObject.getId();
        wtaxType.countrywtaxType = nlObject.getValue('custrecord_ph4014_wtax_type_country_h');
        wtaxType.name = nlObject.getValue('custrecord_4601_wtt_name');
        wtaxType.description = nlObject.getValue('custrecord_4601_wtt_description');
        wtaxType.purchaseTaxAccount = nlObject.getValue('custrecord_4601_wtt_purcaccount');
        wtaxType.salesTaxAccount = nlObject.getValue('custrecord_4601_wtt_saleaccount');
        wtaxType.countryName = nlObject.getValue('custrecord_ph4014_wtax_type_dcountry_h');
        wtaxType.legacyPurchaseDiscountItem = nlObject.getValue('custrecord_ph4014_wtax_discitem_p');
        wtaxType.legacySalesDiscountItem = nlObject.getValue('custrecord_ph4014_wtax_discitem_s');
        wtaxType.wtaxTypeKindReference = nlObject.getValue('custrecord_ph4014_wtax_type_kind_ref');
        wtaxType.wtaxSetup = nlObject.getValue('custrecord_4601_wtt_witaxsetup');
        wtaxType.wtaxBase = nlObject.getValue('custrecord_4601_wtt_witaxbase');
        wtaxType.includeDiscounts = nlObject.getValue('custrecord_4601_wtt_includediscounts');
        wtaxType.purchaseTaxItem = nlObject.getValue('custrecord_4601_wtt_purcitem');
        wtaxType.salesTaxItem = nlObject.getValue('custrecord_4601_wtt_saleitem');
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxTypeDao.convertToObject', WTaxError.LEVEL.ERROR);
    }
    
    return wtaxType;
};

