/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.WTaxCode = function _WTaxCode(id) {
	var code = {
		id : id,
		description : '',
		rate : '',
		effectiveFrom : '',
		validUntil : '',
		subsidiaries : '',
		inactive : '',
		taxType : '',
		countryCode : '',
		countryName : '',
		name : '',
		avaialbleOn : '',
		taxAgency : '',
		includeChildren : '',
		percentageOfBase : '',
		amountThresholds : '',
		isTaxGroup : '',
		groupedWTaxCodes : ''
	};
	return code;
};


WTax.DAO.WTaxCodeDao = function _WTaxCodeDao(params) {
	try {
		var filters = [];
		if(params && params.WTaxTypeId){
			filters.push(new nlobjSearchFilter('custrecord_4601_wtc_witaxtype', null, 'is', params.WTaxTypeId));
		}
		
		var searchColumns = [
			'custrecord_4601_wtc_description',
			'custrecord_4601_wtc_rate',
			'custrecord_4601_wtc_effectivefrom',
			'custrecord_4601_wtc_validuntil',
			'custrecord_4601_wtc_subsidiaries',
			'custrecord_4601_wtc_inactive',
			'custrecord_4601_wtc_witaxtype',
			'custrecord_ph4014_wtax_code_country',
			'custrecord_ph4014_wtax_code_dcountry',
			'custrecord_4601_wtc_name',
			'custrecord_4601_wtc_availableon',
			'custrecord_4601_wtc_taxagency',
			'custrecord_4601_wtc_includechildsubs',
			'custrecord_4601_wtc_percentageofbase',
			'custrecord_4601_wtc_amountthresholds',
			'custrecord_4601_wtc_istaxgroup',
			'custrecord_4601_wtc_groupedwitaxcodes'
		];
		
		var colLength = searchColumns.length;
		var columns = [];
		for(var d = 0; d < colLength; d++){
			columns.push(new nlobjSearchColumn(searchColumns[d]));
		}
		
		this.resultSet = nlapiSearchRecord('customrecord_4601_witaxcode', null, filters, columns);	
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.WTaxCodeDao', WTaxError.LEVEL.ERROR);
	}
};
WTax.DAO.WTaxCodeDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);

WTax.DAO.WTaxCodeDao.prototype.getList = function _getList() {
	var list = [];

	try {
		var rawList = this.resultSet;
		var rawLength = rawList ? rawList.length : 0;
		for (var i = 0; i < rawLength; i++) {
			list.push(this.convertToObject(rawList[i]));
		}
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.WTaxCodeDao.getList', WTaxError.LEVEL.ERROR);
	}
	
	return list;
};


WTax.DAO.WTaxCodeDao.prototype.convertToObject = function _convertToObject(nlObject) {
	var code = new WTax.DAO.WTaxCode();
	
	try {
		code.id = nlObject.getId();
		code.description = nlObject.getValue('custrecord_4601_wtc_description');
		code.rate = nlObject.getValue('custrecord_4601_wtc_rate');
		code.effectiveFrom = nlObject.getValue('custrecord_4601_wtc_effectivefrom');
		code.validUntil = nlObject.getValue('custrecord_4601_wtc_validuntil');
		code.subsidiaries = nlObject.getValue('custrecord_4601_wtc_subsidiaries');
		code.inactive = nlObject.getValue('custrecord_4601_wtc_inactive');
		code.taxType = nlObject.getValue('custrecord_4601_wtc_witaxtype');
		code.countryCode = nlObject.getValue('custrecord_ph4014_wtax_code_country');
		code.countryName = nlObject.getValue('custrecord_ph4014_wtax_code_dcountry');
		code.name = nlObject.getValue('custrecord_4601_wtc_name');
		code.avaialbleOn = nlObject.getValue('custrecord_4601_wtc_availableon');
		code.taxAgency = nlObject.getValue('custrecord_4601_wtc_taxagency');
		code.includeChildren = nlObject.getValue('custrecord_4601_wtc_includechildsubs');
		code.percentageOfBase = nlObject.getValue('custrecord_4601_wtc_percentageofbase');
		code.amountThresholds = nlObject.getValue('custrecord_4601_wtc_amountthresholds');
		code.isTaxGroup = nlObject.getValue('custrecord_4601_wtc_istaxgroup');
		
		var groupedWTaxCodes = nlObject.getValue('custrecord_4601_wtc_groupedwitaxcodes');
		code.groupedWTaxCodes = groupedWTaxCodes ? groupedWTaxCodes.split(',') : [];
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.WTaxCodeDao.convertToObject', WTaxError.LEVEL.ERROR);
	}
	
	return code;
};
