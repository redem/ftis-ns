/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.Vendor = function _Vendor(id) {
	var entity = {
		id : id,
		isPerson : '',
		companyName: '',
		lastName : '',
		firstName : '',
		middleName : '',
		vatRegNumber: '',
		billZipCode : '',
		billAddress1 : '',
		billAddress2 : '',
		billCity: '',
		billState : '',
		billCountry : ''
	};
	return entity;
};

WTax.DAO.VendorDao = function _VendorDao(params) {
};

//NOT USED FOR NOW
//WTax.DAO.VendorDao = function _VendorDao(params) {
//	try {
//		var filters = [];
//		if(params && params.VendorId){
//			filters.push(new nlobjSearchFilter('internalid', null, 'is', vendorId));
//		}
//		
//		if(params && params.IsDefaultBilling){
//			filters.push(new nlobjSearchFilter('isdefaultbilling', null, 'is', 'T'));
//		}
//		
//		var searchColumns = ['isperson',
//		                     'companyname',
//		                     'lastname',
//		                     'firstname',
//		                     'middlename',
//		                     'vatregnumber',
//		                     'billzipcode',
//		                     'billaddress1',
//		                     'billaddress2',
//		                     'billcity',
//		                     'billstate',
//		                     'billcountry'];
//		
//		var colLength = searchColumns.length;
//		var columns = [];
//		for(var s=0; s < colLength; s++){
//			columns.push(new nlobjSearchColumn(searchColumns[s]));
//		}
//		
//		this.resultSet = nlapiSearchRecord('vendor', null, filters, columns);	
//	} catch (ex) {
//		nlapiLogExecution('ERROR', 'WTax.DAO.VendorDao', ex.toString());
//        throw nlapiCreateError('SEARCH_ERROR', 'WTax.DAO.VendorDao');
//	}
//};
//
//NOT USED FOR NOW
///* @return [array] Vendor objects */
//WTax.DAO.VendorDao.prototype.getList = function _getList(startIndex, endIndex) {
//	var list = [];
//
//	if (startIndex == null || typeof(startIndex) == undefined || 
//		endIndex == null || typeof(endIndex) == undefined) {
//		nlapiLogExecution('ERROR', 'WTax.DAO.VendorDao.getList MISSING_PARAMETER', 'startIndex / endIndex is null or undefined.');
//		throw nlapiCreateError('MISSING_PARAMETER', 'startIndex / endIndex is null or undefined.');
//	}
//
//	try {
//		var rawList = this.resultSet.getResults(startIndex, endIndex);
//		var rawLength = rawList ? rawList.length : 0;
//		for (var i = 0; i < rawLength; i++) {
//			list.push(this.convertToVendorObject(rawList[i]));
//		}
//	} catch (ex) {
//		nlapiLogExecution('ERROR', 'WTax.DAO.VendorDao.getList', ex.toString());
//        throw nlapiCreateError('SEARCH_ERROR', 'WTax.DAO.VendorDao.getList');
//	}
//	
//	return list;
//};

/* @return {Vendor}*/
WTax.DAO.VendorDao.prototype.convertToVendorObject = function _convertToVendorObject(nlObject) {
	var entity = new WTax.DAO.Vendor();
	try{
		entity.id = nlObject.getId();
		entity.isPerson = (nlObject.getValue('isperson') === 'T');
		entity.companyName = nlObject.getValue('companyname');
		entity.lastName = nlObject.getValue('lastname');
		entity.firstName = nlObject.getValue('firstname');
		entity.middleName = nlObject.getValue('middlename');
		entity.vatRegNumber = nlObject.getValue('vatregnumber');
		entity.billZipCode = nlObject.getValue('billzipcode');
		entity.billAddress1 = nlObject.getValue('billaddress1');
		entity.billAddress2 = nlObject.getValue('billaddress2');
		entity.billCity = nlObject.getValue('billcity');
		entity.billState = nlObject.getValue('billstate');
		entity.billCountry = nlObject.getValue('billcountry');
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.VendorDao.convertToVendorObject', WTaxError.LEVEL.ERROR);
	}
	return entity;
};

WTax.DAO.VendorDao.prototype.getVendorById = function _getVendorById(id) {
	if (!id) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Vendor ID is required.');
        WTaxError.throwError(error, 'WTax.DAO.VendorDao.getVendorById', WTaxError.LEVEL.ERROR);
	}
	
    var vendor = {};
    try {
		var filters = [];
		filters.push(new nlobjSearchFilter('internalid', null, 'is', id));
		filters.push(new nlobjSearchFilter('isdefaultbilling', null, 'is', 'T'));
		
		var searchColumns = [
            'isperson',
		    'companyname',
		    'lastname',
		    'firstname',
		    'middlename',
		    'vatregnumber',
		    'billzipcode',
		    'billaddress1',
		    'billaddress2',
		    'billcity',
		    'billstate',
		    'billcountry'
	    ];
		
		var colLength = searchColumns.length;
		var columns = [];
		for(var s=0; s < colLength; s++){
			columns.push(new nlobjSearchColumn(searchColumns[s]));
		}
		
		var searchObject = nlapiSearchRecord('vendor', null, filters, columns);	
		
		if(searchObject){
			vendor = this.convertToVendorObject(searchObject[0]);
		}
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.VendorDao.getVendorById', WTaxError.LEVEL.ERROR);
	}
	
	return vendor;
};
