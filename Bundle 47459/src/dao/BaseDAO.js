/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};


WTax.DAO.BaseDao = function _BaseDao(params) {
	if (!this.type || !this.savedSearch) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Incomplete parameter, is null or undefined.');
		WTaxError.throwError(error, 'WTax.DAO.BaseDao', WTaxError.LEVEL.ERROR);
	}

	try {
		var search = nlapiLoadSearch(this.type, this.savedSearch);
		var filters = this.getFilters(params);
		if (filters && (filters.length > 0)) {
			search.addFilters(filters);
		}
		var columns = this.getColumns ? this.getColumns(params) : 0;
		if (columns && columns.length > 0) {
			search.addColumns(columns);
		}
		this.resultSet = search.runSearch();
	} catch (ex) {
		WTaxError.throwError(ex, 'WTax.DAO.BaseDao', WTaxError.LEVEL.ERROR);
	}
};

WTax.DAO.BaseDao.prototype.getFilters = function _getFilters(params) {
	return [];
};

WTax.DAO.BaseDao.prototype.getList = function _getList(start, end) {
	var list = [];

	if (this.isEmpty) {
		return list;
	}

	if ((!start && (start != 0)) || !end) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Start and End indices are required.');
		WTaxError.throwError(error, 'WTax.DAO.BaseDao.getList', WTaxError.LEVEL.ERROR);
	}

	try {
		var rows = this.resultSet.getResults(start, end);
		for (var i = 0; i < rows.length; i++) {
			var row = this.convertToObject(rows[i]);
			if (row) {
				list.push(row);
			}
		}
	} catch (ex) {
		WTaxError.throwError(ex, 'WTax.DAO.BaseDao.getList', WTaxError.LEVEL.ERROR);
	}

	return list;
};

WTax.DAO.BaseDao.prototype.convertToObject = function _convertToObject(row) {
	return row;
};
