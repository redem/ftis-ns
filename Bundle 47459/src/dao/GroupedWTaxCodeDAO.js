/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.GroupedWTaxCode = function _WTaxCode(id) {
	return {
		id: id,
		group: '',
		code: '',
		rate: '',
		taxType: '',
		basis: ''
	};
};


WTax.DAO.GroupedWTaxCodeDao = function _WTaxCodeDao(params) {
    try {
        var filters = [];
		var columns = [
		    new nlobjSearchColumn('custrecord_4601_gwtc_group'),
			new nlobjSearchColumn('custrecord_4601_gwtc_code'),
			new nlobjSearchColumn('custrecord_4601_wtc_rate', 'custrecord_4601_gwtc_code'),                       
            new nlobjSearchColumn('custrecord_4601_wtc_witaxtype', 'custrecord_4601_gwtc_code'),
			new nlobjSearchColumn('custrecord_4601_gwtc_basis')
		];
		
		if (params && params.id) {
		    filters.push(new nlobjSearchFilter('custrecord_4601_gwtc_group', null, 'anyof', params.id));
		}
		
		this.resultSet = nlapiSearchRecord('customrecord_4601_groupedwitaxcode', null, filters, columns);			
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.GroupedWTaxCodeDao', WTaxError.LEVEL.ERROR);
	}
};
WTax.DAO.GroupedWTaxCodeDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);

WTax.DAO.GroupedWTaxCodeDao.prototype.getList = function _getList() {
	var list = [];
    
	try {
	    var rawList = this.resultSet;
		var rawLength = rawList ? rawList.length : 0;
		for (var i = 0; i < rawLength; i++) {
			list.push(this.convertToObject(rawList[i]));
		}
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.GroupedWTaxCodeDao.getList', WTaxError.LEVEL.ERROR);
	}
	
	return list;
};

WTax.DAO.GroupedWTaxCodeDao.prototype.convertToObject = function _convertToObject(nlObject) {
	var code = new WTax.DAO.GroupedWTaxCode();
	
	try {
		code.id = nlObject.getId();
		code.group = nlObject.getValue('custrecord_4601_gwtc_group');
		code.code = nlObject.getValue('custrecord_4601_gwtc_code');
		code.rate = nlObject.getValue('custrecord_4601_wtc_rate', 'custrecord_4601_gwtc_code');
		code.taxType = nlObject.getValue('custrecord_4601_wtc_witaxtype', 'custrecord_4601_gwtc_code');
		code.basis = nlObject.getValue('custrecord_4601_gwtc_basis');
	} catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.GroupedWTaxCodeDao.convertToObject', WTaxError.LEVEL.ERROR);
	}
	
	return code;
};
