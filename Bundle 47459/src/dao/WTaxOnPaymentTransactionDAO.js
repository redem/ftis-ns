/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.WTaxOnPaymentTransaction = function WTaxOnPaymentTransaction() {
    return {
        id: '',
        tranType: '',
        tranId: '',
        transactionNumber: '',
        entity: '',
        grossAmount: 0,
        department: '',
        classification: '',
        location: '',
        lines: []
    };
};

WTax.DAO.WTaxOnPaymentLine = function WTaxOnPaymentLine() {
    return {
        line: '',
        wtaxCode: '',
        wtaxType: '',
        wtaxRate: 0,
        wtaxBase: 0,
        wtaxAmount: 0
    };
};

WTax.DAO.WTaxOnPaymentTransactionDAO = function WTaxOnPaymentTransactionDAO() {
    this.LINES_PER_SEARCH = 1000;
};

WTax.DAO.WTaxOnPaymentTransactionDAO.prototype.getList = function _getList(params) {
    var list = {};
    
    try {
        var context = nlapiGetContext();
        this.hasDepartments = context.getFeature('DEPARTMENTS');
        this.hasLocations = context.getFeature('LOCATIONS');
        this.hasClasses = context.getFeature('CLASSES');
        this.isMultiCurrency = _4601.isMultiCurrencyOn();
        
        var filters = [
            new nlobjSearchFilter('mainline', null, 'is', 'T', null, 1, 0, true),
            new nlobjSearchFilter('custcol_4601_witaxapplies', null, 'is', 'T', null, 1, 0, false),
            new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0, null, 0, 2, false).setFormula('ABS({custcol_4601_witaxamount})')
        ];
        
        var columns = [
            new nlobjSearchColumn('mainline'),
            new nlobjSearchColumn('type'),
            new nlobjSearchColumn('tranid'),
            new nlobjSearchColumn('transactionnumber'),
            new nlobjSearchColumn('entity'),
            new nlobjSearchColumn('linesequencenumber'),
            new nlobjSearchColumn('custcol_4601_witaxcode'),
            new nlobjSearchColumn('custrecord_4601_wtc_witaxtype', 'custcol_4601_witaxcode'),
            new nlobjSearchColumn('custcol_4601_witaxrate'),
            new nlobjSearchColumn('custcol_4601_witaxbaseamount'),
            new nlobjSearchColumn('custcol_4601_witaxamount'),
            this.isMultiCurrency ? new nlobjSearchColumn('fxamount') : new nlobjSearchColumn('amount')
        ];
        
        if (this.hasDepartments) { columns.push(new nlobjSearchColumn('department')); }
        if (this.hasLocations) { columns.push(new nlobjSearchColumn('location')); }
        if (this.hasClasses) { columns.push(new nlobjSearchColumn('class')); }
        
        if (params && params.type) {
            filters.push(new nlobjSearchFilter('type', null, 'anyof', params.type));
        }
        
        if (params && params.id) {
            filters.push(new nlobjSearchFilter('internalid', null, 'anyof', params.id));
        }
        
        if (params && params.voided) {
            filters.push(new nlobjSearchFilter('voided', null, 'is', params.voided));
        }
        
        var sr = nlapiCreateSearch('transaction', filters, columns).runSearch();
        var resultSet = null;
        var index = 0;
        
        do {
            resultSet = sr.getResults(index, index + this.LINES_PER_SEARCH);
            
            for (var i = 0; i < (resultSet || []).length; i++) {
                row = resultSet[i];
                
                if (row.getValue('mainline') == '*') {
                    list[row.getId()] = this.convertToTransactionObject(row);
                } else {
                    var line = this.convertToLineObject(row);
                    list[row.getId()].lines.push(line);
                }
            }
            
            index += (resultSet || []).length;
        } while ((resultSet || []).length == this.LINES_PER_SEARCH);
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxOnPaymentTransactionDAO.getList', WTaxError.LEVEL.ERROR);
    }
    
    return list;
};

WTax.DAO.WTaxOnPaymentTransactionDAO.prototype.convertToTransactionObject = function convertToTransactionObject(row) {
    var tran = new WTax.DAO.WTaxOnPaymentTransaction();
    
    try {
        tran.id = row.getId();
        tran.tranType = row.getText('type');
        tran.tranId = row.getValue('tranid');
        tran.transactionNumber = row.getValue('transactionnumber');
        tran.entity = row.getValue('entity');
        tran.grossAmount = parseFloat(this.isMultiCurrency ? row.getValue('fxamount') : row.getValue('amount')) || 0;
        tran.department = this.hasDepartments ? row.getValue('department') : '';
        tran.classification = this.hasClasses ? row.getValue('class') : '';
        tran.location = this.hasLocations ? row.getValue('location') : '';
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxOnPaymentTransactionDAO.convertToTransactionObject', WTaxError.LEVEL.ERROR);
    }
    
    return tran;
};

WTax.DAO.WTaxOnPaymentTransactionDAO.prototype.convertToLineObject = function convertToLineObject(row) {
    var line = new WTax.DAO.WTaxOnPaymentLine();
    
    try {
        line.line = row.getValue('linesequencenumber');
        line.wtaxCode = row.getValue('custcol_4601_witaxcode');
        line.wtaxType = row.getValue('custrecord_4601_wtc_witaxtype', 'custcol_4601_witaxcode');
        line.wtaxRate = row.getValue('custcol_4601_witaxrate');
        line.wtaxBase = row.getValue('custcol_4601_witaxbaseamount');
        line.wtaxAmount = row.getValue('custcol_4601_witaxamount');                             
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.WTaxOnPaymentTransactionDAO.convertToLineObject', WTaxError.LEVEL.ERROR);
    }
    
    return line;
};
