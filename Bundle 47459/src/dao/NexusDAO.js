/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.Nexus = function _Nexus(id) {
    return {
        id: id,
        country: '',
        state: '',
        description: ''
    };
};


WTax.DAO.NexusDao = function _NexusDao() {
    try {
        var searchColumns = [
            'country',
            'state',
            'description',
        ];
        
        var columns = [];
        for (var d = 0; d < searchColumns.length; d++){
            columns.push(new nlobjSearchColumn(searchColumns[d]));
        }
        
        this.resultSet = nlapiSearchRecord('nexus', null, null, columns);          
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.NexusDao', WTaxError.LEVEL.ERROR);
    }
};
WTax.DAO.NexusDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);


WTax.DAO.NexusDao.prototype.getList = function _getList() {
    var list = [];

    try {
        var rawList = this.resultSet;
        var rawLength = rawList ? rawList.length : 0;
        for (var i = 0; i < rawLength; i++) {
            list.push(this.convertToObject(rawList[i]));
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.NexusDao.getList', WTaxError.LEVEL.ERROR);
    }
    
    return list;
};

WTax.DAO.NexusDao.prototype.getByCountry = function _getByCountry(country) {
	if (!country) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Country is required.');
		WTaxError.throwError(error, 'WTax.DAO.NexusDao.getByCountry', WTaxError.LEVEL.ERROR);
	}

    var nexus = null;
    var list = this.getList();
    
    for (var i = 0; list && i < list.length; i++) {
        if (list[i].country == country) {
            nexus = list[i];
            break;
        }
    }
    
    return nexus;
};

WTax.DAO.NexusDao.prototype.convertToObject = function _convertToNexusObject(row) {
    var nexus = new WTax.DAO.Nexus();
    
    try{
    	nexus.id = row.getId();
        nexus.country = row.getValue('country');
        nexus.state = row.getValue('state');
        nexus.description = row.getValue('description');
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.NexusDao.convertToObject', WTaxError.LEVEL.ERROR);
    }
    
    return nexus;
};
