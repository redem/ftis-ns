/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.TaxPeriod = function _TaxPeriod(id) {
    var period = {
        id:           		id,
        name:         		'',
        startDate:    		'',
        endDate:      		'',
        isInactive:   		'',
        parent:    	  		'',
        fiscalCalendar :	'',
        allClosed:    		'',
        isQuarter:    		false,
        isYear:       		false,
        isAdjustment: 		false
    };
    return period;
};

WTax.DAO.TaxPeriodDao = function _TaxPeriodDao() {
	this.isMultiCalendars = nlapiGetContext().getFeature('multiplecalendars');
};

WTax.DAO.TaxPeriodDao.prototype.searchRecords = function _searchRecords(filters, columns) {
    var periodList = [];
    
    try {
        var searchResults = nlapiSearchRecord('taxperiod', null, filters, columns);
        if (!searchResults) {
            return periodList;
        }
        
        for (var i = 0; i < searchResults.length; i++) {
            var period = this.convertToObject(searchResults[i]);
            periodList.push(period);
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.TaxPeriodDao.searchRecords', WTaxError.LEVEL.ERROR);
    }
    
    return periodList;
};

WTax.DAO.TaxPeriodDao.prototype.convertToObject = function _convertToObject(searchObject) {
	if (!searchObject) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A search object is required.');
		WTaxError.throwError(error, 'WTax.DAO.TaxPeriodDao.convertToObject', WTaxError.LEVEL.ERROR);
	}
	
    var period = {};
    try {
        period = new WTax.DAO.TaxPeriod(searchObject.getId() || '');
        period.name         = searchObject.getValue('periodname')      || '';
        period.startDate    = searchObject.getValue('startdate')       || '';
        period.endDate      = searchObject.getValue('enddate')         || '';
        period.isInactive   = searchObject.getValue('isinactive')  	   || '';
        period.allClosed   	= searchObject.getValue('allclosed')   	   || '';
        period.isQuarter    = (searchObject.getValue('isquarter')   === 'T');
        period.isYear       = (searchObject.getValue('isyear')      === 'T');
        period.isAdjustment = (searchObject.getValue('isadjust')    === 'T');
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.TaxPeriodDao.convertToObject', WTaxError.LEVEL.ERROR);
    }
    return period;
};

WTax.DAO.TaxPeriodDao.prototype.convertRecordToObject = function _convertRecordToObject(params, searchObject) {    
	if (!params || !searchObject) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Missing required parameters.');
		WTaxError.throwError(error, 'WTax.DAO.TaxPeriodDao.convertRecordToObject', WTaxError.LEVEL.ERROR);
	}
	
    var period = {};
        
    try {
        period = new WTax.DAO.TaxPeriod(searchObject.getId() || '');
        period.name         = searchObject.getFieldValue('periodname') || '';
        period.startDate    = searchObject.getFieldValue('startdate')  || '';
        period.endDate      = searchObject.getFieldValue('enddate')    || '';
        period.isInactive   = searchObject.getFieldValue('isinactive') || '';
        period.allClosed   	= searchObject.getFieldValue('allclosed')  || '';
        period.isQuarter    = (searchObject.getFieldValue('isquarter') === 'T');
        period.isYear       = (searchObject.getFieldValue('isyear')    === 'T');
        period.isAdjustment = (searchObject.getFieldValue('isadjust')  === 'T');
        
        if(this.isMultiCalendars){
            var sublist = 'fiscalcalendars';
	        var lineCount = searchObject.getLineItemCount(sublist);
	        for (var i = 1; i <= lineCount; i++) {
	           	//fiscal calendar of the selected subsidiary only!
	            if (searchObject.getLineItemValue(sublist, 'fiscalcalendar', i) == params.FiscalCalendarId) {
	                var p = searchObject.getLineItemValue(sublist, 'parent', i);
	                period.parent = (p == null || isNaN(parseInt(p))) ? null : parseInt(p);
	                if ((period.isYear || period.isQuarter) && params.IsOneWorld){
	                    period.fiscalCalendar = parseInt(params.FiscalCalendarId);
	                }
	                break;
	            }
	        }
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.TaxPeriodDao.convertRecordToObject', WTaxError.LEVEL.ERROR);
    }
    return period;
};

WTax.DAO.TaxPeriodDao.prototype.getPeriodById = function _getPeriodById(id, params) {
	if (!params || !id) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Parameter is null or undefined.');
		WTaxError.throwError(error, 'WTax.DAO.TaxPeriodDao.getPeriodById', WTaxError.LEVEL.ERROR);
	}
	
    var period = {};
    
    try {
    	 if(this.isMultiCalendars){
    		 var searchObject = nlapiLoadRecord('taxperiod', id);
    	     period = this.convertRecordToObject(params,searchObject);
    	}else{
    		var filters = [new nlobjSearchFilter('internalid', null, 'is', id)];
	         var columns = [
	                new nlobjSearchColumn("periodname"),
					new nlobjSearchColumn("startdate"),
					new nlobjSearchColumn("enddate"),
					new nlobjSearchColumn("isinactive"),
					new nlobjSearchColumn("isyear"),
					new nlobjSearchColumn("isquarter"),
					new nlobjSearchColumn("isadjust"),
					new nlobjSearchColumn("allclosed")
	        ];
	    	var searchObject = this.searchRecords(filters, columns);
	    	if(searchObject){
	    		period = searchObject[0];
	    	}
    	}
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.TaxPeriodDao.getPeriodById', WTaxError.LEVEL.ERROR);
    } 
    
    return period;
};

WTax.DAO.TaxPeriodDao.prototype.getCoveredPeriodIds = function _getCoveredPeriodIds(periodFromId, periodToId, params) {
	if (!periodFromId || !periodToId) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'parameter is null or undefined.');
		WTaxError.throwError(error, 'WTax.DAO.TaxPeriodDao.getCoveredPeriodIds', WTaxError.LEVEL.ERROR);
	}
    var ids = [];
    try {
        var periodFrom = this.getPeriodById(periodFromId, params);
        var periodTo = this.getPeriodById(periodToId, params);
        
        var filters = [
                       new nlobjSearchFilter('isinactive', null, 'is', 'F'),
                       new nlobjSearchFilter('startdate', null, 'onorafter', periodFrom.startDate),
                       new nlobjSearchFilter('enddate', null, 'onorbefore', periodTo.endDate),
                       new nlobjSearchFilter('isquarter', null, 'is', 'F'),
                       new nlobjSearchFilter('isyear', null, 'is', 'F')
                   ];
        var results = this.searchRecords(filters, null);
        ids = this.extractIds(results);
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.TaxPeriodDao.getCoveredPeriodIds', WTaxError.LEVEL.ERROR);
    }
    return ids;
};

WTax.DAO.TaxPeriodDao.prototype.extractIds = function _extractIds(list) {
    var ids = [];
    
    for (var i = 0; i < list.length; i++) {
        ids.push(list[i].id);
    };
    
    var reduceFunction = function (list, entry) {
        if (list.slice(-1)[0] !== entry) {
            list.push(entry);
        }
        return list;
    };
    ids = ids.sort().reduce(reduceFunction, []);
    
    return ids;
};

WTax.DAO.TaxPeriodDao.prototype.getQuarterOfBasePeriod = function _getQuarter(periodId, params) {
    try {
        var period = this.getPeriodById(periodId, params);
        if (period.isQuarter) {
            return period;
        }
        if (period.isYear) {
            throw nlapiCreateError('DATA_ERROR', 'Invalid period.');
        }
        
        var filters = [
                       new nlobjSearchFilter('isinactive', null, 'is', 'F'),
                       new nlobjSearchFilter('startdate', null, 'onorbefore', period.startDate),
                       new nlobjSearchFilter('enddate', null, 'onorafter', period.endDate),
                       new nlobjSearchFilter('isquarter', null, 'is', 'T'),
                       new nlobjSearchFilter('isyear', null, 'is', 'F')
                   ];
        var results = this.searchRecords(filters, null);
        return results[0];
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.TaxPeriodDao.getCoveredPeriodIds', WTaxError.LEVEL.ERROR);
    }
    
    return ids;
};

