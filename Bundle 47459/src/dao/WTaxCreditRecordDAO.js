/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.WTaxCreditRecord = function WTaxCreditRecord() {
    return {
        recordType: '',
        internalId: '',
        entity: '',
        subsidiary: '',
        tranDate: '',
        postingPeriod: '',
        account: '',
        classification: '',
        department: '',
        location: '',
        paymentRefId: '',
        docRefId: '',
        memo: '',
        currency: '',
        exchangeRate: '',
        totalWtaxAmount: 0,
        items: []
    };
}

WTax.DAO.WTaxCreditRecordDAO = function WTaxCreditRecordDAO() {
    this.LINES_PER_SEARCH = 1000;
};

WTax.DAO.WTaxCreditRecordDAO.prototype.getList = function getList(params) {
    var list = [];
    
    try {
        var filters = [new nlobjSearchFilter('mainline', null, 'is', 'T')];
        
        var columns = [
            new nlobjSearchColumn('custbody_4601_pymnt_ref_id'),
            new nlobjSearchColumn('custbody_4601_doc_ref_id'),
            new nlobjSearchColumn('memo')
        ];
        
        if (params && params.paymentRefId) {
            filters.push(new nlobjSearchFilter('internalid', 'custbody_4601_pymnt_ref_id', 'anyof', params.paymentRefId));
        }
        
        if (params && params.docRefId) {
            filters.push(new nlobjSearchFilter('internalid', 'custbody_4601_doc_ref_id', 'anyof', params.docRefId));
        }
        
        if (params && params.hasAmount) {
            filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula('ABS({amount})'));
        }
        
        if (params && params.internalId) {
            filters.push(new nlobjSearchFilter('internalid', null, 'is', params.internalId));
        }
        
        var sr = nlapiCreateSearch('transaction', filters, columns).runSearch();
        var resultSet = null;
        var index = 0;
        
        do {
            resultSet = sr.getResults(index, index + this.LINES_PER_SEARCH);
            
            for (var i = 0; i < (resultSet || []).length; i++) {
                list.push(this.convertToObject(resultSet[i]));
            }
            
            index += (resultSet || []).length;
        } while ((resultSet || []).length == this.LINES_PER_SEARCH);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.getList', WTaxError.LEVEL.ERROR);
    }
    
    return list;
};

WTax.DAO.WTaxCreditRecordDAO.prototype.convertToObject = function convertToObject(row) {
    try {
        var obj = new WTax.DAO.WTaxCreditRecord();
        obj.recordType = row.getRecordType();
        obj.internalId = row.getId();
        obj.docRefId = row.getValue('custbody_4601_doc_ref_id');
        obj.paymentRefId = row.getValue('custbody_4601_pymnt_ref_id');
        obj.memo = row.getValue('memo');
        
        return obj;
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.convertToObject', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.setFieldValues = function setFieldValues(record, values, isCreate) {
    try {
        if (isCreate) {
            record.setFieldValue('entity', values.entity);
            record.setFieldValue('subsidiary', values.subsidiary);
        }
        
        record.setFieldValue('trandate', new WTax.DateFormatter().standardFormatToString(values.tranDate));
        record.setFieldValue('period', values.postingPeriod);
        record.setFieldValue('account', values.account);
        record.setFieldValue('class', values.classification);
        record.setFieldValue('department', values.department);
        record.setFieldValue('location', values.location);
        record.setFieldValue('custbody_4601_pymnt_ref_id', values.paymentRefId);
        record.setFieldValue('custbody_4601_doc_ref_id', values.docRefId);
        record.setFieldValue('memo', values.memo);
        record.setFieldValue('currency', values.currency);
        record.setFieldValue('exchangerate', values.exchangeRate);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.setFieldValues', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.addItems = function addItems(record, values) {
    try {
        for (var i = 0; i < values.items.length; i++) {
            record.selectLineItem('item', i+1);
            record.setCurrentLineItemValue('item', 'item', values.items[i].item);
            record.setCurrentLineItemValue('item', 'rate', values.items[i].rate);
            record.setCurrentLineItemValue('item', 'taxcode', values.items[i].vatCode);
            
            if (values.department) {
                record.setCurrentLineItemValue('item', 'department', values.department);
            }
            
            if (values.classification) {
                record.setCurrentLineItemValue('item', 'class', values.classification);
            }
            
            if (values.location) {
                record.setCurrentLineItemValue('item', 'location', values.location);
            }
            
            record.commitLineItem('item');
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.addItems', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.applyToTransaction = function applyToTransaction(record, values) {
    try {
        var line = record.findLineItemValue('apply', 'doc', values.docRefId);
        if (line > 0) {
            record.selectLineItem('apply', line);
            record.setCurrentLineItemValue('apply', 'apply', 'T');
            record.setCurrentLineItemValue('apply', 'amount', Math.abs(values.totalWtaxAmount));
            record.commitLineItem('apply');
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.applyToTransaction', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.setRecordValues = function setRecordValues(record, values, isCreate) {
    try {
        this.setFieldValues(record, values, isCreate);
        this.addItems(record, values);
        this.clearAppliedTransactions(record);
        this.applyToTransaction(record, values);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.setRecordValues', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.clearItems = function clearItems(record) {
    try {
        var lineCount = record.getLineItemCount('item');
        
        for (var i = lineCount; i >= 1; i--) {
            if (i > 1) {
                record.removeLineItem('item', i);
            } else {
                record.selectLineItem('item', i);
                record.setCurrentLineItemValue('item', 'rate', 0);
                record.commitLineItem('item');
            }
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.clearItems', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.clearAppliedTransactions = function clearAppliedTransactions(record) {
    try {
        var lineCount = record.getLineItemCount('apply');
        
        for (var i = 1; i <= lineCount; i++) {
            if (record.getLineItemValue('apply', 'apply', i) == 'T') {
                record.selectLineItem('apply', i);
                record.setCurrentLineItemValue('apply', 'apply', '');
                record.setCurrentLineItemValue('apply', 'amount', '');
                record.commitLineItem('apply');
            }
        }
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.clearAppliedTransactions', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.create = function create(obj) {
    if(!obj) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A credit record object is required.');
        WTaxError.throwError(error, 'WTax.DAO.WTaxCreditRecordDAO.create', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var record = nlapiCreateRecord(obj.recordType, {recordmode: 'dynamic'});
        this.setRecordValues(record, obj, true);
        return nlapiSubmitRecord(record, true, true);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.create', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.cancel = function cancel(obj) {
    if(!obj) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A credit record object is required.');
        WTaxError.throwError(error, 'WTax.DAO.WTaxCreditRecordDAO.update', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var record = nlapiLoadRecord(obj.recordType, obj.internalId);
        record.setFieldValue('custbody_4601_pymnt_ref_id', '');
        record.setFieldValue('custbody_4601_doc_ref_id', '');
        record.setFieldValue('memo', obj.memo);
        
        //do not remove the items and applied transactions, just zero them out
        this.clearItems(record);
        this.clearAppliedTransactions(record);
        
        return nlapiSubmitRecord(record, true, true);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.cancel', WTaxError.LEVEL.ERROR);
    }
};

WTax.DAO.WTaxCreditRecordDAO.prototype.update = function update(obj) {
    if(!obj) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A credit record object is required.');
        WTaxError.throwError(error, 'WTax.DAO.WTaxCreditRecordDAO.update', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var record = nlapiLoadRecord(obj.recordType, obj.internalId, {recordmode: 'dynamic'});
        this.setRecordValues(record, obj, false);
        return nlapiSubmitRecord(record, true, true);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DAO.WTaxCreditRecordDAO.update', WTaxError.LEVEL.ERROR);
    }
};
