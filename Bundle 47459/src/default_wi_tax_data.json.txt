[
    {
        "country": "PH",
        "state": "",
        "lookup": true,
        "autoapply": true,
        "onpurcs": true,
        "notonpurcorders": true,
        "purctaxpoint": "onaccrual",
        "purcappliesto": "lines",
        "onsales": true,
        "notonsaleorders": true,
        "saletaxpoint": "onpayment",
        "saleappliesto": "lines",
        "wiTaxTypes": [
            {
                "name": "WE_PH",
                "description": "Expanded creditable withholding tax",
                "witaxbase": "amount",
                "includediscounts": false,
                "wiTaxCodes": 
                    [
                        {
                            "name": "WC010",
                            "description": "Professionals/ talent fees paid to juridical persons - If the current year's gross income is P720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC011",
                            "description": "Professionals/ talent fees paid to juridical persons - If the current year's gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC050",
                            "description": "Management and technical consultants paid to juridical person - If the current year's gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC051",
                            "description": "Management and technical consultants paid to juridical person - If the currents year's gross income exceeds P720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC100",
                            "description": "Rentals- real/personal properties, poles, satellites and transmission facilities, billiboards - Corporate",
                            "rate": 5.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC110",
                            "description": "Cinematographic film rentals - Corporate",
                            "rate": 5.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC120",
                            "description": "Income payments to prime contractors/sub-contractors - Corporate",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC140",
                            "description": "Gross commission or service fees of custom, insurance, stock, real estate, immigration and commercial brokers and fees of agents of professional entertainers - Corporate",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC156",
                            "description": "Payments made by credit card companies - Corporate",
                            "rate": 0.5,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC157",
                            "description": "Income payments made by the government to its local/resident suppliers of services - Corporate",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC158",
                            "description": "Income payments made by top 20,000 private corporations to their local/resident suppliers of goods - Corporate",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC160",
                            "description": "Income payments made by top 20,000 private corporations to their local/resident suppliers of services - Corporate",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC515",
                            "description": "Commission, rebates, discounts and other similar considerations paid/granted to independent and exclusive distributors, medical/technical and sales representatives and marketing agents and sub-agents of multi-level marketing companies - Corporate",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC535",
                            "description": "Payments made by pre-need companies to funeral parlors - Corporate",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC540",
                            "description": "Tolling fee paid to refineries - Corporate",
                            "rate": 5.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC610",
                            "description": "Income payments made to suppliers of Agricultural products - Corporate",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC630",
                            "description": "Income payments on purchases of minerals, mineral products and quarry resources - Corporate",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC632",
                            "description": "Income payments on purchases of gold by Bangko Sentral ng Pilipinas (BSP) from gold miners/suppliers inder PD 1899, as amended by RA No. 7076 - Corporate",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC640",
                            "description": "Income payments made by the government to its local/resident suppliers of goods - Corporate",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC650",
                            "description": "On gross amount of refund given by Meralco to customers with active contracts as classified by Meralco - Corporate",
                            "rate": 25.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC651",
                            "description": "On gross amount of refund given by Meralco to customers with terminated contracts as classified by Meralco - Corporate",
                            "rate": 32.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC660",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Residential and General Service customers whose monthly electricity consumption exceeds 200 kwh as classified by MERALCO - Corporate",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC661",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Non-Residential customers whose monthly electricity consumption exceeds 200 kwh as classified by MERALCO - Corporate",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC662",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Residential and General Service customers whose monthly electricity consumption exceeds 200 kwh as classified by other electric Distribution Utilities (DU) - Corporate",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC663",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Non-Residential customers whose monthly electricity consumption exceeds 200 kwh as classified by other electric Distribution Utilities (DU) - Corporate",
                            "rate": 20.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC670",
                            "description": "Income payments made by the top five thousand (5,000) individual taxpayers to their local/resident suppliers of goods other than those covered by other rates of withholding tax - Corporation",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC672",
                            "description": "Income payments made by the top five thousand (5,000) individual taxpayers to their local/resident suppliers of services other than those covered by other rates of withholding tax - ii) Corporation",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC680",
                            "description": "Income payments made by political parties and candidates of local and national elections of all their purchase of goods and services as campaign expenditures, and income payments made by individuals or juridical persons for their purchases of goods and services intended to be given as campaign contribution to political parties and candidates - Corporation",
                            "rate": 5.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WC690",
                            "description": "Income payments subject to Withholding Tax received by Real Estate Investment Trust (REIT)",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI010",
                            "description": "Professionals (lawyers, CPAs, engineers, etc.), talent fees paid to individuals - If the current year's gross income is P720,000 and below.",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI011",
                            "description": "Professionals (lawyers, CPAs, engineers, etc.), talent fees paid to individuals - If the current year's gross income exceeds P 720,000.",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI020",
                            "description": "Professional entertainers, such as, but not limited to, actors and actresses, singers, lyricist, composers, emcees - If the current year's gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI021",
                            "description": "Professional entertainers, such as, but not limited to, actors and actresses, singers, lyricist, composers, emcees - If the current year's gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI030",
                            "description": "Professional athletes, including basketball players, pelotaris and jockeys - If the current year's gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI031",
                            "description": "Professional athletes including basketball players, pelotaris and jockeys - If the current year's gross income exceeds P720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI040",
                            "description": "Movie, stage, radio, television and musical directors - If the current year's gross income is P720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI041",
                            "description": "Movie, stage, radio, television and musical directors - If the current year's gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI050",
                            "description": "Management and technical consultants paid to individuals. If the current year's gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI051",
                            "description": "Management and technical consultants paid to individuals - If the current year's gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI060",
                            "description": "Business and bookkeeping agents and agencies - If the current year's gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI061",
                            "description": "Business and bookkeeping agents and agencies - If the current year's gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI070",
                            "description": "Insurance agents and insurance adjusters - If the current year's gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI071",
                            "description": "Insurance agents and insurance adjusters - If the current year's gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI080",
                            "description": "Other recipients of talent fees - If the current year's gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI081",
                            "description": "Other recipients of talent fees - If the current year's gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI090",
                            "description": "Fees of directors who are not employees of the company - If the current year’s gross income is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI091",
                            "description": "Fees of directors who are not employees of the company - If the current year’s gross income exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI100",
                            "description": "Rentals- real/personal properties, poles, satellites and transmission facilities, billboards - Individual",
                            "rate": 5.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI110",
                            "description": "Cinematographic film rentals - Individual",
                            "rate": 5.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI120",
                            "description": "Income payments to prime contractors/sub-contractors - Individual",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI130",
                            "description": "Income distribution to beneficiaries of estates and trusts",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI140",
                            "description": "Gross commission or service fees of custom, insurance, stock, real estate, immigration and commercial brokers and fees of agents of professional entertainers - Individual",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI141",
                            "description": "Payments to medical practitioners by a duly registered professional partnership for the practice of the medical profession - If the currents year's income payments to the medical practitioner is P720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI142",
                            "description": "Payments to medical practitioners by a duly registered professional partnership for the practice of the medical profession - If the currents year's income payments to the medical practitioner exceeds P720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI150",
                            "description": "Payments for medical/dental/veterinary services thru Hospitals/ Clinics/Health Maintenance Organizations, including direct payments to service providers - If the current year's income payments for the medical/dental/veterinary services exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI151",
                            "description": "Payments for medical/dental/veterinary services thru Hospitals/ Clinics/Health Maintenance Organizations, including direct payments to service providers - If the current year's income payments for the medical/dental/veterinary services is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI152",
                            "description": "Payment by the general professional partnerships GPP) to its partners - If the current year's income payments to the partners is P 720,000 and below",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI153",
                            "description": "Payment by the general professional partnerships GPP) to its partners - If the current year's income payments to the partners exceeds P 720,000",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI156",
                            "description": "Payments made by credit card companies - Individual",
                            "rate": 0.5,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI157",
                            "description": "Income payments made by the government to its local/resident suppliers of service - Individual",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI158",
                            "description": "Income payments made by top 20,000 private corporations to their local/resident suppliers of goods - Individual",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI159",
                            "description": "Additional payments to government personnel from importers, shipping and airline companies or their agents for overtime services",
                            "rate": 15.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI160",
                            "description": "Income payments made by top 20,000 private corporations to their local/resident suppliers of services - Individual",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI515",
                            "description": "Commission, rebates, discounts and other similar considerations paid/granted to independent and exclusive distributors, medical/technical and sales representatives and marketing agents and sub-agents of multi-level marketing companies - Individual",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI530",
                            "description": "Gross payments to embalmers by funeral parlors",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI535",
                            "description": "Payments made by pre-need companies to funeral parlors - Individual",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI540",
                            "description": "Tolling fee paid to refineries - Individual",
                            "rate": 5.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI610",
                            "description": "Income payments made to suppliers of Agricultural products - Individual",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI630",
                            "description": "Income payments on purchases of minerals, mineral products and quarry resources - Individual",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI632",
                            "description": "Income payments on purchases of gold by Bangko Sentral ng Pilipinas (BSP) from gold miners/suppliers under PD 1899, as amended by RA No. 7076- Individual",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI640",
                            "description": "Income payments made by the government to its local/resident suppliers of goods - Individual",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI650",
                            "description": "On gross amount of refund given by Meralco to customers with active contracts as classified by Meralco - Individual",
                            "rate": 25.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI651",
                            "description": "On gross amount of refund given by Meralco to customers with terminated contracts as classified by Meralco - Individual",
                            "rate": 32.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI660",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Residential and General Service customers whose monthly electricity consumption exceeds 200 kwh as classified by MERALCO - Individual",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI661",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Non-Residential customers whose monthly electricity consumption exceeds 200 kwh as classified by MERALCO - Individual",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI662",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Residential and General Service customers whose monthly electricity consumption exceeds 200 kwh as classified by other electric Distribution Utilities (DU) - Individual",
                            "rate": 10.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI663",
                            "description": "Withholding on gross amount of interest on the refund of meter deposit whether paid directly to the customers or applied against customer's billing - Non-Residential customers whose monthly electricity consumption exceeds 200 kwh as classified by other electric Distribution Utilities (DU) - Individual",
                            "rate": 20.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI670",
                            "description": "Income payments made by the top five thousand (5,000) individual taxpayers to their local/resident suppliers of goods other than those covered by other rates of withholding tax - Individual",
                            "rate": 1.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI672",
                            "description": "Income payments made by the top five thousand (5,000) individual taxpayers to their local/resident suppliers of services other than those covered by other rates of withholding tax - i) Individual",
                            "rate": 2.0,
                            "availableon": "Both" 
                        },
                        {
                            "name": "WI680",
                            "description": "Income payments made by political parties and candidates of local and national elections of all their purchase of goods and services as campaign expenditures, and income payments made by individuals or juridical persons for their purchases of goods and services intended to be given as campaign contribution to political parties and candidates - Individual",
                            "rate": 5.0,
                            "availableon": "Both" 
                        }
                    ]
            }
        ]
    }
]
