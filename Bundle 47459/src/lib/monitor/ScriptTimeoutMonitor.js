/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};


WTax.ScriptTimeoutMonitor = function(type) {
    if (!type) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A monitor type is required.');
        WTaxError.throwError(error, 'WTax.ScriptTimeoutMonitor', WTaxError.LEVEL.ERROR);
    }
    
    var thresholds = {
        suitelet: 150000, //actual limit is 300,000ms
        scheduledscript: 1800000 //actual limit is 3,600,000ms
    };
    
    WTax.ThresholdMonitor.call(this, {
        type: type,
        thresholds: thresholds
    });
    
    this.startTime = WTax.ScriptTimeoutMonitor.getCurrentTime();
};
WTax.ScriptTimeoutMonitor.prototype = Object.create(WTax.ThresholdMonitor.prototype);


WTax.ScriptTimeoutMonitor.prototype.isThresholdReached = function() {
    return (WTax.ScriptTimeoutMonitor.getCurrentTime() - this.startTime) > this.thresholdValue;
};


WTax.ScriptTimeoutMonitor.getCurrentTime = function() {
    return (new Date()).valueOf();
};
