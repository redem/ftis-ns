/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};


/**
 * @param type - The script type, e.g. suitelet, scheduledscript, etc. 
 */
WTax.ApiGovernanceMonitor = function(type) {
    if (!type) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A monitor type is required.');
        WTaxError.throwError(error, 'WTax.ApiGovernanceMonitor', WTaxError.LEVEL.ERROR);
    }
    
    var thresholds = {
        suitelet: 50,
        scheduledscript: 50
    };
    
    WTax.ThresholdMonitor.call(this, {
        type: type,
        thresholds: thresholds
    });
    
    this.context = nlapiGetContext();
};
WTax.ApiGovernanceMonitor.prototype = Object.create(WTax.ThresholdMonitor.prototype);


/**
 * @returns {Boolean} Whether or not the threshold was reached.
 */
WTax.ApiGovernanceMonitor.prototype.isThresholdReached = function() {
    return this.context.getRemainingUsage() < this.thresholdValue;
};
