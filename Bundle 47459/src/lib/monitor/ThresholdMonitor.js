/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};


WTax.ThresholdMonitor = function(params) {
    this.type = params.type;
    this.thresholds = params.thresholds;
    this.thresholdValue = this.getThresholdValue(this.type);
};


WTax.ThresholdMonitor.prototype.isThresholdReached = function() {
    return false;
};


WTax.ThresholdMonitor.prototype.getThresholdValue = function(type) {
    return this.thresholds[this.type] || 0;
};
