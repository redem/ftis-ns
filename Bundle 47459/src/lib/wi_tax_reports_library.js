/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var SFC;
if (!SFC) SFC = {};
if (!SFC.System) SFC.System = {};

/*
 *  Netsuite version is being monitored on this implementation to cater versions earlier than 2013.1
 *  which did not yet use Multiple Fiscal Calendar. This can be removed at a later time,
 *  when it is assured that all accounts using Withholding Tax bundle has the feature Multiple Fiscal Calendar 
 */
SFC.NetsuiteVersion = "2013.1";
SFC.IsUpgraded = parseFloat(nlapiGetContext().getVersion()) >= parseFloat(SFC.NetsuiteVersion);
SFC.IsOW = nlapiGetContext().getSetting("FEATURE", "SUBSIDIARIES") == "T";

var _4601;
if (!_4601) {
    _4601 = {};
}

_4601.isMultipleCalendar = nlapiGetContext().getFeature('multiplecalendars');

_4601.escapeTextForCSV = function escapeTextForCSV(strToConvert) {
    if (typeof strToConvert == 'string')
        strToConvert = _4601.unescape(strToConvert).replace(/"/g, '""');
    strToConvert = '"' + strToConvert + '"';
    return strToConvert;
};

_4601.escapeTextForPDF = function escapeTextForPDF(strToConvert) {
    if (typeof strToConvert == 'string') {
        strToConvert = nlapiEscapeXML(_4601.unescape(strToConvert));
    }
    return strToConvert;
};

_4601.unescape = function unescape(strToConvert) {
    if (typeof strToConvert == 'string') {
        return strToConvert.replace(/&amp;/g, '&')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'");
    }
    return strToConvert;
};


_4601.GetHeaderInfo = function GetHeaderInfo(subId, taxPeriodId, searchType)
{
    var headerInfo = { 
        PayeeName: "", 
        LegalName: '',
        Name: '',
        PayeeTIN: "", 
        RawTIN: '',
        StartDate: null, 
        EndDate: null 
    };

    var taxPeriod = nlapiLoadRecord("taxperiod", taxPeriodId);

    headerInfo.StartDate = taxPeriod.getFieldValue("startdate");
    headerInfo.EndDate = taxPeriod.getFieldValue("enddate");

    var locale = 1;
    if (nlapiGetContext().getSetting("FEATURE", "SUBSIDIARIES") == "T") //if OW
    {
        var sub = nlapiLoadRecord("subsidiary", subId);
        if (sub.getFieldValue("parent")) //if other than the Parent Company
        {
            headerInfo.PayeeName = sub.getFieldValue("legalname");
            headerInfo.LegalName = sub.getFieldValue("legalname");
            headerInfo.Name = sub.getFieldValue("name");
            headerInfo.PayeeTIN = searchType == 'MAP_DETAIL' || searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(sub.getFieldValue("federalidnumber")) : sub.getFieldValue("federalidnumber");
            headerInfo.RawTIN = sub.getFieldValue('federalidnumber');

            var currencyid = sub.getFieldValue("currency"); 
            if(parseInt(currencyid,10)){ //if currencyid is integer
                var currecord = nlapiLoadRecord("currency", currencyid);
                locale = currecord.getFieldValue("locale");
            }
            else if(currencyid != null){ //when currencyid is text - ie Indonesian Rupiah / Japanese Yen
                var sr = nlapiSearchRecord("currency", null, new nlobjSearchFilter('name', null, 'is', currencyid), null);
                if(sr != null){
                    currecord = nlapiLoadRecord("currency", sr[0].getId());
                    locale = currecord.getFieldValue("locale");
                }
            } 
            //if currencyid is null means MULTICURRENCY feature is off
            
            headerInfo.CurrencyLocale =  locale;

        }else{ //Parent Company
            var ci = nlapiLoadConfiguration("companyinformation");
            headerInfo.PayeeName = ci.getFieldValue("legalname");
            headerInfo.LegalName = sub.getFieldValue("legalname");
            headerInfo.Name = sub.getFieldValue("companyname");
            headerInfo.PayeeTIN = searchType == 'MAP_DETAIL' || searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(ci.getFieldValue("employerid")) : ci.getFieldValue("employerid");
            headerInfo.RawTIN = ci.getFieldValue('employerid');

            var currencyid = nlapiLookupField("subsidiary","1","currency"); 
            
            if(parseInt(currencyid,10)){
                var currecord = nlapiLoadRecord("currency", currencyid);
                locale = currecord.getFieldValue("locale");
            }
            
            headerInfo.CurrencyLocale =  locale;
        }
    }else{ //SI
        var ci = nlapiLoadConfiguration("companyinformation");
        headerInfo.PayeeName = ci.getFieldValue("legalname");
        headerInfo.LegalName = ci.getFieldValue("legalname");
        headerInfo.Name = ci.getFieldValue("companyname");
        headerInfo.PayeeTIN = searchType == 'MAP_DETAIL' || searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(ci.getFieldValue("employerid")) : ci.getFieldValue("employerid");
        headerInfo.RawTIN = ci.getFieldValue('employerid');
        headerInfo.CurrencyLocale =  locale;
    }
    
    headerInfo.PayeeName = !!headerInfo.PayeeName ? headerInfo.PayeeName : '';
    headerInfo.PayeeTIN = !!headerInfo.PayeeTIN ? headerInfo.PayeeTIN : '';
    
    return headerInfo;
};

_4601.HeaderData = function HeaderData(subId, taxPeriodId, searchType)
{
    var headerInfo = { payeeRegisteredName: "", payeeTradeName: "", payeeTradeName: "", payeeTin: "", returnPeriodFromDate: null, returnPeriodToDate: null };

    var taxPeriod = nlapiLoadRecord("taxperiod", taxPeriodId);
    headerInfo.periodDate = taxPeriod.getFieldValue("enddate");
    headerInfo.returnPeriodFromDate = nlapiStringToDate(taxPeriod.getFieldValue("startdate")).toString("MMMM d, yyyy");
    headerInfo.returnPeriodToDate = nlapiStringToDate(taxPeriod.getFieldValue("enddate")).toString("MMMM d, yyyy");

    var locale = 1;
    
    if (nlapiGetContext().getSetting("FEATURE", "SUBSIDIARIES") == "T") //if OW
    {
        var sub = nlapiLoadRecord("subsidiary", subId);
        if (sub.getFieldValue("parent")) //if other than the Parent Company
        {
            headerInfo.payeeRegisteredName = sub.getFieldValue('legalname');
            headerInfo.payeeTradeName = headerInfo.payeeRegisteredName;
            headerInfo.payeeAddress = sub.getFieldValue('addrtext');
            headerInfo.payeeTin = searchType == 'MAP_DETAIL' || searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(sub.getFieldValue("federalidnumber")) : sub.getFieldValue("federalidnumber");
            headerInfo.rawTin = sub.getFieldValue("federalidnumber");
            headerInfo.companyName = sub.getFieldValue("name");

            var currencyid = sub.getFieldValue("currency"); 
            
            if(parseInt(currencyid,10)){
                var currecord = nlapiLoadRecord("currency", currencyid);
                locale = currecord.getFieldValue("locale");
            }else if(currencyid != null){ //when currencyid is text - ie Indonesian Rupiah / Japanese Yen
                var sr = nlapiSearchRecord("currency", null, new nlobjSearchFilter('name', null, 'is', currencyid), null);
                if(sr != null){
                    var currecord = nlapiLoadRecord("currency", sr[0].getId());
                    locale = currecord.getFieldValue("locale");
                }
            }
            
          //if currencyid is null means MULTICURRENCY feature is off
            
            headerInfo.payeeCurrencyLocale =  locale;
        }else{ //Parent Company
              var companyInfo = nlapiLoadConfiguration("companyinformation");
              headerInfo.payeeRegisteredName = companyInfo.getFieldValue("legalname");
              headerInfo.payeeTradeName = headerInfo.payeeRegisteredName;
              headerInfo.payeeAddress = companyInfo.getFieldValue("addresstext");
              headerInfo.payeeTin = searchType == 'MAP_DETAIL' || searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(companyInfo.getFieldValue("employerid")) : companyInfo.getFieldValue("employerid");
              headerInfo.rawTin = companyInfo.getFieldValue('employerid');
              headerInfo.companyName = companyInfo.getFieldValue("companyname");
            
            var currencyid = nlapiLookupField("subsidiary","1","currency"); 
            
            if(parseInt(currencyid,10)){
                var currecord = nlapiLoadRecord("currency", currencyid);
                locale = currecord.getFieldValue("locale");
            }
            
            nlapiLogExecution('debug','none-locale: '+locale);
            headerInfo.payeeCurrencyLocale =  locale;
        }
    }else{ //SI
          var companyInfo = nlapiLoadConfiguration("companyinformation");
          headerInfo.payeeRegisteredName = companyInfo.getFieldValue("legalname");
          headerInfo.payeeTradeName = headerInfo.payeeRegisteredName;
          headerInfo.payeeAddress = companyInfo.getFieldValue("addresstext");
          headerInfo.payeeTin = searchType == 'MAP_DETAIL' || searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(companyInfo.getFieldValue("employerid")) : companyInfo.getFieldValue("employerid");
          headerInfo.rawTin = companyInfo.getFieldValue('employerid');
          headerInfo.companyName = companyInfo.getFieldValue("companyname");
        
        nlapiLogExecution('debug','none-locale: '+locale);
        headerInfo.payeeCurrencyLocale =  locale;
    }
    
    headerInfo.payeeRegisteredName = !!headerInfo.payeeRegisteredName ? headerInfo.payeeRegisteredName : '';
    headerInfo.payeeTradeName = !!headerInfo.payeeTradeName ? headerInfo.payeeTradeName : '';
    headerInfo.payeeAddress = !!headerInfo.payeeAddress ? headerInfo.payeeAddress : '';
    headerInfo.payeeTin = !!headerInfo.payeeTin ? headerInfo.payeeTin : '';

    return headerInfo;
};

/**
 * var wHTaxTypeKindMap = WithholdingTaxTypeKindMap();
 * var arrInternalIds = wHTaxTypeKindMap.get(['Creditable']);
 */
_4601.WithholdingTaxTypeKindMap = function WithholdingTaxTypeKindMap() {
    var taxTypeKindMap;
    
    function searchAllWithholdingTaxTypeKinds() {
        var filters, columns, results;
        
        filters = [];
        columns = [];

        columns.push(new nlobjSearchColumn('internalid'));
        columns.push(new nlobjSearchColumn('custrecord_ph4014_wtax_type_kind_name'));
        results = nlapiSearchRecord('customrecord_ph4014_wtax_type_kind', null, filters, columns);
        
        if (results === null) {
            results = [];
        }
        
        return results;
    }
    
    function createMapOfWithholdingTaxTypeKindsToId(taxTypeKindResults) {
        var i, inputLength, map;
        
        map = {};
        inputLength = taxTypeKindResults.length;
        
        nlapiLogExecution('DEBUG','createMapOfWithholdingTaxTypeKindsToId', inputLength);
        for (i = 0; i < inputLength; i += 1) {
            map[taxTypeKindResults[i].getValue('custrecord_ph4014_wtax_type_kind_name')] = taxTypeKindResults[i].getValue('internalid');
            
            nlapiLogExecution('DEBUG','Map', '[' + taxTypeKindResults[i].getValue('custrecord_ph4014_wtax_type_kind_name') + ']' + 
                    ' = ' + taxTypeKindResults[i].getValue('internalid'));
        }
        return map;
    }
    
    taxTypeKindMap = createMapOfWithholdingTaxTypeKindsToId(
        searchAllWithholdingTaxTypeKinds());
        
    return {
        get : function (keys) {
            var i, keysLength, taxTypeKindIds;

            taxTypeKindIds = [];
            keysLength = keys.length;
            for (i = 0; i <  keysLength; i += 1) {
                if (typeof(taxTypeKindMap[keys[i]]) !== 'undefined') {
                    taxTypeKindIds.push(taxTypeKindMap[keys[i]]);
                }
            }
            return taxTypeKindIds;
        }
    };
}

_4601.WithholdingTaxTypeFetcher = function WithholdingTaxTypeFetcher() {
    
    /**
     * wTaxTypeKindIds - a NON-empty array of withholding type kind ids
     */
    function searchByWithholdingTaxTypeKinds(wTaxTypeKindIds)   {
        var filters, columns, results;
        
        filters = [];
        columns = [];
        
        filters.push(
            new nlobjSearchFilter(
                'custrecord_ph4014_wtax_type_kind_ref', null, 'anyof', wTaxTypeKindIds));
        columns.push(
            new nlobjSearchColumn('internalid'));
                
        results = nlapiSearchRecord(
            'customrecord_4601_witaxtype', null, filters, columns);
        if (results === null) {
            results = [];
        }
        return results;
    }
    
    function createWithholdingTaxTypeIdsFromSearchResult(wTaxTypeResults) {
        var i, inputLength, wTaxTypeIds;
        
        wTaxTypeIds = [];
        inputLength = wTaxTypeResults.length;
        for (i = 0; i < inputLength; i += 1) {
            wTaxTypeIds.push(wTaxTypeResults[i].getValue('internalid'));
        }
        return wTaxTypeIds;
    }
    
    return {
        getTaxTypesFilteredByKind : function (wTaxTypeKindIds) {
            if (!wTaxTypeKindIds || wTaxTypeKindIds.length === 0) {
                throw {
                    name: 'wTaxTypeKindIds must be a non-empty array',
                    message: 'getTaxTypesFilteredByKind(wTaxTypeKindIds)'
                };
            }
            return createWithholdingTaxTypeIdsFromSearchResult(
                searchByWithholdingTaxTypeKinds(wTaxTypeKindIds));
        }
    };
}

_4601.WithholdingTaxCodeFetcher = function WithholdingTaxCodeFetcher() {

        function searchWithholdingTaxCodesByTaxType(withholdingTaxTypeIds) {
            var filters, columns, results;
            filters = [];
            columns = [];
            
            var arrTaxTypeIds = withholdingTaxTypeIds.split('');
            
            columns.push(new nlobjSearchColumn('internalid'));
            filters.push(new nlobjSearchFilter('custrecord_4601_wtc_witaxtype', null, 'anyof', arrTaxTypeIds));
            filters.push(new nlobjSearchFilter('custrecord_4601_wtc_istaxgroup', null, 'is', 'F'));

            results = nlapiSearchRecord('customrecord_4601_witaxcode', null, filters, columns);
            
            return results || [];
        }
        
        function createWithholdingTaxCodeIdsFromSearchResult(wTaxCodeResults) {
            var i, inputLength, wTaxCodeIds;
            
            wTaxCodeIds = [];
            inputLength = wTaxCodeResults.length;
            for (i = 0; i < inputLength; i += 1) {
                wTaxCodeIds.push(wTaxCodeResults[i].getValue('internalid'));
            }
            return wTaxCodeIds;
        }
        
        return {
            getTaxCodesFilteredByType : function (withholdingTaxTypeIds) {
                if (!withholdingTaxTypeIds || withholdingTaxTypeIds.length === 0) {
                throw {
                    name: 'withholdingTaxTypeIds must be a non-empty array',
                    message: 'getTaxCodesFilteredByType(withholdingTaxTypeIds)'
                };
            }
                return createWithholdingTaxCodeIdsFromSearchResult(
                    searchWithholdingTaxCodesByTaxType(withholdingTaxTypeIds));
            }
        };
};


_4601.createTransactionJournalFilterByTaxCode = function createTransactionJournalFilterByTaxCode(wTaxCodeIds) {
    return new nlobjSearchFilter(
        'custcol_4601_witaxcode', null, 'anyof', wTaxCodeIds);
};


/**
 * This is a workaround for the quarter tax period
 * 
 * @param filters
 * @param objSelectedPeriod
 * @returns
 */
_4601.getTaxPeriodFilters = function getTaxPeriodFilters(filters, objSelectedPeriod){
    try {
        if (filters.length > 0)
            filters.push('AND');
        
        if (objSelectedPeriod.GetType() == 'month') {
            filters.push(['taxperiod', 'is', objSelectedPeriod.GetId()]);
        } else if (objSelectedPeriod.GetType() == 'quarter') {
            var childrenTaxPeriod = objSelectedPeriod.GetChildren();
            var childTaxPeriodFilter = [];
            for (var i in childrenTaxPeriod) {
                if (i > 0)
                    childTaxPeriodFilter.push('OR');
                childTaxPeriodFilter.push(['taxperiod', 'is', childrenTaxPeriod[i].GetId()]);
            }
            filters.push(childTaxPeriodFilter);
        }

        return filters;
    } catch(ex) {
        throw nlapiCreateError('WITHHOLDING TAX: Invalid period', 'WITHHOLDING TAX: Invalid period <br>' + ex.toString());
    }
};


/**
 * Filters to handle journals with Reversal Dates
 * 
 * @param filters
 * @returns
 */
_4601.getJournalReversalFilters = function getJournalReversalFilters(filters){
    try {
        
        var today = new Date();
        var reversalDateFilters = [];
        
        if (filters.length > 0)
            filters.push('AND');
        
        reversalDateFilters.push(['reversaldate', 'isempty', null]);
        reversalDateFilters.push('OR');
        reversalDateFilters.push(['reversaldate', 'after', today]);
        
        filters.push(reversalDateFilters);
        
        return filters;
    } catch(ex) {
        throw nlapiCreateError('WITHHOLDING TAX: Invalid Journal Reversal Date', 'WITHHOLDING TAX: Invalid journal reversal date <br>' + ex.toString());
    }
};


/**
 * 
 * @param filters
 * @param join
 * @param wtaxCodes - {
            allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
            groupWtaxCodes: groupWtaxCodes}
 * @returns
 */
_4601.getWTaxCodeFilters = function getWTaxCodeFilters(filters, join, wtaxCodes) {
    var taxCodeJoin = !!join ? join : 'custcol_4601_witaxcode';
    var taxCodes = wtaxCodes.groupWtaxCodes ? wtaxCodes.allowedWithholdingTaxCodeIds.concat(wtaxCodes.groupWtaxCodes) : wtaxCodes.allowedWithholdingTaxCodeIds;
    filters.push('AND');
    filters.push([taxCodeJoin, 'anyof', taxCodes]);

    return filters;
}; 
 

/**
 * 
 * @param {Integer[]} arrValidSubsidiaries
 * @param {String} fieldName
 * @param {String} fieldLabel
 * @param {Integer} idSelectedDefault
 * @param {String} fieldLayoutType
 */
_4601.SubsidiaryComboboxConfiguration = (function () {

    var SubsidiaryComboboxConfiguration = function (
        validSubsidiaryIds, fieldName, fieldLabel, idSelectedDefault, fieldLayoutType, fieldBreakType) 
    {

        function getFieldName() {
            return fieldName;
        }
        this.getFieldName = getFieldName;
        
        function getFieldLabel() {
            return fieldLabel;
        }
        this.getFieldLabel = getFieldLabel;
        
        function getIdSelectedDefault() {
            return idSelectedDefault;
        }
        this.getIdSelectedDefault = getIdSelectedDefault;
        
        function getFieldLayoutType() {
            return fieldLayoutType;
        }
        this.getFieldLayoutType = getFieldLayoutType;
        
        function getFieldBreakType() {
            return fieldBreakType;
        }
        this.getFieldBreakType = getFieldBreakType;
        
        function getValidSubsidiaryIds() {
            return validSubsidiaryIds;
        }
        this.getValidSubsidiaryIds = getValidSubsidiaryIds;
    };
    
    return function (
        validSubsidiaryIds, fieldName, fieldLabel, idSelectedDefault, fieldLayoutType, fieldBreakType) 
    {
        return new SubsidiaryComboboxConfiguration(
            validSubsidiaryIds, fieldName, fieldLabel, idSelectedDefault, fieldLayoutType, fieldBreakType);
    };
})();

_4601.FilteredSubsidiaryCombobox = (function () {
    
    var FilteredSubsidiaryCombobox = function (form, subsComboboxConfig) {
        var cbo = form.addField(
            subsComboboxConfig.getFieldName(), 
            "select", 
            subsComboboxConfig.getFieldLabel());

        if (subsComboboxConfig.getFieldLayoutType() != null)
            cbo.setLayoutType(subsComboboxConfig.getFieldLayoutType());
        
        if (subsComboboxConfig.getFieldBreakType() != null)
            cbo.setBreakType(subsComboboxConfig.getFieldBreakType());
        
        cbo.addSelectOption('', '');

        if (subsComboboxConfig.getValidSubsidiaryIds().length > 0 && 
            subsComboboxConfig.getValidSubsidiaryIds() instanceof Array) {
            var filters = new Array();
            var columns = [new nlobjSearchColumn("name")];
            
            filters.push(new nlobjSearchFilter("isinactive", null, "is", "F"));
            filters.push(new nlobjSearchFilter('internalid', null, 'anyof', subsComboboxConfig.getValidSubsidiaryIds()));      
            
            var rs = nlapiSearchRecord("subsidiary", null, filters, columns);
            if (rs == null)
                return;
        
            for (var i in rs) {
                var id = rs[i].getId();
                var isSelected = (subsComboboxConfig.getIdSelectedDefault() == id);
                cbo.addSelectOption(id, rs[i].getValue("name"), isSelected);
            }
        }
        
        return cbo;
    };
    
    return function (form, subsComboboxConfig) {
        return new FilteredSubsidiaryCombobox(form, subsComboboxConfig); 
    };
})();

_4601.getSubsidiaryIdsThatWTaxAppliesTo = function getSubsidiaryIdsThatWTaxAppliesTo() {
    var subsidiaryIds = new Array();
    
    // Temporary fix for 249008, validation for isOneWorld should be done in the reports
    if (nlapiGetContext().getSetting("FEATURE", "SUBSIDIARIES") == "T") {
        // STEP 1: Get Wtax Setups of all Wtax codes to be able to get the nexuses that has been applied with wtax 
        var searchColumns = new Array();
        var searchFilters = new Array();

        searchColumns.push(new nlobjSearchColumn('custrecord_4601_wtt_witaxsetup', 'custrecord_4601_wtc_witaxtype', 'group').setSort());
        searchFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

        var wTaxSetups = nlapiSearchRecord('customrecord_4601_witaxcode', null, searchFilters, searchColumns);

        if (wTaxSetups != null && wTaxSetups.length > 0) {
            var arrTaxSetups = new Array();

            for (var i in wTaxSetups)
                arrTaxSetups.push(wTaxSetups[i].getValue(searchColumns[0]));

            // STEP 2: Get nexuses of wtax setups

            var nexusColumns = new Array();
            var nexusFilters = new Array();

            nexusColumns.push(new nlobjSearchColumn('custrecord_4601_wts_nexus'));
            nexusFilters.push(new nlobjSearchFilter('internalid', null, 'anyof', arrTaxSetups));

            var wTaxNexus = nlapiSearchRecord('customrecord_4601_witaxsetup', null, nexusFilters, nexusColumns);

            if (wTaxNexus != null && wTaxNexus.length > 0) {
                var arrTaxNexus = new Array();
                for (var j in wTaxNexus)
                    arrTaxNexus.push(wTaxNexus[j].getValue(nexusColumns[0]));

                // STEP 3: Get the country of nexuses which will be used to filter the subsidiaries.
                // This step is necessary since nexus cannot be used as a List in the Wtax Setup record type
                var countryColumns = new Array();
                var countryFilters = new Array();

                countryColumns.push(new nlobjSearchColumn('country'));
                countryFilters.push(new nlobjSearchFilter('internalid', null, 'anyof', arrTaxNexus));

                var wTaxCountry = nlapiSearchRecord('nexus', null, countryFilters, countryColumns);

                if (wTaxCountry!= null && wTaxCountry.length > 0) {
                    var arrTaxCountry = new Array();
                    for (var k in wTaxCountry)
                        arrTaxCountry.push(wTaxCountry[k].getValue(countryColumns[0]));

                    // STEP 4: Get apt subsidiaries related to wtax with respect to the role
                    var subColumns = new Array();
                    var subFilters = new Array();
                    var subsidiaries = new Array();

                    // STEP 4a: Create a dummy wtax setup to be able to get the list of subsidiaries

                    var obj = nlapiCreateRecord('contact');
                    var subsidiary =  obj.getField('subsidiary');
                    var list = subsidiary.getSelectOptions();

                    for(var m = 0; m < list.length; m++) {
                        subsidiaries.push(list[m].getId());
                    }

                    // STEP 4b: Search the subsidiaries  
                    subColumns.push(new nlobjSearchColumn('internalid'));
                    subColumns.push(new nlobjSearchColumn('name'));
                    subFilters.push(new nlobjSearchFilter('country', null, 'anyof', arrTaxCountry));
                    subFilters.push(new nlobjSearchFilter('iselimination', null, 'is', 'F'));
                    subFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
                    subFilters.push(new nlobjSearchFilter('internalid', null, 'anyof', subsidiaries));

                    var wTaxSubs = nlapiSearchRecord('subsidiary', null, subFilters, subColumns);

                    if (wTaxSubs != null && wTaxSubs.length > 0) {
                        for (var l in wTaxSubs)
                            subsidiaryIds.push(wTaxSubs[l].getValue('internalid'));
                    }
                }
            }
        }
    }

    return subsidiaryIds;

};

_4601.FilteredVendorCombobox = function FilteredVendorCombobox(form, fieldName, fieldLabel, idSelectedDefault, fieldLayoutType, relatedParam) {
    var cbo = form.addField(
        fieldName, 
        "select", 
        fieldLabel);
    
    if (fieldLayoutType != null) {
        cbo.setLayoutType(fieldLayoutType);
    }
    
    var columns = new Array();
    var filters = new Array();
    
    columns.push(new nlobjSearchColumn('companyname'));
    columns.push(new nlobjSearchColumn('firstname'));
    columns.push(new nlobjSearchColumn('middlename'));
    columns.push(new nlobjSearchColumn('lastname'));
    columns.push(new nlobjSearchColumn('isperson'));
    columns.push(new nlobjSearchColumn('country'));
    
    filters.push(new nlobjSearchFilter('category', null, 'noneof', _4601.getTaxAgencyVendorCategory()));
    filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula('{internalid}'));
    
    if ((relatedParam.subId != null) && (relatedParam.subId != undefined)){
        filters.push(new nlobjSearchFilter('subsidiary', null, 'is', relatedParam.subId));
    }
    else{
        filters.push(new nlobjSearchFilter('country', null, 'is', relatedParam.nexusId));
    }
    //cbo.addSelectOption("", "");
    var rs = nlapiSearchRecord('vendor', null, filters, columns);
    if (rs != null) {
        for (var i in rs)
        {
            var id = rs[i].getId();
            var isSelected = (idSelectedDefault == id);
            var textSelect = rs[i].getValue('isperson') == 'F' ? rs[i].getValue('companyname') : (rs[i].getValue('firstname') + ' ' + rs[i].getValue('middlename') + ' ' + rs[i].getValue('lastname'));
            cbo.addSelectOption(id, textSelect, isSelected);
        }
    }
    
    return cbo;
};

_4601.NexusCombobox = function NexusCombobox(form, param) { //fieldName, fieldLabel, idSelectedDefault, fieldLayoutType, fieldBreakType
    var cbo = form.addField(
        param.fieldName, 
        "select", 
        param.fieldLabel);
    
    if (param.fieldLayoutType != null) {
        cbo.setLayoutType(param.fieldLayoutType);
    }
    
    if (param.fieldBreakType != null) {
        cbo.setBreakType(param.fieldBreakType);
    }
    
    var nexusColumns = new Array();
    var nexusFilters = new Array();
    
    nexusColumns.push(new nlobjSearchColumn('custrecord_4601_wts_nexus'));
    
    var rsNexus = nlapiSearchRecord('customrecord_4601_witaxsetup', null, nexusFilters, nexusColumns);

    var arrTaxNexus = new Array();
    for (var i in rsNexus) {
        arrTaxNexus.push(rsNexus[i].getValue(nexusColumns[0]));
    }

    var countryColumns = new Array();
    var countryFilters = new Array();

    countryColumns.push(new nlobjSearchColumn('description').setSort());
    countryColumns.push(new nlobjSearchColumn('country'));
    countryFilters.push(new nlobjSearchFilter('internalid', null, 'anyof', arrTaxNexus));

    var rsCountry = nlapiSearchRecord('nexus', null, countryFilters, countryColumns);
    
    cbo.addSelectOption('', '');
    
    if (rsCountry != null) {
        for (var i in rsCountry)
        {
            var id = rsCountry[i].getValue('country'); //rsCountry[i].getId();
            var isSelected = (param.idSelectedDefault == id);
            var textSelect = rsCountry[i].getValue('description');
            cbo.addSelectOption(id, textSelect, isSelected);
        }
    }
    
    return cbo;
};


_4601.populateTotalAmountMap = function populateTotalAmountMap(arrTransID) {
    var totalAmountMap = [];
    var filters = [];
    var columns = [];
    
    filters.push(new nlobjSearchFilter('internalid', null, 'anyof', arrTransID));
    filters.push(new nlobjSearchFilter('custcol_4601_witaxapplies', null, 'is', 'T'));
    
    columns.push(new nlobjSearchColumn('custrecord_4601_wtc_name', 'custcol_4601_witaxcode'));
    columns.push(new nlobjSearchColumn('custcol_4601_witaxcode'));
    columns.push(new nlobjSearchColumn('formulacurrency').setFormula('{custcol_4601_witaxamount}'));
    columns.push(new nlobjSearchColumn('formulacurrency').setFormula('{custcol_4601_witaxbaseamount}'));
    
    var wtaxLines = nlapiCreateSearch('transaction', filters, columns).runSearch();
    
    var index = 0;
    var MAX_LINES = 1000;
    var taxBase = 0.0;
    var taxAmount = 0.0;
    var transID = null;
    var sr = null;
    var srLength = 0;
    
    try {
        do {
            sr = wtaxLines.getResults(index, index + MAX_LINES);
            srLength = sr.length;
            
            for (var i = 0; i < srLength; i++) {
                transID = sr[i].getId();
                taxBase = sr[i].getValue(columns[3]); //custcol_4601_witaxbaseamount
                taxAmount = (sr[i].getValue(columns[2]) || 0); //custcol_4601_witaxamount
                
                if (totalAmountMap[transID] == null) {
                    totalAmountMap[transID] = {};
                    totalAmountMap[transID].totalTaxBase = 0.0;
                    totalAmountMap[transID].totalTaxAmount = 0.0;
                }
                
                totalAmountMap[transID].totalTaxBase += parseFloat(taxBase);
                totalAmountMap[transID].totalTaxAmount += parseFloat(taxAmount);
                index++;
            }
        } while (srLength >= MAX_LINES);
    } catch(e) {
        nlapiLogExecution('ERROR', '_4601.populateTotalAmountMap > index: ' + index, e.toString());
        throw e;
    }
    
    return totalAmountMap;
};

_4601.getTaxAgencyVendorCategory = function getTaxAgencyVendorCategory() {
    var searchColumns = [];
    var searchFilters = [];
    
    searchColumns.push(new nlobjSearchColumn('name'));
    searchFilters.push(new nlobjSearchFilter('istaxagency', null, 'is', 'T'));
    
    var vendCatRS = nlapiSearchRecord('vendorcategory', null, searchFilters, searchColumns);
    
    var taxVendCatIds = new Array();

    for (var i in vendCatRS) {
        taxVendCatIds.push(vendCatRS[i].getId());
    } 
    
    return taxVendCatIds;
}

_4601.FormatTIN = function(tin) 
{
    if (tin == null || tin == "")
        return "- None -";

    //Strip non-alphanumeric characters
    var tmpTIN = tin.replace(/[^A-Za-z0-9]/g, "");

    //Pad incomplete TIN with X's at the end
    if (tmpTIN.length < 9)
        for (var j = 9 - tmpTIN.length; j > 0; --j)
        tmpTIN += "X";

    //Pad incomplete TIN branch with 0's at the end
    if (tmpTIN.length < 13)
        for (var k = 13 - tmpTIN.length; k > 0; --k)
        tmpTIN += "0";

    var formattedTIN = "";
    for (var i = 0; i < tmpTIN.length; ++i)
    {
        if (i == 3)  //Insert "-" between the 3rd and 4th character
            formattedTIN += "-";
        else if (i == 6)  //Insert "-" between the 5th and 6th character
            formattedTIN += "-";
        else if (i == 9)  //Insert "(" between the 8th and 9th character
            formattedTIN += "-";

        formattedTIN += tmpTIN.charAt(i);
    }

    return formattedTIN;
}


_4601.getTaxTypesDetails = function getTaxTypesDetails(allowedWithholdingTaxCodeIds) {
    var taxTypesMap = new Array();
    
    var filters = new Array();
    filters.push(new nlobjSearchFilter('internalid', null, 'anyof', allowedWithholdingTaxCodeIds));

    var columns = new Array();
    columns.push(new nlobjSearchColumn('custrecord_4601_wtc_witaxtype', null, 'group'));
    columns.push(new nlobjSearchColumn('custrecord_4601_wtt_name', 'custrecord_4601_wtc_witaxtype', 'group'));
    columns.push(new nlobjSearchColumn('custrecord_4601_wtt_description', 'custrecord_4601_wtc_witaxtype', 'group'));

    var result = nlapiSearchRecord('customrecord_4601_witaxcode', null, filters, columns);

    var taxTypesMap = new Array();

    for (var i in result) {
        var _taxType = result[i].getValue('custrecord_4601_wtc_witaxtype', null, 'group');
        var _name = result[i].getValue('custrecord_4601_wtt_name', 'custrecord_4601_wtc_witaxtype', 'group');
        var _desc = result[i].getValue('custrecord_4601_wtt_description', 'custrecord_4601_wtc_witaxtype', 'group');

        taxTypesMap[_taxType] = {};
        taxTypesMap[_taxType].name = _name;
        taxTypesMap[_taxType].desc = _desc;
    }
    
    return taxTypesMap;
};


_4601.getPurchaseTaxPoint = function getPurchaseTaxPoint(subId, nexusId) {
    var purcTaxPoint = '';
    var countryCode = subId ? SFC.System.getSubsidiaryCountry(subId) : nexusId;
    
    var filtersNexus = new Array();
    filtersNexus.push(new nlobjSearchFilter('country', null, 'is', countryCode));
    
    var columnsNexus = new Array();
    columnsNexus.push(new nlobjSearchColumn('internalid'));
    
    var result = nlapiSearchRecord('nexus', null, filtersNexus, columnsNexus);
    
    for (var i in result) {
        var nexusId = result[i].getId();
        var setupFilters = new Array();
        setupFilters.push(new nlobjSearchFilter('custrecord_4601_wts_nexus', null, 'equalto', nexusId));

        var setupColumns = new Array();
        setupColumns.push(new nlobjSearchColumn('custrecord_4601_wts_nexus'));
        setupColumns.push(new nlobjSearchColumn('custrecord_4601_wts_purctaxpoint'));

        var setup = nlapiSearchRecord('customrecord_4601_witaxsetup', null, setupFilters, setupColumns);
        for (var j in setup)
            purcTaxPoint = setup[j].getValue('custrecord_4601_wts_purctaxpoint');
    }
    
    return purcTaxPoint;
};


_4601.getSaleTaxPoint = function getSaleTaxPoint(subId, nexusId) {
    var saleTaxPoint = '';
    var countryCode = subId ? SFC.System.getSubsidiaryCountry(subId) : nexusId;
    
    var filtersNexus = new Array();
    filtersNexus.push(new nlobjSearchFilter('country', null, 'is', countryCode));
    
    var columnsNexus = new Array();
    columnsNexus.push(new nlobjSearchColumn('internalid'));
    
    var result = nlapiSearchRecord('nexus', null, filtersNexus, columnsNexus);
    
    for (var i in result) {
        var nexusId = result[i].getId();
        var setupFilters = new Array();
        setupFilters.push(new nlobjSearchFilter('custrecord_4601_wts_nexus', null, 'equalto', nexusId));

        var setupColumns = new Array();
        setupColumns.push(new nlobjSearchColumn('custrecord_4601_wts_nexus'));
        setupColumns.push(new nlobjSearchColumn('custrecord_4601_wts_saletaxpoint'));

        var setup = nlapiSearchRecord('customrecord_4601_witaxsetup', null, setupFilters, setupColumns);
        for (var j in setup)
            saleTaxPoint = setup[j].getValue('custrecord_4601_wts_saletaxpoint');
    }
    
    return saleTaxPoint;
};

//==============================================================================
/**
 * relatedParam parameters
 *  - fieldName, label, defaultId, layoutType, subId
 */
_4601.TaxTypeCombobox = function TaxTypeCombobox(form, relatedParam)
{
    var cbo = form.addField(relatedParam.fieldName, "select", relatedParam.label);
    if(relatedParam.layoutType != null)
        cbo.setLayoutType(relatedParam.layoutType);

    var taxTypes = null;
    if (relatedParam.subId != null && relatedParam.subId != '') 
        taxTypes = new SFC.System.TaxType().FilterBySubsidiary(relatedParam.subId);
    else if (relatedParam.nexusId != null && relatedParam.nexusId != '') 
        taxTypes = new SFC.System.TaxType().FilterByNexus(relatedParam.nexusId);
    else
        taxTypes = new SFC.System.TaxType().LoadAll();

    for (var i in taxTypes)
    {
        var isSelected = relatedParam.defaultId != null && relatedParam.defaultId == taxTypes[i].GetId();
        cbo.addSelectOption(taxTypes[i].GetId(), taxTypes[i].GetName(), isSelected);
    }
    
    cbo.setMandatory(true);
    
    return cbo;
};


/**
 * whTaxTypeComboboxConfig consists of:
 *  - validSubsidiaryIds, idSelectedDefault, fieldSelectType, fieldName, fieldLabel, fieldLayoutType, fieldBreakType, country
 */
_4601.WHTaxTypeCombobox = function (form, whTaxTypeComboboxConfig) {
    var cbo = form.addField(
        whTaxTypeComboboxConfig.fieldName, 
        whTaxTypeComboboxConfig.fieldSelectType, 
        whTaxTypeComboboxConfig.fieldLabel);
    
    var layoutType = whTaxTypeComboboxConfig.fieldLayoutType != null ? whTaxTypeComboboxConfig.fieldLayoutType  : 'normal';
    var breakType = whTaxTypeComboboxConfig.fieldBreakType != null ? whTaxTypeComboboxConfig.fieldBreakType : 'none';
    
    cbo.setLayoutType(layoutType, breakType);
    
    var taxTypes = null;
    if (whTaxTypeComboboxConfig.subId != null && whTaxTypeComboboxConfig.subId != '')
        taxTypes = new SFC.System.TaxType().FilterBySubsidiary(whTaxTypeComboboxConfig.subId);
    else if (whTaxTypeComboboxConfig.nexusId != null && whTaxTypeComboboxConfig.nexusId != '')
        taxTypes = new SFC.System.TaxType().FilterByNexus(whTaxTypeComboboxConfig.nexusId);
//    else 
//      taxTypes = new SFC.System.TaxType().LoadAll();

    for (var i in taxTypes)
    {
        var isSelected = whTaxTypeComboboxConfig.idSelectedDefault != null && whTaxTypeComboboxConfig.idSelectedDefault == taxTypes[i].GetId();
        cbo.addSelectOption(taxTypes[i].GetId(), taxTypes[i].GetName(), isSelected);
    }
    
    return cbo;
};


_4601.getTaxCodesByTaxGroup = function getTaxCodesByTaxGroup(arrTaxCodes, allValidGroupedWtaxCodes) {
    var arrGroupTaxCodes = new Array();
    
    for (i in arrTaxCodes) {
        var groupWtaxCodeID = arrTaxCodes[i];
        if (allValidGroupedWtaxCodes[groupWtaxCodeID] != null)
            arrGroupTaxCodes.push(allValidGroupedWtaxCodes[groupWtaxCodeID].details);
    }
    
    return arrGroupTaxCodes;
};


_4601.getAllValidGroupedTaxCodes = function getAllValidGroupedTaxCodes(allowedWithholdingTaxCodeIds) {
    var arrGroupTaxCodes = new Array();

    var filters = new Array();
    var columns = new Array();

    filters.push(new nlobjSearchFilter('custrecord_4601_gwtc_code', null, 'anyof', allowedWithholdingTaxCodeIds));

    columns.push(new nlobjSearchColumn('custrecord_4601_gwtc_code'));
    columns.push(new nlobjSearchColumn('custrecord_4601_gwtc_basis'));
    columns.push(new nlobjSearchColumn('custrecord_4601_wtc_name', 'custrecord_4601_gwtc_code'));
    columns.push(new nlobjSearchColumn('custrecord_4601_wtc_description', 'custrecord_4601_gwtc_code'));
    columns.push(new nlobjSearchColumn('custrecord_4601_wtc_rate', 'custrecord_4601_gwtc_code'));
    columns.push(new nlobjSearchColumn('custrecord_4601_wtc_witaxtype', 'custrecord_4601_gwtc_code'));

    var rsGroupTaxCodes = nlapiSearchRecord('customrecord_4601_groupedwitaxcode', null, filters, columns);

    if (rsGroupTaxCodes != null) {
        for (i in rsGroupTaxCodes) {
            var groupID = rsGroupTaxCodes[i].getId();
            if (arrGroupTaxCodes[groupID] == null) {
                arrGroupTaxCodes[groupID] = {};
                arrGroupTaxCodes[groupID].details = rsGroupTaxCodes[i];
            }
        }
    }
    
    return arrGroupTaxCodes;
};


_4601.getLineAmountDistribution = function getLineAmountDistribution(taxBase, taxAmount, taxWithheld, totalTaxAmount) {
    var lineCreditWTaxPayment = 0.0;
    var wtaxPaymentRatio = 0;
    
    wtaxPaymentRatio = taxAmount / totalTaxAmount;
    lineCreditWTaxPayment = taxWithheld * wtaxPaymentRatio;
    
    return lineCreditWTaxPayment;
};


_4601.getTaxGroupWithValidTaxCodes = function getTaxGroupWithValidTaxCodes(allowedWithholdingTaxCodeIds) {
    var validTaxGroups = new Array();
    
    var results = new Array();
    var filters = new Array();
    var columns = new Array();
    
    columns.push(new nlobjSearchColumn('custrecord_4601_wtc_groupedwitaxcodes'));        
    
    filters.push(new nlobjSearchFilter('custrecord_4601_wtc_istaxgroup', null, 'is', 'T'));
    filters.push(new nlobjSearchFilter('custrecord_4601_gwtc_code', 'custrecord_4601_wtc_groupedwitaxcodes', 'anyof', allowedWithholdingTaxCodeIds));

    results = nlapiSearchRecord('customrecord_4601_witaxcode', null, filters, columns);
    
    if (results != null) {
        for (var i in results) {
            
            var taxGroupID = results[i].getId();
            validTaxGroups.push(taxGroupID);
        }
    }
    
    return validTaxGroups;
};


_4601.HelperUtils = function (sfcAppInstance, request) {
    /**
     * Retrieves an nlobjFile record corresponding to the
     * given file name
     * 
     * Has an dependency on an instance of SFC.Application
     * and the current request.
     * 
     * return {nlobjFile}
     */
    function getFileByFilename(filename) {
        var fileId = sfcAppInstance.GetFileId(filename);
        var fileRecord;
        
        if (fileId) {
            fileRecord = nlapiLoadFile(fileId);
        } else {
            nlapiLogExecution(
                'ERROR',
                'File not found:', 
                'filename');
            throw {
                name: 'form 1601: missing report logo',
                message: 'filename'};
        }
        return fileRecord;
    }
    
    /**
     * Uses the request URL to derive the base URL for Suitelets
     * 
     * e.g. given the request URL: https://system.netsuite.com/app/...
     * this returns https://system.netsuite.com
     * 
     * if run in the debugger url, https://debugger.netsuite.com/app/..
     * this returns https://debugger.netsuite.com
     */
    function getBaseUrl() {
        return request.getURL().split('/app')[0];
    }
    
    function getImageFullUrl(filename) {
        var fullUrl;
        try {
            fullUrl = getBaseUrl() + getFileByFilename(filename).getURL();
        } catch (e) {
            var arrExceptionMsg = [];
            arrExceptionMsg.push('filename:');
            arrExceptionMsg.push(filename);
            arrExceptionMsg.push('\n');
            if (e instanceof nlobjError) {
                arrExceptionMsg.push(getCode());
                arrExceptionMsg.push(':\n');
                arrExceptionMsg.push(e.getDetails());
            }
            else {
                arrExceptionMsg.push(e.name);
                arrExceptionMsg.push(':\n');
                arrExceptionMsg.push(e.message);
            }
            nlapiLogExecution(
                'DEBUG', 'error:getImageFullUrl', arrExceptionMsg.join(''));
        }
        return fullUrl;
    }
    this.getImageFullUrl = getImageFullUrl;
    
    /**
     * Obtains a request parameter that is expected to be numeric.
     * 
     * If the request parameter does not exist or is not numeric,
     * the defaultValue provided is returned
     * @param {String} reqParamName
     * @param {Number} defaultValue
     */
    function getNumericRequestParameter(reqParamName, defaultValue) {
        if (!isNaN(parseFloat(request.getParameter(reqParamName)))) {
            return request.getParameter(reqParamName);
        } else {
            return defaultValue;
        }
    }
    this.getNumericRequestParameter = getNumericRequestParameter;
    
    
    function getFullFileUrl(filename) {
        var fileUrl;
        try {
            fileUrl = getBaseUrl() + getFileByFilename(filename).getURL();
            
        } catch (ex) {
            var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
            nlapiLogExecution("ERROR", "getURLResource", errorMsg);
        }
        return fileUrl;
    }
    this.getFullFileUrl = getFullFileUrl;

};

//==============================================================================
SFC.System.Application = function(appGuid) {
    this.Context = nlapiGetContext();
    this.Params = {};
    var _AppGuid = appGuid;
    var _AppFolderId = null;

    //--------------------------------------------------------------------------
    this.CreateField = function(form, name, type, label, isMandatory, defaultValue, displayType, layoutType, breakType, tabname) {
        var field; 
        
        if (tabname) {
            field = form.addField(name, type, label, null, tabname);
        } else {
            field = form.addField(name, type, label, null);
        }

        if (isMandatory)
            field.setMandatory(isMandatory);

        if (defaultValue != null)
            field.setDefaultValue(defaultValue);

        if (displayType != null)
            field.setDisplayType(displayType);

        if (layoutType != null) {
            if (breakType != null)
                field.setLayoutType(layoutType, breakType);
            else
                field.setLayoutType(layoutType);
        }

        return field;
    }
    
    //--------------------------------------------------------------------------
    function GetAppFolderId(fileName) {
        var _AppFolderId = null;
        
        if (_AppFolderId == null)  //Lazy load folder id
        {
            //Find folder id using GUID file
            var filters = [new nlobjSearchFilter("name", null, "is", fileName)];
            var rs = nlapiSearchRecord("file", null, filters, [new nlobjSearchColumn("folder")]);
            _AppFolderId = rs[0].getValue("folder");
        }

        return _AppFolderId;
    }

    //--------------------------------------------------------------------------
    this.GetFileId = GetFileId;
    function GetFileId(fileName, isErrorWhenNotFound) {
        //Find file internalid using filename and folder id
        /* filters = [
            new nlobjSearchFilter("name", null, "is", fileName),
            new nlobjSearchFilter("folder", null, "is", GetAppFolderId())]; */
        
        var filters = new Array();
        filters.push( new nlobjSearchFilter("name", null, "is", fileName) );
        filters.push( new nlobjSearchFilter("folder", null, "is", GetAppFolderId(fileName)) );

        var rs = nlapiSearchRecord("file", null, filters);

        if (rs == null) {
            if (isErrorWhenNotFound)
                throw nlapiCreateError("GST101A UI", "File not found (" + fileName + ")");
            else
                return null;
        }

        return rs[0].getId();
    }



    //--------------------------------------------------------------------------
    this.GetFileContent = GetFileContent;
    function GetFileContent(fileName, isErrorWhenNotFound) {
        var fileId = GetFileId(fileName, isErrorWhenNotFound);

        var file = nlapiLoadFile(fileId);

        return file.getValue();
    }



    //--------------------------------------------------------------------------
    this.LoadCurrentUser = LoadCurrentUser;
    function LoadCurrentUser() {
        var user = {};
        user.Id = this.Context.getUser();

        var userRec = nlapiLoadRecord("employee", user.Id);

        user.Name = userRec == null ? "" : userRec.getFieldValue("entityid");

        return user;
    }



    //--------------------------------------------------------------------------
    function InitParams(allParams) {
        var params = {};

        for (var i in allParams)
            params[i] = allParams[i];

        return params;
    }



    //--------------------------------------------------------------------------
    this.Buttonise = function(id, label, onClick, isEnabled ) {
        
        // get the current version of netsuite to determine the ui
        // with old ui : 2010.1.1
        // with new ui : 2010.2
        
        var context_obj = nlapiGetContext();
        var ns_ver = context_obj.version; 
        
        
        var use_2010_1_1_ui = true;
        
        
        if( ns_ver && ns_ver!= '' ){
            var ver_parts = ns_ver.split('.');
            
            if( ver_parts.length > 0 ){
                if( ver_parts[0]== 2010 && ver_parts[1] == 2 ){
                    use_2010_1_1_ui = false;
                }
            }
        }

        var btn_string = '';

        if( use_2010_1_1_ui ){
            // use old vetrsion
            if (isEnabled){
                btn_string = "<table id='tbl_" + id + "' name='tbl_" + id +
                    "' cellpadding=0 cellspacing=0 border=0 style=\"cursor:hand;\"><tr><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_left_cap.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td><TD height=20 valign='bottom' nowrap class='rndbuttonbody' background=\"/images/buttons/upper_body.gif\" style=\"padding-top:2\"><input type='button' id='" + id + "' name='" + id + "' class='rndbuttoninpt' onclick='" + onClick +
                    "' value='" + label + "' onkeypress='event.cancelBubble=true;' onclick='return(clearOnSubmit())' onmousedown=\"this.setAttribute('_mousedown','T'); setButtonDown(true, false, this);\" onmouseup=\"this.setAttribute('_mousedown','F'); setButtonDown(false, false, this);\" onmouseout=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(false, false, this);\" onmouseover=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(true, false, this);\" /></TD><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_right_cap.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td><td>&nbsp;&nbsp;</td></tr></table>"; 
            }
            else{
                btn_string = "<table id='tbl_" + id +
                "' cellpadding=0 cellspacing=0 border=0 ><tr><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_left_capdis.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td><TD height=20 valign='bottom' nowrap class='rndbuttonbody' background=\"/images/buttons/upper_bodydis.gif\" style=\"padding-top:2\"> <INPUT type='submit' id='" + id + "' name='" + id + "' style='vertical-align: middle;' class='rndbuttoninptdis' value='" + label +
                "' disabled onkeypress=\"event.cancelBubble=true;\" onclick=\"return(clearOnSubmit())\" onmousedown=\"this.setAttribute('_mousedown','T'); setButtonDown(true, false, this);\" onmouseup=\"this.setAttribute('_mousedown','F'); setButtonDown(false, false, this);\" onmouseout=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(false, false, this);\" onmouseover=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(true, false, this);\"></TD><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_right_capdis.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td></tr></table>";
            }
        }
        else{
            // use latest ui
            
            btn_string = '<table cellspacing="0" cellpadding="0" border="0" style="margin-right: 6px;" id="tbl_'+ id +'">\
            <tbody>\
                <tr class="pgBntY" id="tr_edit">\
                <td id="tdleftcap_edit">\
                    <img height="50%" width="3" border="0" class="bntLT" src="/images/x.gif">\
                    <img height="50%" width="3" border="0" class="bntLB" src="/images/x.gif">\
                </td>\
                <td nowrap="" height="20" valign="top" class="bntBgB" id="tdbody_edit">\
                    <input type="button" onmouseover="" onmouseout="" onmouseup="" onmousedown="" onclick="'+onClick+'" name="edit" id="'+id+'" value="'+label+'" class="rndbuttoninpt bntBgT" style="" _mousedown="F">\
                </td>\
                <td id="tdrightcap_edit">\
                    <img height="50%" width="3" border="0" class="bntRT" src="/images/x.gif"><img height="50%" width="3" border="0" class="bntRB" src="/images/x.gif">\
                </td>\
                </tr>\
            </tbody>\
            </table>';
            
        }       
         
        
        
        /*
        if (isEnabled)
            return "<table id='tbl_" + id + "' name='tbl_" + id +
                    "' cellpadding=0 cellspacing=0 border=0 style=\"cursor:hand;\"><tr><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_left_cap.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td><TD height=20 valign='bottom' nowrap class='rndbuttonbody' background=\"/images/buttons/upper_body.gif\" style=\"padding-top:2\"><input type='button' id='" + id + "' name='" + id + "' class='rndbuttoninpt' onclick='" + onClick +
                    "' value='" + label + "' onkeypress='event.cancelBubble=true;' onclick='return(clearOnSubmit())' onmousedown=\"this.setAttribute('_mousedown','T'); setButtonDown(true, false, this);\" onmouseup=\"this.setAttribute('_mousedown','F'); setButtonDown(false, false, this);\" onmouseout=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(false, false, this);\" onmouseover=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(true, false, this);\" /></TD><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_right_cap.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td><td>&nbsp;&nbsp;</td></tr></table>";

        return "<table id='tbl_" + id +
                "' cellpadding=0 cellspacing=0 border=0 ><tr><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_left_capdis.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td><TD height=20 valign='bottom' nowrap class='rndbuttonbody' background=\"/images/buttons/upper_bodydis.gif\" style=\"padding-top:2\"> <INPUT type='submit' id='" + id + "' name='" + id + "' style='vertical-align: middle;' class='rndbuttoninptdis' value='" + label +
                "' disabled onkeypress=\"event.cancelBubble=true;\" onclick=\"return(clearOnSubmit())\" onmousedown=\"this.setAttribute('_mousedown','T'); setButtonDown(true, false, this);\" onmouseup=\"this.setAttribute('_mousedown','F'); setButtonDown(false, false, this);\" onmouseout=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(false, false, this);\" onmouseover=\"if(this.getAttribute('_mousedown')=='T') setButtonDown(true, false, this);\"></TD><td nowrap class='rndbuttoncaps' background=\"/images/buttons/upper_right_capdis.gif\"><img src='/images/nav/stretch.gif' border=0 width=4></td></tr></table>";*/
                
                
        return btn_string;
    }


    //--------------------------------------------------------------------------
    this.RenderTemplate = function(template, ds) {
        var content = template;

        for (var i in ds) {
            var re = new RegExp("\\{" + i + "\\}", "g");
            content = content.replace(re, ds[i]);
        }

        return content;
    }
    
    this.RenderTemplateToArray = function(template, objArray) {
        var strBuffer = new Array();
        if (objArray && objArray.length > 0) {
            for(var i=0; i<objArray.length; i++) {
                var translation = _App.RenderTemplate(template, objArray[i]);
                strBuffer.push(translation);
            }
        }
        
        return strBuffer.join(''); 
    }
    
    //Break String to an array character
    this.stringToCharArray = function(value) {
        if (value) {
            return value.split('');
        } else {
            return new Array();
        }
    }
    
    /*  This creates appends specified attribute with corresponding sequence
        Example: 
            attribute name = tin
            array contents = [1,2,3,4,5,6,7,8,9]
            will append the following attributes 
                container.tin_0 = 1
                container.tin_1 = 2
                ....
                
        if expected array size is specified:
            if (expected size > array.size) = will append sequence attribute with empty string up to variance
            if (expected size <= array.size) = will append all contents of array in sequence attribute
        
        if objArray is null or empty 
            if expected size is unspecified = append attribute with empty string value.
            if expected size is specified = append attribute sequence up to expected size with empty value.
        
        if container or attributeName is empty = return from function
        
        Parameters
        container - any object
        attributeName - name of attribute to be appended to contaner
        objArray - any array
        iExpectedArraySize - size of the array appended.  can be null.
    */
    this.appendAttributeArrayToContainer = function (container, attributeName, objArray, iExpectedArraySize) {
        if (!container || !attributeName) {
            return;
        }
    
        if (objArray && objArray.length > 0 ) {
            for(var i = 0; i < objArray.length; i++) {
                eval("container." + attributeName + "_" + i + "='" + objArray[i] +"'"); 
            }

            //Append empty string to array indices without values
            if (iExpectedArraySize && objArray.length < iExpectedArraySize) {
                for(var i = objArray.length; i < iExpectedArraySize; i++) {
                    eval( "container." + attributeName + "_" + i + "=''");  
                }
            }
        } else if (iExpectedArraySize) {
            //no array but with expected array size
            for(var i = 0; i < iExpectedArraySize; i++) {
                    eval( "container." + attributeName + "_" + i + "=''");  
            }
        } else {
            //append empty value to container.attributeName
            eval( "container." + attributeName + "=''");
        }
    }

    /*
     * Retrieve report internal id based on name
     */
    this.getReportIdByName = function getReportIdByName( reportName ){
        var reportId = 0;
        if( reportName!=null ){
            var rs = nlapiSearchGlobal( reportName  );
         
            if( rs.length > 0 ){
                reportId = rs[0].getId().replace(/REPO_/, "");  //remove "REPO_" prefix
            }
        }
        return parseInt(reportId);
    }
     
    this.getSavedSearchIdByName = function getSavedSearchIdByName( savedSearchName ){
        var saveSearchId = 0;
        if( savedSearchName!=null ){
            var rs = nlapiSearchGlobal( savedSearchName );
         
            if( rs.length > 0 ){
                saveSearchId = rs[0].getId();
            }
        }
        return parseInt(saveSearchId);
    }

    this.getJournalTypeIds = function ( arrJournalNames ) {
        var name = new nlobjSearchColumn('name');
        var arrResult = nlapiSearchRecord('customrecord_ph4014_wtax_jrnl_type', null, null, name);

        var journalMap = new Array(); //journalMap[journal name] = journal id;
        for(var i in arrResult) {
            journalMap[arrResult[i].getValue(name)] = arrResult[i].getId();
        }
        
        var journalIds = new Array();
        for(var j in arrJournalNames) {
            journalIds.push(journalMap[arrJournalNames[j]]);
        }
            
        return journalIds;
    }
    
    this.formatMonthYearToString = function (objDate) {
        var strValue = "";
        if(objDate) {
            objDate.getFullYear();
            var intMonth = parseInt(objDate.getMonth()) + parseInt(1);
            if (intMonth < 10) {
                strValue = "0" + intMonth;
            } else {
                strValue = intMonth;
            }
            strValue = strValue + "/" + objDate.getFullYear();
        } 
        return strValue;
    }
}



//==============================================================================
SFC.System.Subsidiary = function(id, shouldThrowError)
{
    if (nlapiGetContext().getSetting("FEATURE", "SUBSIDIARIES") != "T")
    {
        if (shouldThrowError)
            throw nlapiCreateError("SFC.System.Subsidiary", id + " is not a valid subsidiary id.");
        else
            return null;
    }

    var filters = [new nlobjSearchFilter("internalid", null, "is", parseInt(id))];
    var columns = [new nlobjSearchColumn("name"),
                   new nlobjSearchColumn("country"),
                   new nlobjSearchColumn("taxidnum")];

    var rs = nlapiSearchRecord("subsidiary", null, filters, columns);
    if (rs == null)
    {
        if (shouldThrowError)
            throw nlapiCreateError("SFC.System.Subsidiary", id + " is not found.");
        else
            return null;
    }

    var sub = {};

    sub.Id = id;
    sub.Name = rs[0].getValue("name");
    sub.CountryCode = rs[0].getValue("country");
    sub.VRN = rs[0].getValue("taxidnum");

    return sub;
}


SFC.System.TaxCodes = function() {
    var objTaxCodes = new Array();
    this.getTaxCodes = function(){
        if( objTaxCodes.length == 0 ) {
            objTaxCodes = loadAllTaxCodes();
        }
        return objTaxCodes;
    }
    
    this.getTaxInfoByName = function(paramTaxCode) {
        var objTaxCode = new TaxCode("", "", "", "");
        if( objTaxCodes.length == 0 ) {
            objTaxCodes = loadAllTaxCodes();
        }
        
        for(var i in objTaxCodes) {
            if (objTaxCodes[i].name == paramTaxCode) {
                objTaxCode = objTaxCodes[i];
                break;
            }
        }
        return objTaxCode;
    }
    
    this.getTaxInfoById = function(paramTaxId) {
        var objTaxCode = new TaxCode("", "", "", "");
        if( objTaxCodes.length == 0 ) {
            objTaxCodes = loadAllTaxCodes();
        }
        
        for(var i in objTaxCodes) {
            if (objTaxCodes[i].id == paramTaxId) {
                objTaxCode = objTaxCodes[i];
                break;
            }
        }
        
        return objTaxCode;
    }
    
    function loadAllTaxCodes(){
        var arrTaxCodes =  new Array();
        
        //Column Definitions
        var objColName = new nlobjSearchColumn('custrecord_4601_wtc_name');
        var objColDesc = new nlobjSearchColumn('custrecord_4601_wtc_description');
        var objColRate = new nlobjSearchColumn('custrecord_4601_wtc_rate');
        
        //Aggregrated Search Columns
        var objSearchColumns = new Array();
            objSearchColumns.push(objColName);
            objSearchColumns.push(objColDesc);
            objSearchColumns.push(objColRate);
            
        var objResult = nlapiSearchRecord( 'customrecord_4601_witaxcode', null, null, objSearchColumns );
        
        if( objResult && objResult.length  > 0 ) {
            for( var i in objResult ){
                 arrTaxCodes.push(new TaxCode(objResult[i].getId(), objResult[i].getValue(objColName),
                        objResult[i].getValue(objColDesc), objResult[i].getValue(objColRate)
                        ));
            }
        }
        return arrTaxCodes;
    }
    
    TaxCode = function(id, name, desc, rate) {
        return { "id": id, "name": name, "desc": desc, "rate":rate };
    }
    
}


//==============================================================================
SFC.System._TaxPeriods = null;
SFC.System._TaxPeriodTree = null;
SFC.System.TaxPeriod = function(id, shouldThrowError)
{
    var _ShouldThrowError = shouldThrowError;

    var _FiscalCaledar = null; //only for quarter and year
    this.GetFiscalCalendar = function() { return _FiscalCaledar; }
    this.SetFiscalCalendar = function(value) { _FiscalCaledar = value; }
    
    var _Id = null;
    this.GetId = function() { return _Id; }
    this.SetId = function(value) { _Id = value; }

    var _Name = null;
    this.GetName = function() { return _Name; }
    this.SetName = function(value) { _Name = value; }

    var _StartDate = null;
    this.GetStartDate = function() { return _StartDate; }
    this.SetStartDate = function(value) { _StartDate = value; }

    var _EndDate = null;
    this.GetEndDate = function() { return _EndDate; }
    this.SetEndDate = function(value) { _EndDate = value; }

    var _Type = null;  //"year", "quarter", "month"
    this.GetType = function() { return _Type; }
    this.SetType = function(value) { _Type = value; }

    var _IsActive = null;
    this.IsActive = function() { return _IsActive; }
    this.SetActive = function(value) { _IsActive = value; }

    var _IsClosed = null;
    this.IsClosed = function() { return _IsClosed; }
    this.SetClosed = function(value) { _IsClosed = value; }

    var _ParentId = null;
    this.GetParentId = function() { return _ParentId; }
    this.SetParentId = function(value) { _ParentId = value; }

    var _Children = null;
    this.GetChildren = function() {
        if (_Children == null) {
            _Children = [];
            var periods = LoadAll();
            for (var i in periods) {
                if (periods[i].GetParentId() == _Id)
                    _Children.push(periods[i]);
            }
        }

        return _Children;
    }



    if (id != null)
    {
        Load(id);
    }

    this.Load = Load;
    function Load(id, fiscalCalendarId) {
        if (id == null || isNaN(parseInt(id))) {
            if (_ShouldThrowError)
                throw nlapiCreateError("SFC.System.TaxPeriod", id + " is not a valid tax period id.");
            else
                return null;
        }

        try {
            var period = new SFC.System.TaxPeriod();
            period.SetId(id);
            
            if (_4601.isMultipleCalendar) {
                var taxPeriod = nlapiLoadRecord('taxperiod', id);

                period.SetName(taxPeriod.getFieldValue('periodname'));
                period.SetStartDate(nlapiStringToDate(taxPeriod.getFieldValue("startdate")));
                period.SetEndDate(nlapiStringToDate(taxPeriod.getFieldValue("enddate")));
                period.SetActive(taxPeriod.getFieldValue("isinactive") != "T");
                period.SetClosed(taxPeriod.getFieldValue("allclosed") == "T");
                period.SetType(taxPeriod.getFieldValue("isadjust") == "T" ? "adjustment" :
                    taxPeriod.getFieldValue("isyear") == "T" ? "year" :
                    taxPeriod.getFieldValue("isquarter") == "T" ? "quarter" : "month");

                var sublist = 'fiscalcalendars';
                var lineCount = taxPeriod.getLineItemCount(sublist);
                for (var i = 1; i <= lineCount; i++) {
                    if (taxPeriod.getLineItemValue(sublist, 'fiscalcalendar', i) == fiscalCalendarId ) {
                        var p = taxPeriod.getLineItemValue(sublist, 'parent', i);
                        period.SetParentId((p == null || isNaN(parseInt(p))) ? null : parseInt(p));
                        if ((period.GetType() == "year" || period.GetType() == "quarter") && SFC.IsOW)
                            period.SetFiscalCaledar(parseInt(fiscalCalendarId));
                        break;
                    }
                }
                
            } else {
                var columns = [ new nlobjSearchColumn("internalid"),
                                new nlobjSearchColumn("periodname"),
                                new nlobjSearchColumn("startdate"),
                                new nlobjSearchColumn("enddate"),
                                new nlobjSearchColumn("isinactive"),
                                new nlobjSearchColumn("isyear"),
                                new nlobjSearchColumn("isquarter"),
                                new nlobjSearchColumn("isadjust"),
                                new nlobjSearchColumn("parent"),
                                new nlobjSearchColumn("allclosed")];
                var filters = [new nlobjSearchFilter('internalid', null, "is", id)];
                var rs = nlapiSearchRecord("taxperiod", null, filters, columns);
                
                if (rs != null)
                    period = CreateInstanceFromSearchRow(rs[0]);
            }
            
            return period;
        } catch (e) {
            if (_ShouldThrowError)
                throw nlapiCreateError("SFC.System.TaxPeriod", "Tax period id(" + id + ") not found.");
            else
                return null
        }
    }

    this.getPeriodById = function (periodId) {
        var period = new SFC.System.TaxPeriod();
        if (!SFC.System._TaxPeriods) {
            LoadAll();
        } 
        
        for( var i in SFC.System._TaxPeriods) {
            if (SFC.System._TaxPeriods[i].GetId() ==  periodId) {
                period = SFC.System._TaxPeriods[i];
                break;
            }
        }
        
        return period;
    }

    //--------------------------------------------------------------------------
    this.LoadAll = LoadAll;
    function LoadAll(subid) {
        if (SFC.System._TaxPeriods == null) {
            var columns = [ new nlobjSearchColumn("internalid"),
                new nlobjSearchColumn("periodname"),
                new nlobjSearchColumn("startdate"),
                new nlobjSearchColumn("enddate"),
                new nlobjSearchColumn("isinactive"),
                new nlobjSearchColumn("isyear"),
                new nlobjSearchColumn("isquarter"),
                new nlobjSearchColumn("isadjust"),
                new nlobjSearchColumn("parent"),
                new nlobjSearchColumn("allclosed")];
            columns[2].setSort();  //sort by startdate
            columns[5].setSort(true);  //sort by enddate
            columns[6].setSort(true);  //sort by enddate
            
            if (_4601.isMultipleCalendar) {
                columns.push(new nlobjSearchColumn('fiscalcalendar'));
            }
            
            var filters = [];
            SFC.System._TaxPeriods = [];

            var rs = nlapiSearchRecord("taxperiod", null, filters, columns);
            if (rs != null) {
                for (var i in rs) {
                    SFC.System._TaxPeriods.push(CreateInstanceFromSearchRow(rs[i]));
                }
            }
        }

        return SFC.System._TaxPeriods;
    }

    this.GetCurrentPeriod = GetCurrentPeriod;
    function GetCurrentPeriod() {
        var today = new Date();
        var periods = LoadAll();
        var firstPeriod = periods.length > 0 ? periods[0] : null;
        var current = { Month: null, Quarter: null}; //, Year: null 

        for (var i = periods.length - 1; i > 0; --i) {
            if (periods[i].GetStartDate() <= today && periods[i].GetEndDate() >= today) {
                if (periods[i].GetType() == "month")
                    current.Month = periods[i];
                else if (periods[i].GetType() == "quarter")
                    current.Quarter = periods[i];
//                else if (periods[i].GetType() == "year")
//                    current.Year = periods[i];
                break;
            }
        }

        return current.Month != null ? current.Month :
               current.Quarter != null ? current.Quarter :
//               current.Year != null ? current.Year :
                firstPeriod;
    }

    function CreateInstanceFromSearchRow(row) {
        if (row == null)
            return null;

        var period = new SFC.System.TaxPeriod();

        period.SetId(row.getId());
        period.SetName(row.getValue("periodname"));
        period.SetStartDate(nlapiStringToDate(row.getValue("startdate")));
        period.SetEndDate(nlapiStringToDate(row.getValue("enddate")));
        period.SetActive(row.getValue("isinactive") != "T");
        period.SetClosed(row.getValue("allclosed") == "T");
        period.SetType(row.getValue("isadjust") == "T" ? "adjustment" :
                   row.getValue("isyear") == "T" ? "year" :
                   row.getValue("isquarter") == "T" ? "quarter" : "month");

        var p = row.getValue("parent");
        period.SetParentId((p == null || isNaN(parseInt(p))) ? null : parseInt(p));

        if(_4601.isMultipleCalendar){
	        if ((period.GetType() == "year" || period.GetType() == "quarter") && SFC.IsOW) {
	            period.SetFiscalCalendar(row.getValue('fiscalcalendar'));
	        }
        }else{
        	period.SetFiscalCalendar(1);
        }
        
        return period;
    }
};

//==============================================================================
SFC.System.TaxPeriodCombobox = function(form, fieldName, label, defaultId, layoutType)
{
    var cbo = form.addField(fieldName, "select", label);
    if(layoutType != null)
        cbo.setLayoutType(layoutType);

    var taxPeriods = new SFC.System.TaxPeriod().LoadAll();
    var visiblePeriods = [];
    var today = new Date();

    for (var i in taxPeriods)
    {
        if (taxPeriods[i].GetType() == "year")
        {
            if (taxPeriods[i].GetStartDate() > today)
                break;

            visiblePeriods.push(taxPeriods[i]);

            var quarters = taxPeriods[i].GetChildren();
            for (var j in quarters)
            {
                if (quarters[j].GetStartDate() > today)
                    break;

                visiblePeriods.push(quarters[j]);

                var months = quarters[j].GetChildren();
                for (var k in months)
                {
                    if (months[k].GetStartDate() > today)
                        break;

                    visiblePeriods.push(months[k]);
                }
            }
        }
    }

    for (var i in visiblePeriods)
    {
        var periodType = visiblePeriods[i].GetType();
//        var spaces = (periodType == "month") ? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" : (periodType == "quarter") ? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" : "";
        var spaces = (periodType == "month") ? "&nbsp;&nbsp;&nbsp;" : "";
        var isSelected = defaultId != null && defaultId == visiblePeriods[i].GetId();
        cbo.addSelectOption(visiblePeriods[i].GetId(), spaces + visiblePeriods[i].GetName());

        if (isSelected)
            cbo.setDefaultValue(defaultId);
    }
    return cbo;
};


SFC.System.TaxPeriodMonthCombobox = function(form, fieldName, label, defaultId, layoutType)
{
    var cbo = form.addField(fieldName, "select", label);
    if(layoutType != null)
        cbo.setLayoutType(layoutType);

    var taxPeriods = new SFC.System.TaxPeriod().LoadAll();
    var visiblePeriods = [];
    var today = new Date();

    for (var i in taxPeriods)
    {
        if (taxPeriods[i].GetType() == "year")
        {
            if (taxPeriods[i].GetStartDate() > today)
                break;

            //visiblePeriods.push(taxPeriods[i]);

            var quarters = taxPeriods[i].GetChildren();
            for (var j in quarters)
            {
                if (quarters[j].GetStartDate() > today)
                    break;

                //visiblePeriods.push(quarters[j]);

                var months = quarters[j].GetChildren();
                for (var k in months)
                {
                    if (months[k].GetStartDate() > today)
                        break;

                    visiblePeriods.push(months[k]);
                }
            }
        }
    }

    for (var i in visiblePeriods)
    {
        var periodType = visiblePeriods[i].GetType();
        var spaces = (periodType == "month") ? "" : (periodType == "quarter") ? "&nbsp;&nbsp;&nbsp;" : "";
        var isSelected = defaultId != null && defaultId == visiblePeriods[i].GetId();
        cbo.addSelectOption(visiblePeriods[i].GetId(), spaces + visiblePeriods[i].GetName(), isSelected);
    }
};

_4601.getFiscalCalendar = function getFiscalCalendar(subid) {
    var fs = 1;
    
    if(_4601.isMultipleCalendar && subid && SFC.IsOW){
	    var subrec = nlapiLoadRecord('subsidiary', subid);
	    fs = subrec.getFieldValue('taxfiscalcalendar');
	}
    
    return fs;
};


SFC.System.TaxPeriodMonthQuarterCombobox = function(form, fieldName, label, defaultId, layoutType, subid, fs)
{
    var cbo = form.addField(fieldName, "select", label);
    if (layoutType != null)
        cbo.setLayoutType(layoutType);
    cbo.setDisplaySize(120);
    
    if (!!subid) {
        
        var taxPeriods = new SFC.System.TaxPeriod().LoadAll();
        var visiblePeriods = [];
        var months = null;

        for (var i in taxPeriods) {
            // for the meantime, report generation for fiscal year is not supported
            // to incorporate fiscal years, include this if block for "year"
//          if (taxPeriods[i].GetType() == "year" ) {
//              if (SFC.IsOW) {
//                  if (fs != taxPeriods[i].GetFiscalCalendar()) continue;
//              }
//              visiblePeriods.push(taxPeriods[i]);
//
//              var quarters = taxPeriods[i].GetChildren();
//              for (var j in quarters) {
//                  visiblePeriods.push(quarters[j]);
//
//                  months = quarters[j].GetChildren();
//                  for (var k in months) {
//                      visiblePeriods.push(months[k]);
//                  }
//              }
//          } else 
            if (taxPeriods[i].GetType() == "quarter") { //&& taxPeriods[i].GetParentId() == null
                if (SFC.IsOW) {
                    if (fs != taxPeriods[i].GetFiscalCalendar()) continue;
                }
                
                visiblePeriods.push(taxPeriods[i]);

                months = taxPeriods[i].GetChildren();
                for (var l in months)
                {
                    visiblePeriods.push(months[l]);
                }
            } else if (taxPeriods[i].GetType() == "month" && taxPeriods[i].GetParentId() == null) {
                visiblePeriods.push(taxPeriods[i]);
            }
        }

        for (var i in visiblePeriods) {
            var periodType = visiblePeriods[i].GetType();
//          var spaces = (periodType == "month") ? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" : (periodType == "quarter") ? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" : "";
            var spaces = (periodType == "month") ? "&nbsp;&nbsp;&nbsp;" : "";
            var isSelected = defaultId != null && defaultId == visiblePeriods[i].GetId();
            cbo.addSelectOption(visiblePeriods[i].GetId(), spaces + visiblePeriods[i].GetName(), isSelected);
        }
    } else {
        cbo.addSelectOption('', '', true);
    }
    
    return cbo;
};

//==============================================================================
SFC.System.ReportFormCombobox = function (form, fieldName, fieldLabel, idSelectedDefault, fieldLayoutType, fieldBreakType, relatedParam) {
    
    var cbo = form.addField(
            fieldName, 
            'select', 
            fieldLabel);
    
    if(fieldLayoutType != null && fieldBreakType != null)
        cbo.setLayoutType(fieldLayoutType, fieldBreakType);
    else if (fieldLayoutType != null) 
        cbo.setLayoutType(fieldLayoutType);
    
    if ((relatedParam.country != null) && (relatedParam.country != '')) {
            
        if(relatedParam.country.toLowerCase() == 'ph') {
            cbo.addSelectOption('form1601e', 'Form 1601-E', idSelectedDefault == 'form1601e'?true:false);
            cbo.addSelectOption('form2307', 'Form 2307', idSelectedDefault == 'form2307'?true:false);
            cbo.addSelectOption('map', 'Monthly Alphalist of Payees (MAP)', idSelectedDefault == 'map'?true:false);
            cbo.addSelectOption('sawt', 'Summary Alphalist of Withholding Taxes (SAWT)', idSelectedDefault == 'sawt'?true:false);
        }
    }
    
    return cbo;
};

//==============================================================================
SFC.System._TaxTypes = null;
SFC.System.TaxType = function(id, shouldThrowError) {
    var _ShouldThrowError = shouldThrowError;
    
    var _Id = null;
    this.GetId = function() { return _Id; }
    this.SetId = function(value) { _Id = value; }

    var _Name = null;
    this.GetName = function() { return _Name; }
    this.SetName = function(value) { _Name = value; }
    
    if (id != null) {
        Load(id);
    }


    //--------------------------------------------------------------------------
    function Load(id)
    {
        if (id == null || isNaN(parseInt(id)))
        {
            if (_ShouldThrowError)
                throw nlapiCreateError("SFC.System.TaxType", id + " is not a valid tax type id.");
            else
                return null
        }

        var rec = null; 
        
        try
        {
            rec = nlapiLoadRecord("customrecord_4601_witaxtype", id);
        }
        catch(e)
        {
            if (_ShouldThrowError)
                throw nlapiCreateError("SFC.System.TaxType", "Tax type id(" + id + ") not found.");
            else
                return null
        }

        _Id = parseInt(rec.getId());
        _Name = rec.getFieldValue("custrecord_4601_wtt_name");
    }
    
    //--------------------------------------------------------------------------
    this.LoadAll = LoadAll;
    function LoadAll()
    {
        if (SFC.System._TaxTypes == null)
        {
            var columns = GetTaxTypeColumns();
            //columns[2].setSort();  //sort by startdate
            var filters = null;
            SFC.System._TaxTypes = [];

            var rs = nlapiSearchRecord("customrecord_4601_witaxtype", null, filters, columns);
            if (rs != null)
            {
                for (var i in rs)
                {
                    SFC.System._TaxTypes.push(CreateInstanceFromSearchRow(rs[i]));
                }
            }
        }

        return SFC.System._TaxTypes;
    }
    
    //--------------------------------------------------------------------------
    this.FilterBySubsidiary = FilterBySubsidiary;
    function FilterBySubsidiary(subId)
    {
        if (SFC.System._TaxTypes == null)
        {
        	var subsidiary = nlapiLoadRecord('subsidiary', subId);
            
            var nexusCnt = subsidiary.getLineItemCount('nexus');

            var nexusMap = new Array();
            for (var i = 1; i <= nexusCnt; i++) {
                var nexusFilter = null; 
                var nexusId = subsidiary.getLineItemValue('nexus', 'nexusid', i);
                nexusFilter = new nlobjSearchFilter('custrecord_4601_wts_nexus', null, 'equalto', nexusId);
                if (i != nexusCnt) {
                    nexusFilter.setOr(true);
                }
                nexusMap.push(nexusFilter);
            }
            
            var setupColumns = new Array();
            setupColumns.push(new nlobjSearchColumn('custrecord_4601_wts_nexus'));

            var setup = nlapiSearchRecord('customrecord_4601_witaxsetup', null, nexusMap, setupColumns);
            
            if (setup != null) {
                var setupMap = new Array();
                for (var j = 0; j < setup.length; j++)
                    setupMap.push(setup[j].getId());

                var typeFilters = new Array();
                typeFilters.push(new nlobjSearchFilter('custrecord_4601_wtt_witaxsetup', null, 'anyof', setupMap));

                var typeColumns = GetTaxTypeColumns();
//              typeColumns.push(new nlobjSearchColumn('custrecord_4601_wtt_name'));

                var type = nlapiSearchRecord('customrecord_4601_witaxtype', null, typeFilters, typeColumns);
                
                if (type != null) {
                    SFC.System._TaxTypes = [];
                    if (type != null) {
                        for (var i in type) {
                            SFC.System._TaxTypes.push(CreateInstanceFromSearchRow(type[i]));
                        }
                    }
                }
            }
        }

        return SFC.System._TaxTypes || [];
    }
    
    
    //--------------------------------------------------------------------------
    this.FilterByNexus = FilterByNexus;
    function FilterByNexus(nexusId)
    {
        if (SFC.System._TaxTypes == null)
        {
            
            var nexusColumns = new Array();
            var nexusFilters = new Array();
            nexusFilters = new nlobjSearchFilter('country', null, 'is', nexusId);
            
            var rsNexus = nlapiSearchRecord('nexus', null, nexusFilters, nexusColumns);
            
            if (rsNexus != null ) {
                var setupFilters = new Array();
                setupFilters = new nlobjSearchFilter('custrecord_4601_wts_nexus', null, 'equalto', rsNexus[0].getId());
                
                var setupColumns = new Array();
                setupColumns.push(new nlobjSearchColumn('custrecord_4601_wts_nexus'));

                var setup = nlapiSearchRecord('customrecord_4601_witaxsetup', null, setupFilters, setupColumns);
                
                if (setup != null) {
                    var setupMap = new Array();
                    for (var j = 0; j < setup.length; j++)
                        setupMap.push(setup[j].getId());

                    var typeFilters = new Array();
                    typeFilters.push(new nlobjSearchFilter('custrecord_4601_wtt_witaxsetup', null, 'anyof', setupMap));

                    var typeColumns = GetTaxTypeColumns();

                    var type = nlapiSearchRecord('customrecord_4601_witaxtype', null, typeFilters, typeColumns);
                    
                    if (type != null) {
                        SFC.System._TaxTypes = [];
                        for (var i in type) {
                            SFC.System._TaxTypes.push(CreateInstanceFromSearchRow(type[i]));
                        }
                    }
                }
            }
        }

        return SFC.System._TaxTypes || [];
    }
    
    //--------------------------------------------------------------------------
    function GetTaxTypeColumns()
    {
        return [
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("custrecord_4601_wtt_name")
        ];
    }
    
    //--------------------------------------------------------------------------
    function CreateInstanceFromSearchRow(row)
    {
        if (row == null)
            return null

        var taxType = new SFC.System.TaxType();

        taxType.SetId(parseInt(row.getId()));//         typeColumns.push(new nlobjSearchColumn('custrecord_4601_wtt_name'));
        taxType.SetName(row.getValue("custrecord_4601_wtt_name"));

        return taxType;
    }
};

//==============================================================================
SFC.System.getSubsidiaryCountry = null;
SFC.System.getSubsidiaryCountry = function getSubsidiaryCountry(subId) {
    if(subId == null)
        return null;
    
    var subColumns =  new Array();
    subColumns.push(new nlobjSearchColumn('country'));

    var subFilters = new Array();
    subFilters.push(new nlobjSearchFilter('internalid', null, 'is', subId));
    
    var subRec = nlapiSearchRecord('subsidiary', null, subFilters, subColumns);
    
    var country = subRec[0].getValue('country');
    
    return country;
};


/**
* Version: 1.0 Alpha-1 
* Build Date: 13-Nov-2007
* Copyright (c) 2006-2007, Coolite Inc. (http://www.coolite.com/). All rights reserved.
* License: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. 
* Website: http://www.datejs.com/ or http://www.coolite.com/datejs/
*/
Date.CultureInfo = { name: "en-US", englishName: "English (United States)", nativeName: "English (United States)", dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], abbreviatedDayNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], shortestDayNames: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], firstLetterDayNames: ["S", "M", "T", "W", "T", "F", "S"], monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], abbreviatedMonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], amDesignator: "AM", pmDesignator: "PM", firstDayOfWeek: 0, twoDigitYearMax: 2029, dateElementOrder: "mdy", formatPatterns: { shortDate: "M/d/yyyy", longDate: "dddd, MMMM dd, yyyy", shortTime: "h:mm tt", longTime: "h:mm:ss tt", fullDateTime: "dddd, MMMM dd, yyyy h:mm:ss tt", sortableDateTime: "yyyy-MM-ddTHH:mm:ss", universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ", rfc1123: "ddd, dd MMM yyyy HH:mm:ss GMT", monthDay: "MMMM dd", yearMonth: "MMMM, yyyy" }, regexPatterns: { jan: /^jan(uary)?/i, feb: /^feb(ruary)?/i, mar: /^mar(ch)?/i, apr: /^apr(il)?/i, may: /^may/i, jun: /^jun(e)?/i, jul: /^jul(y)?/i, aug: /^aug(ust)?/i, sep: /^sep(t(ember)?)?/i, oct: /^oct(ober)?/i, nov: /^nov(ember)?/i, dec: /^dec(ember)?/i, sun: /^su(n(day)?)?/i, mon: /^mo(n(day)?)?/i, tue: /^tu(e(s(day)?)?)?/i, wed: /^we(d(nesday)?)?/i, thu: /^th(u(r(s(day)?)?)?)?/i, fri: /^fr(i(day)?)?/i, sat: /^sa(t(urday)?)?/i, future: /^next/i, past: /^last|past|prev(ious)?/i, add: /^(\+|after|from)/i, subtract: /^(\-|before|ago)/i, yesterday: /^yesterday/i, today: /^t(oday)?/i, tomorrow: /^tomorrow/i, now: /^n(ow)?/i, millisecond: /^ms|milli(second)?s?/i, second: /^sec(ond)?s?/i, minute: /^min(ute)?s?/i, hour: /^h(ou)?rs?/i, week: /^w(ee)?k/i, month: /^m(o(nth)?s?)?/i, day: /^d(ays?)?/i, year: /^y((ea)?rs?)?/i, shortMeridian: /^(a|p)/i, longMeridian: /^(a\.?m?\.?|p\.?m?\.?)/i, timezone: /^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt)/i, ordinalSuffix: /^\s*(st|nd|rd|th)/i, timeContext: /^\s*(\:|a|p)/i }, abbreviatedTimeZoneStandard: { GMT: "-000", EST: "-0400", CST: "-0500", MST: "-0600", PST: "-0700" }, abbreviatedTimeZoneDST: { GMT: "-000", EDT: "-0500", CDT: "-0600", MDT: "-0700", PDT: "-0800"} };
Date.getMonthNumberFromName = function(name)
{
    var n = Date.CultureInfo.monthNames, m = Date.CultureInfo.abbreviatedMonthNames, s = name.toLowerCase(); for (var i = 0; i < n.length; i++) { if (n[i].toLowerCase() == s || m[i].toLowerCase() == s) { return i; } }
    return -1;
}; Date.getDayNumberFromName = function(name)
{
    var n = Date.CultureInfo.dayNames, m = Date.CultureInfo.abbreviatedDayNames, o = Date.CultureInfo.shortestDayNames, s = name.toLowerCase(); for (var i = 0; i < n.length; i++) { if (n[i].toLowerCase() == s || m[i].toLowerCase() == s) { return i; } }
    return -1;
}; Date.isLeapYear = function(year) { return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); }; Date.getDaysInMonth = function(year, month) { return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]; }; Date.getTimezoneOffset = function(s, dst) { return (dst || false) ? Date.CultureInfo.abbreviatedTimeZoneDST[s.toUpperCase()] : Date.CultureInfo.abbreviatedTimeZoneStandard[s.toUpperCase()]; }; Date.getTimezoneAbbreviation = function(offset, dst)
{
    var n = (dst || false) ? Date.CultureInfo.abbreviatedTimeZoneDST : Date.CultureInfo.abbreviatedTimeZoneStandard, p; for (p in n) { if (n[p] === offset) { return p; } }
    return null;
}; Date.prototype.clone = function() { return new Date(this.getTime()); }; Date.prototype.compareTo = function(date)
{
    if (isNaN(this)) { throw new Error(this); }
    if (date instanceof Date && !isNaN(date)) { return (this > date) ? 1 : (this < date) ? -1 : 0; } else { throw new TypeError(date); } 
}; Date.prototype.equals = function(date) { return (this.compareTo(date) === 0); }; Date.prototype.between = function(start, end) { var t = this.getTime(); return t >= start.getTime() && t <= end.getTime(); }; Date.prototype.addMilliseconds = function(value) { this.setMilliseconds(this.getMilliseconds() + value); return this; }; Date.prototype.addSeconds = function(value) { return this.addMilliseconds(value * 1000); }; Date.prototype.addMinutes = function(value) { return this.addMilliseconds(value * 60000); }; Date.prototype.addHours = function(value) { return this.addMilliseconds(value * 3600000); }; Date.prototype.addDays = function(value) { return this.addMilliseconds(value * 86400000); }; Date.prototype.addWeeks = function(value) { return this.addMilliseconds(value * 604800000); }; Date.prototype.addMonths = function(value) { var n = this.getDate(); this.setDate(1); this.setMonth(this.getMonth() + value); this.setDate(Math.min(n, this.getDaysInMonth())); return this; }; Date.prototype.addYears = function(value) { return this.addMonths(value * 12); }; Date.prototype.add = function(config)
{
    if (typeof config == "number") { this._orient = config; return this; }
    var x = config; if (x.millisecond || x.milliseconds) { this.addMilliseconds(x.millisecond || x.milliseconds); }
    if (x.second || x.seconds) { this.addSeconds(x.second || x.seconds); }
    if (x.minute || x.minutes) { this.addMinutes(x.minute || x.minutes); }
    if (x.hour || x.hours) { this.addHours(x.hour || x.hours); }
    if (x.month || x.months) { this.addMonths(x.month || x.months); }
    if (x.year || x.years) { this.addYears(x.year || x.years); }
    if (x.day || x.days) { this.addDays(x.day || x.days); }
    return this;
}; Date._validate = function(value, min, max, name)
{
    if (typeof value != "number") { throw new TypeError(value + " is not a Number."); } else if (value < min || value > max) { throw new RangeError(value + " is not a valid value for " + name + "."); }
    return true;
}; Date.validateMillisecond = function(n) { return Date._validate(n, 0, 999, "milliseconds"); }; Date.validateSecond = function(n) { return Date._validate(n, 0, 59, "seconds"); }; Date.validateMinute = function(n) { return Date._validate(n, 0, 59, "minutes"); }; Date.validateHour = function(n) { return Date._validate(n, 0, 23, "hours"); }; Date.validateDay = function(n, year, month) { return Date._validate(n, 1, Date.getDaysInMonth(year, month), "days"); }; Date.validateMonth = function(n) { return Date._validate(n, 0, 11, "months"); }; Date.validateYear = function(n) { return Date._validate(n, 1, 9999, "seconds"); }; Date.prototype.set = function(config)
{
    var x = config; if (!x.millisecond && x.millisecond !== 0) { x.millisecond = -1; }
    if (!x.second && x.second !== 0) { x.second = -1; }
    if (!x.minute && x.minute !== 0) { x.minute = -1; }
    if (!x.hour && x.hour !== 0) { x.hour = -1; }
    if (!x.day && x.day !== 0) { x.day = -1; }
    if (!x.month && x.month !== 0) { x.month = -1; }
    if (!x.year && x.year !== 0) { x.year = -1; }
    if (x.millisecond != -1 && Date.validateMillisecond(x.millisecond)) { this.addMilliseconds(x.millisecond - this.getMilliseconds()); }
    if (x.second != -1 && Date.validateSecond(x.second)) { this.addSeconds(x.second - this.getSeconds()); }
    if (x.minute != -1 && Date.validateMinute(x.minute)) { this.addMinutes(x.minute - this.getMinutes()); }
    if (x.hour != -1 && Date.validateHour(x.hour)) { this.addHours(x.hour - this.getHours()); }
    if (x.month !== -1 && Date.validateMonth(x.month)) { this.addMonths(x.month - this.getMonth()); }
    if (x.year != -1 && Date.validateYear(x.year)) { this.addYears(x.year - this.getFullYear()); }
    if (x.day != -1 && Date.validateDay(x.day, this.getFullYear(), this.getMonth())) { this.addDays(x.day - this.getDate()); }
    if (x.timezone) { this.setTimezone(x.timezone); }
    if (x.timezoneOffset) { this.setTimezoneOffset(x.timezoneOffset); }
    return this;
}; Date.prototype.clearTime = function() { this.setHours(0); this.setMinutes(0); this.setSeconds(0); this.setMilliseconds(0); return this; }; Date.prototype.isLeapYear = function() { var y = this.getFullYear(); return (((y % 4 === 0) && (y % 100 !== 0)) || (y % 400 === 0)); }; Date.prototype.isWeekday = function() { return !(this.is().sat() || this.is().sun()); }; Date.prototype.getDaysInMonth = function() { return Date.getDaysInMonth(this.getFullYear(), this.getMonth()); }; Date.prototype.moveToFirstDayOfMonth = function() { return this.set({ day: 1 }); }; Date.prototype.moveToLastDayOfMonth = function() { return this.set({ day: this.getDaysInMonth() }); }; Date.prototype.moveToDayOfWeek = function(day, orient) { var diff = (day - this.getDay() + 7 * (orient || +1)) % 7; return this.addDays((diff === 0) ? diff += 7 * (orient || +1) : diff); }; Date.prototype.moveToMonth = function(month, orient) { var diff = (month - this.getMonth() + 12 * (orient || +1)) % 12; return this.addMonths((diff === 0) ? diff += 12 * (orient || +1) : diff); }; Date.prototype.getDayOfYear = function() { return Math.floor((this - new Date(this.getFullYear(), 0, 1)) / 86400000); }; Date.prototype.getWeekOfYear = function(firstDayOfWeek)
{
    var y = this.getFullYear(), m = this.getMonth(), d = this.getDate(); var dow = firstDayOfWeek || Date.CultureInfo.firstDayOfWeek; var offset = 7 + 1 - new Date(y, 0, 1).getDay(); if (offset == 8) { offset = 1; }
    var daynum = ((Date.UTC(y, m, d, 0, 0, 0) - Date.UTC(y, 0, 1, 0, 0, 0)) / 86400000) + 1; var w = Math.floor((daynum - offset + 7) / 7); if (w === dow) { y--; var prevOffset = 7 + 1 - new Date(y, 0, 1).getDay(); if (prevOffset == 2 || prevOffset == 8) { w = 53; } else { w = 52; } }
    return w;
}; Date.prototype.isDST = function() { console.log('isDST'); return this.toString().match(/(E|C|M|P)(S|D)T/)[2] == "D"; }; Date.prototype.getTimezone = function() { return Date.getTimezoneAbbreviation(this.getUTCOffset, this.isDST()); }; Date.prototype.setTimezoneOffset = function(s) { var here = this.getTimezoneOffset(), there = Number(s) * -6 / 10; this.addMinutes(there - here); return this; }; Date.prototype.setTimezone = function(s) { return this.setTimezoneOffset(Date.getTimezoneOffset(s)); }; Date.prototype.getUTCOffset = function() { var n = this.getTimezoneOffset() * -10 / 6, r; if (n < 0) { r = (n - 10000).toString(); return r[0] + r.substr(2); } else { r = (n + 10000).toString(); return "+" + r.substr(1); } }; Date.prototype.getDayName = function(abbrev) { return abbrev ? Date.CultureInfo.abbreviatedDayNames[this.getDay()] : Date.CultureInfo.dayNames[this.getDay()]; }; Date.prototype.getMonthName = function(abbrev) { return abbrev ? Date.CultureInfo.abbreviatedMonthNames[this.getMonth()] : Date.CultureInfo.monthNames[this.getMonth()]; }; Date.prototype._toString = Date.prototype.toString; Date.prototype.toString = function(format) { var self = this; var p = function p(s) { return (s.toString().length == 1) ? "0" + s : s; }; return format ? format.replace(/dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?/g, function(format) { switch (format) { case "hh": return p(self.getHours() < 13 ? self.getHours() : (self.getHours() - 12)); case "h": return self.getHours() < 13 ? self.getHours() : (self.getHours() - 12); case "HH": return p(self.getHours()); case "H": return self.getHours(); case "mm": return p(self.getMinutes()); case "m": return self.getMinutes(); case "ss": return p(self.getSeconds()); case "s": return self.getSeconds(); case "yyyy": return self.getFullYear(); case "yy": return self.getFullYear().toString().substring(2, 4); case "dddd": return self.getDayName(); case "ddd": return self.getDayName(true); case "dd": return p(self.getDate()); case "d": return self.getDate().toString(); case "MMMM": return self.getMonthName(); case "MMM": return self.getMonthName(true); case "MM": return p((self.getMonth() + 1)); case "M": return self.getMonth() + 1; case "t": return self.getHours() < 12 ? Date.CultureInfo.amDesignator.substring(0, 1) : Date.CultureInfo.pmDesignator.substring(0, 1); case "tt": return self.getHours() < 12 ? Date.CultureInfo.amDesignator : Date.CultureInfo.pmDesignator; case "zzz": case "zz": case "z": return ""; } }) : this._toString(); };
Date.now = function() { return new Date(); }; Date.today = function() { return Date.now().clearTime(); }; Date.prototype._orient = +1; Date.prototype.next = function() { this._orient = +1; return this; }; Date.prototype.last = Date.prototype.prev = Date.prototype.previous = function() { this._orient = -1; return this; }; Date.prototype._is = false; Date.prototype.is = function() { this._is = true; return this; }; Number.prototype._dateElement = "day"; Number.prototype.fromNow = function() { var c = {}; c[this._dateElement] = this; return Date.now().add(c); }; Number.prototype.ago = function() { var c = {}; c[this._dateElement] = this * -1; return Date.now().add(c); }; (function()
{
    var $D = Date.prototype, $N = Number.prototype; var dx = ("sunday monday tuesday wednesday thursday friday saturday").split(/\s/), mx = ("january february march april may june july august september october november december").split(/\s/), px = ("Millisecond Second Minute Hour Day Week Month Year").split(/\s/), de; var df = function(n)
    {
        return function()
        {
            if (this._is) { this._is = false; return this.getDay() == n; }
            return this.moveToDayOfWeek(n, this._orient);
        };
    }; for (var i = 0; i < dx.length; i++) { $D[dx[i]] = $D[dx[i].substring(0, 3)] = df(i); }
    var mf = function(n)
    {
        return function()
        {
            if (this._is) { this._is = false; return this.getMonth() === n; }
            return this.moveToMonth(n, this._orient);
        };
    }; for (var j = 0; j < mx.length; j++) { $D[mx[j]] = $D[mx[j].substring(0, 3)] = mf(j); }
    var ef = function(j)
    {
        return function()
        {
            if (j.substring(j.length - 1) != "s") { j += "s"; }
            return this["add" + j](this._orient);
        };
    }; var nf = function(n) { return function() { this._dateElement = n; return this; }; }; for (var k = 0; k < px.length; k++) { de = px[k].toLowerCase(); $D[de] = $D[de + "s"] = ef(px[k]); $N[de] = $N[de + "s"] = nf(de); } 
} ()); Date.prototype.toJSONString = function() { return this.toString("yyyy-MM-ddThh:mm:ssZ"); }; Date.prototype.toShortDateString = function() { return this.toString(Date.CultureInfo.formatPatterns.shortDatePattern); }; Date.prototype.toLongDateString = function() { return this.toString(Date.CultureInfo.formatPatterns.longDatePattern); }; Date.prototype.toShortTimeString = function() { return this.toString(Date.CultureInfo.formatPatterns.shortTimePattern); }; Date.prototype.toLongTimeString = function() { return this.toString(Date.CultureInfo.formatPatterns.longTimePattern); }; Date.prototype.getOrdinal = function() { switch (this.getDate()) { case 1: case 21: case 31: return "st"; case 2: case 22: return "nd"; case 3: case 23: return "rd"; default: return "th"; } };
(function()
{
    Date.Parsing = { Exception: function(s) { this.message = "Parse error at '" + s.substring(0, 10) + " ...'"; } }; var $P = Date.Parsing; var _ = $P.Operators = { rtoken: function(r) { return function(s) { var mx = s.match(r); if (mx) { return ([mx[0], s.substring(mx[0].length)]); } else { throw new $P.Exception(s); } }; }, token: function(s) { return function(s) { return _.rtoken(new RegExp("^\s*" + s + "\s*"))(s); }; }, stoken: function(s) { return _.rtoken(new RegExp("^" + s)); }, until: function(p)
    {
        return function(s)
        {
            var qx = [], rx = null; while (s.length)
            {
                try { rx = p.call(this, s); } catch (e) { qx.push(rx[0]); s = rx[1]; continue; }
                break;
            }
            return [qx, s];
        };
    }, many: function(p)
    {
        return function(s)
        {
            var rx = [], r = null; while (s.length)
            {
                try { r = p.call(this, s); } catch (e) { return [rx, s]; }
                rx.push(r[0]); s = r[1];
            }
            return [rx, s];
        };
    }, optional: function(p)
    {
        return function(s)
        {
            var r = null; try { r = p.call(this, s); } catch (e) { return [null, s]; }
            return [r[0], r[1]];
        };
    }, not: function(p)
    {
        return function(s)
        {
            try { p.call(this, s); } catch (e) { return [null, s]; }
            throw new $P.Exception(s);
        };
    }, ignore: function(p) { return p ? function(s) { var r = null; r = p.call(this, s); return [null, r[1]]; } : null; }, product: function()
    {
        var px = arguments[0], qx = Array.prototype.slice.call(arguments, 1), rx = []; for (var i = 0; i < px.length; i++) { rx.push(_.each(px[i], qx)); }
        return rx;
    }, cache: function(rule)
    {
        var cache = {}, r = null; return function(s)
        {
            try { r = cache[s] = (cache[s] || rule.call(this, s)); } catch (e) { r = cache[s] = e; }
            if (r instanceof $P.Exception) { throw r; } else { return r; } 
        };
    }, any: function()
    {
        var px = arguments; return function(s)
        {
            var r = null; for (var i = 0; i < px.length; i++)
            {
                if (px[i] == null) { continue; }
                try { r = (px[i].call(this, s)); } catch (e) { r = null; }
                if (r) { return r; } 
            }
            throw new $P.Exception(s);
        };
    }, each: function()
    {
        var px = arguments; return function(s)
        {
            var rx = [], r = null; for (var i = 0; i < px.length; i++)
            {
                if (px[i] == null) { continue; }
                try { r = (px[i].call(this, s)); } catch (e) { throw new $P.Exception(s); }
                rx.push(r[0]); s = r[1];
            }
            return [rx, s];
        };
    }, all: function() { var px = arguments, _ = _; return _.each(_.optional(px)); }, sequence: function(px, d, c)
    {
        d = d || _.rtoken(/^\s*/); c = c || null; if (px.length == 1) { return px[0]; }
        return function(s)
        {
            var r = null, q = null; var rx = []; for (var i = 0; i < px.length; i++)
            {
                try { r = px[i].call(this, s); } catch (e) { break; }
                rx.push(r[0]); try { q = d.call(this, r[1]); } catch (ex) { q = null; break; }
                s = q[1];
            }
            if (!r) { throw new $P.Exception(s); }
            if (q) { throw new $P.Exception(q[1]); }
            if (c) { try { r = c.call(this, r[1]); } catch (ey) { throw new $P.Exception(r[1]); } }
            return [rx, (r ? r[1] : s)];
        };
    }, between: function(d1, p, d2) { d2 = d2 || d1; var _fn = _.each(_.ignore(d1), p, _.ignore(d2)); return function(s) { var rx = _fn.call(this, s); return [[rx[0][0], r[0][2]], rx[1]]; }; }, list: function(p, d, c) { d = d || _.rtoken(/^\s*/); c = c || null; return (p instanceof Array ? _.each(_.product(p.slice(0, -1), _.ignore(d)), p.slice(-1), _.ignore(c)) : _.each(_.many(_.each(p, _.ignore(d))), px, _.ignore(c))); }, set: function(px, d, c)
    {
        d = d || _.rtoken(/^\s*/); c = c || null; return function(s)
        {
            var r = null, p = null, q = null, rx = null, best = [[], s], last = false; for (var i = 0; i < px.length; i++)
            {
                q = null; p = null; r = null; last = (px.length == 1); try { r = px[i].call(this, s); } catch (e) { continue; }
                rx = [[r[0]], r[1]]; if (r[1].length > 0 && !last) { try { q = d.call(this, r[1]); } catch (ex) { last = true; } } else { last = true; }
                if (!last && q[1].length === 0) { last = true; }
                if (!last)
                {
                    var qx = []; for (var j = 0; j < px.length; j++) { if (i != j) { qx.push(px[j]); } }
                    p = _.set(qx, d).call(this, q[1]); if (p[0].length > 0) { rx[0] = rx[0].concat(p[0]); rx[1] = p[1]; } 
                }
                if (rx[1].length < best[1].length) { best = rx; }
                if (best[1].length === 0) { break; } 
            }
            if (best[0].length === 0) { return best; }
            if (c)
            {
                try { q = c.call(this, best[1]); } catch (ey) { throw new $P.Exception(best[1]); }
                best[1] = q[1];
            }
            return best;
        };
    }, forward: function(gr, fname) { return function(s) { return gr[fname].call(this, s); }; }, replace: function(rule, repl) { return function(s) { var r = rule.call(this, s); return [repl, r[1]]; }; }, process: function(rule, fn) { return function(s) { var r = rule.call(this, s); return [fn.call(this, r[0]), r[1]]; }; }, min: function(min, rule)
    {
        return function(s)
        {
            var rx = rule.call(this, s); if (rx[0].length < min) { throw new $P.Exception(s); }
            return rx;
        };
    } 
    }; var _generator = function(op)
    {
        return function()
        {
            var args = null, rx = []; if (arguments.length > 1) { args = Array.prototype.slice.call(arguments); } else if (arguments[0] instanceof Array) { args = arguments[0]; }
            if (args) { for (var i = 0, px = args.shift(); i < px.length; i++) { args.unshift(px[i]); rx.push(op.apply(null, args)); args.shift(); return rx; } } else { return op.apply(null, arguments); } 
        };
    }; var gx = "optional not ignore cache".split(/\s/); for (var i = 0; i < gx.length; i++) { _[gx[i]] = _generator(_[gx[i]]); }
    var _vector = function(op) { return function() { if (arguments[0] instanceof Array) { return op.apply(null, arguments[0]); } else { return op.apply(null, arguments); } }; }; var vx = "each any all".split(/\s/); for (var j = 0; j < vx.length; j++) { _[vx[j]] = _vector(_[vx[j]]); } 
} ()); (function()
{
    var flattenAndCompact = function(ax)
    {
        var rx = []; for (var i = 0; i < ax.length; i++) { if (ax[i] instanceof Array) { rx = rx.concat(flattenAndCompact(ax[i])); } else { if (ax[i]) { rx.push(ax[i]); } } }
        return rx;
    }; Date.Grammar = {}; Date.Translator = { hour: function(s) { return function() { this.hour = Number(s); }; }, minute: function(s) { return function() { this.minute = Number(s); }; }, second: function(s) { return function() { this.second = Number(s); }; }, meridian: function(s) { return function() { this.meridian = s.slice(0, 1).toLowerCase(); }; }, timezone: function(s) { return function() { var n = s.replace(/[^\d\+\-]/g, ""); if (n.length) { this.timezoneOffset = Number(n); } else { this.timezone = s.toLowerCase(); } }; }, day: function(x) { var s = x[0]; return function() { this.day = Number(s.match(/\d+/)[0]); }; }, month: function(s) { return function() { this.month = ((s.length == 3) ? Date.getMonthNumberFromName(s) : (Number(s) - 1)); }; }, year: function(s) { return function() { var n = Number(s); this.year = ((s.length > 2) ? n : (n + (((n + 2000) < Date.CultureInfo.twoDigitYearMax) ? 2000 : 1900))); }; }, rday: function(s) { return function() { switch (s) { case "yesterday": this.days = -1; break; case "tomorrow": this.days = 1; break; case "today": this.days = 0; break; case "now": this.days = 0; this.now = true; break; } }; }, finishExact: function(x)
    {
        x = (x instanceof Array) ? x : [x]; var now = new Date(); this.year = now.getFullYear(); this.month = now.getMonth(); this.day = 1; this.hour = 0; this.minute = 0; this.second = 0; for (var i = 0; i < x.length; i++) { if (x[i]) { x[i].call(this); } }
        this.hour = (this.meridian == "p" && this.hour < 13) ? this.hour + 12 : this.hour; if (this.day > Date.getDaysInMonth(this.year, this.month)) { throw new RangeError(this.day + " is not a valid value for days."); }
        var r = new Date(this.year, this.month, this.day, this.hour, this.minute, this.second); if (this.timezone) { r.set({ timezone: this.timezone }); } else if (this.timezoneOffset) { r.set({ timezoneOffset: this.timezoneOffset }); }
        return r;
    }, finish: function(x)
    {
        x = (x instanceof Array) ? flattenAndCompact(x) : [x]; if (x.length === 0) { return null; }
        for (var i = 0; i < x.length; i++) { if (typeof x[i] == "function") { x[i].call(this); } }
        if (this.now) { return new Date(); }
        var today = Date.today(); var method = null; var expression = !!(this.days != null || this.orient || this.operator); if (expression)
        {
            var gap, mod, orient; orient = ((this.orient == "past" || this.operator == "subtract") ? -1 : 1); if (this.weekday) { this.unit = "day"; gap = (Date.getDayNumberFromName(this.weekday) - today.getDay()); mod = 7; this.days = gap ? ((gap + (orient * mod)) % mod) : (orient * mod); }
            if (this.month) { this.unit = "month"; gap = (this.month - today.getMonth()); mod = 12; this.months = gap ? ((gap + (orient * mod)) % mod) : (orient * mod); this.month = null; }
            if (!this.unit) { this.unit = "day"; }
            if (this[this.unit + "s"] == null || this.operator != null)
            {
                if (!this.value) { this.value = 1; }
                if (this.unit == "week") { this.unit = "day"; this.value = this.value * 7; }
                this[this.unit + "s"] = this.value * orient;
            }
            return today.add(this);
        } else
        {
            if (this.meridian && this.hour) { this.hour = (this.hour < 13 && this.meridian == "p") ? this.hour + 12 : this.hour; }
            if (this.weekday && !this.day) { this.day = (today.addDays((Date.getDayNumberFromName(this.weekday) - today.getDay()))).getDate(); }
            if (this.month && !this.day) { this.day = 1; }
            return today.set(this);
        } 
    } 
    }; var _ = Date.Parsing.Operators, g = Date.Grammar, t = Date.Translator, _fn; g.datePartDelimiter = _.rtoken(/^([\s\-\.\,\/\x27]+)/); g.timePartDelimiter = _.stoken(":"); g.whiteSpace = _.rtoken(/^\s*/); g.generalDelimiter = _.rtoken(/^(([\s\,]|at|on)+)/); var _C = {}; g.ctoken = function(keys)
    {
        var fn = _C[keys]; if (!fn)
        {
            var c = Date.CultureInfo.regexPatterns; var kx = keys.split(/\s+/), px = []; for (var i = 0; i < kx.length; i++) { px.push(_.replace(_.rtoken(c[kx[i]]), kx[i])); }
            fn = _C[keys] = _.any.apply(null, px);
        }
        return fn;
    }; g.ctoken2 = function(key) { return _.rtoken(Date.CultureInfo.regexPatterns[key]); }; g.h = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2]|[1-9])/), t.hour)); g.hh = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2])/), t.hour)); g.H = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/), t.hour)); g.HH = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/), t.hour)); g.m = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.minute)); g.mm = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.minute)); g.s = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.second)); g.ss = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.second)); g.hms = _.cache(_.sequence([g.H, g.mm, g.ss], g.timePartDelimiter)); g.t = _.cache(_.process(g.ctoken2("shortMeridian"), t.meridian)); g.tt = _.cache(_.process(g.ctoken2("longMeridian"), t.meridian)); g.z = _.cache(_.process(_.rtoken(/^(\+|\-)?\s*\d\d\d\d?/), t.timezone)); g.zz = _.cache(_.process(_.rtoken(/^(\+|\-)\s*\d\d\d\d/), t.timezone)); g.zzz = _.cache(_.process(g.ctoken2("timezone"), t.timezone)); g.timeSuffix = _.each(_.ignore(g.whiteSpace), _.set([g.tt, g.zzz])); g.time = _.each(_.optional(_.ignore(_.stoken("T"))), g.hms, g.timeSuffix); g.d = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1]|\d)/), _.optional(g.ctoken2("ordinalSuffix"))), t.day)); g.dd = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1])/), _.optional(g.ctoken2("ordinalSuffix"))), t.day)); g.ddd = g.dddd = _.cache(_.process(g.ctoken("sun mon tue wed thu fri sat"), function(s) { return function() { this.weekday = s; }; })); g.M = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d|\d)/), t.month)); g.MM = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d)/), t.month)); g.MMM = g.MMMM = _.cache(_.process(g.ctoken("jan feb mar apr may jun jul aug sep oct nov dec"), t.month)); g.y = _.cache(_.process(_.rtoken(/^(\d\d?)/), t.year)); g.yy = _.cache(_.process(_.rtoken(/^(\d\d)/), t.year)); g.yyy = _.cache(_.process(_.rtoken(/^(\d\d?\d?\d?)/), t.year)); g.yyyy = _.cache(_.process(_.rtoken(/^(\d\d\d\d)/), t.year)); _fn = function() { return _.each(_.any.apply(null, arguments), _.not(g.ctoken2("timeContext"))); }; g.day = _fn(g.d, g.dd); g.month = _fn(g.M, g.MMM); g.year = _fn(g.yyyy, g.yy); g.orientation = _.process(g.ctoken("past future"), function(s) { return function() { this.orient = s; }; }); g.operator = _.process(g.ctoken("add subtract"), function(s) { return function() { this.operator = s; }; }); g.rday = _.process(g.ctoken("yesterday tomorrow today now"), t.rday); g.unit = _.process(g.ctoken("minute hour day week month year"), function(s) { return function() { this.unit = s; }; }); g.value = _.process(_.rtoken(/^\d\d?(st|nd|rd|th)?/), function(s) { return function() { this.value = s.replace(/\D/g, ""); }; }); g.expression = _.set([g.rday, g.operator, g.value, g.unit, g.orientation, g.ddd, g.MMM]); _fn = function() { return _.set(arguments, g.datePartDelimiter); }; g.mdy = _fn(g.ddd, g.month, g.day, g.year); g.ymd = _fn(g.ddd, g.year, g.month, g.day); g.dmy = _fn(g.ddd, g.day, g.month, g.year); g.date = function(s) { return ((g[Date.CultureInfo.dateElementOrder] || g.mdy).call(this, s)); }; g.format = _.process(_.many(_.any(_.process(_.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/), function(fmt) { if (g[fmt]) { return g[fmt]; } else { throw Date.Parsing.Exception(fmt); } }), _.process(_.rtoken(/^[^dMyhHmstz]+/), function(s) { return _.ignore(_.stoken(s)); }))), function(rules) { return _.process(_.each.apply(null, rules), t.finishExact); }); var _F = {}; var _get = function(f) { return _F[f] = (_F[f] || g.format(f)[0]); }; g.formats = function(fx)
    {
        if (fx instanceof Array)
        {
            var rx = []; for (var i = 0; i < fx.length; i++) { rx.push(_get(fx[i])); }
            return _.any.apply(null, rx);
        } else { return _get(fx); } 
    }; g._formats = g.formats(["yyyy-MM-ddTHH:mm:ss", "ddd, MMM dd, yyyy H:mm:ss tt", "ddd MMM d yyyy HH:mm:ss zzz", "d"]); g._start = _.process(_.set([g.date, g.time, g.expression], g.generalDelimiter, g.whiteSpace), t.finish); g.start = function(s)
    {
        try { var r = g._formats.call({}, s); if (r[1].length === 0) { return r; } } catch (e) { }
        return g._start.call({}, s);
    };
} ()); Date._parse = Date.parse; Date.parse = function(s)
{
    var r = null; if (!s) { return null; }
    try { r = Date.Grammar.start.call({}, s); } catch (e) { return null; }
    return ((r[1].length === 0) ? r[0] : null);
}; Date.getParseFunction = function(fx)
{
    var fn = Date.Grammar.formats(fx); return function(s)
    {
        var r = null; try { r = fn.call({}, s); } catch (e) { return null; }
        return ((r[1].length === 0) ? r[0] : null);
    };
}; Date.parseExact = function(s, fx) { return Date.getParseFunction(fx)(s); };


//for Number formatting, and Negative formatting
_4601.formatCurrency = function formatCurrency(strCurrency, documentType) {
    var numberformat = nlapiGetContext().getSetting('SCRIPT','NUMBERFORMAT');
    var numberformatted;
    strCurrency = ""+ nlapiFormatCurrency(strCurrency);
    x = strCurrency.split('.');
    x1 = x[0];
    var rgx;


    switch(numberformat)
    {
        case "0":
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
        case "1":
        x2 = x.length > 1 ? ',' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
        case "2":
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
        case "3":
        x2 = x.length > 1 ? ',' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
        case "4":
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        var delim = '';
        while (rgx.test(x1)) {
            if (documentType == 'PDF')
                delim = '&#x60C;'
            else
                delim = String.fromCharCode('1548');
            x1 = x1.replace(rgx, '$1' + delim + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
        case "5":
        x2 = x.length > 1 ? ',' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        var delim = '';
        while (rgx.test(x1)) {
            if (documentType == 'PDF')
                delim = '&#x60C;'
            else
                delim = String.fromCharCode('1548');
            x1 = x1.replace(rgx, '$1' + delim + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
        default:
        x2 = x.length > 1 ? '.' + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2'); //check delimiter
        }
        numberformatted = x1 + x2;
        break;
    }
    
    var negativeformat = nlapiGetContext().getSetting('SCRIPT','NEGATIVE_NUMBER_FORMAT'); 

    if(negativeformat == '1'){ //if User Preference Negative format is (-123)
    var rgx2 = /^-\d+(?:\.\d+)?$/; //check delimiter
        if(strCurrency.match(rgx2)){ //check if value is negative
            numberformatted = numberformatted.replace("-","(")+")";
        }
    }

    return numberformatted;
};


_4601.formatRate = function formatRate(rate, documentType) {
    var percentSign = '';
    if (rate.search(/%$/g) > -1) {
        percentSign = '%';
        rate = rate.substr(0, rate.length-1);
    }
    
    rate = _4601.formatCurrency(rate, documentType);
    
    return rate + percentSign;
};

_4601.getCompanyRecord = function getCompanyRecord(is_oneworld, subsidiary_id) {
    var company_rec = {};
    
    try{
        if (is_oneworld){
        	company_rec = nlapiLoadRecord("subsidiary", subsidiary_id);
        } else {
            company_rec = nlapiLoadConfiguration("companyinformation");
        }
    } catch(ex) {
        throw nlapiCreateError("WITHHOLDING TAX: Missing subsidiary id", "WITHHOLDING TAX: Missing subsidiary id.");
    }
    
    return company_rec;
};
