/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

//A lightweight IoC container based on Martin Fowler's Inversion of Control Containers and the Dependency Injection pattern.
var WTax = WTax || {};

WTax.injector = {
	dependencies: {},
	addClass: function(qualifier, obj, isSingleton) {
		this.dependencies[qualifier] = {
			obj: obj,
			isSingleton: isSingleton,
			instance: null
		};
	},
	addObject: function(qualifier, obj) {
		this.dependencies[qualifier] = {
			obj: obj,
			isSingleton: false
		};
	},
	hasDependency: function(qualifier) {
		return this.dependencies[qualifier] ? true : false;
	},
	getObject: function (obj) {
		return this.resolveObjectDependencies(obj);
	},
	getInstance: function(func){
		var obj = new func;
		var dependencies = this.resolveDependencies(func);
		func.apply(obj, dependencies);
		return obj;
	},
	resolveObjectDependencies: function(obj) {
		var args = this.getObjectArgs(obj, true);
		for (var idep = 0; idep < args.length; idep++) {
			//Undefined dependency
			if (!this.dependencies[args[idep]]) {
				continue;
			}

			//Set object
			if (this.dependencies[args[idep]].instance === undefined) {
				obj[args[idep]] = this.dependencies[args[idep]].obj;
				continue;
			}

			//Instantiate class
			var instance = this.dependencies[args[idep]].instance;
			var isSingleton = this.dependencies[args[idep]].isSingleton;
			if (isSingleton && instance) {
				obj[args[idep]] = instance;
			} else if (isSingleton && !instance) {
				instance = new this.dependencies[args[idep]].obj();
				obj[args[idep]] = instance;
			} else {
				obj[args[idep]] = new this.dependencies[args[idep]].obj();
			}
		}
		return obj;
	},
	resolveDependencies: function(func) {
		var args = this.getFunctionArgs(func);
		var dependencies = [];
		for (var idep = 0; idep < args.length; idep++) {
			//Undefined dependency
			if (!this.dependencies[args[idep]]) {
				dependencies.push(null);
				continue;
			}

			//Set object
			if (this.dependencies[args[idep]].instance === undefined) {
				dependencies.push(this.dependencies[args[idep]].obj);
				continue;
			}

			//Instantiate class
			var instance = this.dependencies[args[idep]].instance;
			var isSingleton = this.dependencies[args[idep]].isSingleton;
			if (isSingleton && instance) {
				dependencies.push(instance);
			} else if (isSingleton && !instance) {
				instance = new this.dependencies[args[idep]].obj();
				dependencies.push(instance);
			} else {
				
				dependencies.push(new this.dependencies[args[idep]].obj());
			}
		}
		return dependencies;
	},
	getFunctionArgs: function(func) {
		var args = [];
		//This regex is from require.js
		var FN_ARGS = /^function\s*[^\(]*\(\s*([^\)]*)\)/m;
		args = func.toString().match(FN_ARGS)[1].split(',');
		return args;
	},
	getObjectArgs: function(obj) {
		var args = [];
		for (var o in obj) {
			args.push(o);
		}
		return args;
	}
};