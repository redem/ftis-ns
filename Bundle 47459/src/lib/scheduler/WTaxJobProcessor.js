/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.WTaxJobProcessor = function _WTaxJobProcessor() {
    this.monitors = [];
};

WTax.WTaxJobProcessor.prototype.processJobs = function _processJobs() {
    var scheduler = new WTax.WTaxJobScheduler();
    var dao = new WTax.DAO.WTaxJobDao();
    var jobManager = new WTax.WTaxJobManager(dao);
    
    this.addThresholdMonitor(new WTax.ApiGovernanceMonitor('scheduledscript'));
    this.addThresholdMonitor(new WTax.ScriptTimeoutMonitor('scheduledscript'));
    
    while (jobManager.hasNextJob()) {
        var job = jobManager.getNextJob();

        try {
            var processor = findClass(WTax, job.processor, {
                job: job,
                monitors: this.monitors
            });
            
            jobManager.updateJob({
                id: job.id,
                status: WTax.DAO.WTaxJob.STATUS.PROCESSING
            });
            
            var jobResult = processor.run();
            job = jobResult.job;
            
            switch (jobResult.status) {
                case WTax.DAO.WTaxJob.STATUS.DONE:
                case WTax.DAO.WTaxJob.STATUS.FAILED:
                    job.status = jobResult.status;
                    jobManager.updateJob(job);
                    break;
                case WTax.DAO.WTaxJob.STATUS.RESCHEDULING:
                    job.status = WTax.DAO.WTaxJob.STATUS.PENDING;
                    jobManager.updateJob(job);
                    
                    nlapiLogExecution('DEBUG', 'WTax.WTaxJobProcessor.processJobs', 'Rescheduling job [ID: ' + job.id + ']');
                    scheduler.run();
                    return;
                default:
                    jobManager.updateJob(job);
                    break;
            }
        } catch (ex) {
            job.status = WTax.DAO.WTaxJob.STATUS.FAILED;
            jobManager.updateJob(job);
            WTaxError.logError(ex, 'WTax.WTaxJobProcessor.processJobs', WTaxError.LEVEL.ERROR);
        }
        
        // Send email notification
        if (job.outputData.sendEmail) {
            this.sendEmail(job.outputData.message);
        }
        
        if (this.isThresholdReached()) {
            nlapiLogExecution('DEBUG', 'WTax.WTaxJobProcessor.processJobs', 'Rescheduling job [ID: ' + job.id + ']');
            scheduler.run();
            break;
        }
    }
};

WTax.WTaxJobProcessor.prototype.addThresholdMonitor = function _addThresholdMonitor(monitor) {
    if (!monitor) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A threshold monitor instance is required.');
        WTaxError.throwError(error, 'WTax.WTaxJobProcessor.addThresholdMonitor', WTaxError.LEVEL.ERROR);
    }

    this.monitors.push(monitor);
};

WTax.WTaxJobProcessor.prototype.isThresholdReached = function _isThresholdReached() {
    for (var i = 0; i < this.monitors.length; i++) {
        if (this.monitors[i].isThresholdReached()) {
            return true;
        }
    }
    
    return false;
};

WTax.WTaxJobProcessor.prototype.sendEmail = function _sendEmail(message) {
    var emailObj = this.createEmailNotificationMessage(message);
    new WTax.EmailSender().sendEmail(emailObj);
};

WTax.WTaxJobProcessor.prototype.createEmailNotificationMessage = function _createEmailNotificationMessage(params) {
    if (!params) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A params is required.');
        WTaxError.throwError(error, 'WTax.WTaxJobProcessor.createEmailNotificationMessage', WTaxError.LEVEL.ERROR);
    }

    return {
        sender: params.user,
        recipient: params.user,
        subject: params.subject,
        body: params.body
    };
};
