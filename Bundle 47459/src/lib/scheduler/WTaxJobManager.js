/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.WTaxJobManager = function _WTaxJobManager(jobDao) {
	if(!jobDao) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A jobDao is required.');
		WTaxError.throwError(error, 'WTax.WTaxJobManager', WTaxError.LEVEL.ERROR);
	}

	this.jobDao = jobDao;
	this.queue = jobDao.getList({ status: WTax.DAO.WTaxJob.STATUS.PENDING });
};

WTax.WTaxJobManager.prototype.addJob = function _addJob(job) {
	if(!job) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A job is required.');
		WTaxError.throwError(error, 'WTax.WTaxJobManager.addJob', WTaxError.LEVEL.ERROR);
	}

    try {
        job.status = WTax.DAO.WTaxJob.STATUS.PENDING;

        job = this.jobDao.create(job);

        this.queue.push(job);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxJobManager.addJob', WTaxError.LEVEL.ERROR);
    }

    return job;
};

WTax.WTaxJobManager.prototype.updateJob = function _updateJob(params) {
	if(!params) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A params is required.');
		WTaxError.throwError(error, 'WTax.WTaxJobManager.updateJob', WTaxError.LEVEL.ERROR);
	}

    var job = null;

    try {
        job = new WTax.DAO.WTaxJob();
        job.id = params.id;
        job.status = params.status;
        job.state = params.state;
        job.outputData = params.outputData;
        
        job = this.jobDao.update(job);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.WTaxJobManager.updateJob', WTaxError.LEVEL.ERROR);
    }

    return job;
};

WTax.WTaxJobManager.prototype.getNextJob = function _getNextJob() {
	this.queue = this.loadJobs();
    return this.queue.shift();
};

WTax.WTaxJobManager.prototype.hasNextJob = function _hasNextJob() {
	this.queue = this.loadJobs();
	return this.queue && this.queue.length > 0;
};

WTax.WTaxJobManager.prototype.loadJobs = function _loadJobs() {
	if (this.queue && this.queue.length == 0) {
		return this.jobDao.getList({ status: WTax.DAO.WTaxJob.STATUS.PENDING });
	}
	return this.queue;
};

WTax.WTaxJobManager.prototype.getJobStatus = function _getJobStatus(recordType, recordId) {
	if(!recordType) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A recordType is required.');
		WTaxError.throwError(error, 'WTax.WTaxJobManager.getJobStatus', WTaxError.LEVEL.ERROR);
	}
	if(!recordId) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAM', 'A recordId is required.');
		WTaxError.throwError(error, 'WTax.WTaxJobManager.getJobStatus', WTaxError.LEVEL.ERROR);
	}

    var status = null;
    
    try {
    	var jobParams = {
			recordType: recordType,
			recordId: recordId
    	};
        var job = this.jobDao.getJobByRecordType(jobParams) || {};

        status = job ? job.status : null;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.WTaxJobManager.getJobStatus', WTaxError.LEVEL.ERROR);
    }
    
    return status;
};

WTax.WTaxJobManager.getJobURL = function getJobURL(baseURL, jobId) {
    return [baseURL, nlapiResolveURL('RECORD', WTax.DAO.WTaxJob.RECORD_TYPE, jobId)].join('');
}
