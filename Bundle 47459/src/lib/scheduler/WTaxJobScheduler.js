/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.WTaxJobScheduler = function _WTaxJobScheduler() {
	this.scriptId = 'customscript_wtax_job_processor';
	this.deploymentId = 'customdeploy_wtax_job_processor';
};

WTax.WTaxJobScheduler.prototype.run = function _run(params) {
    nlapiScheduleScript(this.scriptId, this.deploymentId, params);
};
