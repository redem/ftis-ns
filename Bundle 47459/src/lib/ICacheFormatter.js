/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Lib = WTax.Lib || {};

WTax.Lib.CacheFormatter = function _CacheFormatter() {
};

WTax.Lib.CacheFormatter.prototype.convertToObject = function _convertToObject(stringValue) {
    return stringValue;
};

WTax.Lib.CacheFormatter.prototype.convertToText = function _convertToText(objValue) {
    return objValue;
};

WTax.Lib.CacheFormatter.prototype.convertName = function _convertName(objName) {
    return objName;
};

