/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.OrderedHash = (function () {
    function OrderedHash(keyName, elements) {
        var orderedHash = this;

        orderedHash.keyName = keyName;
        orderedHash.keys = [];
        orderedHash.length = 0;

        (elements || []).forEach(function (element) {
            orderedHash.push(element);
        });
    }


    OrderedHash.prototype.push = function push(element) {
        var key = element[this.keyName];
        key = key.toString();

        if (!key) {
            throw new Error('Error in pushing ' + element + ' to the ordered hash.  It has no ' + this.keyName + '.');
        }

        if (this.keys.indexOf(key) > -1) {
            throw new Error('Error in pushing ' + element + ' to the ordered hash.  An element with the same ' + this.keyName + ' already exists.');
        }

        this.keys.push(key);
        this[key] = element;
        this.length++;
    };


    OrderedHash.prototype.getFirst = function getFirst() {
        return this[this.keys[0]];
    };


    OrderedHash.prototype.getLast = function getLast() {
        return this[this.keys[this.keys.length - 1]];
    };


    OrderedHash.prototype.forEach = function forEach(callback) {
        var orderedHash = this;
        orderedHash.keys.forEach(function (id, index) {
            callback(orderedHash[id], index);
        });
    };


    OrderedHash.prototype.map = function map(callback) {
        var results = [];

        this.forEach(function (element) {
            results.push(callback(element));
        });

        return results;
    };


    OrderedHash.prototype.filter = function filter(callback) {
        var filtered = new OrderedHash(this.keyName);

        this.forEach(function (element) {
            if (callback(element)) {
                filtered.push(element);
            }
        });

        return filtered;
    };


    OrderedHash.prototype.toArray = function toArray() {
        return this.map(function (element) { return element; });
    };


    OrderedHash.prototype.every = function every(callback) {
        return this.toArray().every(callback);
    };


    OrderedHash.prototype.some = function some(callback) {
        return this.toArray().some(callback);
    };


    OrderedHash.prototype.reduce = function reduce(callback, initialValue) {
        if (arguments.length !== 2) {
            throw new Error('OrderedHash.prototype.reduce requires the second initialValue argument');
        }
        return Array.prototype.reduce.call(this.toArray(), callback, initialValue);
    };


    return OrderedHash;
}());
