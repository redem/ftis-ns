/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};


WTax.WTaxError = function() {
    return {
        code: '',
        details: '',
        stackTrace: '',
        context: '',
        level: '',
        logTitle: '',
        logDetails: '',
        isWTaxError: true
    };
};


WTax.ErrorHandler = function() {
    this.LEVEL = {
        DEBUG: 'DEBUG',
        AUDIT: 'AUDIT',
        ERROR: 'ERROR',
        EMERGENCY: 'EMERGENCY'
    };
};


WTax.ErrorHandler.prototype.throwError = function(error, context, level) {
    var wtaxError = this.convertError(error, context, level);
    this.logError(wtaxError);
    throw error;
};


WTax.ErrorHandler.prototype.logError = function(error, context, level) {
    var wtaxError = error.isWTaxError ? error : this.convertError(error, context, level);
    nlapiLogExecution(wtaxError.level, wtaxError.logTitle, wtaxError.logDetails);
};


WTax.ErrorHandler.prototype.convertError = function(error, context, level) {
    var wtaxError = new WTax.WTaxError();
    wtaxError.code = error.getCode  ? error.getCode() : error.code || error.name;
    wtaxError.details = error.getDetails ? error.getDetails() : error.message;
    wtaxError.stackTrace = error.getStackTrace ? error.getStackTrace() : '';
    wtaxError.context = context || '';
    wtaxError.level = level || this.LEVEL.DEBUG;
    wtaxError.logTitle = '[' + wtaxError.context + '] ' + wtaxError.code;
    wtaxError.logDetails = wtaxError.details + '\n' + wtaxError.stackTrace;
    
    return wtaxError;
};


WTax.ErrorHandler.prototype.toString = function(error) {
    var wtaxError = error.isWTaxError ? error : this.convertError(error);
    return wtaxError.code + ': ' + wtaxError.details;
};


var WTaxError = new WTax.ErrorHandler();
