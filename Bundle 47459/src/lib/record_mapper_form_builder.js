/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.RecordMapperFormBuilder = (function () {
    function RecordMapperFormBuilder(recordMapper, request, headerHidden, resourceObject) {
    	this.resourceObject = resourceObject;
    	
        this.id = request.getParameter('custpage_id');
        this.isNew = this.id === 'new';
        var newLabel = '';
        if (this.isNew) {
        	var attributes = {
        			1: recordMapper.formTitle 
        	};
        	newLabel = _4601.renderTranslation(resourceObject.RECORD['field'].NEW.label, attributes); //New &1
        }
        var title = this.isNew ? newLabel : recordMapper.formTitle; 

        this.recordMapper = recordMapper;
        this.request = request;
        this.nlobjForm = nlapiCreateForm(title, headerHidden);
    }


    RecordMapperFormBuilder.prototype.build = function build(options) {
        options = options || {};
        this.addButtons();
        this.addHiddenIdField();
        this.addMappingFields(options.fieldModifiers);
        var currentValues = this.getCurrentValues();
        this.nlobjForm.setFieldValues(currentValues);
        this.nlobjForm.setScript(options.scriptId);
        this.showValidationErrors();
        if (options.formModifier) {
            options.formModifier(this.nlobjForm, currentValues);
        }
    };

    RecordMapperFormBuilder.prototype.addButtons = function addButtons() {
        this.nlobjForm.addSubmitButton(this.resourceObject.RECORD['button'].SAVE.label); //Save
        this.nlobjForm.addButton('cancel', this.resourceObject.RECORD['button'].CANCEL.label, "setWindowChanged(window, false); history.back();"); //Cancel
        if (!this.isNew) {
            this.nlobjForm.addField('custpage_delete', 'text').setDisplayType('hidden');
            this.nlobjForm.addButton('delete', this.resourceObject.RECORD['button'].DELETE.label, [ //Delete
                "var isinited = ((window.isinited == null) || (window.isinited == undefined)) ? NS.form.isInited() : window.isinited;",
                "var isvalid = ((window.isvalid == null) || (window.isvalid == undefined)) ? NS.form.isValid() : window.isvalid;",
                "var form = document.forms['main_form'];",
                "if (isinited && isvalid",
                        "&& confirm('" + this.resourceObject.RECORD['validation_msg'].DELETE_RECORD.message + "')", //Are you sure you want to delete this record?
                        "&& (form.custpage_delete.value = 'Delete')",
                        "&& save_record(true)",
                        "&& form.submit()) {",
                    "return true;",
                "}"
            ].join(' '));
        }
    };


    RecordMapperFormBuilder.prototype.addHiddenIdField = function addHiddenIdField() {
        this.nlobjForm.addField('custpage_id', 'text')
            .setDisplayType('hidden')
            .setDefaultValue(this.id);
        
        this.nlobjForm.addField('custpage_resobj', 'longtext')
        	.setDisplayType('hidden')
        	.setDefaultValue(JSON.stringify(this.resourceObject));
    };


    RecordMapperFormBuilder.prototype.addMappingFields = function addMappingFields(fieldModifiers) {
        fieldModifiers = fieldModifiers || {};
        var formBuilder = this;
        formBuilder.recordMapper.mappings.forEach(function (mapping) {
            if (!mapping.excludeInForm) {
                var fieldType = mapping.as || mapping.type;
                var field = formBuilder.nlobjForm.addField(mapping.field, fieldType, mapping.label, mapping.source);

                if (fieldType === 'select' && Array.isArray(mapping.selectOptions)) {
                    mapping.selectOptions.forEach(function (selectOption) {
                        field.addSelectOption.apply(field, selectOption);
                    });
                }
                
                if (mapping.help) {
                	field.setHelpText(mapping.help);
                }

                if (mapping.layoutType) {
                    field.setLayoutType.apply(field, mapping.layoutType);
                }

                if (mapping.mandatory) {
                    field.setMandatory(true);
                }

                if (fieldModifiers[mapping.name]) {
                    fieldModifiers[mapping.name](field);
                }
            }
        });
    };


    RecordMapperFormBuilder.prototype.getCurrentValues = function getCurrentValues() {
        var formBuilder = this;
        var values = {custpage_id: this.id};
        var record = this.id !== 'new'
            ? nlapiLoadRecord(this.recordMapper.recordType, this.id)
            : {getFieldValue: function () {}};

        this.recordMapper.mappings.forEach(function (mapping) {
            var value = formBuilder.request.getParameter(mapping.field) || record.getFieldValue(mapping.field);
            if (value) {
                values[mapping.field] = value;
            }
        });

        return values;
    };


    RecordMapperFormBuilder.prototype.showValidationErrors = function showValidationErrors() {
        var validationErrors = JSON.parse(decodeURIComponent(this.request.getParameter('custpage_errors') || '[]'));

        if (validationErrors.length) {
            var html = "<div id='custpage_alert'></div><script>showAlertBox('custpage_alert', '" + this.resourceObject.RECORD['error_msg'].PROB_FOUND.message + "', '?', NLAlertDialog.TYPE_MEDIUM_PRIORITY);</script>" //Problems were found
                .replace('?', '<ul>' + validationErrors.map(function (validationError) {
                    return '<li>' + validationError.replace(/'/g, "\\'") + '</li>';
                }).join('') + '</ul>');

            this.nlobjForm.addField('custpage_alertfield', 'inlinehtml')
                .setLayoutType('outsideabove', 'startrow')
                .setDefaultValue(html);
        }
    };



    return RecordMapperFormBuilder;
}());
