/**
 * Copyright 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.BaseValidator = function BaseValidator() {
	this.name = '';
	this.stopLine = false;
	this.errorHandlers = [];
};

WTax.BaseValidator.prototype.getValidationFields = function getValidationFields(groupName) {
	var context = nlapiGetContext().getExecutionContext();
	var fields = {
		lineFields: new WTax.DAO.ValidationFieldsDao().getValidationFields({executionContext: context, group: groupName, fieldPosition: 'all,item,expense'}),
		footerFields: new WTax.DAO.ValidationFieldsDao().getValidationFields({executionContext: context, group: groupName, fieldPosition: 'footer'})
//		headerFields: new WTax.DAO.ValidationFieldsDao().getValidationFields({executionContext: context, group: groupName, fieldPosition: 'header'})
	};
	return fields;
};

WTax.BaseValidator.prototype.getValidationRules = function getValidationRules(fields) {
	//TODO: Rules should only be instantiated if used by fields
	var rules = {};
	for (var rule in WTax.Rule) {
		rules[rule] = new WTax.Rule[rule]();
	}
	return rules;
};

WTax.BaseValidator.prototype.notifyErrorHandlers = function notifyErrorHandlers(error) {
	for (var handler in this.errorHandlers) {
		this.errorHandlers[handler].handleError(error);
	}
};

WTax.BaseValidator.prototype.validate = function validate(form) {
	// Implement in subclass
};

WTax.BaseValidator.prototype.validateHeaderFields = function validateHeaderFields(form) {
	var headerFields = this.fields.headerFields;

	for (var f in headerFields) {
		var field = headerFields[f];
		field.value = form.getFieldValue(f);

		if (!this.validateField(field, form)) {
			return false;
		}
	}
	return true;
};

WTax.BaseValidator.prototype.validateSublistFields = function validateSublistFields(form, sublist) {
	var lineFields = this.fields.lineFields;
	var lineCount = form.getLineItemCount(sublist);

	for (var line = 1; line <= lineCount; line++) {
		for (var f in lineFields) {
			var field = lineFields[f];

			if (field.fieldPosition !== 'all' && field.fieldPosition !== sublist) {
				continue;
			}

			//Set current line/sublist
			field.line = line;
			field.sublist = sublist;
			field.value = form.getLineItemValue(sublist, f, line);
			if (!this.validateField(field, form)) {
				return false;
			}

			if (this.stopLine) {
				this.stopLine = false;
				break;
			}
		}
	}
	return true;
};

WTax.BaseValidator.prototype.validateFooterFields = function validateFooterFields(form) {
	var footerFields = this.fields.footerFields;

	for (var f in footerFields) {
		var field = footerFields[f];
		if (!this.validateField(field, form)) {
			return false;
		}
	}
	return true;
};

WTax.BaseValidator.prototype.validateField = function validateField(field, form) {
	for (var r in field.rules){
		var rule = field.rules[r];
		if (!this.rules[rule].run(field, form)) {
			this.notifyErrorHandlers(this.rules[rule].error);
			return false;
		}

		if (this.rules[rule].stopLine) {
			this.stopLine = true;
		}

		if (this.rules[rule].stopRule) {
			break;
		}
	}
	return true;
};

WTax.BaseValidator.prototype.addField = function addField(field) {
	if (!field) {
		throw nlapiCreateError('WTAX_FLD_REQD', 'field is required');
	}
	this.fields[field.id] = field;
};

WTax.BaseValidator.prototype.addRule = function addRule(rule) {
	if (!rule) {
		throw nlapiCreateError('WTAX_FLD_REQD', 'rule is required');
	}
	var ruleInstance = new rule();
	this.rules[ruleInstance.name] = ruleInstance;
};

WTax.BaseValidator.prototype.registerErrorHandler = function registerErrorHandler(errorHandler) {
	if (!errorHandler) {
		throw nlapiCreateError('WTAX_FLD_REQD', 'errorHandler is required');
	}
	this.errorHandlers.push(errorHandler);
};

//Validator specific
WTax.Validator = WTax.Validator || {};
WTax.Validator.AccrualUE = function AccrualUE() {
	WTax.BaseValidator.call(this);
	this.name = 'WTaxAccrualUE';
	this.fields = this.getValidationFields(this.name);
	this.rules = this.getValidationRules();
};
WTax.Validator.AccrualUE.prototype = Object.create(WTax.BaseValidator.prototype);

WTax.Validator.AccrualUE.prototype.validate = function validate(form) {
	this.validateSublistFields(form, 'item');
	this.validateSublistFields(form, 'expense');
	this.validateFooterFields(form);
};

//ValidatorFieldsDao
WTax.DAO = WTax.DAO || {};
WTax.DAO.ValidationFields = function _ValidationFields() {
	return {
		id: '',
		sequence: '',
		rules : '',
		executionContext: '',
		fieldPosition: ''
	};
};

WTax.DAO.ValidationFieldsDao = function _ValidationFieldsDao() {
	try {
		var searchColumns = [
			'custrecord_4601_vf_id',
			'custrecord_4601_vf_rules',
			'custrecord_4601_vf_fieldposition'
		];

		this.columns = [];
		this.columns.push(new nlobjSearchColumn('custrecord_4601_vf_sequence').setSort());
		for (var d = 0; d < searchColumns.length; d++){
			this.columns.push(new nlobjSearchColumn(searchColumns[d]));
		}

	} catch (ex) {
		WTaxError.throwError(ex, 'WTax.DAO.ValidationFieldsDao', WTaxError.LEVEL.ERROR);
	}
};

WTax.DAO.ValidationFieldsDao.prototype.setFilters = function _setFilters(filters) {
	this.filters = [];
	this.filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	this.filters.push(new nlobjSearchFilter('custrecord_4601_vf_executioncontext', null, 'contains', filters.executionContext));
	this.filters.push(new nlobjSearchFilter('custrecord_4601_vf_group', null, 'is', filters.group));
	var fieldPosition = filters.fieldPosition.split(',');

	for (var pos = 0; pos < fieldPosition.length; pos++) {
		var openParens = pos === 0 ? 1 : 0;
		var closeParens = pos === fieldPosition.length -1 ? 1: 0;
		this.filters.push(new nlobjSearchFilter('custrecord_4601_vf_fieldposition', null, 'is', fieldPosition[pos], null, openParens, closeParens, true));
	}
};

WTax.DAO.ValidationFieldsDao.prototype.getList = function _getList() {
	var list = {};

	try {
		var rawList = nlapiSearchRecord('customrecord_4601_validationfields', null, this.filters, this.columns);
		var rawLength = rawList ? rawList.length : 0;
		for (var i = 0; i < rawLength; i++) {
			var convertedObject = this.convertToObject(rawList[i]);
			list[convertedObject.id] = convertedObject;
		}
	} catch (ex) {
		WTaxError.throwError(ex, 'WTax.DAO.ValidationFieldsDao.getList', WTaxError.LEVEL.ERROR);
	}

	return list;
};

WTax.DAO.ValidationFieldsDao.prototype.convertToObject = function _convertToObject(row) {
	var validationField = new WTax.DAO.ValidationFields();

	try {
		validationField.id = row.getValue('custrecord_4601_vf_id');
		validationField.sequence = row.getValue('custrecord_4601_vf_sequence');
		validationField.rules = row.getValue('custrecord_4601_vf_rules').split('|');
		validationField.fieldPosition = row.getValue('custrecord_4601_vf_fieldposition');
	} catch (ex) {
		WTaxError.throwError(ex, 'WTax.DAO.ValidationFieldsDao.convertToObject', WTaxError.LEVEL.ERROR);
	}

	return validationField;
};

WTax.DAO.ValidationFieldsDao.prototype.getValidationFields = function _getValidationFields(filters) {
	try {
		this.setFilters(filters);
		return this.getList();
	} catch (ex) {
		WTaxError.throwError(ex, 'WTax.DAO.ValidationFieldsDao.getByContext', WTaxError.LEVEL.ERROR);
	}
};

//Generic ErrorHandler
WTax.Validator.ErrorHandler = function(){};
WTax.Validator.ErrorHandler.prototype.handleError = function(error) {
	var nsError = nlapiCreateError(error.code, error.message);
	WTaxError.throwError(nsError, error.context, error.level);
};