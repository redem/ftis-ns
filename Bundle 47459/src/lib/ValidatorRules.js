/**
 * Copyright 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Rule = WTax.Rule || {};

WTax.Rule.BaseRule = function BaseRule() {
	this.error = {code: '', message: '', context: '', level: ''};
	this.stopRule = false;
	this.stopLine = false;
};

WTax.Rule.BaseRule.prototype.run = function(field, form) {
	return true;
};

WTax.Rule.BaseRule.prototype.createError = function(params) {
	var raw = this.error.message;
	for (var param in params) {
		var pattern = new RegExp("{" + param + "}", "g");
		raw = raw.replace(pattern, String(params[param]).replace(/\$/gm, "$$$"));
	}
	this.error.message = raw;
};

WTax.Rule.Required = function Required() {
	WTax.Rule.BaseRule.call(this);
	this.error = {code: 'WTAX_FLD_REQD',
				  message: 'Please enter value(s) for {id} on line {line}',
				  context: 'WTax.Rule.Required',
				  level: 'ERROR'};
};
WTax.Rule.Required.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.Required.prototype.run = function(field) {
	if (!field.value) {
		this.createError({id: field.id, line: field.line});
		return false;
	}
	return true;
};

WTax.Rule.RequiredNumber = function RequiredNumber() {
	WTax.Rule.BaseRule.call(this);
	this.error = {code: 'WTAX_FLD_REQD',
				  message: 'Please enter value(s) for {id} on line {line}',
				  context: 'WTax.Rule.RequiredNumber',
				  level: 'ERROR'};
};
WTax.Rule.RequiredNumber.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.RequiredNumber.prototype.run = function(field) {
	var value = field.value;
	if (typeof(value) === 'undefined' || value === null || value === '' || isNaN(value)) {
		this.createError({id: field.id, line: field.line});
		return false;
	}
	return true;
};

WTax.Rule.RequiredPercent = function RequiredPercent() {
	WTax.Rule.BaseRule.call(this);
	this.error = {code: 'WTAX_FLD_REQD',
				  message: 'Please enter value(s) for {id} on line {line}.',
				  context: 'WTax.Rule.RequiredPercent',
				  level: 'ERROR'};
};
WTax.Rule.RequiredPercent.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.RequiredPercent.prototype.run = function(field) {
	var value = parseFloat(field.value.toString().replace(/%$/, ''));
	if (isNaN(value)) {
		this.createError({id: field.id, line: field.line});
		return false;
	}
	return true;
};

WTax.Rule.Sign = function Sign() {
	WTax.Rule.BaseRule.call(this);
	this.error = {
		code: 'WTAX_INCORRECT_SIGN',
		message: 'You have entered an Invalid Field Value {value} for the following field: {id} on line {line}.',
		context: 'WTax.Rule.Sign',
		level: 'ERROR'
	};
	this.tranType = '';
};
WTax.Rule.Sign.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.Sign.prototype.run = function(field, form) {
	var isValid = false;
	var isBase = (field.id === 'custcol_4601_witaxbamt_exp' || field.id === 'custcol_4601_witaxbaseamount') ? true : false;

	this.tranType = this.tranType || getTranType(form.getRecordType());
	var tranAmount = parseFloat(form.getLineItemValue(field.sublist, 'amount', field.line) || 0);

	if (tranAmount === 0) {
		isValid = zeroTransaction(isBase, field.value);
	} else if (tranAmount > 0) {
		isValid = positiveTransaction(this.tranType, isBase, field.value);
	} else {
		isValid = negativeTransaction(this.tranType, isBase, field.value);
	}

	if (!isValid) {
		this.createError({line: field.line, id: field.id, value: field.value});
		return false;
	}
	return true;

	function getTranType(recordType) {
		return ['check', 'expensereport', 'purchaseorder', 'vendorbill', 'vendorpayment'].indexOf(recordType) > -1 ? 'purc' : 'sale';
	}

	function zeroTransaction(isBase, amount) {
		if (isBase && (amount >= 0 || amount <= 0)) {
			return true;
		} else if (!isBase && (amount <= 0 || amount >= 0)) {
			return true;
		}
	}

	function positiveTransaction(tranType, isBase, amount) {
		if (tranType === 'sale') {
			if ((isBase && amount <= 0) || (!isBase && amount >=0)) {
				return true;
			}
		} else if (tranType === 'purc') {
			if ((isBase && amount >= 0) || (!isBase && amount <= 0)) {
				return true;
			}
		}
	}

	function negativeTransaction(tranType, isBase, amount) {
		if (tranType === 'sale') {
			if ((isBase && amount >= 0) || (!isBase && amount <=0)) {
				return true;
			}
		} else if (tranType === 'purc') {
			if ((isBase && amount <= 0) || (!isBase && amount >= 0)) {
				return true;
			}
		}
	}
};

WTax.Rule.NoHeaderDiscount = function NoHeaderDiscount() {
	WTax.Rule.BaseRule.call(this);
	this.error = {
		code: 'WTAX_HEADERDISC_NOT_ALLOWED',
		message: 'Applying a transaction discount is not supported when the withholding tax point is set to On Accrual.',
		context: 'WTax.Rule.NoHeaderDiscount',
		level: 'ERROR'
	};
};
WTax.Rule.NoHeaderDiscount.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.NoHeaderDiscount.prototype.run = function(field, form) {
	var hasDiscount = form.getFieldValue('discountitem');
	if (field.value === 'T' && hasDiscount) {
		return false;
	}
	return true;
};

WTax.Rule.EntityVendor = function EntityVendor() {
	WTax.Rule.BaseRule.call(this);
	this.error = {
		code: 'WTAX_CHEQUE_CUSTOMER_NOT_ALLOWED',
		message: 'You cannot select a customer on a cheque transaction with withholding tax.',
		context: 'WTax.Rule.EntityVendor',
		level: 'ERROR'
	};
	this.entityType = null;
	this.transactionType = null;
};
WTax.Rule.EntityVendor.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.EntityVendor.prototype.run = function(field, form) {
	this.entityType = this.entityType || getEntityType(form.getFieldValue('entity'));
	this.transactionType = this.transactionType || form.getRecordType();

	if (this.transactionType === 'check') {
		if (this.entityType !== 'vendor') {
			return false;
		}
	}
	return true;

	function getEntityType(id) {
		var sr = nlapiSearchRecord('entity', null, new nlobjSearchFilter('internalid', null, 'is', id));
		if (sr && sr.length > 0) {
			return sr[0].getRecordType();
		}
	}
};

WTax.Rule.ApplyWTax = function ApplyWTax() {
	WTax.Rule.BaseRule.call(this);
	this.error = {
		code: 'WTAX_APPLY_ERROR',
		message: 'Check the Apply WH Tax? box to line {line} to apply withholding tax.',
		context: 'WTax.Rule.ApplyWTax',
		level: 'ERROR'
	};
};
WTax.Rule.ApplyWTax.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.ApplyWTax.prototype.run = function(field, form) {
	var sublist = field.sublist;
	var line = field.line;
	var wtaxCode = sublist === 'item' ? 'custcol_4601_witaxcode' : 'custcol_4601_witaxcode_exp';
	var wtaxRate = sublist === 'item' ? 'custcol_4601_witaxrate' : 'custcol_4601_witaxrate_exp';
	var wtaxBase = sublist === 'item' ? 'custcol_4601_witaxbaseamount' : 'custcol_4601_witaxbamt_exp';
	var wtaxAmount = sublist === 'item' ? 'custcol_4601_witaxamount' : 'custcol_4601_witaxamt_exp';

	if (field.value !== 'T') {
		if (form.getLineItemValue(sublist, wtaxCode, line) ||
			form.getLineItemValue(sublist, wtaxRate, line) ||
			form.getLineItemValue(sublist, wtaxBase, line) ||
			form.getLineItemValue(sublist, wtaxAmount, line)) {
			this.createError({line: field.line});
			return false;
		}
		this.stopRule = true;
		this.stopLine = true;
	}
	return true;
};

WTax.Rule.TotalWTaxAmount = function TotalWTaxAmount() {
	WTax.Rule.BaseRule.call(this);
	this.error = {
		code: 'WTAX_TOTAL_VALIDATION',
		message: 'The total Withholding Tax Amount for all Withholding Tax Types cannot be negative. This includes the breakdown of amounts for all applicable Withholding Tax Groups applied on the transaction.',
		context: 'WTax.Rule.TotalWTaxAmount',
		level: 'ERROR'
	};
	this.tranType = null;
	this.wtaxCodes = null;
	this.wtaxAmounts = {};
};

WTax.Rule.TotalWTaxAmount.prototype = Object.create(WTax.Rule.BaseRule.prototype);

WTax.Rule.TotalWTaxAmount.prototype.run = function(field, form) {
	this.wtaxCodes = this.wtaxCodes || getWTaxCodes(form.getFieldValue('nexus'));
	if (field.id == 'wtax_total_amount') {
		if (Object.keys(this.wtaxAmounts).length > 0) {
			this.tranType = this.tranType || getTranType(form.getRecordType());
			for (var taxType in this.wtaxAmounts) {
				var totalWtaxAmount = this.wtaxAmounts[taxType];
				if ((this.tranType === 'sale' && totalWtaxAmount < 0) || (this.tranType === 'purc' && totalWtaxAmount > 0)) {
					this.createError({line: field.line});
					return false;
				}
			}
		}
	} else {
		var taxCode = form.getLineItemValue(field.sublist, field.sublist === 'item' ? 'custcol_4601_witaxcode' : 'custcol_4601_witaxcode_exp', field.line);
		var taxAmount = form.getLineItemValue(field.sublist, field.id, field.line);

		var currentAmount = parseFloat(this.wtaxAmounts[this.wtaxCodes[taxCode]]) || 0;
		currentAmount += parseFloat(taxAmount);
		this.wtaxAmounts[this.wtaxCodes[taxCode]] = currentAmount;
	}
	return true;

	function getTranType(recordType) {
		return ['check', 'expensereport', 'purchaseorder', 'vendorbill', 'vendorpayment'].indexOf(recordType) > -1 ? 'purc' : 'sale';
	}

	function getWTaxCodes(nexus) {
		var wtaxSetups = getWTaxSetupsByNexus(nexus);
		var wtaxTypes = getWTaxTypesBySetups(wtaxSetups);
		var taxCodes = {};
		var filters = new nlobjSearchFilter('custrecord_4601_wtc_witaxtype', null, 'anyof', wtaxTypes);
		var columns = new nlobjSearchColumn('custrecord_4601_wtc_witaxtype');
		var sr = nlapiSearchRecord('customrecord_4601_witaxcode', null, filters, columns);
		for (var isr = 0; sr && isr < sr.length; isr++) {
			taxCodes[sr[isr].getId()] = sr[isr].getValue('custrecord_4601_wtc_witaxtype');
		}
		return taxCodes;
	}

	function getWTaxSetupsByNexus(nexus) {
		var setups = [];
		var sr = nlapiSearchRecord('customrecord_4601_witaxsetup', null, new nlobjSearchFilter('custrecord_4601_wts_nexus', null, 'equalto', nexus));
		for (var isr = 0; sr && isr < sr.length; isr++) {
			setups.push(sr[isr].getId());
		}
		return setups;
	}

	function getWTaxTypesBySetups(setups) {
		var taxTypes = [];
		var sr = nlapiSearchRecord('customrecord_4601_witaxtype', null, new nlobjSearchFilter('custrecord_4601_wtt_witaxsetup', null, 'anyof', setups));
		for (var isr = 0; sr && isr < sr.length; isr++) {
			taxTypes.push(sr[isr].getId());
		}
		return taxTypes;
	}
};
