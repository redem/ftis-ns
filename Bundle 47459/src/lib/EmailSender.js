/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.EmailSender = function _EmailSender() {
};

WTax.EmailSender.prototype.sendEmail = function _sendEmail(emailObj) {

    if (!emailObj) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'emailObj is null or undefined');
    }

    if (!emailObj.sender) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'emailObj.sender is null or undefined');
    }

    if (!emailObj.recipient) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'emailObj.recipient is null or undefined');
    }

    if (!emailObj.subject) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'emailObj.subject is null or undefined');
    }

    try {
    	nlapiLogExecution('debug', 'sending email', JSON.stringify(emailObj));
        nlapiSendEmail(emailObj.sender, emailObj.recipient, emailObj.subject, emailObj.body);
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.EmailSender.sendEmail', WTaxError.LEVEL.ERROR);
    }

};
