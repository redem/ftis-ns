/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Lib = WTax.Lib || {};

WTax.Lib.CacheManager = function _CacheManager(formatter) {
    this.RECORD_NAME = 'customrecord_wtax_cache';
    this.FIELD = {
        NAME : 'name',
        PROPERTIES : 'custrecord_cache_properties'
    };

    this.formatter = formatter;    
};

WTax.Lib.CacheManager.prototype.create = function _create(name, value) {
    var id = -1;
    
    try {
        var cacheRecord = nlapiCreateRecord(this.RECORD_NAME);
        
        var cacheName = name;
        var cacheValue = value;
        
        if (this.formatter) {
            cacheName = this.formatter.convertName(name);
            cacheValue = this.formatter.convertToText(value);
        }
        
        cacheRecord.setFieldValue(this.FIELD.NAME, cacheName);
        cacheRecord.setFieldValue(this.FIELD.PROPERTIES, cacheValue);
        id = nlapiSubmitRecord(cacheRecord);        
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.CacheManager.create', ex.toString());
        throw nlapiCreateError('WTAX_CACHE_ERROR', 'Error in creating cache');
    }
    
    return id;
};

WTax.Lib.CacheManager.prototype.update = function _update(cacheId, value) {
    var id = -1;
    
    try {
        var cacheRecord = nlapiLoadRecord(this.RECORD_NAME, cacheId);
        var cacheValue = value;
        
        if (this.formatter) {
            cacheValue = this.formatter.convertToText(value);
        }
        
        cacheRecord.setFieldValue(this.FIELD.PROPERTIES, cacheValue);
        id = nlapiSubmitRecord(cacheRecord);        
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.CacheManager.update', ex.toString());
        throw nlapiCreateError('WTAX_CACHE_ERROR', 'Error in updating cache');
    }
    
    return id;
};

WTax.Lib.CacheManager.prototype.getPropertiesById = function _getPropertiesById(cacheId) {
    var value = {};
    
    try {
        var cacheRecord = nlapiLoadRecord(this.RECORD_NAME, cacheId);
        value = cacheRecord.getFieldValue(this.FIELD.PROPERTIES);
        
        if (this.formatter) {
            value = this.formatter.convertToObject(value);
        }
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.CacheManager.getPropertiesById', ex.toString());
        throw nlapiCreateError('WTAX_CACHE_ERROR', 'Error in retrieving cache');
    }
    
    return value;
};

WTax.Lib.CacheManager.prototype.getIdByName = function _getIdByName(name) {
    var id = -1;
    
    try {
        var cacheName = name;
        
        if (this.formatter) {
            cacheName = this.formatter.convertName(name);
        }
        
        var cacheRecord = nlapiSearchRecord(this.RECORD_NAME, null, new nlobjSearchFilter(this.FIELD.NAME, null, 'is', cacheName));
        if (cacheRecord) {
            id = cacheRecord[0].getId();            
        }
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.CacheManager.getIdByName', ex.toString());
        throw nlapiCreateError('WTAX_CACHE_ERROR', 'Error in retrieving cache');
    }
    
    return id;
};

WTax.Lib.CacheManager.prototype.remove = function _remove(cacheId) {
    try {
        nlapiDeleteRecord(this.RECORD_NAME, cacheId);
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.CacheManager.remove', ex.toString());
        throw nlapiCreateError('WTAX_CACHE_ERROR', 'Error in deleting cache');
    } 
};
