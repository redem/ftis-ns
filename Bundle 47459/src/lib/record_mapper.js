/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.RecordMapper = (function () {
    function RecordMapper(recordType, options) {
        this.recordType = recordType;
        options = options || {};
        this.prefix = options.prefix;
        this.formTitle = options.formTitle;
        this.listTitle = options.listTitle;
        this.suiteletId = options.suiteletId;
        this.suiteletDeploymentId = options.suiteletDeploymentId;
        this.mappings = new _4601.OrderedHash('name');
    }


    RecordMapper.prototype.addMapping = function addMapping(name, options) {
        options = options || {};
        var recordMapper = this;
        var filter = options.filter || (recordMapper.prefix || '') + name;
        var column = options.column || (recordMapper.prefix || '') + name;
        var field = options.field || (recordMapper.prefix || '') + name;
        var maxIdLength = 40;

        [filter, column, field].forEach(function (id) {
            id.split('.').forEach(function (id) {
                if (id.length > maxIdLength) {
                    throw new Error(id + ' is more than ' + maxIdLength + ' characters long.');
                }
            });
        });

        recordMapper.mappings.push({
            name: name,
            type: options.type || 'text',
            label: options.label || name,
            filter: filter,
            column: column,
            field: field,
            excludeInForm: options.excludeInForm,
            showInList: options.showInList,
            selectOptions: options.selectOptions,
            source: options.source,
            layoutType: options.layoutType,
            mandatory: options.mandatory
        });

        return recordMapper;
    };


    RecordMapper.prototype.mapFromResult = function mapFromResult(res) {
        var id = res.getId().toString();
        var mappedResult = {id: id};
        var columns = {};

        (res.getAllColumns() || []).forEach(function (col) {
            var join = col.getJoin();
            columns[(join ? join + '.' : '') + col.getName()] = col;
        });

        this.mappings.forEach(function (mapping) {
            var col = columns[mapping.column];

            if (col) {
                var value = res.getValue(col);
                var text = res.getText(col);
                mappedResult[mapping.name] = RecordMapper.mapValueFromNetsuite(value, mapping.type);
                if (text) { mappedResult[mapping.name + '_text'] = text; }
            }
        });

        return mappedResult;
    };


    RecordMapper.prototype.mapFromRecord = function mapFromRecord(rec) {
        var id = rec.getId().toString();
        var mappedRecord = {id: id};

        this.mappings.forEach(function (mapping) {
            var value = rec.getFieldValue(mapping.field);
            var text = rec.getFieldText(mapping.field);
            mappedRecord[mapping.name] = RecordMapper.mapValueFromNetsuite(value, mapping.type);
            if (text) { mappedRecord[mapping.name + '_text'] = text; }
        });

        return mappedRecord;
    };


    RecordMapper.prototype.mapFromRequest = function mapFromRequest(req) {
        var id = req.getParameter('custpage_id');
        var mappedRecord = {id: id};

        this.mappings.forEach(function (mapping) {
            var value = req.getParameter(mapping.field);
            mappedRecord[mapping.name] = RecordMapper.mapValueFromNetsuite(value, mapping.type);
        });

        return mappedRecord;
    };


    RecordMapper.mapValueFromNetsuite = function mapValueFromNetsuite(value, type) {
        switch (type) {
            case 'checkbox': value = value === 'T'; break;
            case 'percent':
            case 'float': value = parseFloat(value); break;
            case 'integer': value = parseInt(value, 10); break;
            case 'date': value = nlapiStringToDate(value); break;
            case 'multiselect':
                value = Array.isArray(value)
                    ? value
                    : (value || '')
                        .split(new RegExp(String.fromCharCode(5) + '|,'))
                        .filter(function (e) { return e; });
                break;
        }
        return value;
    };


    RecordMapper.mapValueToNetsuite = function mapValueToNetsuite(value, type) {
        switch (type) {
            case 'checkbox':
                value = value ? 'T' : 'F';
                break;

            case 'percent':
            case 'float':
            case 'integer':
                if (value === '' || isNaN(value)) {
                    value = null;
                }
                break;

            case 'date':
                if (value) {
                    value = nlapiDateToString(value);
                }
                break;
        }
        return value;
    };


    RecordMapper.prototype.all = function all(options) {
        options = options || {};
        var skipColumns = options.skipColumns || null;
        var recordMapper = this;
        var orderedHash = new _4601.OrderedHash('id');
        var filters = (options.filters || []).map(function (filterArgs) {
            var mappingName = filterArgs[0];
            var name = recordMapper.mappings[mappingName]
                    ? recordMapper.mappings[mappingName].filter
                    : mappingName;
            var join = null;
            var parts = name.split('.');
            var operator = filterArgs[1];
            var value = filterArgs[2];
            var value2 = filterArgs[3];

            if (parts.length > 1) {
                join = parts[0];
                name = parts[1];
            }

            var filter = new nlobjSearchFilter(name, join, operator, value, value2);
            if (filterArgs[4]) {
                filter.setFormula(filterArgs[4]);
            }
            return filter;
        });
        
        // skipColumns is used to address the bundle update issue
        // wherein deprecated and new columns are not available yet as search columns
        if (skipColumns == null) {
        	var columns = recordMapper.mappings.filter(function (mapping) {
                return !options.columns || options.columns.indexOf(mapping.name) > -1;
            }).map(function (mapping) {
                var name = mapping.column;
                var parts = name.split('.');

                if (parts.length > 1) {
                    var join = parts[0];
                    name = parts[1];
                }

                return new nlobjSearchColumn(name, join);
            });
        }
        
        // sorts format: [<mappingName>, <order - true if desc, blank if asc>]
        // e.g. name DESC: ['name', 'true']
        // e.g. name ASC: ['name', '']
        //     or ['name']
        // sorting order follows the mappingName placements
        var sorts = (options.sorts || [])
        for (var i in sorts) {
        	var mappingName = sorts[i][0];
        	var name = recordMapper.mappings[mappingName]
                    ? recordMapper.mappings[mappingName].filter
                    : mappingName;
            var parts = name.split('.');
            var join = null;
            
            if (parts.length > 1) {
                join = parts[0];
                name = parts[1];
            }
        	var order = sorts[i][1];
        	var columnID = -1;
        	for (j in columns) {
        		var column = columns[j];
        		if (column.getName() == name && column.getJoin() == join) {
        			columnID = j;
        			break;
        		}
        	}
        	if (columnID >=0) {
        		columns[columnID].setSort(order);
        		if (sorts.length > 1 && columnID != i) {
        			var testSearchColumn = columns[i];
            		columns[i] = columns[columnID];
            		columns[columnID] = testSearchColumn;
        		}
        	}
        }
        
	    (nlapiSearchRecord(recordMapper.recordType, null, filters, columns) || []).forEach(function (res) {
            orderedHash.push(recordMapper.mapFromResult(res));
        });
        	
        return orderedHash;
    };


    RecordMapper.prototype.loadRecord = function loadRecord(id) {
        var recordMapper = this;
        return recordMapper.mapFromRecord(nlapiLoadRecord(recordMapper.recordType, id));
    };


    RecordMapper.prototype.getValidationErrors = function getValidationErrors() {
        return [];
    };


    RecordMapper.prototype.beforeSave = function beforeSave(mappedRecord) {};


    RecordMapper.prototype.saveRecord = function saveRecord(mappedRecord) {
        var recordMapper = this;

        recordMapper.beforeSave(mappedRecord);

        var record = mappedRecord.id === 'new'
            ? nlapiCreateRecord(recordMapper.recordType)
            : nlapiLoadRecord(recordMapper.recordType, mappedRecord.id);

        recordMapper.mappings.forEach(function (mapping) {
            if (typeof mappedRecord[mapping.name] !== 'undefined') {
                var value = RecordMapper.mapValueToNetsuite(mappedRecord[mapping.name], mapping.type);
                record.setFieldValue(mapping.field, value);
            }
        });

        mappedRecord.id = nlapiSubmitRecord(record);
        recordMapper.afterSave(mappedRecord);
        return mappedRecord.id;
    };


    RecordMapper.prototype.afterSave = function afterSave(mappedRecord) {};


    RecordMapper.prototype.deleteRecord = function deleteRecord(id) {
        var mappedRecord = this.loadRecord(id);
        nlapiDeleteRecord(this.recordType, id);
        this.afterDelete(mappedRecord);
    };


    RecordMapper.prototype.afterDelete = function afterDelete(mappedRecord) {};


    RecordMapper.prototype.lookupField = function lookupField(id, mappingName) {
        return nlapiLookupField(this.recordType, id, this.mappings[mappingName].field);
    };


    return RecordMapper;
}());
