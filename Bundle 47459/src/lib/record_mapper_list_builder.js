/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.RecordMapperListBuilder = (function () {
    function RecordMapperListBuilder(recordMapper, getUrl, headerHidden, resourceObject) {
        this.recordMapper = recordMapper;
        this.headerHidden = headerHidden;
        this.resourceObject = resourceObject;
        this.getUrl = getUrl;
        this.nlobjList = nlapiCreateList(recordMapper.listTitle, headerHidden);
    }


    RecordMapperListBuilder.prototype.build = function build(options) {
        options = options || {};

        if (!options.noNewButton) {
            this.addNewButton();
        }

        this.addColumns();
        this.addRows(options);
    };


    RecordMapperListBuilder.prototype.addNewButton = function addNewButton() {
        var url = this.getUrl({custpage_id: 'new'});
        var script = "document.location = '" + url + "';";
        this.nlobjList.addButton('new', this.resourceObject.RECORD['button'].NEW.label , script); //New
    };


    RecordMapperListBuilder.prototype.addColumns = function addColumns() {
        var listBuilder = this;

        var typeOverrides = {
            checkbox: 'text',
            select: 'text'
        };

        this.recordMapper.mappings.forEach(function (mapping) {
            if (mapping.showInList) {
                listBuilder.nlobjList.addColumn(mapping.name, typeOverrides[mapping.type] || mapping.type, mapping.label);
            }
        });
    };


    RecordMapperListBuilder.prototype.addRows = function addRows(options) {
        var listBuilder = this;
        listBuilder.nlobjList.addRows(listBuilder.recordMapper.all({filters: options.filters || []}).map(function (row) {
            listBuilder.recordMapper.mappings.forEach(function (mapping) {
                if (mapping.type === 'checkbox') {
                    row[mapping.name] = row[mapping.name] ? listBuilder.resourceObject.RECORD['field'].YES.label : listBuilder.resourceObject.RECORD['field'].NO.label ; //Yes : No
                } else {
                    var text = row[mapping.name + '_text'];
                    if (text) {
                        delete row[mapping.name + '_text'];
                        row[mapping.name] = text;
                    }
                }
            });

            if (options.rowModifier) {
                options.rowModifier(row);
            }

            return row;
        }));
    };


    return RecordMapperListBuilder;
}());
