/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.RecordMapperSuitelet = (function () {
    function RecordMapperSuitelet(recordMapper, request, response) {
        this.recordMapper = recordMapper;
        this.request = request;
        this.response = response;
        this.headerHidden = this.isHeaderHidden();
        this.ro = _4601.getResourceManager();
    }


    RecordMapperSuitelet.prototype.route = function route() {
        if (this.request.getMethod() === 'GET') {
            if (this.request.getParameter('custpage_id')) {
                this.displayForm();
            } else {
                this.displayList();
            }
        } else {
            if (this.request.getParameter('custpage_delete') === 'Delete') {
                this.processDelete();
            } else {
                this.processSave();
            }
        }
    };


    RecordMapperSuitelet.prototype.isHeaderHidden = function isHeaderHidden() {
        return this.request.getParameter('ifrmcntnr') === 'T';
    };


    RecordMapperSuitelet.prototype.displayList = function displayList() {
    	this.assignLabel('list');
        var listBuilder = new _4601.RecordMapperListBuilder(this.recordMapper, this.getUrl, this.headerHidden, this.ro);
        var options = this.getListBuilderOptions();
        listBuilder.build(options);
        this.response.writePage(listBuilder.nlobjList);
    };


    RecordMapperSuitelet.prototype.displayForm = function displayForm() {
    	this.assignLabel('form');
        var formBuilder = new _4601.RecordMapperFormBuilder(this.recordMapper, this.request, this.headerHidden, this.ro);
        var options = this.getFormBuilderOptions();
        formBuilder.build(options);
        this.response.writePage(formBuilder.nlobjForm);
    };


    RecordMapperSuitelet.prototype.processSave = function processSave() {
        var suitelet = this;
        var mappedRecord = suitelet.recordMapper.mapFromRequest(suitelet.request);
        var errors = suitelet.recordMapper.getValidationErrors(mappedRecord, suitelet.ro);
        var params = {};

        if (errors.length) {
            params.custpage_id = suitelet.request.getParameter('custpage_id');
            params.custpage_errors = encodeURIComponent(JSON.stringify(errors));

            suitelet.recordMapper.mappings.forEach(function (mapping) {
                params[mapping.field] = suitelet.request.getParameter(mapping.field);
            });

        } else {
            suitelet.recordMapper.saveRecord(mappedRecord);
        }

        suitelet.setRedirectUrl(params);
    };


    RecordMapperSuitelet.prototype.processDelete = function processDelete() {
        var id = this.request.getParameter('custpage_id');
        this.recordMapper.deleteRecord(id);
        this.setRedirectUrl();
    };


    RecordMapperSuitelet.prototype.getUrl = function getUrl(params) {
        params = params || {};
        var url = nlapiResolveURL('SUITELET', this.recordMapper.suiteletId, this.recordMapper.suiteletDeploymentId);

        if (this.headerHidden) {
            params.ifrmcntnr = 'T';
        }

        return url + Object.keys(params).map(function (p) {
            return '&' + p + '=' + params[p];
        }).join('');
    };


    RecordMapperSuitelet.prototype.setRedirectUrl = function setRedirectUrl(params) {
        params = params || {};
        nlapiSetRedirectURL('SUITELET', this.recordMapper.suiteletId, this.recordMapper.suiteletDeploymentId, null, params);
    };


    RecordMapperSuitelet.prototype.getListBuilderOptions = function getListBuilderOptions() {
        return {};
    };


    RecordMapperSuitelet.prototype.getFormBuilderOptions = function getFormBuilderOptions() {
        return {};
    };
    
    
    RecordMapperSuitelet.prototype.assignLabel = function assignLabel(type) {};


    return RecordMapperSuitelet;
}());
