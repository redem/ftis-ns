/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.DateFormatter = function DateFormatter() {};

/**
 * Converts a string date (usually from an NS field) into the standard format YYYY-MM-DD. 
 * @param {String} date - Date taken from an NS field.
 * @returns {String} Date in YYYY-MM-DD format.
 */
WTax.DateFormatter.prototype.stringToStandardFormat = function stringToStandardFormat(date) {
    var result = '';
    
    try {
        var dateObj = nlapiStringToDate(date);
        result = [dateObj.getFullYear(), dateObj.getMonth() + 1, dateObj.getDate()].join('-');
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DateFormatter.stringToStandardFormat', WTaxError.LEVEL.ERROR);
    }
    
    return result;
};

/**
 * Converts a string date in YYYY-MM-DD into a string date based on the NS user's preferred date format. 
 * @param {String} date - Date in YYYY-MM-DD format.
 * @returns {String} Date in the NS user's preferred date format.
 */
WTax.DateFormatter.prototype.standardFormatToString = function standardFormatToString(date) {
    var result = null;
    
    try {
        var dateArray = date.split('-');
        var dateObj = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
        result = nlapiDateToString(dateObj);
    } catch(e) {
        WTaxError.throwError(e, 'WTax.DateFormatter.standardFormatToString', WTaxError.LEVEL.ERROR);
    }
    
    return result;
};
