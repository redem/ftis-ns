/**
 * Copyright 2017 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.Constants = WTax.Constants || {};

WTax.Constants.APPLIES_TO = {
	'TOTAL_AMOUNT': '1',
	'INDIVIDUAL_LINE_ITEMS': '2'
};

WTax.Constants.OLD_APPLIES_TO = {
	'TOTAL_AMOUNT': 'T',
	'INDIVIDUAL_LINE_ITEMS': 'F'
};

WTax.Constants.AVAILABLE_ON = {
	'SALE': '1',
	'PURCHASE': '2'
};
