[{
		"id":"GLOBAL_TRY_CATCH",
		"title":"Try globale di cattura errore",
		"error_msg":[
			{
				"id":"TRIGGERED_BY_USER", "message":"Errore innescato da user:" 
			} 
		]
	},{
		"id":"GLOBAL_VALUES",
		"title":"Valori globali",
		"error_msg":[
			{
				"id":"CONTACT_NS_ADMIN", "message":"Per ulteriori informazioni, contattare l'amministratore Netsuite.",
				"id":"WARNING", "message":"Attenzione"
			} 
		]
	},{
		"id":"WTAX_SETUP",
		"title":"Configura imposta ritenuta alla fonte",
		"fields":[
			{
				"id":"APPLIES_TO", 
				"label":"Applica a",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Voci linea individuali"},
					{"id":"TOTAL_AMT", "label":"Importo totale"}
				],
				"help":"Seleziona la base per il calcolo dell'imposta ritenuta alla fonte."
			},{
				"id":"AUTO_APPLY", 
				"label":"Applica automaticamente l'imposta ritenuta alla fonte",
				"help":"Spunta questa casella per applicare l'imposta ritenuta alla fonte nelle transazioni."
			},{
				"id":"AVAILABLE_PURC", 
				"label":"Disponibile per gli acquisti",
				"help":"Spunta questa casella per rendere l'imposta ritenuta alla fonte disponibile nelle transazioni d'acquisto."
			},{
                "id":"NOT_ON_POS", 
                "label":"Disattiva Imposta preventiva in ordini d'acquisto",
                "help":"Seleziona questa casella per disattivare la ritenuta alla fonte negli ordini di acquisto."
            },{
                "id":"NOT_ON_SOS", 
                "label":"Disattiva Imposta preventiva in Ordini di vendita",
                "help":"Seleziona questa casella per disattivare la ritenuta alla fonte degli ordini di vendita."
            },{
				"id":"AVAILABLE_SALES", 
				"label":"Disponibile nelle vendite",
				"help":"Spunta questa casella per rendere l'imposta ritenuta alla fonte disponibile nelle transazioni di vendita."
			},{
				"id":"ENABLE_LOOKUP", 
				"label":"Attiva ricerca imposta su Transazioni",
				"help":"Spunta questa casella ricercare automaticamente il codice imposta predefinito nel record entità o voce in una transazione."
			},{
				"id":"NEXUS", 
				"label":"Nesso",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Un record di configurazione di imposta ritenuta alla fonte è stato creato per il nesso selezionato."
					}
				],
				"help":"Seleziona il nesso di imposta appropriato per questo record."
			},{
				"id":"TAX_POINT", 
				"label":"Punto imposta",
				"select": [
					{"id":"ACCRUAL", "label":"Su accantonamento"},
					{"id":"PAYMENT", "label":"Su pagamento"}
				],
				"help":"Il punto in cui viene riconosciuto un assoggettamento all'imposta ritenuta alla fonte, credito o spesa."
			}
		]
	},{
		"id":"WTAX_TYPE",
		"title":"Tipo di imposta ritenuta alla fonte",
		"title_plural":"Tipi di imposta ritenuta alla fonte",
		"fields":[
			{
				"id":"DESCRIPTION", 
				"label":"Descrizione",
				"help":"Inserisci una descrizione per questo tipo di imposta ritenuta alla fonte, codice imposta o gruppo imposta."
			},{
				"id":"NAME", 
				"label":"Nome",
				"help":"Inserisci un nome per questo tipo di imposta ritenuta alla fonte o gruppo imposta."
			},{
				"id":"NEXUS", 
				"label":"Nesso",
				"help":"Seleziona il nesso di imposta appropriato per questo record.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Esiste già un tipo di imposta ritenuta alla fonte con lo stesso nome del nesso del tipo selezionato."
					}
				]
			},{
				"id":"PURC_ACCOUNT", 
				"label":"Conto fiscale disponibilità/acquisti",
				"help":"Selezionare il conto controllo fiscale di ritenuta alla fonte per le transazioni di acquisto.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "L'account imposte disponibilità/acquisti e beni/vendite devono interessare account differenti."
					}
				]
			},{
				"id":"SALES_ACCOUNT", 
				"label":"Conto fiscale beni/vendite",
				"help":"Selezionare il conto controllo fiscale di ritenuta alla fonte per le transazioni di vendita."
			},{
				"id":"TAX_BASE", 
				"label":"Base di imposta ritenuta alla fonte",
				"select": [
					{"id":"GROSS_AMT", "label":"Importo lordo"},
					{"id":"NET_AMT", "label":"Importo netto"},
					{"id":"TAX_AMT", "label":"Importo imposta"}
				],
				"help":"Seleziona la base per il calcolo dell'imposta ritenuta alla fonte.",
				"error_msg": [
					{
						"id": "TAX_BASE_LOCK",
						"message":"Modifica della base imponibile alla fonte non è consentita in quanto ci sono le transazioni associate a questo tipo di ritenuta alla fonte."
					}
				]
			}
		]
	},{
		"id":"WTAX_CODE",
		"title":"Codice di imposta ritenuta alla fonte",
		"title_plural":"Codici di imposta ritenuta alla fonte",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Disponibile per",
				"select": [
					{"id":"BOTH", "label":"Entrambi"},
					{"id":"PURCHASES", "label":"Acquisti"},
					{"id":"SALES", "label":"Vendite"}
				],
				"help":"Selezionare il tipo di transazioni a cui questo codice imposta ritenuta alla fonte può essere applicato."
			},{
				"id":"DESCRIPTION", 
				"label":"Descrizione",
				"help":"Inserisci una descrizione per questo tipo di imposta ritenuta alla fonte, codice imposta o gruppo imposta."
			},{
				"id":"EFFECTIVE_FROM", 
				"label":"In vigore da",
				"help":"Inserire la data in cui il codice di ritenuta alla fonte prende effetto."
			},{
				"id":"INACTIVE", 
				"label":"Non attivo",
				"help":"Spuntare questa casella per disattivare questo codice di imposta ritenuta alla fonte."
			},{
				"id":"INC_CHILDREN", 
				"label":"Includi",
				"help":"Includi filiali figlie."
			},{
				"id":"NAME", 
				"label":"Codice imposta",
				"help":"Inserire un nominativo per questo codice imposta.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Esiste già un &1 con lo stesso nome nel nesso del tipo di imposta ritenuta alla fonte selezionato."
					}
				]
			},{
				"id":"PERCENTAGE_BASE", 
				"label":"Percentuale di base",
				"help":"Inserire la percentuale dell'importo di base che verrà usato nel calcolo dell'imposta ritenuta alla fonte.<br><br>Impostato come standard al: 100%"
			},{
				"id":"RATE", 
				"label":"Aliquota",
				"help":"Inserire l'aliquota di imposta appropriata in percentuale.<br><br>Esempio: 8%<br><br>Questa percentuale verrà calcolata selezionando questo codice di imposta ritenuta alla fonte nelle transazioni."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Filiali",
				"help":"Determina quali filiali riceveranno questo codice o gruppo codice di imposta ritenuta alla fonte disponibile. Se stai utilizzando OneWorld, NetSuite mostrerà questo campo. L'elenco a cascata mostra la lista delle filiali disponibili. Nel campo filiale, le colonne separano i livelli genitore (alla sinistra) dai rispettivi livelli figli (alla destra)."
			},{
				"id":"TAX_AGENCY", 
				"label":"Agenzia delle entrate",
				"help":"Agenzia delle entrate"
			},{
				"id":"VALID_UNTIL", 
				"label":"Valido fino al",
				"help":"Inserire la data di scadenza del codice di imposta ritenuta alla fonte.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "La data Valido fino a deve essere successiva alla data Valido a partire da."
					}
				]
			},{
				"id":"WTAX_SETUP", 
				"label":"Configura imposta ritenuta alla fonte",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Tipo di imposta ritenuta alla fonte",
				"help":"Selezionare il tipo di codice o di gruppo di imposta ritenuta alla fonte che si sta creando.<br><br>L'account di controllo imposta per questo tipo di imposta ritenuta alla fonte è selezionato come predefinito.<br><br>Puoi creare un nuovo tipo di imposta ritenuta alla fonte in Impostazioni > Imposta ritenuta alla fonte > Tipi di imposta."
			},{
				"id":"WTAX_BASE", 
				"label":"Base di imposta ritenuta alla fonte",
				"help":"Seleziona la base per il calcolo dell'imposta ritenuta alla fonte."
			},{
				"id":"STATE", 
				"label":"Stato"
			},{
				"id":"COUNTRY", 
				"label":"Paese"
			},{
				"id":"NEW", 
				"label":"Nuovo &1"
			},{
				"id":"NEXUS", 
				"label":"Nesso"
			}
		],
		"notif_msg": [
			{
				"id":"NOTIFMSG1", 
				"message": "Modificando questo record verranno aggiornati i seguenti gruppi diimposta ritenuta alla fonte membri di"
			}
		]
	},{
		"id":"WTAX_GROUP",
		"title":"Gruppo imposta ritenuta alla fonte",
		"title_plural":"Gruppi imposta ritenuta alla fonte",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Disponibile per",
				"select": [
					{"id":"PURCHASES", "label":"Acquisti"},
					{"id":"SALES", "label":"Vendite"}
				],
				"help":"Selezionare il tipo di transazioni a cui questo codice imposta ritenuta alla fonte può essere applicato."
			},{
				"id":"DESCRIPTION", 
				"label":"Descrizione",
				"help":"Inserisci una descrizione per questo tipo di imposta ritenuta alla fonte, codice imposta o gruppo imposta."
			},{
				"id":"INACTIVE", 
				"label":"Non attivo",
				"help":"Spuntare questa casella per disattivare questo codice di imposta ritenuta alla fonte."
			},{
				"id":"INC_CHILDREN", 
				"label":"Includi",
				"help":"Includi filiali figlie."
			},{
				"id":"NAME", 
				"label":"Nome",
				"help":"Inserisci un nome per questo tipo di imposta ritenuta alla fonte o gruppo imposta."
			},{
				"id":"RATE", 
				"label":"Aliquota",
				"help":"Inserire l'aliquota di imposta appropriata in percentuale.<br><br>Esempio: 8%<br><br>Questa percentuale verrà calcolata selezionando questo codice di imposta ritenuta alla fonte nelle transazioni."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Filiali",
				"help":"Determina quali filiali riceveranno questo codice o gruppo codice di imposta ritenuta alla fonte disponibile. Se stai utilizzando OneWorld, NetSuite mostrerà questo campo. L'elenco a cascata mostra la lista delle filiali disponibili. Nel campo filiale, le colonne separano i livelli genitore (alla sinistra) dai rispettivi livelli figli (alla destra)."
			},{
				"id":"WTAX_SETUP", 
				"label":"Configura imposta ritenuta alla fonte",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Tipo di imposta ritenuta alla fonte",
				"help":"Selezionare il tipo di codice o di gruppo di imposta ritenuta alla fonte che si sta creando.<br><br>L'account di controllo imposta per questo tipo di imposta ritenuta alla fonte è selezionato come predefinito.<br><br>Puoi creare un nuovo tipo di imposta ritenuta alla fonte in Impostazioni > Imposta ritenuta alla fonte > Tipi di imposta."
			},{
                "id":"WTAX_BASE", 
                "label": "Base di imposta ritenuta alla fonte",
				"help": "Consente di visualizzare la base imponibile del tipo selezionato Ritenuta d'acconto."
            },{
				"id":"NEW", 
				"label":"Nuovo &1"
			},{
				"id":"NEXUS", 
				"label":"Nesso"
			}
		],
		"subtab": [
			{
				"id":"WTAX_CODES", 
				"label":"Codici di imposta ritenuta alla fonte",
				"fields": [
					{
						"id":"BASIS", 
						"label":"Base"
					},{
						"id":"TYPE", 
						"label":"Tipo di imposta ritenuta alla fonte"
					},{
						"id":"TAX_BASE", 
						"label":"Base di imposta ritenuta alla fonte"
					},{
						"id":"WTAX_CODE", 
						"label":"Codice di imposta ritenuta alla fonte"
					},{
						"id":"RATE", 
						"label":"Aliquota"
					}
				]
			}
		],
		"error_msg": [
			{
				"id":"ERRMSG1", 
				"message": "Esiste già un &1 con lo stesso nome del nesso del tipo di imposta ritenuta alla fonte selezionato."
			},{
				"id":"ERRMSG2", 
				"message": "I codici di imposta ritenuta alla fonte devono avere la stessa base di imposta ritenuta alla fonte. Verificare la configurazione del tipo di imposta ritenuta alla fonte."
			}
		]
	},{
		"id":"RECORD",
		"field": [
			{ "id":"NEW", "label":"Nuovo &1" },
			{ "id":"YES", "label":"Sì" },
			{ "id":"NO", "label":"No" }
		],
		"button": [
			{ "id":"NEW", "label":"Nuovo" },
			{ "id":"CANCEL", "label":"Annulla" },
			{ "id":"DELETE", "label":"Elimina" },
			{ "id":"SAVE", "label":"Salva" }
		],
		"validation_msg":[
			{ "id":"DELETE_RECORD", "message":"Continuare l'eliminazione del record?" }
		],
		"error_msg":[
			{ "id":"PROB_FOUND", "message":"Sono stati individuati dei problemi." }
		]
	},{
		"id":"TRANS_CS",
		"form_reload": [
			{
				"id":"RELOADMSG1", 
				"message": "Il modulo deve essere ricaricato per poter applicare l'imposta ritenuta alla fonte. Le modifiche effettuate non verranno salvate."
			},{
				"id":"RELOADMSG2", 
				"message": "Il modulo deve essere ricaricato. Le modifiche effettuate non verranno salvate."
			}
		],
		"form_details": [
			{
				"id":"WTAX_CODE", 
				"message": "Selezionare un codice di imposta ritenuta alla fonte."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "Per visualizzare l'importo del pagamento al netto della ritenuta fiscale, nella sottoscheda Applica ricontrollare 'Applica' casella accanto alla fattura che si desidera pagare."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "Per visualizzare l'importo del pagamento al netto della ritenuta fiscale, nella sottoscheda Applica ricontrollare 'Applica' casella accanto al disegno di legge che si desidera pagare."
			}
		],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "Il totale Importo ritenuta d'acconto per i tipi di tutte le trattenute fiscali non può essere zero o meno. Ciò include la ripartizione degli importi per tutti i gruppi applicabili imposta applicata sulla transazione."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Applicando uno sconto operazione non è supportata quando il punto di ritenuta d'acconto è impostato su \"Su accantonamento.\""
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Applicando uno sconto operazione non è supportata quando il punto di ritenuta d'acconto è impostato su \"Su accantonamento.\""
			}
		]
	},{
		"id":"TRANS_UE",
		"field": [
			{ "id":"AMOUNT", "label":"Importo" },
			{
				"id":"APPLIES_TO", 
				"label":"Applica a",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Voci linea individuali"},
					{"id":"TOTAL_AMT", "label":"Importo totale"}
				],
				"help":"Seleziona la base per il calcolo dell'imposta ritenuta alla fonte."
			},
			{ "id":"APPLY_WH_TAX", "label":"Applicare imposta RF?" },
			{
				"id":"BASE_AMT", 
				"label":"Importo base",
				"help":"Base per il calcolo dell'imposta ritenuta alla fonte."
			},
			{ "id":"DATE", "label":"Data" },
			{ "id":"REF_NO", "label":"Rif. n." },
			{ 
				"id":"TAX_AMOUNT", 
				"label":"Importo imposta",
				"help":"Importo dell'imposta ritenuta alla fonte."
			},
			{ 
				"id":"TAX_CODE", 
				"label":"Codice imposta",
				"help":"Codice o gruppo di ritenuta alla fonte applicabile." 
			},
			{ 
				"id":"TAX_RATE", 
				"label":"Aliquota di imposta",
				"help": "Aliquota di ritenuta alla fonte applicabile." 
			},
			{ "id":"TOTAL", "label":"Totale" },
			{ "id":"TYPE", "label":"Tipo" },
			{ "id":"WITHHELD", "label":"Trattenuta" },
			{ "id":"WTAX_AMT", "label":"Importo imposta RF" },
			{ "id":"WTAX_BASE_AMT", "label":"Imp. base imposta RF" },
			{ "id":"WTAX_CODE", "label":"Codice imposta RF" },
			{ "id":"WTAX_RATE", "label":"Aliquota d'imposta RF" },
			{ "id":"BILL_PAYMENT", "label":"Bill Pagamento" },
			{ "id":"INVOICE_PAYMENT", "label":"Pagamento Fattura" },
			{ "id":"BILL_CREDIT", "label":"Nota di credito" },
			{ "id":"CREDIT_MEMO", "label":"Promemoria credito" }
		],
		"subtab":[
			{ "id":"WTAX", "label":"Ritenuta d'acconto" }
		],
		"button": [
			{ "id":"GO_BACK", "label":"Torna indietro" }
		],
		"field_msg": [
			{
				"id":"CANCEL_WTAX_BILL_CREDIT",
				"message":"Credito conto imposta ritenuta alla fonte da pagamento conto annullato" 
			},{
				"id":"CANCEL_WTAX_CREDIT_MEMO",
				"message":"Promemoria credito imposta ritenuta alla fonte da pagamento fattura annullato"
			},{
				"id":"AMOUNT", "message":"Importo"
			},{
				"id":"WTAX_AMT",
				"message":"Withholding Tax Amount for &1"
			},{
				"id":"REF_NO", "message":"Rif. n."
			}
		],
		"error_msg": [
			{
				"id":"TRAN_LOCK_BILL_CREDIT",
				"message":"Questo credito fatturabile riflette l'importo dell'imposta ritenuta alla fonte per la fattura associata."
			}, {
				"id":"TRAN_LOCK_BILL_PAYMENT",
				"message":"Questo conto ha già un pagamento associato, netto dell'imposta ritenuta alla fonte applicabile."
			}, {
				"id":"TRAN_LOCK_CREDIT_MEMO",
				"message":"Questo promemoria credito riflette l'importo dell'imposta ritenuta alla fonte per la fattura associata."
			}, {
				"id":"TRAN_LOCK_INVOICE_PAYMENT",
				"message":"Questa fattura ha già un pagamento associato, netto dell'imposta ritenuta alla fonte applicabile."
			}, {
				"id":"TRAN_LOCK",
				"message":"Non è possibile apportare modifiche a questa transazione. Per ulteriori informazioni, contattare l'amministratore Netsuite."
			}, {
				"id":"TRAN_LOCK_NS_INFO",
				"message":"Informazioni NetSuite"
			}, {
				"id":"TRAN_LOCK_WARNING",
				"message":"Attenzione"
			}, {
				"id":"SO_MULT_INVENTORY",
				"message":"In ordine di vendita quando Multi-Inventory Location è attiva, un valore per Posizione è richiesta."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"Sulla fattura quando Multi-Inventory Location è attiva, un valore per Posizione è richiesta."
			}, {
				"id":"ERROR",
				"message":"Errore"
			}, {
				"id":"VAT_CODE_CHECK_US",
				"message":"Si prega di assicurarsi che non vi sia almeno 1 codice fiscale con le seguenti impostazioni per il paese corrente / filiale: Vota <br> = Nome fiscale 0,00% <br> =-non imponibili-<br> inattiva = falso"
			}, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"Fare in modo che ci sia almeno un gruppo fiscale con GST / PST o codici fiscali HST con le seguenti impostazioni per il paese / filiale corrente prega:<br><br>Tasso = 0,00%<br>Escludere dai rapporti di IVA = vero (casella di controllo deve essere contrassegnato)<br>Inattivo = falso (casella di controllo non deve essere contrassegnato)"
			}, {
				"id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
				"message":"Si prega di assicurarsi che non vi sia almeno 1 codice fiscale con le seguenti impostazioni per il paese corrente / filiale: Vota <br> = 0,00% <br> escludere dal report IVA = vero (casella di controllo deve essere contrassegnato) <br> Inattivo = falso"
			}, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
		],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
	},{
		"id":"REPORT",
		"report": [
			{ "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Ritenuta d'imposta per i fornitori (dettaglio)" },
			{ "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Dichiarazione trimestrale trattenuta per fornitore - Periodo" },
			{ "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Tassa mensile trattenuta per fornitore - Periodo" },
			{ "id":"WTAX_VENDOR_PDF", "title":"Elenco dei fornitori per i quali le imposte risultano trattenute" },
			{ "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Imposta ritenuta alla fonte per cliente (dettaglio)" },
			{ "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Imposta ritenuta alla fonte per cliente" },
			{ "id":"FROM", "title":"Dal" },
			{ "id":"TO", "title":"Al" },
			{ "id":"WTAX_CUSTOMER_PDF", "title":"Elenco dei clienti per i quali le imposte risultano trattenute" },
			{ "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Riepilogo imposta trattenuta per fornitore" },
			{ "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Riepilogo imposta trattenuta per cliente" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Acquisti per dettaglio codice di imposta ritenuta alla fonte" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"Elenco degli acquisti con cui sono state prelevate le tasse (Dettaglio)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Riepilogo acquisti per codice di imposta ritenuta alla fonte" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"Elenco degli acquisti con cui sono state prelevate le tasse (Sintesi)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Vendite per dettaglio codice di imposta ritenuta alla fonte" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"Elenco delle vendite che sono state trattenute imposte (Dettaglio)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Riepilogo vendite per codice di imposta ritenuta alla fonte" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"Elenco delle vendite che sono state trattenute imposte (Sintesi)" }
		],
		"common_field": [
			{ "id":"AGENT_NAME", "label":"Nome agente ritenuta" },
			{ "id":"AMT_INCOME_PMNT", "label":"Importo di pagamento del reddito" },
			{ "id":"AMT_TAX_WHELD", "label":"Importo delle imposte trattenute" },
			{ "id":"CLASSIFICATION", "label":"Classificazione" },
			{ "id":"COMPANY", "label":"Società" },
			{ "id":"CORPORATION", "label":"Impresa (Nome Registrato)" },
			{ "id":"DESCRIPTION", "label":"Descrizione" },
			{ "id":"END_OF_REPORT", "label":"Fine del report" },
			{ "id":"FIRST_NAME", "label":"Nome" },
			{ "id":"GRAND_TOTAL", "label":"Totale comprensivo" },
			{ 
				"id":"INDIVIDUAL", 
				"label":"Individuale",
				"long_label":"Individuale (cognome, nome, secondo nome)" 
			},
			{ "id":"LAST_NAME", "label":"Cognome" },
			{ "id":"MEMO", "label":"Promemoria" },
			{ "id":"MIDDLE_NAME", "label":"Secondo nome" },
			{ "id":"NEXUS", "label":"Nesso" },
			{ "id":"PAYEE_NAME", "label":"Nome ordinatario" },
			{ "id":"PERIOD_FROM", "label":"per il periodo" }, 
			{ "id":"PERIOD_TO", "label":"al" },
			{ "id":"REFERENCE_NO", "label":"Numero di transazione" },
			{ "id":"SEQ_NO", "label":"Seq. n." },
			{ "id":"SUBSIDIARY", "label":"Filiale" },
			{ "id":"TAX_CODE", "label":"Codice imposta" },
			{ "id":"TAX_PERIOD", "label":"Periodo fiscale" },
			{ "id":"TAX_RATE", "label":"Aliquota di imposta" },
			{ "id":"TAX_REG_NO", "label":"Codice fiscale" },
			{ "id":"TAX_TYPE", "label":"Tipo imposta" },
			{ "id":"TIN", "label":"ID Fiscale" },
			{ "id":"TRANSACTION_TYPE", "label":"Tipo di transazione" },
			{ "id":"WTAX_RATE", "label":"Aliquota d'imposta ritenuta alla fonte" },
			{ "id":"NUM_ONE", "label":"1" },
			{ "id":"NUM_TWO", "label":"2" },
			{ "id":"NUM_THREE", "label":"3" },
			{ "id":"NUM_FOUR", "label":"4" },
			{ "id":"NUM_FIVE", "label":"5" },
			{ "id":"NUM_SIX", "label":"6" },
			{ "id":"NUM_SEVEN", "label":"7" },
			{ "id":"NUM_EIGHT", "label":"8" },
			{ "id":"NUM_NINE", "label":"9" },
			{ "id":"NUM_TEN", "label":"10" },
			{ "id":"NUM_ELEVEN", "label":"11" },
			{ "id":"NUM_TWELVE", "label":"12" }
		],
		"common_field_value":[
			{ "id":"NONE", "value":"Nessuno" }
		],
		"button": [
			{ "id":"SUBMIT", "label":"Invia" },
			{ "id":"PRINT_PDF", "label":"Stampa PDF" },
			{ "id":"SAVE_CSV", "label":"Esporta - CSV" },
			{ "id":"EXPORT_DAT", "label":"Esporta - DAT" },
			{ "id":"RETURN_TO_CRITERIA", "label":"Indietro a Criteri" }
		]
	}
]