[{
		"id":"GLOBAL_TRY_CATCH",
		"title":"Globale Try Catch Fehler",
		"error_msg":[
			{
				"id":"TRIGGERED_BY_USER", "message":"Fehler durch den Benutzer ausgelöst:" 
			} 
		]
	},{
		"id":"GLOBAL_VALUES",
		"title":"Globale Werte",
		"error_msg":[
			{
				"id":"CONTACT_NS_ADMIN", "message":"Kontaktieren Sie Ihren NetSuite-Administrator für weitere Informationen.", 
				"id":"WARNING", "message":"Warnung"
			} 
		]
	},{
		"id":"WTAX_SETUP",
		"title":"Einrichtung Quellensteuer",
		"fields":[
			{
				"id":"APPLIES_TO", 
				"label":"Findet Anwendung auf",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Einzelposten"},
					{"id":"TOTAL_AMT", "label":"Gesamtbetrag"}
				],
				"help":"Geben Sie die Basis zur Berechnung der Quellensteuer ein."
			},{
				"id":"AUTO_APPLY", 
				"label":"Quellensteuer automatisch anwenden",
				"help":"Aktivieren Sie dieses Kästchen, um Quellensteuer in Transaktionen automatisch anzuwenden."
			},{
				"id":"AVAILABLE_PURC", 
				"label":"Bei Warenbezügen verfügbar",
				"help":"Aktivieren Sie dieses Kästchen, um Quellensteuer in Warenbezugstransaktionen verfügbar zu machen."
			},{
                "id":"NOT_ON_POS", 
                "label":"Deaktivieren Quellensteuer in Bestellungen",
                "help":"Markieren Sie dieses Kästchen, um Quellensteuer Bestellungen deaktivieren."
            },{
                "id":"NOT_ON_SOS", 
                "label":"Deaktivieren Quellensteuer in Kundenaufträgen",
                "help":"Markieren Sie dieses Kästchen Quellensteuer in Kundenaufträgen zu deaktivieren."
            },{
				"id":"AVAILABLE_SALES", 
				"label":"Bei Verkäufen verfügbar",
				"help":"Aktivieren Sie dieses Kästchen, um Quellensteuer in Verkaufstransaktionen verfügbar zu machen."
			},{
				"id":"ENABLE_LOOKUP", 
				"label":"Steuersuche bei Transaktionen aktivieren",
				"help":"Aktivieren Sie dieses Kästchen, um automatisch den Standard-Steuercode im Einheits- oder Artikeldatensatz einer Transaktion zu finden."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Ein Datensatz Quellensteuereinrichtung wurde für den gewählten Nexus erstellt."
					}
				],
				"help":"Wählen Sie den angemessenen Steuernexus für diesen Datensatz aus."
			},{
				"id":"TAX_POINT", 
				"label":"Steuerpunkt",
				"select": [
					{"id":"ACCRUAL", "label":"Auf Rückstellung"},
					{"id":"PAYMENT", "label":"Auf Zahlung"}
				],
				"help":"Der Zeitpunkt, zu dem eine Quellensteuerverbindlichkeit, -gutschrift oder -auslage realisiert wird."
			}
		]
	},{
		"id":"WTAX_TYPE",
		"title":"Quellensteuerart",
		"title_plural":"Quellensteuerarten",
		"fields":[
			{
				"id":"DESCRIPTION", 
				"label":"Beschreibung",
				"help":"Geben Sie eine Beschreibung dieser Quellensteuerart, dieses Steuercodes oder dieser Steuergruppe ein."
			},{
				"id":"NAME", 
				"label":"Name",
				"help":"Geben Sie einen Namen für diese Quellensteuerart oder diese Steuergruppe ein."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"help":"Wählen Sie den angemessenen Steuernexus für diesen Datensatz aus.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Eine Quellensteuerart mit demselben Namen besteht im gewählten Nexus bereits."
					}
				]
			},{
				"id":"PURC_ACCOUNT", 
				"label":"Passiv-/Verkaufssteuerkonto",
				"help":"Wählen Sie das Quellensteuersammelkonto für Warenbezugstransaktionen aus.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Passiv-/Warenbezugssteuerkonto und Bestands-/Umsatzsteuerkonto sollten in unterschiedliche Konten gebucht werden."
					}
				]
			},{
				"id":"SALES_ACCOUNT", 
				"label":"Bestands-/Umsatzsteuerkonto",
				"help":"Wählen Sie das Quellensteuersammelkonto für Verkaufstransaktionen aus."
			},{
				"id":"TAX_BASE", 
				"label":"Quellensteuer-Base",
				"select": [
					{"id":"GROSS_AMT", "label":"Bruttobetrag"},
					{"id":"NET_AMT", "label":"Nettobetrag"},
					{"id":"TAX_AMT", "label":"Steuerbetrag"}
				],
				"help":"Geben Sie die Basis zur Berechnung der Quellensteuer ein.",
				"error_msg": [
					{
						"id": "TAX_BASE_LOCK",
						"message":"Bearbeiten der Quellensteuer Basis ist nicht zulässig, da es Transaktionen, verbunden mit dieser Quellensteuertyp sind."
					}
				]
			}
		]
	},{
		"id":"WTAX_CODE",
		"title":"Steuercode",
		"title_plural":"Quellensteuercodes",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Verfügbar am",
				"select": [
					{"id":"BOTH", "label":"Beide"},
					{"id":"PURCHASES", "label":"Warenbezug"},
					{"id":"SALES", "label":"Verkauf"}
				],
				"help":"Wählen Sie die Transaktionsart, auf die dieser Quellensteuercode oder diese Steuergruppe angewendet werden kann"
			},{
				"id":"DESCRIPTION", 
				"label":"Beschreibung",
				"help":"Geben Sie eine Beschreibung dieser Quellensteuerart, dieses Steuercodes oder dieser Steuergruppe ein."
			},{
				"id":"EFFECTIVE_FROM", 
				"label":"Wirksam ab",
				"help":"Geben Sie das Datum ein, ab dem dieser Quellensteuercode wirksam ist."
			},{
				"id":"INACTIVE", 
				"label":"Nicht aktiv",
				"help":"Aktivieren Sie dieses Kästchen, um diesen Quellensteuercode zu deaktivieren."
			},{
				"id":"INC_CHILDREN", 
				"label":"Untergeordnete einschließen",
				"help":"Untergeordnete Niederlassungen einschließen."
			},{
				"id":"NAME", 
				"label":"Steuercode",
				"help":"Geben Sie einen Namen für diesen Steuercode ein.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Ein &1 mit demselben Namen besteht im Nexus der gewählten Quellensteuerart bereits."
					}
				]
			},{
				"id":"PERCENTAGE_BASE", 
				"label":"Prozentsatz des Basisbetrags",
				"help":"Geben Sie den Prozentsatz des Basisbetrags ein, der zur Berechnung der Quellensteuer verwendet wird.<br><br>Standardmäßig ist dieser auf 100% gesetzt."
			},{
				"id":"RATE", 
				"label":"Satz",
				"help":"Geben Sie den angemessenen Quellensteuersatz als Prozentsatz ein.<br><br>Beispiel: 8%<br><br>Dieser Prozentsatz wird berechnet, wenn Sie diesen Quellensteuercode bei Transaktionen auswählen."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Niederlassungen",
				"help":"Dies bestimmt, für welche Niederlassungen dieser Quellensteuercode oder diese Steuergruppe verfügbar sein wird. Wenn Sie OneWorld verwenden, zeigt NetSuite dieses Datenfeld an. Die Dropdown-Liste zeigt die Liste der verfügbaren Niederlassungen an. Im Datenfeld \"Niederlassungen\" trennen Doppelpunkte übergeordnete von untergeordneten Niederlassungen. "
			},{
				"id":"TAX_AGENCY", 
				"label":"Steuerverwaltungsbehörde",
				"help":"Steuerverwaltungsbehörde"
			},{
				"id":"VALID_UNTIL", 
				"label":"Gültig bis",
				"help":"Geben Sie das Datum ein, bis zu dem dieser Quellensteuercode gültig ist.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Das \"Gültig bis\"-Datum muss nach dem \"Wirksam ab\"-Datum liegen."
					}
				]
			},{
				"id":"WTAX_SETUP", 
				"label":"Einrichtung Quellensteuer",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Quellensteuerart",
				"help":"Wählen Sie die Art des Quellensteuercodes oder der Steuergruppe, den/die Sie erstellen.<br><br>Das Steuersammelkonto für diese Art von Quellensteuer ist standardmäßig ausgewählt.<br><br>Sie können unter Setup > Einbehaltene Steuer > Steuerarten eine neue Quellensteuerart erstellen."
			},{
				"id":"WTAX_BASE", 
				"label":"Basis für Quellensteuer",
				"help":"Geben Sie die Basis zur Berechnung der Quellensteuer ein."
			},{
				"id":"STATE", 
				"label":"Bundesland/Bundesstaat"
			},{
				"id":"COUNTRY", 
				"label":"Land"
			},{
				"id":"NEW", 
				"label":"Neu &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"notif_msg": [
			{
				"id":"NOTIFMSG1", 
				"message": "Eine Bearbeitung dieses Datensatzes führt zu einer Aktualisierung der folgenden Quellensteuergruppen, in denen er sich befindet"
			}
		]
	},{
		"id":"WTAX_GROUP",
		"title":"Quellensteuergruppe",
		"title_plural":"Quellensteuergruppen",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Verfügbar am",
				"select": [
					{"id":"PURCHASES", "label":"Warenbezug"},
					{"id":"SALES", "label":"Verkauf"}
				],
				"help":"Wählen Sie die Transaktionsart, auf die dieser Quellensteuercode oder diese Steuergruppe angewendet werden kann"
			},{
				"id":"DESCRIPTION", 
				"label":"Beschreibung",
				"help":"Geben Sie eine Beschreibung dieser Quellensteuerart, dieses Steuercodes oder dieser Steuergruppe ein."
			},{
				"id":"INACTIVE", 
				"label":"Nicht aktiv",
				"help":"Aktivieren Sie dieses Kästchen, um diesen Quellensteuercode zu deaktivieren."
			},{
				"id":"INC_CHILDREN", 
				"label":"Untergeordnete einschließen",
				"help":"Untergeordnete Niederlassungen einschließen."
			},{
				"id":"NAME", 
				"label":"Name",
				"help":"Geben Sie einen Namen für diese Quellensteuerart oder diese Steuergruppe ein."
			},{
				"id":"RATE", 
				"label":"Satz",
				"help":"Geben Sie den angemessenen Quellensteuersatz als Prozentsatz ein. <br><br>Beispiel: 8%<br><br>Dieser Prozentsatz wird berechnet, wenn Sie diesen Quellensteuercode bei Transaktionen auswählen."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Niederlassungen",
				"help":"Dies bestimmt, für welche Niederlassungen dieser Quellensteuercode oder diese Steuergruppe verfügbar sein wird. Wenn Sie OneWorld verwenden, zeigt NetSuite dieses Datenfeld an. Die Dropdown-Liste zeigt die Liste der verfügbaren Niederlassungen an. Im Datenfeld \"Niederlassungen\" trennen Doppelpunkte übergeordnete von untergeordneten Niederlassungen."
			},{
				"id":"WTAX_SETUP", 
				"label":"Einrichtung Quellensteuer",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Quellensteuerart",
				"help":"Wählen Sie die Art des Quellensteuercodes oder der Steuergruppe, den/die Sie erstellen.<br><br>Das Steuersammelkonto für diese Art von Quellensteuer ist standardmäßig ausgewählt.<br><br>Sie können unter Setup > Einbehaltene Steuer > Steuerarten eine neue Quellensteuerart erstellen."
			},{
                "id":"WTAX_BASE", 
                "label": "Quellensteuer-Base",
				"help": "Zeigt die Bemessungsgrundlage der ausgewählten Quellensteuer Typ."
            },{
				"id":"NEW", 
				"label":"Neu &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"subtab": [
			{
				"id":"WTAX_CODES", 
				"label":"Quellensteuercodes",
				"fields": [
					{
						"id":"BASIS", 
						"label":"Basis"
					},{
						"id":"TYPE", 
						"label":"Quellensteuerart"
					},{
						"id":"TAX_BASE", 
						"label":"Basis für Quellensteuer"
					},{
						"id":"WTAX_CODE", 
						"label":"Quellensteuercode"
					},{
						"id":"RATE", 
						"label":"Satz"
					}
				]
			}
		],
		"error_msg": [
			{
				"id":"ERRMSG1", 
				"message": "Eine &1 mit demselben Namen besteht im Nexus der gewählten Quellensteuerart bereits."
			},{
				"id":"ERRMSG2", 
				"message": "Quellensteuercodes sollten dieselbe Quellensteuerbasis haben. Bitte überprüfen Sie die Einstellungen der Quellensteuerart."
			}
		]
	},{
		"id":"RECORD",
		"field": [
			{ "id":"NEW", "label":"Neu &1" },
			{ "id":"YES", "label":" Ja" },
			{ "id":"NO", "label":"Nein" }
		],
		"button": [
			{ "id":"NEW", "label":"Neu" },
			{ "id":"CANCEL", "label":"Abbrechen" },
			{ "id":"DELETE", "label":"Löschen" },
			{ "id":"SAVE", "label":"Speichern" }
		],
		"validation_msg":[
			{ "id":"DELETE_RECORD", "message":"Möchten Sie diesen Datensatz wirklich löschen?" }
		],
		"error_msg":[
			{ "id":"PROB_FOUND", "message":"Probleme gefunden." }
		]
	},{
		"id":"TRANS_CS",
		"form_reload": [
			{
				"id":"RELOADMSG1", 
				"message": "Das Formular muss neu geladen werden, damit Quellensteuer angewendet werden kann. Vorgenommene Änderungen werden nicht gespeichert."
			},{
				"id":"RELOADMSG2", 
				"message": "Das Formular muss neu geladen werden. Vorgenommene Änderungen werden nicht gespeichert."
			}
		],
		"form_details": [
			{
				"id":"WTAX_CODE", 
				"message": "Bitte wählen Sie einen Quellensteuercode aus."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "Um den Auszahlungsbetrag nach Abzug der Quellensteuer, in der Apply-Unterreiter erneut prüfen 'Apply'-Box neben der Rechnung Sie zahlen möchten zeigen."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "Um den Auszahlungsbetrag nach Abzug der Quellensteuer, in der Apply-Unterreiter erneut prüfen 'Apply'-Kästchen neben der Rechnung Sie zahlen möchten zeigen."
			}
		],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "Die gesamte Quellensteuer für alle Quellensteuertypen nicht Null oder weniger betragen. Dazu gehört auch die Aufschlüsselung der für alle geltenden Quellensteuer Gruppen bei der Transaktion angewendet."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Anwenden einer Transaktion Rabatt wird nicht unterstützt, wenn die Quellensteuer Punkt auf \"Auf Rückstellung.\""
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Anwenden einer Transaktion Rabatt wird nicht unterstützt, wenn die Quellensteuer Punkt auf \"Auf Rückstellung.\""
			}
		]
	},{
		"id":"TRANS_UE",
		"field": [
			{ "id":"AMOUNT", "label":"Betrag" },
			{
				"id":"APPLIES_TO", 
				"label":"Findet Anwendung auf",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Einzelposten"},
					{"id":"TOTAL_AMT", "label":"Gesamtbetrag"}
				],
				"help":"Geben Sie die Basis zur Berechnung der Quellensteuer ein."
			},
			{ "id":"APPLY_WH_TAX", "label":"Quellensteuer anwenden?" },
			{
				"id":"BASE_AMT", 
				"label":"Basisbetrag",
				"help":"Die Basis zur Berechnung der Quellensteuer."
			},
			{ "id":"DATE", "label":"Datum" },
			{ "id":"REF_NO", "label":"Ref.-Nr." },
			{ 
				"id":"TAX_AMOUNT", 
				"label":"Steuerbetrag",
				"help":"Der Betrag der Quellensteuer."
			},
			{ 
				"id":"TAX_CODE", 
				"label":"Steuercode",
				"help":"Der zutreffende Quellensteuercode oder die Steuergruppe." 
			},
			{ 
				"id":"TAX_RATE", 
				"label":"Steuersatz",
				"help": "Der zutreffende Quellensteuersatz." 
			},
			{ "id":"TOTAL", "label":"Gesamtsumme" },
			{ "id":"TYPE", "label":"Art" },
			{ "id":"WITHHELD", "label":"Einbehalten" },
			{ "id":"WTAX_AMT", "label":"Quellensteuerbetrag" },
			{ "id":"WTAX_BASE_AMT", "label":"Basisbetrag der Quellensteuer" },
			{ "id":"WTAX_CODE", "label":"Quellensteuercode" },
			{ "id":"WTAX_RATE", "label":"Quellensteuersatz" },
			{ "id":"BILL_PAYMENT", "label":"Bill Payment" },
			{ "id":"INVOICE_PAYMENT", "label":"Rechnung Zahlung" },
			{ "id":"BILL_CREDIT", "label":"Rechnungsgutschrift" },
			{ "id":"CREDIT_MEMO", "label":"Gutschrift" }
		],
		"subtab":[
			{ "id":"WTAX", "label":"Quellensteuer" }
		],
		"button": [
			{ "id":"GO_BACK", "label":"Zurück" }
		],
		"field_msg": [
			{
				"id":"CANCEL_WTAX_BILL_CREDIT",
				"message":"Stornierte Rechnungsgutschrift für Quellensteuer von stornierter Rechnung (Einkauf)"
			},{
				"id":"CANCEL_WTAX_CREDIT_MEMO",
				"message":"Stornierter Gutschriftsvermerk für Quellensteuer von stornierter Rechnungszahlung (Verkauf)"
			},{
				"id":"AMOUNT", "message":"Betrag"
			},{
				"id":"WTAX_AMT",
				"message":"Withholding Tax Amount for &1"
			},{
				"id":"REF_NO", "message":"Ref.-Nr."
			}
		],
		"error_msg": [
			{
				"id":"TRAN_LOCK_BILL_CREDIT",
				"message":"Diese Rechnungsgutschrift (Einkauf) spiegelt den Quellensteuerbetrag der verknüpften Rechnung wider."
			}, {
				"id":"TRAN_LOCK_BILL_PAYMENT",
				"message":"Auf diese Rechnung (Einkauf) wurde bereits eine Zahlung angewendet, abzüglich der zutreffenden Quellensteuer."
			}, {
				"id":"TRAN_LOCK_CREDIT_MEMO",
				"message":"Diese Rechnungsgutschrift (Verkauf) spiegelt den Quellensteuerbetrag der verknüpften Rechnung wider."
			}, {
				"id":"TRAN_LOCK_INVOICE_PAYMENT",
				"message":"Auf diese Rechnung (Verkauf) wurde bereits eine Zahlung angewendet, abzüglich der zutreffenden Quellensteuer."
			}, {
				"id":"TRAN_LOCK",
				"message":"Für diese Transaktion sind keine Bearbeitungen erlaubt. Kontaktieren Sie Ihren NetSuite-Administrator für weitere Informationen."
			}, {
				"id":"TRAN_LOCK_NS_INFO",
				"message":"NetSuite-Informationen"
			}, {
				"id":"TRAN_LOCK_WARNING",
				"message":"Warnung"
			}, {
				"id":"SO_MULT_INVENTORY",
				"message":"On Sales Order, wenn Multi-Location Inventar aktiviert ist, wird ein Wert für die Lage erforderlich."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"Auf Rechnung, wenn Multi-Location Inventar aktiviert ist, wird ein Wert für die Lage erforderlich."
			}, {
				"id":"ERROR",
				"message":"Fehler"
			}, {
				"id":"VAT_CODE_CHECK_US",
				"message":"Bitte stellen Sie sicher, dass es mindestens 1 Abgabenordnung mit den folgenden Einstellungen für das aktuelle Land / Tochter: <br> Rate = 0,00% <br> Tax Name =-Not versteuernde-<br> Inactive = falsch"
			}, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"Bitte stellen Sie sicher, dass es mindestens eine Organschaft mit GST / PST oder HST Steuer-Codes mit den folgenden Einstellungen für das aktuelle Land / Tochter:<br><br>Rate = 0,00%<br>Ausschließen von der Mehrwertsteuer Reports = wahr (Kontrollkästchen markiert werden soll)<br>Inaktive = falsch (Kontrollkästchen nicht angekreuzt werden)"
			}, {
				"id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
				"message":"Bitte stellen Sie sicher, dass es mindestens 1 Abgabenordnung mit den folgenden Einstellungen für das aktuelle Land / Tochter: <br> Rate = 0,00% <br> Aus MwSt. Berichten ausschließen = wahr (Kontrollkästchen markiert werden sollte) <br> Inactive = falsch"
			}, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
		],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
	},{
		"id":"REPORT",
		"report": [
			{ "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Einbehaltene Steuer für Lieferanten (Detail)" },
			{ "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Quarterly Steuer für Hersteller Unbekannt - Periode" },
			{ "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Monatlich Steuer für Hersteller Unbekannt - Periode" },
			{ "id":"WTAX_VENDOR_PDF", "title":"Liste der Lieferanten, von denen Steuern einbehalten wurden" },
			{ "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Von Kunden einbehaltene Steuer (Detail)" },
			{ "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Von Kunden einbehaltene Steuer" },
			{ "id":"FROM", "title":"Von" },
			{ "id":"TO", "title":"Bis" },
			{ "id":"WTAX_CUSTOMER_PDF", "title":"Liste der Kunden, von denen Steuern einbehalten wurden" },
			{ "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Einbehaltene Steuer für Lieferanten Übersicht" },
			{ "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Einbehaltene Steuer für Kunden Übersicht" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Warenbezüge nach Quellensteuercode Details" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"Liste der Käufe durch die Steuern wurden einbehalten (Detail)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Warenbezüge nach Quellensteuercode Übersicht" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"Liste der Käufe durch die Steuern wurden einbehalten (Zusammenfassung)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Verkäufe nach Quellensteuercode Details" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"Liste der Verkäufe durch die Steuern wurden einbehalten (Detail)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Verkäufe nach Quellensteuercode Übersicht" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"Liste der Verkäufe durch die Steuern wurden einbehalten (Zusammenfassung)" }
		],
		"common_field": [
			{ "id":"AGENT_NAME", "label":"Name des Vertreters, der Steuern einbehält" },
			{ "id":"AMT_INCOME_PMNT", "label":"Betrag der Einkommenszahlung" },
			{ "id":"AMT_TAX_WHELD", "label":"Einbehaltener Steuerbetrag" },
			{ "id":"CLASSIFICATION", "label":"Klassifizierung" },
			{ "id":"COMPANY", "label":"Unternehmen" },
			{ "id":"CORPORATION", "label":"Firma (Eingetragener Name)" },
			{ "id":"DESCRIPTION", "label":"Beschreibung" },
			{ "id":"END_OF_REPORT", "label":"Ende des Berichts" },
			{ "id":"FIRST_NAME", "label":"Vorname" },
			{ "id":"GRAND_TOTAL", "label":"Gesamtbetrag" },
			{ 
				"id":"INDIVIDUAL", 
				"label":"Einzelperson",
				"long_label":"Einzelperson (Nachname, Vorname, zweiter Vorname)" 
			},
			{ "id":"LAST_NAME", "label":"Nachname" },
			{ "id":"MEMO", "label":"Vermerk" },
			{ "id":"MIDDLE_NAME", "label":"Zweiter Vorname" },
			{ "id":"NEXUS", "label":"Nexus" },
			{ "id":"PAYEE_NAME", "label":"Name des Zahlumgsempfängers" },
			{ "id":"PERIOD_FROM", "label":"für den zeitraum" },
			{ "id":"PERIOD_TO", "label":"bis" },
			{ "id":"REFERENCE_NO", "label":"Transaktionsnummer" },
			{ "id":"SEQ_NO", "label":"Folgenummer" },
			{ "id":"SUBSIDIARY", "label":"Niederlassung" },
			{ "id":"TAX_CODE", "label":"Steuercode" },
			{ "id":"TAX_PERIOD", "label":"Steuerperiode" },
			{ "id":"TAX_RATE", "label":"Quellensteuersatz" },
			{ "id":"TAX_REG_NO", "label":"Steuerregistrierungsnummer" },
			{ "id":"TAX_TYPE", "label":"Steuerart" },
			{ "id":"TIN", "label":"Tax ID" },
			{ "id":"TRANSACTION_TYPE", "label":"Art der Transaktion" },
			{ "id":"WTAX_RATE", "label":"Quellensteuersatz" },
			{ "id":"NUM_ONE", "label":"1" },
			{ "id":"NUM_TWO", "label":"2" },
			{ "id":"NUM_THREE", "label":"3" },
			{ "id":"NUM_FOUR", "label":"4" },
			{ "id":"NUM_FIVE", "label":"5" },
			{ "id":"NUM_SIX", "label":"6" },
			{ "id":"NUM_SEVEN", "label":"7" },
			{ "id":"NUM_EIGHT", "label":"8" },
			{ "id":"NUM_NINE", "label":"9" },
			{ "id":"NUM_TEN", "label":"10" },
			{ "id":"NUM_ELEVEN", "label":"11" },
			{ "id":"NUM_TWELVE", "label":"12" }
		],
		"common_field_value":[
			{ "id":"NONE", "value":"Keine/r" }
		],
		"button": [
			{ "id":"SUBMIT", "label":"Übermitteln" },
			{ "id":"PRINT_PDF", "label":"PDF drucken" },
			{ "id":"SAVE_CSV", "label":"Export - CSV" },
			{ "id":"EXPORT_DAT", "label":"Export - DAT" },
			{ "id":"RETURN_TO_CRITERIA", "label":"Zu Kriterium zurückkehren" }
		]
	}
]