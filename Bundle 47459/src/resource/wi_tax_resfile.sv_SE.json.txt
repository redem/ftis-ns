[{
		"id":"GLOBAL_TRY_CATCH",
		"title":"Den globala Prova Fånga fel",
		"error_msg":[
			{
				"id":"TRIGGERED_BY_USER", "message":"Fel utlöst av användare:" 
			} 
		]
	},{
		"id":"GLOBAL_VALUES",
		"title":"Globala värden",
		"error_msg":[
			{
				"id":"CONTACT_NS_ADMIN", "message":"För mer information ska du kontakta din NetSuite-administratör.",
				"id":"WARNING", "message":"Varning"
			} 
		]
	},{
		"id":"WTAX_SETUP",
		"title":"Konfiguration av källskatt",
		"fields":[
			{
				"id":"APPLIES_TO", 
				"label":"Gäller för",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Enskilda poster"},
					{"id":"TOTAL_AMT", "label":"Totalbelopp"}
				],
				"help":"Välj det grundbelopp som ska användas vid beräkning av källskatt."
			},{
				"id":"AUTO_APPLY", 
				"label":"Tillämpa källskatt automatiskt",
				"help":"Markera den här kryssrutan om du vill tillämpa källskatt automatiskt i transaktioner."
			},{
				"id":"AVAILABLE_PURC", 
				"label":"Tillgänglig vid inköp",
				"help":"Markera den här kryssrutan om du vill göra källskatt tillgänglig i inköpstransaktioner."
			},{
				"id":"NOT_ON_POS", 
				"label":"Inaktivera källskatt i inköpsorder",
				"help":"Markera rutan för att avaktivera källskatt i inköpsorder."
			},{
				"id":"NOT_ON_SOS", 
				"label":"Inaktivera källskatt i kundorder",
				"help":"Markera rutan för att avaktivera källskatt i kundorder."
			},{
				"id":"AVAILABLE_SALES", 
				"label":"Tillgänglig vid försäljning",
				"help":"Markera den här kryssrutan om du vill göra källskatt tillgänglig i försäljningstransaktioner."
			},{
				"id":"ENABLE_LOOKUP", 
				"label":"Aktivera skatteuppslag i transaktioner",
				"help":"Markera den här kryssrutan om du automatiskt vill slå upp den standardskattekod som definierats i enheten eller artikelposten i en transaktion."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "En konfigurationspost för källskatt har skapats för den valda nexus."
					}
				],
				"help":"Välj lämplig skattenexus för den här posten."
			},{
				"id":"TAX_POINT", 
				"label":"Skatteplats",
				"select": [
					{"id":"ACCRUAL", "label":"Vid periodisering"},
					{"id":"PAYMENT", "label":"Vid betalning"}
				],
				"help":"Den plats då en källskatteskuld, en källskattekredit eller en kostnad erkänns."
			}
		]
	},{
		"id":"WTAX_TYPE",
		"title":"Källskattetyp",
		"title_plural":"Källskattetyper",
		"fields":[
			{
				"id":"DESCRIPTION", 
				"label":"Beskrivning",
				"help":"Ange en beskrivning för den här källskattetypen, källskattekoden eller källskattegruppen."
			},{
				"id":"NAME", 
				"label":"Namn",
				"help":"Ange ett namn för den här källskattetypen eller källskattegruppen."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"help":"Välj lämplig skattenexus för den här posten.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Det finns redan en källskattetyp med samma namn i den valda nexus."
					}
				]
			},{
				"id":"PURC_ACCOUNT", 
				"label":"Konto för skuld-/inköpsskatt",
				"help":"Välj avstämningskontot för källskatt vid inköpstransaktioner.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Skuld-/inköpsskattekontot och tillgångs-/omsättningsskattekontot bör bokföras mot olika konton."
					}
				]
			},{
				"id":"SALES_ACCOUNT", 
				"label":"Konto för tillgångs-/omsättningsskatt",
				"help":"Välj avstämningskontot för källskatt vid försäljningstransaktioner."
			},{
				"id":"TAX_BASE", 
				"label":"Grundbelopp för källskatt",
				"select": [
					{"id":"GROSS_AMT", "label":"Bruttobelopp"},
					{"id":"NET_AMT", "label":"Nettobelopp"},
					{"id":"TAX_AMT", "label":"Skattebelopp"}
				],
				"help":"Välj det grundbelopp som ska användas vid beräkning av källskatt.",
				"error_msg": [
					{
						"id": "TAX_BASE_LOCK",
						"message":"Redigera basen källskatt är inte tillåtet eftersom det finns transaktioner i samband med denna källskatt typ."
					}
				]
			}
		]
	},{
		"id":"WTAX_CODE",
		"title":"Källskattekod",
		"title_plural":"Källskattekoder",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Tillgänglig den",
				"select": [
					{"id":"BOTH", "label":"Båda"},
					{"id":"PURCHASES", "label":"Inköp"},
					{"id":"SALES", "label":"Försäljning"}
				],
				"help":"Välj den typ av transaktioner som den här källskattekoden eller källskattegruppen kan tillämpas på."
			},{
				"id":"DESCRIPTION", 
				"label":"Beskrivning",
				"help":"Ange en beskrivning för den här källskattetypen, källskattekoden eller källskattegruppen."
			},{
				"id":"EFFECTIVE_FROM", 
				"label":"Giltig från",
				"help":"Ange det datum då källskattekoden ska börja gälla."
			},{
				"id":"INACTIVE", 
				"label":"Inaktiv",
				"help":"Markera den här kryssrutan om du vill inaktivera den här källskattekoden."
			},{
				"id":"INC_CHILDREN", 
				"label":"Inkludera underordnade",
				"help":"Inkludera dotterbolag."
			},{
				"id":"NAME", 
				"label":"Skattekod",
				"help":"Ange ett namn för den här skattekoden.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Det finns redan en &1 med samma namn i den valda källskattetypens nexus."
					}
				]
			},{
				"id":"PERCENTAGE_BASE", 
				"label":"Procentandel av grundbeloppet",
				"help":"Ange den procentandel av grundbeloppet som kommer att användas vid beräkning av källskatt.<br><br>Som standard har detta värdet: 100 %"
			},{
				"id":"RATE", 
				"label":"Sats",
				"help":"Ange lämplig källskattesats som en procentsats.<br><br>Exempel: 8 %<br><br>Den här procentsatsen kommer att användas när du väljer den här källskattekoden i transaktioner."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Bolag",
				"help":"Detta bestämmer vilka bolag den här källskattekoden eller källskattekodsgruppen kommer att vara tillgänglig för. Om du använder OneWorld visas det här fältet i NetSuite. I listrutan visas listan över tillgängliga bolag. I fältet Bolag visas överordnade bolag till vänster och deras underordnade bolag till höger, avgränsade med kolon."
			},{
				"id":"TAX_AGENCY", 
				"label":"Skattemyndighet",
				"help":"Skattemyndighet"
			},{
				"id":"VALID_UNTIL", 
				"label":"Giltig till",
				"help":"Ange det datum då källskattekoden ska sluta gälla.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Giltig till-datumet bör infalla efter Giltig från-datumet."
					}
				]
			},{
				"id":"WTAX_SETUP", 
				"label":"Konfiguration av källskatt",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Källskattetyp",
				"help":"Välj vilken typ av källskattekod eller källskattegrupp du vill skapa.<br><br>Skatteavstämningskontot för den här typen av källskatt anges som standard.<br><br>Du kan skapa en ny källskattetyp i Konfiguration > Källskatt > Skattetyper."
			},{
				"id":"WTAX_BASE", 
				"label":"Grundbelopp för källskatt",
				"help":"Välj det grundbelopp som ska användas vid beräkning av källskatt."
			},{
				"id":"STATE", 
				"label":"Delstat"
			},{
				"id":"COUNTRY", 
				"label":"Land"
			},{
				"id":"NEW", 
				"label":"Ny &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"notif_msg": [
			{
				"id":"NOTIFMSG1", 
				"message": "Om du redigerar den här posten kommer följande källskattegrupper som den är medlem i att uppdateras"
			}
		]
	},{
		"id":"WTAX_GROUP",
		"title":"Källskattegrupp",
		"title_plural":"Källskattegrupper",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Tillgänglig den",
				"select": [
					{"id":"PURCHASES", "label":"Inköp"},
					{"id":"SALES", "label":"Försäljning"}
				],
				"help":"Välj den typ av transaktioner som den här källskattekoden eller källskattegruppen kan tillämpas på."
			},{
				"id":"DESCRIPTION", 
				"label":"Beskrivning",
				"help":"Ange en beskrivning för den här källskattetypen, källskattekoden eller källskattegruppen."
			},{
				"id":"INACTIVE", 
				"label":"Inaktiv",
				"help":"Markera den här kryssrutan om du vill inaktivera den här källskattekoden."
			},{
				"id":"INC_CHILDREN", 
				"label":"Inkludera underordnade",
				"help":"Inkludera dotterbolag."
			},{
				"id":"NAME", 
				"label":"Namn",
				"help":"Ange ett namn för den här källskattetypen eller källskattegruppen."
			},{
				"id":"RATE", 
				"label":"Sats",
				"help":"Ange lämplig källskattesats som en procentsats.<br><br>Exempel: 8 %<br><br>Den här procentsatsen kommer att användas när du väljer den här källskattekoden i transaktioner."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Bolag",
				"help":"Detta bestämmer vilka bolag den här källskattekoden eller källskattekodsgruppen kommer att vara tillgänglig för. Om du använder OneWorld visas det här fältet i NetSuite. I listrutan visas listan över tillgängliga bolag. I fältet Bolag visas överordnade bolag till vänster och deras underordnade bolag till höger, avgränsade med kolon."
			},{
				"id":"WTAX_SETUP", 
				"label":"Konfiguration av källskatt",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Källskattetyp",
				"help":"Välj vilken typ av källskattekod eller källskattegrupp du vill skapa.<br><br>Skatteavstämningskontot för den här typen av källskatt anges som standard.<br><br>Du kan skapa en ny källskattetyp i Konfiguration > Källskatt > Skattetyper."
			},{
                "id":"WTAX_BASE", 
                "label": "Grundbelopp för källskatt",
				"help": "Visar skattebasen för den valda källskatt typ."
            },{
				"id":"NEW", 
				"label":"Ny &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"subtab": [
			{
				"id":"WTAX_CODES", 
				"label":"Källskattekoder",
				"fields": [
					{
						"id":"BASIS", 
						"label":"Grund"
					},{
						"id":"TYPE", 
						"label":"Källskattetyp"
					},{
						"id":"TAX_BASE", 
						"label":"Grundbelopp för källskatt"
					},{
						"id":"WTAX_CODE", 
						"label":"Källskattekod"
					},{
						"id":"RATE", 
						"label":"Sats"
					}
				]
			}
		],
		"error_msg": [
			{
				"id":"ERRMSG1", 
				"message": "Det finns redan en &1 med samma namn i den valda källskattetypens nexus."
			},{
				"id":"ERRMSG2", 
				"message": "Källskattekoder bör använda samma grundbelopp för källskatt. Kontrollera källskattetypens inställning."
			}
		]
	},{
		"id":"RECORD",
		"field": [
			{ "id":"NEW", "label":"Ny &1" },
			{ "id":"YES", "label":"Ja" },
			{ "id":"NO", "label":"Nej" }
		],
		"button": [
			{ "id":"NEW", "label":"Nytt" },
			{ "id":"CANCEL", "label":"Avbryt" },
			{ "id":"DELETE", "label":"Ta bort" },
			{ "id":"SAVE", "label":"Spara" }
		],
		"validation_msg":[
			{ "id":"DELETE_RECORD", "message":"Vill du ta bort den här posten?" }
		],
		"error_msg":[
			{ "id":"PROB_FOUND", "message":"Problem påträffades." }
		]
	},{
		"id":"TRANS_CS",
		"form_reload": [
			{
				"id":"RELOADMSG1", 
				"message": "Formuläret måste läsas in på nytt om källskatt ska kunna tillämpas. Ändringarna kommer inte att sparas."
			},{
				"id":"RELOADMSG2", 
				"message": "Formuläret måste läsas in på nytt. Ändringarna kommer inte att sparas."
			}
		],
		"form_details": [
			{
				"id":"WTAX_CODE", 
				"message": "Välj en källskattekod."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "För att visa nettobetalningsbelopp av källskatt, i Verkställ underfliken åter kontrollera 'Verkställ' rutan bredvid den faktura du vill betala."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "För att visa nettobetalningsbelopp av källskatt, i Verkställ underfliken åter kontrollera 'Verkställ' rutan bredvid räkningen du vill betala."
			}
		],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "Den totala källskatt Belopp för alla typer källskatt kan inte vara noll eller mindre. Detta inkluderar uppdelningen av de belopp för alla tillämpliga grupper källskatt tillämpas på transaktionen."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Tillämpa en transaktion rabatt stöds inte när källskatt punkten är satt till \"Vid periodisering.\""
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Tillämpa en transaktion rabatt stöds inte när källskatt punkten är satt till \"Vid periodisering.\""
			}
		]
	},{
		"id":"TRANS_UE",
		"field": [
			{ "id":"AMOUNT", "label":"Belopp" },
			{
				"id":"APPLIES_TO", 
				"label":"Gäller för",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Enskilda poster"},
					{"id":"TOTAL_AMT", "label":"Totalbelopp"}
				],
				"help":"Välj det grundbelopp som ska användas vid beräkning av källskatt."
			},
			{ "id":"APPLY_WH_TAX", "label":"Tillämpa källskatt?" },
			{
				"id":"BASE_AMT", 
				"label":"Grundbelopp",
				"help":"Det grundbelopp som används vid beräkning av källskatt."
			},
			{ "id":"DATE", "label":"Datum" },
			{ "id":"REF_NO", "label":"Ref.nr" },
			{ 
				"id":"TAX_AMOUNT", 
				"label":"Skattebelopp",
				"help":"Källskattebeloppet."
			},
			{ 
				"id":"TAX_CODE", 
				"label":"Skattekod",
				"help":"Den källskattekod eller källskattegrupp som ska tillämpas." 
			},
			{ 
				"id":"TAX_RATE", 
				"label":"Skattesats",
				"help": "Den källskattesats som ska tillämpas." 
			},
			{ "id":"TOTAL", "label":"Summa" },
			{ "id":"TYPE", "label":"Typ" },
			{ "id":"WITHHELD", "label":"Källskatt" },
			{ "id":"WTAX_AMT", "label":"Källskatt - Belopp" },
			{ "id":"WTAX_BASE_AMT", "label":"Källskatt - Grundbelopp" },
			{ "id":"WTAX_CODE", "label":"Källskatt - Skattekod " },
			{ "id":"WTAX_RATE", "label":"Källskatt - Skattesats" },
			{ "id":"BILL_PAYMENT", "label":"Bill Betalning" },
			{ "id":"INVOICE_PAYMENT", "label":"Faktura Betalning" },
			{ "id":"BILL_CREDIT", "label":"Fakturakredit" },
			{ "id":"CREDIT_MEMO", "label":"Kreditnota" }
		],
		"subtab":[
			{ "id":"WTAX", "label":"Källskatt" }
		],
		"button": [
			{ "id":"GO_BACK", "label":"Gå tillbaka" }
		],
		"field_msg": [
			{
				"id":"CANCEL_WTAX_BILL_CREDIT",
				"message":"Källskatt på fakturakrediten har annullerats från den annullerade leverantörsfakturabetalningen" 
			},{
				"id":"CANCEL_WTAX_CREDIT_MEMO",
				"message":"Källskatt på kreditnotan har annullerats från den annullerade fakturabetalningen" 
			},{
				"id":"AMOUNT", "message":"Belopp"
			},{
				"id":"WTAX_AMT",
				"message":"Withholding Tax Amount for &1"
			},{
				"id":"REF_NO", "message":"ref.nr"
			}
		],
		"error_msg": [
			{
				"id":"TRAN_LOCK_BILL_CREDIT",
				"message":"Den här fakturakrediten specificerar källskattebeloppet på den kopplade fakturan."
			}, {
				"id":"TRAN_LOCK_BILL_PAYMENT",
				"message":"Den här leverantörsfakturan har redan en betalning som applicerats mot den, netto efter tillämplig källskatt."
			}, {
				"id":"TRAN_LOCK_CREDIT_MEMO",
				"message":"Den här kreditnotan specificerar källskattebeloppet på den kopplade fakturan."
			}, {
				"id":"TRAN_LOCK_INVOICE_PAYMENT",
				"message":"Den här fakturan har redan en betalning som applicerats mot den, netto efter tillämplig källskatt."
			}, {
				"id":"TRAN_LOCK",
				"message":"Den här transaktionen får inte redigeras. För mer information ska du kontakta din NetSuite-administratör."
			}, {
				"id":"TRAN_LOCK_NS_INFO",
				"message":"NetSuite - Information"
			}, {
				"id":"TRAN_LOCK_WARNING",
				"message":"Varning"
			}, {
				"id":"SO_MULT_INVENTORY",
				"message":"På kundorder när multi-location Inventory är på, är ett värde för plats krävs."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"På faktura när multi-location Inventory är på, är ett värde för plats krävs."
			}, {
				"id":"ERROR",
				"message":"Fel"
			}, {
				"id":"VAT_CODE_CHECK_US",
				"message":"Se till att det finns minst 1 skattelagstiftningen med följande inställningar för den aktuella landet / dotterbolag: <br> Betygsätt = 0,00% <br> Tax Name =-inte skattepliktig-<br> Inaktiv = falskt"
			}, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"Se till att det finns åtminstone en skatt grupp med GST / PST eller HST-koder skatt med följande inställningar för aktuellt land / dotterbolag:<br><br>Betygsätt = 0.00%<br>Uteslut från mervärdesskatt Reports = sant (kryssrutan ska markeras)<br>Inaktiv = falsk (kryssruta ska inte märkas)"
			}, {
				"id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
				"message":"Se till att det finns minst 1 skattelagstiftningen med följande inställningar för den aktuella landet / dotterbolag: <br> Betygsätt = 0,00% <br> Exkludera från mervärdesskatt Rapporter = sant (kryssrutan ska markeras) <br> Inaktiv = falskt"
			}, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
		],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
	},{
		"id":"REPORT",
		"report": [
			{ "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Innehållen källskatt för leverantörer (detalj)" },
			{ "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Kvartalsrapport skatt som innehållits för Leverantör - Perioden" },
			{ "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Månatlig skatt som innehållits för Leverantör - Perioden" },
			{ "id":"WTAX_VENDOR_PDF", "title":"Lista över leverantörer för vilka källskatt har innehållits" },
			{ "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Källskatt, efter kunder (detalj)" }, 
			{ "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Källskatt, efter kunder" }, 
			{ "id":"FROM", "title":"Från" },
			{ "id":"TO", "title":"Till" },
			{ "id":"WTAX_CUSTOMER_PDF", "title":"Lista över kunder för vilka källskatt har innehållits" },
			{ "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Innehållen källskatt för leverantör - Översikt" },
			{ "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Innehållen källskatt för kund - Översikt" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Inköp, efter källskattekod - Detaljer" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"Förteckning över inköp genom vilka skatter som innehålls (Detalj)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Inköp, efter källskattekod - Översikt" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"Förteckning över inköp genom vilka skatter som innehålls (Sammanfattning)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Försäljning, efter källskattekod - Detaljer" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"Förteckning över försäljningen genom vilka skatter som innehålls (Detalj)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Försäljning, efter källskattekod - Översikt" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"Förteckning över försäljningen genom vilka skatter som innehålls (Sammanfattning)" }
		],
		"common_field": [
			{ "id":"AGENT_NAME", "label":"Namn på ombud för innehållen källskatt" },
			{ "id":"AMT_INCOME_PMNT", "label":"Belopp på utbetald inkomst" },
			{ "id":"AMT_TAX_WHELD", "label":"Källskattebelopp" },
			{ "id":"CLASSIFICATION", "label":"Klassificering" },
			{ "id":"COMPANY", "label":"Företag" },
			{ "id":"CORPORATION", "label":"Koncern (Registrerat namn)" },
			{ "id":"DESCRIPTION", "label":"Beskrivning" },
			{ "id":"END_OF_REPORT", "label":"Slut på rapporten" },
			{ "id":"FIRST_NAME", "label":"Förnamn" },
			{ "id":"GRAND_TOTAL", "label":"Slutsumma" },
			{ 
				"id":"INDIVIDUAL", 
				"label":"Privatperson",
				"long_label":"Privatperson (efternamn, förnamn, mellannamn)" 
			},
			{ "id":"LAST_NAME", "label":"Efternamn" },
			{ "id":"MEMO", "label":"PM" },
			{ "id":"MIDDLE_NAME", "label":"Mellannamn" },
			{ "id":"NEXUS", "label":"Nexus" },
			{ "id":"PAYEE_NAME", "label":"Betalningsmottagarens namn" },
			{ "id":"PERIOD_FROM", "label":"för perioden" },
			{ "id":"PERIOD_TO", "label":"till" },
			{ "id":"REFERENCE_NO", "label":"Transaktionsnummer" },
			{ "id":"SEQ_NO", "label":"Sekv.nr" },
			{ "id":"SUBSIDIARY", "label":"Bolag" },
			{ "id":"TAX_CODE", "label":"Skattekod" },
			{ "id":"TAX_PERIOD", "label":"Skatteperiod" },
			{ "id":"TAX_RATE", "label":"Källskattesats" },
			{ "id":"TAX_REG_NO", "label":"Momsregistreringsnummer" },
			{ "id":"TAX_TYPE", "label":"Skattetyp" },
			{ "id":"TIN", "label":"Skatte-ID" },
			{ "id":"TRANSACTION_TYPE", "label":"Transaktionstyp" },
			{ "id":"WTAX_RATE", "label":"Källskattesats" },
			{ "id":"NUM_ONE", "label":"1" },
			{ "id":"NUM_TWO", "label":"2" },
			{ "id":"NUM_THREE", "label":"3" },
			{ "id":"NUM_FOUR", "label":"4" },
			{ "id":"NUM_FIVE", "label":"5" },
			{ "id":"NUM_SIX", "label":"6" },
			{ "id":"NUM_SEVEN", "label":"7" },
			{ "id":"NUM_EIGHT", "label":"8" },
			{ "id":"NUM_NINE", "label":"9" },
			{ "id":"NUM_TEN", "label":"10" },
			{ "id":"NUM_ELEVEN", "label":"11" },
			{ "id":"NUM_TWELVE", "label":"12" }
		],
		"common_field_value":[
			{ "id":"NONE", "value":"Ingen" }
		],
		"button": [
			{ "id":"SUBMIT", "label":"Skicka" },
			{ "id":"PRINT_PDF", "label":"Skriv ut PDF" },
			{ "id":"SAVE_CSV", "label":"Exportera - CSV" },
			{ "id":"EXPORT_DAT", "label":"Exportera - DAT" },
			{ "id":"RETURN_TO_CRITERIA", "label":"Tillbaka till kriterier" }
		]
	}
]