[{
		"id":"GLOBAL_TRY_CATCH",
		"title":"Global Try Catch fout",
		"error_msg":[
			{
				"id":"TRIGGERED_BY_USER", "message":"Fout veroorzaakt door Gebruiker:"  
			} 
		]
	},{
		"id":"GLOBAL_VALUES",
		"title":"Global Values",
		"error_msg":[
			{
				"id":"CONTACT_NS_ADMIN", "message":"Neem voor meer informatie contact op met de NetSuite-beheerder.", 
				"id":"WARNING", "message":"Waarschuwing"
			} 
		]
	},{
		"id":"WTAX_SETUP",
		"title":"Voorheffing instellen",
		"fields":[
			{
				"id":"APPLIES_TO", 
				"label":"Van toepassing op",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Individuele regelitems"},
					{"id":"TOTAL_AMT", "label":"Totaalbedrag"}
				],
				"help":"Selecteer de basis voor het berekenen van voorheffing."
			},{
				"id":"AUTO_APPLY", 
				"label":"Voorheffing automatisch toepassen",
				"help":"Vink dit selectievakje aan om automatisch voorheffing toe te passen bij transacties."
			},{
				"id":"AVAILABLE_PURC", 
				"label":"Beschikbaar voor Aankopen",
				"help":"Vink dit selectievakje aan om voorheffing beschikbaar te maken in aankooptransacties."
			},{
                "id":"NOT_ON_POS", 
                "label":"Schakel bronbelasting in Inkooporders",
                "help":"Vink dit vakje aan de roerende voorheffing uit te schakelen in inkooporders."
            },{
                "id":"NOT_ON_SOS", 
                "label":"Schakel bronbelasting in Verkooporders",
                "help":"Vink dit vakje aan de roerende voorheffing uit te schakelen in verkooporders."
            },{
				"id":"AVAILABLE_SALES", 
				"label":"Beschikbaar voor Verkopen",
				"help":"Vink dit selectievakje aan om voorheffing beschikbaar te maken in verkooptransacties."
			},{
				"id":"ENABLE_LOOKUP", 
				"label":"Belasting bekijken voor transacties instellen",
				"help":"Vink dit selectievakje aan om de standaard belastingcode gedefinieerd in het entiteits- of itemrecord in een transactie automatisch op te zoeken."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Er is een record voor configuratie van voorheffing aangemaakt voor de geselecteerde nexus."
					}
				],
				"help":"Kies de juiste belastingnexus voor dit record."
			},{
				"id":"TAX_POINT", 
				"label":"Belastingdatum",
				"select": [
					{"id":"ACCRUAL", "label":"Op aangroei"},
					{"id":"PAYMENT", "label":"Op betaling"}
				],
				"help":"De datum waarvanaf de belasting, teruggaaf of uitgave wordt gerekend."
			}
		]
	},{
		"id":"WTAX_TYPE",
		"title":"Type voorheffing",
		"title_plural":"Types voorheffing",
		"fields":[
			{
				"id":"DESCRIPTION", 
				"label":"Beschrijving",
				"help":"Voer een beschrijving in van dit type voorheffing, belastingcode of belastinggroep."
			},{
				"id":"NAME", 
				"label":"Naam",
				"help":"Voer een naam in voor dit type voorheffing of belastinggroep."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"help":"Kies de juiste belastingnexus voor dit record.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Er bestaat al een type voorheffing met dezelfde naam in de geselecteerde nexus."
					}
				]
			},{
				"id":"PURC_ACCOUNT", 
				"label":"Rekening verplichtingen/aankoopbelasting",
				"help":"Kies de controlerekening voor voorheffing voor aankooptransacties.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "De rekeningen verplichtingen/aankoopbelasting en activa/verkoopbelasting moeten naar verschillende rekeningen boeken."
					}
				]
			},{
				"id":"SALES_ACCOUNT", 
				"label":"Rekening activa/verkoopbelasting",
				"help":"Kies de controlerekening voor voorheffing voor verkooptransacties."
			},{
				"id":"TAX_BASE", 
				"label":"Basis voorheffing",
				"select": [
					{"id":"GROSS_AMT", "label":"Bruto bedrag"},
					{"id":"NET_AMT", "label":"Netto bedrag"},
					{"id":"TAX_AMT", "label":"Belastingbedrag"}
				],
				"help":"Selecteer de basis voor het berekenen van voorheffing.",
				"error_msg": [
					{
						"id": "TAX_BASE_LOCK",
						"message":"Bewerken van de roerende voorheffing basis is niet toegestaan ​​omdat er transacties verbonden aan deze bronbelasting type."
					}
				]
			}
		]
	},{
		"id":"WTAX_CODE",
		"title":"Voorheffingscode",
		"title_plural":"Voorheffingscodes",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Beschikbaar op",
				"select": [
					{"id":"BOTH", "label":"Beide"},
					{"id":"PURCHASES", "label":"Aankopen"},
					{"id":"SALES", "label":"Verkoop"}
				],
				"help":"Kies het type transacties waarop deze voorheffingscode of belastinggroep toegepast kan worden."
			},{
				"id":"DESCRIPTION", 
				"label":"Beschrijving",
				"help":"Voer een beschrijving in van dit type voorheffing, belastingcode of belastinggroep."
			},{
				"id":"EFFECTIVE_FROM", 
				"label":"Geldig vanaf",
				"help":"Voer de datum in vanaf wanneer deze voorheffingscode in werking treedt."
			},{
				"id":"INACTIVE", 
				"label":"Niet actief",
				"help":"Vink dit selectievakje aan om deze voorheffingscode te deactiveren."
			},{
				"id":"INC_CHILDREN", 
				"label":"Inclusief onderliggende",
				"help":"Inclusief onderliggende dochterondernemingen."
			},{
				"id":"NAME", 
				"label":"Belastingcode",
				"help":"Voer een naam in voor deze belastingcode.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Er bestaat al een &1 met dezelfde naam in de geselecteerde nexus voor het type voorheffing."
					}
				]
			},{
				"id":"PERCENTAGE_BASE", 
				"label":"Percentage van het basisbedra",
				"help":"Voer het percentage in van het basisbedrag voor het berekenen van de in te houden belasting.<br><br>Dit staat standaard op: 100%"
			},{
				"id":"RATE", 
				"label":"Tarief",
				"help":"Voer het tarief voor voorheffing in als percentage.<br><br>Bijvoorbeeld 8%<br><br>Dit is het percentage dat wordt gebruikt wanneer u deze voorheffingscode kiest voor transacties."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Dochterondernemingen",
				"help":"Dit bepaalt voor welke dochterondernemingen deze voorheffingscode of belastinggroep beschikbaar is. Als u OneWorld gebruikt geeft NetSuite dit veld weer. De vervolgkeuzelijst bevat de beschikbare dochterondernemingen. In het veld Dochterondernemingen zijn moedermaatschappijen (links) gescheiden van hun dochters d.m.v. een dubbele punt."
			},{
				"id":"TAX_AGENCY", 
				"label":"Belastingkantoor",
				"help":"Belastingkantoor"
			},{
				"id":"VALID_UNTIL", 
				"label":"Geldig tot",
				"help":"Voer de datum in tot wanneer deze voorheffingscode geldig is.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "De \"geldig tot\"-datum moet later zijn dan de ingangsdatum."
					}
				]
			},{
				"id":"WTAX_SETUP", 
				"label":"Voorheffing instellen",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Type voorheffing",
				"help":"Kies het type voorheffing of belastinggroep die u wilt maken.<br><br>De controlerekening Belastingen voor dit type voorheffing wordt standaard bepaalt.<br><br>U kunt een nieuw type voorheffing maken via Instellingen > Voorheffing > Belastingtypes."
			},{
				"id":"WTAX_BASE", 
				"label":"Basis voorheffing",
				"help":"Selecteer de basis voor het berekenen van voorheffing."
			},{
				"id":"STATE", 
				"label":"Staat"
			},{
				"id":"COUNTRY", 
				"label":"Land"
			},{
				"id":"NEW", 
				"label":"Nieuw &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"notif_msg": [
			{
				"id":"NOTIFMSG1", 
				"message": "Als u dit record bijwerkt worden de volgende groepen voor voorheffing bijgewerkt waar dit record deel van uitmaakt" 
			}
		]
	},{
		"id":"WTAX_GROUP",
		"title":"Voorheffingsgroep",
		"title_plural":"Voorheffingsgroepen",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Beschikbaar op",
				"select": [
					{"id":"PURCHASES", "label":"Aankopen"},
					{"id":"SALES", "label":"Verkoop"}
				],
				"help":"Kies het type transacties waarop deze voorheffingscode of belastinggroep toegepast kan worden."
			},{
				"id":"DESCRIPTION", 
				"label":"Beschrijving",
				"help":"Voer een beschrijving in van dit type voorheffing, belastingcode of belastinggroep."
			},{
				"id":"INACTIVE", 
				"label":"Niet actief",
				"help":"Vink dit selectievakje aan om deze voorheffingscode te deactiveren."
			},{
				"id":"INC_CHILDREN", 
				"label":"Inclusief onderliggende",
				"help":"Inclusief onderliggende dochterondernemingen."
			},{
				"id":"NAME", 
				"label":"Naam",
				"help":"Voer een naam in voor dit type voorheffing of belastinggroep."
			},{
				"id":"RATE", 
				"label":"Tarief",
				"help":"Voer het tarief voor voorheffing in als percentage.<br><br>Bijvoorbeeld 8%<br><br>Dit is het percentage dat wordt gebruikt wanneer u deze voorheffingscode kiest voor transacties."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Dochterondernemingen",
				"help":"Dit bepaalt voor welke dochterondernemingen deze voorheffingscode of belastinggroep beschikbaar is. Als u OneWorld gebruikt geeft NetSuite dit veld weer. De vervolgkeuzelijst bevat de beschikbare dochterondernemingen. In het veld Dochterondernemingen zijn moedermaatschappijen (links) gescheiden van hun dochters d.m.v. een dubbele punt."
			},{
				"id":"WTAX_SETUP", 
				"label":"Voorheffing instellen",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Type voorheffing",
				"help":"Kies het type voorheffing of belastinggroep die u wilt maken.<br><br>De controlerekening Belastingen voor dit type voorheffing wordt standaard bepaalt.<br><br>U kunt een nieuw type voorheffing maken via Instellingen > Voorheffing > Belastingtypes."
			},{
                "id":"WTAX_BASE", 
                "label": "Basis voorheffing",
				"help": "Geeft de fiscale waarde van de geselecteerde Bronbelasting Type."
            },{
				"id":"NEW", 
				"label":"Nieuw &1" 
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"subtab": [
			{
				"id":"WTAX_CODES", 
				"label":"Voorheffingscodes",
				"fields": [
					{
						"id":"BASIS", 
						"label":"Basis"
					},{
						"id":"TYPE", 
						"label":"Type voorheffing"
					},{
						"id":"TAX_BASE", 
						"label":"Basis voorheffing"
					},{
						"id":"WTAX_CODE", 
						"label":"Voorheffingscode"
					},{
						"id":"RATE", 
						"label":"Tarief"
					}
				]
			}
		],
		"error_msg": [
			{
				"id":"ERRMSG1", 
				"message": "Er bestaat al een &1 met dezelfde naam in de geselecteerde nexus voor het type voorheffing."
			},{
				"id":"ERRMSG2", 
				"message": "Codes voor voorheffing moeten hetzelfde basisbedrag voor voorheffing hebben. Controleer de instelling voor het type voorheffing."
			}
		]
	},{
		"id":"RECORD",
		"field": [
			{ "id":"NEW", "label":"Nieuw &1" },
			{ "id":"YES", "label":"Ja" },
			{ "id":"NO", "label":"Nee" }
		],
		"button": [
			{ "id":"NEW", "label":"Nieuw" },
			{ "id":"CANCEL", "label":"Annuleren" },
			{ "id":"DELETE", "label":"Verwijderen" },
			{ "id":"SAVE", "label":"Opslaan" }
		],
		"validation_msg":[
			{ "id":"DELETE_RECORD", "message":"Weet u zeker dat u dit record wilt verwijderen?" }
		],
		"error_msg":[
			{ "id":"PROB_FOUND", "message":"Er hebben zich fouten voorgedaan." }
		]
	},{
		"id":"TRANS_CS",
		"form_reload": [
			{
				"id":"RELOADMSG1", 
				"message": "Het formulier moet opnieuw worden geladen om voorheffing te kunnen toepassen. Wijzigingen worden niet opgeslagen."
			},{
				"id":"RELOADMSG2", 
				"message": "Het formulier moet opnieuw worden geladen. Wijzigingen worden niet opgeslagen."
			}
		],
		"form_details": [
			{
				"id":"WTAX_CODE", 
				"message": "Kies een voorheffingscode."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "Om het te betalen bedrag na aftrek van de roerende voorheffing, in de Toepassen subtabblad opnieuw controleren 'Toepassen' box naast de factuur die u wilt betalen tonen."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "Om het te betalen bedrag na aftrek van de roerende voorheffing, in de Toepassen subtabblad opnieuw controleren 'Toepassen' vakje naast de rekening die u wilt betalen tonen."
			}
		],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "De totale Roerende Voorheffing Bedrag voor alle Roerende Voorheffing soorten kan niet nul of minder. Dit omvat de uitsplitsing van de bedragen voor alle toepasselijke bronbelasting groepen toegepast op de transactie."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Het toepassen van een transactie korting wordt niet ondersteund wanneer de roerende voorheffing is ingesteld op \"Op aangroei.\""
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Het toepassen van een transactie korting wordt niet ondersteund wanneer de roerende voorheffing is ingesteld op \"Op aangroei.\""
			}
		]
	},{
		"id":"TRANS_UE",
		"field": [
			{ "id":"AMOUNT", "label":"Bedrag" },
			{
				"id":"APPLIES_TO", 
				"label":"Van toepassing op",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Individuele regelitems"},
					{"id":"TOTAL_AMT", "label":"Totaalbedrag"}
				],
				"help":"Selecteer de basis voor het berekenen van voorheffing."
			},
			{ "id":"APPLY_WH_TAX", "label":"Voorheffing toepassen?" },
			{
				"id":"BASE_AMT", 
				"label":"Basisbedrag",
				"help":"De basis voor berekening van voorheffing."
			},
			{ "id":"DATE", "label":"Datum" },
			{ "id":"REF_NO", "label":"Referentie" },
			{ 
				"id":"TAX_AMOUNT", 
				"label":"Belastingsbedrag",
				"help":"Het bedrag aan voorheffing."
			},
			{ 
				"id":"TAX_CODE", 
				"label":"Belastingcode",
				"help":"De voorheffingscode of belastinggroep." 
			},
			{ 
				"id":"TAX_RATE", 
				"label":"Belastingtarief",
				"help": "Het tarief voor de voorheffing." 
			},
			{ "id":"TOTAL", "label":"Totaal" },
			{ "id":"TYPE", "label":"Type" },
			{ "id":"WITHHELD", "label":"Ingehouden" },
			{ "id":"WTAX_AMT", "label":"Bedrag voorheffing" },
			{ "id":"WTAX_BASE_AMT", "label":"Basisbedrag voorheffing" },
			{ "id":"WTAX_CODE", "label":"Voorheffingscode" },
			{ "id":"WTAX_RATE", "label":"Tarief voorheffing" },
			{ "id":"BILL_PAYMENT", "label":"Bill Betaling" },
			{ "id":"INVOICE_PAYMENT", "label":"Factuur Betaling" },
			{ "id":"BILL_CREDIT", "label":"Factuur krediet" },
			{ "id":"CREDIT_MEMO", "label":"Creditnota" }
		],
		"subtab":[
			{ "id":"WTAX", "label":"Roerende Voorheffing" }
		],
		"button": [
			{ "id":"GO_BACK", "label":"Terug" }
		],
		"field_msg": [
			{
				"id":"CANCEL_WTAX_BILL_CREDIT",
				"message":"Geannuleerde factuurkrediet voorheffing van ongeldige betaling" 
			},{
				"id":"CANCEL_WTAX_CREDIT_MEMO",
				"message":"Geannuleerde creditnota voorheffing van ongeldige factuurbetaling" 
			},{
				"id":"AMOUNT", "message":"Bedrag"
			},{
				"id":"WTAX_AMT",
				"message":"Withholding Tax Amount for &1" 
			},{
				"id":"REF_NO", "message":"Referentie"
			}
		],
		"error_msg": [
			{
				"id":"TRAN_LOCK_BILL_CREDIT",
				"message":"Deze rekeningnota geeft het bedrag aan voorheffing weer voor de betreffende rekening."
			}, {
				"id":"TRAN_LOCK_BILL_PAYMENT",
				"message":"Er is al een betaling toegepast op deze rekening, na aftrek van de voorheffing."
			}, {
				"id":"TRAN_LOCK_CREDIT_MEMO",
				"message":"Deze creditnota geeft het bedrag aan voorheffing weer voor de betreffende factuur."
			}, {
				"id":"TRAN_LOCK_INVOICE_PAYMENT",
				"message":"Er is al een betaling toegepast op deze factuur, na aftrek van de voorheffing."
			}, {
				"id":"TRAN_LOCK",
				"message":"Deze transactie kan niet worden bewerkt. Neem voor meer informatie contact op met de NetSuite-beheerder."
			}, {
				"id":"TRAN_LOCK_NS_INFO",
				"message":"Informatie over NetSuite"
			}, {
				"id":"TRAN_LOCK_WARNING",
				"message":"Waarschuwing"
			}, {
				"id":"SO_MULT_INVENTORY",
				"message":"Op Sales Order wanneer Multi-Location Inventory is ingeschakeld, wordt een waarde voor Locatie vereist."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"Op Factuur bij Multi-Location Inventory is ingeschakeld, wordt een waarde voor Locatie vereist."
			}, {
				"id":"ERROR",
				"message":"Fout"
			}, {
				"id":"VAT_CODE_CHECK_US",
				"message":"Zorg ervoor dat er tenminste 1 tax code met de volgende instellingen voor de huidige land / dochter: <br> Tarief = 0.00% <br> BTW Name =-niet belastbaar-<br> Inactief = valse"
			}, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"Zorg ervoor dat er minstens een fiscale eenheid met GST / PST of HST BTW-codes met de volgende instellingen voor het huidige land / dochter:<br><br>Tarief = 0.00%<br>Exclusief BTW Van Rapporten = waar (selectievakje moet worden gemarkeerd)<br>Inactief = vals (selectievakje mag NIET worden gemarkeerd)"
			}, {
				"id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
				"message":"Zorg ervoor dat er tenminste 1 tax code met de volgende instellingen voor de huidige land / dochter: <br> Tarief = 0.00% <br> uit te sluiten van BTW Reports = ware (vakje moet worden aangekruist) <br> Inactieve = valse"
			}, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
		],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
	},{
		"id":"REPORT",
		"report": [
			{ "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Voorheffing ingehouden voor leveranciers (detail)" },
			{ "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Driemaandelijks ingehouden voor Vendor - Periode" },
			{ "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Maandelijkse ingehouden voor Vendor - Periode" },
			{ "id":"WTAX_VENDOR_PDF", "title":"Lijst van leveranciers waarvoor voorheffing is ingehouden" },
			{ "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Belasting ingehouden door klanten (detail)" }, 
			{ "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Belasting ingehouden door klanten" },
			{ "id":"FROM", "title":"Van" },
			{ "id":"TO", "title":"Tot" },
			{ "id":"WTAX_CUSTOMER_PDF", "title":"Lijst van klanten waarvoor voorheffing is ingehouden" },
			{ "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Overzicht Voorheffing voor leverancier" },
			{ "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Overzicht Voorheffing voor klant" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Detail Aankopen op voorheffingscode" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"Lijst van aankopen door die belastingen werden ingehouden (Detail)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Overzicht Aankopen op voorheffingscode" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"Lijst van aankopen door die belastingen werden ingehouden (Samenvatting)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Detail Verkopen op voorheffingscode" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"Lijst van de omzet, waardoor belastingen werden ingehouden (Detail)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Overzicht Verkopen op voorheffingscode" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"Lijst van de omzet, waardoor belastingen werden ingehouden (Samenvatting)" }
		],
		"common_field": [
			{ "id":"AGENT_NAME", "label":"Naam inhoudende partij" },
			{ "id":"AMT_INCOME_PMNT", "label":"Bedrag inkomstenbetaling" },
			{ "id":"AMT_TAX_WHELD", "label":"Bedrag voorheffing" },
			{ "id":"CLASSIFICATION", "label":"Classificatie" },
			{ "id":"COMPANY", "label":"Bedrijf" },
			{ "id":"CORPORATION", "label":"Bedrijf (geregistreerde naam)" },
			{ "id":"DESCRIPTION", "label":"Beschrijving" },
			{ "id":"END_OF_REPORT", "label":"Einde rapport" },
			{ "id":"FIRST_NAME", "label":"Voornaam" },
			{ "id":"GRAND_TOTAL", "label":"Totaal generaal" },
			{ 
				"id":"INDIVIDUAL", 
				"label":"Individueel",
				"long_label":"Individueel (achternaam, voornaam, overige namen)" 
			},
			{ "id":"LAST_NAME", "label":"Achternaam" },
			{ "id":"MEMO", "label":"Memo" },
			{ "id":"MIDDLE_NAME", "label":"Overige naam" },
			{ "id":"NEXUS", "label":"Nexus" },
			{ "id":"PAYEE_NAME", "label":"Naam ontvanger" },
			{ "id":"PERIOD_FROM", "label":"voor de periode" },
			{ "id":"PERIOD_TO", "label":"tot" },
			{ "id":"REFERENCE_NO", "label":"Transactie nummer" },
			{ "id":"SEQ_NO", "label":"Reeksnummer" },
			{ "id":"SUBSIDIARY", "label":"Dochteronderneming" },
			{ "id":"TAX_CODE", "label":"Belastingcode" },
			{ "id":"TAX_PERIOD", "label":"Belastingperiode" },
			{ "id":"TAX_RATE", "label":"Tarief voorheffing" },
			{ "id":"TAX_REG_NO", "label":"Belastingnummer" },
			{ "id":"TAX_TYPE", "label":"Belastingtype" },
			{ "id":"TIN", "label":"Btw-nummer" },
			{ "id":"TRANSACTION_TYPE", "label":"Transactie type" },
			{ "id":"WTAX_RATE", "label":"Tarief voorheffing" },
			{ "id":"NUM_ONE", "label":"1" },
			{ "id":"NUM_TWO", "label":"2" },
			{ "id":"NUM_THREE", "label":"3" },
			{ "id":"NUM_FOUR", "label":"4" },
			{ "id":"NUM_FIVE", "label":"5" },
			{ "id":"NUM_SIX", "label":"6" },
			{ "id":"NUM_SEVEN", "label":"7" },
			{ "id":"NUM_EIGHT", "label":"8" },
			{ "id":"NUM_NINE", "label":"9" },
			{ "id":"NUM_TEN", "label":"10" },
			{ "id":"NUM_ELEVEN", "label":"11" },
			{ "id":"NUM_TWELVE", "label":"12" }
		],
		"common_field_value":[
			{ "id":"NONE", "value":"Geen" }
		],
		"button": [
			{ "id":"SUBMIT", "label":"Indienen" },
			{ "id":"PRINT_PDF", "label":"PDF afdrukken" },
			{ "id":"SAVE_CSV", "label":"Exporteren - CSV" },
			{ "id":"EXPORT_DAT", "label":"Exporteren - DAT" },
			{ "id":"RETURN_TO_CRITERIA", "label":"Terug naar criteria" }
		]
	}
]