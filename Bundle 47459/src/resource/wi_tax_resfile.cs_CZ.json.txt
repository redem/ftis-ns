[{
		"id":"GLOBAL_TRY_CATCH",
		"title":"Globální Try Catch chyba",
		"error_msg":[
			{
				"id":"TRIGGERED_BY_USER", "message":"Chyba způsobená uživatelem:" 
			} 
		]
	},{
		"id":"GLOBAL_VALUES",
		"title":"Globální hodnoty",
		"error_msg":[
			{
				"id":"CONTACT_NS_ADMIN", "message":"Provádění úprav není u této transakce povoleno.", 
				"id":"WARNING", "message":"Varování"
			} 
		]
	},{
		"id":"WTAX_SETUP",
		"title":"Nastavení srážkové daně",
		"fields":[
			{
				"id":"APPLIES_TO", 
				"label":"Platí pro",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Jednotlivé položky řádku"},
					{"id":"TOTAL_AMT", "label":"Celková částka"}
				],
				"help":"Vyberte základ pro výpočet srážkové daně."
			},{
				"id":"AUTO_APPLY", 
				"label":"Použít automaticky srážkovou daň",
				"help":"Zaškrtnutím tohoto políčka automaticky použijete srážkovou daň v transakcích."
			},{
				"id":"AVAILABLE_PURC", 
				"label":"K dispozici pro nákupy",
				"help":"Zaškrtnutím tohoto políčka zpřístupníte srážkovou daň v nákupních transakcích."
			},{
                "id":"NOT_ON_POS", 
                "label":"Zakázat srážkovou daň objednávek",
                "help":"Zaškrtněte, pokud chcete deaktivovat srážkovou daň v objednávek."
            },{
                "id":"NOT_ON_SOS", 
                "label":"Zakázat Srážková daň v objednávkách",
                "help":"Zaškrtněte, pokud chcete deaktivovat srážkovou daň v prodejních objednávek."
            },{
				"id":"AVAILABLE_SALES", 
				"label":"K dispozici pro prodej",
				"help":"Zaškrtnutím tohoto políčka zpřístupníte srážkovou daň v prodejních transakcích."
			},{
				"id":"ENABLE_LOOKUP", 
				"label":"Povolit vyhledávání daně v transakcích",
				"help":"Zaškrtnutím tohoto políčka automaticky vyhledáte standardní kód daně definovaný v entitě nebo záznamu položky v příslušné transakci."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Pro vybraný nexus byl vytvořen záznam nastavení srážkové daně."
					}
				],
				"help":"Vyberte příslušný daňový nexus pro tento záznam."
			},{
				"id":"TAX_POINT", 
				"label":"Datum zdanitelného plnění",
				"select": [
					{"id":"ACCRUAL", "label":"Podle časového rozlišení"},
					{"id":"PAYMENT", "label":"Podle platby"}
				],
				"help":"Datum, kdy dochází k daňové povinnosti, kreditu nebo výdajům."
			}
		]
	},{
		"id":"WTAX_TYPE",
		"title":"Typ srážkové daně",
		"title_plural":"Typy srážkové daně",
		"fields":[
			{
				"id":"DESCRIPTION", 
				"label":"Popis",
				"help":"Zadejte popis tohoto typu srážkové daně, kódu daně nebo daňové skupiny."
			},{
				"id":"NAME", 
				"label":"Název",
				"help":"Zadejte název tohoto typu srážkové daně nebo daňové skupiny."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"help":"Vyberte příslušný daňový nexus pro tento záznam.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Typ srážkové daně se stejným názvem již ve vybraném nexusu existuje."
					}
				]
			},{
				"id":"PURC_ACCOUNT", 
				"label":"Daňový účet závazků/nákupů",
				"help":"Vyberte kontrolní účet srážkové daně pro nákupní transakce.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Daňový účet závazků/nákupu a daňový účet aktiv/prodeje by měly účtovat na různých účtech."
					}
				]
			},{
				"id":"SALES_ACCOUNT", 
				"label":"Daňový účet aktiv/prodeje",
				"help":"Vyberte kontrolní účet srážkové daně pro prodejní transakce."
			},{
				"id":"TAX_BASE", 
				"label":"Základ srážkové daně",
				"select": [
					{"id":"GROSS_AMT", "label":"Hrubá částka"},
					{"id":"NET_AMT", "label":"Čistá částka"},
					{"id":"TAX_AMT", "label":"Částka daně"}
				],
				"help":"Vyberte základ pro výpočet srážkové daně.",
				"error_msg": [
					{
						"id": "TAX_BASE_LOCK",
						"message":"Úprava srážkové daně základ není povoleno, protože tam jsou transakce spojené s tímto typem srážkové daně."
					}
				]
			}
		]
	},{
		"id":"WTAX_CODE",
		"title":"Kód srážkové daně",
		"title_plural":"Kódy srážkové daně",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"K dispozici pro",
				"select": [
					{"id":"BOTH", "label":"Oba"},
					{"id":"PURCHASES", "label":"Nákup"},
					{"id":"SALES", "label":"Prodej"}
				],
				"help":"Vyberte typ transakcí, na něž je možné použít tento kód srážkové daně nebo daňovou skupinu."
			},{
				"id":"DESCRIPTION", 
				"label":"Popis",
				"help":"Zadejte popis tohoto typu srážkové daně, kódu daně nebo daňové skupiny."
			},{
				"id":"EFFECTIVE_FROM", 
				"label":"Platné od",
				"help":"Zadejte datum, kdy tento kód srážkové daně vstupuje v platnost."
			},{
				"id":"INACTIVE", 
				"label":"Neaktivní",
				"help":"Zaškrtnutím tohoto políčka deaktivujte tento kód srážkové daně."
			},{
				"id":"INC_CHILDREN", 
				"label":"Zahrnout podřazené",
				"help":"Zahrnout podřazené pobočky."
			},{
				"id":"NAME", 
				"label":"Kód daně",
				"help":"Zadejte název tohoto kódu daně.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "&1 se stejným názvem již ve vybraném nexusu typu srážkové daně existuje."
					}
				]
			},{
				"id":"PERCENTAGE_BASE", 
				"label":"Procento základu",
				"help":"Zadejte % základu daně, které se použije pro výpočet srážkové daně.<br><br>Standardní nastavení: 100%"
			},{
				"id":"RATE", 
				"label":"Sazba",
				"help":"Zadejte příslušnou sazbu srážkové daně v procentech.<br><br>Příklad: 8 % <br><br>Tato procentní hodnota se vypočítá, jestliže vyberete kód srážkové daně v transakcích."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Pobočky",
				"help":"Tímto určíte pobočky, které budou mít k dispozici tento kód srážkové daně nebo daňovou skupinu. Toto pole se v systému NetSuite zobrazí v případě, že používáte službu OneWorld. V rozevírací nabídce se zobrazí seznam dostupných poboček. V poli Pobočky oddělují dvojtečky nadřazené objekty (vlevo) od podřazených objektů (vpravo)."
			},{
				"id":"TAX_AGENCY", 
				"label":"Daňová agentura",
				"help":"Daňová agentura"
			},{
				"id":"VALID_UNTIL", 
				"label":"Platné do",
				"help":"Zadejte datum, kdy platnost tohoto kódu srážkové daně vyprší.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Datum Platné do by mělo být pozdější než datum Platné od."
					}
				]
			},{
				"id":"WTAX_SETUP", 
				"label":"Nastavení srážkové daně",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Typ srážkové daně",
				"help":"Vyberte typ právě vytvářeného kódu srážkové daně nebo daňové skupiny.<br><br>Daňový kontrolní účet tohoto typu srážkové daně je vybrán standardně.<br><br>V části Nastavení > Srážková daň > Typy daní můžete vytvořit nový typ srážkové daně."
			},{
				"id":"WTAX_BASE", 
				"label":"Základ srážkové daně",
				"help":"Vyberte základ pro výpočet srážkové daně."
			},{
				"id":"STATE", 
				"label":"Stát"
			},{
				"id":"COUNTRY", 
				"label":"Země"
			},{
				"id":"NEW", 
				"label":"Nový &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"notif_msg": [
			{
				"id":"NOTIFMSG1", 
				"message": "Úpravou tohoto záznamu dojde k aktualizaci následujících skupin srážkové daně, které jsou členem"
			}
		]
	},{
		"id":"WTAX_GROUP",
		"title":"Skupina srážkové daně",
		"title_plural":"Skupiny srážkové daně",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"K dispozici pro",
				"select": [
					{"id":"PURCHASES", "label":"Nákup"},
					{"id":"SALES", "label":"Prodej"}
				],
				"help":"Vyberte typ transakcí, na něž je možné použít tento kód srážkové daně nebo daňovou skupinu."
			},{
				"id":"DESCRIPTION", 
				"label":"Popis",
				"help":"Zadejte popis tohoto typu srážkové daně, kódu daně nebo daňové skupiny."
			},{
				"id":"INACTIVE", 
				"label":"Neaktivní",
				"help":"Zaškrtnutím tohoto políčka deaktivujte tento kód srážkové daně."
			},{
				"id":"INC_CHILDREN", 
				"label":"Zahrnout podřazené",
				"help":"Zahrnout podřazené pobočky."
			},{
				"id":"NAME", 
				"label":"Název",
				"help":"Zadejte název tohoto typu srážkové daně nebo daňové skupiny."
			},{
				"id":"RATE", 
				"label":"Sazba",
				"help":"Zadejte příslušnou sazbu srážkové daně v procentech.<br><br>Příklad: 8 %<br><br>Tato procentní hodnota se vypočítá, jestliže vyberete kód srážkové daně v transakcích."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Pobočky",
				"help":"Tímto určíte pobočky, které budou mít k dispozici tento kód srážkové daně nebo daňovou skupinu. Toto pole se v systému NetSuite zobrazí v případě, že používáte službu OneWorld. V rozevírací nabídce se zobrazí seznam dostupných poboček. V poli Pobočky oddělují dvojtečky nadřazené objekty (vlevo) od podřazených objektů (vpravo)."
			},{
				"id":"WTAX_SETUP", 
				"label":"Nastavení srážkové daně",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Typ srážkové daně",
				"help":"Vyberte typ právě vytvářeného kódu srážkové daně nebo daňové skupiny.<br><br>Daňový kontrolní účet tohoto typu srážkové daně je vybrán standardně.<br><br>V části Nastavení > Srážková daň > Typy daní můžete vytvořit nový typ srážkové daně."
			},{
                "id":"WTAX_BASE", 
                "label": "Základ srážkové daně",
				"help": "Zobrazí daňový základ vybraného typu srážková daň."
            },{
				"id":"NEW", 
				"label":"Nový &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"subtab": [
			{
				"id":"WTAX_CODES", 
				"label":"Kódy srážkové daně",
				"fields": [
					{
						"id":"BASIS", 
						"label":"Základ"
					},{
						"id":"TYPE", 
						"label":"Typ srážkové daně"
					},{
						"id":"TAX_BASE", 
						"label":"Základ srážkové daně"
					},{
						"id":"WTAX_CODE", 
						"label":"Kód srážkové daně"
					},{
						"id":"RATE", 
						"label":"Sazba"
					}
				]
			}
		],
		"error_msg": [
			{
				"id":"ERRMSG1", 
				"message": "&1 se stejným názvem již ve vybraném nexusu typu srážkové daně existuje."
			},{
				"id":"ERRMSG2", 
				"message": "Kódy srážkové daně by měly mít stejný základ srážkové daně. Zkontrolujte nastavení typu srážkové daně."
			}
		]
	},{
		"id":"RECORD",
		"field": [
			{ "id":"NEW", "label":"Nový &1" },
			{ "id":"YES", "label":"Ano" },
			{ "id":"NO", "label":"Ne" }
		],
		"button": [
			{ "id":"NEW", "label":"Nový" },
			{ "id":"CANCEL", "label":"Zrušit" },
			{ "id":"DELETE", "label":"Odstranit" },
			{ "id":"SAVE", "label":"Uložit" }
		],
		"validation_msg":[
			{ "id":"DELETE_RECORD", "message":"Opravdu chcete odstranit tento záznam?" }
		],
		"error_msg":[
			{ "id":"PROB_FOUND", "message":"Byly nalezeny problémy." }
		]
	},{
		"id":"TRANS_CS",
		"form_reload": [
			{
				"id":"RELOADMSG1", 
				"message": "Chcete-li použít srážkovou daň, je nutné formulář načíst znovu. Provedené změny nebudou uloženy."
			},{
				"id":"RELOADMSG2", 
				"message": "Formulář je nutné opětovně načíst. Provedené změny nebudou uloženy."
			}
		],
		"form_details": [
			{
				"id":"WTAX_CODE", 
				"message": "Vyberte kód srážkové daně."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "Chcete-li zobrazit platební částka po odečtení srážkové daně, v Použít podkartu re-check 'Použít' box vedle faktury, kterou chcete zaplatit."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "Chcete-li zobrazit platební částka po odečtení srážkové daně, v Použít podkartu re-check 'Použít' box vedle zákona, kterou chcete zaplatit."
			}
		],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "Celková Srážková daň Částka pro všechny typy srážková daň nemůže být nula nebo méně. To zahrnuje rozpis částek všech použitelných skupin srážková daň uplatňovaných na transakci."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Použití transakce slevu není podporována, pokud je srážková daň bod nastaveno na \"Podle časového rozlišení.\""
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Použití transakce slevu není podporována, pokud je srážková daň bod nastaveno na \"Podle časového rozlišení.\""
			}
		]
	},{
		"id":"TRANS_UE",
		"field": [
			{ "id":"AMOUNT", "label":"Částka" },
			{
				"id":"APPLIES_TO", 
				"label":"Platí pro",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Jednotlivé položky řádku"},
					{"id":"TOTAL_AMT", "label":"Celková částka"}
				],
				"help":"Vyberte základ pro výpočet srážkové daně."
			},
			{ "id":"APPLY_WH_TAX", "label":"Použít srážkovou daň?" },
			{
				"id":"BASE_AMT", 
				"label":"Základ daně",
				"help":"Základ pro výpočet srážkové daně."
			},
			{ "id":"DATE", "label":"Datum" },
			{ "id":"REF_NO", "label":"Ref. č." },
			{ 
				"id":"TAX_AMOUNT", 
				"label":"Částka daně",
				"help":"Částka srážkové daně."
			},
			{ 
				"id":"TAX_CODE", 
				"label":"Kód daně",
				"help":"Platný kód srážkové daně nebo daňové skupiny." 
			},
			{ 
				"id":"TAX_RATE", 
				"label":"Daňová sazba",
				"help": "Platná sazba srážkové daně." 
			},
			{ "id":"TOTAL", "label":"Celkem" },
			{ "id":"TYPE", "label":"Typ" },
			{ "id":"WITHHELD", "label":"Sražená částka" },
			{ "id":"WTAX_AMT", "label":"Částka srážkové daně" },
			{ "id":"WTAX_BASE_AMT", "label":"Základ srážkové daně" },
			{ "id":"WTAX_CODE", "label":"Kód srážkové daně" },
			{ "id":"WTAX_RATE", "label":"Sazba srážkové daně" },
			{ "id":"BILL_PAYMENT", "label":"Bill platba" },
			{ "id":"INVOICE_PAYMENT", "label":"Faktura platba" },
			{ "id":"BILL_CREDIT", "label":"Vyúčtování kreditu" },
			{ "id":"CREDIT_MEMO", "label":"Poznámka ke kreditu" }
		],
		"subtab":[
			{ "id":"WTAX", "label":"Srážková daň" }
		],
		"button": [
			{ "id":"GO_BACK", "label":"Přejít zpět" }
		],
		"field_msg": [
			{
				"id":"CANCEL_WTAX_BILL_CREDIT",
				"message":"Zrušené vyúčtování kreditu srážkové daně ze zrušené platby vyúčtování"
			},{
				"id":"CANCEL_WTAX_CREDIT_MEMO",
				"message":"Zrušená poznámka ke kreditu srážkové daně ze zrušené platby faktury"
			},{
				"id":"AMOUNT", "message":"Částka"
			},{
				"id":"WTAX_AMT",
				"message":"Withholding Tax Amount for &1"
			},{
				"id":"REF_NO", "message":"Ref. č."
			}
		],
		"error_msg": [
			{
				"id":"TRAN_LOCK_BILL_CREDIT",
				"message":"Toto vyúčtování kreditu reflektuje částku srážkové daně souvisejícího vyúčtování."
			}, {
				"id":"TRAN_LOCK_BILL_PAYMENT",
				"message":"Na toto vyúčtování je již použita platba bez příslušné srážkové daně."
			}, {
				"id":"TRAN_LOCK_CREDIT_MEMO",
				"message":"Tato poznámka ke kreditu reflektuje částku srážkové daně související faktury."
			}, {
				"id":"TRAN_LOCK_INVOICE_PAYMENT",
				"message":"Na tuto fakturu je již použita platba bez příslušné srážkové daně."
			}, {
				"id":"TRAN_LOCK",
				"message":"Provádění úprav není u této transakce povoleno. Další informace vám poskytne správce systému NetSuite."
			}, {
				"id":"TRAN_LOCK_NS_INFO",
				"message":"Informace o systému NetSuite"
			}, {
				"id":"TRAN_LOCK_WARNING",
				"message":"Varování"
			}, {
				"id":"SO_MULT_INVENTORY",
				"message":"Na prodejní objednávky, kdy Multi-Umístění Inventory je na, je nutné hodnotu pro umístění."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"Na faktuře, kdy Multi-Místo Inventory je na, je nutné hodnotu pro umístění."
			}, {
				"id":"ERROR",
				"message":"Chyba"
			}, {
				"id":"VAT_CODE_CHECK_US",
				"message":"Ujistěte se, že je alespoň 1 daňový kód s následujícím nastavením pro aktuální země / dceřiné společnosti: <br>Sazba = 0,00% <br> Daňové Název =-nepodléhá dani-teď <br>Neaktivní = falešný"
			}, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"Prosím, ujistěte se, že je přinejmenším daň skupina s GST / HST PST nebo daňových kódů s následujícími nastaveními pro aktuální zemi / dceřiné společnosti:<br><br>Sazba = 0.00%<br>Vyloučit z DPH Reports = skutečný (políčko by mělo být označeno)<br>Neaktivní = falešný (políčko by mělo být označeno)"
			}, {
				"id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
				"message":"Ujistěte se, že je alespoň 1 daňový kód s následujícím nastavením pro aktuální zemi / dceřiné společnosti: <br>Sazba = 0,00% <br>Vyloučit z DPH zprávách = výrazný (checkbox by měl být označen) <br> Neaktivní = falešný"
			}, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
		],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
	},{
		"id":"REPORT",
		"report": [
			{ "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Daň sražená dodavatelům (Podrobnosti)" },
			{ "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Čtvrtletní Sražená daň pro dodavatele - Období" },
			{ "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Měsíční Sražená daň pro dodavatele - Období" },
			{ "id":"WTAX_VENDOR_PDF", "title":"Seznam dodavatelů, jimž byly sraženy daně" },
			{ "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Částka daně sražená zákazníky (Podrobnosti)" }, 
			{ "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Částka daně sražená zákazníky" }, 
			{ "id":"FROM", "title":"Od" },
			{ "id":"TO", "title":"Na" },
			{ "id":"WTAX_CUSTOMER_PDF", "title":"Seznam zákazníků, jimž byly sraženy daně" },
			{ "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Sražená daň za Prodejci (Shrnutí)" },
			{ "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Sražená daň podle zákazníků (Shrnutí)" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Nákupy podle zákona srážková daň (Podrobnosti)" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"Seznam nákupů, které byly sraženy daně (Podrobnosti)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Nákupy podle zákona srážková daň (Shrnutí)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"Seznam nákupů, které byly sraženy daně (Shrnutí)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Prodej od zákoníku srážková daň (Podrobnosti)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"Seznam prodejů, které byly sraženy daně (Podrobnosti)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Prodej od zákoníku srážková daň (Shrnutí)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"Seznam prodejů, které byly sraženy daně (Shrnutí)" }
		],
		"common_field": [
			{ "id":"AGENT_NAME", "label":"Název subjektu srážejícího daň" },
			{ "id":"AMT_INCOME_PMNT", "label":"Částka příjmové platby" },
			{ "id":"AMT_TAX_WHELD", "label":"Částka sražené daně" },
			{ "id":"CLASSIFICATION", "label":"Klasifikace" },
			{ "id":"COMPANY", "label":"Společnost" },
			{ "id":"CORPORATION", "label":"Korporace (registrovaný název)" },
			{ "id":"DESCRIPTION", "label":"Popis" },
			{ "id":"END_OF_REPORT", "label":"Konec sestavy" },
			{ "id":"FIRST_NAME", "label":"Křestní jméno" },
			{ "id":"GRAND_TOTAL", "label":"Celková suma" },
			{ 
				"id":"INDIVIDUAL", 
				"label":"Jednotlivec",
				"long_label":"Jednotlivec (příjmení, křestní jméno, prostřední jméno)" 
			},
			{ "id":"LAST_NAME", "label":"Příjmení" },
			{ "id":"MEMO", "label":"Poznámka" },
			{ "id":"MIDDLE_NAME", "label":"Prostřední jméno" },
			{ "id":"NEXUS", "label":"Nexus" },
			{ "id":"PAYEE_NAME", "label":"Jméno příjemce platby" },
			{ "id":"PERIOD_FROM", "label":"pro období" }, 
			{ "id":"PERIOD_TO", "label":"až" },
			{ "id":"REFERENCE_NO", "label":"Číslo transakce" },
			{ "id":"SEQ_NO", "label":"Pořadové číslo" },
			{ "id":"SUBSIDIARY", "label":"Pobočka" },
			{ "id":"TAX_CODE", "label":"Kód daně" },
			{ "id":"TAX_PERIOD", "label":"Zdaňovací období" },
			{ "id":"TAX_RATE", "label":"Sazba srážkové daně" },
			{ "id":"TAX_REG_NO", "label":"Daňové identifikační číslo" },
			{ "id":"TAX_TYPE", "label":"Typ daně" },
			{ "id":"TIN", "label":"Daňové ID" },
			{ "id":"TRANSACTION_TYPE", "label":"Typ transakce" },
			{ "id":"WTAX_RATE", "label":"Sazba srážkové daně" },
			{ "id":"NUM_ONE", "label":"1" },
			{ "id":"NUM_TWO", "label":"2" },
			{ "id":"NUM_THREE", "label":"3" },
			{ "id":"NUM_FOUR", "label":"4" },
			{ "id":"NUM_FIVE", "label":"5" },
			{ "id":"NUM_SIX", "label":"6" },
			{ "id":"NUM_SEVEN", "label":"7" },
			{ "id":"NUM_EIGHT", "label":"8" },
			{ "id":"NUM_NINE", "label":"9" },
			{ "id":"NUM_TEN", "label":"10" },
			{ "id":"NUM_ELEVEN", "label":"11" },
			{ "id":"NUM_TWELVE", "label":"12" }
		],
		"common_field_value":[
			{ "id":"NONE", "value":"Žádný" }
		],
		"button": [
			{ "id":"SUBMIT", "label":"Odeslat" },
			{ "id":"PRINT_PDF", "label":"Tisk PDF" },
			{ "id":"SAVE_CSV", "label":"Exportovat - CSV" },
			{ "id":"EXPORT_DAT", "label":"Exportovat - DAT" },
			{ "id":"RETURN_TO_CRITERIA", "label":"Zpět na kritéria" }
		]
	}
]