[{
		"id":"GLOBAL_TRY_CATCH",
		"title":"Глобальная попытке поймать ошибку",
		"error_msg":[
			{
				"id":"TRIGGERED_BY_USER", "message":"Ошибка вызвана пользователя:" 
			} 
		]
	},{
		"id":"GLOBAL_VALUES",
		"title":"Глобальные значения",
		"error_msg":[
			{
				"id":"CONTACT_NS_ADMIN", "message":"Для получения дополнительной информации обратитесь к администратору NetSuite.", 
				"id":"WARNING", "message":"ПРЕДУПРЕЖДЕНИЕ"
			} 
		]
	},{
		"id":"WTAX_SETUP",
		"title":"Настройка налога на источники дохода",
		"fields":[
			{
				"id":"APPLIES_TO", 
				"label":"Применено к",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Отдельные позиции строки"},
					{"id":"TOTAL_AMT", "label":"Итоговая сумма"}
				],
				"help":"Выберите основу для расчета налога на источники дохода."
			},{
				"id":"AUTO_APPLY", 
				"label":"Автоматическое применение налога на источники дохода",
				"help":"Отметьте это поле, чтобы автоматически применять налог на источники дохода в операциях."
			},{
				"id":"AVAILABLE_PURC", 
				"label":"Доступно в закупках",
				"help":"Отметьте это поле, чтобы сделать налог на источники дохода доступным в операциях закупок."
			},{
                "id":"NOT_ON_POS", 
                "label":"Отключить Удержание налога в Заказы",
                "help":"Установите этот флажок, чтобы отключить налога в заказах."
            },{
                "id":"NOT_ON_SOS", 
                "label":"Отключить Удержание налога в Заказы",
                "help":"Установите этот флажок, чтобы отключить налога в заказах."
            },{
				"id":"AVAILABLE_SALES", 
				"label":"Доступно в продажах",
				"help":"Отметьте это поле, чтобы сделать налог на источники дохода доступным в операциях продаж."
			},{
				"id":"ENABLE_LOOKUP", 
				"label":"Искать коды налогов в операциях",
				"help":"Отметьте это поле, чтобы искать код налога по умолчанию, определенный в записи предприятия или товара в операции."
			},{
				"id":"NEXUS", 
				"label":"Физическое присутствие",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Запись настройки налога на источники дохода создана для выбранного физического присутствия."
					}
				],
				"help":"Выберите соответствующее физическое присутствие для этой записи."
			},{
				"id":"TAX_POINT", 
				"label":"Дата начисления налога",
				"select": [
					{"id":"ACCRUAL", "label":"По принципу начисления"},
					{"id":"PAYMENT", "label":"По принципу оплаты"}
				],
				"help":"Дата начисления обязательств, кредита или уплаты налога на источники дохода."
			}
		]
	},{
		"id":"WTAX_TYPE",
		"title":"Тип налога на источники дохода",
		"title_plural":"Типы налога на источники дохода",
		"fields":[
			{
				"id":"DESCRIPTION", 
				"label":"Описание",
				"help":"Введите описание этого типа налога на источники дохода, кода налога или группы налога."
			},{
				"id":"NAME", 
				"label":"Имя",
				"help":"Введите имя этого типа налога на источники дохода или группы налога."
			},{
				"id":"NEXUS", 
				"label":"Физическое присутствие",
				"help":"Выберите соответствующее физическое присутствие для этой записи.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Тип налога на источники дохода с таким же названием уже существует в выбранном физическом присутствии."
					}
				]
			},{
				"id":"PURC_ACCOUNT", 
				"label":"Счет налога с обязательств/закупок",
				"help":"Выберите счет контроля налога на источники дохода для операций закупок..",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Налоговый счет обязательств/закупок и налоговый счет активов/продаж следует провести по разным счетам."
					}
				]
			},{
				"id":"SALES_ACCOUNT", 
				"label":"Счет налога с активов/продаж",
				"help":"Выберите счет контроля налога на источники дохода для операций продаж."
			},{
				"id":"TAX_BASE", 
				"label":"Основа налога на источники дохода",
				"select": [
					{"id":"GROSS_AMT", "label":"Сумма с налогами"},
					{"id":"NET_AMT", "label":"Чистая сумма"},
					{"id":"TAX_AMT", "label":"Сумма налога"}
				],
				"help":"Выберите основу для расчета налога на источники дохода.",
				"error_msg": [
					{
						"id": "TAX_BASE_LOCK",
						"message":"Редактирование базы подоходного налога не допускается, так как есть операции, связанные с этим типом налога."
					}
				]
			}
		]
	},{
		"id":"WTAX_CODE",
		"title":"Код налога на источники дохода",
		"title_plural":"Коды налога на источники дохода",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"доступных на",
				"select": [
					{"id":"BOTH", "label":"ОБА"},
					{"id":"PURCHASES", "label":"Закупки"},
					{"id":"SALES", "label":"Продажи"}
				],
				"help":"Выберите тип операции, к которой можно применить код или группу налога на источники дохода."
			},{
				"id":"DESCRIPTION", 
				"label":"Описание",
				"help":"Введите описание этого типа налога на источники дохода, кода налога или группы налога."
			},{
				"id":"EFFECTIVE_FROM", 
				"label":"Вступает в силу с",
				"help":"Введите дату, на которую код налога на источники дохода вступает в силу."
			},{
				"id":"INACTIVE", 
				"label":"Неактивно",
				"help":"Отметьте это поле, чтобы отключить этот код налога на источники дохода."
			},{
				"id":"INC_CHILDREN", 
				"label":"Включить дочерние объекты",
				"help":"Включить дочерние компании."
			},{
				"id":"NAME", 
				"label":"Код налога",
				"help":"Введите название для этого кода налога.",
				"error_msg": [
					{
						"id":"ERRMSG1",
						"message": "&1 с таким же названием уже существует в выбранном физическом присутствии типа налога на источники дохода."
					}
				]
			},{
				"id":"PERCENTAGE_BASE", 
				"label":"Процентное соотношение от базовой",
				"help":"Введите процентное соотношение от базовой суммы, которая используется для расчета налога на источники дохода.<br><br>По умолчанию установлена на 100%"
			},{
				"id":"RATE", 
				"label":"Цена",
				"help":"Введите соответствующую ставку налога на источники дохода в процентном отношении.<br><br>Пример: 8%<br><br>Это процентное отношение рассчитывается, когда вы выбираете этот код налога на источники дохода в операциях."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Дочерние организации",
				"help":"Это определяет, у каких дочерних организаций доступен данный код налога на источники дохода или группа кодов налога. Если вы используете OneWorld, то NetSuite отображает это поле. Раскрывающийся список отображает доступные дочерние компании. В поле \"Дочерняя компания\", запятые отделяют родительские элементы (слева) от дочерних элементов (справа)."
			},{
				"id":"TAX_AGENCY", 
				"label":"Налоговое ведомство",
				"help":"Налоговое ведомство"
			},{
				"id":"VALID_UNTIL", 
				"label":"Действителен до",
				"help":"Введите дату, до которой действителен код налога на источники дохода.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Дата \"Действительно до\" должна быть позже даты \"Действует с\"."
					}
				]
			},{
				"id":"WTAX_SETUP", 
				"label":"Настройка налога на источники дохода",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Тип налога на источники дохода",
				"help":"Выберите тип налога на источники дохода или группу налога, который вы создаете.<br><br>Счет налогового контроля для этого типа налога на источники дохода выбирается по умолчанию.<br><br>Вы можете создать новый тип налога на источники дохода в Настройки > Налог на источники дохода > Типы налога."
			},{
				"id":"WTAX_BASE", 
				"label":"Основа налога на источники дохода",
				"help":"Выберите основу для расчета налога на источники дохода."
			},{
				"id":"STATE", 
				"label":"Регион"
			},{
				"id":"COUNTRY", 
				"label":"Страна"
			},{
				"id":"NEW", 
				"label":"Новый &1"
			},{
				"id":"NEXUS", 
				"label":"Физическое присутствие"
			}
		],
		"notif_msg": [
			{
				"id":"NOTIFMSG1", 
				"message": "Изменение этой записи обновит следующие группы налога на источники дохода, членами которых она является" 
			}
		]
	},{
		"id":"WTAX_GROUP",
		"title":"Группа налога на источники дохода",
		"title_plural":"Группы налога на источники дохода",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"ОБА",
				"select": [
					{"id":"PURCHASES", "label":"Закупки"},
					{"id":"SALES", "label":"Продажи"}
				],
				"help":"Выберите тип операции, к которой можно применить код или группу налога на источники дохода."
			},{
				"id":"DESCRIPTION", 
				"label":"Описание",
				"help":"Введите описание этого типа налога на источники дохода, кода налога или группы налога."
			},{
				"id":"INACTIVE", 
				"label":"Неактивно",
				"help":"Отметьте это поле, чтобы отключить этот код налога на источники дохода."
			},{
				"id":"INC_CHILDREN", 
				"label":"Включить дочерние объекты",
				"help":"Включить дочерние компании."
			},{
				"id":"NAME", 
				"label":"Имя",
				"help":"Введите имя этого типа налога на источники дохода или группы налога."
			},{
				"id":"RATE", 
				"label":"Цена",
				"help":"Введите соответствующую ставку налога на источники дохода в процентном отношении.<br><br>Пример: 8%<br><br>Это процентное отношение рассчитывается, когда вы выбираете этот код налога на источники дохода в операциях."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Дочерние организации",
				"help":"Это определяет, у каких дочерних организаций доступен данный код налога на источники дохода или группа кодов налога. Если вы используете OneWorld, то NetSuite отображает это поле. Раскрывающийся список отображает доступные дочерние компании. В поле \"Дочерняя компания\", запятые отделяют родительские элементы (слева) от дочерних элементов (справа). "
			},{
				"id":"WTAX_SETUP", 
				"label":"Настройка налога на источники дохода",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Тип налога на источники дохода",
				"help":"Выберите тип налога на источники дохода или группу налога, который вы создаете.<br><br>Счет налогового контроля для этого типа налога на источники дохода выбирается по умолчанию.<br><br>Вы можете создать новый тип налога на источники дохода в Настройки > Налог на источники дохода > Типы налога."
			},{
                "id":"WTAX_BASE", 
                "label": "Основа налога на источники дохода",
				"help": "Отображает налоговой базы выбранного типа налога."
            },{
				"id":"NEW", 
				"label":"Новый &1" 
			},{
				"id":"NEXUS", 
				"label":"Физическое присутствие"
			}
		],
		"subtab": [
			{
				"id":"WTAX_CODES", 
				"label":"Коды налога на источники дохода",
				"fields": [
					{
						"id":"BASIS", 
						"label":"основа"
					},{
						"id":"TYPE", 
						"label":"Тип налога на источники дохода"
					},{
						"id":"TAX_BASE", 
						"label":"Основа налога на источники дохода"
					},{
						"id":"WTAX_CODE", 
						"label":"Код налога на источники дохода"
					},{
						"id":"RATE", 
						"label":"Цена"
					}
				]
			}
		],
		"error_msg": [
			{
				"id":"ERRMSG1", 
				"message": "&1 с таким же названием уже существует в выбранном физическом присутствии типа налога на источники дохода."
			},{
				"id":"ERRMSG2", 
				"message": "Кода налога на источники дохода должны иметь одинаковую основу для налога на источники дохода. Проверьте настройки типа налога на источники дохода."
			}
		]
	},{
		"id":"RECORD",
		"field": [
			{ "id":"NEW", "label":"Новый &1" }, 
			{ "id":"YES", "label":"Да" },
			{ "id":"NO", "label":"Нет" }
		],
		"button": [
			{ "id":"NEW", "label":"Новый" },
			{ "id":"CANCEL", "label":"Отменить" },
			{ "id":"DELETE", "label":"Удалить" },
			{ "id":"SAVE", "label":"Сохранить" }
		],
		"validation_msg":[
			{ "id":"DELETE_RECORD", "message":"Вы действительно хотите удалить эту запись?" }
		],
		"error_msg":[
			{ "id":"PROB_FOUND", "message":"Найдены проблемы." }
		]
	},{
		"id":"TRANS_CS",
		"form_reload": [
			{
				"id":"RELOADMSG1", 
				"message": "Форму необходимо загрузить повторно, чтобы применить налог на источники дохода. Внесенные изменения не будут сохранены."
			},{
				"id":"RELOADMSG2", 
				"message": "Форму необходимо загрузить повторно. Внесенные изменения не будут сохранены."
			}
		],
		"form_details": [
			{
				"id":"WTAX_CODE", 
				"message": "Выберите код налога на источники дохода."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "Чтобы показать, чистая сумма платежа налога у источника выплаты, в списке Применить вкладки повторно установите флажок 'Применить' рядом с счет-фактуру вы хотите заплатить."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "Чтобы показать, чистая сумма платежа налога у источника выплаты, в списке Применить вкладки повторно установите флажок 'Применить' рядом с Законопроект вы хотите заплатить."
			}
		],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "Общая сумма налога для всех типов Удержание налога не может быть нулевой или меньше. Это включает в себя разбивку суммы по всем действующим Удержание налога Группы применяются на сделку."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Применение сделки скидка не поддерживается, когда точка налога установлена ​​в положение \"По принципу начисления\"."
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Применение сделки скидка не поддерживается, когда точка налога установлена ​​в положение \"По принципу начисления\"."
			}
		]
	},{
		"id":"TRANS_UE",
		"field": [
			{ "id":"AMOUNT", "label":"Сумма" },
			{
				"id":"APPLIES_TO", 
				"label":"Применено к",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Отдельные позиции строки"},
					{"id":"TOTAL_AMT", "label":"Итоговая сумма"}
				],
				"help":"Выберите основу для расчета налога на источники дохода."
			},
			{ "id":"APPLY_WH_TAX", "label":"Применить налог на источники дохода?" },
			{
				"id":"BASE_AMT", 
				"label":"Базовая сумма",
				"help":"Основа для расчета налога на источники дохода."
			},
			{ "id":"DATE", "label":"Дата" },
			{ "id":"REF_NO", "label":"# основания" },
			{ 
				"id":"TAX_AMOUNT", 
				"label":"Сумма налога",
				"help":"Сумма налога на источники дохода."
			},
			{ 
				"id":"TAX_CODE", 
				"label":"Код налога",
				"help":"Применяемый код или группа налога на источники дохода." 
			},
			{ 
				"id":"TAX_RATE", 
				"label":"Ставка налога",
				"help": "Применяемая ставка налога на источники дохода." 
			},
			{ "id":"TOTAL", "label":"Итого" },
			{ "id":"TYPE", "label":"Тип" },
			{ "id":"WITHHELD", "label":"Удержано" },
			{ "id":"WTAX_AMT", "label":"Сумма налога на источники дохода" },
			{ "id":"WTAX_BASE_AMT", "label":"Базовая сумма налога на источники дохода" },
			{ "id":"WTAX_CODE", "label":"Код налога на источники дохода" },
			{ "id":"WTAX_RATE", "label":"Ставка налога на источники дохода" },
			{ "id":"BILL_PAYMENT", "label":"Оплата счетов" },
			{ "id":"INVOICE_PAYMENT", "label":"оплата счетов" },
			{ "id":"BILL_CREDIT", "label":"Возврат денег поставщику" },
			{ "id":"CREDIT_MEMO", "label":"Кредитовое уведомление" }
		],
		"subtab":[
			{ "id":"WTAX", "label":"Удержание налога" }
		],
		"button": [
			{ "id":"GO_BACK", "label":"Вернуться" }
		],
		"field_msg": [
			{
				"id":"CANCEL_WTAX_BILL_CREDIT",
				"message":"Отменен кредит суммы налога на источники дохода из аннулированного платежа по счету"
			},{
				"id":"CANCEL_WTAX_CREDIT_MEMO",
				"message":"Отменено кредитовое авизо налога на источники дохода из аннулированного платежа по счету-фактуре" 
			},{
				"id":"AMOUNT", "message":"Сумма"
			},{
				"id":"WTAX_AMT",
				"message":"Withholding Tax Amount for &1"
			},{
				"id":"REF_NO", "message":"# основания"
			}
		],
		"error_msg": [
			{
				"id":"TRAN_LOCK_BILL_CREDIT",
				"message":"Этот кредит счета отражает сумму налога на источники дохода для соответствующего счета."
			}, {
				"id":"TRAN_LOCK_BILL_PAYMENT",
				"message":"К этому счету уже проведен платеж, за вычетом применимого налога на источники дохода."
			}, {
				"id":"TRAN_LOCK_CREDIT_MEMO",
				"message":"Это кредитное авизо отражает сумму налога на источники дохода для соответствующего счета-фактуры."
			}, {
				"id":"TRAN_LOCK_INVOICE_PAYMENT",
				"message":"К этому счету-фактуре уже проведен платеж, за вычетом применимого налога на источники дохода."
			}, {
				"id":"TRAN_LOCK",
				"message":"Изменение этой операции не разрешено. Для получения дополнительной информации обратитесь к администратору NetSuite."
			}, {
				"id":"TRAN_LOCK_NS_INFO",
				"message":"Сведения о NetSuite"
			}, {
				"id":"TRAN_LOCK_WARNING",
				"message":"ПРЕДУПРЕЖДЕНИЕ"
			}, {
				"id":"SO_MULT_INVENTORY",
				"message":"На заказ продажи, когда Multi-Место инвентаризации включен, значение расположение не требуется."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"На счета, когда Multi-Место инвентаризации включен, значение расположение не требуется."
			}, {
				"id":"ERROR",
				"message":"Ошибка"
			}, {
				"id":"VAT_CODE_CHECK_US",
				"message":"Пожалуйста, убедитесь, что есть по крайней мере 1 Налогового Кодекса со следующими параметрами для текущей страны / дочернего общества: <br>ставка = 0,00% <br> Имя Налогового = не облагается налогом, <br> неактивных = Снята"
			}, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"Пожалуйста, убедитесь, что есть по крайней мере налогу группе с GST / PST или кодов налога HST со следующими параметрами для текущей страны / дочернего общества:<br><br>скорость = 0,00%<br>Исключить из отчетов НДС = правда (галочка должна быть отмечена)<br>Неактивные = ложный (флажок, должны быть отмечены)"
			}, {
				"id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
				"message":"Пожалуйста, убедитесь, что есть по крайней мере 1 Налогового Кодекса со следующими параметрами для текущей страны / дочернего общества: <br> Rate = 0,00% <br> Исключить из отчетов НДС = Отмеченные (флажок должен быть отмечен) <br> неактивных = Снята"
			}, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
		],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
	},{
		"id":"REPORT",
		"report": [
			{ "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Удержанный налог с поставщиков (подробно)" },
			{ "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Ежеквартальный удержанного налога для Vendor - Период" },
			{ "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Ежемесячный налог, удержанный за Vendor - Период" },
			{ "id":"WTAX_VENDOR_PDF", "title":"Список поставщиков, с которых удержаны налоги" },
			{ "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Налог, удержанный заказчиками (подробное описание)" },
			{ "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Налог, удержанный заказчиками" },
			{ "id":"FROM", "title":"С" },
			{ "id":"TO", "title":"до" },
			{ "id":"WTAX_CUSTOMER_PDF", "title":"Список клиентов, с которых удержаны налоги" },
			{ "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Сводное описание удержанного налога для поставщика" },
			{ "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Сводное описание удержанного налога для клиента" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Подробное описание закупок по коду налога на источники дохода" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"Список покупок с помощью которых удерживаются налоги (Детали)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Сводное описание закупок по коду налога на источники дохода" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"Список покупок с помощью которых удерживаются налоги (Резюме)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Подробное описание продаж по коду налога на источники дохода" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"Список продаж по которой налоги, удержанные (Подробности)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Сводное описание продаж по коду налога на источники дохода" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"Список продаж которых удерживаются налоги (Резюме)" }
		],
		"common_field": [
			{ "id":"AGENT_NAME", "label":"Имя агента, удерживающего налоги" },
			{ "id":"AMT_INCOME_PMNT", "label":"Сумма выплаченных доходов" },
			{ "id":"AMT_TAX_WHELD", "label":"Сумма удержанного налога" },
			{ "id":"CLASSIFICATION", "label":"Классификация" },
			{ "id":"COMPANY", "label":"Организация" },
			{ "id":"CORPORATION", "label":"Корпорация (зарегистрированное имя)" },
			{ "id":"DESCRIPTION", "label":"Описание" },
			{ "id":"END_OF_REPORT", "label":"Конец отчета" },
			{ "id":"FIRST_NAME", "label":"Имя" },
			{ "id":"GRAND_TOTAL", "label":"Общий итог" },
			{ 
				"id":"INDIVIDUAL", 
				"label":"Физ. лицо",
				"long_label":"Физ. лицо (фамилия, имя, отчество)" 
			},
			{ "id":"LAST_NAME", "label":"Фамилия" },
			{ "id":"MEMO", "label":"Комментарий" },
			{ "id":"MIDDLE_NAME", "label":"Отчество" },
			{ "id":"NEXUS", "label":"Физическое присутствие" },
			{ "id":"PAYEE_NAME", "label":"Имя получателя платежа" },
			{ "id":"PERIOD_FROM", "label":"для периода: от" }, 
			{ "id":"PERIOD_TO", "label":"до" },
			{ "id":"REFERENCE_NO", "label":"номер операции" },
			{ "id":"SEQ_NO", "label":"№ п/п" },
			{ "id":"SUBSIDIARY", "label":"Дочерняя организация" },
			{ "id":"TAX_CODE", "label":"Код налога" },
			{ "id":"TAX_PERIOD", "label":"Налоговый период" },
			{ "id":"TAX_RATE", "label":"Ставка налога" },
			{ "id":"TAX_REG_NO", "label":"Регистрационный номер налога" },
			{ "id":"TAX_TYPE", "label":"Тип налога" },
			{ "id":"TIN", "label":"ИНН" },
			{ "id":"TRANSACTION_TYPE", "label":"Вид операции" },
			{ "id":"WTAX_RATE", "label":"Ставка налога на источники дохода" },
			{ "id":"NUM_ONE", "label":"1" },
			{ "id":"NUM_TWO", "label":"2" },
			{ "id":"NUM_THREE", "label":"3" },
			{ "id":"NUM_FOUR", "label":"4" },
			{ "id":"NUM_FIVE", "label":"5" },
			{ "id":"NUM_SIX", "label":"6" },
			{ "id":"NUM_SEVEN", "label":"7" },
			{ "id":"NUM_EIGHT", "label":"8" },
			{ "id":"NUM_NINE", "label":"9" },
			{ "id":"NUM_TEN", "label":"10" },
			{ "id":"NUM_ELEVEN", "label":"11" },
			{ "id":"NUM_TWELVE", "label":"12" }
		],
		"common_field_value":[
			{ "id":"NONE", "value":"НЕТ" }
		],
		"button": [
			{ "id":"SUBMIT", "label":"Передать" },
			{ "id":"PRINT_PDF", "label":"Распечатать PDF" },
			{ "id":"SAVE_CSV", "label":"Экспорт - CSV" },
			{ "id":"EXPORT_DAT", "label":"Экспорт - DAT" },
			{ "id":"RETURN_TO_CRITERIA", "label":"Возврат к критериям" }
		]
	}
]