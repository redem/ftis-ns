[{
        "id":"GLOBAL_TRY_CATCH",
        "title":"Global Try Catch Error",
        "error_msg":[
            {
                "id":"TRIGGERED_BY_USER", "message":"Error triggered by USER: "
            } 
        ]
    },{
        "id":"GLOBAL_VALUES",
        "title":"Global Values",
        "error_msg":[
            {
                "id":"CONTACT_NS_ADMIN", "message":"For more information contact your NetSuite Administrator.",
                "id":"WARNING", "message":"Warning"
            } 
        ]
    },{
        "id":"WTAX_SETUP",
        "title":"Withholding Tax Setup",
        "fields":[
            {
                "id":"APPLIES_TO", 
                "label":"Applies To",
                "select": [
                    {"id":"INDIVIDUAL_LINES", "label":"Individual Line Items"},
                    {"id":"TOTAL_AMT", "label":"Total Amount"}
                ],
                "help":"Select the basis for computing withholding tax."
            },{
                "id":"AUTO_APPLY", 
                "label":"Auto Apply Withholding Tax",
                "help":"Check this box to automatically apply withholding tax in transactions."
            },{
                "id":"AVAILABLE_PURC", 
                "label":"Available on Purchases",
                "help":"Check this box to make withholding tax available in purchase transactions."
            },{
                "id":"NOT_ON_POS", 
                "label":"Disable Withholding Tax in Purchase Orders",
                "help":"Check this box to deactivate withholding tax in purchase orders."
            },{
                "id":"NOT_ON_SOS", 
                "label":"Disable Withholding Tax in Sales Orders",
                "help":"Check this box to deactivate withholding tax in sales orders."
            },{
                "id":"AVAILABLE_SALES", 
                "label":"Available on Sales",
                "help":"Check this box to make withholding tax available in sales transactions."
            },{
                "id":"ENABLE_LOOKUP", 
                "label":"Enable Tax Lookup on Transactions",
                "help":"Check this box to automatically lookup the default tax code defined in the entity or item record in a transaction."
            },{
                "id":"NEXUS", 
                "label":"Nexus",
                "error_msg": [
                    {
                        "id":"ERRMSG1", 
                        "message": "A Withholding Tax Setup record has been created for the selected nexus."
                    }
                ],
                "help":"Select the nexus for this setup."
            },{
                "id":"TAX_POINT", 
                "label":"Tax Point",
                "select": [
                    {"id":"ACCRUAL", "label":"On Accrual"},
                    {"id":"PAYMENT", "label":"On Payment"}
                ],
                "help":"The point when a withholding tax liability, credit or expense is recognized."
            }
        ]
    },{
        "id":"WTAX_TYPE",
        "title":"Withholding Tax Type",
        "title_plural":"Withholding Tax Types",
        "fields":[
            {
                "id":"DESCRIPTION", 
                "label":"Description",
                "help":"Enter a description of this withholding tax type, tax code or tax group."
            },{
                "id":"NAME", 
                "label":"Name",
                "help":"Enter a name for this withholding tax type or tax group."
            },{
                "id":"NEXUS", 
                "label":"Nexus",
                "help":"Select the nexus for this setup.",
                "error_msg": [
                    {
                        "id":"ERRMSG1", 
                        "message": "A Withholding Tax Type with the same name already exists in the selected nexus."
                    }
                ]
            },{
                "id":"PURC_ACCOUNT", 
                "label":"Liability/Purchase Tax Account",
                "help":"Select the withholding tax control account for purchase transactions.",
                "error_msg": [
                    {
                        "id":"ERRMSG1", 
                        "message": "Liability/Purchase Tax Account and Asset/Sales Tax Account should post to different accounts."
                    }
                ]
            },{
                "id":"SALES_ACCOUNT", 
                "label":"Asset/Sales Tax Account",
                "help":"Select the withholding tax control account for sales transactions."
            },{
                "id":"TAX_BASE", 
                "label":"Withholding Tax Base",
                "select": [
                    {"id":"GROSS_AMT", "label":"Gross Amount"},
                    {"id":"NET_AMT", "label":"Net Amount"},
                    {"id":"TAX_AMT", "label":"Tax Amount"}
                ],
                "help":"Select the basis for computing withholding tax.",
                "error_msg": [
                    {
                        "id": "TAX_BASE_LOCK",
                        "message":"Editing the withholding tax base is not allowed since there are transactions associated to this withholding tax type."
                    }
                ]
            }
        ]
    },{
        "id":"WTAX_CODE",
        "title":"Withholding Tax Code",
        "title_plural":"Withholding Tax Codes",
        "fields":[
            {
                "id":"AVAILABLE_ON", 
                "label":"Available On",
                "select": [
                    {"id":"BOTH", "label":"Both"},
                    {"id":"PURCHASES", "label":"Purchases"},
                    {"id":"SALES", "label":"Sales"}
                ],
                "help":"Choose the type of transactions this withholding tax code or tax group can be applied to."
            },{
                "id":"DESCRIPTION", 
                "label":"Description",
                "help":"Enter a description of this withholding tax type, tax code or tax group."
            },{
                "id":"EFFECTIVE_FROM", 
                "label":"Effective From",
                "help":"Enter the date when this withholding tax code takes effect."
            },{
                "id":"INACTIVE", 
                "label":"Inactive",
                "help":"Check this box to inactivate this withholding tax code."
            },{
                "id":"INC_CHILDREN", 
                "label":"Include Children",
                "help":"Include child subsidiaries."
            },{
                "id":"NAME", 
                "label":"Tax Code",
                "help":"Enter a name for this tax code.",
                "error_msg": [
                    {
                        "id":"ERRMSG1", 
                        "message": "A &1 with the same name already exists in the selected withholding tax type's nexus."
                    }
                ]
            },{
                "id":"PERCENTAGE_BASE", 
                "label":"Percentage of Base",
                "help":"Enter the percentage of the base amount which will be used when computing withholding tax.<br><br>This is set by default to: 100%"
            },{
                "id":"RATE", 
                "label":"Rate",
                "help":"Enter the appropriate withholding tax rate as a percentage.<br><br>Example: 8%<br><br>This percentage will be calculated when you select this withholding tax code in transactions."
            },{
                "id":"SUBSIDIARIES", 
                "label":"Subsidiaries",
                "help":"This determines what subsidiaries will have this withholding tax code or tax code group available. If you are using OneWorld, then NetSuite displays this field. The drop-down list displays the list of available subsidiaries. In the Subsidiaries field, colons separate Parents (on the left) from their Children (on the right)."
            },{
                "id":"TAX_AGENCY", 
                "label":"Tax Agency",
                "help":"Tax Agency"
            },{
                "id":"VALID_UNTIL", 
                "label":"Valid Until",
                "help":"Enter the date when this withholding tax code expires.",
                "error_msg": [
                    {
                        "id":"ERRMSG1", 
                        "message": "Valid Until date should be later than the Effective From date."
                    }
                ]
            },{
                "id":"WTAX_SETUP", 
                "label":"Withholding Tax Setup",
                "help":""
            },{
                "id":"WTAX_TYPE", 
                "label":"Withholding Tax Type",
                "help":"Select the type of withholding tax code or tax group you are creating.<br><br>The tax control account for this type of withholding tax is selected by default.<br><br>You can create a new withholding tax type in Setup > Withholding Tax > Tax Types."
            },{
                "id":"WTAX_BASE", 
                "label":"Withholding Tax Base",
                "help":"Select the basis for computing withholding tax."
            },{
                "id":"STATE", 
                "label":"State"
            },{
                "id":"COUNTRY", 
                "label":"Country"
            },{
                "id":"NEW", 
                "label":"New &1"
            },{
                "id":"NEXUS", 
                "label":"Nexus"
            }
        ],
        "notif_msg": [
            {
                "id":"NOTIFMSG1", 
                "message": "Editing this record will update the following Withholding Tax Groups which it is a member of"
            }
        ]
    },{
        "id":"WTAX_GROUP",
        "title":"Withholding Tax Group",
        "title_plural":"Withholding Tax Groups",
        "fields":[
            {
                "id":"AVAILABLE_ON", 
                "label":"Available On",
                "select": [
                    {"id":"PURCHASES", "label":"Purchases"},
                    {"id":"SALES", "label":"Sales"}
                ],
                "help":"Choose the type of transactions this withholding tax code or tax group can be applied to."
            },{
                "id":"DESCRIPTION", 
                "label":"Description",
                "help":"Enter a description of this withholding tax type, tax code or tax group."
            },{
                "id":"INACTIVE", 
                "label":"Inactive",
                "help":"Check this box to inactivate this withholding tax code."
            },{
                "id":"INC_CHILDREN", 
                "label":"Include Children",
                "help":"Include child subsidiaries."
            },{
                "id":"NAME", 
                "label":"Name",
                "help":"Enter a name for this withholding tax type or tax group."
            },{
                "id":"RATE", 
                "label":"Rate",
                "help":"Enter the appropriate withholding tax rate as a percentage.<br><br>Example: 8%<br><br>This percentage will be calculated when you select this withholding tax code in transactions."
            },{
                "id":"SUBSIDIARIES", 
                "label":"Subsidiaries",
                "help":"This determines what subsidiaries will have this withholding tax code or tax code group available. If you are using OneWorld, then NetSuite displays this field. The drop-down list displays the list of available subsidiaries. In the Subsidiaries field, colons separate Parents (on the left) from their Children (on the right)."
            },{
                "id":"WTAX_SETUP", 
                "label":"Withholding Tax Setup",
                "help":""
            },{
                "id":"WTAX_TYPE", 
                "label":"Withholding Tax Type",
                "help":"Select the type of withholding tax code or tax group you are creating.<br><br>The tax control account for this type of withholding tax is selected by default.<br><br>You can create a new withholding tax type in Setup > Withholding Tax > Tax Types."
            },{
                "id":"WTAX_BASE", 
                "label": "Withholding Tax Base",
				"help": "Displays the tax base of the selected Withholding Tax Type."
            },{
                "id":"NEW", 
                "label":"New &1"
            },{
                "id":"NEXUS", 
                "label":"Nexus"
            }
        ],
        "subtab": [
            {
                "id":"WTAX_CODES", 
                "label":"Withholding Tax Codes",
                "fields": [
                    {
                        "id":"BASIS", 
                        "label":"Basis"
                    },{
                        "id":"TYPE", 
                        "label":"Type"
                    },{
                        "id":"TAX_BASE", 
                        "label":"Tax Base"
                    },{
                        "id":"WTAX_CODE", 
                        "label":"Tax Code"
                    },{
                        "id":"RATE", 
                        "label":"Rate"
                    }
                ]
            }
        ],
        "error_msg": [
            {
                "id":"ERRMSG1", 
                "message": "A &1 with the same name already exists in the selected withholding tax type's nexus."
            },{
                "id":"ERRMSG2", 
                "message": "Withholding tax codes should have the same withholding tax base. Please check the withholding tax type setting."
            }
        ]
    },{
        "id":"RECORD",
        "field": [
            { "id":"NEW", "label":"New &1" },
            { "id":"YES", "label":"Yes" },
            { "id":"NO", "label":"No" }
        ],
        "button": [
            { "id":"NEW", "label":"New" },
            { "id":"CANCEL", "label":"Cancel" },
            { "id":"DELETE", "label":"Delete" },
            { "id":"SAVE", "label":"Save" }
        ],
        "validation_msg":[
            { "id":"DELETE_RECORD", "message":"Are you sure you want to delete this record?" }
        ],
        "error_msg":[
            { "id":"PROB_FOUND", "message":"Problems were found." }
        ]
    },{
        "id":"TRANS_CS",
        "form_reload": [
            {
                "id":"RELOADMSG1", 
                "message": "The form needs to be reloaded to be able to apply withholding tax. Changes made will not be saved."
            },{
                "id":"RELOADMSG2", 
                "message": "The form needs to be reloaded. Changes made will not be saved."
            }
        ],
        "form_details": [
            {
                "id":"WTAX_CODE", 
                "message": "Please select a Withholding Tax Code."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "To show the payment amount net of withholding tax, in the Apply subtab re-check the 'Apply' box next to the invoice you want to pay."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "To show the payment amount net of withholding tax, in the Apply subtab re-check the 'Apply' box next to the bill you want to pay."
            }
        ],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "The total Withholding Tax Amount for all Withholding Tax Types cannot be zero or less. This includes the breakdown of amounts for all applicable Withholding Tax Groups applied on the transaction."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Applying a transaction discount is not supported when the withholding tax point is set to \"On Accrual.\""
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Applying a transaction discount is not supported when the withholding tax point is set to \"On Accrual.\""
			}
		]
    },{
        "id":"TRANS_UE",
        "field": [
            { "id":"AMOUNT", "label":"Amount" },
            {
                "id":"APPLIES_TO", 
                "label":"Applies To",
                "select": [
                    {"id":"INDIVIDUAL_LINES", "label":"Individual Line Items"},
                    {"id":"TOTAL_AMT", "label":"Total Amount"}
                ],
                "help":"Select the basis for computing withholding tax."
            },
            { "id":"APPLY_WH_TAX", "label":"Apply WH Tax?" },
            {
                "id":"BASE_AMT", 
                "label":"Base Amount",
                "help":"The basis for computing withholding tax."
            },
            { "id":"DATE", "label":"Date" },
            { "id":"REF_NO", "label":"Ref No" },
            { 
                "id":"TAX_AMOUNT", 
                "label":"Tax Amount",
                "help":"The amount of withholding tax."
            },
            { 
                "id":"TAX_CODE", 
                "label":"Tax Code",
                "help":"The applicable withholding tax code or tax group." 
            },
            { 
                "id":"TAX_RATE", 
                "label":"Tax Rate",
                "help": "The applicable withholding tax rate." 
            },
            { "id":"TOTAL", "label":"Total" },
            { "id":"TYPE", "label":"Type" },
            { "id":"WITHHELD", "label":"Withheld" },
            { "id":"WTAX_AMT", "label":"WH Tax Amount" },
            { "id":"WTAX_BASE_AMT", "label":"WH Tax Base Amount" },
            { "id":"WTAX_CODE", "label":"WH Tax Code" },
            { "id":"WTAX_RATE", "label":"WH Tax Rate" },
            { "id":"BILL_PAYMENT", "label":"Bill Payment" },
            { "id":"INVOICE_PAYMENT", "label":"Invoice Payment" },
            { "id":"BILL_CREDIT", "label":"Bill Credit" },
            { "id":"CREDIT_MEMO", "label":"Credit Memo" }
        ],
        "subtab":[
            { "id":"WTAX", "label":"Withholding Tax" }
        ],
        "button": [
            { "id":"GO_BACK", "label":"Go Back" }
        ],
        "field_msg": [
            {
                "id":"CANCEL_WTAX_BILL_CREDIT",
                "message":"Canceled Withholding Tax Bill Credit from Voided Bill Payment &1"
            },{
                "id":"CANCEL_WTAX_CREDIT_MEMO",
                "message":"Canceled Withholding Tax Credit Memo from Voided Invoice Payment &1"
            },{
                "id":"AMOUNT", "message":"Amount"
            },{
                "id":"WTAX_AMT",
                "message":"Withholding Tax Amount for &1"
            },{
                "id":"REF_NO", "message":"Ref No."
            }
        ],
        "error_msg": [
            {
                "id":"TRAN_LOCK_BILL_CREDIT",
                "message":"This bill credit reflects the withholding tax amount for the associated bill."
            }, {
                "id":"TRAN_LOCK_BILL_PAYMENT",
                "message":"This bill already has a payment applied to it, net of applicable withholding tax."
            }, {
                "id":"TRAN_LOCK_CREDIT_MEMO",
                "message":"This credit memo reflects the withholding tax amount for the associated invoice."
            }, {
                "id":"TRAN_LOCK_INVOICE_PAYMENT",
                "message":"This invoice already has a payment applied to it, net of applicable withholding tax."
            }, {
                "id":"TRAN_LOCK",
                "message":"Editing is not allowed for this transaction. For more information contact your NetSuite Administrator."
            }, {
                "id":"TRAN_LOCK_NS_INFO",
                "message":"NetSuite Information"
            }, {
                "id":"TRAN_LOCK_WARNING",
                "message":"Warning"
            }, {
                "id":"SO_MULT_INVENTORY",
                "message":"On Sales Order when Multi-Location  Inventory is on, a value for Location is required."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"On Invoice when Multi-Location  Inventory is on, a value for Location is required."
            }, {
                "id":"ERROR",
                "message":"Error"
            }, {
                "id":"VAT_CODE_CHECK_US",
                "message":"Please make sure that there is at least 1 tax code with the following settings for the current country/subsidiary:<br>Rate = 0.00%<br>Tax Name = -Not Taxable-<br>Inactive = false (checkbox should NOT be marked)"
            }, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"Please make sure that there is at least a tax group with GST/PST or HST tax codes with the following settings for the current country/subsidiary:<br><br>Rate = 0.00%<br>Exclude From VAT Reports = true (checkbox should be marked)<br>Inactive = false (checkbox should NOT be marked)"
            }, {
                "id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
                "message":"Please make sure that there is at least 1 tax code with the following settings for the current country/subsidiary:<br>Rate = 0.00%<br>Exclude From VAT Reports = true (checkbox should be marked)<br>Inactive = false (checkbox should NOT be marked)<br><br>HINT: Usually the above settings can be found in a tax code with the name/code containing \"UNDEF_\""
            }, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
        ],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
    },{
        "id":"REPORT",
        "report": [
            { "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Tax Withheld for Vendors (Detail)" },
            { "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Quarterly Tax Withheld for Vendor - Period" },
            { "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Monthly Tax Withheld for Vendor - Period" },
            { "id":"WTAX_VENDOR_PDF", "title":"List of vendors from whom taxes were withheld" },
            { "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Tax Withheld by Customers (Detail)" },
            { "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Tax Withheld by Customers" },
            { "id":"FROM", "title":"From" },
            { "id":"TO", "title":"To" },
            { "id":"WTAX_CUSTOMER_PDF", "title":"List of customers from whom taxes were withheld" },
            { "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Tax Withheld for Vendors (Summary)" },
            { "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Tax Withheld by Customers (Summary)" },
            { "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Purchases by Withholding Tax Code (Detail)" },
            { "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"List of purchases by which taxes were withheld (Detail)" },
            { "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Purchases by Withholding Tax Code (Summary)" },
            { "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"List of purchases by which taxes were withheld (Summary)" },
            { "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Sales by Withholding Tax Code (Detail)" },
            { "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"List of sales by which taxes were withheld (Detail)" },
            { "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Sales by Withholding Tax Code (Summary)" },
            { "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"List of sales by which taxes were withheld (Summary)" }
        ],
        "common_field": [
            { "id":"AGENT_NAME", "label":"Withholding Agent's Name" },
            { "id":"AMT_INCOME_PMNT", "label":"Amount of Income Payment" },
            { "id":"AMT_TAX_WHELD", "label":"Amount of Tax Withheld" },
            { "id":"CLASSIFICATION", "label":"Classification" },
            { "id":"COMPANY", "label":"Company" },
            { "id":"CORPORATION", "label":"Corporation (Registered Name)" },
            { "id":"DESCRIPTION", "label":"Description" },
            { "id":"END_OF_REPORT", "label":"End of Report" },
            { "id":"FIRST_NAME", "label":"First Name" },
            { "id":"GRAND_TOTAL", "label":"Grand Total" },
            { 
                "id":"INDIVIDUAL", 
                "label":"Individual",
                "long_label":"Individual (Last Name, First Name, Middle Name)" 
            },
            { "id":"LAST_NAME", "label":"Last Name" },
            { "id":"MEMO", "label":"Memo" },
            { "id":"MIDDLE_NAME", "label":"Middle Name" },
            { "id":"NEXUS", "label":"Nexus" },
            { "id":"PAYEE_NAME", "label":"Payee's Name" },
            { "id":"PERIOD_FROM", "label":"for the period" },
            { "id":"PERIOD_TO", "label":"to" },
            { "id":"REFERENCE_NO", "label":"Transaction Number" },
            { "id":"SEQ_NO", "label":"Seq No" },
            { "id":"SUBSIDIARY", "label":"Subsidiary" },
            { "id":"TAX_CODE", "label":"Tax Code" },
            { "id":"TAX_PERIOD", "label":"Tax Period" },
            { "id":"TAX_RATE", "label":"Tax Rate" },
            { "id":"TAX_REG_NO", "label":"Tax Registration Number" },
            { "id":"TAX_TYPE", "label":"Tax Type" },
            { "id":"TIN", "label":"TIN" },
            { "id":"TRANSACTION_TYPE", "label":"Transaction Type" },
            { "id":"WTAX_RATE", "label":"Withholding Tax Rate" },
            { "id":"NUM_ONE", "label":"1" },
            { "id":"NUM_TWO", "label":"2" },
            { "id":"NUM_THREE", "label":"3" },
            { "id":"NUM_FOUR", "label":"4" },
            { "id":"NUM_FIVE", "label":"5" },
            { "id":"NUM_SIX", "label":"6" },
            { "id":"NUM_SEVEN", "label":"7" },
            { "id":"NUM_EIGHT", "label":"8" },
            { "id":"NUM_NINE", "label":"9" },
            { "id":"NUM_TEN", "label":"10" },
            { "id":"NUM_ELEVEN", "label":"11" },
            { "id":"NUM_TWELVE", "label":"12" }
        ],
        "common_field_value":[
            { "id":"NONE", "value":"None" }
        ],
        "button": [
            { "id":"SUBMIT", "label":"Submit" },
            { "id":"PRINT_PDF", "label":"Print PDF" },
            { "id":"SAVE_CSV", "label":"Export - CSV" },
            { "id":"EXPORT_DAT", "label":"Export - DAT" },
            { "id":"RETURN_TO_CRITERIA", "label":"Return To Criteria" }
        ]
    }
]