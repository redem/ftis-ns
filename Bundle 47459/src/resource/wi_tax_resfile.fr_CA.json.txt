[{
		"id":"GLOBAL_TRY_CATCH",
		"title":"Try Catch mondial erreur",
		"error_msg":[
			{
				"id":"TRIGGERED_BY_USER", "message":"Erreur déclenchée par l'utilisateur:" 
			} 
		]
	},{
		"id":"GLOBAL_VALUES",
		"title":"Valeurs mondiales",
		"error_msg":[
			{
				"id":"CONTACT_NS_ADMIN", "message":"Les modifications ne sont pas permises pour cette transaction.",
				"id":"WARNING", "message":"Avertissement"
			} 
		]
	},{
		"id":"WTAX_SETUP",
		"title":"Configuration de la retenue d'impôt",
		"fields":[
			{
				"id":"APPLIES_TO", 
				"label":"S'applique à",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Lignes d'articles individuelles"},
					{"id":"TOTAL_AMT", "label":"Montant total"}
				],
				"help":"Sélectionnez la base de calcul de la retenue d'impôt."
			},{
				"id":"AUTO_APPLY", 
				"label":"Appliquer la retenue d'impôt automatiquement",
				"help":"Cochez cette case afin que la retenue d'impôt soit appliquée automatiquement lors des transactions."
			},{
				"id":"AVAILABLE_PURC", 
				"label":"Disponible pour les achats",
				"help":"Cochez cette case afin que la retenue d'impôt soit disponible lors des transactions d'achat."
			},{
                "id":"NOT_ON_POS", 
                "label":"Désactiver la retenue d'impôt sur ​​les bons de commande",
                "help":"Cochez cette case pour désactiver retenue à la source dans les bons de commande."
            },{
                "id":"NOT_ON_SOS", 
                "label":"Désactiver la retenue d'impôt sur ​​les commandes clients",
                "help":"Cochez cette case pour désactiver retenue à la source dans les ordres de vente."
            },{
				"id":"AVAILABLE_SALES", 
				"label":"Disponible pour les ventes",
				"help":"Cochez cette case afin que la retenue d'impôt soit disponible lors des transactions de vente."
			},{
				"id":"ENABLE_LOOKUP", 
				"label":"Activer la consultation des impôts et taxes sur les transactions",
				"help":"Cochez cette case afin de consulter automatiquement le code fiscal par défaut défini dans l'entité ou le dossier d'article lors d'une transaction."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Un dossier de configuration de retenue d'impôt a été créé pour le nexus sélectionné."
					}
				],
				"help":"Sélectionner le nexus d'impôts approprié pour ce dossier."
			},{
				"id":"TAX_POINT", 
				"label":"Stade fiscal",
				"select": [
					{"id":"ACCRUAL", "label":"Au moment du cumul"},
					{"id":"PAYMENT", "label":"Au moment du paiement"}
				],
				"help":"Le stade auquel une retenue d'impôt exigible, un crédit ou une dépense est reconnu."
			}
		]
	},{
		"id":"WTAX_TYPE",
		"title":"Type de retenue d'impôt",
		"title_plural":"Types de retenue d'impôt",
		"fields":[
			{
				"id":"DESCRIPTION", 
				"label":"Description",
				"help":"Saisissez une description de ce type de retenue d'impôt, de ce code fiscal ou de ce groupe fiscal."
			},{
				"id":"NAME", 
				"label":"Nom",
				"help":"Saisissez un nom pour ce type de retenue d'impôt ou ce groupe fiscal."
			},{
				"id":"NEXUS", 
				"label":"Nexus",
				"help":"Sélectionner le nexus d'impôts approprié pour ce dossier.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Un type de retenue d'impôt avec le même nom existe déjà dans le nexus sélectionné."
					}
				]
			},{
				"id":"PURC_ACCOUNT", 
				"label":"Compte de taxes sur les éléments de passif/achats",
				"help":"Sélectionnez le compte de contrôle de la retenue d'impôt pour les transactions d'achat.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Les compte de taxes sur les éléments de passif/achats et compte de taxes sur les actifs/ventes doivent reporter sur des comptes différents."
					}
				]
			},{
				"id":"SALES_ACCOUNT", 
				"label":"Compte de taxes sur les actifs/ventes",
				"help":"Sélectionnez le compte de contrôle de la retenue d'impôt pour les transactions de vente."
			},{
				"id":"TAX_BASE", 
				"label":"Montant de base de retenue d'impôt",
				"select": [
					{"id":"GROSS_AMT", "label":"Montant brut"},
					{"id":"NET_AMT", "label":"Montant net"},
					{"id":"TAX_AMT", "label":"Montant des impôts"}
				],
				"help":"Sélectionnez la base de calcul de la retenue d'impôt.",
				"error_msg": [
					{
						"id": "TAX_BASE_LOCK",
						"message":"Modification de la base retenue à la source n'est pas autorisé car il ya des transactions associées à ce type d'impôt à la source."
					}
				]
			}
		]
	},{
		"id":"WTAX_CODE",
		"title":"Code de retenue d'impôt",
		"title_plural":"Codes de retenue d'impôt",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Disponible pour",
				"select": [
					{"id":"BOTH", "label":"Les deux"},
					{"id":"PURCHASES", "label":"Achats"},
					{"id":"SALES", "label":"Ventes"}
				],
				"help":"Choisissez les types de transactions auxquels ce code de retenue d'impôt ou groupe fiscal peu être appliqué."
			},{
				"id":"DESCRIPTION", 
				"label":"Description",
				"help":"Saisissez une description de ce type de retenue d'impôt, de ce code fiscal ou de ce groupe fiscal."
			},{
				"id":"EFFECTIVE_FROM", 
				"label":"Entrée en vigueur",
				"help":"Indiquez la date à laquelle le code de cette retenue d'impôt prend effet."
			},{
				"id":"INACTIVE", 
				"label":"Inactif",
				"help":"Cochez cette case afin de désactiver ce code de retenue d'impôt."
			},{
				"id":"INC_CHILDREN", 
				"label":"Inclure enfants",
				"help":"Inclure les filiales enfant."
			},{
				"id":"NAME", 
				"label":"Code fiscal",
				"help":"Indiquez un nom pour ce code fiscal.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "Un &1 avec le même nom existe déjà dans le nexus du type de retenue d'impôt sélectionné."
					}
				]
			},{
				"id":"PERCENTAGE_BASE", 
				"label":"Pourcentage du montant de base",
				"help":"Indiquez le pourcentage du montant de base qui sera utilisé lors du calcul de la retenue d'impôt.<br><br>Il est défini par défaut à : 100 %"
			},{
				"id":"RATE", 
				"label":"Taux",
				"help":"Indiquez le taux de retenue d'impôt approprié sous forme de pourcentage.<br><br>Exemple : 8 %<br><br>Ce pourcentage est calculé lorsque vous sélectionnez ce code de retenue d'impôt pour les transactions."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Filiales",
				"help":"Cette option détermine pour quelles filiales ce code de retenue d'impôt ou ce groupe de code fiscal sera disponible. Si vous utilisez OneWorld, NetSuite affiche ce champ. La liste déroulante affiche la liste des filiales disponibles. Dans le champ Filiales, les Parents (à gauche) sont séparées de leurs Enfants (à droite) par deux-points."
			},{
				"id":"TAX_AGENCY", 
				"label":"Centre des impôts",
				"help":"Centre des impôts"
			},{
				"id":"VALID_UNTIL", 
				"label":"Valide jusqu'à",
				"help":"Indiquez la date à laquelle le code de cette retenue d'impôt expire.",
				"error_msg": [
					{
						"id":"ERRMSG1", 
						"message": "La date du champ Valide jusqu'au doit être ultérieure à la date du champ Entrée en vigueur."
					}
				]
			},{
				"id":"WTAX_SETUP", 
				"label":"Configuration de la retenue d'impôt",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Type de retenue d'impôt",
				"help":"Sélectionnez le type de code de retenue d'impôt ou de groupe fiscal que vous créez.<br><br>Le compte de contrôle fiscal pour ce type de retenue d'impôt est sélectionné par défaut.<br><br>Vous pouvez créer un nouveau type de retenue d'impôt dans Configuration > Retenue d'impôt > Types d'impôt."
			},{
				"id":"WTAX_BASE", 
				"label":"Montant de base de retenue d'impôt",
				"help":"Sélectionnez la base de calcul de la retenue d'impôt."
			},{
				"id":"STATE", 
				"label":"État"
			},{
				"id":"COUNTRY", 
				"label":"Pays"
			},{
				"id":"NEW", 
				"label":"Nouvelle &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"notif_msg": [
			{
				"id":"NOTIFMSG1", 
				"message": "Les modifications de ce dossier entraîneront la mise à jour des groupes de retenue d'impôt suivants, dont il est membre"
			}
		]
	},{
		"id":"WTAX_GROUP",
		"title":"Groupe de retenue d'impôt",
		"title_plural":"Groupes de retenue d'impôt",
		"fields":[
			{
				"id":"AVAILABLE_ON", 
				"label":"Disponible pour",
				"select": [
					{"id":"PURCHASES", "label":"Achats"},
					{"id":"SALES", "label":"Ventes"}
				],
				"help":"Choisissez les types de transactions auxquels ce code de retenue d'impôt ou groupe fiscal peu être appliqué."
			},{
				"id":"DESCRIPTION", 
				"label":"Description",
				"help":"Saisissez une description de ce type de retenue d'impôt, de ce code fiscal ou de ce groupe fiscal."
			},{
				"id":"INACTIVE", 
				"label":"Inactif",
				"help":"Cochez cette case afin de désactiver ce code de retenue d'impôt."
			},{
				"id":"INC_CHILDREN", 
				"label":"Inclure enfants",
				"help":"Inclure les filiales enfant."
			},{
				"id":"NAME", 
				"label":"Nom",
				"help":"Saisissez un nom pour ce type de retenue d'impôt ou ce groupe fiscal."
			},{
				"id":"RATE", 
				"label":"Taux",
				"help":"Indiquez le taux de retenue d'impôt approprié sous forme de pourcentage.<br><br>Exemple : 8 %<br><br>Ce pourcentage est calculé lorsque vous sélectionnez ce code de retenue d'impôt pour les transactions."
			},{
				"id":"SUBSIDIARIES", 
				"label":"Filiales",
				"help":"Cette option détermine pour quelles filiales ce code de retenue d'impôt ou ce groupe de code fiscal sera disponible. Si vous utilisez OneWorld, NetSuite affiche ce champ. La liste déroulante affiche la liste des filiales disponibles. Dans le champ Filiales, les Parents (à gauche) sont séparées de leurs Enfants (à droite) par deux-points."
			},{
				"id":"WTAX_SETUP", 
				"label":"Configuration de la retenue d'impôt",
				"help":""
			},{
				"id":"WTAX_TYPE", 
				"label":"Type de retenue d'impôt",
				"help":"Sélectionnez le type de code de retenue d'impôt ou de groupe fiscal que vous créez.<br><br>Le compte de contrôle fiscal pour ce type de retenue d'impôt est sélectionné par défaut.<br><br>Vous pouvez créer un nouveau type de retenue d'impôt dans Configuration > Retenue d'impôt > Types d'impôt."
			},{
                "id":"WTAX_BASE", 
                "label": "Montant de base de retenue d'impôt",
				"help": "Affiche l'assiette fiscale de l'impôt anticipé type sélectionné."
            },{
				"id":"NEW", 
				"label":"Nouvelle &1"
			},{
				"id":"NEXUS", 
				"label":"Nexus"
			}
		],
		"subtab": [
			{
				"id":"WTAX_CODES", 
				"label":"Codes de retenue d'impôt",
				"fields": [
					{
						"id":"BASIS", 
						"label":"Base"
					},{
						"id":"TYPE", 
						"label":"Type de retenue d'impôt"
					},{
						"id":"TAX_BASE", 
						"label":"Montant de base de retenue d'impôt"
					},{
						"id":"WTAX_CODE", 
						"label":"Code de retenue d'impôt"
					},{
						"id":"RATE", 
						"label":"Taux"
					}
				]
			}
		],
		"error_msg": [
			{
				"id":"ERRMSG1", 
				"message": "Un &1 avec le même nom existe déjà dans le nexus du type de retenue d'impôt sélectionné."
			},{
				"id":"ERRMSG2", 
				"message": "Les codes de retenue d'impôt doivent avoir un montant de base de retenue d'impôt similaire. Veuillez cocher le paramètre de type de retenue d'impôt."
			}
		]
	},{
		"id":"RECORD",
		"field": [
			{ "id":"NEW", "label":"Nouvelle &1" },
			{ "id":"YES", "label":"Oui" },
			{ "id":"NO", "label":"N°" }
		],
		"button": [
			{ "id":"NEW", "label":"Nouvelle" },
			{ "id":"CANCEL", "label":"Annuler" },
			{ "id":"DELETE", "label":"Supprimer" },
			{ "id":"SAVE", "label":"Enregistrer" }
		],
		"validation_msg":[
			{ "id":"DELETE_RECORD", "message":"Êtes-vous sûr de vouloir supprimer ce dossier?" }
		],
		"error_msg":[
			{ "id":"PROB_FOUND", "message":"Problèmes trouvés." }
		]
	},{
		"id":"TRANS_CS",
		"form_reload": [
			{
				"id":"RELOADMSG1", 
				"message": "Afin de pouvoir appliquer la retenue d'impôt, le formulaire doit être rechargé. Les modifications effectuées ne seront pas enregistrées."
			},{
				"id":"RELOADMSG2", 
				"message": "Le formulaire doit être rechargé. Les modifications effectuées ne seront pas enregistrées."
			}
		],
		"form_details": [
			{
				"id":"WTAX_CODE", 
				"message": "Veuillez sélectionner un code de retenue d'impôt."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_INV_WARNING", 
                "message": "Pour afficher le montant net versement du précompte professionnel, dans le sous-onglet Appliquer re-cocher la case 'Appliquer' à côté de la facture que vous souhaitez payer."
            },
            {
                "id":"WTAX_AMOUNT_FIELD_BILL_WARNING", 
                "message": "Pour afficher le montant net versement du précompte professionnel, dans le sous-onglet Appliquer re-cocher la case 'Appliquer' à côté de la facture que vous souhaitez payer."
			}
		],
		"form_save": [
			{
				"id": "WTAX_TOTAL_VALIDATION",
				"message": "Le montant total des retenues d'impôt pour tous les types de retenues d'impôt ne peut pas être zéro ou moins. Cela comprend la ventilation des montants applicables pour tous les groupes de retenue d'impôt appliqué sur la transaction."
			}
		],
		"validate_field": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Application d'une décote de la transaction n'est pas prise en charge lorsque le point de retenue à la source est réglée sur \"Au moment du cumul.\""
			}
		],
		"validate_line": [
			{
				"id": "HEADER_DISCOUNT_VALIDATION",
				"message": "Application d'une décote de la transaction n'est pas prise en charge lorsque le point de retenue à la source est réglée sur \"Au moment du cumul.\""
			}
		]
	},{
		"id":"TRANS_UE",
		"field": [
			{ "id":"AMOUNT", "label":"Montant" },
			{
				"id":"APPLIES_TO", 
				"label":"S'applique à",
				"select": [
					{"id":"INDIVIDUAL_LINES", "label":"Lignes d'articles individuelles"},
					{"id":"TOTAL_AMT", "label":"Montant total"}
				],
				"help":"Sélectionnez la base de calcul de la retenue d'impôt."
			},
			{ "id":"APPLY_WH_TAX", "label":"Appliquer la retenue d'impôt?" },
			{
				"id":"BASE_AMT", 
				"label":"Montant de base",
				"help":"La base de calcul de la retenue d'impôt."
			},
			{ "id":"DATE", "label":"Date" },
			{ "id":"REF_NO", "label":"N° de réf." },
			{ 
				"id":"TAX_AMOUNT", 
				"label":"Montant des impôts",
				"help":"Le montant de la retenue d'impôt."
			},
			{ 
				"id":"TAX_CODE", 
				"label":"Code fiscal",
				"help":"Le code de retenue d'impôt ou groupe fiscal applicable." 
			},
			{ 
				"id":"TAX_RATE", 
				"label":"Taux d'imposition",
				"help": "Le taux de retenue d'impôt applicable." 
			},
			{ "id":"TOTAL", "label":"Total" },
			{ "id":"TYPE", "label":"Type" },
			{ "id":"WITHHELD", "label":"Retenu" },
			{ "id":"WTAX_AMT", "label":"Montant de la retenue d'impôt" },
			{ "id":"WTAX_BASE_AMT", "label":"Montant de base de la retenue d'impôt" },
			{ "id":"WTAX_CODE", "label":"Code de la retenue d'impôt" },
			{ "id":"WTAX_RATE", "label":"Taux de la retenue d'impôt" },
			{ "id":"BILL_PAYMENT", "label":"Paiement des factures" },
			{ "id":"INVOICE_PAYMENT", "label":"Paiement de factures" },
			{ "id":"BILL_CREDIT", "label":"Traite" },
			{ "id":"CREDIT_MEMO", "label":"Avoir" }
		],
		"subtab":[
			{ "id":"WTAX", "label":"Retenue à la source" }
		],
		"button": [
			{ "id":"GO_BACK", "label":"Revenir en arrière" }
		],
		"field_msg": [
			{
				"id":"CANCEL_WTAX_BILL_CREDIT",
				"message":"Traite de retenue d'impôt annulée du paiement de facture annulé"
			},{
				"id":"CANCEL_WTAX_CREDIT_MEMO",
				"message":"Avoir de retenue d'impôt annulé du paiement de facture annulé" 
			},{
				"id":"AMOUNT", "message":"Montant"
			},{
				"id":"WTAX_AMT",
				"message":"Withholding Tax Amount for &1"
			},{
				"id":"REF_NO", "message":"N° de réf."
			}
		],
		"error_msg": [
			{
				"id":"TRAN_LOCK_BILL_CREDIT",
				"message":"Cette traite reflète le montant de la retenue d'impôt pour la facture associée."
			}, {
				"id":"TRAN_LOCK_BILL_PAYMENT",
				"message":"Un paiement est déjà appliqué à cette facture, net de retenue d'impôt applicable."
			}, {
				"id":"TRAN_LOCK_CREDIT_MEMO",
				"message":"Cet avoir reflète le montant de la retenue d'impôt pour la facture associée."
			}, {
				"id":"TRAN_LOCK_INVOICE_PAYMENT",
				"message":"Un paiement est déjà appliqué à cette facture, net de retenue d'impôt applicable."
			}, {
				"id":"TRAN_LOCK",
				"message":"Les modifications ne sont pas permises pour cette transaction. Pour plus de renseignements, contactez votre administrateur NetSuite."
			}, {
				"id":"TRAN_LOCK_NS_INFO",
				"message":"Informations sur NetSuite"
			}, {
				"id":"TRAN_LOCK_WARNING",
				"message":"Avertissement"
			}, {
				"id":"SO_MULT_INVENTORY",
				"message":"Sur commande lorsque les ventes multi-Lieu inventaire est activée, une valeur de position est nécessaire."
            }, {
                "id":"INVOICE_MULT_INVENTORY",
                "message":"Sur la facture lorsque Multi-Lieu inventaire est activée, une valeur de position est nécessaire."
			}, {
				"id":"ERROR",
				"message":"Erreur"
			}, {
				"id":"VAT_CODE_CHECK_US",
				"message":"S'il vous plaît assurez-vous qu'il ya au moins 1 code fiscal avec les paramètres suivants pour le pays actuel / filiale: Taux = 0,00% <br> Nom de l'impôt sur <br> = Non-imposable-<br> inactifs = faux"
			}, {
				"id":"VAT_CODE_CHECK_CA",
				"message":"S'il vous plaît assurez-vous qu'il ya au moins un groupe d'intégration fiscale de la TPS / TVQ ou des codes de taxe HST avec les paramètres suivants pour le pays / la filiale actuelle:<br><br>Taux = 0,00%<br>Exclure des rapports de TVA = vrai (case doit être cochée)<br>Inactif = faux (case à cocher doit pas être marquée)"
			}, {
				"id":"VAT_CODE_CHECK_OTHER_COUNTRIES",
				"message":"S'il vous plaît assurez-vous qu'il ya au moins 1 code fiscal avec les paramètres suivants pour le pays actuel / filiale: Taux <br> = <br> 0,00% exclure de Rapports TVA = vrai (case à cocher doit être marqué) <br> Inactif = faux"
			}, {
                "id":"WTAX_ON_PAYMENT_LOCK",
                "message":"Payment processing is still in progess."
            }, {
                "id":"VOIDING_JOURNALS_NOT_SUPPORTED_FOR_WTAX",
                "message":"Voiding Bill Payments with Withholding Tax using reversing journals is not supported."
            }
		],
        "email": [
            {
                "id": "WTAX_ON_PAYMENT_FAILED_SUBJECT",
                "message": "Failed Withholding Tax processing for Payment&1"
            },
            {
                "id": "WTAX_ON_PAYMENT_FAILED_BODY",
                "message": "Posting of Withholding Tax for the following transaction/s was not successful:\n&1\n\nError encountered:\n&2\nFor more information, see \"Withholding Tax Troubleshooting Guide\" in the Help Center.\n\nOnce you have resolved the error, please click the link below to reprocess the posting of withholding tax:\n&3"
            }
        ]
	},{
		"id":"REPORT",
		"report": [
			{ "id":"WTAX_VENDOR_DETAIL_REPORT", "title":"Impôt retenu pour les fournisseurs (détails)" },
			{ "id":"WTAX_VENDOR_QUARTERLY_LIST", "title":"Impôt trimestriel masqué pour le fournisseur - Période" },
			{ "id":"WTAX_VENDOR_MONTHLY_LIST", "title":"Mensuel Taxes masqué pour le fournisseur - Période" },
			{ "id":"WTAX_VENDOR_PDF", "title":"Liste des fournisseurs dont les impôts ont été retenus" },
			{ "id":"WTAX_CUSTOMER_DETAIL_REPORT", "title":"Impôt retenu par clients (détails)" },
			{ "id":"WTAX_CUSTOMER_DETAIL_LIST", "title":"Impôt retenu par clients" }, 
			{ "id":"FROM", "title":"De" },
			{ "id":"TO", "title":"Jusqu'à" },
			{ "id":"WTAX_CUSTOMER_PDF", "title":"Liste des clients dont les impôts ont été retenus" },
			{ "id":"WTAX_VENDOR_SUMMARY_REPORT", "title":"Résumé de l'impôt retenu pour les fournisseurs" },
			{ "id":"WTAX_CUSTOMER_SUMMARY_REPORT", "title":"Résumé de l'impôt retenu pour les clients" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_REPORT", "title":"Détails des achats par code de retenue d'impôt" },
			{ "id":"WTAX_VENDOR_PURC_DETAIL_PDF", "title":"Liste des achats par lequel les taxes ont été retenus (Détail)" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_REPORT", "title":"Résumé de l'impôt retenu pour les fournisseurs" },
			{ "id":"WTAX_VENDOR_PURC_SUMMARY_PDF", "title":"Liste des achats par lequel les taxes ont été retenus (Résumé)" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_REPORT", "title":"Détails des ventes par code de retenue d'impôt" },
			{ "id":"WTAX_CUSTOMER_SALES_DETAIL_PDF", "title":"Liste des ventes de laquelle les impôts ont été retenus (Détail)" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_REPORT", "title":"Résumé des ventes par code de retenue d'impôt" },
			{ "id":"WTAX_CUSTOMER_SALES_SUMMARY_PDF", "title":"Liste des ventes de laquelle les impôts ont été retenus (Résumé)" }
		],
		"common_field": [
			{ "id":"AGENT_NAME", "label":"Nom de l'agent chargé de la retenue" },
			{ "id":"AMT_INCOME_PMNT", "label":"Montant du versement du revenu" },
			{ "id":"AMT_TAX_WHELD", "label":"Montant de l'impôt retenu" },
			{ "id":"CLASSIFICATION", "label":"Classement" },
			{ "id":"COMPANY", "label":"Société" },
			{ "id":"CORPORATION", "label":"Société (Raison sociale)" },
			{ "id":"DESCRIPTION", "label":"Description" },
			{ "id":"END_OF_REPORT", "label":"Fin du rapport" },
			{ "id":"FIRST_NAME", "label":"Prénom" },
			{ "id":"GRAND_TOTAL", "label":"Total général" },
			{ 
				"id":"INDIVIDUAL", 
				"label":"Individu",
				"long_label":"Individu (Nom, Prénom, Deuxième prénom)" 
			},
			{ "id":"LAST_NAME", "label":"Nom" },
			{ "id":"MEMO", "label":"Note" },
			{ "id":"MIDDLE_NAME", "label":"Deuxième prénom" },
			{ "id":"NEXUS", "label":"Nexus" },
			{ "id":"PAYEE_NAME", "label":"Nom du bénéficiaire" },
			{ "id":"PERIOD_FROM", "label":"pour la période" },
			{ "id":"PERIOD_TO", "label":"à" },
			{ "id":"REFERENCE_NO", "label":"Numéro de transaction" },
			{ "id":"SEQ_NO", "label":"N° de séq." },
			{ "id":"SUBSIDIARY", "label":"Filiale" },
			{ "id":"TAX_CODE", "label":"Code fiscal" },
			{ "id":"TAX_PERIOD", "label":"Période d'imposition" },
			{ "id":"TAX_RATE", "label":"Taux de retenue d'impôt" },
			{ "id":"TAX_REG_NO", "label":"Numéro d'identification fiscale" },
			{ "id":"TAX_TYPE", "label":"Type d'impôt" },
			{ "id":"TIN", "label":"Numéro d'identification fiscale" },
			{ "id":"TRANSACTION_TYPE", "label":"Type de transaction" },
			{ "id":"WTAX_RATE", "label":"Taux de retenue d'impôt" },
			{ "id":"NUM_ONE", "label":"1" },
			{ "id":"NUM_TWO", "label":"2" },
			{ "id":"NUM_THREE", "label":"3" },
			{ "id":"NUM_FOUR", "label":"4" },
			{ "id":"NUM_FIVE", "label":"5" },
			{ "id":"NUM_SIX", "label":"6" },
			{ "id":"NUM_SEVEN", "label":"7" },
			{ "id":"NUM_EIGHT", "label":"8" },
			{ "id":"NUM_NINE", "label":"9" },
			{ "id":"NUM_TEN", "label":"10" },
			{ "id":"NUM_ELEVEN", "label":"11" },
			{ "id":"NUM_TWELVE", "label":"12" }
		],
		"common_field_value":[
			{ "id":"NONE", "value":"Aucun" }
		],
		"button": [
			{ "id":"SUBMIT", "label":"Soumettre" },
			{ "id":"PRINT_PDF", "label":"Imprimer le PDF" },
			{ "id":"SAVE_CSV", "label":"Export - CSV" },
			{ "id":"EXPORT_DAT", "label":"Export - DAT" },
			{ "id":"RETURN_TO_CRITERIA", "label":"Retourner aux critères" }
		]
	}
]