/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getWiTaxGroupMapper = function getWiTaxGroupMapper() {
    var taxType = {};
    ['witaxsetup', 'witaxbase', 'purcaccount', 'purcitem', 'saleaccount', 'saleitem'].forEach(function (fieldName) {
        taxType[fieldName] = 'custrecord_4601_wtc_witaxtype.custrecord_4601_wtt_' + fieldName;
    });

    var recordMapper = new _4601.RecordMapper('customrecord_4601_witaxcode', {
        prefix: 'custrecord_4601_wtc_',
        formTitle: '', //Withholding Tax Group
        listTitle: '', //Withholding Tax Groups
        suiteletId: 'customscript_4601_witaxgroup',
        suiteletDeploymentId: 'customdeploy_4601_witaxgroup'
    })
        .addMapping('name', {label: '', showInList: true, mandatory: true}) //Name
        .addMapping('description', {type: 'textarea', label: '', showInList: true}) //Description
        .addMapping('rate', {type: 'percent', label: '', showInList: true, layoutType: ['normal', 'startcol']}); //Rate

    if (_4601.isOneWorld()) {
        recordMapper
                .addMapping('subsidiaries', {type: 'multiselect', label: ''}) //Subsidiaries
                .addMapping('includechildsubs', {type: 'checkbox', label: ''}); //Include Children
    }

    recordMapper
            .addMapping('witaxtype', {type: 'select', label: '', showInList: true, mandatory: true}) //Withholding Tax Type
            .addMapping('witaxsetup', {label: '', column: taxType.witaxsetup, filter: taxType.witaxsetup, field: taxType.witaxsetup, excludeInForm: true}) //Withholding Tax Setup
            .addMapping('availableon', {type: 'select', label: '', mandatory: true, selectOptions: [ //Available On
                ['', ''],
                ['onpurcs', ''], //Purchases
                ['onsales', ''] //Sales
            ]})
            .addMapping('inactive', {type: 'checkbox', label: ''}) //Inactive
            .addMapping('istaxgroup', {type: 'checkbox', excludeInForm: true})
            .addMapping('groupedwitaxcodes', {type: 'multiselect', excludeInForm: true});


    recordMapper.getValidationErrors = function getValidationErrors(mappedRecord, resourceObject) {
        var validationErrors = [];
        var wiTaxSetupId = _4601.getWiTaxTypeMapper().lookupField(mappedRecord.witaxtype, 'witaxsetup');

        var wiTaxGroupsWithSameName = recordMapper.all({columns: ["istaxgroup"], filters: [
            ['internalid', 'noneof', mappedRecord.id],
            ['witaxsetup', 'is', wiTaxSetupId],
            ['name', 'is', mappedRecord.name]
        ]}).getFirst();
        
        if (wiTaxGroupsWithSameName) {
            var recTypeForErrMessage = wiTaxGroupsWithSameName.istaxgroup === true ? resourceObject.WTAX_GROUP.title : resourceObject.WTAX_CODE.title;
            
            validationErrors.push(_4601.renderTranslation(resourceObject.WTAX_GROUP['error_msg'].ERRMSG1.message, {1: recTypeForErrMessage})); //The Withholding Tax Code/Group already exists in the selected Withholding Tax Type's nexus.
        }
        
        var taxBase = null;
        var errTaxBase = false;
        if (mappedRecord.groupedwitaxcodes) {
        	for (var i in mappedRecord.groupedwitaxcodes) {
        		var wTaxCode = mappedRecord.groupedwitaxcodes[i];
        		if (wTaxCode != null && wTaxCode.witaxbase != taxBase) {
        			if (taxBase!=null) {
        				errTaxBase = true;
            			break;
        			} else 
        				taxBase = wTaxCode.witaxbase;
        		}
        	}
        }
        
        if (errTaxBase)
        	validationErrors.push(resourceObject.WTAX_GROUP['error_msg'].ERRMSG2.message); //Withholding tax codes should have the same withholding tax base. Please check the withholding tax type setting.

        return validationErrors;
    };


    recordMapper.beforeSave = function beforeSave(mappedRecord) {
        mappedRecord.istaxgroup = true;
    };


    return recordMapper;
};
