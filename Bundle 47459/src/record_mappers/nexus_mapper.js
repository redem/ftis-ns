/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getNexusMapper = function getNexusMapper() {
    var nexusMapper = new _4601.RecordMapper('nexus')
            .addMapping('country')
//            .addMapping('state')
            .addMapping('description');

    nexusMapper.nexusesWithNoWiTaxSetup = function nexusesWithNoWiTaxSetup(nexuses, wiTaxSetups) {
        var wiTaxSetupNexusIds = wiTaxSetups.map(function (wiTaxSetup) {
            return wiTaxSetup.nexus;
        });

        return nexuses.filter(function (nexus) {
            return wiTaxSetupNexusIds.indexOf(nexus.id) === -1;
        });
    };

    return nexusMapper;
};
