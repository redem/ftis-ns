/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getWiTaxConfigurationMapper = function getWiTaxConfigurationMapper() {
    return new _4601.RecordMapper('customrecord_4601_witaxconfiguration', {prefix: 'custrecord_4601_wtconf_'})
        .addMapping('name')
        .addMapping('value')
};


_4601.getConfigurationValue = (function () {
    var wiTaxConfiguration;

    function getConfigurationValue(confName) {
        if (!wiTaxConfiguration) {
            wiTaxConfiguration = _4601.getWiTaxConfigurationMapper().all();
        }

        var conf = wiTaxConfiguration.filter(function (conf) {
            return conf.name === confName;
        }).toArray()[0];

        if (conf) {
            return conf.value;
        } else {
            throw new Error('Withholding Tax Configuration: "' + confName + '" is not set.');
        }
    }

    return getConfigurationValue;
}());
