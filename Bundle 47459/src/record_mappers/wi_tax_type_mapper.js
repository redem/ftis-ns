/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getWiTaxTypeMapper = function getWiTaxTypeMapper() {
    var nexusFieldId = 'custrecord_4601_wtt_witaxsetup.custrecord_4601_wts_nexus';

    var wiTaxTypeMapper = new _4601.RecordMapper('customrecord_4601_witaxtype', {
        prefix: 'custrecord_4601_wtt_',
        formTitle: '', //Withholding Tax Type
        listTitle: '', //Withholding Tax Types
        suiteletId: 'customscript_4601_witaxtype',
        suiteletDeploymentId: 'customdeploy_4601_witaxtype'
    })
        .addMapping('name', {label: '', showInList: true, mandatory: true}) //Name
        .addMapping('description', {type: 'textarea', label: '', showInList: true}) //Description
        .addMapping('witaxsetup', {type: 'select', label: '', mandatory: true, layoutType: ['normal', 'startcol']}) //Nexus
        .addMapping('nexus', {label: '', showInList: true, column: nexusFieldId, filter: nexusFieldId, excludeInForm: true}) //Nexus
        .addMapping('witaxbase', {type: 'select', label: '', mandatory: true})
        // TODO implement Include Discounts
        //.addMapping('includediscounts', {type: 'checkbox', label: 'Include Discounts'})
        .addMapping('purcaccount', {type: 'select', label: '', mandatory: true, showInList: true, layoutType: ['normal', 'startcol']}) //Liability/Purchase Tax Account
        .addMapping('purcitem', {excludeInForm: true})
        .addMapping('saleaccount', {type: 'select', label: '', mandatory: true, showInList: true}) //Asset/Sales Tax Account
        .addMapping('saleitem', {excludeInForm: true});

    var taxTypeKindId = 'custrecord_ph4014_wtax_type_kind_ref';
    wiTaxTypeMapper.addMapping('taxtypekind', {filter: taxTypeKindId, column: taxTypeKindId, field: taxTypeKindId, excludeInForm: true});

    wiTaxTypeMapper.getValidationErrors = function getValidationErrors(mappedRecord, resourceObject) {
        var validationErrors = [];

        var wiTaxTypesWithSameName = wiTaxTypeMapper.all({columns: [], filters: [
            ['internalid', 'noneof', mappedRecord.id],
            ['witaxsetup', 'is', mappedRecord.witaxsetup],
            ['name', 'is', mappedRecord.name]
        ]});

        if (wiTaxTypesWithSameName.length) {
            validationErrors.push(resourceObject.WTAX_TYPE['fields'].NEXUS['error_msg'].ERRMSG1.message); //A WT Type with the same name already exists in the selected nexus.
        }

        // validate that the posting accounts (purc and sale) are not the same
        else if (mappedRecord.purcaccount == mappedRecord.saleaccount) {
            validationErrors.push(resourceObject.WTAX_TYPE['fields'].PURC_ACCOUNT['error_msg'].ERRMSG1.message); //Liability/Purchase Tax Account and Asset/Sales Tax Account should post to different accounts.
        }
        
        // TODO validate the uniqueness of the discount item names

        return validationErrors;
    };


    wiTaxTypeMapper.beforeSave = function beforeSave(mappedRecord) {
        ['purc', 'sale'].forEach(function (type) {
            var itemFieldName = type + 'item';
            var accountId = mappedRecord[type + 'account'];

            if (accountId) {
                var account = nlapiLoadRecord('account', accountId);
                var itemName = account.getFieldValue('acctname') + ' (' + mappedRecord.name + ')';
                var itemRecord;
                if (mappedRecord.id === 'new') {
                    itemRecord = nlapiCreateRecord('discountitem');
                } else {
//                    var itemId = wiTaxTypeMapper.lookupField(mappedRecord.id, itemFieldName);
                    var record = nlapiLoadRecord(wiTaxTypeMapper.recordType, mappedRecord.id);
                    var itemId = record.getFieldValue('custrecord_4601_wtt_' + itemFieldName);
                    itemRecord = itemId ? nlapiLoadRecord('discountitem', itemId) : nlapiCreateRecord('discountitem');
                }

                itemRecord.setFieldValue('itemid', itemName);
                itemRecord.setFieldValue('rate', 0);
                itemRecord.setFieldValue('account', accountId);
                itemRecord.setFieldValues('subsidiary', account.getFieldValues('subsidiary'));
                itemRecord.setFieldValue('includechildren', account.getFieldValue('includechildren'));

                mappedRecord[itemFieldName] = nlapiSubmitRecord(itemRecord);
            }
        });
    };


    wiTaxTypeMapper.afterDelete = function afterDelete(mappedRecord) {
        ['purc', 'sale'].forEach(function (type) {
            nlapiDeleteRecord('discountitem', mappedRecord[type + 'item']);
        });
    };
    
    wiTaxTypeMapper.getTaxBaseOfTaxType = function getTaxBase(wiTaxTypeId) {
        var filters = new Array();
        var columns = new Array();
        
        filters.push(new nlobjSearchFilter('internalid', null, 'is', wiTaxTypeId));
        
        columns.push(new nlobjSearchColumn('custrecord_4601_wtt_witaxbase'));
        
        var taxBase = nlapiSearchRecord('customrecord_4601_witaxtype', null, filters, columns);
        
        if (taxBase != null)
            return taxBase[0].getValue('custrecord_4601_wtt_witaxbase');
        else 
            return taxBase;
    }
    

    wiTaxTypeMapper.getAccountOptions = function getAccountOptions(wiTaxSetupId, isOneWorld) {
        var listAccountTypes = [];
        listAccountTypes.push('OthCurrAsset');
        listAccountTypes.push('FixedAsset');
        listAccountTypes.push('OthAsset');
        listAccountTypes.push('OthCurrLiab');
        listAccountTypes.push('LongTermLiab');
        listAccountTypes.push('Equity');
        listAccountTypes.push('Income');
        listAccountTypes.push('COGS');
        listAccountTypes.push('Expense');
        listAccountTypes.push('OthIncome');

        var acctCol = [];
        acctCol.push(new nlobjSearchColumn('internalid',null,'GROUP'));
        acctCol.push(new nlobjSearchColumn('name',null,'GROUP'));

        var acctFil = [];
        acctFil.push(new nlobjSearchFilter('isinactive',null,'is','F'));
        acctFil.push(new nlobjSearchFilter('type',null,'anyof', listAccountTypes));
        acctFil.push(new nlobjSearchFilter('specialaccounttype', null, 'noneof', ['CumulTransAdj']));
        
        var accountOptions = [];
        
        if (isOneWorld) {
            var rec = nlapiLoadRecord('customrecord_4601_witaxsetup',wiTaxSetupId);
            var nexusId = rec.getFieldValue('custrecord_4601_wts_nexus');

            var nexus = nlapiLoadRecord('nexus', nexusId);
             
            var subFil = [];
            subFil.push(new nlobjSearchFilter('isinactive',null,'is','F'));
            subFil.push(new nlobjSearchFilter('iselimination',null,'is','F'));
            subFil.push(new nlobjSearchFilter('country',null,'is', nexus.getFieldValue('country')));
            
            var subRes = nlapiSearchRecord('subsidiary', null, subFil);
            var subIds = (subRes || []).map(function(a) { return a.getId(); });
            acctFil.push(new nlobjSearchFilter('subsidiary', null, 'anyof', subIds.length > 0 ? subIds : '@NONE@'));
        }
        
        var s = nlapiCreateSearch('account', acctFil, acctCol).runSearch();
        var r = null;
        var index = 0;
        
        do {
            r = s.getResults(index, index + 1000);
            
            if (r) {
                for (var j = 0; j < r.length; j++) {
                    accountOptions.push({id: r[j].getValue('internalid', null, 'GROUP'), name: r[j].getValue('name', null, 'GROUP')});
                    index++;
                }
            }
        } while (r.length >= 1000)
        
        return accountOptions;
    };
    
    wiTaxTypeMapper.getTaxBaseOptions = function getTaxBaseOptions(wiTaxSetupId, labels) {
        
        var taxBaseOptions = [];
        
        if(wiTaxSetupId){
            taxBaseOptions.push({id: '', name: ''});
            taxBaseOptions.push({id: 'amount', name: labels.NET_AMT.label});
                    
            var setupwtax = nlapiLookupField('customrecord_4601_witaxsetup', wiTaxSetupId, 'custrecord_4601_wts_nexus'); 
            var nexuscountry = nlapiLookupField('nexus', setupwtax, 'country'); 
            
            
            if(nexuscountry != 'US' && nexuscountry != 'CA'){
                taxBaseOptions.push({id: 'grossamt', name: labels.GROSS_AMT.label});
                taxBaseOptions.push({id: 'tax1amt', name: labels.TAX_AMT.label});
            }
        }     
        return taxBaseOptions;
    };


    return wiTaxTypeMapper;
};
