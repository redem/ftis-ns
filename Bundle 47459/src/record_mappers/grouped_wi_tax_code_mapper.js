/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getGroupedWiTaxCodeMapper = function getGroupedWiTaxCodeMapper() {
    var taxCode = {};
    ['rate', 'witaxtype', 'availableon', 'inactive', 'subsidiaries', 'includechildsubs'].forEach(function (fieldName) {
        taxCode[fieldName] = 'custrecord_4601_gwtc_code.custrecord_4601_wtc_' + fieldName;
    });

    var taxGroup = {};
    ['name'].forEach(function (fieldName) {
        taxGroup[fieldName] = 'custrecord_4601_gwtc_group.custrecord_4601_wtc_' + fieldName;
    });

    return new _4601.RecordMapper('customrecord_4601_groupedwitaxcode', {prefix: 'custrecord_4601_gwtc_'})
            .addMapping('group')
            .addMapping('groupname', {column: taxGroup.name})
            .addMapping('code')
            .addMapping('basis', {type: 'float'})
            .addMapping('rate', {type: 'percent', filter: taxCode.rate, column: taxCode.rate})
            .addMapping('witaxtype', {filter: taxCode.witaxtype, column: taxCode.witaxtype})
            .addMapping('availableon', {filter: taxCode.availableon, column: taxCode.availableon})
            .addMapping('inactive', {type: 'checkbox', filter: taxCode.inactive, column: taxCode.inactive})
            .addMapping('subsidiaries', {type: 'multiselect', filter: taxCode.subsidiaries, column: taxCode.subsidiaries})
            .addMapping('includechildsubs', {type: 'checkbox', filter: taxCode.includechildsubs, column: taxCode.includechildsubs});
};
