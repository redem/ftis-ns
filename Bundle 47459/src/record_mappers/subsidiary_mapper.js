/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getSubsidiaryMapper = function getSubsidiaryMapper() {
    var subsidiaryMapper = new _4601.RecordMapper('subsidiary')
            .addMapping('name')
            .addMapping('isinactive')
            .addMapping('iselimination')
            .addMapping('country');


    subsidiaryMapper.filterByIds = function filterByIds(subsidiaries, ids, includeChildSubs) {
        var parents = subsidiaries.filter(function (subsidiary) {
            return ids.indexOf(subsidiary.id) > -1;
        });

        if (includeChildSubs) {
            var parentNames = parents.map(function (parent) {
                return parent.name;
            });

            // from XRegExp:
            function regexpEscape(str) {
                return str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            }

            return subsidiaries.filter(function (subsidiary) {
                return parentNames.some(function (parentName) {
                    var pattern = new RegExp('^' + regexpEscape(parentName));
                    return subsidiary.name.match(pattern);
                });
            });
        } else {
            return parents;
        }
    };


    return subsidiaryMapper;
};
