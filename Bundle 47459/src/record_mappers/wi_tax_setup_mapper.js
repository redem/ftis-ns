/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getWiTaxSetupMapper = function getWiTaxSetupMapper() {
    var taxPointOptions = [
        ['', ''],
        ['onaccrual', ''], //'On Accrual'; rm.WTAX_SETUP['fields'].TAX_POINT['select'].ACCRUAL.label
        ['onpayment', ''] //'On Payment'; rm.WTAX_SETUP['fields'].TAX_POINT['select'].PAYMENT.label
    ];

    var appliesToOptions = [
        ['T', ''], //Total Amount
        ['F', ''] //Individual Line Items
    ];

    var recordMapper = new _4601.RecordMapper('customrecord_4601_witaxsetup', {
        prefix: 'custrecord_4601_wts_',
        formTitle: '', //'Withholding Tax Setup'; rm.WTAX_SETUP.title
        listTitle: '', //'Withholding Tax Setups'; rm.WTAX_SETUP.title_plural
        suiteletId: 'customscript_4601_witaxsetup',
        suiteletDeploymentId: 'customdeploy_4601_witaxsetup'
    })
        .addMapping('nexus', {type: 'select', label: '', showInList: true, mandatory: true}) //Nexus; rm.WTAX_SETUP['fields'].NEXUS.label
        .addMapping('lookup', {type: 'checkbox', label: ''}) //Enable Tax Lookup on Transactions
        .addMapping('autoapply', {type: 'checkbox', label: ''}) //Auto Apply Withholding Tax
        .addMapping('onpurcs', {type: 'checkbox', label: '', showInList: true, layoutType: ['normal', 'startcol']}) //Available on Purchases
        .addMapping('notonpurcorders', {type: 'checkbox', label: '', showInList: true}) //Disable on Purchse Orders
        .addMapping('purctaxpoint', {type: 'select', label: '', selectOptions: taxPointOptions}) //Tax Point
        .addMapping('purcappliesto', {type: 'select', label: '', selectOptions: appliesToOptions}) //Applies To
        .addMapping('onsales', {type: 'checkbox', label: '', showInList: true, layoutType: ['normal', 'startcol']}) //Available on Sales
    	.addMapping('notonsaleorders', {type: 'checkbox', label: '', showInList: true}) //Disable on Sale Orders
        .addMapping('saletaxpoint', {type: 'select', label: '', selectOptions: taxPointOptions}) //Tax Point
        .addMapping('saleappliesto', {type: 'select', label: '', selectOptions: appliesToOptions}); //Applies To
    
    var applyOnTaxLine = 'custrecord_ph4014_wtax_apply_on_ttl';
    recordMapper.addMapping('applyontaxline', {filter: applyOnTaxLine, column: applyOnTaxLine, field: applyOnTaxLine, excludeInForm: true});


    recordMapper.getValidationErrors = function getValidationErrors(mappedRecord) {
        var validationErrors = [];
        var nexusMapper = _4601.getNexusMapper();
        var nexuses = nexusMapper.all();
        var wiTaxSetups = recordMapper.all();

        if (mappedRecord.id === 'new' && nexusMapper.nexusesWithNoWiTaxSetup(nexuses, wiTaxSetups).map(function (nexus) {
            return nexus.id;
        }).indexOf(mappedRecord.nexus) === -1) {
            validationErrors.push('A withholding tax setup has been created for the selected nexus.');
        }

        return validationErrors;
    };
    
    recordMapper.afterSave = function afterSave(mappedRecord) {
    	var recTaxSetup = nlapiLoadRecord(recordMapper.recordType, mappedRecord.id);
    	recTaxSetup.setFieldValue('custrecord_ph4014_wtax_useroverride', 'T');
    	
    	var id = nlapiSubmitRecord(recTaxSetup);
    };

    return recordMapper;
};
