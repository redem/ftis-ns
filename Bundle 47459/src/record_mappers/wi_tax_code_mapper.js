/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!_4601) { var _4601 = {}; }

_4601.getWiTaxCodeMapper = function getWiTaxCodeMapper() {
    var taxType = {};
    ['name', 'witaxsetup', 'witaxbase', 'purcaccount', 'purcitem', 'saleaccount', 'saleitem'].forEach(function (fieldName) {
        taxType[fieldName] = 'custrecord_4601_wtc_witaxtype.custrecord_4601_wtt_' + fieldName;
    });

    var recordMapper = new _4601.RecordMapper('customrecord_4601_witaxcode', {
        prefix: 'custrecord_4601_wtc_',
        formTitle: '', //Withholding Tax Code
        listTitle: '', //Withholding Tax Codes
        suiteletId: 'customscript_4601_witaxcode',
        suiteletDeploymentId: 'customdeploy_4601_witaxcode'
    })
        .addMapping('name', {label: '', showInList: true, mandatory: true}) //Tax Code
        .addMapping('description', {type: 'textarea', label: '', showInList: true}) //Description
        .addMapping('rate', {type: 'percent', label: '', showInList: true, mandatory: true}) //Rate

        .addMapping('effectivefrom', {type: 'date', label: ''}) //Effective From
        .addMapping('validuntil', {type: 'date', label: ''}) //Valid Until

        .addMapping('percentageofbase', {type: 'percent', label: '', layoutType: ['normal', 'startcol']}) //Percentage of Base
        // TODO implement Amount Thresholds
//        .addMapping('amountthresholds', {type: 'select', label: 'Amount Thresholds', selectOptions: [
//            ['', ''],
//            ['witaxminamount', 'Withholding tax minimum amount'],
//            ['witaxmaxamount', 'Withholding tax maximum amount'],
//            ['minwitaxbaseamount', 'Minimum base amount'],
//            ['witaxbaseexemptamount', 'Withholding tax base exempt amount']
//        ]})
        .addMapping('inactive', {type: 'checkbox', label: ''}) //Inactive
        .addMapping('taxagency', {type: 'select', label: ''}) //Tax Agency
        .addMapping('witaxtype', {type: 'select', label: '', showInList: true, mandatory: true}) //Withholding Tax Type
        .addMapping('witaxtypename', {column: taxType.name, filter: taxType.name, excludeInForm: true})
        .addMapping('witaxsetup', {label: '', column: taxType.witaxsetup, filter: taxType.witaxsetup, field: taxType.witaxsetup, excludeInForm: true}) //Withholding Tax Setup
        .addMapping('witaxbase', {label: '', column: taxType.witaxbase, filter: taxType.witaxbase, excludeInForm: true}) //Withholding Tax Base
        .addMapping('purcaccount', {column: taxType.purcaccount, filter: taxType.purcaccount, excludeInForm: true})
        .addMapping('purcitem', {column: taxType.purcitem, filter: taxType.purcitem, excludeInForm: true})
        .addMapping('saleaccount', {column: taxType.saleaccount, filter: taxType.saleaccount, excludeInForm: true})
        .addMapping('saleitem', {column: taxType.saleitem, filter: taxType.saleitem, excludeInForm: true})
        .addMapping('availableon', {type: 'select', label: '', mandatory: true, selectOptions: [ //Available On
            ['', ''],
            ['both', ''], //Both
            ['onpurcs', ''], //Purchases
            ['onsales', ''] //Sales
        ]})
        .addMapping('istaxgroup', {type: 'checkbox', excludeInForm: true})
        .addMapping('groupedwitaxcodes', {type: 'multiselect', excludeInForm: true})
        .addMapping('nexus', {label: '', showInList: true, column: taxType.witaxsetup, excludeInForm: true}) //Nexus
        ;

    if (_4601.isOneWorld()) {
        recordMapper
                .addMapping('subsidiaries', {type: 'multiselect', label: '', layoutType: ['normal', 'startcol']}) //Subsidiaries
                .addMapping('includechildsubs', {type: 'checkbox', label: ''}); //Include Children
    }


    recordMapper.getValidationErrors = function getValidationErrors(mappedRecord, resourceObject) {
        var validationErrors = [];
        var wiTaxSetupId = _4601.getWiTaxTypeMapper().lookupField(mappedRecord.witaxtype, 'witaxsetup');

        var wiTaxCodesWithSameName = recordMapper.all({columns: ["istaxgroup"], filters: [
            ['internalid', 'noneof', mappedRecord.id],
            ['witaxsetup', 'is', wiTaxSetupId],
            ['name', 'is', mappedRecord.name]
        ]}).getFirst();

        if (wiTaxCodesWithSameName) {
            var recTypeForErrMessage = wiTaxCodesWithSameName.istaxgroup === true ? resourceObject.WTAX_GROUP.title : resourceObject.WTAX_CODE.title;
            
            validationErrors.push(_4601.renderTranslation(resourceObject.WTAX_CODE['fields'].NAME['error_msg'].ERRMSG1.message, {1: recTypeForErrMessage})); //The Withholding Tax Code/Group already exists in the selected Withholding Tax Type's nexus.
        }
        
		if (mappedRecord.effectivefrom > mappedRecord.validuntil) {
			validationErrors.push(resourceObject.WTAX_CODE['fields'].VALID_UNTIL['error_msg'].ERRMSG1.message); //Valid Until date should be later than Effective From date.
		}

        return validationErrors;
    };


    recordMapper.beforeSave = function beforeSave(mappedRecord) {
        mappedRecord.istaxgroup = false;
        mappedRecord.percentageofbase = parseFloat(mappedRecord.percentageofbase) || 100;
    };


    recordMapper.afterSave = function afterSave(mappedRecord) {
        var groupedWiTaxCodeMapper = _4601.getGroupedWiTaxCodeMapper();

        var thisAsGrouped = groupedWiTaxCodeMapper.all({
            filters: [['code', 'is', mappedRecord.id]],
            columns: ['group']
        });

        if (thisAsGrouped.length) {
            var wiTaxGroupMapper = _4601.getWiTaxGroupMapper();
            
            if(_4601.isOneWorld())
            {
	            var subsidiaryMapper = _4601.getSubsidiaryMapper();
	            var subsidiaries = subsidiaryMapper.all({columns: ['name'], filters: [
	                ['isinactive', 'is', 'F'],
	                ['iselimination', 'is', 'F']
	            ]});
            }

            var wiTaxGroups = wiTaxGroupMapper.all({
                filters: [['internalid', 'anyof', thisAsGrouped.map(function (groupedWiTaxCode) {
                    return groupedWiTaxCode.group;
                })]],
                columns: ['groupedwitaxcodes', 'availableon', 'subsidiaries', 'includechildsubs']
            });

            var groupedWiTaxCodeIds = wiTaxGroups.reduce(function (ids, wiTaxGroup) {
                wiTaxGroup.groupedwitaxcodes.forEach(function (id) {
                    if (ids.indexOf(id) === -1) {
                        ids.push(id);
                    }
                });
                return ids;
            }, []);

            var groupedWiTaxCodes = groupedWiTaxCodeMapper.all({
                filters: [['internalid', 'anyof', groupedWiTaxCodeIds]],
                columns: ['code', 'rate', 'basis', 'availableon', 'inactive', 'subsidiaries', 'includechildsubs']
            });

            wiTaxGroups.forEach(function (wiTaxGroup) {
                var keeperIds = [];
                var gonerIds = [];

                if(_4601.isOneWorld())
                {
	                var wiTaxGroupSubsidiaryIds = subsidiaryMapper.filterByIds(subsidiaries, wiTaxGroup.subsidiaries, wiTaxGroup.includechildsubs)
	                        .map(function (subsidiary) {
	                            return subsidiary.id;
	                        });
	
	                wiTaxGroup.groupedwitaxcodes.forEach(function (id) {
	                    var wiTaxCode = groupedWiTaxCodes[id];
	                    var wiTaxCodeSubsidiaryIds = subsidiaryMapper.filterByIds(subsidiaries, wiTaxCode.subsidiaries, wiTaxCode.includechildsubs)
	                            .map(function (subsidiary) {
	                                return subsidiary.id;
	                            });
	
	                    var bin = [wiTaxGroup.availableon, 'both'].indexOf(wiTaxCode.availableon) > -1
	                            && !wiTaxCode.inactive
	                            && wiTaxGroupSubsidiaryIds.every(function (id) {
	                                return wiTaxCodeSubsidiaryIds.indexOf(id) > -1;
	                            })
	                            ? keeperIds
	                            : gonerIds;
	                    bin.push(id);
	                });
                }
                else
                {
                	wiTaxGroup.groupedwitaxcodes.forEach(function (id) {
	                    var wiTaxCode = groupedWiTaxCodes[id];
	
	                    var bin = [wiTaxGroup.availableon, 'both'].indexOf(wiTaxCode.availableon) > -1
	                            && !wiTaxCode.inactive
	                            ? keeperIds
	                            : gonerIds;
	                    bin.push(id);
	                });
                }

                var rate = keeperIds.reduce(function (rate, id) {
                    var groupedWiTaxCode = groupedWiTaxCodes[id];
                    return rate + (groupedWiTaxCode.rate * groupedWiTaxCode.basis / 100);
                }, 0);

                wiTaxGroupMapper.saveRecord({
                    id: wiTaxGroup.id,
                    rate: rate,
                    groupedwitaxcodes: keeperIds
                });

                gonerIds.forEach(function (id) {
                    groupedWiTaxCodeMapper.deleteRecord(id);
                });
            });
        }
    };


    return recordMapper;
};
