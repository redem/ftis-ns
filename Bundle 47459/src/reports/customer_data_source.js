/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Jul 2012     ljamolod
 *
 */

if (!SAWT_REPORT) var SAWT_REPORT = {};

if (!_4601) { var _4601 = {}; }

SAWT_REPORT.getCustomerData = (function () {
	var _NONE = "- None -";
	var _MAX_NUM_SEARCH_RESULTS = 1000;
	
    //column names for retrieval
    var _colTaxCode = 'custcol_4601_witaxcode';
    var _colTaxAmount = 'custcol_4601_witaxamount';
    var _colTaxBaseAmount = 'custcol_4601_witaxbaseamount';
    var _colCustomerId = 'internalid';
    var _colJournalType = 'custcol_ph4014_src_jrnltrantypeid';
    var _colFirstname = 'firstname';
    var _colLastName = 'lastname';
    var _colMiddleName = 'middlename';
    var _colCompanyName = 'companyname';
    var _colTin = 'vatregnumber';
    var _colIsPerson = 'isperson';
    var _colType = 'type';
    var _colInternalID = 'internalid';
    var _labelTaxCodeName = 'taxCodeName';
    var _labelTaxCodeDesc = 'taxCodeDescription';
    var _labelTaxCodeRate = 'taxCodeRate';
    var _labelTaxTypeDesc = 'taxTypeDesc';
    var _labelIsTaxGroup = 'isTaxGroup';
    var _labelGroupTaxCodes = 'groupTaxCodes';
    var _labelTransID = 'transactionID';
    
    var _objReferenceNo = 'tranid';
    var _objRecordType = 'recordtype';
    var _objMemo = 'memomain';
    var _objTransactionDate = 'trandate';
    
    var billCreditMap = new Array();
    var totalTaxBase = {sumTaxBase : 0.0};
    var totalTaxWithheld = {sumTaxWithheld : 0.0};
    
    var allowedWithholdingTaxCodeIds = new Array();
    var groupWtaxCodes = new Array();
    var allValidGroupedWtaxCodes = new Array();
    var childrenTaxPeriod = new Array();
    var objSelectedPeriod = null;
    
    function getCustomerData (WTaxCodeIds, params, searchType) {
        var sawtSequenceNumber = 1;
        var sawtLineItemMap = new Array();
        var _IsGenericWHTaxSearch = true;
        var arrSawtLineItem = new Array();
        allowedWithholdingTaxCodeIds = WTaxCodeIds;
        
        if (allowedWithholdingTaxCodeIds.length > 0) {
        	var taxTypesMap = _4601.getTaxTypesDetails(allowedWithholdingTaxCodeIds);
            var saleTaxPoint = _4601.getSaleTaxPoint(params.subid, params.nexusid); 
            
            groupWtaxCodes = _4601.getTaxGroupWithValidTaxCodes(allowedWithholdingTaxCodeIds);
            
            allValidGroupedWtaxCodes = _4601.getAllValidGroupedTaxCodes(allowedWithholdingTaxCodeIds);
            objSelectedPeriod = new SFC.System.TaxPeriod().Load(params.periodfrom, params.fiscalCalendarId);
            if (objSelectedPeriod.GetType() == 'quarter' && childrenTaxPeriod.length == 0)
  				 childrenTaxPeriod = objSelectedPeriod.GetChildren();
            
            //search for new data
            arrSawtLineItem = getCustomerData.extractPayeeData(getCustomerData.getSAWTSearchResults(params, _IsGenericWHTaxSearch, saleTaxPoint, searchType), sawtLineItemMap,
            					arrSawtLineItem, sawtSequenceNumber, totalTaxBase, totalTaxWithheld, taxTypesMap, saleTaxPoint, searchType, _IsGenericWHTaxSearch);
            
            if (saleTaxPoint != 'onpayment')
            	//search for old data
            	arrSawtLineItem = getCustomerData.extractPayeeData(getCustomerData.getSAWTSearchResults(params, !_IsGenericWHTaxSearch, saleTaxPoint, searchType), sawtLineItemMap,
            					arrSawtLineItem, sawtSequenceNumber, totalTaxBase, totalTaxWithheld, taxTypesMap, saleTaxPoint, searchType, !_IsGenericWHTaxSearch);
            
            arrSawtLineItem = getCustomerData.cleanUpPayeeData(arrSawtLineItem);
        }
        
        return new SAWT_REPORT.SawtLineItemsWithTotal(arrSawtLineItem, Math.abs(totalTaxBase.sumTaxBase), Math.abs(totalTaxWithheld.sumTaxWithheld));
    }
    
  
    getCustomerData.getSAWTSearchResults = function getSAWTSearchResults(params, isGenericSearch, saleTaxPoint, searchType)
    {
        var searchFilters = getCustomerData.createPayeeSearchFilters(params, isGenericSearch, saleTaxPoint);
        var searchColumns = getCustomerData.createPayeeSearchColumns(params, isGenericSearch, saleTaxPoint, searchType);

        var search = nlapiCreateSearch('transaction', searchFilters, searchColumns);
        var searchResults = search.runSearch();
		
        return searchResults;
    };
    
    getCustomerData.getPaymentRecordIds = function getPaymentRecordIds(params) {
    	var arrDocRefIds = new Array();
    	
    	arrDocRefIds = getCustomerData.getPaymentTransactions(params, arrDocRefIds);
		arrDocRefIds = getCustomerData.getCashTransactions(params, arrDocRefIds);
    	return arrDocRefIds;
    };
    
	getCustomerData.getCashTransactions = function getCashTransactions(params, arrDocRefIds) {
    	// Get Cash Sale
		var saleTypes = [];
		saleTypes.push('CashSale');
		saleTypes.push('CashRfnd');
		
		var paymentFilters = [];
		paymentFilters.push(['type', 'anyof', saleTypes]);
		paymentFilters.push('AND');
		paymentFilters.push(['customer.internalid', 'noneof', '@NONE@']); //retrieve records with customer only
		paymentFilters.push('AND');
		paymentFilters.push(['custcol_ph4014_src_jrnltrantypeid', 'anyof','@NONE@']); //filter out old records
		paymentFilters.push('AND');
		paymentFilters.push(['custcol_4601_witaxapplies', 'is', 'T']);
		paymentFilters.push('AND');
		paymentFilters.push(['isreversal', 'is', 'F']);
		paymentFilters = _4601.getTaxPeriodFilters(paymentFilters, objSelectedPeriod);
		paymentFilters = _4601.getWTaxCodeFilters(paymentFilters, null, {
			allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
			groupWtaxCodes: groupWtaxCodes});
		if (_isOneWorldInstance() && params.subid) {
			paymentFilters.push('AND');
			paymentFilters.push(['subsidiary', 'is', params.subid]);
		}
		
		var index = 0;
        var search = nlapiCreateSearch('transaction', paymentFilters);
        var resultSet = search.runSearch();
        
        do {
            var sr = resultSet.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
            
            if (sr) {
                for (var i = 0; i < sr.length; i++) {
                    arrDocRefIds.push(sr[i].getId());
                }
            }
            
            index += sr.length;
        } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);

		return arrDocRefIds;
    };
    
    getCustomerData.getPaymentTransactions = function getPaymentTransactions(params,  arrDocRefIds) {
    	// Get Credit Memos
    	var creditFilters = [];

    	var arrPayTransTypes = [];
    	arrPayTransTypes.push('CustCred');
    	creditFilters.push(['type', 'anyof', arrPayTransTypes]);
    	creditFilters.push('AND');
    	creditFilters.push(['custbody_4601_pymnt_ref_id', 'noneof', '@NONE@']);
    	creditFilters.push('AND');
    	creditFilters.push(['custbody_4601_doc_ref_id', 'noneof', '@NONE@']);
    	creditFilters.push('AND');
    	creditFilters.push(['mainline', 'is', 'T']);
    	creditFilters = _4601.getTaxPeriodFilters(creditFilters, objSelectedPeriod);
    	creditFilters = _4601.getWTaxCodeFilters(creditFilters, 'custbody_4601_doc_ref_id.custcol_4601_witaxcode', {
    		allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
    		groupWtaxCodes: groupWtaxCodes});

    	if (_isOneWorldInstance() && params.subid) {
    		creditFilters.push('AND');
    		creditFilters.push(['subsidiary', 'is', params.subid]);
    	}

    	var creditColumns = [];
    	creditColumns.push(new nlobjSearchColumn('custbody_4601_doc_ref_id'));
    	creditColumns.push(new nlobjSearchColumn('custbody_4601_pymnt_ref_id'));
    	creditColumns.push(new nlobjSearchColumn('amount'));

    	var index = 0;
    	var _docRef = '';
        var _pymntRef = '';
        var _amount = 0.0;
        var search = nlapiCreateSearch('transaction', creditFilters, creditColumns);
        var resultSet = search.runSearch();
        
        do {
            var sr = resultSet.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
            
            if (sr) {
                for (var i = 0; i < sr.length; i++) {
                    _docRef = sr[i].getValue('custbody_4601_doc_ref_id');
                    _pymntRef = sr[i].getValue('custbody_4601_pymnt_ref_id');
                    _amount = sr[i].getValue('amount');
                    if (_amount != 0) {
                        if (billCreditMap[_docRef] == null) {
                            arrDocRefIds.push(_docRef);
                            billCreditMap[_docRef] = {};
                        }
                        billCreditMap[_docRef][_pymntRef] = {}; 
                        billCreditMap[_docRef][_pymntRef].pymntRef = _pymntRef;
                        billCreditMap[_docRef][_pymntRef].taxWithheld = Math.abs(_amount);
                    }
                }
            }
            
            index += sr.length;
        } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);
    	
    	return arrDocRefIds;
    };
    
    
    getCustomerData.getTransactions = function getTransactions(params, arrTransID) {
    	var filters = [];
    	
    	if (allowedWithholdingTaxCodeIds != null && allowedWithholdingTaxCodeIds.length > 0) {
    		var arrTransTypes = new Array();
    		arrTransTypes.push('CashSale');
    		arrTransTypes.push('CustInvc');
    		arrTransTypes.push('CustCred');
            
        	filters.push(['type', 'anyof', arrTransTypes]);
        	filters.push('AND');
        	filters.push(['customer.internalid', 'noneof', '@NONE@']);
        	filters.push('AND');
        	filters.push([_colJournalType, 'anyof','@NONE@']);
        	filters.push('AND');
        	filters.push(['custcol_4601_witaxapplies', 'is', 'T']);
        	filters.push('AND');
        	filters.push(['isreversal', 'is','F']);
        	filters.push('AND');
			filters.push(['custcol_4601_witaxbaseamount', 'isnotempty', null]);
			
        	if (params.subid != null && _isOneWorldInstance()){
        		filters.push('AND');
        		filters.push(['subsidiary', 'is', params.subid]);
        	}
        	filters = _4601.getTaxPeriodFilters(filters, objSelectedPeriod);

        	var filterTaxCodes = [];
        	filterTaxCodes.push(['custcol_4601_witaxcode', 'anyof', allowedWithholdingTaxCodeIds]);
        	if (groupWtaxCodes != null && groupWtaxCodes.length > 0) {
        		filterTaxCodes.push(
        				'OR',
        				['custcol_4601_witaxcode', 'anyof', groupWtaxCodes]
        		);
        	}
        	filters.push('AND');
        	filters.push(filterTaxCodes);
            
        	var index = 0;
            var search = nlapiCreateSearch('transaction', filters);
            var resultSet = search.runSearch();
            
            do {
                var sr = resultSet.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
                
                if (sr) {
                    for (var i = 0; i < sr.length; i++) {
                        arrTransID.push(sr[i].getId());
                    }
                }
                
                index += sr.length;
            } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);
    	}
    	
		return arrTransID;
    };
    
    
    getCustomerData.createPayeeSearchFilters = function createPayeeSearchFilters(params, isNewSearch, saleTaxPoint) {
    	
    	var searchFilters = new Array();

    	if (_isOneWorldInstance() && params.subid) {
    		searchFilters.push(['subsidiary', 'is', params.subid]);
    		searchFilters.push('AND');
    	}

    	var arrTransID = new Array();
    	//filters for new search should also filter out results that can be returned by the old search
    	if (isNewSearch) {
    		if (saleTaxPoint == 'onpayment') {
    			var arrPaymentIds = getCustomerData.getPaymentRecordIds(params);
    			if (arrPaymentIds.length > 0)
    				searchFilters.push([_colInternalID, 'anyof', arrPaymentIds]);
    			else
    				//Since there are no payment records retrieved, set the internalid to NONE
    				//which in essence results to no record/s to be searched;
    				//otherwise it will seach records intended for onaccrual setup
    				searchFilters.push([_colInternalID, 'is', '@NONE@']);
    		} else if (saleTaxPoint == 'onaccrual') {
    			arrTransID = getCustomerData.getTransactions(params, arrTransID);
    			if (arrTransID.length > 0)
    				searchFilters.push([_colInternalID, 'anyof', arrTransID]);
    			else
    				searchFilters.push([_colInternalID, 'is', '@NONE@']);
    		}
    		searchFilters.push('AND');
    		searchFilters.push(['customer.internalid', 'noneof', '@NONE@']); //retrieve records with customer only
    		searchFilters.push('AND');
    		searchFilters.push([_colJournalType, 'anyof','@NONE@']); //filter out old records
    		searchFilters.push('AND');
    		searchFilters.push(['custcol_4601_witaxapplies', 'is', 'T']);
    		searchFilters.push('AND');
    		searchFilters.push(['isreversal', 'is', 'F']);
    		searchFilters.push('AND');
    		searchFilters.push(['posting', 'is', 'T']);
    		searchFilters.push('AND');
    		searchFilters.push(['voided', 'is', 'F']);
    	} else {
    		var journalTypes = new Array();
    		journalTypes.push("CustPymt");
    		journalTypes.push("CashSale");
    		var journalTypeIds = _App.getJournalTypeIds(journalTypes);

    		searchFilters.push([_colJournalType, 'anyof',journalTypeIds]);
    		searchFilters.push('AND');
    		searchFilters.push(['custcol_4601_witaxline', 'is', 'T']);
    		searchFilters.push('AND');
    		searchFilters.push(['custcol_4601_witaxapplies', 'is', 'T']);
    		searchFilters.push('AND');
    		searchFilters.push([_colTaxCode, 'anyof', allowedWithholdingTaxCodeIds]);
    		searchFilters.push('AND');
    		searchFilters.push(['custbody_ph4014_wtax_reversal_flag', 'is', 'F']);
    		searchFilters.push('AND');
    		searchFilters.push(['custcol_ph4014_src_custid', 'noneof', '@NONE@']);
    		searchFilters = _4601.getTaxPeriodFilters(searchFilters, objSelectedPeriod);
    		searchFilters = _4601.getJournalReversalFilters(searchFilters);    	}

    	return searchFilters;
    };
    
    getCustomerData.createPayeeSearchColumns = function createPayeeSearchColumns(params, isNewSearch, saleTaxPoint, searchType) {
    	var searchColumns = new Array();
    	searchColumns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM')
    		.setFormula('{'+ _colTaxAmount +'}').setLabel(_colTaxAmount));
    	searchColumns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM')
			.setFormula('{'+ _colTaxBaseAmount +'}').setLabel(_colTaxBaseAmount));
    	searchColumns.push(new nlobjSearchColumn('formulapercent', null, 'group').setFormula(
			'{custcol_4601_witaxcode.custrecord_4601_wtc_rate}').setLabel(_labelTaxCodeRate));
    	searchColumns.push( new nlobjSearchColumn(_colInternalID,null,'group').setLabel(_labelTransID));
    	searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
			'{custcol_4601_witaxcode.custrecord_4601_wtc_istaxgroup}').setLabel(_labelIsTaxGroup));
		searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
			'{custcol_4601_witaxcode.custrecord_4601_wtc_groupedwitaxcodes}').setLabel(_labelGroupTaxCodes));
		searchColumns.push(new nlobjSearchColumn(_colTaxCode,null,'group'));
    	
    	if (searchType == 'DETAIL' || searchType == 'SAWT_DETAIL' || searchType.search(/SALES_/) === 0) {
    		searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_4601_witaxcode.custrecord_4601_wtc_witaxtype}').setLabel(_labelTaxTypeDesc));
    		searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_4601_witaxcode.custrecord_4601_wtc_name}').setLabel(_labelTaxCodeName).setSort(false));
    		searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
    			'{custcol_4601_witaxcode.custrecord_4601_wtc_description}').setLabel(_labelTaxCodeDesc));
    		
    		if (searchType == 'SALES_DETAIL') {
    			searchColumns.push(new nlobjSearchColumn(_objReferenceNo, null, 'group'));
                searchColumns.push(new nlobjSearchColumn(_objRecordType, null, 'group'));
                searchColumns.push(new nlobjSearchColumn(_objMemo, null, 'group'));
                searchColumns.push(new nlobjSearchColumn(_objTransactionDate, null, 'group'));
    		}
    	}
    		
    	if (isNewSearch) {
    		searchColumns.push(new nlobjSearchColumn(_colCustomerId,'customer','group'));
    		searchColumns.push(new nlobjSearchColumn(_colFirstname,'customer','group'));
    		searchColumns.push(new nlobjSearchColumn(_colLastName,'customer','group'));
    		searchColumns.push(new nlobjSearchColumn(_colMiddleName,'customer','group'));
    		searchColumns.push(new nlobjSearchColumn(_colCompanyName,'customer','group'));
    		searchColumns.push(new nlobjSearchColumn(_colTin,'customer','group'));
    		searchColumns.push(new nlobjSearchColumn(_colIsPerson,'customer','group'));
    		
    	} else {
		    //customer columns
		    searchColumns.push(new nlobjSearchColumn('custcol_ph4014_src_custid',null,'group').setLabel(_colCustomerId));
		    searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
		    		'{custcol_ph4014_src_custid.' + _colCompanyName + '}').setLabel(_colCompanyName));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_ph4014_src_custid.' + _colIsPerson + '}').setLabel(_colIsPerson));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_ph4014_src_custid.' + _colLastName + '}').setLabel(_colLastName));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_ph4014_src_custid.' + _colMiddleName + '}').setLabel(_colMiddleName));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_ph4014_src_custid.' + _colFirstname +'}').setLabel(_colFirstname));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{custcol_ph4014_src_custid.' + _colTin + '}').setLabel(_colTin));
    	}
    	
    	searchColumns.push( new nlobjSearchColumn(_colType,null,'group'));
    	
    	return searchColumns;
    };

    getCustomerData.setupColumnLookupByName = function setupColumnLookupByName(searchResultRow)
    {
        var _searchColumnLookupByName = new Array();

        var allColumns = searchResultRow.getAllColumns();
        
        for (var i = 0; allColumns && i < allColumns.length; ++i)
        {
        	var columnIndex = (allColumns[i].getLabel() == null || allColumns[i].getLabel() == '') ? 
        						allColumns[i].getName() : allColumns[i].getLabel();
            _searchColumnLookupByName[columnIndex] = allColumns[i];
        }
        
        return _searchColumnLookupByName;
    };

    /**
     * Extracts the payee data from the result set.
     * 
     * @param resultSet The result set to extract the data from
     * @param sawtLineItemMap Associative array containing the SAWT line items
     * @param arrSawtLineItem The array that contains the line items
     * @param sawtSequenceNumber The sequence number for SAWT
     * @param totalTaxBase The sum of all tax base amounts
     * @param totalTaxWithheld The sum of all withheld tax
     * @returns
     */
    getCustomerData.extractPayeeData = function extractPayeeData(resultSet, sawtLineItemMap, arrSawtLineItem, sawtSequenceNumber, totalTaxBase, totalTaxWithheld, taxTypesMap, saleTaxPoint, 
    		searchType, isGenericSearch) {
    	var offset = 0;
		var searchResults = null;
    	var transID = '';
    	var totalAmountMap = new Array();
    	var pymntRef = '';
		
    	if (resultSet != null) {
    		searchResults = resultSet.getResults(offset, offset + _MAX_NUM_SEARCH_RESULTS);
    		if (searchResults != null && searchResults.length > 0) {
        		var lookupSearchColumn = getCustomerData.setupColumnLookupByName(searchResults[0]);
        		while (searchResults != null && searchResults.length > 0) {
	        		if (saleTaxPoint == 'onpayment') {
	            		var arrTransID = new Array();
	            		
	            		for (var j in searchResults) {
	                		transID = searchResults[j].getValue(lookupSearchColumn[_labelTransID]);
	                		if (transID != null && billCreditMap[transID] != null)
	                			arrTransID.push(transID);
	                	}
	            		
	            		if (arrTransID.length > 0)
	            			totalAmountMap = _4601.populateTotalAmountMap(arrTransID);
	            	}
	        		
	        		for (var i = 0; searchResults && i < searchResults.length; ++i)
	    	        {
	    	            transID = searchResults[i].getValue(lookupSearchColumn[_labelTransID]);
	    	            
	    	            if (saleTaxPoint == 'onpayment' && billCreditMap[transID] != null) {
	    	    			var billPayments = billCreditMap[transID];
	    	    			
	    	    			for (var h in billPayments) {
	    	    				var payment = billPayments[h];
	    	    				pymntRef = payment.pymntRef;
	    	    				sawtLineItemMap = getCustomerData.calculateAmountTransaction(searchResults, i, arrSawtLineItem, sawtSequenceNumber, saleTaxPoint, sawtLineItemMap, taxTypesMap, 
	    	    						searchType, lookupSearchColumn, totalAmountMap, transID, pymntRef, isGenericSearch);
	    	    			}
	    	    		} else {
	    	    			sawtLineItemMap = getCustomerData.calculateAmountTransaction(searchResults, i, arrSawtLineItem, sawtSequenceNumber, saleTaxPoint, sawtLineItemMap, taxTypesMap, 
	    	    					searchType, lookupSearchColumn, totalAmountMap, transID, pymntRef, isGenericSearch);
	    	    		}
    	            }
	        		
    	            offset = offset + _MAX_NUM_SEARCH_RESULTS;
            		searchResults = resultSet.getResults(offset, offset + _MAX_NUM_SEARCH_RESULTS);
    	        }
        	}
        }
       return arrSawtLineItem;
    };
    
    
    getCustomerData.calculateAmountTransaction = function calculateAmountTransaction(searchResults, i, arrSawtLineItem, sawtSequenceNumber, saleTaxPoint, sawtLineItemMap, taxTypesMap, 
    		searchType, lookupSearchColumn, totalAmountMap, transID, pymntRef, isGenericSearch) {
    	var taxCodeId = '';
		var isTaxGroup = '';
		var groupTaxCodes = null;
		var taxCodeName = '';
		var taxCodeDesc = '';
		var taxRate = '';
		var taxType = '';
		var refNo = '';
    	var recordType = '';
    	var memo = '';
    	var tranDate = '';
    	var tmpTaxBase = 0.0;
		var tmpTaxWitheld = 0.0;
		var tmpTaxRate = '';
		var taxRateComp = 0.0;
		var wtaxDetails = {};
    	
    	tmpTaxBase = parseFloat(searchResults[i].getValue(lookupSearchColumn[_colTaxBaseAmount]),10);
        tmpTaxWitheld = parseFloat(searchResults[i].getValue(lookupSearchColumn[_colTaxAmount]), 10);
        tmpTaxRate = searchResults[i].getValue(lookupSearchColumn[_labelTaxCodeRate]);
        taxRateComp = parseFloat(tmpTaxRate.replace('%', ''));
        isTaxGroup = searchResults[i].getValue(lookupSearchColumn[_labelIsTaxGroup]);
        groupTaxCodes = searchResults[i].getValue(lookupSearchColumn[_labelGroupTaxCodes]);
    	
    	if (searchType == 'DETAIL' || searchType == 'SAWT_DETAIL' || searchType.search(/SALES_/) === 0) {
    		if (isTaxGroup != 'T')
    			taxType = searchResults[i].getValue(lookupSearchColumn[_labelTaxTypeDesc]);
            
            if (searchType == 'SALES_DETAIL') {
            	refNo = searchResults[i].getValue(lookupSearchColumn[_objReferenceNo]) || '';
    			recordType = searchResults[i].getValue(lookupSearchColumn[_objRecordType]) || '';
    			memo = searchResults[i].getValue(lookupSearchColumn[_objMemo]) || '';
    			tranDate = searchResults[i].getValue(lookupSearchColumn[_objTransactionDate]) || '';
            }
        }
            
        if (isTaxGroup == 'T') { 
        	var taxRateTotalComp = 0.0;
			var taxAmountTotal = 0.0;
        	var arrTaxCodes = groupTaxCodes.split(',');
			var arrGroupTaxCodes = _4601.getTaxCodesByTaxGroup(arrTaxCodes, allValidGroupedWtaxCodes);
			var taxBasis = 0.0;
            var partialTaxAmount = 0;
			
			if (saleTaxPoint == 'onpayment' && transID != null && billCreditMap[transID] != null)
				taxAmountTotal = _4601.getLineAmountDistribution(tmpTaxBase, tmpTaxWitheld, billCreditMap[transID][pymntRef].taxWithheld, totalAmountMap[transID].totalTaxAmount);
			else
				taxAmountTotal = tmpTaxWitheld;

			taxRateTotalComp = taxRateComp;
			for (var j in arrGroupTaxCodes) {
				taxCodeId = arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_code');
				taxBasis = parseFloat(arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_basis'));
				taxCodeName = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_name', 'custrecord_4601_gwtc_code');
				taxCodeDesc = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_description', 'custrecord_4601_gwtc_code') == '' ? _NONE : arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_description', 'custrecord_4601_gwtc_code');
				taxType = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_witaxtype', 'custrecord_4601_gwtc_code');
				taxRate = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_rate', 'custrecord_4601_gwtc_code');
				taxRateComp = parseFloat(taxRate.replace('%', ''));
				
				if (allowedWithholdingTaxCodeIds.indexOf(taxCodeId) > -1) {
					if (saleTaxPoint == 'onaccrual')
					    partialTaxAmount = -(tmpTaxBase * ((taxRateComp * (taxBasis/100))/100));
    				else if (saleTaxPoint == 'onpayment') {
    					var taxRateRatio = taxRateTotalComp == 0 ? 0 : taxRateComp / taxRateTotalComp;
    					partialTaxAmount = taxRateRatio * taxAmountTotal;
    					tmpTaxBase = -(taxRateTotalComp == 0 ? 0 : partialTaxAmount / (taxRateComp/100));
    				}
    				
					wtaxDetails = {
							tmpTaxBase: (searchType == 'SUMMARY' && j > 0) ? 0 : (isGenericSearch ? -tmpTaxBase : tmpTaxBase), 
							tmpTaxWitheld: partialTaxAmount, 
							taxCodeId: taxCodeId, 
							taxType: taxType, 
							tmpTaxRate: taxRate,
							taxCodeName: taxCodeName, 
							taxCodeDesc: taxCodeDesc, 
							refNo: refNo, 
							recordType: recordType, 
							memo: memo, 
							tranDate: tranDate, 
							searchType: searchType, 
							transID: transID
					};
					
    				sawtLineItemMap = getCustomerData.populateLineItemMap(sawtLineItemMap, arrSawtLineItem, searchResults, i, lookupSearchColumn, sawtSequenceNumber, wtaxDetails, taxTypesMap);
    				getCustomerData.updateGrandTotal(parseFloat(wtaxDetails.tmpTaxBase || 0), parseFloat(wtaxDetails.tmpTaxWitheld || 0));
				}
			}
        } else {
        	taxCodeId = searchResults[i].getValue(lookupSearchColumn[_colTaxCode]);
        	
        	if (allowedWithholdingTaxCodeIds.indexOf(taxCodeId) > -1) {
        		if (searchType != 'SUMMARY') {
                	taxCodeName = searchResults[i].getValue(lookupSearchColumn[_labelTaxCodeName]);
                	taxCodeDesc = searchResults[i].getValue(lookupSearchColumn[_labelTaxCodeDesc]);
            	}
            	
            	if (saleTaxPoint == 'onpayment' && transID != null && billCreditMap[transID] != null) {
            		tmpTaxWitheld = _4601.getLineAmountDistribution(tmpTaxBase, tmpTaxWitheld, billCreditMap[transID][pymntRef].taxWithheld, totalAmountMap[transID].totalTaxAmount);
    				tmpTaxBase = -(tmpTaxWitheld / (taxRateComp/100));
    			}
            	
            	wtaxDetails = {
						tmpTaxBase: isGenericSearch ? -tmpTaxBase : tmpTaxBase, 
						tmpTaxWitheld: tmpTaxWitheld, 
						taxCodeId: taxCodeId, 
						taxType: taxType,
						tmpTaxRate: tmpTaxRate,
						taxCodeName: taxCodeName, 
						taxCodeDesc: taxCodeDesc, 
						refNo: refNo, 
						recordType: recordType, 
						memo: memo, 
						tranDate: tranDate, 
						searchType: searchType, 
						transID: transID
				};
            	
            	sawtLineItemMap = getCustomerData.populateLineItemMap(sawtLineItemMap, arrSawtLineItem, searchResults, i, lookupSearchColumn, sawtSequenceNumber, wtaxDetails, taxTypesMap);
            	getCustomerData.updateGrandTotal(parseFloat(wtaxDetails.tmpTaxBase || 0), parseFloat(wtaxDetails.tmpTaxWitheld || 0));
        	}
        }
        
		return sawtLineItemMap;
    };
    
    
    getCustomerData.updateGrandTotal = function updateGrandTotal(taxBase, taxAmount) {
        totalTaxBase.sumTaxBase += taxBase;
        totalTaxWithheld.sumTaxWithheld += taxAmount;
    };
    
    
    getCustomerData.getLineAmountDistribution = function getLineAmountDistribution(taxBase, taxAmount, transID, totalAmountMap, pymntRef) {
		var creditWTaxPayment = 0.0;
		var lineCreditWTaxPayment = 0.0;
		
		amountRatio = getCustomerData.getAmountRatio(taxBase, taxAmount);
		creditWTaxPayment = billCreditMap[transID][pymntRef].taxWithheld;
		
		var wtaxPaymentRatio = taxAmount / totalAmountMap[transID].totalTaxAmount;
		
		lineCreditWTaxPayment = creditWTaxPayment * wtaxPaymentRatio;
		
		return lineCreditWTaxPayment;
    };
    
    getCustomerData.getAmountRatio = function getAmountRatio(taxBase, taxAmount) {
    	return (taxAmount / (taxBase - taxAmount));
    };
    
    
    getCustomerData.populateLineItemMap = function populateLineItemMap(sawtLineItemMap, arrSawtLineItem, searchResults, i, lookupSearchColumn, sawtSequenceNumber, wtaxDetails, taxTypesMap) {
    	
    	var sawtLineItem = null;
    	
    	var arrMsg = [];
    	
    	var customerTin = customerTin = searchResults[i].getValue(lookupSearchColumn[_colTin]) == _NONE ? 
				'' : searchResults[i].getValue(lookupSearchColumn[_colTin]);
    	var customerId = searchResults[i].getValue(lookupSearchColumn[_colCustomerId]);
    	var taxTypeDesc = wtaxDetails.taxType == '' ? '' : taxTypesMap[wtaxDetails.taxType].desc ;
    	
    	var transType = searchResults[i].getValue(lookupSearchColumn[_colType]);
    	
    	if (transType == 'CashRfnd') {
    		wtaxDetails.tmpTaxBase = -wtaxDetails.tmpTaxBase;
    		wtaxDetails.tmpTaxWitheld = -wtaxDetails.tmpTaxWitheld; 
        }
    	
    	var taxBase = !!wtaxDetails.tmpTaxBase ? wtaxDetails.tmpTaxBase : 0;
    	var taxWitheld = !!wtaxDetails.tmpTaxWitheld ? wtaxDetails.tmpTaxWitheld : 0;
    	
    	//If the Customer - Tax Code ID combination is not yet in the map, create a new SAWT line item entry
        //Otherwise, just add to the base amount and withheld amount in the existing entry
    	if (wtaxDetails.searchType == 'DETAIL' || wtaxDetails.searchType == 'SAWT_DETAIL') {
    		if (!(sawtLineItemMap[customerId + '-' + wtaxDetails.taxCodeId])) {
            	sawtLineItem = new SAWT_REPORT.SawtLineItem(
    	                sawtSequenceNumber,
    	                customerTin, // waTin, 
    					'', // waRegName, 
    					'', // returnPeriodFromDate 
    					'', // returnPeriodToDate 
    					wtaxDetails.taxCodeName, // lookup atc 
    					wtaxDetails.taxCodeDesc, // lookup desc 
    					taxBase,
    					wtaxDetails.tmpTaxRate, // tax code rate
    					taxWitheld,
    					wtaxDetails.taxCodeId,
    					customerId,
    					wtaxDetails.refNo,
    					wtaxDetails.recordType,
    					wtaxDetails.memo,
    					wtaxDetails.tranDate);

    	            sawtLineItem.setRegName(searchResults[i].getValue(lookupSearchColumn[_colCompanyName]));
                    sawtLineItem.setIsPerson(searchResults[i].getValue(lookupSearchColumn[_colIsPerson]));
                    sawtLineItem.setLastName(searchResults[i].getValue(lookupSearchColumn[_colLastName]));
                    sawtLineItem.setMiddleName(searchResults[i].getValue(lookupSearchColumn[_colMiddleName]));
                    sawtLineItem.setFirstName(searchResults[i].getValue(lookupSearchColumn[_colFirstname]));
                    sawtLineItem.taxTypeDesc = taxTypeDesc;
                    
                    arrSawtLineItem.push(sawtLineItem);
                    sawtLineItemMap[customerId + '-' + wtaxDetails.taxCodeId] = sawtLineItem;
                    sawtSequenceNumber += 1;
            } else {
            	sawtLineItemMap[customerId + '-' + wtaxDetails.taxCodeId].addToTaxBase(taxBase);
                sawtLineItemMap[customerId + '-' + wtaxDetails.taxCodeId].addToTaxWithheld(taxWitheld);
            }
    	} else if (wtaxDetails.searchType == 'SUMMARY') {
    		if (!(sawtLineItemMap[customerId])) {
            	sawtLineItem = new SAWT_REPORT.SawtLineItem(
    	                sawtSequenceNumber,
    	                customerTin, // waTin, 
    					'', // waRegName, 
    					'', // returnPeriodFromDate 
    					'', // returnPeriodToDate 
    					wtaxDetails.taxCodeName, // lookup atc 
    					wtaxDetails.taxCodeDesc, // lookup desc 
    					taxBase,
    					wtaxDetails.tmpTaxRate, // tax code rate
    					taxWitheld,
    					wtaxDetails.taxCodeId,
    					customerId,
    					wtaxDetails.refNo,
    					wtaxDetails.recordType,
    					wtaxDetails.memo,
    					wtaxDetails.tranDate);

    	            sawtLineItem.setRegName(searchResults[i].getValue(lookupSearchColumn[_colCompanyName]));
                    sawtLineItem.setIsPerson(searchResults[i].getValue(lookupSearchColumn[_colIsPerson]));
                    sawtLineItem.setLastName(searchResults[i].getValue(lookupSearchColumn[_colLastName]));
                    sawtLineItem.setMiddleName(searchResults[i].getValue(lookupSearchColumn[_colMiddleName]));
                    sawtLineItem.setFirstName(searchResults[i].getValue(lookupSearchColumn[_colFirstname]));
                    sawtLineItem.taxTypeDesc = taxTypeDesc;
                    
                    arrSawtLineItem.push(sawtLineItem);
                    sawtLineItemMap[customerId] = sawtLineItem;
                    sawtSequenceNumber += 1;
            } else {
            	sawtLineItemMap[customerId].addToTaxBase(taxBase);
                sawtLineItemMap[customerId].addToTaxWithheld(taxWitheld);
            }
    	} else if (wtaxDetails.searchType == 'SALES_DETAIL') {
    		if (!(sawtLineItemMap[wtaxDetails.taxCodeId + '-' + wtaxDetails.transID])) {
            	sawtLineItem = new SAWT_REPORT.SawtLineItem(
    	                sawtSequenceNumber,
    	                customerTin, // waTin, 
    					'', // waRegName, 
    					'', // returnPeriodFromDate 
    					'', // returnPeriodToDate 
    					wtaxDetails.taxCodeName, // lookup atc 
    					wtaxDetails.taxCodeDesc, // lookup desc 
    					taxBase,
    					wtaxDetails.tmpTaxRate, // tax code rate
    					taxWitheld,
    					wtaxDetails.taxCodeId,
    					customerId);

    	            sawtLineItem.setRegName(searchResults[i].getValue(lookupSearchColumn[_colCompanyName]));
                    sawtLineItem.setIsPerson(searchResults[i].getValue(lookupSearchColumn[_colIsPerson]));
                    sawtLineItem.setLastName(searchResults[i].getValue(lookupSearchColumn[_colLastName]));
                    sawtLineItem.setMiddleName(searchResults[i].getValue(lookupSearchColumn[_colMiddleName]));
                    sawtLineItem.setFirstName(searchResults[i].getValue(lookupSearchColumn[_colFirstname]));
                    sawtLineItem.taxTypeDesc = taxTypeDesc;
                    
                    sawtLineItem.refNo = wtaxDetails.refNo;
                    var correctedRecordType = '';
                    if (wtaxDetails.recordType == 'invoice') correctedRecordType = 'Invoice';
                    else if (wtaxDetails.recordType == 'cashsale') correctedRecordType = 'Cash Sale';
                    else if (wtaxDetails.recordType == 'creditmemo') correctedRecordType = 'Credit Memo';
                    else correctedRecordType = wtaxDetails.recordType;
                    sawtLineItem.recordType = correctedRecordType;
                    sawtLineItem.memo = (wtaxDetails.memo != _NONE) ? wtaxDetails.memo : '';
                    sawtLineItem.tranDate = wtaxDetails.tranDate;
                    
                    arrSawtLineItem.push(sawtLineItem);
                    sawtLineItemMap[wtaxDetails.taxCodeId + '-' + wtaxDetails.transID] = sawtLineItem;
                    sawtSequenceNumber += 1;
            } else {
            	sawtLineItemMap[wtaxDetails.taxCodeId + '-' + wtaxDetails.transID].addToTaxBase(taxBase);
                sawtLineItemMap[wtaxDetails.taxCodeId + '-' + wtaxDetails.transID].addToTaxWithheld(taxWitheld);
            }
    	} else if (wtaxDetails.searchType == 'SALES_SUMMARY') {
    		if (!(sawtLineItemMap[wtaxDetails.taxCodeId])) {
            	sawtLineItem = new SAWT_REPORT.SawtLineItem(
    	                sawtSequenceNumber,
    	                '', // waTin, 
    					'', // waRegName, 
    					'', // returnPeriodFromDate 
    					'', // returnPeriodToDate 
    					wtaxDetails.taxCodeName, // lookup atc 
    					wtaxDetails.taxCodeDesc, // lookup desc 
    					taxBase,
    					wtaxDetails.tmpTaxRate, // tax code rate
    					taxWitheld,
    					wtaxDetails.taxCodeId,
    					customerId);

                    sawtLineItem.taxTypeDesc = taxTypeDesc;

                    arrSawtLineItem.push(sawtLineItem);
                    sawtLineItemMap[wtaxDetails.taxCodeId] = sawtLineItem;
                    sawtSequenceNumber += 1;
            } else {
            	sawtLineItemMap[wtaxDetails.taxCodeId].addToTaxBase(taxBase);
                sawtLineItemMap[wtaxDetails.taxCodeId].addToTaxWithheld(taxWitheld);
            }
    	}
        
        
        arrMsg.push('[' + sawtSequenceNumber + ']customer ' + customerId);
        arrMsg.push('[' + sawtSequenceNumber + ']wtax_code ' + searchResults[i].getValue(
        		lookupSearchColumn[_labelTaxCodeName]));
        arrMsg.push('[' + sawtSequenceNumber + ']wtax_desc ' + searchResults[i].getValue(
        		lookupSearchColumn[_labelTaxCodeDesc]));
        arrMsg.push('['  + sawtSequenceNumber +  ']tax_base ' + taxBase);
        arrMsg.push('['  + sawtSequenceNumber +  ']tax_wheld ' +  taxWitheld);
        nlapiLogExecution('DEBUG', 'Tax Withheld by Customer search results', arrMsg.join('<br/>'));
        
        return sawtLineItemMap;
    };
    
    
    getCustomerData.cleanUpPayeeData = function cleanUpPayeeData(arrSawtLineItem) {
    	var newArrSawtLineItem = new Array();
    	
    	for (var i in arrSawtLineItem) {
    		var sawtLineItem = arrSawtLineItem[i];
    		if (sawtLineItem.getTaxBase() != 0 && sawtLineItem.getTaxWithheld() != 0) {
    			newArrSawtLineItem.push(sawtLineItem);
    		} 
    	}
    	
    	return newArrSawtLineItem;
    };
    
    return getCustomerData;
    
}());


//=============================================================================
//Represents a sawt line item
SAWT_REPORT.SawtLineItem = function (
  sequenceNo,  //@param {String}
  waTin,   //@param {String}
  waRegName,  //@param {String}
  returnPeriodFromDate,  //@param {String}
  returnPeriodToDate,  //@param {String}
  taxCode,  //@param {String}
  taxDesc,  //@param {String}
  wtaxBase,  //@param {String}
  wtaxRate,  //@param {String}
  amtTaxWithheld,  //@param {String}
  wTaxCodeId,  //@param {String}
  wCustomerId,
	wRefNo,
	wRecordType,
	wMemo,
	wTranDate,
	wTaxTypeDesc)  //@param {String}
{
  var isPerson = false;
  var lastName = "";
  var middleName = "";
  var firstName = "";
  var taxTypeDesc = "";
  var classification = "";
  var refNo = '';
  var recordType = '';
  var memo = '';
  var tranDate = '';
  
  this.tin = waTin;
  this.seqNo = sequenceNo;
  this.regName = waRegName;
  this.returnPeriodFromDate = returnPeriodFromDate; 
  this.returnPeriodToDate = returnPeriodToDate;
  this.atc = taxCode;
  this.desc = taxDesc;
  this.taxBase = wtaxBase;
  this.taxRate = wtaxRate;
  this.taxWithheld = amtTaxWithheld;
  this.idWTaxCode = wTaxCodeId;
  this.idCustomer = wCustomerId;
  this.refNo = wRefNo;
  this.recordType = wRecordType;
  this.memo = wMemo;
  this.tranDate = wTranDate;
  this.taxTypeDesc = taxTypeDesc;
      
  //Public Interface
  this.setTin = function(value) { this.tin = value; };
  this.getTin = function() { return this.tin; };
  this.setRegName = function(value) { this.regName = value; };
  this.getRegName = function() { return this.regName; };
  this.setReturnPeriodFromDate = function(value) { this.returnPeriodFromDate = value; };
  this.setReturnPeriodToDate = function(value) { this.returnPeriodToDate = value; };
  this.setAtc = function(value) { this.atc = value; };
  this.getAtc = function() { return this.atc; };
  this.setDesc = function(value) { this.desc = value; };
  this.getDesc = function() { return this.desc; };
  this.getTaxBase = function() { return this.taxBase; };
  this.setTaxBase = function(value) { this.taxBase = value; };
  this.addToTaxBase = function(value) { this.taxBase += parseFloat(value, 10); };
  this.getTaxWithheld = function() { return this.taxWithheld; };
  this.setTaxWithheld = function(value) { this.taxWithheld = value; };
  this.addToTaxWithheld = function(value) { this.taxWithheld += parseFloat(value, 10); };
  this.setIdWtaxCode = function(value) { this.idWTaxCode = value; };
  this.getIdWTaxCode = function() { return this.idWTaxCode; };
  this.setIdCustomer = function setIdCustomer(value) { this.idCustomer = value; };
  this.getIdCustomer = function() { return this.idCustomer; };
  this.setTaxRate = function(value) {this.taxRate = value; };
  this.getTaxRate = function() {return this.taxRate; };
  this.setIsPerson = function(value) { isPerson = (value === "T"); this.setClassification(value); };
  this.IsPerson = function() { return isPerson; };
  this.setLastName = function(value) { lastName = value; };
  this.getLastName = function() { return lastName; };
  this.setMiddleName = function(value) { middleName = value; };
  this.getMiddleName = function() { return middleName; };
  this.setFirstName = function(value) { firstName = value; };
  this.getFirstName = function() { return firstName; };
  this.setTaxTypeDesc = function(value) { taxTypeDesc = value;};
  this.getTaxTypeDesc = function() { return taxTypeDesc; };
  this.setClassification = function(value) { classification = value == "T" ? "Individual" : "Company"; };
  this.getClassificaiton = function() { return classification; };
  this.setRefNo = function(value) { refNo = value;};
  this.getRefNo = function() { return refNo; };
  this.setRecordType = function(value) { recordType = value;};
  this.getRecordType = function() { return recordType; };
  this.setMemo = function(value) { memo = value;};
  this.getMemo = function() { return memo; };
  this.setTranDate = function(value) { tranDate = value;};
  this.getTranDate = function() { return tranDate; };
  this.toCSV = _ToCSV;
  this.toSalesCSV = _ToSalesCSV;
  this.toListRow = _ToListRow;
  this.toXMLEscapedRow = _ToXMLEscapedRow;

  function _ToCSV(sequenceNumber, searchType)
  {
      var csvComps = [];

      csvComps.push(sequenceNumber);                      //Sequence No.
      csvComps.push(isPerson ? "" : _4601.escapeTextForCSV(this.regName));    //Registered Name - business entity only
      csvComps.push(isPerson ? _4601.escapeTextForCSV(lastName) : "");   //Lastname - person only
      csvComps.push(isPerson ? _4601.escapeTextForCSV(firstName) : "");  //Firstname - person only
      csvComps.push(isPerson ? _4601.escapeTextForCSV(middleName) : ""); //Middlename - person only
      csvComps.push(_4601.escapeTextForCSV(searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(this.tin) : this.tin));            //TIN - formatted xxx-xxx-xxx-xxxx
      csvComps.push(_4601.escapeTextForCSV(classification));                        //Classification
      if (searchType == 'DETAIL' || searchType == 'SAWT_DETAIL') {
    	  csvComps.push(_4601.escapeTextForCSV(this.atc));                        //ATC
          csvComps.push(this.taxTypeDesc == null ? '' : _4601.escapeTextForCSV(this.taxTypeDesc));                        //Tax Type
          csvComps.push(this.desc == null ? '' : _4601.escapeTextForCSV(this.desc));                       //Nature of Income Payment
      }
      csvComps.push(_4601.escapeTextForCSV(_4601.formatCurrency(this.taxBase)));        //Amount of income tax payment (tax base)
      if (searchType == 'DETAIL' || searchType == 'SAWT_DETAIL') csvComps.push(_4601.escapeTextForCSV(_4601.formatRate(this.taxRate)));                             //Tax rate
      csvComps.push(_4601.escapeTextForCSV(_4601.formatCurrency(this.taxWithheld)));    //Amount of tax witheld

      return csvComps.join(',');
  };
  
  function _ToSalesCSV(sequenceNumber, searchType)
  {
      var csvComps = [];

      csvComps.push(sequenceNumber);                      //Sequence No.
      csvComps.push(_4601.escapeTextForCSV(this.atc));                        //ATC
      if (searchType == 'SALES_SUMMARY')
    	  csvComps.push(this.desc == null ? '' : _4601.escapeTextForCSV(this.desc));                       //Description
      csvComps.push(this.taxTypeDesc == null ? '' : _4601.escapeTextForCSV(this.taxTypeDesc));                        //Tax Type
      if (searchType == 'SALES_DETAIL') {
    	  csvComps.push(isPerson ? "" : _4601.escapeTextForCSV(this.regName));    //Registered Name - business entity only
          csvComps.push(isPerson ? _4601.escapeTextForCSV(lastName) : "");   //Lastname - person only
          csvComps.push(isPerson ? _4601.escapeTextForCSV(firstName) : "");  //Firstname - person only
          csvComps.push(isPerson ? _4601.escapeTextForCSV(middleName) : ""); //Middlename - person only
          csvComps.push(_4601.escapeTextForCSV(this.tin));            //TIN - formatted xxx-xxx-xxx-xxxx
          csvComps.push(_4601.escapeTextForCSV(this.refNo));                        //Transaction Number / Reference No
          csvComps.push(_4601.escapeTextForCSV(this.recordType));                        //Transaction Type
          csvComps.push(_4601.escapeTextForCSV(this.memo));                       //Memo
      }
      csvComps.push(_4601.escapeTextForCSV(_4601.formatCurrency(this.taxBase)));        //Amount of income tax payment (tax base)
      csvComps.push(_4601.escapeTextForCSV(_4601.formatRate(this.taxRate)));                             //Tax rate
      csvComps.push(_4601.escapeTextForCSV(_4601.formatCurrency(this.taxWithheld)));    //Amount of tax witheld

      return csvComps.join(',');
  };

  function _ToListRow(sequenceNumber, searchType)
  {
      return {
          seq_no: sequenceNumber.toString(),
          reg_name: isPerson ? "" : this.regName,
          ind_name: isPerson ? getIndividualName(lastName, firstName, middleName) : "",
          tin: searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(this.tin) : this.tin,
          classification: classification,
          atc: this.atc,
          tax_type: this.taxTypeDesc,
          nature: this.desc,
          tax_base: this.taxBase,
          tax_rate: this.taxRate,
          tax_withheld: this.taxWithheld,
          refNo: this.refNo,
          recordType: this.recordType,
          memo: this.memo,
          tranDate: this.tranDate
      };
  }

  function getIndividualName(lastname, firstname, middlename)
  {
      var _NONE = "- None -";
      var individualname = "";

      if (lastname && lastname != _NONE)
          individualname = lastname;

      if (firstname && individualname)
          individualname = individualname + ", " + firstname;
      else if (firstname && firstname != _NONE)
          individualname = firstname;

      if ((middlename && middlename != _NONE) && individualname)
          individualname = individualname + " " + middlename;
      else if (middlename && middlename != _NONE)
          individualname = middlename;

      return individualname;
  }

  function _ToXMLEscapedRow(sequenceNumber, searchType)
  {
	  var rate = _4601.formatRate(this.taxRate);
      return {
          seqNo: sequenceNumber.toString(),
          regName: isPerson ? "" : _4601.escapeTextForPDF(this.regName),
          indName: isPerson ? _4601.escapeTextForPDF(getIndividualName(lastName, firstName, middleName)) : "",
          entityName: isPerson ? _4601.escapeTextForPDF(getIndividualName(lastName, firstName, middleName)) : _4601.escapeTextForPDF(this.regName),
          tin: searchType == 'SAWT_DETAIL' ? _4601.FormatTIN(this.tin) : this.tin,
          classification: nlapiEscapeXML(classification),
          atc: this.atc,
          taxType: nlapiEscapeXML(this.taxTypeDesc == null ? '' : this.taxTypeDesc),
          nature: nlapiEscapeXML(this.desc == null ? '' : this.desc),
          taxBase: _4601.formatCurrency(this.taxBase),
          taxRate: _4601.formatRate(this.taxRate),
          taxWithheld: _4601.formatCurrency(this.taxWithheld),
          refNo: this.refNo,
          recordType: this.recordType,
          memo: _4601.escapeTextForPDF(this.memo),
          tranDate: this.tranDate
      };
  }

  function enQuote(s, quoteChar)
  {
      if (typeof (quoteChar) === 'undefined')
          quoteChar = '"';

      return quoteChar + s + quoteChar;
  }
};


SAWT_REPORT.SawtLineItemsWithTotal = function(sawtLineItems, sumTaxBase, sumTaxWithheld)
{
    this.getSawtLineItems = function() { return sawtLineItems; };
    this.getSumTaxBase = function() { return sumTaxBase; };
    this.getSumTaxWithheld = function() { return sumTaxWithheld; };
    this.setSumTaxBase = function(v) { sumTaxBase = v; };
    this.setSumTaxWithheld = function(v) { sumTaxWithheld = v; };
    this.setReturnPeriodValueForAllLineItems = _setReturnPeriodValueForAllLineItems;

    function _setReturnPeriodValueForAllLineItems(strFormattedReturnPeriodFromDate, strFormattedReturnPeriodToDate)
    {
        for (var i in sawtLineItems)
        {
            if (sawtLineItems.hasOwnProperty(i))
            {
                sawtLineItems[i].setReturnPeriodFromDate(strFormattedReturnPeriodFromDate);
                sawtLineItems[i].setReturnPeriodToDate(strFormattedReturnPeriodToDate);
            }
        }
    }
};