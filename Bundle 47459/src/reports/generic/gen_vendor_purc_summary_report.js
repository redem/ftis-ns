/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * 
 * library dependencies:
 * 		wi_tax_reports_library.js
 * 		vendor_data_source.js
 * 
 * @author mmoya
 */
if (!MAP_REPORT) var MAP_REPORT = {};

if (!_4601) { var _4601 = {}; }

var _App = new SFC.System.Application("c54f4fb9-a81f-40e0-bf8f-ae7d00f57073");

_App._DEPLOY_ID_SETUP = "customdeploy_gen_vendor_purc_summary";
_App._DEPLOY_ID_ONLINE = "customdeploy_gen_vendor_purc_summry_list";
_App._DEPLOY_ID_PRINTING = 'customdeploy_gen_vendor_purc_summary_pdf';
_App._DEPLOY_ID_CSV = "customdeploy_gen_vendor_purc_summary_csv";
_App._SCRIPT_ID = "customscript_gen_vendor_purc_summary";

_App._ROW_TEMPLATE = 'gen_vendor_purc_summary_rows.htm';
_App._ROW_TOTAL_TEMPLATE = 'gen_vendor_purc_summary_rows_total.htm';
_App._PRINT_TEMPLATE = 'gen_vendor_purc_summary_template.xml';

var searchType = 'PURC_SUMMARY';


function main(request, response)
{
	var creditableWTaxCodeIds = new Array();

	var taxtype = request.getParameter('custpage_taxtype');
	if (taxtype)
		creditableWTaxCodeIds = _4601.WithholdingTaxCodeFetcher().getTaxCodesFilteredByType(taxtype);

	switch (_App.Context.getDeploymentId())
	{
		case _App._DEPLOY_ID_ONLINE:
			new MAP_REPORT.App( request, response ).Run(creditableWTaxCodeIds);
			break;
			
		case _App._DEPLOY_ID_SETUP:
			new MAP_REPORT.Setup( request, response ).Run();
			break;
			
		case _App._DEPLOY_ID_PRINTING:
			new MAP_REPORT.Print( request, response ).Run(creditableWTaxCodeIds);
			break;
			
		case _App._DEPLOY_ID_CSV:
			new MAP_REPORT.Csv( request, response ).Run(creditableWTaxCodeIds);
			break;	
			
		default:
			new MAP_REPORT.Setup( request, response ).Run();
	}
}


/**
 * Sort by:
 *     - Tax type
 *     - Tax code name
 *     - Transaction Type
 *     - Transaction Number (Reference No)
 *     - Date
 * @param lhs
 * @param rhs
 * @returns {Number}
 */
MAP_REPORT.SortFunction = function SortFunction(lhs, rhs)
{
	var lhsTaxType = lhs.tax_type.toUpperCase();
	var rhsTaxType = rhs.tax_type.toUpperCase();
	
	if (lhsTaxType < rhsTaxType) 
		return -1;
	else if (lhsTaxType > rhsTaxType)
		return 1;
	else {
		var lhsTaxCode = lhs.atc.toUpperCase();
		var rhsTaxCode = rhs.atc.toUpperCase();
		
		if (lhsTaxCode < rhsTaxCode)
			return -1;
		else if (lhsTaxCode > rhsTaxCode)
			return 1;
		else
			return 0;
	}
};

MAP_REPORT.Setup = function Setup( request, response ){
	
	var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
	var _request = request;
	
	var _periodId = null;
	var _subId = null;
	var _taxTypeId = null;
	var _nexusId = null;
	
	//Initialize the form by assembling search parameters;
	function formInit() {
		var resourceObject = _4601.getResourceManager();
		var currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod();
		var is_post_back = _request.getMethod() == 'POST' ? true : false;
		var is_refresh = _request.getParameter('refresh');
		var subIdsThatWTaxAppliesTo = _4601.getSubsidiaryIdsThatWTaxAppliesTo();
		var _periodId = null;
		
		if( is_post_back || is_refresh == 'y'){ //Redirect to List Page
			var arrParam = new Array();
			
			_periodId = !!_request.getParameter('custpage_periodid') ? _request.getParameter('custpage_periodid') : currentPeriod.GetId();
			arrParam['custpage_periodid'] = _periodId;
				
			if( _IsOneWorld ) {
				_subId = isNaN(parseInt(_request.getParameter('custpage_subsidiary'))) ? _Context.getSubsidiary() : _request.getParameter('custpage_subsidiary');
				arrParam['custpage_subsidiary'] = _subId;
			} else {
				_nexusId = _request.getParameter('custpage_nexus') == null ? nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country") : _request.getParameter('custpage_nexus');
				arrParam['custpage_nexus'] = _nexusId;
			}
			
			_taxTypeId = _request.getParameter('custpage_taxtype');
			arrParam['custpage_taxtype'] = _taxTypeId;
			
			if (is_refresh != 'y')
				nlapiSetRedirectURL('SUITELET', _App._SCRIPT_ID, _App._DEPLOY_ID_ONLINE, null, arrParam);

		//Initial Search Screen
        } else { 
			_periodId = _request.getParameter('custpage_periodid');

			if (!_periodId && currentPeriod != null)
				_periodId = currentPeriod.GetId();

			if (_IsOneWorld)
				_subId = isNaN(parseInt(_request.getParameter('custpage_subsidiary'))) ? null : _request.getParameter('custpage_subsidiary');
			else
				_nexusId = _request.getParameter('custpage_nexus') == null ? null : _request.getParameter('custpage_nexus'); 
		}
		
		var form = nlapiCreateForm(resourceObject.REPORT['report'].WTAX_VENDOR_PURC_SUMMARY_REPORT.title); //'Purchases by Withholding Tax Code (Summary)'
		form.setScript('customscript_map_client');
		
		var refreshUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_SETUP);
		_App.CreateField(form, "urlrefresh", "text", "", false, refreshUrl, "hidden", "normal");
		
        //Subsidiary
        if (_IsOneWorld) {
            var subsComboboxConfig = new _4601.SubsidiaryComboboxConfiguration(
            	subIdsThatWTaxAppliesTo,
                'custpage_subsidiary',
                resourceObject.REPORT['common_field'].SUBSIDIARY.label, //'Subsidiary'
                _subId,
                'outsideabove',
                'startrow');
            var objSubsidiaryCombo = new _4601.FilteredSubsidiaryCombobox(form, subsComboboxConfig);
			objSubsidiaryCombo.setMandatory(true);
		} else {
			var nexusComboBoxParam = {
					fieldName: 'custpage_nexus', 
					fieldLabel: resourceObject.REPORT['common_field'].NEXUS.label, //'Nexus' 
					idSelectedDefault: _nexusId, 
					fieldLayoutType: 'outsideabove',
					fieldBreakType: 'startrow'
			};
			
			var objNexusCombo = new _4601.NexusCombobox(form, nexusComboBoxParam);
			objNexusCombo.setMandatory(true);
		}
        
        var subsidiary = _IsOneWorld ? subIdsThatWTaxAppliesTo : _subId;
        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        
        // Tax Period
        var objTaxCombo = new SFC.System.TaxPeriodMonthQuarterCombobox(
        		form, 
        		"custpage_periodid", 
        		resourceObject.REPORT['common_field'].TAX_PERIOD.label, //"Tax Period" 
        		_periodId, 
        		'outsideabove',
        		_IsOneWorld ? (!!_subId ? _subId : null) : (!!_nexusId ? _nexusId : null),
        		fiscalCalendarId);
        objTaxCombo.setMandatory(true);
        
        //add the WH Type field
        var comboboxParam = {
    			validSubsidiaryIds: subsidiary, 
    			idSelectedDefault: null, 
    			fieldSelectType: 'multiselect',
    			fieldName: 'custpage_taxtype',
    			fieldLabel: resourceObject.REPORT['common_field'].TAX_TYPE.label, //'Tax Type'
    			fieldLayoutType: 'normal',
    			fieldBreakType: 'none',
    			subId: _IsOneWorld ? _subId : '',
    			nexusId: _IsOneWorld ? '' : _nexusId
    	};
    	var objTaxTypeCombo = new _4601.WHTaxTypeCombobox(form, comboboxParam);
        objTaxTypeCombo.setMandatory(true);
        
		form.addSubmitButton(resourceObject.REPORT['button'].SUBMIT.label); //'Submit'
		return form;
	}


	this.Run = function() {
		var form = formInit();
		response.writePage(form);
	};
};


MAP_REPORT.Csv = function Csv(request, response)
{
    var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
    var _request = request;
    var _periodId = null;
    var _subId = null;
    var _nexusId = null;

    //Initialize the form by assembling search parameters;
    function formInit()
    {
        _periodId = _request.getParameter('custpage_periodid');

        if (_IsOneWorld) {
            _subId = isNaN(parseInt(_request.getParameter('custpage_subsidiary'))) ? _Context.getSubsidiary() : _request.getParameter('custpage_subsidiary');
        } else {
            _nexusId = _request.getParameter('custpage_nexus');
        }
    }


    function convertToDelimitedString(objPayee, sequenceNumber)
    {
        var arrPayee = [];

        arrPayee.push(sequenceNumber);                                                      //Sequence No.
        arrPayee.push(objPayee.atc);                                                        //ATC
        arrPayee.push(_4601.escapeTextForCSV(objPayee.desc));                                           //Description
        arrPayee.push(_4601.escapeTextForCSV(objPayee.tax_type));                                       //Tax Type Description
        arrPayee.push(_4601.escapeTextForCSV(_4601.formatCurrency(objPayee.tax_base)));                                                   //Amount of income tax payment (tax base)
        arrPayee.push(_4601.escapeTextForCSV(_4601.formatRate(objPayee.tax_rate)));                                       //Tax rate
        arrPayee.push(_4601.escapeTextForCSV(_4601.formatCurrency(objPayee.tax_amount)));                                                 //Amount of tax witheld

        return arrPayee.join(",");
    }

    function _WriteHeader(response, subId, taxPeriodId, resourceObject)
    {
        var headerInfo = _4601.GetHeaderInfo(subId, taxPeriodId, searchType);

        response.writeLine(resourceObject.REPORT['report'].WTAX_VENDOR_PURC_SUMMARY_REPORT.title.toUpperCase()); //"Purchases by Withholding Tax Code (Summary)"
        response.writeLine("\"" + resourceObject.REPORT['common_field'].PERIOD_FROM.label.toUpperCase() + " " + headerInfo.StartDate + " " + resourceObject.REPORT['common_field'].PERIOD_TO.label + " " + headerInfo.EndDate + "\""); //FOR THE PERIOD: " TO "
        response.writeLine("");
        response.writeLine(resourceObject.REPORT['common_field'].TIN.label + ": " + headerInfo.PayeeTIN); //TIN
        response.writeLine(_4601.escapeTextForCSV(resourceObject.REPORT['common_field'].AGENT_NAME.label.toUpperCase() + ': '  + headerInfo.PayeeName)); //PAYEE'S NAME
        response.writeLine("");
        //"SEQ NO,CORPORATION (Registered Name),LAST NAME,FIRST NAME,MIDDLE NAME,TAX REGISTRATION NUMBER,CLASSIFICATION,TAX CODE,TAX TYPE,DESCRIPTION,AMOUNT OF INCOME PAYMENT,WITHHOLDING TAX RATE,AMOUNT OF TAX WITHHELD"
        response.writeLine(resourceObject.REPORT['common_field'].SEQ_NO.label.toUpperCase() + ',' +
        		resourceObject.REPORT['common_field'].TAX_CODE.label.toUpperCase() + ',' +
        		resourceObject.REPORT['common_field'].DESCRIPTION.label.toUpperCase() + ',' +
        		resourceObject.REPORT['common_field'].TAX_TYPE.label.toUpperCase() + ',' +
        		resourceObject.REPORT['common_field'].AMT_INCOME_PMNT.label.toUpperCase() + ',' + 
        		resourceObject.REPORT['common_field'].WTAX_RATE.label.toUpperCase() + ',' + 
        		resourceObject.REPORT['common_field'].AMT_TAX_WHELD.label.toUpperCase());
        response.writeLine("'(" + resourceObject.REPORT['common_field'].NUM_ONE.label + 
        		"),'(" + resourceObject.REPORT['common_field'].NUM_TWO.label + 
        		"),'(" + resourceObject.REPORT['common_field'].NUM_THREE.label +
        		"),'(" + resourceObject.REPORT['common_field'].NUM_FOUR.label +
        		"),'(" + resourceObject.REPORT['common_field'].NUM_FIVE.label +
        		"),'(" + resourceObject.REPORT['common_field'].NUM_SIX.label +
        		"),'(" + resourceObject.REPORT['common_field'].NUM_SEVEN.label +
        		")");
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
    }

    function _WriteFooter(response, grandTotal, resourceObject)
    {
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
        response.writeLine(resourceObject.REPORT['common_field'].GRAND_TOTAL.label.toUpperCase() + ",,,," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.Income)) + ",," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.TaxWithheld))); //GRAND TOTAL
		response.writeLine("");
        response.writeLine(resourceObject.REPORT['common_field'].END_OF_REPORT.label.toUpperCase()); //"END OF REPORT"
    }

    this.Run = function(creditableWTaxCodeIds)
    {
        formInit();

        response.setContentType("CSV", "vendor_purc_summary.csv", "attachment");
        
        var resourceObject = _4601.getResourceManager();

        _WriteHeader(response, _subId, _periodId, resourceObject);

        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        var reportData = MAP_REPORT.getVendorData(creditableWTaxCodeIds, { periodId: _periodId, subid: _subId, nexusid: _nexusId, fiscalCalendarId: fiscalCalendarId }, searchType);
        var reportLineItems = reportData.getMapLineItems();
        var grandTotal = reportData.getGrandTotal();
        
        for (var i = 0; i < reportLineItems.length; ++i)
            response.writeLine(convertToDelimitedString(reportLineItems[i], i + 1));

        _WriteFooter(response, grandTotal, resourceObject);
    };
};

MAP_REPORT.App = function App(request, response)
{
    var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
    var _request = request;

    var _periodId = null;
    var _subId = null;
    var _nexusId = null;
    var _taxTypeId = null;

    //Initialize the form by assembling search parameters;
    function formInit()
    {
    	var currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod();
        
        if (_request.getMethod() == 'POST') {
            if (currentPeriod != null)
                _periodId = currentPeriod.GetId();

            if (_IsOneWorld)
            	_subId = _Context.getSubsidiary();
            else
            	_nexusId = nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country");
        
        //Redirected from Setup
        } else {
            
            _periodId = _request.getParameter('custpage_periodid');
            _taxTypeId = _request.getParameter('custpage_taxtype');

            if (_IsOneWorld) 
                _subId = isNaN(_request.getParameter('custpage_subsidiary')) ? _Context.getSubsidiary() : _request.getParameter('custpage_subsidiary');
            else
            	_nexusId = _request.getParameter('custpage_nexus') == null ? nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country") : _request.getParameter('custpage_nexus');
        }
    }

    this.Run = function(creditableWTaxCodeIds)
    {
        formInit();
        
        var resourceObject = _4601.getResourceManager();

        var csvUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_CSV);
        var printUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_PRINTING);
        var backUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_SETUP);
        var queryStr = "&custpage_periodid=" + _periodId + (_IsOneWorld ? "&custpage_subsidiary=" + _subId : "&custpage_nexus=" + _nexusId) + "&custpage_taxtype=" + _taxTypeId
        
        var headerInfo = _4601.GetHeaderInfo(_subId, _periodId, searchType);

        var formTitle = [resourceObject.REPORT['report'].WTAX_VENDOR_PURC_SUMMARY_REPORT.title,
                        	resourceObject.REPORT['report'].FROM.title, headerInfo.StartDate,
                        	resourceObject.REPORT['report'].TO.title, headerInfo.EndDate].join(' ');
      
        
        var alphaList = nlapiCreateList(formTitle, false);

        alphaList.addColumn('seq_no', 'text', resourceObject.REPORT['common_field'].SEQ_NO.label.toUpperCase(), 'RIGHT'); //'SEQ NO'
        alphaList.addColumn('atc', 'text', resourceObject.REPORT['common_field'].TAX_CODE.label.toUpperCase(), 'LEFT'); //'TAX CODE'
        alphaList.addColumn('desc', 'text', resourceObject.REPORT['common_field'].DESCRIPTION.label.toUpperCase(), 'LEFT'); //'DESCRIPTION'
        alphaList.addColumn('tax_type', 'text', resourceObject.REPORT['common_field'].TAX_TYPE.label.toUpperCase(), 'LEFT'); //'TAX TYPE'
        
        var amtcoltype = 'currency';
        if((headerInfo.CurrencyLocale == "id_ID") || (headerInfo.CurrencyLocale == "ja_JP")) {
        	amtcoltype = 'text';
        }
        
        alphaList.addColumn('tax_base', amtcoltype, resourceObject.REPORT['common_field'].AMT_INCOME_PMNT.label.toUpperCase(), 'RIGHT'); //'AMOUNT OF INCOME PAYMENT'
        alphaList.addColumn('tax_rate', amtcoltype, resourceObject.REPORT['common_field'].WTAX_RATE.label.toUpperCase(), 'RIGHT'); //'WITHHOLDING TAX RATE'
        alphaList.addColumn('tax_amount', amtcoltype, resourceObject.REPORT['common_field'].AMT_TAX_WHELD.label.toUpperCase(), 'RIGHT'); //'AMOUNT OF TAX WITHHELD'

        alphaList.addButton('printBtn', resourceObject.REPORT['button'].PRINT_PDF.label, "window.open('" + printUrl + queryStr + "')"); //'Print PDF'
        alphaList.addButton('csvBtn', resourceObject.REPORT['button'].SAVE_CSV.label, "window.open('" + csvUrl + queryStr + "')"); //'Save CSV'
        alphaList.addButton('filterBtn', resourceObject.REPORT['button'].RETURN_TO_CRITERIA.label, "document.location='" + backUrl + queryStr + "'"); //'Start Over'
        
        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        var data = MAP_REPORT.getVendorData(creditableWTaxCodeIds, { periodId: _periodId, subid: _subId, nexusid: _nexusId, fiscalCalendarId: fiscalCalendarId }, searchType);
        var reportLineItems = data.getMapLineItems();
        
        for (var i = 0; i < reportLineItems.length; ++i)
        {
        	reportLineItems[i].seq_no = (i + 1).toString();
        	
        	if((headerInfo.CurrencyLocale == "id_ID") || (headerInfo.CurrencyLocale == "ja_JP")) {
        		reportLineItems[i].tax_base = reportLineItems[i].tax_base.toString();
        		reportLineItems[i].tax_amount = reportLineItems[i].tax_amount.toString();
        	}
            alphaList.addRow(reportLineItems[i]);
        }

        response.writePage(alphaList);
    };
};


MAP_REPORT.Print = function Print(request, response)
{
    var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
    var _request = request;
    var _periodId = null;
    var _subId = null;
    var _nexusId = null;

    //Initialize the form by assembling search parameters;
    function formInit()
    {
        _periodId = _request.getParameter('custpage_periodid');

        if (_IsOneWorld) {
            _subId = isNaN(_request.getParameter('custpage_subsidiary')) ? _Context.getSubsidiary() : _request.getParameter('custpage_subsidiary');
        } else {
            _nexusId = _request.getParameter('custpage_nexus');
        }
    }

    function createContent(startDate, endDate, payeeTIN, payeeName, body)
    {
    	var resourceObject = _4601.getResourceManager();
    	var helperUtils = new _4601.HelperUtils(_App, _request);    	
    	var arabicfont = nlapiEscapeXML(helperUtils.getFullFileUrl("koodak.ttf"));
    	
        var mainTemplate = _App.GetFileContent(_App._PRINT_TEMPLATE, true);
        var elements = {
        	arabicfont: arabicfont,
        	ReportTitle: resourceObject.REPORT['report'].WTAX_VENDOR_PURC_SUMMARY_PDF.title.toUpperCase(),
        	Label_PeriodFrom: resourceObject.REPORT['common_field'].PERIOD_FROM.label.toUpperCase(),
        	Label_PeriodTo: resourceObject.REPORT['common_field'].PERIOD_TO.label.toUpperCase(),
            PeriodStartDate: startDate.toString("MMMM d, yyyy"),
            PeriodEndDate: endDate.toString("MMMM d, yyyy"),
            Label_TIN: resourceObject.REPORT['common_field'].TIN.label.toUpperCase(),
            PayeeTIN: _4601.escapeTextForPDF(payeeTIN),
            Label_AgentName: resourceObject.REPORT['common_field'].AGENT_NAME.label.toUpperCase(),
            PayeeName: _4601.escapeTextForPDF(payeeName),
            Label_SeqNo: resourceObject.REPORT['common_field'].SEQ_NO.label.toUpperCase(),
            Label_TaxCode: resourceObject.REPORT['common_field'].TAX_CODE.label.toUpperCase(),
            Label_Description: resourceObject.REPORT['common_field'].DESCRIPTION.label.toUpperCase(),
            Label_TaxType: resourceObject.REPORT['common_field'].TAX_TYPE.label.toUpperCase(),
            Label_AmtIncomePmnt: resourceObject.REPORT['common_field'].AMT_INCOME_PMNT.label.toUpperCase(),
            Label_TaxRate: resourceObject.REPORT['common_field'].TAX_RATE.label.toUpperCase(),
            Label_AmtTaxWheld: resourceObject.REPORT['common_field'].AMT_TAX_WHELD.label.toUpperCase(),
            recordtable: body
        };

        return _App.RenderTemplate(mainTemplate, elements);
    }

    function createBody(reportData)
    {
    	var reportLineItems = reportData.getMapLineItems();
        var grandTotal = reportData.getGrandTotal();
        
        formatAmounts(reportLineItems);
        escapeText(reportLineItems);
        adjustSeqNo(reportLineItems);

        var rowTemplate = _App.GetFileContent(_App._ROW_TEMPLATE, true);
        var totalTemplate = _App.GetFileContent(_App._ROW_TOTAL_TEMPLATE, true);

        var objTotal = {totalTaxAmount : 0.0, totalBaseAmount: 0.0};
        objTotal.totalTaxAmount = _4601.formatCurrency(grandTotal.TaxWithheld, 'PDF');
        objTotal.totalBaseAmount = _4601.formatCurrency(grandTotal.Income, 'PDF');

        var htmlRows = _App.RenderTemplateToArray(rowTemplate, reportLineItems);
        var htmlTotal = _App.RenderTemplate(totalTemplate, objTotal);

        return htmlRows + htmlTotal;
    }
    
    function adjustSeqNo(objArray) {
    	for (var i = 0; i < objArray.length; i++) {
    		objArray[i].seq_no = i + 1;
    	}
    }

    function formatAmounts(arrPayeeInfo)
    {
        if (arrPayeeInfo == null)
            return;

        for (var i = 0; i < arrPayeeInfo.length; ++i) {
            arrPayeeInfo[i].tax_base = _4601.formatCurrency(arrPayeeInfo[i].tax_base, 'PDF');
            arrPayeeInfo[i].tax_amount = _4601.formatCurrency(arrPayeeInfo[i].tax_amount, 'PDF');
            arrPayeeInfo[i].tax_rate = _4601.formatRate(arrPayeeInfo[i].tax_rate, 'PDF');
        }
    }

    function escapeText(arrPayeeInfo)
    {
        for (var i in arrPayeeInfo) {
            arrPayeeInfo[i].reg_name = _4601.escapeTextForPDF(arrPayeeInfo[i].reg_name);
			arrPayeeInfo[i].entity_name = _4601.escapeTextForPDF(arrPayeeInfo[i].entity_name);
            arrPayeeInfo[i].atc = _4601.escapeTextForPDF(arrPayeeInfo[i].atc);
            arrPayeeInfo[i].desc = _4601.escapeTextForPDF(arrPayeeInfo[i].desc);
            arrPayeeInfo[i].tin = _4601.escapeTextForPDF(arrPayeeInfo[i].tin);
        }
    }

    this.Run = function(creditableWTaxCodeIds) {
        formInit();

        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        var reportData = MAP_REPORT.getVendorData(creditableWTaxCodeIds, { periodId: _periodId, subid: _subId, nexusid: _nexusId, fiscalCalendarId: fiscalCalendarId }, searchType);

        var body = createBody(reportData);
        var headerInfo = _4601.GetHeaderInfo(_subId, _periodId, searchType);
        var content = createContent(headerInfo.StartDate, headerInfo.EndDate, headerInfo.PayeeTIN, headerInfo.PayeeName, body);
        
        var pdfFile = null;
        
        try {
        	pdfFile = nlapiXMLToPDF('<?xml version="1.0" encoding="ISO-8859-1"?>\n<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);
        } catch (err) {
        	nlapiLogExecution('DEBUG', 'Error Creating PDF File',  err);
        	
        	content = nlapiEscapeXML(content);
        	
        	pdfFile = nlapiXMLToPDF('<?xml version="1.0" encoding="ISO-8859-1"?>\n<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);
        }
        
        response.setContentType("PDF", 'vendor_purc_summary.pdf', 'attachment');
        response.write(pdfFile.getValue());
    };
};

