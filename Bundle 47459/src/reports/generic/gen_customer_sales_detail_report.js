/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * 
 * Suitelet to handle the display for SAWT (Summary Alphalist of Witholding
 * Agents/Payors of Income Payments subjected to Creditable Witholding Tax at
 * Source.
 * 
 * library dependencies:
 *  wi_tax_reports_library.js
 *  vendor_data_source.js
 * 
 * References: FRD 4121, FRD 4601
 * https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=138&id=4121 
 * http://twiki.corp.netsuite.com/twiki/bin/view/PM/FeatureDocument4121
 * @author ruy, mmoya
 */

if (!SAWT_REPORT) var SAWT_REPORT = {};

var _App = new SFC.System.Application("c54f4fb9-a81f-40e0-bf8f-ae7d00f57073");

_App._ROW_TEMPLATE = 'gen_customer_sales_detail_rows_print.htm';
_App._ROW_PRINT_TEMPLATE = 'gen_customer_sales_detail_template.xml';

_App._ID_DEPLOYMENT_UI = 'customdeploy_gen_cust_sales_detail';
_App._ID_DEPLOYMENT_LIST_UI = 'customdeploy_gen_cust_sales_detail_list';
_App._ID_DEPLOYMENT_CSV = 'customdeploy_gen_cust_sales_detail_csv';
_App._ID_DEPLOYMENT_PDF = 'customdeploy_gen_cust_sales_detail_pdf';
_App._SCRIPT_ID = "customscript_gen_cust_sales_detail";

var searchType = 'SALES_DETAIL';

function dispatchPerDeploymentType(request, response)
{
    var creditableWTaxCodeIds = new Array();
    
    var taxtype = request.getParameter('custpage_taxtype');
    
    if (taxtype)
    	creditableWTaxCodeIds = _4601.WithholdingTaxCodeFetcher().getTaxCodesFilteredByType(taxtype);
    
	switch(nlapiGetContext().getDeploymentId()) {
        case _App._ID_DEPLOYMENT_LIST_UI:
            new SAWT_REPORT.ListViewSuitelet(request, response).run(creditableWTaxCodeIds);
            break;
            
		case _App._ID_DEPLOYMENT_UI:
            new SAWT_REPORT.UIBuilderSuitelet(request, response).run();
            break;
            
		case _App._ID_DEPLOYMENT_CSV:
            new SAWT_REPORT.CSVOutputSuitelet(request, response).run(creditableWTaxCodeIds);
            break;
            
		case _App._ID_DEPLOYMENT_PDF:
            new SAWT_REPORT.PrintPDFSuitelet(request, response).run(creditableWTaxCodeIds);
            break;
		    
        default:
            nlapiLogExecution('ERROR', 'invalid deployment context:',nlapiGetContext().getDeploymentId());
		    break;
	}
}

function _isOneWorldInstance() 
{
    return nlapiGetContext().getSetting("FEATURE", "SUBSIDIARIES") === "T";
}

function formatDateMMDDYYYY(d, sep)
{
    function zeroPadIfLessThan10(n) {
        return (n < 10) ? ('0' + n) : ('' + n);
    }
    
    var result = [];
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    
    result.push(zeroPadIfLessThan10(month));
    result.push(zeroPadIfLessThan10(day));
    result.push(zeroPadIfLessThan10(year));
    return result.join(sep);
}

/**
 * Sort by:
 *     - Tax type
 *     - Tax code name
 *     - Transaction Type
 *     - Transaction Number (Reference No)
 *     - Date
 * @param lhs
 * @param rhs
 * @returns {Number}
 */
SAWT_REPORT.SortFunction = function(lhs, rhs)
{
	var lhsTaxType = !lhs.taxTypeDesc ? '' : lhs.taxTypeDesc.toUpperCase();
	var rhsTaxType = !rhs.taxTypeDesc ? '' : rhs.taxTypeDesc.toUpperCase();
	
	if (lhsTaxType < rhsTaxType) 
		return -1;
	else if (lhsTaxType > rhsTaxType)
		return 1;
	else {
		var lhsTaxCode = lhs.atc.toUpperCase();
		var rhsTaxCode = rhs.atc.toUpperCase();
		
		if (lhsTaxCode < rhsTaxCode)
			return -1;
		else if (lhsTaxCode > rhsTaxCode)
			return 1;
		else {
			var lhsRecordType = lhs.recordType.toUpperCase();
			var rhsRecordType = rhs.recordType.toUpperCase();
			
			if (lhsRecordType < rhsRecordType)
				return -1;
			else if (lhsRecordType > rhsRecordType)
				return 1;
			else {
				var lhsRefNo = lhs.refNo.toUpperCase();
				var rhsRefNo = rhs.refNo.toUpperCase();
				
				if (lhsRefNo < rhsRefNo)
					return -1;
				else if (lhsRefNo > rhsRefNo)
					return 1;
				else {
					var lhsTranDate = lhs.tranDate.toUpperCase();
					var rhsTranDate = rhs.tranDate.toUpperCase();
					
					if (lhsTranDate < rhsTranDate)
						return -1;
					else if (lhsTranDate > rhsTranDate)
						return 1;
					else {
						return 0;
					}
				}
			}
		}
	}
};

SAWT_REPORT.UIBuilderSuitelet = function(request, response)
{
	var _subId = '';
	var _nexusId = '';
	var _IsOneWorld = _isOneWorldInstance();

    function initializeForm(taxPeriodId)
    {
    	var resourceObject = _4601.getResourceManager();
    	var subIdsThatWTaxAppliesTo = _4601.getSubsidiaryIdsThatWTaxAppliesTo();
    	
        var form;
        var idSubsidiary = '';

        if (_IsOneWorld)
        	_subId = _IsOneWorld ? isNaN(parseInt(request.getParameter('custpage_subsidiary'))) ? null : request.getParameter('custpage_subsidiary') : null;
        else
        	_nexusId = request.getParameter('custpage_nexus') == null ? null : request.getParameter('custpage_nexus');
        
        form = nlapiCreateForm(resourceObject.REPORT['report'].WTAX_CUSTOMER_SALES_DETAIL_REPORT.title); //'Tax Withheld by Customer (Detail)'
        form.setScript('customscript_sawt_client');

        // Subsidiary
        if (_IsOneWorld) {
            idSubsidiary = getIdSubsidiaryForOneWorld();
            var subsComboboxConfig = new _4601.SubsidiaryComboboxConfiguration(
            	subIdsThatWTaxAppliesTo,
                'custpage_subsidiary',
                resourceObject.REPORT['common_field'].SUBSIDIARY.label, //'Subsidiary'
                idSubsidiary,
                'outsideabove',
                'startrow');
            var objSubsidiaryCombo = new _4601.FilteredSubsidiaryCombobox(form, subsComboboxConfig);
            objSubsidiaryCombo.setMandatory(true);
        } else {
			var nexusComboBoxParam = {
					fieldName: 'custpage_nexus', 
					fieldLabel: resourceObject.REPORT['common_field'].NEXUS.label, //'Nexus' 
					idSelectedDefault: _nexusId, 
					fieldLayoutType: 'outsideabove',
					fieldBreakType: 'startrow'
			};
			
			var objNexusCombo = new _4601.NexusCombobox(form, nexusComboBoxParam);
			objNexusCombo.setMandatory(true);
        }
        
    	var subsidiary = _IsOneWorld ? subIdsThatWTaxAppliesTo : _subId;
    	var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
    	
        // Tax Period
        var taxPeriodComboBox = new SFC.System.TaxPeriodMonthQuarterCombobox(
    			form,
    			'tax_period_id',
    			resourceObject.REPORT['common_field'].TAX_PERIOD.label, //'Tax Period'
    			taxPeriodId,
    			'outsideabove',
    			_IsOneWorld ? (!!_subId ? _subId : null) : (!!_nexusId ? _nexusId : null),
				fiscalCalendarId);
        taxPeriodComboBox.setMandatory(true);
        
        //add the WH Type field
    	var comboboxParam = {
    			validSubsidiaryIds: subsidiary, 
    			idSelectedDefault: null, 
    			fieldSelectType: 'multiselect',
    			fieldName: 'custpage_taxtype',
    			fieldLabel: resourceObject.REPORT['common_field'].TAX_TYPE.label, //'Tax Type'
    			fieldLayoutType: 'normal',
    			fieldBreakType: 'startrow',
    			subId: _IsOneWorld ? idSubsidiary : '',
    	    	nexusId: _IsOneWorld ? '' : _nexusId
    	};
    	var objTaxTypeCombo = new _4601.WHTaxTypeCombobox(form, comboboxParam);
    	objTaxTypeCombo.setMandatory(true);
    	
    	var refreshUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._ID_DEPLOYMENT_UI);
		_App.CreateField(form, "urlrefresh", "text", "", false, refreshUrl, "hidden", "normal");
        
        form.addSubmitButton(resourceObject.REPORT['button'].SUBMIT.label); //'Submit'

        return form;
    }

    function getIdSubsidiaryForOneWorld()
    {
        var idSubsidiary;
        if (isNaN(parseInt(request.getParameter('custpage_subsidiary'), 10)))
        	idSubsidiary = null;
        else
            idSubsidiary = request.getParameter('custpage_subsidiary');
        return idSubsidiary;
    }

    this.run = function() {
        var taxPeriodFromId = new SFC.System.TaxPeriod().GetCurrentPeriod();
        var subsidiaryId = null; // for OneWorld only
        var taxTypeId = null;
        var is_refresh = request.getParameter('refresh');
        var nexusId = null;
        var taxPeriodId = null;

        var form;
        //from submit of parameters
        if (request.getMethod() === 'POST' || is_refresh == 'y') {
            taxPeriodId = !!request.getParameter('tax_period_id') ? request.getParameter('tax_period_id') : taxPeriodFromId.GetId();
            
            taxTypeId = request.getParameter('custpage_taxtype');
			
            if (_IsOneWorld)
                subsidiaryId = request.getParameter('custpage_subsidiary');
            else
            	nexusId = request.getParameter('custpage_nexus') == null ? nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country") : request.getParameter('custpage_nexus');
            
            if (is_refresh != 'y') {
                // if not initial load of form
                if (taxPeriodId != null && taxTypeId != null) {
                    var paramMap = {};
                    paramMap['tax_period_id'] = taxPeriodId;
                    if (_IsOneWorld)
                    	paramMap['custpage_subsidiary'] = subsidiaryId;
                    else
                    	paramMap['custpage_nexus'] = nexusId;
                    paramMap['custpage_taxtype'] = taxTypeId;
                    
                    nlapiSetRedirectURL('SUITELET', nlapiGetContext().getScriptId(), _App._ID_DEPLOYMENT_LIST_UI, false, paramMap);
                }
            }
        //initial load of form
        } else {
        	taxPeriodId = request.getParameter('tax_period_id');
            taxTypeId = request.getParameter('custpage_taxtype');
            
            if (!taxPeriodId && taxPeriodFromId != null)
            	taxPeriodId = taxPeriodFromId.GetId();

            if (_IsOneWorld)
                subsidiaryId = getIdSubsidiaryForOneWorld();
        }
        
        form = initializeForm(taxPeriodId);

        response.writePage(form);
    };
};

SAWT_REPORT.CSVOutputSuitelet = function(request, response)
{
    this.run = function(allowedWithholdingTaxCodeIds)
    {
    	var resourceObject = _4601.getResourceManager();
        var taxPeriodId = request.getParameter("tax_period_id");
        var subsidiaryId = request.getParameter('custpage_subsidiary');
        var nexusId = request.getParameter('custpage_nexus');
        var fiscalCalendarId = _4601.getFiscalCalendar(subsidiaryId);
        
        var sawtLineItemsWithTotal = SAWT_REPORT.getCustomerData(allowedWithholdingTaxCodeIds, {
	            periodfrom: taxPeriodId,
	            periodto: taxPeriodId,
	            subid: subsidiaryId, 
	            nexusid: nexusId,
	            fiscalCalendarId: fiscalCalendarId },
            searchType);
			
		response.setContentType('CSV', 'customer_sales_details.csv', 'attachment');

        var headerValues = _4601.HeaderData(subsidiaryId, taxPeriodId, searchType);

        sawtLineItemsWithTotal.setReturnPeriodValueForAllLineItems(headerValues.returnPeriodFromDate, headerValues.returnPeriodToDate);
		 _WriteHeader(response, headerValues, resourceObject);
		 
		var grandTotal = { Income: sawtLineItemsWithTotal.getSumTaxBase(), TaxWithheld: sawtLineItemsWithTotal.getSumTaxWithheld()};
		 
        var sawtLineItems = sawtLineItemsWithTotal.getSawtLineItems();
		sawtLineItems.sort(SAWT_REPORT.SortFunction);
		
        for (var i = 0; i < sawtLineItems.length; ++i)
            response.writeLine(sawtLineItems[i].toSalesCSV(i + 1, searchType));

        _WriteFooter(response, grandTotal, resourceObject);
    };

    function _WriteHeader(response, headerValues, resourceObject)
    {
        response.writeLine(resourceObject.REPORT['report'].WTAX_CUSTOMER_SALES_DETAIL_REPORT.title.toUpperCase()); //"Sales by Withholding Tax Code (Detail)"
        response.writeLine("\"" + resourceObject.REPORT['common_field'].PERIOD_FROM.label.toUpperCase() + ": " + headerValues.returnPeriodFromDate + " " +
        		resourceObject.REPORT['common_field'].PERIOD_TO.label.toUpperCase() + " " + headerValues.returnPeriodToDate + "\""); //FOR THE PERIOD
        response.writeLine("");
        response.writeLine(resourceObject.REPORT['common_field'].TIN.label.toUpperCase() + ": " + headerValues.payeeTin); //TIN
        response.writeLine(_4601.escapeTextForCSV(resourceObject.REPORT['common_field'].AGENT_NAME.label.toUpperCase() + ': ' + headerValues.payeeRegisteredName)); //PAYEE'S NAME
        response.writeLine("");
        //"SEQ NO,CORPORATION (Registered Name),LAST NAME,FIRST NAME,MIDDLE NAME,TAX REGISTRATION NUMBER,CLASSIFICATION,TAX CODE,TAX TYPE,DESCRIPTION,AMOUNT OF INCOME PAYMENT,WITHHOLDING TAX RATE,AMOUNT OF TAX WITHHELD"
        response.writeLine(resourceObject.REPORT['common_field'].SEQ_NO.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].TAX_CODE.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].TAX_TYPE.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].CORPORATION.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].LAST_NAME.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].FIRST_NAME.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].MIDDLE_NAME.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].TAX_REG_NO.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].REFERENCE_NO.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].TRANSACTION_TYPE.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].MEMO.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].AMT_INCOME_PMNT.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].WTAX_RATE.label.toUpperCase() + "," +
        		resourceObject.REPORT['common_field'].AMT_TAX_WHELD.label.toUpperCase());
        response.writeLine("'(" + resourceObject.REPORT['common_field'].NUM_ONE.label +"),'(" +
        		resourceObject.REPORT['common_field'].NUM_TWO.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_THREE.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_FOUR.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_FIVE.label + "),,,'(" +
        		resourceObject.REPORT['common_field'].NUM_SIX.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_SEVEN.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_EIGHT.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_NINE.label + "),'(" + 
        		resourceObject.REPORT['common_field'].NUM_TEN.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_ELEVEN.label + "),'(" +
        		resourceObject.REPORT['common_field'].NUM_TWELVE.label + ")");
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,,,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
    }

    function _WriteFooter(response, grandTotal, resourceObject)
    {
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,,,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
        response.writeLine(resourceObject.REPORT['common_field'].GRAND_TOTAL.label + ",,,,,,,,,,," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.Income)) + ",," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.TaxWithheld))); //GRAND TOTAL
		response.writeLine("");
        response.writeLine(resourceObject.REPORT['common_field'].END_OF_REPORT.label); //"END OF REPORT"
    }
};


//=============================================================================
SAWT_REPORT.ListViewSuitelet = function(request, response)
{
    this.run = function(allowedWithholdingTaxCodeIds)
    {
    	var resourceObject = _4601.getResourceManager();
    	var _IsOneWorld = _isOneWorldInstance();
    	
        var taxPeriodId = request.getParameter('tax_period_id');
        var taxTypeId = request.getParameter('custpage_taxtype');
        
        var subId = '';
        var nexusId = null;
        if (_IsOneWorld) {
            subId = request.getParameter('custpage_subsidiary');
            if (isNaN(parseInt(subId, 10))) {
                nlapiLogExecution('ERROR', 'InvalidArgument', 'subId must be a number : ' + subId);
                throw { name: 'InvalidArgument', message: 'subId must be a number : ' + subId };
            }
        } else {
        	nexusId = request.getParameter('custpage_nexus') == null ? nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country") : request.getParameter('custpage_nexus');
        }
        
        if (isNaN(parseInt(taxPeriodId, 10))) {
            nlapiLogExecution('ERROR', 'InvalidArgument', 'taxPeriodId must be a number : ' + taxPeriodId);
            throw {name: 'InvalidArgument', message: 'taxPeriodId must be a number : ' + taxPeriodId };
        }

        var headerValues = _4601.HeaderData(subId, taxPeriodId, searchType);
        var list = nlapiCreateList(resourceObject.REPORT['report'].WTAX_CUSTOMER_SALES_DETAIL_REPORT.title + ' ' + 
        		resourceObject.REPORT['report'].FROM.title + ' ' + headerValues.returnPeriodFromDate + ' ' +
        		resourceObject.REPORT['report'].TO.title + ' ' + headerValues.returnPeriodToDate);

        var scriptId = nlapiGetContext().getScriptId();
        list.addColumn('seq_no', 'text', resourceObject.REPORT['common_field'].SEQ_NO.label.toUpperCase(), 'right'); //"SEQ NO"
        list.addColumn('atc', 'text', resourceObject.REPORT['common_field'].TAX_CODE.label.toUpperCase(), 'LEFT'); //"TAX CODE"
        list.addColumn('tax_type', 'text', resourceObject.REPORT['common_field'].TAX_TYPE.label.toUpperCase(), 'LEFT'); //"TAX TYPE"
        list.addColumn('reg_name', 'text', resourceObject.REPORT['common_field'].CORPORATION.label.toUpperCase(), 'LEFT'); //"CORPORATION (Registered Name)"
        list.addColumn('ind_name', 'text', resourceObject.REPORT['common_field'].INDIVIDUAL.long_label.toUpperCase(), 'LEFT'); //"INDIVIDUAL (Last Name, First Name, Middle Name)"
        list.addColumn('tin', 'text', resourceObject.REPORT['common_field'].TAX_REG_NO.label.toUpperCase(), 'LEFT'); //"TAX REGISTRATION NUMBER"
        list.addColumn('refno', 'text', resourceObject.REPORT['common_field'].REFERENCE_NO.label.toUpperCase(), 'LEFT'); //'TRANSACTION NUMBER'
        list.addColumn('recordtype', 'text', resourceObject.REPORT['common_field'].TRANSACTION_TYPE.label.toUpperCase(), 'LEFT'); //'TRANSACTION TYPE'
        list.addColumn('memo', 'text', resourceObject.REPORT['common_field'].MEMO.label.toUpperCase(), 'LEFT'); //'MEMO'
        
        var amtcoltype = 'currency';
        if((headerValues.payeeCurrencyLocale == "id_ID") || (headerValues.payeeCurrencyLocale == "ja_JP")){
        	amtcoltype = 'text';
        }
        
        list.addColumn('tax_base', amtcoltype, resourceObject.REPORT['common_field'].AMT_INCOME_PMNT.label.toUpperCase(), 'right'); //"AMOUNT OF INCOME PAYMENT"
        list.addColumn('tax_rate', amtcoltype, resourceObject.REPORT['common_field'].WTAX_RATE.label.toUpperCase(), 'right'); //"WITHHOLDING TAX RATE"
        list.addColumn('tax_withheld', amtcoltype, resourceObject.REPORT['common_field'].AMT_TAX_WHELD.label.toUpperCase(), 'right'); //"AMOUNT OF TAX WITHHELD"

        var printUrlComps = [];
        printUrlComps.push('window.open(');
        printUrlComps.push("'");
        printUrlComps.push(nlapiResolveURL('SUITELET', scriptId, _App._ID_DEPLOYMENT_PDF));
        printUrlComps.push('&tax_period_id=', taxPeriodId);
        printUrlComps.push(_IsOneWorld ? '&custpage_subsidiary=' + subId : '&custpage_nexus=' + nexusId);
        printUrlComps.push('&custpage_taxtype=', taxTypeId);
        printUrlComps.push("'");
        printUrlComps.push(');');

        list.addButton('btn_print_sawt', resourceObject.REPORT['button'].PRINT_PDF.label, printUrlComps.join('')); //'Print PDF'

        var csvUrlComps = [];
        csvUrlComps.push('window.open(');
        csvUrlComps.push("'");
        csvUrlComps.push(nlapiResolveURL('SUITELET', scriptId, _App._ID_DEPLOYMENT_CSV));
        csvUrlComps.push('&tax_period_id=', taxPeriodId);
        csvUrlComps.push(_IsOneWorld ? '&custpage_subsidiary=' + subId : '&custpage_nexus=' + nexusId);
        csvUrlComps.push('&custpage_taxtype=', taxTypeId);
        csvUrlComps.push("'");
        csvUrlComps.push(');');

        list.addButton('btn_to_csv', resourceObject.REPORT['button'].SAVE_CSV.label, csvUrlComps.join('')); //'Save CSV'

        var returnToFormUrlComps = [];
        returnToFormUrlComps.push('window.location =');
        returnToFormUrlComps.push("'");
        returnToFormUrlComps.push(nlapiResolveURL('SUITELET', scriptId, _App._ID_DEPLOYMENT_UI));
        returnToFormUrlComps.push('&tax_period_id=', taxPeriodId);
        if (_IsOneWorld)
        	returnToFormUrlComps.push('&custpage_subsidiary=', subId);
        else
        	returnToFormUrlComps.push('&custpage_nexus=', nexusId);
        returnToFormUrlComps.push("';");

        list.addButton('restart', resourceObject.REPORT['button'].RETURN_TO_CRITERIA.label, returnToFormUrlComps.join('')); //'Start Over'
        
        var fiscalCalendarId = _4601.getFiscalCalendar(subId);
        var sawtLineItemsWithTotals = SAWT_REPORT.getCustomerData(allowedWithholdingTaxCodeIds, {
	        	periodfrom: taxPeriodId,
	        	periodto: taxPeriodId,
	        	subid: subId, 
	        	nexusid: nexusId,
	        	taxType: taxTypeId, 
	        	fiscalCalendarId: fiscalCalendarId}, 
        	searchType);
        sawtLineItemsWithTotals.setReturnPeriodValueForAllLineItems(headerValues.returnPeriodFromDate, 
        	headerValues.returnPeriodToDate);
        
        var sawtLineItems = sawtLineItemsWithTotals.getSawtLineItems();
        sawtLineItems.sort(SAWT_REPORT.SortFunction);

        for (var i = 0; i < sawtLineItems.length; ++i) {
            try {
            	if((headerValues.payeeCurrencyLocale == "id_ID") || (headerValues.payeeCurrencyLocale == "ja_JP")) {
            		sawtLineItems[i].setTaxBase(parseInt(sawtLineItems[i].getTaxBase()).toString());
            		sawtLineItems[i].setTaxWithheld(parseInt(sawtLineItems[i].getTaxWithheld()).toString());
            	}
            	
                list.addRow(sawtLineItems[i].toListRow(i + 1));
            } catch (e) {
                nlapiLogExecution('ERROR', 'Tax Withheld by Customer.ListViewSuitelet list ui', 'failed to add row ' + i);
            }
        }
        
        response.writePage(list);
    };
};

SAWT_REPORT.PrintPDFSuitelet = function(request, response)
{
    this.run = function(allowedWithholdingTaxCodeIds)
    {
    	var resourceObject = _4601.getResourceManager();
        var taxPeriodId = request.getParameter("tax_period_id");
        var taxTypeId = request.getParameter("custpage_tax_type");
        var subsidiaryId = request.getParameter('custpage_subsidiary');
        var nexusId = request.getParameter('custpage_nexus');
        var fiscalCalendarId = _4601.getFiscalCalendar(subsidiaryId);
        
        var sawtLineItemsWithTotal = SAWT_REPORT.getCustomerData(allowedWithholdingTaxCodeIds, {
	            periodfrom: taxPeriodId,
	            periodto: taxPeriodId,
	            subid: subsidiaryId,
	            nexusid: nexusId,
	            taxTypeId: taxTypeId, 
	            fiscalCalendarId: fiscalCalendarId},
            searchType);
            
        var headerValues = _4601.HeaderData(subsidiaryId, taxPeriodId, searchType);

        sawtLineItemsWithTotal.setReturnPeriodValueForAllLineItems(headerValues.returnPeriodFromDate, 
    		headerValues.returnPeriodToDate);

        var sawtLineItems = sawtLineItemsWithTotal.getSawtLineItems();
        sawtLineItems.sort(SAWT_REPORT.SortFunction);
        var pdfTemplate = _App.GetFileContent(_App._ROW_PRINT_TEMPLATE, true);
        var row_pdfTemplate = _App.GetFileContent(_App._ROW_TEMPLATE, true);

        var reportRows = [];
        for (var i = 0; i < sawtLineItems.length; ++i)
            reportRows.push(_App.RenderTemplate(row_pdfTemplate, sawtLineItems[i].toXMLEscapedRow(i + 1, searchType)));

        var s = sawtLineItems.length;
        
        var helperUtils = new _4601.HelperUtils(_App, request);    	
    	var arabicfont = nlapiEscapeXML(helperUtils.getFullFileUrl("koodak.ttf"));

        var attributes = {
        	arabicfont: arabicfont,
        	reportTitle: resourceObject.REPORT['report'].WTAX_CUSTOMER_SALES_DETAIL_PDF.title.toUpperCase(),
        	label_periodFrom: resourceObject.REPORT['common_field'].PERIOD_FROM.label.toUpperCase(),
        	label_periodTo: resourceObject.REPORT['common_field'].PERIOD_TO.label.toUpperCase(),
        	label_TIN: resourceObject.REPORT['common_field'].TIN.label.toUpperCase(),
        	label_payeeName: resourceObject.REPORT['common_field'].AGENT_NAME.label.toUpperCase(),
        	label_SeqNo: resourceObject.REPORT['common_field'].SEQ_NO.label.toUpperCase(),
        	label_TaxCode: resourceObject.REPORT['common_field'].TAX_CODE.label.toUpperCase(),
            label_TaxType: resourceObject.REPORT['common_field'].TAX_TYPE.label.toUpperCase(),
            label_Corporation: resourceObject.REPORT['common_field'].CORPORATION.label.toUpperCase(),
            label_IndividualLong: resourceObject.REPORT['common_field'].INDIVIDUAL.long_label.toUpperCase(),
            label_TaxRegNo: resourceObject.REPORT['common_field'].TAX_REG_NO.label.toUpperCase(),
            label_ReferenceNo: resourceObject.REPORT['common_field'].REFERENCE_NO.label.toUpperCase(),
            label_TransactionType: resourceObject.REPORT['common_field'].TRANSACTION_TYPE.label.toUpperCase(),
            label_Memo: resourceObject.REPORT['common_field'].MEMO.label.toUpperCase(),
            label_AmtIncomePmnt: resourceObject.REPORT['common_field'].AMT_INCOME_PMNT.label.toUpperCase(),
            label_TaxRate: resourceObject.REPORT['common_field'].TAX_RATE.label.toUpperCase(),
            label_AmtTaxWheld: resourceObject.REPORT['common_field'].AMT_TAX_WHELD.label.toUpperCase(),
            payeeRegisteredName: _4601.escapeTextForPDF(headerValues.payeeRegisteredName),
            payeeTradeName: _4601.escapeTextForPDF(headerValues.payeeTradeName),
            payeeAddress: _4601.escapeTextForPDF(headerValues.payeeAddress),
            payeeTin: _4601.escapeTextForPDF(headerValues.payeeTin),
            returnPeriodFromDate: nlapiEscapeXML(headerValues.returnPeriodFromDate),
            returnPeriodToDate: nlapiEscapeXML(headerValues.returnPeriodToDate),
            PeriodStartDate: headerValues.returnPeriodFromDate,
            PeriodEndDate: headerValues.returnPeriodToDate,
            lineItems: reportRows.join(""),
            sumTaxBase: _4601.formatCurrency(sawtLineItemsWithTotal.getSumTaxBase()),
            sumTaxWithheld: _4601.formatCurrency(sawtLineItemsWithTotal.getSumTaxWithheld()),
            debug:s
        };

        var content = _App.RenderTemplate(pdfTemplate, attributes);

        try {
            var pdfFile = nlapiXMLToPDF('<?xml version="1.0"?>\n' + '<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);
            response.setContentType("PDF", 'customer_sales_details.pdf', 'attachment');
            response.write(pdfFile.getValue());
        } catch (e) {
            var arrMsg = [];
            response.setContentType("PLAINTEXT", '', 'inline');
            if (e instanceof nlobjError) {
                arrMsg.push(e.getCode() + ':\n' + e.getDetails() + '\n');
                arrMsg.push(content);
            } else {
                arrMsg.push(e.name + ':\n');
                arrMsg.push(e.message + '\n');
                arrMsg.push(content);
            }
            var logException = true;
            if (logException)
                nlapiLogExecution('ERROR', 'PDF generation', arrMsg.join());
            else
                response.write('error:' + arrMsg.join());
        }
    };
};