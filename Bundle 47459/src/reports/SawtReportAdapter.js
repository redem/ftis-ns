/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.SawtReport = WTax.SawtReport || {};

WTax.SawtReport.Header = function _HeaderInfo() {
	return {
	    alphaType : '',
	    formTypeCode : '',
	    payeeTIN : '',
	    payeeBranchCode : '',
	    payeeRegName : '',
	    payeeFirstName : '',
	    payeeMiddleName : '',
	    payeeLastName : '',
	    periodDate : '',
	    payeeRDOCode : ''
	};
};

WTax.SawtReport.Details = function _DetailsInfo() {
	return {
	    alphaType : '',
	    formTypeCode : '',
	    sequenceNumber : '',
	    wtaxAgentTIN : '',
	    wtaxAgentBranchCode : '',
	    wtaxAgentRegName : '',
	    wtaxAgentLastName : '',
	    wtaxAgentFirstName : '',
	    wtaxAgentMiddleName : '',
	    periodDate : '',
	    atcCode : '',
	    natureOfIncomePayment : '',
	    taxRate : '',
	    taxBase : '',
	    taxWithheld : ''
	};
};

WTax.SawtReport.Control = function _ControlInfo() {
	return {
	    alphaType : '',
	    formTypeCode : '',
	    payeeTIN : '',
	    payeeBranchCode : '',
	    periodDate : '',
	    totalTaxBase : '',
	    totalTaxWithheld : '',
	};
};

WTax.SawtReport.FileName = function _FileNameInfo() {
    return {
        payeeTIN: '',
        payeeBranchCode: '',
        periodDateFileName: '',
        formTypeCode: ''
    };
};


function _Adapter(formTypeCode) {
	this.formTypeCode = formTypeCode;
	this.ALPHA_TYPE = {
	    HEADER: 'HSAWT',
	    DETAILS: 'DSAWT',
	    CONTROL: 'CSAWT'
	};
	this.FORM_TYPE_PREFIX = {
        HEADER: 'H',
        DETAILS: 'D',
        CONTROL: 'C'
	};
};

WTax.SawtReport.Adapter = _Adapter;
WTax.SawtReport.Adapter.prototype = new WTax.ReportAdapter();
WTax.SawtReport.Adapter.prototype.constructor = _Adapter; 

WTax.SawtReport.Adapter.prototype.getConvertedHeaderInfo = function _getConvertedHeaderInfo(headerInfo, taxInfo) {
	var convertedHeaderInfo = new WTax.SawtReport.Header();
	convertedHeaderInfo.alphaType = this.ALPHA_TYPE.HEADER;
	convertedHeaderInfo.formTypeCode = this.FORM_TYPE_PREFIX.HEADER + this.formTypeCode;
	convertedHeaderInfo.payeeTIN = this.getTaxIdNum(headerInfo.rawTin);
	convertedHeaderInfo.payeeBranchCode = this.getBranchCode(headerInfo.rawTin);
	convertedHeaderInfo.payeeRegName = headerInfo.payeeRegisteredName || headerInfo.companyName;
	convertedHeaderInfo.payeeRDOCode = taxInfo.rdoCode;
	convertedHeaderInfo.payeeFirstName = taxInfo.firstName;
	convertedHeaderInfo.payeeMiddleName = taxInfo.middleName;
	convertedHeaderInfo.payeeLastName = taxInfo.lastName;
	convertedHeaderInfo.periodDate = headerInfo.periodDate;
	return convertedHeaderInfo;
};


WTax.SawtReport.Adapter.prototype.getConvertedGrandTotal = function _getConvertedGrandTotal(grandTotal, headerInfo) {
	var convertedGrandTotal = new WTax.SawtReport.Control();
	convertedGrandTotal.alphaType = this.ALPHA_TYPE.CONTROL; 
	convertedGrandTotal.formTypeCode = this.FORM_TYPE_PREFIX.CONTROL + this.formTypeCode;
	convertedGrandTotal.payeeTIN = this.getTaxIdNum(headerInfo.rawTin);
	convertedGrandTotal.payeeBranchCode = this.getBranchCode(headerInfo.rawTin);
	convertedGrandTotal.periodDate = headerInfo.periodDate;
	convertedGrandTotal.totalTaxBase = grandTotal.income;
	convertedGrandTotal.totalTaxWithheld = grandTotal.taxWithheld;
	return convertedGrandTotal;
};

WTax.SawtReport.Adapter.prototype.convertLine = function _convertLine(sawtLine, headerInfo, sequenceNumber) {
	var convertedSawtLine = new WTax.SawtReport.Details();
	convertedSawtLine.alphaType = this.ALPHA_TYPE.DETAILS;
	convertedSawtLine.formTypeCode = this.FORM_TYPE_PREFIX.DETAILS + this.formTypeCode;
	convertedSawtLine.sequenceNumber = sequenceNumber;
	convertedSawtLine.wtaxAgentTIN = this.getTaxIdNum(sawtLine.tin);
	convertedSawtLine.wtaxAgentBranchCode = this.getBranchCode(sawtLine.tin);
	convertedSawtLine.wtaxAgentRegName = sawtLine.regName;
	convertedSawtLine.wtaxAgentLastName = sawtLine.getLastName();
	convertedSawtLine.wtaxAgentFirstName = sawtLine.getFirstName();
	convertedSawtLine.wtaxAgentMiddleName = sawtLine.getMiddleName();
	convertedSawtLine.periodDate = headerInfo.periodDate;
	convertedSawtLine.atcCode = sawtLine.atc;
	//convertedSawtLine.natureOfIncomePayment = sawtLine.desc;
	convertedSawtLine.taxRate = sawtLine.taxRate;
	convertedSawtLine.taxBase = sawtLine.taxBase;
	convertedSawtLine.taxWithheld = sawtLine.taxWithheld;
	return convertedSawtLine;
};

WTax.SawtReport.Adapter.prototype.getConvertedFileNameObject = function _getConvertedFileNameObject(headerInfo, formTypeCode) {
    var fileNameObj = new WTax.SawtReport.FileName();
    fileNameObj.payeeTIN = this.getTaxIdNum(headerInfo.rawTin);
    fileNameObj.payeeBranchCode = this.getBranchCode(headerInfo.rawTin);
    fileNameObj.periodDateFileName = headerInfo.periodDate;
    fileNameObj.formTypeCode = formTypeCode;
    return fileNameObj;
};



