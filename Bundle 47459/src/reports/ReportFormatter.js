/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.FieldTypes = {
    TEXT: 'text',
    TEXT_QUOTES: 'text_quotes',
    DATE: 'date',
    DATE_SEPARATOR: 'date_separator', 
    INTEGER: 'integer',
    INTEGER_PADDED: 'integer_padded',
    DECIMAL: 'decimal'
};

WTax.ReportFormatter = function _ReportFormatter() {
    this.fields = {};
    this.NONE = '- None -';
    this.MONTH_SEPARATOR = '/';
    this.ZERO = '0000000000';
    this.TEMPLATE = {};
    this.CONTENT_TYPE = 'PLAINTEXT';
};

WTax.ReportFormatter.prototype.getContentType = function _getContentType() {
    return this.CONTENT_TYPE;
};

WTax.ReportFormatter.prototype.formatFileName = function _formatFileName(mapData) {
    return mapData.toString();
};

WTax.ReportFormatter.prototype.format = function _format(mapData) {
    return mapData.toString();
};

WTax.ReportFormatter.prototype.formatElement = function _formatElement(mapElement, template) {
    var obj = this.formatDataObject(mapElement);
    return _App.RenderTemplate(template, obj);
};

WTax.ReportFormatter.prototype.formatDataObject = function _formatDataObject(raw) {
    var formatted = {};
    
    for (var f in raw) {
        var rawField = raw[f];
        var fieldDef = this.fields[f];
        
        formatted[f] = fieldDef ? this.formatField(rawField, fieldDef) : rawField;
    }
    
    return formatted;
};

WTax.ReportFormatter.prototype.formatField = function _formatField(data, fieldDefinition) {
    var result = '';
    
    if (data == this.NONE) {
        data = '';
    }
    
    switch (fieldDefinition.type) {
        case WTax.FieldTypes.TEXT:
            result = this.formatString(data, fieldDefinition.width);
            break;
        case WTax.FieldTypes.TEXT_QUOTES:
            result = this.formatString(data, fieldDefinition.width);
            if (result) {
                result = '"' + result + '"';
            }
            break;
        case WTax.FieldTypes.DATE:
            result = this.formatDate(data, '');
            break;
        case WTax.FieldTypes.DATE_SEPARATOR:
            result = this.formatDate(data, this.MONTH_SEPARATOR);
            break;
        case WTax.FieldTypes.INTEGER:
            result = this.formatInteger(data);
            break;
        case WTax.FieldTypes.INTEGER_PADDED:
            result = this.formatInteger(data);
            result = this.padNumber(result, fieldDefinition.width);
            break;
        case WTax.FieldTypes.DECIMAL:
            result = this.formatDecimal(data);
            break;            
        default:
            result = data;
    }
    
    return result;
};

WTax.ReportFormatter.prototype.formatDate = function _formatDate(dateObj, separator) {
    if (!dateObj) {
        return '';
    }
    dateObj = nlapiStringToDate(dateObj); 
    formatted = '' + this.padNumber(dateObj.getMonth() + 1, 2) + separator + dateObj.getFullYear(); 
    return formatted;
};

WTax.ReportFormatter.prototype.padNumber = function _padNumber(data, width) {
    if (!data) {
        data = 0;
    }
    
    // Data: Integers only (positive, no decimal values)
    // Width should be positive
    var zero = this.ZERO + Math.abs(data);
    return zero.slice(-width);
};

WTax.ReportFormatter.prototype.replaceInvalidCharacters = function _replaceInvalidCharacters(data) {
    // Replace invalid characters as necessary
    return data.replace(/'|,/g, '');
};

WTax.ReportFormatter.prototype.formatString = function _formatString(data, width) {
    if (!data) {
        return '';
    }
    
    var formatted = this.replaceInvalidCharacters(data);
    return formatted.substr(0, width);
};

WTax.ReportFormatter.prototype.formatInteger = function _formatInteger(data) {
    if (!data) {
        return 0;
    }
    
    return parseInt(data, 10).toFixed(0);
};

WTax.ReportFormatter.prototype.formatDecimal = function _formatDecimal(data) {
    if (!data) {
        return '0.00';
    }
    
    return parseFloat(data).toFixed(2);
};

