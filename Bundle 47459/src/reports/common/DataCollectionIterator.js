/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};


WTax.DataCollectionIterator = function(dao, params) {
    if (!params) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Required call parameters are missing.');
        WTaxError.throwError(error, 'WTax.DataCollectionIterator', WTaxError.LEVEL.ERROR);
    } else if (!dao) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A DAO is required.');
        WTaxError.throwError(error, 'WTax.DataCollectionIterator', WTaxError.LEVEL.ERROR);
    }
    
    this.MAX_LINES = 1000;
    this.dao = dao;
    this.start = params.start || 0;
    this.end = params.end;
    this.nextIndex = params.start || 0;
    this.hasError = false;
    this.data = [];
};


WTax.DataCollectionIterator.prototype.getData = function() {
    this.data = [];
    
    try {
        var endIndex = this.end ? Math.min(this.nextIndex + this.MAX_LINES, this.end) : (this.nextIndex + this.MAX_LINES);
        this.data = this.dao.getList(this.nextIndex, endIndex);
        this.nextIndex = endIndex;
    } catch(e) {
        this.hasError = true;
        WTaxError.logError(e, 'WTax.DataCollectionIterator.getData', WTaxError.LEVEL.ERROR);
    }
    
    return this.data;
};


WTax.DataCollectionIterator.prototype.hasNext = function() {
    return (this.data.length >= this.MAX_LINES) && !this.hasError;
};
