/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTAX_APP_ID = 'c54f4fb9-a81f-40e0-bf8f-ae7d00f57073';
var FILENAME = {
    BIR_LOGO    : 'wi_tax_print_logo.gif',
    FONT        : 'koodak.ttf',
    BLACK_PIXEL : 'wi_tax_black_pixel.gif'
};

var LABEL = {
    REPORT_HEADER                : 'Withholding Tax Reports',
    REFRESH                      : 'Refresh',
    PRINT                        : 'Print',
    PRINT_INDIVIDUAL             : 'Print Individual 2307',
    SUBSIDIARY                   : 'Subsidiary',
    NEXUS                        : 'Nexus',
    REPORT                       : 'Report',
    TAX_PERIOD                   : 'Tax Period',
    TAX_TYPE                     : 'Tax Type',
    VENDOR                       : 'Vendor',
    SELECT_TXN 				     : 'Select Transactions',
    SAVE                         : 'Save',
    CANCEL                       : 'Cancel',
    SCRIPT_ID                    : 'Script ID',
    TXN_COUNT                    : 'Number of Transactions',
    ALL_TXN_ID_LIST              : 'All Transaction ID List',
    SELECTED_TXN_ID_LIST         : 'Selected Transaction ID List',
    TAX_PERIOD_LIST              : 'Tax Period ID List',
    PURCHASE_TAX_POINT           : 'Purchase Tax Point',
    WTAX_CODE_LIST               : 'WTAX Code ID List',
    CACHE_ID                     : 'Selected Transaction Cache ID',
    RELOAD_CACHE                 : 'Reload Cache',
    REPORT_NOT_AVAILABLE_HEADER  : 'No Report Available',
    REPORT_NOT_AVAILABLE_MESSAGE : 'The withholding tax report for this country is not currently supported in NetSuite.',
    VENDOR_NOT_SELECTED          : 'Vendor is required. Please specify a vendor and click the Refresh button before clicking the Select Transactions button.',
    REPORT_FILTERS_ARE_CHANGED   : 'The report filters have been changed. Please refresh the page first.',
    SAVE_TXN_ERROR_HEADER        : 'Form 2307 Error',
    SAVE_TXN_ERROR_MESSAGE       : 'Unable to save the selected transactions.'
};

var FORM2307 = 'form2307';
var FORM2307_COUNTRY = 'PH';
var FORM2307_MAX_EWT_ROWS = 13;
var FORM2307_MAX_WBT_ROWS = 16;

var REPORT_MAP = {
    'form2307': {
        script              : '',
        clientScript        : 'customscript_form_2307_client',
        onlineDeployment    : 'customdeploy_form_2307_online',
        printDeployment     : 'customdeploy_form_2307_print',
        selectTxnDeployment : 'customdeploy_form_2307_selecttxn',
        printIndividualDeployment : 'customdeploy_form_2307_printindividual',
    },
    'form1601e' : {
        script             : 'customscript_form_1601',
        onlineDeployment   : 'customdeploy_form_1601_online'
    },
    'sawt' : {
        script             : 'customscript_sawt',
        onlineDeployment   : 'customdeploy_sawt'
    },
    'map' : {
        script             : 'customscript_map',
        onlineDeployment   : 'customdeploy_map'
    }
};

var FIELD_NAME = {
    REFRESH                 : 'custpage_refresh',    
    SUBSIDIARY              : 'custpage_subsidiary',
    NEXUS                   : 'custpage_nexus',
    REPORT                  : 'custpage_countryreport',
    TAX_PERIOD              : 'custpage_taxperiod',
    TAX_TYPE                : 'custpage_taxtype',
    VENDOR                  : 'custpage_vendor',
    PRINT                   : 'custpage_print',
    PRINT_INDIVIDUAL        : 'custpage_printindividual',
    MESSAGE                 : 'custpage_message',
    REPORT_FORM             : 'custpage_report',
    ERROR_HEADER            : 'custpage_errormessage',
    PAYEE_TIN               : 'payeetin',
    PAYEE_NAME              : 'payeename',
    PAYEE_ADDRESS           : 'payeeaddress',
    PAYEE_ZIP               : 'payeezip',
    PAYEE_FOREIGN_ADDRESS   : 'foreignaddress',
    PAYEE_FOREIGN_ZIP       : 'foreignzip',
    SELECT_TXN              : 'custpage_selecttxn',
    SAVE                    : 'custpage_save',
    CANCEL                  : 'custpage_cancel',
    TXN_LIST                : 'custpage_txnlist',
    REQUEST_TYPE            : 'requesttype',
    START_IDX               : 'start',
    PAGE_SIZE               : 'pagesize',
    SCRIPT_ID               : 'custpage_script_id',
    TXN_COUNT               : 'txncount',
    ALL_TXN_ID_LIST         : 'alltxnidlist',
    SELECTED_TXN_ID_LIST    : 'selectedtxnidlist',
    TAX_PERIOD_LIST         : 'taxperiodlist',
    PURCHASE_TAX_POINT      : 'purchasetaxpoint',
    WTAX_CODE_LIST          : 'wtaxcodelist',
    CACHE_ID                : 'cacheid',
    RELOAD_CACHE            : 'reloadcache'
};

var WINDOW_NAME = {
    SELECT_TXN : 'custwindow_selecttxn',
};

var MESSAGE_PRIORITY = {
    LOW     : 'NLAlertDialog.TYPE_LOW_PRIORITY',
    MEDIUM  : 'NLAlertDialog.TYPE_MEDIUM_PRIORITY',
    HIGH    : 'NLAlertDialog.TYPE_HIGH_PRIORITY',
};

var BLANK = '&nbsp;';

var TAX_POINT = {
    ACCRUAL : 'onaccrual',
    PAYMENT : 'onpayment',
};

var LENGTH = {
    TIN : 12,
    ZIP : 4
};

var REQUEST = {
    SELECT_TXN_FORM      : 'select_txn_form',
    SELECT_TXN_PAGE_DATA : 'select_txn_page_data',
    SELECT_TXN_SAVE_TXN  : 'select_txn_save_txn',
};


