/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};


/**
 * @param params.dao - The name of the Data Access Object to get data with.
 * @param params.start - The index number of the first result to return, inclusive.
 * @param params.end - The index number of the last result to return, exclusive.
 */
WTax.DataCollectionService = function() {
    this.monitors = [];
};


WTax.DataCollectionService.prototype.getData = function(params) {
    var data = [];
    
    if (!params) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Required call parameters are missing.');
        WTaxError.throwError(error, 'WTax.DataCollectionService.getData', WTaxError.LEVEL.ERROR);
    } else if (!params.daoName) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A DAO is required.');
        WTaxError.throwError(error, 'WTax.DataCollectionService.getData', WTaxError.LEVEL.ERROR);
    }
    
    var dao = new WTax.DAO[params.daoName](params.daoParameters);
    var iterator = new WTax.DataCollectionIterator(dao, {start: params.start, end: params.end});
    
    do {
        data = data.concat(iterator.getData());
    } while (iterator.hasNext() && !this.isThresholdReached())
    
    return data;
};


WTax.DataCollectionService.prototype.addThresholdMonitor = function(monitor) {
    if (!monitor) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A threshold monitor instance is required.');
        WTaxError.throwError(error, 'WTax.DataCollectionService.addThresholdMonitor', WTaxError.LEVEL.ERROR);
    }
    
    this.monitors.push(monitor);
};


WTax.DataCollectionService.prototype.isThresholdReached = function() {
    var result = false;
    
    for (var i = 0; i < this.monitors.length; i++) {
        result = this.monitors[i].isThresholdReached();
        
        if (result) {
            break;
        }
    }
    
    return result;
};
