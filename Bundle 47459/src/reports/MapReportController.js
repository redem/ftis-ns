/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.MapReport = WTax.MapReport || {};

WTax.MapReport.Controller = function(params) { 
	if (!params) {
		return;
	}
	
	this.creditableWTaxCodeIds = params.creditableWTaxCodeIds;
	this.formTypeCode = params.formTypeCode;
	this.periodId = params.periodId;
	this.searchType = params.searchType;
	this.subId = params.subId;
	this.nexusId = params.nexusId;

	this.adapter = params.adapter;
	this.ds = params.ds;
	this.header = params.header;
	this.formatter = params.formatter;
	this.taxSetupDao = params.taxSetupDao;
};

WTax.MapReport.Controller.prototype.Run = function _run() {
	var output = {
	    fileName: '',
	    contentType: '',
	    contents: ''
	};

	try {
		var mapData = {
			details: [], 
			control: {}, 
			header: {}
		};

		var fiscalCalendarId = this.header.getFiscalCalendar(this.subId);
		var vendorDataParams = {
			periodId: this.periodId, 
			subid: this.subId, 
			nexusid: this.nexusId,
			fiscalCalendarId: fiscalCalendarId
		};
		var headerInfo = this.header.GetHeaderInfo(this.subId, this.periodId, this.searchType);
		var rdoCode = this.taxSetupDao.getSubsidiaryRDOCode(this.subId);
		mapData.header = this.adapter.getConvertedHeaderInfo(headerInfo, rdoCode);

		var reportData = this.ds.getVendorData(this.creditableWTaxCodeIds, vendorDataParams, this.searchType);
		mapData.details = this.adapter.getConvertedLineItems(reportData.getMapLineItems(), headerInfo);
		mapData.control = this.adapter.getConvertedGrandTotal(reportData.getGrandTotal(), headerInfo);
		
		var fileNameObj = this.adapter.getConvertedFileNameObject(headerInfo, this.formTypeCode);
		output.fileName = this.formatter.formatFileName(fileNameObj);
        output.contentType = this.formatter.getContentType();
		output.contents = this.formatter.format(mapData);
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.MapReport.Controller.Run', ex.message || ex.toString());
	}

	return output;
};
