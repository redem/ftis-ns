/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.SubsidiaryTaxInfo = function _SubsidiaryTaxInfo() {
    var taxInfo = {
        rdoCode: '',
        firstName: '',
        middleName: '',
        lastName: ''
    };
    return taxInfo;
};

WTax.TaxSetupDao = function _TaxSetupDao() {
    this.phCountryCode = 'PH';
    this.ITRBundleGUID = 'e5775970-8e28-40ff-ad4a-956e88304834';
    this.ITR_NAME = {
        RDO: 'RDO',
        FIRST_NAME: 'ContactFirst',
        MIDDLE_NAME: 'ContactMiddle',
        LAST_NAME: 'ContactLast'
    };
    
    this.isItrInstalled = this.isITRBundleInstalled();
};

WTax.TaxSetupDao.prototype.isITRBundleInstalled = function _isITRBundleInstalled() {
    var searchFilters = [new nlobjSearchFilter('name', null, 'is', this.ITRBundleGUID)];
    var searchResults = nlapiSearchRecord('file', null, searchFilters, null);
    return searchResults != null ? true : false;
};

WTax.TaxSetupDao.prototype.getSubsidiaryRDOCode = function _getSubsidiaryRDOCode(subsidiaryId) {
    var rdoCode = '';
    if (this.isItrInstalled) {
        var searchFilters = [
            new nlobjSearchFilter('custrecord_vat_cfg_name', null, 'is', this.ITR_NAME.RDO),
            new nlobjSearchFilter('custrecord_vat_cfg_country', null, 'is', this.phCountryCode)];
        if (subsidiaryId) {
            searchFilters.push(new nlobjSearchFilter('custrecord_vat_cfg_subsidiary', null, 'is', subsidiaryId));
        }
        
        var searchColumns = [new nlobjSearchColumn('custrecord_vat_cfg_value')];
        var searchResults = nlapiSearchRecord('customrecord_tax_return_setup_item', null, searchFilters, searchColumns);
        
        if (searchResults && searchResults.length > 0) {
            rdoCode = searchResults[0].getValue('custrecord_vat_cfg_value');
        }
    }
    return rdoCode;
};

WTax.TaxSetupDao.prototype.getSubsidiaryTaxInfo = function _getSubsidiaryTaxInfo(subsidiaryId) {
    var taxInfo = new WTax.SubsidiaryTaxInfo();
    
    if (!this.isItrInstalled) {
        return taxInfo;
    }
    
    try {
        var searchFilters = [new nlobjSearchFilter('custrecord_vat_cfg_country', null, 'is', this.phCountryCode)];
        if (subsidiaryId) {
            searchFilters.push(new nlobjSearchFilter('custrecord_vat_cfg_subsidiary', null, 'is', subsidiaryId));
        }
        
        var searchColumns = [
            new nlobjSearchColumn('custrecord_vat_cfg_value'),
            new nlobjSearchColumn('custrecord_vat_cfg_name')];
        var searchResults = nlapiSearchRecord('customrecord_tax_return_setup_item', null, searchFilters, searchColumns);
        if (!searchResults) {
            return taxInfo;
        }
        
        for (var i = 0; i< searchResults.length; i++) {
            var name = searchResults[i].getValue('custrecord_vat_cfg_name');
            var value = searchResults[i].getValue('custrecord_vat_cfg_value');
            
            switch (name) {
                case this.ITR_NAME.RDO:
                    taxInfo.rdoCode = value;
                    break;
                case this.ITR_NAME.FIRST_NAME:
                    taxInfo.firstName = value;
                    break;
                case this.ITR_NAME.MIDDLE_NAME:
                    taxInfo.middleName = value;
                    break;
                case this.ITR_NAME.LAST_NAME:
                    taxInfo.lastName = value;
                    break;
                default:
                    nlapiLogExecution('DEBUG', 'Error in WTax.TaxSetupDao.getSubsidiaryTaxInfo', 'Incorrect data: ' + name);
                    break;
            }
        }
    } catch (ex) {
            nlapiLogExecution('ERROR', 'WTax.TaxSetupDao.getSubsidiaryTaxInfo', ex.message || ex.toString());
    }
    
    return taxInfo;
};

