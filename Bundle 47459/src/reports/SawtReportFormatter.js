/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.SawtReport = WTax.SawtReport || {};

WTax.SawtReport.FieldDefinitions = function _FieldDefinitions(){};

WTax.SawtReport.FieldDefinitions.prototype.getFields = function _getFields() {
    var fields = {
        alphaType: {
            type: WTax.FieldTypes.TEXT,
            width: 5
        },
        formTypeCode: {
            type: WTax.FieldTypes.TEXT,
            width: 6
        },
        payeeTIN: {
            type: WTax.FieldTypes.TEXT,
            width: 9
        },
        payeeBranchCode: {
            type: WTax.FieldTypes.INTEGER_PADDED,
            width: 4
        },
        payeeRegName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 50
        },
        payeeLastName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        payeeFirstName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        payeeMiddleName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        periodDate: {
            type: WTax.FieldTypes.DATE_SEPARATOR,
            width: 7
        },
        periodDateFileName: {
            type: WTax.FieldTypes.DATE,
            width: 6
        },
        payeeRDOCode: {
            type: WTax.FieldTypes.INTEGER_PADDED,
            width: 3
        },
        sequenceNumber: {
            type: WTax.FieldTypes.INTEGER,
            width: 8
        },
        wtaxAgentTIN: {
            type: WTax.FieldTypes.TEXT,
            width: 9
        },
        wtaxAgentBranchCode: {
            type: WTax.FieldTypes.INTEGER_PADDED,
            width: 4
        },
        wtaxAgentRegName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 50
        },
        wtaxAgentLastName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        wtaxAgentFirstName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        wtaxAgentMiddleName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        atcCode: {
            type: WTax.FieldTypes.TEXT,
            width: 5
        },
        natureOfIncomePayment: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 50
        },
        taxRate: {
            type: WTax.FieldTypes.DECIMAL,
            width: 5
        },
        taxBase: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        },
        taxWithheld: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        },
        totalTaxBase: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        },
        totalTaxWithheld: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        }
    };

    return fields;

};

function _Formatter() {
    var fieldDef = new WTax.SawtReport.FieldDefinitions();
    this.fields = fieldDef.getFields();
    this.TEMPLATE = {};
    this.TEMPLATE.HEADER = '{alphaType},{formTypeCode},{payeeTIN},{payeeBranchCode},{payeeRegName},{payeeLastName},{payeeFirstName},{payeeMiddleName},{periodDate},{payeeRDOCode}\n';
    this.TEMPLATE.DETAILS = '{alphaType},{formTypeCode},{sequenceNumber},{wtaxAgentTIN},{wtaxAgentBranchCode},{wtaxAgentRegName},{wtaxAgentLastName},{wtaxAgentFirstName},{wtaxAgentMiddleName},{periodDate},{natureOfIncomePayment},{atcCode},{taxRate},{taxBase},{taxWithheld}\n';
    this.TEMPLATE.CONTROL = '{alphaType},{formTypeCode},{payeeTIN},{payeeBranchCode},{periodDate},{totalTaxBase},{totalTaxWithheld}';
    this.TEMPLATE.FILE_NAME = '{payeeTIN}{payeeBranchCode}{periodDateFileName}{formTypeCode}.dat';
}

WTax.SawtReport.Formatter = _Formatter;
WTax.SawtReport.Formatter.prototype = new WTax.ReportFormatter();
WTax.SawtReport.Formatter.prototype.constructor = _Formatter; 

WTax.SawtReport.Formatter.prototype.formatFileName = function _formatFileName(fileNameObj) {
    return this.formatElement(fileNameObj, this.TEMPLATE.FILE_NAME);
};

WTax.SawtReport.Formatter.prototype.format = function _format(sawtData) {
    var output = '';
    output += this.formatElement(sawtData.header, this.TEMPLATE.HEADER);

    for (var i = 0; i < sawtData.details.length; i++) {
        output += this.formatElement(sawtData.details[i], this.TEMPLATE.DETAILS);
    }

    output += this.formatElement(sawtData.control, this.TEMPLATE.CONTROL);
    return output;

};
