/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Jul 2012     ljamolod
 *
 */

/**
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @return {void}
 */
function fieldChanged(type, name, linenum){
	if (name == 'custpage_countryreport') {
		var params = {};
		var newurl = '';
		
		if (nlapiGetFieldValue('custpage_countryreport') == 'form1601e') {
			newurl = [nlapiResolveURL('SUITELET', 'customscript_form_1601', 'customdeploy_form_1601_online')];
		} else if (nlapiGetFieldValue('custpage_countryreport') == 'map') {
			newurl = [nlapiResolveURL('SUITELET', 'customscript_map', 'customdeploy_map')];
		} else if (nlapiGetFieldValue('custpage_countryreport') == 'form2307') {
			newurl = [nlapiResolveURL('SUITELET', 'customscript_4601_countryreports', 'customdeploy_form_2307_online')];
		}
		
		if (nlapiGetFieldValue('custpage_subsidiary') != null) {
			params.custpage_subsidiary = nlapiGetFieldValue('custpage_subsidiary');
		} else if (nlapiGetFieldValue('custpage_nexus') != null) {
            params.custpage_nexus = nlapiGetFieldValue('custpage_nexus');
        }
		
		newurl += Object.keys(params).map(function (p) {
            return '&' + p + '=' + params[p];
        }).join('');
		
		window.open(newurl, '_self');
	} else if (name == 'custpage_subsidiary') {
	    setWindowChanged(window, false);
		var url = [];
		url.push(nlapiGetFieldValue("urlrefresh"));
		url.push('&refresh=y');
		url.push('&custpage_subsidiary=' + nlapiGetFieldValue('custpage_subsidiary'));
		url.push('&tax_period_id=' + nlapiGetFieldValue('tax_period_id'));
		window.location = url.join("");
	} else if (name == 'custpage_nexus') {
	    setWindowChanged(window, false);
		var url = [];
		url.push(nlapiGetFieldValue("urlrefresh"));
		url.push('&refresh=y');
		url.push('&custpage_nexus=' + nlapiGetFieldValue('custpage_nexus'));
		url.push('&tax_period_id=' + nlapiGetFieldValue('tax_period_id'));
		window.location = url.join("");
	}
}
