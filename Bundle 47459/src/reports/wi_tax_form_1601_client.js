/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * client script of RP form 1601
 */

/**
 * stores the data of the tax periods of the current company
 */
var taxPeriodData = new Array();

//data place holders
var _box = new Array();

if (!_4601) { var _4601 = {}; }

function form_1601_printForm(){
    var _IsOneWorld = nlapiGetContext().getSetting("FEATURE", "SUBSIDIARIES") == "T";
    
	if(  nlapiGetField('custpage_subsidiary') != null && nlapiGetFieldValue('custpage_subsidiary') == '' ){
		alert('Please enter value(s) for: Subsidiary');
		return false;
	}
	
	var url = [nlapiGetFieldValue("urlprint"),
        '&periodFrom=', nlapiGetFieldValue('periodfrom'),
		'&periodTo=', nlapiGetFieldValue('periodfrom'), 
		_IsOneWorld ? '&custpage_subsidiary=' + nlapiGetFieldValue('custpage_subsidiary') : '&custpage_nexus=' + nlapiGetFieldValue('custpage_nexus'),
		'&custpage_taxtype=', nlapiGetFieldValue('custpage_taxtype'),
		'&box15a=', _box['15a'],
		'&box15b=', _box['15b'],
		'&box17a=', _box['17a'],
		'&box17b=', _box['17b'],
		'&box17c=', _box['17c']].join("");
	window.open(url);
}


function fieldChanged( type, name ){
	var params = {};
	var newurl = '';
	
	if (name == 'custpage_countryreport') {
		if (nlapiGetFieldValue('custpage_countryreport') == 'form2307') {
			newurl = [nlapiResolveURL('SUITELET', 'customscript_4601_countryreports', 'customdeploy_form_2307_online')];
		} else if (nlapiGetFieldValue('custpage_countryreport') == 'sawt') {
			newurl = [nlapiResolveURL('SUITELET', 'customscript_sawt', 'customdeploy_sawt')];
		} else if (nlapiGetFieldValue('custpage_countryreport') == 'map') {
			newurl = [nlapiResolveURL('SUITELET', 'customscript_map', 'customdeploy_map')];
		}
		
		if (nlapiGetFieldValue('custpage_subsidiary') != null) {
			params.custpage_subsidiary = nlapiGetFieldValue('custpage_subsidiary');
    	} else if (nlapiGetFieldValue('custpage_nexus') != null) {
            params.custpage_nexus = nlapiGetFieldValue('custpage_nexus');
        }
		
		newurl += Object.keys(params).map(function (p) {
            return '&' + p + '=' + params[p];
        }).join('');
		
		window.open(newurl, '_self');
	} else if (name == 'custpage_subsidiary') {
	    setWindowChanged(window, false);
		var url = [];
		url.push(nlapiGetFieldValue("urlrefresh"));
		url.push('&refresh=y');
		url.push('&custpage_subsidiary=' + nlapiGetFieldValue('custpage_subsidiary'));
		url.push('&periodfrom=' + nlapiGetFieldValue('periodfrom'));
		window.location = url.join("");
	} else if (name == 'custpage_nexus') {
	    setWindowChanged(window, false);
		var url = [];
		url.push(nlapiGetFieldValue("urlrefresh"));
		url.push('&refresh=y');
		url.push('&custpage_nexus=' + nlapiGetFieldValue('custpage_nexus'));
		url.push('&periodfrom=' + nlapiGetFieldValue('periodfrom'));
		window.location = url.join("");
	}
}


function onChangeTxField( index, field ) {
	if( field.value == '' ) {
		field.value = _4601.formatCurrency( 0 );
	}
	
	_box[index] = parseFloat(field.value) || 0;
	
	// update all boxes
	updateTaxBoxes();
}


function updateTaxBoxes() {
	_box['14'] = nlapiGetFieldValue('box14') || 0;
	
	// update box 15c
	// formula = 15a + 15b
	_box['15c'] = parseFloat( nlapiFormatCurrency(_box['15a'] || 0) ) + parseFloat( nlapiFormatCurrency(_box['15b'] || 0) ) || 0;
	document.getElementById('wh-field-15A').innerHTML = '<input onchange="onChangeTxField(\'15a\', this)" type="text" value="' + _4601.formatCurrency(_box['15a'] || 0) + '" id="box15a" onkeypress="return IsNumeric(event, this);">';
	document.getElementById('wh-field-15B').innerHTML = '<input onchange="onChangeTxField(\'15b\', this)" type="text" value="' + _4601.formatCurrency(_box['15b'] || 0) + '" id="box15b" onkeypress="return IsNumeric(event, this);">';
	document.getElementById('wh-field-15C').innerHTML = _4601.formatCurrency(_box['15c']);
	
	// update box 16
	//formula = 14 - 15c
	_box['16'] = parseFloat( nlapiFormatCurrency(_box['14'] || 0) ) - parseFloat( nlapiFormatCurrency(_box['15c'] || 0) ) || 0;
	document.getElementById('wh-field-16').innerHTML = _4601.formatCurrency(_box['16']);
	
	
	// update 17d
	// formula = 17a + 17b + 17c
	_box['17d'] = parseFloat( nlapiFormatCurrency(_box['17a'] || 0) ) + parseFloat(nlapiFormatCurrency(_box['17b'] || 0) ) + parseFloat( nlapiFormatCurrency(_box['17c'] || 0) ) || 0;
	document.getElementById('wh-field-17A').innerHTML = '<input style="font-size: 11px;" onchange="onChangeTxField(\'17a\', this)" type="text" value="' + _4601.formatCurrency(_box['17a'] || 0) + '" onkeypress="return IsNumeric(event, this);" id="box17a">';
	document.getElementById('wh-field-17B').innerHTML = '<input style="font-size: 11px;" onchange="onChangeTxField(\'17b\', this)" type="text" value="' + _4601.formatCurrency(_box['17b'] || 0) + '" onkeypress="return IsNumeric(event, this);" id="box17b">';
	document.getElementById('wh-field-17C').innerHTML = '<input style="font-size: 11px;" onchange="onChangeTxField(\'17c\', this)" type="text" value="' + _4601.formatCurrency(_box['17c'] || 0) + '" onkeypress="return IsNumeric(event, this);" id="box17c">';
	document.getElementById('wh-field-17D').innerHTML = _4601.formatCurrency(_box['17d']);
	
	// update total box18
	//formula 16 + 17d
	_box['18'] = parseFloat( nlapiFormatCurrency(_box['16'] || 0) ) + parseFloat( nlapiFormatCurrency(_box['17d'] || 0) ) || 0;
	document.getElementById('wh-field-18').innerHTML = _4601.formatCurrency(_box['18']);
}


function IsNumeric(eventObj, obj) {
    var keyCode = (document.all) ? eventObj.keyCode : eventObj.which;  //Check For Browser Type

    if (keyCode <= 31)  //Ignore control characters
        return true;

    if (keyCode == ".".charCodeAt(0))
        return obj.value.indexOf(".") == -1;

    return (keyCode >= "0".charCodeAt(0)) && (keyCode <= "9".charCodeAt(0));
}