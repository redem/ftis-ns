/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.SawtReport = WTax.SawtReport || {};

WTax.SawtReport.Controller = function(params) { 
    if (!params) {
        return;
    }
    
    this.wtaxCodeIds = params.wtaxCodeIds;
    this.periodId = params.periodId;
    this.subId = params.subId;
    this.nexusId = params.nexusId;
    this.searchType = params.searchType;
    this.fiscalCalendarId = params.fiscalCalendarId;
    this.formType = params.formType;

    this.adapter = params.adapter;
    this.ds = params.ds;
    this.header = params.header;
    this.formatter = params.formatter;
    this.taxSetupDao = params.taxSetupDao;
};

WTax.SawtReport.Controller.prototype.Run = function _run() {
    var output = {
        fileName: '',
        contentType: '',
        contents: ''
    };

    try {
        var sawtData = {
            details: [], 
            control: {}, 
            header: {},
        };

        // Header
        var headerInfo = this.header.HeaderData(this.subId, this.periodId, this.searchType);
        var taxInfo = this.taxSetupDao.getSubsidiaryTaxInfo(this.subId);
        sawtData.header = this.adapter.getConvertedHeaderInfo(headerInfo, taxInfo);

        // Details
        var dsParams = {
            periodfrom: this.periodId,
            periodto: this.periodId,
            subid: this.subId,
            nexusid: this.nexusId,
            fiscalCalendarId: this.fiscalCalendarId
        };
        var reportData = this.ds.getCustomerData(this.wtaxCodeIds, dsParams, this.searchType);
        reportData.setReturnPeriodValueForAllLineItems(this.periodDate, this.periodDate);
        var sawtLineItems = reportData.getSawtLineItems();
        sawtLineItems.sort(this.ds.SortFunction);
        sawtData.details = this.adapter.getConvertedLineItems(sawtLineItems, headerInfo);

        // Control
        var grandTotal = { 
            income: reportData.getSumTaxBase(), 
            taxWithheld: reportData.getSumTaxWithheld()
        };
        sawtData.control = this.adapter.getConvertedGrandTotal(grandTotal, headerInfo);
        
        // Set output
        var fileNameObj = this.adapter.getConvertedFileNameObject(headerInfo, this.formType);
        output.fileName = this.formatter.formatFileName(fileNameObj);
        output.contentType = this.formatter.getContentType();
        output.contents = this.formatter.format(sawtData);
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.SawtReport.Controller.Run', ex.message || ex.toString());
    }

    return output;
};
