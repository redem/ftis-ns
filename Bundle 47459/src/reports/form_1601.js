/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * The suitelet that will launch the RP EXPANDED withholding tax form 
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Mar 2012     ljamolod
 *
 */

if (!form1601) { var form1601 = {}; }

if (!_4601) { var _4601 = {}; }

var _App = new SFC.System.Application("c54f4fb9-a81f-40e0-bf8f-ae7d00f57073");

_App._DEPLOY_ID_PRINTING = "customdeploy_form_1601_print";
_App._DEPLOY_ID_ONLINE = "customdeploy_form_1601_online";

_App.BIR_LOGO_FILENAME = 'wi_tax_print_logo.gif';
_App.BLACK_PIXEL_FILENAME = 'wi_tax_black_pixel.gif';

form1601.REPORT_NOT_AVAILABLE_HEADER = 'No Report Available';
form1601.REPORT_NOT_AVAILABLE_MESSAGE = 'The withholding tax report for this country is not currently supported in NetSuite.';

function main( request, response ) {
	nlapiLogExecution("DEBUG", "Withholding_Tax_Form_16301.main","");
	var deployId, helperUtils,
	    creditableWTaxCodeIds;
		
	var _taxTypeId = request.getParameter('custpage_taxtype');
	creditableWTaxCodeIds = (_taxTypeId == null) ? {} : _4601.WithholdingTaxCodeFetcher().getTaxCodesFilteredByType(_taxTypeId);
	
	deployId = _App.Context.getDeploymentId();
	
	helperUtils = new _4601.HelperUtils(_App, request);
	switch( deployId ){
		case _App._DEPLOY_ID_ONLINE :
			new form1601.loadOnline(request, response, helperUtils, creditableWTaxCodeIds);
		break;
		
		case _App._DEPLOY_ID_PRINTING :
			new form1601.loadPdf(request, response, helperUtils, creditableWTaxCodeIds);
		break;
		
		default: 
			nlapiLogExecution('ERROR',
                'Invalid Deployment Id:',
				deployId);
            throw {
                name: 'Invalid deployment ID',
				message: 'System error: Form 1601 - Invalid deployment ID'};
	}
}



form1601.loadOnline = function loadOnline(request, response, helperUtils, creditableWTaxCodeIds) {
		var _Context = nlapiGetContext();
		var _request = request;
		
		var _FromPeriodId = null;
	    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
		var _subId = null;
		var _taxTypeId = null;
		var _nexusId = null;
		
		var countryCode = null;
		
		var box14 = 0;
		
		var is_post_back = _request.getMethod() == 'POST' ? true : false;
		var is_refresh =_request.getParameter('refresh');
		var currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod();	
		nlapiLogExecution("DEBUG", "Withholding_Tax_Form_16301:Current Period", currentPeriod.GetId());
		
		// process post data if available
		if( is_post_back || is_refresh == 'y'){
			_FromPeriodId = _request.getParameter('periodfrom');
			
			if (is_refresh != 'y')
				_taxTypeId = _request.getParameter('custpage_taxtype');
		} else {
            if (currentPeriod != null){
                _FromPeriodId = currentPeriod.GetId();
            }
		}
		
		if( _IsOneWorld ) {
			_subId = _request.getParameter('custpage_subsidiary');
		} else {
		    _nexusId = _request.getParameter('custpage_nexus');
		}
		
		var reportData = form1601.getData(creditableWTaxCodeIds, {periodfrom:_FromPeriodId, subid:_subId, nexusid: _nexusId}  );
		
		var pageData = { box14:0, box15a:0, box15b:0, box15c:0, box16:0, box17d:0, box18:0,  box17a:0, box17b:0, box17c:0};
		var rowTemplate = _App.GetFileContent("wi_tax_form_1601_template_online_row.htm", true);
		
		pageData.tcRows = '';
		pageData.box14 = 0 ;
		if ( reportData!=null && reportData.length > 0 ){
			var hasPrintableData = false;
			for( var r in reportData ){
				var rowData = {  };
				
				// filter out empty records
				if( reportData[r].sumWh != 0 ){
					hasPrintableData = true;	
					rowData.sumWh = _4601.formatCurrency(reportData[r].sumWh);
					rowData.rate = _4601.formatRate(reportData[r].rate);
					rowData.atc = reportData[r].code;
					rowData.base = _4601.formatCurrency(reportData[r].sumBase);
					rowData.desc = reportData[r].desc;
					
					pageData.tcRows += 	_App.RenderTemplate(rowTemplate, rowData);
					pageData.box14 += parseFloat(reportData[r].sumWh);
				}
			}
			box14 = pageData.box14;
		}
		
		pageData.box16 = _4601.formatCurrency(box14);
		pageData.box15a = _4601.formatCurrency(0);
		pageData.box15b = _4601.formatCurrency(0);
		pageData.box14 = _4601.formatCurrency(box14);
		pageData.box15c = _4601.formatCurrency(0);
		pageData.box17a = _4601.formatCurrency(0);
		pageData.box17b = _4601.formatCurrency(0);
		pageData.box17c = _4601.formatCurrency(0);
		pageData.box17d = _4601.formatCurrency(0);
		pageData.box18 = _4601.formatCurrency(box14);
		
		if (!hasPrintableData) {
			var rowData = {};
			rowData.sumWh = "";
			rowData.rate = "";
			rowData.atc = "";
			rowData.base = "";
			rowData.desc = "";
			pageData.tcRows = _App.RenderTemplate(rowTemplate, rowData);
		}

		// create the main form and attach client script
		var form = nlapiCreateForm('Withholding Tax Reports');
		form.setScript('customscript_form_1601_client');
		
		var box14Field = _App.CreateField(form, "box14", "text", "", false, box14, "hidden", "normal");
		
		pageData.PhBirLogoUrl = helperUtils.getImageFullUrl(_App.BIR_LOGO_FILENAME);
		
		pageData.arabicfont = helperUtils.getFullFileUrl('koodak.ttf');
		
		// render template
		var div = ["<div id=\"RP4119-1601\">"];
		div.push("<br>");
        div.push(_App.RenderTemplate(_App.GetFileContent("wi_tax_form_1601_template_main.htm", true), pageData ));
        div.push("</div>");
		
		var template = div.join("");
		
		// the print button
		var htmPrintButton = _App.Buttonise("btnprint", "Print", "OnPrint();", true);
		var printUrl = nlapiResolveURL("SUITELET", _App.Context.getScriptId(), _App._DEPLOY_ID_PRINTING);
		
		var btnPrint = form.addButton('printButt', 'Print','form_1601_printForm()');
		
		var refreshUrl = nlapiResolveURL("SUITELET", _App.Context.getScriptId(), _App._DEPLOY_ID_ONLINE);
		_App.CreateField(form, "urlrefresh", "text", "", false, refreshUrl, "hidden", "normal");
		
		if (_IsOneWorld) {
			var subsComboboxConfig = new _4601.SubsidiaryComboboxConfiguration(
                _4601.getSubsidiaryIdsThatWTaxAppliesTo(),
				'custpage_subsidiary',
				'Subsidiary',
				_subId,
				'outsideabove');
			new _4601.FilteredSubsidiaryCombobox(form, subsComboboxConfig);
			
			countryCode = SFC.System.getSubsidiaryCountry(_subId);
		} else {
			var nexusComboBoxParam = {
					fieldName: 'custpage_nexus', 
					fieldLabel: 'Nexus',
					idSelectedDefault: _nexusId, 
					fieldLayoutType: 'outsideabove',
					fieldBreakType: 'startrow'
			};
			
			var objNexusCombo = new _4601.NexusCombobox(form, nexusComboBoxParam);
			objNexusCombo.setMandatory(true);
			
			countryCode = _nexusId;
		}
		
		_reportId = 'form1601e';
		var relatedReport = {country: countryCode};
		var isReportAvailable = new SFC.System.ReportFormCombobox(form, 'custpage_countryreport', 'Reports', _reportId, 'outsideabove', null, relatedReport);
		
		new SFC.System.TaxPeriodMonthCombobox(form, "periodfrom", "Tax Period", _FromPeriodId, "outsideabove");
		
		new _4601.TaxTypeCombobox(form, {
			fieldName: 'custpage_taxtype', 
			label: 'Tax Type', 
			defaultId: _taxTypeId, 
			layoutType: 'outsideabove', 
			subId: _IsOneWorld ? _subId : '',
	    	nexusId: _IsOneWorld ? '' : _nexusId}
		);
		
		// add submit button
		var btnRefresh = form.addSubmitButton('Refresh');
		
		if (isReportAvailable.getSelectOptions().length > 0) {
			// add the template as inline text to the form
			_App.CreateField(form, "reporttemplate", "inlinehtml", "", false, template, "normal");
			_App.CreateField(form, "urlprint", "text", "", false, printUrl, "hidden", "normal");
		} else {
			//Disable buttons
			btnRefresh.setDisabled(true);
			btnPrint.setDisabled(true);
			
            var errorMessage = 
                ["<br><div id='div__errormessage'/><script>showAlertBox('div__errormessage', '", 
                 form1601.REPORT_NOT_AVAILABLE_HEADER, "','",
                 form1601.REPORT_NOT_AVAILABLE_MESSAGE,
                 "', NLAlertDialog.TYPE_LOW_PRIORITY, '825px', '', '', false);</script>"].join("");               
            _App.CreateField(form, "custpage_errormessage", "inlinehtml", "", false, errorMessage);
		}
		
		// write response
		response.writePage(form);
	
};


form1601.loadPdf = (function () {
	function loadPdf(request, response, helperUtils, creditableWTaxCodeIds){
	    loadPdf._request = request;
	    loadPdf._Context = nlapiGetContext();
	    loadPdf._IsOneWorld = loadPdf._Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
		loadPdf._FromPeriodId = loadPdf._request.getParameter('periodFrom');
		loadPdf._subId = loadPdf._IsOneWorld ? loadPdf._request.getParameter('custpage_subsidiary') : null;
		loadPdf._nexusId = loadPdf._request.getParameter('custpage_nexus') || null;
	    
	    loadPdf.run(helperUtils, creditableWTaxCodeIds, response);
	}
	
    //ego: Deviation from 2307 implementation.  Instead of returning array, will return a string and 
    //will use SFC.System.Application.stringToArray to break it into array bits.
	loadPdf.getPrintPeriod = function getPrintPeriod( dateObject ){
		var strDate = "";
		
		if (dateObject) {
			var iMonth = parseInt(dateObject.getMonth()) + parseInt(1);
			if( iMonth < 10 ){
				strDate = '0' + iMonth.toString();
			} else {
				strDate = iMonth.toString();
			}
			
			var iYear = dateObject.getFullYear();
			strDate += iYear.toString();
		}
		
		return strDate;
	};
    
	loadPdf.stripNumber = function stripNumber( num ) {
    	var newNum = "";
    	
    	if (num) {
    		newNum = num.replace(/-/g, "");
    		newNum = newNum.replace(/\(/g, "");
    		newNum = newNum.replace(/\)/g, "");
    		newNum = newNum.replace(/\s/g, "");
    		newNum = newNum.replace(/\+/g, "");
    	} else {
    		newNum = num;
    	}
    	
		return newNum;
	};
	
	
	loadPdf.run = function run(helperUtils, creditableWTaxCodeIds, response){
		loadPdf._FromPeriodId = loadPdf._request.getParameter('periodFrom');
		
		if (!loadPdf._FromPeriodId) {
			nlapiLogExecution("DEBUG", "Form1601.Print: Run: No Period Id Specified", loadPdf._FromPeriodId);
			return;
		}
		
		var pageData = {};
		var headerData = {};
		
		//Populate Header Data
		
		var taxPeriod = nlapiLoadRecord("taxperiod", loadPdf._FromPeriodId);
		if( taxPeriod != null ){
			var datStartDate = nlapiStringToDate(taxPeriod.getFieldValue("startdate"));
			var strPeriodDisplay = loadPdf.getPrintPeriod(datStartDate);
			headerData.monthDisplay = strPeriodDisplay;
		} else {
			headerData.monthDisplay = "";
		}
		
		var company_record = _4601.getCompanyRecord(loadPdf._IsOneWorld, loadPdf._request.getParameter("custpage_subsidiary"));
		
		headerData.zipCode = company_record.getFieldValue("zip");
		headerData.telephone = loadPdf.stripNumber(company_record.getFieldValue("addrphone") || company_record.getFieldValue("phone"));
		headerData.tin = loadPdf.stripNumber(company_record.getFieldValue("federalidnumber") || company_record.getFieldValue("employerid"));
		headerData.whAgentName = nlapiEscapeXML(company_record.getFieldValue("legalname") || company_record.getFieldValue("name"));
		
		var comp_addr = company_record.getFieldValue("addr1") || company_record.getFieldValue("address1");
		var comp_addr2 = company_record.getFieldValue("addr2") || company_record.getFieldValue("address2");
		var comp_city = company_record.getFieldValue("city");
		var comp_state = company_record.getFieldText("dropdownstate") || company_record.getFieldValue("state");
				
		headerData.whAddress = [
            comp_addr ? comp_addr + " " : "",
            comp_addr2 ? comp_addr2 + " " : "",
            comp_city ? comp_city + " " : "",
            comp_state ? comp_state : ""
        ].join("");
		
		//Log contents of header data
		nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601:Period", headerData.monthDisplay);
		nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601:Tin", headerData.tin);
		nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601:Telephone", headerData.telephone);

		//Append Broken Down Attributes
		//Tax Period Display
		var arrPeriodDisplay = _App.stringToCharArray(headerData.monthDisplay);
		if (arrPeriodDisplay) {
			nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601:Size of period array (should be 6)", arrPeriodDisplay.length);
		}
		_App.appendAttributeArrayToContainer(headerData, "monthDisplay", arrPeriodDisplay, 6);
		
		//TIN
		var arrTin = _App.stringToCharArray(headerData.tin);
		if(arrTin) {
			nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601:Size of tin array (should be 12)", arrTin.length);
		}
		_App.appendAttributeArrayToContainer(headerData, "tin", arrTin, 12);
		
		//Zip Code
		var arrZipCode = _App.stringToCharArray(headerData.zipCode);
		if (arrZipCode) {
			nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601:Size of zip code array (should be 4)", arrZipCode.length);
		}
		_App.appendAttributeArrayToContainer(headerData, "zipCode", arrZipCode, 4);
		
		//Telephone Number
		//Concatenate 
		var arrTelephone;
		if (headerData.telephone && headerData.telephone.length > 7) {
			var completeTelepone = headerData.telephone;  
			arrTelephone = _App.stringToCharArray(completeTelepone.substr(completeTelepone.length-7, completeTelepone.length));
		} else if (headerData.telephone && headerData.telephone.length <= 7) {
			arrTelephone = _App.stringToCharArray(headerData.telephone);
		}
		if (arrTelephone) {
			nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601:Size of telephone array (should be 7)", arrTelephone.length);
		}
		
		_App.appendAttributeArrayToContainer(headerData, "telephone", arrTelephone, 7);
		
		pageData.box17a = helperUtils.getNumericRequestParameter('box17a', 0.0);
		pageData.box17b = helperUtils.getNumericRequestParameter('box17b', 0.0);
		pageData.box17c = helperUtils.getNumericRequestParameter('box17c', 0.0);
		
		pageData.box17d = parseFloat(pageData.box17a) + parseFloat(pageData.box17b) + parseFloat(pageData.box17c); 
		
		pageData.box15a = helperUtils.getNumericRequestParameter('box15a', 0.0); 
		pageData.box15b = helperUtils.getNumericRequestParameter('box15b', 0.0);
		
		pageData.box15c = parseFloat(pageData.box15a) + parseFloat(pageData.box15b);
		
		var reportData = form1601.getData(creditableWTaxCodeIds, {periodfrom:loadPdf._FromPeriodId, subid:loadPdf._subId, nexusid: loadPdf._nexusId}  );

        headerData.PhBirLogoUrl = nlapiEscapeXML(
            helperUtils.getImageFullUrl(_App.BIR_LOGO_FILENAME));
		nlapiLogExecution('DEBUG', 'headerData.PhBirLogoUrl',headerData.PhBirLogoUrl);

		pageData.header_template = _App.RenderTemplate(
            _App.GetFileContent("wi_tax_form_1601_template_print_head.htm", true), 
			headerData );

        pageData.separatedBoxBGImageUrl = nlapiEscapeXML(
            helperUtils.getImageFullUrl(_App.BLACK_PIXEL_FILENAME));
        nlapiLogExecution('DEBUG', 'pageData.separatedBoxBGImageUrl',pageData.separatedBoxBGImageUrl);


        var row_template = _App.GetFileContent("wi_tax_form_1601_template_print_row.htm", true);
		pageData.rows = '';
		
		pageData.box14 =  0 ;
		
		if( reportData.length > 0 ){
			var hasPrintableData = false;
			for( var r in reportData ){
				if (reportData[r].sumWh != 0) {
					hasPrintableData = true;
					var rowData = {};
					
					nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601: Formatting - description", reportData[r].desc);
					nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601: Formatting - atc", reportData[r].atc);
					
					rowData.sumWh = _4601.formatCurrency(reportData[r].sumWh);
					rowData.rate = _4601.formatRate(reportData[r].rate);
					rowData.atc = nlapiEscapeXML(reportData[r].code);
					rowData.base = _4601.formatCurrency(reportData[r].sumBase);
					rowData.desc = nlapiEscapeXML(reportData[r].desc);
					
					pageData.rows += _App.RenderTemplate(row_template, rowData);
					
					pageData.box14 += parseFloat(reportData[r].sumWh);
					
				}
			}
			
			if (!hasPrintableData) {
				var rowData = {};
				rowData.sumWh = "&nbsp;";
				rowData.rate = "&nbsp;";
				rowData.atc = "&nbsp;";
				rowData.base = "&nbsp;";
				rowData.desc = "&nbsp;";
				pageData.rows = _App.RenderTemplate(row_template, rowData);	
			}
		} 
		
		pageData.box16 = parseFloat(pageData.box14) - parseFloat(pageData.box15c) ;
		pageData.box18 = parseFloat(pageData.box16) + parseFloat(pageData.box17d);
		
		// post formatting
		pageData.box14 = _4601.formatCurrency( pageData.box14 );
		pageData.box15a = _4601.formatCurrency(pageData.box15a);
		pageData.box15b = _4601.formatCurrency(pageData.box15b);
		pageData.box15c = _4601.formatCurrency(pageData.box15c);
		pageData.box16 = _4601.formatCurrency(pageData.box16);
		pageData.box17a = _4601.formatCurrency(pageData.box17a);
		pageData.box17b = _4601.formatCurrency(pageData.box17b);
		pageData.box17c = _4601.formatCurrency(pageData.box17c);
		pageData.box17d = _4601.formatCurrency(pageData.box17d);
		pageData.box18 = _4601.formatCurrency(pageData.box18);
		
		pageData.arabicfont = nlapiEscapeXML(helperUtils.getFullFileUrl("koodak.ttf"));
		
		// generate the adjustment rows
        try {
        	pageData.adjustments = _App.RenderTemplate(_App.GetFileContent("wi_tax_form_1601_template_print_adjustments_print.htm", true), pageData );

        	var template = _App.GetFileContent("wi_tax_form_1601_print.xml", true);

        	var content = _App.RenderTemplate(template, pageData );

		
			var pdfFile = nlapiXMLToPDF('<?xml version="1.0" encoding="ISO-8859-1"?>\n<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);
		} catch (e) {
	        var arrExceptionMsg = [];
	        if (e instanceof nlobjError) {
	            arrExceptionMsg.push(getCode());
	            arrExceptionMsg.push(':\n');
	            arrExceptionMsg.push(e.getDetails());
	        }
	        else {
	            arrExceptionMsg.push(e.name);
	            arrExceptionMsg.push(':\n');
	            arrExceptionMsg.push(e.message);
	        }
			
			response.write(content);
	        nlapiLogExecution('DEBUG', 'error:methodName', arrExceptionMsg.join(''));
			return;
	    }
        response.setContentType("PDF");
        response.write(pdfFile.getValue());
	};
	
	return loadPdf;
}());
