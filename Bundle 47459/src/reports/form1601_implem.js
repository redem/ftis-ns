/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Mar 2012     ljamolod
 *
 */

if (!form1601) { var form1601 = {}; }

if (!_4601) { var _4601 = {}; }

form1601.getData = (function () {
    var _MAX_NUM_SEARCH_RESULTS = 1000;
    var _phImplem = 'ph';
    var _genericImplem = 'generic';
    var _labelIsTaxGroup = 'isTaxGroup';
    var _labelGroupTaxCodes = 'groupTaxCodes';
    var _labelTransID = 'transactionID';
    var _labelTaxCodeRate = 'taxCodeRate';
    var _labelRecordType = 'recordtype';
    
    //Defined retrieval fields
    var objBaseAmount  = "custcol_4601_witaxbaseamount";
    var objTaxAmount =  "custcol_4601_witaxamount";
    var objTaxCode = "custcol_4601_witaxcode";
    
    var groupWtaxCodes = [];
    var allValidGroupedWtaxCodes = [];
        
    function getData(allowedWithholdingTaxCodeIds, params){

        getData.allowedWithholdingTaxCodeIds = allowedWithholdingTaxCodeIds;
        getData.params = params;
        getData.reportData = {};
        getData.arrResult;
        getData.taxAccrual = getData.getTaxCodes();
        getData._Context = nlapiGetContext();
        getData._IsOneWorld = getData._Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
        getData.implem = null;
        
        if (allowedWithholdingTaxCodeIds.length > 0) {
            getData.billCreditMap = [];
            
            var purcTaxPoint = _4601.getPurchaseTaxPoint(getData.params.subid, getData.params.nexusid);
            
            groupWtaxCodes = _4601.getTaxGroupWithValidTaxCodes(allowedWithholdingTaxCodeIds);
            
            allValidGroupedWtaxCodes = _4601.getAllValidGroupedTaxCodes(allowedWithholdingTaxCodeIds);

            if (purcTaxPoint != 'onpayment') {
                getData.implem = _phImplem;
                getData.arrResult = getData.getSearchResults(purcTaxPoint);
                getData.taxAccrual = getData.accrualComputation(getData.taxAccrual, getData.arrResult, purcTaxPoint);
            }
            
            getData.implem = _genericImplem;
            getData.arrResult = getData.getSearchResults(purcTaxPoint);
            getData.taxAccrual = getData.accrualComputation(getData.taxAccrual, getData.arrResult, purcTaxPoint);
            
            getData.reportData = getData.taxAccrual;
        }
        
        return getData.reportData; 
    }
    
    getData.getPaymentRatios = function getPaymentRatios(arrResult, purcTaxPoint, objCols) {
        var pymntIndex = 0;
        var totalAmountMap = [];
        
        // Get total amounts (base and withheld) to be used in computing ratios
        if (purcTaxPoint == 'onpayment') {
            var arrTransID = [];

            do {
                 var sr = arrResult.getResults(pymntIndex, pymntIndex + _MAX_NUM_SEARCH_RESULTS);
                 
                 if (sr) {
                     for (var i = 0; i < sr.length; i++) {
                        transID = sr[i].getValue(objCols[_labelTransID]);
                        if (transID && getData.billCreditMap[transID]) {
                            arrTransID.push(transID);
                        }
                     }
                 }
                 
                 pymntIndex += sr.length;
            } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);
            
            if (arrTransID.length > 0)
                 totalAmountMap = _4601.populateTotalAmountMap(arrTransID);
        }
        
        return totalAmountMap;
    };
    
    getData.computeTaxAccruals = function computeTaxAccruals(arrResult,objCols,totalAmountMap,taxAccrual,purcTaxPoint) {
        var objAccrual = [];
        var index = 0;
        var transID = '';
        var pymntRef = '';
        
        do {
            var sr = arrResult.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
            
            if (sr) {
                for (var i = 0; i < sr.length; i++) {
                    transID = sr[i].getValue(objCols[_labelTransID]);
                    
                    if (purcTaxPoint == 'onpayment' && getData.billCreditMap[transID] != null) {
                        var billPayments = getData.billCreditMap[transID];
                        
                        for (var h in billPayments) {
                            var payment = billPayments[h];
                            pymntRef = payment.pymntRef;
                            objAccrual = getData.calculateAmountTransaction(sr, i, objCols, purcTaxPoint, totalAmountMap, objAccrual, transID, pymntRef);
                        }
                    } else {
                        objAccrual = getData.calculateAmountTransaction(sr, i, objCols, purcTaxPoint, totalAmountMap, objAccrual, transID, pymntRef);
                    }
                }
            }
            
            index += sr.length;
       } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);

        for( var k in taxAccrual ) {
            if (objAccrual[taxAccrual[k].id]) {
                isNaN(parseFloat(objAccrual[taxAccrual[k].id].taxAmount));
                taxAccrual[k].sumWh += getData.getAbsoluteValueIfFloat(objAccrual[taxAccrual[k].id].taxAmount); 
                taxAccrual[k].sumBase += getData.getAbsoluteValueIfFloat(objAccrual[taxAccrual[k].id].baseAmount);
            }
        }
        
        return taxAccrual;
    };
    
    
    
    getData.accrualComputation = function accrualComputation(taxAccrual, arrResult, purcTaxPoint) {
        var totalAmountMap = [];
        var objCols = [];
        
        if (arrResult) {
            objCols = arrResult.getResults(0,1).length > 0 ? getData.getColumnSearchObjects(arrResult.getResults(0,1)[0].getAllColumns()) : [];
            totalAmountMap = getData.getPaymentRatios(arrResult, purcTaxPoint, objCols);
            taxAccrual = getData.computeTaxAccruals(arrResult,objCols,totalAmountMap,taxAccrual,purcTaxPoint);
        } else {
            nlapiLogExecution("DEBUG", "Withholding_Tax_Form_1601: Search Executed - No Results", "");
        }
        
        return taxAccrual;
    };
    
    
    getData.calculateAmountTransaction = function calculateAmountTransaction(arrResult, i, objCols, purcTaxPoint, totalAmountMap, objAccrual, transID, pymntRef) {
        var taxCode = '';
        var isTaxGroup = '';
        var groupTaxCodes = null;
        var taxBase = 0.0;
        var taxAmount = 0.0;
        var taxRate = '';
        var taxRateComp = 0.0;
        var recordType = '';
        var implementation = getData.implem;
        
        taxBase = arrResult[i].getValue(objCols[objBaseAmount]);
        taxAmount = arrResult[i].getValue(objCols[objTaxAmount]);
        taxRate = arrResult[i].getValue(objCols[_labelTaxCodeRate]);
        taxRateComp = parseFloat(taxRate.replace('%', ''));
        recordType = arrResult[i].getValue(objCols[_labelRecordType]);

        if (!(recordType == 'check' && getData.isPaymentVoided(transID))) {
            isTaxGroup = arrResult[i].getValue(objCols[_labelIsTaxGroup]);
            groupTaxCodes = arrResult[i].getValue(objCols[_labelGroupTaxCodes]);
            if (isTaxGroup == 'T') {
                var taxRateTotalComp = 0.0;
                var taxAmountTotal = 0.0;
                var arrTaxCodes = groupTaxCodes.split(',');
                var arrGroupTaxCodes = _4601.getTaxCodesByTaxGroup(arrTaxCodes, allValidGroupedWtaxCodes);
                var taxBasis = 0.0;
                
                if (purcTaxPoint == 'onpayment' && transID != null && getData.billCreditMap[transID] != null)
                    taxAmountTotal = getData.getLineAmountDistribution(taxBase, taxAmount, transID, totalAmountMap, pymntRef);
                else
                    taxAmountTotal = taxAmount;
                
                taxRateTotalComp = taxRateComp;
                for (var j in arrGroupTaxCodes) {
                    taxCode = arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_code');
                    taxBasis = parseFloat(arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_basis'));
                    taxRate = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_rate', 'custrecord_4601_gwtc_code');
                    taxRateComp = parseFloat(taxRate.replace('%', ''));
                    
                    if (getData.allowedWithholdingTaxCodeIds.indexOf(taxCode) > -1) {
                        if (purcTaxPoint == 'onaccrual')
                            taxAmount = -(taxBase * ((taxRateComp * (taxBasis/100)))/100);
                        else if (purcTaxPoint == 'onpayment') {
                            var taxRateRatio = taxRateTotalComp == 0 ? 0 : taxRateComp / taxRateTotalComp;
                            taxAmount = taxRateRatio * taxAmountTotal;
                            taxBase = -(taxRateTotalComp == 0 ? 0 : taxAmount / (taxRateComp/100));
                        }
                        
                        objAccrual = getData.populateLineItemMap(objAccrual, taxCode, taxBase, implementation == 'ph' ? taxAmount : -taxAmount);
                    }
                }
            } else {
                taxCode = arrResult[i].getValue(objCols[objTaxCode]);

                if (getData.allowedWithholdingTaxCodeIds.indexOf(taxCode) > -1) {
                    if (purcTaxPoint == 'onpayment' && transID != null && getData.billCreditMap[transID] != null) {
                        taxAmount = getData.getLineAmountDistribution(taxBase, taxAmount, transID, totalAmountMap, pymntRef);
                        taxBase = -(taxAmount / (taxRateComp/100));
                        
                    }
                    else if(purcTaxPoint == 'onaccrual' && recordType == 'vendorcredit'){
                        taxAmount *= -1;
                        taxBase *= -1;
                    }

                    objAccrual = getData.populateLineItemMap(objAccrual, taxCode, taxBase, implementation == 'ph' ? taxAmount : -taxAmount);
                }
            }
        }
        return objAccrual;
    };
    
    
    getData.isPaymentVoided = function isPaymentVoided(mainPaymentId){
         var filters = [];
         filters.push(new nlobjSearchFilter("voided", null, "is", "T"));
         filters.push(new nlobjSearchFilter("mainline", null, "is", "T"));
         filters.push(new nlobjSearchFilter("internalid", null, "is", mainPaymentId));
         return nlapiSearchRecord('transaction', null, filters);
     };
    
    
    getData.getLineAmountDistribution = function getLineAmountDistribution(taxBase, taxAmount, transID, totalAmountMap, pymntRef) {
        var creditWTaxPayment = 0.0;
        var lineCreditWTaxPayment = 0.0;
        
        amountRatio = getData.getAmountRatio(taxBase, taxAmount);
        creditWTaxPayment = getData.billCreditMap[transID][pymntRef].taxWithheld;
        
        var wtaxPaymentRatio = taxAmount / totalAmountMap[transID].totalTaxAmount;
        
        lineCreditWTaxPayment = creditWTaxPayment * wtaxPaymentRatio;
        
        return lineCreditWTaxPayment;
    };
    
    
    getData.getAmountRatio = function getAmountRatio(taxBase, taxAmount) {
        return (taxAmount / (taxBase - taxAmount));
    };
    
    
    getData.populateLineItemMap = function populateLineItemMap(objAccrual, taxCode, taxBase, taxAmount) {
        if (!objAccrual[taxCode]) {
            objAccrual[taxCode] = { "baseAmount":parseFloat(0.0), "taxAmount":parseFloat(0.0) };
        }
        objAccrual[taxCode].baseAmount += getData.getAbsoluteValueIfFloat(taxBase);
        objAccrual[taxCode].taxAmount += getData.getAbsoluteValueIfFloat(taxAmount);
        
        return objAccrual;
    };
    

    getData.getColumnSearchObjects = function getColumnSearchObjects(arrObjSearchCols) {
        var arrCols = {};
        for(var i in arrObjSearchCols) {
            if (arrObjSearchCols[i].getLabel() != null)
                arrCols[arrObjSearchCols[i].getLabel()] = arrObjSearchCols[i];
            else
                arrCols[arrObjSearchCols[i].getName()] = arrObjSearchCols[i];
        }
        return arrCols;
    };

    getData.getAbsoluteValueIfFloat = function getAbsoluteValueIfFloat(num) {
        return isNaN(parseFloat(num))?0:parseFloat(num);
    };
    
    
    getData.getSearchResults = function getSearchResults(purcTaxPoint) {
        var implementation = getData.implem;
        var arrTransID = [];
        var columns = [];
        var filters = [];
        
        //common columns
        columns.push(new nlobjSearchColumn( // wTaxCodeName
                'custcol_4601_witaxcode', //'custcol_ph4014_wtax_code', - deprecated
                '',
                'group'));
        columns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM') // tax base amount
                 .setFormula('{custcol_4601_witaxbaseamount}') //'custcol_ph4014_wtax_bamt', - deprecated
                .setLabel('custcol_4601_witaxbaseamount'));
        columns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM') // wtax amount
                 .setFormula('{custcol_4601_witaxamount}') //'custcol_ph4014_wtax_amt', - deprecated
                .setLabel('custcol_4601_witaxamount'));
        columns.push(new nlobjSearchColumn(
               'taxperiod',
               '',
               'group'));
        columns.push(new nlobjSearchColumn(
                'formulatext', 
                null, 
                'group').setFormula('{custcol_4601_witaxcode.custrecord_4601_wtc_istaxgroup}').setLabel(_labelIsTaxGroup));
        columns.push( new nlobjSearchColumn('formulatext', 
                null, 
                'group').setFormula('{custcol_4601_witaxcode.custrecord_4601_wtc_groupedwitaxcodes}').setLabel(_labelGroupTaxCodes));
        columns.push(new nlobjSearchColumn(
                'internalid', 
                null, 
                'group').setLabel(_labelTransID));
        columns.push(new nlobjSearchColumn(
                'formulapercent', 
                null, 
                'group').setFormula('{custcol_4601_witaxcode.custrecord_4601_wtc_rate}').setLabel(_labelTaxCodeRate));
        columns.push(new nlobjSearchColumn(
                   'recordtype',
                   '',
                   'group').setLabel(_labelRecordType));
        
        //common filters
        if( getData.params.subid != null && getData._IsOneWorld){
            filters.push(['subsidiary', 'is', getData.params.subid]);
            filters.push('AND');
        }
        
        //implementation-related columns and filters
        if (implementation == _phImplem) {
            var journalTypes = [];
            journalTypes.push("CardChrg");
            journalTypes.push("Check");
            journalTypes.push("VendBill");
            var journalTypeIds = _App.getJournalTypeIds(journalTypes);
            
            filters.push(['custcol_4601_witaxline', 'is', 'T']);
            filters.push('AND');
            filters.push(['type', 'is', 'Journal']);
            filters.push('AND');
            filters.push(['custcol_ph4014_src_jrnltrantypeid', 'anyof', journalTypeIds]);
            filters.push('AND');
            filters.push(['custbody_ph4014_wtax_reversal_flag', 'is', 'F']);
            filters.push('AND');
            filters.push(['custcol_ph4014_src_vendorid', 'noneof', '@NONE@']);
            filters.push('AND');
            filters.push(['custcol_4601_witaxcode', 'anyof', getData.allowedWithholdingTaxCodeIds]);
            filters.push('AND');
            filters.push(['taxperiod', 'is', getData.params.periodfrom]);
            filters = _4601.getJournalReversalFilters(filters);
        } else if (implementation == _genericImplem) {
            if (purcTaxPoint == 'onpayment') {
                var arrPaymentIds = getData.getPaymentRecordIds(getData.params, getData.allowedWithholdingTaxCodeIds);
                if (arrPaymentIds.length > 0)
                    filters.push(['internalid', 'anyof', arrPaymentIds]);
                else
                    //Since there are no payment records retrieved, set the internalid to NONE
                    //which in essence results to no record/s to be searched;
                    //otherwise it will seach records intended for onaccrual setup
                    filters.push(['internalid', 'is', '@NONE@']);
            } else if (purcTaxPoint == 'onaccrual') {
                arrTransID = getData.getTransactions(getData.params, getData.allowedWithholdingTaxCodeIds, 'NONGROUP', arrTransID);
                arrTransID = getData.getTransactions(getData.params, getData.allowedWithholdingTaxCodeIds, 'GROUP', arrTransID);
                
                if (arrTransID.length > 0)
                    filters.push(['internalid', 'anyof', arrTransID]);
                else
                    filters.push(['internalid', 'is', '@NONE@']);
            }
            
            filters.push('AND');
            filters.push(['custcol_4601_witaxapplies', 'is', 'T']);
            filters.push('AND');
            filters.push(['posting', 'is', 'T']);
            filters.push('AND');
            filters.push(['voided', 'is', 'F']);
        }
        
        var rsTransactions = nlapiCreateSearch('transaction', filters, columns);
        var arrResult = rsTransactions.runSearch();
        
        return arrResult;
    };
    
    
    getData.getTransactions = function getTransactions(params, allowedWithholdingTaxCodeIds, searchCriteria, arrTransID) {
        var filters = [];
        var columns = [];
        
        if (searchCriteria != 'GROUP' || (searchCriteria == 'GROUP' && groupWtaxCodes != null && groupWtaxCodes.length > 0)) {
            var arrTransTypes = [];
            arrTransTypes.push('Check');
            arrTransTypes.push('VendBill');
            arrTransTypes.push('VendCred');
            filters.push(new nlobjSearchFilter('type', null, 'anyof', arrTransTypes));
            
            if (searchCriteria == 'NONGROUP')
                filters.push(new nlobjSearchFilter(objTaxCode, null, 'anyof', allowedWithholdingTaxCodeIds));
            else
                filters.push(new nlobjSearchFilter(objTaxCode, null, 'anyof', groupWtaxCodes));
            filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.periodfrom));
            filters.push(new nlobjSearchFilter('internalid', 'vendor', 'noneof', '@NONE@')); //retrieve transactions with vendors only
            filters.push(new nlobjSearchFilter('custcol_4601_witaxapplies', null, 'is', 'T'));
            filters.push(new nlobjSearchFilter('isreversal',null,'is','F'));
            filters.push(new nlobjSearchFilter('custcol_4601_witaxbaseamount',null,'isnotempty',null));
            if (params.subid != null && getData._IsOneWorld)
                filters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.subid));
            
            var index = 0;
            var rsTransactions = nlapiCreateSearch('transaction', filters, columns);
            var resultSet = rsTransactions.runSearch();
            
            do {
                var sr = resultSet.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
                
                if (sr) {
                    for (var i = 0; i < sr.length; i++) {
                        arrTransID.push(sr[i].getId());
                    }
                }
                
                index += sr.length;
                
            } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);
        }
        return arrTransID;
    };
    
    
    getData.getPaymentRecordIds = function getPaymentRecordIds(params, allowedWithholdingTaxCodeIds) {
        var arrDocRefIds = [];
        
        arrDocRefIds = getData.getPaymentTransactions(params, allowedWithholdingTaxCodeIds, 'GROUP', arrDocRefIds);
        arrDocRefIds = getData.getPaymentTransactions(params, allowedWithholdingTaxCodeIds, 'NONGROUP', arrDocRefIds);
        
        return arrDocRefIds;
    };
    
    
    getData.getPaymentTransactions = function getPaymentTransactions(params, allowedWithholdingTaxCodeIds, searchCriteria, arrDocRefIds) {
        if (searchCriteria != 'GROUP' || (searchCriteria == 'GROUP' && groupWtaxCodes != null && groupWtaxCodes.length > 0)) {
            // Get Bill Credit
            var creditFilters = [];
            creditFilters.push(new nlobjSearchFilter('type', null, 'is', 'VendCred'));
            creditFilters.push(new nlobjSearchFilter('custbody_4601_pymnt_ref_id', null, 'noneof', '@NONE@'));
            creditFilters.push(new nlobjSearchFilter('custbody_4601_doc_ref_id', null, 'noneof', '@NONE@'));
            if (searchCriteria == 'NONGROUP')
                creditFilters.push(new nlobjSearchFilter(objTaxCode, 'custbody_4601_doc_ref_id', 'anyof', allowedWithholdingTaxCodeIds));
            else
                creditFilters.push(new nlobjSearchFilter(objTaxCode, 'custbody_4601_doc_ref_id', 'anyof', groupWtaxCodes));
            creditFilters.push(new nlobjSearchFilter('taxperiod', null, 'is', getData.params.periodfrom));
            creditFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
            
            var creditColumns = [];
            creditColumns.push(new nlobjSearchColumn('custbody_4601_doc_ref_id'));
            creditColumns.push(new nlobjSearchColumn('custbody_4601_pymnt_ref_id'));
            creditColumns.push(new nlobjSearchColumn('amount'));
            
            var rsCredit = nlapiSearchRecord('transaction', null, creditFilters, creditColumns);
            
            var _docRef = '';
            var _pymntRef = '';
            var _amount = 0.0;
            for (var i in rsCredit) {
                _docRef = rsCredit[i].getValue('custbody_4601_doc_ref_id');
                _pymntRef = rsCredit[i].getValue('custbody_4601_pymnt_ref_id');
                _amount = rsCredit[i].getValue('amount');
                if (_amount != 0) {
                    if (getData.billCreditMap[_docRef] == null) {
                        arrDocRefIds.push(_docRef);
                        getData.billCreditMap[_docRef] = {};
                    }
                    getData.billCreditMap[_docRef][_pymntRef] = {}; 
                    getData.billCreditMap[_docRef][_pymntRef].pymntRef = _pymntRef;
                    getData.billCreditMap[_docRef][_pymntRef].taxWithheld = _amount;
                }
            }
            
            var purcTypes = [];
            purcTypes.push('Check');
            
            var paymentFilters = [];
            if (params.subid != null && getData._IsOneWorld)
                paymentFilters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.subid));
            paymentFilters.push(new nlobjSearchFilter('type', null, 'anyof', purcTypes));
            paymentFilters.push(new nlobjSearchFilter('taxperiod', null, 'is', getData.params.periodfrom));
            paymentFilters.push(new nlobjSearchFilter('internalid', 'vendor', 'noneof', '@NONE@')); //retrieve transactions with vendors only
            paymentFilters.push(new nlobjSearchFilter('custcol_ph4014_src_jrnltrantypeid', '', 'anyof','@NONE@')); //filter out old records
            paymentFilters.push(new nlobjSearchFilter('custcol_4601_witaxapplies', null, 'is', 'T'));
            paymentFilters.push(new nlobjSearchFilter('isreversal',null,'is','F'));
            if (searchCriteria == 'NONGROUP')
                paymentFilters.push(new nlobjSearchFilter(objTaxCode, null, 'anyof', allowedWithholdingTaxCodeIds));
            else
                paymentFilters.push(new nlobjSearchFilter(objTaxCode, null, 'anyof', groupWtaxCodes));
            
            var paymentColumns = [];
                            
            var rsPayments = nlapiSearchRecord('transaction', null, paymentFilters, paymentColumns);
            
            for (var i in rsPayments)
                arrDocRefIds.push(rsPayments[i].getId());
        }
        
        return arrDocRefIds;
    };
    
    
    /**
     * Creates an array of accrual fields for each tax code
     */
    getData.getTaxCodes = function getTaxCodes() {
        var phTaxCodes = [];
    
        var searchColumns = [ new nlobjSearchColumn('custrecord_4601_wtc_name'), //custrecord_ph4014_wtax_code_name - deprecated 
                              new nlobjSearchColumn('custrecord_4601_wtc_description'), //custrecord_ph4014_wtax_code_desc - deprecated 
                              new nlobjSearchColumn('custrecord_4601_wtc_rate') ]; // custrecord_ph4014_wtax_code_rate - deprecated
        var tcsearch = nlapiSearchRecord( 'customrecord_4601_witaxcode', null, null, searchColumns ); //customrecord_ph4014_wtax_code - deprecated

        if( tcsearch !=null && tcsearch.length  > 0 ){
            for( var i in tcsearch ){
                phTaxCodes.push( { id: tcsearch[i].getId(), code: tcsearch[i].getValue( searchColumns[0] ), desc: tcsearch[i].getValue( searchColumns[1] ), rate:tcsearch[i].getValue( searchColumns[2] ), sumWh:0, sumBase:0 } );
            }
        }
        return phTaxCodes;
    };

    return getData;
    
}());