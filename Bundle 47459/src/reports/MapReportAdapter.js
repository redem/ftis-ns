/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.MapReport = WTax.MapReport || {};

WTax.MapReport.Header = function _HeaderInfo() {
	return {
		alphaType: '',
		formTypeCode: '',
		wtaxAgentTIN: '',
		wtaxAgentBranchCode: '',
		wtaxAgentRegName: '',
		returnPeriod: '',
		wtaxAgentRDOCode: ''
	};
};

WTax.MapReport.Details = function _DetailsInfo() {
	return {
		alphaType: '',
		formTypeCode: '',
		sequenceNumber: '',
		payeeTIN: '',
		payeeBranchCode: '',
		payeeRegName: '',
		payeeLastName: '',
		payeeFirstName: '',
		payeeMiddleName: '',
		returnPeriod: '',
		atcCode: '',
		natureOfIncomePayment: '',
		taxRate: '',
		taxBase: '',
		taxWithheld: ''
	};
};

WTax.MapReport.Control = function _ControlInfo() {
	return {
		alphaType: '',
		formTypeCode: '',
		wtaxAgentTIN: '',
		wtaxAgentBranchCode: '',
		returnPeriod: '',
		totalTaxBase: '',
		totalTaxWithheld: ''
	};
};
WTax.MapReport.FileName = function _FileNameInfo() {	return {        wtaxAgentTIN: '',        wtaxAgentBranchCode: '',        returnPeriodFileName: '',        formTypeCode: ''    };};
function _Adapter(formTypeCode) {
	this.formTypeCode = formTypeCode;	this.ALPHA_TYPE = {		    HEADER: 'HMAP',		    DETAILS: 'DMAP',		    CONTROL: 'CMAP'		};	this.FORM_TYPE_PREFIX = {	        HEADER: 'H',	        DETAILS: 'D',	        CONTROL: 'C'		};
};

WTax.MapReport.Adapter = _Adapter;
WTax.MapReport.Adapter.prototype = new WTax.ReportAdapter();
WTax.MapReport.Adapter.prototype.constructor = _Adapter; 

WTax.MapReport.Adapter.prototype.getConvertedHeaderInfo = function _getConvertedHeaderInfo(headerInfo, rdoCode) {
	var convertedHeaderInfo = new WTax.MapReport.Header();
	convertedHeaderInfo.alphaType = this.ALPHA_TYPE.HEADER;
	convertedHeaderInfo.formTypeCode = this.FORM_TYPE_PREFIX.HEADER + this.formTypeCode;
	convertedHeaderInfo.wtaxAgentTIN = this.getTaxIdNum(headerInfo.RawTIN);
	convertedHeaderInfo.wtaxAgentBranchCode = this.getBranchCode(headerInfo.RawTIN);
	convertedHeaderInfo.wtaxAgentRegName = headerInfo.LegalName || headerInfo.Name;
	convertedHeaderInfo.returnPeriod = headerInfo.EndDate;
	convertedHeaderInfo.wtaxAgentRDOCode = rdoCode;
	return convertedHeaderInfo;
};

WTax.MapReport.Adapter.prototype.getConvertedGrandTotal = function _getConvertedGrandTotal(grandTotal, headerInfo) {
	var convertedGrandTotal = new WTax.MapReport.Control();
	convertedGrandTotal.alphaType = this.ALPHA_TYPE.CONTROL ; 
	convertedGrandTotal.formTypeCode = this.FORM_TYPE_PREFIX.CONTROL + this.formTypeCode;
	convertedGrandTotal.wtaxAgentTIN = this.getTaxIdNum(headerInfo.RawTIN);
	convertedGrandTotal.wtaxAgentBranchCode = this.getBranchCode(headerInfo.RawTIN);
	convertedGrandTotal.returnPeriod = headerInfo.EndDate;
	convertedGrandTotal.totalTaxBase = grandTotal.Income;
	convertedGrandTotal.totalTaxWithheld = grandTotal.TaxWithheld;
	return convertedGrandTotal;
};

WTax.MapReport.Adapter.prototype.convertLine = function _convertLine(mapLine, headerInfo, sequenceNumber) {
	var convertedMapLine = new WTax.MapReport.Details();
	convertedMapLine.alphaType = this.ALPHA_TYPE.DETAILS;
	convertedMapLine.formTypeCode = this.FORM_TYPE_PREFIX.DETAILS + this.formTypeCode;
	convertedMapLine.sequenceNumber = sequenceNumber;
	convertedMapLine.payeeTIN = this.getTaxIdNum(mapLine.tin_raw); 
	convertedMapLine.payeeBranchCode = this.getBranchCode(mapLine.tin_raw);
	convertedMapLine.payeeRegName = mapLine.reg_name;
	convertedMapLine.payeeLastName = mapLine.lastname;
	convertedMapLine.payeeFirstName = mapLine.firstname;
	convertedMapLine.payeeMiddleName = mapLine.middlename;
	convertedMapLine.returnPeriod = headerInfo.EndDate;
	convertedMapLine.atcCode = mapLine.atc;
	//convertedMapLine.natureOfIncomePayment = mapLine.desc;
	convertedMapLine.taxRate = mapLine.tax_rate;
	convertedMapLine.taxBase = mapLine.tax_base;
	convertedMapLine.taxWithheld = mapLine.tax_amount;
	return convertedMapLine;
};WTax.MapReport.Adapter.prototype.getConvertedFileNameObject = function _getConvertedFileNameObject(headerInfo, formTypeCode) {    var fileNameObj = new WTax.MapReport.FileName();    fileNameObj.wtaxAgentTIN = this.getTaxIdNum(headerInfo.RawTIN);    fileNameObj.wtaxAgentBranchCode = this.getBranchCode(headerInfo.RawTIN);    fileNameObj.returnPeriodFileName = headerInfo.EndDate;    fileNameObj.formTypeCode = this.formTypeCode;    return fileNameObj;};

