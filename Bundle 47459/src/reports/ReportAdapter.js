/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};

WTax.ReportAdapter = function _adapter(formTypeCode) {
    this.formTypeCode = formTypeCode;
};

WTax.ReportAdapter.prototype.getConvertedHeaderInfo = function _getConvertedHeaderInfo(headerInfo, subsidiaryId) {
    return headerInfo;
};

WTax.ReportAdapter.prototype.getConvertedLineItems = function _getConvertedLineItems(LineItems, headerInfo) {
    var convertedLineItems = [];
    
    for (var i = 0; i < LineItems.length; i++) {
        
        var convertedLine = this.convertLine(LineItems[i], headerInfo, i + 1);
        convertedLineItems.push(convertedLine);
    }
    return convertedLineItems;
};

WTax.ReportAdapter.prototype.getConvertedGrandTotal = function _getConvertedGrandTotal(grandTotal, headerInfo) {
    return grandTotal;
};

WTax.ReportAdapter.prototype.convertLine = function _convertLine(mapLine, headerInfo, sequenceNumber) {
    return mapLine;
};

WTax.ReportAdapter.prototype.getTaxIdNum = function _getTaxIdNum(vatRegNo) {
    var taxIdNum = '';
    if (vatRegNo) {
        taxIdNum = vatRegNo.slice(0, 9);
    }
    return taxIdNum;
};


WTax.ReportAdapter.prototype.getBranchCode = function _getBranchCode(vatRegNo) {
    var branchCode = '';
    if (vatRegNo) {
        branchCode = vatRegNo.slice(9,13);
    }
    return branchCode;
};
