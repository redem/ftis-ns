/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * 
 * library dependencies:
 * 		wi_tax_reports_library.js
 * 		vendor_data_source.js
 * 
 * @author mmoya
 */
if (!MAP_REPORT) var MAP_REPORT = {};

if (!_4601) { var _4601 = {}; }

var _App = new SFC.System.Application("c54f4fb9-a81f-40e0-bf8f-ae7d00f57073");

_App._DEPLOY_ID_SETUP = "customdeploy_map";
_App._DEPLOY_ID_ONLINE = "customdeploy_map_list";
_App._DEPLOY_ID_PRINTING = 'customdeploy_map_pdf';
_App._DEPLOY_ID_CSV = "customdeploy_map_csv";
_App._DEPLOY_ID_DAT = "customdeploy_map_dat";
_App._SCRIPT_ID = "customscript_map";

_App._FOOTER_TEMPLATE = 'map_footer.htm';
_App._ROW_TEMPLATE = 'map_rows.htm';
_App._ROW_TOTAL_TEMPLATE = 'map_rows_total.htm';
_App._PRINT_TEMPLATE = 'map_template.xml';

_App.TAX_REPORT_NAME = 'Script Report : WTax Journals By Vendor';

var searchType = 'MAP_DETAIL';

MAP_REPORT.REPORT_NOT_AVAILABLE_HEADER = 'No Report Available';
MAP_REPORT.REPORT_NOT_AVAILABLE_MESSAGE = 'The withholding tax report for this country is not currently supported in NetSuite.';

function main(request, response)
{
	var creditableWTaxCodeIds = new Array();

	var taxtype = request.getParameter('custpage_taxtype');
	if (taxtype)
		creditableWTaxCodeIds = _4601.WithholdingTaxCodeFetcher().getTaxCodesFilteredByType(taxtype);

	switch (_App.Context.getDeploymentId())
	{
		case _App._DEPLOY_ID_ONLINE:
			new MAP_REPORT.App( request, response ).Run(creditableWTaxCodeIds);
			break;
			
		case _App._DEPLOY_ID_SETUP:
			new MAP_REPORT.Setup( request, response ).Run();
			break;
			
		case _App._DEPLOY_ID_PRINTING:
			new MAP_REPORT.Print( request, response ).Run(creditableWTaxCodeIds);
			break;
			
		case _App._DEPLOY_ID_CSV:
			new MAP_REPORT.Csv( request, response ).Run(creditableWTaxCodeIds);
			break;	
			
        case _App._DEPLOY_ID_DAT:
            new MAP_REPORT.Dat( request, response ).Run(creditableWTaxCodeIds);
            break;  

        default:
			new MAP_REPORT.Setup( request, response ).Run();
	}
}

MAP_REPORT.SortFunction = function SortFunction(lhs, rhs)
{
    //seqNo, tin, atc, tax_id, taxBase, taxAmount, lastname, firstname, middlename, companyname, payeeflag)
    if (lhs.payeeflag == "T")
    {
        if (rhs.payeeflag == "T")
        {
            //Person to person comparison
            var lhsLastName = lhs.lastname.toUpperCase();
            var rhsLastName = rhs.lastname.toUpperCase();
            if (lhsLastName < rhsLastName)
                return -1;
            else if (lhsLastName > rhsLastName)
                return 1;
            else
            {
                var lhsFirstName = lhs.firstname.toUpperCase();
                var rhsFirstName = rhs.firstname.toUpperCase();
                if (lhsFirstName < rhsFirstName)
                    return -1;
                else if (lhsFirstName > rhsFirstName)
                    return 1;
                else
                {
                    var lhsMidName = lhs.middlename.toUpperCase();
                    var rhsMidName = rhs.middlename.toUpperCase();
                    if (lhsMidName < rhsMidName)
                        return -1;
                    
                    if (lhsMidName > rhsMidName)
                        return 1;
                    
                    return 0;
                }
            }
        }
        else
            return 1;
    }
    else
    {
        if (rhs.payeeflag == "T")
            return -1;
        else
        {
            //Corporation to corporation comparison
            var lhsRegName = lhs.reg_name.toUpperCase();
            var rhsRegName = rhs.reg_name.toUpperCase();
            return lhsRegName < rhsRegName ? -1 : lhsRegName > rhsRegName ? 1 : 0;
        }
    }
};

MAP_REPORT.Setup = function Setup( request, response ){
	
	var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
	var _request = request;
	
	var _periodId = null;
	var _subId = null;
	var _taxTypeId = null;
	var _nexusId = null;
	
	//Initialize the form by assembling search parameters;
	function formInit() {
		var currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod();
		var is_post_back = _request.getMethod() == 'POST' ? true : false;
		var is_refresh = _request.getParameter('refresh');
		var subIdsThatWTaxAppliesTo = _4601.getSubsidiaryIdsThatWTaxAppliesTo();
		var _periodId = null;
		
		if( _IsOneWorld )
			_subId = isNaN(parseInt(_request.getParameter('custpage_subsidiary'))) ? null : _request.getParameter('custpage_subsidiary');
		else
			_nexusId = _request.getParameter('custpage_nexus') == null ? null : _request.getParameter('custpage_nexus');
		
		if( is_post_back || is_refresh == 'y'){ //Redirect to List Page
			var arrParam = new Array();
			
			_periodId = !!_request.getParameter('custpage_periodid') ? _request.getParameter('custpage_periodid') : currentPeriod.GetId();
			arrParam['custpage_periodid'] = _periodId;
				
			if( _IsOneWorld )
				arrParam['custpage_subsidiary'] = _subId;
			else
				arrParam['custpage_nexus'] = _nexusId;

			_taxTypeId = _request.getParameter('custpage_taxtype');
			arrParam['custpage_taxtype'] = _taxTypeId;
			
			if (is_refresh != 'y')
				nlapiSetRedirectURL('SUITELET', _App._SCRIPT_ID, _App._DEPLOY_ID_ONLINE, null, arrParam);

		//Initial Search Screen
        } else { 
			_periodId = _request.getParameter('custpage_periodid');

			if (!_periodId && currentPeriod != null)
				_periodId = currentPeriod.GetId();

		}
		
		var form = nlapiCreateForm('Withholding Tax Reports');
		form.setScript('customscript_map_client');
		
		var refreshUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_SETUP);
		_App.CreateField(form, "urlrefresh", "text", "", false, refreshUrl, "hidden", "normal");
		
        //Subsidiary
        var countryCode = null;
        if (_IsOneWorld) {
            var subsComboboxConfig = new _4601.SubsidiaryComboboxConfiguration(
            	subIdsThatWTaxAppliesTo,
                'custpage_subsidiary',
                'Subsidiary',
                _subId,
                'outsideabove',
                'startrow');
            var objSubsidiaryCombo = new _4601.FilteredSubsidiaryCombobox(form, subsComboboxConfig);
			objSubsidiaryCombo.setMandatory(true);
			countryCode = SFC.System.getSubsidiaryCountry(_subId);
		} else {
			var nexusComboBoxParam = {
					fieldName: 'custpage_nexus', 
					fieldLabel: 'Nexus',
					idSelectedDefault: _nexusId, 
					fieldLayoutType: 'outsideabove',
					fieldBreakType: 'startrow'
			};
			
			var objNexusCombo = new _4601.NexusCombobox(form, nexusComboBoxParam);
			objNexusCombo.setMandatory(true);
			countryCode = _nexusId;
		}
        
        var _reportId = _reportId == null ? 'map' : _request.getParameter('custpage_countryreport');
		var relatedReport = {country: countryCode};
		var isReportAvailable = new SFC.System.ReportFormCombobox(form, 'custpage_countryreport', 'Reports', _reportId, 'outsideabove', null, relatedReport);
		isReportAvailable.setMandatory(true);
        
        var subsidiary = _IsOneWorld ? subIdsThatWTaxAppliesTo : _subId;
        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        
        // Tax Period
        var objTaxCombo = new SFC.System.TaxPeriodMonthQuarterCombobox(
        		form, 
        		"custpage_periodid", 
        		"Tax Period", 
        		_periodId, 
        		'outsideabove',
        		_IsOneWorld ? (!!_subId ? _subId : null) : (!!_nexusId ? _nexusId : null),
        		fiscalCalendarId);
        objTaxCombo.setMandatory(true);
        
        //add the WH Type field
        var comboboxParam = {
    			validSubsidiaryIds: subsidiary, 
    			idSelectedDefault: null, 
    			fieldSelectType: 'multiselect',
    			fieldName: 'custpage_taxtype',
    			fieldLabel: 'Tax Type',
    			fieldLayoutType: 'normal',
    			fieldBreakType: 'none',
    			subId: _IsOneWorld ? _subId : '',
    			nexusId: _IsOneWorld ? '' : _nexusId
    	};
    	var objTaxTypeCombo = new _4601.WHTaxTypeCombobox(form, comboboxParam);
        objTaxTypeCombo.setMandatory(true);
        
		var btnSubmit = form.addSubmitButton('Submit');
		
		if (isReportAvailable.getSelectOptions().length <= 0) {
			//Disable buttons
			btnSubmit.setDisabled(true);
			
            var errorMessage = 
                ["<br><div id='div__errormessage'/><script>showAlertBox('div__errormessage', '", 
                 MAP_REPORT.REPORT_NOT_AVAILABLE_HEADER, "','",
                 MAP_REPORT.REPORT_NOT_AVAILABLE_MESSAGE,
                 "', NLAlertDialog.TYPE_LOW_PRIORITY, '825px', '', '', false);</script>"].join("");               
            _App.CreateField(form, "custpage_errormessage", "inlinehtml", "", false, errorMessage, "normal", "outsidebelow", "startrow");
		}
        
		return form;
	}


	this.Run = function() {
		var form = formInit();
		response.writePage(form);
	};
};


MAP_REPORT.Csv = function Csv(request, response)
{
    var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
    var _request = request;
    var _periodId = null;
    var _subId = null;
    var _nexusId = null;

    //Initialize the form by assembling search parameters;
    function formInit()
    {
        _periodId = _request.getParameter('custpage_periodid');

        if (_IsOneWorld) {
            _subId = isNaN(parseInt(_request.getParameter('custpage_subsidiary'))) ? _Context.getSubsidiary() : _request.getParameter('custpage_subsidiary');
        } else {
            _nexusId = _request.getParameter('custpage_nexus');
        }
    }


    function convertToDelimitedString(objPayee, sequenceNumber)
    {
        var arrPayee = [];

        arrPayee.push(sequenceNumber);                                                      //Sequence No.
        arrPayee.push(objPayee.tin);                                                        //TIN - formatted xxx-xxx-xxx-xxxx
        arrPayee.push(objPayee.payeeflag == "T" ? "" : _4601.escapeTextForCSV(objPayee.reg_name));      //Registered Name - business entity only
        arrPayee.push(objPayee.payeeflag == "T" ? _4601.escapeTextForCSV(objPayee.lastname) : "");      //Lastname - person only
        arrPayee.push(objPayee.payeeflag == "T" ? _4601.escapeTextForCSV(objPayee.firstname) : "");     //Firstname - person only
        arrPayee.push(objPayee.payeeflag == "T" ? _4601.escapeTextForCSV(objPayee.middlename) : "");    //Middlename - person only
        arrPayee.push(objPayee.atc);                                                        //ATC
        arrPayee.push(_4601.escapeTextForCSV(objPayee.desc));                                           //Nature of Income Payment
        arrPayee.push(_4601.escapeTextForCSV(_4601.formatCurrency(objPayee.tax_base)));                                                   //Amount of income tax payment (tax base)
        arrPayee.push(_4601.escapeTextForCSV(_4601.formatRate(objPayee.tax_rate)));                                       //Tax rate
        arrPayee.push(_4601.escapeTextForCSV(_4601.formatCurrency(objPayee.tax_amount)));                                                 //Amount of tax witheld

        return arrPayee.join(",");
    }

    function _WriteHeader(response, subId, taxPeriodId)
    {
        var headerInfo = _4601.GetHeaderInfo(subId, taxPeriodId, searchType);

        response.writeLine("ALPHABETICAL LIST OF PAYEES FROM WHOM TAXES WERE WITHHELD ");
        response.writeLine("\"FOR THE PERIOD: " + headerInfo.StartDate + " TO " + headerInfo.EndDate + "\"");
        response.writeLine("");
        response.writeLine("TIN: " + headerInfo.PayeeTIN);
        response.writeLine(_4601.escapeTextForCSV("PAYEE'S NAME: " + headerInfo.PayeeName));
        response.writeLine("");
        response.writeLine("SEQ NO,TAXPAYER IDENTIFICATION NUMBER,CORPORATION (Registered Name),LAST NAME,FIRST NAME,MIDDLE NAME,ATC CODE,NATURE OF PAYMENT,AMOUNT OF INCOME PAYMENT,TAX RATE,AMOUNT OF TAX WITHHELD");
        response.writeLine("'(1),'(2),'(3),'(4),,,'(5),'(6),'(7),,'(8)");
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,,,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
    }

    function _WriteFooter(response, grandTotal)
    {
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,,,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
        response.writeLine("GRAND TOTAL,,,,,,,," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.Income)) + ",," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.TaxWithheld)));
		response.writeLine("");
        response.writeLine("END OF REPORT");
    }

    this.Run = function(creditableWTaxCodeIds)
    {
        formInit();

        response.setContentType("CSV", "alphalist.csv", "attachment");

        _WriteHeader(response, _subId, _periodId);

        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        var reportData = MAP_REPORT.getVendorData(creditableWTaxCodeIds, { periodId: _periodId, subid: _subId, nexusid: _nexusId, fiscalCalendarId: fiscalCalendarId }, searchType);
        var reportLineItems = reportData.getMapLineItems();
        var grandTotal = reportData.getGrandTotal();
        
        for (var i = 0; i < reportLineItems.length; ++i)
            response.writeLine(convertToDelimitedString(reportLineItems[i], i + 1));

        _WriteFooter(response, grandTotal);
    };
};


MAP_REPORT.Dat = function Dat(request, response)
{
	var _Context = nlapiGetContext();
	var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
	var _periodId = request.getParameter('custpage_periodid');
	var _subId = _IsOneWorld ? isNaN(parseInt(request.getParameter('custpage_subsidiary'))) ? _Context.getSubsidiary() : request.getParameter('custpage_subsidiary') : '';
	var _nexusId = request.getParameter('custpage_nexus');

	this.Run = function(creditableWTaxCodeIds)
	{
	    var formType = '1601E';
		WTax.injector.addObject('ds', MAP_REPORT);
		WTax.injector.addObject('header', _4601);
		WTax.injector.addObject('formTypeCode', formType);
		WTax.injector.addObject('adapter', WTax.injector.getInstance(WTax.MapReport.Adapter));
		WTax.injector.addClass('formatter', WTax.MapReport.Formatter);
		WTax.injector.addClass('taxSetupDao', WTax.TaxSetupDao);
		
		var params = {
			creditableWTaxCodeIds: creditableWTaxCodeIds,
			periodId: _periodId,
			searchType: searchType,
			subId: _subId, 
			nexusId: _nexusId,
			adapter: null,
			ds: null, 
			header: null, 
			formatter: null,
            taxSetupDao: null
		};
		
		WTax.injector.addObject('params', WTax.injector.getObject(params));
		var controller = WTax.injector.getInstance(WTax.MapReport.Controller);
		var output = controller.Run();
		
		response.setContentType(output.contentType, output.fileName);
		response.write(output.contents);
	};
};

MAP_REPORT.App = function App(request, response)
{
    var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
    var _request = request;

    var _periodId = null;
    var _subId = null;
    var _nexusId = null;
    var _taxTypeId = null;

    //Initialize the form by assembling search parameters;
    function formInit()
    {
    	var currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod();
        
        if (_request.getMethod() == 'POST') {
            if (currentPeriod != null)
                _periodId = currentPeriod.GetId();

            if (_IsOneWorld)
            	_subId = _Context.getSubsidiary();
            else
            	_nexusId = nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country");
        
        //Redirected from Setup
        } else {
            
            _periodId = _request.getParameter('custpage_periodid');
            _taxTypeId = _request.getParameter('custpage_taxtype');

            if (_IsOneWorld) 
                _subId = isNaN(_request.getParameter('custpage_subsidiary')) ? _Context.getSubsidiary() : _request.getParameter('custpage_subsidiary');
            else
            	_nexusId = _request.getParameter('custpage_nexus') == null ? nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country") : _request.getParameter('custpage_nexus');
        }
    }

    this.Run = function(creditableWTaxCodeIds)
    {
        formInit();
        
        var resourceObject = _4601.getResourceManager();

        var csvUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_CSV);
        var datUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_DAT);
        var printUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_PRINTING);
        var backUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._DEPLOY_ID_SETUP);
        var queryStr = "&custpage_periodid=" + _periodId + "&custpage_taxtype=" + _taxTypeId;
        queryStr += (_IsOneWorld ? "&custpage_subsidiary=" + _subId : "&custpage_nexus=" + _nexusId);
        
        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        var objSelectedPeriod = new SFC.System.TaxPeriod().Load(_periodId, fiscalCalendarId);

        var alphaList =
            (objSelectedPeriod.GetType() == "quarter") ? nlapiCreateList('Quarterly Tax Withheld for Vendor - Period ' + MAP_REPORT.getPeriodInfo(objSelectedPeriod), false) :
            (objSelectedPeriod.GetType() == "month") ? nlapiCreateList('Monthly Tax Withheld for Vendor - Period ' + MAP_REPORT.getPeriodInfo(objSelectedPeriod), false) :
            {};

        alphaList.addColumn('seq_no', 'text', 'SEQ NO', 'RIGHT');
        alphaList.addColumn('tin', 'text', 'TAXPAYER IDENTIFICATION NUMBER', 'LEFT');
        alphaList.addColumn('reg_name', 'text', 'CORPORATION (Registered Name)', 'LEFT');
        alphaList.addColumn('ind_name', 'text', "INDIVIDUAL (Last Name, First Name, Middle Name)", 'LEFT');
        alphaList.addColumn('atc', 'text', 'ATC CODE', 'LEFT');
        alphaList.addColumn('desc', 'text', 'NATURE OF PAYMENT', 'LEFT');
        alphaList.addColumn('tax_base', 'currency', 'AMOUNT OF INCOME PAYMENT', 'RIGHT');
        alphaList.addColumn('tax_rate', 'currency', 'TAX RATE', 'RIGHT');
        alphaList.addColumn('tax_amount', 'currency', 'AMOUNT OF TAX WITHHELD', 'RIGHT');

        alphaList.addButton('printBtn', resourceObject.REPORT['button'].PRINT_PDF.label, "window.open('" + printUrl + queryStr + "')");
        alphaList.addButton('csvBtn', resourceObject.REPORT['button'].SAVE_CSV.label, "window.open('" + csvUrl + queryStr + "')");
        alphaList.addButton('datBtn', resourceObject.REPORT['button'].EXPORT_DAT.label, "window.open('" + datUrl + queryStr + "')");

        alphaList.addButton('filterBtn', resourceObject.REPORT['button'].RETURN_TO_CRITERIA.label, "document.location='" + backUrl + queryStr + "'");
        
        var data = MAP_REPORT.getVendorData(creditableWTaxCodeIds, { periodId: _periodId, subid: _subId, nexusid: _nexusId, fiscalCalendarId: fiscalCalendarId }, searchType);
        var reportLineItems = data.getMapLineItems();
        
        for (var i = 0; i < reportLineItems.length; ++i)
        {
        	reportLineItems[i].seq_no = (i + 1).toString();
            alphaList.addRow(reportLineItems[i]);
        }

        response.writePage(alphaList);
    };
};


MAP_REPORT.Print = function Print(request, response)
{
    var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
    var _request = request;
    var _periodId = null;
    var _subId = null;
    var _nexusId = null;

    //Initialize the form by assembling search parameters;
    function formInit()
    {
        _periodId = _request.getParameter('custpage_periodid');

        if (_IsOneWorld) {
            _subId = isNaN(_request.getParameter('custpage_subsidiary')) ? _Context.getSubsidiary() : _request.getParameter('custpage_subsidiary');
        } else {
            _nexusId = _request.getParameter('custpage_nexus');
        }
    }

    function createContent(startDate, endDate, payeeTIN, payeeName, body, footer)
    {
        var helperUtils = new _4601.HelperUtils(_App, _request);
    	var arabicfont = nlapiEscapeXML(helperUtils.getFullFileUrl("koodak.ttf"))
        
        var mainTemplate = _App.GetFileContent(_App._PRINT_TEMPLATE, true);
        var elements = {
            arabicfont: arabicfont,
            PeriodStartDate: startDate.toString("MMMM d, yyyy"),
            PeriodEndDate: endDate.toString("MMMM d, yyyy"),
            PayeeTIN: payeeTIN,
            PayeeName: payeeName,
            footer: footer,
            recordtable: body
        };

        return _App.RenderTemplate(mainTemplate, elements);
    }

    function createFooter()
    {
        return  _App.GetFileContent(_App._FOOTER_TEMPLATE, true);
    }

    function createBody(reportData)
    {
    	var reportLineItems = reportData.getMapLineItems();
        var grandTotal = reportData.getGrandTotal();
        
        formatAmounts(reportLineItems);
        escapeText(reportLineItems);
        adjustSeqNo(reportLineItems);

        var rowTemplate = _App.GetFileContent(_App._ROW_TEMPLATE, true);
        var totalTemplate = _App.GetFileContent(_App._ROW_TOTAL_TEMPLATE, true);

        var objTotal = {totalTaxAmount : 0.0, totalBaseAmount: 0.0};
        objTotal.totalTaxAmount = _4601.formatCurrency(grandTotal.TaxWithheld, 'PDF');
        objTotal.totalBaseAmount = _4601.formatCurrency(grandTotal.Income, 'PDF');

        var htmlRows = _App.RenderTemplateToArray(rowTemplate, reportLineItems);
        var htmlTotal = _App.RenderTemplate(totalTemplate, objTotal);

        return htmlRows + htmlTotal;
    }
    
    function adjustSeqNo(objArray) {
    	for (var i = 0; i < objArray.length; i++) {
    		objArray[i].seq_no = i + 1;
    	}
    }

    function formatAmounts(arrPayeeInfo)
    {
        if (arrPayeeInfo == null)
            return;

        for (var i = 0; i < arrPayeeInfo.length; ++i) {
            arrPayeeInfo[i].tax_base = _4601.formatCurrency(arrPayeeInfo[i].tax_base, 'PDF');
            arrPayeeInfo[i].tax_amount = _4601.formatCurrency(arrPayeeInfo[i].tax_amount, 'PDF');
            arrPayeeInfo[i].tax_rate = _4601.formatRate(arrPayeeInfo[i].tax_rate, 'PDF');
        }
    }

    function escapeText(arrPayeeInfo)
    {
        for (var i in arrPayeeInfo) {
            arrPayeeInfo[i].reg_name = nlapiEscapeXML(arrPayeeInfo[i].reg_name);
			arrPayeeInfo[i].entity_name = nlapiEscapeXML(arrPayeeInfo[i].entity_name);
            arrPayeeInfo[i].atc = nlapiEscapeXML(arrPayeeInfo[i].atc);
            arrPayeeInfo[i].desc = nlapiEscapeXML(arrPayeeInfo[i].desc);
            arrPayeeInfo[i].tin = nlapiEscapeXML(arrPayeeInfo[i].tin);
        }
    }

    this.Run = function(creditableWTaxCodeIds) {
        formInit();

        var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
        var reportData = MAP_REPORT.getVendorData(creditableWTaxCodeIds, { periodId: _periodId, subid: _subId, nexusid: _nexusId, fiscalCalendarId: fiscalCalendarId }, searchType);

        var body = createBody(reportData);
        var footer = createFooter();
        var headerInfo = _4601.GetHeaderInfo(_subId, _periodId, searchType);
        var content = createContent(headerInfo.StartDate, headerInfo.EndDate, headerInfo.PayeeTIN, headerInfo.PayeeName, body, footer);
        
        var pdfFile = null;
        
        try {
        	pdfFile = nlapiXMLToPDF('<?xml version="1.0" encoding="ISO-8859-1"?>\n<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);
        } catch (err) {
        	nlapiLogExecution('DEBUG', 'Error Creating PDF File',  err);
        	
        	content = nlapiEscapeXML(content);
        	
        	pdfFile = nlapiXMLToPDF('<?xml version="1.0" encoding="ISO-8859-1"?>\n<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);
        }
        
        response.setContentType("PDF", 'alphalist.pdf', 'attachment');
        response.write(pdfFile.getValue());
    };
};

