/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Apr 2012     ljamolod
 *
 */

if (!form2307) { var form2307 = {}; }

if (!_4601) { var _4601 = {}; }

form2307.getData = (function () {
	var _labelTaxCodeName = 'taxCodeName';
	var _labelTaxCode = 'taxCode';
	var _labelIsTaxGroup = 'isTaxGroup';
	var _labelGroupTaxCodes = 'groupTaxCodes';
	var _labelTransID = 'transactionID';
	var _labelTaxCodeRate = 'taxCodeRate';
	var _labelRecordType = 'recordtype';

	var _implemPH = 'ph';
	var _implemgeneric = 'generic';

	var allowedWithholdingTaxCodeIds = new Array();
	var groupWtaxCodes = new Array();
	var allValidGroupedWtaxCodes = new Array();
	var billCreditMap = new Array();
	var childrenTaxPeriod = new Array();
	var objSelectedPeriod = null;

	function getData(WTaxCodeIds, params){
		objSelectedPeriod = new SFC.System.TaxPeriod().Load(params.taxperiod, params.fiscalCalendarId);
		getData._includedPeriods = getData.getMonthTaxPeriodsByQuarter(params.taxperiod);
		getData._Context = nlapiGetContext();
		getData._IsOneWorld = getData._Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
		getData.implem = null;
		allowedWithholdingTaxCodeIds = WTaxCodeIds;

		if (allowedWithholdingTaxCodeIds.length > 0) {
			var purcTaxPoint = _4601.getPurchaseTaxPoint(params.subId);

			groupWtaxCodes = _4601.getTaxGroupWithValidTaxCodes(allowedWithholdingTaxCodeIds);

			allValidGroupedWtaxCodes = _4601.getAllValidGroupedTaxCodes(allowedWithholdingTaxCodeIds);

			var collatedResults = {};

			if (purcTaxPoint != 'onpayment') {
				// get results from PH implementation
				getData.implem = _implemPH;
				var oldSummarizedSearchResults = getData.getSearchResults(params, purcTaxPoint);
				getData.collatedResults = getData.collateByTaxcodeName(oldSummarizedSearchResults, collatedResults, purcTaxPoint);
			}

			// get results from new implementataion
			getData.implem = _implemgeneric;
			var genericSummarizedSearchResults = getData.getSearchResults(params, purcTaxPoint);
			collatedResults = getData.collateByTaxcodeName(genericSummarizedSearchResults, collatedResults, purcTaxPoint);
		}

		return collatedResults;
	}


	getData.getMonthTaxPeriodsByQuarter = function getMonthTaxPeriodsByQuarter( parentQuarterId ){
		var taxPeriods = new Array();
		var id = null;

		if (objSelectedPeriod.GetType() == 'month') {
			id = objSelectedPeriod.GetId();
			taxPeriods[id] = {};
			taxPeriods[id]['startdate_value'] = objSelectedPeriod.GetStartDate();
		} else if (objSelectedPeriod.GetType() == 'quarter') {
			var childrenTaxPeriod = objSelectedPeriod.GetChildren();

			for (var i in childrenTaxPeriod) {
				id = childrenTaxPeriod[i].GetId();
				taxPeriods[id] = {};
				taxPeriods[id]['startdate_value'] = childrenTaxPeriod[i].GetStartDate();
			}
		}

		return taxPeriods;
	};


	/**
	 * Performs the summation/accumulation
	 * while processing the search results. Works for summarized journal
	 * transactions or non-summarized journal transactions.
	 * 
	 * This produces a 2D data structure 
	 * {
	 *   'WI110' : {
	 *     'baseTaxablePerMonthOfQuarter': [0.0, 0.0, 0.0],
	 *     'baseTaxableAmountTotal' : 0.0,
	 *     'taxAmountQuarterTotal' : 0.0
	 *   }
	 * }
	 * 
	 * @param {nlobjSearchResult[]} vendorJournalTransactions
	 */	
	getData.collateByTaxcodeName = function collateByTaxcodeName(vendorJournalTransactions, collatedResults, purcTaxPoint){
		var searchColumnNames = {};
		var transID = '';
		var totalAmountMap = new Array();
		var pymntRef = '';

		if (vendorJournalTransactions) {
			var allColumns = vendorJournalTransactions[0].getAllColumns();
			searchColumnNames = {};
			for (i in allColumns) {
				if (allColumns[i].getLabel() != null)
					searchColumnNames[allColumns[i].getLabel()] = allColumns[i];
				else
					searchColumnNames[allColumns[i].getName()] = allColumns[i];
			}
		}

		// Get total amounts (base and withheld) to be used in computing ratios
		if (purcTaxPoint == 'onpayment') {
			var arrTransID = new Array();

			for (var j in vendorJournalTransactions) {
				transID = vendorJournalTransactions[j].getValue(searchColumnNames[_labelTransID]);
				if (transID != null && billCreditMap[transID] != null)
					arrTransID.push(transID);
			}

			if (arrTransID.length > 0)
				totalAmountMap = _4601.populateTotalAmountMap(arrTransID);
		}

		if (vendorJournalTransactions) {
			for (i in vendorJournalTransactions) {
				transID = vendorJournalTransactions[i].getValue(searchColumnNames[_labelTransID]);

				if (purcTaxPoint == 'onpayment' && billCreditMap[transID] != null) {
					var billPayments = billCreditMap[transID];

					for (var h in billPayments) {
						var payment = billPayments[h];
						pymntRef = payment.pymntRef;
						collatedResults = getData.calculateAmountTransaction(vendorJournalTransactions, i, totalAmountMap, collatedResults, searchColumnNames, purcTaxPoint, transID, pymntRef);
					}
				} else {
					collatedResults = getData.calculateAmountTransaction(vendorJournalTransactions, i, totalAmountMap, collatedResults, searchColumnNames, purcTaxPoint, transID, pymntRef);
				}
			}
		}
		return collatedResults;
	};


	getData.calculateAmountTransaction = function calculateAmountTransaction(vendorJournalTransactions, i, totalAmountMap, collatedResults, searchColumnNames, purcTaxPoint, transID, pymntRef) {
		var taxCodeName = '';
		var taxAmount = 0.0;
		var taxBase = 0.0;
		var isTaxGroup = '';
		var groupTaxCodes = null;
		var taxPeriod = '';
		var nonTaxGroupCount = -1;
		var taxRate = '';
		var taxRateComp = 0.0;
		var taxCode = '';
		var recordType = '';
		var implementation = getData.implem;

		taxBase = parseFloat(vendorJournalTransactions[i].getValue(searchColumnNames['custcol_4601_witaxbaseamount']));
		taxAmount = vendorJournalTransactions[i].getValue(searchColumnNames['custcol_4601_witaxamount']);
		taxRate = vendorJournalTransactions[i].getValue(searchColumnNames[_labelTaxCodeRate]);
		taxRateComp = parseFloat(taxRate.replace('%', ''));
		taxPeriod = vendorJournalTransactions[i].getValue(searchColumnNames['taxperiod']);
		isTaxGroup = vendorJournalTransactions[i].getValue(searchColumnNames[_labelIsTaxGroup]);
		groupTaxCodes = vendorJournalTransactions[i].getValue(searchColumnNames[_labelGroupTaxCodes]);
		recordType = vendorJournalTransactions[i].getValue(searchColumnNames[_labelRecordType]);

		if (!(recordType == 'check' && getData.isPaymentVoided(transID))) {
			if (isTaxGroup == 'T') {
				var taxRateTotalComp = 0.0;
				var taxAmountTotal = 0.0;
				var arrTaxCodes = groupTaxCodes.split(',');
				var arrGroupTaxCodes = _4601.getTaxCodesByTaxGroup(arrTaxCodes, allValidGroupedWtaxCodes);
				var taxBasis = 0.0;

				if (purcTaxPoint == 'onpayment' && transID != null && billCreditMap[transID] != null)
					taxAmountTotal = getData.getLineAmountDistribution(taxBase, taxAmount, transID, totalAmountMap, pymntRef);
				else
					taxAmountTotal = taxAmount;

				taxRateTotalComp = taxRateComp;
				for (var j in arrGroupTaxCodes) {
					taxBasis = parseFloat(arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_basis'));
					taxCodeName = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_name', 'custrecord_4601_gwtc_code');
					taxCode = arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_code');
					taxRate = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_rate', 'custrecord_4601_gwtc_code');
					taxRateComp = parseFloat(taxRate.replace('%', ''));

					if (allowedWithholdingTaxCodeIds.indexOf(taxCode) > -1) {
						if (purcTaxPoint == 'onaccrual')
							taxAmount = -(taxBase * ((taxRateComp * (taxBasis/100)))/100);
						else if (purcTaxPoint == 'onpayment') {
							var taxRateRatio = taxRateTotalComp == 0 ? 0 : taxRateComp / taxRateTotalComp;
							taxAmount = taxRateRatio * taxAmountTotal;
							taxBase = -(taxRateTotalComp == 0 ? 0 : taxAmount / (taxRateComp/100));
						}

						var wtaxDetails = {
								taxPeriod: taxPeriod, 
								taxCodeName: taxCodeName, 
								taxAmount: implementation == 'ph' ? taxAmount : -taxAmount, 
										taxBase: taxBase, 
										isTaxGroup: isTaxGroup, 
										taxGroupCount: j
						};

						collatedResults = getData.populateLineItemMap(collatedResults, searchColumnNames, wtaxDetails);
					}
				}
			} else {
				taxCode = vendorJournalTransactions[i].getValue(searchColumnNames[_labelTaxCode]);
				if (allowedWithholdingTaxCodeIds.indexOf(taxCode) > -1) {
					taxCodeName = vendorJournalTransactions[i].getValue(searchColumnNames[_labelTaxCodeName]);

					if (purcTaxPoint == 'onpayment' && transID != null && billCreditMap[transID] != null) {
						taxAmount = getData.getLineAmountDistribution(taxBase, taxAmount, transID, totalAmountMap, pymntRef);
						taxBase = -(taxAmount / (taxRateComp/100));

					}

					var wtaxDetails = {
							taxPeriod: taxPeriod, 
							taxCodeName: taxCodeName, 
							taxAmount: implementation == 'ph' ? taxAmount : -taxAmount, 
									taxBase: taxBase, 
									isTaxGroup: isTaxGroup, 
									taxGroupCount: nonTaxGroupCount
					};

					collatedResults = getData.populateLineItemMap(collatedResults, searchColumnNames, wtaxDetails);
				}
			}
		}

		return collatedResults;			
	};

	getData.isPaymentVoided = function isPaymentVoided(mainPaymentId){
		var filters = [];
		filters.push(new nlobjSearchFilter("voided", null, "is", "T"));
		filters.push(new nlobjSearchFilter("mainline", null, "is", "T"));
		filters.push(new nlobjSearchFilter("internalid", null, "is", mainPaymentId));
		return nlapiSearchRecord('transaction', null, filters);
	};


	getData.getLineAmountDistribution = function getLineAmountDistribution(taxBase, taxAmount, transID, totalAmountMap, pymntRef) {
		var creditWTaxPayment = 0.0;
		var lineCreditWTaxPayment = 0.0;

		amountRatio = getData.getAmountRatio(taxBase, taxAmount);
		creditWTaxPayment = billCreditMap[transID][pymntRef].taxWithheld;

		var wtaxPaymentRatio = taxAmount / totalAmountMap[transID].totalTaxAmount;

		lineCreditWTaxPayment = creditWTaxPayment * wtaxPaymentRatio;

		return lineCreditWTaxPayment;
	};


	getData.getAmountRatio = function getAmountRatio(taxBase, taxAmount) {
		return (taxAmount / (taxBase - taxAmount));
	};


	// taxPeriod, taxCodeName, taxAmount, taxBase, isTaxGroup, taxGroupCount
	getData.populateLineItemMap = function populateLineItemMap(collatedResults, searchColumnNames, wtaxDetails) {
		var taxPeriod = wtaxDetails.taxPeriod;
		var taxCodeName = wtaxDetails.taxCodeName;
		var taxAmount = wtaxDetails.taxAmount;
		var taxBase = wtaxDetails.taxBase;
		var isTaxGroup = wtaxDetails.isTaxGroup;
		var taxGroupCount = wtaxDetails.taxGroupCount;
		var monthIndex = '';

		var Form2307TaxPerMonthOfQuarter = function(){
			return {
				'baseTaxablePerMonthOfQuarter': [0.0, 0.0, 0.0],
				'groupBaseTaxablePerMonthOfQuarter': [0.0, 0.0, 0.0],
				'baseTaxableAmountForQuarter': 0.0,
				'groupBaseTaxableAmountForQuarter': 0.0,
				'wTaxAmountForQuarter': 0.0
			}
		};

		monthIndex = getData._includedPeriods[taxPeriod]['startdate_value'].getMonth();

		if (!collatedResults[taxCodeName]) {
			collatedResults[taxCodeName] = new Form2307TaxPerMonthOfQuarter();
		}

		if (!isNaN(parseFloat(taxBase))) {
			if (isTaxGroup != 'T' || (isTaxGroup == 'T' && taxGroupCount == 0)) {
				collatedResults[taxCodeName]['baseTaxablePerMonthOfQuarter'][monthIndex % 3] +=
					parseFloat(taxBase);
				collatedResults[taxCodeName]['baseTaxableAmountForQuarter'] += parseFloat(taxBase);
			} else {
				collatedResults[taxCodeName]['groupBaseTaxablePerMonthOfQuarter'][monthIndex % 3] +=
					parseFloat(taxBase);
				collatedResults[taxCodeName]['groupBaseTaxableAmountForQuarter'] += parseFloat(taxBase);
			}

		}

		if (!isNaN(parseFloat(taxAmount))) {
			collatedResults[taxCodeName]['wTaxAmountForQuarter'] += parseFloat(taxAmount);
		}

		return collatedResults;
	};


	/**
	 * Returns the dynamic search results for transaction entries
	 * relevant to the BIR form 2307 of the given vendor 
	 * @param {Int|String} Id
	 */
	getData.getSearchResults = function getSearchResults(params, purcTaxPoint) {
		var implementation = getData.implem;

		var arrTransID = [];
		var columns = [];
		var filters = [];
		var results = [];

		//common columns
		columns.push( new nlobjSearchColumn(
				'taxperiod',
				null,
		'group'));
		columns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM') // tax base amount
		.setFormula('{custcol_4601_witaxbaseamount}') //'custcol_ph4014_wtax_bamt', - deprecated
		.setLabel('custcol_4601_witaxbaseamount')
		);
		columns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM') // wtax amount
		.setFormula('{custcol_4601_witaxamount}') //'custcol_ph4014_wtax_amt', - deprecated
		.setLabel('custcol_4601_witaxamount')
		);
		columns.push(new nlobjSearchColumn(
				'formulatext', 
				null, 
		'group').setFormula('{custcol_4601_witaxcode.custrecord_4601_wtc_istaxgroup}').setLabel(_labelIsTaxGroup));
		columns.push(new nlobjSearchColumn(
				'formulatext', 
				null, 
		'group').setFormula('{custcol_4601_witaxcode.custrecord_4601_wtc_groupedwitaxcodes}').setLabel(_labelGroupTaxCodes));
		columns.push(new nlobjSearchColumn(
				'internalid', 
				null, 
		'group').setLabel(_labelTransID));
		columns.push(new nlobjSearchColumn(
				'formulapercent', 
				null, 
		'group').setFormula('{custcol_4601_witaxcode.custrecord_4601_wtc_rate}').setLabel(_labelTaxCodeRate));
		columns.push( new nlobjSearchColumn(
				'recordtype',
				null,
		'group').setLabel(_labelRecordType));
		columns.push( new nlobjSearchColumn('formulatext', 
				null, 
		'group').setFormula('{custcol_4601_witaxcode.custrecord_4601_wtc_name}').setLabel(_labelTaxCodeName));
		columns.push( new nlobjSearchColumn(
				'custcol_4601_witaxcode',
				null,
		'group').setLabel(_labelTaxCode));


		//common filters
		if (params.subId && getData._IsOneWorld) {
			filters.push(['subsidiary', 'is', params.subId]);
			filters.push('AND');
		}

		//implementation-related columns and filters
		if (implementation == _implemPH) {
			columns.push( new nlobjSearchColumn(
					'custcol_ph4014_src_vendorid',
					'',
			'group'));

			var journalTypes = [];
			journalTypes.push("CardChrg");
			journalTypes.push("Check");
			journalTypes.push("VendBill");
			var journalTypeIds = _App.getJournalTypeIds(journalTypes);
			
			filters.push(['custcol_4601_witaxline', 'is', 'T']);
			filters.push('AND');
			filters.push(['type', 'is', 'Journal']);
			filters.push('AND');
			filters.push(['custcol_ph4014_src_jrnltrantypeid', 'anyof', journalTypeIds]);
			filters.push('AND');
			filters.push(['custbody_ph4014_wtax_reversal_flag', 'is', 'F']);
			filters.push('AND');
			filters.push(['custcol_ph4014_src_vendorid', 'is', params.vendorId]);
			filters.push('AND');
			filters.push(['custcol_4601_witaxcode', 'anyof', allowedWithholdingTaxCodeIds]);
			filters = _4601.getTaxPeriodFilters(filters, objSelectedPeriod);
			filters = _4601.getJournalReversalFilters(filters);
		} else if (implementation == _implemgeneric) {
			if (purcTaxPoint == 'onpayment') {
				var arrPaymentIds = getData.getPaymentRecordIds(params);
				if (arrPaymentIds.length > 0)
					filters.push(['internalid', 'anyof', arrPaymentIds]);
				else
					//Since there are no payment records retrieved, set the internalid to NONE
					//which in essence results to no record/s to be searched;
					//otherwise it will seach records intended for onaccrual setup
					filters.push(['internalid', 'is', '@NONE@']);
			} else if (purcTaxPoint == 'onaccrual') {
				arrTransID = getData.getTransactions(params, arrTransID);
				if (arrTransID.length > 0)
					filters.push(['internalid', 'anyof', arrTransID]);
				else
					filters.push(['internalid', 'is', '@NONE@']);

			}

			columns.push(new nlobjSearchColumn(
					'entityid',
					'vendor',
			'group'));
		}

		results = nlapiSearchRecord('transaction', null, filters, columns);	

		return results;
	};


	getData.getPaymentRecordIds = function getPaymentRecordIds(params) {
		var arrDocRefIds = new Array();

		arrDocRefIds = getData.getPaymentTransactions(params, arrDocRefIds);
		arrDocRefIds = getData.getCheckTransactions(params, arrDocRefIds);
		return arrDocRefIds;
	};


	getData.getCheckTransactions = function getCheckTransactions(params, arrDocRefIds) {
		// Get Check
		var purcTypes = new Array();
		purcTypes.push('Check');

		var paymentFilters = new Array();
		paymentFilters.push(['type', 'anyof', purcTypes]);
		paymentFilters.push('AND');
		paymentFilters.push(['vendor.internalid', 'noneof', '@NONE@']);
		paymentFilters.push('AND');
		paymentFilters.push(['custcol_ph4014_src_jrnltrantypeid', 'anyof','@NONE@']); //filter out old records
		paymentFilters.push('AND');
		paymentFilters.push(['custcol_4601_witaxapplies', 'is', 'T']);
		paymentFilters.push('AND');
		paymentFilters.push(['isreversal', 'is', 'F']);
		paymentFilters.push('AND');
		paymentFilters.push(['entity', 'is', params.vendorId]);
		paymentFilters = _4601.getTaxPeriodFilters(paymentFilters, objSelectedPeriod);
		paymentFilters = _4601.getWTaxCodeFilters(paymentFilters, null, {
			allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
			groupWtaxCodes: groupWtaxCodes});
		if (params.subid != null && _IsOneWorld){
			paymentFilters.push('AND');
			paymentFilters.push(['subsidiary', 'is', params.subid]);
		}

		var paymentColumns = new Array();

		var rsPayments = nlapiSearchRecord('transaction', null, paymentFilters, paymentColumns);

		for (var i in rsPayments)
			arrDocRefIds.push(rsPayments[i].getId());

		return arrDocRefIds;
	};


	getData.getPaymentTransactions = function getPaymentTransactions(params, arrDocRefIds) {
		// Get Bill Credit
		var creditFilters = new Array();

		creditFilters.push(['type', 'is', 'VendCred']);
		creditFilters.push('AND');
		creditFilters.push(['custbody_4601_pymnt_ref_id', 'noneof', '@NONE@']);
		creditFilters.push('AND');
		creditFilters.push(['custbody_4601_doc_ref_id', 'noneof', '@NONE@']);
		creditFilters.push('AND');
		creditFilters.push(['mainline', 'is', 'T']);
		creditFilters.push('AND');
		creditFilters.push(['entity', 'is', params.vendorId]);
		creditFilters = _4601.getTaxPeriodFilters(creditFilters, objSelectedPeriod);
    	creditFilters = _4601.getWTaxCodeFilters(creditFilters, 'custbody_4601_doc_ref_id.custcol_4601_witaxcode', {
    		allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
			groupWtaxCodes: groupWtaxCodes});

		var creditColumns = new Array();
		creditColumns.push(new nlobjSearchColumn('custbody_4601_doc_ref_id'));
		creditColumns.push(new nlobjSearchColumn('custbody_4601_pymnt_ref_id'));
		creditColumns.push(new nlobjSearchColumn('amount'));

		var rsCredit = nlapiSearchRecord('transaction', null, creditFilters, creditColumns);

		var _docRef = '';
		var _pymntRef = '';
		var _amount = 0.0;
		for (var i in rsCredit) {
			_docRef = rsCredit[i].getValue('custbody_4601_doc_ref_id');
			_pymntRef = rsCredit[i].getValue('custbody_4601_pymnt_ref_id');
			_amount = rsCredit[i].getValue('amount');
			if (_amount != 0) {
				if (billCreditMap[_docRef] == null) {
					arrDocRefIds.push(_docRef);
					billCreditMap[_docRef] = {};
				}
				billCreditMap[_docRef][_pymntRef] = {}; 
				billCreditMap[_docRef][_pymntRef].pymntRef = _pymntRef;
				billCreditMap[_docRef][_pymntRef].taxWithheld = _amount;
			}
		}


		return arrDocRefIds;
	};


	getData.getTransactions = function getTransactions(params, arrTransID) {
		var filters = [];
		var columns = new Array();

		if (allowedWithholdingTaxCodeIds != null && allowedWithholdingTaxCodeIds.length > 0) {
			var arrTransTypes = new Array();
			arrTransTypes.push('Check');
			arrTransTypes.push('VendBill');
			arrTransTypes.push('VendCred');


			filters.push(['type', 'anyof', arrTransTypes]);
			filters.push('AND');
			filters.push(['vendor.internalid', 'noneof', '@NONE@']);
			filters.push('AND');
			filters.push(['custcol_4601_witaxapplies', 'is', 'T']);
			filters.push('AND');
			filters.push(['isreversal', 'is','F']);
			filters.push('AND');
			filters.push(['posting', 'is', 'T']);
			filters.push('AND');
			filters.push(['voided', 'is', 'F']);
			filters.push('AND');
			filters.push(['entity', 'is', params.vendorId]);
			filters.push('AND');
			filters.push(['custcol_4601_witaxbaseamount', 'isnotempty', null]);
			if (params.subid != null && _IsOneWorld){
				filters.push('AND');
				filters.push(['subsidiary', 'is', params.subid]);
			}
			filters = _4601.getTaxPeriodFilters(filters, objSelectedPeriod);
        	filters = _4601.getWTaxCodeFilters(filters, null, {
        		allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
    			groupWtaxCodes: groupWtaxCodes});

			var rsTransactions = nlapiSearchRecord('transaction', null, filters, columns);

			if (rsTransactions != null) {
				for (var i in rsTransactions)
					arrTransID.push(rsTransactions[i].getId());
			}
		}

		return arrTransID;
	};


	return getData;
}());