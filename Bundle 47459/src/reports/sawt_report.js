/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * 
 * Suitelet to handle the display for SAWT (Summary Alphalist of Witholding
 * Agents/Payors of Income Payments subjected to Creditable Witholding Tax at
 * Source.
 * 
 * library dependencies:
 *  wi_tax_reports_library.js
 *  vendor_data_source.js
 * 
 * References: FRD 4121, FRD 4601
 * https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=138&id=4121 
 * http://twiki.corp.netsuite.com/twiki/bin/view/PM/FeatureDocument4121
 * @author ruy, mmoya
 */

if (!SAWT_REPORT) var SAWT_REPORT = {};

var _App = new SFC.System.Application("c54f4fb9-a81f-40e0-bf8f-ae7d00f57073");

_App._ROW_TEMPLATE = 'sawt_rows_print.htm';
_App._ROW_PRINT_TEMPLATE = 'sawt_template.xml';

_App._ID_DEPLOYMENT_UI = 'customdeploy_sawt';
_App._ID_DEPLOYMENT_LIST_UI = 'customdeploy_sawt_list';
_App._ID_DEPLOYMENT_CSV = 'customdeploy_sawt_list_csv';
_App._ID_DEPLOYMENT_PDF = 'customdeploy_sawt_list_pdf';
_App._SCRIPT_ID = "customscript_sawt";
_App._ID_DEPLOYMENT_DAT = 'customdeploy_sawt_list_dat';

var searchType = 'SAWT_DETAIL';
var context = nlapiGetContext();

SAWT_REPORT.REPORT_NOT_AVAILABLE_HEADER = 'No Report Available';
SAWT_REPORT.REPORT_NOT_AVAILABLE_MESSAGE = 'The withholding tax report for this country is not currently supported in NetSuite.';



function dispatchPerDeploymentType(request, response)
{
    var creditableWTaxCodeIds = new Array();
    
    var taxtype = request.getParameter('custpage_taxtype');
    
    if (taxtype)
    	creditableWTaxCodeIds = _4601.WithholdingTaxCodeFetcher().getTaxCodesFilteredByType(taxtype);
    
	switch(nlapiGetContext().getDeploymentId()) {
        case _App._ID_DEPLOYMENT_LIST_UI:
            new SAWT_REPORT.ListViewSuitelet(request, response).run(creditableWTaxCodeIds);
            break;
            
		case _App._ID_DEPLOYMENT_UI:
            new SAWT_REPORT.UIBuilderSuitelet(request, response).run();
            break;
            
		case _App._ID_DEPLOYMENT_CSV:
            new SAWT_REPORT.CSVOutputSuitelet(request, response).run(creditableWTaxCodeIds);
            break;
            
		case _App._ID_DEPLOYMENT_PDF:
            new SAWT_REPORT.PrintPDFSuitelet(request, response).run(creditableWTaxCodeIds);
            break;
            
		case _App._ID_DEPLOYMENT_DAT:
            new SAWT_REPORT.DATOutputSuitelet(request, response).run(creditableWTaxCodeIds);
            break;
		    
        default:
            nlapiLogExecution('ERROR', 'invalid deployment context:',nlapiGetContext().getDeploymentId());
		    break;
	}
}

function _isOneWorldInstance() 
{
    return context.getSetting("FEATURE", "SUBSIDIARIES") === "T";
}

function formatDateMMDDYYYY(d, sep)
{
    function zeroPadIfLessThan10(n) {
        return (n < 10) ? ('0' + n) : ('' + n);
    }
    
    var result = [];
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    
    result.push(zeroPadIfLessThan10(month));
    result.push(zeroPadIfLessThan10(day));
    result.push(zeroPadIfLessThan10(year));
    return result.join(sep);
}

SAWT_REPORT.SortFunction = function(lhs, rhs)
{
    if (lhs.IsPerson())
    {
        if (rhs.IsPerson())
        {
            //Person to person comparison
            var lhsLastName = lhs.getLastName().toUpperCase();
            var rhsLastName = rhs.getLastName().toUpperCase();
            if (lhsLastName < rhsLastName)
                return -1;
            else if (lhsLastName > rhsLastName)
                return 1;
            else
            {
                var lhsFirstName = lhs.getFirstName().toUpperCase()
                var rhsFirstName = rhs.getFirstName().toUpperCase();
                if (lhsFirstName < rhsFirstName)
                    return -1;
                else if (lhsFirstName > rhsFirstName)
                    return 1;
                else
                {
                    var lhsMidName = lhs.getMiddleName().toUpperCase();
                    var rhsMidName = rhs.getMiddleName().toUpperCase();
                    if (lhsMidName < rhsMidName)
                        return -1;
                    else if (lhsMidName > rhsMidName)
                        return 1;
                    else
                        return 0;
                }
            }
        }
        else
            return 1;
    }
    else
    {
        if (rhs.IsPerson())
            return -1;
        else
        {
            //Corporation to corporation comparison
            var lhsRegName = lhs.getRegName().toUpperCase()
            var rhsRegName = rhs.getRegName().toUpperCase();
            return lhsRegName < rhsRegName ? -1 :
                       lhsRegName > rhsRegName ? 1 : 0;
        }
    }
};

SAWT_REPORT.UIBuilderSuitelet = function(request, response)
{
	var _subId = '';
	var _nexusId = '';

    function initializeForm(taxPeriodId)
    {
    	var subIdsThatWTaxAppliesTo = _4601.getSubsidiaryIdsThatWTaxAppliesTo();
    	
        var form;
        var idSubsidiary = '';

        scriptId = nlapiGetContext().getScriptId();
        if (_isOneWorldInstance())
        	_subId = _isOneWorldInstance() ? isNaN(parseInt(request.getParameter('custpage_subsidiary'))) ? null : request.getParameter('custpage_subsidiary') : null;
        else
        	_nexusId = request.getParameter('custpage_nexus') == null ? null : request.getParameter('custpage_nexus');
        
        form = nlapiCreateForm('Withholding Tax Reports');
        form.setScript('customscript_sawt_client');
        
		// Subsidiary
		var countryCode = null;
        if (_isOneWorldInstance()) {
            idSubsidiary = getIdSubsidiaryForOneWorld();
            var subsComboboxConfig = new _4601.SubsidiaryComboboxConfiguration(
                subIdsThatWTaxAppliesTo,
                'custpage_subsidiary',
                'Subsidiary',
                idSubsidiary,
                'outsideabove',
                'startrow');
            var objSubsidiaryCombo = new _4601.FilteredSubsidiaryCombobox(form, subsComboboxConfig);
            objSubsidiaryCombo.setMandatory(true);
            countryCode = SFC.System.getSubsidiaryCountry(idSubsidiary);
        } else {
        	var nexusComboBoxParam = {
					fieldName: 'custpage_nexus', 
					fieldLabel: 'Nexus', 
					idSelectedDefault: _nexusId, 
					fieldLayoutType: 'outsideabove',
					fieldBreakType: 'startrow'
			};
			
			var objNexusCombo = new _4601.NexusCombobox(form, nexusComboBoxParam);
			objNexusCombo.setMandatory(true);
			countryCode = _nexusId;
		}
        
    	var subsidiary = _isOneWorldInstance() ? subIdsThatWTaxAppliesTo : _subId;
    	var fiscalCalendarId = _4601.getFiscalCalendar(_subId);
    	
    	var _reportId = _reportId == null ? 'sawt' : _request.getParameter('custpage_countryreport');
		var relatedReport = {country: countryCode};
		var isReportAvailable = new SFC.System.ReportFormCombobox(form, 'custpage_countryreport', 'Reports', _reportId, 'outsideabove', null, relatedReport);
		
        // Tax Period
        var taxPeriodComboBox = new SFC.System.TaxPeriodMonthQuarterCombobox(
    			form,
    			'tax_period_id',
    			'Tax Period',
    			taxPeriodId,
    			'outsideabove',
    			_isOneWorldInstance() ? (!!_subId ? _subId : null) : (!!_nexusId ? _nexusId : null),
				fiscalCalendarId);
        taxPeriodComboBox.setMandatory(true);
        
        //add the WH Type field
    	var comboboxParam = {
    			validSubsidiaryIds: subsidiary, 
    			idSelectedDefault: null, 
    			fieldSelectType: 'multiselect',
    			fieldName: 'custpage_taxtype',
    			fieldLabel: 'Tax Type',
    			fieldLayoutType: 'normal',
    			fieldBreakType: 'startrow',
    			subId: _isOneWorldInstance() ? idSubsidiary : '',
    	    	nexusId: _isOneWorldInstance() ? '' : _nexusId
    	};
    	var objTaxTypeCombo = new _4601.WHTaxTypeCombobox(form, comboboxParam);
    	objTaxTypeCombo.setMandatory(true);
        
		var refreshUrl = nlapiResolveURL("SUITELET", _App._SCRIPT_ID, _App._ID_DEPLOYMENT_UI);
		_App.CreateField(form, "urlrefresh", "text", "", false, refreshUrl, "hidden", "normal");
        
        var btnSubmit = form.addSubmitButton('Submit');
        
        if (isReportAvailable.getSelectOptions().length <= 0) {
			//Disable buttons
			btnSubmit.setDisabled(true);
			
            var errorMessage = 
                ["<br><div id='div__errormessage'/><script>showAlertBox('div__errormessage', '", 
                 SAWT_REPORT.REPORT_NOT_AVAILABLE_HEADER, "','",
                 SAWT_REPORT.REPORT_NOT_AVAILABLE_MESSAGE,
                 "', NLAlertDialog.TYPE_LOW_PRIORITY, '825px', '', '', false);</script>"].join("");
            _App.CreateField(form, "custpage_errormessage", "inlinehtml", "", false, errorMessage, "normal", "outsidebelow", "startrow");
		}

        return form;
    }

    function getIdSubsidiaryForOneWorld()
    {
        var idSubsidiary;
        if (isNaN(parseInt(request.getParameter('custpage_subsidiary'), 10)))
            idSubsidiary = nlapiGetContext().getSubsidiary();
        else
            idSubsidiary = request.getParameter('custpage_subsidiary');
        return idSubsidiary;
    }

    this.run = function() {
        var taxPeriodFromId = new SFC.System.TaxPeriod().GetCurrentPeriod();
        var subsidiaryId = null; // for OneWorld only
        var taxTypeId = null;
        var is_refresh = request.getParameter('refresh');
        var nexusId = null;
        var taxPeriodId = null;

        var form;
        //from submit of parameters
        if (request.getMethod() === 'POST' || is_refresh == 'y') {
            taxPeriodId = !!request.getParameter('tax_period_id') ? request.getParameter('tax_period_id') : taxPeriodFromId.GetId();
            
            taxTypeId = request.getParameter('custpage_taxtype');
			
            if (_isOneWorldInstance())
                subsidiaryId = request.getParameter('custpage_subsidiary');
            else
            	nexusId = request.getParameter('custpage_nexus') == null ? nlapiCreateRecord("customrecord_4601_companyinfoloader").getFieldValue("custrecord_4601_country") : request.getParameter('custpage_nexus');
            
            if (is_refresh != 'y') {
                // if not initial load of form
                if (taxPeriodId != null && taxTypeId != null) {
                    var paramMap = {};
                    paramMap['tax_period_id'] = taxPeriodId;
                    if (_isOneWorldInstance())
                    	paramMap['custpage_subsidiary'] = subsidiaryId;
                    else
                    	paramMap['custpage_nexus'] = nexusId;
                    paramMap['custpage_taxtype'] = taxTypeId;
                    
                    nlapiSetRedirectURL('SUITELET', nlapiGetContext().getScriptId(), _App._ID_DEPLOYMENT_LIST_UI, false, paramMap);
                }
            }
        //initial load of form
        } else {
        	taxPeriodId = request.getParameter('tax_period_id');
            taxTypeId = request.getParameter('custpage_taxtype');
            
            if (!taxPeriodId && taxPeriodFromId != null)
            	taxPeriodId = taxPeriodFromId.GetId();

            if (_isOneWorldInstance())
                subsidiaryId = getIdSubsidiaryForOneWorld();
        }
        
        form = initializeForm(taxPeriodId);

        response.writePage(form);
    };
};

SAWT_REPORT.CSVOutputSuitelet = function(request, response)
{
    this.run = function(allowedWithholdingTaxCodeIds)
    {
        var taxPeriodId = request.getParameter("tax_period_id");
        var subsidiaryId = request.getParameter('custpage_subsidiary');
        var nexusId = request.getParameter('custpage_nexus');
        var fiscalCalendarId = _4601.getFiscalCalendar(subsidiaryId);
        
        var sawtLineItemsWithTotal = SAWT_REPORT.getCustomerData(allowedWithholdingTaxCodeIds, {
	            periodfrom: taxPeriodId,
	            periodto: taxPeriodId,
	            subid: subsidiaryId, 
	            nexusid: nexusId,
	            fiscalCalendarId: fiscalCalendarId },
            searchType);
			
		response.setContentType('CSV', 'sawt.csv', 'attachment');

        var headerValues = _4601.HeaderData(subsidiaryId, taxPeriodId, searchType);

        sawtLineItemsWithTotal.setReturnPeriodValueForAllLineItems(headerValues.returnPeriodFromDate, headerValues.returnPeriodToDate);
		 _WriteHeader(response, headerValues);
		 
		var grandTotal = { Income: sawtLineItemsWithTotal.getSumTaxBase(), TaxWithheld: sawtLineItemsWithTotal.getSumTaxWithheld()};
		 
        var sawtLineItems = sawtLineItemsWithTotal.getSawtLineItems();
		sawtLineItems.sort(SAWT_REPORT.SortFunction);
		
        for (var i = 0; i < sawtLineItems.length; ++i)
            response.writeLine(sawtLineItems[i].toCSV(i + 1, searchType));

        _WriteFooter(response, grandTotal);
    };

    function _WriteHeader(response, headerValues)
    {
        response.writeLine("SUMMARY ALPHALIST OF WITHHOLDING TAXES (SAWT)");
        response.writeLine("\"FOR THE PERIOD: " + headerValues.returnPeriodFromDate + " TO " + headerValues.returnPeriodToDate + "\"");
        response.writeLine("");
        response.writeLine("TIN: " + headerValues.payeeTin);
        response.writeLine(_4601.escapeTextForCSV("PAYEE'S NAME: " + headerValues.payeeRegisteredName));
        response.writeLine("");
        response.writeLine("SEQ NO,TAXPAYER IDENTIFICATION NUMBER,CORPORATION (Registered Name),LAST NAME,FIRST NAME,MIDDLE NAME,ATC CODE,NATURE OF PAYMENT,AMOUNT OF INCOME PAYMENT,TAX RATE,AMOUNT OF TAX WITHHELD");
        response.writeLine("'(1),'(2),'(3),'(4),,,'(5),'(6),'(7),,'(8)");
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,,,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
    }

    function _WriteFooter(response, grandTotal)
    {
        response.writeLine("-----------------------------,-----------------------------,-----------------------------,,,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------,-----------------------------");
        response.writeLine("GRAND TOTAL,,,,,,,,,," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.Income)) + ",," + _4601.escapeTextForCSV(_4601.formatCurrency(grandTotal.TaxWithheld)));
		response.writeLine("");
        response.writeLine("END OF REPORT");
    }
};

SAWT_REPORT.DATOutputSuitelet = function(request, response) {
    
    this.run = function(allowedWithholdingTaxCodeIds)
    {
        var taxPeriodId = request.getParameter("tax_period_id");
        var subsidiaryId = request.getParameter('custpage_subsidiary');
        var nexusId = request.getParameter('custpage_nexus');
        
        var fiscalCalendarId = _4601.getFiscalCalendar(subsidiaryId);
        var periodObj = new SFC.System.TaxPeriod().Load(taxPeriodId, fiscalCalendarId);
        var formType = '2550M';
        if (periodObj.GetType() == 'quarter') {
            formType = '2550Q';
        }   
        
        var params = {
            wtaxCodeIds: allowedWithholdingTaxCodeIds,
            periodId: taxPeriodId,
            subId: subsidiaryId, 
            nexusId: nexusId,
            searchType: searchType,
            fiscalCalendarId: fiscalCalendarId,
            formType: formType,
            adapter: null,
            ds: null, 
            header: null, 
            formatter: null,
            taxSetupDao: null
        };
        
        WTax.injector.addObject('ds', SAWT_REPORT);
        WTax.injector.addObject('header', _4601);
        WTax.injector.addObject('formTypeCode', formType);
        WTax.injector.addObject('adapter', WTax.injector.getInstance(WTax.SawtReport.Adapter));
        WTax.injector.addClass('formatter', WTax.SawtReport.Formatter);
        WTax.injector.addClass('taxSetupDao', WTax.TaxSetupDao);
        WTax.injector.addObject('params', WTax.injector.getObject(params));
        
        var controller = WTax.injector.getInstance(WTax.SawtReport.Controller);
        var output = controller.Run();
        response.setContentType(output.contentType, output.fileName);
        response.write(output.contents);
    };
};



//=============================================================================
SAWT_REPORT.ListViewSuitelet = function(request, response)
{
    this.run = function(allowedWithholdingTaxCodeIds)
    {
        var taxPeriodId = request.getParameter('tax_period_id');
        var taxTypeId = request.getParameter('custpage_taxtype');
        
        var subId = '';
        var nexusId = null;
        if (_isOneWorldInstance()) {
            subId = request.getParameter('custpage_subsidiary');
            if (isNaN(parseInt(subId, 10))) {
                nlapiLogExecution('ERROR', 'InvalidArgument', 'subId must be a number : ' + subId);
                throw { name: 'InvalidArgument', message: 'subId must be a number : ' + subId };
            }
        } else {
            nexusId = request.getParameter('custpage_nexus');
        }
        
        if (isNaN(parseInt(taxPeriodId, 10))) {
            nlapiLogExecution('ERROR', 'InvalidArgument', 'taxPeriodId must be a number : ' + taxPeriodId);
            throw {name: 'InvalidArgument', message: 'taxPeriodId must be a number : ' + taxPeriodId };
        }

        var headerValues = _4601.HeaderData(subId, taxPeriodId, searchType);
        var list = nlapiCreateList('SAWT From ' + headerValues.returnPeriodFromDate + ' To ' + headerValues.returnPeriodToDate);
        
        var resourceObject = _4601.getResourceManager();

        var scriptId = nlapiGetContext().getScriptId();
        list.addColumn('seq_no', 'text', "SEQ NO", 'right');
        list.addColumn('tin', 'text', "TAXPAYER IDENTIFICATION NUMBER", 'LEFT');
        list.addColumn('reg_name', 'text', "CORPORATION (Registered Name)", 'LEFT');
        list.addColumn('ind_name', 'text', "INDIVIDUAL (Last Name, First Name, Middle Name)", 'LEFT');
        list.addColumn('atc', 'text', "ATC CODE", 'LEFT');
        list.addColumn('nature', 'text', "NATURE OF PAYMENT", 'LEFT');
        list.addColumn('tax_base', 'currency', "AMOUNT OF INCOME PAYMENT", 'right');
        list.addColumn('tax_rate', 'currency', "TAX RATE", 'right');
        list.addColumn('tax_withheld', 'currency', "AMOUNT OF TAX WITHHELD", 'right');

        var printUrlComps = [];
        printUrlComps.push('window.open(');
        printUrlComps.push("'");
        printUrlComps.push(nlapiResolveURL('SUITELET', scriptId, _App._ID_DEPLOYMENT_PDF));
        printUrlComps.push('&tax_period_id=', taxPeriodId);
        printUrlComps.push(_isOneWorldInstance() ? '&custpage_subsidiary=' + subId : '&custpage_nexus=' + nexusId);
        printUrlComps.push('&custpage_taxtype=', taxTypeId);
        printUrlComps.push("'");
        printUrlComps.push(');');

        list.addButton('btn_print_sawt', resourceObject.REPORT['button'].PRINT_PDF.label, printUrlComps.join(''));

        var csvUrlComps = [];
        csvUrlComps.push('window.open(');
        csvUrlComps.push("'");
        csvUrlComps.push(nlapiResolveURL('SUITELET', scriptId, _App._ID_DEPLOYMENT_CSV));
        csvUrlComps.push('&tax_period_id=', taxPeriodId);
        csvUrlComps.push(_isOneWorldInstance() ? '&custpage_subsidiary=' + subId : '&custpage_nexus=' + nexusId);
        csvUrlComps.push('&custpage_taxtype=', taxTypeId);
        csvUrlComps.push("'");
        csvUrlComps.push(');');

        list.addButton('btn_to_csv', resourceObject.REPORT['button'].SAVE_CSV.label, csvUrlComps.join(''));

        var datUrlComps = [];
        datUrlComps.push('window.open(');
        datUrlComps.push("'");
        datUrlComps.push(nlapiResolveURL('SUITELET', scriptId, _App._ID_DEPLOYMENT_DAT));
        datUrlComps.push('&tax_period_id=', taxPeriodId);
        datUrlComps.push(_isOneWorldInstance() ? '&custpage_subsidiary=' + subId : '&custpage_nexus=' + nexusId);
        datUrlComps.push('&custpage_taxtype=', taxTypeId);
        datUrlComps.push("'");
        datUrlComps.push(');');

        list.addButton('btn_to_dat', resourceObject.REPORT['button'].EXPORT_DAT.label, datUrlComps.join(''));

        var returnToFormUrlComps = [];
        returnToFormUrlComps.push('window.location =');
        returnToFormUrlComps.push("'");
        returnToFormUrlComps.push(nlapiResolveURL('SUITELET', scriptId, _App._ID_DEPLOYMENT_UI));
        returnToFormUrlComps.push('&tax_period_id=', taxPeriodId);
        if (_isOneWorldInstance())
        	returnToFormUrlComps.push('&custpage_subsidiary=', subId);
        else
        	returnToFormUrlComps.push('&custpage_nexus=', nexusId);
        returnToFormUrlComps.push("';");

        list.addButton('restart', resourceObject.REPORT['button'].RETURN_TO_CRITERIA.label, returnToFormUrlComps.join(''));
        
        var fiscalCalendarId = _4601.getFiscalCalendar(subId);
        var sawtLineItemsWithTotals = SAWT_REPORT.getCustomerData(allowedWithholdingTaxCodeIds, {
	        	periodfrom: taxPeriodId,
	        	periodto: taxPeriodId,
	        	subid: subId, 
	        	nexusid: nexusId,
	        	taxType: taxTypeId, 
	        	fiscalCalendarId: fiscalCalendarId}, 
        	searchType);
        sawtLineItemsWithTotals.setReturnPeriodValueForAllLineItems(headerValues.returnPeriodFromDate, 
        	headerValues.returnPeriodToDate);
        
        var sawtLineItems = sawtLineItemsWithTotals.getSawtLineItems();
        sawtLineItems.sort(SAWT_REPORT.SortFunction);

        for (var i = 0; i < sawtLineItems.length; ++i) {
            try {
                list.addRow(sawtLineItems[i].toListRow(i + 1, searchType));
            } catch (e) {
                nlapiLogExecution('ERROR', 'SAWT_REPORT.ListViewSuitelet list ui', 'failed to add row ' + i);
            }
        }
        
        response.writePage(list);
    };
};

SAWT_REPORT.PrintPDFSuitelet = function(request, response)
{
    this.run = function(allowedWithholdingTaxCodeIds)
    {
        var taxPeriodId = request.getParameter("tax_period_id");
        var taxTypeId = request.getParameter("custpage_tax_type");
        var subsidiaryId = request.getParameter('custpage_subsidiary');
        var nexusId = request.getParameter('custpage_nexus');
        var fiscalCalendarId = _4601.getFiscalCalendar(subsidiaryId);
        
        var sawtLineItemsWithTotal = SAWT_REPORT.getCustomerData(allowedWithholdingTaxCodeIds, {
	            periodfrom: taxPeriodId,
	            periodto: taxPeriodId,
	            subid: subsidiaryId,
	            nexusid: nexusId,
	            taxTypeId: taxTypeId, 
	            fiscalCalendarId: fiscalCalendarId},
            searchType);
            
        var headerValues = _4601.HeaderData(subsidiaryId, taxPeriodId, searchType);
        
        sawtLineItemsWithTotal.setReturnPeriodValueForAllLineItems(headerValues.returnPeriodFromDate, 
    		headerValues.returnPeriodToDate);

        var sawtLineItems = sawtLineItemsWithTotal.getSawtLineItems();
        sawtLineItems.sort(SAWT_REPORT.SortFunction);
        var pdfTemplate = _App.GetFileContent(_App._ROW_PRINT_TEMPLATE, true);
        var row_pdfTemplate = _App.GetFileContent(_App._ROW_TEMPLATE, true);

        var reportRows = [];
        for (var i = 0; i < sawtLineItems.length; ++i)
            reportRows.push(_App.RenderTemplate(row_pdfTemplate, sawtLineItems[i].toXMLEscapedRow(i + 1, searchType)));

        var s = sawtLineItems.length;
        
        var helperUtils = new _4601.HelperUtils(_App, request);    	
    	var arabicfont = nlapiEscapeXML(helperUtils.getFullFileUrl("koodak.ttf"));

        var attributes = {
            arabicfont: arabicfont,
            payeeRegisteredName: _4601.escapeTextForPDF(headerValues.payeeRegisteredName),
            payeeTradeName: _4601.escapeTextForPDF(headerValues.payeeTradeName),
            payeeAddress: _4601.escapeTextForPDF(headerValues.payeeAddress),
            payeeTin: _4601.escapeTextForPDF(headerValues.payeeTin),
            returnPeriodFromDate: nlapiEscapeXML(headerValues.returnPeriodFromDate),
            returnPeriodToDate: nlapiEscapeXML(headerValues.returnPeriodToDate),
            PeriodStartDate: headerValues.returnPeriodFromDate,
            PeriodEndDate: headerValues.returnPeriodToDate,
            lineItems: reportRows.join(""),
            sumTaxBase: _4601.formatCurrency(sawtLineItemsWithTotal.getSumTaxBase()),
            sumTaxWithheld: _4601.formatCurrency(sawtLineItemsWithTotal.getSumTaxWithheld()),
            debug:s
        };

        var content = _App.RenderTemplate(pdfTemplate, attributes);

        try {
            var pdfFile = nlapiXMLToPDF('<?xml version="1.0"?>\n' + '<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);
            response.setContentType("PDF", 'sawt.pdf', 'attachment');
            response.write(pdfFile.getValue());
        } catch (e) {
            var arrMsg = [];
            response.setContentType("PLAINTEXT", '', 'inline');
            if (e instanceof nlobjError) {
                arrMsg.push(e.getCode() + ':\n' + e.getDetails() + '\n');
                arrMsg.push(content);
            } else {
                arrMsg.push(e.name + ':\n');
                arrMsg.push(e.message + '\n');
                arrMsg.push(content);
            }
            var logException = true;
            if (logException)
                nlapiLogExecution('ERROR', 'PDF generation', arrMsg.join());
            else
                response.write('error:' + arrMsg.join());
        }
    };
};
