/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.MapReport = WTax.MapReport || {};
WTax.MapReport.FieldDefinitions = function _FieldDefinitions() {
    // Do nothing
};
WTax.MapReport.FieldDefinitions.prototype.getFields = function _getFields() {
    var fields = {
        alphaType: {
            type: WTax.FieldTypes.TEXT,
            width: 5
        },
        formTypeCode: {
            type: WTax.FieldTypes.TEXT,
            width: 6
        },
        wtaxAgentTIN: {
            type: WTax.FieldTypes.TEXT,
            width: 9
        },
        wtaxAgentBranchCode: {
            type: WTax.FieldTypes.INTEGER_PADDED,
            width: 4
        },
        wtaxAgentRegName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 50
        },
        returnPeriod: {
            type: WTax.FieldTypes.DATE_SEPARATOR,
            width: 7
        },
        returnPeriodFileName: {
            type: WTax.FieldTypes.DATE,
            width: 6
        },
        wtaxAgentRDOCode: {
            type: WTax.FieldTypes.INTEGER_PADDED,
            width: 3
        },
        sequenceNumber: {
            type: WTax.FieldTypes.INTEGER,
            width: 8
        },
        payeeTIN: {
            type: WTax.FieldTypes.TEXT,
            width: 9
        },
        payeeBranchCode: {
            type: WTax.FieldTypes.INTEGER_PADDED,
            width: 4
        },
        payeeRegName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 50
        },
        payeeLastName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        payeeFirstName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        payeeMiddleName: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 30
        },
        atcCode: {
            type: WTax.FieldTypes.TEXT,
            width: 5
        },
        natureOfIncomePayment: {
            type: WTax.FieldTypes.TEXT_QUOTES,
            width: 50
        },
        taxRate: {
            type: WTax.FieldTypes.DECIMAL,
            width: 5
        },
        taxBase: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        },
        taxWithheld: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        },
        totalTaxBase: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        },
        totalTaxWithheld: {
            type: WTax.FieldTypes.DECIMAL,
            width: 14
        }
    };
    return fields;
};
function _Formatter() {
    var fieldDef = new WTax.MapReport.FieldDefinitions();
    this.fields = fieldDef.getFields();
    this.TEMPLATE = {};
    this.TEMPLATE.HEADER = '{alphaType},{formTypeCode},{wtaxAgentTIN},{wtaxAgentBranchCode},{wtaxAgentRegName},{returnPeriod},{wtaxAgentRDOCode}\n';
    this.TEMPLATE.DETAILS = '{alphaType},{formTypeCode},{sequenceNumber},{payeeTIN},{payeeBranchCode},{payeeRegName},{payeeLastName},{payeeFirstName},{payeeMiddleName},{returnPeriod},{atcCode},{natureOfIncomePayment},{taxRate},{taxBase},{taxWithheld}\n';
    this.TEMPLATE.CONTROL = '{alphaType},{formTypeCode},{wtaxAgentTIN},{wtaxAgentBranchCode},{returnPeriod},{totalTaxBase},{totalTaxWithheld}';
    this.TEMPLATE.FILE_NAME = '{wtaxAgentTIN}{wtaxAgentBranchCode}{returnPeriodFileName}{formTypeCode}.dat';
}
WTax.MapReport.Formatter = _Formatter;
WTax.MapReport.Formatter.prototype = new WTax.ReportFormatter();
WTax.MapReport.Formatter.prototype.constructor = _Formatter; 

WTax.MapReport.Formatter.prototype.formatFileName = function _formatFileName(fileNameObj) {
    return this.formatElement(fileNameObj, this.TEMPLATE.FILE_NAME);
};
WTax.MapReport.Formatter.prototype.format = function _format(mapData) {
    var output = '';
    output += this.formatElement(mapData.header, this.TEMPLATE.HEADER);
    for (var i = 0; i < mapData.details.length; i++) {
        output += this.formatElement(mapData.details[i], this.TEMPLATE.DETAILS);
    }
    output += this.formatElement(mapData.control, this.TEMPLATE.CONTROL);
    return output;
};