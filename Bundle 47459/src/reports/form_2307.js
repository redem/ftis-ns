/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description Here
 * You are using the default templates which should be customized to your needs.
 * You can change your user name under Preferences->NetSuite Plugin->Code Templates.
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Mar 2012     ljamolod
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @return {void} Any output is written via response object
 */

if (!form2307) { var form2307 = {}; }

if (!_4601) { var _4601 = {}; }

var _App = new SFC.System.Application("c54f4fb9-a81f-40e0-bf8f-ae7d00f57073");
var _TaxPeriod = new SFC.System.TaxPeriod();

_App._DEPLOY_ID_PRINTING = "customdeploy_form_2307_print";
_App._DEPLOY_ID_ONLINE = "customdeploy_form_2307_online";

_App.BIR_LOGO_FILENAME = 'wi_tax_print_logo.gif';
_App.BLACK_PIXEL_FILENAME = 'wi_tax_black_pixel.gif';

form2307.REPORT_NOT_AVAILABLE_HEADER = 'No Report Available';
form2307.REPORT_NOT_AVAILABLE_MESSAGE = 'The withholding tax report for this country is not currently supported in NetSuite.';

function main( request, response ){
	var deployId, dataProvider, helperUtils, 
        creditableWTaxCodeIds;
    
	
	var _taxTypeId = request.getParameter('custpage_taxtype');
	creditableWTaxCodeIds = (_taxTypeId == null) ? {} : _4601.WithholdingTaxCodeFetcher().getTaxCodesFilteredByType(_taxTypeId);
	
	deployId = _App.Context.getDeploymentId();
	helperUtils = new _4601.HelperUtils(_App, request);
	switch( deployId ){
		case _App._DEPLOY_ID_ONLINE :
			new form2307.loadOnline(request, response, helperUtils, creditableWTaxCodeIds);
		break;
		
		case _App._DEPLOY_ID_PRINTING :
			new form2307.loadPdf(request, response, helperUtils, creditableWTaxCodeIds);
		break;
		
		default: 
			nlapiLogExecution('ERROR',
                'Invalid Deployment Id:',
				deployId);
            throw {
                name: 'Invalid deployment ID',
				message: 'System error: Form 2307 - Invalid deployment ID'};
	}
}


form2307.loadOnline = (function () {
	function loadOnline(request, response, helperUtils, creditableWTaxCodeIds) {
		loadOnline._Context = nlapiGetContext();
		loadOnline._IsOneWorld = loadOnline._Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
		loadOnline._request = request;
		loadOnline._subId = null;
		loadOnline._vendorId = null;
		loadOnline._taxPeriodQuarter = null;
		loadOnline._taxTypeId = null;
		loadOnline._nexusId = null;
		
		loadOnline.run(helperUtils, creditableWTaxCodeIds, response);
	}
	
	loadOnline.run = function(helperUtils, creditableWTaxCodeIds, response) { //Refresh
		var is_post_back = loadOnline._request.getMethod() == 'POST' ? true : false;
		var is_refresh = loadOnline._request.getParameter('refresh');
		var data = {};
		var vendorInfo = null;
		var currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod();
		var countryCode = null;
		
		if(loadOnline._IsOneWorld)
			loadOnline._subId = isNaN(parseInt(loadOnline._request.getParameter('custpage_subsidiary'))) ? null : loadOnline._request.getParameter('custpage_subsidiary');
		else
			loadOnline._nexusId = loadOnline._request.getParameter('custpage_nexus') == null ? null : loadOnline._request.getParameter('custpage_nexus');
		
		var fiscalCalendarId = _4601.getFiscalCalendar(loadOnline._subId);
		
		if(is_post_back  || is_refresh == 'y'){
			loadOnline._vendorId = loadOnline._request.getParameter('custpage_vendor');
			loadOnline._taxPeriodQuarter = !!loadOnline._request.getParameter('custpage_taxperiod') ? loadOnline._request.getParameter('custpage_taxperiod') : currentPeriod.GetId();
			
			if ((is_refresh != 'y') && (loadOnline._vendorId != "")) {
				loadOnline._taxTypeId = loadOnline._request.getParameter('custpage_taxtype');
			
				var params = {vendorId:loadOnline._vendorId, 
						subId:loadOnline._subId, 
						taxperiod:loadOnline._taxPeriodQuarter, 
						taxTypeId: loadOnline._taxTypeId,
						fiscalCalendarId: fiscalCalendarId};
				var collatedResults = form2307.getData(creditableWTaxCodeIds, params);
				data = form2307.formatForReport(collatedResults);
				vendorInfo = form2307.getPayeeInfo(loadOnline._vendorId);
			}
		} else { //Initial Display
			if (currentPeriod == null){
				currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod();	
            }
			loadOnline._taxPeriodQuarter = currentPeriod.GetId();
		}
		
		var form = nlapiCreateForm('Withholding Tax Reports');
		var pageData = {};

		pageData.or_payee_name = '';
		pageData.or_payee_tin = '';
		pageData.or_payee_address = '';
		pageData.or_payee_zip = '';

		if ((is_post_back) && (loadOnline._vendorId != "")) {
			var or_payeeName = form.addField('custpage_or_payeename', 'text');
			or_payeeName.setDefaultValue( vendorInfo.name );
			or_payeeName.setDisplayType('hidden');
			pageData.or_payee_name = vendorInfo.name;
			
			var or_payeeTin = form.addField('custpage_or_payeetin', 'text');
			or_payeeTin.setDisplayType('hidden');
			or_payeeTin.setDefaultValue( vendorInfo.tin );
			pageData.or_payee_tin = _4601.FormatTIN(vendorInfo.tin);
			
			var or_payee_reg_address = form.addField('custpage_payee_reg_address', 'textarea');
			or_payee_reg_address.setDefaultValue( vendorInfo.regAddr );
			or_payee_reg_address.setDisplayType('hidden');
			pageData.or_payee_address = vendorInfo.regAddr;
			
			var or_payeezip = form.addField('custpage_or_payeezip', 'text', 'Zip Code');
			or_payeezip.setDefaultValue(vendorInfo.zip);
			or_payeezip.setDisplayType('hidden');
			pageData.or_payee_zip = vendorInfo.zip;

			var or_payee_reg_faddress = form.addField('custpage_payee_freg_address', 'textarea', 'Foreign Address');
			or_payee_reg_faddress.setDisplayType('hidden');
			
			var or_payeeFzip = form.addField('custpage_or_payeefzip', 'text', 'Foreign Zip Code');
			or_payeeFzip.setDisplayType('hidden');
			or_payeeFzip.setDisplaySize(4);
		}

        pageData.PhBirLogoUrl = helperUtils.getImageFullUrl(_App.BIR_LOGO_FILENAME);
		form.setScript('customscript_form_2307_client');
		
		var btnRefresh = form.addSubmitButton('Refresh');
		
		var printUrl = nlapiResolveURL("SUITELET", _App.Context.getScriptId(), _App._DEPLOY_ID_PRINTING);
		var btnPrint = form.addButton('printButt', 'Print','form_2307_printForm()');

		_App.CreateField(form, "urlprint", "text", "", false, printUrl, "hidden", "normal");
		
		var refreshUrl = nlapiResolveURL("SUITELET", _App.Context.getScriptId(), _App._DEPLOY_ID_ONLINE);
		_App.CreateField(form, "urlrefresh", "text", "", false, refreshUrl, "hidden", "normal");
		
		if( loadOnline._IsOneWorld ){
            var subsComboboxConfig = new _4601.SubsidiaryComboboxConfiguration(
            	_4601.getSubsidiaryIdsThatWTaxAppliesTo(),
                'custpage_subsidiary',
                'Subsidiary',
                loadOnline._subId,
                'outsideabove');
            new _4601.FilteredSubsidiaryCombobox(form, subsComboboxConfig);
            var defaultSubId = isNaN(parseInt(loadOnline._request.getParameter('custpage_subsidiary'))) ? null : loadOnline._request.getParameter('custpage_subsidiary');
            countryCode = SFC.System.getSubsidiaryCountry(defaultSubId);
		} else {
			var nexusComboBoxParam = {
					fieldName: 'custpage_nexus', 
					fieldLabel: 'Nexus',
					idSelectedDefault: loadOnline._nexusId, 
					fieldLayoutType: 'outsideabove',
					fieldBreakType: 'startrow'
			};
			
			var objNexusCombo = new _4601.NexusCombobox(form, nexusComboBoxParam);
			objNexusCombo.setMandatory(true);
			
			countryCode = loadOnline._nexusId;
		}
		
		loadOnline._reportId = loadOnline._reportId == null ? 'form2307' : loadOnline._request.getParameter('custpage_countryreport');
		var relatedReport = {country: countryCode};
		var isReportAvailable = new SFC.System.ReportFormCombobox(form, 'custpage_countryreport', 'Reports', loadOnline._reportId, 'outsideabove', null, relatedReport);

        // Tax Period
        var objTaxCombo = new SFC.System.TaxPeriodMonthQuarterCombobox(
        		form, 
        		"custpage_taxperiod", 
        		"Tax Period", 
        		loadOnline._taxPeriodQuarter, 
        		'outsideabove',
        		loadOnline._IsOneWorld ? (!!loadOnline._subId ? loadOnline._subId : null) : (!!loadOnline._nexusId ? loadOnline._nexusId : null),
        		fiscalCalendarId);
        objTaxCombo.setMandatory(true);
		
		new _4601.TaxTypeCombobox(form, {
				fieldName: 'custpage_taxtype', 
				label: 'Tax Type', 
				defaultId: loadOnline._taxTypeId, 
				layoutType: 'outsideabove', 
				subId: loadOnline._IsOneWorld ? loadOnline._subId : '',
				nexusId: loadOnline._IsOneWorld ? '' : loadOnline._nexusId}
		);
		
		if (isReportAvailable.getSelectOptions().length > 0) {
			var relatedVendor = {};
			if( loadOnline._IsOneWorld ){
				relatedVendor = {subId: loadOnline._subId};
			}else{
				relatedVendor = {nexusId: loadOnline._nexusId};
			}

			var isVendorAvailable = form.addField('custpage_vendor','select','Vendor','vendor');
			isVendorAvailable.setDefaultValue(loadOnline._vendorId);
			isVendorAvailable.setDisplayType('normal');
			isVendorAvailable.setLayoutType('outsideabove','startrow');
			
			var template = loadOnline.formatPageData( data, pageData, helperUtils );
			_App.CreateField(form, "reporttemplate", "inlinehtml", "", false, template, 'normal', 'outsideabove', 'startrow');
			
			if (isVendorAvailable.getSelectOptions().length <=0) {
				btnRefresh.setDisabled(true);
				btnPrint.setDisabled(true);
			}
		} else {
			//Disable buttons
			btnRefresh.setDisabled(true);
			btnPrint.setDisabled(true);
			
            var errorMessage = 
                ["<br><div id='div__errormessage'/><script>showAlertBox('div__errormessage', '", 
                 form2307.REPORT_NOT_AVAILABLE_HEADER, "','",
                 form2307.REPORT_NOT_AVAILABLE_MESSAGE,
                 "', NLAlertDialog.TYPE_LOW_PRIORITY, '825px', '', '', false);</script>"].join("");               
            _App.CreateField(form, "custpage_errormessage", "inlinehtml", "", false, errorMessage);
		}
		
		response.writePage(form);
	};
	
	loadOnline.formatPageData = function formatPageData( data, pageData, helperUtils ) {
		var rowtemplate = _App.GetFileContent("wi_tax_form_2307_template_online_row.htm", true);
		pageData.expandedWithholding = '';
		var rowData = data.expandedWithHolding;
		var formatted_row_data = {};
		
		if( rowData!=undefined && rowData.length > 0 ){
			for( var i in rowData){
				// format the output

				formatted_row_data.detail = rowData[i].detail == undefined ? '' : rowData[i].detail;
				formatted_row_data.atc = rowData[i].atc;
				formatted_row_data.qtr1 = _4601.formatCurrency( rowData[i].qtr1 == undefined ? 0 : rowData[i].qtr1  );
				formatted_row_data.qtr2 = _4601.formatCurrency(  rowData[i].qtr2 == undefined ? 0 : rowData[i].qtr2 );
				formatted_row_data.qtr3 = _4601.formatCurrency(rowData[i].qtr3 == undefined ? 0 : rowData[i].qtr3);
				formatted_row_data.total = _4601.formatCurrency(rowData[i].total);
				formatted_row_data.withheld = _4601.formatCurrency(rowData[i].withheld);
				
				pageData.expandedWithholding += _App.RenderTemplate(rowtemplate, formatted_row_data);
			}	
		}
		else{
			var emptyRowData = {detail:'&nbsp;', atc:'&nbsp;', qtr1:'&nbsp;', qtr2:'&nbsp;', qtr3:'&nbsp;',total:'&nbsp;', withheld:'&nbsp;'};
			for(var x=0; x<5 ;x++){
				pageData.expandedWithholding += _App.RenderTemplate(rowtemplate, emptyRowData);
			}
		}
		
		// prepare business tax rows
		rowData = data.businessTax;
		pageData.businesTaxRows = '';
		for (var i in rowData) {
			// format the output
			formatted_row_data.detail = rowData[i].detail;
			formatted_row_data.atc = rowData[i].atc;
			formatted_row_data.qtr1 = _4601.formatCurrency(rowData[i].qtr1);
			formatted_row_data.qtr2 = _4601.formatCurrency(   rowData[i].qtr2 == undefined ? 0 : rowData[i].qtr2   );
			formatted_row_data.qtr3 = _4601.formatCurrency(rowData[i].qtr3);
			formatted_row_data.total = _4601.formatCurrency(rowData[i].total);
			formatted_row_data.withheld = _4601.formatCurrency(rowData[i].withheld);
			
			pageData.businesTaxRows += _App.RenderTemplate(rowtemplate, formatted_row_data);
		}
		
		pageData.ex_qtr1_total = ''; 
		pageData.ex_qtr2_tota2 = '';
		pageData.ex_qtr3_tota3 = '';
		pageData.ex_all_total = '';
		pageData.ex_wh_total = '';
		if( data.totalsExpandedWh != undefined ){
			pageData.ex_qtr1_total = _4601.formatCurrency( data.totalsExpandedWh.qtr1 ); 
			pageData.ex_qtr2_tota2 = _4601.formatCurrency( data.totalsExpandedWh.qtr2 );
			pageData.ex_qtr3_tota3 = _4601.formatCurrency( data.totalsExpandedWh.qtr3 );
			pageData.ex_all_total = _4601.formatCurrency( data.totalsExpandedWh.alltotal );
			pageData.ex_wh_total = _4601.formatCurrency( data.totalsExpandedWh.totalwh );	
		}
		
    	pageData.arabicfont = nlapiEscapeXML(helperUtils.getFullFileUrl("koodak.ttf"));
		
		var div = ["<div id=\"form2307\">"];
		div.push("<br>");
        div.push(_App.RenderTemplate(_App.GetFileContent("wi_tax_form_2307_template_main.htm", true), pageData ));
        div.push("</div>");
		
		var template = div.join("");
		return template; 
	};
	
	return loadOnline;
}());



form2307.loadPdf = (function () {
	function loadPdf(request, response, helperUtils, creditableWTaxCodeIds) {
		loadPdf._Context = nlapiGetContext();
		loadPdf._IsOneWorld = loadPdf._Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
		
		loadPdf._FromPeriodId = null;;
		loadPdf._vendorId = null;
		loadPdf._subId = null;
		loadPdf._request = request;
		loadPdf._taxPeriodQuarter = null;
		
		loadPdf.run(helperUtils, creditableWTaxCodeIds, response);
	}
	
	
	loadPdf.getDateRangeDisplayArray = function getDateRangeDisplayArray( dateObject ) {
		var dateHolder = {};
		var dateFormattedArray = new Array();
		
		dateHolder.month = parseInt(dateObject.getMonth()) + parseInt(1);
		if( dateHolder.month < 10 ){
			dateHolder.month = '0'+dateHolder.month;
		}
		
		dateFormattedArray = dateFormattedArray.concat( dateHolder.month.toString().split('') );
		dateHolder.date = dateObject.getDate() ;
		if( dateHolder.date < 10){
			dateHolder.date = '0'+dateHolder.date;
		}

		dateFormattedArray = dateFormattedArray.concat( dateHolder.date.toString().split('') );
		dateHolder.year = dateObject.getFullYear() +"";
        dateHolder.year = (dateHolder.year).substring(2,4);
		dateFormattedArray = dateFormattedArray.concat( dateHolder.year.toString().split('') );
		return dateFormattedArray;
	};
	
	
	loadPdf.run = function run(helperUtils, creditableWTaxCodeIds, response) {
		var pageData = {};
		var headerData = {};
		var template = _App.GetFileContent("wi_tax_form_2307_print.xml", true);
		
		loadPdf._subId = loadPdf._request.getParameter('custpage_subsidiary');
		loadPdf._vendorId = loadPdf._request.getParameter('custpage_vendor');
		loadPdf._taxPeriodQuarter = loadPdf._request.getParameter('custpage_taxperiod') == null || loadPdf._request.getParameter('custpage_taxperiod') == undefined ? null : loadPdf._request.getParameter('custpage_taxperiod');
		var periodRecord = nlapiLoadRecord('taxperiod', loadPdf._taxPeriodQuarter);
		
		for (var fromPeriod = 0; fromPeriod < 6 ; fromPeriod++) {
			headerData['fromPeriod_' + fromPeriod] = 0;
		}
		
		for (var toPeriod = 0; toPeriod < 6 ; toPeriod++) {
			headerData['toPeriod_' + toPeriod] = 0;
		}
		
		if( periodRecord!=null ) {
			var startPeriodDate = nlapiStringToDate( periodRecord.getFieldValue('startdate') );
			var endPerioddate = nlapiStringToDate( periodRecord.getFieldValue('enddate') );
			var dateFormattedArray = loadPdf.getDateRangeDisplayArray( startPeriodDate );
			for( var from = 0; from < dateFormattedArray.length; from++ ){
				headerData['fromPeriod_' + from] = dateFormattedArray[from];
			} 
			
			dateFormattedArray = loadPdf.getDateRangeDisplayArray( endPerioddate );
			for( var to = 0; to < dateFormattedArray.length; to++ ){
				headerData['toPeriod_' + to] = dateFormattedArray[to];
			}
		}
		
		var company_record = _4601.getCompanyRecord(loadPdf._IsOneWorld, loadPdf._request.getParameter("custpage_subsidiary"));
		
		var payor_zip = company_record.getFieldValue("zip");
		var payor_tin = company_record.getFieldValue("federalidnumber") || company_record.getFieldValue("employerid");
		headerData.payor_name = nlapiEscapeXML(company_record.getFieldValue("legalname") || company_record.getFieldValue("name"));
		
		var comp_addr = company_record.getFieldValue("addr1") || company_record.getFieldValue("address1");
		var comp_addr2 = company_record.getFieldValue("addr2") || company_record.getFieldValue("address2");
		var comp_city = company_record.getFieldValue("city");
		var comp_state = company_record.getFieldText("dropdownstate") || company_record.getFieldValue("state");
				
		headerData.payor_address = [
            comp_addr ? comp_addr + " " : "",
            comp_addr2 ? comp_addr2 + " " : "",
            comp_city ? comp_city + " " : "",
            comp_state ? comp_state : ""
        ].join("");
		
		
		var params = {vendorId:loadPdf._vendorId, subId:loadPdf._subId, taxperiod:loadPdf._taxPeriodQuarter};
		var collatedResults = form2307.getData(creditableWTaxCodeIds, params);
		data = form2307.formatForReport(collatedResults);

		// format the TIN
		for( var payorTin = 0; payorTin <12 ; payorTin++ ){
			headerData['payor_tin_' + payorTin] = 0;
		}
		
		if( payor_tin != null && payor_tin != '' ){
			// sanitize
			payor_tin = payor_tin.replace(/[^0-9]/g, '');
			var payor_tin_arr = payor_tin.split(''); 
			
			for (var payorTinArr = 0; payorTinArr < payor_tin_arr.length; payorTinArr++){
				headerData['payor_tin_' + payorTinArr] = payor_tin_arr[payorTinArr];
			}
		}
		
		for( var payorZip = 0; payorZip <4 ; payorZip++ ){
			headerData['payor_zip_' + payorZip] = '';
		}
		
		if (payor_zip != null && payor_zip != '') {
			payor_zip = payor_zip.replace(/[^0-9]/g, '');
			var payor_zip_arr = payor_zip.split('');
			for( var payorZipArr = 0 ; payorZipArr < payor_zip_arr.length ; payorZipArr ++ ){
				headerData['payor_zip_' + payorZipArr] = payor_zip_arr[payorZipArr];
			} 
		}
		
		var vendorInfo = form2307.getPayeeInfo(loadPdf._vendorId);
		var rawFieldValue = loadPdf._request.getParameter('custpage_or_payeename');
		
		var filteredFieldValue = rawFieldValue == null || rawFieldValue == '' ? vendorInfo.name : rawFieldValue;
		
		headerData.payee_name = filteredFieldValue;
		
		rawFieldValue = loadPdf._request.getParameter('custpage_payee_reg_address');
		filteredFieldValue=  rawFieldValue == null || rawFieldValue == '' ? vendorInfo.regAddr : rawFieldValue;
		headerData.payee_address = filteredFieldValue; 
		
		rawFieldValue = loadPdf._request.getParameter('custpage_or_payeetin');
		filteredFieldValue = rawFieldValue == null || rawFieldValue == '' ? vendorInfo.tin : rawFieldValue;
		
		
		vendorVatNumber = filteredFieldValue;
		vendorVatNumber = vendorVatNumber.replace(/[^0-9]/g, '');
		
		var vatNoArr = vendorVatNumber.split('');
		var vatNoArrLen = vatNoArr.length;
		
		for( var payeeVatNo = 0; payeeVatNo < 12 ; payeeVatNo++ ){
			headerData['payeeVatNo_part_' + payeeVatNo] = 0;
		}
		
		if( vatNoArrLen > 0 ){
			for( var vNo =0 ; vNo < vatNoArrLen; vNo++){
				headerData['payeeVatNo_part_' + vNo] = vatNoArr[vNo];
			}	
		}
		
		for( var payeeZipNo = 0; payeeZipNo < 4 ; payeeZipNo ++ ){
			headerData['payeeZipNo_part_' + payeeZipNo] = '';
		}

		vendorZipCode = null;
		rawFieldValue = loadPdf._request.getParameter('custpage_or_payeezip');
		filteredFieldValue = rawFieldValue == null || rawFieldValue == '' ? vendorInfo.zip : rawFieldValue;
		vendorZipCode = filteredFieldValue;
		
		if (vendorZipCode != '') {
			// clean unwanted chars
			vendorZipCode = vendorZipCode.replace(/[^0-9]/g, '');
			var vendorZipCodeArr = vendorZipCode.split('');
			
			for (var pz = 0; pz < vendorZipCodeArr.length; pz++) {
				headerData['payeeZipNo_part_' + pz] = vendorZipCodeArr[pz];
			}
		}
		
		// foreign address
		rawFieldValue = loadPdf._request.getParameter('custpage_payee_freg_address');
		filteredFieldValue = rawFieldValue == null || rawFieldValue == '' ? '' : rawFieldValue;
		headerData.foreign_address = filteredFieldValue; 
		
		// foreign address zip code
		rawFieldValue = loadPdf._request.getParameter('custpage_or_payeefzip');
		filteredFieldValue = rawFieldValue == null || rawFieldValue == '' ? '' : rawFieldValue;
		vendorFZipCode = filteredFieldValue;
		
		for( var payeeFZipNo = 0; payeeFZipNo < 4 ; payeeFZipNo ++ ){
			headerData['payeeFZipNo_part_' + payeeFZipNo] = '';
		}
		
		if (vendorFZipCode != '') {
			// clean unwanted chars
			vendorFZipCode = vendorFZipCode.replace(/[^0-9]/g, '');
			var vendorFZipCodeArr = vendorFZipCode.split('');
			
			for (var pfz = 0; pfz < vendorFZipCodeArr.length; pfz++) {
				headerData['payeeFZipNo_part_' + pfz] = vendorFZipCodeArr[pfz];
			}
		}
		var headerTemplate = _App.GetFileContent("wi_tax_form_2307_template_print_head.htm", true);
		
		headerData.PhBirLogoUrl = nlapiEscapeXML(
            helperUtils.getImageFullUrl(_App.BIR_LOGO_FILENAME));
		
		pageData.headerTemplate = _App.RenderTemplate( headerTemplate , headerData );
		
		var rowTemplate = _App.GetFileContent("wi_tax_form_2307_template_print_row.htm", true);
		
		pageData.rowsExpandedTax = '';		
		var rowData = {};
		
		for( var i in data.expandedWithHolding){
			// format the output
			rowData.taxDetail =  nlapiEscapeXML(data.expandedWithHolding[i].detail);
			rowData.atc =  nlapiEscapeXML(data.expandedWithHolding[i].atc);
			rowData.qtr1 =  _4601.formatCurrency( data.expandedWithHolding[i].qtr1 == undefined ? 0 : data.expandedWithHolding[i].qtr1 );
			rowData.qtr2 =  _4601.formatCurrency( data.expandedWithHolding[i].qtr2 == undefined ? 0 : data.expandedWithHolding[i].qtr2 );
			rowData.qtr3 =  _4601.formatCurrency( data.expandedWithHolding[i].qtr3 == undefined ? 0 : data.expandedWithHolding[i].qtr3 );
			rowData.total =  _4601.formatCurrency( data.expandedWithHolding[i].total );
			rowData.qtrWHTax =  _4601.formatCurrency( data.expandedWithHolding[i].withheld );
			pageData.rowsExpandedTax += _App.RenderTemplate(rowTemplate, rowData )+'\n';
		}
		
		// prepare the business tax data
		pageData.rowsBusinessTax = '';
		
		// prepare totals
		pageData.ex_qtr1_total = _4601.formatCurrency( data.totalsExpandedWh.qtr1 );
		pageData.ex_qtr2_tota2 = _4601.formatCurrency( data.totalsExpandedWh.qtr2 );
		pageData.ex_qtr3_tota3 = _4601.formatCurrency( data.totalsExpandedWh.qtr3 );
		pageData.ex_all_total = _4601.formatCurrency( data.totalsExpandedWh.alltotal );
		pageData.ex_wh_total = _4601.formatCurrency( data.totalsExpandedWh.totalwh );
		
        pageData.separatedBoxBGImageUrl = nlapiEscapeXML(
            helperUtils.getImageFullUrl(_App.BLACK_PIXEL_FILENAME));
        
        pageData.arabicfont = nlapiEscapeXML(helperUtils.getFullFileUrl("koodak.ttf"));
		
		var content = _App.RenderTemplate(template, pageData );
		
		try {
			var pdfFile = nlapiXMLToPDF('<?xml version="1.0"?>\n<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + content);

			response.setContentType("PDF");
			response.write(pdfFile.getValue());
		} catch (e) {
			response.write(content);
		}
	};
	
	return loadPdf;
}());


form2307.getPayeeInfo = function getPayeeInfo(vendorId) {
	if( vendorId != undefined ){
		_vendorId = vendorId; 
	}

	var payeeInfo = { name:'', regAddr:'', zip:'', tin:'', foreignAddr:'', foreignZip:'' };
	var vendorRecord = nlapiLoadRecord('vendor', _vendorId);
	if (vendorRecord.getFieldValue('isperson') == 'F')
		payeeInfo.name = vendorRecord.getFieldValue('companyname') == null ? '' : vendorRecord.getFieldValue('companyname');
	else {
		var lastName = vendorRecord.getFieldValue('lastname') == null ? '' : vendorRecord.getFieldValue('lastname') + ', ';
		var firstName = vendorRecord.getFieldValue('firstname') == null ? '' : vendorRecord.getFieldValue('firstname');
		var middleName = vendorRecord.getFieldValue('middlename') == null ? '' : vendorRecord.getFieldValue('middlename');
		payeeInfo.name = lastName + firstName + middleName;
	}
	payeeInfo.regAddr = vendorRecord.getFieldValue('defaultaddress') == null ? '' :
	    vendorRecord.getFieldValue('defaultaddress').split('\n').join(' ');
	
	payeeInfo.tin = vendorRecord.getFieldValue('vatregnumber') == '' || vendorRecord.getFieldValue('vatregnumber')== null ? '' : vendorRecord.getFieldValue('vatregnumber');

	if (vendorRecord.getLineItemCount('addressbook') > 0) {
		for (var ln = 1; ln <= vendorRecord.getLineItemCount('addressbook'); ln++) {
			if (vendorRecord.getLineItemValue('addressbook', 'defaultbilling', ln) == 'T') {
				vendorZipCode = vendorRecord.getLineItemValue('addressbook', 'zip', ln);
				break;
			}
		}
		var vendorZipCode = vendorZipCode == null ? '' : vendorZipCode;
		if (vendorZipCode != '') {
			vendorZipCode = vendorZipCode.replace(/[^0-9]/g, '');
		}
		payeeInfo.zip = vendorZipCode;
	}
	return payeeInfo;
};


form2307.formatForReport = (function() {
	/**
	 * Takes the collated results and produces the object that will be used by
	 * the template as the data source to replace the placeholders.
	 * @param {Object} collatedResults
	 */
	function formatForReport(collatedResults) {
        var taxCodeRecords = formatForReport.getAllWTcodes();
        var formattedOutput = formatForReport.prepareEmptyData();
        var totalBaseMon1 = 0.0;
        var totalBaseMon2 = 0.0;
        var totalBaseMon3 = 0.0;
        var totalBaseQtr = 0.0;
        var totalMon1 = 0.0;
        var totalMon2 = 0.0;
        var totalMon3 = 0.0;
        var totalQtr = 0.0;
		
		for (var taxCodeName in collatedResults) {
			totalMon1 = totalBaseMon1 = parseFloat(collatedResults[taxCodeName]['baseTaxablePerMonthOfQuarter'][0]);		
			totalMon1 = totalBaseMon1 + parseFloat(collatedResults[taxCodeName]['groupBaseTaxablePerMonthOfQuarter'][0]);

			totalMon2 = totalBaseMon2 = parseFloat(collatedResults[taxCodeName]['baseTaxablePerMonthOfQuarter'][1]);
			totalMon2 = totalBaseMon2 + parseFloat(collatedResults[taxCodeName]['groupBaseTaxablePerMonthOfQuarter'][1]);

			totalMon3 = totalBaseMon3 = parseFloat(collatedResults[taxCodeName]['baseTaxablePerMonthOfQuarter'][2]);
			totalMon3 = totalBaseMon3 + parseFloat(collatedResults[taxCodeName]['groupBaseTaxablePerMonthOfQuarter'][2]);

			totalQtr = totalBaseQtr = parseFloat(collatedResults[taxCodeName]['baseTaxableAmountForQuarter']);
			totalQtr = totalBaseQtr + parseFloat(collatedResults[taxCodeName]['groupBaseTaxableAmountForQuarter']);
			
			if (totalQtr != 0) {
				var record = {
		                atc: taxCodeName,
						detail: taxCodeRecords[taxCodeName].desc, 
						qtr1: totalMon1, 
						qtr2: totalMon2,
						qtr3: totalMon3, 
						total: totalQtr,
						withheld: collatedResults[taxCodeName]['wTaxAmountForQuarter'] 
		            };
		            formattedOutput.expandedWithHolding.push(record);
					
		            formattedOutput.totalsExpandedWh.qtr1 += parseFloat(totalBaseMon1);
		            formattedOutput.totalsExpandedWh.qtr2 += parseFloat(totalBaseMon2);
		            formattedOutput.totalsExpandedWh.qtr3 += parseFloat(totalBaseMon3);
		            formattedOutput.totalsExpandedWh.alltotal += parseFloat(totalBaseQtr);
		            formattedOutput.totalsExpandedWh.totalwh += parseFloat(record.withheld);
			}
		}
		return formattedOutput;
	};
	
	formatForReport.prepareEmptyData = function prepareEmptyData(  ) {
		var data = { expandedWithHolding: new Array(), totalsExpandedWh:{ qtr1:0, qtr2:0, qtr3:0, alltotal:0, totalwh:0 }, businessTax: new Array()  };
		return data;
	};
	
	formatForReport.getAllWTcodes = function getAllWTcodes(){
		var wtCodeRecords = new Array();
		var assocColumns = new Array();
		
		assocColumns['rate'] = new nlobjSearchColumn('custrecord_4601_wtc_rate'); //custrecord_ph4014_wtax_code_rate - deprecated
		assocColumns['desc'] = new nlobjSearchColumn('custrecord_4601_wtc_description'); //custrecord_ph4014_wtax_code_desc - deprecated
		assocColumns['name'] = new nlobjSearchColumn('custrecord_4601_wtc_name'); //custrecord_ph4014_wtax_code_name - deprecated		
		
		var columns = [ assocColumns['rate'], assocColumns['desc'], assocColumns['name'] ];
		var filters = null;
		
		var result = nlapiSearchRecord('customrecord_4601_witaxcode', null, filters, columns); //customrecord_ph4014_wtax_code - deprecated
		if( result!=null && result.length > 0 ){
			for( var r in result ){
				wtCodeRecords[ result[r].getValue( assocColumns['name']) ] = { rate: result[r].getValue( assocColumns['rate'] ), 
									desc:result[r].getValue( assocColumns['desc']), 
									name:result[r].getValue( assocColumns['name'])} ;
			}
		}
		return wtCodeRecords;
	};
	
	return formatForReport;
		
}());