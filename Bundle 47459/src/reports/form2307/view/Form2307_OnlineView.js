/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.OnlineView = function _OnlineView() {
    WTax.Form2307.View.call(this);
};

WTax.Form2307.OnlineView.prototype = Object.create(WTax.Form2307.View.prototype);

WTax.Form2307.OnlineView.prototype.createForm = function _createForm(viewData) {
    var form = {};
    
    try {
        form = nlapiCreateForm(LABEL.REPORT_HEADER);
        form.setScript(REPORT_MAP[FORM2307].clientScript);
        this.addButtons(form, viewData);
        this.addMessage(form, viewData);
        this.addComboBoxes(form, viewData);
        this.addOnlineForm(form, viewData);
        this.addHiddenFields(form, viewData);
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.OnlineView.createOnlineForm', WTaxError.LEVEL.ERROR);
    }   
    
    return form;
};

WTax.Form2307.OnlineView.prototype.addButtons = function _addButtons(form, viewData) {
    if ((viewData.fieldData.reportId == FORM2307) && 
        (viewData.fieldData.countryCode == FORM2307_COUNTRY)) {
        this.addForm2307Buttons(form, viewData);
    } else {
        this.addDefaultButtons(form, viewData);
    }
};

WTax.Form2307.OnlineView.prototype.addDefaultButtons = function _addDefaultButtons(form, viewData) {
    form.addSubmitButton(LABEL.REFRESH).setDisabled(true);
    form.addButton(FIELD_NAME.PRINT, LABEL.PRINT, 'onPrintButtonClicked()').setDisabled(true);
};

WTax.Form2307.OnlineView.prototype.addForm2307Buttons = function _addForm2307Buttons(form, viewData) {
    var refreshBtn = [
        '<table id="tbl_submitter" cellpadding="0" cellspacing="0" border="0" class="uir-button" style="margin-right: 6px; cursor: hand;">',
        '<tbody> <tr id="tr_submitter" class="pgBntG pgBntB">',
        '<td id="tdleftcap_submitter"><img src="/images/nav/ns_x.gif" class="bntLT" border="0" height="50%" width="3"> ',
        '<img src="/images/nav/ns_x.gif" class="bntLB" border="0" height="50%" width="3"></td>',
        '<td id="tdbody_submitter" height="20" valign="top" nowrap="" class="bntBgB">',
        '<input type="submit" style="" class="rndbuttoninpt bntBgT" value="Refresh" id="submitter" name="submitter"', 
        'onmousedown="this.setAttribute(" _mousedown","T"); setButtonDown(true, false, this);" ',
        'onmouseup="this.setAttribute(" _mousedown","F"); setButtonDown(false, false, this);" ',
        'onmouseout="if(this.getAttribute("_mousedown")=="T") setButtonDown(false, false, this);"', 
        'onmouseover="if(this.getAttribute("_mousedown")=="T") setButtonDown(true, false, this);" _mousedown="F"></td>',
        '<td id="tdrightcap_submitter"><img src="/images/nav/ns_x.gif" height="50%" class="bntRT" border="0" width="3">',
        '<img src="/images/nav/ns_x.gif" height="50%" class="bntRB" border="0" width="3"></td>',
        '</tr></tbody></table>'                      
        ].join('');
    this.app.CreateField(form, FIELD_NAME.REFRESH, 'inlinehtml', LABEL.REFRESH, false, refreshBtn, 'normal', 'outsideabove');
    
    var divider = [
        '<table border="0" cellpadding="0" cellspacing="0" class="uir-button-menu-divider" style="margin-left:1px;"><tbody><tr>',
        '<td class="rndbuttoncaps" bgcolor="#B5B5B5"><img src="/images/nav/ns_x.gif" border="0" width="1" height="19">',
        '</td></tr></tbody></table>'].join('');
    this.app.CreateField(form, 'divider', 'inlinehtml', '', false, divider, 'normal', 'outsideabove');

    var printMenu = [
        { label: LABEL.PRINT_INDIVIDUAL, handler: 'onPrintIndividualButtonClicked()'}
        ];
    var printButton = this.createExtJSButton(FIELD_NAME.PRINT, LABEL.PRINT, 'onPrintButtonClicked()', true, printMenu);
    this.app.CreateField(form, FIELD_NAME.PRINT, 'inlinehtml', LABEL.PRINT, false, printButton, 'normal', 'outsideabove');

    var selectTxnButton = this.createExtJSButton(FIELD_NAME.SELECT_TXN, LABEL.SELECT_TXN, 'onSelectTxnButtonClicked()', true);
    this.app.CreateField(form, FIELD_NAME.SELECT_TXN, 'inlinehtml', LABEL.SELECT_TXN, false, selectTxnButton, 'normal', 'outsideabove');
};

WTax.Form2307.OnlineView.prototype.createExtJSButton = function _createExtJSButton(id, label, onClick, isEnabled, buttonItems) {
    var eventHandler = '';
    var addButton = '';
    if (buttonItems) {
        addButton = "Ext.onReady(function(){new Ext.SplitButton({";
        eventHandler = ["handler: function(){", onClick, "},",
                        "menu: new Ext.menu.Menu({items: [", this.createExtJSMenu(buttonItems), "]})"].join('');
    } else {
        addButton = "Ext.onReady(function(){new Ext.Button({";
        eventHandler = ["handler: function(){", onClick, "}"].join('');
    }
    
    var button = ["<span id='_", id, "'/>", 
                  "<script type='text/javascript'>",
                  addButton,
                  "id: '", id, "',",
                  "text: '", label, "',", 
                  "renderTo:'_", id, "',",
                  "style: 'margin:3px',",
                  "disabled:", (!isEnabled).toString(), ",",
                  "listeners: {menushow: function(){Ext.select('.x-menu').setStyle({'background-image': 'none'})}, afterrender: function() {  Ext.select('.x-btn-text').setStyle({'font-size': '14px','color': '#333333','font-weight': 600,'padding':'0px 12px','height':'23px','font-family':'Open Sans,Helvetica,sans-serif'});}}, ",
                  eventHandler,
                  "})}); </script>"
                  ];
    
    return button.join('');
};

WTax.Form2307.OnlineView.prototype.createExtJSMenu = function _createExtJSMenu(buttons) {
    var buttonString = [];
    for (var button = 0; button < buttons.length; button++) {
        buttonString.push("{text: '" + buttons[button].label + "',style:'font-size:14px; color:#333333; font-weight:600; padding:0px 12px; line-height:23px; height:23px;font-family: Open Sans,Helvetica,sans-serif;', listeners: {afterrender: function(){Ext.select('.x-menu').setStyle({'background-image': 'none'})}}");
        buttons[button].handler ? buttonString.push(", handler: function(){" + buttons[button].handler + "}}") : buttonString.push("}");
        if (button < buttons.length) {
            buttonString.push(", ");
        }
    }
    return buttonString.join("");
};

WTax.Form2307.OnlineView.prototype.addMessage = function _addMessage(form, viewData) {
    if (viewData.message.messageHeader) {
        var errorMessage = 
            ["<br><div id='div_message'/><script>showAlertBox('div_message', '", 
             viewData.message.messageHeader, 
             "','",
             viewData.message.messageText,
             "', ", 
             viewData.message.messagePriority,
             ", '825px', '', '', false);</script>"].join('');
        this.app.CreateField(form, FIELD_NAME.MESSAGE, 'inlinehtml', '', false, errorMessage);
    }
};

WTax.Form2307.OnlineView.prototype.addComboBoxes = function _addComboBoxes(form, viewData) {
    // TODO: Remove dependencies
    var subOrNexus;

    if (viewData.isOneWorld) {
        var subCombo = new _4601.SubsidiaryComboboxConfiguration(
            viewData.fieldData.subList,
            FIELD_NAME.SUBSIDIARY,
            LABEL.SUBSIDIARY,
            viewData.fieldData.subId,
            'outsideabove',
            'startrow');
        new _4601.FilteredSubsidiaryCombobox(form, subCombo).setDisplaySize(220);
        subOrNexus = viewData.fieldData.subId;
    } else {
        var nexusComboParams = {
            fieldName:          FIELD_NAME.NEXUS, 
            fieldLabel:         LABEL.NEXUS,
            idSelectedDefault:  viewData.fieldData.nexus, 
            fieldLayoutType:    'outsideabove',
            fieldBreakType:     'startrow'
        };
        var nexusCombo = new _4601.NexusCombobox(form, nexusComboParams);
        nexusCombo.setMandatory(true);
        nexusCombo.setDisplaySize(220);
        subOrNexus = viewData.fieldData.nexus;
    }
    
    new SFC.System.ReportFormCombobox(form, FIELD_NAME.REPORT, LABEL.REPORT, viewData.fieldData.reportId, 'outsideabove', null, { country: viewData.fieldData.countryCode })
        .setDisplaySize(260);
    
    var taxPeriodCombo = new SFC.System.TaxPeriodMonthQuarterCombobox(
        form, 
        FIELD_NAME.TAX_PERIOD, 
        LABEL.TAX_PERIOD, 
        viewData.fieldData.taxPeriodId, 
        'outsideabove',
        subOrNexus,
        viewData.fieldData.fiscalCalendarId);
    taxPeriodCombo.setMandatory(true);
    taxPeriodCombo.setDisplaySize(120);
    
    new _4601.TaxTypeCombobox(form, {
        fieldName:  FIELD_NAME.TAX_TYPE, 
        label:      LABEL.TAX_TYPE, 
        defaultId:  viewData.fieldData.taxTypeId, 
        layoutType: 'outsideabove', 
        subId:      viewData.fieldData.subId,
        nexusId:    viewData.fieldData.nexus}
    ).setDisplaySize(120);
    
    if (viewData.fieldData.reportId == FORM2307 && viewData.fieldData.countryCode == FORM2307_COUNTRY) {
        var vendorComboBox = form.addField(FIELD_NAME.VENDOR, 'select', LABEL.VENDOR, 'vendor');
        vendorComboBox.setDisplayType('normal');
        vendorComboBox.setLayoutType('outsideabove','startrow');
        vendorComboBox.setDisplaySize(220);
        if (viewData.fieldData.vendorId) {
            vendorComboBox.setDefaultValue(viewData.fieldData.vendorId);
        }
    }
};

WTax.Form2307.OnlineView.prototype.addOnlineForm = function _addOnlineForm(form, viewData) {
    if (!viewData.fieldData.subId && !viewData.fieldData.nexus && (viewData.fieldData.countryCode != FORM2307_COUNTRY)) {
        return;
    }
    
    var reportData = this.formatReportData(viewData);
    var div = ['<div id=\"form2307\">'];
    div.push('<br>');
    div.push(this.app.RenderTemplate(viewData.fileDep.formTemplate, reportData));
    div.push('</div>');
    var template = div.join('');  
    this.app.CreateField(form, FIELD_NAME.REPORT_FORM, 'inlinehtml', '', false, template, 'normal', 'outsideabove', 'startrow');
};

WTax.Form2307.OnlineView.prototype.addHiddenFields = function _addHiddenFields(form, viewData) {
    this.app.CreateField(form, FIELD_NAME.SCRIPT_ID,            'text',     LABEL.SCRIPT_ID,            false, viewData.fieldData.script,             'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.PURCHASE_TAX_POINT,   'text',     LABEL.PURCHASE_TAX_POINT,   false, viewData.fieldData.purchaseTaxPoint,   'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.ALL_TXN_ID_LIST,      'longtext', LABEL.ALL_TXN_ID_LIST,      false, viewData.fieldData.allTxnIdList,       'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.SELECTED_TXN_ID_LIST, 'longtext', LABEL.SELECTED_TXN_ID_LIST, false, viewData.fieldData.selectedTxnIdList,  'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.TXN_COUNT,            'integer',  LABEL.TXN_COUNT,            false, viewData.fieldData.txnCount,           'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.CACHE_ID,             'integer',  LABEL.CACHE_ID,             false, viewData.fieldData.cacheId,            'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.RELOAD_CACHE,         'checkbox', LABEL.RELOAD_CACHE,         false, 'F',                                   'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.TAX_PERIOD_LIST,      'longtext', LABEL.TAX_PERIOD_LIST,      false, viewData.fieldData.taxPeriodList,      'hidden', 'normal');
    this.app.CreateField(form, FIELD_NAME.WTAX_CODE_LIST,       'longtext', LABEL.WTAX_CODE_LIST,       false, viewData.fieldData.wtaxCodeIdList,     'hidden', 'normal');
};
