/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.SelectTxnView = function _SelectTxnView() {
    WTax.Form2307.View.call(this);
};

WTax.Form2307.SelectTxnView.prototype = Object.create(WTax.Form2307.View.prototype);

WTax.Form2307.SelectTxnView.prototype.createForm = function _createForm(viewData) {
    var form = {};
    
    try {
        form = nlapiCreateForm(LABEL.SELECT_TXN);
        this.addButtons(form);
        this.addOnlineForm(form, viewData);
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.SelectTxnView.createOnlineForm', WTaxError.LEVEL.ERROR);
    }   
    
    return form;
};

WTax.Form2307.SelectTxnView.prototype.addButtons = function _addButtons(form) {
    form.addButton(FIELD_NAME.SAVE, LABEL.SAVE, 'OnSave()');
    form.addButton(FIELD_NAME.CANCEL, LABEL.CANCEL, 'OnCancel()');
};

WTax.Form2307.SelectTxnView.prototype.addOnlineForm = function _addOnlineForm(form, viewData) {
    var txnForm = this.app.RenderTemplate(viewData.formTemplate, viewData.formParams);
    this.app.CreateField(form, FIELD_NAME.TXN_LIST, 'inlinehtml', '', false, txnForm, 'normal', 'outsideabove', 'startrow');
};
