/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.PdfTemplateRenderer = function _PdfTemplateRenderer() {
	Handlebars.registerHelper('fillTable', function(rowNum, maxRowNum, options) {
		var row = [];

		for (var i = rowNum; i < maxRowNum; i++) {
			row.push('<tr>');
			row.push('<td height="15px">&nbsp;</td>');
			row.push('<td></td>');
			row.push('<td></td>');
			row.push('<td></td>');
			row.push('<td></td>');
			row.push('<td></td>');
			row.push('<td style="border: none;"></td>');
			row.push('</tr>');
		}
	
		return row.join('');
	});
};

WTax.Form2307.PdfTemplateRenderer.prototype.renderHandlebarsTemplate = function _renderHandlebarsTemplate(template, ds) {

	if (!template) {
		var templateError = nlapiCreateError('MISSING_PARAMETER', 'template is null or undefined.');
        WTaxError.throwError(templateError, 'WTax.Form2307.PdfTemplateRenderer.renderHandlebarsTemplate', WTaxError.LEVEL.ERROR);
	}
	if (!ds) {
		var dsError =  nlapiCreateError('MISSING_PARAMETER', 'ds is null or undefined.');
        WTaxError.throwError(dsError, 'WTax.Form2307.PdfTemplateRenderer.renderHandlebarsTemplate', WTaxError.LEVEL.ERROR);
	}

	var compiledTemplate = Handlebars.compile(template);
	return compiledTemplate(ds);
};