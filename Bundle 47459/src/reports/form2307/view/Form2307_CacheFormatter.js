/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.CacheFormatter = function _CacheFormatter() {
    this.PREFIX = 'form2307_';
    this.SEPARATOR = ',';
};

WTax.Form2307.CacheFormatter.prototype = Object.create(WTax.Lib.CacheFormatter.prototype);

WTax.Form2307.CacheFormatter.prototype.convertToObject = function _formatFileName(stringValue) {
    var obj = stringValue ? stringValue.split(this.SEPARATOR) : [];
    for (var i = 0; i < obj.length; i++) {
        if (isNaN(obj[i])) {
            var error = nlapiCreateError('FORM2307_CACHE_ERROR', 'Cache is not a number');
            WTaxError.throwError(error, 'WTax.Form2307.CacheFormatter.convertToObject', WTaxError.LEVEL.ERROR);
        }
        obj[i] = Number(obj[i]);
    }
    
    return obj;
};

WTax.Form2307.CacheFormatter.prototype.convertToText = function _formatFileName(objValue) {
    return objValue.join(this.SEPARATOR);
};

WTax.Form2307.CacheFormatter.prototype.convertName = function _formatFileName(objName) {
    return this.PREFIX + objName;
};

