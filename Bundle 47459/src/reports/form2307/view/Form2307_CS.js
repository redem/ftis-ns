/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var context = nlapiGetContext();
var isOneWorld = context.getFeature('SUBSIDIARIES');

function onPrintButtonClicked() {
    handlePrint(REPORT_MAP[FORM2307].printDeployment);
}

function onPrintIndividualButtonClicked() {
    handlePrint(REPORT_MAP[FORM2307].printIndividualDeployment);
}

function handlePrint(deployment) {
    if (!nlapiGetFieldValue(FIELD_NAME.VENDOR)) {
        return;
    }
    var params = getParamsByClassName('wtax');
    params[FIELD_NAME.CACHE_ID] = nlapiGetFieldValue(FIELD_NAME.CACHE_ID);
    var url = getForm2307Url(deployment, params);
    window.open(url);
}

function onSelectTxnButtonClicked() {
    if (!nlapiGetFieldValue(FIELD_NAME.VENDOR)) {
        alert(LABEL.VENDOR_NOT_SELECTED);
        return;
    } else if (NS.form.isChanged()) {
        alert(LABEL.REPORT_FILTERS_ARE_CHANGED);
        return;
    }
    
    var params = {};
    params[FIELD_NAME.PURCHASE_TAX_POINT] = nlapiGetFieldValue(FIELD_NAME.PURCHASE_TAX_POINT);
    params[FIELD_NAME.TXN_COUNT] = nlapiGetFieldValue(FIELD_NAME.TXN_COUNT);
    params[FIELD_NAME.TAX_PERIOD_LIST] = nlapiGetFieldValue(FIELD_NAME.TAX_PERIOD_LIST);
    params[FIELD_NAME.WTAX_CODE_LIST] = nlapiGetFieldValue(FIELD_NAME.WTAX_CODE_LIST);

    var url = getForm2307Url(REPORT_MAP[FORM2307].selectTxnDeployment, params);
    nlExtOpenWindow(url, WINDOW_NAME.SELECT_TXN, Math.ceil(screen.availWidth * 0.6), 600, null, false, LABEL.SELECT_TXN);
}

function onFieldChange(type, name) {
    switch (name) {
        case FIELD_NAME.SUBSIDIARY:
            // Fall-through expected
        case FIELD_NAME.NEXUS:
            refreshPage();
            break;
        case FIELD_NAME.TAX_PERIOD:
            // Fall-through expected
        case FIELD_NAME.TAX_TYPE:
            // Fall-through expected
        case FIELD_NAME.VENDOR:
            nlapiSetFieldValue(FIELD_NAME.RELOAD_CACHE, 'T');
            break;
        case FIELD_NAME.REPORT:
            onReportChange();
            break;
        default:
            // Do nothing
    }
}

function onReportChange() {
    var url = '';
    var report = nlapiGetFieldValue(FIELD_NAME.REPORT) || '';
    if (report != FORM2307) {
        url = getUrl(REPORT_MAP[report].script, REPORT_MAP[report].onlineDeployment);
        window.open(url, '_self');
    }
}

function OnSelectTxnSave(txnList) {
    refreshPage();
}

function refreshPage() {
    var params = {};
    params[FIELD_NAME.CACHE_ID] = nlapiGetFieldValue(FIELD_NAME.CACHE_ID);
    window.location = getForm2307Url(REPORT_MAP[FORM2307].onlineDeployment, params);
}

function getForm2307Url(deployment, params) {
    var scriptId = nlapiGetFieldValue(FIELD_NAME.SCRIPT_ID);
    var url = getUrl(scriptId, deployment);
    if (params) {
        url = addParamsToUrl(url, params);
    }
    return url;
}

function getUrl(scriptId, deployment) {
    var url = nlapiResolveURL('SUITELET', scriptId, deployment);
    var params = {};
    
    if (isOneWorld) {
        params[FIELD_NAME.SUBSIDIARY] = nlapiGetFieldValue(FIELD_NAME.SUBSIDIARY);
    } else {
        params[FIELD_NAME.NEXUS] = nlapiGetFieldValue(FIELD_NAME.NEXUS);
    }
    
    params[FIELD_NAME.TAX_PERIOD] = nlapiGetFieldValue(FIELD_NAME.TAX_PERIOD);
    params[FIELD_NAME.TAX_TYPE] = nlapiGetFieldValue(FIELD_NAME.TAX_TYPE);
    params[FIELD_NAME.VENDOR] = nlapiGetFieldValue(FIELD_NAME.VENDOR);
    
    return addParamsToUrl(url, params);
}

function updateField(fieldName, fieldObj){
    nlapiSetFieldValue(fieldName, fieldObj.value, true);
    return true;
}

function getParamsByClassName(className) {
    className = '.' + className;
    var params = {};
    var elements = Ext.select(className).elements;

    for(var ielements = 0; ielements < elements.length; ielements++) {
        var element = elements[ielements];
        params[element.id] = escape(element.innerHTML || nlapiGetFieldValue(element.id));
    }
    return params;
}

function addParamsToUrl(baseUrl, params) {
    var url = [baseUrl];
    
    for (var i in params) {
        url.push('&' + i + '=' + params[i]);
    }
    
    return url.join('');
}

function saveTxnIdList(idList){
    var list = idList.join(',');
    nlapiSetFieldValue(FIELD_NAME.SELECTED_TXN_ID_LIST, list);
    
    var scriptId = nlapiGetFieldValue(FIELD_NAME.SCRIPT_ID);
    var url = nlapiResolveURL('SUITELET', scriptId, REPORT_MAP[FORM2307].selectTxnDeployment);
    var cacheId = -1;
    
    var params = {};
    params[FIELD_NAME.REQUEST_TYPE] = REQUEST.SELECT_TXN_SAVE_TXN;
    params[FIELD_NAME.SELECTED_TXN_ID_LIST] = list;
    params[FIELD_NAME.CACHE_ID] = nlapiGetFieldValue(FIELD_NAME.CACHE_ID);

    Ext.Ajax.request({
        url: url,
        timeout: 300000,
        method: 'POST',
        params: params,
        success: function(xhr) {
            var obj = Ext.decode(xhr.responseText);
            cacheId = obj.cacheId;
            nlapiSetFieldValue(FIELD_NAME.CACHE_ID, cacheId);
        },
        callback: function() {
            closePopup();
            refreshPage();
        },
        failure: function() {
            Ext.Msg.show({
                title:   LABEL.SAVE_TXN_ERROR_HEADER,
                msg:     LABEL.SAVE_TXN_ERROR_MESSAGE,
                buttons: Ext.Msg.OK,
                width:   400,
                icon:    Ext.MessageBox.Error
            });
        }
    });
}

function closePopup(){
    NS.form.setChanged(false);
    Ext.WindowMgr.get(WINDOW_NAME.SELECT_TXN).close();
}
