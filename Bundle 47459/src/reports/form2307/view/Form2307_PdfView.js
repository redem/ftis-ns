/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.PdfView = function _PdfView() {
    WTax.Form2307.View.call(this);
};

WTax.Form2307.PdfView.prototype = Object.create(WTax.Form2307.View.prototype);

WTax.Form2307.PdfView.prototype.createForm = function _createForm(viewData) {
    try {
    	var templateRenderer = new WTax.Form2307.PdfTemplateRenderer();
        var mainData = this.formatReportMain(viewData);

        mainData.header = this.formatReportHeader(viewData);
        mainData.content = this.formatReportBody(viewData);

        var form = templateRenderer.renderHandlebarsTemplate(viewData.fileDep.formTemplate, mainData);
        var pdf = {
            form     : form,
            fileName : viewData.fileName
        };

        return pdf;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.PdfView.createForm', WTaxError.LEVEL.ERROR);
    }     
};

WTax.Form2307.PdfView.prototype.formatReportMain = function _formatReportMain(viewData) {
	var reportMain = {
		payeeName : viewData.payee.name || '',
		payeeTin : viewData.payee.tin,
		payeeAddress : viewData.payee.address || '',
		payeeZip : viewData.payee.zip || '',
		logoUrl : viewData.fileDep.logoUrl,
		reportFont : viewData.fileDep.fontUrl,
		pixelUrl : viewData.fileDep.pixelUrl,
		ewtRowsPerPage : FORM2307_MAX_EWT_ROWS,
		wbtRowsPerPage : FORM2307_MAX_WBT_ROWS
	};

	return reportMain;
};

WTax.Form2307.PdfView.prototype.formatReportHeader = function _formatReportHeader(viewData) {
	var reportHeader = viewData.header;
	var payorName = reportHeader.payorName;
	var payorAddress = reportHeader.payorAddress;

	reportHeader.payorName = this.truncateText(payorName, 97);
	reportHeader.payorAddress = this.truncateText(payorAddress, 67);

	return reportHeader;
};

WTax.Form2307.PdfView.prototype.formatReportBody = function _formatReportBody(viewData) {
    var formattedTransaction = [];
   	var transaction = viewData.content;
   	var hasPageBreak = false;

	for (var t = 0; t < transaction.length; t++) {
		var page = transaction[t];
		var formattedPage = [];

		for (var p = 0; p < page.length; p++) {
			var taxRows = [];
			var keys = Object.keys(page[p].wtaxRows);

			hasPageBreak = !hasPageBreak ? formattedPage.length > 0 : hasPageBreak;

		    for (var i = 0; i < keys.length; i++) {
		        var taxCode = keys[i];
		        var row = this.formatRow(page[p].wtaxRows[taxCode]);
		        row.atc = row.atc.length > 5 ? row.atc.substring(0, 5) : row.atc; // limit to 5 characters
		        row.detail = this.truncateText(row.detail, 75); // limit to 75 characters
		        taxRows.push(row);
		    }

		    formattedPage.push({
		    	hasPageBreak: hasPageBreak,
		    	taxRows: taxRows,
		    	totals: {
		    		totalBaseAmount     : viewData.fieldData.vendorId ? _4601.formatCurrency(page[p].totals.totalBaseAmount || 0) : '',
	   		        totalQuarterAmount1 : viewData.fieldData.vendorId ? _4601.formatCurrency(page[p].totals.quarterAmount1  || 0) : '',
	   		        totalQuarterAmount2 : viewData.fieldData.vendorId ? _4601.formatCurrency(page[p].totals.quarterAmount2  || 0) : '',
	   		        totalQuarterAmount3 : viewData.fieldData.vendorId ? _4601.formatCurrency(page[p].totals.quarterAmount3  || 0) : '',
	   		        totalTaxAmount      : viewData.fieldData.vendorId ? _4601.formatCurrency(page[p].totals.totalTaxAmount  || 0) : ''
		    	}
		    });
		}
    	formattedTransaction.push(formattedPage);
	}

    return formattedTransaction;
};

WTax.Form2307.PdfView.prototype.truncateText = function _truncateText(text, maxLength) {
	var ellipses = '...';

	if (text && text.length > maxLength) {
		text = text.substring(0, maxLength - ellipses.length) + ellipses;
	}
	
	return text;
};
