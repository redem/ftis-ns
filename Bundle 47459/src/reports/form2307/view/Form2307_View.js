/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.View = function _View() {
    this.app = new SFC.System.Application(WTAX_APP_ID);
    // TODO: Remove dependency to SFC
};

WTax.Form2307.View.prototype.createForm = function _createForm(viewData) {
    var error = nlapiCreateError('NOT_IMPLEMENTED', 'WTax.Form2307.View.createForm not yet implemented');
    WTaxError.throwError(error, 'WTax.Form2307.View.createForm not yet implemented', WTaxError.LEVEL.ERROR);
};

WTax.Form2307.View.prototype.formatReportData = function _formatReportData(viewData) {
    var reportData = this.formatReportMain(viewData);
    reportData.taxRows = this.formatReportBody(viewData);
    return reportData;
};

WTax.Form2307.View.prototype.formatReportMain = function _formatReportMain(viewData) {
    var reportHeader = {
        payeeName           : viewData.payee.name    || '', 
        payeeTin            : viewData.payee.tin, 
        payeeAddress        : viewData.payee.address || '', 
        payeeZip            : viewData.payee.zip     || '',
        logoUrl             : viewData.fileDep.logoUrl,
        reportFont          : viewData.fileDep.fontUrl,
        pixelUrl            : viewData.fileDep.pixelUrl,
        totalBaseAmount     : viewData.fieldData.vendorId ? _4601.formatCurrency(viewData.totals.totalBaseAmount || 0) : '',
        totalQuarterAmount1 : viewData.fieldData.vendorId ? _4601.formatCurrency(viewData.totals.quarterAmount1  || 0) : '',
        totalQuarterAmount2 : viewData.fieldData.vendorId ? _4601.formatCurrency(viewData.totals.quarterAmount2  || 0) : '',
        totalQuarterAmount3 : viewData.fieldData.vendorId ? _4601.formatCurrency(viewData.totals.quarterAmount3  || 0) : '',
        totalTaxAmount      : viewData.fieldData.vendorId ? _4601.formatCurrency(viewData.totals.totalTaxAmount  || 0) : ''
    };
    
    nlapiLogExecution('DEBUG', 'formatReportMain reportHeader', JSON.stringify(reportHeader));
    return reportHeader;
};

WTax.Form2307.View.prototype.formatReportBody = function _formatReportBody(viewData) {
    var keys = Object.keys(viewData.wtaxRows);
    
    if (keys.length == 0) {
        var emptyReportBody = '';
        var emptyRow = this.app.RenderTemplate(viewData.fileDep.rowTemplate, this.getEmptyRow());
        for (var i = 0; i < 5; i++) {
            emptyReportBody += emptyRow;
        }
        return emptyReportBody;
    }
    
    var reportBody = '';
    for (var i = 0; i < keys.length; i++) {
        var taxCode = keys[i];
        var row = this.formatRow(viewData.wtaxRows[taxCode]);
        reportBody += this.app.RenderTemplate(viewData.fileDep.rowTemplate, row);
    }
    
    return reportBody;
};

WTax.Form2307.View.prototype.formatRow = function _formatRow(data) {
    var row = new WTax.Form2307.WTaxCodeRow();
    row.detail          = data.detail || '';
    row.atc             = data.atc    || '';
    row.quarterAmount1  = _4601.formatCurrency(data.quarterAmount1  || 0);
    row.quarterAmount2  = _4601.formatCurrency(data.quarterAmount2  || 0);
    row.quarterAmount3  = _4601.formatCurrency(data.quarterAmount3  || 0);
    row.totalBaseAmount = _4601.formatCurrency(data.totalBaseAmount || 0);
    row.totalTaxAmount  = _4601.formatCurrency(data.totalTaxAmount  || 0);
    return row;
};

WTax.Form2307.View.prototype.getEmptyRow = function _getEmptyRow() {
    var emptyRow = new WTax.Form2307.WTaxCodeRow();
    emptyRow.detail          = BLANK;
    emptyRow.atc             = BLANK;
    emptyRow.quarterAmount1  = BLANK;
    emptyRow.quarterAmount2  = BLANK;
    emptyRow.quarterAmount3  = BLANK;
    emptyRow.totalBaseAmount = BLANK;
    emptyRow.totalTaxAmount  = BLANK;   
    return emptyRow;
};
