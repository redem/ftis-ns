/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.OnlineConverter = function _OnlineConverter() {
};

WTax.Form2307.OnlineConverter.prototype = Object.create(WTax.Form2307.ReportConverter.prototype);

