/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.FieldData = function _SelectionData() {
    var data = {
        // Hidden Fields
        script            : '',
        purchaseTaxPoint  : '',
        selectedTxnIdList : [],
        txnCount          : 0,
        wtaxCodeIdList    : [],
        // Combo Boxes
        subId         : '',
        nexus         : '',
        vendorId      : '',
        taxPeriodId   : '',
        taxTypeId     : '',
        reportId      : '',
        subList       : [],
        nexusList     : [],
        reportList    : [],
        taxPeriodList : [],
        taxTypeList   : [],
        countryCode   : ''
    };
    return data;
};

// TODO: This would be updated if there are more types of messages
WTax.Form2307.Message = function _Message() {
    var msg = {
        messagePriority : MESSAGE_PRIORITY.LOW,
        messageHeader   : LABEL.REPORT_NOT_AVAILABLE_HEADER,
        messageText     : LABEL.REPORT_NOT_AVAILABLE_MESSAGE
    };
    return msg;
};

WTax.Form2307.Payee = function _Payee() {
    var payee = {
        name            : '',
        tin             : '',
        address         : '',
        zip             : '',
        foreignAddress  : '',
        foreignZip      : '',
    };
    return payee;
};

WTax.Form2307.WTaxCodeRow = function _WTaxCodeRow() {
    var row = {
        detail          : '',
        atc             : '',
        quarterAmount1  : 0,
        quarterAmount2  : 0,
        quarterAmount3  : 0,
        totalBaseAmount : 0,
        totalTaxAmount  : 0
    };
    return row;
};

WTax.Form2307.WTaxTotals = function _WTaxTotals() {
    var total = {
        quarterAmount1  : 0,
        quarterAmount2  : 0,
        quarterAmount3  : 0,
        totalBaseAmount : 0,
        totalTaxAmount  : 0
    };
    return total;
};

WTax.Form2307.ViewData = function _ViewData() {
    var data = {
        isOneWorld    : '',
        fieldData     : {},
        message       : {},
        payee         : {},
        wtaxRows      : [],
        wtaxTotals    : {},
        fileDep       : {}
    };
    return data;
};


WTax.Form2307.ReportConverter = function _ReportConverter() {
};

WTax.Form2307.ReportConverter.prototype.getAllRequestParams = function _getAllRequestParams(request) {
    try {
        var requestParams = this.getRequestParams(request) || {};
        
        requestParams.subId         = requestParams[FIELD_NAME.SUBSIDIARY] ? Number(requestParams[FIELD_NAME.SUBSIDIARY]) : '';
        requestParams.nexus         = requestParams[FIELD_NAME.NEXUS]      ? requestParams[FIELD_NAME.NEXUS]                : '';
        requestParams.vendorId      = requestParams[FIELD_NAME.VENDOR]     ? Number(requestParams[FIELD_NAME.VENDOR])     : '';
        requestParams.taxPeriodId   = requestParams[FIELD_NAME.TAX_PERIOD] ? Number(requestParams[FIELD_NAME.TAX_PERIOD]) : '';
        requestParams.taxTypeId     = requestParams[FIELD_NAME.TAX_TYPE]   ? Number(requestParams[FIELD_NAME.TAX_TYPE])   : '';
        requestParams.tin           = requestParams[FIELD_NAME.PAYEE_TIN]  ? requestParams[FIELD_NAME.PAYEE_TIN].replace(/-/g, '').substring(0, LENGTH.TIN)  : '';
        requestParams.name          = requestParams[FIELD_NAME.PAYEE_NAME]            ? unescape(requestParams[FIELD_NAME.PAYEE_NAME]) : '';
        requestParams.address       = requestParams[FIELD_NAME.PAYEE_ADDRESS]         ? unescape(requestParams[FIELD_NAME.PAYEE_ADDRESS]) : '';
        requestParams.foreignAddress= requestParams[FIELD_NAME.PAYEE_FOREIGN_ADDRESS] ? unescape(requestParams[FIELD_NAME.PAYEE_FOREIGN_ADDRESS]) : '';
        requestParams.zip           = requestParams[FIELD_NAME.PAYEE_ZIP]             ? Number(requestParams[FIELD_NAME.PAYEE_ZIP])   : '';
        requestParams.foreignZip    = requestParams[FIELD_NAME.PAYEE_FOREIGN_ZIP]     ? Number(requestParams[FIELD_NAME.PAYEE_FOREIGN_ZIP])   : '';
        requestParams.cacheId       = requestParams[FIELD_NAME.CACHE_ID] || '';
        requestParams.reloadCache   = requestParams[FIELD_NAME.RELOAD_CACHE] == 'T';
        
        return requestParams;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.ReportConverter.getRequestParams', WTaxError.LEVEL.ERROR);
    }    
};

WTax.Form2307.ReportConverter.prototype.getRequestParams = function _getRequestParams(request) {
    var tempParams = request.getAllParameters();
    var params = {};
    for (var p in tempParams) {
        params[p] = tempParams[p];
    }
    
    return params;
};


/**
 * 
 * @param taxCodeList
 * @param groupCodeList
 * @returns A map of WTax Code objects with the following structure:
 *          {
 *              WTax_Code_Internal_ID: {
 *                  code: '',
 *                  description: '',
 *                  rate: '',
 *                  children: {
 *                      Child_WTax_Code_Internal_ID: {
 *                          basis: '',
 *                          effectiveRate: ''
 *                      },
 *                      ...
 *                  }
 *              },
 *              ...
 *          }
 *          For a WTax Group, the component WTax Codes are stored in the 'children' object with the corresponding ratios.
 *          For a simple WTax Code, the 'children' object will contain the WTax Code itself with the 'basis' set to 100. 
 */
WTax.Form2307.ReportConverter.prototype.createTaxCodeMap = function _createTaxCodeMap(rawData) {
    try {
        if (Object.keys(rawData.wtaxObj).length <= 0) {
            return {};
        }
        
        var taxCodeList = rawData.wtaxCodeList;
        var groupCodeList = rawData.groupedWtaxCode;
        var groupCodeMap = this.createGroupedTaxCodeMap(groupCodeList);
        var map = {};
        
        for (var t = 0; taxCodeList && t < taxCodeList.length; t++) {
            var taxCode = taxCodeList[t];
            
            var taxCodeObj = {
                code: taxCode.name,
                description: taxCode.description,
                rate: parseFloat(taxCode.rate),
                childrenRaw: taxCode.groupedWTaxCodes,
                children: {}
            };
    
            map[taxCode.id] = taxCodeObj;
        }
        
        for (var mapping in map) {
            if (map[mapping].childrenRaw.length > 0) {
                for (var g = 0; g < map[mapping].childrenRaw.length; g++) {
                    var groupedWTaxCodeId = map[mapping].childrenRaw[g];
                    var wTaxCode = groupCodeMap[groupedWTaxCodeId].code;
                    
                    if (map[wTaxCode]) {
                        var groupCodeObj = {};
                        groupCodeObj['basis'] = groupCodeMap[groupedWTaxCodeId].basis;
                        groupCodeObj['effectiveRate'] = groupCodeObj.basis * map[wTaxCode].rate / 100;
                        
                        map[mapping].children[wTaxCode] = groupCodeObj;
                    }
                }
            } else {
                map[mapping].children[mapping] = {
                    basis: 100,
                    effectiveRate: map[mapping].rate
                };
            }
            
            delete map[mapping].childrenRaw;
        }
        
        return map;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.ReportConverter.createTaxCodeMap', WTaxError.LEVEL.ERROR);
    }    
};


WTax.Form2307.ReportConverter.prototype.createGroupedTaxCodeMap = function _createGroupedTaxCodeMap(groupCodeList) {
    var map = {};
    
    for (var g = 0; groupCodeList && g < groupCodeList.length; g++) {
        var groupCode = groupCodeList[g];
        
        map[groupCode.id] = {
            group: groupCode.group,
            code: groupCode.code,
            basis: parseFloat(groupCode.basis)
        };
    }
    
    return map;
};


WTax.Form2307.ReportConverter.prototype.convertData = function _convertData(rawData) {
    try {
        if (rawData.purchaseTaxPoint == TAX_POINT.PAYMENT) {
            this.calculateBillRatios(rawData.billTotalsList);
        }
        
        var taxCodeMap = this.createTaxCodeMap(rawData);
        var reportLines = this.convertWTaxList(rawData, taxCodeMap);
        
        var data          = new WTax.Form2307.ViewData();
        data.isOneWorld   = rawData.isOneWorld;
        data.fieldData    = this.convertFieldData(rawData);
        data.message      = this.convertMessage(data.fieldData);
        data.fileDep      = rawData.fileDep;
        data.payee        = this.convertPayee(rawData.vendorObj);
        data.wtaxRows     = reportLines.rows;
        data.totals       = reportLines.totals;
        data.header       = this.convertHeader(rawData);

        return data;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.ReportConverter.convertData', WTaxError.LEVEL.ERROR);
    }
};

WTax.Form2307.ReportConverter.prototype.convertHeader = function _convertData(rawData) {
    return {};
};

WTax.Form2307.ReportConverter.prototype.convertFieldData = function _convertFieldData(rawData) {
    try {
        var fieldData = new WTax.Form2307.FieldData();
        
        fieldData.subId              = rawData.subId;
        fieldData.nexus              = rawData.nexus;
        fieldData.vendorId           = rawData.vendorId;
        fieldData.taxPeriodId        = rawData.taxPeriodId;
        fieldData.taxTypeId          = rawData.taxTypeId;
        fieldData.reportId           = (rawData.subId || rawData.nexus) ? rawData.reportId || FORM2307 : '';
        fieldData.subList            = rawData.subList;
        fieldData.nexusList          = rawData.nexusList;
        fieldData.reportList         = rawData.reportList;
        fieldData.taxTypeList        = rawData.taxTypeList;    
        fieldData.countryCode        = rawData.country;
        fieldData.fiscalCalendarId   = rawData.fiscalCalendarId;
        fieldData.script             = rawData.script;
        fieldData.purchaseTaxPoint   = rawData.purchaseTaxPoint;
        fieldData.selectedTxnIdList  = rawData.selectedTxnIdList ? rawData.selectedTxnIdList.join(',') : '';
        fieldData.allTxnIdList       = rawData.allTxnIdList ? rawData.allTxnIdList.join(',') : '';
        fieldData.wtaxCodeIdList     = rawData.wtaxCodeIds ? rawData.wtaxCodeIds.join(',') : '';
        fieldData.taxPeriodList      = rawData.periodIds ? rawData.periodIds.join(',') : '';
        fieldData.txnCount           = rawData.txnCount || 0;
        fieldData.cacheId            = rawData.cacheId;
        
        return fieldData;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.ReportConverter.convertFieldData', WTaxError.LEVEL.ERROR);
    }    
};

WTax.Form2307.ReportConverter.prototype.convertMessage = function _convertMessage(rawData) {
    if (rawData.reportId && (rawData.countryCode == FORM2307_COUNTRY)) {
        return {};
    }
    
    return new WTax.Form2307.Message();
};

WTax.Form2307.ReportConverter.prototype.convertPayee = function _convertPayee(rawData) {
    var payee = new WTax.Form2307.Payee();
    
    if(rawData){
        if (!rawData.isPerson) {
            payee.name = rawData.companyName || '';
        }
        else {
            var lastName = rawData.lastName ? rawData.lastName + ',' : '';
            var firstName = rawData.firstName || '';
            var middleName = rawData.middleName || '';
            payee.name = [lastName, firstName, middleName].join(' ');
        }
        
        var vaddr1 = rawData.billAddress1 || '';
        var vaddr2 = rawData.billAddress2 || '';
        var vcity = rawData.billCity || '';
        var vstate = rawData.billState || '';
        var vcountry = rawData.billCountry || '';
        payee.address = [vaddr1, vaddr2, vcity, vstate, vcountry].join(' ');
        
        var ptin = rawData.vatRegNumber || ''; 
        payee.tin = this.formatTin(ptin);
        payee.zip = rawData.billZipCode == null ? '' : rawData.billZipCode.replace(/[^0-9]/g, '');
    }
    
    return payee;
};

WTax.Form2307.ReportConverter.prototype.convertWTaxList = function _convertWTaxList(rawData, taxCodeMap) {
    var lineCount = rawData.wtaxObj.length;
    var params = {
        taxCodeMap: taxCodeMap,
        wtaxRows: {}, 
        totals: new WTax.Form2307.WTaxTotals(), 
        periodIds: rawData.periodIdsInQuarter,
    };
    
    for (var i = 0; i < lineCount; i++) {
        params.row = rawData.wtaxObj[i];
        this.convertWTaxRow(params);
    }
    
    return {
        rows: params.wtaxRows,
        totals: params.totals
    };
};

WTax.Form2307.ReportConverter.prototype.convertWTaxRow = function _convertWTaxRow(params) {
    try {
        var taxCodeMap = params.taxCodeMap; 
        var wtaxRows = params.wtaxRows;
        var row = params.row;
        var totals = params.totals;
        var periodIds = params.periodIds;
        var taxCode = row.taxCode;
        
        if (!taxCode) {
            nlapiLogExecution('DEBUG', 'WTax.Form2307.ReportConverter.convertWTaxRow row', JSON.stringify(row));
            return;
        }
        
        var children = taxCodeMap[taxCode].children;
        for (var c in children) {
        	var rowData = this.convertRowData(wtaxRows, c, taxCodeMap, periodIds, row, children);
        	
            wtaxRows[c] = rowData.rowObj;
            
            totals.quarterAmount1 += rowData.amounts.q1;
            totals.quarterAmount2 += rowData.amounts.q2;
            totals.quarterAmount3 += rowData.amounts.q3;
            totals.totalBaseAmount += rowData.amounts.taxBase;
            totals.totalTaxAmount += rowData.amounts.taxAmount;
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.ReportConverter.convertWTaxRow', WTaxError.LEVEL.ERROR);
    }       
};

WTax.Form2307.ReportConverter.prototype.convertRowData = function _convertRowData(wtaxRows, taxCode, taxCodeMap, periodIds, row, children) {
    var rowObj = wtaxRows[taxCode] === undefined ? new WTax.Form2307.WTaxCodeRow() : wtaxRows[taxCode];
    rowObj.detail = taxCodeMap[taxCode].description;
    rowObj.atc = taxCodeMap[taxCode].code;

    var ratio = this.billRatiosMap && this.billRatiosMap[row.id] ? this.billRatiosMap[row.id].ratio : 1;
    var taxBase = parseFloat(row.taxBase) * ratio;
    var q1 = periodIds[0] == row.taxPeriod ? taxBase : 0;
    var q2 = periodIds[1] == row.taxPeriod ? taxBase : 0;
    var q3 = periodIds[2] == row.taxPeriod ? taxBase : 0;
    var taxAmount = parseFloat(row.taxBase) * children[taxCode].effectiveRate * ratio / 100;

    rowObj.quarterAmount1 += q1;
    rowObj.quarterAmount2 += q2;
    rowObj.quarterAmount3 += q3;
    rowObj.totalBaseAmount += taxBase;
    rowObj.totalTaxAmount += taxAmount;
    
    return {
    	rowObj: rowObj,
    	amounts: {
	    	q1: q1,
	    	q2: q2,
	    	q3: q3,
	    	taxBase: taxBase,
	    	taxAmount: taxAmount
    	}
    };
};


WTax.Form2307.ReportConverter.prototype.getBillIdsFromPayments = function _getBillIdsFromPayments(paymentList) {
    try {
        this.billRatiosMap = {};
        var billIdList = [];
        
        for (var i = 0; paymentList && i < paymentList.length; i++) {
            var payment = paymentList[i];
            if (payment.amount) {
                if (this.billRatiosMap[payment.docRefId] == undefined) {
                    this.billRatiosMap[payment.docRefId] = {};
                    this.billRatiosMap[payment.docRefId].paidTaxAmount = 0;
                }
                
                this.billRatiosMap[payment.docRefId].paidTaxAmount += -parseFloat(payment.amount);
                
                if (billIdList.indexOf(payment.docRefId) == -1) {
                    billIdList.push(payment.docRefId);
                }
            }
        }
        
        return billIdList;
    } catch(e) { 
        WTaxError.throwError(ex, 'WTax.Form2307.ReportConverter.getBillIdsFromPayments', WTaxError.LEVEL.ERROR);
    }
};


WTax.Form2307.ReportConverter.prototype.calculateBillRatios = function _calculateBillRatios(billTotalsList) {
    for (var j = 0; billTotalsList && j < billTotalsList.length; j++) {
        var bill = billTotalsList[j];
        
        if (this.billRatiosMap[bill.id]) {
            this.billRatiosMap[bill.id].totalTaxAmount = -parseFloat(bill.totalTaxAmount);
            this.billRatiosMap[bill.id].ratio = this.billRatiosMap[bill.id].paidTaxAmount / this.billRatiosMap[bill.id].totalTaxAmount;
        }
    }
};


// TODO: Move to base dao
WTax.Form2307.ReportConverter.prototype.extractIds = function _extractIds(list) {
    try {
        var ids = [];
        
        for (var i = 0; i < list.length; i++) {
            ids.push(list[i].id);
        };
        
        var reduceFunction = function (list, entry) {
            if (list.slice(-1)[0] !== entry) {
                list.push(entry);
            }
            return list;
        };
        ids = ids.sort().reduce(reduceFunction, []);
        
        return ids;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.ReportConverter.extractIds', WTaxError.LEVEL.ERROR);
    }      
};

WTax.Form2307.ReportConverter.prototype.extractTxnIds = function _extractTxnIds(idList) {
    if (!idList) {
        return [];
    }
    var list = idList.accrualIdList || [];
    if (idList.billIdList) {
        list = list.concat(idList.billIdList);
    }
    if (idList.chequeIdList) {
        list = list.concat(idList.chequeIdList);
    }
    
    return list;
};


WTax.Form2307.ReportConverter.prototype.convertListToMap = function _convertListToMap(list, key, aggregator) {
    if (!list || list.length == 0) {
        return {};
    }
    
    aggregator = aggregator || function(a, b) {
        return b;
    };
    
    var map = {};
    
    for (var i = 0; i < list.length; i++) {
        if (map[list[i][key]]) {
            map[list[i][key]] = aggregator(map[list[i][key]], list[i]);
        } else {
            map[list[i][key]] = list[i];
        }
    }
    
    return map;
};


WTax.Form2307.ReportConverter.prototype.pad = function _pad(data, digits, rightpad) {
    var zero = '0000000000000000';
    var paddedvalue = '';
    var padded = '';
    if(rightpad){
        padded = data + zero;
        paddedvalue = padded.slice(0,digits);
    }else{
        padded = zero + data;
        paddedvalue = padded.slice(-digits);
    }
    return paddedvalue;
};


WTax.Form2307.ReportConverter.prototype.formatTin = function _formatTin(tin) {
    if (!tin) {
        return '';
    }
    
    var xPadding = 'XXXXXXXXX';
    var zeroPadding = '0000';
    var tin = tin.substring(0, 13);
    
    if (tin.length < 9) {
        tin = (tin + xPadding).substring(0, 9);
    }
    
    if (tin.length < 13) {
        tin = (tin + zeroPadding).substring(0, 13);
    }
    
    tin = [
        tin.substring(0, 3),
        tin.substring(3, 6),
        tin.substring(6, 9),
        tin.substring(9)
    ].join('-');
    
    return tin;
};
