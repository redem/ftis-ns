/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};


WTax.Form2307.SelectTxnViewData = function _SelectTxnViewData() {
    return {
      transactionList: [],
      selectedTransactions: [],
      formTemplate: '',
      formParams: ''
    };
};


WTax.Form2307.SelectTxnConverter = function _SelectTxnConverter() {
};
WTax.Form2307.SelectTxnConverter.prototype = Object.create(WTax.Form2307.ReportConverter.prototype);

WTax.Form2307.SelectTxnConverter.prototype.getAllRequestParams = function _getAllRequestParams(request) {
    var params = this.getRequestParams(request) || {};
    var requestParams = {};
    
    requestParams.requestType   = params[FIELD_NAME.REQUEST_TYPE] || '';
    requestParams.nexus         = params[FIELD_NAME.NEXUS] || '';
    requestParams.subId         = params[FIELD_NAME.SUBSIDIARY] ? Number(params[FIELD_NAME.SUBSIDIARY]) : '';
    requestParams.vendorId      = params[FIELD_NAME.VENDOR]     ? Number(params[FIELD_NAME.VENDOR])     : '';
    requestParams.taxTypeId     = params[FIELD_NAME.TAX_TYPE]   ? Number(params[FIELD_NAME.TAX_TYPE])   : '';

    requestParams.startIdx  = params[FIELD_NAME.START_IDX]   ? Number(params[FIELD_NAME.START_IDX])   : 0;
    requestParams.pageSize  = params[FIELD_NAME.PAGE_SIZE]   ? Number(params[FIELD_NAME.PAGE_SIZE])   : 0;
    requestParams.txnCount  = params[FIELD_NAME.TXN_COUNT]   ? Number(params[FIELD_NAME.TXN_COUNT])   : 0;
    
    requestParams.allTxnIdList     = this.convertList(params[FIELD_NAME.ALL_TXN_ID_LIST]);
    requestParams.selectedTxnIdList= this.convertList(params[FIELD_NAME.SELECTED_TXN_ID_LIST]);
    requestParams.periodIds        = this.convertList(params[FIELD_NAME.TAX_PERIOD_LIST]);
    requestParams.wtaxCodeIds      = this.convertList(params[FIELD_NAME.WTAX_CODE_LIST]);
    requestParams.purchaseTaxPoint = params[FIELD_NAME.PURCHASE_TAX_POINT] || '';
    requestParams.cacheId          = params[FIELD_NAME.CACHE_ID] || '';
    
    return requestParams;
};

WTax.Form2307.SelectTxnConverter.prototype.convertList = function _convertList(stringList) {
    if (!stringList) {
        return [];
    }
    var list = stringList.split(',') || [];
    for (var i = 0; i < list.length; i++) {
        list[i] = Number(list[i]);
    }
    return list;
};

WTax.Form2307.SelectTxnConverter.prototype.convertData = function _convertData(rawData) {
    var viewData = new WTax.Form2307.SelectTxnViewData();
    viewData.formTemplate = rawData.formTemplate;
    viewData.formParams = this.getFormParams(rawData);
    return viewData;
};


WTax.Form2307.SelectTxnConverter.prototype.getFormParams = function _getFormParams(rawData) {
    var viewData = {};
    
    viewData.txnUrl = rawData.txnUrl + '&' + FIELD_NAME.REQUEST_TYPE + '=' + REQUEST.SELECT_TXN_PAGE_DATA;
    viewData.pageSize = rawData.pageSize;

    var txnParams = this.getPageDataParams(rawData);
    viewData.txnParams = JSON.stringify(txnParams);
    
    return viewData;
};

WTax.Form2307.SelectTxnConverter.prototype.getPageDataParams = function _getPageDataParams(rawData) {
    var txnParams = {};
    txnParams[FIELD_NAME.SUBSIDIARY] = rawData.subId;
    txnParams[FIELD_NAME.NEXUS] = rawData.nexus;
    txnParams[FIELD_NAME.VENDOR] = rawData.vendorId;
    txnParams[FIELD_NAME.TAX_TYPE] = rawData.taxTypeId;
    txnParams[FIELD_NAME.PAGE_SIZE] = rawData.pageSize;
    txnParams[FIELD_NAME.TXN_COUNT] = rawData.txnCount;
    txnParams[FIELD_NAME.PURCHASE_TAX_POINT] = rawData.purchaseTaxPoint;
    txnParams[FIELD_NAME.TAX_PERIOD_LIST] = rawData.periodIds.join(',');
    txnParams[FIELD_NAME.WTAX_CODE_LIST] = rawData.wtaxCodeIds.join(',');
    return txnParams;
};

WTax.Form2307.SelectTxnConverter.prototype.convertTxnToGridData = function(txn, index, payment) {
    var ratio = payment ? parseFloat(payment.amount) / parseFloat(txn.totalTaxAmount) : 1;
    
    return gridData = [
        index,
        txn.id,
        txn.date,
        txn.refNo,
        _4601.formatCurrency(txn.totalBaseAmount * ratio),
        _4601.formatCurrency(-txn.totalTaxAmount * ratio)
    ];
};

WTax.Form2307.SelectTxnConverter.prototype.convertTxnListToGridData = function(txnList, paymentList) {
    var list = [];
    for (var i = 0; txnList && i < txnList.length; i++) {
        list.push(this.convertTxnToGridData(txnList[i], i, paymentList ? paymentList[txnList[i].id] : null));
    }
    return list;
};
