/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.PdfIndividualConverter = function _PdfIndividualConverter() {
    WTax.Form2307.PdfConverter.call(this);
};

WTax.Form2307.PdfIndividualConverter.prototype = Object.create(WTax.Form2307.PdfConverter.prototype);

WTax.Form2307.PdfIndividualConverter.prototype.convertWTaxList = function _convertWTaxList(rawData, taxCodeMap) {
    var lineCount = rawData.wtaxObj ? rawData.wtaxObj.length : 0;
    var params = {
        taxCodeMap: taxCodeMap,
        periodIds: rawData.periodIdsInQuarter,
        transactionMap: {}
    };

    for (var i = 0; i < lineCount; i++) {
        params.row = rawData.wtaxObj[i];
        this.convertWTaxRow(params);
    }

    return params.transactionMap;
};

WTax.Form2307.PdfIndividualConverter.prototype.convertWTaxRow = function _convertWTaxRow(params) {
    try {
        var taxCodeMap = params.taxCodeMap; 
        var row = params.row;
        var periodIds = params.periodIds;
        var taxCode = row.taxCode;
        var transactionMap = params.transactionMap;

        if (!taxCode) {
            nlapiLogExecution('DEBUG', 'WTax.Form2307.PdfIndividualConverter.convertWTaxRow row', JSON.stringify(row));
            return;
        }

        if (!transactionMap[row.id]) {
            transactionMap[row.id] = {
                wtaxRows: {}
            };
        }

        var wtaxRows = transactionMap[row.id].wtaxRows;
        var children = taxCodeMap[taxCode].children;
        for (var c in children) {
            var rowData = this.convertRowData(wtaxRows, c, taxCodeMap, periodIds, row, children);
            transactionMap[row.id].wtaxRows[c] = rowData.rowObj;
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.PdfIndividualConverter.convertWTaxRow', WTaxError.LEVEL.ERROR);
    }
};

WTax.Form2307.PdfIndividualConverter.prototype.convertToContent = function _convertToContent(transactions) {
    var keys = Object.keys(transactions);
    if (keys.length == 0) {
        // return a transaction with a single page with empty wtaxRows and zero totals
        return [[{
            wtaxRows: [],
            totals: new WTax.Form2307.WTaxTotals()
        }]];
    }

    var txnList = [];
    for (var txn in transactions) {
        var wtaxRows = transactions[txn].wtaxRows;

        txnList.push(this.convertToPage(wtaxRows));
    }

    return txnList;
};
