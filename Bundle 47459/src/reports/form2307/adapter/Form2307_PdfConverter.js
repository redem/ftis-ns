/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.PdfConverter = function _PdfConverter() {
};

WTax.Form2307.PdfConverter.prototype = Object.create(WTax.Form2307.ReportConverter.prototype);

WTax.Form2307.PdfConverter.prototype.convertHeader = function _convertHeader(rawData) {
    var headerObj = {
        payeeName      : nlapiEscapeXML(rawData.name || rawData.vendorObj.name),
        payeeAddress   : nlapiEscapeXML(rawData.address || rawData.vendorObj.regAddr),
        foreignAddress : nlapiEscapeXML(rawData.foreignAddress || ''),
        payorName      : nlapiEscapeXML(rawData.companyObj.name),
        payorAddress   : nlapiEscapeXML(rawData.companyObj.address),
    };
    var startDate = this.convertDateToList(rawData.periodObj.startDate);
    var endDate = this.convertDateToList(rawData.periodObj.endDate);
    
    this.addToHeaderObj(headerObj, 'fromPeriod', startDate);
    this.addToHeaderObj(headerObj, 'toPeriod', endDate);
    
    var rawTin = rawData.tin || rawData.vendorObj.vatRegNumber;
    var tin = rawTin ? rawTin.replace(/-/g, '') : '            ';
    this.addToHeaderObj(headerObj, 'payeeTin', tin);

    var rawZip = (rawData.zip || '').toString();
    var zip = this.formatZip(rawZip);
    this.addToHeaderObj(headerObj, 'payeeZip', zip);

    var rawForeignZip = (rawData.foreignZip || '').toString();
    var foreignZip = this.formatZip(rawForeignZip);
    this.addToHeaderObj(headerObj, 'payeeFZip', foreignZip);
    
    var coTin = this.formatTin(rawData.companyObj.tin);
    coTin = coTin ? coTin.replace(/-/g, '') : '            ';
    this.addToHeaderObj(headerObj, 'payorTin', coTin);
    
    var rawCoZip = (rawData.companyObj.zip || '').toString();
    var coZip = this.formatZip(rawCoZip);
    this.addToHeaderObj(headerObj, 'payorZip', coZip);
    
    return headerObj;
};

WTax.Form2307.PdfConverter.prototype.convertDateToList = function _convertDateToList(dateString) {
    var data = this.convertDateToString(dateString);
    return data.split('');
};

WTax.Form2307.PdfConverter.prototype.convertDateToString = function _convertDateToString(dateString) {
    var date = nlapiStringToDate(dateString);
    var data = '';
    data += this.pad(date.getMonth() + 1, 2);
    data += this.pad(date.getDate(), 2);
    data += this.pad(date.getYear(), 2);
    return data;
};

WTax.Form2307.PdfConverter.prototype.addToHeaderObj = function _addToHeaderObj(headerObj, prefix, list) {
    for (var i = 0; i < list.length; i++) {
        headerObj[prefix + i] = list[i];
    }
};


WTax.Form2307.PdfConverter.prototype.formatZip = function _formatZip(zip) {
    if (zip.length > 4) {
    	return this.pad(zip, LENGTH.ZIP).split('');
    }
    
    return zip.split('');
};

WTax.Form2307.PdfConverter.prototype.convertData = function _convertData(rawData) {
    try {
        if (rawData.purchaseTaxPoint == TAX_POINT.PAYMENT) {
            this.calculateBillRatios(rawData.billTotalsList);
        }

        var taxCodeMap   = this.createTaxCodeMap(rawData);
        var transactions = this.convertWTaxList(rawData, taxCodeMap);

        var data          = new WTax.Form2307.ViewData();
        data.isOneWorld   = rawData.isOneWorld;
        data.fieldData    = this.convertFieldData(rawData);
        data.message      = this.convertMessage(data.fieldData);
        data.fileDep      = rawData.fileDep;
        data.payee        = this.convertPayee(rawData.vendorObj);
        data.fileName     = this.convertFileName(rawData);
        data.header       = this.convertHeader(rawData);
        data.content      = this.convertToContent(transactions);

        return data;
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.Form2307.PdfConverter.convertData', WTaxError.LEVEL.ERROR);
    }
};

WTax.Form2307.PdfConverter.prototype.convertToContent = function _convertToContent(transactions) {
    return [this.convertToPage(transactions.rows)];
};

WTax.Form2307.PdfConverter.prototype.convertFileName = function _convertToFileName(rawData) {
    var fileNameObj = [];
    
    var name = this.getFileNameVendorName(rawData.vendorObj);
    if (name) {
        fileNameObj.push(name); 
    }
    
    var tin = this.getFileNameTin(rawData.vendorObj);
    if (tin) {
        fileNameObj.push(tin); 
    }
    
    var date = this.getFileNameDate(rawData.periodObj.endDate);
    if (date) {
        fileNameObj.push(date); 
    }
    
    return fileNameObj.join('_') + '.pdf';
};

WTax.Form2307.PdfConverter.prototype.getFileNameVendorName = function _getFileNameVendorName(vendorObj) {
    if (!vendorObj) {
        return '';
    }
    
    if (!vendorObj.isPerson) {
        return vendorObj.companyName || '';
    }
    
    var name = [];
    if (vendorObj.firstName) {
        name.push(vendorObj.firstName);
    }
    if (vendorObj.middleName) {
        name.push(vendorObj.middleName);
    }
    if (vendorObj.lastName) {
        name.push(vendorObj.lastName);
    }
    return name.join(' ');
};

WTax.Form2307.PdfConverter.prototype.getFileNameTin = function _getFileNameTin(vendorObj) {
    if (!vendorObj || !vendorObj.vatRegNumber) {
        return '';
    }
    
    var tin = vendorObj.vatRegNumber.replace(/-/g, '');
    return tin.slice(0, 13);
};

WTax.Form2307.PdfConverter.prototype.getFileNameDate = function _getFileNameDate(dateString) {
    if (!dateString) {
        return '';
    }
    var date = nlapiStringToDate(dateString);
    var data = '';
    data += this.pad(date.getMonth() + 1, 2);
    data += this.pad(date.getDate(), 2);
    data += this.pad(date.getFullYear(), 4);
    return data;
};

WTax.Form2307.PdfConverter.prototype.convertToPage = function _convertToPage(wtaxRows) {
	var keys = Object.keys(wtaxRows);
	var pageRows = [];
	var pageTotals = new WTax.Form2307.WTaxTotals();

	if (keys.length == 0) {
		// return a page with empty wtaxRows and zero totals
		return [{
			wtaxRows: pageRows,
			totals: pageTotals
		}];
	}

	var keyCount = keys.length;
	var page = [];

	for (var i = 0; i < keyCount; i++) {
	    var taxCode = keys[i];
		pageTotals.quarterAmount1 += wtaxRows[taxCode].quarterAmount1;
		pageTotals.quarterAmount2 += wtaxRows[taxCode].quarterAmount2;
		pageTotals.quarterAmount3 += wtaxRows[taxCode].quarterAmount3;
		pageTotals.totalBaseAmount += wtaxRows[taxCode].totalBaseAmount;
		pageTotals.totalTaxAmount += wtaxRows[taxCode].totalTaxAmount;
		pageRows.push(wtaxRows[taxCode]);
		
		if (pageRows.length == FORM2307_MAX_EWT_ROWS || (i + 1) == keyCount) {
			page.push({
				wtaxRows: pageRows,
				totals: pageTotals,
			});
			pageRows = [];
			pageTotals = new WTax.Form2307.WTaxTotals();
		}
	}
	
	return page;
};
