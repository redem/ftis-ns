/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.SelectTxnCreator = function _SelectTxnFormCreator(converter, viewCreator) {
    WTax.Form2307.FormCreator.call(this, converter, viewCreator);
    this.TEMPLATE = {};
    this.TEMPLATE.FORM = 'Form2307_SelectTxnTemplate.html';
};

WTax.Form2307.SelectTxnCreator.prototype = Object.create(WTax.Form2307.FormCreator.prototype);

WTax.Form2307.SelectTxnCreator.prototype.processRequest = function _processRequest(requestObj, responseObj) {
    var requestParams = this.converter.getAllRequestParams(requestObj);
    nlapiLogExecution('DEBUG', 'SelectTxnCreator.processRequest : requestType', requestParams.requestType);
    nlapiLogExecution('DEBUG', 'SelectTxnCreator.processRequest : requestParams', JSON.stringify(requestParams));
    
    switch(requestParams.requestType) {
        case REQUEST.SELECT_TXN_PAGE_DATA :
            var pageData = this.getPageData(requestParams);
            responseObj.write(JSON.stringify(pageData));
            break;
        case REQUEST.SELECT_TXN_SAVE_TXN :
            var cacheInfo = this.saveTxn(requestParams);
            responseObj.write(JSON.stringify(cacheInfo));
            break;
        default:    
            var form = this.getForm(requestParams);
            responseObj.writePage(form);
            break;
    }
};

WTax.Form2307.SelectTxnCreator.prototype.getForm = function _getForm(requestParams) {
    try {
        var rawData = requestParams;
        
        var context = nlapiGetContext();
        var configuration = nlapiLoadConfiguration('userpreferences');
        rawData.isOneWorld = context.getFeature('SUBSIDIARIES');
        var app = new SFC.System.Application(WTAX_APP_ID);
        
        rawData.pageSize = configuration.getFieldValue('LISTSEGMENTSIZE');
        rawData.txnUrl = nlapiResolveURL('SUITELET', context.getScriptId(), REPORT_MAP[FORM2307].selectTxnDeployment);
        rawData.formTemplate = app.GetFileContent(this.TEMPLATE.FORM, true);
        var viewData = this.converter.convertData(rawData);
        return this.viewCreator.createForm(viewData);
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.SelectTxnCreator.getForm', ex.toString());
        throw nlapiCreateError('FORM_2307_POPUP', 'Error in creating popup');
    }
};

WTax.Form2307.SelectTxnCreator.prototype.getPageData = function _getPageData(requestParams) {
    try {
        var start = requestParams.startIdx || 0;
        var limit = requestParams.pageSize || 0;
        var total = requestParams.txnCount || 0;
        
        var result = {
            total : total,
            data: [],
            message: 'loading successful', 
            success: true
        };
        
        var pageData = this.dataService.getData({
            daoName: 'Form2307_TransactionTotalsDao',
            daoParameters: {
                InternalId: requestParams.allTxnIdList,
                WTaxCodeIds: requestParams.wtaxCodeIds
            },
            start: start,
            end: start + limit
        });
        
        var payments = null;
        if (requestParams.purchaseTaxPoint == TAX_POINT.PAYMENT) {
            payments = this.dataService.getData({
                daoName: 'Form2307_PaymentDao',
                daoParameters: {
                    BillIds: this.converter.extractIds(pageData),
                    SelectedPeriodIds: requestParams.periodIds
                }
            });
            
            payments = this.converter.convertListToMap(payments, 'docRefId', function(a, b) {
                return {
                    id: a.id,
                    docRefId: a.docRefId,
                    paymentRefId: a.paymentRefId,
                    amount: parseFloat(a.amount) + parseFloat(b.amount)
                };
            });
        }
        
        result.data = this.converter.convertTxnListToGridData(pageData, payments);
        
        return result;
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.SelectTxnCreator.getPageData', ex.toString());
        return {success:false};
    }
};

WTax.Form2307.SelectTxnCreator.prototype.saveTxn = function _saveTxn(requestParams) {
    try {
        var formatter = new WTax.Form2307.CacheFormatter();
        var cacheManager = new WTax.Lib.CacheManager(formatter);
        var userId = nlapiGetContext().getUser();
        var id = requestParams.cacheId;
        if (id) {
            id = cacheManager.update(requestParams.cacheId, requestParams.selectedTxnIdList);
        } else {
            id = cacheManager.create(userId, requestParams.selectedTxnIdList);
        }
        
        nlapiLogExecution('DEBUG', 'SelectTxnCreator.saveTxn : cache id', id);
        
        return { cacheId : id, success: true };
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.SelectTxnCreator.saveTxn', ex.toString());
        return { success: false };
    }    
};
