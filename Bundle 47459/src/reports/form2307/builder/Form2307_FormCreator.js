/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.FormCreator = function _FormCreator(converter, viewCreator) {
	this.TEMPLATE = {
		FORM    : '',
		HEADER  : '',
		ROW     : ''
	};

	this.converter = converter;
	this.viewCreator = viewCreator;
	this.dataService = new WTax.DataCollectionService();
	this.dataService.addThresholdMonitor(new WTax.ApiGovernanceMonitor('suitelet'));
	this.dataService.addThresholdMonitor(new WTax.ScriptTimeoutMonitor('suitelet'));
};

WTax.Form2307.FormCreator.prototype = Object.create(WTax.Form2307.ReportCreator.prototype);

WTax.Form2307.FormCreator.prototype.processRequest = function _processRequest(requestObj, responseObj) {
	var form = this.getForm(requestObj);
	responseObj.writePage(form);
};

WTax.Form2307.FormCreator.prototype.getForm = function _getForm(requestObj) {
	try {
		var context = nlapiGetContext();
		var rawData = this.converter.getAllRequestParams(requestObj);
		var currentPeriod = new SFC.System.TaxPeriod().GetCurrentPeriod().GetId();

		nlapiLogExecution('DEBUG', 'FormCreator.getForm : requestParams', JSON.stringify(rawData));

		rawData.isOneWorld = context.getFeature('SUBSIDIARIES');
		rawData.isMultiCurrency = context.getFeature('MULTICURRENCY');
		rawData.script = context.getScriptId();
		rawData.userId = context.getUser();
		rawData.companyObj = this.getCompanyInfo(rawData);
		rawData.fiscalCalendarId = _4601.getFiscalCalendar(rawData.subId);
		rawData.taxPeriodId = rawData.taxPeriodId || currentPeriod;

		var taxDaoParams = { FiscalCalendarId: rawData.fiscalCalendarId };
		var taxDao = new WTax.DAO.TaxPeriodDao();
		rawData.periodIds = taxDao.getCoveredPeriodIds(rawData.taxPeriodId, rawData.taxPeriodId, taxDaoParams);
		rawData.periodObj = taxDao.getPeriodById(rawData.taxPeriodId, taxDaoParams);

		rawData.vendorObj = rawData.vendorId ? new WTax.DAO.VendorDao().getVendorById(rawData.vendorId) : '';
		rawData.subList = this.getSubIds();
		rawData.fileDep = this.getFileDependencies(requestObj);
		rawData.country = this.getCountry(rawData);

		if (rawData.country) {
			var nexusId = new WTax.DAO.NexusDao().getByCountry(rawData.country).id;
			rawData.purchaseTaxPoint = new WTax.DAO.WTaxSetupDao().getByNexus(nexusId).purchaseTaxPoint;
		}

		var serviceParams = {
			daoName : 'WTaxCodeDao',
			daoParameters : {'WTaxTypeId': rawData.taxTypeId}
		};
		rawData.wtaxCodeList = rawData.taxTypeId ? this.dataService.getData(serviceParams) : [];
		rawData.wtaxCodeIds = rawData.taxTypeId ? this.converter.extractIds(rawData.wtaxCodeList) : [];

		serviceParams.daoName = 'GroupedWTaxCodeDao';
		rawData.groupedWtaxCode = rawData.vendorId ? this.dataService.getData(serviceParams) : [];

		rawData.allTxnIdList = rawData.vendorId ? this.converter.extractTxnIds(this.getTxnIdList(rawData)) : [];
		var cache = rawData.vendorId ? this.getCache(rawData) : {};
		rawData.cacheId = cache.id;
		rawData.selectedTxnIdList = rawData.vendorId ? cache.content : [];
		rawData.txnCount = rawData.vendorId ? rawData.allTxnIdList.length : 0;

		var txnData = rawData.vendorId ? this.getTxnData(rawData) : {};
		rawData.wtaxObj = rawData.vendorId ? txnData.purchaseList : [];
		rawData.billTotalsList = rawData.vendorId ? txnData.billTotalsList : [];

		var quarterPeriod = rawData.vendorId ? taxDao.getQuarterOfBasePeriod(rawData.taxPeriodId, taxDaoParams) : '';
		rawData.periodIdsInQuarter = rawData.vendorId ? taxDao.getCoveredPeriodIds(quarterPeriod.id, quarterPeriod.id, taxDaoParams) : [];

		var viewData = this.converter.convertData(rawData);
		return this.viewCreator.createForm(viewData);
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.Form2307.FormCreator.getForm', ex.toString());
		throw nlapiCreateError('FORM_2307', 'Error in creating report');
	}
};

WTax.Form2307.FormCreator.prototype.getFileDependencies = function _getFileDependencies(requestObj) {
	try {
		// TODO: Create FileDao
		var app = new SFC.System.Application(WTAX_APP_ID);
		var helperUtils = new _4601.HelperUtils(app, requestObj);
		var fileObj = {
			logoUrl        : nlapiEscapeXML(helperUtils.getImageFullUrl(FILENAME.BIR_LOGO)),
			fontUrl        : nlapiEscapeXML(helperUtils.getFullFileUrl(FILENAME.FONT)),
			pixelUrl       : nlapiEscapeXML(helperUtils.getFullFileUrl(FILENAME.BLACK_PIXEL)),
		};

		if (this.TEMPLATE.FORM) {
			fileObj.formTemplate = app.GetFileContent(this.TEMPLATE.FORM, true);
		}
		if (this.TEMPLATE.ROW) {
			fileObj.rowTemplate = app.GetFileContent(this.TEMPLATE.ROW, true);
		}
		if (this.TEMPLATE.HEADER) {
			fileObj.headerTemplate = app.GetFileContent(this.TEMPLATE.HEADER, true);
		}

		return fileObj;
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.Form2307.FormCreator.getFileDependencies', ex.toString());
		throw nlapiCreateError('FORM_2307_SEARCH_ERROR', 'Error in accessing data');
	}
};

WTax.Form2307.FormCreator.prototype.getSubIds = function _getSubIds() {
	try {
		// TODO: Create SubsidiaryDao
		return _4601.getSubsidiaryIdsThatWTaxAppliesTo();
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.Form2307.FormCreator.getSubIds', ex.toString());
		throw nlapiCreateError('FORM_2307_SEARCH_ERROR', 'Error in accessing data');
	}
};

WTax.Form2307.FormCreator.prototype.getCountry = function _getCountry(rawData) {
	try {
		if (!rawData.subId && !rawData.nexus) {
			return '';
		}

		if (rawData.isOneWorld) {
			return SFC.System.getSubsidiaryCountry(rawData.subId);
		} else {
			return rawData.nexus;
		}
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.Form2307.FormCreator.getCountry', ex.toString());
		throw nlapiCreateError('FORM_2307_DATA_ERROR', 'Error in processing data');
	}
};


WTax.Form2307.FormCreator.prototype.getTxnData = function _getTxnData(params) {
	try {
		var transactions = {
			purchaseList: [],
			billTotalsList: []
		};

		if (!params.vendorId) {
			return transactions;
		}

		var idList = params.selectedTxnIdList;

		var serviceParams = {
			daoName: '',
			daoParameters: {
				VendorId          : params.vendorId,
				SubId             : params.subId,
				IsOneWorld        : params.isOneWorld,
				IsMultiCurrency   : params.isMultiCurrency,
				SelectedPeriodIds : params.periodIds,
				WTaxCodeIds       : params.wtaxCodeIds,
				purchaseTaxPoint  : params.purchaseTaxPoint,
				InternalId        : []
			}
		};

		serviceParams.daoParameters.InternalId = idList;
		serviceParams.daoName = 'Form2307_PurchaseDao';
		transactions.purchaseList = this.dataService.getData(serviceParams);

		if (params.purchaseTaxPoint == TAX_POINT.PAYMENT) {
			serviceParams.daoName = 'Form2307_TransactionTotalsDao';
			serviceParams.daoParameters = {InternalId: idList, TransactionTypes: ['VendBill']};
			serviceParams.daoParameters.WTaxCodeIds = null;
			transactions.billTotalsList = this.dataService.getData(serviceParams);
		} else if (params.purchaseTaxPoint == TAX_POINT.ACCRUAL) {
			serviceParams.daoName = 'Form2307_PHImplemDao';
			var phList = this.dataService.getData(serviceParams);
			transactions.purchaseList = transactions.purchaseList.concat(phList);
		}

		return transactions;
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.Form2307.FormCreator.getTxnData', ex.toString());
		throw nlapiCreateError('FORM_2307_SEARCH_ERROR', 'Error in accessing data');
	}

};

WTax.Form2307.FormCreator.prototype.getTxnIdList = function _getTxnIdList(params) {
	try {
		var idList = {
			accrualIdList: [],
			billIdList: [],
			chequeIdList: []
		};

		var serviceParams = {
			daoName: '',
			daoParameters: {
				VendorId          : params.vendorId,
				SubId             : params.subId,
				IsOneWorld        : params.isOneWorld,
				SelectedPeriodIds : params.periodIds,
				WTaxCodeIds       : params.wtaxCodeIds,
				purchaseTaxPoint  : params.purchaseTaxPoint
			}
		};

		if (serviceParams.daoParameters.purchaseTaxPoint == TAX_POINT.ACCRUAL) {
			serviceParams.daoName = 'Form2307_AccrualDao';
			var billList = this.dataService.getData(serviceParams);
			idList.accrualIdList = this.converter.extractIds(billList);
		} else {
			serviceParams.daoName = 'Form2307_CheckDao';
			var chequeList = this.dataService.getData(serviceParams);
			idList.chequeIdList = this.converter.extractIds(chequeList);

			serviceParams.daoName = 'Form2307_PaymentDao';
			var paymentList = this.dataService.getData(serviceParams);
			idList.billIdList = this.converter.getBillIdsFromPayments(paymentList);
		}

		return idList;
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.Form2307.FormCreator.getTxnIdList', ex.toString());
		throw nlapiCreateError('FORM_2307_SEARCH_ERROR', 'Error in accessing data');
	}
};


//TODO: Create sub dao / company dao
WTax.Form2307.FormCreator.prototype.getCompanyInfo = function _getCompanyInfo(params) {
	try {
		var company = {};
		if (!params.subId && !params.nexus) {
			return company;
		}

		var companyRecord = _4601.getCompanyRecord(params.isOneWorld, params.subId);

		// TODO: Move this to dao and adapter!
		company.zip = companyRecord.getFieldValue('zip');
		var ctin = companyRecord.getFieldValue('federalidnumber') || companyRecord.getFieldValue('employerid');
		company.tin = ctin ? ctin.substring(0,12) : '';
		company.name = companyRecord.getFieldValue('legalname') || companyRecord.getFieldValue('name');

		var address1 = companyRecord.getFieldValue('addr1') || companyRecord.getFieldValue('address1');
		var address2 = companyRecord.getFieldValue('addr2') || companyRecord.getFieldValue('address2');
		var city = companyRecord.getFieldValue('city');
		var state = companyRecord.getFieldText('dropdownstate') || companyRecord.getFieldValue('state');

		company.address = [
			address1 ? address1 + ' ' : '',
			address2 ? address2 + ' ' : '',
			city ? city + ' ' : '',
			state ? state : ''
		].join('');

		return company;
	} catch (ex) {
		nlapiLogExecution('ERROR', 'WTax.Form2307.FormCreator.getCompanyInfo', ex.toString());
		throw nlapiCreateError('FORM_2307_SEARCH_ERROR', 'Error in accessing data');
	}
};


WTax.Form2307.FormCreator.prototype.getCache = function _getCache(params) {
	var cache = {
		id: '',
		content: ''
	};

	var formatter = new WTax.Form2307.CacheFormatter();
	var manager = new WTax.Lib.CacheManager(formatter);

	if (params.cacheId && !params.reloadCache) {
		cache.id = params.cacheId;
		cache.content = manager.getPropertiesById(params.cacheId);
	} else if (params.cacheId && params.reloadCache) {
		cache.id = params.cacheId;
		cache.content = params.allTxnIdList;
		manager.update(cache.id, params.allTxnIdList);
	} else if (!params.cacheId) {
		cache.id = manager.getIdByName(params.userId);

		if (cache.id > -1) {
			manager.update(cache.id, params.allTxnIdList);
		} else {
			cache.id = manager.create(params.userId, params.allTxnIdList);
		}

		cache.content = params.allTxnIdList;
	}

	return cache;
}
