/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.OnlineFormCreator = function _OnlineFormCreator(converter, viewCreator) {
    WTax.Form2307.FormCreator.call(this, converter, viewCreator);
    this.TEMPLATE = {
        FORM    : 'Form2307_OnlineTemplate.html',
        HEADER  : '',
        ROW     : 'Form2307_OnlineRowTemplate.html'
    };
};

WTax.Form2307.OnlineFormCreator.prototype = Object.create(WTax.Form2307.FormCreator.prototype);

