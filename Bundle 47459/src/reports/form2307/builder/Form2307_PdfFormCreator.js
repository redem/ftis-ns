/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var WTax = WTax || {};
WTax.Form2307 = WTax.Form2307 || {};

WTax.Form2307.PdfFormCreator = function _PdfFormCreator(converter, viewCreator) {
    WTax.Form2307.FormCreator.call(this, converter, viewCreator);
    this.TEMPLATE = {
        FORM    : 'Form2307_PdfTemplate.xml',
        HEADER  : '',
        ROW     : ''
    };
};

WTax.Form2307.PdfFormCreator.prototype = Object.create(WTax.Form2307.FormCreator.prototype);

WTax.Form2307.PdfFormCreator.prototype.processRequest = function _processRequest(requestObj, responseObj) {
    try {
        var pdf = this.getForm(requestObj);
        var pdfFile = nlapiXMLToPDF('<?xml version="1.0"?>\n<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">\n' + pdf.form);
        responseObj.setContentType('PDF', pdf.fileName, 'attachment');
        responseObj.write(pdfFile.getValue());
    } catch (ex) {
        nlapiLogExecution('ERROR', 'WTax.Form2307.PdfFormCreator.processRequest', ex.toString());
        throw nlapiCreateError('FORM_2307_PDF_ERROR', 'Unable to create pdf form');
    } 
};

