/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.Form2307_PHImplem = function _PHImplem(id) {
	var txn = {
		id : id,
		taxPeriod : '',
		taxBase : '',
		taxAmount : '',
		isTaxGroup : '',
		groupedTaxCode : '',
		taxRate : '',
		recordType : '',
		taxName : '',
		taxCode : '',
		vendorId : ''
	};
	return txn;
};

WTax.DAO.Form2307_PHImplemDao = function _PHImplementationDao(params) {
	if (!params) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Parameter is null or undefined.');
		WTaxError.throwError(error, 'WTax.DAO.Form2307_PHImplemDao', WTaxError.LEVEL.ERROR);
	}

	if (!params.VendorId || !params.SelectedPeriodIds || !params.WTaxCodeIds) {
		var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Incomplete parameter, is null or undefined.');
		WTaxError.throwError(error, 'WTax.DAO.Form2307_PHImplemDao', WTaxError.LEVEL.ERROR);
	}

	this.type = 'transaction';
	this.savedSearch = 'customsearch_2307_ph_implem_trxn';
	
	this.IsMultiCurrency = params.IsMultiCurrency;
	this.columns = {
		baseAmount: new nlobjSearchColumn('formulacurrency', null, 'sum'),
		taxAmount: new nlobjSearchColumn('formulacurrency', null, 'sum')
	};
	
	WTax.DAO.BaseDao.call(this, params);
};
WTax.DAO.Form2307_PHImplemDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);

WTax.DAO.Form2307_PHImplemDao.prototype.getFilters = function _getFilters(params) {
	var filters = [
				   new nlobjSearchFilter('entity', null, 'is', params.VendorId),
				   new nlobjSearchFilter('custcol_4601_witaxcode', null, 'anyof', params.WTaxCodeIds)
				   ];

	if (params.IsOneWorld && params.SubId) {
		filters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.SubId));
	}

	if (params.SelectedPeriodIds.length == 1) {
		filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[0]));
	} else if (params.SelectedPeriodIds.length == 3) {
		filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[0], null, 1, 0, true));
		filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[1], null, 0, 0, true));
		filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[2], null, 0, 1, false));
	} else {
		var error = nlapiCreateError('INVALID_PARAMETER', 'SelectedPeriodIds is incorrect');
		WTaxError.throwError(error, 'WTax.DAO.Form2307_PHImplemDao', WTaxError.LEVEL.ERROR);
	}

	var journalTypes = ['CardChrg','Check','VendBill'];
	var journalTypeIds = this.getJournalTypeIds(journalTypes);
	filters.push(new nlobjSearchFilter('custcol_ph4014_src_jrnltrantypeid', null, 'anyof', journalTypeIds));
	filters = this.getJournalReversalFilters(filters);

	return filters;
};

WTax.DAO.Form2307_PHImplemDao.prototype.getColumns = function _getColumns(params) {
	var columns = [];
	if (this.IsMultiCurrency) {
		this.columns.baseAmount.setFormula('{custcol_4601_witaxbaseamount}*{exchangerate}');
		columns.push(this.columns.baseAmount);

		this.columns.taxAmount.setFormula('{custcol_4601_witaxamount}*{exchangerate}');
		columns.push(this.columns.taxAmount);
	}
	return columns;
};

WTax.DAO.Form2307_PHImplemDao.prototype.convertToObject = function _convertToObject(nlObject) {
	var txn = new WTax.DAO.Form2307_PHImplem();

	try {
		txn.id = nlObject.getId();
		txn.taxPeriod = nlObject.getValue('taxperiod', null, 'group');
		txn.taxBase = this.IsMultiCurrency ? nlObject.getValue(this.columns.baseAmount) : nlObject.getValue('custcol_4601_witaxbaseamount', null, 'sum');
		txn.taxAmount = this.IsMultiCurrency ? nlObject.getValue(this.columns.taxAmount) : nlObject.getValue('custcol_4601_witaxamount', null, 'sum');
		txn.isTaxGroup = nlObject.getValue('custrecord_4601_wtc_istaxgroup', 'custcol_4601_witaxcode', 'group');
		txn.groupedTaxCode = nlObject.getValue('custrecord_4601_wtc_groupedwitaxcodes', 'custcol_4601_witaxcode', 'group');
		txn.taxRate = nlObject.getValue('custrecord_4601_wtc_rate', 'custcol_4601_witaxcode', 'group');
		txn.recordType = nlObject.getValue('recordtype', null, 'group');
		txn.taxName = nlObject.getValue('custrecord_4601_wtc_name', 'custcol_4601_witaxcode', 'group');
		txn.taxCode = nlObject.getValue('custcol_4601_witaxcode', null, 'group');
		txn.vendorId = nlObject.getValue('custcol_ph4014_src_vendorid', null, 'group');
	} catch (ex) {
		WTaxError.throwError(ex, 'WTax.DAO.Form2307_PHImplemDao.convertToObject', WTaxError.LEVEL.ERROR);
	}

	return txn;
};

WTax.DAO.Form2307_PHImplemDao.prototype.getJournalTypeIds = function _getJournalTypeIds(journalNames) {
	var journalIds = [];

	try {
		var name = new nlobjSearchColumn('name');
		var arrResult = nlapiSearchRecord('customrecord_ph4014_wtax_jrnl_type', null, null, name);

		var journalMap = [];
		for(var i in arrResult) {
			journalMap[arrResult[i].getValue(name)] = arrResult[i].getId();
		}

		for(var j in journalNames) {
			journalIds.push(journalMap[journalNames[j]]);
		}
	} catch(ex) {
		WTaxError.throwError(ex, 'WTax.DAO.Form2307_PHImplemDao.getJournalTypeIds', WTaxError.LEVEL.ERROR);
	}

	return journalIds;
};

WTax.DAO.Form2307_PHImplemDao.prototype.getJournalReversalFilters = function _getJournalReversalFilters(filters) {
	try {
		filters.push(nlobjSearchFilter('reversaldate', null, 'isempty', null, null, 1, 0, true));
		filters.push(nlobjSearchFilter('reversaldate', null, 'after', new Date(), null, 0, 1, false));
	} catch(ex) {
		WTaxError.throwError(ex, 'WTax.DAO.Form2307_PHImplemDao.getJournalReversalFilters', WTaxError.LEVEL.ERROR);
	}

	return filters;
};
