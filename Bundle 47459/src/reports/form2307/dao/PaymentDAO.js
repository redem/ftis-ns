/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.Form2307_Payment = function _Payment(id) {
    var txn = {
        id : id,
        docRefId : '',
        paymentRefId : '',
        amount : ''
    };
    return txn;
};

WTax.DAO.Form2307_PaymentDao = function _PaymentDao(params) {
    if (!params) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Parameter is null or undefined.');
        WTaxError.throwError(error, 'WTax.DAO.Form2307_PaymentDao', WTaxError.LEVEL.ERROR);
    }
    
    try {
        var search = nlapiLoadSearch('transaction', 'customsearch_2307_payment_trxn');
        var filters = [];
        
        if (params.BillIds) {
            if (params.BillIds.length > 0) {
                filters.push(new nlobjSearchFilter('internalid', 'custbody_4601_doc_ref_id', 'anyof', params.BillIds));
            } else {
                this.isEmpty = true;
                return;
            }
        }
        
        if (params.VendorId) {
            filters = [new nlobjSearchFilter('entity', null, 'is', params.VendorId)];
        }
        
        if (params.WTaxCodeIds) {
            filters.push(new nlobjSearchFilter('custcol_4601_witaxcode', 'custbody_4601_doc_ref_id', 'anyof', params.WTaxCodeIds));
        }
        
        if (params.SelectedPeriodIds) {
            var selectedPeriodLength = params.SelectedPeriodIds ? params.SelectedPeriodIds.length : 0;
            var selectedPeriod = params.SelectedPeriodIds;
            
            if (selectedPeriodLength == 1){
                filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[0]));
            } else {
                filters.push(new nlobjSearchFilter('taxperiod', null, 'is', selectedPeriod[0], null, 1, 0, true));
                filters.push(new nlobjSearchFilter('taxperiod', null, 'is', selectedPeriod[1], null, 0, 0, true));
                filters.push(new nlobjSearchFilter('taxperiod', null, 'is', selectedPeriod[2], null, 0, 1, false));    
            }
        }
        
        search.addFilters(filters);
        this.resultSet = search.runSearch();
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.Form2307_PaymentDao', WTaxError.LEVEL.ERROR);
    }
};
WTax.DAO.Form2307_PaymentDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);

WTax.DAO.Form2307_PaymentDao.prototype.convertToObject = function _convertToObject(nlObject) {
    var txn = new WTax.DAO.Form2307_Payment();
    
    try {
        txn.id = nlObject.getValue('internalid', null, 'GROUP');
        txn.docRefId = nlObject.getValue('internalid', 'custbody_4601_doc_ref_id', 'MAX');
        txn.paymentRefId = nlObject.getValue('internalid', 'custbody_4601_pymnt_ref_id', 'MAX');
        txn.amount = nlObject.getValue('amount', null, 'MIN');
        
        if (txn.amount == 0) {
            // Do not add to results list
            return null;
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.Form2307_PaymentDao.getList', WTaxError.LEVEL.ERROR);
    }
    
    return txn;
};
