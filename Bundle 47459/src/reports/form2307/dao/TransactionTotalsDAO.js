/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};


WTax.DAO.Form2307_TransactionTotals = function _Form2307_TransactionTotals(id) {
    return {
        id: id,
        date: '',
        refNo: '',
        totalBaseAmount: 0,
        totalTaxAmount: 0
    };
};

WTax.DAO.Form2307_TransactionTotalsDao = function _Form2307_TransactionTotalsDao(params) {
    if (!params) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Parameter is null or undefined.');
        WTaxError.throwError(error, 'WTax.DAO.Form2307_TransactionTotalsDao', WTaxError.LEVEL.ERROR);
    }
    
    if (!params.InternalId || (params.InternalId.length <= 0)) {
        this.isEmpty = true;
        this.resultSet = null;
        return;
    }
    
    this.type = 'transaction';
    this.savedSearch = 'customsearch_2307_transaction_totals';

	this.IsMultiCurrency = nlapiGetContext().getFeature('MULTICURRENCY');
	this.columns = {
		baseAmount: new nlobjSearchColumn('formulacurrency', null, 'sum'),
		taxAmount: new nlobjSearchColumn('formulacurrency', null, 'sum')
	};

    WTax.DAO.BaseDao.call(this, params);
};
WTax.DAO.Form2307_TransactionTotalsDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);

WTax.DAO.Form2307_TransactionTotalsDao.prototype.getFilters = function _getFilters(params) {
    if (this.isEmpty) {
        return [];
    }
    
    var filters = [new nlobjSearchFilter('internalid', null, 'anyof', params.InternalId)];
    
    if (params.WTaxCodeIds) {
        filters.push(new nlobjSearchFilter('custcol_4601_witaxcode', null, 'anyof', params.WTaxCodeIds));
    }
    
    if (params.TransactionTypes) {
        filters.push(new nlobjSearchFilter('type', null, 'anyof', params.TransactionTypes));
    }
    
    return filters;
};

WTax.DAO.Form2307_TransactionTotalsDao.prototype.getColumns = function _getColumns(params) {
	var columns = [];
	if (this.IsMultiCurrency) {
		this.columns.baseAmount.setFormula('{custcol_4601_witaxbaseamount}*{exchangerate}');
		columns.push(this.columns.baseAmount);

		this.columns.taxAmount.setFormula('{custcol_4601_witaxamount}*{exchangerate}');
		columns.push(this.columns.taxAmount);
	}
	return columns;
};

WTax.DAO.Form2307_TransactionTotalsDao.prototype.convertToObject = function _convertToObject(row) {
    var obj = new WTax.DAO.Form2307_TransactionTotals(row.getValue('internalid', null, 'GROUP'));
    
    try {
        obj.date = row.getValue('trandate', null, 'GROUP');
        obj.refNo = row.getValue('tranid', null, 'GROUP');
		obj.totalBaseAmount = this.IsMultiCurrency ? row.getValue(this.columns.baseAmount) : row.getValue('custcol_4601_witaxbaseamount', null, 'SUM');
		obj.totalTaxAmount = this.IsMultiCurrency ?  row.getValue(this.columns.taxAmount) : row.getValue('custcol_4601_witaxamount', null, 'SUM');
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.Form2307_TransactionTotalsDao.convertToObject', WTaxError.LEVEL.ERROR);
    }
    
    return obj;
};
