/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};

WTax.DAO.Form2307_Check = function _Check(id) {
	var check = {
		id : id
	};
	return check;
};

WTax.DAO.Form2307_CheckDao = function _CheckDao(params) {
    if (!params) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Parameter is null or undefined.');
        WTaxError.throwError(error, 'WTax.DAO.Form2307_CheckDao', WTaxError.LEVEL.ERROR);
    }
    
    if (!params.VendorId || !params.SelectedPeriodIds || !params.WTaxCodeIds) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Incomplete parameter, is null or undefined.');
        WTaxError.throwError(error, 'WTax.DAO.Form2307_CheckDao', WTaxError.LEVEL.ERROR);
    }

    this.type = 'transaction';
    this.savedSearch = 'customsearch_2307_payment_check_trxn';
    WTax.DAO.BaseDao.call(this, params);
};
WTax.DAO.Form2307_CheckDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);

WTax.DAO.Form2307_CheckDao.prototype.getFilters = function _getFilters(params) {
    var filters = [
                   new nlobjSearchFilter('entity', null, 'is', params.VendorId),
                   new nlobjSearchFilter('custcol_4601_witaxcode', null, 'anyof', params.WTaxCodeIds)
                   ];
    
    if (params.IsOneWorld && params.SubId) {
        filters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.SubId));
    }

    if (params.SelectedPeriodIds.length == 1) {
        filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[0]));
    } else if (params.SelectedPeriodIds.length == 3) {
        filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[0], null, 1, 0, true));
        filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[1], null, 0, 0, true));
        filters.push(new nlobjSearchFilter('taxperiod', null, 'is', params.SelectedPeriodIds[2], null, 0, 1, false));    
    } else {
        var error = nlapiCreateError('INVALID_PARAMETER', 'SelectedPeriodIds is incorrect');
        WTaxError.throwError(error, 'WTax.DAO.Form2307_CheckDao', WTaxError.LEVEL.ERROR);
    }
    
    return filters;
};

/* @return {Check} Check transaction*/
WTax.DAO.Form2307_CheckDao.prototype.convertToObject = function _convertToObject(nlObject) {
	var txn = new WTax.DAO.Form2307_Check();
	
	try {
		txn.id = nlObject.getValue('internalid', null, 'GROUP');
	} catch (ex) {
	    WTaxError.throwError(ex, 'WTax.DAO.Form2307_CheckDao.convertToObject', WTaxError.LEVEL.ERROR);
	}
	
	return txn;
};
