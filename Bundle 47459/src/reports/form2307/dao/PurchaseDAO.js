/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

if (!WTax) { var WTax = {}; }
WTax.DAO = WTax.DAO || {};


WTax.DAO.Form2307_Purchase = function _Purchase(id) {
    var txn = {
        id : id,
        taxPeriod : '',
        taxBase : '',
        taxAmount : '',
        isTaxGroup : '',
        groupedTaxCode : '',
        taxRate : '',
        recordType : '',
        taxName : '',
        taxCode : ''
    };
    return txn;
};


WTax.DAO.Form2307_PurchaseDao = function _PurchaseDao(params) {
    if (!params) {
        var error = nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'Parameter is null or undefined.');
        WTaxError.throwError(error, 'WTax.DAO.Form2307_PurchaseDao', WTaxError.LEVEL.ERROR);
    }
    
    if (!params.InternalId || (params.InternalId.length <= 0)) {
        this.isEmpty = true;
        this.resultSet = null;
        return;
    }

    this.type = 'transaction';
    this.savedSearch = 'customsearch_2307_trxn';
    
    this.IsMultiCurrency = params.IsMultiCurrency;
    this.columns = {
        baseAmount: new nlobjSearchColumn('formulacurrency', null, 'sum'),
        taxAmount: new nlobjSearchColumn('formulacurrency', null, 'sum')
    };
    
    this.purchaseTaxPoint = params.purchaseTaxPoint;
    
    WTax.DAO.BaseDao.call(this, params);
};
WTax.DAO.Form2307_PurchaseDao.prototype = Object.create(WTax.DAO.BaseDao.prototype);

WTax.DAO.Form2307_PurchaseDao.prototype.getFilters = function _getFilters(params) {
    if (this.isEmpty) {
        return [];
    }
    
    var filters = [new nlobjSearchFilter('internalid', null, 'anyof', params.InternalId)];
    if (params.IsOneWorld && params.SubId) {
        filters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.SubId));
    }
    return filters;
};

WTax.DAO.Form2307_PurchaseDao.prototype.getColumns = function _getColumns(params) {
    var columns = [];
    if (this.IsMultiCurrency) {
        this.columns.baseAmount.setFormula('{custcol_4601_witaxbaseamount}*{exchangerate}');
        columns.push(this.columns.baseAmount);

        this.columns.taxAmount.setFormula('{custcol_4601_witaxamount}*{exchangerate}');
        columns.push(this.columns.taxAmount);
    }
    return columns;
};

WTax.DAO.Form2307_PurchaseDao.prototype.convertToObject = function _convertToObject(nlObject) {
    var txn = new WTax.DAO.Form2307_Purchase();
    
    try {
        txn.id = nlObject.getValue('internalid', null, 'group');
        txn.taxPeriod = nlObject.getValue('taxperiod', null, 'group');
        txn.taxBase = this.IsMultiCurrency ? nlObject.getValue(this.columns.baseAmount) : nlObject.getValue('custcol_4601_witaxbaseamount', null, 'sum');
        txn.taxAmount = this.IsMultiCurrency? nlObject.getValue(this.columns.taxAmount) : nlObject.getValue('custcol_4601_witaxamount', null, 'sum');
        txn.isTaxGroup = nlObject.getValue('custrecord_4601_wtc_istaxgroup', 'custcol_4601_witaxcode', 'group');
        txn.groupedTaxCode = nlObject.getValue('custrecord_4601_wtc_groupedwitaxcodes', 'custcol_4601_witaxcode', 'group');
        txn.taxRate = nlObject.getValue('custrecord_4601_wtc_rate', 'custcol_4601_witaxcode', 'group');
        txn.recordType = nlObject.getValue('recordtype', null, 'group');
        txn.taxName = nlObject.getValue('custrecord_4601_wtc_name', 'custcol_4601_witaxcode', 'group');
        txn.taxCode = nlObject.getValue('custcol_4601_witaxcode', null, 'group');
        
        if (this.purchaseTaxPoint == 'onaccrual' && 
            txn.recordType == 'vendorcredit'){
            txn.taxBase *= -1;
            txn.taxAmount *= -1;
        }
    } catch (ex) {
        WTaxError.throwError(ex, 'WTax.DAO.Form2307_PurchaseDao.convertToObject', WTaxError.LEVEL.ERROR);
    }
    
    return txn;
};
