/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */
if (!MAP_REPORT) var MAP_REPORT = {};

if (!_4601) { var _4601 = {}; }

MAP_REPORT.getVendorData = (function () {
	var _NONE = "- None -";
    
    var _Context = nlapiGetContext();
    var _IsOneWorld = _Context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
    
    var _MAX_NUM_SEARCH_RESULTS = 1000;
    
    //Defined retrieval fields from old implementation
    var _obj_ph_VendorID = "custcol_ph4014_src_vendorid";
    
    //Retrieval fields from new implementation
    var _obj_BaseAmount = "custcol_4601_witaxbaseamount";
    var _obj_TaxAmount = "custcol_4601_witaxamount";
    var _obj_TaxCode = "custcol_4601_witaxcode";
    
    var _objPayeeFirst = "firstname";
    var _objPayeeMiddle = "middlename";
    var _objPayeeLast = "lastname";
    var _objPayeeCompany = "companyname";
    var _objPayeeFlag = "isperson";
    var _objTIN = "vatregnumber";
    var _objInternalID = "internalid";
    var _objReferenceNo = 'tranid';
    var _objRecordType = 'recordtype';
    var _objMemo = 'memomain';
    var _objTransactionDate = 'trandate';
    
    var _objTaxCodeRate = "taxCodeRate";
    var _objTaxCodeName = "taxCodeName";
    var _objTaxCodeDesc = "taxCodeDesc";
    var _objTaxTypeDesc = "taxTypeDesc";
    var _objIsTaxGroup = 'isTaxGroup';
    var _objGroupTaxCodes = 'groupTaxCodes';
    var _objTransID = 'transactionID';
    
    var billCreditMap = [];
    var grandTotal = { Income: 0, TaxWithheld: 0 };
    var allowedWithholdingTaxCodeIds = [];
    var groupWtaxCodes = [];
    var allValidGroupedWtaxCodes = [];
    var childrenTaxPeriod = [];
    var objSelectedPeriod = null;
    
    /**
     * searchType
     *  - SUMMARY
     *  - DETAIL
     *  - PURC_DETAIL
     *  - PURC_SUMMARY
     *  - MAP_DETAIL
     */
	function getVendorData(WTaxCodeIds, params, searchType) {
		var alphaListLineItems = [];
        var alphaListLineItemsMap = [];
    	var isGenericSearch = true;
    	allowedWithholdingTaxCodeIds = WTaxCodeIds;
    	getVendorData.params = params;
    	
    	if (allowedWithholdingTaxCodeIds.length > 0) {
    	    var taxTypesMap = _4601.getTaxTypesDetails(allowedWithholdingTaxCodeIds);
        	var purcTaxPoint = _4601.getPurchaseTaxPoint(params.subid, params.nexusid);
        	groupWtaxCodes = _4601.getTaxGroupWithValidTaxCodes(allowedWithholdingTaxCodeIds); //array of wtax group IDs
        	allValidGroupedWtaxCodes = _4601.getAllValidGroupedTaxCodes(allowedWithholdingTaxCodeIds);
        	objSelectedPeriod = new SFC.System.TaxPeriod().Load(params.periodId, params.fiscalCalendarId);
        	
        	if (objSelectedPeriod.GetType() == 'quarter' && childrenTaxPeriod.length == 0) {
        	    childrenTaxPeriod = objSelectedPeriod.GetChildren();
        	}
        	
        	if (purcTaxPoint != 'onpayment') {
        	    //old
        	    alphaListLineItems = getVendorData.getMAPSearchResults(params, grandTotal, alphaListLineItemsMap, alphaListLineItems, 
        	        !isGenericSearch, purcTaxPoint, taxTypesMap, searchType);
        	}
            
            //new
            alphaListLineItems = getVendorData.getMAPSearchResults(params, grandTotal, alphaListLineItemsMap, alphaListLineItems, 
            		isGenericSearch, purcTaxPoint, taxTypesMap, searchType);
            
        	//sort
            alphaListLineItems.sort(MAP_REPORT.SortFunction); 

            
            getVendorData.appendPeriodInfo(alphaListLineItems);
    	}

        return new MAP_REPORT.LineItemsWithTotal(alphaListLineItems, grandTotal);
    }
    
    
    getVendorData.getMAPSearchResults = function getMAPSearchResults(params, grandTotal, alphaListLineItemsMap, alphaListLineItems, 
    		isGenericSearch, purcTaxPoint, taxTypesMap, searchType) {
    	var payeeSearchFilters = getVendorData.createPayeeSearchFilters(params, isGenericSearch, purcTaxPoint);
        var searchColumns = getVendorData.createPayeeSearchColumns(isGenericSearch, searchType);
        
        var search = nlapiCreateSearch('transaction', payeeSearchFilters, searchColumns);
        var resultSet = search.runSearch();
        
        if (resultSet != null) {
        	
        	var offset = 0;
            var arrResult = [];
            
            try {
            	arrResult = resultSet.getResults(offset,_MAX_NUM_SEARCH_RESULTS);
            } catch (err) {
            	nlapiLogExecution('ERROR', 'Error occurred while searching for alphalist of payees', err);
            }
            
            if (arrResult != null && arrResult.length > 0) {
	         	var	searchObjColumns = getVendorData.lookupSearchColumns(searchColumns);   //create associative array of payee columns
	         	
	        	while (arrResult != null && arrResult.length > 0) {
	        		getVendorData.extractPayeeData(offset, grandTotal, arrResult, searchColumns, searchObjColumns, alphaListLineItemsMap, alphaListLineItems, 
	        				taxTypesMap, purcTaxPoint, searchType, isGenericSearch);
	        		offset = offset + _MAX_NUM_SEARCH_RESULTS;
	            	arrResult = resultSet.getResults(offset,offset + _MAX_NUM_SEARCH_RESULTS);
	        	}
	        	
	        	alphaListLineItems =  getVendorData.cleanUpPayeeData(alphaListLineItems);
            }
        }
        
        return alphaListLineItems;
    };
    
    
    getVendorData.cleanUpPayeeData = function cleanUpPayeeData(arrSawtLineItem) {
    	var newArrSawtLineItem = [];
    	
    	for (var i in arrSawtLineItem) {
    		if (arrSawtLineItem[i].tax_base != 0 && arrSawtLineItem[i].tax_amount != 0) {
    			newArrSawtLineItem.push(arrSawtLineItem[i]);
    		} 
    	}
    	
    	return newArrSawtLineItem;
    };
    
    
    getVendorData.lookupSearchColumns = function lookupSearchColumns(arrObjSearchCols)
    {
        var arrCols = [];
        for (var i in arrObjSearchCols)
        {
        	var columnIndex = (arrObjSearchCols[i].getLabel() == null || arrObjSearchCols[i].getLabel() == '') ? 
        						arrObjSearchCols[i].getName() : arrObjSearchCols[i].getLabel();
        						
            arrCols[columnIndex] = arrObjSearchCols[i];
        }
        
        return arrCols;
    };


    getVendorData.appendPeriodInfo = function appendPeriodInfo(arrPayeeInfo)
    {
        var periodInfoDisplay = MAP_REPORT.getPeriodInfo(objSelectedPeriod);
        for (var i in arrPayeeInfo)
        {
            arrPayeeInfo[i].return_period = periodInfoDisplay;
        }
    };

    
    getVendorData.getPaymentRecordIds = function getPaymentRecordIds(params) {
    	var arrDocRefIds = [];
    	
    	arrDocRefIds = getVendorData.getPaymentTransactions(params, arrDocRefIds);
    	arrDocRefIds = getVendorData.getCheckTransactions(params, arrDocRefIds);
    	return arrDocRefIds;
    };
    
    
    getVendorData.getCheckTransactions = function getCheckTransactions(params, arrDocRefIds) {
    	// Get Check
    	var purcTypes = [];
		purcTypes.push('Check');
		
		var paymentFilters = [];
		paymentFilters.push(['type', 'anyof', purcTypes]);
		paymentFilters.push('AND');
		paymentFilters.push(['vendor.internalid', 'noneof', '@NONE@']); //to filter out customer checks
		paymentFilters.push('AND');
		paymentFilters.push(['custcol_ph4014_src_jrnltrantypeid', 'anyof','@NONE@']); //filter out old records
		paymentFilters.push('AND');
		paymentFilters.push(['custcol_4601_witaxapplies', 'is', 'T']);
		paymentFilters.push('AND');
		paymentFilters.push(['isreversal', 'is', 'F']);
		paymentFilters = _4601.getTaxPeriodFilters(paymentFilters, objSelectedPeriod);
		paymentFilters = _4601.getWTaxCodeFilters(paymentFilters, null, {
			allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
			groupWtaxCodes: groupWtaxCodes});
		if (params.subid != null && _IsOneWorld){
			paymentFilters.push('AND');
			paymentFilters.push(['subsidiary', 'is', params.subid]);
    	}
		
		var index = 0;
        var search = nlapiCreateSearch('transaction', paymentFilters);
        var resultSet = search.runSearch();
        
        do {
            var sr = resultSet.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
            
            if (sr) {
                for (var i = 0; i < sr.length; i++) {
                    arrDocRefIds.push(sr[i].getId());
                }
            }
            
            index += sr.length;
        } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);
		
		return arrDocRefIds;
    };
    
    
    getVendorData.getPaymentTransactions = function getPaymentTransactions(params, arrDocRefIds) {
    	// Get Bill Credit
    	var creditFilters = [];

    	creditFilters.push(['type', 'is', 'VendCred']);
    	creditFilters.push('AND');
    	creditFilters.push(['custbody_4601_pymnt_ref_id', 'noneof', '@NONE@']);
    	creditFilters.push('AND');
    	creditFilters.push(['custbody_4601_doc_ref_id', 'noneof', '@NONE@']);
    	creditFilters.push('AND');
    	creditFilters.push(['mainline', 'is', 'T']);
    	creditFilters = _4601.getTaxPeriodFilters(creditFilters, objSelectedPeriod);
    	creditFilters = _4601.getWTaxCodeFilters(creditFilters, 'custbody_4601_doc_ref_id.custcol_4601_witaxcode', {
    		allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
			groupWtaxCodes: groupWtaxCodes});

    	var creditColumns = [];
    	creditColumns.push(new nlobjSearchColumn('custbody_4601_doc_ref_id'));
    	creditColumns.push(new nlobjSearchColumn('custbody_4601_pymnt_ref_id'));
    	creditColumns.push(new nlobjSearchColumn('amount'));

    	var _docRef = '';
        var _pymntRef = '';
        var _amount = 0.0;
    	var index = 0;
        var search = nlapiCreateSearch('transaction', creditFilters, creditColumns);
        var resultSet = search.runSearch();
        
        do {
            var sr = resultSet.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
            
            if (sr) {
                for (var i = 0; i < sr.length; i++) {
                    _docRef = sr[i].getValue('custbody_4601_doc_ref_id');
                    _pymntRef = sr[i].getValue('custbody_4601_pymnt_ref_id');
                    _amount = sr[i].getValue('amount');
                    if (_amount != 0) {
                        if (billCreditMap[_docRef] == null) {
                            arrDocRefIds.push(_docRef);
                            billCreditMap[_docRef] = {};
                        }
                        billCreditMap[_docRef][_pymntRef] = {}; 
                        billCreditMap[_docRef][_pymntRef].pymntRef = _pymntRef;
                        billCreditMap[_docRef][_pymntRef].taxWithheld = _amount;
                    }
                }
            }
            
            index += sr.length;
        } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);
        
		return arrDocRefIds;
    };
    
    
    getVendorData.getTransactions = function getTransactions(params, arrTransID) {
    	var filters = [];
    	var columns = [];
    	
    	columns.push(new nlobjSearchColumn('internalid', null, 'group'));
    	
    	if (allowedWithholdingTaxCodeIds != null && allowedWithholdingTaxCodeIds.length > 0) {
    		var arrTransTypes = [];
        	arrTransTypes.push('Check');
        	arrTransTypes.push('VendBill');
        	arrTransTypes.push('VendCred');


        	filters.push(['type', 'anyof', arrTransTypes]);
        	filters.push('AND');
        	filters.push(['vendor.internalid', 'noneof', '@NONE@']);
        	filters.push('AND');
        	filters.push(['custcol_4601_witaxapplies', 'is', 'T']);
        	filters.push('AND');
        	filters.push(['isreversal', 'is','F']);
        	filters.push('AND');
			filters.push(['posting', 'is', 'T']);
			filters.push('AND');
			filters.push(['voided', 'is', 'F']);
			filters.push('AND');
			filters.push(['custcol_4601_witaxbaseamount', 'isnotempty', null]);

        	if (params.subid != null && _IsOneWorld){
        		filters.push('AND');
        		filters.push(['subsidiary', 'is', params.subid]);
        	}
        	
        	filters = _4601.getTaxPeriodFilters(filters, objSelectedPeriod);
        	filters = _4601.getWTaxCodeFilters(filters, null, {
        		allowedWithholdingTaxCodeIds: allowedWithholdingTaxCodeIds,
    			groupWtaxCodes: groupWtaxCodes});
        	
        	var index = 0;
        	var search = nlapiCreateSearch('transaction', filters, columns);
        	var resultSet = search.runSearch();
        	
            do {
                var sr = resultSet.getResults(index, index + _MAX_NUM_SEARCH_RESULTS);
                
                if (sr) {
                    for (var i = 0; i < sr.length; i++) {
                        arrTransID.push(sr[i].getValue('internalid', null, 'group'));
                    }
                }
                
                index += sr.length;
            } while (sr.length >= _MAX_NUM_SEARCH_RESULTS);
        }
    	
		return arrTransID;
    };
    
    
    /**
     * Create search filters for payees.
     * 
     * @param params
     * @param allowedWithholdingTaxCodeIds
     * @param isNewSearch
     * @param purcTaxPoint
     * @returns {Array}
     */
    getVendorData.createPayeeSearchFilters = function createPayeeSearchFilters(params, isNewSearch, purcTaxPoint) {
        
        var filters = [];
        
        //filters for new search should also filter out results that can be returned by the old search
        if (isNewSearch) {
            var arrTransID = [];
            
            if (params.subid != null && _IsOneWorld) {
                filters.push(new nlobjSearchFilter('subsidiary', null, 'is', params.subid));
            }
            
            if (purcTaxPoint == 'onpayment') {
                var arrPaymentIds = getVendorData.getPaymentRecordIds(params);
                if (arrPaymentIds.length > 0){
                    filters.push(new nlobjSearchFilter(_objInternalID, null, 'anyof', arrPaymentIds));
                }
                else {
                    //Since there are no payment records retrieved, set the internalid to NONE
                    //which in essence results to no record/s to be searched;
                    //otherwise it will seach records intended for onaccrual setup
                    filters.push(new nlobjSearchFilter(_objInternalID, null, 'is', '@NONE@'));
                }
            } else if (purcTaxPoint == 'onaccrual') {
                arrTransID = getVendorData.getTransactions(params, arrTransID);
                if (arrTransID.length > 0){
                    var maxIdCount = 300;
                    
                    for (var start = 0, end = 0; end < arrTransID.length; start += maxIdCount) {
						var leftparens = (start == 0) ? 1 : 0;
                        end = ((start + maxIdCount) < arrTransID.length) ? (start + maxIdCount) : arrTransID.length;
						var rightparens = (end == arrTransID.length) ? 1 : 0;
                        var isor = (end == arrTransID.length) ? false : true;
                        
                        filters.push(new nlobjSearchFilter(_objInternalID, null, 'anyof', arrTransID.slice(start,end), null, leftparens, rightparens, isor));
                    }
                }else{
                    filters.push(new nlobjSearchFilter(_objInternalID, null, 'is', '@NONE@'));
                }
            }
        } else {
            if (params.subid != null && _IsOneWorld) {
                filters.push(['subsidiary', 'is', params.subid]);
                filters.push('AND');
            }

            //Create the filters for old search
            var journalTypes = [];
            journalTypes.push("CardChrg");
            journalTypes.push("Check");
            journalTypes.push("VendBill");
            var journalTypeIds = _App.getJournalTypeIds(journalTypes);
            
            filters.push(['custcol_4601_witaxline', 'is', 'T']);
            filters.push('AND');
            filters.push(['type', 'is', 'Journal']);
            filters.push('AND');
            filters.push(['custcol_ph4014_src_jrnltrantypeid', 'anyof', journalTypeIds]);
            filters.push('AND');
            filters.push(['custbody_ph4014_wtax_reversal_flag', 'is', 'F']);
            filters.push('AND');
            filters.push(['custcol_ph4014_src_vendorid', 'noneof', '@NONE@']);
            filters.push('AND');
            filters.push([_obj_TaxCode, 'anyof', allowedWithholdingTaxCodeIds]);
            filters = _4601.getTaxPeriodFilters(filters, objSelectedPeriod);
            filters = _4601.getJournalReversalFilters(filters);
        }
        
        return filters;
    };
    
	/**
	 * Create search columns for payees.
	 * 
	 * @param isNewSearch
	 * @returns {Array}
	 */
    getVendorData.createPayeeSearchColumns = function createPayeeSearchColumns(isNewSearch, searchType) {
		var searchColumns = [];
		
        searchColumns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM')
        		.setFormula('{'+ _obj_TaxAmount +'}').setLabel(_obj_TaxAmount));
        searchColumns.push(new nlobjSearchColumn('formulacurrency', null, 'SUM')
        		.setFormula('{'+ _obj_BaseAmount +'}').setLabel(_obj_BaseAmount));
        searchColumns.push( new nlobjSearchColumn(_objInternalID,null,'group').setLabel(_objTransID));
        searchColumns.push(new nlobjSearchColumn('formulapercent', null, 'group').setFormula(
				'{custcol_4601_witaxcode.custrecord_4601_wtc_rate}').setLabel(_objTaxCodeRate));
        searchColumns.push(new nlobjSearchColumn(_objRecordType, null, 'group'));
        searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_4601_witaxcode.custrecord_4601_wtc_istaxgroup}').setLabel(_objIsTaxGroup));
		searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
				'{custcol_4601_witaxcode.custrecord_4601_wtc_groupedwitaxcodes}').setLabel(_objGroupTaxCodes));
		searchColumns.push(new nlobjSearchColumn(_obj_TaxCode, null, 'group'));
        
        if(searchType == 'DETAIL' || searchType == 'MAP_DETAIL' || searchType.search(/PURC_/) === 0) {
            searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
            		'{custcol_4601_witaxcode.custrecord_4601_wtc_name}').setLabel(_objTaxCodeName));
            searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
            		'{custcol_4601_witaxcode.custrecord_4601_wtc_description}').setLabel(_objTaxCodeDesc));
            searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{custcol_4601_witaxcode.custrecord_4601_wtc_witaxtype}').setLabel(_objTaxTypeDesc));
		    
		    if (searchType.search(/PURC_/) === 0) {
	            searchColumns.push(new nlobjSearchColumn(_objReferenceNo, null, 'group'));
	            searchColumns.push(new nlobjSearchColumn(_objMemo, null, 'group'));
			    searchColumns.push(new nlobjSearchColumn(_objTransactionDate, null, 'group'));
	        }
        }
        
		if (isNewSearch) {
			searchColumns.push(new nlobjSearchColumn(_objInternalID,'vendor','group'));
	        searchColumns.push(new nlobjSearchColumn(_objPayeeFirst,'vendor','group'));
	        searchColumns.push(new nlobjSearchColumn(_objPayeeMiddle,'vendor','group'));
	        searchColumns.push(new nlobjSearchColumn(_objPayeeLast,'vendor','group'));
	        searchColumns.push(new nlobjSearchColumn(_objPayeeCompany,'vendor','group'));
	        searchColumns.push(new nlobjSearchColumn(_objTIN,'vendor','group'));
	        searchColumns.push(new nlobjSearchColumn(_objPayeeFlag,'vendor','group'));
		} else {
			searchColumns.push(new nlobjSearchColumn(_obj_ph_VendorID,null,'group').setLabel(_objInternalID));
			searchColumns.push(new nlobjSearchColumn('formulanumeric', null, 'group').setFormula(
					'{'+ _obj_ph_VendorID + '.' + _objInternalID + '}').setLabel(_objInternalID));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{'+ _obj_ph_VendorID + '.' + _objPayeeFirst + '}').setLabel(_objPayeeFirst));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{'+ _obj_ph_VendorID + '.' + _objPayeeMiddle + '}').setLabel(_objPayeeMiddle));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{'+ _obj_ph_VendorID + '.' + _objPayeeLast + '}').setLabel(_objPayeeLast));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{'+ _obj_ph_VendorID + '.' + _objPayeeCompany + '}').setLabel(_objPayeeCompany));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{'+ _obj_ph_VendorID + '.' + _objTIN + '}').setLabel(_objTIN));
			searchColumns.push(new nlobjSearchColumn('formulatext', null, 'group').setFormula(
					'{'+ _obj_ph_VendorID + '.' + _objPayeeFlag + '}').setLabel(_objPayeeFlag));
		}
			
		return searchColumns;
	};
	
	
    getVendorData.extractPayeeData = function extractPayeeData(offset, grandTotal, arrResult, searchColumns, searchObjColumns, alphaListLineItemsMap, 
    		alphaListLineItems, taxTypesMap, purcTaxPoint, searchType, isGenericSearch) {
    	var transID = '';
    	var totalAmountMap = [];
    	var pymntRef = '';
    	var voidedTransactions = getVendorData.getVoidedTransactionsInternalId();
    	
    	// Get total amounts (base and withheld) to be used in computing ratios
    	if (purcTaxPoint == 'onpayment') {
    		var arrTransID = [];
    		
    		for (var j in arrResult) {
        		transID = arrResult[j].getValue(searchObjColumns[_objTransID]);
        		if (transID != null && billCreditMap[transID] != null)
        			arrTransID.push(transID);
        	}
    		
    		if (arrTransID.length > 0)
    			totalAmountMap = _4601.populateTotalAmountMap(arrTransID);
    	}
		
		for (var i in arrResult) {
			transID = arrResult[i].getValue(searchObjColumns[_objTransID]);
			// externalize the recordType
			var recordType = arrResult[i].getValue(searchObjColumns[_objRecordType]) || ''; 
			//check if voided/check transaction
			if (recordType == 'check' && voidedTransactions.indexOf(transID) > -1){	
				continue;
			}
			//check if payment/accrual
			if (purcTaxPoint == 'onpayment' && billCreditMap[transID] != null) {	
				var billPayments = billCreditMap[transID];
				for (var h in billPayments) {
					var payment = billPayments[h];
					pymntRef = payment.pymntRef;
					alphaListLineItemsMap = getVendorData.calculateAmountTransaction(offset, arrResult[i], i, searchObjColumns, alphaListLineItemsMap, alphaListLineItems, 
							taxTypesMap, totalAmountMap, transID, searchType, purcTaxPoint, pymntRef, isGenericSearch);
				}
			} else {
				alphaListLineItemsMap = getVendorData.calculateAmountTransaction(offset, arrResult[i], i, searchObjColumns, alphaListLineItemsMap, alphaListLineItems, 
						taxTypesMap, totalAmountMap, transID, searchType, purcTaxPoint, pymntRef, isGenericSearch);
			}
		}
    };
    
    
    getVendorData.getVoidedTransactionsInternalId = function getVoidedTransactionsInternalId() {
    	var TRANSACTION_TYPES = ['Check', 'VendBill', 'VendCred', 'Journal'];
    	var PAGE_LIMIT = 1000;
    	var resultsIndex = 0;
    	var voidedTransactionIds = [];
    	var filters = [];
    	
        if (getVendorData.params.subid != null && _IsOneWorld) {
            filters.push(['subsidiary', 'is', getVendorData.params.subid]);
            filters.push('AND');
        }        
    	filters.push(['type', 'anyof', TRANSACTION_TYPES]);
    	filters.push('AND');
    	filters.push(['voided', 'is', 'T']);
    	filters.push('AND');
    	filters.push(['mainline', 'is', 'T']);
    	filters = _4601.getTaxPeriodFilters(filters, objSelectedPeriod);
    	
	    var search = nlapiCreateSearch('transaction', filters, null);
	    var resultSet = search.runSearch();
	    
		var list = [];
		var resLength = 0;
        do {
            list = resultSet.getResults(resultsIndex, resultsIndex + PAGE_LIMIT);
            
            if (!list) {
                break;
            }
            
			resLength = list.length;
            for (var i=0; i < resLength; i++) {
    	    	voidedTransactionIds.push(list[i].getId());
    	    }
			resultsIndex += resLength;
        } while (resLength >= PAGE_LIMIT);
        
	    return voidedTransactionIds;
    };
    
    getVendorData.calculateAmountTransaction = function calculateAmountTransaction(offset, arrResult, i, searchObjColumns, alphaListLineItemsMap, alphaListLineItems,
    		taxTypesMap, totalAmountMap, transID, searchType, purcTaxPoint, pymntRef, isGenericSearch) {
    	var taxCode = '';
    	var taxType = '';
    	var isTaxGroup = '';
    	var groupTaxCodes = null;
    	var taxCodeName = '';
    	var taxCodeDesc = '';
    	var refNo = '';
    	var memo = '';
    	var tranDate = '';
    	var recordType = '';
    	var taxBase = 0.0;
    	var taxAmount = 0.0;
    	var taxRate = '';
    	var taxRateComp = 0.0;
    	var wtaxDetails = {};
    	
    	taxRate = arrResult.getValue(searchObjColumns[_objTaxCodeRate]);
		taxRateComp = parseFloat(taxRate.replace('%', '')) || 0;
		recordType = arrResult.getValue(searchObjColumns[_objRecordType]) || '';
		taxBase = parseFloat(arrResult.getValue(searchObjColumns[_obj_BaseAmount])) || 0;
		taxAmount = parseFloat(arrResult.getValue(searchObjColumns[_obj_TaxAmount])) || 0;
		isTaxGroup = arrResult.getValue(searchObjColumns[_objIsTaxGroup]);
		groupTaxCodes = arrResult.getValue(searchObjColumns[_objGroupTaxCodes]);
			
		if (searchType == 'DETAIL' || searchType == 'MAP_DETAIL' || searchType.search(/PURC_/) === 0) {
			if (isTaxGroup != 'T')
				taxType = arrResult.getValue(searchObjColumns[_objTaxTypeDesc]);
    		
    		if (searchType.search(/PURC_/) === 0) {
    			refNo = arrResult.getValue(searchObjColumns[_objReferenceNo]) || '';
    			memo = arrResult.getValue(searchObjColumns[_objMemo]) || '';
    			tranDate = arrResult.getValue(searchObjColumns[_objTransactionDate]) || '';
    		}
		}
    		
		if (isTaxGroup == 'T') {
			var taxRateTotalComp = 0.0;
			var taxAmountTotal = 0.0;
			var arrTaxCodes = groupTaxCodes.split(',');
			var arrGroupTaxCodes = _4601.getTaxCodesByTaxGroup(arrTaxCodes, allValidGroupedWtaxCodes);
			var taxBasis = 0.0;
			var partialTaxAmount = 0;
			
			if (purcTaxPoint == 'onpayment' && transID != null && billCreditMap[transID] != null)
				taxAmountTotal = _4601.getLineAmountDistribution(taxBase, taxAmount, billCreditMap[transID][pymntRef].taxWithheld, totalAmountMap[transID].totalTaxAmount) || 0;
			else
				taxAmountTotal = taxAmount;
			
			taxRateTotalComp = taxRateComp;
			for (var j in arrGroupTaxCodes) {
				taxCode = arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_code');
				taxBasis = parseFloat(arrGroupTaxCodes[j].getValue('custrecord_4601_gwtc_basis'));
				taxCodeName = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_name', 'custrecord_4601_gwtc_code');
				taxCodeDesc = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_description', 'custrecord_4601_gwtc_code') == '' ? _NONE : arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_description', 'custrecord_4601_gwtc_code');
				taxType = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_witaxtype', 'custrecord_4601_gwtc_code');
				taxRate = arrGroupTaxCodes[j].getValue('custrecord_4601_wtc_rate', 'custrecord_4601_gwtc_code');
				taxRateComp = parseFloat(taxRate.replace('%', ''));
				
				if (allowedWithholdingTaxCodeIds.indexOf(taxCode) > -1) {
    				if (purcTaxPoint == 'onaccrual')
    				    partialTaxAmount = -(taxBase * ((taxRateComp * (taxBasis/100)))/100);
    				else if (purcTaxPoint == 'onpayment') {
    					var taxRateRatio = taxRateTotalComp == 0 ? 0 : taxRateComp / taxRateTotalComp;
    					partialTaxAmount = taxRateRatio * taxAmountTotal;
    					taxBase = -(taxRateTotalComp == 0 ? 0 : partialTaxAmount / (taxRateComp/100));
    				}
    				
    				wtaxDetails = {
    						taxCode: taxCode, 
    						taxBase: (searchType == 'SUMMARY' && j > 0) ? 0 : taxBase, 
    						taxAmount: !isGenericSearch ? partialTaxAmount : -partialTaxAmount,
    						taxType: taxType, 
    						taxCodeName: taxCodeName, 
    						taxCodeDesc: taxCodeDesc, 
    						taxRate: taxRate, 
    						refNo: refNo, 
    						recordType: recordType, 
    						memo: memo, 
    						transID: transID, 
    						tranDate: tranDate, 
    						searchType: searchType
    				};

    				alphaListLineItemsMap = getVendorData.populateLineItemMap(alphaListLineItemsMap, alphaListLineItems, arrResult, i, searchObjColumns, offset, wtaxDetails, taxTypesMap);
    				getVendorData.updateGrandTotal(grandTotal, parseFloat(wtaxDetails.taxBase || 0), parseFloat(wtaxDetails.taxAmount || 0));
				}
			}
		} else {
			taxCode = arrResult.getValue(searchObjColumns[_obj_TaxCode]);

			if (allowedWithholdingTaxCodeIds.indexOf(taxCode) > -1) {
				if (searchType != 'SUMMARY') {
					taxCodeName = arrResult.getValue(searchObjColumns[_objTaxCodeName]);
					taxCodeDesc = arrResult.getValue(searchObjColumns[_objTaxCodeDesc]);
				}

				if (purcTaxPoint == 'onpayment' && transID != null && billCreditMap[transID] != null) {
					taxAmount = _4601.getLineAmountDistribution(taxBase, taxAmount, billCreditMap[transID][pymntRef].taxWithheld, totalAmountMap[transID].totalTaxAmount);
					taxBase = -(taxAmount / (taxRateComp/100));
				}
				else if(purcTaxPoint == 'onaccrual' && recordType == 'vendorcredit'){
				    taxAmount = -taxAmount;
				    taxBase = -taxBase;
				}

				wtaxDetails = {
						taxCode: taxCode, 
						taxBase: taxBase, 
						taxAmount: !isGenericSearch ? taxAmount : -taxAmount,
						taxType: taxType, 
						taxCodeName: taxCodeName, 
						taxCodeDesc: taxCodeDesc, 
						taxRate: taxRate, 
						refNo: refNo, 
						recordType: recordType, 
						memo: memo, 
						transID: transID, 
						tranDate: tranDate, 
						searchType: searchType
				};

				alphaListLineItemsMap = getVendorData.populateLineItemMap(alphaListLineItemsMap, alphaListLineItems, arrResult, i, searchObjColumns, offset, wtaxDetails, taxTypesMap);
				getVendorData.updateGrandTotal(grandTotal, parseFloat(wtaxDetails.taxBase || 0), parseFloat(wtaxDetails.taxAmount || 0));
			}
		}

    	return alphaListLineItemsMap;
    };
    
    
    getVendorData.updateGrandTotal = function updateGrandTotal(total, taxBase, taxAmount) {
        total.Income += taxBase;
        total.TaxWithheld += taxAmount;
    };
    
    
    getVendorData.populateLineItemMap = function populateLineItemMap(alphaListLineItemsMap, alphaListLineItems, arrResult, i, searchObjColumns, offset, wtaxDetails, taxTypesMap) {
    	var msg = '';
    	var vendorId = arrResult.getValue(searchObjColumns[_objInternalID]);
    	
    	var arrRecordType = [];
    	arrRecordType['vendorbill'] = 'Vendor Bill';
    	arrRecordType['check'] = 'Check';
    	arrRecordType['vendorcredit'] = 'Bill Credit';
    	arrRecordType['journalentry'] = 'Journal Entry';
    	
    	/*
    	 *	wtaxDetails = {
    	 *		taxCode, 
    	 *		taxBase,
    	 *		taxAmount,
    	 *		taxType,
    	 *		taxCodeName, 
    	 *		taxCodeDesc, 
    	 *		taxRate, 
    	 *		refNo, 
    	 *		recordType, 
    	 *		memo, 
    	 *		transID, 
    	 *		tranDate, 
    	 *		searchType
    	 *  }; 
    	 */
    	
    	var taxBase = !!wtaxDetails.taxBase ? wtaxDetails.taxBase : 0;
    	var taxAmount = !!wtaxDetails.taxAmount ?  wtaxDetails.taxAmount : 0;
    	
    	if (wtaxDetails.searchType == 'SUMMARY') {
    		if (!alphaListLineItemsMap[vendorId]) {
        		var customerTin = arrResult.getValue(searchObjColumns[_objTIN]) == _NONE ? '' : arrResult.getValue(searchObjColumns[_objTIN]);
    			//(seqNo, tin, atc, tax_id, taxBase, taxAmount, lastname, firstname, middlename, companyname, payeeflag
    			var objPayee = new MAP_REPORT._Payee(Number(parseInt(i) + parseInt(1) + offset).toString(),
    					wtaxDetails.searchType == 'MAP_DETAIL' ? _4601.FormatTIN(customerTin) : customerTin,
    					wtaxDetails.taxCodeName,
    					wtaxDetails.taxCode,
    					taxBase,
    					taxAmount,
    					arrResult.getValue(searchObjColumns[_objPayeeLast]),
    					arrResult.getValue(searchObjColumns[_objPayeeFirst]),
    					arrResult.getValue(searchObjColumns[_objPayeeMiddle]),
    					arrResult.getValue(searchObjColumns[_objPayeeCompany]),
    					arrResult.getValue(searchObjColumns[_objPayeeFlag]));
                
                //replace the description and rate
                objPayee.desc = wtaxDetails.taxCodeDesc;
                objPayee.tax_rate = wtaxDetails.taxRate;
                var taxTypeDesc = wtaxDetails.taxType == '' ? '' : taxTypesMap[wtaxDetails.taxType].desc ;
                objPayee.tax_type = taxTypeDesc;
                objPayee.tin_raw = customerTin;

                alphaListLineItems.push(objPayee);
                alphaListLineItemsMap[vendorId] = objPayee;
                
                msg = 'Vendor: ' + objPayee.entity_name + '; TIN: ' + objPayee.tin + '; Tax Code: ' + objPayee.atc + 
         		'; Tax Amt: ' + objPayee.tax_amount + '; Nature of Payment: ' + objPayee.desc + '; Tax Rate: ' + 
         		objPayee.tax_rate + '<br/>';
                nlapiLogExecution('DEBUG', 'Payee Data', msg);
    		} else {
    			alphaListLineItemsMap[vendorId].addToTaxBase(taxBase);
    			alphaListLineItemsMap[vendorId].addToTaxAmount(taxAmount);
    		}
    	} else if (wtaxDetails.searchType == 'PURC_DETAIL') {
    		if (!alphaListLineItemsMap[wtaxDetails.taxCode + '-' + wtaxDetails.transID]) {
	    		var customerTin = arrResult.getValue(searchObjColumns[_objTIN]) == _NONE ? '' : arrResult.getValue(searchObjColumns[_objTIN]);
				//(seqNo, tin, atc, tax_id, taxBase, taxAmount, lastname, firstname, middlename, companyname, payeeflag
				var objPayee = new MAP_REPORT._Payee(Number(parseInt(i) + parseInt(1) + offset).toString(),
						customerTin, //_4601.FormatTIN(customerTin),
						wtaxDetails.taxCodeName,
						wtaxDetails.taxCode,
						taxBase,
						taxAmount,
						arrResult.getValue(searchObjColumns[_objPayeeLast]),
						arrResult.getValue(searchObjColumns[_objPayeeFirst]),
						arrResult.getValue(searchObjColumns[_objPayeeMiddle]),
						arrResult.getValue(searchObjColumns[_objPayeeCompany]),
						arrResult.getValue(searchObjColumns[_objPayeeFlag]));
	            
	            //replace the description and rate
	            objPayee.tax_rate = wtaxDetails.taxRate;
	            var taxTypeDesc = wtaxDetails.taxType == '' ? '' : taxTypesMap[wtaxDetails.taxType].desc ;
	            objPayee.tax_type = taxTypeDesc;
	            objPayee.tin_raw = customerTin;
	            
	            //purchase-related details
	            objPayee.refno = wtaxDetails.refNo == _NONE ? '' : wtaxDetails.refNo;
	            objPayee.recordtype = !!arrRecordType[wtaxDetails.recordType] ? arrRecordType[wtaxDetails.recordType] : wtaxDetails.recordType;
	            objPayee.memo = wtaxDetails.memo == _NONE ? '' : wtaxDetails.memo;
	            objPayee.date = (wtaxDetails.tranDate == null || wtaxDetails.tranDate == '')? '' : nlapiStringToDate(wtaxDetails.tranDate);
	            
	            alphaListLineItems.push(objPayee);
	            alphaListLineItemsMap[wtaxDetails.taxCode + '-' + wtaxDetails.transID] = objPayee;
	            
	            msg = 'Vendor: ' + objPayee.entity_name + '; TIN: ' + objPayee.tin + '; Tax Code: ' + objPayee.atc + 
	     		'; Tax Amt: ' + objPayee.tax_amount + '; Nature of Payment: ' + objPayee.desc + '; Tax Rate: ' + 
	     		objPayee.tax_rate + '<br/>';
	            nlapiLogExecution('DEBUG', 'Payee Data', msg);
    		} else {
				alphaListLineItemsMap[wtaxDetails.taxCode + '-' + wtaxDetails.transID].addToTaxBase(taxBase);
    			alphaListLineItemsMap[wtaxDetails.taxCode + '-' + wtaxDetails.transID].addToTaxAmount(taxAmount);
    		}
    	} else if (wtaxDetails.searchType == 'PURC_SUMMARY') { 
    		if (!alphaListLineItemsMap[wtaxDetails.taxCode]) {
    			//(seqNo, tin, atc, tax_id, taxBase, taxAmount, lastname, firstname, middlename, companyname, payeeflag
    			var objPayee = new MAP_REPORT._Payee(Number(parseInt(i) + parseInt(1) + offset).toString(),
    					'', //_4601.FormatTIN(customerTin),
    					wtaxDetails.taxCodeName,
    					wtaxDetails.taxCode,
    					taxBase,
    					taxAmount,
    					'', //arrResult[i].getValue(searchObjColumns[_objPayeeLast]),
    					'', //arrResult[i].getValue(searchObjColumns[_objPayeeFirst]),
    					'', //arrResult[i].getValue(searchObjColumns[_objPayeeMiddle]),
    					'', //arrResult[i].getValue(searchObjColumns[_objPayeeCompany]),
    					'' //arrResult[i].getValue(searchObjColumns[_objPayeeFlag])
    					);
                
                //replace the description and rate
                objPayee.tax_rate = wtaxDetails.taxRate;
                var taxTypeDesc = wtaxDetails.taxType == '' ? '' : taxTypesMap[wtaxDetails.taxType].desc ;
                objPayee.tax_type = taxTypeDesc;
                objPayee.desc = wtaxDetails.taxCodeDesc;
                objPayee.tin_raw = '';
                
                //purchase-related details
                objPayee.refno = wtaxDetails.refNo == _NONE ? '' : wtaxDetails.refNo;
                objPayee.recordtype = !!arrRecordType[wtaxDetails.recordType] ? arrRecordType[wtaxDetails.recordType] : wtaxDetails.recordType;
                objPayee.memo = wtaxDetails.memo == _NONE ? '' : wtaxDetails.memo;
                objPayee.date = (wtaxDetails.tranDate == null || wtaxDetails.tranDate == '')? '' : nlapiStringToDate(wtaxDetails.tranDate);
                
                alphaListLineItems.push(objPayee);
                alphaListLineItemsMap[wtaxDetails.taxCode] = objPayee;
                
                msg = 'Vendor: ' + objPayee.entity_name + '; TIN: ' + objPayee.tin + '; Tax Code: ' + objPayee.atc + 
         		'; Tax Amt: ' + objPayee.tax_amount + '; Nature of Payment: ' + objPayee.desc + '; Tax Rate: ' + 
         		objPayee.tax_rate + '<br/>';
                nlapiLogExecution('DEBUG', 'Payee Data', msg);
			} else {
				alphaListLineItemsMap[wtaxDetails.taxCode].addToTaxBase(taxBase);
				alphaListLineItemsMap[wtaxDetails.taxCode].addToTaxAmount(taxAmount);
			}
    	} else {
    		if (!alphaListLineItemsMap[wtaxDetails.vendorId + '-' + wtaxDetails.taxCode]) {
        		var customerTin = arrResult.getValue(searchObjColumns[_objTIN]) == _NONE ? '' : arrResult.getValue(searchObjColumns[_objTIN]);
    			//(seqNo, tin, atc, tax_id, taxBase, taxAmount, lastname, firstname, middlename, companyname, payeeflag
    			var objPayee = new MAP_REPORT._Payee(Number(parseInt(i) + parseInt(1) + offset).toString(),
    					wtaxDetails.searchType == 'MAP_DETAIL' ? _4601.FormatTIN(customerTin) : customerTin,
    					wtaxDetails.taxCodeName,
    					wtaxDetails.taxCode,
    					taxBase,
    					taxAmount,
    					arrResult.getValue(searchObjColumns[_objPayeeLast]),
    					arrResult.getValue(searchObjColumns[_objPayeeFirst]),
    					arrResult.getValue(searchObjColumns[_objPayeeMiddle]),
    					arrResult.getValue(searchObjColumns[_objPayeeCompany]),
    					arrResult.getValue(searchObjColumns[_objPayeeFlag]));
                
                //replace the description and rate
                objPayee.desc = wtaxDetails.taxCodeDesc;
                objPayee.tax_rate = wtaxDetails.taxRate;
                var taxTypeDesc = wtaxDetails.taxType == '' ? '' : taxTypesMap[wtaxDetails.taxType].desc ;
                objPayee.tax_type = taxTypeDesc;
                objPayee.tin_raw = customerTin;

                alphaListLineItems.push(objPayee);
                alphaListLineItemsMap[vendorId + '-' + wtaxDetails.taxCode] = objPayee;
                
                msg = 'Vendor: ' + objPayee.entity_name + '; TIN: ' + objPayee.tin + '; Tax Code: ' + objPayee.atc + 
         		'; Tax Amt: ' + objPayee.tax_amount + '; Nature of Payment: ' + objPayee.desc + '; Tax Rate: ' + 
         		objPayee.tax_rate + '<br/>';
                nlapiLogExecution('DEBUG', 'Payee Data', msg);
    		} else {
    			alphaListLineItemsMap[vendorId + '-' + wtaxDetails.taxCode].addToTaxBase(taxBase);
    			alphaListLineItemsMap[vendorId + '-' + wtaxDetails.taxCode].addToTaxAmount(taxAmount);
    		}
    	}
    	
    	return alphaListLineItemsMap;
    };
    
    return getVendorData;
    
}());

MAP_REPORT.getPeriodInfo = function getPeriodInfo(selectedPeriod) {
	var paramSelectedPeriod = null;
	
	if (selectedPeriod.length > 0)
		paramSelectedPeriod = selectedPeriod[0];
	else
		paramSelectedPeriod = selectedPeriod;
	
    if (paramSelectedPeriod.GetType() == "quarter") {
        var tempQtrStart = paramSelectedPeriod.GetStartDate();
        var tempQtrEnd = paramSelectedPeriod.GetEndDate();
        return _App.formatMonthYearToString(tempQtrStart) + "-" + _App.formatMonthYearToString(tempQtrEnd);
    }

    if (paramSelectedPeriod.GetType() == "month") {
        var tempMonthStart = paramSelectedPeriod.GetStartDate();
        return _App.formatMonthYearToString(tempMonthStart);
    }

    return paramSelectedPeriod.GetName();
};

MAP_REPORT.LineItemsWithTotal = function LineItemsWithTotal(mapLineItems, grandTotal) {
	this.grandTotal = grandTotal;
	this.mapLineItems = mapLineItems;
	
	this.getMapLineItems = function() { return this.mapLineItems; };
    this.getGrandTotalIncome = function() { return this.grandTotal.Income; };
    this.getGrandTotalTaxWithheld = function() { return this.grandTotal.TaxWithheld; };
    this.setGrandTotal = function(v) { this.grandTotal = v; };
    this.getGrandTotal = function() { 
    	this.grandTotal.TaxWithheld = Math.abs(this.grandTotal.TaxWithheld); 
    	return this.grandTotal; 
    };
};

MAP_REPORT._Payee = (function () {
	var _NONE = "- None -";
	
	function _Payee(seqNo, tin, atc, tax_id, taxBase, taxAmount, lastname, firstname, middlename, companyname, payeeflag) {
	    this.seq_no = seqNo;
	    this.tin = tin;
	    this.reg_name = payeeflag == "T" ? "" : companyname;
	    this.ind_name = payeeflag == "T" ? _Payee.getIndividualName(lastname, firstname, middlename) : "";
		this.entity_name = payeeflag == "T" ? _Payee.getIndividualName(lastname, firstname, middlename) : companyname;
	    this.return_period = "";
	    this.atc = atc;
	    this.tax_id = tax_id;
	    this.desc = "";
	    this.tax_base = taxBase;
	    this.tax_rate = "";
	    this.tax_amount = taxAmount;
	    this.payeeflag = payeeflag;
	    this.lastname = lastname;
	    this.firstname = firstname;
	    this.middlename = middlename;
	    this.classification = payeeflag == "T" ? "Individual" : "Company";
	    this.tax_type = "";
	    this.refno = "";
	    this.recordtype = "";
	    this.memo = "";
	    this.trandate = "";
	    this.tin_raw = '';
	    
	    this.addToTaxBase = function(value) { this.tax_base += parseFloat(value, 10); };
	    this.addToTaxAmount = function(value) { this.tax_amount += parseFloat(value, 10); };
	}
	
	
	_Payee.getIndividualName = function getIndividualName(lastname, firstname, middlename)
    {
        var individualname = '';

        if (lastname && lastname != _NONE)
        {
            individualname = lastname;
        }

        if (firstname && individualname)
        {
            individualname = individualname + ", " + firstname;
        }
        else if (firstname && firstname != _NONE)
        {
            individualname = firstname;
        }

        if ((middlename && middlename != _NONE) && individualname)
        {
            individualname = individualname + " " + middlename;
        }
        else if (middlename && middlename != _NONE)
        {
            individualname = middlename;
        }
        
        return individualname;
    };
    
    return _Payee;
}());
