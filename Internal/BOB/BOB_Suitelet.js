var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/encode','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {encode} encode
 */
function(file, http, record, render, runtime, search, serverWidget, url, encode) {
    RT = runtime;
    ENCODEMODULE = encode ;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var params = this.request.parameters;
			var ret ;
			if(params.method && params.cfm){
				switch(params.method){
					case 'potoso':
						//231 ftt
						//215 ftis
						//var crec = record.load('purchaseorder',params.cfm);
						//513923 sample po
						/**
						var record = '';
						require(['N/record'],function(wew){
							record = wew
						});**/
						/**
						var rec = record.copy({
						    type: 'purchaseorder',
						    id: '417027',
						    isDynamic: true
						    });
						
						rec.setValue('subsidiary',"3");
						rec.setValue('entity',161);
						rec.setValue('entitynexus',3);
						
						
						rec.setValue('location',5);
						rec.setValue('currency',1);
						**/
						
						
						/**var rec = record.create({
						    type: record.Type.PURCHASEORDER, 
						    isDynamic: true,
						    defaultValues: {
						        entity: 161
						    } 
						});
						rec.setValue('location',5);
						rec.setValue('currency',1);
						
						
						var recordId = rec.save({
						    enableSourcing: true,
						    ignoreMandatoryFields: true
						});
						
						this.response.sendRedirect({
						    type: http.RedirectType.RECORD,
						    identifier: 'purchaseorder',
						    id:recordId,
						    editMode:true
						});**/
						
						this.response.sendRedirect({
					    type: http.RedirectType.RECORD,
					    identifier: record.Type.PURCHASE_ORDER,
					    parameters:{
					    	mtd :'poso',
					    	cf : 174,//ftt form for po //215 sales order
					    	entity : 161,//1565
					    	cfm: params.cfm,//created from
					    	loc : 5 //5 ftth 14drop ship sg
					    	
					    },
					    editMode:true
						});
						
					default:
						ret = {status:'false',message:'Invalid Request'};
				}
			}else{
				ret = {status:'false',message:'Invalid Request'};
			}
			
			this.response.write(JSON.stringify(ret));
			
		};
		
		
		this.POST = function(){
			var params = this.request;
			
			this.response.write(JSON.stringify(params));
			//this.response.writeLine(JSON.stringify(ret));
		
		};
		
    }
    
    
    function dcpts(obj){
    	var data = {status:'success'};
    	//nlapiSetRedirectURL('RECORD ', obj.type, obj.id);

    	return obj;
    	
    	
    };
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
	    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
   
    
    
    
    return {
        onRequest: setGo
    };
    
});