/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/currency', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/file', 'N/render','SuiteScripts/Internal/tools/ftis_underscore.js','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {currency} currency
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {file} file
 * @param {render} render
 */
function(currency, record, runtime, search, serverWidget, url, file, render) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;

		this.runFunction = function() {
			
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			
			var params = this.request.parameters;
			if(params.ifid){
				var xmlStr = file.load('42461');//bob pl tempalte pdf  sandbox//42312  //production 42461
				
				nyapiLog('xmlStr',xmlStr);//*
	            var renderer = render.create();
	            renderer.templateContent = xmlStr.getContents();
	            nyapiLog('renderers',renderer);//*
	            
	            renderer.addRecord({
	            	templateName: 'record',
				    record: record.load({
				        type: 'salesorder',
				        id: params.ifid
				        })
			    });
	            renderer.addCustomDataSource({
	                format: render.DataSource.OBJECT,
	                alias: "recojsrd",
	                data: {'details':[{a:1},{a:2}]}
	                });
	            
	            nyapiLog('renderer',renderer);//*
	            
	            var newfile = renderer.renderAsPdf();
	            
	            this.response.write(newfile); 	
			}
			
			if(params.soid){
				var xmlStr = file.load('42355');//bob pinvoice tempalte html 42312 //production 42355
	            var renderer = render.create();
	            renderer.templateContent = xmlStr.getContents();
	         	
	            var objRecord = record.load({
			        type: 'salesorder',
			        id: params.soid
			        });
	            
            	renderer.addRecord({
   	            	templateName: 'record',
   				    record: objRecord
   			    });
	            
	            var newfile = renderer.renderAsString();
	            var pdfile = render.xmlToPdf({
	                xmlString: newfile
	            });
	            nyapiLog('wew',pdfile);
	            this.response.renderPdf(newfile);
			}
			
		};
		
		this.POST = function(){
			var params = this.request.parameters;
			nyapiLog('params post', params);
			if(params.ifid){
				//31170 prod 
				//sandbox 30737 premium
				var cow = {};
				cow.items = [];
				cow.record= {};
	            var loadFile = file.load('42461');    //sandbox 42212   //production 42461
	          
				var s = loadFile.getContents();
				
				var objRecord = nyapiLoad('salesorder',params.ifid);
				
				var itemcount = objRecord.getLineCount('item');
				nyapiLog('objRecord',objRecord);
				
				for(var x = 0; x < itemcount; x++){
					var data = {};
					
					var item =  objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'item',
					    line: x
					});
					var itemtext =  objRecord.getSublistText({
					    sublistId: 'item',
					    fieldId: 'item',
					    line: x
					});
					
					data.item = itemtext;					
					var custcol_ftis_cpn = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_cpn',
					    line: x
					});
					data.custcol_ftis_cpn = custcol_ftis_cpn;
					
					var custcol_ftis_cust_rev = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_cust_rev',
					    line: x
					});
					data.custcol_ftis_cust_rev = custcol_ftis_cust_rev;
					
					var custcol_ftis_category = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_category',
					    line: x
					});
					data.custcol_ftis_category = custcol_ftis_category;
					
					var custcol_ftis_cust_po = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_cust_po',
					    line: x
					});
					data.custcol_ftis_cust_po = custcol_ftis_cust_po;
					
					var quantity = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'quantity',
					    line: x
					});
					data.quantity = quantity;
					
					var unitsdisplay = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'units_display',
					    line: x
					});
					data.unitsdisplay = unitsdisplay;
					nyapiLog('unitsdisplay',unitsdisplay);
					var custcol_ftis_carton_no = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_carton_no',
					    line: x
					});
					data.custcol_ftis_carton_no = custcol_ftis_carton_no;
					
					var custcol_ftis_gross_weight = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_gross_weight',
					    line: x
					});
					data.custcol_ftis_gross_weight = custcol_ftis_gross_weight;
					
					var custcol_ftis_pallet_no = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_pallet_no',
					    line: x
					});
					data.custcol_ftis_pallet_no = custcol_ftis_pallet_no;
					
					var custcol_ftis_pallet_weight = objRecord.getSublistValue({
					    sublistId: 'item',
					    fieldId: 'custcol_ftis_pallet_weight',
					    line: x
					});
					data.custcol_ftis_pallet_weight = custcol_ftis_pallet_weight;
					
					
					cow.items.push(data);
					
					
				}
				
				
				var incotext = objRecord.getText('custbody_ftis_incoterm_tran');
				var rec = nyapiIn(objRecord);
				rec.fields.custbody_ftis_incoterm_tran_text = incotext || ' ';
				
				var data = {};
				data.issuedby = objRecord.getValue('issuedby');
				data.subsidiary = objRecord.getValue('subsidiary');
				data.custrecord_subsidiary_address = objRecord.getValue('custrecord_subsidiary_address');
				data.tranid = objRecord.getValue('tranid');
				data.custbody_bill_of_lading = objRecord.getValue('custbody_bill_of_lading');
				data.custbody_ftis_incoterm_tran = objRecord.getValue('custbody_ftis_incoterm_tran');
				data.custbody_track = objRecord.getValue('custbody_track');
				data.trandate = objRecord.getValue('trandate');
				data.custbody_vessel = objRecord.getValue('custbody_vessel');
				data.custbody_etd_date = objRecord.getValue('custbody_etd_date');
				data.custbody_created_by = objRecord.getValue('custbody_created_by');
				data.custbody_container = objRecord.getValue('custbody_container');
				data.custbody_eta_date = objRecord.getValue('custbody_eta_date');
				data.custbody_shipping_type = objRecord.getValue('custbody_shipping_type');
				data.custbody_no_of_carton = objRecord.getValue('custbody_no_of_carton');
				data.custbody_ftis_no_of_pallet = objRecord.getValue('custbody_ftis_no_of_pallet');
				data.billaddress = objRecord.getValue('billaddress');
				data.shipaddress = objRecord.getValue('shipaddress');
				
				
				var subsearch = search.lookupFields({
				    type: 'Subsidiary',
				    id: data.subsidiary,
				    columns: ['custrecord_subsidiary_address', 'custrecord_subsidiary_logo', 'legalname']
				});
				
				var subdata = {};
				subdata.custrecord_subsidiary_address = subsearch.custrecord_subsidiary_address;
				subdata.custrecord_subsidiary_logo = subsearch.custrecord_subsidiary_logo[0].text;
				subdata.legalname = subsearch.legalname;
				
				cow.record = rec.fields;
				cow.subsidiary = subdata;
				
				var html = s;
				html = html.process(cow);
			
				var fileObj = file.create({
				    name: 'LastPurhcasePrice.xml' ,
				    fileType: file.Type.XMLDOC,
				    contents: html,
				    description: 'sample',
				    encoding: file.Encoding.UTF8
				});
				
				this.response.setHeader({name:'content-type',value:'XMLDOC'});
		        	
		        this.response.writeFile(fileObj); 		
	        	
			}else{
				this.response.write('INVALID REQUEST POST');
			}
			
		};
		
    }
    
    function isObjEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    
    function nyapiIn(nsObj) {
		var tos = JSON.stringify(nsObj);
		return JSON.parse(tos);
	}
    
    function nyapiLoad(type, id) {
		var rec = record.load({
			type : type,
			id : id,
			isDynamic : true
		});
		
		//rec = nyapiIn(rec);
		return rec;
	}
    
    function nyapiLog(title_log, details_log) {
    	log.error({
    		title : title_log,
    		details : details_log
    	});
    }
    
    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    
    
    return {
        onRequest: setGo
    };
    
});