var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;
/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/currency', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/file', 'N/render','SuiteScripts/Internal/tools/ftis_underscore.js','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {currency} currency
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {file} file
 * @param {render} render
 */
function(currency, record, runtime, search, serverWidget, url, file, render) {
	RT = runtime;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var form = serverWidget.createForm({title:'Last Purchase Price Report'});
			FORM = form;
			form.clientScriptFileId = 35797;
			var params = this.request.parameters;
			nyapiLog('param',params);
			var slist = [];
			var ddate = form.addField({id:'custpage_date',label : 'Date',type:'date'});
			if(params.date){
				ddate.defaultValue  = params.date;
			
				var itir = _getItemIR(params.date);
				nyapiLog('itir',itir);
				var irtran  = _.uniq(itir.tran);
				nyapiLog('irtran',irtran);
				if(irtran.length){
					var sample = _getTran(irtran);
					slist = _mergedata(itir.data,sample.data);
				}
				
				
			
			}			
			
			var sublist = form.addSublist({
				id: 'custpage_result',
				type: 'list',
				label: 'Result'
			});

			var it = sublist.addField({id : 'custpage_item' ,source:'item',type: 'select',label :'Item'}); 
			
			it.updateDisplayType({
			    displayType: serverWidget.FieldDisplayType.INLINE
			});
			
			var tr = sublist.addField({id : 'custpage_tranid',source:'transaction',type: 'select',label :'IR #'});
			tr.updateDisplayType({
			    displayType: serverWidget.FieldDisplayType.INLINE
			});
			
			sublist.addField({id : 'custpage_trandate' ,type: 'date',label :'IR DATE'});
			sublist.addField({id : 'custpage_currency' ,type: 'text',label :'CURRENCY'}); 
			sublist.addField({id : 'custpage_formulanumericrate' ,type: 'text',label :'Original Buy Price'}); 
			sublist.addField({id : 'custpage_exchangerate' ,type: 'text',label :'EXCHANGE RATE'});
			sublist.addField({id : 'custpage_rate' ,type: 'text',label :'Buy Price USD'}); 
			
			
			if(slist.length){
				form.addButton({
					id : 'custpage_ex2',
					label : 'Download Excel',
					functionName : 'dlexcl'
				});
			}
			
			if(params.date){
				var bag = {};
				bag.fdate = Date.parse(params.date).toString('d MMM yyyy');
				bag.data = [];
				for (var x = 0; x < slist.length; x++){
					var sdata = slist[x].itemdetail;
					var data = {};
					data.item = sdata.item.text;
					data.tranid = "IR #"+sdata.tranid.value;
					data.trandate = Date.parse(sdata.trandate.value).toString('MM/d/yyyy');
					data.currency = sdata.currency.text;
					data.exchangerate = sdata.exchangerate.value;
					data.rate = sdata.rate.value;
					data.formulanumericrate = sdata.formulanumericrate.value;
					
					sublist.setSublistValue({id : 'custpage_item' ,line: x,value :sdata.item.value}); 
					sublist.setSublistValue({id : 'custpage_tranid' ,line: x,value :slist[x].tranid});
					sublist.setSublistValue({id : 'custpage_trandate' ,line: x,value :sdata.trandate.value});
					sublist.setSublistValue({id : 'custpage_currency' ,line: x,value :data.currency}); 
					sublist.setSublistValue({id : 'custpage_exchangerate',line: x ,value : data.exchangerate});
					sublist.setSublistValue({id : 'custpage_rate' ,line: x,value :data.rate}); 
					sublist.setSublistValue({id : 'custpage_formulanumericrate', line: x ,value: data.formulanumericrate});
					bag.data.push(data);
				}
				
				this.sessionObj.set({
				    name: "prata", 
				    value: JSON.stringify(bag)
				    });
				
			}
			
			this.response.writePage(form);
		};
		this.POST = function(){
			this.response.write('post');
		};
    }
    function _mergedata(data,idata){
    	for(var y = 0; y < data.length; y++){
			var ditem = data[y].item;
			var dtran = data[y].tranid;
	
			for(var x = 0; x < idata.length; x++){
				var item = idata[x].item;
				var tran = idata[x].tranid;
				if(ditem == item && dtran == tran){				
					for (var key in idata[x].itemdetail) {
						data[y].itemdetail[key] = idata[x].itemdetail[key];
					}	
				}	
			}
			
		}
    	return data;
		
    }
    
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    
    
    function _getTran(arr){
    	var item = getTran(arr);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.data = [];
	
		for(var line = 0; line < result.length; line++){
			 var t = {};
			 var tranid = result[line].getValue({name:'internalid',summary:'GROUP'});
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 t.tranid = tranid;
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.data.push(t);
		}
		
		return cont;
    	
    }
    function _getItemIR(date){
    	var item = getItemIR(date);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.data = [];
		cont.tran = [];

		for(var line = 0; line < result.length; line++){
			 var t = {};
			 var tranid = result[line].getValue({name:'internalid',summary:'MAX'});
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 t.tranid = tranid;
			 
			 
			 
			 cont.tran.push(tranid);
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.data.push(t);
		}
		
		return cont;
    }
    
    
 	function getTran(arr){
    	
    	var itemreceiptSearchObj = search.create({
    		   type: "itemreceipt",   		   
    		   filters: [
    		             ["type","anyof","ItemRcpt"], 
    		             "AND", 
    		             ["mainline","is","F"], 
    		             "AND",
    		             ["internalid","anyof",arr], 
    		             "AND", 
    		             ["cogs","is","F"], 
    		             "AND", 
    		             ["taxline","is","F"], 
    		             "AND", 
    		             ["item","noneof","@NONE@"]
    		          ],
    		   columns: [
    		             search.createColumn({
    		                name: "internalid",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "trandate",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "currency",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "exchangerate",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "item",
    		                summary: "GROUP",
    		                sort: search.Sort.ASC
    		             }),
    		             search.createColumn({
    		                name: "rate",
    		                summary: "MAX"
    		             }),
    		             search.createColumn({
    		                name: "tranid",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "formulanumericrate",
    		                summary: "GROUP",
    		                formula: "ROUND({fxamount}/{quantity},4)"
    		             })
    		          ]
    		});
    
    	var resultSet = itemreceiptSearchObj.run();	
		
		var a = {};
		a.result =itemreceiptSearchObj;
		a.columns = resultSet.columns;
		return a;
  
    }
    
    
    function getItemIR(date){
    	var itemreceiptSearchObj = search.create({
    		   type: "itemreceipt",
    		   filters: [
    		             ["type","anyof","ItemRcpt"], 
    				      "AND", 
    				      ["mainline","is","F"], 
    				      "AND", 
    				      ["createdfrom.name","noneof","161","162"], 
    				      "AND", 
    				      ["item.isinactive","is","F"], 
    				      "AND", 
    				      ["trandate","onorbefore",date], 
    				      "AND", 
    				      ["createdfrom.type","anyof","PurchOrd"], 
    				      "AND", 
    				      ["createdfrom.mainline","is","T"], 
    				      "AND", 
    				      ["taxline","is","F"], 
    				      "AND", 
    				      ["createdfrom.taxline","is","F"], 
    				      "AND", 
    				      ["item","noneof","@NONE@"], 
    				      "AND", 
    				      ["rate","greaterthanorequalto","0.00"]
    				   ],
    		   columns: [
    		      search.createColumn({
    		         name: "internalid",
    		         summary: "MAX",
    		         sort: search.Sort.ASC
    		      }),
    		      search.createColumn({
    		         name: "item",
    		         summary: "GROUP",
    		         sort: search.Sort.ASC
    		      })
    		   ]
    		});
    	var resultSet = itemreceiptSearchObj.run();	
		
		var a = {};
		a.result =itemreceiptSearchObj;
		a.columns = resultSet.columns;
		return a;
    	
    }
    
    function getAllSearchResults(search) {
    	var searchResultSet = search.run();
        var startIdx = 0;
        var endIdx = 1000;
        var allSearchResults = [];
        do {
        	 var searchResults = searchResultSet.getRange({
                 start : startIdx,
                 end : endIdx
             }); 
            if (searchResults) {
            	allSearchResults = allSearchResults.concat(searchResults);
                startIdx += 1000;
                endIdx += 1000;
            }
        }
        while(searchResults && searchResults.length > 0);
        return allSearchResults;
    }

    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    
    
    return {
        onRequest: setGo
    };
    
});
