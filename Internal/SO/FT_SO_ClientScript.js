var MDE;
/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */

define(['N/record', 'N/runtime', 'N/search', 'N/url'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {format} url
 */


function(record, runtime, search,url) {
	
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
    	
    }
    
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
	

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	/**var curRec = scriptContext.currentRecord;

    	if(scriptContext.sublistId == 'item'){
    		if(scriptContext.fieldId == 'expectedshipdate'){
    			var index = curRec.getCurrentSublistIndex(scriptContext.sublistId);
    			
    			var expectdate = curRec.getCurrentSublistValue(scriptContext.sublistId,scriptContext.fieldId);
    			lvl.inp.push((expectdate)?expectdate:null);
    			var kpidate = curRec.getCurrentSublistValue(scriptContext.sublistId,'custcol_ftis_kpi_cd_input_date');
    			lvl.outp.push((kpidate)?kpidate:null);
    			//console.log('fchange' +outp);
    			
    			if(expectdate){
    				console.log(kpidate);
    				if(!kpidate){
    					console.log('updating');
    					var now = new Date();
        				curRec.setCurrentSublistValue(scriptContext.sublistId,'custcol_ftis_kpi_cd_input_date',now);
    				}
    			}
    		}
    		
    	}*/
    	
    	//bob So enhancement to set form in each subsidiary
    }


    /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
     function validateLine(scriptContext) {
    	var curRec = scriptContext.currentRecord;
    	 console.log(scriptContext);
    	 console.log(curRec);
    	 
     	if(scriptContext.sublistId == 'item'){
     		//if(scriptContext.fieldId == 'expectedshipdate'){
     			
     			
     			var expectdate = curRec.getCurrentSublistValue(scriptContext.sublistId,'expectedshipdate');
     			inp.push((expectdate)?expectdate:null);
     			//var kpidate = curRec.getCurrentSublistValue(scriptContext.sublistId,'custcol_ftis_kpi_cd_input_date');
     			//outp.push((kpidate)?kpidate:null);
     			console.log('validate ' +outp);
     			
     			if(expectdate){
     				if(!inp[0]){
     					console.log('updating');
     					var now = new Date();
         				curRec.setCurrentSublistValue(scriptContext.sublistId,'custcol_ftis_kpi_cd_input_date',now);
     				}
     			}else{
    				console.log('to clear ' +outp);
    				if(!outp[0]){
    					curRec.setCurrentSublistValue(scriptContext.sublistId,'custcol_ftis_kpi_cd_input_date','');
    				}
    			}
     		//}
     		
     	}
     	return true;
     }

     var parseQueryString = function() {

         var str = window.location.search;
         var objURL = {};

         str.replace(
             new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
             function( $0, $1, $2, $3 ){
                 objURL[ $1 ] = $3;
             }
         );
         return objURL;
     };
     
     
     //bob pl printing	
     function bobpls(scriptContext){
     	var param = {
     			ifid: window.nlapiGetRecordId()
     	};
     	var output = url.resolveScript({
     	    scriptId: 'customscript_bob_print',
     	    deploymentId: 'customdeploy_bob_print',
     	    params: param
     	});
     	window.open(output);
  	    window.focus();
     } 
    
     
     function bobpl(){
     	//alert('comming');
    	var param = {
      			ifid: window.nlapiGetRecordId()
      	};
     	var url = window.nlapiResolveURL('SUITELET','customscript_bob_print','customdeploy_bob_print');
     	var wew = nlapiRequestURL(url,param,null,null,'POST');
     	download('packinglist.xml',wew.body);
    	window.open(url);
  	    window.focus();
     }
     
     function download(filename, text) {
         var pom = document.createElement('a');
         pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
         pom.setAttribute('download', filename);

         if (document.createEvent) {
             var event = document.createEvent('MouseEvents');
             event.initEvent('click', true, true);
             pom.dispatchEvent(event);
         }
         else {
             pom.click();
         }
     }
     
     function bobpinvoice(){
    	
 		var param = {
     			soid: window.nlapiGetRecordId()
     	};
     	var output = url.resolveScript({
     	    scriptId: 'customscript_bob_print',
     	    deploymentId: 'customdeploy_bob_print',
     	    params: param
     	});
 		window.open(output);
  	    window.focus();
 			
     }

     
     
    return {
        pageInit: pageInit,
        bobpl:bobpl,
        bobpinvoice :bobpinvoice 
    };
    
});
