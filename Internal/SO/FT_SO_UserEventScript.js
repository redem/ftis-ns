/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(
		[ 'N/runtime', 'N/search', 'N/error', 'N/record','SuiteScripts/Internal/date.js'],
		/**
		 * @param {runtime} runtime
		 * @param {search} search
		 * @param {error} error
		 * @param {record} record
		 */
		function(runtime, search, error, record) {

			/**
			 * Function definition to be triggered before record is loaded.
			 *
			 * @param {Object} scriptContext
			 * @param {Record} scriptContext.newRecord - New record
			 * @param {string} scriptContext.type - Trigger type
			 * @param {Form} scriptContext.form - Current form
			 * @Since 2015.2
			 */
			function beforeLoad(scriptContext) {
				try{
					var newRec = scriptContext.newRecord;
					var form =scriptContext.form ;
					
					var ent = newRec.getValue('entity');
					nyapiLog('ent',ent);
					if(scriptContext.type == 'view'){
						if(ent == '1565' || ent =='939'){
							form.clientScriptFileId = '29167';//sandbox 29167  29167 //
							
							form.addButton({
			       				id : 'custpage_bob_pl',
			       				label : 'BOB Packing List',
			       				functionName : 'bobpl'
			       			});   

							form.addButton({
			       				id : 'custpage_bob_invc',
			       				label : 'P Invoice',
			       				functionName : 'bobpinvoice'
			       			});   
						}
					}
				}catch(exx){
					nyapiLog('bf error so',exx);
				}
				
				
			}

			/**
			 * Function definition to be triggered before record is loaded.
			 *
			 * @param {Object} scriptContext
			 * @param {Record} scriptContext.newRecord - New record
			 * @param {Record} scriptContext.oldRecord - Old record
			 * @param {string} scriptContext.type - Trigger type
			 * @Since 2015.2
			 */
			function beforeSubmit(scriptContext) {
				try {
					if(scriptContext.type != 'delete'){
						var cuRec = scriptContext.newRecord;
						var poRef = cuRec.getValue('otherrefnum');
						if(poRef){
							var param = {};
							param.id = cuRec.id;
							param.po = poRef;

							var proc = validatepo2(param);

							if (proc) {
								throw error
										.create({
											name : 'Error',
											message : 'Existing Customer PO# found. Please enter a unique value to proceed.',
											notifyOff : false
										});
							}
						}
					}
					
					

				} catch (ex) {
					throw "<h1>" + ex.message
							+ "</h1>";
					nyapiLog('bfs error so',ex);
				}

			}

			/**
			 * Function definition to be triggered before record is loaded.
			 *
			 * @param {Object} scriptContext
			 * @param {Record} scriptContext.newRecord - New record
			 * @param {Record} scriptContext.oldRecord - Old record
			 * @param {string} scriptContext.type - Trigger type
			 * @Since 2015.2
			 */
			function afterSubmit(scriptContext) {
				try{
					nyapiLog('scriptContext',scriptContext);
					if(scriptContext.type == 'create' || scriptContext.type == 'edit'){
						 var newRec = scriptContext.newRecord;
						 
						 var sub = newRec.getValue('subsidiary');
						 if(sub != '7'){
							 var oldRec = scriptContext.oldRecord;
							 var nL = getExpected(newRec);
							 var cust = newRec.getValue('entity');
							 
							
						     var today = new Date();
							 var utc = today.getTime() - (today.getTimezoneOffset() * 60000);
							 var now = new Date(utc + (3600000 * 22));
						 
							 
							 var oL = [];
							 if(oldRec){
								 oL = getExpected(oldRec);		 
							 }
							 
							 nyapiLog('oL',oL);	 
							 nyapiLog('nL',nL);	 
							 nyapiLog('newRec',newRec);	 
							 
							 var rec = record.load({
					        	    type: newRec.type, 
					        	    id: newRec.id,
					        	    isDynamic: true
					        	});
							 
							 nyapiLog('rece cust',cust);
							 if(cust == '1565'){
								 rec.setValue('custbody_ftis_enduser_shippingmark','F.T. Industrial Supplies (Thailand) Co., Ltd');
							 }
								 
							 
							 for(var line = 0; line < nL.length; line++){
								
								 var idx = nL[line].idx;
								 nyapiLog('nlline',nL[line]);
								 var vcd = (nL[line].vcd)?nL[line].vcd:'';
							 
								 var kpi = (nL[line].kpi)?nL[line].kpi:''; 
								 var olkpi = null;
								
								 if(oL[line]){
									 olkpi = (oL[line].kpi)?oL[line].kpi:'';
								 }

								 
								 if(vcd && !olkpi && !kpi){
									 nyapiLog('setting',idx);
									 rec.selectLine('item', idx);
									 rec.setCurrentSublistValue({
					    				 sublistId: 'item',
					    			     fieldId: 'custcol_ftis_kpi_cd_input_date',
					    			     value: now
					    			 }); 
									 rec.commitLine({
										sublistId: 'item'
									 });
								 }					 
							 }
							 rec.save(); 
							 
						 }
						 
						 
						 
					}
				}catch(exx){
					nyapiLog('aft error so',exx);
				}
				
			}

			
			function getExpected(rec){
				var container = [];
				var count = rec.getLineCount('item');
				for(var x = 0; x < count; x++){
					
					var line = {};
					var vcd = rec.getSublistValue('item','expectedshipdate',x);
					var kpi = rec.getSublistValue('item','custcol_ftis_kpi_cd_input_date',x);
					line.idx = x;
					line.vcd = (vcd)?vcd:'';
					line.kpi = (kpi)?kpi:'';
					container.push(line);

				}
				return container;
			}
			
			
			function compareJSON(obj1, obj2) { 
				  var ret = {}; 
				  for(var i in obj2) { 
				    if(!obj1.hasOwnProperty(i) || obj2[i] !== obj1[i]) { 
				      ret[i] = obj2[i]; 
				    } 
				  } 
				  return ret; 
				}; 
			
			function validatepo2(po) {

				var filters = [ [ "type", "anyof", "SalesOrd" ], "AND",
						[ "otherrefnum", "equalto", po.po ] ];
				if (po.id) {
					filters.push("AND");
					filters.push([ "internalid", "noneof", po.id ]);

				}
				var salesorderSearchObj = search.create({
					type : "salesorder",
					filters : filters,
					columns : [ "internalid" ]
				});
				var searchResultCount = salesorderSearchObj.runPaged().count;
				return searchResultCount;

			}
			function nyapiLog(title_log, details_log) {
				log.debug({
					title : title_log,
					details : details_log
				});
				log.error({
					title : title_log,
					details : details_log
				});
			}

			return {
				beforeLoad:beforeLoad,
				afterSubmit : afterSubmit
			};

		});
