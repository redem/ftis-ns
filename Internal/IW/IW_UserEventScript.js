/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	nyapiLog('bl scriptContext','scriptContext');
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	nyapiLog('b4s scriptContext','scriptContext');
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	nyapiLog('aft scriptContext',scriptContext.type);

		var cur = scriptContext.newRecord;
		
    	
    	if(scriptContext.type != 'delete'){
    		
    		var val = (cur.getValue('custrecord_wf_json'))?cur.getValue('custrecord_wf_json').split(','):[];
    		var header = (cur.getValue('custrecord_wf_header'))? cur.getValue('custrecord_wf_header').split(','):[];
    		
    		var cont = [];
    		
    		if(cur.id){
    			
    			var type =  cur.getValue('custrecord_iw_itemtype');
    			var rectype = (type == "5")? "lotnumberedassemblyitem":"lotnumberedinventoryitem";
    			
    			var upitem = record.submitFields({
        		    type: rectype,
        		    id: cur.getValue('custrecord_ftis_item'),
        		    values: {
        		        'custitem_fcast': cur.id
        		    },
        		    options: {
        		        enableSourcing: false,
        		        ignoreMandatoryFields : true
        		    }
        		});
    		}
    	}
    		
		if(scriptContext.type == 'create'){
			try {
	    		for(var x = 0; x < header.length; x++){
	    			var data = {};
	    			data.month = header[x];
	    			data.ap = nyapiGetPeriod(data.month);
	    			data.qty = val[x];
	    			
	    			var objRec = record.create({
	    				type:'customrecord_ft_iw_detail' , 
	    				isDynamic: true
	    			});
	    			objRec.setValue({fieldId:'custrecord_iw_parent',value:cur.id});
	    			objRec.setValue({fieldId:'custrecord_ft_iw_month',value:data.ap.id});
	    			objRec.setValue({fieldId:'custrecord_ft_iw_qty',value:data.qty});
	    			
	    			
						var callId = objRec.save();;
						log.debug('Call record created successfully', 'Id: ' + callId);
					
	    			
	    			data.id = objRec;
	    			cont.push(data);
	    			
	    		
	    		}	
			} catch (ex) {
			    log.error('error '+ ex);
			}
		}
    		
    	
    }
    
    function nyapiLog(title_log, details_log) {
    	log.debug({
    		title : title_log,
    		details : details_log
    	});
    }
	
	function nyapiGetPeriod(month){
		var accountingperiodSearchObj = search.create({
		type: "accountingperiod",
		filters: [
		  ["periodname","startswith",month]
		],
		columns: [
		  search.createColumn({
			 name: "periodname",
			 sort: search.Sort.ASC
		  })
		]
		});
		var searchResultCount = accountingperiodSearchObj.runPaged().count;
		var data = {};
		accountingperiodSearchObj.run().each(function(result){
		// .run().each has a limit of 4,000 results
			
			data.id = result.id;
			data.value = result.getValue('periodname');
			return true;
		});
		return data;
		
	}
	

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});