/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {

    }
    
    function getRate(trandate,currency){
    	nyapiLog('date',trandate);
    	var container = {};
    	container.status = false;
    	
    	var customrecord_ft_curSearchObj = search.create({
    		   type: "customrecord_ft_cur",
    		   filters: [
    		      ["custrecord_cur_date","on",trandate],
    		      "AND",
    		      ["custrecord_cur_cur","anyof",currency]
    		   ],
    		   columns: [
    		      "custrecord_cur_sell",
    		      "custrecord_cur_buy",
    		      "custrecord_cur_date",
    		      "custrecord_cur_cur"
    		      
    		   ]
    		});
    		var searchResultCount = customrecord_ft_curSearchObj.runPaged().count;
    		if(searchResultCount){
    			container.status = true;
    			customrecord_ft_curSearchObj.run().each(function(result){
    	    		   // .run().each has a limit of 4,000 results
    				   container.selling = result.getValue('custrecord_cur_sell');
    				   container.buying = result.getValue('custrecord_cur_buy');
    	    		   return true;
    	    		});
    		}
    		
    		return container;
    }
    
    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
