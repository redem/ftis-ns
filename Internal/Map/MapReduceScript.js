/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/email', 'N/file', 'N/http', 'N/record', 'N/runtime', 'N/search'],
/**
 * @param {email} email
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(email, file, http, record, runtime, search) {
   
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {

    }

    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */
    function map(context) {
    	nyapiLog(context);
    }


    function nyapiLog(title_log, details_log) {
    	log.debug({
    		title : title_log,
    		details : details_log
    	});
    }

    return {
        getInputData: getInputData,
        map: map,
        
    };
    
});
