/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/runtime', 'N/search', 'N/url','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {format} format
 * @param {runtime} runtime
 * @param {search} search
 * @param {url} url
 */
function(format, runtime, search, url) {

    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
	var params;
    function pageInit(scriptContext) {
    	console.log('scriptContext');
    	if (window.onbeforeunload){
  	  	   window.onbeforeunload=function() { null;};
  	  	}
    	var tdColor = '#FF0000';
    	
    	var tbdy = item_splits.tBodies[0];
    	var tr = tbdy.children;
    	
    	for(var x = 1; x < tr.length; x++){
    		var data = tr[x].children;
    		var vcode = data[2].innerText;
    		tr[x].setAttribute(
	    			'style',
	    			//This is the magic CSS that changes the color
	    			//	This is Same method used when NetSuite returns saved search results
	    			//	with user defined row high lighting logic!
	    			'background-color: '+tdColor+'!important;border-color: white '+tdColor+' '+tdColor+' '+tdColor+'!important;'
	    		);
    		
    		console.log(vcode);
    			
    	}
    	
    }
    
    return {
        pageInit: pageInit
    };
    
});
