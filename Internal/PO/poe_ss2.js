var RT;
/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 */
function(record, runtime, search, serverWidget, url) {
	   RT = runtime;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var form = serverWidget.createForm({title:'Update Purhcase Order'});
			var sublist = form.addSublist({
				id: 'custpage_sub_bil',
				type: 'list',
				label: 'Result'
			});
			sublist.style = serverWidget.ListStyle.GRID;
			
			sublist.addField({id : 'custpage_itemid' ,type: 'text',label :'ITEM'}); 
			sublist.addField({id : 'custpage_custitem_ftis_brand' ,type: 'text',label :'Brand'});
			sublist.addField({id : 'custpage_formulanumerictotoh' ,type: 'text',label :'TOTAL ON HAND'}); 
			
			this.response.writePage(form,true);
		};
		
		
		
    }
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
    return {
        onRequest: setGo
    };
    
});
