var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;
/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime','N/record','N/search', 'N/url','N/encode','SuiteScripts/Internal/date.js'],
/**
 * @param {runtime} runtime
 * @param {record} record
 * @param {search} search
 * @param {url} url
 * @param {encode} encode
 */
function(runtime, record, search, url, encode) {
    RT = runtime;
    ENCODEMODULE = encode ;
    PARAMS = '';
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	
       	FORM = scriptContext.form;
     	var newRec = scriptContext.newRecord ;
     	nyapiLog('scriptContext.type',scriptContext.type);
		try{			
			getInjectableForm();
			if(scriptContext.type == 'view'){
				
	       		if(newRec.id){
		       		var data = ft_getBC(newRec.id);
		       		FORM.addTab({
		       		    id : 'custpage_tab_billcredit',
		       		    label : 'Bill Credit'
		       		});
			       	 var sublist = form.addSublist({
			             id: 'custpage_sub_bil',
			             type: 'inlineeditor',
			             label: 'Bill Credit List',
			             tab:'custpage_tab_billcredit'
			         });
			       	var tranid = sublist.addField({id : 'tranid',type : 'select',label : 'Bill Credit #', source:'transaction'});
			       	var trandate = sublist.addField({id : 'trandate',type : 'text',label : 'Bill Credit Date'});
			       	sublist.addField({id : 'amount',type : 'currency',label : 'Amount'});       
			       	if(data){
				       	for(var x=0; x < data.length; x++){
				       		sublist.setSublistValue({id : 'tranid',line : x,value : data[x].internalid});
				       		sublist.setSublistValue({id : 'trandate',line : x,value : data[x].trandate});
				       		sublist.setSublistValue({id : 'amount',line : x,value : data[x].amount});
				       		
				       	}	
			       	}
		       	}
	       	}
			
			
	
		}catch(exx){
			nyapiLog('error beofreload po',exx);
		}
       
    }
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	try{
    		nyapiLog('scriptContext.b f',scriptContext.type);
    		nyapiLog('scriptContext.b f',scriptContext);
    		var nr = scriptContext.newRecord;
    		 if(scriptContext.type == 'create'){
    			 var ent =nr.getValue('entity');
    			 if(ent){ 
    				var vendorcat = search.lookupFields({
    				type: search.Type.VENDOR,
    				id: ent,
    				columns: ['category']
    				});
    				if(vendorcat.category.length){
    					nr.setValue('custbodycustbody_ftis_vendor_category',vendorcat.category[0].value);
    			 	} 
    			 }
    		 }
    	}catch(exx){
    		nyapiLog('error before submit',exx);
    	}
		
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	try{
    		nyapiLog('aft scriptContext.type',scriptContext.type);
    		if(scriptContext.type == 'create' || scriptContext.type == 'copy'){
    			var newRec = scriptContext.newRecord;  
    			var curRec = record.load({
    				type: newRec.type, 
    				id: newRec.id,
    				isDynamic: true,
    			});
    			
    			//future price validation
    			//var ent = newRec.getValue('entity');
    			//if(ent != '161'){}
    			
    			var itemcount = curRec.getLineCount({
    				sublistId: 'item'
    			});
    			var cline =1;
    			for(var x= 0; x < itemcount; x++){
    				curRec.selectLine({
    					sublistId: 'item',
    					line: x
    				});
    				
    				var ftrd = curRec.getCurrentSublistValue({
    					sublistId: 'item',
    					fieldId: 'custcol_ft_req_date_wh_arr'
    				});
    				
    				if(ftrd){
    					curRec.setCurrentSublistValue({
    						 sublistId: 'item',
    						fieldId: 'custcol_ftis_orgreqdate',
    						value: ftrd
    					});
    				}    				
    				var item = curRec.getCurrentSublistValue({
    					sublistId: 'item',
    					fieldId: 'item'
    				});
    				
    				/**
					var fprice = [];
					Future Price
					fprice = getFprice(item,vendor,tdate); 
					if(fprice.length){
						//add validation if 2 price
						var pdata = fprice[0];
						curRec.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'rate',
							value: pdata.custrecord_fp_cp
						});
						curRec.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_ftis_expired',
							value: false
						});
					}else{
						curRec.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'rate',
							value: 0
						});
						curRec.setCurrentSublistValue({
							sublistId: 'item',
							fieldId: 'custcol_ftis_expired',
							value: true
						});
					}
						nyapiLog('fprice',fprice);
					**/
					
    				
    				
    				var lineno = curRec.getCurrentSublistValue({
    					sublistId: 'item',
    					fieldId: 'custcol_ebizwolotlineno'
    				});
    				nyapiLog('cline',cline);
    				if(isValEmpty(lineno)){
    					nyapiLog('in cline',cline);
    					curRec.setCurrentSublistValue({
    						sublistId: 'item',
    						fieldId: 'custcol_ebizwolotlineno',
    						value: cline
    					});
    				}
    							
    				var matchbil = search.lookupFields({
    					type: search.Type.ITEM,
    					id: item,
    					columns: ['matchbilltoreceipt']
    				});
    				
    				curRec.setCurrentSublistValue({
    					 sublistId: 'item',
    					fieldId: 'matchbilltoreceipt',
    					value: matchbil.matchbilltoreceipt
    				});
    				
    				curRec.commitLine({
    				   sublistId: 'item'
    				});
    				cline++;
    			}
    			curRec.save({enableSourcing:true,
    				ig​n​o​r​e​M​a​n​d​a​t​o​r​y​F​i​e​l​d​s:true});   
    		}
    	}catch(exx){
    		nyapiLog('error aft submit', exx);
    	}
    	 	
    }
    
    
    function getFprice(item,vendor,tdate){
    	var fil =[ 
	    	search.createFilter({
		   		 name: 'isinactive',
				 operator: search.Operator.IS,
				 values: "F"
			}),
			search.createFilter({
		   		 name: 'custrecord_fp_item',
				 operator: search.Operator.ANYOF,
				 values: item.split(',')
			}),
			search.createFilter({
		   		 name: 'custrecord_ft_vendor',
				 operator: search.Operator.ANYOF,
				 values: vendor.split(',')
			}),
			search.createFilter({
	    		 name: 'formulanumeric',
	    		 operator: search.Operator.GREATERTHAN,
	    		 formula:"CASE WHEN TO_DATE ('"+tdate+"','dd/mm/yyyy') BETWEEN {custrecord_fp_from} AND {custrecord_fp_to} THEN 1 ELSE 0 END",
	    		 values: 0
	    	})
		];
  
    	var customrecord_ftis_fpSearchObj = search.create({
		   type: "customrecord_ftis_fp",
		   filters: fil,
		   columns: [
		             "internalid",
		      "custrecord_fp_item",
		      "custrecord_ft_vendor",
		      "custrecord_fp_currency",
		      "custrecord_fp_from",
		      "custrecord_fp_to",
		      "custrecord_fp_cp"
		   ]
		});
    	
    	
		var bag = [];
		customrecord_ftis_fpSearchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			var data ={};
			data.internalid = result.getValue('internalid');
			data.custrecord_fp_item = result.getValue('custrecord_fp_item');
			data.custrecord_ft_vendor = result.getValue('custrecord_ft_vendor');
			data.custrecord_fp_currency = result.getValue('custrecord_fp_currency');
			data.custrecord_fp_from = result.getValue('custrecord_fp_from');
			data.custrecord_fp_to = result.getValue('custrecord_fp_to');
			data.custrecord_fp_cp = result.getValue('custrecord_fp_cp');
			bag.push(data);
		   return true;
		});
		
		return bag;
		
    }
    
    
    function isObjEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    
    function isValEmpty(val)
    {
        if (val === null || val === undefined)
            return true;
        val = new String(val);
        return (val.length == 0) || !/\S/.test(val);
    }
    
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
		
	}
    function ft_getBC(id){
    	var results =[];
    	var vs = search.create({
    		   type: "vendorcredit",
    		   filters: [
    		      ["type","anyof","VendCred"], 
    		      "AND", 
    		      ["custcol_ft_po","anyof",id]
    		   ],
    		   columns: [
    		      "tranid",
    		      search.createColumn({
    		         name: "trandate",
    		         sort: search.Sort.ASC
    		      }),
    		      "custcol_ft_po",
    		      "amount",
    		      "internalid"
    		   ]
    		});
    		vs.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			var data = {};
    			data.internalid = result.getValue('internalid');
    			data.tranid = result.getValue('tranid');
    			data.trandate = result.getValue('trandate');
    			data.custcol_ft_po = result.getValue('custcol_ft_po');
    			data.amount = result.getValue('amount');
    			
    			results.push(data);
    			
    			
    		   return true;
    		});
    	return results;
    }
    
    
    function getInjectableForm(){
    	var bodyAreaField = FORM.addField({
            id : 'custpage_bodyareafield',
            type : 'inlinehtml',
            label : 'Body Area Field'
        });
     
        //*********** Prepare HTML and scripts to Inject ***********
        //var body = getBody();
        var body = '';
        clientCode = clientCode.replace('$DEPLOYMENT_URL$', DEPLOYMENT_URL);
        var base64ClientCode = toBase64(clientCode);
        var scriptToInject = 'console.log(\'Added bottom script element\');';
        scriptToInject += "eval( atob(\'" + base64ClientCode + "\') );";
     
        //*********** Injecting HTML and scripts into an the Body Area Field ***********
        bodyAreaField.defaultValue = '<script>var script = document.createElement(\'script\');script.setAttribute(\'type\', \'text/javascript\');script.appendChild(document.createTextNode(\"' + scriptToInject + '\" ));document.body.appendChild(script);</script>';
       // return form;
    }
    
    var clientCode  = 'run(); ' + function run() {
        console.log('Running client code');
        
        //*********** GLOBAL VARIABLES ***********
        $ = NS.jQuery;
        DEPLOYMENT_URL = '$DEPLOYMENT_URL$'; 
        THISURL = $(location).attr('href');
        
        //*********** After DOM loads run this: ***********
        $(function() {
        	isRUMEnabled=false;
            injectHeaderScripts(); //Loads Libraries that will be placed on header (Optional)
            $(window).bind('load', injectHTML); //Replaces Suitelet's body with custom HTML once the window has fully loaded(Required)
            waitForLibraries(['swal'], runCustomFunctions); //Runs additional logic after required libraries have loaded (Optional)
        });
        
        return;
        //*********** HELPER FUNCTIONS ***********
        
        
        /**
         * Loads Libraries that will be placed on header (Optional) 
         */
        function injectHeaderScripts(){
            console.log('loadHeaderLibraries START');
            loadjscssfile("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");
            loadjscssfile("/core/media/media.nl?id=28923&c=240300&h=e893ca0aae3486f8c88b&_xt=.js");
            console.log('loadHeaderLibraries END'); 
     
            //*********** HELPER FUNCTION ***********
             function loadjscssfile(filename) {
                var filetype = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
                if (filetype == "js") { //if filename is a external JavaScript file
                    var fileref = document.createElement('script');
                    fileref.setAttribute("type", "text/javascript");
                    fileref.setAttribute("src", filename);
                } else if (filetype == "css") { //if filename is an external CSS file
                    var fileref = document.createElement("link");
                    fileref.setAttribute("rel", "stylesheet");
                    fileref.setAttribute("type", "text/css");
                    fileref.setAttribute("href", filename);
                }
                if (typeof fileref != "undefined"){
                    document.getElementsByTagName("head")[0].appendChild(fileref);
                }
                console.log(filename + ' plugin loaded');
            }
        }
        
        function waitForLibraries(libraries, functionToRun){
            var loadedLibraries = 0;
            for (var i in libraries) {
                var library = libraries[i];
                if(eval('typeof ' + library)!='undefined'){
                    loadedLibraries++;
                }
            }
            
            window.setTimeout(function(){
                if(loadedLibraries != libraries.length){
                    waitForLibraries(libraries, functionToRun);
                } else{
                    console.log(library + ' loaded');
                    functionToRun();
                }
              },500);
        }
        
        function injectHTML(){
        	
        	var tdColor = '#FF0000';
        	var tbdy = item_splits.tBodies[0];
        	var tr = tbdy.children;
        	
        	for(var x = 1; x < tr.length; x++){
        		var data = tr[x].children;
        		var vcode = data[2].innerText;
        		
        		
        		var vcoded = Date.parseExact(vcode, "d/M/yyyy");
        		if(vcoded instanceof Date){
        			if(vcoded < getNow()){
        				data[2].setAttribute(
            	    			'style',
            	    			//This is the magic CSS that changes the color
            	    			//	This is Same method used when NetSuite returns saved search results
            	    			//	with user defined row high lighting logic!
            	    			'background-color: '+tdColor+'!important;border-color: white '+tdColor+' '+tdColor+' '+tdColor+'!important;'
            	    		);
        					

        				
        				
        				
        				
        				NS.jQuery('.uir-record-status').replaceWith('<div class="uir-record-status">Please Check Vendor Price Expiry</div>');
        				NS.jQuery('#print').hide();
        				NS.jQuery('#secondaryprint').hide();
        				
        			}
        		}
        		
        	}
        	   
        }
        function runCustomFunctions() {
            console.log('clientFunctions START');
            var DEPLOYMENT_URL = '$DEPLOYMENT_URL$';
        }
    };
    
    function getParameter(parameterName)    {
    	var queryString = document.location.href;
    	var parameterName = parameterName + "=";
    	if (!queryString.length)
    	  return null;

    	var begin = queryString.indexOf("&" + parameterName);
    	if (begin == -1)
    	  begin = queryString.indexOf("?" + parameterName);
    	if ( begin != -1 )
    	{
    	  begin += parameterName.length;
    	  var end = queryString.indexOf ("&", begin);
    	  if (end == -1)
    	    end = queryString.length;
    	  return decodeURIComponent( queryString.substring(begin+1, end));
    	}
    	else  
    	{
    	  return null;
    	}
    }
    
    function toBase64(stringInput){
        return ENCODEMODULE.convert({
            string: stringInput,
            inputEncoding: ENCODEMODULE.Encoding.UTF_8,
            outputEncoding: ENCODEMODULE.Encoding.BASE_64
        });
    }
    
    return {
        beforeLoad: beforeLoad,
        beforeSubmit:beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
