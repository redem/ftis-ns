/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/http', 'N/record', 'N/runtime', 'N/search','N/url','N/file','SuiteScripts/Internal/date.js'],
/**
 * @param {http} http
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {url} url
 * @param {url} file
 */
function(http, record, runtime, search, url,file) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
    	if (window.onbeforeunload){
   	  	   window.onbeforeunload=function() { null;};
   	  	}
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	var field = scriptContext.fieldId;
    	
    	
    	if(field == 'custpage_date'){

	    	//var vdate = curRec.getValue(field);
    		var loc = window.nlapiGetFieldValue('custpage_location');
	    	var vdate = window.nlapiGetFieldValue('custpage_date');
	    	
	    	alert( 'transaction date is on and before ' + vdate);
	    	if(loc && vdate){
		    	var output = url.resolveScript({
		    	    scriptId: 'customscript_lr_s',
		    	    deploymentId: 'customdeploy_lr_s2'
		    	});
		    	
		    	var path = output + "&loc="+loc + "&date="+vdate;
		    	location = path;
	    	}
        	
    	}
    	
    	if(field == 'custpage_location'){

	    	//var vdate = curRec.getValue(field);
	    	var loc = window.nlapiGetFieldValue('custpage_location');
	    	var vdate = window.nlapiGetFieldValue('custpage_date');
	    	
	    	if(loc && vdate){
		    	var output = url.resolveScript({
		    	    scriptId: 'customscript_lr_s',
		    	    deploymentId: 'customdeploy_lr_s2'
		    	});
		    	
		    	var path = output + "&loc="+loc + "&date="+vdate;
		    	location = path;
	    	}
        	
    	}
    }
    
    function dlexcl(){
    	//alert('comming');
    	var url = window.nlapiResolveURL('SUITELET','customscript_lr_s','customdeploy_lr_s2');
    	var wew = nlapiRequestURL(url,'POST');
    	download('lastpurchaseprice.xml',wew.body);
    }
    
    function download(filename, text) {
        var pom = document.createElement('a');
        pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        pom.setAttribute('download', filename);

        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else {
            pom.click();
        }
    }

    return {
    	pageInit:pageInit,
        fieldChanged: fieldChanged,
        dlexcl : dlexcl
    };
    
});
