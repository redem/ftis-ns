var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;
/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/currency', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/file', 'N/render','SuiteScripts/Internal/tools/ftis_underscore.js','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {currency} currency
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {file} file
 * @param {render} render
 */
function(currency, record, runtime, search, serverWidget, url, file, render) {
	RT = runtime;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var form = serverWidget.createForm({title:'Last Purchase Price Report'});
			FORM = form;
			form.clientScriptFileId = 34967;
			
			form.addSubmitButton({
			    label : 'Submit Button'
			});
			var params = this.request.parameters;
			nyapiLog('param',params);
			var slist = [];

			//var subs = form.addField({id:'custpage_subs',label : 'Subsidiary',type:'select',source:'subsidiary'});
			//var loca = form.addField({id:'custpage_location',label : 'LOCATION',type:'select',source:'location'});
			var loca = form.addField({id:'custpage_location',label : 'LOCATION',type:'multiselect',source:'location'});
			
			var ddate = form.addField({id:'custpage_date',label : 'Date',type:'date'});
			
			if(params.date){
				ddate.defaultValue  = params.date;
				loca.defaultValue  = params.loc.split('\u0005');
				var locat = params.loc.split('\u0005');
			
				var itir = _getItemIR(params.date,locat);
				nyapiLog('itir',itir.tran.length);
				var irtran  = _.uniq(itir.tran);
				nyapiLog('irtran', irtran.length);
				if(irtran.length > 0){
					var sample = _getTran(irtran);
					slist = _mergedata(itir.data,sample.data);
					nyapiLog('slist',slist);
				}
				
				
			
			}			
			
			var sublist = form.addSublist({
				id: 'custpage_result',
				type: 'list',
				label: 'Result'
			});

			var it = sublist.addField({id : 'custpage_item' ,source:'item',type: 'select',label :'Item'}); 
			
			it.updateDisplayType({
			    displayType: serverWidget.FieldDisplayType.INLINE
			});
			
			var tr = sublist.addField({id : 'custpage_tranid',source:'transaction',type: 'select',label :'IR #'});
			tr.updateDisplayType({
			    displayType: serverWidget.FieldDisplayType.INLINE
			});
			sublist.addField({id : 'custpage_ftis_brand' ,type: 'text',label :'Brand'});
			sublist.addField({id : 'custpage_location' ,type: 'text',label :'Location'});
			
			sublist.addField({id : 'custpage_trandate' ,type: 'date',label :'IR DATE'});
			sublist.addField({id : 'custpage_currency' ,type: 'text',label :'CURRENCY'}); 
			sublist.addField({id : 'custpage_formulanumericrate' ,type: 'text',label :'Original Buy Price'}); 
			sublist.addField({id : 'custpage_exchangerate' ,type: 'text',label :'EXCHANGE RATE'});
			sublist.addField({id : 'custpage_rate' ,type: 'text',label :'Buy Price USD'}); 
			
			
			if(slist.length){
				form.addButton({
					id : 'custpage_ex2',
					label : 'Download Excel',
					functionName : 'dlexcl'
				});
			}
			
			if(params.date){
				var bag = {};
				bag.fdate = Date.parse(params.date).toString('d MMM yyyy');
				bag.data = [];
				for (var x = 0; x < slist.length; x++){
					var sdata = slist[x].itemdetail;
					var data = {};
					data.item = sdata.item.text;
					data.tranid = "IR #"+sdata.tranid.value;
					data.trandate = Date.parse(sdata.trandate.value).toString('MM/d/yyyy');
					data.currency = sdata.currency.text;
					data.exchangerate = sdata.exchangerate.value;
					data.rate = sdata.rate.value;
					data.formulanumericrate = sdata.formulanumericrate.value;
					
					sublist.setSublistValue({id : 'custpage_item' ,line: x,value :sdata.item.value}); 
					sublist.setSublistValue({id : 'custpage_tranid' ,line: x,value :slist[x].tranid||"N/A"});
					sublist.setSublistValue({id : 'custpage_ftis_brand' ,line: x,value :sdata.custitem_ftis_brand.text||"N/A"});
					sublist.setSublistValue({id : 'custpage_location' ,line: x,value :sdata.location.text||"N/A"});
					sublist.setSublistValue({id : 'custpage_trandate' ,line: x,value :sdata.trandate.value||"N/A"});
					sublist.setSublistValue({id : 'custpage_currency' ,line: x,value :data.currency||"N/A"}); 
					sublist.setSublistValue({id : 'custpage_exchangerate',line: x ,value : data.exchangerate||"N/A"});
					sublist.setSublistValue({id : 'custpage_rate' ,line: x,value :data.rate||"N/A"}); 
					sublist.setSublistValue({id : 'custpage_formulanumericrate', line: x ,value: data.formulanumericrate||"N/A"});
					bag.data.push(data);
				}
				
				this.sessionObj.set({
				    name: "prata", 
				    value: JSON.stringify(bag)
				    });
				
			}
			
			this.response.writePage(form);
			
		};
		
		this.POST = function(){
			var cow = this.sessionObj.get({name: "prata"});
			cow = JSON.parse(cow);
			var loadFile = file.load('35243');
			var s = loadFile.getContents();
			
			var html = s;
			html = html.process(cow);
			
			var fileObj = file.create({
			    name: 'LastPurhcasePrice.xml' ,
			    fileType: file.Type.XMLDOC,
			    contents: html,
			    description: 'sample',
			    encoding: file.Encoding.UTF8
			});
			
			this.response.setHeader({name:'content-type',value:'XMLDOC'});
	        	
	        this.response.writeFile(fileObj); 		
		};
		
    }
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    function _mergedata(data,idata){
    	for(var y = 0; y < data.length; y++){
			var ditem = data[y].item;
			var dtran = data[y].tranid;
	
			for(var x = 0; x < idata.length; x++){
				var item = idata[x].item;
				var tran = idata[x].tranid;
				if(ditem == item && dtran == tran){				
					for (var key in idata[x].itemdetail) {
						data[y].itemdetail[key] = idata[x].itemdetail[key];
					}	
				}	
			}
		}
    	return data;
		
    }
    
    
    
    function _getTran(arr){
    	var item = getTran(arr);
    	nyapiLog('item', item);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.data = [];
	
		for(var line = 0; line < result.length; line++){
			 var t = {};
			 var tranid = result[line].getValue({name:'internalid',summary:'GROUP'});
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 t.tranid = tranid;
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.data.push(t);
		}
		
		return cont;
    	
    }
    
    function getTran(arr){
    	
    	var itemreceiptSearchObj = search.create({
    		   type: "itemreceipt",   		   
    		   filters: [   		             
    		             ["type","anyof","ItemRcpt"], 
    		             "AND", 
    		             ["mainline","is","F"], 
    		             "AND", 
    		             ["internalid","anyof",arr]
    		          ],
    		   columns: [
    		             search.createColumn({
    		                name: "internalid",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "trandate",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "currency",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "exchangerate",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "item",
    		                summary: "GROUP",
    		                sort: search.Sort.ASC
    		             }),
    		             search.createColumn({
    		                name: "rate",
    		                summary: "MAX"
    		             }),
    		             search.createColumn({
    		                name: "tranid",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
     		                name: "location",
     		                summary: "GROUP"
     		             }),
    		             search.createColumn({
    		                name: "formulanumericrate",
    		                summary: "GROUP",
    		                formula: "ROUND({fxamount}/{quantity},4)"
    		             })
    		          ]
    		});
    
    	var resultSet = itemreceiptSearchObj.run();	
		
		var a = {};
		a.result =itemreceiptSearchObj;
		a.columns = resultSet.columns;
		return a;
  
    }
    
    
    
    function _getItemIR(date,loc){
    	
    	var item = getItemIR(date,loc);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.data = [];
		cont.tran = [];

		for(var line = 0; line < result.length; line++){
			 var t = {};
			 var tranid = result[line].getValue({name:'internalid',summary:'MAX'});
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 t.tranid = tranid;
			 
			 cont.tran.push(tranid);
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.data.push(t);
		}
		
		return cont;
    }
   
    
    function getItemIR(date,loc){
    	/**var itemreceiptSearchObj = search.create({
    		   type: "itemreceipt",
    		   filters: [
    		      ["type","anyof","ItemRcpt"], 
    		      "AND", 
    		      ["mainline","is","F"], 
    		      "AND", 
    		      ["name","noneof","161"], 
    		      "AND", 
    		      ["formulatext: {createdfrom}","contains","Purchase Order #"], 
    		     // "AND", 
    		      //["location","anyof","14","16"], 
    		      //["location","anyof","14","15","16","5"],
    		      "AND", 
    		      ["trandate","onorbefore",date],
    		      "AND",
    		      ["location","anyof",loc]
    		   ],
    		   columns: [
    		      search.createColumn({
    		         name: "internalid",
    		         summary: "MAX",
    		         sort: search.Sort.ASC
    		      }),
    		      search.createColumn({
    		         name: "item",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
    		          name: "custitem_ftis_brand",
    		          join: "item",
    		          summary: "GROUP"
    		       })
    		   ]
    		});
    	var resultSetss = itemreceiptSearchObj.run();	
		**/
    	
    	
    	var itemreceiptSearchObj = search.create({
    		   type: "itemreceipt",
    		   filters:
    			   [
		             ["type","anyof","ItemRcpt"], 
		             "AND", 
		             ["mainline","is","F"], 
		             "AND", 
		             ["trandate","onorbefore",date], 
		             "AND", 
		             ["createdfrom.type","anyof","PurchOrd"], 
		             "AND", 
		             ["createdfrom.name","noneof","161","162","1565"], 
		             "AND", 
		             ["item.type","anyof","Assembly","InvtPart"],
		             "AND",
	    		     ["location","anyof",loc],
	    		      "AND", 
	    		      ["rate","greaterthan","0.00"]
    		       ]
    		   ,
    		   columns: [
    		      search.createColumn({
    		         name: "internalid",
    		         summary: "MAX"
    		      }),
    		      search.createColumn({
    		         name: "item",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
    		          name: "custitem_ftis_brand",
    		          join: "item",
    		          summary: "GROUP"
    		      })
    		   ]
    		});
    		
    		
		
    	var resultSet = itemreceiptSearchObj.run();
    	
    	
		var a = {};
		a.result = itemreceiptSearchObj;
		a.columns = resultSet.columns;
		return a;
    	
    	
    	
    }
    
    
    
    
    function getAllSearchResults(search) {
    	var searchResultSet = search.run();
        var startIdx = 0;
        var endIdx = 1000;
        var allSearchResults = [];
        do {
        	 var searchResults = searchResultSet.getRange({
                 start : startIdx,
                 end : endIdx
             }); 
            if (searchResults) {
            	allSearchResults = allSearchResults.concat(searchResults);
                startIdx += 1000;
                endIdx += 1000;
            }
        }
        while(searchResults && searchResults.length > 0);
        return allSearchResults;
    }

    return {
        onRequest: setGo
    };
    
   
    
});
