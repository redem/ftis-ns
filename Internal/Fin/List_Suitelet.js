/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/http', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'SuiteScripts/Internal/tools/ftis_underscore.js'],
/**
 * @param {http} http
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 */
function(http, runtime, search, serverWidget) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	
    	var params = context.request.parameters;
		nyapiLog('param',params);
		var slist = [];
		
		
    	
    	var list = serverWidget.createList({
    	    title : 'Simple List',
    	    hideNavBar:true
    	});
    	
    	
    	var it = list.addColumn({
    	    id : 'item',
    	    type : serverWidget.FieldType.TEXT,
    	    label : 'Item'
    	});
    	
    	
    	
    	list.addColumn({
    	    id : 'tranid',
    	    type : serverWidget.FieldType.TEXT,
    	    label : 'IR #'
    	});
    	list.addColumn({
    	    id : 'trandate',
    	    type : serverWidget.FieldType.DATE,
    	    label : 'IR DATE'
    	});
    	list.addColumn({
    	    id : 'currency',
    	    type : serverWidget.FieldType.TEXT,
    	    label : 'CURRENCY'
    	});
    	list.addColumn({
    	    id : 'formulanumericrate',
    	    type : serverWidget.FieldType.TEXT,
    	    label : 'Original Buy Price'
    	});
    	list.addColumn({
    	    id : 'exchangerate',
    	    type : serverWidget.FieldType.TEXT,
    	    label : 'EXCHANGE RATE'
    	});
    	list.addColumn({
    	    id : 'rate',
    	    type : serverWidget.FieldType.TEXT,
    	    label : 'Buy Price USD'
    	});
    	
    	
    	
    	
    	if(params.date){
			var itir = _getItemIR(params.date);
			
			var irtran  = _.uniq(itir.tran);
			
			var sample = _getTran(irtran);
			
			slist = _mergedata(itir.data,sample.data);
			nyapiLog('slist',slist);
			for(var x =0; x < slist.length; x++){
				var sdet = slist[x].itemdetail;
				list.addRow({
				    row : {	item : sdet.item.text,
				    		tranid: sdet.tranid.value,
				    		trandate: sdet.trandate.value,
				    		currency: sdet.currency.text,
				    		formulanumericrate: sdet.formulanumericrate.value,
				    		exchangerate: sdet.exchangerate.value,
				    		rate: sdet.rate.value
				    	}
				});
			}
			
	
		}			
    	
    	
    	
    	
    	
    	context.response.writePage(list);
    	
    }
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    
    
    function _mergedata(data,idata){
    	for(var y = 0; y < data.length; y++){
			var ditem = data[y].item;
			var dtran = data[y].tranid;
	
			for(var x = 0; x < idata.length; x++){
				var item = idata[x].item;
				var tran = idata[x].tranid;
				if(ditem == item && dtran == tran){				
					for (var key in idata[x].itemdetail) {
						data[y].itemdetail[key] = idata[x].itemdetail[key];
					}	
				}	
			}
			
		}
    	return data;
		
    }
    
    
    
    function _getTran(arr){
    	var item = getTran(arr);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.data = [];
	
		for(var line = 0; line < result.length; line++){
			 var t = {};
			 var tranid = result[line].getValue({name:'internalid',summary:'GROUP'});
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 t.tranid = tranid;
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.data.push(t);
		}
		
		return cont;
    	
    }
    
    function getTran(arr){
    	
    	var itemreceiptSearchObj = search.create({
    		   type: "itemreceipt",   		   
    		   filters: [
    		             ["type","anyof","ItemRcpt"], 
    		             "AND", 
    		             ["mainline","is","F"], 
    		             "AND",
    		             ["internalid","anyof",arr], 
    		             "AND", 
    		             ["cogs","is","F"], 
    		             "AND", 
    		             ["taxline","is","F"], 
    		             "AND", 
    		             ["item","noneof","@NONE@"]
    		          ],
    		   columns: [
    		             search.createColumn({
    		                name: "internalid",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "trandate",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "currency",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "exchangerate",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "item",
    		                summary: "GROUP",
    		                sort: search.Sort.ASC
    		             }),
    		             search.createColumn({
    		                name: "rate",
    		                summary: "MAX"
    		             }),
    		             search.createColumn({
    		                name: "tranid",
    		                summary: "GROUP"
    		             }),
    		             search.createColumn({
    		                name: "formulanumericrate",
    		                summary: "GROUP",
    		                formula: "ROUND({fxamount}/{quantity},4)"
    		             })
    		          ]
    		});
    
    	var resultSet = itemreceiptSearchObj.run();	
		
		var a = {};
		a.result =itemreceiptSearchObj;
		a.columns = resultSet.columns;
		return a;
  
    }
    
    
    
    function _getItemIR(date){
    	var item = getItemIR(date);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.data = [];
		cont.tran = [];

		for(var line = 0; line < result.length; line++){
			 var t = {};
			 var tranid = result[line].getValue({name:'internalid',summary:'MAX'});
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 t.tranid = tranid;
			 
			 
			 
			 cont.tran.push(tranid);
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.data.push(t);
		}
		
		return cont;
    }
   
    
    function getItemIR(date){
    	var itemreceiptSearchObj = search.create({
    		   type: "itemreceipt",
    		   filters: [
    		      ["type","anyof","ItemRcpt"], 
    		      "AND", 
    		      ["mainline","is","F"], 
    		      "AND", 
    		      ["name","noneof","161"], 
    		      "AND", 
    		      ["formulatext: {createdfrom}","contains","Purchase Order #SG"], 
    		      "AND", 
    		      ["location","anyof","14","16"], 
    		      "AND", 
    		      ["item.othervendor","anyof","161"], 
    		      "AND", 
    		      ["item.isinactive","is","F"],
    		      "AND", 
    		      ["trandate","onorbefore",date]
    		   ],
    		   columns: [
    		      search.createColumn({
    		         name: "internalid",
    		         summary: "MAX",
    		         sort: search.Sort.ASC
    		      }),
    		      search.createColumn({
    		         name: "item",
    		         summary: "GROUP",
    		         sort: search.Sort.ASC
    		      }),
    		      search.createColumn({
    		         name: "trandate",
    		         summary: "MAX"
    		      }),
    		      search.createColumn({
    		         name: "currency",
    		         summary: "GROUP"
    		      })
    		   ]
    		});
    	var resultSet = itemreceiptSearchObj.run();	
		
		var a = {};
		a.result =itemreceiptSearchObj;
		a.columns = resultSet.columns;
		return a;
    	
    	
    	
    }
    
    function getAllSearchResults(search) {
    	var searchResultSet = search.run();
        var startIdx = 0;
        var endIdx = 1000;
        var allSearchResults = [];
        do {
        	 var searchResults = searchResultSet.getRange({
                 start : startIdx,
                 end : endIdx
             }); 
            if (searchResults) {
            	allSearchResults = allSearchResults.concat(searchResults);
                startIdx += 1000;
                endIdx += 1000;
            }
        }
        while(searchResults && searchResults.length > 0);
        return allSearchResults;
    }
    

    return {
        onRequest: onRequest
    };
    
});
