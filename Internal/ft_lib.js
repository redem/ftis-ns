/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 13 Jul 2017 noeh.canizares
 * 
 */
Array.prototype.inArray = function(value) {
	// Returns true if the passed value is found in the
	// array. Returns false if it is not.
	var i;
	for (i = 0; i < this.length; i++) {
		if (this[i] == value) {
			return true;
		}
	}
	return false;
};

function getNow()
{

  var d = new Date();
  return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);

}