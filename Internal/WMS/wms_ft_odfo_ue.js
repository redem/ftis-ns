/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search','N/url','N/https','N/task','SuiteScripts/Internal/date.js'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {url} url
 * @param {https} https
 * @param {task} task
 */
function(record, runtime, search, url, https, task) {
   
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	try{
    		nyapiLog('wew',runtime.executionContext);
        	if (runtime.executionContext === runtime.ContextType.USEREVENT || runtime.executionContext === runtime.ContextType.USERINTERFACE || runtime.executionContext === runtime.ContextType.SUITELET  || runtime.executionContext === runtime.ContextType.SCHEDULED){
        		var nrec = scriptContext.newRecord;
        		nyapiLog('nrec',nrec);
        		var sub = nrec.getValue('subsidiary');
        		nyapiLog('sub',sub);
        		
        		var itemcount = newRecord.getLineCount({
        			sublistId: 'item'
        	 	});
				
        		for(var x = 0 ; x<itemcount; x++){
        			var item = newRecord.getSublistValue({
    	 			    sublistId: 'item',
    	 			    fieldId: 'item',
    	 			    line: x
    	 			});
        			var itemdet = nrec.getSublistSubrecord({
    					sublistId: 'item',
    					fieldId: 'inventorydetail',
    					line: x
    				});
        			
        			
        		}
        	
				
				
				
        		
        		
        	}
    	}catch(ex){
    		nyapiLog('ex aft',ex);
    	}
    
    	
    }
    function nyapiLog(title_log, details_log) {
    	log.error({
    		title : title_log,
    		details : details_log
    	});
    }
    
    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
