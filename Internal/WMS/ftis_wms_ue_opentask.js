/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/email', 'N/file', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','SuiteScripts/Internal/tools/ftis_underscore.js','SuiteScripts/Internal/date.js'],
/**
 * @param {email} email
 * @param {file} file
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 */
function(email, file, record, runtime, search, serverWidget, url) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	var nrec = scriptContext.newRecord;
    	nyapiLog('nrec', nrec);
  
    	try{
    		var tasktype = nrec.getValue('custrecord_wmsse_tasktype');
//        	var taskstat = nrec.getValue('custrecord_wmsse_wms_status_flag');
        	nyapiLog('tasktype',tasktype);
        	
        	if(tasktype == '2'||tasktype == '9' ){ //putaway
        		var item = nrec.getValue('custrecord_wmsse_sku');
        		var bin = nrec.getValue('custrecord_wmsse_actendloc');
        	
            	nyapiLog('tasktype',tasktype);   
         
            	var itemlp = search.lookupFields({
            	    type: 'item',
            	    id: item,
            	    columns: ['custitem_wmsse_itemgroup','name']
            	});
            
            	if(!isEmpty(itemlp)){
            		var igstats = ['3'];//prod 3 //sandbox 
            		var opitemgc = itemlp.custitem_wmsse_itemgroup[0];
            		nyapiLog('opitemgc',opitemgc);
            		//add validation if inspection
            		if(opitemgc){
            			if(_.contains(igstats, opitemgc.value)){
                			//to inactive the bin
                			
                    		nyapiLog('vbin',bin);
                    		if(bin){
                    			var fieldLookUp = search.lookupFields({
                    			    type: 'bin',
                    			    id: bin,
                    			    columns: ['custrecord_wmsse_bin_loc_type', 'memo']
                    			});
                    			
                    			var btres = fieldLookUp.custrecord_wmsse_bin_loc_type;
                    			var bintpe = (fieldLookUp.custrecord_wmsse_bin_loc_type.length)?btres[0].value:0;
                    			nyapiLog('bintpe',bintpe);
                    			
                    			if(bintpe != '3'){
                    				var ubin = record.submitFields({
                            		    type: 'bin',
                            		    id: bin,
                            		    values: {
                            		        memo: 'Put away for inspection inactive',
                            		        isinactive : 'T'
                            		    },
                            		    options: {
                            		        enableSourcing: true,
                            		        ignoreMandatoryFields : true
                            		    }
                            		});
                        			sendEmailib(nrec,itemlp.name,ubin);
                    			}
                    		}
                		}
            		}
            		
            	}
            	
            	//custitem_wmsse_itemgroup item group
            	//open task custrecord_wmsse_actendloc bin 
            	
        	}
    		
    	}catch(exx){
    		nyapiLog('error ft opentask',exx);
    		
    		
    	}
    	
    	
    }
    
    function sendEmailib(nrec,itemname,bin) {
    	if(bin){

    		var rcc = [3136,56,36];
    		
    		
    		var output = url.resolveRecord({
        	    recordType: 'bin',
        	    recordId: bin
        	});
    		
    		var bintxt = search.lookupFields({
        	    type: 'bin',
        	    id: bin,
        	    columns: ['binnumber']
        	});
    		
    		var subj = 'Bin '+bintxt.binnumber +' is now Inactive';
    		var ndate = Date.now();
    		var qty = nrec.getValue('custrecord_wmsse_act_qty');
    		var lotno = nrec.getValue('custrecord_wmsse_batch_num');
    		var item = nrec.getValue('custrecord_wmsse_parent_sku_no');
    	
    	
    		var user = nrec.getValue('custrecord_wmsse_upd_user_no');
    		
    		var username = search.lookupFields({
        	    type: 'employee',
        	    id: user,
        	    columns: ['entityid']
        	});
    		
    		var orderno = nrec.getValue('name');
    		var conf = nrec.getValue('custrecord_wmsse_nsconfirm_ref_no');
    		var todisp = (orderno)?orderno:conf;
    		
    		var bdy = 'Date : '+ ndate
    				+' <br/>Bin : <a href="'+output+'">'+bintxt.binnumber
    				+'</a> <br/> Quantity '+qty +'<br/> Lot no : '+lotno
    				+'<br/> Item : '+itemname
    				+'<br/> User Name : '+ username.entityid
    				+'<br/> Order No : '+orderno;
    		
    		var recip = getRecipient(1);
    		var lloc = nrec.getValue('custrecord_wmsse_wms_location');
    		for(var y=0; y<recip.length;y++){
    			
    			
				if(lloc == recip[y].custrecord_ft_es_loc){ 	                
	  	            email.send({
	  	                author: '3976',
	  	                recipients: recip[y].custrecord_ft_es_rec,
	  	                subject: subj,
	  	                body: bdy
	  	            });
  	                
  				
				}else if(!recip[y].custrecord_ft_es_loc){
					email.send({
	  					author: '3976',
	  					recipients:recip[y].custrecord_ft_es_rec,
	  	                subject: subj,
	  	                body: bdy
					});
				}	
			}
    		//3780 set to donot reply originaly   
    	}
    }
    
    
    function getRecipient(met){
    	var cont = [];
    	var searchObj = search.create({
    		   type: "customrecord_ft_email_setup",
    		   filters: [
    		      ["custrecord_ft_es_mod","anyof",met],
    		      "AND", 
    		      ["isinactive","is","F"]
    		   ],
    		   columns: [
    		             "internalid",
    		      "custrecord_ft_es_loc",
    		      "custrecord_ft_es_mod",
    		      "custrecord_ft_es_rec"
    		   ]
    		});

		searchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
		   var data = {};
		   data.custrecord_ft_es_loc = result.getValue('custrecord_ft_es_loc');
		   data.custrecord_ft_es_mod = result.getValue('custrecord_ft_es_mod');
		   data.custrecord_ft_es_rec = result.getValue('custrecord_ft_es_rec');
		   cont.push(data);
		   return true;
		});
		
		return cont;
    } 
    
    
    function isEmpty(obj) {

        // null and undefined are "empty"
        if (obj == null) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        // If it isn't an object at this point
        // it is empty, but it can't be anything *but* empty
        // Is it empty?  Depends on your application.
        if (typeof obj !== "object") return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    }
    
   
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
		log.error({
			title : title_log,
			details : details_log
		});
	}

    return {
        afterSubmit: afterSubmit
    };
    
});
