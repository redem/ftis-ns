/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/http', 'N/https', 'N/runtime', 'N/search'],
/**
 * @param {http} http
 * @param {https} https
 * @param {runtime} runtime
 * @param {search} search
 */
function(http, https, runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	
    	nyapiLog('bf nr',scriptContext.newRecord);
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	nyapiLog('bsp type',scriptContext.type);
    	nyapiLog('bsp nr',scriptContext.newRecord);
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	nyapiLog('aft type',scriptContext.type);
    	nyapiLog('aft nr',scriptContext.newRecord);
		//rec = nlapiViewCurrentLineItemSubrecord('inventory','inventorydetail')
		//wew = rec.getCurrentLineItemValue('inventoryassignment','tobinnumber')
		//nlapiSubmitField('bin',wew,'isinactive','T')
		
    	
    }
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    
    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
