/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/email', 'N/file', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','SuiteScripts/Internal/tools/ftis_underscore.js'],
/**
 * @param {email} email
 * @param {file} file
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 */
function(email, file, record, runtime, search, serverWidget, url) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	var nrec = scriptContext.newRecord;
    	
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	var type = scriptContext.type;
    	var nrec = scriptContext.newRecord;
    	
    	var invcount = nrec.getLineCount({
    	    sublistId: 'inventory'
    	});
    	nyapiLog('invc',invcount);
    	
    	for(var x=0; x< invcount;x++){ //starts with zero
    		var item = nrec.getSublistValue({ 
        	    sublistId: 'inventory',
        	    fieldId: 'item',
        	    line: 0
        	});
    		nyapiLog('item', item);
    		var objSubRecord = nrec.getSublistSubrecord({
        	    sublistId: 'inventory',
        	    fieldId: 'inventorydetail',
        	    line: 0
        	});
    		nyapiLog('objSubRecord', objSubRecord);
    		
    		var len = objSubRecord.getLineCount('inventoryassignment');
        	nyapiLog('len',len);
        	for(var y =0; y < len; y++){

        		var binnum = objSubRecord.getSublistValue({
            	    sublistId: 'inventoryassignment',
            	    fieldId: 'binnumber',
            	    line: y
            	});
        		var tobinnumber = objSubRecord.getSublistValue({
            	    sublistId: 'inventoryassignment',
            	    fieldId: 'tobinnumber',
            	    line: y
            	});
        		
        		
        		nyapiLog(item,tobinnumber);
        		var itemlp = search.lookupFields({
            	    type: 'item',
            	    id: item,
            	    columns: ['custitem_wmsse_itemgroup']
            	});
        		if(!isEmpty(itemlp)){
        			var igstats = ['204','203','101','2'];
            		var opitemgc = itemlp.custitem_wmsse_itemgroup[0];
            		nyapiLog('opitemgc',opitemgc);
            		
            		if(_.contains(igstats, opitemgc.value)){
            			var fieldLookUp = search.lookupFields({
            			    type: 'bin',
            			    id: tobinnumber,
            			    columns: ['custrecord_wmsse_bin_loc_type', 'memo']
            			});
            			
            			var btres = fieldLookUp.custrecord_wmsse_bin_loc_type;
            			var bintpe = (fieldLookUp.custrecord_wmsse_bin_loc_type.length)?btres[0].value:0;
            			nyapiLog('bintpe',bintpe);
            			
            			if(bintpe != '3'){
            				var ubin = record.submitFields({
                    		    type: 'bin',
                    		    id: tobinnumber,
                    		    values: {
                    		        memo: 'Put away inactive',
                    		        isinactive : 'T'
                    		    },
                    		    options: {
                    		        enableSourcing: true,
                    		        ignoreMandatoryFields : true
                    		    }
                    		});
                			nyapiLog('email must be sent ubin',ubin);
                			
            			}
            		}
        		}
        		
        		
        		nyapiLog(item,tobinnumber);
        		
        		
        		
        		
        		
        		
        	}
    		
    	}
    
    	
    	/**var tasktype = nrec.getValue('custrecord_wmsse_tasktype');
    	var taskstat = nrec.getValue('custrecord_wmsse_wms_status_flag');
    	
    	if(tasktype == '2' && taskstat == '3'){ //putaway
    		var item = nrec.getValue('custrecord_wmsse_sku');
    		var bin = nrec.getValue('custrecord_wmsse_actendloc');
    		nyapiLog('type',type);
        	nyapiLog('tasktype',tasktype);   	
     
        	var itemlp = search.lookupFields({
        	    type: 'item',
        	    id: item,
        	    columns: ['custitem_wmsse_itemgroup']
        	});
        
        	if(!isEmpty(itemlp)){
        		var igstats = ['204','203','101','2'];
        		var opitemgc = itemlp.custitem_wmsse_itemgroup[0];
        		nyapiLog('opitemgc',opitemgc);
        		//add validation if inspection
        		 
        		if(_.contains(igstats, opitemgc.value)){
        			//to inactive the bin
        			
            		nyapiLog('vbin',bin);
            		if(bin){
            			
            			
            			var fieldLookUp = search.lookupFields({
            			    type: 'bin',
            			    id: bin,
            			    columns: ['custrecord_wmsse_bin_loc_type', 'memo']
            			});
            			
            			var btres = fieldLookUp.custrecord_wmsse_bin_loc_type;
            			var bintpe = (fieldLookUp.custrecord_wmsse_bin_loc_type.length)?btres[0].value:0;
            			
            			if(bintpe != '3'){
            				var ubin = record.submitFields({
                    		    type: 'bin',
                    		    id: bin,
                    		    values: {
                    		        memo: 'Put away inactive',
                    		        isinactive : 'T'
                    		    },
                    		    options: {
                    		        enableSourcing: true,
                    		        ignoreMandatoryFields : true
                    		    }
                    		});
                			nyapiLog('ubin',ubin);
            			}
            			
            			
            			
            			
            		}
            		
            		
        		}
        		
        		
        		
        	}
        	
        	
        	
        	
        	//custitem_wmsse_itemgroup item group
        	//open task custrecord_wmsse_actendloc bin
        	
    	}**/
    	
    	
    	
    }
    
    function isEmpty(obj) {

        // null and undefined are "empty"
        if (obj == null) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        // If it isn't an object at this point
        // it is empty, but it can't be anything *but* empty
        // Is it empty?  Depends on your application.
        if (typeof obj !== "object") return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    }
    
   
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
		log.error({
			title : title_log,
			details : details_log
		});
	}

    return {
        afterSubmit: afterSubmit
    };
    
});
