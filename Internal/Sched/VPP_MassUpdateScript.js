/**
 * @NApiVersion 2.x
 * @NScriptType MassUpdateScript
 * @NModuleScope SameAccount
 */
define(['N/currency', 'N/format', 'N/record', 'N/runtime', 'N/search'],
/**
 * @param {currency} currency
 * @param {format} format
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(currency, format, record, runtime, search) {
    
    /**
     * Definition of Mass Update trigger point.
     *
     * @param {Object} params
     * @param {string} params.type - Record type of the record being processed by the mass update
     * @param {number} params.id - ID of the record being processed by the mass update
     *
     * @since 2016.1
     */
    function each(params) {
    	if(params.type == 'otherchargeitem'){
    		var newRec = record.load({type:params.type,id:params.id,isDynamic:true});
    		
    		var vcount = newRec.getLineCount('itemvendor');
    		if(vcount > 0){
    			nyapiLog('newRec',newRec);
    		}
    		nyapiLog('vcount',vcount);
    	
    	}
    	var nowdate = new Date();
    	var formattedDateString = format.format({
            value: nowdate,
            type: format.Type.DATE,
            timezone: 69
        });
       	nyapiLog(nowdate,formattedDateString);
       	nyapiLog(nowdate,formattedDateString.toString());
       	
       	
    	var rate = currency.exchangeRate({
            source: 'SGD',
            target: 'USD',
            date: nowdate
        });
 
    	nyapiLog('rate',rate);
    	
    	
    	
    	
    }
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    
    return {
        each: each
    };
    
});
