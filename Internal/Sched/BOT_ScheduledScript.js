/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(
		[ 'N/currency', 'N/format', 'N/http', 'N/https', 'N/record',
				'N/runtime', 'N/search', 'SuiteScripts/Internal/date.js' ],
		/**
		 * @param {currency}
		 *            currency
		 * @param {format}
		 *            format
		 * @param {http}
		 *            http
		 * @param {https}
		 *            https
		 * @param {record}
		 *            record
		 * @param {runtime}
		 *            runtime
		 * @param {search}
		 *            search
		 */
		function(currency, format, http, https, record, runtime, search) {
			//161
			
			/**
			 * Definition of the Scheduled script trigger point.
			 * 
			 * @param {Object}
			 *            scriptContext
			 * @param {string}
			 *            scriptContext.type - The context in which the script
			 *            is executed. It is one of the values from the
			 * @Since 2015.2
			 */
			function execute(scriptContext){
				nyapiLog('scriptContext',scriptContext);
				//const today = new Date();
				//const utc = today.getTime() - (today.getTimezoneOffset() * 60000);
				//const dumydate = new Date(utc + (3600000 * 22)).toString('MMMM d, yyyy');
				const dumydate = getNow().toString('MMMM d, yyyy');
				nyapiLog('dumydate',dumydate);
				
				var gnd = Date.parse(dumydate);
				nyapiLog('gnd',gnd);
	
				var exeDate = gnd.toString('yyyy-MM-dd');
				nyapiLog('exeDate', exeDate);
				
				
				var nd = Date.parse(dumydate);
				var parsedDate = nd.toString('d/M/yyyy');
				var usthrate = getThRate(parsedDate, 1);
				nyapiLog('usthrate', usthrate);
				
				if (!usthrate.status) {
					var USdata = getRate(exeDate, exeDate, 'USD');	
					var usd = USdata.data[0];
					nyapiLog('USdata', USdata);
					
					
					if (USdata.status) {
						
						var nextday = nd.addDays(1);
						nyapiLog('nextday', nextday);
						var pnd = nextday.toString('d/M/yyyy');
						nyapiLog('USD create new thailand tax rate', nextday);
						nyapiLog('record ' + nd, pnd);
						
						var usrec = record.create({
							type : 'customrecord_ft_cur',
							isDynamic : true
						});
						usrec.setText('custrecord_cur_exdate', parsedDate);
						usrec.setText('custrecord_cur_efdate', pnd);
						usrec.setValue('custrecord_cur_cur', 1);
						usrec.setValue('custrecord_cur_sell',usd.details[0].selling);
						usrec.setValue('custrecord_cur_buy',usd.details[0].buying);
						usrec.save();
						nyapiLog('us record', usrec);
						
					} else{
						nyapiLog('US Copy yesterday rate');
						var yesterday = nd.addDays(-1);
						var yexedate = yesterday.toString('d/M/yyyy');
						nyapiLog('no rate',yexedate);
						var rthrate = getThRate(yexedate, 1);
						nyapiLog('no rate rthrate',rthrate);
						
						if(rthrate.status){
							var wnd = Date.parse(dumydate);
							var wnextday = wnd.addDays(1);
							nyapiLog('wnextday', wnextday);
							var wpnd = wnextday.toString('d/M/yyyy');
							
							var ydata = rthrate.data[0];
							var unrec = record.copy({
							    type: 'customrecord_ft_cur',
							    id: ydata.internalid,
							    isDynamic: true
							});
							unrec.setText('custrecord_cur_exdate', parsedDate);
							unrec.setText('custrecord_cur_efdate', wpnd);
							unrec.save();
							nyapiLog('usd rec weekend created!',unrec);
							
						}else{
							nyapiLog('USno rate for yesterday',rthrate);
						}
					}
				}else {
					
					nyapiLog('usd rate already created', usthrate);
				}
			
				var EUnd = Date.parse(dumydate);
				var EUparsedDate = EUnd.toString('d/M/yyyy');
				var euthrate = getThRate(EUparsedDate, 4);
				
				if (!euthrate.status) {
					var EUdata = getRate(exeDate, exeDate, 'EUR');
					var eur = EUdata.data[0];
					nyapiLog('EUdata', EUdata);

					if (EUdata.status) {
						
						var EUnextday = EUnd.addDays(1);
						nyapiLog('EUnextday', EUnextday);
						nyapiLog('EU create new thailand tax rate', EUnextday);
						var EUpnd = EUnextday.toString('d/M/yyyy');
						nyapiLog('record ' + EUnd, EUpnd);
						
						var EUrec = record.create({
							type : 'customrecord_ft_cur',
							isDynamic : true
						});
						EUrec.setText('custrecord_cur_exdate', EUparsedDate);
						EUrec.setText('custrecord_cur_efdate', EUpnd);
						EUrec.setValue('custrecord_cur_cur', 4);
						EUrec.setValue('custrecord_cur_sell', eur.details[0].selling);
						EUrec.setValue('custrecord_cur_buy', eur.details[0].buying);
						EUrec.save();
						nyapiLog('eu record', EUrec);
						
					}else{
						
						nyapiLog('EU Copy yesterday rate');
						var yesterday = EUnd.addDays(-1);
						var yexedate = yesterday.toString('d/M/yyyy');
						nyapiLog('yexedate',yexedate);
						var rthrate = getThRate(yexedate, 4);
						nyapiLog('EU no rate rthrate',rthrate);
						if(rthrate.status){
							
							var wnd = Date.parse(dumydate);
							var wnextday = wnd.addDays(1);
							nyapiLog('wnextday', wnextday);
							var wpnd = wnextday.toString('d/M/yyyy');
							
							var ydata = rthrate.data[0];
							var unrec = record.copy({
							    type: 'customrecord_ft_cur',
							    id: ydata.internalid,
							    isDynamic: true
							});
							unrec.setText('custrecord_cur_exdate', EUparsedDate);
							unrec.setText('custrecord_cur_efdate', wpnd);
							unrec.save();
							nyapiLog('rec weekend created!',unrec);
							
						}else{
							nyapiLog('USno rate for yesterday',rthrate);
						}
						
					}
					
				}else{
					
					nyapiLog('eu rate already created', euthrate);
				}
				
				
				var AUnd = Date.parse(dumydate);
				var AUparsedDate = AUnd.toString('d/M/yyyy');
				var Authrate = getThRate(AUparsedDate, 5);
				nyapiLog('AU Debug',Authrate);
				if (!Authrate.status) {
					var AUdata = getRate(exeDate, exeDate, 'AUD');
					var aud = AUdata.data[0];
					nyapiLog('AUdata', AUdata);

					if (AUdata.status) {
						var AUnextday = AUnd.addDays(1);
						nyapiLog('AU create new thailand tax rate AUnextday', AUnextday);
						var AUpnd = AUnextday.toString('d/M/yyyy');
						nyapiLog('AU record ' + AUnd, AUpnd);
						
						var AUrec = record.create({
							type : 'customrecord_ft_cur',
							isDynamic : true
						});
						AUrec.setText('custrecord_cur_exdate', AUparsedDate);
						AUrec.setText('custrecord_cur_efdate', AUpnd);
						AUrec.setValue('custrecord_cur_cur', 5);
						AUrec.setValue('custrecord_cur_sell', aud.details[0].selling);
						AUrec.setValue('custrecord_cur_buy', aud.details[0].buying);
						AUrec.save();
						nyapiLog('AU record', AUrec);
						
					}else{
						
						nyapiLog('AU Copy yesterday rate');
						var yesterday = AUnd.addDays(-1);
						var yexedate = yesterday.toString('d/M/yyyy');
						nyapiLog('yexedate',yexedate);
						var rthrate = getThRate(yexedate, 5);
						nyapiLog('EU no rate rthrate',rthrate);
						if(rthrate.status){
							
							var wnd = Date.parse(dumydate);
							var wnextday = wnd.addDays(1);
							nyapiLog('wnextday', wnextday);
							var wpnd = wnextday.toString('d/M/yyyy');
							
							var ydata = rthrate.data[0];
							var unrec = record.copy({
							    type: 'customrecord_ft_cur',
							    id: ydata.internalid,
							    isDynamic: true
							});
							unrec.setText('custrecord_cur_exdate', AUparsedDate);
							unrec.setText('custrecord_cur_efdate', wpnd);
							unrec.save();
							nyapiLog('AU rec weekend created!',unrec);
							
						}else{
							nyapiLog('AU no rate for yesterday',rthrate);
						}
						
					}
					
				}else{
					
					nyapiLog('AU rate already created', Authrate);
				}
				
				
				
				
				
				
			
			}

			function getRate(sdate, edate, cur) {
				var obj = {};
				obj.status = false;
				var url = "https://iapi.bot.or.th/Stat/Stat-ExchangeRate/DAILY_AVG_EXG_RATE_V1/?start_period="
						+ sdate + "&end_period=" + edate + "&currency=" + cur;
				var hdr = {
					'api-key' : 'U9G1L457H6DCugT7VmBaEacbHV9RX0PySO05cYaGsm'
				};
				var response = https.request({
					method : https.Method.GET,
					url : url,
					headers : hdr
				});
				var resat = JSON.parse(response.body);
				var result = resat.result;

				if (result.success) {
					var data = result.data.data_detail;
					var box = [];

					for ( var line in data) {
						var detail = {};
						detail.exRateDate = sdate;
						detail.currency = cur;
						detail.details = [];

						var raw = data[line];
						var period = '';
						var eperiod = '';
						var isWeekend = false;
						if (raw.period) {
							obj.status = true;
							period = Date.parse(raw.period);
							eperiod = period.addDays(1);
							isWeekend = (period.getDay() == "0")
									|| (period.getDay() == "6");
						}

						var items = {};
						items.buying = raw.buying_transfer;
						items.selling = raw.selling;
						items.date = period;
						items.edate = eperiod;
						items.isWeekend = isWeekend;
						detail.details.push(items);
						box.push(detail);
					}
					obj.data = box;
				}

				return obj;

			}

			function getThRate(rdate, cur) {
				var obj = {};
				obj.status = false;
				var customrecord_ft_curSearchObj = search.create({
					type : "customrecord_ft_cur",
					filters : [ [ "custrecord_cur_exdate", "on", rdate ],
							"AND", [ "custrecord_cur_cur", "anyof", cur ] ],
					columns : [ "internalid", "custrecord_cur_exdate",
							"custrecord_cur_efdate", "custrecord_cur_sell",
							"custrecord_cur_buy", "custrecord_cur_cur" ]
				});
				var searchResultCount = customrecord_ft_curSearchObj.runPaged().count;
				if (searchResultCount) {
					obj.status = true;
					var container = [];
					customrecord_ft_curSearchObj
							.run()
							.each(
									function(result) {
										// .run().each has a limit of 4,000
										// results
										var data = {};
										data.internalid = result
												.getValue('internalid');
										data.custrecord_cur_exdate = result
												.getValue('custrecord_cur_exdate');
										data.custrecord_cur_efdate = result
												.getValue('custrecord_cur_efdate');
										data.custrecord_cur_sell = result
												.getValue('custrecord_cur_sell');
										data.custrecord_cur_buy = result
												.getValue('custrecord_cur_buy');
										data.custrecord_cur_cur = result
												.getValue('custrecord_cur_cur');
										container.push(data);
										return true;
									});
					obj.data = container;
				}

				return obj;

			}
			
			function getNow()
			{

			  var d = new Date();
			  return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);

			}
			
			function nyapiLog(title_log, details_log) {
				log.debug({
					title : title_log,
					details : details_log
				});
			}

			return {
				execute : execute
			};

		});
