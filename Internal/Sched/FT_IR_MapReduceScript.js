/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define([ 'N/file', 'N/record', 'N/runtime', 'N/search' ],
/**
 * @param {file} file
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(file, record, runtime, search) {
	
	var wew = [];
	function saveRec(recid,rate){
		
		var rec= record.submitFields({
		    type: record.Type.ITEM_RECEIPT,
		    id: recid,
		    values: {
		    	exchangerate:rate
		    },
		    options:{
		    	enablesourcing:false,
		    	ignoreMandatoryFields:true
		    }
		});
		wew.push(rec);
		nyapiLog(rec, wew.length);
		nyapiLog(rec, wew);
	}

	
	/**
	 * Marks the beginning of the Map/Reduce process and generates input data.
	 *
	 * @typedef {Object} ObjectRef
	 * @property {number} id - Internal ID of the record instance
	 * @property {string} type - Record type id
	 *
	 * @return {Array|Object|Search|RecordRef} inputSummary
	 * @since 2015.1
	 */
	function getInputData() {
		//batch 1 26420 tempaltecsv
		//batch 2 26556 batch 2
		var temp = file.load('26556');
		var content = temp.getContents();
		var aContents = content.split('\n');
		var container = [];
		
		for (var i = 1; i < aContents.length; i++) {
		
			var sLine = aContents[i];
			if (sLine.indexOf('\r') != -1) {
				sLine = sLine.replace('\r', '');
				var aLine = sLine.split(',');
				if (aLine != null) {
					var wew = {};
					for (var k = 0; k < aLine.length; k++) {
						
						wew.id =  aLine[0];
						wew.rate=  aLine[1];
					
					}
					container.push(wew);
				}
			}
		}


		return container;
		
	}

	/**
	 * Executes when the map entry point is triggered and applies to each key/value pair.
	 *
	 * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
	 * @since 2015.1
	 */
	function map(context) {

		 var param = context.value;		
		 var wew = JSON.parse(param);
		 saveRec(wew.id,wew.rate);
		 context.write(wew.id, wew.rate);
	}

	/**
	 * Executes when the reduce entry point is triggered and applies to each group.
	 *
	 * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
	 * @since 2015.1
	 */
	function reduce(context) {
		 nyapiLog('reduce',context);
		 context.write(context.key, context.values.length);
	}

	/**
	 * Executes when the summarize entry point is triggered and applies to the result set.
	 *
	 * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
	 * @since 2015.1
	 */
	function summarize(summary) {
		nyapiLog('summarize',summary);
		
		 if (summary.isRestarted)
         {
             log.audit({details: 'Summary stage is being restarted!'});
         }
         var totalRecordsUpdated = 0;
         summary.output.iterator().each(function (key, value)
             {
                  log.audit({
                      title: key + ' records updated', 
                      details: value
                  });
                  totalRecordsUpdated += parseInt(value);
                  return true;
              });
         log.audit({
             title: 'Total records updated', 
             details: totalRecordsUpdated
             });
		
	}

	function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
	return {
		getInputData : getInputData,
		map : map,
		reduce : reduce,
		summarize : summarize
	};

});
