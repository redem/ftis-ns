/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search) {
    

    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
		var crec = scriptContext.currentRecord;
    	if(scriptContext.mode == 'create'){
			window.nlapiSetFieldValue('autoleadtime',false);
    	}
		
    	
    }
    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	var hold = ['16','24','28','8']; //8TH-Quality Hold

    	/**if(scriptContext.fieldId == 'custitem_ftis_coc' || scriptContext.fieldId ==  'custitem_ftis_coa' || scriptContext.fieldId =='custitem_ftis_ecn_sticker' || scriptContext.fieldId =='custitem_fits_incoming_inspec' || scriptContext.fieldId =='custitem_ftis_ul_label'){
	    		var a = window.nlapiGetFieldValue('custitem_ftis_coc');
				var b = window.nlapiGetFieldValue('custitem_ftis_coa');
				var c = window.nlapiGetFieldValue('custitem_ftis_ecn_sticker');
				var d = window.nlapiGetFieldValue('custitem_fits_incoming_inspec');
				var e = window.nlapiGetFieldValue('custitem_ftis_ul_label');
	
				if(a == 'T'|| b == 'T'|| c == 'T'|| d == 'T'|| e == 'T'){
						window.nlapiSetFieldValue('custitem_ebizdefskustatus',8);
				}else{
						window.nlapiSetFieldValue('custitem_ebizdefskustatus','');
				}
    	}**/
    	
    	
    }
    

    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {

    	var valid = true;
		var a = window.nlapiGetFieldValue('custitem_ftis_coc');
		var b = window.nlapiGetFieldValue('custitem_ftis_coa');
		var c = window.nlapiGetFieldValue('custitem_ftis_ecn_sticker');
		var d = window.nlapiGetFieldValue('custitem_fits_incoming_inspec');
		var e = window.nlapiGetFieldValue('custitem_ftis_ul_label');

		if(a == 'T'|| b == 'T'|| c == 'T'|| d == 'T'|| e == 'T'){
				
				var hold = [16,24,28,8];
				var skustat = window.nlapiGetFieldValue('custitem_ebizdefskustatus');
				if(!skustat || !hold.inArray(skustat)){
					alert('You must set the Default SKU Status to [Physical goods IR WH]-Quality Hold');
					valid = false;					
				}
				
		}
		
		
		
		return valid;
	
    	
    }
    
    

    Array.prototype.inArray = function (value)
    {
     // Returns true if the passed value is found in the
     // array. Returns false if it is not.
     var i;
     for (i=0; i < this.length; i++)
     {
       if (this[i] == value)
       {
         return true;
       }
     }
     return false;
    };


    return {
		pageInit:pageInit,
		saveRecord: saveRecord
    };
    
});
