/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	var items = getItem();
    	var data = items.result;
    	for(var x = 0 ; x < items.lcount; x++){

    		var objRecord = record.load({
    		    type: record.Type.OTHER_CHARGE_ITEM, 
    		    id: data[x].id,
    		    isDynamic: true,
    		});
    		
    		var recordId = objRecord.save({
    		    enableSourcing: true,
    		    ignoreMandatoryFields: true
    		});
    		nyapiLog('recordid',recordId);
    		
    	}
    	nyapiLog(items);
    	
    }
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
	
	
    function getItem(){
    	var otherchargeitemSearchObj = search.create({
		   type: "otherchargeitem",
		   filters: [
		      ["type","anyof","OthCharge"], 
		      "AND", 
		      ["isinactive","is","F"]
		   ],
		   columns: [
		      "internalid",
		      search.createColumn({
		         name: "itemid",
		         sort: search.Sort.ASC
		      }),
		      "cost"
		   ]
		});
		var searchResultCount = otherchargeitemSearchObj.runPaged().count;
		var data = {};
		data.lcount = searchResultCount;
		data.result = [];
		otherchargeitemSearchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			var res = {};
			res.id = result.getValue('internalid');
			res.name = result.getValue('itemid');
			data.result.push(res);
		   return true;
		});
		
		return data;
    }

    return {
        execute: execute
    };
    
});
