var RT;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 */
function(file, http, record, render, runtime, search, serverWidget, url) {
    RT = runtime;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;

		this.runFunction = function() {
			
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var form = serverWidget.createForm({title:'Helicpter View'});
			var params = this.request.parameters;
			nyapiLog('param',params);
			var brand = form.addField({id:'custpage_brand',label : 'Brand',source:'customlist_ftis_brand_list',type:'select'});
			brand.defaultValue = params.brand?params.brand:'';
			form.clientScriptFileId =29891;
			var sublist = form.addSublist({
				id: 'custpage_sub_bil',
				type: 'list',
				label: 'Result'
			});
			sublist.style = serverWidget.ListStyle.GRID;
			
			//1
			sublist.addField({id : 'custpage_internalid',type: 'text',label :'Item id'}); 
			sublist.addField({id : 'custpage_custrecord_ftis_biz_model',type: 'text',label :'Model'}); 
			sublist.addField({id : 'custpage_custrecord_ftis_customeritemref_custname' ,type: 'text',label :'Client'}); 
			sublist.addField({id : 'custpage_itemid' ,type: 'text',label :'Stock Code'}); 
			sublist.addField({id : 'custpage_custrecord_ftis_customeritemref_custpart' ,type: 'text',label :'Client Stock'}); 
			sublist.addField({id : 'custpage_custitem_ftis_brand' ,type: 'text',label :'Brand'}); 
			sublist.addField({id : 'custpage_othervendor' ,type: 'text',label :'Vendor'}); 
			sublist.addField({id : 'custpage_custrecord_ftitemloc_shipping_lead_time' ,type: 'text',label :'Transit Lead Time'}); 
			sublist.addField({id : 'custpage_custrecord_ftis_supplier_buffer_lt' ,type: 'text',label :'Buffer LT'}); 
			sublist.addField({id : 'custpage_custrecord_ftitemloc_vendprodleadtime' ,type: 'text',label :'Vendor LT'}); 
			sublist.addField({id : 'custpage_custitem_ftis_pur_resc_win' ,type: 'text',label :'Reschedule'}); 
			sublist.addField({id : 'custpage_custitem_ftis_canc_window' ,type: 'text',label :'Cancellation'}); 
			sublist.addField({id : 'custpage_custitem_ftis_purc_moq' ,type: 'text',label :'MOQ'}); 
			sublist.addField({id : 'custpage_locationfixedlotsize' ,type: 'text',label :'SPQ'}); 
			sublist.addField({id : 'custpage_formulanumericftsg' ,type: 'text',label :'ON HAND FT-SG'}); 
			sublist.addField({id : 'custpage_formulanumericcsg' ,type: 'text',label :'ON HAND CONSIGN-SG'});
			sublist.addField({id : 'custpage_formulanumericftnz' ,type: 'text',label :'ON HAND <BR> FT-NZ'}); 
			sublist.addField({id : 'custpage_formulanumericftth' ,type: 'text',label :'ON HAND FT TH'});
			sublist.addField({id : 'custpage_formulanumericvmith' ,type: 'text',label :'ON HAND VMI TH'}); 
			sublist.addField({id : 'custpage_formulanumericbillth' ,type: 'text',label :'ON HAND BILL TH'}); 
			//2
			sublist.addField({id : 'custpage_formulanumericitftsg' ,type: 'text',label :'In transit FT-SG'}); 
			sublist.addField({id : 'custpage_formulanumericitftth' ,type: 'text',label :'In transit FT-TH'});
			//3
			sublist.addField({id : 'custpage_formulanumericpo' ,type: 'text',label :'TOTAL INCOMMING PO'}); 
			sublist.addField({id : 'custpage_formulanumericso' ,type: 'text',label :'TOTAL INCOMMING SO'}); 
			sublist.addField({id : 'custpage_formulanumericiso' ,type: 'text',label :'INCOMMING SO PER CLIENT'}); 
			
			if(params.brand){
				var mdata = [];
				var item = _getFtItem(params.brand);
				var data = item.a;
				var incomming = _getFtIncoming(params.brand);	
				var idata = incomming.a;
				var iso = _getFtIncomingSo();
				var sdata = iso.a;
	
				mdata = _mergePOSO(data,idata);
					
				mdata = _mergeSO(mdata,sdata);
				
				for(var col = 0; col < mdata.length; col++){
					
					var item = mdata[col].item;
					var details = mdata[col].itemdetail;
				
					for (var key in details) {
						var d =''; 
					    var a = details[key];
						switch(key){
							case 'custrecord_ftis_biz_model':
								d = a.text;
								break;
							case 'custrecord_ftis_customeritemref_custname': 
								d = a.text;
								break;
							case 'custitem_ftis_brand': 
								d = a.text;
								break;
							case 'othervendor': 
								d = a.text;
								break;
							default:
								d = (a.value)?a.value:a.text;
						}
						var f = "custpage_"+key;
						
						sublist.setSublistValue({id : f, line: col,value : d});		
					}	
				}
				
				
			}

			RT.getCurrentScript().getRemainingUsage = function(){return 1000;};
	
			nyapiLog("sRemaining governanssce units: ", RT.getCurrentScript().getRemainingUsage());
			
			//this.response.write(JSON.stringify(data));
				
			this.response.writePage(form);

		};
		this.POST = function(){
			
		};
		
    }
	function _mergePOSO(data,idata){	
		for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var exist = false;
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				if(db == da){
					exist = true;
					for (var key in idata[x].itemdetail) {
						if(key == 'formulanumericso'){
							data[y].itemdetail['formulanumericso'] = idata[x].itemdetail[key];
						}
						if(key == 'formulanumericpo'){
							data[y].itemdetail['formulanumericpo'] = idata[x].itemdetail[key];
						}
					}
				}	
			}
			if(!exist){
				data[y].itemdetail['formulanumericso'] = {'value':0,'text':"0"};
				data[y].itemdetail['formulanumericpo'] = {'value':0,'text':"0"};
			}
		}
		
		return data;
	}
	function _mergeSO(data,idata){
		nyapiLog('data',data);
		nyapiLog('sdata',idata);
		
		for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var daa = data[y].itemdetail.custrecord_ftis_customeritemref_custname.value;
			var exist = false;
			
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				var dba = idata[x].mainname;
				if(db == da && dba == daa){		
					exist = true;
					for (var key in idata[x].itemdetail) {
						if(key == 'formulanumericiso'){
							data[y].itemdetail[key] = idata[x].itemdetail[key];
						}
					}	
				}					
			}
			if(!exist){
				data[y].itemdetail['formulanumericiso'] = {'value':0,'text':"0"};					
			}
		}
		return data;
	}
	
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
	
	function _getFtIncomingSo(){
		var item = getIncomingSo();
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		
		var cont = {};
		cont.col = itemcol;
		cont.a = [];
		for(var line = 0; line < result.length; line++){
			 var t = {};
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 t.mainname = result[line].getValue({name:'mainname',summary:'GROUP'}); 
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b;
			 cont.a.push(t);
		}
		return cont;
		
    }
    
    function getIncomingSo(){
    	var salesorderSearchObj = search.create({
    		   type: "salesorder",
    		   filters: [
    		      ["type","anyof","SalesOrd"], 
    		      "AND", 
    		      ["status","noneof","SalesOrd:C","SalesOrd:H","SalesOrd:G"], 
    		      "AND", 
    		      ["formulanumeric: {quantity}-{quantityshiprecv}","greaterthan","0"], 
    		      "AND", 
    		      ["taxline","is","F"], 
    		      "AND", 
    		      ["mainline","is","F"], 
    		      "AND", 
    		      ["closed","is","F"], 
    		      "AND", 
    		      ["mainname","noneof","1565","939"]
    		   ],
    		   columns: [
    		      search.createColumn({
    		         name: "mainname",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
    		         name: "item",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
    		         name: "custitem_ftis_brand",
    		         join: "item",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
 				     name: "formulanumericiso",
 				     summary: "SUM",
 				     formula: "{quantity}"
 				  })	
    		   ]
    		});

    		var resultSet = salesorderSearchObj.run();	
			    		
    		var a = {};
    		a.result = salesorderSearchObj;
    		a.columns = resultSet.columns;
    		return a;
    }

    function _getFtIncoming(brand){
		var item = getIncoming(brand);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.a = [];

		for(var line = 0; line < result.length; line++){
			 var t = {};
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.a.push(t);
		}
		
		return cont;
    }
    
    
    function _getFtItem(brand){
		var item = getItems(brand);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.a = [];

		for(var line = 0; line < result.length; line++){
			 var t = {};
			 t.item = result[line].getValue({name:'internalid',summary:'GROUP'}); 
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.a.push(t);
		}
		
		
		return cont;
		nyapiLog('data getitem',cont);
    }
    
    
    function getItems(brand){	
    	var itemSearchObj = search.create({
		   type: "item",
		   filters: [
		      ["isinactive","is","F"], 
		      "AND", 
		      ["custrecord_ftitemloc_item_name.isinactive","is","F"], 
		      "AND", 
		      ["custrecord_ftis_customeritemref_nsitem.isinactive","is","F"], 
		      "AND", 
		      ["custitem_ftis_brand","anyof",brand],
		      "AND", 
		      ["vendor.internalid","noneof","161","162"],
		      "AND",
		      ["custrecord_ftis_customeritemref_nsitem.custrecord_ftis_customeritemref_custname","noneof","1565","939"]
		   ],
		   columns: [
				  search.createColumn({
				     name: "internalid",
				 summary: "GROUP",
				     sort: search.Sort.ASC
				  }),
				  search.createColumn({
				     name: "custrecord_ftis_biz_model",
				 join: "CUSTRECORD_FTIS_CUSTOMERITEMREF_NSITEM",
				 summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "custrecord_ftis_customeritemref_custname",
				 join: "CUSTRECORD_FTIS_CUSTOMERITEMREF_NSITEM",
				 summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "itemid",
				 summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "custrecord_ftis_customeritemref_custpart",
				 join: "CUSTRECORD_FTIS_CUSTOMERITEMREF_NSITEM",
				 summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "custitem_ftis_brand",
				 summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "othervendor",
				     summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "custrecord_ftitemloc_shipping_lead_time",
				 join: "CUSTRECORD_FTITEMLOC_ITEM_NAME",
				 summary: "MAX"
				  }),
				  search.createColumn({
				     name: "custrecord_ftis_supplier_buffer_lt",
				 join: "CUSTRECORD_FTITEMLOC_ITEM_NAME",
				 summary: "MAX"
				  }),
				  search.createColumn({
				     name: "custrecord_ftitemloc_vendprodleadtime",
				 join: "CUSTRECORD_FTITEMLOC_ITEM_NAME",
				 summary: "MAX"
				  }),
				  search.createColumn({
				     name: "custitem_ftis_pur_resc_win",
				 summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "custitem_ftis_canc_window",
				 summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "custitem_ftis_purc_moq",
				 summary: "MAX"
				  }),
				  search.createColumn({
				     name: "locationfixedlotsize",
				 summary: "MAX"
				  }),
				 search.createColumn({
					 name: 'formulanumericftsg',
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-SG',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
				     name: 'formulanumericcsg',
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'CONSIGN-SG',{locationquantityonhand},0)"
					      }),
					
				  search.createColumn({
				     name: 'formulanumericftnz',
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-NZ',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
					 name: "formulanumericftth",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-TH',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
					 name: "formulanumericvmith",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'VMI-TH',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
					 name: "formulanumericbillth",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'BILL-TH',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
					 name: "formulanumericitftsg",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-SG',{locationquantityintransit},0)"
				  }),
				  search.createColumn({
				     name: "formulanumericitftth",
				     summary: "MAX",
				     formula: "DECODE({inventorylocation},'FT-TH',{locationquantityintransit},0)"
				  })	
		   ]
		});
		var resultSet = itemSearchObj.run();	
		
		var a = {};
		a.result =itemSearchObj;
		a.columns = resultSet.columns;
		return a;
    }
    
    function getIncoming(brand){
    	var transactionSearchObj = search.create({
    		   type: "transaction",
    		   filters: [
    		      ["type","anyof","PurchOrd","SalesOrd"], 
    		      "AND", 
    		      ["status","noneof","SalesOrd:C","SalesOrd:H","SalesOrd:G","PurchOrd:H","PurchOrd:G"], 
    		      "AND", 
    		      ["closed","is","F"], 
    		      "AND", 
    		      ["mainline","is","F"], 
    		      "AND", 
    		      ["formulanumeric: {quantity}-{quantityshiprecv}","greaterthan","0"], 
    		      "AND", 
    		      ["taxline","is","F"], 
    		      "AND", 
    		      ["mainname","noneof","1565","939","161","162"],
    		      "AND", 
    		      ["item.custitem_ftis_brand","anyof",brand]
    		   ],
    		   columns: [
    		      search.createColumn({
    		         name: "item",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
    		         name: "custcol_brand",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
    		         name: "formulanumericpo",
    		         summary: "SUM",
    		         formula: "DECODE({type},'Purchase Order',{quantity},0)"
    		      }),
    		      search.createColumn({
    		         name: "formulanumericso",
    		         summary: "SUM",
    		         formula: "DECODE({type},'Sales Order',{quantity},0)"
    		      })
    		   ]
    		});
  
			var resultSet = transactionSearchObj.run();	
			    		
    		var a = {};
    		a.result =transactionSearchObj;
    		a.columns = resultSet.columns;
    		return a;
    }
 
    function getAllSearchResults(search) {
    	var searchResultSet = search.run();
        var startIdx = 0;
        var endIdx = 1000;
        var allSearchResults = [];
        do {
        	 var searchResults = searchResultSet.getRange({
                 start : startIdx,
                 end : endIdx
             }); 
            if (searchResults) {
            	allSearchResults = allSearchResults.concat(searchResults);
                startIdx += 1000;
                endIdx += 1000;
            }
        }
        while(searchResults && searchResults.length > 0);
        return allSearchResults;
    }
    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    
    return {
        onRequest: setGo
    };
    
});
