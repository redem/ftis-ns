var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;
/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/currency', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/file', 'N/render','SuiteScripts/Internal/tools/ftis_underscore.js','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {currency} currency
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {file} file
 * @param {render} render
 */
function(currency, record, runtime, search, serverWidget, url, file, render) {
	RT = runtime;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		this.GET = function(){
			
		};
		this.POST = function(){
			try{
				var temp = {};
				var params = JSON.parse(this.request.body);

				nyapiLog('request',this.request);
				
				nyapiLog('paramsssss',params);
				
				switch(params.nstype){
					case "vendor":
						if(params.method == 'confirmVendor'){
							var result = confVendor(params.params);
							nyapiLog('resultsssss',result);
							temp = result;
						}
						break;
					default:
						temp.success = false;
						temp.message = 'Invalid nstype '+params.type;
				}
				
				this.response.write(JSON.stringify(temp));
				
			}catch(poex){
				nyapiLog('post error',poex);
			}

		};
		
    }
    function confVendor(arr){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'confirm vendor';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	//arr = arr.split(',');
    	for(var x=0; x<arr.length;x++){
    		var data ={};
    		data.internalid = arr[x];
    		try{
				var id = record.submitFields({
				    type: record.Type.VENDOR,
				    id: data.internalid,
				    values: {
				    	custentity_coty_dsv_sync: true
				    },
				    options: {
				        enableSourcing: false,
				        ignoreMandatoryFields : true
				    }
				});
				data.message = "vendor successfully confirmed "+id;
				cont.data.processed.push(data);
    		}catch(ex){
    			data.message = ex;
				cont.data.fail.push(data);
    		}
    	}
    	return cont;
    }
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
    function _mergedata(data,idata){
    	for(var y = 0; y < data.length; y++){
			var ditem = data[y].item;
			var dtran = data[y].tranid;
	
			for(var x = 0; x < idata.length; x++){
				var item = idata[x].item;
				var tran = idata[x].tranid;
				if(ditem == item && dtran == tran){				
					for (var key in idata[x].itemdetail) {
						data[y].itemdetail[key] = idata[x].itemdetail[key];
					}	
				}	
			}
		}
    	return data;
    }
     
    function getAllSearchResults(search) {
    	var searchResultSet = search.run();
        var startIdx = 0;
        var endIdx = 1000;
        var allSearchResults = [];
        do {
        	 var searchResults = searchResultSet.getRange({
                 start : startIdx,
                 end : endIdx
             }); 
            if (searchResults) {
            	allSearchResults = allSearchResults.concat(searchResults);
                startIdx += 1000;
                endIdx += 1000;
            }
        }
        while(searchResults && searchResults.length > 0);
        return allSearchResults;
    }

    return {
        onRequest: setGo
    };
    
   
    
});
