/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
define(['N/email', 'N/file', 'N/https', 'N/record', 'N/runtime', 'N/search', 'N/url','N/format','SuiteScripts/Internal/tools/ftis_underscore.js'],
/**
 * @param {email} email
 * @param {file} file
 * @param {https} https
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {url} url
 * @param {format} format
 */
function(email, file, https, record, runtime, search, url,format) {

	this.sessionObj = runtime.getCurrentSession();
	
	
	var message = {};
    /**
     * Function called upon sending a GET request to the RESTlet.
     *
     * @param {Object} requestParams - Parameters from HTTP request URL; parameters will be passed into function as an Object (for all supported content types)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.1
     */
    function doGet(requestParams) {
    	try{
    		nyapiLog('requestParams',requestParams);
    		message.success = true;
    		return message;
    		
    	}catch(exx){
    		message.success = false;
    		message.message = exx;
    		return message;
    	}
    	
    }

    /**
     * Function called upon sending a PUT request to the RESTlet.
     *
     * @param {string | Object} requestBody - The HTTP request body; request body will be passed into function as a string when request Content-Type is 'text/plain'
     * or parsed into an Object when request Content-Type is 'application/json' (in which case the body must be a valid JSON)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.2
     */
    function doPut(requestBody) {
    	try{
    		
    	}catch(exx){
    		message.message = exx;
    		return message;
    	}
    }
    function putPO(){
    	return "postPo";
    }


    /**
     * Function called upon sending a POST request to the RESTlet.
     *
     * @param {string | Object} requestBody - The HTTP request body; request body will be passed into function as a string when request Content-Type is 'text/plain'
     * or parsed into an Object when request Content-Type is 'application/json' (in which case the body must be a valid JSON)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.2
     */
    function doPost(requestBody) {
    	var scriptObj = runtime.getCurrentScript();
    	var data = requestBody;
    	nyapiLog('data1',data);
    	try{
    		message.success = true;
    		if(data instanceof Object){
    			var nstype = (data.nstype)?data.nstype:false;
    			if(typeof(nstype) == 'string'){
    				switch(data.nstype){
	    				case 'vendor':
	    					switch(data.method){
			    				case 'getAllVendor':
			    					var results = getAllVendor();
			    					message.message = 'Get All Vendor';
									message.data = results;
			    					break;
			    				case 'confirmVendor':
			    					if(data.params instanceof Array){
			    						var result = confVendor(data.params);
										message = result;
			    					}else{
			    						message.success = false;
										message.message = "invalid parameter";
			    					}
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method "+data.method;
	    					}
	    					break;
	    				case 'item':
	    					switch(data.method){
			    				case 'getAllItem':
			    					var results = getAllItem();
		    						message.message = 'Get All Item';
		    						message.data = results;
			    					break;
			    				case 'confirmItem':
			    					if(data.params instanceof Array){
			    						var result = confItem(data.params);
										message = result;
			    					}else{
			    						message.success = false;
										message.message = "invalid parameter";
			    					}
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method " + data.method;
	    					}
	    					break;
	    				case 'kit':
	    					switch(data.method){
			    				case 'getAllItemKit':
			    					var results = getAllItemKit();
		    						message.message = 'Get All Kit Items';
		    						message.data = results;
			    					break;
			    				case 'confirmItemKit':
			    					if(data.params instanceof Array){
			    						var result = confItemKit(data.params);
										message = result;
			    					}else{
			    						message.success = false;
										message.message = "invalid parameter";
			    					}
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method " + data.method;
	    					}
	    					break;
	    				case 'customer':
	        				switch(data.method){
			    				case 'getAllCustomer':
			    					var results = getAllCustomer();
		    						message.message = 'Get All Customer';
		    						message.data = results;
			    					break;
			    				case 'confirmCustomer':
			    					if(data.params instanceof Array){
			    						var result = confCustomer(data.params);
										message = result;
			    					}else{
			    						message.success = false;
										message.message = "invalid parameter";
			    					}
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method " + data.method;
	    					}
	    					break;
	    				case 'bin':
	        				switch(data.method){
			    				case 'getAllBin':
			    					var results = getAllBin();
		    						message.message = 'Get Bin';
		    						message.data = results;
			    					break;
			    				case 'confirmBin':
			    					if(data.params instanceof Array){
			    						var result = confBin(data.params);
										message = result;
			    					}else{
			    						message.success = false;
										message.message = "invalid parameter";
			    					}
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method " + data.method;
	    					}
	    					break;
	    				case 'location':
	        				switch(data.method){
			    				case 'getAllLocation':
			    					var results = getAllLocation();
		    						message.message = 'Get Location';
		    						message.data = results;
			    					break;
			    				case 'confirmLocation':
			    					if(data.params instanceof Array){
			    						var result = confLocation(data.params);
										message = result;
			    					}else{
			    						message.success = false;
										message.message = "invalid parameter";
			    					}
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method " + data.method;
	    					}
	    					break;
	    				case 'purchaseorder':
	        				switch(data.method){
			    				case 'getAllPO':
			    					var results = getAllPO();
		    						message.message = 'Get PO';
		    						message.data = results;
			    					break;
			    					
			    				case 'confirmPO':
			    					if(data.params instanceof Array){
			    						var result = confPO(data.params);
										message = result;
			    					}else{
			    						message.success = false;
										message.message = "invalid parameter";
			    					}
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method " + data.method;
	    					}
	    					break;
	    				case 'itemreceipt':
	        				switch(data.method){
			    				case 'postIR':
			    					var results = postIR(data.params[0]);
		    						message = results;
			    					break;
			    				default:
			    					message.success = false;
									message.message = "invalid method " + data.method;
	    					}
	    					break;
	    				default:
	    					message.success = false;
	        				message.message = "invalid NSTYPE";
	    			}
    			}else{
    				message.success = false;
        			message.message = "Invalid Request";
    			}
    		}else{
    			message.success = false;
    			message.message = "invalid Request";
    		}
    		nyapiLog("Remaining governance units: ", scriptObj.getRemainingUsage());
    		nyapiLog("return", message);
    		
    		return message;
    	}catch(ex){
    		nyapiLog('post error',ex);
    		message.message = ex;
    		return message;
    	}
    }
    
    
    function confBin(arr){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'confirm bins';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	
    	for(var x=0; x < arr.length;x++){
    		var data ={};
    		data.internalid = arr[x];
    		try{
				var rec = record.load({
				    type: "bin",
				    id: data.internalid});
				
				rec.setValue({
				    fieldId: 'custrecord_dsv_sync_bin',
				    value: true,
				    ignoreFieldChange: true
				});
				var id = rec.save({
				    enableSourcing: true,
				    ignoreMandatoryFields: true
				});
				data.message = "bins confirmed successfully  "+id;
				cont.data.processed.push(data);
    		}catch(ex){
    			data.message = ex;
				cont.data.fail.push(data);
    		}
    	}
    	return cont;
    }
    
    function confLocation(arr){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'confirm location';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	
    	for(var x=0; x < arr.length;x++){
    		var data ={};
    		data.internalid = arr[x];
    		try{
				var id = record.submitFields({
				    type: record.Type.LOCATION,
				    id: data.internalid,
				    values: {
				    	custrecord_dsv_sync_loc: true,
				    	usebins:true
				    },
				    options: {
				        enableSourcing: false,
				        ignoreMandatoryFields : true
				    }
				});
				data.message = "location confirmed successfully  "+id;
				cont.data.processed.push(data);
    		}catch(ex){
    			data.message = ex;
				cont.data.fail.push(data);
    		}
    	}
    	return cont;
    }
    
    function getAllLocation(){
    	var locationsearch = search.create({
 		   type: "location",
 		   filters:
 		   [
 		      ["subsidiary","anyof","7"], 
 		      "AND", 
 		      ["custrecord_dsv_sync_loc","is","F"]
 		   ],
 		   columns:
 		   [
 		      "internalid",
 		      "name",
 		      "custrecord_dsv_sync_loc"
 		   ]
 		});
    	var cont = [];
     	locationsearch.run().each(function(result){
 		    //.run().each has a limit of 4,000 results
     		var data = {};
     		data.internalid = result.getValue('internalid');
     		data.name = result.getValue('name');
     		data.custrecord_dsv_sync_loc = result.getValue('custrecord_dsv_sync_loc');
     		cont.push(data);
     		return true;
 		});
    	return cont;
    }
    function postIR2(obj){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'post IR';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	if(obj.createdfrom){
    		try {
	    		var IR = record.transform({
	        	    fromType: record.Type.PURCHASE_ORDER,
	        	    fromId: obj.createdfrom,
	        	    toType: record.Type.ITEM_RECEIPT
	        	});
	    		var lineitems = obj.items;
	    		
	    		var numLines = IR.getLineCount({
	    		    sublistId: 'item'
	    		});
	    		nyapiLog(numLines,'numLines');
	    		nyapiLog('IRs',IR.getSublists());
	    		nyapiLog('lineitems',lineitems);
	    		
	    		
	    		
	    		for(var x = 0; x< numLines; x++){
	    			
	    				var itemline = IR.getSublistValue({
						    sublistId: 'item',
						    fieldId: 'line',
						    line: x
						});
	    				nyapiLog('itemline',itemline);
	    				var selectedline = IR.getSublistValue({
						    sublistId: 'item',
						    fieldId: 'itemreceive',
						    line: x
						});
	    				var filter ={};
	    				filter.line = parseInt(itemline);
	    				nyapiLog('selectedline',filter);
	    				
	    				
	    				var dataitem = _.findWhere(lineitems,filter);
	    				
	    				nyapiLog('dataitem',dataitem);
	    				if(dataitem){
	    					nyapiLog('adddata',dataitem);
	    		
	    					
	    					IR.setSublistValue({
			    			    sublistId: 'item',
			    			    fieldId: 'itemreceive',
			    			    value: true,
			    			    line:x
			    			});
			    			IR.setSublistValue({
			    			    sublistId: 'item',
			    			    fieldId: 'item',
			    			    value: dataitem.item,
			    			    line:x
			    			});
			    			IR.setSublistValue({
			    			    sublistId: 'item',
			    			    fieldId: 'quantity',
			    			    line:x,
			    			    value: dataitem.quantity
			    			});
			    			var objSubrecord = IR.getSublistSubrecord({
			    			    sublistId: 'item',
			    			    fieldId: 'inventorydetail',
			    			    line:x
			    			});
			    			var linedetail = dataitem.inventorydetail;
			    			
			    			var wew = objSubrecord.getLineCount({
		    	    		    sublistId: 'inventoryassignment'
		    	    		});

		    				nyapiLog('wews', wew);
			    			for(var y =0 ; y < linedetail.length; y++){
			    				nyapiLog('objSubrecord', objSubrecord);
			    				
			    				objSubrecord.setSublistValue({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'receiptinventorynumber',
				    			    line : y,
				    			    value: linedetail[y].receiptinventorynumber
				    			});
				    			objSubrecord.setSublistValue({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'quantity',
				    			    line : y,
				    			    value: linedetail[y].quantity
				    			});
				    
				    			objSubrecord.setSublistText({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'expirationdate',
				    			    line : y,
				    			    text: linedetail[y].expirationdate
				    			});
				    			objSubrecord.setSublistValue({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'binnumber',
				    			    line : y,
				    			    value: linedetail[y].binnumber
				    			});
			    			}
			    			nyapiLog('objSubrecordss',objSubrecord);
	    					
	    				}
	    				
	    				
	    				
	    				
	    				
		    			
		    	
	    		}
	    		
	    	    var recId = IR.save();
	    	    
	    		cont.data.processed.push(recId);
	            nyapiLog('IR created', recId);
	            
	        }catch(e) {
	        	cont.success = false;
	        	cont.data.fail.push(e);
	        	
	        	
	        }
	        
	        return cont;
    	}
    }
    
    
    
    
    
    function postIR(obj){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'post IR';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	if(obj.createdfrom){
    		try {
	    		var IR = record.transform({
	        	    fromType: record.Type.PURCHASE_ORDER,
	        	    fromId: obj.createdfrom,
	        	    toType: record.Type.ITEM_RECEIPT
	        	});
	    		var lineitems = obj.items;
	    		
	    		var numLines = IR.getLineCount({
	    		    sublistId: 'item'
	    		});
	    		nyapiLog(numLines,'numLines');
	    		nyapiLog('IRs',IR.getSublists());
	    		nyapiLog('lineitems',lineitems);
	    		for(var x = 0; x< numLines; x++){
	    				var itemline = IR.getSublistValue({
						    sublistId: 'item',
						    fieldId: 'line',
						    line: x
						});
	    				nyapiLog('itemline',itemline);
	    				var filter ={};
	    				filter.line = parseInt(itemline);
	    				nyapiLog('selectedline',filter);
	    				var dataitem = _.findWhere(lineitems,filter);
	    				nyapiLog('dataitem',dataitem);
	    				if(dataitem){
	    					nyapiLog('adddata',dataitem);			
	    					IR.setSublistValue({
			    			    sublistId: 'item',
			    			    fieldId: 'itemreceive',
			    			    value: true,
			    			    line:x
			    			});
	    					
			    			IR.setSublistValue({
			    			    sublistId: 'item',
			    			    fieldId: 'item',
			    			    value: dataitem.item,
			    			    line:x
			    			});
			    			IR.setSublistValue({
			    			    sublistId: 'item',
			    			    fieldId: 'quantity',
			    			    line:x,
			    			    value: dataitem.quantity
			    			});
			    			
			    			var objSubrecord = IR.getSublistSubrecord({
			    			    sublistId: 'item',
			    			    fieldId: 'inventorydetail',
			    			    line:x
			    			});
			    			var linedetail = dataitem.inventorydetail;
			    			
			    			var wew = objSubrecord.getLineCount({
		    	    		    sublistId: 'inventoryassignment'
		    	    		});
		    				nyapiLog('wews', wew);
		    				nyapiLog('objSubrecord', objSubrecord);
			    			for(var y =0 ; y < linedetail.length; y++){
			    				var item = objSubrecord.getSublistValue({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'receiptinventorynumber',
				    			    line : y
				    			});
			    				nyapiLog('line receiptinventorynumber '+y,item);
			    				
			    				objSubrecord.setSublistValue({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'receiptinventorynumber',
				    			    line : y,
				    			    value: linedetail[y].receiptinventorynumber
				    			});
				    			objSubrecord.setSublistValue({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'quantity',
				    			    line : y,
				    			    value: linedetail[y].quantity
				    			});
				    
				    			objSubrecord.setSublistText({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'expirationdate',
				    			    line : y,
				    			    text: linedetail[y].expirationdate
				    			});
				    			objSubrecord.setSublistValue({
				    			    sublistId: 'inventoryassignment',
				    			    fieldId: 'binnumber',
				    			    line : y,
				    			    value: linedetail[y].binnumber
				    			});
			    			}
			    			nyapiLog('objSubrecordss',objSubrecord);
			    			
	    				}
		    	
	    		}
	    		
	    	    var recId = IR.save();
	    	    
	    		cont.data.processed.push(recId);
	            nyapiLog('IR created', recId);
	            
	        }catch(e) {
	        	cont.success = false;
	        	cont.data.fail.push(e);
	        	
	        	
	        }
	        
	        return cont;
    	}
    }
    
    function confPO(arr){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'confirm purchaseorder';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	//arr = arr.split(',');
    	for(var x=0; x<arr.length;x++){
    		var data ={};
    		data.internalid = arr[x];
    		try{
				var id = record.submitFields({
				    type: record.Type.PURCHASE_ORDER,
				    id: data.internalid,
				    values: {
				    	custbody_coty_dsv_sync: true
				    },
				    options: {
				        enableSourcing: false,
				        ignoreMandatoryFields : true
				    }
				});
				data.message = "Purchase Order confirmed successfully "+id;
				cont.data.processed.push(data);
    		}catch(ex){
    			data.message = ex;
				cont.data.fail.push(data);
    		}
    	}
    	return cont;
    }
    
    function getAllPO(){
    	var posearchss = search.create({
    		   type: "purchaseorder",
    		   filters:
    		   [
    		      ["type","anyof","PurchOrd"], 
    		      "AND", 
    		      ["custbody_coty_dsv_sync","is","F"], 
    		      "AND", 
    		      ["mainline","is","F"], 
    		      "AND", 
    		      ["subsidiary","anyof","7"], 
    		      "AND", 
    		      ["taxline","is","F"], 
    		      "AND", 
    		      ["status","anyof","PurchOrd:E","PurchOrd:B"], 
    		      "AND", 
    		      ["closed","is","F"]
    		   ],
    		   columns:
    		   [
    		      "internalid",
    		      "tranid",
    		      "mainname",
    		      "trandate",
    		      "custbody_coty_dsv_sync",
    		      "shipdate",
    		      "statusref",
    		      "location",
    		      "line",
    		      "item",
    		      "quantity",
    		      "quantityshiprecv",
    		      search.createColumn({
    		         name: "formulanumeric",
    		         formula: "{quantity}-{quantityshiprecv}"
    		      }),
    		      "closed"
    		   ]
    		});
    	
    	var posearch = search.create({
    		   type: "purchaseorder",
    		   filters:
    		   [
    		      ["type","anyof","PurchOrd"], 
    		      "AND", 
    		      ["custbody_coty_dsv_sync","is","F"], 
    		      "AND", 
    		      ["mainline","is","F"], 
    		      "AND", 
    		      ["subsidiary","anyof","7"], 
    		      "AND", 
    		      ["taxline","is","F"], 
    		      "AND", 
    		      ["status","anyof","PurchOrd:E","PurchOrd:B"], 
    		      "AND", 
    		      ["closed","is","F"]
    		   ],
    		   columns:
    		   [
    		      "internalid",
    		      "tranid",
    		      "mainname",
    		      "trandate",
    		      "custbody_coty_dsv_sync",
    		      "shipdate",
    		      "statusref",
    		      "location",
    		      "line",
    		      "item",
    		     /** search.createColumn({
    		         name: "internalid",
    		         join: "inventoryDetail"
    		      }),
    		      search.createColumn({
    		         name: "item",
    		         join: "inventoryDetail"
    		      }),
    		      search.createColumn({
    		         name: "inventorynumber",
    		         join: "inventoryDetail"
    		      }),
    		      search.createColumn({
    		         name: "binnumber",
    		         join: "inventoryDetail"
    		      }),
    		      search.createColumn({
    		         name: "quantity",
    		         join: "inventoryDetail"
    		      }),**/
    		      "quantityshiprecv",
    		      "quantity",
    		      search.createColumn({
    		         name: "formulanumeric",
    		         formula: "{quantity}-{quantityshiprecv}"
    		      }),
    		      "closed"
    		   ]
    		});
    	
    	var cont = [];
		posearch.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			var data = {};
			data.internalid = result.getValue('internalid');
			data.entity = result.getValue('mainname');
			data.custbody_coty_dsv_sync = result.getValue('custbody_coty_dsv_sync');
			data.trandate = result.getValue('trandate');
			data.statusref = result.getValue('statusref');
			
			
			data.lines = [];
		
			var lines = {};
			lines.line = result.getValue('line');
			lines.item = result.getValue('item');
			lines.quantity = result.getValue('quantity');
			lines.quantityshiprecv = result.getValue('quantityshiprecv');
			lines.remaining = result.getValue('formulanumeric');
			lines.closed = result.getValue('closed');
			/**lines.inventoryDetail = [];
			
			var inventorydetail = {};
			var invid = result.getValue({
				    name: 'internalid',
				    join: 'inventoryDetail'
				  });
			inventorydetail.internlaid = invid;
			
			var invitem = result.getValue({
				    name: 'item',
				    join: 'inventoryDetail'
				  });
			inventorydetail.item = invitem;
			
			var invbinnumber = result.getValue({
			    name: 'binnumber',
			    join: 'inventoryDetail'
			  });
			inventorydetail.binnumber = invbinnumber;
			
			var invqty = result.getValue({
			    name: 'quantity',
			    join: 'inventoryDetail'
			  });
			inventorydetail.quantity = invqty;
			**/
			var filter ={};
			filter.internalid = data.internalid;
			
			var even = _.findWhere(cont,filter);
			
			if(even instanceof Object){
				even.lines.push(lines);
				
			}else{
				data.lines.push(lines);
				cont.push(data);
			}		
		   return true;
		});
		
		return cont;
    		
    }
    
    function getAllBin(){
    	var cont = [];
    	var binSearchObj = search.create({
		   type: "bin",
		   filters:
		   [
		      ["location","anyof","34"]
		   ],
		   columns:
		   [
		      search.createColumn({
		         name: "binnumber",
		         sort: search.Sort.ASC
		      }),
		      "custrecord_dsv_sync_bin",
		      "internalid",
		      "location"
		      
		   ]
		});
    	
		binSearchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			var data= {};
			data.internalid = result.getValue('internalid');
			data.binnumber = result.getValue('binnumber');
			data.custrecord_dsv_sync_bin = result.getValue('custrecord_dsv_sync_bin');
			data.location = result.getValue('location');
			cont.push(data);
			
		   return true;
		});
		
		return cont;
    }
    
    function isObjEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
    
    
    function confVendor(arr){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'confirm vendor';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	//arr = arr.split(',');
    	for(var x=0; x<arr.length;x++){
    		var data ={};
    		data.internalid = arr[x];
    		try{
				var id = record.submitFields({
				    type: record.Type.VENDOR,
				    id: data.internalid,
				    values: {
				    	custentity_coty_dsv_sync: true
				    },
				    options: {
				        enableSourcing: false,
				        ignoreMandatoryFields : true
				    }
				});
				data.message = "vendor confirmed successfully"+id;
				cont.data.processed.push(data);
    		}catch(ex){
    			data.message = ex;
				cont.data.fail.push(data);
    		}
    	}
    	return cont;
    }
    
    
    function getAllVendor(){
    	var vendorSearchObj = search.create({
		   type: "vendor",
		   filters:
		   [
			   ["subsidiary","anyof","7"], 
			   "AND", 
			   ["custentity_coty_dsv_sync","is","F"]
		   ],
		   columns:
		   [
		      "internalid",
		      search.createColumn({
		         name: "entityid",
		         sort: search.Sort.ASC
		      }),
		      "custentity_coty_dsv_sync",
		      "shipaddress",
		      "billaddress"
		   ]
		});
		var cont = [];
		vendorSearchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			var data = {};
			data.internalid = result.getValue('internalid');
			data.entityid = result.getValue('entityid');
			data.custentity_coty_dsv_sync = result.getValue('custentity_coty_dsv_sync');
			data.shipaddress = result.getValue('shipaddress');
			data.billaddress = result.getValue('billaddress');
			cont.push(data);
		   return true;
		});
		return cont;
		
    }
    
    function confItem(arr){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'confirm Item';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	//arr = arr.split(',');
    	for(var x=0; x<arr.length;x++){
    		var data ={};
    		data.internalid = arr[x];
    		try{				
				var id = record.submitFields({
				    type: record.Type.LOT_NUMBERED_INVENTORY_ITEM,
				    id: data.internalid,
				    values: {
				    	'custitem_coty_dsv_sync': true
				    },
				    options: {
				        enableSourcing: false,
				        ignoreMandatoryFields : true
				    }
				});
				data.message = "Item confirmed successfully  "+id;
				cont.data.processed.push(data);
    		}catch(ex){
    			data.message = ex;
				cont.data.fail.push(data);
    		}
    	}
    	return cont;
    }
    
    
    function getAllItem(){
    	var inventoryitemSearchObj = search.create({
    		   type: "inventoryitem",
    		   filters: [
    		      ["subsidiary","anyof","7"], 
    		      "AND", 
    		      ["type","anyof","InvtPart"],
    		      "AND",
    		      ["custitem_coty_dsv_sync","is","F"]
    		      
    		   ],
    		   columns: [
    		      "internalid",
    		      search.createColumn({
    		         name: "itemid",
    		         sort: search.Sort.ASC
    		      }),
    		      "displayname",
    		      "type",
    		      "custitem_coty_dsv_sync",
    		      "unitstype"
    		   ]
    		});
    	var cont =[];
		inventoryitemSearchObj.run().each(function(result){
			var data ={};
			data.internlaid = result.getValue('internalid');
			data.itemid = result.getValue('itemid');
			data.displayname = result.getValue('displayname');
			data.type = result.getValue('type');
			data.unitstype = result.getValue('unitstype');
			data.custitem_coty_dsv_sync = result.getValue('custitem_coty_dsv_sync');
			
			cont.push(data);
		   return true;
		});
		return cont;
    }
    
    function confCustomer(arr){
    	var cont ={};
    	cont.success = true;
    	cont.message = 'confirm customer';
    	cont.data = {};
    	cont.data.fail = [];
    	cont.data.processed = [];
    	//arr = arr.split(',');
    	for(var x=0; x<arr.length;x++){
    		var data ={};
    		data.internalid = arr[x];
    		try{
				var id = record.submitFields({
				    type: record.Type.CUSTOMER,
				    id: data.internalid,
				    values: {
				    	custentity_coty_dsv_sync: true
				    },
				    options: {
				        enableSourcing: false,
				        ignoreMandatoryFields : true
				    }
				});
				data.message = "customer confirmed successfully  "+id;
				cont.data.processed.push(data);
    		}catch(ex){
    			data.message = ex;
				cont.data.fail.push(data);
    		}
    	}
    	return cont;
    }
    
    function getAllCustomer(){
    	
    	var customerSearchObj = search.create({
		   type: "customer",
		   filters: [
		      ["subsidiary","anyof","7"],
		      "AND", 
			  ["custentity_coty_dsv_sync","is","F"]
		   ],
		   columns: [
		      "internalid",
		      search.createColumn({
		         name: "entityid",
		         sort: search.Sort.ASC
		      }),
		      "shipaddress",
		      "billaddress",
		      "custentity_coty_dsv_sync"
		   ]
		});
    	
		var cont = [];
		
		customerSearchObj.run().each(function(result){
			var data = {};
			data.internalid = result.getValue('internalid');
			data.entityid = result.getValue('entityid');
			data.shipaddress = result.getValue('shipaddress');
			data.billaddress = result.getValue('billaddress');
			data.custentity_coty_dsv_sync = result.getValue('custentity_coty_dsv_sync');
			
			cont.push(data);
		   return true;
		});
		
		return cont;
    }
    
    return {
        'get': doGet,
        put: doPut,
        post: doPost
    };
    
});
