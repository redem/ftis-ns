var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/encode','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 */
function(file, http, record, render, runtime, search, serverWidget, url, encode) {
    RT = runtime;
    ENCODEMODULE = encode ;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var form = serverWidget.createForm({title:'Test Gap Analysis'});
			FORM = form;
			var params = this.request.parameters;
			nyapiLog('param',params);
			
			
			
			var planner = form.addField({id:'custpage_planner',label : 'Planner',type:'select'});
			
			planner.addSelectOption({
			    value : 41,
			    text : 'Gerald Hai'
			});
			planner.addSelectOption({
			    value : 34,
			    text : 'Yeo Boon Xin'
			});
			planner.addSelectOption({
			    value : 2192,
			    text : 'Apple'
			});
			
			
			if(params.planner){
				planner.defaultValue = params.planner;
			}
			
			var rptlevel = form.addField({
			    id : 'custpage_level',
			    type : serverWidget.FieldType.SELECT,
			    label : 'Report Level'
			});
			
			rptlevel.addSelectOption({
			    value : 1,
			    text : 'Level 1'
			});
			rptlevel.addSelectOption({
			    value : 2,
			    text : 'Level 2'
			});
			if(params.level){
				rptlevel.defaultValue = params.level;
			}
			
			
			var sublist = form.addSublist({
				id: 'custpage_sub_bil',
				type: 'list',
				label: 'Result'
			});
			
			
			//sublist.style = serverWidget.ListStyle.GRID;
			var cols = [];
			
			//1
			//sublist.addField({id : 'custpage_internalid',type: 'text',label :'Item id'}); 
			sublist.addField({id : 'custpage_itemid' ,type: 'text',label :'ITEM'}); 
			cols.push({id:'custpage_itemid',label:'ITEM'});
			sublist.addField({id : 'custpage_custitem_ftis_brand' ,type: 'text',label :'Brand'}); 
			cols.push({id:'custpage_custitem_ftis_brand',label:'Brand'});
			//sublist.addField({id : 'custpage_formulanumericftsg' ,type: 'text',label :'ON HAND FT-SG'}); 
			//sublist.addField({id : 'custpage_formulanumericcsg' ,type: 'text',label :'ON HAND CONSIGN-SG'});
			//sublist.addField({id : 'custpage_formulanumericftnz' ,type: 'text',label :'ON HAND FT-NZ'}); 
			//sublist.addField({id : 'custpage_formulanumericftth' ,type: 'text',label :'ON HAND FT TH'});
			//sublist.addField({id : 'custpage_formulanumericvmith' ,type: 'text',label :'ON HAND VMI TH'}); 
			//sublist.addField({id : 'custpage_formulanumericbillth' ,type: 'text',label :'ON HAND BILL TH'});
			if(params.level == 2){
				sublist.addField({id : 'custpage_custrecord_forecast_ass' ,type: 'text',label :'ADDITIONAL SAFETY STOCKS'}); 
				sublist.addField({id : 'custpage_custrecord_forecast_ms' ,type: 'text',label :'MINIMUM STOCKS'}); 
				sublist.addField({id : 'custpage_custrecord_forecast_tbs' ,type: 'text',label :'TOTAL BUFFER STOCKS'}); 
				cols.push({id:'custpage_custrecord_forecast_ass',label:" ADDITIONAL SAFETY STOCKS"});
				cols.push({id:'custpage_custrecord_forecast_ms',label:" MINIMUM STOCKS"});
				cols.push({id:'custpage_custrecord_forecast_tbs',label: " TOTAL BUFFER STOCKS"});
			}
			sublist.addField({id : 'custpage_formulanumerictotoh' ,type: 'text',label :'TOTAL ON HAND'}); 
			cols.push({id:'custpage_formulanumerictotoh',label:'TOTAL ON HAND'});
			
			if(params.level == 2){
				sublist.addField({id : 'custpage_formulanumericnoweek' ,type: 'text',label :'NUMBER OF WEEK'}); 
				cols.push({id:'custpage_formulanumericnoweek',label:"NUMBER OF WEEK"});
			}
			
			//2
			//sublist.addField({id : 'custpage_formulanumericitftsg' ,type: 'text',label :'In transit FT-SG'}); 
			//sublist.addField({id : 'custpage_formulanumericitftth' ,type: 'text',label :'In transit FT-TH'});
			
			//3
			//sublist.addField({id : 'custpage_formulanumericpo' ,type: 'text',label :'TOTAL INCOMMING PO'}); 
			//sublist.addField({id : 'custpage_formulanumericso' ,type: 'text',label :'TOTAL INCOMMING SO'}); 
			
			if(params.level == 2){
				sublist.addField({id : 'custpage_custrecord_ftis_cus_soh',type: 'text',label :'Customer Stock on Hand'});
			}
				cols.push({id:'custpage_custrecord_ftis_cus_soh',label:'Customer Stock on Hand'});
			if(params.level == 2){
				sublist.addField({id : 'custpage_formulanumericuqty' ,type: 'text',label :'Customer Usage at MTD'});
			}
				cols.push({id:'custpage_formulanumericuqty',label:'Customer Usage as of MTD'});
			if(params.level == 2){
				sublist.addField({id : 'custpage_formulanumericfcuqty' ,type: 'text',label :'CUSTOMER USAGE FROM LAST FORECAST TO MTD'});
			}
				cols.push({id:'custpage_formulanumericfcuqty',label:'CUSTOMER USAGE FROM LAST FORECAST TO MTD'});
			
		
			
			
			
			var today = getNow();
			for(var x = 0; x < 7; x++){
				var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();
				var lftl =  Date.parse('today + '+x+' month').toString('MMM, yyyy') + " ";
				
				if(params.level == 2){
					sublist.addField({id : 'custpage_formulanumericfo'+ltf ,type: 'text',label :lftl + " LAST FORECAST"});
				}
				if(x==0){ 
					if(params.level == 2){
						sublist.addField({id : 'custpage_formulanumericpso' ,type: 'text',label :'OVER DUE SO'}); 
					}
					cols.push({id:'custpage_formulanumericpso',label:'OVER DUE SO'});
				}
				if(params.level == 2){
					sublist.addField({id : 'custpage_formulanumericso'+ltf ,type: 'text',label :lftl + " SALES BACKLOG"});
				}
				sublist.addField({id : 'custpage_formulanumericbal'+ltf ,type: 'text',label :lftl + " GAP"});
				if(params.level == 2){
					sublist.addField({id : 'custpage_formulanumericit'+ltf ,type: 'text',label :lftl + " INTRANSIT"});
					sublist.addField({id : 'custpage_formulanumericpo'+ltf ,type: 'text',label :lftl + " PURCHASE BACKLOG"});
				}
				cols.push({id:'custpage_formulanumericfo'+ltf,label:lftl + " LAST FORECAST"});
				cols.push({id:'custpage_formulanumericso'+ltf,label:lftl + " SALES BACKLOG"});
				cols.push({id:'custpage_formulanumericbal'+ltf,label:lftl + " GAP"});
				cols.push({id:'custpage_formulanumericit'+ltf,label:lftl + " INTRANSIT"});
				cols.push({id:'custpage_formulanumericpo'+ltf,label:lftl + " PURCHASE BACKLOG"});
				
				if(x==0){
					if(params.level == 2){
						sublist.addField({id : 'custpage_formulanumericppo' ,type: 'text',label :'DELINQUENT PO'}); 
					}
					cols.push({id:'custpage_formulanumericppo',label:" DELINQUENT PO"});
					
				}
				if(params.level == 2){
					sublist.addField({id : 'custpage_formulanumericoh'+ltf ,type: 'text',label :"SOH END "+ lftl});
				}
				cols.push({id:'custpage_formulanumericoh'+ltf,label:"SOH END "+ lftl});
			}
			
			
			//if(params.brand){
			if(params.planner){

				if(params.level == 1){
					form.addButton({
						id : 'custpage_ex1',
						label : 'Download Excel',
						functionName : 'exclick'
					});
				}else{
					form.addButton({
						id : 'custpage_ex2',
						label : 'Download Excel',
						functionName : 'exclick2'
					});
				}
				
				
				
				
				
				var mdata = [];

				//var item = _getFtItem(params.brand);
				var item = _getFtItem(params.planner);
				var data = item.a;

				//var incomming = _getFtIncoming(params.brand);
				//mdata = _mergePOSO(data,incomming.a);
				
				//var ndata = _getTran(params.brand);
				var ndata = _getTran(params.planner);
				mdata = _mergeFORE(data,ndata.a);
				
				
				//var fcast = _getFcast(params.brand);
				var fcast = _getFcast(params.planner);
				mdata = _mergeFcast(mdata,fcast.a);
				
				//var usage = _getUsage(params.brand);
				var usage = _getUsage(params.planner);
				
				mdata = _mergeUsage(mdata,usage.a);
				
				//var intransit = _getIntransit(params.brand);
				var intransit = _getIntransit(params.planner);
				mdata = _mergeIntransit(mdata,intransit.a);
				var cow = {};
				cow.beef = [];
				cow.col = cols;
				for(var col = 0; col < mdata.length; col++){
					var body = {};
					var item = mdata[col].item;				
					var detail = mdata[col].itemdetail;
					
					//for (var key in details) {
						var f = "custpage_";
						var itemv = detail['itemid'].value;
						var s = {};
						s.id = item;
						s.value = itemv;
						
						var samp = "<a href='javascript:void(0)' id='tlink' onclick='return executeOnClick("+JSON.stringify(s)+");'>"+itemv+"</a>";
						
						
						sublist.setSublistValue({id : f+"itemid", line: col,value : samp});	
						
						body[f+"itemid"] = detail['itemid'].value;
						
						sublist.setSublistValue({id : f+"custitem_ftis_brand", line: col,value : detail['custitem_ftis_brand'].text});	
						body[f+"custitem_ftis_brand"] = detail['custitem_ftis_brand'].text;
						
						sublist.setSublistValue({id : f+"custitem_ftis_planner_buyer", line: col,value : detail['custitem_ftis_planner_buyer'].text});	
						body[f+"custitem_ftis_planner_buyer"] = detail['custitem_ftis_planner_buyer'].text;
						
						var addlabel = "custrecord_forecast_ass";
						var adds = (detail[addlabel].value)?detail[addlabel].value:0;
						sublist.setSublistValue({id : f+addlabel, line: col,value : adds});	
						body[f+addlabel] =adds;
						
						var mslabel = "custrecord_forecast_ms";
						var mss = (detail[mslabel].value)?detail[mslabel].value:0;
						sublist.setSublistValue({id : f+mslabel, line: col,value : mss});	
						body[f+mslabel] =mss;
						
						var tbslabel = "custrecord_forecast_tbs";
						var tbss = (detail[tbslabel].value)?detail[tbslabel].value:0;
						sublist.setSublistValue({id : f+tbslabel, line: col,value : tbss});	
						body[f+tbslabel] =tbss;
						
						var onhandlabel = "formulanumerictotoh";
						var onhand = (detail[onhandlabel].value)?detail[onhandlabel].value:detail[onhandlabel].text;
						sublist.setSublistValue({id : f+onhandlabel, line: col,value : onhand});	
						body[f+onhandlabel] = onhand;
						onhand = Math.abs(onhand);
						if(typeof(onhand)!='number'){
							onhand = Math.abs(onhand);
						}
						
						var sohlbl = "custrecord_ftis_cus_soh";
						var soh = (detail[sohlbl].value)?detail[sohlbl].value:detail[sohlbl].text;
						soh = (soh)?soh:0;
						sublist.setSublistValue({id : f+sohlbl, line: col,value : soh.toString()});	
						body[f+sohlbl] =soh;
						soh =Math.abs(soh);
						
						var ohsoh = onhand + soh;
						
						var nowek = "formulanumericnoweek";
						var nowekval = (detail[nowek].value)?detail[nowek].value:detail[nowek].text;
						if(Math.abs(nowekval)){
							nowekval = ohsoh/((Math.abs(nowekval)/3)/13);
						}else{
							//nowekval = ((ohsoh/3)/13);
						}
						
						nowekval = Math.abs(nowekval).toFixed(2).replace(".00", "");
						body[f+nowek] = nowekval;
						
						sublist.setSublistValue({id : f+nowek, line: col,value : nowekval});
						
						var usagelbl = "formulanumericuqty";
						var usage = (detail[usagelbl].value)?detail[usagelbl].value:detail[usagelbl].text;
						sublist.setSublistValue({id : f+usagelbl, line: col,value : usage});	
						body[f+usagelbl] =usage;
				
						var fcusagelbl = "formulanumericfcuqty";
						var fcusage = (detail[fcusagelbl].value)?detail[fcusagelbl].value:detail[usagelbl].text;
						sublist.setSublistValue({id : f+fcusagelbl, line: col,value : fcusage});	
						body[f+fcusagelbl] =fcusage;
						
						
						
						
						var today = getNow();

						for(var x = 0; x < 7; x++){
							var inegative = 0;
							var negative = 0;
							var fnega = 0;
							var positive = 0;
							var ppo = 0;
							var pso = 0;
							var inc = 0;
							var mforecast = 0;
							var mso = 0;
							var fusage = 0;
							
							
							var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();

							var mflbl = 'formulanumericfo'+ltf;
							mforecast =  (detail[mflbl].value)?detail[mflbl].value:detail[mflbl].text;
							sublist.setSublistValue({id : f+mflbl, line: col,value : mforecast});	
							body[f+mflbl] =mforecast;
							mforecast = Math.abs(mforecast);
							
							var msolbl = 'formulanumericso'+ltf;
							mso = (detail[msolbl].value)?detail[msolbl].value:detail[msolbl].text;
							sublist.setSublistValue({id : f+msolbl, line: col,value : mso});	
							body[f+msolbl] =mso;
							mso = Math.abs(mso);
							
							if(x==0){ 							
								var ppolbl = 'formulanumericppo';
								ppo = (detail[ppolbl].value)?detail[ppolbl].value:detail[ppolbl].text;
								sublist.setSublistValue({id : f+ppolbl, line: col,value : ppo});
								body[f+ppolbl] = ppo;
								ppo = Math.abs(ppo);
								
								var psolbl = 'formulanumericpso';
								pso = (detail[psolbl].value)?detail[psolbl].value:detail[psolbl].text;
								sublist.setSublistValue({id : f+psolbl, line: col,value : pso});	
								body[f+psolbl] =pso;
								pso = Math.abs(pso);
								
								positive = Math.abs(soh);
								fusage = Math.abs(fcusage);
						
							}
							
							fnega = mso + pso;
							if(mforecast > fnega){
								inegative = mforecast;
							}else{
								inegative = fnega;
							}
							negative = inegative - fusage;

							var initoh = onhand + positive;
							
							onhand = initoh - negative;
							
							sublist.setSublistValue({id : f+"formulanumericbal"+ltf, line: col,value : onhand.toString()});	
							body[ f+"formulanumericbal"+ltf] = onhand;
						
							var mitlbl = 'formulanumericit'+ltf;
							var mit = (!detail[mitlbl])?0:((detail[mitlbl].value)?detail[mitlbl].value:detail[mitlbl].text);
							sublist.setSublistValue({id : f+mitlbl, line: col,value : mit});
							body[f+mitlbl] = mit;
							
							
							var mpolbl = 'formulanumericpo'+ltf;
							var mpo =  (detail[mpolbl].value)?detail[mpolbl].value:detail[mpolbl].text;
							sublist.setSublistValue({id : f+mpolbl, line: col,value : mpo});
							body[f+mpolbl] =mpo;
							
							inc = Math.abs(mit) + Math.abs(ppo) + Math.abs(mpo);
				
							
							onhand = onhand + inc;
							
							sublist.setSublistValue({id : f+"formulanumericoh"+ltf, line: col, value : onhand.toString()});	
							body[ f+"formulanumericoh"+ltf] = onhand;

						}
						
						
						cow.beef.push(body);
						this.sessionObj.set({
						    name: "cow", 
						    value: JSON.stringify(cow)
						    });
					
				}
			}
			getInjectableForm();
			
			form.clientScriptFileId = 36701;
			
			this.response.writePage(form);
			
		};
		
		this.POST = function(){
			
			var cow = this.sessionObj.get({name: "cow"});
			cow = JSON.parse(cow);
			var loadFile = file.load('32483');
			var s = loadFile.getContents();
			
			var html = s;
			html = html.process(cow);
			
			var fileObj = file.create({
			    name: 'GapAnalysis.xml' ,
			    fileType: file.Type.XMLDOC,
			    contents: html,
			    description: 'sample',
			    encoding: file.Encoding.UTF8
			});
			
			this.response.setHeader({name:'content-type',value:'XMLDOC'});
	        	
	        this.response.writeFile(fileObj); 		
	     
			
		
		
		};
		
    }
    
    function _mergeIntransit(data,idata){
    	for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var exist = false;
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				if(db == da){
					for (var key in idata[x].itemdetail) {
						if(key !='item'){
							exist = true;
							data[y].itemdetail[key] = idata[x].itemdetail[key];
						}
					}
				}	
			}
			if(!exist){
				for(var x = 0; x < 7; x++){			    		
		    		var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();
		    		data[y].itemdetail["formulanumericit"+ltf] = {'value':0,'text':"0"};	    		
		    	}
			}
		}
    	return data;
    }
    
    
    
    function _getIntransit(brand){
		var item = getIntransit(brand);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.a = [];
		
		for(var line = 0; line < result.length; line++){
			 var t = {};
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.a.push(t);
		}
		return cont;
    }
    
    function getIntransit(brand){
    	
    	var col =  [
		      search.createColumn({
			         name: "item",
			         summary: "GROUP"
			      })
			   ];
    	
    	for(var x = 0; x < 7; x++){
     		var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();
     		var scol = search.createColumn({
 	   	         name: "formulanumericit"+ltf,
 	   	         summary: "SUM",
 	   	         formula: "CASE WHEN {expectedreceiptdate} BETWEEN TRUNC(ADD_MONTHS({today},"+x+"),'month') AND LAST_DAY(ADD_MONTHS({today},"+x+")) THEN {transferorderquantityshipped}-{transferorderquantityreceived} ELSE 0 END"
     		     });     		
     		col.push(scol);
     	}
    	
    	var transactionSearchObj = search.create({
		   type: "transferorder",
		   filters: [
		      ["type","anyof","TrnfrOrd"], 
		      "AND", 
		      ["status","anyof","TrnfrOrd:D","TrnfrOrd:E","TrnfrOrd:F"],
		      "AND", 
		      ["item.custitem_ftis_planner_buyer","anyof",brand], 
		      "AND", 
		      ["closed","is","F"], 
		      "AND", 
		      ["cogs","is","F"], 
		      "AND", 
		      ["mainline","is","F"], 
		      "AND", 
		      ["formulanumeric: CASE WHEN {location} = {transferlocation} THEN 0 ELSE 1 END","equalto","1"] 
		   ],
		   columns: col
		});

		var resultSet = transactionSearchObj.run();	
		var a = {};
		a.result = transactionSearchObj;
		a.columns = resultSet.columns;
		return a;

    }
    
    function _mergeUsage(data,idata){
    	for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var exist = false;
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				if(db == da){
					for (var key in idata[x].itemdetail) {
							exist = true;
							data[y].itemdetail[key] = idata[x].itemdetail[key];
					}
				}	
			}
			if(!exist){
				data[y].itemdetail['formulanumericuqty'] = {'value':0,'text':"0"};	
				data[y].itemdetail['formulanumericfcuqty'] = {'value':0,'text':"0"};
			
				
			}
		}
    	return data;
    }

    function _getUsage(brand){
		var item = getUsage(brand);
		
		var result = getAllSearchResults(item.result);
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.a = [];
			
		for(var line = 0; line < result.length; line++){
			 var t = {};
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
					 var data = {};
					 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
					 var text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
					 data.text = (text)?text:"0";
					 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.a.push(t);
		}
		return cont;
    }
    
    
    function getUsage(brand){
    	var itemfulfillmentSearchObj = search.create({
		   type: "itemfulfillment",
		   filters: [
		      ["type","anyof","ItemShip"], 
		      "AND", 
		      ["status","anyof","ItemShip:C"], 
		      "AND", 
		      ["formulatext: {name}","isnotempty",""], 
		      "AND", 
		      ["shipping","is","F"], 
		      "AND", 
		      ["cogs","is","F"], 
		      "AND", 
		      ["taxline","is","F"], 
		      "AND", 
		      ["closed","is","F"], 
		      "AND", 
		      ["createdfrom.type","anyof","SalesOrd"], 
		      "AND", 
		      ["name","noneof","1565","161"], 
		      "AND", 
		      ["formulanumeric: CASE WHEN {trandate} >= TRUNC({today},'month') THEN 1 ELSE 0 END ","equalto","1"], 
		      "AND", 
		      ["item.custitem_ftis_planner_buyer","anyof",brand]
		   ],
		   columns: [
		      search.createColumn({
		         name: "item",
		         summary: "GROUP",
		         sort: search.Sort.ASC
		      }),
		      search.createColumn({
		         name: "custitem_ftis_brand",
		         join: "item",
		         summary: "GROUP"
		      }),
		      search.createColumn({
		         name: "formulanumericuqty",
		         summary: "SUM",
		         formula: "CASE WHEN {trandate} BETWEEN TRUNC({today},'month') AND LAST_DAY({today}) THEN {quantity} ELSE 0 END"
		      }),
		      search.createColumn({
			         name: "formulanumericfcuqty",
			         summary: "SUM",
			         formula: "CASE WHEN {trandate} BETWEEN {item.custitem_fcast_date} AND {today} THEN {quantity} ELSE 0 END"
			      })
		   ]
		});
		
    	var resultSet = itemfulfillmentSearchObj.run();	
		var a = {};
		a.result = itemfulfillmentSearchObj;
		a.columns = resultSet.columns;
		return a;

    }
    
	function _mergeFcast(data,idata){
		
    	for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var exist = false;
			
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				if(db == da){
					exist = true;									
					for (var key in idata[x].itemdetail) {
						data[y].itemdetail[key] = idata[x].itemdetail[key];
					}	
				}	
			}
			if(!exist){
				data[y].itemdetail["custrecord_ftis_cus_soh"] = {'value':0,'text':"0"};
				data[y].itemdetail['custrecord_forecast_ass'] = {'value':0,'text':"0"};
				data[y].itemdetail['custrecord_forecast_ms'] = {'value':0,'text':"0"};
				data[y].itemdetail['custrecord_forecast_tbs'] = {'value':0,'text':"0"};
				data[y].itemdetail['formulanumericnoweek'] = {'value':0,'text':"0"};
				for(var x = 0; x < 7; x++){			    		
		    		var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();
		    		data[y].itemdetail["formulanumericfo"+ltf] = {'value':0,'text':"0"};	    		
		    	}
			}
		}
    	return data;
		
    }
    
   
    function _getFcast(brand){
		
		var initfor = getBrandForecast(brand);
		
		var cont = {};
		cont.col = [];
		cont.a = [];
		
		if(initfor.fcast.length){
			var fcast = getDetailForecast(initfor);
			var result = getAllSearchResults(fcast.result);	
			var itemcol =  fcast.columns;
			cont.col = itemcol;
			
			for(var line = 0; line < result.length; line++){
				 var t = {};
				 t.item = result[line].getValue({name:'custrecord_ftis_item',summary:'GROUP'}); 
				 t.fcast = result[line].getValue({name:'custrecord_forecast_date',summary:'GROUP'});
				 var b = {};
				 for(var col = 0; col < itemcol.length; col++){
					 var cols = itemcol[col];
					 var data = {};
					 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
					 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
					 b[cols.name] = data;
				 }
				 t.itemdetail = b;
				 cont.a.push(t);
			}
		}
		return cont;
		
    }
    
    function getDetailForecast(obj){

    	var col = [
 		      search.createColumn({
 		         name: "internalid",
 		         summary: "GROUP"
 		      }),
 		      search.createColumn({
 		         name: "custrecord_ftis_item",
 		         summary: "GROUP",
 		         sort: search.Sort.ASC
 		      }),
 		      search.createColumn({
 		         name: "custrecord_ftis_cus_soh",
 		         summary: "GROUP"
 		      }),
 		      search.createColumn({
 		         name: "custrecord_forecast_ass",
 		         summary: "GROUP"
 		      }),
 		     search.createColumn({
 		         name: "custrecord_forecast_ms",
 		         summary: "GROUP"
 		      }),
 		     search.createColumn({
 		         name: "custrecord_forecast_tbs",
 		         summary: "GROUP"
 		      }),
 		     search.createColumn({
 		         name: "custrecord_forecast_date",
 		         summary: "GROUP"
 		      }),
 		      search.createColumn({
 		         name: "formulanumeric",
 		         summary: "MAX",
 		         formula: "CASE WHEN TO_DATE({custrecord_iw_parent.custrecord_ft_iw_month}, 'MMYYYY') BETWEEN TRUNC({today},'month') AND LAST_DAY({today}) THEN {custrecord_iw_parent.custrecord_ft_iw_qty} ELSE 0 END"
 		      }),
 		     search.createColumn({
 		         name: "formulanumericnoweek",
 		         summary: "SUM",
 		         formula: "CASE WHEN TO_DATE({custrecord_iw_parent.custrecord_ft_iw_month}, 'MMYYYY') BETWEEN TRUNC(ADD_MONTHS({today},0),'month') AND LAST_DAY(ADD_MONTHS({today},2)) THEN {custrecord_iw_parent.custrecord_ft_iw_qty} ELSE 0 END"
     		    })
  		   ];

     	for(var x = 0; x < 7; x++){
     		
     		var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();
     		var scol = search.createColumn({
 	   	         name: "formulanumericfo"+ltf,
 	   	         summary: "MAX",
 	   	         formula: "CASE WHEN TO_DATE({custrecord_iw_parent.custrecord_ft_iw_month}, 'MMYYYY') BETWEEN TRUNC(ADD_MONTHS({today},"+x+"),'month') AND LAST_DAY(ADD_MONTHS({today},"+x+")) THEN {custrecord_iw_parent.custrecord_ft_iw_qty} ELSE 0 END"
     		     });     		
     		col.push(scol);
     	}
     	
     	var transactionSearchObj = search.create({
 		   type: "customrecord_ftis_item_demand_plan",
 		   filters: [
 		      ["isinactive","is","F"], 
 		      "AND", 
 		      ["custrecord_ftis_item.isinactive","is","F"],
 		      "AND", 
 		      ["internalid","anyof",obj.fcast]
 		   ],
 		   columns: col
 		});

		var resultSet = transactionSearchObj.run();	
		var a = {};
		a.result = transactionSearchObj;
		a.columns = resultSet.columns;
		return a;
    }
   
	function nvl(val,val2)
	{
	    return val == null ? val2 : val;
	}
	
   function getBrandForecast(brand){
    	var customrecord_ftis_item_demand_planSearchObj = search.create({
    		   type: "customrecord_ftis_item_demand_plan",
    		   filters: [
    		      ["custrecord_ftis_item.custitem_ftis_planner_buyer","anyof",brand],
    		      "AND",
    		      ["isinactive","is","F"]
    		   ],
    		   columns: [
    		      search.createColumn({
    		         name: "custrecord_forecast_date",
    		         summary: "MAX"
    		      }),
    		      search.createColumn({	
    		         name: "created",
    		         summary: "MAX"
    		      }),
    		      search.createColumn({
    		         name: "custrecord_ftis_item",
    		         summary: "GROUP"
    		      }),
    		      search.createColumn({
    		         name: "internalid",
    		         summary: "MAX"
    		      })
    		   ]
    		});
    	var res = {};
    	res.cont = [];
    	res.fcast = [];
		customrecord_ftis_item_demand_planSearchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			var data = {};
			data.item = result.getValue({name:'custrecord_ftis_item',summary:'GROUP'});
			data.forecast = result.getValue({name:'internalid',summary:'MAX'});
			res.cont.push(data);
			res.fcast.push(data.forecast);
			return true;
		});
		
		return res;
    		
    }
    
    
	function _mergeFORE(data,idata){
    	for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var exist = false;
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				if(db == da){
					exist = true;					
					for (var key in idata[x].itemdetail) {
						data[y].itemdetail[key] = idata[x].itemdetail[key];
					}	
				}	
			}
			if(!exist){
				data[y].itemdetail["formulanumericppo"] = {'value':0,'text':"0"};
				data[y].itemdetail["formulanumericpso"] = {'value':0,'text':"0"};
				for(var x = 0; x < 7; x++){			    		
		    		var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();
		    		data[y].itemdetail["formulanumericso"+ltf] = {'value':0,'text':"0"};
					data[y].itemdetail["formulanumericpo"+ltf] = {'value':0,'text':"0"};		    		
		    	}
			}
		}
    	return data;
		
    }
    
    function _getTran(brand){
		var item = getTran(brand);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.a = [];
			
		for(var line = 0; line < result.length; line++){
			 var t = {};
			 t.item = result[line].getValue({name:'item',summary:'GROUP'}); 
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.a.push(t);
		}
		return cont;
    }
    
    function getTran(brand){
    	
    	var col = [
     		      search.createColumn({
     		         name: "item",
     		         summary: "GROUP"
     		      }),
     		     search.createColumn({
     		         name: "formulanumericpso",
     		         summary: "SUM",
     		         formula: "DECODE({type},'Sales Order',(CASE WHEN NVL({shipdate},{custcol_ft_req_date_wh_arr}) < TRUNC({today}) THEN ({quantity}-{quantityshiprecv}) ELSE 0 END),0)"
     		      }),
     		     search.createColumn({
     		         name: "formulanumericppo",
     		         summary: "GROUP",
     		         formula: "DECODE({type},'Purchase Order',(CASE WHEN {custcol_ftis_etawarehouse} < TRUNC({today}) THEN ({quantity}-{quantityshiprecv}) ELSE 0 END),0)"
     		     })
     		   ];

    	for(var x = 0; x < 7; x++){
    		
    		var ltf = Date.parse('today + '+x+' month').toString('MMMyy').toLocaleLowerCase();
    		
    		var custformulapo = "DECODE({type},'Purchase Order',(CASE WHEN {custcol_ftis_etawarehouse} BETWEEN (TRUNC(ADD_MONTHS({today},"+x+"),'month')-1) AND LAST_DAY(ADD_MONTHS({today},"+x+")) THEN ({quantity}-{quantityshiprecv}) ELSE 0 END),0)";
    		var custformalaso = "DECODE({type},'Sales Order',(CASE WHEN (NVL({shipdate},{custcol_ft_req_date_wh_arr})) BETWEEN (TRUNC(ADD_MONTHS({today},"+x+"),'month')-1) AND LAST_DAY(ADD_MONTHS({today},"+x+")) THEN ({quantity}-{quantityshiprecv}) ELSE 0 END),0)";
    		if(x === 0){
    			custformalaso = "DECODE({type},'Sales Order',(CASE WHEN (NVL({shipdate},{custcol_ft_req_date_wh_arr})) BETWEEN ({today}-1) AND LAST_DAY({today}) THEN ({quantity}-{quantityshiprecv}) ELSE 0 END),0)" ;	   	      
   				custformulapo = "DECODE({type},'Purchase Order',(CASE WHEN {custcol_ftis_etawarehouse} BETWEEN ({today}-1) AND LAST_DAY({today}) THEN ({quantity}-{quantityshiprecv}) ELSE 0 END),0)";
    		}
			var scol = search.createColumn({
   	         name: "formulanumericso"+ltf,
   	         summary: "SUM",
   	         formula: custformalaso
   	      	});
			var pcol = search.createColumn({
  	         name: "formulanumericpo"+ltf,
  	         summary: "SUM",
  	         formula: custformulapo
  	      	});

    		col.push(scol);
    		col.push(pcol);
    		
    	}
    	//"TrnfrOrd"
    	var transactionSearchObj = search.create({
    		   type: "transaction",
    		   filters: [
    	    		      /**["type","anyof","PurchOrd","SalesOrd"], 
    	    		      "AND", 
    	    		      ["status","noneof","SalesOrd:C","SalesOrd:H","SalesOrd:G","PurchOrd:H","PurchOrd:G"], 
    	    		      "AND", 
    	    		      ["closed","is","F"], 
    	    		      "AND", 
    	    		      ["mainline","is","F"], 
    	    		      "AND", 
    	    		      ["taxline","is","F"],
    	    		      "AND", 
    	    		      ["formulanumeric: {quantity}-{quantityshiprecv}","greaterthan","0"],
    	    		      "AND", 
    	    		      ["mainname","noneof","1565","939","161","162"], 
    	    		      "AND", 
    	    		      ["item.custitem_ftis_brand","anyof",brand]**/
    	    		      ["type","anyof","PurchOrd","SalesOrd"], 
					      "AND", 
					      ["status","anyof","PurchOrd:B","PurchOrd:D","PurchOrd:E","SalesOrd:B","SalesOrd:D","SalesOrd:E"], 
					      "AND", 
					      ["closed","is","F"], 
					      "AND", 
					      ["mainline","is","F"], 
					      "AND", 
					      ["taxline","is","F"], 
					      "AND", 
    	    		      ["formulanumeric: {quantity}-{quantityshiprecv}","greaterthan","0"],
					      "AND",
					      ["formulanumeric: CASE WHEN {type} = 'Sales Order' THEN CASE WHEN {intercotransaction} IS NULL THEN 0 ELSE 1 END ELSE 0 END","lessthan","1"], 
					      "AND", 
					      ["item.custitem_ftis_planner_buyer","anyof",brand], 
					      "AND", 
					      ["formulanumeric: CASE WHEN {type} = 'Purchase Order' THEN CASE WHEN {createdfrom.intercotransaction} IS NULL THEN 0 ELSE 1 END ELSE 0 END","lessthan","1"]
					    	    		      
    	    		      
    	    		   ],
    		   columns: col
    		});

		var resultSet = transactionSearchObj.run();	
		var a = {};
		a.result = transactionSearchObj;
		a.columns = resultSet.columns;
		return a;

    }    
    
	function _mergePOSO(data,idata){	
		for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var exist = false;
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				if(db == da){
					exist = true;
					for (var key in idata[x].itemdetail) {
						if(key == 'formulanumericso'){
							data[y].itemdetail['formulanumericso'] = idata[x].itemdetail[key];
						}
						if(key == 'formulanumericpo'){
							data[y].itemdetail['formulanumericpo'] = idata[x].itemdetail[key];
						}
					}
				}	
			}
			if(!exist){
				data[y].itemdetail['formulanumericso'] = {'value':0,'text':"0"};
				data[y].itemdetail['formulanumericpo'] = {'value':0,'text':"0"};
			}
		}
		
		return data;
	}
	
	function _mergeSO(data,idata){

		
		for(var y = 0; y < data.length; y++){
			var da = data[y].item;
			var daa = data[y].itemdetail.custrecord_ftis_customeritemref_custname.value;
			var exist = false;
			
			for(var x = 0; x < idata.length; x++){
				var db = idata[x].item;
				var dba = idata[x].mainname;
				if(db == da && dba == daa){		
					exist = true;
					for (var key in idata[x].itemdetail) {
						if(key == 'formulanumericiso'){
							data[y].itemdetail[key] = idata[x].itemdetail[key];
						}
					}	
				}					
			}
			if(!exist){
				data[y].itemdetail['formulanumericiso'] = {'value':0,'text':"0"};					
			}
		}
		return data;
	}
	
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
	
    
    

   
    
    function _getFtItem(brand){
		var item = getItems(brand);
		var result = getAllSearchResults(item.result);		
		var itemcol =  item.columns;
		var cont = {};
		cont.col = itemcol;
		cont.a = [];

		for(var line = 0; line < result.length; line++){
			 var t = {};
			 t.item = result[line].getValue({name:'internalid',summary:'GROUP'}); 
			 var b = {};
			 for(var col = 0; col < itemcol.length; col++){
				 var cols = itemcol[col];
				 var data = {};
				 data.value = result[line].getValue({name:cols.name,join:cols.join,summary:cols.summary});
				 data.text = result[line].getText({name:cols.name,join:cols.join,summary:cols.summary});
				 b[cols.name] = data;
			 }
			 t.itemdetail = b; 
			 cont.a.push(t);
		}
		
		return cont;
    }
    
    
    function getItems(brand){	
    	var itemSearchObj = search.create({
		   type: "item",
		   filters: [
		      ["isinactive","is","F"], 
		      "AND", 
		      ["type","anyof","InvtPart","Assembly"],
		      "AND", 
		      ["custitem_ftis_planner_buyer","anyof",brand]
		   ],
		   

		   columns: [
				  search.createColumn({
				     name: "internalid",
				     summary: "GROUP",
				     sort: search.Sort.ASC
				  }),	
				  search.createColumn({
				     name: "itemid",
				     summary: "GROUP"
				  }),
				  search.createColumn({
				     name: "custitem_ftis_brand",
				     summary: "GROUP"
				  }),
				  search.createColumn({
					     name: "custitem_ftis_planner_buyer",
					     summary: "GROUP"
					  }),
				  /**
				  search.createColumn({
					 name: 'formulanumericftsg',
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-SG',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
				     name: 'formulanumericcsg',
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'CONSIGN-SG',{locationquantityonhand},0)"
					      }),
					
				  search.createColumn({
				     name: 'formulanumericftnz',
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-NZ',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
					 name: "formulanumericftth",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-TH',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
					 name: "formulanumericvmith",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'VMI-TH',{locationquantityonhand},0)"
				  }),
				  search.createColumn({
					 name: "formulanumericbillth",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'BILL-TH',{locationquantityonhand},0)"
				  }),
				 search.createColumn({
					 name: "formulanumericitftsg",
					 summary: "MAX",
					 formula: "DECODE({inventorylocation},'FT-SG',{locationquantityintransit},0)"
				  }),
				  search.createColumn({
				     name: "formulanumericitftth",
				     summary: "MAX",
				     formula: "DECODE({inventorylocation},'FT-TH',{locationquantityintransit},0)"
				  }),**/
				  search.createColumn({
					     name: "formulanumerictotoh",
					     summary: "SUM",
					     formula: "CASE WHEN {inventorylocation} IN('FT-SG','CONSIGN-SG','FT-NZ','FT-TH','VMI-TH','BILL-TH','WIP','DROPSHIP-SG','FT-AU') THEN {locationquantityonhand} ELSE 0 END"
					  })
				  
		   ]
		});
		var resultSet = itemSearchObj.run();	
		
		var a = {};
		a.result =itemSearchObj;
		a.columns = resultSet.columns;
		return a;
    }
    
   
    function getAllSearchResults(search) {
    	var searchResultSet = search.run();
        var startIdx = 0;
        var endIdx = 1000;
        var allSearchResults = [];
        do {
        	 var searchResults = searchResultSet.getRange({
                 start : startIdx,
                 end : endIdx
             }); 
            if (searchResults) {
            	allSearchResults = allSearchResults.concat(searchResults);
                startIdx += 1000;
                endIdx += 1000;
            }
        }
        while(searchResults && searchResults.length > 0);
        return allSearchResults;
    }
    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
    var clientCode  = 'run(); ' + function run() {
        console.log('Running client code');
        
        //*********** GLOBAL VARIABLES ***********
        $ = NS.jQuery;
        DEPLOYMENT_URL = '$DEPLOYMENT_URL$'; 
        THISURL = $(location).attr('href');
        
        //*********** After DOM loads run this: ***********
        $(function() {
            injectHeaderScripts(); //Loads Libraries that will be placed on header (Optional)
            $(window).bind('load', injectHTML); //Replaces Suitelet's body with custom HTML once the window has fully loaded(Required)
            waitForLibraries(['swal'], runCustomFunctions); //Runs additional logic after required libraries have loaded (Optional)
        });
        
        return;
        //*********** HELPER FUNCTIONS ***********
        
        
        /**
         * Loads Libraries that will be placed on header (Optional) 
         */
        function injectHeaderScripts(){
            console.log('loadHeaderLibraries START');
           // loadjscssfile( "https://code.jquery.com/jquery-1.11.3.min.js" );
            //loadjscssfile( "https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js");
            //loadjscssfile("https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css");
            //loadjscssfile("https://unpkg.com/sweetalert/dist/sweetalert.min.js"); 
            //loadjscssfile("https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.js");
            //loadjscssfile("https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.css");
            loadjscssfile("/core/media/media.nl?id=33504&c=240300&h=4f450687e259df2f2873&_xt=.js");
            loadjscssfile("/core/media/media.nl?id=33404&c=240300&h=8572f7b58c66933ea932&_xt=.css");
            loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css");
            loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css");
            loadjscssfile( "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" );

            console.log('loadHeaderLibraries END'); 
     
            //*********** HELPER FUNCTION ***********
             function loadjscssfile(filename) {
                var filetype = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
                if (filetype == "js") { //if filename is a external JavaScript file
                    var fileref = document.createElement('script');
                    fileref.setAttribute("type", "text/javascript");
                    fileref.setAttribute("src", filename);
                } else if (filetype == "css") { //if filename is an external CSS file
                    var fileref = document.createElement("link");
                    fileref.setAttribute("rel", "stylesheet");
                    fileref.setAttribute("type", "text/css");
                    fileref.setAttribute("href", filename);
                }
                if (typeof fileref != "undefined"){
                    document.getElementsByTagName("head")[0].appendChild(fileref);
                }
                console.log(filename + ' plugin loaded');
            }
        }
     
        function runCustomFunctions() {
            console.log('clientFunctions START');
            var DEPLOYMENT_URL = '$DEPLOYMENT_URL$';
           // swal(nlapiGetContext().name.split(' ').shift(), 'NOISE ONE!', "success");
        }
        
        function waitForLibraries(libraries, functionToRun){
            var loadedLibraries = 0;
            for (var i in libraries) {
                var library = libraries[i];
                if(eval('typeof ' + library)!='undefined'){
                    loadedLibraries++;
                }
            }
            
            window.setTimeout(function(){
                if(loadedLibraries != libraries.length){
                    waitForLibraries(libraries, functionToRun);
                } else{
                    console.log(library + ' loaded');
                    functionToRun();
                }
              },500);
        }
        
        function injectHTML(){
            var html = ' $PAGEBODY$ '; //This string will be replaced by the Suitelet
            
            //jQuery("#main_form")[0].outerHTML = html;
        }
    };
    
    function getInjectableForm(){
    	var f = 'function executeOnClick(item){ console.log(item); var url ="/app/site/hosting/scriptlet.nl?script=1257&deploy=1&item="+item.id; nlExtOpenWindow(url,"STOCK BALANCE "+item.value,"800"); }';//window.open(url,"_blank"); //nlExtOpenWindow(url,"STOCK BALANCE "+item.value,"1000","700");
        var bodyAreaField = FORM.addField({
            id : 'custpage_bodyareafield',
            type : serverWidget.FieldType.INLINEHTML,
            label : 'Body Area Field'
        });
     
        //*********** Prepare HTML and scripts to Inject ***********
        //var body = getBody();
        var body = '';
        clientCode = clientCode.replace('$DEPLOYMENT_URL$', DEPLOYMENT_URL);
        var base64ClientCode = toBase64(clientCode);
        var scriptToInject = 'console.log(\'Added bottom script element\');';
        scriptToInject += "eval( atob(\'" + base64ClientCode + "\') );";
     
        //*********** Injecting HTML and scripts into an the Body Area Field ***********
        bodyAreaField.defaultValue = '<script>var script = document.createElement(\'script\');script.setAttribute(\'type\', \'text/javascript\');script.appendChild(document.createTextNode(\"' + scriptToInject + '\" ));document.body.appendChild(script);  '+f+' </script>';
       // return form;
    }
    function getBody(){
        return "<div style='width:100%' class='container-fluid'> " +
            //Replace with your HTML
            " <div class='container theme-showcase' role='main'> <div class='jumbotron'> <h1>Theme example</h1> <p>This is a template showcasing the optional theme stylesheet included in Bootstrap. Use it as a starting point to create something more unique by building on or modifying it.</p></div><div class='page-header'> <h1>Buttons</h1> </div><p> <button type='button' class='btn btn-lg btn-default'>Default</button> <button type='button' class='btn btn-lg btn-primary'>Primary</button> <button type='button' class='btn btn-lg btn-success'>Success</button> <button type='button' class='btn btn-lg btn-info'>Info</button> <button type='button' class='btn btn-lg btn-warning'>Warning</button> <button type='button' class='btn btn-lg btn-danger'>Danger</button> <button type='button' class='btn btn-lg btn-link'>Link</button> </p><p> <button type='button' class='btn btn-default'>Default</button> <button type='button' class='btn btn-primary'>Primary</button> <button type='button' class='btn btn-success'>Success</button> <button type='button' class='btn btn-info'>Info</button> <button type='button' class='btn btn-warning'>Warning</button> <button type='button' class='btn btn-danger'>Danger</button> <button type='button' class='btn btn-link'>Link</button> </p><p> <button type='button' class='btn btn-sm btn-default'>Default</button> <button type='button' class='btn btn-sm btn-primary'>Primary</button> <button type='button' class='btn btn-sm btn-success'>Success</button> <button type='button' class='btn btn-sm btn-info'>Info</button> <button type='button' class='btn btn-sm btn-warning'>Warning</button> <button type='button' class='btn btn-sm btn-danger'>Danger</button> <button type='button' class='btn btn-sm btn-link'>Link</button> </p><p> <button type='button' class='btn btn-xs btn-default'>Default</button> <button type='button' class='btn btn-xs btn-primary'>Primary</button> <button type='button' class='btn btn-xs btn-success'>Success</button> <button type='button' class='btn btn-xs btn-info'>Info</button> <button type='button' class='btn btn-xs btn-warning'>Warning</button> <button type='button' class='btn btn-xs btn-danger'>Danger</button> <button type='button' class='btn btn-xs btn-link'>Link</button> </p><div class='page-header'> <h1>Tables</h1> </div><div class='row'> <div class='col-md-6'> <table class='table'> <thead> <tr> <th>#</th> <th>First Name</th> <th>Last Name</th> <th>Username</th> </tr></thead> <tbody> <tr> <td>1</td><td>Mark</td><td>Otto</td><td>@mdo</td></tr><tr> <td>2</td><td>Jacob</td><td>Thornton</td><td>@fat</td></tr><tr> <td>3</td><td>Larry</td><td>the Bird</td><td>@twitter</td></tr></tbody> </table> </div><div class='col-md-6'> <table class='table table-striped'> <thead> <tr> <th>#</th> <th>First Name</th> <th>Last Name</th> <th>Username</th> </tr></thead> <tbody> <tr> <td>1</td><td>Mark</td><td>Otto</td><td>@mdo</td></tr><tr> <td>2</td><td>Jacob</td><td>Thornton</td><td>@fat</td></tr><tr> <td>3</td><td>Larry</td><td>the Bird</td><td>@twitter</td></tr></tbody> </table> </div></div><div class='row'> <div class='col-md-6'> <table class='table table-bordered'> <thead> <tr> <th>#</th> <th>First Name</th> <th>Last Name</th> <th>Username</th> </tr></thead> <tbody> <tr> <td rowspan='2'>1</td><td>Mark</td><td>Otto</td><td>@mdo</td></tr><tr> <td>Mark</td><td>Otto</td><td>@TwBootstrap</td></tr><tr> <td>2</td><td>Jacob</td><td>Thornton</td><td>@fat</td></tr><tr> <td>3</td><td colspan='2'>Larry the Bird</td><td>@twitter</td></tr></tbody> </table> </div><div class='col-md-6'> <table class='table table-condensed'> <thead> <tr> <th>#</th> <th>First Name</th> <th>Last Name</th> <th>Username</th> </tr></thead> <tbody> <tr> <td>1</td><td>Mark</td><td>Otto</td><td>@mdo</td></tr><tr> <td>2</td><td>Jacob</td><td>Thornton</td><td>@fat</td></tr><tr> <td>3</td><td colspan='2'>Larry the Bird</td><td>@twitter</td></tr></tbody> </table> </div></div><div class='page-header'> <h1>Thumbnails</h1> </div><img data-src='holder.js/200x200' class='img-thumbnail' alt='A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera'> <div class='page-header'> <h1>Labels</h1> </div><h1> <span class='label label-default'>Default</span> <span class='label label-primary'>Primary</span> <span class='label label-success'>Success</span> <span class='label label-info'>Info</span> <span class='label label-warning'>Warning</span> <span class='label label-danger'>Danger</span> </h1> <h2> <span class='label label-default'>Default</span> <span class='label label-primary'>Primary</span> <span class='label label-success'>Success</span> <span class='label label-info'>Info</span> <span class='label label-warning'>Warning</span> <span class='label label-danger'>Danger</span> </h2> <h3> <span class='label label-default'>Default</span> <span class='label label-primary'>Primary</span> <span class='label label-success'>Success</span> <span class='label label-info'>Info</span> <span class='label label-warning'>Warning</span> <span class='label label-danger'>Danger</span> </h3> <h4> <span class='label label-default'>Default</span> <span class='label label-primary'>Primary</span> <span class='label label-success'>Success</span> <span class='label label-info'>Info</span> <span class='label label-warning'>Warning</span> <span class='label label-danger'>Danger</span> </h4> <h5> <span class='label label-default'>Default</span> <span class='label label-primary'>Primary</span> <span class='label label-success'>Success</span> <span class='label label-info'>Info</span> <span class='label label-warning'>Warning</span> <span class='label label-danger'>Danger</span> </h5> <h6> <span class='label label-default'>Default</span> <span class='label label-primary'>Primary</span> <span class='label label-success'>Success</span> <span class='label label-info'>Info</span> <span class='label label-warning'>Warning</span> <span class='label label-danger'>Danger</span> </h6> <p> <span class='label label-default'>Default</span> <span class='label label-primary'>Primary</span> <span class='label label-success'>Success</span> <span class='label label-info'>Info</span> <span class='label label-warning'>Warning</span> <span class='label label-danger'>Danger</span> </p><div class='page-header'> <h1>Badges</h1> </div><p> <a href='#'>Inbox <span class='badge'>42</span></a> </p><ul class='nav nav-pills' role='tablist'> <li role='presentation' class='active'><a href='#'>Home <span class='badge'>42</span></a></li><li role='presentation'><a href='#'>Profile</a></li><li role='presentation'><a href='#'>Messages <span class='badge'>3</span></a></li></ul> <div class='page-header'> <h1>Dropdown menus</h1> </div><div class='dropdown theme-dropdown clearfix'> <a id='dropdownMenu1' href='#' class='sr-only dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Dropdown <span class='caret'></span></a> <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'> <li class='active'><a href='#'>Action</a></li><li><a href='#'>Another action</a></li><li><a href='#'>Something else here</a></li><li role='separator' class='divider'></li><li><a href='#'>Separated link</a></li></ul> </div><div class='page-header'> <h1>Navs</h1> </div><ul class='nav nav-tabs' role='tablist'> <li role='presentation' class='active'><a href='#'>Home</a></li><li role='presentation'><a href='#'>Profile</a></li><li role='presentation'><a href='#'>Messages</a></li></ul> <ul class='nav nav-pills' role='tablist'> <li role='presentation' class='active'><a href='#'>Home</a></li><li role='presentation'><a href='#'>Profile</a></li><li role='presentation'><a href='#'>Messages</a></li></ul> <div class='page-header'> <h1>Navbars</h1> </div><nav class='navbar navbar-default'> <div class='container'> <div class='navbar-header'> <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='.navbar-collapse'> <span class='sr-only'>Toggle navigation</span> <span class='icon-bar'></span> <span class='icon-bar'></span> <span class='icon-bar'></span> </button> <a class='navbar-brand' href='#'>Project name</a> </div><div class='navbar-collapse collapse'> <ul class='nav navbar-nav'> <li class='active'><a href='#'>Home</a></li><li><a href='#about'>About</a></li><li><a href='#contact'>Contact</a></li><li class='dropdown'> <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Dropdown <span class='caret'></span></a> <ul class='dropdown-menu'> <li><a href='#'>Action</a></li><li><a href='#'>Another action</a></li><li><a href='#'>Something else here</a></li><li role='separator' class='divider'></li><li class='dropdown-header'>Nav header</li><li><a href='#'>Separated link</a></li><li><a href='#'>One more separated link</a></li></ul> </li></ul> </div></div></nav> <nav class='navbar navbar-inverse'> <div class='container'> <div class='navbar-header'> <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='.navbar-collapse'> <span class='sr-only'>Toggle navigation</span> <span class='icon-bar'></span> <span class='icon-bar'></span> <span class='icon-bar'></span> </button> <a class='navbar-brand' href='#'>Project name</a> </div><div class='navbar-collapse collapse'> <ul class='nav navbar-nav'> <li class='active'><a href='#'>Home</a></li><li><a href='#about'>About</a></li><li><a href='#contact'>Contact</a></li><li class='dropdown'> <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Dropdown <span class='caret'></span></a> <ul class='dropdown-menu'> <li><a href='#'>Action</a></li><li><a href='#'>Another action</a></li><li><a href='#'>Something else here</a></li><li role='separator' class='divider'></li><li class='dropdown-header'>Nav header</li><li><a href='#'>Separated link</a></li><li><a href='#'>One more separated link</a></li></ul> </li></ul> </div></div></nav> <div class='page-header'> <h1>Alerts</h1> </div><div class='alert alert-success' role='alert'> <strong>Well done!</strong> You successfully read this important alert message. </div><div class='alert alert-info' role='alert'> <strong>Heads up!</strong> This alert needs your attention, but it's not super important. </div><div class='alert alert-warning' role='alert'> <strong>Warning!</strong> Best check yo self, you're not looking too good. </div><div class='alert alert-danger' role='alert'> <strong>Oh snap!</strong> Change a few things up and try submitting again. </div><div class='page-header'> <h1>Progress bars</h1> </div><div class='progress'> <div class='progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 60%;'><span class='sr-only'>60% Complete</span></div></div><div class='progress'> <div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: 40%'><span class='sr-only'>40% Complete (success)</span></div></div><div class='progress'> <div class='progress-bar progress-bar-info' role='progressbar' aria-valuenow='20' aria-valuemin='0' aria-valuemax='100' style='width: 20%'><span class='sr-only'>20% Complete</span></div></div><div class='progress'> <div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 60%'><span class='sr-only'>60% Complete (warning)</span></div></div><div class='progress'> <div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='80' aria-valuemin='0' aria-valuemax='100' style='width: 80%'><span class='sr-only'>80% Complete (danger)</span></div></div><div class='progress'> <div class='progress-bar progress-bar-striped' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 60%'><span class='sr-only'>60% Complete</span></div></div><div class='progress'> <div class='progress-bar progress-bar-success' style='width: 35%'><span class='sr-only'>35% Complete (success)</span></div><div class='progress-bar progress-bar-warning' style='width: 20%'><span class='sr-only'>20% Complete (warning)</span></div><div class='progress-bar progress-bar-danger' style='width: 10%'><span class='sr-only'>10% Complete (danger)</span></div></div><div class='page-header'> <h1>List groups</h1> </div><div class='row'> <div class='col-sm-4'> <ul class='list-group'> <li class='list-group-item'>Cras justo odio</li><li class='list-group-item'>Dapibus ac facilisis in</li><li class='list-group-item'>Morbi leo risus</li><li class='list-group-item'>Porta ac consectetur ac</li><li class='list-group-item'>Vestibulum at eros</li></ul> </div><div class='col-sm-4'> <div class='list-group'> <a href='#' class='list-group-item active'> Cras justo odio </a> <a href='#' class='list-group-item'>Dapibus ac facilisis in</a> <a href='#' class='list-group-item'>Morbi leo risus</a> <a href='#' class='list-group-item'>Porta ac consectetur ac</a> <a href='#' class='list-group-item'>Vestibulum at eros</a> </div></div><div class='col-sm-4'> <div class='list-group'> <a href='#' class='list-group-item active'> <h4 class='list-group-item-heading'>List group item heading</h4> <p class='list-group-item-text'>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p></a> <a href='#' class='list-group-item'> <h4 class='list-group-item-heading'>List group item heading</h4> <p class='list-group-item-text'>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p></a> <a href='#' class='list-group-item'> <h4 class='list-group-item-heading'>List group item heading</h4> <p class='list-group-item-text'>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p></a> </div></div></div><div class='page-header'> <h1>Panels</h1> </div><div class='row'> <div class='col-sm-4'> <div class='panel panel-default'> <div class='panel-heading'> <h3 class='panel-title'>Panel title</h3> </div><div class='panel-body'> Panel content </div></div><div class='panel panel-primary'> <div class='panel-heading'> <h3 class='panel-title'>Panel title</h3> </div><div class='panel-body'> Panel content </div></div></div><div class='col-sm-4'> <div class='panel panel-success'> <div class='panel-heading'> <h3 class='panel-title'>Panel title</h3> </div><div class='panel-body'> Panel content </div></div><div class='panel panel-info'> <div class='panel-heading'> <h3 class='panel-title'>Panel title</h3> </div><div class='panel-body'> Panel content </div></div></div><div class='col-sm-4'> <div class='panel panel-warning'> <div class='panel-heading'> <h3 class='panel-title'>Panel title</h3> </div><div class='panel-body'> Panel content </div></div><div class='panel panel-danger'> <div class='panel-heading'> <h3 class='panel-title'>Panel title</h3> </div><div class='panel-body'> Panel content </div></div></div></div><div class='page-header'> <h1>Wells</h1> </div><div class='well'> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p></div><div class='page-header'> <h1>Carousel</h1> </div><div id='carousel-example-generic' class='carousel slide' data-ride='carousel'> <ol class='carousel-indicators'> <li data-target='#carousel-example-generic' data-slide-to='0' class='active'></li><li data-target='#carousel-example-generic' data-slide-to='1'></li><li data-target='#carousel-example-generic' data-slide-to='2'></li></ol> <div class='carousel-inner' role='listbox'> <div class='item active'> <img data-src='holder.js/1140x500/auto/#777:#555/text:First slide' alt='First slide'> </div><div class='item'> <img data-src='holder.js/1140x500/auto/#666:#444/text:Second slide' alt='Second slide'> </div><div class='item'> <img data-src='holder.js/1140x500/auto/#555:#333/text:Third slide' alt='Third slide'> </div></div><a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'> <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span> <span class='sr-only'>Previous</span> </a> <a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'> <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span> <span class='sr-only'>Next</span> </a> </div></div>"
            
            + "</div>";
    }
    
    
    function toBase64(stringInput){
        return ENCODEMODULE.convert({
            string: stringInput,
            inputEncoding: ENCODEMODULE.Encoding.UTF_8,
            outputEncoding: ENCODEMODULE.Encoding.BASE_64
        });
    }
    function runCustomFunctions() {
        console.log('clientFunctions START');
        var DEPLOYMENT_URL = '$DEPLOYMENT_URL$';
        swal('Good job ' + nlapiGetContext().name.split(' ').shift(), 'This code is loaded in the footer. Thanks for trying my script!', "success");
    }
    
    
    return {
        onRequest: setGo
    };
    
});