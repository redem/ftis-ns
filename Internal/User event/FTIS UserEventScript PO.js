/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record','N/runtime','N/log','N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {log} log
 * @param {search} search
 */
function(record,runtime,log,search) {
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} ctx
     * @param {Record} ctx.newRecord - New record
     * @param {string} ctx.type - Trigger type
     * @param {Form} ctx.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(ctx) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} ctx
     * @param {Record} ctx.newRecord - New record
     * @param {Record} ctx.oldRecord - Old record
     * @param {string} ctx.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(ctx) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} ctx
     * @param {Record} ctx.newRecord - New record
     * @param {Record} ctx.oldRecord - Old record
     * @param {string} ctx.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(ctx) {
    	//if(ctx.type == 'specialorder'){   
        	var nrec = ctx.newRecord;
        	var cent = nrec.getValue("entity");

        	var vendorp = nflp(search.Type.VENDOR, cent, ['printtransactions']);
        	nlog('vendorid nflp printtransactions',vendorp);       	
    	//}
    }
    
    function nlog(title_log,details_log){
        log.debug({
            title:title_log,
            details:details_log
        });
    }
    
    function nflp(type_data,id_data,fields_data){
        var data = search.lookupFields({
            type:type_data,
            id:id_data,
            columns:fields_data
        });

        return data;

    }
    
    function nload(type,id){
    	var rec = record.load({
    		type : type,
    		id : id,
    		isDynamic: true
    	});
    	
    	return rec;
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
