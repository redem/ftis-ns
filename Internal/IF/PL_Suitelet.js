/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget',
		'SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {file} file
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 */
function(file, record, render, runtime, search, serverWidget) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;

		this.runFunction = function() {
			
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			
			var params = this.request.parameters;
			if(params.ifid){
				
				var rec = nyapiLoad('itemfulfillment',params.ifid);
				var xrec = nyapiIn(rec);
				//31170 prod 
				//sandbox 30737 premium
				var xmlStr = file.load('31170');
	            var renderer = render.create();
	            renderer.templateContent = xmlStr.getContents();
	            nyapiLog('record', xrec);
	            renderer.addRecord({
	            	templateName: 'record',
				    record: record.load({
				        type: 'itemfulfillment',
				        id: params.ifid
				        })
			    });
	            renderer.addCustomDataSource({
	                format: render.DataSource.OBJECT,
	                alias: "recojsrd",
	                data: {'details':[{a:1},{a:2}]}
	                });
	            
	            
	            var newfile = renderer.renderAsPdf();
	            
	            this.response.writeFile(newfile,true); 		
	            
				
				
			}else{
				this.response.write('INVALID REQUEST');
			}
		};
		
    }
    
    
    function nyapiIn(nsObj) {
		var tos = JSON.stringify(nsObj);
		return JSON.parse(tos);
	}
    
    function nyapiLoad(type, id) {
		var rec = record.load({
			type : type,
			id : id,
			isDynamic : true
		});

		return rec;
	}
    
    function nyapiLog(title_log, details_log) {
    	log.debug({
    		title : title_log,
    		details : details_log
    	});
    }
    
    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    
    
    return {
        onRequest: setGo
    };
    
});