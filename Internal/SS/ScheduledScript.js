/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Jul 2017     noeh.canizares
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	
	
	
	var po = nlapiSearchRecord("purchaseorder",null,
			[
			   ["type","anyof","PurchOrd"], 
			   "AND", 
			   ["status","anyof","PurchOrd:D","PurchOrd:F","PurchOrd:E","PurchOrd:B","PurchOrd:A"], 
			   "AND", 
			   ["mainline","is","F"], 
			   "AND", 
			   ["matchbilltoreceipt","is","F"], 
			   "AND", 
			   ["item.type","anyof","Assembly","InvtPart"]
			], 
			[
			   new nlobjSearchColumn("internalid",null,null), 
			   new nlobjSearchColumn("type",null,null)
			]
			);
	
	var obj = [];
	for(var x = 0 ; x < po.length; x++ ){
		var comp = {};
		comp.internal = po[x].getId();
		obj.push(comp);
	}
	
	for ( var z = 0; z < obj.length; z++){
		var rec  = nlapiLoadRecord('purchaseorder',obj[z].internal);
		
		var count = rec.getLineItemCount('item');
		
		for(var x = 1 ; x <= count; x++){
			rec.selectLineItem('item',z);
			var item = rec.getCurrentLineItemValue('item','item');
			var matbil = nlapiLookupField('inventoryitem',item,'matchbilltoreceipt');
			rec.setCurrentLineItemValue('item', 'matchbilltoreceipt', matbil);
			rec.commitLineItem('item');
			
		}
		nlapiSubmitRecord(rec,false,false);
		nlapiLogExecution('Debug','rec', rec);
	}
	
	
	

}
