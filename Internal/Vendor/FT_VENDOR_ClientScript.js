/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/record'],
/**
 * @param {runtime} runtime
 * @param {record} record
 */
function(runtime, record) {
    
     /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) {
    	var sublist = scriptContext.sublistId;
    	console.log(scriptContext);
    	if(sublist == 'submachine'){
	    	var curRec = scriptContext.currentRecord;
	    	var taxitem = curRec.getCurrentSublistValue(sublist,'taxitem');
	    	if(!taxitem){
	    		alert('Tax Code Column Field is Mandatory');
	    		return false;
	    	}
    	}
    	return true;
    }

 
   
    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {
    	console.log(scriptContext);
    	var curRec = scriptContext.currentRecord;
    	var subm = curRec.getLineCount('submachine');
    	console.log(subm);
    	if(subm < 1){
	        alert('You must insert Subsidiary Tax Code under Subsudiaries Tab');
	   		return false;
    	}
    	
    	return true;
    	
    	
    	
    }

    return {
        validateLine: validateLine,
        saveRecord: saveRecord
    };
    
});
