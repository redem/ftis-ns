/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {currentRecord} currentRecord
 */
function(record, runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	
	
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
		
	

		
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
		nyapiLog('aft', scriptContext.type);
    	nyapiLog('aft runtime.envType', runtime.envType);
    	nyapiLog('aft runtime.executionContext ', runtime.executionContext);
		var orec = scriptContext.newRecord;
		
		var nrec = nyapiLoad(orec.type,orec.id);
		var lcount = nrec.getLineCount({
				sublistId: 'item'
			});
			nyapiLog('lcount',lcount);
			
		for(var x = 0 ; x < lcount; x++){

			nrec.selectLine({
				sublistId: 'item',
				line: x
			});
			
			var qty = nrec.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'quantity'
			});
			
			var rate = nrec.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'rate'
			});
			var erate = qty * rate;
			
			
			
			var it = nrec.getCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'item'
			});
			nyapiLog('it',it);
			
			nrec.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'item',
				value: it
			});
			
			nrec.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'estimatedamount',
				value: erate
			});
			
			
			nrec.commitLine({
				"sublistId": "item"
			  });
		}
		nrec.save();
		
		
    }
    
    function isObjEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    
    function nyapiLoad(type, id) {
		var rec = record.load({
			type : type,
			id : id,
			isDynamic : true
		});

		return rec;
	}
    
    function nyapiLog(title_log, details_log) {
    	log.error({
    		title : title_log,
    		details : details_log
    	});
    }
    
    
    return {
        afterSubmit: afterSubmit
    };
    
});
