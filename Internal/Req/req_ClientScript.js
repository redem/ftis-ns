var MODE;
/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
		MODE = scriptContext.mode;
		console.log(MODE);
    	if(MODE == 'copy'){
    		try{
    			var nrec = scriptContext.currentRecord;
    			var wew = nrec.getLineCount('item');
    		
    			for(var x = 1 ; x <= wew; x++){
    				window.nlapiSelectLineItem('item',x);
    				
    				  var itemid = window.nlapiGetCurrentLineItemValue('item', 'item');
    				if(itemid){
    					var fvndor = window.nlapiGetCurrentLineItemValue('item','povendor');
    					if(!fvndor){
    						var vndor = window.nlapiLookupField('item',item,'vendor');
    						if(vndor){
    							window.nlapiSetCurrentLineItemValue('item','povendor',vndor);
    						}
    					}
    				   
    				}
    				
    				window.nlapiSetCurrentLineItemValue('item','quantity',0);
    				window.nlapiSetCurrentLineItemValue('item','estimatedamount','0');
    				
    				
    				window.nlapiCommitLineItem('item');
    				
    			
    			}
    		}catch(exxx){
    			console.log(exxx);
    		}
    		
		
    	}
    }

	
    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
		var sid =scriptContext.sublistId;
		var f = scriptContext.fieldId;
		if(sid == 'item'){
			window.nlapiDisableLineItemField('item','povendor','inline');
			if(f == 'quantity'){
				window.nlapiSetCurrentLineItemValue('item','estimatedamount','0');
			
			}
		
		}

    }

	
    /**
     * Function to be executed after line is selected.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function lineInit(scriptContext) {

		if(scriptContext.sublistId == 'item'){
			//crec.getSublistField(scriptContext.sublistId ,'estimatedamount',index).isDisabled = true;
			var item = window.nlapiGetCurrentLineItemValue('item','item');
			var fvndor = window.nlapiGetCurrentLineItemValue('item','povendor');
			if(item && !fvndor){
				var vndor = window.nlapiLookupField('item',item,'vendor');
				if(vndor){
					window.nlapiSetCurrentLineItemValue('item','povendor',vndor);
				}
			}
			
			window.nlapiDisableLineItemField('item','povendor','inline');
			window.nlapiSetCurrentLineItemValue('item','estimatedamount','0');
				
		}

    }

    /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) {
		console.log(scriptContext);
		var index =window.nlapiGetCurrentLineItemIndex('item');
		var valid = true;
		
		if(scriptContext.sublistId == 'item'){
			var ftrd = window.nlapiGetCurrentLineItemValue('item','custcol_ft_req_date_wh_arr');
			
			if(!ftrd){
				alert('Please input FTRD for line item ' + index);
				valid = false;
			}
			
		}
		
	
		
		return valid;
    }
	
   
	
    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {
		var valid = true;
		var nrec = scriptContext.currentRecord;
		var wew = nrec.getLineCount('item');
	
		for(var x = 1 ; x <= wew; x++){
			window.nlapiSelectLineItem('item',x);	
			var qty = window.nlapiGetCurrentLineItemValue('item','quantity');
			
			if(qty < 1){
				valid = false;
				alert('Please input a quantity in the line item ' + x);
				break;
				
			}		
		}
		
		return valid;
    }
	

	function setTimeout(aFunction, milliseconds){
		var date = new Date();
		date.setMilliseconds(date.getMilliseconds() + milliseconds);
		while(new Date() < date){
		}
		
		return aFunction;
	}

    function nyapiLoad(type, id) {
		var rec = record.load({
			type : type,
			id : id,
			isDynamic : true
		});

		return rec;
	}
    
    function nyapiLog(title_log, details_log) {
    	log.debug({
    		title : title_log,
    		details : details_log
    	});
    }

    return {
        pageInit: pageInit,
		fieldChanged: fieldChanged,
        validateLine: validateLine,
		saveRecord: saveRecord,
		lineInit:lineInit
		
    };
    
});



