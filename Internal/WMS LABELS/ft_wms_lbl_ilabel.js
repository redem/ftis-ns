var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE;
/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record','N/email','N/runtime', 'N/search','N/url','N/https','N/task','N/encode','SuiteScripts/Internal/date.js','SuiteScripts/Internal/tools/ftis_underscore.js'],
/**
 * @param {record} record
 * @param {email} email
 * @param {runtime} runtime
 * @param {search} search
 * @param {url} url
 * @param {https} https
 * @param {task} task
 * @param {encode} encode
 */
function(record,email, runtime, search, url, https, task, encode) {
	 RT = runtime;
	 ENCODEMODULE = encode ;
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	try{
    		if(scriptContext.type == 'view'){
    			var nrec = scriptContext.newRecord;
    			var sub = nrec.getValue('subsidiary');
        		FORM = scriptContext.form;
    			if(sub == '4' || sub == '3'){
    				getInjectableForm();
    			}
        		
    		}
    	}catch(ex){
    		nyapiLog('bf error if',ex);
    	}
    }
    
    function getInjectableForm(){
    	var bodyAreaField = FORM.addField({
            id : 'custpage_bodyareafield',
            type : 'inlinehtml',
            label : 'Body Area Field'
        });
     
        //*********** Prepare HTML and scripts to Inject ***********
        //var body = getBody();
        var body = '';
        clientCode = clientCode.replace('$DEPLOYMENT_URL$', DEPLOYMENT_URL);
        var base64ClientCode = toBase64(clientCode);
        var scriptToInject = 'console.log(\'Added bottom script element\');';
        scriptToInject += "eval( atob(\'" + base64ClientCode + "\') );";
     
        //*********** Injecting HTML and scripts into an the Body Area Field ***********
        bodyAreaField.defaultValue = '<script>var script = document.createElement(\'script\');script.setAttribute(\'type\', \'text/javascript\');script.appendChild(document.createTextNode(\"' + scriptToInject + '\" ));document.body.appendChild(script);</script>';
       // return form;
    }
    
    var clientCode  = 'run(); ' + function run() {
        console.log('Running client code');
        
        //*********** GLOBAL VARIABLES ***********
        $ = NS.jQuery;
        DEPLOYMENT_URL = '$DEPLOYMENT_URL$'; 
        THISURL = $(location).attr('href');
        
        //*********** After DOM loads run this: ***********
        $(function() {
            injectHeaderScripts(); //Loads Libraries that will be placed on header (Optional)
            $(window).bind('load', injectHTML); //Replaces Suitelet's body with custom HTML once the window has fully loaded(Required)
            waitForLibraries(['swal'], runCustomFunctions); //Runs additional logic after required libraries have loaded (Optional)
        });
        
        return;
        //*********** HELPER FUNCTIONS ***********
        
        
	    /**
	     * Loads Libraries that will be placed on header (Optional) 
	     */
	    function injectHeaderScripts(){
	        console.log('loadHeaderLibraries START');
	 
	        //loadjscssfile("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");
	
	        console.log('loadHeaderLibraries END'); 
	 
	        //*********** HELPER FUNCTION ***********
	         function loadjscssfile(filename) {
	            var filetype = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
	            if (filetype == "js") { //if filename is a external JavaScript file
	                var fileref = document.createElement('script');
	                fileref.setAttribute("type", "text/javascript");
	                fileref.setAttribute("src", filename);
	            } else if (filetype == "css") { //if filename is an external CSS file
	                var fileref = document.createElement("link");
	                fileref.setAttribute("rel", "stylesheet");
	                fileref.setAttribute("type", "text/css");
	                fileref.setAttribute("href", filename);
	            }
	            if (typeof fileref != "undefined"){
	                document.getElementsByTagName("head")[0].appendChild(fileref);
	            }
	            console.log(filename + ' plugin loaded');
	        }
	    }
	    
	    function waitForLibraries(libraries, functionToRun){
	        var loadedLibraries = 0;
	        for (var i in libraries) {
	            var library = libraries[i];
	            if(eval('typeof ' + library)!='undefined'){
	                loadedLibraries++;
	            }
	        }
	        
	        window.setTimeout(function(){
	            if(loadedLibraries != libraries.length){
	                waitForLibraries(libraries, functionToRun);
	            } else{
	                console.log(library + ' loaded');
	                functionToRun();
	            }
	          },500);
	    }
	        
	    function injectHTML(){
	    	$("#markpacked").hide();	
	    }
	    function runCustomFunctions() {
            console.log('clientFunctions START');
            var DEPLOYMENT_URL = '$DEPLOYMENT_URL$';
        }
	   
    };
    function toBase64(stringInput){
        return ENCODEMODULE.convert({
            string: stringInput,
            inputEncoding: ENCODEMODULE.Encoding.UTF_8,
            outputEncoding: ENCODEMODULE.Encoding.BASE_64
        });
    }
    
    

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	
    	
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	try{
    		var subdep = ['3','4'];
        	nyapiLog('wew',runtime.executionContext);
	    	if (runtime.executionContext == 'USEREVENT' || runtime.executionContext == 'USERINTERFACE' || runtime.executionContext == 'SUITELET' || runtime.executionContext == 'SCHEDULED'){
    	    	var nrec = scriptContext.newRecord;
    	    	var subs = nrec.getValue('subsidiary');
    	    	nyapiLog('wew1',subs);
    	    	if(_.contains(subdep,subs)){
    	    		var ttype = nrec.getValue('type');
		        	var tid = nrec.getValue('id');
		        	var etype = scriptContext.type;
	    	    	if(etype == 'create' ){
	    	    		var data = {};
	    	    		var iid=nrec.getValue('id');
	    	    		var wmsloc = getItemLocation();
	    	    		
	    	    		
	    	    		var rloc = '';
            			//15 nz
            			var ordertype = nrec.getValue('ordertype');
            			
	    	    		
	    	        	switch(ttype){
	    	        		case 'itemrcpt':
	    	        			var output = url.resolveRecord({
	    	    	        	    recordType: record.Type.ITEM_RECEIPT,
	    	    	        	    recordId: iid
	    	    	        	});
	    	    	    		
	    	        			var itemcount = nrec.getLineCount({
	    	            			sublistId: 'item'
	    	            	 	});
	    	    				var recip = getRecipient(2);
	    	            		for(var x = 0 ; x<itemcount; x++){
	    	            			var item = nrec.getSublistValue({
	    	        	 			    sublistId: 'item',
	    	        	 			    fieldId: 'item',
	    	        	 			    line: x
	    	        	 			});
	    	            			
	    	            			var itemlp = search.lookupFields({
	    	                    	    type: 'item',
	    	                    	    id: item,
	    	                    	    columns: ['name']
	    	                    	});
	    	            		
	    	            			var lloc = nrec.getSublistValue({
	    	        	 			    sublistId: 'item',
	    	        	 			    fieldId: 'location',
	    	        	 			    line: x
	    	        	 			});
	    	            			rloc = lloc;
	    	            			nyapiLog('ordertype tran',ordertype);
	    	            			
	    	            			nyapiLog('location tran',lloc);
	    	            			nyapiLog('location wmsloc',wmsloc);
	    	            			
	    	            			if(_.contains(wmsloc,lloc)){
	    	            				var itemdet = nrec.getSublistSubrecord({
		    	        					sublistId: 'item',
		    	        					fieldId: 'inventorydetail',
		    	        					line: x
		    	        				});
		    	            			
		    	            			var itemrec = search.lookupFields({
		    	            			    type: 'item',
		    	            			    id: item,
		    	            			    columns: ['custitem_datecode_method', 'custitem_ftis_tcl']
		    	            			});
		    	            			var wmsdatecode = (itemrec.custitem_datecode_method.length)?itemrec.custitem_datecode_method[0].value:0;
		    	            			var tracecodelen = (itemrec.custitem_ftis_tcl)?itemrec.custitem_ftis_tcl:0;
		    	            			
		    	            			var tranid =nrec.getValue('tranid');
	    	            				
	    	            				if(wmsdatecode =='4' && tracecodelen > 0){
			    	            			nyapiLog('itemdet',itemdet);  
			    	            			var itemdetcount = itemdet.getLineCount({
			    	        					sublistId: 'inventoryassignment'
			    	        					});
			    	            			for(var z = 0 ; z<itemdetcount; z++){
			    	    		  			  	var lotno = itemdet.getSublistValue({
			    	    							sublistId: 'inventoryassignment',
			    	    							fieldId: 'receiptinventorynumber',
			    	    							line: z
			    	    						});
			    	    		  				var expdate = itemdet.getSublistValue({
			    	    							sublistId: 'inventoryassignment',
			    	    							fieldId: 'expirationdate',
			    	    							line: z
			    	    						});
			    	    		  				
			    	    		  				if(tracecodelen != lotno.length){
			    	    		  					var subj = 'Lot Sequential  Item tracecode length mismatch';
			    	    		  					var bdy = 'TraceCode length mismatch for'
			    	    		  					+'<br/> Lot no : '+lotno
			    	    		    				+'<br/> Item : '+itemlp.name
			    	    		    				+'<br/> Item Reciept : <a href='+output+'>'+tranid+'</a>';
			    	    		    				
			    	    		  					for(var y=0; y<recip.length;y++){
			    	    		  						if(lloc == recip[y].custrecord_ft_es_loc){
			    	    		  							email.send({
						    	    		  					author: '3976',
						    	    		  	                recipients:recip[y].custrecord_ft_es_rec,
						    	    		  	                subject: subj,
						    	    		  	                body: bdy
						    	    		  				});
			    	    		  						}else if(!recip[y].custrecord_ft_es_loc){
			    	    		  							email.send({
						    	    		  					author: '3976',
						    	    		  					recipients:recip[y].custrecord_ft_es_rec,
						    	    		  	                subject: subj,
						    	    		  	                body: bdy
						    	    		  				});
			    	    		  						}	
			    	    		  					}	
			    	    		  				}
			    	    		  				nyapiLog('xlotexpdate',expdate);
			    	    		  			  	nyapiLog('xlot',lotno);		  	
			    	    		  			}	            				
		    	            			}
	    	            				nyapiLog('label i generation');
	    	            				data.custscript_ft_wms_lbltype = 'Ilabel';
	    	            			}
	    	            		}
	    	        			
	    	        			break;
	    	        		case 'itemship':
	    	        			var newRecord = record.load({
	    	        			    type: record.Type.ITEM_FULFILLMENT, 
	    	        			    id: tid,
	    	        			    isDynamic: true,
	    	        			});
	    	        			var iftype = newRecord.getValue('status');
	    	        			nyapiLog('iftype',iftype);
	    	        			
	    	        			if(iftype == 'Picked'){
	    	        				
	    		        			data.custscript_ft_wms_lbltype = "OLabel";
	    	        			}
	    	        			break;	
	    	        	}
	    	        	data.custscript_ft_wms_lblid = tid;
	    	        	
	    	        	nyapiLog('request data',data);

	    	        	nyapiLog('rloc',rloc);
	    	        	
	    	        	if(data.custscript_ft_wms_lbltype){	            			
	    	        		if(ordertype != 'TrnfrOrd' || rloc != '15'){
	    	        			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
		        	        	scriptTask.scriptId = 1624;//sandbox 1616 1624
		        	        	scriptTask.deploymentId = 'customdeploy_ft_wms_core_sc';
		        	        	scriptTask.params = data;
		        	        	var scriptTaskId = scriptTask.submit();
		        	        	nyapiLog('request',scriptTaskId);  
	    	        		}
	    	        	}
	    	        }
    	    	}
    		}
    	}catch(exx){
    		nyapiLog('error label generation',exx);
    	}
    	
    }
    
    
    function createILabel(newRecord){
    		
    	  var itemcount = newRecord.getLineCount({
    		    sublistId: 'item'
    	  });
    	  
    	  for(var x = 0 ; x<itemcount; x++){
    		  var item = newRecord.getSublistValue({
    			    sublistId: 'item',
    			    fieldId: 'item',
    			    line: x
    			});
    		  
    		  var itemdet = newRecord.getSublistSubrecord({
    			    sublistId: 'item',
    			    fieldId: 'inventorydetail',
    			    line: x
    		  });
    		  nyapiLog('itemdet',itemdet);
    		  
    		  var itemdetcount = itemdet.getLineCount({
    			  sublistId: 'inventoryassignment'
    		  });
    		  
    		  var qtyid = getItemDimension(item);
    		  nyapiLog('qtyid',qtyid);
    		  
    		  for(var z = 0 ; z<itemdetcount; z++){
					var expqty = itemdet.getSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'quantity',
						line: z
					});
  
					var caseqty = parseFloat(expqty) / parseFloat(qtyid);
					if (caseqty > 0) {
						caseqty = Math.ceil(caseqty);
					}
					var totalqty = caseqty.toFixed();
    				if (totalqty == 0) {
    					totalqty = 1;
    				}
        		  
					var totalcount = totalqty;
					var tempexpqty = expqty;
					var count = 0;
					var lqty = 0;
					
					for (var i = 0; i < totalcount; i++) {
						count++;
						nyapiLog('tempexpqty',tempexpqty);
						nyapiLog('qtyid',qtyid);
						var rec = record.create({
						     type: 'customrecord_wmsse_ext_labelprinting',
						     isDynamic: true
						});
						if(tempexpqty < qtyid){
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: tempexpqty}); //qty / moq per case
						}else{
							tempexpqty = parseFloat(tempexpqty) - parseFloat(qtyid);
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: qtyid}); //qty / moq per case
						}
						
					
						var irid = newRecord.getValue('tranid');	
						var irids = newRecord.getValue('id');
						nyapiLog('internalid',irids);
						rec.setValue({fieldId: 'name', value: irid});
						rec.setValue({fieldId: 'custrecord_wmsse_label_labeltype', value: 'Ilabel'});
						rec.setValue({fieldId: 'custrecord_wmsse_ext_template', value: 'Ilabel'});
						var cft= newRecord.getText('createdfrom');
						rec.setValue({fieldId: 'custrecord_wmsse_label_order', value: cft});
						var location = newRecord.getValue('location');
						rec.setValue({fieldId: 'custrecord_wmsse_ext_location', value: location});//location 16 -sg
						// rec.setValue({fieldId: 'custrecord_wmsse_label_custom6', value: 52});//remove
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom1', value: item}); //item
						var custom10 = count + " of " + totalcount;
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom10', value: custom10});//number of label   
						var lotno = itemdet.getSublistValue({
							sublistId: 'inventoryassignment',
							fieldId: 'receiptinventorynumber',
							line: z
						});
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom2', value: lotno}); //lotno
						var rdatecode = itemdet.getSublistValue({
							sublistId: 'inventoryassignment',
							fieldId: 'expirationdate',
							line: z
						});
						var datecode = rdatecode.toString('d/M/yyyy');
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom3', value: datecode}); //expiry date
						rec.setValue({fieldId: 'custrecord_wmsse_label_printoption', value: true});
						
						rec.save(true);
						nyapiLog('created',rec);
						
					}
    		  }
    	  }
    	
    	 
    	  
    }
    
    function createOLabel(newRecord){
    	 var itemcount = newRecord.getLineCount({
 		    sublistId: 'item'
 	  });
 	  
 	  for(var x = 0 ; x<itemcount; x++){
 		  var item = newRecord.getSublistValue({
 			    sublistId: 'item',
 			    fieldId: 'item',
 			    line: x
 			});
 		 var itemname = newRecord.getSublistText({
			    sublistId: 'item',
			    fieldId: 'itemname',
			    line: x
			});
 		 var custpo = newRecord.getSublistText({
			    sublistId: 'item',
			    fieldId: 'custcol_ftis_cust_po',
			    line: x
			});
 		 var cpn = newRecord.getSublistText({
			    sublistId: 'item',
			    fieldId: 'custcol_ftis_cpn',
			    line: x
			});
		  
 		 
 		  
 		  var itemdet = newRecord.getSublistSubrecord({
 			    sublistId: 'item',
 			    fieldId: 'inventorydetail',
 			    line: x
 		  });
 		  nyapiLog('itemdet',itemdet);
 		  
 		  var itemdetcount = itemdet.getLineCount({
 			  sublistId: 'inventoryassignment'
 		  });
 		  
 		  var qtyid = getItemDimension(item);
 		  nyapiLog('qtyid',qtyid);
 		  
 		  for(var z = 0 ; z < itemdetcount; z++){
 			  
					var expqty = itemdet.getSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'quantity',
						line: z
					});

					var caseqty = parseFloat(expqty) / parseFloat(qtyid);
					if (caseqty > 0) {
						caseqty = Math.ceil(caseqty);
					}
					var totalqty = caseqty.toFixed();
	 				if (totalqty == 0) {
	 					totalqty = 1;
	 				}
     		  
					var totalcount = totalqty;
					var tempexpqty = expqty;
					var count = 0;
					for (var i = 0; i < totalcount; i++) {
						count++;
						nyapiLog('tempexpqty',tempexpqty);
						nyapiLog('qtyid',qtyid);
						var rec = record.create({
						     type: 'customrecord_wmsse_ext_labelprinting',
						     isDynamic: true
						});
						
						if(tempexpqty < qtyid){
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom5', value: tempexpqty}); //qty / moq per case
						}else{
							tempexpqty = parseFloat(tempexpqty) - parseFloat(qtyid);
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom5', value: qtyid}); //qty / moq per case
						}
						
						
						var soid = newRecord.getText('createdfrom');
						rec.setValue({fieldId: 'name', value: soid});
						rec.setValue({fieldId: 'custrecord_wmsse_label_order', value: soid});
						
						var tdate = newRecord.getValue('trandate');
						var datecode = tdate.toString('d/M/yyyy');
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: datecode});
						
						//NA
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom7', value: ''});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference2', value: ''});
						
						
						var shpadresee = newRecord.getValue('shipattention');
						rec.setValue({fieldId: 'custrecord_wmsse_label_shipaddressee', value: shpadresee});
						
						var loc = newRecord.getValue('location');
						rec.setValue({fieldId: 'custrecord_label_ext_location', value: loc});
						rec.setValue({fieldId: 'custrecord_label_template', value: 'OLabel'});					
						rec.setValue({fieldId: 'custrecord_label_labeltype', value: 'OLabel'});
						
					
						var lotno = itemdet.getSublistValue({
							sublistId: 'inventoryassignment',
							fieldId: 'receiptinventorynumber',
							line: z
						});
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom6', value: lotno});
						var custom8 = count + " of " + totalcount;//1 of 1
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom8', value: custom8});

						rec.setValue({fieldId: 'custrecord_wmsse_external_item', value: itemname});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference3', value: custpo});
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom10', value: cpn});
						rec.save(true);
						nyapiLog('olabel',rec);
						var scriptObj = runtime.getCurrentScript();
						nyapiLog("Remaining governance units: ", + scriptObj.getRemainingUsage());
					}
 		  }
 	  }
 	
 	 
    }
    
    function getItemDimension(item){
    	var itds = search.create({
    		   type: "customrecord_ebiznet_skudims",
    		   filters: [
    		      ["custrecord_ebizitemdims","anyof",item], 
    		      "AND", 
    		      ["isinactive","is","F"], 
    		      "AND", 
    		      ["custrecord_ebizuomskudim","anyof","2"]
    		   ],
    		   columns: [
    		      "custrecord_ebizitemdims",
    		      "custrecord_ebizuomskudim",
    		      "custrecord_ebizqty"
    		   ]
    		});
    		var qty = '';
    		itds.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			qty = result.getValue('custrecord_ebizqty');
    		   return true;
    		});
    		return qty;
    }
    
    function getRecipient(met){
    	var cont = [];
    	var searchObj = search.create({
    		   type: "customrecord_ft_email_setup",
    		   filters: [
    		      ["custrecord_ft_es_mod","anyof",met],
    		      "AND", 
    		      ["isinactive","is","F"]
    		   ],
    		   columns: [
    		             "internalid",
    		      "custrecord_ft_es_loc",
    		      "custrecord_ft_es_mod",
    		      "custrecord_ft_es_rec"
    		   ]
    		});

		searchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
		   var data = {};
		   data.custrecord_ft_es_loc = result.getValue('custrecord_ft_es_loc');
		   data.custrecord_ft_es_mod = result.getValue('custrecord_ft_es_mod');
		   data.custrecord_ft_es_rec = result.getValue('custrecord_ft_es_rec');
		   cont.push(data);
		   return true;
		});
		
		return cont;
    } 
    
    function nyapiLog(title_log, details_log) {
    	log.error({
    		title : title_log,
    		details : details_log
    	});
    }
    
    function getItemLocation(){
    		var loca = search.create({
    		   type: "location",
    		   filters: [
    		      ["custrecord_wmsse_make_wh_site","is","T"]
    		   ],
    		   columns: [
    		      search.createColumn({
    		         name: "name",
    		         sort: search.Sort.ASC
    		      }),
    		      "internalid",
    		      "custrecord_ebiz_whlocationtype"
    		   ]
    		});
    		var cont = [];
    		loca.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			cont.push(result.getValue('internalid'));
    		   return true;
    		});
    		return cont;
    }
    

    return {
    	beforeLoad:beforeLoad,
    	afterSubmit: afterSubmit
    };
    
});