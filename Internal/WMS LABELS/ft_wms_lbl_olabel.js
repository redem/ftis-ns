/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search','SuiteScripts/Internal/date.js'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	var newRecord = scriptContext.newRecord;
    	nyapiLog('new Record',newRecord);
    	//addvalidation
    	//if location is wms
    	
    	try{
    		createLabel(newRecord);
    	}catch(exx){
    		nyapiLog('error label generation',exx);
    	}
    }
    
    
    function createLabel(newRecord){
    		
    	  var itemcount = newRecord.getLineCount({
    		    sublistId: 'item'
    	  });
    	  
    	  for(var x = 0 ; x<itemcount; x++){
    		  var item = newRecord.getSublistValue({
    			    sublistId: 'item',
    			    fieldId: 'item',
    			    line: x
    			});
    		  
    		  var itemdet = newRecord.getSublistSubrecord({
    			    sublistId: 'item',
    			    fieldId: 'inventorydetail',
    			    line: x
    		  });
    		  nyapiLog('itemdet',itemdet);
    		  
    		  var itemdetcount = itemdet.getLineCount({
    			  sublistId: 'inventoryassignment'
    		  });
    		  
    		  var qtyid = getItemDimension(item);
    		  nyapiLog('qtyid',qtyid);
    		  
    		  for(var z = 0 ; z<itemdetcount; z++){
					var expqty = itemdet.getSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'quantity',
						line: z
					});
  
					var caseqty = parseFloat(expqty) / parseFloat(qtyid);
					if (caseqty > 0) {
						caseqty = Math.ceil(caseqty);
					}
					var totalqty = caseqty.toFixed();
    				if (totalqty == 0) {
    					totalqty = 1;
    				}
        		  
					var totalcount = totalqty;
					var tempexpqty = expqty;
					var count = 0;
					var lqty = 0;
					for (var i = 0; i < totalcount; i++) {
						count++;
						nyapiLog('tempexpqty',tempexpqty);
						nyapiLog('qtyid',qtyid);
						var rec = record.create({
						     type: 'customrecord_wmsse_ext_labelprinting',
						     isDynamic: true
						});
						var soid = newRecord.getText('createdfrom');
						rec.setValue({fieldId: 'name', value: soid});
						rec.setValue({fieldId: 'custrecord_wmsse_label_order', value: soid});
						
						var tdate = newRecord.getValue('trandate');
						var datecode = tdate.toString('d/M/yyyy');
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: datecode});
						
						//NA
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom7', value: ''});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference2', value: ''});
						
						rec.setValue({fieldId: 'custrecord_wmsse_label_shipaddressee', value: ''});
						
						var loc = newRecord.getValue('location');
						rec.setValue({fieldId: 'custrecord_label_ext_location', value: loc});
						rec.setValue({fieldId: 'custrecord_label_template', value: 'OLabel'});					
						rec.setValue({fieldId: 'custrecord_label_labeltype', value: 'OLabel'});
						
						
						
						
						/**var expqty = itemdet.getSublistValue({
							sublistId: 'inventoryassignment',
							fieldId: 'quantity',
							line: z
						});**/
					
						var qty= '';//expqty
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom5', value: qty});
						//lotno
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom6', value: ''});
						//1 of 1
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom8', value: ''});
						//item
						rec.setValue({fieldId: 'custrecord_wmsse_external_item', value: ''});
						//customer po line item
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference3', value: ''});
						var cpn = '';//line item custcol_ftis_cpn
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom10', value: cpn});

					}
    		  }
    	  }
    	
    	 
    	  
    }
    
    function getLocation(loc){
    	var result = record.load({type:'location','id':loc,'dynamic':true);
    	result.toJSON();
    	var add = result.getSubRecord('mainaddress');	
    	var res = {};
		res.shipfromaddress = add.getValue('addr1') + " " + add.getValue('addr2');
		
		res.shipfromcity = add.getValue('city');
		res.shipfromstate = add.getValue('state');
		res.shipfromzipcode = add.getValue('zip');
		
    
    }
    
    
    function getItemDimension(item){
    	var itds = search.create({
    		   type: "customrecord_ebiznet_skudims",
    		   filters: [
    		      ["custrecord_ebizitemdims","anyof",item], 
    		      "AND", 
    		      ["isinactive","is","F"], 
    		      "AND", 
    		      ["custrecord_ebizuomskudim","anyof","2"]
    		   ],
    		   columns: [
    		      "custrecord_ebizitemdims",
    		      "custrecord_ebizuomskudim",
    		      "custrecord_ebizqty"
    		   ]
    		});
    		var qty = '';
    		itds.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			qty = result.getValue('custrecord_ebizqty');
    		   return true;
    		});
    		return qty;
    }
    
    function nyapiLog(title_log, details_log) {
    	log.error({
    		title : title_log,
    		details : details_log
    	});
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
