var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE,FORM;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/encode','N/task', 'N/https','SuiteScripts/Internal/date.js'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {encode} encode
 * @param {task} task
 * @param {https} https
 */
function(file, http, record, render, runtime, search, serverWidget, url, encode, task, https) {
    RT = runtime;
    ENCODEMODULE = encode ;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		this.scriptObj =runtime.getCurrentScript();
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var params = this.request.parameters;
			
			var form = serverWidget.createForm({
			    title : 'FTIS Label Generator <br><hr/>'
			});
			//sandbox 34754
			form.clientScriptFileId = 40640;
			
			
		
			var mess = form.addField({
			    id : 'custpage_message',
				type : serverWidget.FieldType.TEXT,
				label : 'Message Type'
			});
			mess.updateDisplayType({
			    displayType : serverWidget.FieldDisplayType.HIDDEN
			});
			
			var message = this.sessionObj.get({name:"message"});
			
			mess.defaultValue = message;
			this.sessionObj.set({name: "message", value: ""});
			
			form.addFieldGroup({
			    id : 'fieldgroupid',
			    label : 'Label Type'
			});
			var lbltype = form.addField({
			    id : 'custpage_lbltype',
				type : serverWidget.FieldType.SELECT,
				label : 'Label Type',
				container : 'fieldgroupid'
			});
			
			lbltype.addSelectOption({value : '0',text : 'Please Select type of Label'});
			lbltype.addSelectOption({value : '1',text : 'I Label Standard Pack'});
			lbltype.addSelectOption({value : '2',text : 'I Label Non Standard Pack'});
			
			lbltype.addSelectOption({value : '3',text : 'O Label Standard Pack'});
			lbltype.addSelectOption({value : '4',text : 'O Label Non Standard Pack'});
			
			//lbltype.addSelectOption({value : '5',text : 'Small Label'});
			lbltype.addSelectOption({value : '6',text : 'Shipping Mark'});
			
			
			switch(params.mn){
				case '1': //ilabel
					form.title =  'FTIS I Label Standard Pack Generator <br><hr/>';
					lbltype.defaultValue = params.mn;
					form.addSubmitButton({
					    label : 'Generate I Label'
					});
					break;
				case '2': //ilabel Non standard pack
					form.title =  'FTIS I Label Non Standard Pack Generator <br><hr/>';
					lbltype.defaultValue = params.mn;
					form.addSubmitButton({
					    label : 'Generate I Label'
					});
					break;
				case '3': //o label
					form.title =  'FTIS O Label Standard Pack Generator <br><hr/>';
					lbltype.defaultValue = params.mn;
					form.addSubmitButton({
					    label : 'Generate O Label'
					});
					
					break;
				case '4': //o label Non
					form.title =  'FTIS O Label Non Standard Pack Generator <br><hr/>';
					lbltype.defaultValue = params.mn;
					form.addSubmitButton({
					    label : 'Generate O Label'
					});
					
					break;
				case '5': //small label 
					form.title =  'FTIS Small Label Generator <br><hr/>';
					lbltype.defaultValue = params.mn;
					form.addSubmitButton({
					    label : 'Generate Small Label'
					});
					
					break;
				case '6': //shipping mark
					form.title =  'FTIS Shipping Mark Label Generator <br><hr/>';
					lbltype.defaultValue = params.mn;
					form.addFieldGroup({
					    id : 'fieldgroupid4',
					    label : 'Shipping Mark Details'
					});
					var nocart = form.addField({
						 id : 'custpage_nocart',
						type : serverWidget.FieldType.INTEGER,
						label : 'Cart #',
						container : 'fieldgroupid4'
						
					});
					nocart.isMandatory = true;
					var gwe = form.addField({
					    id : 'custpage_gwe',
						type : serverWidget.FieldType.TEXT,
						label : 'Gross Weight',
						container : 'fieldgroupid4'
					});
					gwe.isMandatory = true;
					var shpdate = form.addField({
					    id : 'custpage_shpdate',
						type : serverWidget.FieldType.DATE,
						label : 'Ship Date',
						container : 'fieldgroupid4'
					});
					shpdate.isMandatory = true;
					var shpmrk = form.addField({
					    id : 'custpage_shpmark',
						type : serverWidget.FieldType.TEXTAREA,
						label : 'Shiping Mark',
						container : 'fieldgroupid4'
					});
					var shpadd = form.addField({
					    id : 'custpage_shpadd',
						type : serverWidget.FieldType.TEXTAREA,
						label : 'Ship to',
						container : 'fieldgroupid4'
					});
					shpadd.isMandatory = true;
					
					form.addSubmitButton({
					    label : 'Generate Shipping Mark Label'
					});
					
					break;
			}		
			
			if(params.mn > 0 && params.mn < 7){
				
				var tran = form.addField({
				    id : 'custpage_lbltran',
					type : serverWidget.FieldType.SELECT,
					label : 'Transaction',
					source:'transaction',
					container : 'fieldgroupid'
				});
				
				if(params.tran){
					tran.defaultValue = params.tran;
				}
				
				if(params.mn == 2){
					var ilist = form.addSublist({
					    id : 'custpage_ilist',
					    type : serverWidget.SublistType.INLINEEDITOR,
					    label : 'Results'
					});			
					var fitem = ilist.addField({id : 'cutpage_sitem',type : serverWidget.FieldType.SELECT,label : 'Item',source: 'item'});
					fitem.isMandatory = true;

					var fnsp = ilist.addField({id : 'custpage_nssp',type : serverWidget.FieldType.FLOAT,label : 'Non Standard Pack'});
					
					var spacks = ilist.addField({id : 'custpage_ssp',type : serverWidget.FieldType.FLOAT,label : 'Standard Pack'});
				
					
					ilist.addField({id : 'custpage_sqty',type : serverWidget.FieldType.TEXT,label : 'Total Qty'});
					var fbx = ilist.addField({id : 'custpage_boxno',type : serverWidget.FieldType.TEXT,label : 'No of Box * 1 of 10'});
				
					var flotno =ilist.addField({id : 'custpage_slotno',type : serverWidget.FieldType.TEXT,label : 'Lot No'});
					flotno.isMandatory = true;
			
					var fxd = ilist.addField({id : 'custpage_sexdate',type : serverWidget.FieldType.TEXT,label : 'Expiry Date'});

					var floc = ilist.addField({id : 'custpage_sloc',type : serverWidget.FieldType.SELECT,label : 'Location',source: 'location'});
					floc.isMandatory = true;
					
					
					ilist.addButton({
					    id : 'custpage_btnmkp',
					    label : 'Make Copy',
					    functionName:'mkci'
					});
					
					if(params.tran){
						try{
							var tranRec = record.load({
				    		    type: record.Type.ITEM_RECEIPT, 
				    		    id: params.tran
				    		});
							nyapiLog('tranRec',tranRec);
							var cftran = form.addField({
							    id : 'custpage_lbltrancf',
								type : serverWidget.FieldType.TEXT,
								label : 'Created From',
								container : 'fieldgroupid'
							});
							var cft= tranRec.getText('createdfrom');
							var nc = cft.indexOf('#') + 1;
							var strlen = cft.length;
							var soid = cft.substr(nc,strlen);
							cftran.defaultValue = soid || '';
							
						
							var trandate = form.addField({
							    id : 'custpage_trandate',
								type : serverWidget.FieldType.DATE,
								label : 'Transaction Date',
								container : 'fieldgroupid'
							});
							
							trandate.defaultValue = tranRec.getValue('trandate') || ' ';
							
							
							
							var itemcount = tranRec.getLineCount({
							    sublistId: 'item'
							});
							
							var li = 0;
							for(var x=0; x< itemcount; x++){
								
								var ritem = tranRec.getSublistValue('item','item',x);
								
						
								var spack = getItemDimension(ritem);
								var rqty = tranRec.getSublistValue('item','quantity',x);
								var loc = tranRec.getSublistValue({
						   				sublistId: 'item',
						   				fieldId: 'location',
						   				line: x
						   			});
						   		var custpo = tranRec.getSublistText({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ftis_cust_po',
						   			    line: x
						   			});
						   		var  cpn= tranRec.getSublistText({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ftis_cpn',
						   			    line: x
						   			});
						   		 
						   		var custrev = tranRec.getSublistValue({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ftis_cust_rev',
						   			    line: x
						   			});
						   		 
						   		var ctnno = tranRec.getSublistValue({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ftis_carton_no',
						   			    line: x
						   			});
						   		var wmslineno = tranRec.getSublistValue({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ebizwolotlineno',
						   			    line: x
						   			});
						   		
							   	var itemdet = tranRec.getSublistSubrecord({
									    sublistId: 'item',
									    fieldId: 'inventorydetail',
									    line: x
								});
								  
								//nyapiLog('itemdetss',itemdet);
								  
								var itemdetcount = itemdet.getLineCount({
									sublistId: 'inventoryassignment'
								});
								for(var z = 0 ; z < itemdetcount; z++){
									var lotno = itemdet.getSublistText({
										sublistId: 'inventoryassignment',
										fieldId: 'receiptinventorynumber',
										line: z
									});
									var quantity = itemdet.getSublistText({
										sublistId: 'inventoryassignment',
										fieldId: 'quantity',
										line: z
									});
									var expirationdate = itemdet.getSublistText({
										sublistId: 'inventoryassignment',
										fieldId: 'expirationdate',
										line: z
									});
									
									ilist.setSublistValue({id: 'cutpage_sitem',line: li,value: ritem});
									ilist.setSublistValue({id: 'custpage_ssp',line: li,value: spack || 0});
									ilist.setSublistValue({id: 'custpage_sqty',line: li,value: quantity});
									ilist.setSublistValue({id: 'custpage_slotno',line: li,value: lotno||' '});
									ilist.setSublistValue({id: 'custpage_sexdate',line: li,value: expirationdate|| " "});
									ilist.setSublistValue({id: 'custpage_sloc',line: li,value: loc});
								
									li++;
									nyapiLog('li',li);
								}
							}						
						}catch(exx){
							nyapiLog('Olabel exx',exx);
						}
					}	
				}
							
				if(params.mn == 5){ // small ui
					var slist = form.addSublist({
					    id : 'custpage_smlist',
					    type : serverWidget.SublistType.INLINEEDITOR,
					    label : 'Results'
					});			
					slist.addField({id : 'cutpage_sitem',type : serverWidget.FieldType.SELECT,label : 'Item',source: 'item'});
					slist.addField({id : 'custpage_nsp',type : serverWidget.FieldType.TEXT,label : 'Non Standard Pack'});
					slist.addField({id : 'custpage_sweight',type : serverWidget.FieldType.TEXT,label : 'Weight'});
					slist.addField({id : 'custpage_scart',type : serverWidget.FieldType.TEXT,label : 'Carton'});
					slist.addField({id : 'custpage_ssp',type : serverWidget.FieldType.TEXT,label : 'Standard Pack'});
					slist.addField({id : 'custpage_sqty',type : serverWidget.FieldType.TEXT,label : 'Total Qty'});
					slist.addField({id : 'custpage_location',type : serverWidget.FieldType.SELECT,label : 'LOCATION',source:'location'});
					slist.addField({id : 'custpage_scpnitem',type : serverWidget.FieldType.TEXT,label : 'Cust P/N'});
					slist.addField({id : 'custpage_spnitem',type : serverWidget.FieldType.TEXT,label : 'P/N'});
					slist.addField({id : 'custpage_spo',type : serverWidget.FieldType.TEXT,label : 'PO'});
					slist.addField({id : 'custpage_scoo',type : serverWidget.FieldType.TEXT,label : 'COO'});
				
					
					slist.addButton({
					    id : 'custpage_btnmkp',
					    label : 'Make Copy',
					    functionName:'mkcs'
					});
					
					if(params.tran){
						try{
							var tranRec = record.load({
				    		    type: record.Type.ITEM_FULFILLMENT, 
				    		    id: params.tran
				    		});
							
						
							var cftran = form.addField({
							    id : 'custpage_lbltrancf',
								type : serverWidget.FieldType.TEXT,
								label : 'Created From',
								container : 'fieldgroupid'
							});
							var cft= tranRec.getText('createdfrom');
							var nc = cft.indexOf('#') + 1;
							var strlen = cft.length;
							var soid = cft.substr(nc,strlen);
							cftran.defaultValue = soid || '';
							
							var trandate = form.addField({
							    id : 'custpage_trandate',
								type : serverWidget.FieldType.DATE,
								label : 'Transaction Date',
								container : 'fieldgroupid'
							});
							
							trandate.defaultValue = tranRec.getValue('trandate') || ' ';
							
							var itemcount = tranRec.getLineCount({
							    sublistId: 'item'
							});
							
							var li = 0;
							
							
							for(var x=0; x< itemcount; x++){
								var ritem = tranRec.getSublistValue('item','item',x);
								var spack = getItemDimension(ritem);
								var rqty = tranRec.getSublistValue('item','quantity',x);
								var loc = tranRec.getSublistValue({
						   				sublistId: 'item',
						   				fieldId: 'location',
						   				line: x
						   			});
								var cpn = tranRec.getSublistValue({
					   				sublistId: 'item',
					   				fieldId: 'custcol_ftis_cpn',
					   				line: x
					   			});
								var crtno = tranRec.getSublistValue({
					   				sublistId: 'item',
					   				fieldId: 'custcol_ftis_carton_no',
					   				line: x
					   			});
								var po = tranRec.getSublistValue({
					   				sublistId: 'item',
					   				fieldId: 'custcol_ftis_cust_po',
					   				line: x
					   			});
								var coo = tranRec.getSublistValue({
					   				sublistId: 'item',
					   				fieldId: 'custcol_country_of_origin',
					   				line: x
					   			});

								var location = tranRec.getSublistValue({
					   				sublistId: 'item',
					   				fieldId: 'location',
					   				line: x
					   			});

															
								slist.setSublistValue({id: 'cutpage_sitem',line: li,value: ritem});
								slist.setSublistValue({id: 'custpage_scpnitem',line: li,value: cpn});
								slist.setSublistValue({id: 'custpage_spnitem',line: li,value: cpn});
								slist.setSublistValue({id: 'custpage_ssp',line: li,value: spack});
								slist.setSublistValue({id: 'custpage_spo',line: li,value: po|| ' '});
								slist.setSublistValue({id: 'custpage_scart',line: li,value: crtno|| ' '});
								slist.setSublistValue({id: 'custpage_sqty',line: li,value: rqty || ' '});
								slist.setSublistValue({id: 'custpage_location',line: li,value: location});
								slist.setSublistValue({id: 'custpage_scoo',line: li,value: coo || ' '});
								
								li++;
								
							}
							
							
						}catch(ex){
							nyapiLog('5 small label bf',ex);
						}
						
					}
					
				}
				
				
				if(params.mn == 4){
					
					var olist = form.addSublist({
					    id : 'custpage_olist',
					    type : serverWidget.SublistType.INLINEEDITOR,
					    label : 'Results'
					});			
					var fitem = olist.addField({id : 'cutpage_sitem',type : serverWidget.FieldType.SELECT,label : 'Item',source: 'item'});
					fitem.isMandatory = true;

					var fnsp = olist.addField({id : 'custpage_nssp',type : serverWidget.FieldType.FLOAT,label : 'Non Standard Pack'});
					
					var spacks = olist.addField({id : 'custpage_ssp',type : serverWidget.FieldType.FLOAT,label : 'Standard Pack'});
					/**spacks.updateDisplayType({
					    displayType : serverWidget.FieldDisplayType.HIDDEN
					});**/
					
					olist.addField({id : 'custpage_sqty',type : serverWidget.FieldType.TEXT,label : 'Total Qty'});
					var fbx = olist.addField({id : 'custpage_boxno',type : serverWidget.FieldType.TEXT,label : 'No of Box * 1 of 10'});
				
					var flotno =olist.addField({id : 'custpage_slotno',type : serverWidget.FieldType.TEXT,label : 'Lot No'});
					flotno.isMandatory = true;
			
					var fxd = olist.addField({id : 'custpage_sexdate',type : serverWidget.FieldType.TEXT,label : 'Expiry Date'});

					var floc = olist.addField({id : 'custpage_sloc',type : serverWidget.FieldType.SELECT,label : 'Location',source: 'location'});
					floc.isMandatory = true;
					olist.addField({id : 'custpage_scustpo',type : serverWidget.FieldType.TEXT,label : 'Customer PO'});
					olist.addField({id : 'custpage_scpn',type : serverWidget.FieldType.TEXT,label : 'CPN'});
					olist.addField({id : 'custpage_scustrev',type : serverWidget.FieldType.TEXT,label : 'Customer Revision'});
					olist.addField({id : 'custpage_swmslineno',type : serverWidget.FieldType.TEXT,label : 'WMS LINE NO'});
					olist.addField({id : 'custpage_swmsdatalocation',type : serverWidget.FieldType.TEXT,label : 'DATABASE LOCATION'});
					
					olist.addButton({
					    id : 'custpage_btnmkp',
					    label : 'Make Copy',
					    functionName:'mkc'
					});
					//window.custpage_olist_machine.copyline();
					if(params.tran){
						try{
							var tranRec = record.load({
				    		    type: record.Type.ITEM_FULFILLMENT, 
				    		    id: params.tran
				    		});
							
							var cftran = form.addField({
							    id : 'custpage_lbltrancf',
								type : serverWidget.FieldType.TEXT,
								label : 'Created From',
								container : 'fieldgroupid'
							});
							var cft= tranRec.getText('createdfrom');
							var nc = cft.indexOf('#') + 1;
							var strlen = cft.length;
							var soid = cft.substr(nc,strlen);
							cftran.defaultValue = soid || '';
							
							var fshpadr = form.addField({
							    id : 'custpage_shpadr',
								type : serverWidget.FieldType.TEXT,
								label : 'Ship Address',
								container : 'fieldgroupid'
							});
							var shpadresee = tranRec.getSubrecord({
								fieldId: 'shippingaddress'
								});
							var shpadr = shpadresee.getValue('attention');
							fshpadr.defaultValue = shpadr || ' ';
							
							var trandate = form.addField({
							    id : 'custpage_trandate',
								type : serverWidget.FieldType.DATE,
								label : 'Transaction Date',
								container : 'fieldgroupid'
							});
							
							trandate.defaultValue = tranRec.getValue('trandate') || ' ';
							
							
							
							var itemcount = tranRec.getLineCount({
							    sublistId: 'item'
							});
							
							var li = 0;
							for(var x=0; x< itemcount; x++){
								var ritem = tranRec.getSublistValue('item','item',x);
								var spack = getItemDimension(ritem);
								var rqty = tranRec.getSublistValue('item','quantity',x);
								var loc = tranRec.getSublistValue({
						   				sublistId: 'item',
						   				fieldId: 'location',
						   				line: x
						   			});
						   		var custpo = tranRec.getSublistText({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ftis_cust_po',
						   			    line: x
						   			});
						   		
						   		var dataloca = tranRec.getSublistText({
					   			    sublistId: 'item',
					   			    fieldId: 'custcol_ftis_database_location',
					   			    line: x
					   			});
					   		
						   		
						   		var  cpn= tranRec.getSublistText({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ftis_cpn',
						   			    line: x
						   			});
						   		 
						   		var custrev = tranRec.getSublistValue({
						   			    sublistId: 'item',
						   			    fieldId: 'custcol_ftis_cust_rev',
						   			    line: x
						   			});
						   		 
						   		var wmslineno = tranRec.getSublistValue({
						   			    sublistId: 'item',
						   			    fieldId: 'orderline',
						   			    line: x
						   			});
						   		
							   	var itemdet = tranRec.getSublistSubrecord({
									    sublistId: 'item',
									    fieldId: 'inventorydetail',
									    line: x
								});
								  
								nyapiLog('itemdetss',itemdet);
								  
								var itemdetcount = itemdet.getLineCount({
									sublistId: 'inventoryassignment'
								});
								for(var z = 0 ; z < itemdetcount; z++){
									var lotno = itemdet.getSublistText({
										sublistId: 'inventoryassignment',
										fieldId: 'issueinventorynumber',
										line: z
									});
									var quantity = itemdet.getSublistText({
										sublistId: 'inventoryassignment',
										fieldId: 'quantity',
										line: z
									});
									var expirationdate = itemdet.getSublistText({
										sublistId: 'inventoryassignment',
										fieldId: 'expirationdate',
										line: z
									});
									
									olist.setSublistValue({id: 'cutpage_sitem',line: li,value: ritem});
									olist.setSublistValue({id: 'custpage_ssp',line: li,value: spack || 0});
									olist.setSublistValue({id: 'custpage_sqty',line: li,value: quantity});
									olist.setSublistValue({id: 'custpage_slotno',line: li,value: lotno});
									olist.setSublistValue({id: 'custpage_sexdate',line: li,value: expirationdate|| " "});
									olist.setSublistValue({id: 'custpage_sloc',line: li,value: loc});
									olist.setSublistValue({id: 'custpage_scustpo',line: li,value: custpo || " "});
									olist.setSublistValue({id: 'custpage_scpn',line: li,value: cpn || " "});
									olist.setSublistValue({id: 'custpage_scustrev',line: li, value:custrev ||" "});
									olist.setSublistValue({id: 'custpage_swmslineno',line: li,value: wmslineno ||" "});
									olist.setSublistValue({id: 'custpage_swmsdatalocation',line: li,value: dataloca ||" "});
									
									li++;
									nyapiLog('li',li);
								}
							
								
							}
							    						
						}catch(exx){
							nyapiLog('Olabel exx',exx);
						}
					}
					
					
				}
				
				
			}		
			this.response.writePage(form);
		};
		
		
		this.POST = function(){
			try{
				
				var params = this.request.parameters;
				var res = {};
				res.message = 'Invalid Request';
				
				var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
	        	scriptTask.scriptId = 1624;//sandbox 1616 1624
	        	scriptTask.deploymentId = 'customdeploy_ft_wms_core_sc';
				
				if(params){
					//params = JSON.parse(params);
					res.body = params;
					res.message = "success";
				}
				nyapiLog('params',params);
				
				if(params.custpage_lbltype == '1'){
					var data = {};
					data.custscript_ft_wms_lbltype = 'Ilabel';
					data.custscript_ft_wms_lblid = params.custpage_lbltran;
					nyapiLog('data',data);
		        	scriptTask.params = data;
		        	var scriptTaskId = scriptTask.submit();
		        	nyapiLog('request',scriptTaskId);
		        	this.sessionObj.set({name: "message", value: "I Label Standard Pack Successfully Created"});
		        	this.response.sendRedirect({
						  type: http.RedirectType.SUITELET,
						    identifier: 'customscript_ft_wms_lbl_gen',
						    id :'customdeploy_ft_wms_lbl_gen',
						    parameters: {mn: 1}	
					});	
		        	
					
				}
				
				if(params.custpage_lbltype == '2'){
					
					var str = params.custpage_ilistdata;
					var strat = str.split('\u0002');
					nyapiLog('strat',strat);
					var cont = [];
					for(var y = 0; y<strat.length; y++){
						var sitem = strat[y].split("\u0001");	
						var data = {};
						data.itemdisplay = sitem[0];
						data.item = sitem[1];
						data.npack = sitem[2];
						data.spack	 = sitem[3];
						data.tqty = sitem[4];
						data.boxno = sitem[5];
						data.lotno = sitem[6];
						data.exdate = sitem[7];
						data.loc = sitem[8];
						
						cont.push(data);
					}
					
					nyapiLog(cont.length,cont);
					
					
					
					
					var cfrec = params.custpage_lbltrancf;
					for(var i= 0; i<cont.length; i++){
						var rec = record.create({
						     type: 'customrecord_wmsse_ext_labelprinting',
						     isDynamic: true
						});
						var cft = params.custpage_lbltran_display;
						var nc = cft.indexOf('#') + 1;
						var strlen = cft.length;
						var irid = cft.substr(nc,strlen);
						
						rec.setValue({fieldId: 'name', value: irid});
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: cont[i].npack});
						rec.setValue({fieldId: 'custrecord_wmsse_label_labeltype', value: 'Ilabel'});
						rec.setValue({fieldId: 'custrecord_wmsse_ext_template', value: 'Ilabel'});
						rec.setValue({fieldId: 'custrecord_wmsse_label_order', value: cfrec});
						var location = cont[i].loc;
						rec.setValue({fieldId: 'custrecord_wmsse_ext_location', value: location});//location 16 -sg
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom1', value: cont[i].itemdisplay}); //itemname
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom10', value: cont[i].boxno});//number of label   
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom2', value: cont[i].lotno}); //lotno
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom3', value: cont[i].exdate}); //expiry date
						
						rec.setValue({fieldId: 'custrecord_wmsse_label_receivedate', value: params.custpage_trandate});
						rec.save(true);
						nyapiLog('rec',rec);
					}
				
					
					this.sessionObj.set({name: "message", value: "I Label Non Standard Pack Successfully Created"});
		        	this.response.sendRedirect({
						  type: http.RedirectType.SUITELET,
						    identifier: 'customscript_ft_wms_lbl_gen',
						    id :'customdeploy_ft_wms_lbl_gen',
						    parameters: {mn: 2}	
					});	
		        	
					
				}
				
				if(params.custpage_lbltype == '3'){
					var data = {};
					data.custscript_ft_wms_lbltype = "OLabel";
					data.custscript_ft_wms_lblid = params.custpage_lbltran;
					nyapiLog('data',data);
		        	scriptTask.params = data;
		        	var scriptTaskId = scriptTask.submit();
		        	nyapiLog('request',scriptTaskId);
		        	this.sessionObj.set({name: "message", value: "O Label Standard Pack Successfully Created"});
		        	this.response.sendRedirect({
						  type: http.RedirectType.SUITELET,
						    identifier: 'customscript_ft_wms_lbl_gen',
						    id :'customdeploy_ft_wms_lbl_gen',
						    parameters: {mn: 3}	
					});	
		        	
				}
				
				if(params.custpage_lbltype == '4'){//OLabel
					//custpage_lbltran
					var samplecount = this.request.getLineCount({
					    group: 'custpage_olist'
					});
					nyapiLog('samplecount',samplecount);
					
					var xcont = [];
					for(var w = 0; w<samplecount; w++){
						var cutpage_sitem = this.request.getSublistValue({group: 'custpage_olist',name: 'cutpage_sitem',line: w	});
						var cutpage_sitem_display = this.request.getSublistValue({group: 'custpage_olist',name: 'cutpage_sitem_display',line: w	});
						
						var custpage_nssp = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_nssp',line: w	});
						var custpage_ssp = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_ssp',line: w	});
						var custpage_sqty = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_sqty',line: w	});
						var custpage_boxno = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_boxno',line: w	});
						var custpage_slotno = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_slotno',line: w	});
						var custpage_sexdate = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_sexdate',line: w	});						
						var custpage_sloc = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_sloc',line: w	});
						var custpage_scustpo = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_scustpo',line: w	});
						var custpage_scpn = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_scpn',line: w	});
						var custpage_scustrev = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_scustrev',line: w	});
						var custpage_swmslineno = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_swmslineno',line: w	});
						var custpage_swmsdatalocation = this.request.getSublistValue({group: 'custpage_olist',name: 'custpage_swmsdatalocation',line: w	});
						
						
						
						var data = {};
						data.cutpage_sitem_display = cutpage_sitem_display;
						data.cutpage_sitem = cutpage_sitem;
						data.custpage_nssp = custpage_nssp||'';
						data.custpage_ssp = custpage_ssp||'';
						data.custpage_sqty = custpage_sqty;
						data.custpage_boxno = custpage_boxno ||'';
						data.custpage_slotno = custpage_slotno;
						data.custpage_sexdate = custpage_sexdate;
						data.custpage_sloc = custpage_sloc;
						data.custpage_scustpo = custpage_scustpo;
						data.custpage_scpn = custpage_scpn;
						data.custpage_scustrev = custpage_scustrev;
						data.custpage_swmslineno = custpage_swmslineno||'';
						data.custpage_swmsdatalocation = custpage_swmsdatalocation||'';
	
						xcont.push(data);
						
					}
					nyapiLog('xcont',xcont);
					
				
					
					var str = params.custpage_olistdata;
					var strat = str.split('\u0002');
					//nyapiLog('strat',strat);
					var cont = [];
					
					for(var y = 0; y<strat.length; y++){
						var sitem = strat[y].split("\u0001");	
						nyapiLog('wew',sitem);
						var data = {};
						data.itemdisplay = sitem[0];
						data.item = sitem[1];
						data.npack = sitem[2];
						data.spack	 = sitem[3];
						data.tqty = sitem[4];
						
						data.boxno = sitem[5];
						data.lotno = sitem[6];
						data.exdate = sitem[7];
						
						data.loc = sitem[8];
						data.custpo = sitem[9];
						
						data.cpn = sitem[10];
						data.custrev = sitem[11];
						data.wmsline = sitem[12];
						data.dataloc = sitem[13];
						cont.push(data);
					}
					
					//nyapiLog(cont.length,cont);
					var cfts = params.custpage_lbltran_display;
					var ncs = cfts.indexOf('#')+1;
					var strlens = cfts.length;
					var irids = cfts.substr(ncs,strlens);
					
					for(var x= 0 ; x<xcont.length;x++){
						
						var rec = record.create({
						     type: 'customrecord_wmsse_ext_labelprinting',
						     isDynamic: true
						});
						//rec.setValue({fieldId: 'custrecord_wmsse_external_item', value: cont[x].itemdisplay});//itemdisplay
						rec.setValue({fieldId: 'custrecord_wmsse_external_item', value: xcont[x].cutpage_sitem_display});//itemdisplay
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom5', value: xcont[x].custpage_nssp}); //npack
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom8', value: xcont[x].custpage_boxno});//boxno
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom6', value: xcont[x].custpage_slotno}); //lotno
						rec.setValue({fieldId: 'custrecord_wmsse_ext_location', value: xcont[x].custpage_sloc});//loc
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference3', value: xcont[x].custpage_scustpo});
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom10', value: xcont[x].custpage_scpn});//cpn
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference2', value: xcont[x].custpage_scustrev});//custrev						
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom7', value: xcont[x].custpage_swmslineno});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference4', value: xcont[x].custpage_swmsdatalocation});
												
						rec.setValue({fieldId: 'name', value: irids});
						rec.setValue({fieldId: 'custrecord_wmsse_label_order', value: params.custpage_lbltrancf});//params.custpage_lbltrancf
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: params.custpage_trandate});//custpage_trandate
						rec.setValue({fieldId: 'custrecord_wmsse_label_shipaddressee', value: params.custpage_shpadr});//custpage_shpadr		
						rec.setValue({fieldId: 'custrecord_wmsse_ext_template', value: 'OLabel'});					
						rec.setValue({fieldId: 'custrecord_wmsse_label_labeltype', value: 'OLabel'});
						rec.setValue({fieldId: 'custrecord_wmsse_label_receivedate', value: params.custpage_trandate});
	
											
						rec.save(true);	
						
						nyapiLog('rec',rec);
					}
					
					var rdata = {};
					rdata.custscript_ft_wms_lbltype = "OLabel";
					rdata.tran = params.custpage_lbltran;
					
					this.sessionObj.set({name: "message", value: "O Label Non standard pack Successfully Created"});
					
					this.response.sendRedirect({
						  type: http.RedirectType.SUITELET,
						    identifier: 'customscript_ft_wms_lbl_gen',
						    id :'customdeploy_ft_wms_lbl_gen',
						    parameters: {mn: 4}	
					});	
				}
				
				
				
				
				if(params.custpage_lbltype == '6'){//shipmark
					
					var nc= (params.custpage_nocart)?params.custpage_nocart:0;
					
					for(var z = 1; z <= nc; z++ ){
						
						var rec = record.create({
						     type: 'customrecord_wmsse_ext_labelprinting',
						     isDynamic: true
						});
						
						var cfts = params.custpage_lbltran_display;
						var ncs = cfts.indexOf('#')+1;
						var strlens = cfts.length;
						var irids = cfts.substr(ncs,strlens);
						
						rec.setValue('custrecord_wmsse_label_itemdesc',params.custpage_shpadd);
						rec.setValue('name',params.custpage_lbltran);
						rec.setValue('custrecord_wmsse_label_order',irids);
						
						var shipmark = (params.custpage_shpmark)?params.custpage_shpmark:' ';
						
						
						rec.setValue({fieldId: 'custrecord_wmsse_label_shipattention', value: shipmark});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference1', value: params.custpage_shpdate||''});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference2', value: params.custpage_gwe||''});
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom1', value: z});
						rec.setValue({fieldId: 'custrecord_wmsse_label_labeltype', value: 'shpmark'});
						rec.setValue({fieldId: 'custrecord_wmsse_ext_template', value: 'shpmark'});
						rec.setValue({fieldId: 'custrecord_wmsse_ext_location', value: '16'});//default to s
						
						rec.save(true);
						nyapiLog('no sticker ',z);	
					}
					this.sessionObj.set({name: "message", value: "Shipmark Label Successfully Created"});
					
					this.response.sendRedirect({
						  type: http.RedirectType.SUITELET,
						    identifier: 'customscript_ft_wms_lbl_gen',
						    id :'customdeploy_ft_wms_lbl_gen',
						    parameters: {mn: 6}
					});	
				}
				
			}catch(exx){
				this.response.write(JSON.stringify(exx));
			}		
		};
    }
    
    
   
    
    
    function getItemDimension(item){
    	var itds = search.create({
    		   type: "customrecord_ebiznet_skudims",
    		   filters: [
    		      ["custrecord_ebizitemdims","anyof",item], 
    		      "AND", 
    		      ["isinactive","is","F"], 
    		      "AND", 
    		      ["custrecord_ebizuomskudim","anyof","2"]
    		   ],
    		   columns: [
    		      "custrecord_ebizitemdims",
    		      "custrecord_ebizuomskudim",
    		      "custrecord_ebizqty"
    		   ]
    		});
    		var qty = '';
    		itds.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			qty = result.getValue('custrecord_ebizqty');
    		   return true;
    		});
    		return qty;
    }
       
    function NaddField(id,label,type,source){
    	FORM.addField({
			id: id,
			label:label,
			type:type,
			source:source
		});	
    }
    
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
	    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
   
    
    return {
        onRequest: setGo
    };
    
});