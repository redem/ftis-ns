/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/currency', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/file', 'N/render','SuiteScripts/Internal/tools/ftis_underscore.js','SuiteScripts/Internal/date.js'],
/**
 * @param {currency} currency
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {file} file
 * @param {render} render
 */
function(currency, record, runtime, search, serverWidget, url, file, render) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	try{
    		var scriptObj = runtime.getCurrentScript();
    		var labelid = scriptObj.getParameter({name:'custscript_ft_wms_lblid'});
    		var labeltype = scriptObj.getParameter({name:'custscript_ft_wms_lbltype'});
    		
    		nyapiLog(labelid,labeltype);
        	switch(labeltype){
    	    	case 'Ilabel':
    	    		var newRecord = record.load({
    	    		    type: record.Type.ITEM_RECEIPT,
    	    		    id: labelid
    	    		});
    	    		createILabel(newRecord);
    	    		
    	    		break;
    	    	case 'OLabel':
    	    		var newRecord = record.load({
    	    		    type: record.Type.ITEM_FULFILLMENT, 
    	    		    id: labelid
    	    		});
    	    		createOLabel(newRecord);
    	    		break;	    	
        	}
    		
    	}catch(ex){
    		nyapiLog('error sc',ex);
    	}
		
    	
    	 	
    	
    }
    
    
    function createOLabel(newRecord){
     var crrecid = newRecord.getValue('createdfrom');
     var crec= '';
     try{
    	 crec = record.load({type:'salesorder',id:crrecid});
     }catch(wew){
    	 nyapiLog('catch crec', wew);
    	 crec = record.load({type:'transferorder',id:crrecid});
     }
     
     var ifid = newRecord.getValue('id');
     
     var creccount = crec.getLineCount({
		    sublistId: 'item'
	  });
     var ccont = [];
     for(var z = 0; z < creccount; z++){
    	 var cdata={};
    	 var citem = crec.getSublistValue({
			    sublistId: 'item',
			    fieldId: 'item',
			    line: z
			});
    	 var cquantity = crec.getSublistValue({
			    sublistId: 'item',
			    fieldId: 'quantity',
			    line: z
			});
    	 var cline = crec.getSublistValue({
			    sublistId: 'item',
			    fieldId: 'line',
			    line: z
			});
    	 var itempicked = crec.getSublistValue({
			    sublistId: 'item',
			    fieldId: 'itempicked',
			    line: z
			});
    	 var itempacked = crec.getSublistValue({
			    sublistId: 'item',
			    fieldId: 'itempacked',
			    line: z
			});
    	 var quantityfulfilled = crec.getSublistValue({
			    sublistId: 'item',
			    fieldId: 'quantityfulfilled',
			    line: z
			});
    	
    	 
    	 cdata.line = cline;
    	 cdata.tranid = crec.getValue('tranid');
    	 cdata.item = citem;
    	 cdata.quantity = cquantity;
    	 cdata.itempicked = itempicked;
    	 cdata.itempacked = itempacked;
    	 cdata.quantityfulfilled = quantityfulfilled;
    	
    	 ccont.push(cdata);
    	 
    	 
    	
     }
     nyapiLog('ccont',ccont);
     
   	  var itemcount = newRecord.getLineCount({
		    sublistId: 'item'
	  });
	  
	  for(var x = 0 ; x<itemcount; x++){
		  var item = newRecord.getSublistValue({
			    sublistId: 'item',
			    fieldId: 'item',
			    line: x
			});
		var itemvalidation = search.lookupFields({type:'item',id:item,columns:['custitem_is_nonsp']});
		
		if(!itemvalidation.custitem_is_nonsp){
			
			 var loc = newRecord.getSublistValue({
					sublistId: 'item',
					fieldId: 'location',
					line: x
				});
			 

			 var vitem = newRecord.getSublistValue({
				    sublistId: 'item',
				    fieldId: 'item',
				    line: x
				});
			 var vqty = newRecord.getSublistValue({
				    sublistId: 'item',
				    fieldId: 'quantity',
				    line: x
				});
			
			
			 var itemname = newRecord.getSublistText({
				    sublistId: 'item',
				    fieldId: 'itemname',
				    line: x
				});
			 var custpo = newRecord.getSublistText({
				    sublistId: 'item',
				    fieldId: 'custcol_ftis_cust_po',
				    line: x
				});
			 var cpn = newRecord.getSublistText({
				    sublistId: 'item',
				    fieldId: 'custcol_ftis_cpn',
				    line: x
				});
			 
			 var custrev = newRecord.getSublistValue({
				    sublistId: 'item',
				    fieldId: 'custcol_ftis_cust_rev',
				    line: x
				});
			  
			 var dbaseloc = newRecord.getSublistValue({
				    sublistId: 'item',
				    fieldId: 'custcol_ftis_database_location',
				    line: x
				});
			 
			 var lineno = newRecord.getSublistValue({
				    sublistId: 'item',
				    fieldId: 'orderline',
				    line: x
				});
			 var cfv= newRecord.getValue('createdfrom');
			 var rawopt =  getLineOpt(cfv,newRecord.id,vqty,vitem);
			 nyapiLog('rawopt', rawopt);
			 var cft= newRecord.getText('createdfrom');
			 var nc = cft.indexOf('#') + 1;
			 var strlen = cft.length;
			 var soid = cft.substr(nc,strlen);
			 nyapiLog('soid',soid);
			  
			 var shpadresee = newRecord.getSubrecord({
	 		    fieldId: 'shippingaddress'
			 });
			 var shpadr = shpadresee.getValue('attention');
			 
			  
			  var itemdet = newRecord.getSublistSubrecord({
				    sublistId: 'item',
				    fieldId: 'inventorydetail',
				    line: x
			  });
			  
			  nyapiLog('itemdetss',itemdet);
			  
			  var itemdetcount = itemdet.getLineCount({
				  sublistId: 'inventoryassignment'
			  });
			  
			  var qtyid = getItemDimension(item);
			  nyapiLog('qtyid',qtyid);
			  
			  for(var z = 0 ; z < itemdetcount; z++){
				  
					var expqty = itemdet.getSublistValue({
						sublistId: 'inventoryassignment',
						fieldId: 'quantity',
						line: z
					});

					var caseqty = parseFloat(expqty) / parseFloat(qtyid);
					if (caseqty > 0) {
						caseqty = Math.ceil(caseqty);
					}
					var totalqty = caseqty.toFixed();
	 				if (totalqty == 0) {
	 					totalqty = 1;
	 				}
    		  
					var totalcount = totalqty;
					var tempexpqty = expqty;
					var count = 0;
					for (var i = 0; i < totalcount; i++) {
						count++;
						nyapiLog('tempexpqty',tempexpqty);
						nyapiLog('qtyid',qtyid);
						var rec = record.create({
						     type: 'customrecord_wmsse_ext_labelprinting',
						     isDynamic: true
						});
						var tqty =0;
						if(tempexpqty < qtyid){
							tqty = tempexpqty;
						}else{
							tempexpqty = parseFloat(tempexpqty) - parseFloat(qtyid);
							tqty = qtyid;
						}
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom5', value: tqty}); //qty / moq per case
						
						rec.setValue({fieldId: 'name', value: ifid});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference4', value: dbaseloc||''});
						rec.setValue({fieldId: 'custrecord_wmsse_label_order', value: soid});
						
						var tdate = newRecord.getValue('trandate');
						var tdatecode = tdate.toString('d/M/yyyy');
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: tdatecode});
						
						//NA
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference2', value: ''});
												
						rec.setValue({fieldId: 'custrecord_wmsse_label_shipaddressee', value: shpadr});
												
						nyapiLog('location ', loc);
						rec.setValue({fieldId: 'custrecord_wmsse_ext_location', value: loc});
						rec.setValue({fieldId: 'custrecord_wmsse_ext_template', value: 'OLabel'});					
						rec.setValue({fieldId: 'custrecord_wmsse_label_labeltype', value: 'OLabel'});
						//rec.setValue({fieldId: 'custrecord_wmsse_label_printoption', value: true});
						
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference2', value: custrev});
					
						var lotno = itemdet.getSublistText({
							sublistId: 'inventoryassignment',
							fieldId: 'issueinventorynumber',
							line: z
						});
					
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom6', value: lotno});
						var custom8 = count + "of" + totalcount;//1 of 1
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom8', value: custom8});

						rec.setValue({fieldId: 'custrecord_wmsse_external_item', value: itemname});
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference3', value: custpo});
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom10', value: cpn});
							
						rec.setValue({fieldId: 'custrecord_wmsse_label_custom7', value: rawopt.custrecord_wmsse_line_no || ' '});
						
						var qrcode = cpn +', '+lotno+ ', '+tqty+', '+lineno+', '+custom8;
						rec.setValue({fieldId: 'custrecord_wmsse_label_reference1', value: qrcode});

						rec.save(true);
						nyapiLog('olabel',rec);
						var scriptObj = runtime.getCurrentScript();
						nyapiLog("Remaining governance units: ", + scriptObj.getRemainingUsage());
					}
			  }
		}
	  }
	
	 
   }
    
    function createILabel(newRecord){
		
  	  var itemcount = newRecord.getLineCount({
  		    sublistId: 'item'
  	  });
  	  
  	  for(var x = 0 ; x<itemcount; x++){
			var item = newRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'item',
			    line: x
			});
			var itemvalidation = search.lookupFields({type:'item',id:item,columns:['custitem_is_nonsp']});
			if(!itemvalidation.custitem_is_nonsp){
				var itemname = newRecord.getSublistValue({
					sublistId: 'item',
					fieldId: 'itemname',
				    line: x
				});
				var location = newRecord.getSublistValue({
					sublistId: 'item',
					fieldId: 'location',
				    line: x
				});
				
				  
				var itemdet = newRecord.getSublistSubrecord({
										sublistId: 'item',
										fieldId: 'inventorydetail',
										line: x
				  				});
				
							  
				var itemdetcount = itemdet.getLineCount({
					sublistId: 'inventoryassignment'
					});
				  
				var qtyid = getItemDimension(item);
				nyapiLog('qtyid',qtyid);
	  		  
				 var cft= newRecord.getText('createdfrom');
				 var nc = cft.indexOf('#') + 1;
				 var strlen = cft.length;
				 nyapiLog('nc',nc);
				 nyapiLog('strlen',strlen);
				 var soid = cft.substr(nc,strlen);
				
				
	  		  	for(var z = 0 ; z<itemdetcount; z++){
		  			  	var expqty = itemdet.getSublistValue({
							sublistId: 'inventoryassignment',
							fieldId: 'quantity',
							line: z
						});
					
						var caseqty = parseFloat(expqty) / parseFloat(qtyid);
						if (caseqty > 0) {
							caseqty = Math.ceil(caseqty);
						}
						var totalqty = caseqty.toFixed();
						if (totalqty == 0) {
							totalqty = 1;
						}
					  
						var totalcount = totalqty;
						var tempexpqty = expqty;
						var count = 0;
						
						for (var i = 0; i < totalcount; i++) {
							count++;
							nyapiLog('tempexpqty',tempexpqty);
							nyapiLog('qtyid',qtyid);
							var rec = record.create({
							     type: 'customrecord_wmsse_ext_labelprinting',
							     isDynamic: true
							});
							var tqty = 0;
							if(tempexpqty < qtyid){
								tqty = tempexpqty;
								rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: tqty}); //qty / moq per case
							}else{
								tempexpqty = parseFloat(tempexpqty) - parseFloat(qtyid);
								tqty =qtyid;
								rec.setValue({fieldId: 'custrecord_wmsse_label_custom4', value: qtyid}); //qty / moq per case
							}
							
							var irid = newRecord.getValue('tranid');	
							var irids = newRecord.getValue('id');
							var tdate = newRecord.getValue('trandate');
							var tdatecode = tdate.toString('d/M/yyyy');
							rec.setValue({fieldId: 'custrecord_wmsse_label_receivedate', value: tdatecode});
							
							nyapiLog('internalid',irids);
							rec.setValue({fieldId: 'name', value: irid});
							rec.setValue({fieldId: 'custrecord_wmsse_label_labeltype', value: 'Ilabel'});
							rec.setValue({fieldId: 'custrecord_wmsse_ext_template', value: 'Ilabel'});
												
							rec.setValue({fieldId: 'custrecord_wmsse_label_order', value: soid});
							rec.setValue({fieldId: 'custrecord_wmsse_ext_location', value: location});//location 16 -sg
							// rec.setValue({fieldId: 'custrecord_wmsse_label_custom6', value: 52});//remove
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom1', value: itemname}); //itemname
							var custom10 = count + "of" + totalcount;
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom10', value: custom10});//number of label   
							var lotno = itemdet.getSublistValue({
								sublistId: 'inventoryassignment',
								fieldId: 'receiptinventorynumber',
								line: z
							});
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom2', value: lotno}); //lotno
							var rdatecode = itemdet.getSublistValue({
								sublistId: 'inventoryassignment',
								fieldId: 'expirationdate',
								line: z
							});
							var datecode = rdatecode.toString('d/M/yyyy');
							rec.setValue({fieldId: 'custrecord_wmsse_label_custom3', value: datecode}); //expiry date
							//resssc.setValue({fieldId: 'custrecord_wmsse_label_printoption', value: true});
							
							var qr = itemname+'item, '+lotno +'lotno, '+tqty +' qty';
							rec.setValue({fieldId: 'custrecord_wmsse_label_reference1', value: qr}); //qrcode
							
							
							rec.save(true);
							nyapiLog('created',rec);
					
						}
	  		  }
			}
  	  }
  	  
    }
    
    function getLineOpt(orderno,confirmno,qty,sku){
    	var tsearch = search.create({
    		   type: "customrecord_wmsse_trn_opentask",
    		   filters: [
    		      ["custrecord_wmsse_order_no","anyof",orderno], 
    		      "AND", 
    		      ["custrecord_wmsse_nsconfirm_ref_no","anyof",confirmno], 
    		      "AND",
    		      ["custrecord_wmsse_expe_qty","anyof",qty],
    		      "AND",
    		      ["custrecord_wmsse_tasktype","anyof","3"], 
    		      "AND", 
    		      ["custrecord_wmsse_wms_status_flag","anyof","28"],
    		      "AND", 
    		      ["custrecord_wmsse_sku","anyof",sku]
    		   ],
    		   columns: [
    		      "internalid",
    		      "custrecord_wmsse_sku",
    		      "custrecord_wmsse_expe_qty",
    		      "custrecord_wmsse_line_no",
    		      "custrecord_wmsse_wms_status_flag",
    		      "custrecord_wmsse_tasktype",
    		      "custrecord_wmsse_order_no",
    		      "custrecord_wmsse_nsconfirm_ref_no"
    		   ]
    		});
    		var data = {};
    		tsearch.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    		
    			data.internalid = result.getValue('internalid');
    			data.custrecord_wmsse_sku = result.getValue('custrecord_wmsse_sku');
    			data.custrecord_wmsse_expe_qty = result.getValue('custrecord_wmsse_expe_qty');
    			data.custrecord_wmsse_line_no = result.getValue('custrecord_wmsse_line_no');
    			data.custrecord_wmsse_nsconfirm_ref_no = result.getValue('custrecord_wmsse_nsconfirm_ref_no');
    			data.custrecord_wmsse_order_no = result.getValue('custrecord_wmsse_order_no');
    			data.custrecord_wmsse_tasktype = result.getValue('custrecord_wmsse_tasktype');
    			
    			
    		   return true;
    		});
    		
    		
    		return data;
    		
    		
    	
    	
    }
    
    function getItemDimension(item){
    	var itds = search.create({
    		   type: "customrecord_ebiznet_skudims",
    		   filters: [
    		      ["custrecord_ebizitemdims","anyof",item], 
    		      "AND", 
    		      ["isinactive","is","F"], 
    		      "AND", 
    		      ["custrecord_ebizuomskudim","anyof","2"]
    		   ],
    		   columns: [
    		      "custrecord_ebizitemdims",
    		      "custrecord_ebizuomskudim",
    		      "custrecord_ebizqty"
    		   ]
    		});
    		var qty = '';
    		itds.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			qty = result.getValue('custrecord_ebizqty');
    		   return true;
    		});
    		return qty;
    }
    
    function nyapiLog(title_log, details_log) {
    	log.error({
    		title : title_log,
    		details : details_log
    	});
    }

    return {
        execute: execute
    };
    
});
