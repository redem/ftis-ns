/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/currency', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/file', 'N/render','SuiteScripts/Internal/tools/ftis_underscore.js','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {currency} currency
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {file} file
 * @param {render} render
 */
function(currency, record, runtime, search, serverWidget, url, file, render) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;

		this.runFunction = function() {
			
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			try{
				nyapiLog('GET');
				var body = this.request.parameters;
				nyapiLog('param',body);
			    var scriptObj = runtime.getCurrentScript();
				nyapiLog("Remaining governance units: ", + scriptObj.getRemainingUsage());
			    
			    this.response.writeLine(body); 		
			}catch(exx){
				nyapiLog('exx',exx);
			}
		};
		
		this.POST = function(){
			
			try{
				nyapiLog('POST');
				var body = this.request;
				nyapiLog('body',body);
			    var scriptObj = runtime.getCurrentScript();
				nyapiLog("Remaining governance units: ", + scriptObj.getRemainingUsage());
			    
			    this.response.writeLine(body); 		
			}catch(exx){
				nyapiLog('exx',exx);
			}
			
	        
		};
		
    }
    
    
    function nyapiIn(nsObj) {
		var tos = JSON.stringify(nsObj);
		return JSON.parse(tos);
	}
    
    function nyapiLoad(type, id) {
		var rec = record.load({
			type : type,
			id : id,
			isDynamic : true
		});
		
		//rec = nyapiIn(rec);
		return rec;
	}
    
    function nyapiLog(title_log, details_log) {
    	log.error({
    		title : title_log,
    		details : details_log
    	});
    }
    
    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    
    
    return {
        onRequest: setGo
    };
    
});