/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url', 'N/render','N/format'],
/**
 * @param {file} file
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {render} render
 * @param {format} format
 */
function(file, record, runtime, search, serverWidget, url, render, format) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;

		this.runFunction = function() {
			
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		this.GET = function() {
			var sessionObj = runtime.getCurrentSession();
			var param = this.request.parameters;
			
			if(param.vpid){
				var cri = sessionObj.get({name: "_cri"});
				var scri = JSON.parse(cri);
				var capply = (typeof(scri.apply) =='object')?scri.apply:[];
				var ccredit = (typeof(scri.credit)=='object')?scri.credit:[];
				var plines = [];
				//cred
				//doc bil credit
				//doc2 is bill
				//payment bill payment
				
				//appy
				//doc bill
				//total
				//amount

				var newapp = [];
				var xbill = [];
				for(var ap in capply){
					newapp.push(capply[ap]);
					var dbill = {};
					dbill.bill = capply[ap].internalid;
					dbill.refnum = capply[ap].refnum;
					dbill.amount = capply[ap].amount;
					dbill.cdate =  capply[ap].duedate;
					
					xbill.push(dbill);
				}
		
				var newcredit = [];
				for(var cd in ccredit){
					if(ccredit[cd].trantype == "VendCred"){
						var bill = ccredit[cd].doc2;
						var nlso = search.lookupFields({
							 type:'vendorbill',
							 id: bill,
							 columns:['tranid','duedate']
						});
						ccredit[cd].dd = nlso['duedate'];
						ccredit[cd].tranid = nlso['tranid'];
						
						newcredit.push(ccredit[cd]);	
					}					
				}

				for(var cc in newcredit){
						var data = {};
						data.billcred = newcredit[cc].doc;
						data.bill = newcredit[cc].doc2;
						data.payment = newcredit[cc].payment;
						data.amount = newcredit[cc].amount;
						data.refnum = newcredit[cc].refnum;
						data.cdate = newcredit[cc].creditdate;	
						plines.push(data);		
				}
				
				var groups2 = {};
				
				for (var i = 0; i < plines.length; i++) {
					  var groupName = plines[i].bill;
					  if (!groups2[groupName]) {
						  groups2[groupName] = [];
						 }
					  groups2[groupName].push(plines[i]);
				}
				
				
				
				for(var line in xbill){
					var xbid = xbill[line].bill;
					var ifex = groups2[xbid];
					if(ifex){
						groups2[xbid].push(xbill[line]);
					}else{
						groups2[xbid]=[xbill[line]]; 
					}
					
				}
				
				var arrgroup2 =[];
				for (var groupName in groups2) {		
					if(groupName !="undefined"){
						var wew ={};
						wew.id = groupName;
						wew.details = groups2[groupName];
						arrgroup2.push(wew);
					}
				}

				var output2 = [];
				for (var i=0; i < arrgroup2.length; i++) {
					var data = {};
					var opt= {};
					var arr = arrgroup2[i].details;
					var bill = arrgroup2[i].id;
					data.id = bill;
					data.details = arr;
					var printamount = 0.00;
					var nlso = search.lookupFields({
						 type:'vendorbill',
						 id: bill,
						 columns:['tranid','duedate']
					});
					var dd  = nlso['duedate'];
					var tranid = nlso['tranid'];
					
					
					for(var x = 0; x < arr.length; x++){
						printamount += parseFloat(arr[x].amount)*1;
					}
					opt.tranid = tranid;
					opt.printamount = addCommas(printamount.toFixed(2));
					opt.dd = dd;
					data.toprint = opt;
					arrgroup2[i].opt= opt;
					output2.push(opt);
				}
				
				var groups = {};
				for (var i = 0; i < plines.length; i++) {
				  var groupName = plines[i].refnum;
				  if (!groups[groupName]) {
				    groups[groupName] = [];
				  }
				  groups[groupName].push(plines[i]);
				}
				myArray = [];
				for (var groupName in groups) {		
					if(groupName !="undefined"){
						var wew ={};
						wew.id = groupName;
						wew.details = groups[groupName];
						myArray.push(wew);
					}
				}
						
				var output = [];
				for (var i=0; i < myArray.length; i++) {
					var data = {};
					var opt= {};
					var arr = myArray[i].details;
					var refnum = myArray[i].id;
					data.id = refnum;
					data.details = arr;
					var amnt = 0.00;
					var cdate = '';
					
					for(var x = 0; x < arr.length; x++){
						amnt += parseFloat(arr[x].amount)*1;
						cdate = arr[x].cdate;
					}
					opt.refnum = refnum;
					opt.amount = addCommas(amnt.toFixed(2));
					opt.cdate = cdate;
					data.toprint = opt;
					myArray[i].opt= opt;
					output.push(opt);
				}
				
				
				var recs = record.load({
					type:'vendorpayment',
					id:param.vpid,
					isDynamic:false
				});
				
				var rec = nyapiIn(recs);
	    		var recFields = rec.fields;
	    		nyapiLog('recFields',recFields);
	
				
				var acct = recFields.account;
				var accntname = search.lookupFields({
					 type:'account',
					 id: acct,
					 columns:['name']
				});
				
				recFields.address = (recFields.address)?recFields.address.replace(/\r\n/gi,'<BR/>'):'';
				recFields.accntname = accntname['name'];
				
				var subs = recFields.subsidiary;
				
				var subssss = search.lookupFields({
					 type:'subsidiary',
					 id: subs,
					 columns:['custrecord_subsidiary_logo','custrecord_subsidiary_address','namenohierarchy','legalname']
				});
				var size = "A4";
				if(subs == 3){
					size = "A4-Landscape";
				}
		
				output2.sort(function(a, b){
				    return a.dd > b.dd;
				});
				
				var srecF = JSON.stringify(recFields).replace(/&/gi,'&amp;');
				
				var jrecF = JSON.parse(srecF);
				
				jrecF.custrecord_subsidiary_logo = subssss.custrecord_subsidiary_logo[0].text.replace(/&/gi,'&amp;');
				jrecF.custrecord_subsidiary_address = subssss.custrecord_subsidiary_address;
				jrecF.subsidiaryname = subssss.legalname;
				
				
				jrecF.size = size;
				
				var totals = format.format({
				    value: jrecF.total,
				    type: format.Type.CURRENCY
				    });
				
				var trandate = format.format({
				    value: jrecF.trandate,
				    type: format.Type.DATE
				    });
				
				jrecF.total = totals;
				jrecF.trandate = trandate;
				
	    		var outtemp = {};
	    		outtemp.record = jrecF;
				outtemp.capply = output2;
				outtemp.credit = output;
				
				//28600 sandbox
				
				var xmlStr = file.load('27837');
	            var renderer = render.create();
	            renderer.templateContent = xmlStr.getContents();
	            
	            renderer.addCustomDataSource({
	                format: render.DataSource.OBJECT,
	                alias: "JS",
	                data: outtemp
	                });
	            
	            
	            var newfile = renderer.renderAsPdf();
	            this.response.writeFile(newfile,true); 		
	    
	    		
				
				
			}else{
				this.response.write("Invalid Request");
			}
		
			
		};
		

		this.POST = function() {
			this.response.write('post');
		};
		
    }
    
    function nyapiIn(nsObj){
    	var tos = JSON.stringify(nsObj);
    	return JSON.parse(tos);
    }
    
    function addCommas(n){
        var rx=  /(\d+)(\d{3})/;
        return String(n).replace(/^\d+/, function(w){
               while(rx.test(w)){
                       w= w.replace(rx, '$1,$2');
               }
               return w;
        });
    }

    
    Array.prototype.inArray = function (value)
    {
     // Returns true if the passed value is found in the
     // array. Returns false if it is not.
     var i;
     for (i=0; i < this.length; i++)
     {
       if (this[i] == value)
       {
         return true;
       }
     }
     return false;
    };
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    return {
        onRequest: setGo
    };
    
});
