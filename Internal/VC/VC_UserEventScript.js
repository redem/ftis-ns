/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 */
function(record, runtime, search, serverWidget, url) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	var form = scriptContext.form;
    	var newr = scriptContext.newRecord;
    	
       	if(scriptContext.type == 'view'){

    		var rec = nyapiIn(newr);
    		var recFields = rec.fields;
    		var sublist = rec.sublists;
    		var apply = sublist.apply;
    		var credit = sublist.credit;

    		
    		var sessionObj = runtime.getCurrentSession();
    		
    		var wew = {};
    		wew.credit = credit;
    		wew.apply = apply;

    		sessionObj.set({
    		    name: "_cri", 
    		    value: JSON.stringify(wew)
    		    });
    		
    		var hand = '';
    		require(['N/url'], function(url) {
	    			var output = url.resolveScript({
	    				scriptId: 'customscript_vp_ss_2',
	    				deploymentId: 'customdeploy_vp_ss_2',
	    				returnExternalUrl: false,
	    				params:{vpid:recFields.id}
	    			});
	    			hand = output;
    		});
    		

    		var url = "window.open('"+hand+"','_blank')";

			//form.clientScriptFileId = f;
        	form.addButton({
        	    id : 'custpage_vpp',
        	    label : 'Print Vendor Payment',
        	    functionName : url
        	});
       	}
    }
    
    function nyapiIn(nsObj){
    	var tos = JSON.stringify(nsObj);
    	return JSON.parse(tos);
    }
    
    Array.prototype.inArray = function (value)
    {
     // Returns true if the passed value is found in the
     // array. Returns false if it is not.
     var i;
     for (i=0; i < this.length; i++)
     {
       if (this[i] == value)
       {
         return true;
       }
     }
     return false;
    };
    
    function nyapiLog(title_log, details_log) {
		log.debug({
			title : title_log,
			details : details_log
		});
	}
    

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {

    }

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
