/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/url'],
/**
 * @param {runtime} runtime
 * @param {url} url
 */
function(runtime, url) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	var newr = scriptContext.newRecord;
    	var form = scriptContext.form;
    
    	nlog('form2',JSON.stringify(form));
    	var sub = newr.getValue('subsidiary');
    	var cat = newr.getValue('custbodycustbody_ftis_vendor_category');
    	
    	var vcat = ["7","8"];
    	var vsub = ["4","3"];
    	
    	if(vcat.inArray(cat) && vsub.inArray(sub)){
    		var f = form.getField('custbody_ft_shipment_date');
    		nlog('form',f);
    		f.isMandatory = true;
    	}
    	
    	
    	
    }
    function nlog(title_log,details_log){
        log.debug({
            title:title_log,
            details:details_log
        });
    }
    
    Array.prototype.inArray = function (value)
    {
     // Returns true if the passed value is found in the
     // array. Returns false if it is not.
     var i;
     for (i=0; i < this.length; i++)
     {
       if (this[i] == value)
       {
         return true;
       }
     }
     return false;
    };

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {

    }

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
