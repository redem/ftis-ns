/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/runtime','N/search'],
/**
 * @param {runtime} runtime
 * @param {search} search
 */
function(runtime,search) {
    


    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	
		var cr = scriptContext.currentRecord;
		var sub = cr.getValue('subsidiary');
    	if(scriptContext.fieldId == 'subsidiary' || scriptContext.fieldId == 'custbodycustbody_ftis_vendor_category'){
    	
    		
        	var cat = cr.getValue('custbodycustbody_ftis_vendor_category');
        	var f = cr.getField('custbody_ft_shipment_date');
    		
    		var vcat = ["7","6"];
        	var vsub = ["4","3"];
        	
        	if(vcat.inArray(cat) && vsub.inArray(sub)){
	        
	    		f.isMandatory = true;
        	}else{
        		f.isMandatory = false;
        	}
    	}
        	
   
    	
    	
    
    }
    Array.prototype.inArray = function (value)
    {
     // Returns true if the passed value is found in the
     // array. Returns false if it is not.
     var i;
     for (i=0; i < this.length; i++)
     {
       if (this[i] == value)
       {
         return true;
       }
     }
     return false;
    };
    
    
	function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
    
   
    return {
        fieldChanged: fieldChanged
    };
    
});
