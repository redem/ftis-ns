var RT,FORM,DEPLOYMENT_URL,ENCODEMODULE,FORM;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/http', 'N/record', 'N/render', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url','N/encode','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {file} file
 * @param {http} http
 * @param {record} record
 * @param {render} render
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 * @param {encode} encode
 */
function(file, http, record, render, runtime, search, serverWidget, url, encode) {
    RT = runtime;
    ENCODEMODULE = encode ;
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	this.request = context.request;
		this.response = context.response;
		this.sessionObj = runtime.getCurrentSession();
		
		this.runFunction = function() {
			if (this.request.method == 'GET') {
				this.GET();
			} else {
				this.POST();
			}
		};
		
		this.GET = function(){
			var params = this.request.parameters;
			
			
			var form = serverWidget.createForm({
			    title : 'Bulk Printing <br><hr/> FTIS Packing List'
			});
			
			FORM= form;
			form.clientScriptFileId = 37616;
			
			var tcust = form.addField({
				id: 'custpage_cust',
				label:'Customer',
				type:'select',
				source:'customer'
			});	
			
			if(params.cust){
				tcust.defaultValue = params.cust;
			}
			
			var tdate = form.addField({
				id: 'custpage_date',
				label:'Transaction Date',
				type:'date',
			});	
			
			if(params.date){
				tdate.defaultValue = params.date;
			}
			
			var inct = form.addField({
				id: 'custpage_inct',
				label:'IncoTerm',
				type:'select',
				source:'incoterm'
			});	
			
			if(params.inct){
				inct.defaultValue = params.inct;
			}
			
			var slist = form.addSublist({
				id:'custpage_list',
				label :'Results',
				type:'list'
			});
			
			
			slist.addField({
			    id : 'custpage_select',
			    type : serverWidget.FieldType.CHECKBOX,
			    label : 'Select'
			});
			
			var intid = slist.addField({
			    id : 'custpage_intid',
			    type : serverWidget.FieldType.TEXT,
			    label : 'Internal Id'
			});	
			
			intid.updateDisplayType({
		    	displayType : serverWidget.FieldDisplayType.HIDDEN
			});

			slist.addField({
			    id : 'custpage_refno',
			    type : serverWidget.FieldType.TEXT,
			    label : 'Reference Number'
			});
			slist.addField({
			    id : 'custpage_tran',
			    type : serverWidget.FieldType.TEXT,
			    label : 'DO Number'
			});
			
			slist.addField({
			    id : 'custpage_tranname',
			    type : serverWidget.FieldType.TEXT,
			    label : 'Customer'
			});

			slist.addField({
			    id : 'custpage_trandate',
			    type : serverWidget.FieldType.DATE,
			    label : 'Transaction Date'
			});

			slist.addField({
			    id : 'custpage_shipvia',
			    type : serverWidget.FieldType.TEXT,
			    label : 'Ship Via'
			});
			slist.addField({
			    id : 'custpage_incoterm',
			    type : serverWidget.FieldType.TEXT,
			    label : 'Incoterm'
			});
			slist.addField({
			    id : 'custpage_vessel',
			    type : serverWidget.FieldType.TEXT,
			    label : 'Vessel'
			});
			slist.addField({
			    id : 'custpage_shpmark',
			    type : serverWidget.FieldType.TEXT,
			    label : 'Shipping Mark'
			});
			
	

			
			
			
			if(params.cust && params.date && params.inct){
				
				form.addButton({
					id:'custpage_print',
					label:'Print to Excel',
					functionName:"btnclick"
				});
				
				var rest = getList(params.cust,params.date,params.inct);
				nyapiLog('res',rest);
				var result = JSON.stringify(rest);
				var wew= JSON.parse(result);
				
				for(var x = 0; x < wew.length; x++){
					
					var data = wew[x];
					var val = data.values;
					nyapiLog('val',data);
					
					slist.setSublistValue({
				    	id : 'custpage_intid',
				    	line : x,
				    	value :data.id
					});
					
					slist.setSublistValue({
					    id : 'custpage_refno',
					    line : x,
					    value : data.values.createdfrom[0].text
					});
					
					slist.setSublistValue({
					    id : 'custpage_tran',
					    line : x,
					    value : "Item Fulfillment #"+data.values.tranid
					});
					
					slist.setSublistValue({
					    id : 'custpage_tranname',
					    line : x,
					    value :  data.values.mainname[0].text
					});
					slist.setSublistValue({
					    id : 'custpage_trandate',
					    line : x,
					    value : data.values.trandate
					});
					slist.setSublistValue({
					    id : 'custpage_shipvia',
					    line : x,
					    value :data.values.shipmethod[0].text ||" "
					});
					var inct  =(data.values.custbody_ftis_incoterm_tran[0])?data.values.custbody_ftis_incoterm_tran[0].text:' ';
					slist.setSublistValue({
					    id : 'custpage_incoterm',
					    line : x,
					    value :inct
					});
					slist.setSublistValue({
					    id : 'custpage_vessel',
					    line : x,
					    value :data.values.custbody_vessel||" "
					});
					slist.setSublistValue({
					    id : 'custpage_shpmark',
					    line : x,
					    value :data.values.custbody_ftis_enduser_shippingmark ||" "
					});
				}
			}
			this.response.writePage(form);
		};
		
		
		this.POST = function(){
			var params = this.request.body;
			var res = {};
			res.message = 'Invalid Request';
			
			if(params){
				params = JSON.parse(params);
				res.body = params[0].tran;
				res.message = "success";
			}
			
			
			
			
			
			
			this.response.write(JSON.stringify(res));
			//this.response.writeLine(JSON.stringify(ret));
		
		};
		
    }
    
    function getList(cust,date,inct){
    	var itemfulfillmentSearchObj = search.create({
    		   type: "itemfulfillment",
    		   filters: [
    		      ["type","anyof","ItemShip"], 
    		      "AND", 
    		      ["name","anyof",cust], 
    		      "AND", 
    		      ["mainline","is","T"], 
    		      "AND", 
    		      ["trandate","on",date], 
    		      "AND", 
    		      ["status","anyof","ItemShip:C"],
    		      "AND",
    		      ["custbody_ftis_incoterm_tran","anyof",inct]
    		   ],
    		   columns: [
    		      "internalid",
    		      "createdfrom",
    		      "mainname",
    		      "custbody_vessel",
    		      "custbody_ftis_enduser_shippingmark",
    		      "shipmethod",
    		      "custbody_ftis_incoterm_tran",
    		      "tranid",
    		      search.createColumn({
    		         name: "trandate",
    		         sort: search.Sort.ASC
    		      }),
    		      "shipaddress"
    		   ]
    		});
    		var cont = [];
    		itemfulfillmentSearchObj.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			cont.push(result);
    		   return true;
    		});
    		
    		return cont;
    		
    		
    }
    
    function NaddField(id,label,type,source){
    	FORM.addField({
			id: id,
			label:label,
			type:type,
			source:source
		});	
    }
    
    function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
	    
    function setGo(context) {
		new onRequest(context).runFunction();
	}
    function getNow()
    {
      var d = new Date();
      return new Date(d.getTime() + (480 + d.getTimezoneOffset()) * 60000);
    }
    
    function nvl(val,val2)
    {
        return val == null ? val2 : val;
    }
    
   
    
    
    
    return {
        onRequest: setGo
    };
    
});