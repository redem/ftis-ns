var MDE = '';
/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/runtime','N/search','SuiteScripts/Internal/tools/ftis_underscore.js'],
/**
 * @param {runtime} runtime
 * @param {search} search
 */
function(runtime, search) {
	MDE ='';
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
    	
    	if (window.onbeforeunload){
   	  	   window.onbeforeunload=function() { null;};
   	  	}
    	
    	
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    		var durl = window.nlapiResolveURL('SUITELET','customscript_bp_ft_pl','customdeploy_bp_ft_pl');
    		if(scriptContext.fieldId == 'custpage_cust'){
    			var enti = window.nlapiGetFieldValue(scriptContext.fieldId);
    			var ddate = window.nlapiGetFieldValue('custpage_date');
    			var inct = window.nlapiGetFieldValue('custpage_inct');
    			if(ddate && enti && inct){
    				durl+= "&cust="+enti+"&date="+ddate+'&inct='+inct;
    	    		location = durl;
    	        	
    	        	
    	    	}
    		}
    		
    		if(scriptContext.fieldId == 'custpage_date'){
    			
    			var ddate = window.nlapiGetFieldValue(scriptContext.fieldId);
    			var enti = window.nlapiGetFieldValue('custpage_cust');
    			var inct = window.nlapiGetFieldValue('custpage_inct');
    			if(ddate && enti && inct){
    	    		
    				durl += "&cust="+enti+"&date="+ddate+'&inct='+inct;
    	    		location = durl;
    	        	
    	    	}
    		}
    		
    		if(scriptContext.fieldId == 'custpage_inct'){
    			
    			var inct = window.nlapiGetFieldValue('custpage_inct');
    			var ddate = window.nlapiGetFieldValue('custpage_date');
    			var enti = window.nlapiGetFieldValue('custpage_cust');
    			
    			
    			if(ddate && enti && inct){
    	    		
    				durl+= "&cust="+enti+"&date="+ddate+'&inct='+inct;
    	    		location = durl;
    	        	
    	    	}
    		}
    	

    }

    function btnclick(){
    
    	var scount = window.nlapiGetLineItemCount('custpage_list');
    	var cont = [];
    	var valid = [];
    	for(var x=1; x<=scount; x++){
    		var data = {};
    		var select = window.nlapiGetLineItemValue('custpage_list','custpage_select',x);
    		var tran  = window.nlapiGetLineItemValue('custpage_list','custpage_tran',x);
    		var vessel  = window.nlapiGetLineItemValue('custpage_list','custpage_vessel',x);
    		if(select == "T"){
    			data.tran = tran;
        		data.vessel = vessel;
        		valid.push(vessel);
    			data.intid = window.nlapiGetLineItemValue('custpage_list','custpage_intid',x);
    			cont.push(data);
    		}
    		
    	}
    	if(valid.length){
    		if(!!_.reduce(valid,function(a, b){ return (a === b) ? a : NaN; })){
    			var durl = window.nlapiResolveURL('SUITELET','customscript_bp_ft_pl','customdeploy_bp_ft_pl');
    			var param = JSON.stringify(cont);
    			var res = window.nlapiRequestURL(durl, param, 'POST');
    			console.log('res start');
    			console.log(res.getBody());
    			console.log('res end');
    		}else{
    			alert("Must select the same vessel");
    		}
    	}else{
    		alert("you must select Item fulfilment to generate Packing list");
    	}
		
    	
		
    	console.log(cont);
    	
    }
    
    
    return {
        pageInit: pageInit,
        fieldChanged:fieldChanged,
        btnclick:btnclick
       
    };
    
});