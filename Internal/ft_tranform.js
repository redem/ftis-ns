/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/url'],
/**
 * @param {runtime} runtime
 * @param {url} url
 */
function(runtime, url) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	try{
    		var newrec = scriptContext.newRecord;
        	var nsub = newrec.getValue('subsidiary');
    		var nloc = newrec.getValue('subsidiary');
    		
    		//purchase order 15 internalid
    		var cform = [];
    		
    		if(nsub && nloc){
        		cform = getFtForm(15,nsub,nloc);
    		}
    		
    		if(cform.length){
    			nyapiLog('cform',cform);
    		}
    		
        	
        	
    	}catch(ex){
    		nyapiLog('Error BF',ex);
    	}
    	
    }
    
    function getFtForm(type,subs,loc){
    	var customrecord_ft_manage_formSearchObj = search.create({
    		   type: "customrecord_ft_manage_form",
    		   filters: [
    		      ["custrecord_ft_fm_ttype","anyof",type], 
    		      "AND", 
    		      ["custrecord_ft_fm_loc","anyof",loc], 
    		      "AND", 
    		      ["custrecord_ft_fm_sub","anyof",subs]
    		   ],
    		   columns: [
    		      "custrecord_ft_fm_ttype",
    		      "custrecord_ft_fm_sub",
    		      "custrecord_ft_fm_loc",
    		      "custrecord_ft_fm_form"
    		   ]
    		});
		var cont= [];
		customrecord_ft_manage_formSearchObj.run().each(function(result){
		   var data = {};
		   data.type = result.getValue('custrecord_ft_fm_ttype');
		   data.subsidiary = result.getValue('custrecord_ft_fm_sub');
		   data.location = result.getValue('custrecord_ft_fm_loc');
		   data.form = result.getValue('custrecord_ft_fm_form');
		   cont.push(data);
		   return true;
		});
    	return cont;
    }
    
    function nyapiLog(title_log,details_log){
        log.error({
            title:title_log,
            details:details_log
        });
    }


    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {

    }

    return {
        beforeLoad: beforeLoad
    };
    
});
