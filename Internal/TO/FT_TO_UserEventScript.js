/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	
    	nyapiLog('scriptContext',JSON.stringify(scriptContext));
    	var newRec = scriptContext.newRecord;   	
    	
    	
    	var curRec = record.load({
    	    type: newRec.type, 
    	    id: newRec.id,
    	    isDynamic: true,
    	});
    	
    
    	var loc = curRec.getValue('transferlocation');
    	nyapiLog('loc',loc);		
    	var itemcount = curRec.getLineCount({
		    sublistId: 'item'
		});
    	
     	for(var x= 0; x < itemcount; x++){
			curRec.selectLine({
			    sublistId: 'item',
			    line: x
			});
			var item = curRec.getCurrentSublistValue({
			    sublistId: 'item',
			    fieldId: 'item'
			});
			nyapiLog('item', item);
			var param = {
					'item':item,
					'location':loc
					};
			var aCost = getAverageCost(param);
			if(aCost){
				curRec.setCurrentSublistValue({
					 sublistId: 'item',
				    fieldId: 'rate',
				    value: aCost
				});
			}
			
			nyapiLog('cost', aCost);
			curRec.commitLine({
			   sublistId: 'item'
			});
		}
    	curRec.save({
    		  enableSourcing: false,
    		  ignoreMandatoryFields: false
    	}); 
    	
    	
    	
    }

    function getAverageCost(obj){
    	var itemSearchObj = search.create({
    		   type: "item",
    		   filters: [
    		      ["internalid","anyof",obj.item], 
    		      "AND", 
    		      ["inventorylocation","anyof",obj.location]
    		   ],
    		   columns: [
    		      search.createColumn({
    		         name: "itemid",
    		         sort: search.Sort.ASC
    		      }),
    		      "locationaveragecost",
    		      "inventorylocation"
    		   ]
    		});
    		var rest = '';
    		itemSearchObj.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			rest = result.getValue('locationaveragecost');
    		   return true;
    		});
    		nyapiLog('rest',rest);
    		return rest;
    }
    
    function nyapiLog(title_log,details_log){
        log.debug({
            title:title_log,
            details:details_log
        });
    }
    
    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
