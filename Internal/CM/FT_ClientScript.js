/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/url'],
/**
 * @param {runtime} runtime
 * @param {url} url
 */
function(runtime, url) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
    	if (window.onbeforeunload){
    	  	   window.onbeforeunload=function() { null;};
    	  	}
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	
    	if(scriptContext.fieldId == 'entity'){
    		var cr = scriptContext.currentRecord;
    		var ent = cr.getValue(scriptContext.fieldId);

    		var cmurl = url.resolveRecord({
    		    recordType: 'creditmemo'
    		});
    		var loc = cmurl +"&entity=" + ent;
    		location = loc;
    	}
    
    	
    }

    /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) {

    }
   

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged
    };
    
});
