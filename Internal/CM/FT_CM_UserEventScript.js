/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/url'],
/**
 * @param {runtime} runtime
 * @param {url} url
 */
function(runtime, url) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	var newr = scriptContext.newRecord;
    	var req = scriptContext.request;
    	nyapiLog('req',req.parameters);
    	
    }

    function nyapiLog(title_log,details_log){
        log.debug({
            title:title_log,
            details:details_log
        });
    }

    return {
        beforeLoad: beforeLoad
    };
    
});
