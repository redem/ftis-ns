/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/file', 'N/runtime', 'N/search', 'N/url','SuiteScripts/Internal/date.js','SuiteScripts/QRCODE/lib/trimpath.js'],
/**
 * @param {format} format
 * @param {file} file
 * @param {runtime} runtime
 * @param {search} search
 * @param {url} url
 */
function(format, file, runtime, search, url) {

    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
	var params;
    function pageInit(scriptContext) {
    	console.log('scriptContext');
    	params = parseQueryString();
    	console.log(scriptContext);
    	if (window.onbeforeunload){
  	  	   window.onbeforeunload=function() { null;};
  	  	}
    	console.log(scriptContext);
    	var rses = runtime.getCurrentSession();
    	
    	NS.jQuery('#csvFileInput').change('on',function(){ 
			handleFiles(this.files);
			window.nlapiSetFieldValue('custpage_indicator','wew');

		});
    	
    	var tdColor = '#FF0000';
    	var tddColor ='#ffbf00';
    	var subname = 'custpage_sub_bil';
    	var subl = window.nlapiGetLineItemCount(subname);
    	console.log(subl);
    	var line = 0;
    	for(var x = 1; x <= subl; x++){
    		var hint = false;
    		for(var y = 0; y < 7; y++){
    			var ltf = Date.parse('today'+y+' month').toString('MMMyy').toLocaleLowerCase();
    			var vname = 'custpage_formulanumericbal'+ltf;
        		var gap = window.nlapiGetLineItemValue(subname,vname,x);
        		hint = (gap < 0)?true:false;
        		if(hint){
        			var trDom = document.getElementById("custpage_sub_bilrow"+line);
    	    	    var trDomChild = trDom.children;
    	    	    var ttbs = 0;
    	    	    for (var t=0; t < trDomChild.length; t++)
    		    	{
    		    		var wewew = trDomChild[t];
    		    		var s = wewew;
    	    			var num = wewew.innerHTML;
    	    			
    	    			var xx = (num<0)?true:false;
    	    			if(xx){
    	    				wewew.setAttribute(
    				    			'style',
    				    			//This is the magic CSS that changes the color
    				    			//	This is Same method used when NetSuite returns saved search results
    				    			//	with user defined row high lighting logic!
    				    			'background-color: '+tdColor+'!important;border-color: white '+tdColor+' '+tdColor+' '+tdColor+'!important;'
    				    		);
    	    			}
    		    	}
        		}

        		if(params.level == 2){
        			var trDom = document.getElementById("custpage_sub_bilrow"+line);
    	    	    var trDomChild = trDom.children;
    	    	    var ttbs = 0;
    	    	    var oh = 0;
    	    	    var ohh = null;
    	    	    var xx = false;
    	    	    for (var t=0; t <= 5; t++)
    		    	{
    		    		var wewew = trDomChild[t];
    		    		var s = wewew;
    	    			var num = wewew.innerHTML;
    	    			if(t == 4){
    	    				ttbs = num;
    	    			}
    	    			if(t == 5){
    	    				oh = Math.abs(num);
    	    				ohh = wewew;
    	    			}
    	    			xx = (ttbs > oh)?true:false;

    		    	}
    	    		if(xx){
    	    			console.log(ohh);
	    				ohh.setAttribute(
				    			'style',
				    			//This is the magic CSS that changes the color
				    			//	This is Same method used when NetSuite returns saved search results
				    			//	with user defined row high lighting logic!
				    			'background-color: '+tddColor+'!important;border-color: white '+tddColor+' '+tddColor+' '+tddColor+'!important;'
				    		);
	    			}
        		}
        		
        		
        		
    		}	
    		/**if(hint){
    			var trDom = document.getElementById("custpage_sub_bilrow"+line);
	    	    var trDomChild = trDom.children;
	    	    console.log("wewewew   "+x +"   " +trDomChild.length + " line  ");
	    	    for (var t=0; t < trDomChild.length; t++)
		    	{
		    		var wewew = trDomChild[t];
	    			var num = wewew.innerHTML;
	    			var xx = (num<0)?true:false;
	    			if(xx){
	    				wewew.setAttribute(
				    			'style',
				    			//This is the magic CSS that changes the color
				    			//	This is Same method used when NetSuite returns saved search results
				    			//	with user defined row high lighting logic!
				    			'background-color: '+tdColor+'!important;border-color: white '+tdColor+' '+tdColor+' '+tdColor+'!important;'
				    		);
	    			}
		    	}
    		}
    		**/
    		
    		var xhint = false;
    		
    		
    		line++;
    	}
    	
    	if(!params.level || params.level == 0 || params.level > 2){
    		window.nlapiSetFieldValue('custpage_level',1);
    		
    	}
    	
    	
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	
    	var curRec = scriptContext.currentRecord;
    	if(scriptContext.fieldId == 'custpage_brand'){
	    	var vbrand = curRec.getValue('custpage_brand');
	    	if(vbrand){
		    	var output = url.resolveScript({
		    	    scriptId: 'customscript_rr_s2',
		    	    deploymentId: 'customdeploy_rr_s2'
		    	});
		    	
		    	var path = output + "&brand="+vbrand +'&level='+params.level;
		    	location = path;
	    	}
    	}
    	
    	
    	if(scriptContext.fieldId == 'custpage_filess'){
    	
	    	var vfile = curRec.getValue(scriptContext.fieldId);
	    	console.log(vfile);
    	}
    	
    	if(scriptContext.fieldId == 'custpage_file'){
    		var vfiles = curRec.getValue(scriptContext.fieldId);
	    	console.log(vfiles);
	    	
    	}
    	
    	
    	if(scriptContext.fieldId == 'custpage_level'){
    		var output = url.resolveScript({
	    	    scriptId: 'customscript_rr_s2',
	    	    deploymentId: 'customdeploy_rr_s2'
	    	});
    		
    		var vlevel = window.nlapiGetFieldValue(scriptContext.fieldId);
    		output += '&level='+ vlevel;
    		
    		var brand = window.nlapiGetFieldValue('custpage_brand');
    		if(brand){
    			output += '&brand='+brand;
    		}
    	
    		
    		location = output;
    		
    	}
    
    	
    }
 
    function handleFiles(files) {
    	// Check for the various File API support.
    	if (window.FileReader) {
    		// FileReader are supported.
    		getAsText(files[0]);
    	} else {
    		alert('FileReader are not supported in this browser.');
    	}
    }

    function getAsText(fileToRead) {
    	var reader = new FileReader();
    	// Handle errors load
    	reader.onload = loadHandler;
    	reader.onerror = errorHandler;
    	// Read file into memory as UTF-8      
    	reader.readAsText(fileToRead);
    }

    function loadHandler(event) {
    	var csv = event.target.result;
    	processData(csv);             
    }

    function processData(csv) {
    	try{
	        var allTextLines = csv.split(/\r\n|\n/);
	        var lines = [];
	    	var jdata = [];
	    	var hcol = [];
	    	var dcol = [];
	    	
	    	for(var y = 0; y < 1; y++){
	    		var ld = allTextLines.shift().split(',');
	    		console.log(ld.length);
	    		for(var x = 0; x < ld.length; x++){
	    			if(ld.length > 20){
	    				throw 'error';
	    			}
	    			/**if(x <=6){
	    				hcol.push(ld[x]);
	    			}else{
	    				dcol.push(ld[x]);
	    			}	**/
	    			hcol.push(ld[x]);
	    		}
	    	}
	    	console.log(hcol);
	    	console.log(dcol);
	    	
	       /** while (allTextLines.length) {
	        	var ld = allTextLines.split(',');
	        	
	        	var data = {};
	        	data.header = {};
	        	data.detail = [];
	        	for(var x = 0; x < ld.length; x++){
	        		var h = {};
	        		h.id = x
	        		var d = {};
	        		h[hcol[x]] = ld[x];
	        		if(x <=6){
	        			h[hcol[x]] = ld[x];
	    			}else{
	    				dcol.push(ld[x]);
	    			}	
	        	}
	        	
	        	
	            lines.push(ld);
	        }**/
	    	
	        var cont = [];
	        while (allTextLines.length -1) {
	        	var ld = allTextLines.shift().split(',');
	        	var data = {};
	        	data.header = {};
	        	data.detail =[];
	        	for(var x = 0; x < ld.length; x++){
	        		var d = {};
	        		var field = hcol[x];
	        		if(field){
	        			if(x <=6){
		        			data.header[field] = ld[x];
		    			}else{
		    				d[field] = ld[x];
		    				data.detail.push(d);
		    			}	
	        		}else{
	        			throw 'out of bounce. cell'+ x;
	        		}	        	
	        	}

	        	cont.push(data);
	        	lines.push(ld);
	        }
	        
	        console.log(cont);
	        
	    	drawOutput(lines);
    	}catch(ex){
    		alert('invalid data <br/>' + ex);
    		location.reload();
    	}
    	
    }

    function errorHandler(evt) {
    	if(evt.target.error.name == "NotReadableError") {
    		alert("Canno't read file !");
    	}
    }

    function drawOutput(lines){
    	//Clear previous data
    	document.getElementById("output").innerHTML = "";
    	var table = document.createElement("table");
    	for (var i = 0; i < lines.length; i++) {
    		var row = table.insertRow(-1);
    		for (var j = 0; j < lines[i].length; j++) {
    			var firstNameCell = row.insertCell(-1);
    			firstNameCell.appendChild(document.createTextNode(lines[i][j]));
    		}
    	}
    	document.getElementById("output").appendChild(table);
    }

    
    function impClick(){
    	//var vfile = curRec.getValue('custpage_file');
    	var vfile = window.nlapiGetFieldValue('custpage_file');
    	console.log(vfile);
    	if(vfile){

    		var output = url.resolveScript({
	    	    scriptId: 'customscript_ft_import',
	    	    deploymentId: 'customdeploy_ft_import'
	    	});
    		
	    	var path = output + "&vfile=T";
	    	location = path;
    	}
    	
    	
    }
    
    function download(filename, text) {
        var pom = document.createElement('a');
        pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        pom.setAttribute('download', filename);

        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else {
            pom.click();
        }
    }
    

    function exclick(){
		swal({
		  title: 'Level 1!',
		  text: 'Coming soon.',
		  timer: 5000,
		  type:'success',
		  onOpen: function () {
		    swal.showLoading();
		  }
		});
    	
    }
    
    function exclick2(){
    	var url = window.nlapiResolveURL('SUITELET','customscript_rr_s2','customdeploy_rr_s2');
    	var wew = nlapiRequestURL(url,'POST');
    	download('gapanalysis.xml',wew.body);
    	
		swal({
		  text: 'Download Complete!.',
		  type:'success',
		  timer: 5000,
		  onOpen: function () {
		    swal.showLoading();
		  }
		}).then(
		  function () {},
		  // handling the promise rejection
		  function (dismiss) {
		    if (dismiss === 'timer') {
		      console.log('I was closed by the timer');
		    }
		  }
		);
    	
    	//nlExtOpenWindow('https://system.na2.netsuite.com/app/site/hosting/scriptlet.nl?script=1257&deploy=1&item=1073','Noeh GWAPO')
    	
    }
    
    function exclick3(){
    	window.parent.swal({
  		  title: 'Level 3!',
  		  text:'Coming Soon',
  		  timer: 5000,
  		  type:'success',
  		  onOpen: function () {
  		    window.parent.swal.showLoading();
  		  }
  		});
    }
    
    var parseQueryString = function() {

        var str = window.location.search;
        var objURL = {};

        str.replace(
            new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
            function( $0, $1, $2, $3 ){
                objURL[ $1 ] = $3;
            }
        );
        return objURL;
    };
    
    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        impClick:impClick,
        handleFiles: handleFiles,
        exclick: exclick,
        exclick2: exclick2,
        exclick3: exclick3
    };
    
});
