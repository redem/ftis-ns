var ENCODEMODULE, FILEMODULE, RUNTIMEMODULE, SEARCHMODULE, UIMODULE, URLMODULE, DEPLOYMENT_URL; 

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/encode', 'N/file', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url'],runNow);

/**
 * @param {encode} encode
 * @param {file} file
 * @param {runtime} runtime
 * @param {search} search
 * @param {serverWidget} serverWidget
 * @param {url} url
 */
function runNow(encode, file, runtime, search, serverWidget, url) {
	ENCODEMODULE= encode;
	FILEMODUL= file;
    RUNTIMEMODULE= runtime;
    SEARCHMODULE= search;
    UIMODULE = serverWidget;
    URLMODULE = url;

	  
	var returnObj = {};
	returnObj.onRequest = execute;
	return returnObj;
    
}

/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function execute(context) {
	new onRequest(context).runFunction();
}

/**
 * Gets deployment URL. Useful for sending POST requests to this same suitelet.
 * @returns {string} Deployment URL
 */
function getDeploymentURL(){
    return URLMODULE.resolveScript({
        scriptId: RUNTIMEMODULE.getCurrentScript().id,
        deploymentId: RUNTIMEMODULE.getCurrentScript().deploymentId,
        returnExternalUrl: false,
    });
}
 
function toBase64(stringInput){
    return ENCODEMODULE.convert({
        string: stringInput,
        inputEncoding: ENCODEMODULE.Encoding.UTF_8,
        outputEncoding: ENCODEMODULE.Encoding.BASE_64
    });
}


function onRequest(context) {
	this.request = context.request;
	this.response = context.response;

	this.runFunction = function() {
		
		if (this.request.method == 'GET') {
			this.GET();
		} else {
			this.POST();
		}
	};
	this.GET = function() {
		var ww = getDeploymentURL();
		this.response.write(ww);
	};
}