/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/record', 'N/runtime', 'N/search'],
/**
 * @param {format} format
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(format, record, runtime, search) {
	
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	
    	var newRec =  scriptContext.newRecord;
    	var subs = newRec.getValue('subsidiary');
    	try{
    		nyapiLog('subs',scriptContext.type);
        	if(scriptContext.type == 'create' || scriptContext.type == 'edit' || scriptContext.type == 'xedit'){
        		if(subs == 3){//TH only
                	var wew = newRec.getText('trandate');
                	var cur = newRec.getValue('currency');
                	var cf = newRec.getValue('ordertype');
                	
                	var xrate = getRate(wew,cur);
                	
                	nyapiLog(wew,cur);
                	
                	nyapiLog('rate',newRec.type);
                	        	
                	if(xrate.status){
            			switch(newRec.type){
            			case "invoice":
            				newRate = xrate.buying;
            				newRec.setValue({
                    		    fieldId: 'exchangerate',
                    		    value: newRate
                    		});
            				break;
            			case "itemreceipt":
            				if(cf == "RtnAuth"){
            					newRate = xrate.buying;
            				}else{
            					newRate = xrate.selling;
            				}
            				newRec.setValue({
                    		    fieldId: 'exchangerate',
                    		    value: newRate
                    		});
            				
            				break;
            			case "vendorreturnauthorization":
            				newRate = xrate.selling;
            				newRec.setValue({
                    		    fieldId: 'exchangerate',
                    		    value: newRate
                    		});
            				break;
            			case "returnauthorization":
            				newRate = xrate.buying;
            				newRec.setValue({
                    		    fieldId: 'exchangerate',
                    		    value: newRate
                    		});
            				break;
            			}
                	}   
            	}
        	}
        	
    		
    	}catch(exx){
    		nyapiLog('error bf submit',exx);
    	}
    	

    }
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function aftSubmit(scriptContext) {
    	var newRec =  scriptContext.newRecord;
    	var subs = newRec.getValue('subsidiary');
    	try{
    		nyapiLog('subs',subs);
        	if(scriptContext.type != 'delete'){
        		if(subs == 3){//TH only
            		var rec = record.load({
                 	    type: newRec.type, 
                 	    id: newRec.id,
                 	    isDynamic: true,
                 	});
                	
                	var wew = rec.getText('trandate');
                	var cur = rec.getValue('currency');
                	var cf = rec.getValue('ordertype');
                	
                	var rate = getRate(wew,cur);
                	
                	nyapiLog(wew,cur);
                	
                	nyapiLog(rate,newRec.type);
                	        	
                	if(rate.status){
                		var newRate = 0;
                	
            			switch(newRec.type){
            			case "invoice":
            				newRate = rate.buying;
            				break;
            			case "itemreceipt":
            				if(cf == "RtnAuth"){
            					newRate = rate.buying;
            				}else{
            					newRate = rate.selling;
            				}
            				
            				break;
            			case "vendorreturnauthorization":
            				newRate = rate.selling;
            				break;
            			case "returnauthorization":
            				newRate = rate.buying;
            				break;
            			case "vendorbill":
            				newRate = rate.selling;
            				break;
            			}
            			
            			
                		nyapiLog('rates',rate.selling);
                		rec.setValue({
                		    fieldId: 'exchangerate',
                		    value: newRate
                		});
                		
                		
                		
                	}   
            	}
        	}
        	
    		
    	}catch(exx){
    		nyapiLog('error aft submit',exx);
    	}
    	
    	
     	
    }
    
    function getRate(trandate,currency){
    	nyapiLog('date',trandate);
    	var container = {};
    	container.status = false;
    	
    	var customrecord_ft_curSearchObj = search.create({
    		   type: "customrecord_ft_cur",
    		   filters: [
    		      ["custrecord_cur_efdate","on",trandate],
    		      "AND",
    		      ["custrecord_cur_cur","anyof",currency]
    		   ],
    		   columns: [
    		      "custrecord_cur_sell",
    		      "custrecord_cur_buy",
    		      "custrecord_cur_efdate",
    		      "custrecord_cur_cur"
    		      
    		   ]
    		});
    		var searchResultCount = customrecord_ft_curSearchObj.runPaged().count;
    		if(searchResultCount){
    			container.status = true;
    			customrecord_ft_curSearchObj.run().each(function(result){
    	    		   // .run().each has a limit of 4,000 results
    				   container.selling = result.getValue('custrecord_cur_sell');
    				   container.buying = result.getValue('custrecord_cur_buy');
    	    		   return true;
    	    		});
    		}
    		
    		return container;
    }
    
    
	function nyapiLog(title_log, details_log) {
		log.error({
			title : title_log,
			details : details_log
		});
	}
    
    return {
    	beforeSubmit: beforeSubmit
    };
    
});
