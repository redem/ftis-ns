var SEARCH_ID = 'customsearch_folder_file_search';
var SCRIPT_PARAM = 'custscript1';

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @return {void}
 */

function updateInvNum(type) {
    var idFolder = nlapiGetContext().getSetting('SCRIPT',SCRIPT_PARAM);
  	var context = nlapiGetContext();
	var idFile;
    //var sScriptParam = oContext.getSetting('SCRIPT', 'custscript_ie_dashboard_tile');
    
	var aResult = nlapiSearchRecord('folder',SEARCH_ID);
	
	if(aResult != null) {
		var aColumns = aResult[0].getAllColumns();
		for(var i = 0; i < aResult.length; i++) {
			idFile = aResult[i].getValue(aColumns[1]);
			
			if(idFile)
				break;
		}
		nlapiLogExecution('DEBUG', 'fileID', idFile);
		var objFile = nlapiLoadFile(idFile);
		var delimiter = ',';
		var sContents = objFile.getValue();
		
		if(sContents) {
			var aContents = sContents.split('\n');
			
			for(var i = 1; i < aContents.length; i++) {
				var sLine = aContents[i];
				if(sLine.indexOf('\r') != -1) {
					sLine = sLine.replace('\r','');
					
					var aLine = sLine.split(',');
					
					if(aLine != null) {
						for(var k = 0; k < aLine.length; k++) {
							var idInv = aLine[0];
							var ccd = aLine[1];
							var ccn = aLine[2];
							var cce = aLine[3];
							var inv = nlapiLoadRecord('inventorynumber',idInv);
							inv.setFieldValue('custitemnumber_customclearance',ccd);
							inv.setFieldValue('custitemnumber_customclearancenum',ccn);
							inv.setFieldValue('custitemnumber_ftlot_customexchangerate', cce);
							nlapiSubmitRecord(inv);
							nlapiLogExecution('DEBUG', 'updatedRecord', idInv);
							                          
                          if ( context.getRemainingUsage() <= 100 && (i+1) < searchresults.length )
      {
         var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId())
         if ( status == 'QUEUED' )
            break;     
      }
						}
					}
				}
			}
		}
	}
}