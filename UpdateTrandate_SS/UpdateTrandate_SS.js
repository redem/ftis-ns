/**
 * Copyright NetSuite, Inc. 2014 All rights reserved. 
 * The following code is a demo prototype. Due to time constraints of a demo,
 * the code may contain bugs, may not accurately reflect user requirements 
 * and may not be the best approach. Actual implementation should not reuse 
 * this code without due verification.
 * 
 * (Module description here. Whole header length should not exceed 
 * 100 characters in width. Use another line if needed.)
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Sept 2016    gbautista
 * 
 */
 
var SEARCH_ID = 'customsearch_folder_file_search';
var SCRIPT_PARAM = 'custscript_ft_folder_id';

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @return {void}
 */
function fetchAndUpdateItemReceipts(type) {
    var idFolder = nlapiGetContext().getSetting('SCRIPT',SCRIPT_PARAM);
  	var context = nlapiGetContext();
	var idFile;
    //var sScriptParam = oContext.getSetting('SCRIPT', 'custscript_ie_dashboard_tile');
    
	var aResult = nlapiSearchRecord('folder',SEARCH_ID);
	
	if(aResult != null) {
		var aColumns = aResult[0].getAllColumns();
		for(var i = 0; i < aResult.length; i++) {
			idFile = aResult[i].getValue(aColumns[1]);
			
			if(idFile)
				break;
		}
		nlapiLogExecution('DEBUG', 'fileID', idFile);
		var objFile = nlapiLoadFile(idFile);
		var delimiter = ',';
		var sContents = objFile.getValue();
		
		if(sContents) {
			var aContents = sContents.split('\n');
			
			for(var i = 1; i < aContents.length; i++) {
				var sLine = aContents[i];
				if(sLine.indexOf('\r') != -1) {
					sLine = sLine.replace('\r','');
					
					var aLine = sLine.split(',');
					
					if(aLine != null) {
						for(var k = 0; k < aLine.length; k++) {
							var idReceipt = aLine[0];
							var sTranDate = aLine[1];
							
							nlapiSubmitField('itemreceipt',idReceipt,'exchangerate',sTranDate);
                                                        nlapiLogExecution('DEBUG', 'updatedRecord', idReceipt);
                          
                          if ( context.getRemainingUsage() <= 100 && (i+1) < searchresults.length )
      {
         var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId())
         if ( status == 'QUEUED' )
            break;     
      }
						}
					}
				}
			}
		}
	}
}