/**
 * Copyright � 2017, 2017, Oracle and/or its affiliates. All rights reserved.
 */

var VAT;
if (!VAT) VAT = {};
VAT.taxtrantype = "BILL";

VAT.LogError = function (ex, functionname) {
	var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
	nlapiLogExecution("ERROR", functionname, errorMsg);
};

//Business Logic Functions
//Setting the nexus NOTC
VAT.NexusNOTC = function () {
	this.run = function(mode) {
		if (VAT.taxtrantype != "RETURN" && VAT.taxtrantype != "RCPT" &&  VAT.taxtrantype != "CHK" && VAT.taxtrantype != "CREDMEM" &&
				VAT.taxtrantype != "INV" && VAT.taxtrantype != "SALESORD" && VAT.taxtrantype != "BILL" && VAT.taxtrantype != "BILLCRED") {return;}

		initNexusNOTC(mode);
	};

	function initNexusNOTC(mode) {
		try {
			if (mode == 'edit') { //Intrastat
				var nexus_notc = nlapiGetFieldValue('custpage_temp_nexus_notc');

				if (nexus_notc) {
					nlapiSetFieldValue('custbody_nexus_notc', nexus_notc);
				}
			}
		} catch (ex) {VAT.LogError(ex, "VAT.NexusNOTC.initNexusNOTC");}
	}
};

VAT.TaxFieldConductor = function TaxFieldConductor() {
	var hasVisibleSlaves = false;
	var fieldsFilteredByNexus = {
		'custbody_mode_of_transport': '',
		'custcol_statistical_procedure_sale' : '',
		'custcol_statistical_procedure_purc' : '',
		'custbody_transaction_region' : ''
	};

	function getFieldValues(list) {
		for (var f in list) {
			var field = nlapiGetField(f);
			if (field != null && !field.isHidden()) {
				hasVisibleSlaves = true;
				list[f] = nlapiGetFieldValue(f);
			}
		}
	}

	function restoreFieldValues(list) {
		for (var f in list) {
				nlapiSetFieldValue(f, list[f], false);
		}
	}

	this.run = function run() {
		getFieldValues(fieldsFilteredByNexus);

		if (hasVisibleSlaves) {
			nlapiSetFieldValue('custbody_itr_nexus', nlapiGetFieldValue('custpage_itr_nexus'), true, true);
			restoreFieldValues(fieldsFilteredByNexus);
		}
	};
};


//Declared Functions
function onPageInit(mode) {
	try {
		var currentcontext = nlapiGetContext().getExecutionContext();
		var executioncontext = VAT.Constants.ScriptMap["customscript_tax_tran_cs"].executioncontext;
		var clientcontext = executioncontext ? executioncontext.join(",") : "";
		if (clientcontext.indexOf(currentcontext) == -1) {return true;}

		//populate the tran VAT.taxtrantype
		var trantype = nlapiGetFieldValue("type");

		for(var imap in VAT.Constants.TransactionMap) {
			if (VAT.Constants.TransactionMap[imap].alternatecode == trantype) {
				VAT.taxtrantype = imap;
				break;
			}
		}

		new VAT.TaxFieldConductor().run();
		new VAT.NexusNOTC().run(mode);

	} catch(ex) {VAT.LogError(ex, "onPageInit");}
	return true;
}

function onSaveRecord() {
    try {
        var nexus = nlapiGetFieldValue('nexus_country');
        if(nexus == 'BE') {
            nlapiSetFieldValue('custbody_delivery_terms', nlapiGetFieldValue('custpage_delivery_terms'));
        }
    } catch (ex) {
        VAT.LogError(ex, "onSaveRecord");
    }
    return true;
}

var _countryNexusMap = {};
var NEXUS_FIELD_MAP = {
	documentNumber: {sublist: '', id: 'custbody_itr_doc_number', nexus:['CZ']}
};

function onFieldChanged(sublistid, fieldid, linenum) {
	if (fieldid !== 'nexus') {
		return;
	}

	var nexus = nlapiGetFieldValue('nexus');
	setDisplayFields(nexus);
}

function setDisplayFields(nexusId) {
	for (var f in NEXUS_FIELD_MAP) {
		var nexusField = NEXUS_FIELD_MAP[f];
		var field = nlapiGetField(nexusField.id);
		if (!field) {
			continue;
		}
		var country = _countryNexusMap[nexusId] ? _countryNexusMap[nexusId] : nlapiLookupField('nexus', nexusId, 'country');
		_countryNexusMap[nexusId] = country;

		if (nexusField.nexus.indexOf(country) > -1) {
			field.setDisplayType('normal');
		} else {
			field.setDisplayType('hidden');
		}
	}
}

function onMark(mark) {
	if (!nlapiGetLineItemField('item', 'custcol_5892_eutriangulation')) {
		alert('The EU Triangulation column is hidden on the transaction form used.\n\n' +
			  'To use the Mark/Clear EU Triangulation Column buttons, ' +
			  'edit the transaction form to show the EU Triangulation column.');
	}

	var itemCount = nlapiGetLineItemCount('item');
	if (itemCount == 0) {
		return;
	}

	for (var item = 1; item <= itemCount; item++) {
		nlapiSetLineItemValue('item', 'custcol_5892_eutriangulation', item, mark);
	}
	nlapiRefreshLineItems('item');
}