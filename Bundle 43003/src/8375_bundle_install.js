/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

function afterInstall(toversion) {
    // schedule script for generating the transaction field cache
    nlapiScheduleScript("customscript_generate_field_cache", "customdeploy_generate_field_cache");
}

function afterUpdate(fromversion, toversion) {
	var params = {};
	params["custscript_maintenance_stage"] = "start";
	nlapiScheduleScript("customscript_tax_bundle_maintenance", "customdeploy_tax_bundle_maintenance", params);
	
	// schedule script for generating the transaction field cache
	nlapiScheduleScript("customscript_generate_field_cache", "customdeploy_generate_field_cache");
}
