/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       08 Jan 2015     rwong            Initial
 * 2.00       29 Jan 2015     rwong            Updated highcharts definition and included support for drilldowns
 * 3.00       09 Feb 2015     rwong            Added min max configuration for datetime axis; Added min max for drilldowns
 *                                             Added support for resize of chart.
 **************************************************************************************************************************
 * 1.00       20 Feb 2015     rwong            Ported SPM to APM
 * 2.00       27 Feb 2015     rwong            Ported to Performance Search API
 * 4.00       03 Mar 2015     jmarimla         Fix date formatting
 * 5.00       10 Apr 2015     rwong            Updated tooltip format
 * 6.00       27 Apr 2015     jmarimla         Removed duplicate date on hover
 * 7.00       09 Jul 2015     jmarimla         Added drillup event, Added suitescript summary call on drilldown
 * 8.00       11 Aug 2015     rwong            Updated tooltip for SSA with support for drilldown
 *
 */

PSGP.APM.SSA.Highcharts = {

        perfChart : null,
        
        perfChartInterval : 1000,
        
        perfChartConfig : {},
        
        perfChartDrilldownConfig: {},

        renderPerfChart : function(chartData, config) {
            var containerId = 'psgp-apm-ssa-container-perfchart';

            var chartConfig = {
                    chart: {
                        renderTo: containerId,
                        type: 'column',
                        width: Ext4.getCmp(containerId).getWidth(),
                        height: Ext4.getCmp(containerId).getHeight(),
                        events: {
                            drilldown: function (e) {
                                if (!e.seriesOptions) {
                                    PSGP.APM.SSA.dataStores.suiteScriptParams.drilldownStartDate = e.point.dateObj.substring(0, 19);
                                    PSGP.APM.SSA.dataStores.suiteScriptParams.drilldownEndDate = e.point.nextDateObj.substring(0, 19);

                                    var chart = this;
                                    PSGP.APM.SSA.dataStores.suiteScriptParams.drilldown = 'T';
                                    PSGP.APM.SSA.dataStores.callPerfChartDrilldownRESTlet(chart, e.point);
                                    PSGP.APM.SSA.dataStores.callSuiteScriptSummaryRESTlet();
                                }
                            },
                            drillup: function () {
                                PSGP.APM.SSA.Highcharts.perfChartInterval = PSGP.APM.SSA.Highcharts.perfChartConfig.pointInterval;
                            	PSGP.APM.SSA.dataStores.suiteScriptParams.drilldown = 'F';
                                PSGP.APM.SSA.dataStores.callSuiteScriptSummaryRESTlet();
                            }
                        }
                    },

                    credits: {
                        enabled: false
                    },

                    title: {
                        text: 'SuiteScript Execution Over Time',
                        style : {
                            color : '#666',
                            fontFamily : 'Arial',
                            fontSize : '16px',
                            fontWeight : 'normal'
                        }
                    },

                    xAxis: {
                        type: 'datetime',
                        title: {
                            text: 'Timeline',
                            style : {
                                color : '#666',
                                fontFamily : 'Arial',
                                fontSize : '14px',
                                fontWeight: 'normal'
                            }
                        },
                        labels : {
                            style : {
                                color : '#666',
                                fontFamily : 'Arial',
                                fontSize : '11px',
                                fontWeight: 'normal'
                            }
                        },
                        min: config.xAxis.min,
                        max: config.xAxis.max,
                        dateTimeLabelFormats: {
                            second: '%l:%M:%S %p',
                            minute: '%l:%M %p',
                            hour: '%l:%M %p',
                            day: '%m/%d<br>%l:%M %p',
                            week: '%m/%d<br>%l:%M %p',
                            month: '%m/%d<br>%l:%M %p',
                            year: '%l:%M %p'
                        },
                        startOnTick: true,
                        endOnTick: true
                    },

                    yAxis: {
                        title: {
                            text: 'Execution Time',
                            style : {
                                color : '#666',
                                fontFamily : 'Arial',
                                fontSize : '14px',
                                fontWeight: 'normal'
                            }
                        },
                        labels : {
                            style : {
                                color : '#666',
                                fontFamily : 'Arial',
                                fontSize : '11px',
                                fontWeight: 'normal'
                            }
                        }
                    },

                    exporting: {
                        enabled: false
                    },

                    legend : {
                        enabled : false
                    },

                    tooltip: {
                        crosshairs: {
                            width: 1,
                            color: '#bbbbbb',
                            dashStyle: 'solid'
                        },
                        formatter: function () {
                            if(this.y == 0)
                                return false;

                            var table = '<table>';
                            
                            var groupAggMS = PSGP.APM.SSA.Highcharts.perfChartInterval;
                            var groupAggString = '';
                            if (groupAggMS < 1000*60*60) {
                                var value = groupAggMS / (1000*60);
                                var label = (value > 1) ? 'mins' : 'min';
                                groupAggString = '' + value + ' ' + label;
                            } else {
                                var value = groupAggMS / (1000*60*60);
                                var label = (value > 1) ? 'hrs' : 'hr';
                                groupAggString = '' + value + ' ' + label;
                            }
                            

                            Ext4.each(this.points, function() {

                            	var fromDate  = Highcharts.dateFormat('%l:%M %p', this.x);
                            	var toDate    = Highcharts.dateFormat('%l:%M %p', this.x + groupAggMS);
                            	var pointDate = Highcharts.dateFormat('%b %d %Y', this.x);
                                
                                table += '<tr><td align="center" colspan="3"><b>' + pointDate + '</b></td></tr>';
                                table += '<tr><td align="center" colspan="3"><b>' + groupAggString + ' from ' + fromDate + ' - ' + toDate + '</b></td></tr>'; 
                                table += '<tr><td align="center">Execution Time</td><td>:</td><td align="center">'+ this.y +'s</td></tr>';
                                table += '<tr><td align="center">Instance Count</td><td>:</td><td align="center">'+ this.point.logsTotal +'</td></tr>';
                            });

                            table += '</table>';

                            return table;
                        },
                        shared: true,
                        useHTML: true
                    },

                    plotOptions: {
                        column: {
                            pointPadding: 0.1,
                            borderWidth: 0,
                            groupPadding: 0
                        }
                    },

                    series: [{
                        name: 'Timeline',
                        data: chartData
                    }],

                    drilldown: {
                        series: []
                    }
            };

            if(this.perfChart) {
                this.perfChart.destroy();
            }

            this.perfChart = new Highcharts.Chart(chartConfig);
            this.perfChart.reflow();
        },

        resizePerfChart: function() {
            if (this.perfChart) {
                this.perfChart.setSize(Ext4.getCmp('psgp-apm-ssa-container-perfchart').getWidth(), Ext4.getCmp('psgp-apm-ssa-container-perfchart').getHeight());
            }
        }
};
