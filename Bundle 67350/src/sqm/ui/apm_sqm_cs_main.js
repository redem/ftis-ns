/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       20 Sep 2016     jmarimla         Initial
 *
 */

(function (initApplication) {
	
	initApplication($, Highcharts, window, document);
	
} ( function ($, Highcharts, window, document) {

	$(function() {
		APMSQM.mainPanel.adjustCSS();
		APMSQM.mainPanel.render();
	});
	
}) 
);