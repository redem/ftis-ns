/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Nov 2017     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	
	console.log(type);
}

/**
NS.jQuery(window).bind("load", function() {
	   // code here
	var tdColor = '#FF9494';
	var stable = NS.jQuery('#item_splits')[0];
    var ctbody = stable.children[0];
    var tbody = ctbody.children;
    //0 header
    //1 data
    var hasExpired = false;
    for(var x = 1; x < tbody.length; x++){
    	var cline = tbody[x].children[12];
    	var cex = tbody[x].children[13];
    	if(cex){
    		if(cex.innerText == "Yes"){
    			hasExpired = true;
        		cline.setAttribute(
            			'style',
            			//This is the magic CSS that changes the color
            			//	This is Same method used when NetSuite returns saved search results
            			//	with user defined row high lighting logic!
            			'background-color: '+tdColor+'!important;border-color: white '+tdColor+' '+tdColor+' '+tdColor+'!important;'
            	);
        	}
    	}
    }
    
    if(hasExpired){
    	var node = document.createElement("div");     
    	node.setAttribute('class','uir-record-status');
    	node.setAttribute('style',' color:Red;');
    	
    	var textnode = document.createTextNode("Price Expired");         // Create a text node
    	node.appendChild(textnode);  
    	NS.jQuery('#tbl_printlabel')[0].remove();
    	NS.jQuery('#tbl_menu_print')[0].remove();
    	NS.jQuery('.uir-page-title-secondline')[0].appendChild(node);
    	
    }
    
    /**var c = nlapiGetLineItemCount('item');
	nlapiLogExecution('ERROR','wew',c);
	for(var x =1; x<=c;x++){
		nlapiSelectLineItem('item',x);
		Searchitem(document.forms['item_form'].elements.item_display.value);
		nlapiCommitLineItem('item');
		window.item_machine.doAddEdit();
		
		
		
	} 
    

});
**/

//bob
function bobpo(){
	var url = window.nlapiResolveURL('SUITELET','customscript_bob_ss','customdeploy_bob_ss');
	var cfm = window.nlapiGetFieldValue('createdfrom');
	var tid = window.nlapiGetRecordId();
	var ttype= window.nlapiGetRecordType();
    window.open(url +'&method=potoso'+'&cfm='+tid+'&type='+ttype,'_blank');
    window.focus();
}

