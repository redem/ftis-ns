/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Nov 2017     noeh.canizares
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	var subl = nlapiGetLineItemField('item', 'custcol_ftis_ldr');
	subl.setDisplayType('hidden');
	if(type == 'view' || type == 'edit'){
		form.setScript('customscript_po_s1_cs');
	}
	
	
	
}
