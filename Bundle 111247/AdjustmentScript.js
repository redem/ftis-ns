/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Jun 2015     poojasekaran1
 *
 */

/**
 * Function to check for inventory adjustments and initiate the scheduled script to process the records.
 * ScriptType: Suitelet
 *
 * @param{nlobjRequest} requestRequest object
 * @param{nlobjResponse} responseResponse object
 * @returns{Void} Any output is written via response object
 */
function adjustInventory(request, response) {
    var method = request.getMethod();
    var form = nlapiCreateForm('Inventory Adjustment Form');
    var inlineHtml = form.addField('custpage_html', 'inlinehtml');
    var html = "";

    var columns = [];
    columns[columns.length] = new nlobjSearchColumn('internalid', null, 'count');

    var filter = new nlobjSearchFilter('custrecord_inv_count_processed', null, 'is', 'F');
    var results = nlapiSearchRecord('customrecord_inventory_counts', null, filter, columns); //governance: 10

    if (method == 'GET') {
        if (results) {
            var records = Number(results[0].getValue('internalid', null, 'count'));
            html += records + ' records found for processing';

            if (records > 0) {
                form.addSubmitButton('Process Records');
            }
        } else {
            html += "No records were found for processing in the Inventory Counts record."
        }
    } else if (method == 'POST') {
        //execute scheduled script
        nlapiScheduleScript('customscript_inv_adj_process_sched');
        html += "The Scheduled Script, Inventory Adjustment Process - Sched, has been queued. You will receive an email when the process is complete.<br />";
    }
    inlineHtml.setDefaultValue(html);
    response.writePage(form);
}

/**
 * Function to process inventory adjustments sitting in the InventoryCounts record
 * ScriptType: Scheduled
 */
function processInventoryAdjustments() {
    var context = nlapiGetContext();
    var email = "";
    var columns = [];
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_item');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_loc');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_adj_acct');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_date');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_adj_qty');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_cost');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_sub');
    columns[columns.length] = new nlobjSearchColumn('internalid').setSort();
    columns[columns.length] = new nlobjSearchColumn('custrecord_memo');
    columns[columns.length] = new nlobjSearchColumn('custrecord_header_memo');
    columns[columns.length] = new nlobjSearchColumn('custrecord_group');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_count_bin');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_item_status');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_lp');
    columns[columns.length] = new nlobjSearchColumn('custrecord_inv_lot_no');

    
    var filters = [];
    filters[0] = new nlobjSearchFilter('custrecord_inv_count_processed', null, 'is', 'F');

    var results = executeSearch('customrecord_inventory_counts', filters, columns);

    if (results) {
        var errors = [];
        var additions = {};
        var adjustments = [];
        var groupitems = {};
       // var grouparr = [];

        for (var i = 0; i < results.length; i++) {
            var sub = results[i].getValue('custrecord_inv_count_sub');
            var qty = Number(results[i].getValue('custrecord_inv_count_adj_qty'));
            var lotnumber= results[i].getValue('custrecord_inv_lot_no')

            var group = results[i].getValue('custrecord_group')|| 'na';
                   	 nlapiLogExecution('DEBUG', 'group', group);
                   	 
                   	 if (!(group in groupitems)) {
                   		groupitems[group] = {
                   				additions : {}
                   		};
                     }

                        if (!(sub in groupitems[group].additions)) {
                        	groupitems[group].additions[sub] = [];
                        }



                 groupitems[group].additions[sub].push({
                item: results[i].getValue('custrecord_inv_count_item'),
                //serial: results[i].getValue('custrecord_inv_count_serial'),
                location: results[i].getValue('custrecord_inv_count_loc'),
                account: results[i].getValue('custrecord_inv_count_adj_acct'),
                date: results[i].getValue('custrecord_inv_count_date'),
                quantity: results[i].getValue('custrecord_inv_count_adj_qty'),
                id: results[i].getId(),
                itemText: results[i].getText('custrecord_inv_count_item'),
                subsidiaryName: results[i].getText('custrecord_inv_count_sub'),
                locationName: results[i].getText('custrecord_inv_count_loc'),
                cost: results[i].getValue('custrecord_inv_count_cost'),
                memo: results[i].getValue('custrecord_memo'),
                headerMemo: results[i].getValue('custrecord_header_memo'),
                lotNumber: results[i].getValue('custrecord_inv_lot_no'),
                itemStatus: results[i].getValue('custrecord_inv_item_status'),
                binNumber: results[i].getValue('custrecord_inv_count_bin'),
                lpNumber: results[i].getValue('custrecord_inv_lp'),
                
                line: i + 1
            });

        }

        adjustments.push(additions);

        nlapiLogExecution('DEBUG', 'adjustments', JSON.stringify(adjustments));
        nlapiLogExecution('DEBUG', 'group items', JSON.stringify(groupitems));
        
        
        
    for(var grp in groupitems){   
// for (var x = 0; x < adjustments.length; x++) {
            //for (sub in adjustments[x]) {
            	for (sub in groupitems[grp].additions) {
                nlapiLogExecution('DEBUG', 'sub', sub);
                nlapiLogExecution('DEBUG', 'groupitems[grp].additions', JSON.stringify(groupitems[grp].additions));
                nlapiLogExecution('DEBUG', 'groupitems[grp].additions[sub][0].headerMemo', groupitems[grp].additions[sub][0].headerMemo);
                var adjustmentRecord = nlapiCreateRecord('inventoryadjustment', {'recordmode': 'dynamic'}); //governance: 10
                adjustmentRecord.setFieldValue('subsidiary', sub);
                
                adjustmentRecord.setFieldValue('memo', groupitems[grp].additions[sub][0].headerMemo); //header memo is assumed to be the same for every transaction
                adjustmentRecord.setFieldValue('account', groupitems[grp].additions[sub][0].account); //account is assumed to be the same for every transaction
                adjustmentRecord.setFieldValue('trandate', groupitems[grp].additions[sub][0].date); //date is assumed to be the same for every transaction
                
                
                //adjustmentRecord.setFieldValue('memo', adjustments[x][sub][0].headerMemo); //header memo is assumed to be the same for every transaction
                //adjustmentRecord.setFieldValue('account', adjustments[x][sub][0].account); //account is assumed to be the same for every transaction
                //adjustmentRecord.setFieldValue('trandate', adjustments[x][sub][0].date); //date is assumed to be the same for every transaction
                
                var processed = [];
                var exceptions = [];

                //for (var i = 0; i < adjustments[x][sub].length; i++) {
                    
                	  for (var i = 0; i < groupitems[grp].additions[sub].length; i++) {
                    try {
                        /*adjustmentRecord.selectNewLineItem('inventory');
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'item', adjustments[x][sub][i].item);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'location', adjustments[x][sub][i].location);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'adjustqtyby', adjustments[x][sub][i].quantity);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'unitcost', adjustments[x][sub][i].cost);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'memo', adjustments[x][sub][i].memo);
                        adjustmentRecord.commitLineItem('inventory');*/
                        
                        adjustmentRecord.selectNewLineItem('inventory');
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'item', groupitems[grp].additions[sub][i].item);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'location', groupitems[grp].additions[sub][i].location);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'adjustqtyby', groupitems[grp].additions[sub][i].quantity);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'unitcost', groupitems[grp].additions[sub][i].cost);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'memo', groupitems[grp].additions[sub][i].memo);
                        adjustmentRecord.setCurrentLineItemValue('inventory', 'serialnumbers', groupitems[grp].additions[sub][i].lotNumber );
                        
                        /*
                        var invDetailSubrecord = adjustmentRecord.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
                        invDetailSubrecord.selectNewLineItem('inventoryassignment');
                        invDetailSubrecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', 'EIOJNF98');

                        invDetailSubrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', groupitems[grp].additions[sub][i].quantity);
                        invDetailSubrecord.commitLineItem('inventoryassignment');
                        //invDetailSubrecord.cancel(); //undo this subrecord operation

                        //adjustmentRecord.commitLineItem('item'); // no subrecord is saved with this line.
*/
                        adjustmentRecord.commitLineItem('inventory');

                        /*processed.push({
                            id: adjustments[x][sub][i].id,
                            itemText: adjustments[x][sub][i].itemText
                                //serial: adjustments[x][sub][i].serial
                        });*/
                        
                        processed.push({
                            id: groupitems[grp].additions[sub][i].id,
                            itemText: groupitems[grp].additions[sub][i].itemText
                                //serial: adjustments[x][sub][i].serial
                        });

                        nlapiLogExecution('DEBUG', 'item', groupitems[grp].additions[sub][i].item);
                    } catch (e) {
                        exceptions.push({
                            error: e.getDetails(),
                            type: "EXCEPTION",
                            id: groupitems[grp].additions[sub][i].id,
                            line: groupitems[grp].additions[sub][i].line
                        });

                        nlapiLogExecution('DEBUG', 'exceptions array', JSON.stringify(exceptions));
                    }

                    pauseScriptIfNecessary(context);
                }

                try {
                    //submit the adjustment record only if at least 1 line was successfully added to the record
                    if (processed.length > 0) {
                        //try{
                        var id = nlapiSubmitRecord(adjustmentRecord, true, false); //governance: 10
                        var number = nlapiLookupField('inventoryadjustment', id, 'tranid'); //governance: 10
                        /*}catch (e) {
						exceptions.push({
							error: e,
							id: adjustments[x][sub][i].id
						});
					}*/

                        email += 'Inventory Adjustment #' + ' ' + number + ' successfully created with ' + processed.length + ' inventory adjustments.<br />';
                        nlapiLogExecution('DEBUG', 'Adjustment Created', number);

                        //update the processed flag for each adjustment
                        for (var i = 0; i < processed.length; i++) {
                            try {
                                nlapiSubmitField('customrecord_inventory_counts', processed[i].id, 'custrecord_inv_count_processed', 'T'); //governance: 10
                            } catch (ex) {
                                errors.push({
                                    error: ex,
                                    type: "ITEM",
                                    item: processed[i].itemText,
                                    number: processed[i].serial
                                });
                            }
                        }
                    }

                    //update the processed flag and exception message for each exception
                    for (var i = 0; i < exceptions.length; i++) {
                        try {
                            nlapiSubmitField('customrecord_inventory_counts', exceptions[i].id, ['custrecord_inv_count_processed', 'custrecord_inv_count_ex_msg'], ['T', exceptions[i].error]); //governance: 10
                        } catch (ex) {
                            errors.push({
                                error: ex,
                                type: "ITEM",
                                item: exceptions[i].itemText,
                                number: exceptions[i].serial
                            });
                        }
                    }
                } catch (e) {
                    nlapiLogExecution('ERROR', 'Error Saving Adjustment', e);
                    errors.push({
                        error: e.getDetails(),
                        type: "SUB",
                        subsidiary: groupitems[grp].additions[sub][0].subsidiaryName
                    });
                }

                pauseScriptIfNecessary(context);
            }
        //}
    }

        if (errors.length > 0) {
            email += '<br /><br />';
        }

        var subErrors = "";
        var itemErrors = "";
        var serialErrors = "";
        var exceptionErrors = "";

        for (var i = 0; i < errors.length; i++) {
            if (errors[i].type == "SUB") {
                subErrors += 'The following error occured while attempting to save the inventory adjustment for subsidiary ' + errors[i].subsidiary + ':<br />' + errors[i].error + '<br />';
            } else if (errors[i].type == "ITEM") {
                itemErrors += 'Unable to update the processed flag for serial number: ' + errors[i].number + '<br />';
            } else if (errors[i].type == "SERIAL") {
                serialErrors += 'Unable to process serial number ' + errors[i].number + '.<br />' + errors[i].error + '<br />';
            }
        }
        for (var j = 0; j < exceptions.length; j++) {

            if (exceptions[j].type == "EXCEPTION") {
                //exceptionErrors += 'Line' + exceptions[j].line  + ' of the import failed because of:' + exceptions[j].error'<br />';
                exceptionErrors += 'Line' + ' ' + exceptions[j].line + ' of the import failed because of ' + exceptions[j].error + '<br />';
            }
        }

        email += subErrors + itemErrors + serialErrors + exceptionErrors;

        nlapiSendEmail(context.getUser(), context.getUser(), 'Inventory Adjustment Process Complete', email);
    }
}

function pauseScriptIfNecessary(context) {
    if (context.getRemainingUsage() <= 100) {
        var state = nlapiYieldScript();

        if (state.status == 'FAILURE') {
            nlapiLogExecution('ERROR', 'Failed to yield script, exiting: Reason = ' + state.reason + ' / Size = ' + state.size);
            throw 'Failed to yield script';
        } else if (state.status == 'RESUME') {
            nlapiLogExecution('AUDIT', 'Resuming script because of ' + state.reason + '.  Size = ' + state.size);
        }
    }
}

/**
 * Thisfunction willexecute asearch againstthe providedrecord usingthe provided
 * columnsand filtersand will continously re-executeuntil allrecords areretrieved
 * 
 * @paramrecord
 * @paramfilters
 * @paramcolumns
 *@returns
 */
function executeSearch(record, filters, columns) {
    var results = nlapiSearchRecord(record, null, filters, columns);
    var completeResultSet = results; //container of the complete result set
    while (results && results.length == 1000) { //re-run the search if limit has been reached   
        var lastId = results[999].getValue('internalid'); //note the last record retrieved
        filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastId); //create new filter to restrict the next search based on the last record returned
        results = nlapiSearchRecord(record, null, filters, columns);

        if (results) {
            completeResultSet = completeResultSet.concat(results); //add the result to the complete result set
        }
    }
    return completeResultSet;
}

function userEventBeforeSubmit(type) {
    if (type == 'create' || type == 'edit' || type == 'xedit') {
        if (isNullOrEmpty(nlapiGetFieldValue('custrecord_inv_count_cost'))) {
            var location = null, item = null;

            if (type == 'xedit') {
                var fields = nlapiLookupField(nlapiGetRecordType(), nlapiGetRecordId(), ['custrecord_inv_count_loc', 'custrecord_inv_count_item']);
                location = fields['custrecord_inv_count_loc'];
                item = fields['custrecord_inv_count_item'];
            } else {
                location = nlapiGetFieldValue('custrecord_inv_count_loc');
                item = nlapiGetFieldValue('custrecord_inv_count_item');
            }

            nlapiLogExecution('DEBUG', 'location', location);
            nlapiLogExecution('DEBUG', 'item', item);

            var filters = [];
            filters.push(new nlobjSearchFilter('internalid', null, 'is', item));
            filters.push(new nlobjSearchFilter('inventorylocation', null, 'anyof', location));

            var columns = [];
            columns.push(new nlobjSearchColumn('locationquantityonhand'));
            columns.push(new nlobjSearchColumn('locationaveragecost'));
            columns.push(new nlobjSearchColumn('inventorylocation'));

            var results = nlapiSearchRecord('item', null, filters, columns);

            if (results) {
                nlapiLogExecution('DEBUG', 'results.length', results.length)
                for (var j = 0; j < results.length; j++) {
                    var loc = results[j].getValue('inventorylocation');
                    nlapiLogExecution('DEBUG', 'loc', loc);
                    //if(loc == location){
                    var qoh = results[j].getValue('locationquantityonhand');
                    nlapiLogExecution('DEBUG', 'qoh', qoh);
                    //}

                    var avecost;
                    if (qoh == '0' || isNullOrEmpty(qoh)) {
                        avecost = '0'
                    } else {
                        avecost = results[j].getValue('locationaveragecost');
                    }

                    nlapiLogExecution('DEBUG', 'avecost', avecost);

                    nlapiSetFieldValue('custrecord_inv_count_cost', avecost);

                }
            }
        }
    }
}

function isNullOrEmpty(val) {
    return (val == null || val === '');
}