//small label code

function eBiz_Custom_fn() {} 
function GetProductLabelSpecificPrintername(printertype)
{
	var printername="";

	var printerfilter= new Array();
	var userId;
	userId = nlapiGetUser();
	if(userId=="-4")
	{
		userId="";
	}
	nlapiLogExecution('Debug', 'DefaultUser', userId);
	var printerfilterarray = new Array();
	if(userId!=null && userId!="")
	{

		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_employee', null, 'anyof', userId));

	}
	else
	{
		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_carrier', null, 'is', "DefaultPrinter"));

	}
	printerfilter.push(new nlobjSearchFilter('name', null, 'is', printertype));
	var printercolumn = new Array();
	printercolumn.push(new nlobjSearchColumn('custrecord_printer_printername'));
	var printersearchresults = nlapiSearchRecord('customrecord_printer_preferences', null, printerfilter,printercolumn);
	if(printersearchresults!=null && printersearchresults!="")
	{
		printername=printersearchresults[0].getValue("custrecord_printer_printername");
	}


	return printername;

}
/*
 * Generate Product Label function required SmallLabel Id
 * Based on SmallLabel id we need to get the subsidiary, customer name, cust PO, cust pn, item, package num, qty, cartonnum, coo, enduserpn, weight from Item Fulfillment
 **/

function GenerateCustumerSpecificSmallLabel(vebizOrdNo,salesorderdetails,trantype,foname)
{
	nlapiLogExecution('Debug', 'Into GenerateCustumerSpecificSmallLabel', vebizOrdNo);
	//var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
	//var salesorderdetails =nlapiLoadRecord(trantype, vebizOrdNo);
	/*var fields = ['subsidiary', 'entity','custcol_ftis_cpn','custcol_ftis_carton_no','custcol_ftis_enduser_partnum','custcol_ftis_gross_weight','location','otherrefnum'];
	var columns = nlapiLookupField(trantype,vebizOrdNo,fields);
	var subsidiary=columns.subsidiary;
	var custname=columns.entity;
	var custpn=columns.custcol_ftis_cpn;
	var custPO=columns.otherrefnum;
	//var item=columns.itemname;
	//var packagenum=columns.packagenum;
	//var qty=columns.quantity;
	var cartonnum=columns.custcol_ftis_carton_no;
	var enduserpn=columns.custcol_ftis_enduser_partnum;
	var weight=columns.custcol_ftis_gross_weight;
	var location=columns.location;*/
	var smalllabelrequired;
	var customerid=salesorderdetails.getFieldValue('entity');
	//var customerrec=nlapiLoadRecord('customer', customerid);
	//if(customerrec!=null && customerrec!='')
	//{

		//smalllabelrequired=customerrec.getFieldValue('custentity_ftis_small_label');

	//}
	var fields = ['custentity_ftis_small_label'];

	var columns = nlapiLookupField('customer',customerid,fields);
	smalllabelrequired = columns.custentity_ftis_small_label;
	nlapiLogExecution('Debug', 'Into smalllabelrequired', smalllabelrequired);
	if(smalllabelrequired=='T')
	{
		var subsidiary=salesorderdetails.getFieldText('subsidiary');
		var custname=salesorderdetails.getFieldValue('attention');
		var custpn=salesorderdetails.getFieldText('custcol_ftis_cpn');
		var custPO=salesorderdetails.getFieldValue('otherrefnum');
		var cartonnum=salesorderdetails.getFieldValue('custcol_ftis_carton_no');
		var enduserpn=salesorderdetails.getFieldValue('custcol_ftis_enduser_partnum');
		var weight=salesorderdetails.getFieldValue('custcol_ftis_gross_weight');
		var location=salesorderdetails.getFieldValue('location');
		var salesOrder=salesorderdetails.getFieldValue('tranid');
		var cpn="";
		var coo;
		var item;
		var qty;
		var packagenum;
		var custPO;
		/**
		 * Printer Name must be get the Printer Detail custom record. Based on Label Type and user Printer Name can be populated in 
		 * - External Label printing custom record
		 * 
		 * */
		var printername=GetProductLabelSpecificPrintername("SmallLabel");
		/**
		 * Populate required information in Custom And Reference Fields.
		 * Populate LabelTemplate in LabelTemplate Custom field
		 * same external fields are used to populate. Based on Requirement we can use remaining fields

		 * */

		var waverfilterarray= new Array();
		//waverfilterarray.push( new nlobjSearchFilter('name', null, 'is', fullfillmentOrder));
		waverfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
		waverfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
		waverfilterarray.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));
		waverfilterarray.push(new nlobjSearchFilter('name', null, 'is',foname));
		waverfilterarray.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
		var wavecolumnarray = new Array();        
		wavecolumnarray[0]=new nlobjSearchColumn('custrecord_act_qty',null,'sum');
		wavecolumnarray[1]=new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
		wavecolumnarray[2]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
		wavecolumnarray[3]=new nlobjSearchColumn('custrecord_total_weight',null,'sum');
		wavecolumnarray[4]=new nlobjSearchColumn('name',null,'group');
		wavecolumnarray[5]=new nlobjSearchColumn('custrecord_sku', null, 'group');
		wavecolumnarray[6]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		//wavecolumnarray[3]=new nlobjSearchColumn('formulanumeric',null,'SUM');
		//wavecolumnarray[3].setFormula("TO_NUMBER({custrecord_total_weight})");
		var wavesearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waverfilterarray, wavecolumnarray);	

		if(wavesearchresults!=null && wavesearchresults!='')
		{
			var containercount=wavesearchresults.length;
			var count=1;
			var ParentExt= nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
			for(var x=0;x<wavesearchresults.length;x++)
			{

				var packagenum=containercount +" of "+ count;
				count++;
				var actqty=wavesearchresults[x].getValue('custrecord_act_qty',null,'sum');
				actqty=actqty +" of "+ actqty;
				var weight=wavesearchresults[x].getValue('custrecord_total_weight',null,'sum');
				var getCartonLPNo=wavesearchresults[x].getValue('custrecord_container_lp_no',null,'group');
				var fonumber=wavesearchresults[x].getValue('name',null,'group');
				var ItemId= wavesearchresults [x].getValue('custrecord_sku', null, 'group');
				 
				var itemname=wavesearchresults [x].getText('custrecord_sku', null, 'group');

				var expqty=wavesearchresults[x].getValue('custrecord_expe_qty',null,'sum');
				 
				var orignalexpqty=wavesearchresults[x].getValue('custrecord_expe_qty',null,'sum');
				//var item=getitembycontlp(vebizOrdNo,getCartonLPNo);

				var qty=fnSmallGetitemweight(ItemId,location);
			 

				var totalqty=0;
				var caseqty=0
				if(qty>0)
				{
					caseqty=parseFloat(expqty)/parseFloat(qty);
					caseqty=Math.ceil(caseqty);
					totalqty=caseqty.toFixed();
				}
				
				if(totalqty<=0)
				{
					totalqty=1;
				}
				nlapiLogExecution('Debug', 'ftisSmall-TotalQty-caseqty-LabelQty', totalqty+""+qty+""+caseqty);

				var remainqty;

				var totalcount=totalqty;
				var count=0;

				var lineitemcount=salesorderdetails.getLineItemCount('item');
				for(var p=1;p<=lineitemcount;p++)
				{
					var iteminternalid=salesorderdetails.getLineItemValue('item','item',p)
					if(iteminternalid==ItemId)
					{							
						cpn = salesorderdetails.getLineItemValue('item','custcol_ftis_cpn',p);
						 
						coo=salesorderdetails.getLineItemValue('item','custcol_country_of_origin',p);
					 

						custPO=salesorderdetails.getLineItemValue('item','custcol_ftis_cust_po',p);
					 
						break;


					}


				}
				var boxqty=0;
				var boxcount=1;
				for (var i=0; i<totalqty;i++) 
				{
					count++;
					if(parseInt(expqty)<parseInt(qty))
					{
						expqty=expqty;
					}
					else
					{
						expqty=parseInt(qty)*parseInt(boxcount);
					}
					boxcount++;

					//boxqty=parseInt(boxqty)+parseInt(qty)
					if(i>0)
					{

						if(i==(totalqty-1))
						{
							//expqty=parseInt(orignalexpqty)-parseInt(i*expqty);
							//expqty=parseInt(expqty)+parseInt((i*expqty));
							expqty=orignalexpqty;

						}
					}
					ParentExt.selectNewLineItem('recmachcustrecord_label_ext_parentlabel');
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom1',subsidiary);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom2',custname);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom3',cpn);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom4',custPO);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom5',itemname);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom6',count +" of "+totalcount);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom7',expqty +" of " +orignalexpqty);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom8',getCartonLPNo);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom9',enduserpn);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom10',weight);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_reference1',coo);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_ebizorder',vebizOrdNo);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_order',salesOrder);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_reference3',salesOrder);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_template',"SmallLabel")
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_ebizprinter',printername);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_labeltype',"SmallLabel");
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_ext_location',location);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','name',fonumber);
					
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_printoption',"T"); // AMU 07/01/2016 No shipping mark to print by default.
					
					ParentExt.commitLineItem('recmachcustrecord_label_ext_parentlabel');										 



				}
			}
			var submitid=nlapiSubmitRecord(ParentExt);
			nlapiLogExecution('Debug', 'Bulk-submitid', submitid);
		}
	}



}

function getitembycontlp (vebizOrdNo,vebizcontainerlp)
{
	nlapiLogExecution('Debug', 'into getitembycontlp : ', vebizcontainerlp);	
	var filters = new Array();

	var columns= new Array();


	filters.push(new nlobjSearchFilter('custrecord_container_lp_no',null,'is',vebizcontainerlp));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
	//filters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no','is', 'T'));


	columns[0]=new nlobjSearchColumn('custrecord_sku', null, 'group');

	var searchRes = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);

	var itemname;

	if(searchRes!=null && searchRes!='')
	{
		itemname = searchRes[0].getText('custrecord_sku', null, 'group');

		nlapiLogExecution('Debug', 'itemname is:',itemname );


	}
	return itemname;


}

//Getting case quantity  from Item dimensions
function fnSmallGetitemweight(itemid,location){
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', [itemid]));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	filter.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', [2]));	
	if(location!=null && location!='')
	{
		filter.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@',location]));	
	}

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;	
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	nlapiLogExecution('Debug', 'searchRec',searchRec);

	var casequantity=0;
	if(searchRec!=null && searchRec!='')
	{
		casequantity=searchRec[0].getValue('custrecord_ebizqty');		
	}
	nlapiLogExecution('Debug', 'casequantity',casequantity);
	return casequantity;

}
