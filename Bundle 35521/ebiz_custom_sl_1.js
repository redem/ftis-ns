function eBiz_Custom_fn() {} 

function generateCustomBartenderPickLabel(vwaveno) 
{
	nlapiLogExecution('ERROR', 'Generate O Label: ',vwaveno);



	var waverfilterarray= new Array();
	waverfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)));
	waverfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	waverfilterarray.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
	waverfilterarray.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is',"T"));	
	var wavecolumnarray = new Array();
	//wavecolumnarray [0] = new nlobjSearchColumn('custrecord_ebiz_clus_no', null, 'group');
	wavecolumnarray [0] = new nlobjSearchColumn('name', null, 'group');
	//wavecolumnarray [2] = new nlobjSearchColumn('custrecord_container', null, 'group');
	wavecolumnarray [1] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
	wavecolumnarray[2]=new nlobjSearchColumn('Internalid','custrecord_ebiz_order_no','group').setSort(true);
	wavecolumnarray[3]=new nlobjSearchColumn('custrecord_batch_no',null,'group');
	wavecolumnarray[4]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	wavecolumnarray[5]=new nlobjSearchColumn('custrecord_sku', null, 'group');
	

	//location
	wavecolumnarray[6]=new nlobjSearchColumn('custrecord_wms_location', null, 'group');
	
//	customer name
//	wavecolumnarray[9]=new nlobjSearchColumn('custbody_customername_nohierarchy','custrecord_ebiz_order_no''group'); //AMU
	var wavesearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waverfilterarray, wavecolumnarray);

	var cpn="";
	var vmiLocation ="";
	var vmiLocationText ="";
	
	
	var count=0;
	
	var printername = GetProductLabelSpecificPrintername("OLabel");
	if(wavesearchresults!=null && wavesearchresults!='')
	{
	    var totalcount = wavesearchresults.length;
	    nlapiLogExecution('ERROR', 'wavesearchresults length', totalcount);
		for(var g=0;g<wavesearchresults.length;g++)
		{
			count++;
			var vebizOrdNo=wavesearchresults[g].getValue('Internalid','custrecord_ebiz_order_no','group');
			nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
			var lotnum=wavesearchresults[g].getValue('custrecord_batch_no',null,'group');
			nlapiLogExecution('ERROR', 'lotnum', lotnum);
			var expqty=wavesearchresults[g].getValue('custrecord_expe_qty',null,'sum');
			nlapiLogExecution('ERROR', 'expqty', expqty);
			var orignalexpqty=wavesearchresults[g].getValue('custrecord_expe_qty',null,'sum');
			//Item Text
			var itemText= wavesearchresults [g].getText('custrecord_sku', null, 'group');
			nlapiLogExecution('ERROR', 'itemText', itemText);
			//Item Id
			var ItemId= wavesearchresults [g].getValue('custrecord_sku', null, 'group');
			nlapiLogExecution('ERROR', 'ItemId', ItemId);


			var wmslocation=wavesearchresults[g].getValue('custrecord_wms_location', null, 'group');
			nlapiLogExecution('ERROR', 'wmslocation', wmslocation);
			
			var containerlp=wavesearchresults[g].getValue('custrecord_container_lp_no', null, 'group');
			nlapiLogExecution('ERROR', 'containerlp', containerlp);

			var itemfulfillmentlist= Itemfulfillmentlist(vebizOrdNo);
			nlapiLogExecution('ERROR', 'itemfulfillmentlist', itemfulfillmentlist);
			var customerpo;
			try
			{
				if(itemfulfillmentlist != null && itemfulfillmentlist!='')
				{
					var shiptoAddress1,shiptoAddress2,shiptocity,shiptostate,shiptocountry,shiptocompany,shiptozipcode;
					var attention=itemfulfillmentlist[0].getValue('shipattention');
					var companyid=itemfulfillmentlist[0].getValue('custbody_nswms_company');
					var companyname=itemfulfillmentlist[0].getText('custbody_nswms_company');
					var shipaddress=itemfulfillmentlist[0].getValue('shipaddressee');
					nlapiLogExecution('ERROR', 'companyname', companyid);
					shiptoAddress1=itemfulfillmentlist[0].getValue('shipaddress1');
					shiptoAddress2=itemfulfillmentlist[0].getValue('shipaddress2');
					shiptocity=itemfulfillmentlist[0].getValue('shipcity');
					shiptostate=itemfulfillmentlist[0].getValue('shipstate');
					shiptocountry=itemfulfillmentlist[0].getValue('shipcountry');
					shiptocompany=itemfulfillmentlist[0].getText('entity'); // AMU EDIT
					shiptozipcode=itemfulfillmentlist[0].getValue('shipzip');
					var location=itemfulfillmentlist[0].getValue('location');
					var locationtext=itemfulfillmentlist[0].getText('location');
					orderno=itemfulfillmentlist[0].getValue('tranid');
					//FTIS O Label Custom fields
					//cust part number
					var partnum=itemfulfillmentlist[0].getValue('custcol_ftis_cpn');
					// FTIS customer revision 
					var custrev=itemfulfillmentlist[0].getValue('custcol_ftis_cust_rev'); //Added by AMU 10/06/2016
// FTIS customer db location
					var dblocation=itemfulfillmentlist[0].getValue('custcol_ftis_database_location'); //Added by AMU 04/07/2016
					//cust PO number
					var custpo=itemfulfillmentlist[0].getValue('custcol_ftis_cust_po');
					//Item Fulfillment number
					var itemful=itemfulfillmentlist[0].getValue('tranid');
					//ship date
					var shipdate=itemfulfillmentlist[0].getValue('shipdate');
					//Customer PO#
					var ponum=itemfulfillmentlist[0].getValue('otherrefnum');

					//Third party part number
					var thirdpartypn=itemfulfillmentlist[0].getValue('custbody_ftis_enduser_pn');
					
				}
				//Search for Location address
				var shipfromcity,shipfromcountry,shipfromzipcode,shipfromaddress,shipfromphone,shipfromstate;
				//Search for ShipFromAdress  from Locations 
				var locationadress =nlapiLoadRecord('Location',location);

				var shipattention="";

				if((locationadress !="") && (locationadress !=null))
				{


					shipfromaddress=locationadress.getFieldValue('addr1');

					if((shipfromaddress==null)||(shipfromaddress==''))
					{
						shipfromaddress='';
					}
					var addr2=locationadress.getFieldValue('addr2');

					if((addr2==null)||(addr2==''))
					{
						addr2='';
					}
					shipfromaddress=shipfromaddress+" " + addr2;



					shipfromcity=locationadress.getFieldValue('city');
					if((shipfromcity==null)||(shipfromcity))
					{
						shipfromcity='';
					}

					shipfromstate=locationadress.getFieldValue('state');
					if((shipfromstate==null)||(shipfromstate==''))
					{
						shipfromstate='';
					}

					shipfromzipcode =locationadress.getFieldValue('zip');
					if((shipfromzipcode==null)||(shipfromzipcode==''))
					{
						shipfromzipcode='';
					}

					companyname=locationadress.getFieldValue('addressee');
					if((companyname==null)||(companyname==''))
					{
						companyname='';
					}

					shipfromphone=locationadress.getFieldValue('addrphone');
					if((shipfromphone==null)||(shipfromphone==''))
					{
						shipfromphone='';
					}

					shipfromcountry =locationadress.getFieldValue('country');
					if((shipfromcountry==null)||(shipfromcountry==''))
					{
						shipfromcountry='';
					}
					
					
				}

				//item level fileds
				var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
				var salesorderdetails =nlapiLoadRecord(trantype, vebizOrdNo);
				shipattention=salesorderdetails.getFieldValue('shipattention');
				var lineitemcount=salesorderdetails.getLineItemCount('item');
				for(var p=1;p<=lineitemcount;p++)
				{
					vmiLocationText = "";
					var iteminternalid=salesorderdetails.getLineItemValue('item','item',p)
					if(iteminternalid==ItemId)
					{							
						cpn = salesorderdetails.getLineItemValue('item','custcol_ftis_cpn',p);
						nlapiLogExecution('DEBUG', 'cpn', cpn);
						customerpo=salesorderdetails.getLineItemValue('item','custcol_ftis_cust_po',p);
						nlapiLogExecution('DEBUG', 'customerpo', customerpo);
						custrev = salesorderdetails.getLineItemValue('item','custcol_ftis_cust_rev',p); // Added by AMU 10/06/2016
						nlapiLogExecution('DEBUG', 'custrev', custrev);

dblocation= salesorderdetails.getLineItemValue('item','custcol_ftis_database_location',p); // Added by AMU 04/07/2016
						nlapiLogExecution('DEBUG', 'dblocation', dblocation);
						
						
						if(trantype == 'transferorder'){
							vmiLocation = salesorderdetails.getFieldValue('transferlocation');
							if(vmiLocation == '11' || vmiLocation == '18'){
								var custbinNumber = salesorderdetails.getLineItemValue('item','custcol_cust_bin_num_to',p);
								nlapiLogExecution('DEBUG', 'custbinNumber', custbinNumber);
								if(custbinNumber == null || custbinNumber == undefined) {custbinNumber ='';}
								vmiLocationText = "VMI "+ custbinNumber;
							}
						}
						
						break;


					}


				}

				
				var labeltype="OLabel";

				var qty=fnGetitemweight(ItemId,wmslocation);
				var totalqty = 0;
				var caseqty = 0
				if (qty > 0) 
                {
				    caseqty = parseFloat(expqty) / parseFloat(qty);
				    caseqty = Math.ceil(caseqty);
				    totalqty = caseqty.toFixed();
				}

				if (totalqty <= 0) 
                {
				    totalqty = 1;
				}
				nlapiLogExecution('Debug', 'ftisshippingmark-TotalQty-caseqty-LabelQty', totalqty + "" + qty + "" + caseqty);

				var remainqty;

				var totalcount=totalqty;
				var count=0;
				 
				for (var i=0; i<totalqty;i++) 
				{
					count++;
					var extlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting'); 
					extlabelrecord.setFieldValue('custrecord_label_ebizorder',vebizOrdNo);
					extlabelrecord.setFieldValue('custrecord_label_order',orderno); 

					extlabelrecord.setFieldValue('custrecord_label_addr1',shipfromaddress); 
					extlabelrecord.setFieldValue('custrecord_label_state',shipfromstate);
					extlabelrecord.setFieldValue('custrecord_label_zip',shipfromzipcode);
					extlabelrecord.setFieldValue('custrecord_label_city',shipfromcity);
					extlabelrecord.setFieldValue('custrecord_label_labeltype',labeltype);

					extlabelrecord.setFieldValue('name',orderno);  
					//Ship to Adress
					extlabelrecord.setFieldValue('custrecord_label_shipaddressee',shipattention); 
					extlabelrecord.setFieldValue('custrecord_label_shipaddr1',shiptoAddress1); 
					extlabelrecord.setFieldValue('custrecord_label_addr2',shiptoAddress2); 

					extlabelrecord.setFieldValue('custrecord_label_shipcity',shiptocity); 
					extlabelrecord.setFieldValue('custrecord_label_shipstate',shiptostate); 
					extlabelrecord.setFieldValue('custrecord_label_shipcountry',shiptocountry); 
					extlabelrecord.setFieldValue('custrecord_label_shipzip',shiptozipcode); 
					extlabelrecord.setFieldValue('custrecord_label_shipattention',shipaddress);

					//FTIS extlabel record
					extlabelrecord.setFieldValue('custrecord_label_custom10',cpn);
					extlabelrecord.setFieldValue('custrecord_label_item',itemText);
					//extlabelrecord.setFieldValue('custrecord_label_custom1',custpo); AMU COMMENTED on 04/07/2016 as its duplicate with CUSTOM2 and CUSTOM1 is not mapped in O LABEL TEMPLATE. reused for dblocation
extlabelrecord.setFieldValue('custrecord_label_custom1',dblocation); 
					extlabelrecord.setFieldValue('custrecord_label_custom2',custpo);
					extlabelrecord.setFieldValue('custrecord_label_custom3',itemful);
					extlabelrecord.setFieldValue('custrecord_label_custom4',shipdate);
					expqty=qty;
					if(i>0)
					{

						if(i==(totalqty-1))
						{
							expqty=parseInt(orignalexpqty)-parseInt(i*expqty);
						}
					}
					extlabelrecord.setFieldValue('custrecord_label_custom5',expqty);
					extlabelrecord.setFieldValue('custrecord_label_custom6',lotnum);
					extlabelrecord.setFieldValue('custrecord_label_custom7',thirdpartypn);
					extlabelrecord.setFieldValue('custrecord_label_custom8',count +" of "+totalcount);
					extlabelrecord.setFieldValue('custrecord_label_custom9', vwaveno);
					//extlabelrecord.setFieldValue('custrecord_label_custom10', lpnum);
					extlabelrecord.setFieldValue('custrecord_label_ebizprinter',printername);
					extlabelrecord.setFieldValue('custrecord_label_reference3',customerpo);
					//template name as label name		
					extlabelrecord.setFieldValue('custrecord_label_template',"OLabel");
					//Location 
					extlabelrecord.setFieldValue('custrecord_label_ext_location',wmslocation);
					extlabelrecord.setFieldValue('custrecord_label_reference4',companyname);
					extlabelrecord.setFieldValue('custrecord_label_reference1',containerlp);
					extlabelrecord.setFieldValue('custrecord_label_licenseplatenumber',containerlp);
					extlabelrecord.setFieldValue('custrecord_label_reference2',custrev); // Added by AMU 10/06/2016
					extlabelrecord.setFieldValue('custrecord_label_reference5',vmiLocationText); // Added by AMU 10/06/2016
					

					var tranid = nlapiSubmitRecord(extlabelrecord);
				}



			}
			catch(exp)
			{
				nlapiLogExecution('ERROR', 'exception at O label', exp);
			}
		}

	}


}

function Itemfulfillmentlist(vebizOrdNo) {
	nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('otherrefnum');
	columns[1] = new nlobjSearchColumn('custbody_nswms_company');
	columns[2] = new nlobjSearchColumn('shipaddress1');
	columns[3] = new nlobjSearchColumn('shipaddress2');
	columns[4] = new nlobjSearchColumn('shipcity');
	columns[5] = new nlobjSearchColumn('shipstate');
	columns[6] = new nlobjSearchColumn('shipcountry');
	columns[7] = new nlobjSearchColumn('entity');
	columns[8] = new nlobjSearchColumn('shipzip');
	//custom part numner
	columns[9] = new nlobjSearchColumn('custcol_ftis_cpn');
	//columns[10] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[10] = new nlobjSearchColumn('location');
	columns[11] =new nlobjSearchColumn('tranid');
	columns[12] =new nlobjSearchColumn('shipattention');
	//cust col number
	//cust PO number
	columns[13] =new nlobjSearchColumn('custcol_ftis_cust_po');
	//ship date
	columns[14] =new nlobjSearchColumn('trandate');
	//
	//columns[15] =new nlobjSearchColumn('entity'); // Incorrect Column Index used in original code. moved to index 18.



	//Third party part number
	columns[15] =new nlobjSearchColumn('custbody_ftis_enduser_pn');

	columns[16] =new nlobjSearchColumn('shipaddressee');
	columns[17] =new nlobjSearchColumn('shipdate');
	columns[18] =new nlobjSearchColumn('entity'); // Incorrect Column Index used in original code. moved to index 18.
	columns[19] =new nlobjSearchColumn('custcol_ftis_cust_rev'); // Added by AMU 10/06/2016
columns[20] =new nlobjSearchColumn('custcol_ftis_database_location'); // Added by AMU 04/07/2016

	
	var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
	nlapiLogExecution('ERROR', 'trantype', trantype);
	var itemfulfillmentlist = nlapiSearchRecord(trantype, null, filters, columns);
	return itemfulfillmentlist ;
}
function GetProductLabelSpecificPrintername(printertype)
{
	var printername="";

	var printerfilter= new Array();
	var userId;
	userId = nlapiGetUser();
	if(userId=="-4")
	{
		userId="";
	}
	nlapiLogExecution('Debug', 'DefaultUser', userId);
	var printerfilterarray = new Array();
	if(userId!=null && userId!="")
	{

		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_employee', null, 'anyof', userId));

	}
	else
	{
		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_carrier', null, 'is', "DefaultPrinter"));

	}
	printerfilter.push(new nlobjSearchFilter('name', null, 'is', printertype));
	var printercolumn = new Array();
	printercolumn.push(new nlobjSearchColumn('custrecord_printer_printername'));
	var printersearchresults = nlapiSearchRecord('customrecord_printer_preferences', null, printerfilter,printercolumn);
	if(printersearchresults!=null && printersearchresults!="")
	{
		printername=printersearchresults[0].getValue("custrecord_printer_printername");
	}


	return printername;

}


//Getting quantity  from Item dimensions
function fnGetitemweight(itemid,location){
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', [itemid]));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	filter.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', [2]));	
	if(location!=null && location!='')
	{
		filter.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@',location]));	
	}

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;	
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	nlapiLogExecution('Debug', 'searchRec',searchRec);

	var casequantity='';
	if(searchRec!=null && searchRec!='')
	{
		casequantity=searchRec[0].getValue('custrecord_ebizqty');		
	}
	nlapiLogExecution('Debug', 'casequantity',casequantity);
	return casequantity;

}



