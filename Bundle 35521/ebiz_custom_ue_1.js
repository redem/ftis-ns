
function eBiz_Custom_fn(type){
	nlapiLogExecution('DEBUG', 'type', type);

	if (type == 'create') 
	{     
		try
		{

			var rec = nlapiGetNewRecord();

			var recId = rec.getId();
			var itemValue = rec.getFieldValue('custrecord_ebizsku');
			var vlotexpiry = rec.getFieldValue('custrecord_ebizexpirydate');
			var vName = rec.getFieldValue('name');

			var str = ' type. ' + type + '<br>' ;
			str = str + 'recId.' + recId + '<br>';
			str = str + 'temValue. ' + itemValue + '<br>';
			str = str + 'vName. ' + vName + '<br>';
			str = str + 'vlotexpiry. ' + vlotexpiry + '<br>';

			nlapiLogExecution("Error", "New Rec Value",str);

			//

			var vColumn = nlapiLookupField('item',itemValue,['custitem_ftismfgdatecode','custitem_ebiz_item_cap_expiry_date']);

			var vMfgCode = vColumn.custitem_ftismfgdatecode;
			var vCaptureExpiry = vColumn.custitem_ebiz_item_cap_expiry_date;

			nlapiLogExecution("Error", "vMfgCode",vMfgCode);
			nlapiLogExecution("Error", "vCaptureExpiry",vCaptureExpiry);


			var vExpiryDate = '';
			var vPasreString = '';

			vPasreString = vName.substring(0,parseInt(vMfgCode));

			nlapiLogExecution("Error", "vPasreString",vPasreString);

			if(vPasreString==null || vPasreString=='')
			{
				vPasreString=0;
				nlapiLogExecution("Error", "if vPasreString",vPasreString);
			}



			if(!(isNaN(vPasreString)))
			{
				vExpiryDate = addDays(parseFloat(vPasreString));
				nlapiLogExecution("Error", "if vExpiryDate",vExpiryDate);
			}
			else
			{
				vExpiryDate = '';
				vPasreString='';
			}


			nlapiLogExecution("Error", "vExpiryDate",vExpiryDate);

			if(vCaptureExpiry!='T' || (vlotexpiry==null || vlotexpiry==''))
			{
				var vLoadRec = nlapiLoadRecord('customrecord_ebiznet_batch_entry',recId);
				if(vPasreString!='' && vPasreString!=null)
					vLoadRec.setFieldValue('custrecord_ebizbatchseqno',vPasreString);

				vLoadRec.setFieldValue('custrecord_ebizexpirydate',vExpiryDate);
				nlapiSubmitRecord(vLoadRec, false, true);
			}

		}
		catch(e)
		{
			var vLoadRec = nlapiLoadRecord('customrecord_ebiznet_batch_entry',recId);
			vLoadRec.setFieldValue('custrecord_ebizbatchseqno',vPasreString);
			vLoadRec.setFieldValue('custrecord_ebizexpirydate','');
			nlapiSubmitRecord(vLoadRec, false, true);
			nlapiLogExecution('Error', 'Exception in Lot Expiry Date Determination',e);
		}
	}
}

function addDays(NoofHours)
{
	nlapiLogExecution('Error', 'addDays NoofHours',NoofHours);
	if(NoofHours!=null && NoofHours!='' && NoofHours>0)
	{
		var now = new Date('11/29/2016');

		var NoofDays=parseFloat(NoofHours)/24;
		NoofDays = Math.ceil(NoofDays);
		nlapiLogExecution('Error', 'NoofDays',NoofDays);
		var curdate = ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());

		var newdate = nlapiAddDays(new Date(curdate), NoofDays);

		var dtsettingFlag = DateSetting();

		nlapiLogExecution('Error', 'dtsettingFlag',dtsettingFlag);

		if(dtsettingFlag == 'DD/MM/YYYY')
		{
			return ((parseFloat(newdate.getDate())) + '/' + (parseFloat(newdate.getMonth()) + 1) + '/' +newdate.getFullYear());
		}
		else
		{
			return ((parseFloat(newdate.getMonth()) + 1) + '/' + (parseFloat(newdate.getDate())) + '/' + newdate.getFullYear());
		}
	}
	else
	{
		//var vdefdate='04/01/2016';
		var vdefdate='';
		return vdefdate;
	}

}

/*function getinventoryid(lotnumber,vExpiryDate)
{
	nlapiLogExecution('DEBUG', 'into get getinventoryid', lotnumber);
	nlapiLogExecution('DEBUG', 'into vExpiryDate', vExpiryDate);
	var inventoryfilter = new Array();
	inventoryfilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', lotnumber));
	var inventorycolumns = new Array();
	inventorycolumns[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	var searchinventoryrecords = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryfilter, inventorycolumns);
	if (searchinventoryrecords != null) {
		for (var v = 0; v < searchinventoryrecords.length; v++) {
			var inventoryinternalid = searchinventoryrecords[v].getId();           
			var inventorySetvalue = nlapiLoadRecord('customrecord_ebiznet_createinv', inventoryinternalid);
			inventorySetvalue.setFieldValue('custrecord_ebiz_expdate', vExpiryDate);
			
			var id = nlapiSubmitRecord(inventorySetvalue, true);

		}
	}
}*/


