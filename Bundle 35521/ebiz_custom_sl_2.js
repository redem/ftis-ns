function eBiz_Custom_fn() {
}
function generatecustomitemlabel(line, item, poid, wmslocation, newRecord) {
	// nlapiLogExecution('Debug', 'Generate ItemLabel',poid+","+item);
	// var Purchaseorderlist= PurchaseorderReslist(line,item,poid);
	try {
		// if(Purchaseorderlist != null && Purchaseorderlist != '')
		// {

		// FTIS I Label Custom fields
		// Stockcode

		// var itemnum=Purchaseorderlist[0].getText('custrecord_sku');
		var itemnum = newRecord.getFieldText('custrecord_sku');
		// PO #
		// var ponum=Purchaseorderlist[0].getValue('custrecord_ebiz_order_no');
		var ponum = newRecord.getFieldValue('custrecord_ebiz_order_no');
		// Datecode
		// var dc=Purchaseorderlist[0].getValue('custrecord_batch_no');
		var dc = newRecord.getFieldValue('custrecord_batch_no');
		// Quantity
		// var expqty=Purchaseorderlist[0].getValue('custrecord_expe_qty');
		var expqty = newRecord.getFieldValue('custrecord_expe_qty');
		// var
		// orignalexpqty=Purchaseorderlist[0].getValue('custrecord_expe_qty');
		var orignalexpqty = newRecord.getFieldValue('custrecord_expe_qty');
		// Line number
		// var linenum=Purchaseorderlist[0].getValue('custrecord_line_no');
		var linenum = newRecord.getFieldValue('custrecord_line_no');
		// Licence Plate number
		// var lpnum=Purchaseorderlist[0].getValue('custrecord_lpno');
		var lpnum = newRecord.getFieldValue('custrecord_lpno');
		// Supplier part number
		// var supplierpn=Purchaseorderlist[0].getValue('custrecord_sku');
		var supplierpn = newRecord.getFieldValue('custrecord_sku');
		// Wave number
		// Wave number not availabel in INBOUND process
		// var wavenum=Purchaseorderlist[0].getValue('custrecord_ebiz_wave_no');
		var wavenum = newRecord.getFieldValue('custrecord_ebiz_wave_no');
		// orderno
		// var orderno =
		// Purchaseorderlist[0].getValue('custrecord_ebiz_order_no');
		var orderno = newRecord.getFieldValue('custrecord_ebiz_order_no');
		var ordertext = newRecord.getFieldText('custrecord_ebiz_order_no');
		var itemfulfillmentId = newRecord.getFieldValue('name');
		// var ordertext =
		// Purchaseorderlist[0].getText('custrecord_ebiz_order_no');
		// itemfulfillmentId
		// var itemfulfillmentId = Purchaseorderlist[0].getValue('name');

		var labeltype = "Ilabel";
		var printername = GetProductLabelSpecificPrintername("Ilabel");
		var vUOM = "CASE";
		var qty = fnGetitemweight(item);
		nlapiLogExecution('DEBUG', 'Quantity from item dimesions', qty);
		nlapiLogExecution('DEBUG', 'expqty', expqty);

		var caseqty = parseFloat(expqty) / parseFloat(qty);
		if (caseqty > 0) {
			caseqty = Math.ceil(caseqty);
		}
		var totalqty = caseqty.toFixed();
		if (totalqty == 0) {
			totalqty = 1;
		}

		var remainqty;

		var totalcount = totalqty;
		var count = 0;
		nlapiLogExecution('DEBUG', 'totalqty', totalqty);
		for (var i = 0; i < totalqty; i++) {
			count++;
			var extlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting');
			extlabelrecord.setFieldValue('custrecord_label_ebizorder', orderno);
			extlabelrecord.setFieldValue('custrecord_label_order', ordertext);

			extlabelrecord.setFieldValue('custrecord_label_labeltype',
					labeltype);

			extlabelrecord.setFieldValue('name', orderno);
			// Ship to Adress

			// FTIS I extlabel record
			extlabelrecord.setFieldValue('custrecord_label_custom1', itemnum);
			extlabelrecord.setFieldValue('custrecord_label_custom2', ponum);
			extlabelrecord.setFieldValue('custrecord_label_custom3', dc);
			expqty = qty;
			if (i > 0) {

				if (i == (totalqty - 1)) {
					expqty = parseInt(orignalexpqty) - parseInt(i * expqty);
				}
			}
			extlabelrecord.setFieldValue('custrecord_label_custom4', expqty);
			extlabelrecord.setFieldValue('custrecord_label_custom5', linenum);
			extlabelrecord.setFieldValue('custrecord_label_custom6', lpnum);
			extlabelrecord
					.setFieldValue('custrecord_label_custom7', supplierpn);
			extlabelrecord.setFieldValue('custrecord_label_custom8', wavenum);
			extlabelrecord.setFieldValue('custrecord_label_ebizprinter',
					printername);
			extlabelrecord.setFieldValue('custrecord_label_ext_location',
					wmslocation);
			extlabelrecord.setFieldValue('custrecord_label_custom10', count
					+ " of " + totalcount);
			// template name as label name
			extlabelrecord.setFieldValue('custrecord_label_template', "Ilabel")

			var tranid = nlapiSubmitRecord(extlabelrecord);
		}

		// }

	} catch (exception) {
		nlapiLogExecution('DEBUG', 'Exception in generatecustomitemlabel',
				exception);
	}
}

// function PurchaseorderReslist(vebizOrdNo)
// {
// nlapiLogExecution('ERROR', 'into PurchaseorderReslist', vebizOrdNo);

// var filters = new Array();
// filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
// filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
// var columns = new Array();
// columns[0] = new nlobjSearchColumn('item');
// columns[1] = new nlobjSearchColumn('tranid');
// columns[2] = new nlobjSearchColumn('custrecord_lotno');
// columns[3] = new nlobjSearchColumn('lineno');
// columns[4] = new nlobjSearchColumn('licenceplatenum');
// columns[5] = new nlobjSearchColumn('custcol_ft_deliveryorderno');

// var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
// nlapiLogExecution('ERROR', 'trantype', trantype);
// var Purchaseorderlist = nlapiSearchRecord(trantype, null, filters, columns);
// return Purchaseorderlist ;
// }
function PurchaseorderReslist(line, item, poid) {
	try {
		nlapiLogExecution('ERROR', 'into PurchaseorderReslist', line + ","
				+ item + "," + poid);

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,
				'anyof', poid));
		// filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',
		// item));
		// filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is',
		// line));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,
				'anyof', [ '2' ]));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null,
				'anyof', [ '2' ]));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
		columns.push(new nlobjSearchColumn('custrecord_sku'));
		columns.push(new nlobjSearchColumn('custrecord_batch_no'));
		columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
		columns.push(new nlobjSearchColumn('custrecord_lpno'));
		columns.push(new nlobjSearchColumn('custrecord_line_no'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
		columns.push(new nlobjSearchColumn('name'));

		var searchresults = nlapiSearchRecord(
				'customrecord_ebiznet_trn_opentask', null, filters, columns);

		nlapiLogExecution('DEBUG', 'PurchaseorderReslist searchresults',
				searchresults);

		if (searchresults != null)
			nlapiLogExecution('DEBUG',
					'PurchaseorderReslist searchresults length',
					searchresults.length);
	} catch (exception) {
		nlapiLogExecution('DEBUG', 'Exception in PurchaseorderReslist',
				exception);
	}
	return searchresults;

}

function GetProductLabelSpecificPrintername(printertype) {
	try {
		nlapiLogExecution('DEBUG', 'into GetProductLabelSpecificPrintername',
				printertype);
		var printername = "";

		var printerfilter = new Array();
		var userId;
		userId = nlapiGetUser();
		if (userId == "-4") {
			userId = "";
		}
		nlapiLogExecution('Debug', 'DefaultUser', userId);
		var printerfilterarray = new Array();
		if (userId != null && userId != "") {

			printerfilterarray.push(new nlobjSearchFilter(
					'custrecord_printer_employee', null, 'anyof', userId));

		} else {
			printerfilterarray
					.push(new nlobjSearchFilter('custrecord_printer_carrier',
							null, 'is', "DefaultPrinter"));

		}
		printerfilter.push(new nlobjSearchFilter('name', null, 'is',
				printertype));
		var printercolumn = new Array();
		printercolumn.push(new nlobjSearchColumn(
				'custrecord_printer_printername'));
		var printersearchresults = nlapiSearchRecord(
				'customrecord_printer_preferences', null, printerfilter,
				printercolumn);
		if (printersearchresults != null && printersearchresults != "") {
			printername = printersearchresults[0]
					.getValue("custrecord_printer_printername");
		}

	} catch (exception) {
		nlapiLogExecution('DEBUG',
				'Exception in GetProductLabelSpecificPrintername', exception);
	}
	return printername;

}

// Getting quantity from Item dimensions
function fnGetitemweight(itemid) {
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof',
			[ itemid ]));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null,
			'anyof', [ 2 ]));

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');
	column[1] = new nlobjSearchColumn('custrecord_ebizqty');
	searchRec = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter,
			column);
	nlapiLogExecution('Debug', 'searchRec', searchRec);

	var casequantity = '';
	if (searchRec != null && searchRec != '') {
		casequantity = searchRec[0].getValue('custrecord_ebizqty');
	}
	nlapiLogExecution('Debug', 'casequantity', casequantity);
	return casequantity;

}
