function eBiz_Custom_fn() {} 
function GetProductLabelSpecificPrintername(printertype)
{
	var printername="";

	var printerfilter= new Array();
	var userId;
	userId = nlapiGetUser();
	if(userId=="-4")
	{
		userId="";
	}
	nlapiLogExecution('Debug', 'DefaultUser', userId);
	var printerfilterarray = new Array();
	if(userId!=null && userId!="")
	{

		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_employee', null, 'anyof', userId));

	}
	else
	{
		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_carrier', null, 'is', "DefaultPrinter"));

	}
	printerfilter.push(new nlobjSearchFilter('name', null, 'is', printertype));
	var printercolumn = new Array();
	printercolumn.push(new nlobjSearchColumn('custrecord_printer_printername'));
	var printersearchresults = nlapiSearchRecord('customrecord_printer_preferences', null, printerfilter,printercolumn);
	if(printersearchresults!=null && printersearchresults!="")
	{
		printername=printersearchresults[0].getValue("custrecord_printer_printername");
	}


	return printername;

}
/*
 * Generate Product Label function required Item Fulfillment Id Based on Item
 * Fulfillment id we need to get the End user shipping mark, Subsidiary Address,
 * Ship To Address, Item Fulfillment Number, No of carton, Date packed, and
 * Weight from Item Fulfillment
 */

function GenerateCustumerSpecificUccLabel(vebizOrdNo,getCartonLPNo,salesorderdetails,getFONO)
{

	nlapiLogExecution('DEBUG', "into generate ship mark label", "into generate ship mark label");
	nlapiLogExecution('DEBUG', 'vebizOrdNo',vebizOrdNo);
	nlapiLogExecution('DEBUG', 'getCartonLPNo',getCartonLPNo);
	nlapiLogExecution('DEBUG', 'getFONO',getFONO);

	var shipmarklabelrequired;
	var customerid=salesorderdetails.getFieldValue('entity');
	//var customerrec=nlapiLoadRecord('customer', customerid);
	//if(customerrec!=null && customerrec!='')
	//{

		//shipmarklabelrequired=customerrec.getFieldValue('custentity_ftisshippingmark');

	//}
	
 
	var fields = ['custentity_ftisshippingmark'];

	var columns = nlapiLookupField('customer',customerid,fields);
	shipmarklabelrequired = columns.custentity_ftisshippingmark;
	nlapiLogExecution('Debug', 'Into custentity_ftisshippingmark', shipmarklabelrequired);
	if(shipmarklabelrequired=='T')
	{
		/**
		 * Printer Name must be get the Printer Detail custom record. Based
		 * on Label Type and user Printer Name can be populated in -
		 * External Label printing custom record
		 * 
		 */
		var printername=GetProductLabelSpecificPrintername("ShipMarkLabel");
		/**
		 * Populate required information in Custom And Reference Fields.
		 * Populate LabelTemplate in LabelTemplate Custom field same
		 * external fields are used to populate. Based on Requirement we can
		 * use remaining fields
		 * 
		 */
		var shippingmark=salesorderdetails.getFieldValue('custbody_ftis_enduser_shippingmark');
		//var subsidiaryaddress=columns.subsidiaryaddress;
		var subsidiary=salesorderdetails.getFieldValue('subsidiary');
		//subsidiary address
		var subsidiaryaddress=nlapiLoadRecord('subsidiary',subsidiary);
		var address=subsidiaryaddress.getFieldValue('mainaddress_text');
		var newaddress=address.split('\r\n');

		var addresses=newaddress[0];
		var address1=newaddress[1];
		var address2=newaddress[2];
		var zip=newaddress[3];
		var country=newaddress[4];

		var shipaddressee,shipaddr1,shipaddr2,shipcity,shipcountry,shipstate,shipzip,shipphone,totalshipaddr;
		//ship address
		shipaddressee=salesorderdetails.getFieldValue('shipaddressee'); 
	 
		shipaddr1=salesorderdetails.getFieldValue('shipaddr1'); 
	 
		shipaddr2=salesorderdetails.getFieldValue('shipaddr2');
		shipcity=salesorderdetails.getFieldValue('shipcity'); 
		 
		shipcountry=salesorderdetails.getFieldValue('shipcountry'); 
		shipstate=salesorderdetails.getFieldValue('shipstate'); 
		shipzip=salesorderdetails.getFieldValue('shipzip'); 
		 
		shipphone=salesorderdetails.getFieldValue('shipphone'); 
      var salesOrder=salesorderdetails.getFieldValue('tranid'); 
		totalshipaddr=salesorderdetails.getFieldValue('shipaddress'); 
		
		if(totalshipaddr!=null && totalshipaddr!='')
			{
		totalshipaddr=totalshipaddr.replace(/<br\s*\/?>/mg,",");
			}
	 
		//end of ship to address
		var shipdate=salesorderdetails.getFieldValue('shipdate'); 

		var weight="";
		var wmslocation="";
		var wavenum="";
		var filters= new Array();

		//weight from open task
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
		filters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3])); 
		filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',['28'])); 
		filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is',getCartonLPNo)); 
		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_total_weight',null,'sum');
		columns[1] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		columns[2]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columns[3]=new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
		columns[4]=new nlobjSearchColumn('custrecord_sku', null, 'group');
		columns[5]=new nlobjSearchColumn('custrecord_ebiz_wave_no', null, 'group');
		var opentasksearchresults  = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(opentasksearchresults!=null && opentasksearchresults!='')
		{
			
			var ParentExt= nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
			for(var x=0;x<opentasksearchresults.length;x++)
			{
				weight=opentasksearchresults[x].getValue('custrecord_total_weight',null,'sum');
				wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location',null,'group');

				var ItemId= opentasksearchresults [x].getValue('custrecord_sku', null, 'group');
				 

				wavenum= opentasksearchresults [x].getValue('custrecord_ebiz_wave_no', null, 'group');
				 
				var itemname=opentasksearchresults [x].getText('custrecord_sku', null, 'group');

				var expqty=opentasksearchresults[x].getValue('custrecord_expe_qty',null,'sum');
				 
				var orignalexpqty=opentasksearchresults[x].getValue('custrecord_expe_qty',null,'sum');
				var qty=fnShipMarkGetitemweight(ItemId,wmslocation);
			 

				
				var totalqty=0;
				var caseqty=0
				if(qty>0)
				{
					caseqty=parseFloat(expqty)/parseFloat(qty);
					caseqty=Math.ceil(caseqty);
					totalqty=caseqty.toFixed();
				}
				
				if(totalqty<=0)
				{
					totalqty=1;
				}
				nlapiLogExecution('Debug', 'ftisshippingmark-TotalQty-caseqty-LabelQty', totalqty+""+qty+""+caseqty);
				var remainqty;

				var totalcount=totalqty;
				var count=0;

			 
				
				for (var i=0; i<totalqty;i++) 
				{
					count++;
					expqty=qty;
					if(i>0)
					{

						if(i==(totalqty-1))
						{
							expqty=parseInt(orignalexpqty)-parseInt(i*expqty);
						}
					}
					ParentExt.selectNewLineItem('recmachcustrecord_label_ext_parentlabel');
					//var ExternalLabelRecord = nlapiCreateRecord('customrecord_ext_labelprinting');
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','name',getFONO);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom1',shippingmark);
					var shipmarktext;
					
					if(shippingmark!=null && shippingmark!='')
					{
						
						shipmarktext= shippingmark; 
						
						ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom1',shipmarktext);
					}
					//subsidiary address

					//ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipfromaddressee',addresses);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipfromaddressee',newaddress);					
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_addr1',address1);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom7',address2);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_zip',zip);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_country',country);
					//Ship to address
					
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_order',salesOrder);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipaddressee',shipaddressee);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_reference1',totalshipaddr);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipaddr1',shipaddr1);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_addr2',shipaddr2);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipcity',shipcity);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipcountry',shipcountry);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipstate',shipstate);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_shipzip',shipzip);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom2',shipphone);
					//end of ship to address

					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_ebizorder',vebizOrdNo);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom3',getFONO);//FO NUmber
					//ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom4',getCartonLPNo);//LP Number
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom4',count +" of "+totalcount);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom5',shipdate);//Ship Date
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom6',weight);//Carton Weight
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_custom10',wavenum);//Wave#
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_ebizprinter',printername);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_labeltype',"ShipMarkLabel");
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_template',"ShipMarkLabel");
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_ext_location',wmslocation);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_item',itemname);
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_licenseplatenumber',getCartonLPNo);
					
					ParentExt.setCurrentLineItemValue('recmachcustrecord_label_ext_parentlabel','custrecord_label_printoption',"T"); // AMU 07/01/2016 No shipping mark to print by default.
					
					//var tranid = nlapiSubmitRecord(ExternalLabelRecord);
					//nlapiLogExecution('DEBUG', 'internalid', tranid);	
					ParentExt.commitLineItem('recmachcustrecord_label_ext_parentlabel');
				}
			}
			
			var submitid=nlapiSubmitRecord(ParentExt);
			nlapiLogExecution('Debug', 'Bulk-submitid', submitid);
		}
	}

}

//Getting case quantity  from Item dimensions
function fnShipMarkGetitemweight(itemid,location){
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', [itemid]));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	filter.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', [2]));	
	if(location!=null && location!='')
	{
		filter.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@',location]));	
	}

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;	
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	nlapiLogExecution('Debug', 'searchRec',searchRec);

	var casequantity=0;
	if(searchRec!=null && searchRec!='')
	{
		casequantity=searchRec[0].getValue('custrecord_ebizqty');		
	}
	nlapiLogExecution('Debug', 'casequantity',casequantity);
	return casequantity;

}
